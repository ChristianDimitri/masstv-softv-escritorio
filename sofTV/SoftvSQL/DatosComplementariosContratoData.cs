using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;

namespace Softv.DAO
{
  /// <summary>
  /// Class                   : Softv.DAO.DatosComplementariosContratoData
  /// Generated by            : Desarroll@, Class Generator (c) 2009
  /// Description             : DatosComplementariosContrato Data Access Object
  /// File                    : DatosComplementariosContratoDAO.cs
  /// Creation date           : 18/06/2011
  /// Creation time           : 10:41:54 a.m.
  /// </summary>
  public class DatosComplementariosContratoData : DatosComplementariosContratoProvider
  {
      /// <summary>
      /// </summary>
      /// <param name="DatosComplementariosContrato">Object DatosComplementariosContrato added to list</param>
      public override int AddDatosComplementariosContrato(DatosComplementariosContratoEntity entity_DatosComplementariosContrato)
      {
          int result=0;
          using(SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DatosComplementariosContrato.ConnectionString))
          {
              SqlCommand comandoSql = new SqlCommand("Softv_AddDatosComplementariosContrato", connection);
              comandoSql.CommandType = CommandType.StoredProcedure;

              comandoSql.Parameters.Add("@idDatosComplementarios", SqlDbType.BigInt);
              comandoSql.Parameters["@idDatosComplementarios"].Direction = ParameterDirection.Output;

               comandoSql.Parameters.AddWithValue("@Contrato", (object)entity_DatosComplementariosContrato.Contrato ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@NoFolio", (object)entity_DatosComplementariosContrato.NoFolio ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@CantidadNumerica", (object)entity_DatosComplementariosContrato.CantidadNumerica ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@CantidadConLetra", String.IsNullOrEmpty(entity_DatosComplementariosContrato.CantidadConLetra) ? DBNull.Value : (object)entity_DatosComplementariosContrato.CantidadConLetra );
               comandoSql.Parameters.AddWithValue("@SerieFinalPagare", (object)entity_DatosComplementariosContrato.SerieFinalPagare ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@Fecha", (object)entity_DatosComplementariosContrato.Fecha ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@Complementario1", String.IsNullOrEmpty(entity_DatosComplementariosContrato.Complementario1) ? DBNull.Value : (object)entity_DatosComplementariosContrato.Complementario1 );
               comandoSql.Parameters.AddWithValue("@Porcentaje", (object)entity_DatosComplementariosContrato.Porcentaje ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@Clv_Session", (object)entity_DatosComplementariosContrato.Clv_Session ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@Clv_Factura", (object)entity_DatosComplementariosContrato.Clv_Factura ?? (object)DBNull.Value);

              try
              {
                  if (connection.State == ConnectionState.Closed)
                      connection.Open();
                  result = ExecuteNonQuery(comandoSql);
              }
              catch (Exception ex)
              {
                  throw new Exception("Error adding DatosComplementariosContrato " + ex.Message, ex);
              }
              finally
              {
                  connection.Close();
              }
              if (result > 0)
                  result = int.Parse (comandoSql.Parameters["@idDatosComplementarios"].Value.ToString());
          }
          return result;

      }

      /// <summary>
      /// Deletes a DatosComplementariosContrato
      /// </summary>
      /// <param name="System.String[]">DatosComplementariosContratoid to delete</param>
      public override int DeleteDatosComplementariosContrato(long? idDatosComplementarios)
      {
          int result=0;
          using(SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DatosComplementariosContrato.ConnectionString))
          {
              SqlCommand comandoSql = new SqlCommand("Softv_DeleteDatosComplementariosContrato", connection);
              comandoSql.CommandType = CommandType.StoredProcedure;
              comandoSql.Parameters.AddWithValue("@idDatosComplementarios", idDatosComplementarios);

              try
              {
                  if (connection.State == ConnectionState.Closed)
                     connection.Open();
                  result = ExecuteNonQuery(comandoSql);
              }
              catch (Exception ex)
              {
                  throw new Exception("Error deleting DatosComplementariosContrato " + ex.Message, ex);
              }
              finally
              {
                  if(connection != null)
                  connection.Close();
              }
          }
          return result;

      }

      /// <summary>
      /// Edits a DatosComplementariosContrato
      /// </summary>
      /// <param name="DatosComplementariosContrato">Objeto DatosComplementariosContrato a editar</param>
      public override int EditDatosComplementariosContrato(DatosComplementariosContratoEntity entity_DatosComplementariosContrato)
      {
          int result=0;
          using(SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DatosComplementariosContrato.ConnectionString))
          {
              SqlCommand comandoSql = new SqlCommand("Softv_EditDatosComplementariosContrato", connection);
              comandoSql.CommandType = CommandType.StoredProcedure;

               comandoSql.Parameters.AddWithValue("@idDatosComplementarios", (object)entity_DatosComplementariosContrato.idDatosComplementarios ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@Contrato", (object)entity_DatosComplementariosContrato.Contrato ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@NoFolio", (object)entity_DatosComplementariosContrato.NoFolio ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@CantidadNumerica", (object)entity_DatosComplementariosContrato.CantidadNumerica ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@CantidadConLetra", String.IsNullOrEmpty(entity_DatosComplementariosContrato.CantidadConLetra) ? DBNull.Value : (object)entity_DatosComplementariosContrato.CantidadConLetra );
               comandoSql.Parameters.AddWithValue("@SerieFinalPagare", (object)entity_DatosComplementariosContrato.SerieFinalPagare ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@Fecha", (object)entity_DatosComplementariosContrato.Fecha ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@Complementario1", String.IsNullOrEmpty(entity_DatosComplementariosContrato.Complementario1) ? DBNull.Value : (object)entity_DatosComplementariosContrato.Complementario1 );
               comandoSql.Parameters.AddWithValue("@Porcentaje", (object)entity_DatosComplementariosContrato.Porcentaje ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@Clv_Session", (object)entity_DatosComplementariosContrato.Clv_Session ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@Clv_Factura", (object)entity_DatosComplementariosContrato.Clv_Factura ?? (object)DBNull.Value);

              try
              {
                  if (connection.State == ConnectionState.Closed)
                      connection.Open();
                  result = ExecuteNonQuery(comandoSql);
              }
              catch (Exception ex)
              {
                  throw new Exception("Error updating DatosComplementariosContrato " + ex.Message, ex);
              }
              finally
              {
                  if(connection != null)
                      connection.Close();
              }
          }
          return result;
      }

      /// <summary>
      /// Gets all DatosComplementariosContrato
      /// </summary>
      public override List<DatosComplementariosContratoEntity> GetDatosComplementariosContrato()
      {
          List<DatosComplementariosContratoEntity> DatosComplementariosContratoList = new List<DatosComplementariosContratoEntity>();
          using(SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DatosComplementariosContrato.ConnectionString))
          {
              SqlCommand comandoSql = new SqlCommand("Softv_GetDatosComplementariosContrato", connection);
              comandoSql.CommandType = CommandType.StoredProcedure;

              IDataReader rd = null;
              try
              {
                  if (connection.State == ConnectionState.Closed)
                  connection.Open();
                  rd = ExecuteReader(comandoSql);

                  while (rd.Read())
                  {
                      DatosComplementariosContratoList.Add(GetDatosComplementariosContratoFromReader(rd));
                  }
              }
              catch (Exception ex)
              {
                  throw new Exception("Error getting data DatosComplementariosContrato "  + ex.Message, ex);
              }
              finally
              {
                  if(connection!=null)
                      connection.Close();
                  if(rd != null)
                      rd.Close();
              }
          }
          return DatosComplementariosContratoList;
      }

      /// <summary>
      /// Gets DatosComplementariosContrato by System.String[]
      /// </summary>
      /// <param name="System.String[]">Id del DatosComplementariosContrato a consultar</param>
      public override DatosComplementariosContratoEntity GetDatosComplementariosContratoById(long? idDatosComplementarios)
      {
          using(SqlConnection connection = new SqlConnection(SoftvSettings.Settings.DatosComplementariosContrato.ConnectionString))
          {
              SqlCommand comandoSql = new SqlCommand("Softv_GetDatosComplementariosContratoById", connection);
              DatosComplementariosContratoEntity entity_DatosComplementariosContrato = null;
              comandoSql.CommandType = CommandType.StoredProcedure;

              comandoSql.Parameters.AddWithValue("@idDatosComplementarios", idDatosComplementarios);

              IDataReader rd = null;
              try
              {
                  if (connection.State == ConnectionState.Closed)
                      connection.Open();
                  rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                  if (rd.Read())
                      entity_DatosComplementariosContrato = GetDatosComplementariosContratoFromReader(rd);
              }
              catch (Exception ex)
              {
                  throw new Exception("Error getting data DatosComplementariosContrato "  + ex.Message, ex);
              }
              finally
              {
                  if(connection!=null)
                      connection.Close();
                  if(rd != null)
                      rd.Close();
              }
              return entity_DatosComplementariosContrato;
          }
      }
  }
}
