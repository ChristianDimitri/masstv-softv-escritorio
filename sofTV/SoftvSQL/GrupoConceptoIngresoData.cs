using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;

namespace Softv.DAO
{
  /// <summary>
  /// Class                   : Softv.DAO.GrupoConceptoIngresoData
  /// Generated by            : Desarroll@, Class Generator (c) 2009
  /// Description             : GrupoConceptoIngreso Data Access Object
  /// File                    : GrupoConceptoIngresoDAO.cs
  /// Creation date           : 28/07/2011
  /// Creation time           : 11:10:57 a.m.
  /// </summary>
  public class GrupoConceptoIngresoData : GrupoConceptoIngresoProvider
  {
      /// <summary>
      /// </summary>
      /// <param name="GrupoConceptoIngreso">Object GrupoConceptoIngreso added to list</param>
      public override int AddGrupoConceptoIngreso(GrupoConceptoIngresoEntity entity_GrupoConceptoIngreso)
      {
          int result=0;
          using(SqlConnection connection = new SqlConnection(SoftvSettings.Settings.GrupoConceptoIngreso.ConnectionString))
          {
              SqlCommand comandoSql = new SqlCommand("Softv_AddGrupoConceptoIngreso", connection);
              comandoSql.CommandType = CommandType.StoredProcedure;

              comandoSql.Parameters.Add("@IdGrupo", SqlDbType.Int);
              comandoSql.Parameters["@IdGrupo"].Direction = ParameterDirection.Output;

               comandoSql.Parameters.AddWithValue("@Concepto", String.IsNullOrEmpty(entity_GrupoConceptoIngreso.Concepto) ? DBNull.Value : (object)entity_GrupoConceptoIngreso.Concepto );
               comandoSql.Parameters.AddWithValue("@Posicion", (object)entity_GrupoConceptoIngreso.Posicion ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@Op", (object)entity_GrupoConceptoIngreso.Op ?? (object)DBNull.Value);

              try
              {
                  if (connection.State == ConnectionState.Closed)
                      connection.Open();
                  result = ExecuteNonQuery(comandoSql);
              }
              catch (Exception ex)
              {
                  throw new Exception("Error adding GrupoConceptoIngreso " + ex.Message, ex);
              }
              finally
              {
                  connection.Close();
              }
              if (result > 0)
                  result = (int)comandoSql.Parameters["@IdGrupo"].Value;
          }
          return result;

      }

      /// <summary>
      /// Deletes a GrupoConceptoIngreso
      /// </summary>
      /// <param name="System.String[]">GrupoConceptoIngresoid to delete</param>
      public override int DeleteGrupoConceptoIngreso(int? IdGrupo)
      {
          int result=0;
          using(SqlConnection connection = new SqlConnection(SoftvSettings.Settings.GrupoConceptoIngreso.ConnectionString))
          {
              SqlCommand comandoSql = new SqlCommand("Softv_DeleteGrupoConceptoIngreso", connection);
              comandoSql.CommandType = CommandType.StoredProcedure;
              comandoSql.Parameters.AddWithValue("@IdGrupo", IdGrupo);

              try
              {
                  if (connection.State == ConnectionState.Closed)
                     connection.Open();
                  result = ExecuteNonQuery(comandoSql);
              }
              catch (Exception ex)
              {
                  throw new Exception("Error deleting GrupoConceptoIngreso " + ex.Message, ex);
              }
              finally
              {
                  if(connection != null)
                  connection.Close();
              }
          }
          return result;

      }

      /// <summary>
      /// Edits a GrupoConceptoIngreso
      /// </summary>
      /// <param name="GrupoConceptoIngreso">Objeto GrupoConceptoIngreso a editar</param>
      public override int EditGrupoConceptoIngreso(GrupoConceptoIngresoEntity entity_GrupoConceptoIngreso)
      {
          int result=0;
          using(SqlConnection connection = new SqlConnection(SoftvSettings.Settings.GrupoConceptoIngreso.ConnectionString))
          {
              SqlCommand comandoSql = new SqlCommand("Softv_EditGrupoConceptoIngreso", connection);
              comandoSql.CommandType = CommandType.StoredProcedure;

               comandoSql.Parameters.AddWithValue("@IdGrupo", (object)entity_GrupoConceptoIngreso.IdGrupo ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@Concepto", String.IsNullOrEmpty(entity_GrupoConceptoIngreso.Concepto) ? DBNull.Value : (object)entity_GrupoConceptoIngreso.Concepto );
               comandoSql.Parameters.AddWithValue("@Posicion", (object)entity_GrupoConceptoIngreso.Posicion ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@Op", (object)entity_GrupoConceptoIngreso.Op ?? (object)DBNull.Value);

              try
              {
                  if (connection.State == ConnectionState.Closed)
                      connection.Open();
                  result = ExecuteNonQuery(comandoSql);
              }
              catch (Exception ex)
              {
                  throw new Exception("Error updating GrupoConceptoIngreso " + ex.Message, ex);
              }
              finally
              {
                  if(connection != null)
                      connection.Close();
              }
          }
          return result;
      }

      /// <summary>
      /// Gets all GrupoConceptoIngreso
      /// </summary>
      public override List<GrupoConceptoIngresoEntity> GetGrupoConceptoIngreso()
      {
          List<GrupoConceptoIngresoEntity> GrupoConceptoIngresoList = new List<GrupoConceptoIngresoEntity>();
          using(SqlConnection connection = new SqlConnection(SoftvSettings.Settings.GrupoConceptoIngreso.ConnectionString))
          {
              SqlCommand comandoSql = new SqlCommand("Softv_GetGrupoConceptoIngreso", connection);
              comandoSql.CommandType = CommandType.StoredProcedure;

              IDataReader rd = null;
              try
              {
                  if (connection.State == ConnectionState.Closed)
                  connection.Open();
                  rd = ExecuteReader(comandoSql);

                  while (rd.Read())
                  {
                      GrupoConceptoIngresoList.Add(GetGrupoConceptoIngresoFromReader(rd));
                  }
              }
              catch (Exception ex)
              {
                  throw new Exception("Error getting data GrupoConceptoIngreso "  + ex.Message, ex);
              }
              finally
              {
                  if(connection!=null)
                      connection.Close();
                  if(rd != null)
                      rd.Close();
              }
          }
          return GrupoConceptoIngresoList;
      }

      /// <summary>
      /// Gets GrupoConceptoIngreso by System.String[]
      /// </summary>
      /// <param name="System.String[]">Id del GrupoConceptoIngreso a consultar</param>
      public override GrupoConceptoIngresoEntity GetGrupoConceptoIngresoById(int? IdGrupo)
      {
          using(SqlConnection connection = new SqlConnection(SoftvSettings.Settings.GrupoConceptoIngreso.ConnectionString))
          {
              SqlCommand comandoSql = new SqlCommand("Softv_GetGrupoConceptoIngresoById", connection);
              GrupoConceptoIngresoEntity entity_GrupoConceptoIngreso = null;
              comandoSql.CommandType = CommandType.StoredProcedure;

              comandoSql.Parameters.AddWithValue("@IdGrupo", IdGrupo);

              IDataReader rd = null;
              try
              {
                  if (connection.State == ConnectionState.Closed)
                      connection.Open();
                  rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                  if (rd.Read())
                      entity_GrupoConceptoIngreso = GetGrupoConceptoIngresoFromReader(rd);
              }
              catch (Exception ex)
              {
                  throw new Exception("Error getting data GrupoConceptoIngreso "  + ex.Message, ex);
              }
              finally
              {
                  if(connection!=null)
                      connection.Close();
                  if(rd != null)
                      rd.Close();
              }
              return entity_GrupoConceptoIngreso;
          }
      }
  }
}
