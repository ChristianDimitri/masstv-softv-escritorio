﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace SoftvConfiguration
{
    public class SoftvSection : ConfigurationSection
    {
        /// <summary>
        /// Gets default connection String. If it doesn't exist then
        /// returns global connection string
        /// </summary>
        [ConfigurationProperty("DefaultConnectionString")]
        public String ConnectionString
        {
            get
            {
                string connectionString = (string)base["DefaultConnectionString"];
                connectionString = String.IsNullOrEmpty(connectionString) ?
                    Globals.DataAccess.GlobalConectionString :
                    (string)base["DefaultConnectionString"];
                return connectionString;
            }
        }

        /// <summary>
        /// Gets default assembly name for TestSimulator data clases
        /// </summary>
        [ConfigurationProperty("DefaultAssembly", DefaultValue = "SoftvSQL")]
        public String Assembly
        {
            get { return (string)base["DefaultAssembly"]; }
        }

        /// <summary>
        /// Gets ProductividadParaTecnico configuration data
        /// </summary>
        [ConfigurationProperty("ProductividadParaTecnico")]
        public ProductividadParaTecnicoElement ProductividadParaTecnico
        {
            get { return (ProductividadParaTecnicoElement)base["ProductividadParaTecnico"]; }
        }

        /// <summary>
        /// Gets RangoLimiteDeCredito configuration data
        /// </summary>
        [ConfigurationProperty("RangoLimiteDeCredito")]
        public RangoLimiteDeCreditoElement RangoLimiteDeCredito
        {
            get { return (RangoLimiteDeCreditoElement)base["RangoLimiteDeCredito"]; }
        }

        /// <summary>
        /// Gets LimiteDeCredito configuration data
        /// </summary>
        [ConfigurationProperty("LimiteDeCredito")]
        public LimiteDeCreditoElement LimiteDeCredito
        {
            get { return (LimiteDeCreditoElement)base["LimiteDeCredito"]; }
        }

        /// <summary>
        /// Gets DetLimiteDeCredito configuration data
        /// </summary>
        [ConfigurationProperty("DetLimiteDeCredito")]
        public DetLimiteDeCreditoElement DetLimiteDeCredito
        {
            get { return (DetLimiteDeCreditoElement)base["DetLimiteDeCredito"]; }
        }

        /// <summary>
        /// Gets DetLimiteDeCredito configuration data
        /// </summary>
        [ConfigurationProperty("Colonia")]
        public ColoniaElement Colonia
        {
            get { return (ColoniaElement)base["Colonia"]; }
        }

        /// <summary>
        /// Gets Ciudad configuration data
        /// </summary>
        [ConfigurationProperty("Ciudad")]
        public CiudadElement Ciudad
        {
            get { return (CiudadElement)base["Ciudad"]; }
        }

        /// <summary>
        /// Gets Servicio configuration data
        /// </summary>
        [ConfigurationProperty("Servicio")]
        public ServicioElement Servicio
        {
            get { return (ServicioElement)base["Servicio"]; }
        }

        /// <summary>
        /// Gets Servicio configuration data
        /// </summary>
        [ConfigurationProperty("MesaControlUsuario")]
        public MesaControlUsuarioElement MesaControlUsuario
        {
            get { return (MesaControlUsuarioElement)base["MesaControlUsuario"]; }
        }

        /// <summary>
        /// Gets Cvecolciu configuration data
        /// </summary>
        [ConfigurationProperty("Cvecolciu")]
        public CvecolciuElement Cvecolciu
        {
            get { return (CvecolciuElement)base["Cvecolciu"]; }
        }

        /// <summary>
        /// Gets RelColoniasSer configuration data
        /// </summary>
        [ConfigurationProperty("RelColoniasSer")]
        public RelColoniasSerElement RelColoniasSer
        {
            get { return (RelColoniasSerElement)base["RelColoniasSer"]; }
        }

        /// <summary>
        /// Gets MotAtenTel configuration data
        /// </summary>
        [ConfigurationProperty("MotAtenTel")]
        public MotAtenTelElement MotAtenTel
        {
            get { return (MotAtenTelElement)base["MotAtenTel"]; }
        }
        [ConfigurationProperty("AtenTel")]
        public AtenTelElement AtenTel
        {
            get { return (AtenTelElement)base["AtenTel"]; }
        }
        /// <summary>
        /// Gets DescargaMaterial configuration data
        /// </summary>
        [ConfigurationProperty("DescargaMaterial")]
        public DescargaMaterialElement DescargaMaterial
        {
            get { return (DescargaMaterialElement)base["DescargaMaterial"]; }
        }

        /// <summary>
        /// Gets IncentivoPorContrato configuration data
        /// </summary>
        [ConfigurationProperty("IncentivoPorContrato")]
        public IncentivoPorContratoElement IncentivoPorContrato
        {
            get { return (IncentivoPorContratoElement)base["IncentivoPorContrato"]; }
        }

        /// <summary>
        /// Gets MetaIncentivo configuration data
        /// </summary>
        [ConfigurationProperty("MetaIncentivo")]
        public MetaIncentivoElement MetaIncentivo
        {
            get { return (MetaIncentivoElement)base["MetaIncentivo"]; }
        }

        /// <summary>
        /// Gets AtencionDeLlamadas configuration data
        /// </summary>
        [ConfigurationProperty("AtencionDeLlamadas")]
        public AtencionDeLlamadasElement AtencionDeLlamadas
        {
            get { return (AtencionDeLlamadasElement)base["AtencionDeLlamadas"]; }
        }

        /// <summary>
        /// Gets DatosComplementariosContrato configuration data
        /// </summary>
        [ConfigurationProperty("DatosComplementariosContrato")]
        public DatosComplementariosContratoElement DatosComplementariosContrato
        {
            get { return (DatosComplementariosContratoElement)base["DatosComplementariosContrato"]; }
        }

        /// <summary>
        /// Gets RelArticuloAccesorio configuration data
        /// </summary>
        [ConfigurationProperty("RelArticuloAccesorio")]
        public RelArticuloAccesorioElement RelArticuloAccesorio
        {
            get { return (RelArticuloAccesorioElement)base["RelArticuloAccesorio"]; }
        }

        /// <summary>
        /// Gets RelTipoCajaAccesorioInstalacion configuration data
        /// </summary>
        [ConfigurationProperty("RelTipoCajaAccesorioInstalacion")]
        public RelTipoCajaAccesorioInstalacionElement RelTipoCajaAccesorioInstalacion
        {
            get { return (RelTipoCajaAccesorioInstalacionElement)base["RelTipoCajaAccesorioInstalacion"]; }
        }

        /// <summary>
        /// Gets PrioridadQueja configuration data
        /// </summary>
        [ConfigurationProperty("PrioridadQueja")]
        public PrioridadQuejaElement PrioridadQueja
        {
            get { return (PrioridadQuejaElement)base["PrioridadQueja"]; }
        }

        /// <summary>
        /// Gets PrioridadQueja configuration data
        /// </summary>
        [ConfigurationProperty("Trabajo")]
        public TrabajoElement Trabajo
        {
            get { return (TrabajoElement)base["Trabajo"]; }
        }

        /// <summary>
        /// Gets PrioridadQueja configuration data
        /// </summary>
        [ConfigurationProperty("RelMaterialTrabajo")]
        public RelMaterialTrabajoElement RelMaterialTrabajo
        {
            get { return (RelMaterialTrabajoElement)base["RelMaterialTrabajo"]; }
        }

        /// <summary>
        /// Gets GrupoConceptoIngreso configuration data
        /// </summary>
        [ConfigurationProperty("GrupoConceptoIngreso")]
        public GrupoConceptoIngresoElement GrupoConceptoIngreso
        {
            get { return (GrupoConceptoIngresoElement)base["GrupoConceptoIngreso"]; }
        }

        /// <summary>
        /// Gets GrupoIngreso configuration data
        /// </summary>
        [ConfigurationProperty("GrupoIngreso")]
        public GrupoIngresoElement GrupoIngreso
        {
            get { return (GrupoIngresoElement)base["GrupoIngreso"]; }
        }

    }
}
