using System;
using System.Configuration;

namespace SoftvConfiguration
{
  public class RelTipoCajaAccesorioInstalacionElement: ConfigurationElement
  {
      /// <summary>
      /// Gets assembly name for RelTipoCajaAccesorioInstalacion class
      /// </summary>
      [ConfigurationProperty( "Assembly")]
      public String Assembly
      {
          get
          {
              string assembly = (string)base["Assembly"];
              assembly = String.IsNullOrEmpty(assembly) ?
                  SoftvSettings.Settings.Assembly :
                  (string)base["Assembly"];
              return assembly;
          }
      }

      /// <summary>
      /// Gets class name for RelTipoCajaAccesorioInstalacion 
      /// </summary>
      [ConfigurationProperty("DataClass", DefaultValue = "Softv.DAO.RelTipoCajaAccesorioInstalacionData")]
      public String DataClass
      {
          get { return (string)base["DataClass"]; }
      }

      /// <summary>
      /// Gets connection string for database RelTipoCajaAccesorioInstalacion access
      /// </summary>
      [ConfigurationProperty("ConnectionString")]
      public String ConnectionString
      {
          get
          {
              string connectionString = (string)base["ConnectionString"];
              connectionString = String.IsNullOrEmpty(connectionString) ?
                  SoftvSettings.Settings.ConnectionString :
                  (string)base["ConnectionString"];
              return connectionString;
          }
      }
  }
}
