using System;
using System.Configuration;

namespace SoftvConfiguration
{
  public class GrupoConceptoIngresoElement: ConfigurationElement
  {
      /// <summary>
      /// Gets assembly name for GrupoConceptoIngreso class
      /// </summary>
      [ConfigurationProperty( "Assembly")]
      public String Assembly
      {
          get
          {
              string assembly = (string)base["Assembly"];
              assembly = String.IsNullOrEmpty(assembly) ?
                  SoftvSettings.Settings.Assembly :
                  (string)base["Assembly"];
              return assembly;
          }
      }

      /// <summary>
      /// Gets class name for GrupoConceptoIngreso 
      /// </summary>
      [ConfigurationProperty("DataClass", DefaultValue = "Softv.DAO.GrupoConceptoIngresoData")]
      public String DataClass
      {
          get { return (string)base["DataClass"]; }
      }

      /// <summary>
      /// Gets connection string for database GrupoConceptoIngreso access
      /// </summary>
      [ConfigurationProperty("ConnectionString")]
      public String ConnectionString
      {
          get
          {
              string connectionString = (string)base["ConnectionString"];
              connectionString = String.IsNullOrEmpty(connectionString) ?
                  SoftvSettings.Settings.ConnectionString :
                  (string)base["ConnectionString"];
              return connectionString;
          }
      }
  }
}
