using System;
using System.Configuration;

namespace SoftvConfiguration
{
  public class IncentivoPorContratoElement: ConfigurationElement
  {
      /// <summary>
      /// Gets assembly name for IncentivoPorContrato class
      /// </summary>
      [ConfigurationProperty( "Assembly")]
      public String Assembly
      {
          get
          {
              string assembly = (string)base["Assembly"];
              assembly = String.IsNullOrEmpty(assembly) ?
                  SoftvSettings.Settings.Assembly :
                  (string)base["Assembly"];
              return assembly;
          }
      }

      /// <summary>
      /// Gets class name for IncentivoPorContrato 
      /// </summary>
      [ConfigurationProperty("DataClass", DefaultValue = "Softv.DAO.IncentivoPorContratoData")]
      public String DataClass
      {
          get { return (string)base["DataClass"]; }
      }

      /// <summary>
      /// Gets connection string for database IncentivoPorContrato access
      /// </summary>
      [ConfigurationProperty("ConnectionString")]
      public String ConnectionString
      {
          get
          {
              string connectionString = (string)base["ConnectionString"];
              connectionString = String.IsNullOrEmpty(connectionString) ?
                  SoftvSettings.Settings.ConnectionString :
                  (string)base["ConnectionString"];
              return connectionString;
          }
      }
  }
}
