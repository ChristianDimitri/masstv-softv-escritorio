﻿Public Class FrmDepartamentos

    Public publicClvDepto As Long
    Public publicDescripcionDepto As String
    Public publicOpcionDepto As String

    Dim Depto As New ClassDepartamentos

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub FrmDepartamentos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)

        'SE LLENA EL FORMULARIO (INICIO)
        llenaFormulario()
        'SE LLENA EL FORMULARIO (FIN)
    End Sub

    Private Sub llenaFormulario()
        If publicOpcionDepto = "C" Or publicOpcionDepto = "M" Then
            'SE LLENAN LOS TXT CON LA INFORMACIÓN DE DEPARTAMENTO ANTERIORMENTE SELECCIONADO (INICIO)
            Me.txtClvDepto.Text = publicClvDepto
            Me.txtDescripcion.Text = publicDescripcionDepto
            'SE LLENAN LOS TXT CON LA INFORMACIÓN DE DEPARTAMENTO ANTERIORMENTE SELECCIONADO (FIN)
        End If

        'SE HABILITAN O DESHABILITAN LOS CAMPOS SEGÚN SEA LA OPCIÓN SELECCIONADA (INICIO)
        If publicOpcionDepto = "C" Then
            Me.txtDescripcion.ReadOnly = True
            Me.btnGuardar.Enabled = False
        Else
            Me.txtDescripcion.ReadOnly = False
            Me.btnGuardar.Enabled = True
        End If
        'SE HABILITAN O DESHABILITAN LOS CAMPOS SEGÚN SEA LA OPCIÓN SELECCIONADA (FIN)
    End Sub

    'MÉTODO PARA INSERTAR UN NUEVO REGISTRO 
    Private Sub Inserta(ByVal prmDescripcion As String)
        Me.txtClvDepto.Text = Depto.uspInsertaTblDepartamentos(prmDescripcion)

        MsgBox("Registro Almacenado Satisfactoriamente", MsgBoxStyle.Information)
        Me.Close()
    End Sub

    'MÉTODO PARA MODIFICAR UN REGISTRO 
    Private Sub Modifica(ByVal prmClvDepto As Long, ByVal prmDescripcion As String)
        Depto.uspModificaTblDepartamentos(prmClvDepto, prmDescripcion)

        MsgBox("Registro Almacenado Satisfactoriamente", MsgBoxStyle.Information)
        Me.Close()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        'SE VALIDA QUE HAYAN INGRESADO UNA DESCRIPCIÓN (INICIO)
        If Len(Me.txtDescripcion.Text) = 0 Then
            MsgBox("Capture la Descripción del Departamento a Registrar", MsgBoxStyle.Information)
            Exit Sub
        End If
        'SE VALIDA QUE HAYAN INGRESADO UNA DESCRIPCIÓN (FIN)

        'SE VALIDA LA OPCION A REALIZAR Y SE MANDA A MODIFICAR O INSERTAR SEGÚN SEA EL CASO (INICIO)
        If publicOpcionDepto = "N" Then
            Inserta(Me.txtDescripcion.Text)
        ElseIf publicOpcionDepto = "M" Then
            Modifica(CLng(Me.txtClvDepto.Text), Me.txtDescripcion.Text)
        End If
        'SE VALIDA LA OPCION A REALIZAR Y SE MANDA A MODIFICAR O INSERTAR SEGÚN SEA EL CASO (FIN)
    End Sub
End Class