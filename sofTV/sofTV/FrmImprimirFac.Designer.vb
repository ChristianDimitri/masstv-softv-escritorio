<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmImprimirFac
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2()
        Me.Dame_Servicios_SeleccionadosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_Servicios_SeleccionadosTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Dame_Servicios_SeleccionadosTableAdapter()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_Servicios_SeleccionadosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CrystalReportViewer1
        '
        Me.CrystalReportViewer1.ActiveViewIndex = -1
        Me.CrystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CrystalReportViewer1.DisplayGroupTree = False
        Me.CrystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrystalReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
        Me.CrystalReportViewer1.SelectionFormula = ""
        Me.CrystalReportViewer1.ShowGroupTreeButton = False
        Me.CrystalReportViewer1.Size = New System.Drawing.Size(1016, 713)
        Me.CrystalReportViewer1.TabIndex = 1
        Me.CrystalReportViewer1.ViewTimeSelectionFormula = ""
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Dame_Servicios_SeleccionadosBindingSource
        '
        Me.Dame_Servicios_SeleccionadosBindingSource.DataMember = "Dame_Servicios_Seleccionados"
        Me.Dame_Servicios_SeleccionadosBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Dame_Servicios_SeleccionadosTableAdapter
        '
        Me.Dame_Servicios_SeleccionadosTableAdapter.ClearBeforeFill = True
        '
        'FrmImprimirFac
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1016, 713)
        Me.Controls.Add(Me.CrystalReportViewer1)
        Me.Name = "FrmImprimirFac"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Imprimir Factura"
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_Servicios_SeleccionadosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents Dame_Servicios_SeleccionadosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_Servicios_SeleccionadosTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Dame_Servicios_SeleccionadosTableAdapter
    Private WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
End Class
