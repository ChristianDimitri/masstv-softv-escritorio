<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmIAPARAsigna
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CLV_CABLEMODEMLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmIAPARAsigna))
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.CONCAPARBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.CONCAPARBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.CONCAPARBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.NoFuncionaCheckBox = New System.Windows.Forms.CheckBox()
        Me.CLV_CABLEMODEMNewTextBox = New System.Windows.Forms.TextBox()
        Me.MaccablemodemNew = New System.Windows.Forms.TextBox()
        Me.Contratonet2 = New System.Windows.Forms.TextBox()
        Me.MacCablemodem2 = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.CajasTextBox = New System.Windows.Forms.TextBox()
        Me.CMBLabel5 = New System.Windows.Forms.Label()
        Me.CONIAPARBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.CONIAPARBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.CONIAPARBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.MacCableModemTextBox = New System.Windows.Forms.TextBox()
        Me.CLV_CABLEMODEMTextBox = New System.Windows.Forms.TextBox()
        Me.Contratonet1 = New System.Windows.Forms.TextBox()
        Me.ContratonetTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_OrdenTextBox = New System.Windows.Forms.TextBox()
        Me.MacCablemodem1 = New System.Windows.Forms.TextBox()
        Me.ClaveTextBox = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TreeView2 = New System.Windows.Forms.TreeView()
        Me.MUESTRAICAM_porSOLDIGBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRAICAM_porSOLDIGTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRAICAM_porSOLDIGTableAdapter()
        Me.MUESTRACONTDIGBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACONTDIGTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACONTDIGTableAdapter()
        Me.MUESTRACABLEMODEMSDELCLI_porOpcionDIGBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACABLEMODEMSDELCLI_porOpcionDIGTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACABLEMODEMSDELCLI_porOpcionDIGTableAdapter()
        Me.CONIAPARTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONIAPARTableAdapter()
        Me.BORDetOrdSer_INTELIGENTEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BORDetOrdSer_INTELIGENTETableAdapter = New sofTV.NewSofTvDataSetTableAdapters.BORDetOrdSer_INTELIGENTETableAdapter()
        Me.MUESTRACABLEMODEMS_disponiblesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACABLEMODEMS_disponiblesTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACABLEMODEMS_disponiblesTableAdapter()
        Me.CONCAPARTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONCAPARTableAdapter()
        Me.BORCAPARBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BORCAPARTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.BORCAPARTableAdapter()
        Me.NUECAPARBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NUECAPARTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.NUECAPARTableAdapter()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        CLV_CABLEMODEMLabel = New System.Windows.Forms.Label()
        Me.Panel2.SuspendLayout()
        CType(Me.CONCAPARBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONCAPARBindingNavigator.SuspendLayout()
        CType(Me.CONCAPARBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.CONIAPARBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONIAPARBindingNavigator.SuspendLayout()
        CType(Me.CONIAPARBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAICAM_porSOLDIGBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACONTDIGBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACABLEMODEMSDELCLI_porOpcionDIGBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BORDetOrdSer_INTELIGENTEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACABLEMODEMS_disponiblesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BORCAPARBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NUECAPARBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'CLV_CABLEMODEMLabel
        '
        CLV_CABLEMODEMLabel.AutoSize = True
        CLV_CABLEMODEMLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CLV_CABLEMODEMLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CLV_CABLEMODEMLabel.Location = New System.Drawing.Point(20, 81)
        CLV_CABLEMODEMLabel.Name = "CLV_CABLEMODEMLabel"
        CLV_CABLEMODEMLabel.Size = New System.Drawing.Size(104, 15)
        CLV_CABLEMODEMLabel.TabIndex = 24
        CLV_CABLEMODEMLabel.Text = "Tarjeta Nuevo :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label2.Location = New System.Drawing.Point(22, 143)
        Me.Label2.Name = "Label2"
        Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label2.Size = New System.Drawing.Size(150, 15)
        Me.Label2.TabIndex = 34
        Me.Label2.Text = "Tarjeta Digital Nuevo :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label3.Location = New System.Drawing.Point(22, 36)
        Me.Label3.Name = "Label3"
        Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label3.Size = New System.Drawing.Size(153, 15)
        Me.Label3.TabIndex = 29
        Me.Label3.Text = "Tarjeta Digital Actual  :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label1.Location = New System.Drawing.Point(22, 36)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(79, 15)
        Me.Label1.TabIndex = 29
        Me.Label1.Text = "Asignar a  :"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel2.Controls.Add(Me.Button1)
        Me.Panel2.Controls.Add(Me.CONCAPARBindingNavigator)
        Me.Panel2.Controls.Add(Me.CheckBox1)
        Me.Panel2.Controls.Add(Me.NoFuncionaCheckBox)
        Me.Panel2.Controls.Add(Me.CLV_CABLEMODEMNewTextBox)
        Me.Panel2.Controls.Add(Me.MaccablemodemNew)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.Contratonet2)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.MacCablemodem2)
        Me.Panel2.Location = New System.Drawing.Point(333, 28)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Panel2.Size = New System.Drawing.Size(440, 262)
        Me.Panel2.TabIndex = 36
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.SystemColors.Control
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(25, 172)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(224, 30)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "&Tarjetas Disponibles"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'CONCAPARBindingNavigator
        '
        Me.CONCAPARBindingNavigator.AddNewItem = Nothing
        Me.CONCAPARBindingNavigator.BindingSource = Me.CONCAPARBindingSource
        Me.CONCAPARBindingNavigator.CountItem = Nothing
        Me.CONCAPARBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONCAPARBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.CONCAPARBindingNavigatorSaveItem})
        Me.CONCAPARBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONCAPARBindingNavigator.MoveFirstItem = Nothing
        Me.CONCAPARBindingNavigator.MoveLastItem = Nothing
        Me.CONCAPARBindingNavigator.MoveNextItem = Nothing
        Me.CONCAPARBindingNavigator.MovePreviousItem = Nothing
        Me.CONCAPARBindingNavigator.Name = "CONCAPARBindingNavigator"
        Me.CONCAPARBindingNavigator.PositionItem = Nothing
        Me.CONCAPARBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONCAPARBindingNavigator.Size = New System.Drawing.Size(440, 25)
        Me.CONCAPARBindingNavigator.TabIndex = 3
        Me.CONCAPARBindingNavigator.TabStop = True
        Me.CONCAPARBindingNavigator.Text = "BindingNavigator1"
        '
        'CONCAPARBindingSource
        '
        Me.CONCAPARBindingSource.DataMember = "CONCAPAR"
        Me.CONCAPARBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(165, 22)
        Me.BindingNavigatorDeleteItem.Text = "&BORRAR ASIGNACIÓN"
        '
        'CONCAPARBindingNavigatorSaveItem
        '
        Me.CONCAPARBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONCAPARBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONCAPARBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONCAPARBindingNavigatorSaveItem.Name = "CONCAPARBindingNavigatorSaveItem"
        Me.CONCAPARBindingNavigatorSaveItem.Size = New System.Drawing.Size(87, 22)
        Me.CONCAPARBindingNavigatorSaveItem.Text = "&ACEPTAR"
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CheckBox1.Location = New System.Drawing.Point(187, 74)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(112, 19)
        Me.CheckBox1.TabIndex = 0
        Me.CheckBox1.Text = "Esta Dañado "
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'NoFuncionaCheckBox
        '
        Me.NoFuncionaCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONCAPARBindingSource, "NoFunciona", True))
        Me.NoFuncionaCheckBox.Location = New System.Drawing.Point(111, 172)
        Me.NoFuncionaCheckBox.Name = "NoFuncionaCheckBox"
        Me.NoFuncionaCheckBox.Size = New System.Drawing.Size(104, 24)
        Me.NoFuncionaCheckBox.TabIndex = 39
        Me.NoFuncionaCheckBox.TabStop = False
        '
        'CLV_CABLEMODEMNewTextBox
        '
        Me.CLV_CABLEMODEMNewTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCAPARBindingSource, "CLV_CABLEMODEMNew", True))
        Me.CLV_CABLEMODEMNewTextBox.Location = New System.Drawing.Point(115, 172)
        Me.CLV_CABLEMODEMNewTextBox.Name = "CLV_CABLEMODEMNewTextBox"
        Me.CLV_CABLEMODEMNewTextBox.Size = New System.Drawing.Size(100, 20)
        Me.CLV_CABLEMODEMNewTextBox.TabIndex = 36
        '
        'MaccablemodemNew
        '
        Me.MaccablemodemNew.BackColor = System.Drawing.Color.Gainsboro
        Me.MaccablemodemNew.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MaccablemodemNew.CausesValidation = False
        Me.MaccablemodemNew.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCAPARBindingSource, "Maccablemodem2", True))
        Me.MaccablemodemNew.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaccablemodemNew.ForeColor = System.Drawing.Color.DarkRed
        Me.MaccablemodemNew.Location = New System.Drawing.Point(185, 144)
        Me.MaccablemodemNew.Name = "MaccablemodemNew"
        Me.MaccablemodemNew.ReadOnly = True
        Me.MaccablemodemNew.Size = New System.Drawing.Size(227, 19)
        Me.MaccablemodemNew.TabIndex = 1
        '
        'Contratonet2
        '
        Me.Contratonet2.Location = New System.Drawing.Point(61, 177)
        Me.Contratonet2.Name = "Contratonet2"
        Me.Contratonet2.Size = New System.Drawing.Size(48, 20)
        Me.Contratonet2.TabIndex = 32
        Me.Contratonet2.TabStop = False
        '
        'MacCablemodem2
        '
        Me.MacCablemodem2.BackColor = System.Drawing.Color.Gainsboro
        Me.MacCablemodem2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MacCablemodem2.CausesValidation = False
        Me.MacCablemodem2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MacCablemodem2.ForeColor = System.Drawing.Color.DarkRed
        Me.MacCablemodem2.Location = New System.Drawing.Point(185, 36)
        Me.MacCablemodem2.Name = "MacCablemodem2"
        Me.MacCablemodem2.ReadOnly = True
        Me.MacCablemodem2.Size = New System.Drawing.Size(239, 19)
        Me.MacCablemodem2.TabIndex = 100
        Me.MacCablemodem2.TabStop = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.SystemColors.Control
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(637, 296)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 4
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.CajasTextBox)
        Me.Panel1.Controls.Add(Me.CMBLabel5)
        Me.Panel1.Controls.Add(Me.CONIAPARBindingNavigator)
        Me.Panel1.Controls.Add(Me.Button2)
        Me.Panel1.Controls.Add(Me.MacCableModemTextBox)
        Me.Panel1.Controls.Add(Me.CLV_CABLEMODEMTextBox)
        Me.Panel1.Controls.Add(Me.Contratonet1)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.ContratonetTextBox)
        Me.Panel1.Controls.Add(Me.Clv_OrdenTextBox)
        Me.Panel1.Controls.Add(Me.MacCablemodem1)
        Me.Panel1.Controls.Add(Me.ClaveTextBox)
        Me.Panel1.Controls.Add(CLV_CABLEMODEMLabel)
        Me.Panel1.Location = New System.Drawing.Point(333, 28)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Panel1.Size = New System.Drawing.Size(431, 227)
        Me.Panel1.TabIndex = 34
        '
        'CajasTextBox
        '
        Me.CajasTextBox.BackColor = System.Drawing.Color.Gainsboro
        Me.CajasTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CajasTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CajasTextBox.ForeColor = System.Drawing.Color.DarkRed
        Me.CajasTextBox.Location = New System.Drawing.Point(145, 128)
        Me.CajasTextBox.Name = "CajasTextBox"
        Me.CajasTextBox.ReadOnly = True
        Me.CajasTextBox.Size = New System.Drawing.Size(224, 19)
        Me.CajasTextBox.TabIndex = 42
        Me.CajasTextBox.TabStop = False
        '
        'CMBLabel5
        '
        Me.CMBLabel5.AutoSize = True
        Me.CMBLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel5.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CMBLabel5.Location = New System.Drawing.Point(20, 128)
        Me.CMBLabel5.Name = "CMBLabel5"
        Me.CMBLabel5.Size = New System.Drawing.Size(107, 15)
        Me.CMBLabel5.TabIndex = 41
        Me.CMBLabel5.Text = "Caja Asignada :"
        '
        'CONIAPARBindingNavigator
        '
        Me.CONIAPARBindingNavigator.AddNewItem = Nothing
        Me.CONIAPARBindingNavigator.BindingSource = Me.CONIAPARBindingSource
        Me.CONIAPARBindingNavigator.CountItem = Nothing
        Me.CONIAPARBindingNavigator.DeleteItem = Me.ToolStripButton1
        Me.CONIAPARBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.CONIAPARBindingNavigatorSaveItem})
        Me.CONIAPARBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONIAPARBindingNavigator.MoveFirstItem = Nothing
        Me.CONIAPARBindingNavigator.MoveLastItem = Nothing
        Me.CONIAPARBindingNavigator.MoveNextItem = Nothing
        Me.CONIAPARBindingNavigator.MovePreviousItem = Nothing
        Me.CONIAPARBindingNavigator.Name = "CONIAPARBindingNavigator"
        Me.CONIAPARBindingNavigator.PositionItem = Nothing
        Me.CONIAPARBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONIAPARBindingNavigator.Size = New System.Drawing.Size(431, 25)
        Me.CONIAPARBindingNavigator.TabIndex = 40
        Me.CONIAPARBindingNavigator.Text = "BindingNavigator1"
        '
        'CONIAPARBindingSource
        '
        Me.CONIAPARBindingSource.DataMember = "CONIAPAR"
        Me.CONIAPARBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton1.Size = New System.Drawing.Size(140, 22)
        Me.ToolStripButton1.Text = "&BORRAR Aparato"
        '
        'CONIAPARBindingNavigatorSaveItem
        '
        Me.CONIAPARBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONIAPARBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONIAPARBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONIAPARBindingNavigatorSaveItem.Name = "CONIAPARBindingNavigatorSaveItem"
        Me.CONIAPARBindingNavigatorSaveItem.Size = New System.Drawing.Size(87, 22)
        Me.CONIAPARBindingNavigatorSaveItem.Text = "ACEPTAR"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.SystemColors.Control
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(25, 175)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(224, 30)
        Me.Button2.TabIndex = 38
        Me.Button2.TabStop = False
        Me.Button2.Text = "Tarjeta Disponibles"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'MacCableModemTextBox
        '
        Me.MacCableModemTextBox.BackColor = System.Drawing.Color.Gainsboro
        Me.MacCableModemTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MacCableModemTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONIAPARBindingSource, "MacCableModem", True))
        Me.MacCableModemTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MacCableModemTextBox.ForeColor = System.Drawing.Color.DarkRed
        Me.MacCableModemTextBox.Location = New System.Drawing.Point(146, 81)
        Me.MacCableModemTextBox.Name = "MacCableModemTextBox"
        Me.MacCableModemTextBox.ReadOnly = True
        Me.MacCableModemTextBox.Size = New System.Drawing.Size(224, 19)
        Me.MacCableModemTextBox.TabIndex = 39
        Me.MacCableModemTextBox.TabStop = False
        '
        'CLV_CABLEMODEMTextBox
        '
        Me.CLV_CABLEMODEMTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONIAPARBindingSource, "CLV_CABLEMODEM", True))
        Me.CLV_CABLEMODEMTextBox.Location = New System.Drawing.Point(25, 182)
        Me.CLV_CABLEMODEMTextBox.Name = "CLV_CABLEMODEMTextBox"
        Me.CLV_CABLEMODEMTextBox.Size = New System.Drawing.Size(100, 20)
        Me.CLV_CABLEMODEMTextBox.TabIndex = 30
        Me.CLV_CABLEMODEMTextBox.TabStop = False
        '
        'Contratonet1
        '
        Me.Contratonet1.Location = New System.Drawing.Point(24, 182)
        Me.Contratonet1.Name = "Contratonet1"
        Me.Contratonet1.Size = New System.Drawing.Size(101, 20)
        Me.Contratonet1.TabIndex = 26
        Me.Contratonet1.TabStop = False
        '
        'ContratonetTextBox
        '
        Me.ContratonetTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONIAPARBindingSource, "Contratonet", True))
        Me.ContratonetTextBox.Location = New System.Drawing.Point(25, 182)
        Me.ContratonetTextBox.Name = "ContratonetTextBox"
        Me.ContratonetTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ContratonetTextBox.TabIndex = 23
        Me.ContratonetTextBox.TabStop = False
        '
        'Clv_OrdenTextBox
        '
        Me.Clv_OrdenTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONIAPARBindingSource, "Clv_Orden", True))
        Me.Clv_OrdenTextBox.Location = New System.Drawing.Point(25, 179)
        Me.Clv_OrdenTextBox.Name = "Clv_OrdenTextBox"
        Me.Clv_OrdenTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Clv_OrdenTextBox.TabIndex = 21
        '
        'MacCablemodem1
        '
        Me.MacCablemodem1.BackColor = System.Drawing.Color.Gainsboro
        Me.MacCablemodem1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MacCablemodem1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MacCablemodem1.ForeColor = System.Drawing.Color.DarkRed
        Me.MacCablemodem1.Location = New System.Drawing.Point(115, 36)
        Me.MacCablemodem1.Name = "MacCablemodem1"
        Me.MacCablemodem1.ReadOnly = True
        Me.MacCablemodem1.Size = New System.Drawing.Size(239, 19)
        Me.MacCablemodem1.TabIndex = 27
        Me.MacCablemodem1.TabStop = False
        '
        'ClaveTextBox
        '
        Me.ClaveTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONIAPARBindingSource, "Clave", True))
        Me.ClaveTextBox.Location = New System.Drawing.Point(24, 182)
        Me.ClaveTextBox.Name = "ClaveTextBox"
        Me.ClaveTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ClaveTextBox.TabIndex = 19
        Me.ClaveTextBox.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label4.Location = New System.Drawing.Point(3, 1)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(192, 16)
        Me.Label4.TabIndex = 33
        Me.Label4.Text = "Por Asignar Tarjeta Digital"
        '
        'TreeView2
        '
        Me.TreeView2.BackColor = System.Drawing.Color.Gainsboro
        Me.TreeView2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TreeView2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView2.ForeColor = System.Drawing.Color.Navy
        Me.TreeView2.Location = New System.Drawing.Point(15, 28)
        Me.TreeView2.Name = "TreeView2"
        Me.TreeView2.Size = New System.Drawing.Size(303, 262)
        Me.TreeView2.TabIndex = 32
        Me.TreeView2.TabStop = False
        '
        'MUESTRAICAM_porSOLDIGBindingSource
        '
        Me.MUESTRAICAM_porSOLDIGBindingSource.DataMember = "MUESTRAICAM_porSOLDIG"
        Me.MUESTRAICAM_porSOLDIGBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRAICAM_porSOLDIGTableAdapter
        '
        Me.MUESTRAICAM_porSOLDIGTableAdapter.ClearBeforeFill = True
        '
        'MUESTRACONTDIGBindingSource
        '
        Me.MUESTRACONTDIGBindingSource.DataMember = "MUESTRACONTDIG"
        Me.MUESTRACONTDIGBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRACONTDIGTableAdapter
        '
        Me.MUESTRACONTDIGTableAdapter.ClearBeforeFill = True
        '
        'MUESTRACABLEMODEMSDELCLI_porOpcionDIGBindingSource
        '
        Me.MUESTRACABLEMODEMSDELCLI_porOpcionDIGBindingSource.DataMember = "MUESTRACABLEMODEMSDELCLI_porOpcionDIG"
        Me.MUESTRACABLEMODEMSDELCLI_porOpcionDIGBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRACABLEMODEMSDELCLI_porOpcionDIGTableAdapter
        '
        Me.MUESTRACABLEMODEMSDELCLI_porOpcionDIGTableAdapter.ClearBeforeFill = True
        '
        'CONIAPARTableAdapter
        '
        Me.CONIAPARTableAdapter.ClearBeforeFill = True
        '
        'BORDetOrdSer_INTELIGENTEBindingSource
        '
        Me.BORDetOrdSer_INTELIGENTEBindingSource.DataMember = "BORDetOrdSer_INTELIGENTE"
        Me.BORDetOrdSer_INTELIGENTEBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'BORDetOrdSer_INTELIGENTETableAdapter
        '
        Me.BORDetOrdSer_INTELIGENTETableAdapter.ClearBeforeFill = True
        '
        'MUESTRACABLEMODEMS_disponiblesBindingSource
        '
        Me.MUESTRACABLEMODEMS_disponiblesBindingSource.DataMember = "MUESTRACABLEMODEMS_disponibles"
        Me.MUESTRACABLEMODEMS_disponiblesBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRACABLEMODEMS_disponiblesTableAdapter
        '
        Me.MUESTRACABLEMODEMS_disponiblesTableAdapter.ClearBeforeFill = True
        '
        'CONCAPARTableAdapter
        '
        Me.CONCAPARTableAdapter.ClearBeforeFill = True
        '
        'BORCAPARBindingSource
        '
        Me.BORCAPARBindingSource.DataMember = "BORCAPAR"
        Me.BORCAPARBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'BORCAPARTableAdapter
        '
        Me.BORCAPARTableAdapter.ClearBeforeFill = True
        '
        'NUECAPARBindingSource
        '
        Me.NUECAPARBindingSource.DataMember = "NUECAPAR"
        Me.NUECAPARBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NUECAPARTableAdapter
        '
        Me.NUECAPARTableAdapter.ClearBeforeFill = True
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Location = New System.Drawing.Point(14, 6)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(303, 20)
        Me.Panel3.TabIndex = 37
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'FrmIAPARAsigna
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(793, 358)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.TreeView2)
        Me.Name = "FrmIAPARAsigna"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Asiganción de Tarjetas Digitales"
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.CONCAPARBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONCAPARBindingNavigator.ResumeLayout(False)
        Me.CONCAPARBindingNavigator.PerformLayout()
        CType(Me.CONCAPARBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.CONIAPARBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONIAPARBindingNavigator.ResumeLayout(False)
        Me.CONIAPARBindingNavigator.PerformLayout()
        CType(Me.CONIAPARBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAICAM_porSOLDIGBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACONTDIGBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACABLEMODEMSDELCLI_porOpcionDIGBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BORDetOrdSer_INTELIGENTEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACABLEMODEMS_disponiblesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BORCAPARBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NUECAPARBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents NoFuncionaCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents CLV_CABLEMODEMNewTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MaccablemodemNew As System.Windows.Forms.TextBox
    Friend WithEvents Contratonet2 As System.Windows.Forms.TextBox
    Friend WithEvents MacCablemodem2 As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents MacCableModemTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CLV_CABLEMODEMTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Contratonet1 As System.Windows.Forms.TextBox
    Friend WithEvents ContratonetTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_OrdenTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MacCablemodem1 As System.Windows.Forms.TextBox
    Friend WithEvents ClaveTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TreeView2 As System.Windows.Forms.TreeView
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents MUESTRAICAM_porSOLDIGBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAICAM_porSOLDIGTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRAICAM_porSOLDIGTableAdapter
    Friend WithEvents MUESTRACONTDIGBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACONTDIGTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACONTDIGTableAdapter
    Friend WithEvents MUESTRACABLEMODEMSDELCLI_porOpcionDIGBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACABLEMODEMSDELCLI_porOpcionDIGTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACABLEMODEMSDELCLI_porOpcionDIGTableAdapter
    Friend WithEvents CONIAPARBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONIAPARTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONIAPARTableAdapter
    Friend WithEvents BORDetOrdSer_INTELIGENTEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BORDetOrdSer_INTELIGENTETableAdapter As sofTV.NewSofTvDataSetTableAdapters.BORDetOrdSer_INTELIGENTETableAdapter
    Friend WithEvents MUESTRACABLEMODEMS_disponiblesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACABLEMODEMS_disponiblesTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACABLEMODEMS_disponiblesTableAdapter
    Friend WithEvents CONCAPARBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONCAPARTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONCAPARTableAdapter
    Friend WithEvents BORCAPARBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BORCAPARTableAdapter As sofTV.NewSofTvDataSetTableAdapters.BORCAPARTableAdapter
    Friend WithEvents NUECAPARBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NUECAPARTableAdapter As sofTV.NewSofTvDataSetTableAdapters.NUECAPARTableAdapter
    Friend WithEvents CONIAPARBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONIAPARBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONCAPARBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONCAPARBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CajasTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel5 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
