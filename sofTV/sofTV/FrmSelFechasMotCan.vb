﻿Imports System.Data.SqlClient
Imports System.Collections.Generic
Public Class FrmSelFechasMotCan
    Public Shared listParametros As New List(Of SqlParameter)

    Public Shared Sub limpiaParametros()
        listParametros = New List(Of SqlParameter)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        GloBndSelFecha = False
        locbndrepcancelaciones = False
        Me.Close()
    End Sub

    Public Shared Sub CreateMyParameter(ByVal prmNombre As String, ByRef prmTipo As SqlDbType, ByRef prmValor As Object)
        Dim par As New SqlParameter(prmNombre, prmTipo)
        par.Value = prmValor
        listParametros.Add(par)
    End Sub

    Public Function Consulta(ByVal procedimiento As String)
        Dim cnn As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand(procedimiento, cnn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        If listParametros.Count > 0 Then
            cmd.Parameters.AddRange(listParametros.ToArray())
        End If
        Dim DA As New SqlDataAdapter(cmd)
        Try
            cnn.Open()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            cnn.Close()
        End Try
    End Function

    Private Sub uspInsertaTodosSeleccionMotCan(ByVal prmClvSession As Long)
        limpiaParametros()
        CreateMyParameter("@Clv_session", SqlDbType.BigInt, prmClvSession)
        Consulta("uspInsertaTodosSeleccionMotCan")
    End Sub

    Private Sub uspInsertaTodosSeleccionMotCanTmp(ByVal prmClvSession As Long)
        limpiaParametros()
        CreateMyParameter("@Clv_session", SqlDbType.BigInt, prmClvSession)
        Consulta("uspInsertaTodosSeleccionMotCanTmp")
    End Sub

    Private Sub uspInsertaunoSeleccionMotCantmp(ByVal prmClvSession As Long, ByVal prmClvMonCan As Integer)
        limpiaParametros()
        CreateMyParameter("@Clv_session", SqlDbType.BigInt, prmClvSession)
        CreateMyParameter("@Clv_MotCan", SqlDbType.Int, prmClvMonCan)
        Consulta("uspInsertaunoSeleccionMotCantmp")
    End Sub

    Private Sub uspInsertaunoSeleccionMotCan(ByVal prmClvSession As Long, ByVal prmClvMonCan As Integer)
        limpiaParametros()
        CreateMyParameter("@Clv_session", SqlDbType.BigInt, prmClvSession)
        CreateMyParameter("@Clv_motcan", SqlDbType.Int, prmClvMonCan)
        Consulta("uspInsertaunoSeleccionMotCan")
    End Sub

    Private Sub uspMuestraSeleccionaMotCanTmpCONSULTA(ByVal prmClvSession As Long)
        Dim DT As DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_session", SqlDbType.BigInt, prmClvSession)
        Me.lbSelMotCan.DataSource = BaseII.ConsultaDT("uspMuestraSeleccionaMotCanTmpCONSULTA")
        Me.lbSelMotCan.DisplayMember = "MOTCAN"
        Me.lbSelMotCan.ValueMember = "Clv_Motcan"
        'DT = BaseII.ConsultaDT("uspMuestraSeleccionaMotCanTmpCONSULTA")
        'Dim I As Integer
        'For I = 0 To DT.Rows.Count - 1
        '    Me.lbSelMotCan.Items.Add(DT.Rows(I)(2).ToString)
        'Next
    End Sub

    Private Sub uspMuestraSeleccionaMonCanTmpNUEVO(ByVal prmClvSession As Long)
        limpiaParametros()
        CreateMyParameter("@Clv_session", SqlDbType.BigInt, prmClvSession)
        Consulta("uspMuestraSeleccionaMonCanTmpNUEVO")
    End Sub

    Private Sub uspMuestraSeleccionMotCanCONSULTA(ByVal prmClvSession As Long)
        Dim DT As DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_session", SqlDbType.BigInt, prmClvSession)
        Me.lbMotCanSel.DataSource = BaseII.ConsultaDT("uspMuestraSeleccionMotCanCONSULTA")
        Me.lbMotCanSel.DisplayMember = "MOTCAN"
        Me.lbMotCanSel.ValueMember = "Clv_Motcan"
        'DT = BaseII.ConsultaDT("uspMuestraSeleccionMotCanCONSULTA")
        'Dim I As Integer
        'For I = 0 To DT.Rows.Count - 1
        '    Me.lbMotCanSel.Items.Add(DT.Rows(I)(2).ToString)
        'Next
    End Sub


    Private Sub FrmSelFechasMotCan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        uspMuestraSeleccionaMonCanTmpNUEVO(LocClv_session)
        uspMuestraSeleccionaMotCanTmpCONSULTA(LocClv_session)

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        uspInsertaTodosSeleccionMotCan(LocClv_session)
        uspMuestraSeleccionMotCanCONSULTA(LocClv_session)
        uspMuestraSeleccionaMotCanTmpCONSULTA(LocClv_session)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        uspInsertaTodosSeleccionMotCanTmp(LocClv_session)
        uspMuestraSeleccionaMotCanTmpCONSULTA(LocClv_session)
        uspMuestraSeleccionMotCanCONSULTA(LocClv_session)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        uspInsertaunoSeleccionMotCantmp(LocClv_session, CInt(Me.lbSelMotCan.SelectedValue))
        uspMuestraSeleccionMotCanCONSULTA(LocClv_session)
        uspMuestraSeleccionaMotCanTmpCONSULTA(LocClv_session)
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        uspInsertaunoSeleccionMotCan(LocClv_session, CInt(Me.lbMotCanSel.SelectedValue))
        uspMuestraSeleccionaMotCanTmpCONSULTA(LocClv_session)
        uspMuestraSeleccionMotCanCONSULTA(LocClv_session)
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Dim x As Integer = 0
        x = Me.lbMotCanSel.Items.Count()
        If x = 0 Then
            MsgBox("Selecciona al menos un Motivo de cancelación", MsgBoxStyle.Information)
            Exit Sub
        End If
        GloFecha_Ini = Me.dtpFecIni.Text
        GloFecha_Fin = Me.dtpFecFin.Text
        If LocValidaHab = 0 Then
            GloFecha_Ini = Me.dtpFecIni.Text
            GloFecha_Fin = Me.dtpFecFin.Text
            FrmSelPeriodo.Show()
        Else
            GloFecha_Ini = Me.dtpFecIni.Text
            GloFecha_Fin = Me.dtpFecFin.Text
            If bnd_Canc_Sin_Mens = False Then
                bndfechareport = True
            ElseIf bnd_Canc_Sin_Mens = True Then
                bnd_Canc_Sin_Mens = False
                bnd_Canc_Sin_Mens_buena = True
            End If
        End If
        Me.Close()

    End Sub

    Private Sub dtpFecIni_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFecIni.ValueChanged
        'Me.dtpFecFin.MinDate = dtpFecFin.Value
    End Sub

End Class