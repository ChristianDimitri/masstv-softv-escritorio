﻿Imports System.Data.SqlClient
Public Class DAAtencionTelefonica

    'Genera la nueva Atención telefónica
    Public Shared Sub AgregaAtencionTelefonica(ByVal Clv_usuario As Integer, ByVal Contrato As Integer, ByVal Descripcion As String, ByVal Solucion As Integer, ByVal HoraInicial As String, ByVal HoraFin As String, ByVal Fecha As DateTime, ByVal Clv_trabajo As Integer, ByVal Clv_Queja As Integer, ByVal Clv_TipSer As Integer, ByVal idTurno As Integer)
        Dim conn As New SqlConnection(MiConexion)

        Try

            Dim comando As New SqlClient.SqlCommand("AgregaRelDetOrdSerRetiroAparato", conn)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 0

            comando.Parameters.Add("@clv_llamada", SqlDbType.Int)
            comando.Parameters("@clv_llamada").Direction = ParameterDirection.Output
            comando.Parameters("@Contclv_llamadarato").Value = 0

            comando.Parameters.Add("@clv_Usuario", SqlDbType.Int)
            comando.Parameters("@clv_Usuario").Direction = ParameterDirection.Input
            comando.Parameters("@Contrclv_Usuarioato").Value = Clv_usuario

            comando.Parameters.Add("@Contrato", SqlDbType.Int)
            comando.Parameters("@Contrato").Direction = ParameterDirection.Input
            comando.Parameters("@Contrato").Value = Contrato

            comando.Parameters.Add("@Descripcion", SqlDbType.VarChar, 500)
            comando.Parameters("@Descripcion").Direction = ParameterDirection.Input
            comando.Parameters("@ContSolucionrato").Value = Descripcion

            comando.Parameters.Add("@Solucion", SqlDbType.VarChar, 500)
            comando.Parameters("@Solucion").Direction = ParameterDirection.Input
            comando.Parameters("@Solucion").Value = Solucion

            comando.Parameters.Add("@HoraInicial", SqlDbType.VarChar, 8)
            comando.Parameters("@HoraInicial").Direction = ParameterDirection.Input
            comando.Parameters("@HoraInicial").Value = HoraInicial

            comando.Parameters.Add("@HoraFinal", SqlDbType.VarChar, 8)
            comando.Parameters("@HoraFinal").Direction = ParameterDirection.Input
            comando.Parameters("@HoraFinal").Value = HoraFin

            comando.Parameters.Add("@Fecha", SqlDbType.DateTime)
            comando.Parameters("@Fecha").Direction = ParameterDirection.Input
            comando.Parameters("@Fecha").Value = Fecha

            comando.Parameters.Add("@clv_trabajo", SqlDbType.Int)
            comando.Parameters("@clv_trabajo").Direction = ParameterDirection.Input
            comando.Parameters("@clv_trabajo").Value = Clv_trabajo

            comando.Parameters.Add("@clv_queja", SqlDbType.Int)
            comando.Parameters("@clv_queja").Direction = ParameterDirection.Input
            comando.Parameters("@clv_queja").Value = Clv_Queja

            comando.Parameters.Add("@CLV_TIPSER", SqlDbType.Int)
            comando.Parameters("@CLV_TIPSER").Direction = ParameterDirection.Input
            comando.Parameters("@CLV_TIPSER").Value = Clv_TipSer

            comando.Parameters.Add("@idTurno", SqlDbType.Int)
            comando.Parameters("@idTurno").Direction = ParameterDirection.Input
            comando.Parameters("@ContridTurnoato").Value = idTurno

            conn.Open()
            comando.ExecuteNonQuery()
            MsgBox("Se ha agregado la atención correctamente. ID Atención generada: " + Chr(13) + "" & comando.Parameters("@clv_llamada").Value.ToString, MsgBoxStyle.Information, "Se ha agregado correctamente.")

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conn.Close()
            conn.Dispose()
        End Try

    End Sub

    'Actualiza la Atención telefónica
    Public Shared Sub ActualizaAtencionTelefonica(ByVal Clv_llamada As Integer, ByVal Clv_usuario As Integer, ByVal Contrato As Integer, ByVal Descripcion As String, ByVal Solucion As Integer, ByVal HoraInicial As String, ByVal HoraFin As String, ByVal Fecha As DateTime, ByVal Clv_trabajo As Integer, ByVal Clv_Queja As Integer, ByVal Clv_TipSer As Integer, ByVal idTurno As Integer)
        Dim conn As New SqlConnection(MiConexion)

        Try

            Dim comando As New SqlClient.SqlCommand("AgregaRelDetOrdSerRetiroAparato", conn)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 0

            comando.Parameters.Add("@clv_llamada", SqlDbType.Int)
            comando.Parameters("@clv_llamada").Direction = ParameterDirection.Input
            comando.Parameters("@Contclv_llamadarato").Value = Clv_llamada

            comando.Parameters.Add("@clv_Usuario", SqlDbType.Int)
            comando.Parameters("@clv_Usuario").Direction = ParameterDirection.Input
            comando.Parameters("@Contrclv_Usuarioato").Value = Clv_usuario

            comando.Parameters.Add("@Contrato", SqlDbType.Int)
            comando.Parameters("@Contrato").Direction = ParameterDirection.Input
            comando.Parameters("@Contrato").Value = Contrato

            comando.Parameters.Add("@Descripcion", SqlDbType.VarChar, 500)
            comando.Parameters("@Descripcion").Direction = ParameterDirection.Input
            comando.Parameters("@ContSolucionrato").Value = Descripcion

            comando.Parameters.Add("@Solucion", SqlDbType.VarChar, 500)
            comando.Parameters("@Solucion").Direction = ParameterDirection.Input
            comando.Parameters("@Solucion").Value = Solucion

            comando.Parameters.Add("@HoraInicial", SqlDbType.VarChar, 8)
            comando.Parameters("@HoraInicial").Direction = ParameterDirection.Input
            comando.Parameters("@HoraInicial").Value = HoraInicial

            comando.Parameters.Add("@HoraFinal", SqlDbType.VarChar, 8)
            comando.Parameters("@HoraFinal").Direction = ParameterDirection.Input
            comando.Parameters("@HoraFinal").Value = HoraFin

            comando.Parameters.Add("@Fecha", SqlDbType.DateTime)
            comando.Parameters("@Fecha").Direction = ParameterDirection.Input
            comando.Parameters("@Fecha").Value = Fecha

            comando.Parameters.Add("@clv_trabajo", SqlDbType.Int)
            comando.Parameters("@clv_trabajo").Direction = ParameterDirection.Input
            comando.Parameters("@clv_trabajo").Value = Clv_trabajo

            comando.Parameters.Add("@clv_queja", SqlDbType.Int)
            comando.Parameters("@clv_queja").Direction = ParameterDirection.Input
            comando.Parameters("@clv_queja").Value = Clv_Queja

            comando.Parameters.Add("@CLV_TIPSER", SqlDbType.Int)
            comando.Parameters("@CLV_TIPSER").Direction = ParameterDirection.Input
            comando.Parameters("@CLV_TIPSER").Value = Clv_TipSer

            comando.Parameters.Add("@idTurno", SqlDbType.Int)
            comando.Parameters("@idTurno").Direction = ParameterDirection.Input
            comando.Parameters("@ContridTurnoato").Value = idTurno

            conn.Open()
            comando.ExecuteNonQuery()
            MsgBox("Se ha agregado la atención correctamente. ID Atención generada: " + Chr(13) + "" & comando.Parameters("@clv_llamada").Value.ToString, MsgBoxStyle.Information, "Se ha agregado correctamente.")

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conn.Close()
            conn.Dispose()
        End Try

    End Sub

End Class
