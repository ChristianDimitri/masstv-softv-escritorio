Imports System.Data.SqlClient
Public Class FrmCNRDig
    Private Contrato As String = Nothing
    Private MacAddress As String = Nothing
    Private Paquete As String = Nothing
    Private Comando As String = Nothing
    Private Resultado As String = Nothing
    Private Fecha As String = Nothing
    Private Sub DameDatosBitacora()
        Try
            If eOpcion = "M" Then
                If IsNumeric(Me.Numero_de_contratoTextBox.Text) = False Then
                    Contrato = 0
                Else
                    Contrato = Me.Numero_de_contratoTextBox.Text
                End If

                MacAddress = Me.Mac_addressTextBox.Text
                Paquete = Me.PaqueteTextBox.Text
                Comando = Me.ComandoTextBox.Text
                Resultado = Me.ResultadoTextBox.Text
                Fecha = Me.Fecha_habilitarDateTimePicker.Text
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub GuardaDatosBitacora()
        Try

            If eOpcion = "M" Then
                bitsist(GloUsuario, CLng(Contrato), LocGloSistema, Me.Text, "Consecutivo en dbo.Cnr_Dig:" + CStr(eConsecutivo), "Contrato: " + Contrato, "Contrato: " + Me.Numero_de_contratoTextBox.Text, LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Contrato), LocGloSistema, Me.Text, "Consecutivo en dbo.Cnr_Dig:" + CStr(eConsecutivo), "MacAddress: " + MacAddress, "MacAddress: " + Me.Mac_addressTextBox.Text, LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Contrato), LocGloSistema, Me.Text, "Consecutivo en dbo.Cnr_Dig:" + CStr(eConsecutivo), "Paquete: " + Paquete, "Paquete: " + Me.PaqueteTextBox.Text, LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Contrato), LocGloSistema, Me.Text, "Consecutivo en dbo.Cnr_Dig:" + CStr(eConsecutivo), "Comando: " + Comando, "Comando: " + Me.ComandoTextBox.Text, LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Contrato), LocGloSistema, Me.Text, "Consecutivo en dbo.Cnr_Dig:" + CStr(eConsecutivo), "Resultado :" + Resultado, "Resultado :" + Me.ResultadoTextBox.Text, LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Contrato), LocGloSistema, Me.Text, "Consecutivo en dbo.Cnr_Dig:" + CStr(eConsecutivo), "Fecha: " + Fecha, "Fecha: " + Me.Fecha_habilitarDateTimePicker.Text, LocClv_Ciudad)
            ElseIf eOpcion = "N" Then
                bitsist(GloUsuario, CLng(Me.Numero_de_contratoTextBox.Text), LocGloSistema, Me.Text, "Nuevo", "", "Contrato: " + Me.Numero_de_contratoTextBox.Text + "/ MacAddress: " + Me.Mac_addressTextBox.Text + " / Paquete: " + Me.PaqueteTextBox.Text + " / Comando: " + Me.ComandoTextBox.Text + " / Resultado: " + Me.ResultadoTextBox.Text + "/ FechaHabilitar: " + Me.Fecha_habilitarDateTimePicker.Text, LocClv_Ciudad)
            End If
            
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub ConCNRDigBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConCNRDigBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()

        'Try
        If eOpcion = "N" Then
            Me.ComandoTextBox.Text = Me.ComboBox1.Text

            If Me.Numero_de_contratoTextBox.Text.Length > 0 Then
                If Me.Mac_addressTextBox.Text.Length > 0 Then
                    If Me.PaqueteTextBox.Text.Length > 0 Then
                        If Me.ComandoTextBox.Text.Length > 0 Then
                            Me.ConCNRDigTableAdapter.Connection = CON
                            Me.ConCNRDigTableAdapter.Insert(Me.Numero_de_contratoTextBox.Text, Me.CLV_UNICANETTextBox.Text, Me.Mac_addressTextBox.Text, Me.PaqueteTextBox.Text, Me.ComandoTextBox.Text, Me.ResultadoTextBox.Text, Me.Descripcion_transaccionTextBox.Text, Me.Fecha_ejecucionDateTimePicker.Value, Me.Clv_OrdenTextBox.Text, Me.Fecha_habilitarDateTimePicker.Value)
                            GuardaDatosBitacora()
                            MsgBox(mensaje5)
                            Me.Close()
                        Else
                            MsgBox("Asigna el Comando.", , "Atenci�n")
                        End If
                    Else
                        MsgBox("Asigna o Captura el Paquete", , "Atenci�n")
                    End If
                Else
                    MsgBox("Captura la Mac Address.", , "Atenci�n")
                End If
            Else
                MsgBox("Captura el Contrato.", , "Atenci�n")
            End If
        End If




        If eOpcion = "M" Then


            If Me.Numero_de_contratoTextBox.Text.Length > 0 Then
                If Me.Mac_addressTextBox.Text.Length > 0 Then
                    If Me.ComandoTextBox.Text.Length > 0 Then
                        If Me.ResultadoTextBox.Text.Length > 0 Then

                            Me.Validate()
                            Me.ConCNRDigBindingSource.EndEdit()
                            Me.ConCNRDigTableAdapter.Connection = CON
                            Me.ConCNRDigTableAdapter.Update(eConsecutivo, Me.Numero_de_contratoTextBox.Text, Me.CLV_UNICANETTextBox.Text, Me.Mac_addressTextBox.Text, Me.PaqueteTextBox.Text, Me.ComandoTextBox.Text, Me.ResultadoTextBox.Text, Me.Descripcion_transaccionTextBox.Text, Me.Fecha_ejecucionDateTimePicker.Value, Me.Clv_OrdenTextBox.Text, Me.Fecha_habilitarDateTimePicker.Value)
                            GuardaDatosBitacora()
                            MsgBox(mensaje5)
                            Me.Close()
                        Else
                            MsgBox("Asigna el Resultado.", , "Error")
                        End If
                    Else
                        MsgBox("Asigna el Comando.", , "Atenci�n")
                    End If
                Else
                    MsgBox("Captura la Mac Address.", , "Atenci�n")
                End If
            Else
                MsgBox("Captura el Contrato.", , "Atenci�n")
            End If
        End If


        If eOpcion = "C" Then
            Me.ConCNRDigTableAdapter.Connection = CON
            Me.ConCNRDigTableAdapter.Fill(Me.DataSetEric.ConCNRDig, eConsecutivo)
            Me.ComboBox1.Visible = False
            Me.ComandoTextBox.Visible = True
            Me.ComboBox2.Visible = False
            Me.ResultadoTextBox.Visible = True

        End If




        'Catch
        'MsgBox("Se ha producido un Error.", , "Error")
        'End Try
        CON.Close()
    End Sub

    Private Sub FrmCNRDig_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        Me.Fecha_habilitarDateTimePicker.Value = Today
        Me.Fecha_habilitarDateTimePicker.MinDate = Today

        If eOpcion = "N" Then
            'Me.ConCNRDigBindingSource.AddNew()
            Me.ResultadoTextBox.Text = 0
            Me.Clv_OrdenTextBox.Text = 0
            Me.CLV_UNICANETTextBox.Text = 0
            Me.ComboBox2.Visible = False
        End If

        If eOpcion = "C" Then
            Me.ConCNRDigTableAdapter.Connection = CON
            Me.ConCNRDigTableAdapter.Fill(Me.DataSetEric.ConCNRDig, eConsecutivo)
            Me.Numero_de_contratoTextBox.ReadOnly = True
            Me.Mac_addressTextBox.ReadOnly = True
            Me.PaqueteTextBox.ReadOnly = True
            Me.Fecha_habilitarDateTimePicker.Enabled = False
            Me.ComandoTextBox.Visible = True
            Me.ComboBox1.Visible = False
            Me.ResultadoTextBox.Visible = True
            Me.ComboBox2.Visible = False
            Me.ConCNRDigBindingNavigator.Enabled = False



            'If Me.PaqueteTextBox.Text = "DEM1" Then
            '    Me.CheckBox1.Checked = True
            'ElseIf Me.PaqueteTextBox.Text = "DEM2" Then
            '    Me.CheckBox2.Checked = True
            'ElseIf Me.PaqueteTextBox.Text = "DEM1, DEM2" Or Me.PaqueteTextBox.Text = "DEM2, DEM1" Then
            '    Me.CheckBox1.Checked = True
            '    Me.CheckBox2.Checked = True
            'End If

            Me.CheckBox1.Enabled = False
            Me.CheckBox2.Enabled = False

        End If

        If eOpcion = "M" Then
            Me.ConCNRDigTableAdapter.Connection = CON
            Me.ConCNRDigTableAdapter.Fill(Me.DataSetEric.ConCNRDig, eConsecutivo)
            Me.ComandoTextBox.Visible = True
            Me.ComboBox1.Visible = True
            Me.ResultadoTextBox.Visible = True
            Me.ComboBox2.Visible = True


            'If Me.PaqueteTextBox.Text = "DEM1" Then
            '    Me.CheckBox1.Checked = True
            'ElseIf Me.PaqueteTextBox.Text = "DEM2" Then
            '    Me.CheckBox2.Checked = True
            'ElseIf Me.PaqueteTextBox.Text = "DEM1, DEM2" Or Me.PaqueteTextBox.Text = "DEM2, DEM1" Then
            '    Me.CheckBox1.Checked = True
            '    Me.CheckBox2.Checked = True
            'End If
        End If

        CON.Close()
        DameDatosBitacora()
        BaseII.limpiaParametros()
        Dim DT As DataTable
        DT = BaseII.ConsultaDT("upsConsultaComandosCNRDIG")
        ComboBox1.DisplayMember = "Comando"
        ComboBox1.DataSource = DT


    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Me.ComandoTextBox.Text = Me.ComboBox1.Text
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        Me.ResultadoTextBox.Text = Me.ComboBox2.Text
    End Sub

    Private Sub Numero_de_contratoTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Numero_de_contratoTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.Numero_de_contratoTextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged

        If Me.CheckBox1.Checked = True Then
            If Me.PaqueteTextBox.Text.Length = 0 Then
                Me.PaqueteTextBox.Text = "DEM1"
            Else
                Me.PaqueteTextBox.Text = Me.PaqueteTextBox.Text & ", DEM1"
            End If
        Else
            If Me.PaqueteTextBox.Text = "DEM1" Then
                Me.PaqueteTextBox.Text = ""
            Else
                If Me.CheckBox2.Checked = True Then
                    Me.PaqueteTextBox.Text = "DEM2"
                Else
                    Me.PaqueteTextBox.Text = ""
                End If

            End If
        End If

    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged

        If Me.CheckBox2.Checked = True Then
            If Me.PaqueteTextBox.Text.Length = 0 Then
                Me.PaqueteTextBox.Text = "DEM2"
            Else
                Me.PaqueteTextBox.Text = Me.PaqueteTextBox.Text & ", DEM2"
            End If
        Else
            If Me.PaqueteTextBox.Text = "DEM2" Then
                Me.PaqueteTextBox.Text = ""
            Else
                If Me.CheckBox1.Checked = True Then
                    Me.PaqueteTextBox.Text = "DEM1"
                Else
                    Me.PaqueteTextBox.Text = ""
                End If
            End If
        End If

    End Sub
End Class