﻿Public Class FrmGrupoIngresos

    Private Sub SaveButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveButton.Click

        If ConceptoText.Text.Length = 0 Then
            MessageBox.Show("Captura un Concepto.")
            Exit Sub
        End If

        If PosicionText.Text.Length = 0 Then
            MessageBox.Show("Captura una Posición.")
            Exit Sub
        End If

        If IsNumeric(PosicionText.Text) = False Then
            MessageBox.Show("Captura una Posición numérica.")
            Exit Sub
        End If

        If tbIdProgramacion.Text.Length = 0 Then
            MessageBox.Show("Captura Id Programación.")
            Exit Sub
        End If

        If IsNumeric(tbIdProgramacion.Text) = False Then
            MessageBox.Show("Captura Id Programación válido.")
            Exit Sub
        End If

        DialogResult = DialogResult.OK

        'Try
        '    Dim valida As Integer
        '    valida = CInt(PosicionText.Text)
        '    If ConceptoText.Text.Trim <> "" Then
        '        If valida > 0 Then
        '            Me.DialogResult = DialogResult.OK
        '        Else
        '            MsgBox("La Posición tiene que ser mayor a 0", MsgBoxStyle.Exclamation, "Error")
        '        End If
        '    Else
        '        MsgBox("Ingrese un concepto", MsgBoxStyle.Exclamation, "Error")
        '    End If
        'Catch ex As Exception
        '    MsgBox("La Posición tiene que ser numerica", MsgBoxStyle.Exclamation, "Error")
        'End Try


    End Sub

    Private Sub ExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitButton.Click
        Me.DialogResult = DialogResult.Cancel
    End Sub

    Private Sub FrmGrupoIngresos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
    End Sub
End Class