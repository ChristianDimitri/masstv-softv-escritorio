Imports System.Data.SqlClient
Public Class FrmEtiquetas

    Private Sub FrmEtiquetas_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

    End Sub

    Private Sub FrmEtiquetas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MUESTRABANCOS' Puede moverla o quitarla seg�n sea necesario.
        Me.MUESTRABANCOSTableAdapter.Connection = CON
        Me.MUESTRABANCOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRABANCOS)
        Me.ComboBox1.Text = ""
        'If GloClv_tipser2 = 2 Or GloClv_tipser2 = 3 Then
        '    Me.RadioButton5.Visible = False
        'End If
        CON.Close()
    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        If Me.RadioButton1.Checked = True Then
            GloOpEtiqueta = "0"
            Me.ComboBox1.Enabled = False
        End If
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        If Me.RadioButton2.Checked = True Then
            GloOpEtiqueta = "1"
            Me.ComboBox1.Enabled = False
        End If
    End Sub

    Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton3.CheckedChanged
        If Me.RadioButton3.Checked = True Then
            GloOpEtiqueta = "2"
            Me.ComboBox1.Enabled = False
        End If
    End Sub

    Private Sub RadioButton4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton4.CheckedChanged
        If Me.RadioButton4.Checked = True Then
            GloOpEtiqueta = "3"
            Me.ComboBox1.Enabled = False
        End If
    End Sub

    Private Sub RadioButton5_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton5.CheckedChanged
        If Me.RadioButton5.Checked = True Then
            GloOpEtiqueta = "4"
            Me.ComboBox1.Enabled = False
        End If
    End Sub

    Private Sub RadioButton6_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton6.CheckedChanged
        If Me.RadioButton6.Checked = True Then
            GloOpEtiqueta = "6"
            Me.ComboBox1.Enabled = True
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        Select Case LocValidaHab
            Case 0
                If GloOpEtiqueta = "6" Then
                    If IsNumeric(Me.ComboBox1.SelectedValue) = True And Len(Trim(Me.ComboBox1.Text)) > 0 Then
                        GloSelBanco = Me.ComboBox1.SelectedValue
                        FrmSelPeriodo.Show()
                        Me.Close()
                    Else
                        MsgBox("Seleccione el Banco", MsgBoxStyle.Information)
                        Exit Sub
                    End If
                ElseIf GloOpEtiqueta <> "6" Then
                    FrmSelPeriodo.Show()
                    Me.Close()
                End If
            Case 1
                If LocOp = 6 Then
                    If GloOpEtiqueta = "6" Then
                        If IsNumeric(Me.ComboBox1.SelectedValue) = True And Len(Trim(Me.ComboBox1.Text)) > 0 Then
                            GloSelBanco = Me.ComboBox1.SelectedValue
                            GloBndEtiqueta = True
                            Me.Close()
                        Else
                            MsgBox("Seleccione el Banco", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                    ElseIf GloOpEtiqueta <> "6" Then
                        GloBndEtiqueta = True
                        Me.Close()
                    End If

                End If
                If LocOp = 7 Then
                    If GloOpEtiqueta = "6" Then
                        If IsNumeric(Me.ComboBox1.SelectedValue) = True And Len(Trim(Me.ComboBox1.Text)) > 0 Then
                            GloSelBanco = Me.ComboBox1.SelectedValue
                            FrmSelRecord.Show()
                            'bndAvisos2 = True
                            Me.Close()
                        Else
                            MsgBox("Seleccione el Banco", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                    ElseIf GloOpEtiqueta <> "6" Then
                        'bndAvisos2 = True
                        If GloOpEtiqueta <> "1" Then
                            If IdSistema = "SA" And (Me.RadioButton4.Checked = True Or Me.RadioButton5.Checked = True Or Me.RadioButton2.Checked = True) Then
                                'If GloOpEtiqueta = "1" Then
                                '    FrmTelsi.Show()
                                'Else
                                bndAvisos2 = True
                                'End If
                            Else
                                ' FrmSelRecord.Show()                   
                                If Me.RadioButton7.Checked Then
                                    FrmFiltroFechasBajas.Show()
                                    Me.Close()
                                Else
                                    bndAvisos2 = True
                                    FrmImprimir.Show()
                                End If
                            End If
                        ElseIf GloOpEtiqueta = "1" Then
                            If IdSistema = "SA" And (Me.RadioButton4.Checked = True Or Me.RadioButton5.Checked = True Or Me.RadioButton2.Checked = True) Then
                                FrmTelsi.Show()
                            Else
                                bndAvisos2 = True
                                'FrmSelRecord.Show()
                                FrmImprimir.Show()
                            End If
                        End If

                        Me.Close()
                        End If

                End If
        End Select

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        GloBndEtiqueta = False
        Me.Close()
    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub

    Private Sub RadioButton7_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton7.CheckedChanged
        'If Me.RadioButton7.Checked = True Then
        '    GloOpEtiqueta = "5"
        '    Me.ComboBox1.Enabled = False
        'End If
        If Me.RadioButton7.Checked = True Then
            GloOpEtiqueta = "3"
            Me.ComboBox1.Enabled = False
        End If
    End Sub


End Class