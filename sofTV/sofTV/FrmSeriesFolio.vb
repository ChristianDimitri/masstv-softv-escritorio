﻿Imports System.Data
Imports System.Data.SqlClient
Imports sofTV.Base

Public Class FrmSeriesFolio

    Public serie As String, folios As Integer
    Dim consulta As New CBase

    Private Sub FrmSeriesFolio_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        LlenaCombo()
    End Sub

    Private Sub LlenaCombo()
        Dim dt As DataTable = consulta.consultarDT("SP_SerieFolio", Nothing)

        If dt.Rows.Count > 0 Then
            Cmb_serie.DataSource = dt
            Cmb_serie.DisplayMember = "Serie"
            Cmb_serie.ValueMember = "Clave"
        End If

    End Sub

    Private Sub InsertarSerie(ByVal vserie As String, ByVal vfolios As Integer)
        Try
            Using conexion As New SqlConnection(MiConexion)
                conexion.Open()
                Dim cmm As New SqlCommand()
                cmm.Connection = conexion
                cmm.CommandText = "SP_InsertarSerieFolio"
                cmm.CommandType = CommandType.StoredProcedure

                cmm.Parameters.Add("@serie", SqlDbType.NVarChar, 150)
                cmm.Parameters.Add("@folio", SqlDbType.Int)

                cmm.Parameters("@serie").Value = vserie
                cmm.Parameters("@folio").Value = vfolios

                cmm.ExecuteNonQuery()
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Btn_Imprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Imprimir.Click
        If Cmb_serie.SelectedValue <> 0 And IsNumeric(Txt_Serie.Text) Then
            serie = Cmb_serie.GetItemText(Cmb_serie.SelectedItem).ToString()
            folios = Convert.ToInt32(Txt_Serie.Text.ToString())

            InsertarSerie(serie, folios)

            LocGloOpRep = 20
            GloSeries = serie

            FrmImprimirFac.Show()
        Else
            MsgBox("Verifique que la cantidad de folios sea numérico o seleccione una serie", MsgBoxStyle.Information)
        End If

        LocGloOpRep = 0
        Me.Close()
    End Sub

    Private Sub Btn_Salir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Salir.Click
        Me.Close()
    End Sub
End Class