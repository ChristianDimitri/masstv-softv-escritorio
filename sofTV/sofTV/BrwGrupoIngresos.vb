﻿Imports sofTV.BAL

Public Class BrwGrupoIngresos

    Private Sub NuevoButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NuevoButton.Click

        Dim Frm As New FrmGrupoIngresos
        If (Frm.ShowDialog() = DialogResult.OK) Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@IDGRUPOINGRESOS", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@CONCEPTO", SqlDbType.VarChar, Frm.ConceptoText.Text, 250)
            BaseII.CreateMyParameter("@POS", SqlDbType.Int, Frm.PosicionText.Text)
            BaseII.CreateMyParameter("@CLVCOMPANIA", SqlDbType.Int, GloClvCompania)
            BaseII.CreateMyParameter("@IDPROGRAMACION", SqlDbType.Int, Frm.tbIdProgramacion.Text)
            BaseII.Inserta("Softv_AddGrupoIngreso")
        End If

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLVCOMPANIA", SqlDbType.Int, GloClvCompania)
        GrupoConceptosDataGrid.DataSource = BaseII.ConsultaDT("Softv_GetGrupoIngreso")

        'Dim Frm As New FrmGrupoIngresos
        'If (Frm.ShowDialog() = DialogResult.OK) Then
        '    Dim GrupoIngreso As New GrupoIngreso
        '    GrupoIngreso.Add(Nothing, Frm.ConceptoText.Text, CInt(Frm.PosicionText.Text))
        'End If
        'GrupoConceptosDataGrid.DataSource = GrupoIngreso.GetAll()
    End Sub

    Private Sub BrwGrupoIngresos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, 0)
        cbCompania.DataSource = BaseII.ConsultaDT("MUESTRAtblCompanias")

        'Dim GrupoIngreso As New GrupoIngreso
        'GrupoConceptosDataGrid.DataSource = GrupoIngreso.GetAll()
    End Sub

    Private Sub ConsultarButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarButton.Click

        If GrupoConceptosDataGrid.RowCount = 0 Then
            MessageBox.Show("Selecciona un registro a Consultar.")
            Exit Sub
        End If

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@ID", SqlDbType.Int, GrupoConceptosDataGrid.SelectedCells(0).Value)
        BaseII.CreateMyParameter("@CLVCOMPANIA", SqlDbType.Int, GloClvCompania)
        Dim dT As New DataTable
        dT = BaseII.ConsultaDT("Softv_GetByIdGrupoIngreso")

        Dim Frm As New FrmGrupoIngresos
        Frm.IdGrupoText.Text = dT.Rows(0).Item(0).ToString()
        Frm.ConceptoText.Text = dT.Rows(0).Item(1).ToString()
        Frm.PosicionText.Text = dT.Rows(0).Item(2).ToString()
        Frm.tbIdProgramacion.Text = dT.Rows(0).Item(3).ToString()
        Frm.ConceptoText.ReadOnly = True
        Frm.PosicionText.ReadOnly = True
        Frm.tbIdProgramacion.ReadOnly = True
        Frm.SaveButton.Enabled = False
        Frm.ShowDialog()

        'Dim GrupoIngreso As New GrupoIngreso
        'Dim Frm As New FrmGrupoIngresos
        'If CInt(GrupoConceptosDataGrid.SelectedRows(0).Cells(0).Value) > 0 Then
        '    GrupoIngreso = GrupoIngreso.GetOne(CInt(GrupoConceptosDataGrid.SelectedRows(0).Cells(0).Value))
        '    Frm.IdGrupoText.Text = GrupoIngreso.IdGrupoIngresos.ToString
        '    Frm.ConceptoText.Text = GrupoIngreso.Concepto.ToString
        '    Frm.PosicionText.Text = GrupoIngreso.Pos.ToString
        '    Frm.ConceptoText.ReadOnly = True
        '    Frm.PosicionText.ReadOnly = True
        '    Frm.SaveButton.Enabled = False
        '    Frm.ShowDialog()
        'Else
        '    MsgBox("Seleccione un grupo de ingresos", MsgBoxStyle.Information, "Error")
        'End If

    End Sub

    Private Sub ModificarButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ModificarButton.Click

        If GrupoConceptosDataGrid.RowCount = 0 Then
            MessageBox.Show("Selecciona un registro a Consultar.")
            Exit Sub
        End If

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@ID", SqlDbType.Int, GrupoConceptosDataGrid.SelectedCells(0).Value)
        BaseII.CreateMyParameter("@CLVCOMPANIA", SqlDbType.Int, GloClvCompania)
        Dim dT As New DataTable
        dT = BaseII.ConsultaDT("Softv_GetByIdGrupoIngreso")

        Dim Frm As New FrmGrupoIngresos
        Frm.IdGrupoText.Text = dT.Rows(0).Item(0).ToString()
        Frm.ConceptoText.Text = dT.Rows(0).Item(1).ToString()
        Frm.PosicionText.Text = dT.Rows(0).Item(2).ToString()
        Frm.tbIdProgramacion.Text = dT.Rows(0).Item(3).ToString
        If (Frm.ShowDialog() = DialogResult.OK) Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@IDGRUPOINGRESOS", SqlDbType.Int, Frm.IdGrupoText.Text)
            BaseII.CreateMyParameter("@CONCEPTO", SqlDbType.VarChar, Frm.ConceptoText.Text, 250)
            BaseII.CreateMyParameter("@POS", SqlDbType.Int, Frm.PosicionText.Text)
            BaseII.CreateMyParameter("@IDPROGRAMACION", SqlDbType.Int, Frm.tbIdProgramacion.Text)
            BaseII.Inserta("Softv_EditGrupoIngreso")
        End If

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLVCOMPANIA", SqlDbType.Int, GloClvCompania)
        GrupoConceptosDataGrid.DataSource = BaseII.ConsultaDT("Softv_GetGrupoIngreso")

        'Dim GrupoIngreso As New GrupoIngreso
        'Dim Frm As New FrmGrupoIngresos
        'If CInt(GrupoConceptosDataGrid.SelectedRows(0).Cells(0).Value) > 0 Then
        '    GrupoIngreso = GrupoIngreso.GetOne(CInt(GrupoConceptosDataGrid.SelectedRows(0).Cells(0).Value))
        '    Frm.IdGrupoText.Text = GrupoIngreso.IdGrupoIngresos.ToString
        '    Frm.ConceptoText.Text = GrupoIngreso.Concepto.ToString
        '    Frm.PosicionText.Text = GrupoIngreso.Pos.ToString
        '    If (Frm.ShowDialog() = DialogResult.OK) Then
        '        GrupoIngreso.Edit(CInt(Frm.IdGrupoText.Text), Frm.ConceptoText.Text, CInt(Frm.PosicionText.Text))
        '    End If
        'Else
        '    MsgBox("Seleccione un grupo de ingresos", MsgBoxStyle.Information, "Error")
        'End If
        'GrupoConceptosDataGrid.DataSource = GrupoIngreso.GetAll()
    End Sub

    Private Sub SalirButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalirButton.Click
        Me.Close()

    End Sub

    Private Sub GrupoConceptosDataGrid_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GrupoConceptosDataGrid.CellClick
        Try
            IdGrupoLabel1.Text = GrupoConceptosDataGrid.SelectedRows(0).Cells(0).Value.ToString
            ConceptoLabel1.Text = GrupoConceptosDataGrid.SelectedRows(0).Cells(1).Value.ToString
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GrupoConceptosDataGrid_CurrentCellChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GrupoConceptosDataGrid.CurrentCellChanged
        Try
            IdGrupoLabel1.Text = GrupoConceptosDataGrid.SelectedRows(0).Cells(0).Value.ToString
            ConceptoLabel1.Text = GrupoConceptosDataGrid.SelectedRows(0).Cells(1).Value.ToString
        Catch ex As Exception

        End Try

    End Sub

    Private Sub IdGrupoButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IdGrupoButton.Click

        If cbCompania.Text.Length = 0 Then
            Exit Sub
        End If

        If IdGrupoTextBox.Text.Length = 0 Then
            MessageBox.Show("Captura la Clave.")
            Exit Sub
        End If

        If IsNumeric(IdGrupoTextBox.Text) = False Then
            MessageBox.Show("Captura una Clave numérica.")
            Exit Sub
        End If

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@id", SqlDbType.Int, CInt(IdGrupoTextBox.Text))
        BaseII.CreateMyParameter("@CLVCOMPANIA", SqlDbType.Int, cbCompania.SelectedValue)
        GrupoConceptosDataGrid.DataSource = BaseII.ConsultaDT("Softv_GetByIdGrupoIngreso")

        'Try
        '    BaseII.limpiaParametros()
        '    BaseII.CreateMyParameter("@id", SqlDbType.Int, CInt(IdGrupoTextBox.Text))
        '    GrupoConceptosDataGrid.DataSource = BaseII.ConsultaDT("Softv_GetByIdGrupoIngreso")
        'Catch ex As Exception
        '    MsgBox("La Clave es un valor numerico", MsgBoxStyle.Exclamation, "Error")
        '    GrupoConceptosDataGrid.DataSource = GrupoIngreso.GetAll()
        'End Try
    End Sub

    Private Sub ConceptoButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConceptoButton.Click
        If cbCompania.Text.Length = 0 Then
            Exit Sub
        End If

        If ConceptoTextBox.Text.Length = 0 Then
            MessageBox.Show("Captura un Concepto.")
            Exit Sub
        End If

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Concepto", SqlDbType.NVarChar, ConceptoTextBox.Text, 250)
        BaseII.CreateMyParameter("@CLVCOMPANIA", SqlDbType.Int, cbCompania.SelectedValue)
        GrupoConceptosDataGrid.DataSource = BaseII.ConsultaDT("Softv_GetByConceptoGrupoIngreso")

        'BaseII.limpiaParametros()
        'BaseII.CreateMyParameter("@Concepto", SqlDbType.NVarChar, ConceptoTextBox.Text, 250)
        'GrupoConceptosDataGrid.DataSource = BaseII.ConsultaDT("Softv_GetByConceptoGrupoIngreso")

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        If GrupoConceptosDataGrid.RowCount = 0 Then
            MessageBox.Show("Selecciona un registro a Consultar.")
            Exit Sub
        End If

        If (MsgBox("Desea Elimar el Grupo", MsgBoxStyle.YesNo, "Error") = MsgBoxResult.Yes) Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@IDGRUPOINGRESOS", SqlDbType.Int, GrupoConceptosDataGrid.SelectedCells(0).Value)
            BaseII.Inserta("Softv_DeleteGrupoIngreso")
        End If

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLVCOMPANIA", SqlDbType.Int, GloClvCompania)
        GrupoConceptosDataGrid.DataSource = BaseII.ConsultaDT("Softv_GetGrupoIngreso")

        'If CInt(GrupoConceptosDataGrid.SelectedRows(0).Cells(0).Value) > 0 Then
        '    If (MsgBox("Desea Elimar el Grupo", MsgBoxStyle.YesNo, "Error") = MsgBoxResult.Yes) Then
        '        Dim GrupoIngreso As New GrupoIngreso
        '        GrupoIngreso.Delete(CInt(GrupoConceptosDataGrid.SelectedRows(0).Cells(0).Value))
        '        GrupoConceptosDataGrid.DataSource = GrupoIngreso.GetAll()
        '    End If
        'Else
        '    MsgBox("Seleccione un grupo de ingresos", MsgBoxStyle.Information, "Error")
        'End If
    End Sub

    Private Sub cbCompania_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbCompania.SelectedIndexChanged
        If cbCompania.Text.Length = 0 Then
            Exit Sub
        End If

        GloClvCompania = cbCompania.SelectedValue

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLVCOMPANIA", SqlDbType.Int, GloClvCompania)
        GrupoConceptosDataGrid.DataSource = BaseII.ConsultaDT("Softv_GetGrupoIngreso")

    End Sub
End Class