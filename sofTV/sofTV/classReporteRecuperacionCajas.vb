﻿Imports System.Data.SqlClient
Imports System.Collections.Generic

Public Class classReporteRecuperacionCajas

#Region "Propiedades"
    Private _op As Integer
    Public Property op As Integer
        Get
            Return _op
        End Get
        Set(value As Integer)
            _op = value
        End Set
    End Property

    Private _mes As Integer
    Public Property mes As Integer
        Get
            Return _mes
        End Get
        Set(value As Integer)
            _mes = value
        End Set
    End Property

    Private _anio As Integer
    Public Property anio As Integer
        Get
            Return _anio
        End Get
        Set(value As Integer)
            _anio = value
        End Set
    End Property

    Private _tituloReporte As String
    Public Property tituloReporte As String
        Get
            Return _tituloReporte
        End Get
        Set(value As String)
            _tituloReporte = value
        End Set
    End Property

    Private _nombreEmpresa As String
    Public Property nombreEmpresa As String
        Get
            Return _nombreEmpresa
        End Get
        Set(value As String)
            _nombreEmpresa = value
        End Set
    End Property

    Private _ciudadEmpresa As String
    Public Property ciudadEmpresa As String
        Get
            Return _ciudadEmpresa
        End Get
        Set(value As String)
            _ciudadEmpresa = value
        End Set
    End Property

    Private _clvTipServ As Integer
    Public Property clvTipServ As Integer
        Get
            Return _clvTipServ
        End Get
        Set(value As Integer)
            _clvTipServ = value
        End Set
    End Property
#End Region

#Region "Constructores"
    Public Sub New()
        _op = 0
        _mes = 0
        _anio = 0
        _tituloReporte = ""
        _nombreEmpresa = ""
        _ciudadEmpresa = ""
        _clvTipServ = 0
    End Sub
#End Region

#Region "Métodos"
    Dim Cajas As New BaseIII
    Dim listNombreTablas As List(Of String)

    Public Sub uspGeneraReporteDetalladoClientesMorososSinSeñal()
        Try
            Dim DS As DataSet
            Dim diccioFormulasReporte As Dictionary(Of String, String)

            Cajas.limpiaParametros()
            Cajas.CreateMyParameter("@op", SqlDbType.Int, _op)
            Cajas.CreateMyParameter("@clvTipSer", SqlDbType.Int, _clvTipServ)

            listNombreTablas = New List(Of String)
            listNombreTablas.Add("tblDatosGeneralesMorososSinSeñal")
            listNombreTablas.Add("tblDetalleMorososSinSeñal")
            listNombreTablas.Add("tblDatosFinalesReorte")

            DS = Cajas.ConsultaDS("uspGeneraReporteDetalladoClientesMorososSinSeñal", listNombreTablas)

            diccioFormulasReporte = New Dictionary(Of String, String)
            diccioFormulasReporte.Add("Empresa", _nombreEmpresa)
            diccioFormulasReporte.Add("Subtitulo", _ciudadEmpresa)
            diccioFormulasReporte.Add("TituloReporte", _tituloReporte)

            Cajas.llamarReporteCentralizado(RutaReportes + "\rptGeneraReporteDetalladoClientesMorososSinSeñal", DS, diccioFormulasReporte)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub uspReporteResumenRecuperacionCajas()
        Try
            Dim DS As DataSet
            Dim diccioFormulasReporte As Dictionary(Of String, String)

            Cajas.limpiaParametros()
            Cajas.CreateMyParameter("@mes", SqlDbType.Int, _mes)
            Cajas.CreateMyParameter("@anio", SqlDbType.Int, _anio)
            Cajas.CreateMyParameter("@clvTipSer", SqlDbType.Int, _clvTipServ)

            listNombreTablas = New List(Of String)
            listNombreTablas.Add("tblDatosFinalesReorte")

            DS = Cajas.ConsultaDS("uspReporteResumenRecuperacionCajas", listNombreTablas)

            diccioFormulasReporte = New Dictionary(Of String, String)
            diccioFormulasReporte.Add("Empresa", _nombreEmpresa)
            diccioFormulasReporte.Add("Subtitulo", _ciudadEmpresa)
            diccioFormulasReporte.Add("TituloReporte", _tituloReporte)

            Cajas.llamarReporteCentralizado(RutaReportes + "\rptReporteResumenRecuperacionCajas", DS, diccioFormulasReporte)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function uspMuestraMesesDelAnio() As DataTable
        Try
            Cajas.limpiaParametros()
            uspMuestraMesesDelAnio = Cajas.ConsultaDT("uspMuestraMesesDelAnio")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#End Region

End Class
