﻿Public Class FrmMuestraCantidadesGenerar
    Dim CAPAR As New ClassCAPAR

    Private Sub FrmMuestraCantidadesGenerar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        CAPAR.uspCalculaClientesOrdenesCapar(LocClv_session, gloClvTipSerReportes)
        Me.txtTotalClientes.Text = CAPAR.noClientes
        Me.txtTotalCajas.Text = CAPAR.noCajas
        Me.txtTotalOrdenes.Text = CAPAR.noClientes
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        CAPAR.uspEliminaSessionCambioAparato(LocClv_session)
        Me.Close()
    End Sub

    Private Sub btnGenerar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerar.Click
        If Me.txtTotalClientes.Text > 0 Then
            CAPAR.uspMandaOrdenesCambioAparato(LocClv_session, gloClvTipSerReportes)
            MsgBox("Proceso Generado Exitosamente", MsgBoxStyle.Information)
            Me.Close()
        Else
            MsgBox("No hay Registros para Procesar", MsgBoxStyle.Information)
        End If
    End Sub
End Class