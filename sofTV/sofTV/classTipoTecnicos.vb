﻿Public Class classTipoTecnicos

    Dim tipoTecnico As New BaseIII

    Public Function consulta(ByVal prmId As Integer, ByVal prmNombre As String, ByVal prmOp As Integer) As DataTable
        Try
            tipoTecnico.limpiaParametros()
            tipoTecnico.CreateMyParameter("@id", SqlDbType.Int, CInt(prmId))
            tipoTecnico.CreateMyParameter("@nombre", SqlDbType.VarChar, CStr(prmNombre), 250)
            tipoTecnico.CreateMyParameter("@op", SqlDbType.Int, CInt(prmOp))

            consulta = tipoTecnico.ConsultaDT("uspConsultaTipoTecnico")
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Sub Actualiza(ByVal prmCable As Boolean, ByVal prmAntena As Boolean, ByVal prmClave As Integer)
        Try
            tipoTecnico.limpiaParametros()
            tipoTecnico.CreateMyParameter("@Antena", SqlDbType.Bit, CByte(prmAntena))
            tipoTecnico.CreateMyParameter("@Cable", SqlDbType.Bit, CByte(prmCable))
            tipoTecnico.CreateMyParameter("@Tecnico", SqlDbType.Int, CInt(prmClave))

            tipoTecnico.Inserta("uspModificaRelTipoTecnicos")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class
