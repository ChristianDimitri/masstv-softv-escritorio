﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class brwTipoInstalaciones
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.gpxBusquedaTipoInstalacion = New System.Windows.Forms.GroupBox()
        Me.rbBusquedaTodos = New System.Windows.Forms.RadioButton()
        Me.rbBusquedaNoActivos = New System.Windows.Forms.RadioButton()
        Me.rbBusquedaActivos = New System.Windows.Forms.RadioButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnBusquedaDescripcion = New System.Windows.Forms.Button()
        Me.lblBusquedaDescripcion = New System.Windows.Forms.Label()
        Me.txtBusquedaDescripcion = New System.Windows.Forms.TextBox()
        Me.lblBusquedaClave = New System.Windows.Forms.Label()
        Me.txtBusquedaClave = New System.Windows.Forms.TextBox()
        Me.gbxDetalleTipoInstalacion = New System.Windows.Forms.GroupBox()
        Me.cbxActivoDetalle = New System.Windows.Forms.CheckBox()
        Me.lblDetalleDescripcion = New System.Windows.Forms.Label()
        Me.txtDetalleClave = New System.Windows.Forms.TextBox()
        Me.txtDetalleDescripcion = New System.Windows.Forms.TextBox()
        Me.lblDetalleClave = New System.Windows.Forms.Label()
        Me.gbxDatosTipoInstalacion = New System.Windows.Forms.GroupBox()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.btnConsultar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.dgvTipoInstalaciones = New System.Windows.Forms.DataGridView()
        Me.colIdTipoInstalacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDescripcionTipoInstalacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colActivoTipoInstalacion = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.gpxBusquedaTipoInstalacion.SuspendLayout()
        Me.gbxDetalleTipoInstalacion.SuspendLayout()
        Me.gbxDatosTipoInstalacion.SuspendLayout()
        CType(Me.dgvTipoInstalaciones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gpxBusquedaTipoInstalacion
        '
        Me.gpxBusquedaTipoInstalacion.Controls.Add(Me.rbBusquedaTodos)
        Me.gpxBusquedaTipoInstalacion.Controls.Add(Me.rbBusquedaNoActivos)
        Me.gpxBusquedaTipoInstalacion.Controls.Add(Me.rbBusquedaActivos)
        Me.gpxBusquedaTipoInstalacion.Controls.Add(Me.Label1)
        Me.gpxBusquedaTipoInstalacion.Controls.Add(Me.btnBusquedaDescripcion)
        Me.gpxBusquedaTipoInstalacion.Controls.Add(Me.lblBusquedaDescripcion)
        Me.gpxBusquedaTipoInstalacion.Controls.Add(Me.txtBusquedaDescripcion)
        Me.gpxBusquedaTipoInstalacion.Controls.Add(Me.lblBusquedaClave)
        Me.gpxBusquedaTipoInstalacion.Controls.Add(Me.txtBusquedaClave)
        Me.gpxBusquedaTipoInstalacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpxBusquedaTipoInstalacion.Location = New System.Drawing.Point(12, 12)
        Me.gpxBusquedaTipoInstalacion.Name = "gpxBusquedaTipoInstalacion"
        Me.gpxBusquedaTipoInstalacion.Size = New System.Drawing.Size(200, 414)
        Me.gpxBusquedaTipoInstalacion.TabIndex = 0
        Me.gpxBusquedaTipoInstalacion.TabStop = False
        Me.gpxBusquedaTipoInstalacion.Text = "Búsqueda por :"
        '
        'rbBusquedaTodos
        '
        Me.rbBusquedaTodos.AutoSize = True
        Me.rbBusquedaTodos.Checked = True
        Me.rbBusquedaTodos.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbBusquedaTodos.Location = New System.Drawing.Point(51, 66)
        Me.rbBusquedaTodos.Name = "rbBusquedaTodos"
        Me.rbBusquedaTodos.Size = New System.Drawing.Size(60, 17)
        Me.rbBusquedaTodos.TabIndex = 0
        Me.rbBusquedaTodos.TabStop = True
        Me.rbBusquedaTodos.Text = "Todos"
        Me.rbBusquedaTodos.UseVisualStyleBackColor = True
        '
        'rbBusquedaNoActivos
        '
        Me.rbBusquedaNoActivos.AutoSize = True
        Me.rbBusquedaNoActivos.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbBusquedaNoActivos.Location = New System.Drawing.Point(102, 98)
        Me.rbBusquedaNoActivos.Name = "rbBusquedaNoActivos"
        Me.rbBusquedaNoActivos.Size = New System.Drawing.Size(87, 17)
        Me.rbBusquedaNoActivos.TabIndex = 2
        Me.rbBusquedaNoActivos.Text = "No Activos"
        Me.rbBusquedaNoActivos.UseVisualStyleBackColor = True
        '
        'rbBusquedaActivos
        '
        Me.rbBusquedaActivos.AutoSize = True
        Me.rbBusquedaActivos.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbBusquedaActivos.Location = New System.Drawing.Point(9, 98)
        Me.rbBusquedaActivos.Name = "rbBusquedaActivos"
        Me.rbBusquedaActivos.Size = New System.Drawing.Size(67, 17)
        Me.rbBusquedaActivos.TabIndex = 1
        Me.rbBusquedaActivos.Text = "Activos"
        Me.rbBusquedaActivos.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 42)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 16)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Status :"
        '
        'btnBusquedaDescripcion
        '
        Me.btnBusquedaDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBusquedaDescripcion.Location = New System.Drawing.Point(102, 281)
        Me.btnBusquedaDescripcion.Name = "btnBusquedaDescripcion"
        Me.btnBusquedaDescripcion.Size = New System.Drawing.Size(92, 26)
        Me.btnBusquedaDescripcion.TabIndex = 5
        Me.btnBusquedaDescripcion.Text = "&Buscar"
        Me.btnBusquedaDescripcion.UseVisualStyleBackColor = True
        '
        'lblBusquedaDescripcion
        '
        Me.lblBusquedaDescripcion.AutoSize = True
        Me.lblBusquedaDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBusquedaDescripcion.Location = New System.Drawing.Point(9, 178)
        Me.lblBusquedaDescripcion.Name = "lblBusquedaDescripcion"
        Me.lblBusquedaDescripcion.Size = New System.Drawing.Size(99, 16)
        Me.lblBusquedaDescripcion.TabIndex = 4
        Me.lblBusquedaDescripcion.Text = "Descripción :"
        '
        'txtBusquedaDescripcion
        '
        Me.txtBusquedaDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBusquedaDescripcion.Location = New System.Drawing.Point(6, 197)
        Me.txtBusquedaDescripcion.Multiline = True
        Me.txtBusquedaDescripcion.Name = "txtBusquedaDescripcion"
        Me.txtBusquedaDescripcion.Size = New System.Drawing.Size(188, 78)
        Me.txtBusquedaDescripcion.TabIndex = 4
        '
        'lblBusquedaClave
        '
        Me.lblBusquedaClave.AutoSize = True
        Me.lblBusquedaClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBusquedaClave.Location = New System.Drawing.Point(9, 124)
        Me.lblBusquedaClave.Name = "lblBusquedaClave"
        Me.lblBusquedaClave.Size = New System.Drawing.Size(56, 16)
        Me.lblBusquedaClave.TabIndex = 2
        Me.lblBusquedaClave.Text = "Clave :"
        '
        'txtBusquedaClave
        '
        Me.txtBusquedaClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBusquedaClave.Location = New System.Drawing.Point(6, 143)
        Me.txtBusquedaClave.Name = "txtBusquedaClave"
        Me.txtBusquedaClave.Size = New System.Drawing.Size(118, 22)
        Me.txtBusquedaClave.TabIndex = 3
        '
        'gbxDetalleTipoInstalacion
        '
        Me.gbxDetalleTipoInstalacion.Controls.Add(Me.cbxActivoDetalle)
        Me.gbxDetalleTipoInstalacion.Controls.Add(Me.lblDetalleDescripcion)
        Me.gbxDetalleTipoInstalacion.Controls.Add(Me.txtDetalleClave)
        Me.gbxDetalleTipoInstalacion.Controls.Add(Me.txtDetalleDescripcion)
        Me.gbxDetalleTipoInstalacion.Controls.Add(Me.lblDetalleClave)
        Me.gbxDetalleTipoInstalacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxDetalleTipoInstalacion.Location = New System.Drawing.Point(12, 432)
        Me.gbxDetalleTipoInstalacion.Name = "gbxDetalleTipoInstalacion"
        Me.gbxDetalleTipoInstalacion.Size = New System.Drawing.Size(200, 286)
        Me.gbxDetalleTipoInstalacion.TabIndex = 1
        Me.gbxDetalleTipoInstalacion.TabStop = False
        Me.gbxDetalleTipoInstalacion.Text = "Detalle Seleccionado"
        '
        'cbxActivoDetalle
        '
        Me.cbxActivoDetalle.AutoSize = True
        Me.cbxActivoDetalle.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cbxActivoDetalle.Enabled = False
        Me.cbxActivoDetalle.Location = New System.Drawing.Point(6, 197)
        Me.cbxActivoDetalle.Name = "cbxActivoDetalle"
        Me.cbxActivoDetalle.Size = New System.Drawing.Size(70, 20)
        Me.cbxActivoDetalle.TabIndex = 6
        Me.cbxActivoDetalle.Text = "Activo"
        Me.cbxActivoDetalle.UseVisualStyleBackColor = True
        '
        'lblDetalleDescripcion
        '
        Me.lblDetalleDescripcion.AutoSize = True
        Me.lblDetalleDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDetalleDescripcion.Location = New System.Drawing.Point(9, 85)
        Me.lblDetalleDescripcion.Name = "lblDetalleDescripcion"
        Me.lblDetalleDescripcion.Size = New System.Drawing.Size(99, 16)
        Me.lblDetalleDescripcion.TabIndex = 12
        Me.lblDetalleDescripcion.Text = "Descripción :"
        '
        'txtDetalleClave
        '
        Me.txtDetalleClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDetalleClave.Location = New System.Drawing.Point(6, 50)
        Me.txtDetalleClave.Name = "txtDetalleClave"
        Me.txtDetalleClave.ReadOnly = True
        Me.txtDetalleClave.Size = New System.Drawing.Size(118, 22)
        Me.txtDetalleClave.TabIndex = 10
        '
        'txtDetalleDescripcion
        '
        Me.txtDetalleDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDetalleDescripcion.Location = New System.Drawing.Point(6, 104)
        Me.txtDetalleDescripcion.Multiline = True
        Me.txtDetalleDescripcion.Name = "txtDetalleDescripcion"
        Me.txtDetalleDescripcion.ReadOnly = True
        Me.txtDetalleDescripcion.Size = New System.Drawing.Size(188, 78)
        Me.txtDetalleDescripcion.TabIndex = 11
        '
        'lblDetalleClave
        '
        Me.lblDetalleClave.AutoSize = True
        Me.lblDetalleClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDetalleClave.Location = New System.Drawing.Point(9, 31)
        Me.lblDetalleClave.Name = "lblDetalleClave"
        Me.lblDetalleClave.Size = New System.Drawing.Size(56, 16)
        Me.lblDetalleClave.TabIndex = 9
        Me.lblDetalleClave.Text = "Clave :"
        '
        'gbxDatosTipoInstalacion
        '
        Me.gbxDatosTipoInstalacion.Controls.Add(Me.btnEliminar)
        Me.gbxDatosTipoInstalacion.Controls.Add(Me.btnSalir)
        Me.gbxDatosTipoInstalacion.Controls.Add(Me.btnModificar)
        Me.gbxDatosTipoInstalacion.Controls.Add(Me.btnConsultar)
        Me.gbxDatosTipoInstalacion.Controls.Add(Me.btnNuevo)
        Me.gbxDatosTipoInstalacion.Controls.Add(Me.dgvTipoInstalaciones)
        Me.gbxDatosTipoInstalacion.Location = New System.Drawing.Point(218, 12)
        Me.gbxDatosTipoInstalacion.Name = "gbxDatosTipoInstalacion"
        Me.gbxDatosTipoInstalacion.Size = New System.Drawing.Size(778, 706)
        Me.gbxDatosTipoInstalacion.TabIndex = 1
        Me.gbxDatosTipoInstalacion.TabStop = False
        '
        'btnEliminar
        '
        Me.btnEliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminar.Location = New System.Drawing.Point(652, 337)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(120, 37)
        Me.btnEliminar.TabIndex = 3
        Me.btnEliminar.Text = "&Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(652, 663)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(120, 37)
        Me.btnSalir.TabIndex = 4
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnModificar.Location = New System.Drawing.Point(652, 98)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(120, 37)
        Me.btnModificar.TabIndex = 2
        Me.btnModificar.Text = "&Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'btnConsultar
        '
        Me.btnConsultar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConsultar.Location = New System.Drawing.Point(652, 55)
        Me.btnConsultar.Name = "btnConsultar"
        Me.btnConsultar.Size = New System.Drawing.Size(120, 37)
        Me.btnConsultar.TabIndex = 1
        Me.btnConsultar.Text = "&Consultar"
        Me.btnConsultar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuevo.Location = New System.Drawing.Point(652, 12)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(120, 37)
        Me.btnNuevo.TabIndex = 0
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'dgvTipoInstalaciones
        '
        Me.dgvTipoInstalaciones.AllowUserToAddRows = False
        Me.dgvTipoInstalaciones.AllowUserToDeleteRows = False
        Me.dgvTipoInstalaciones.BackgroundColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvTipoInstalaciones.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvTipoInstalaciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTipoInstalaciones.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIdTipoInstalacion, Me.colDescripcionTipoInstalacion, Me.colActivoTipoInstalacion})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvTipoInstalaciones.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvTipoInstalaciones.Location = New System.Drawing.Point(6, 12)
        Me.dgvTipoInstalaciones.Name = "dgvTipoInstalaciones"
        Me.dgvTipoInstalaciones.ReadOnly = True
        Me.dgvTipoInstalaciones.RowHeadersVisible = False
        Me.dgvTipoInstalaciones.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvTipoInstalaciones.Size = New System.Drawing.Size(640, 688)
        Me.dgvTipoInstalaciones.TabIndex = 0
        '
        'colIdTipoInstalacion
        '
        Me.colIdTipoInstalacion.DataPropertyName = "idTipoInstalacion"
        Me.colIdTipoInstalacion.HeaderText = "Clave Tipo"
        Me.colIdTipoInstalacion.Name = "colIdTipoInstalacion"
        Me.colIdTipoInstalacion.ReadOnly = True
        '
        'colDescripcionTipoInstalacion
        '
        Me.colDescripcionTipoInstalacion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colDescripcionTipoInstalacion.DataPropertyName = "descripcionTipo"
        Me.colDescripcionTipoInstalacion.HeaderText = "Descripción"
        Me.colDescripcionTipoInstalacion.Name = "colDescripcionTipoInstalacion"
        Me.colDescripcionTipoInstalacion.ReadOnly = True
        '
        'colActivoTipoInstalacion
        '
        Me.colActivoTipoInstalacion.DataPropertyName = "activo"
        Me.colActivoTipoInstalacion.HeaderText = "Activo"
        Me.colActivoTipoInstalacion.Name = "colActivoTipoInstalacion"
        Me.colActivoTipoInstalacion.ReadOnly = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'brwTipoInstalaciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1008, 730)
        Me.Controls.Add(Me.gbxDatosTipoInstalacion)
        Me.Controls.Add(Me.gbxDetalleTipoInstalacion)
        Me.Controls.Add(Me.gpxBusquedaTipoInstalacion)
        Me.Name = "brwTipoInstalaciones"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "brwTipoInstalaciones"
        Me.gpxBusquedaTipoInstalacion.ResumeLayout(False)
        Me.gpxBusquedaTipoInstalacion.PerformLayout()
        Me.gbxDetalleTipoInstalacion.ResumeLayout(False)
        Me.gbxDetalleTipoInstalacion.PerformLayout()
        Me.gbxDatosTipoInstalacion.ResumeLayout(False)
        CType(Me.dgvTipoInstalaciones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gpxBusquedaTipoInstalacion As System.Windows.Forms.GroupBox
    Friend WithEvents gbxDetalleTipoInstalacion As System.Windows.Forms.GroupBox
    Friend WithEvents gbxDatosTipoInstalacion As System.Windows.Forms.GroupBox
    Friend WithEvents dgvTipoInstalaciones As System.Windows.Forms.DataGridView
    Friend WithEvents lblBusquedaClave As System.Windows.Forms.Label
    Friend WithEvents txtBusquedaClave As System.Windows.Forms.TextBox
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents lblBusquedaDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtBusquedaDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents btnBusquedaDescripcion As System.Windows.Forms.Button
    Friend WithEvents rbBusquedaTodos As System.Windows.Forms.RadioButton
    Friend WithEvents rbBusquedaNoActivos As System.Windows.Forms.RadioButton
    Friend WithEvents rbBusquedaActivos As System.Windows.Forms.RadioButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents btnConsultar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents colIdTipoInstalacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDescripcionTipoInstalacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colActivoTipoInstalacion As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents cbxActivoDetalle As System.Windows.Forms.CheckBox
    Friend WithEvents lblDetalleDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtDetalleClave As System.Windows.Forms.TextBox
    Friend WithEvents txtDetalleDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents lblDetalleClave As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
