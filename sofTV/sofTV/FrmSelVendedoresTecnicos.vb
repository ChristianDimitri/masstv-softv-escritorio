﻿Imports System.Data.SqlClient
Imports System.Text

Public Class FrmSelVendedoresTecnicos

    Private Sub MuestraVendedoresTecnicos()

        Dim conexion As New SqlConnection(MiConexion)
        Dim sBuilder As New StringBuilder("EXEC MuestraVendedoresTecnicos ")
        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
        Dim dTable As New DataTable
        Dim bSource As New BindingSource

        Try
            dAdapter.Fill(dTable)
            bSource.DataSource = dTable
            cbTecnico.DataSource = bSource
        Catch ex As Exception
            MsgBox(ex.InnerException, MsgBoxStyle.Exclamation)

        End Try

    End Sub

    Private Sub bnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnAceptar.Click

        If cbTecnico.Text.Length = 0 Then
            eBndVendedorTecnico = False
            eClaveVendedorTecnico = 0
            eNombreVendedorTecnico = ""
            MsgBox("Selecciona un técnico.", MsgBoxStyle.Information)
            Exit Sub
        End If

        eBndVendedorTecnico = True
        eClaveVendedorTecnico = cbTecnico.SelectedValue
        eNombreVendedorTecnico = cbTecnico.Text
        Me.Close()
    End Sub

    Private Sub bnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnCancelar.Click
        eBndVendedorTecnico = False
        eClaveVendedorTecnico = 0
        eNombreVendedorTecnico = ""
        Me.Close()
    End Sub


    Private Sub FrmSelVendedoresTecnicos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        MuestraVendedoresTecnicos()
    End Sub
End Class