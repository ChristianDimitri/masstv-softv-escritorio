﻿Public Class FrmFiltroSector
    Public Clv_Sector As Integer = 0
    Public NombreSector As String = ""
#Region "Controles y eventos"
    Private Sub FrmFiltroSector_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Me.lblTitulo.ForeColor = Color.Black
        Try
            cmbSector.DataSource = DASectores.CargaSectores
            cmbSector.DisplayMember = "SECTOR"
            cmbSector.ValueMember = "CLV_SECTOR"
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub
    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
        Me.Dispose()
    End Sub
    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Try
            Clv_Sector = cmbSector.SelectedValue
            NombreSector = cmbSector.Text
            Me.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub
#End Region
End Class