﻿Public Class BrwPrioridadQueja

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        opcion = "N"
        FrmPrioridadQueja.Show()

    End Sub

    Private Sub btnConsulta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsulta.Click
        opcion = "C"
        Dim Clave As Integer = 0

        If (Me.lblInfClave.Text <> "") And (IsNumeric(Me.lblInfClave.Text)) Then
            Clave = CType(Me.lblInfClave.Text, Integer)
        End If
        FrmPrioridadQueja.proClave = Clave
        FrmPrioridadQueja.proConcepto = Me.lblInfConcepto.Text

        FrmPrioridadQueja.Show()
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        opcion = "M"
        Dim Clave As Integer = 0

        If (Me.lblInfClave.Text <> "") And (IsNumeric(Me.lblInfClave.Text)) Then
            Clave = CType(Me.lblInfClave.Text, Integer)
        End If
        FrmPrioridadQueja.proClave = Clave
        FrmPrioridadQueja.proConcepto = Me.lblInfConcepto.Text

        FrmPrioridadQueja.Show()
    End Sub

    Private Sub BrwPrioridadQueja_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        opcion = ""
        Me.dgvPrioridades.DataSource = sofTV.BAL.PrioridadQueja.GetAll
        If Me.dgvPrioridades.RowCount > 0 Then
            'Me.dgvPrioridades.Rows(0).Selected = True
            Me.dgvPrioridades.Rows(0).Cells(0).Selected = True
            Me.lblInfClave.Text = Me.dgvPrioridades.Rows(0).Cells(0).Value
            Me.lblInfConcepto.Text = Me.dgvPrioridades.Rows(0).Cells(1).Value
        End If
    End Sub

    Private Sub BrwPrioridadQueja_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        opcion = ""
        Me.dgvPrioridades.DataSource = sofTV.BAL.PrioridadQueja.GetAll
        If Me.dgvPrioridades.RowCount > 0 Then
            'Me.dgvPrioridades.Rows(0).Selected = True
            Me.dgvPrioridades.Rows(0).Cells(0).Selected = True
            Me.lblInfClave.Text = Me.dgvPrioridades.Rows(0).Cells(0).Value
            Me.lblInfConcepto.Text = Me.dgvPrioridades.Rows(0).Cells(1).Value
        End If
    End Sub

    Private Sub btnBuscarClave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscarClave.Click
        Dim Clave As Integer = 0

        If (Me.txtClave.Text <> "") And (IsNumeric(Me.txtClave.Text)) Then
            Clave = CType(Me.txtClave.Text, Integer)
            Me.dgvPrioridades.DataSource = sofTV.BAL.PrioridadQueja.GetPrioridadQuejaByclvPrioridadQueja(Clave)
        Else
            Me.dgvPrioridades.DataSource = sofTV.BAL.PrioridadQueja.GetAll
        End If

    End Sub

    Private Sub btnBuscarConcepto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscarConcepto.Click
        If Me.txtConcepto.Text = "" Then
            Me.dgvPrioridades.DataSource = sofTV.BAL.PrioridadQueja.GetAll
        Else
            Me.dgvPrioridades.DataSource = sofTV.BAL.PrioridadQueja.GetPrioridadQuejaByDescripcion(Me.txtConcepto.Text)
        End If

    End Sub

    Private Sub dgvPrioridades_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPrioridades.CellClick
        Me.lblInfClave.Text = Me.dgvPrioridades.SelectedCells.Item(0).Value
        Me.lblInfConcepto.Text = Me.dgvPrioridades.SelectedCells.Item(1).Value
    End Sub

   
    Private Sub dgvPrioridades_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPrioridades.CellContentClick

    End Sub

    Private Sub dgvPrioridades_CellContextMenuStripChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPrioridades.CellContextMenuStripChanged

    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.close()
    End Sub
End Class