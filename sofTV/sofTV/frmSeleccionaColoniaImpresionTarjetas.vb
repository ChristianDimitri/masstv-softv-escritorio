﻿Public Class frmSeleccionaColoniaImpresionTarjetas

    

#Region "EVENTOS"
    Private Sub frmSeleccionaColoniaImpresionTarjetas_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Label1.ForeColor = Color.Black
        Label2.ForeColor = Color.Black
        UspLlenaColoniasTarjetasTMP()
        UspMuestraColoniasTarjetasTMP()
        UspMuestraColoniasTarjetas()

    End Sub
#End Region
#Region "METODOS"
    Private Sub UspLlenaColoniasTarjetasTMP()
        Try
            BaseII.limpiaParametros()
            BaseII.Inserta("UspLlenaColoniasTarjetasTMP")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspMuestraColoniasTarjetasTMP()
        Try
            BaseII.limpiaParametros()
            Me.lbColoniasASeleccionar.DataSource = BaseII.ConsultaDT("UspMuestraColoniasTarjetasTMP")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspMuestraColoniasTarjetas()
        Try
            BaseII.limpiaParametros()
            Me.lbColoniasSeleccionadas.DataSource = BaseII.ConsultaDT("UspMuestraColoniasTarjetas")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspSeleccionaUnaColoniasTarjetas()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVCOLONIA", SqlDbType.Int, Me.lbColoniasASeleccionar.SelectedValue)
            BaseII.Inserta("UspSeleccionaUnaColoniasTarjetas")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspSeleccionaTodasColoniasTarjetas()
        Try
            BaseII.limpiaParametros()
            BaseII.Inserta("UspSeleccionaTodasColoniasTarjetas")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspDeseleccionaUnaColoniasTarjetas()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVCOLONIA", SqlDbType.Int, Me.lbColoniasSeleccionadas.SelectedValue)
            BaseII.Inserta("UspDeseleccionaUnaColoniasTarjetas")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspDeseleccionaTodasColoniasTarjetas()
        Try
            BaseII.limpiaParametros()
            BaseII.Inserta("UspDeseleccionaTodasColoniasTarjetas")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "CONTROLES"

    Private Sub btSeleccionarUnaColonia_Click(sender As System.Object, e As System.EventArgs) Handles btSeleccionarUnaColonia.Click
        If lbColoniasASeleccionar.Items.Count > 0 Then
            UspSeleccionaUnaColoniasTarjetas()
            UspMuestraColoniasTarjetasTMP()
            UspMuestraColoniasTarjetas()
        End If
    End Sub

    Private Sub btnSeleccionarTodasColonias_Click(sender As System.Object, e As System.EventArgs) Handles btnSeleccionarTodasColonias.Click
        If lbColoniasASeleccionar.Items.Count > 0 Then
            UspSeleccionaTodasColoniasTarjetas()
            UspMuestraColoniasTarjetasTMP()
            UspMuestraColoniasTarjetas()
        End If
    End Sub

    Private Sub btnDeseleccionarTodasColonias_Click(sender As System.Object, e As System.EventArgs) Handles btnDeseleccionarTodasColonias.Click
        If lbColoniasSeleccionadas.Items.Count > 0 Then
            UspDeseleccionaTodasColoniasTarjetas()
            UspMuestraColoniasTarjetasTMP()
            UspMuestraColoniasTarjetas()
        End If
    End Sub

    Private Sub btnDeseleccionarUnaColonia_Click(sender As System.Object, e As System.EventArgs) Handles btnDeseleccionarUnaColonia.Click
        If lbColoniasSeleccionadas.Items.Count > 0 Then
            UspDeseleccionaUnaColoniasTarjetas()
            UspMuestraColoniasTarjetasTMP()
            UspMuestraColoniasTarjetas()
        End If

    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        If Me.lbColoniasSeleccionadas.Items.Count = 0 Then
            MsgBox("Debe de seleccionar por lo menos una Colonia", MsgBoxStyle.Information)
            Exit Sub
        End If
        frmSeleccionaCiudadImpresionTarjetas.Show()
        Me.Close()
    End Sub
#End Region

   
End Class