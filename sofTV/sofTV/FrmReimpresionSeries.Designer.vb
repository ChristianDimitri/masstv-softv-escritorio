﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmReimpresionSeries
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Btn_Salir = New System.Windows.Forms.Button()
        Me.Btn_ReImprimir = New System.Windows.Forms.Button()
        Me.Cmb_serie = New System.Windows.Forms.ComboBox()
        Me.CMBSerie = New System.Windows.Forms.Label()
        Me.Txt_inicio = New System.Windows.Forms.TextBox()
        Me.Txt_fin = New System.Windows.Forms.TextBox()
        Me.CMBinicio = New System.Windows.Forms.Label()
        Me.CMBfin = New System.Windows.Forms.Label()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.SuspendLayout()
        '
        'Btn_Salir
        '
        Me.Btn_Salir.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Salir.Location = New System.Drawing.Point(261, 175)
        Me.Btn_Salir.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Btn_Salir.Name = "Btn_Salir"
        Me.Btn_Salir.Size = New System.Drawing.Size(145, 42)
        Me.Btn_Salir.TabIndex = 11
        Me.Btn_Salir.Text = "&Salir"
        Me.Btn_Salir.UseVisualStyleBackColor = True
        '
        'Btn_ReImprimir
        '
        Me.Btn_ReImprimir.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_ReImprimir.Location = New System.Drawing.Point(87, 175)
        Me.Btn_ReImprimir.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Btn_ReImprimir.Name = "Btn_ReImprimir"
        Me.Btn_ReImprimir.Size = New System.Drawing.Size(147, 42)
        Me.Btn_ReImprimir.TabIndex = 10
        Me.Btn_ReImprimir.Text = "&Imprimir"
        Me.Btn_ReImprimir.UseVisualStyleBackColor = True
        '
        'Cmb_serie
        '
        Me.Cmb_serie.FormattingEnabled = True
        Me.Cmb_serie.Location = New System.Drawing.Point(114, 54)
        Me.Cmb_serie.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Cmb_serie.Name = "Cmb_serie"
        Me.Cmb_serie.Size = New System.Drawing.Size(292, 23)
        Me.Cmb_serie.TabIndex = 7
        '
        'CMBSerie
        '
        Me.CMBSerie.AutoSize = True
        Me.CMBSerie.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBSerie.Location = New System.Drawing.Point(61, 57)
        Me.CMBSerie.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBSerie.Name = "CMBSerie"
        Me.CMBSerie.Size = New System.Drawing.Size(49, 16)
        Me.CMBSerie.TabIndex = 6
        Me.CMBSerie.Text = "Serie:"
        '
        'Txt_inicio
        '
        Me.Txt_inicio.Location = New System.Drawing.Point(134, 109)
        Me.Txt_inicio.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Txt_inicio.Name = "Txt_inicio"
        Me.Txt_inicio.Size = New System.Drawing.Size(97, 21)
        Me.Txt_inicio.TabIndex = 12
        '
        'Txt_fin
        '
        Me.Txt_fin.Location = New System.Drawing.Point(306, 109)
        Me.Txt_fin.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Txt_fin.Name = "Txt_fin"
        Me.Txt_fin.Size = New System.Drawing.Size(99, 21)
        Me.Txt_fin.TabIndex = 13
        '
        'CMBinicio
        '
        Me.CMBinicio.AutoSize = True
        Me.CMBinicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBinicio.Location = New System.Drawing.Point(57, 111)
        Me.CMBinicio.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBinicio.Name = "CMBinicio"
        Me.CMBinicio.Size = New System.Drawing.Size(73, 16)
        Me.CMBinicio.TabIndex = 14
        Me.CMBinicio.Text = "Folio del:"
        '
        'CMBfin
        '
        Me.CMBfin.AutoSize = True
        Me.CMBfin.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBfin.Location = New System.Drawing.Point(239, 112)
        Me.CMBfin.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBfin.Name = "CMBfin"
        Me.CMBfin.Size = New System.Drawing.Size(64, 16)
        Me.CMBfin.TabIndex = 15
        Me.CMBfin.Text = "Folio al:"
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'FrmReimpresionSeries
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(488, 246)
        Me.Controls.Add(Me.CMBfin)
        Me.Controls.Add(Me.CMBinicio)
        Me.Controls.Add(Me.Txt_fin)
        Me.Controls.Add(Me.Txt_inicio)
        Me.Controls.Add(Me.Btn_Salir)
        Me.Controls.Add(Me.Btn_ReImprimir)
        Me.Controls.Add(Me.Cmb_serie)
        Me.Controls.Add(Me.CMBSerie)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.MaximizeBox = False
        Me.Name = "FrmReimpresionSeries"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reimpresion de Folios para Vendedores"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Btn_Salir As System.Windows.Forms.Button
    Friend WithEvents Btn_ReImprimir As System.Windows.Forms.Button
    Friend WithEvents Cmb_serie As System.Windows.Forms.ComboBox
    Friend WithEvents CMBSerie As System.Windows.Forms.Label
    Friend WithEvents Txt_inicio As System.Windows.Forms.TextBox
    Friend WithEvents Txt_fin As System.Windows.Forms.TextBox
    Friend WithEvents CMBinicio As System.Windows.Forms.Label
    Friend WithEvents CMBfin As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
