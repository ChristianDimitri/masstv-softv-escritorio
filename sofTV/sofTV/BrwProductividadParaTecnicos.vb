﻿Imports sofTV.BAL

Public Class BrwProductividadParaTecnicos
    Public IdProductividadParaTecnicos As Integer
    Private Sub BrwProductividadParaTecnicos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        recargaGrid()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        eOpcion = "N"
        Dim frm As FrmProductividadParaTecnicos = New FrmProductividadParaTecnicos()
        frm.ShowDialog()
        recargaGrid()
    End Sub

    Private Sub dgvDatos_CurrentCellChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvDatos.CurrentCellChanged
        Try

            IdProductividadParaTecnicos = Integer.Parse(Me.dgvDatos.CurrentRow.Cells(0).Value)

        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        eOpcion = "M"
        Dim frm As FrmProductividadParaTecnicos = New FrmProductividadParaTecnicos()
        frm.IdProductividadParaTecnicos = IdProductividadParaTecnicos
        frm.ShowDialog()
         recargaGrid()
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        If (dgvDatos.SelectedRows.Count > 0) Then
            If (MessageBox.Show("Desea Eliminar El Registro con la Clave " + IdProductividadParaTecnicos.ToString() + " ", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes) Then

                ProductividadParaTecnico.Delete(IdProductividadParaTecnicos)
                recargaGrid()
            End If
        Else
            MessageBox.Show("No Hay mas Valores A Eliminar")
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Public Sub recargaGrid()
        dgvDatos.DataSource = ProductividadParaTecnico.GetAll()
    End Sub

End Class