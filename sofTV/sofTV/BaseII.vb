﻿Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports CrystalDecisions.CrystalReports.Engine

Public Class BaseII

    Public Shared listParametros As New List(Of SqlParameter)
    Public Shared dicoPar As New Dictionary(Of String, Object)

    Public Shared Sub limpiaParametros()
        listParametros = New List(Of SqlParameter)
    End Sub

    '''''''SOBRE-ESCRITURA DEL MÉTODO QUE SE LE PUEDE ENVIAR UN PROCEDIMIENTO PARA EJECUTARLO CON PARÁMETROS
    Public Shared Sub Inserta(ByVal Procedure As String)
        Dim dc As New DataSet()
        Dim CON As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand(Procedure, CON)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0

        If listParametros.Count > 0 Then
            cmd.Parameters.AddRange(listParametros.ToArray())
        End If

        Try
            CON.Open()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub


    Public Shared Function ProcedimientoOutPut(ByVal Procedure As String) As Dictionary(Of String, Object)
        dicoPar = New Dictionary(Of String, Object)

        Dim dc As New DataSet()
        Dim CON As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand(Procedure, CON)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        If listParametros.Count > 0 Then
            cmd.Parameters.AddRange(listParametros.ToArray())
        End If

        Try
            CON.Open()
            cmd.ExecuteNonQuery()

            For Each par As SqlParameter In cmd.Parameters
                If (par.Direction = ParameterDirection.Output) Then
                    dicoPar.Add(par.ParameterName, par.Value)
                End If
            Next

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
        ProcedimientoOutPut = dicoPar
    End Function

    '''''''MÉTODO QUE DEVUELVE UN DATATABLE ENVIANDOLE UN PROCEDIMIENTO CON PARÁMETROS (N NÚMERO DE PARÁMETROS)
    Public Shared Function ConsultaDT(ByVal Procedure As String) As DataTable
        Dim DT As New DataTable()
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand(Procedure, CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.CommandTimeout = 0
        If listParametros.Count > 0 Then
            CMD.Parameters.AddRange(listParametros.ToArray())
        End If

        Dim DA As New SqlDataAdapter(CMD)
        DA.Fill(DT)
        ConsultaDT = DT
    End Function

    '''''FUNCIÓN QUE DEVUELVE UN VALOR BOOLEANO ENVIANDOLE UN PROCEDIMIENTO CON PARÁMETROS(N NÚMERO DE PARÁMETROS)
    Public Shared Function ConsultaBol(ByVal Procedure As String) As Boolean
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand(Procedure, CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.CommandTimeout = 0
        If listParametros.Count > 0 Then
            CMD.Parameters.AddRange(listParametros.ToArray())
        End If

        Try
            CON.Open()
            ConsultaBol = CMD.ExecuteScalar
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Function

    ''''''''''''''''MÉTODO QUE DEVUELVE UN PARÁMETRO AL CUÁL SE LE PASA EL NOMBRE,TIPO DE DATO Y EL VALOR DE ESTE (PARAMETROS QUE NO REQUIEREN ESPECÍFICAR LONGITUD)
    Public Shared Sub CreateMyParameter(ByVal prmNombre As String, ByRef prmTipo As SqlDbType, ByRef prmValor As Object)
        Dim par As New SqlParameter(prmNombre, prmTipo)
        par.Value = prmValor
        listParametros.Add(par)
    End Sub

    ''''''''''''''''SOBRE-ESCRITURA DEL MÉTODO QUE DEVUELVE UN PARÁMETRO AL CUÁL SE LE PASA EL NOMBRE,TIPO DE DATO,TAMAÑO Y EL VALOR DE ESTE (PARAMETROS QUE REQUIEREN ESPECÍFICAR LONGITUD)
    Public Shared Sub CreateMyParameter(ByVal prmNombre As String, ByRef prmTipo As SqlDbType, ByRef prmValor As Object, ByRef prmTamanio As Integer)
        Dim par As New SqlParameter(prmNombre, prmTipo, prmTamanio)
        par.Value = prmValor
        listParametros.Add(par)
    End Sub

    Public Shared Sub CreateMyParameter(ByVal prmNombre As String, ByRef prmDireccion As ParameterDirection, ByRef prmTipo As SqlDbType)
        Dim par As New SqlParameter(prmNombre, prmTipo)
        par.Direction = prmDireccion
        listParametros.Add(par)
    End Sub

    ''''''''''''''''SOBRE-ESCRITURA DEL MÉTODO QUE DEVUELVE UN PARÁMETRO AL CUÁL SE LE PASA EL NOMBRE,TIPO DE DATO,TAMAÑO,DIRECCIÓN Y EL VALOR DE ESTE (PARAMETROS QUE REQUIEREN ESPECÍFICAR LONGITUD)
    Public Shared Sub CreateMyParameter(ByVal prmNombre As String, ByRef prmTipo As SqlDbType, ByRef prmTamanio As Integer, ByRef prmDireccion As ParameterDirection, ByRef prmValor As Object)
        Dim par As New SqlParameter(prmNombre, prmTipo, prmTamanio)
        par.Direction = prmDireccion
        par.Value = prmValor
        listParametros.Add(par)
    End Sub
    Public Shared Sub CreateMyParameter(ByVal prmNombre As String, ByRef prmTipo As SqlDbType, ByRef prmDireccion As ParameterDirection)
        Dim par As New SqlParameter(prmNombre, prmTipo)
        par.Direction = prmDireccion
        listParametros.Add(par)
    End Sub
    Public Shared Sub CreateMyParameter(ByVal prmNombre As String, ByRef prmDireccion As ParameterDirection, ByRef prmTipo As SqlDbType, ByRef prmTamanio As Integer)
        Dim par As New SqlParameter(prmNombre, prmTipo, prmTamanio)
        par.Direction = prmDireccion
        listParametros.Add(par)
    End Sub


    '''''''''''''''SOBRE-ESCRITURA DEL MÉTODO QUE DEVUELVE UN DATASET DE UN PROCEDIMIENTO CON PARÁMETROS Y ASIGNACIÓN DE NOMBRE A LAS TABLAS QUE DEVUELVE EL DATASET  (TABLE.NAME)
    Public Shared Function ConsultaDS(ByVal Procedure As String, ByRef ltablesnames As List(Of String)) As DataSet
        Dim dc As New DataSet()
        Dim cnn As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand(Procedure, cnn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        If listParametros.Count > 0 Then
            cmd.Parameters.AddRange(listParametros.ToArray())
        End If

        Dim da As New SqlDataAdapter(cmd)
        da.Fill(dc)
        Dim i As Integer = 0
        For i = 0 To ltablesnames.Count - 1
            dc.Tables(i).TableName = ltablesnames(i)
        Next
        Return dc
    End Function

    ''''''''''''''MÉTODO QUE MANDA EL REPORTE A UN REPORT DOCUMENT Y SÓLO SE REQUIERE PASAR EL NOMRE DEL REPORTE
    Public Shared Sub llamarReporteCentralizado(ByVal prmNombreReporte As String, ByRef prmdc As DataSet)

        Dim frmic As New FrmImprimirCentralizada()
        Dim rd As New ReportDocument()
        rd.Load(prmNombreReporte + ".rpt")
        rd.SetDataSource(prmdc)
        frmic.rd = rd
        frmic.ShowDialog()
    End Sub

    ''''''''''''''MÉTODO QUE MANDA EL REPORTE A UN REPORT DOCUMENT Y SÓLO SE REQUIERE PASAR EL NOMRE DEL REPORTE Y EL ARREGLO DE FORMULAS (EN CASO DE HABERLAS)
    Public Shared Sub llamarReporteCentralizado(ByVal prmNombreReporte As String, ByRef prmdc As DataSet, ByRef prmDicoFormulas As Dictionary(Of String, String))

        Dim frmic As New FrmImprimirCentralizada()
        Dim rd As New ReportDocument()
        rd.Load(prmNombreReporte + ".rpt")
        rd.SetDataSource(prmdc)

        For Each kvp As KeyValuePair(Of String, String) In prmDicoFormulas
            rd.DataDefinition.FormulaFields(kvp.Key).Text = "'" & kvp.Value & "'"
        Next kvp
        frmic.rd = rd
        frmic.Text = "Vista Previa para Imprimir"
        frmic.ShowDialog()
    End Sub

    ''''''''''''''MÉTODO QUE MANDA EL REPORTE A UN REPORT DOCUMENT Y SÓLO SE REQUIERE PASAR EL NOMRE DEL REPORTE, EL ARREGLO DE PARÁMETROS, ARREGLO DE FORMULAS (EN CASO DE HABERLAS) O PARAMETROS
    Public Shared Sub llamarReporteCentralizado(ByVal prmNombreReporte As String, ByRef prmdc As DataSet, ByRef prmDicoParametros As Dictionary(Of String, String), ByRef prmDicoFormulas As Dictionary(Of String, String))

        Dim frmic As New FrmImprimirCentralizada()
        Dim rd As New ReportDocument()
        rd.Load(prmNombreReporte + ".rpt")
        rd.SetDataSource(prmdc)
        For Each kvp As KeyValuePair(Of String, String) In prmDicoParametros
            rd.SetParameterValue(kvp.Key, kvp.Value)
        Next kvp

        For Each kvp As KeyValuePair(Of String, String) In prmDicoFormulas
            rd.DataDefinition.FormulaFields(kvp.Key).Text = "'" & kvp.Value & "'"
        Next kvp
        frmic.rd = rd
        frmic.ShowDialog()
        'frmic.rd = rd
    End Sub

    'Public Shared listParametros As New List(Of SqlParameter)
    'Public Shared dicoPar As New Dictionary(Of String, Object)

    'Public Shared Sub limpiaParametros()
    '    listParametros = New List(Of SqlParameter)
    'End Sub

    ''''''''SOBRE-ESCRITURA DEL MÉTODO QUE SE LE PUEDE ENVIAR UN PROCEDIMIENTO PARA EJECUTARLO CON PARÁMETROS
    'Public Shared Sub Inserta(ByVal Procedure As String)
    '    Dim dc As New DataSet()
    '    Dim CON As New SqlConnection(MiConexion)
    '    Dim cmd As New SqlCommand(Procedure, CON)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.CommandTimeout = 0
    '    If listParametros.Count > 0 Then
    '        cmd.Parameters.AddRange(listParametros.ToArray())
    '    End If

    '    Try
    '        CON.Open()
    '        cmd.ExecuteNonQuery()
    '    Catch ex As Exception
    '        MsgBox(ex.Message, MsgBoxStyle.Information)
    '    Finally
    '        CON.Close()
    '        CON.Dispose()
    '    End Try
    'End Sub

    'Public Shared Function ProcedimientoOutPut(ByVal Procedure As String) As Dictionary(Of String, Object)
    '    dicoPar = New Dictionary(Of String, Object)

    '    Dim dc As New DataSet()
    '    Dim CON As New SqlConnection(MiConexion)
    '    Dim cmd As New SqlCommand(Procedure, CON)
    '    cmd.CommandType = CommandType.StoredProcedure

    '    If listParametros.Count > 0 Then
    '        cmd.Parameters.AddRange(listParametros.ToArray())
    '    End If

    '    Try
    '        CON.Open()
    '        cmd.ExecuteNonQuery()

    '        For Each par As SqlParameter In cmd.Parameters
    '            If (par.Direction = ParameterDirection.Output) Then
    '                dicoPar.Add(par.ParameterName, par.Value)
    '            End If
    '        Next

    '    Catch ex As Exception
    '        MsgBox(ex.Message, MsgBoxStyle.Information)
    '    Finally
    '        CON.Close()
    '        CON.Dispose()
    '    End Try
    '    ProcedimientoOutPut = dicoPar
    'End Function

    ''Public Shared Function ProcedimientoOutPut(ByVal Procedure As String) As Dictionary(Of String, Object)
    ''    dicoPar = New Dictionary(Of String, Object)

    ''    Dim dc As New DataSet()
    ''    Dim CON As New SqlConnection(MiConexion)
    ''    Dim cmd As New SqlCommand(Procedure, CON)
    ''    cmd.CommandType = CommandType.StoredProcedure
    ''    cmd.CommandTimeout = 0
    ''    If listParametros.Count > 0 Then
    ''        cmd.Parameters.AddRange(listParametros.ToArray())
    ''    End If

    ''    Try
    ''        CON.Open()
    ''        cmd.ExecuteNonQuery()

    ''        For Each par As SqlParameter In cmd.Parameters
    ''            If (par.Direction = ParameterDirection.Output) Then
    ''                dicoPar.Add(par.ParameterName, par.Value)
    ''            End If
    ''        Next

    ''    Catch ex As Exception
    ''        MsgBox(ex.Message, MsgBoxStyle.Information)
    ''    Finally
    ''        CON.Close()
    ''        CON.Dispose()
    ''    End Try
    ''    ProcedimientoOutPut = dicoPar
    ''End Function

    ''''''''MÉTODO QUE DEVUELVE UN DATATABLE ENVIANDOLE UN PROCEDIMIENTO CON PARÁMETROS (N NÚMERO DE PARÁMETROS)
    'Public Shared Function ConsultaDT(ByVal Procedure As String) As DataTable
    '    Dim DT As New DataTable()
    '    Dim CON As New SqlConnection(MiConexion)
    '    Dim CMD As New SqlCommand(Procedure, CON)
    '    CMD.CommandType = CommandType.StoredProcedure
    '    CMD.CommandTimeout = 0
    '    If listParametros.Count > 0 Then
    '        CMD.Parameters.AddRange(listParametros.ToArray())
    '    End If

    '    Dim DA As New SqlDataAdapter(CMD)
    '    DA.Fill(DT)
    '    ConsultaDT = DT
    'End Function

    ''''''FUNCIÓN QUE DEVUELVE UN VALOR BOOLEANO ENVIANDOLE UN PROCEDIMIENTO CON PARÁMETROS(N NÚMERO DE PARÁMETROS)
    'Public Shared Function ConsultaBol(ByVal Procedure As String) As Boolean
    '    Dim CON As New SqlConnection(MiConexion)
    '    Dim CMD As New SqlCommand(Procedure, CON)
    '    CMD.CommandType = CommandType.StoredProcedure
    '    CMD.CommandTimeout = 0
    '    If listParametros.Count > 0 Then
    '        CMD.Parameters.AddRange(listParametros.ToArray())
    '    End If

    '    Try
    '        CON.Open()
    '        ConsultaBol = CMD.ExecuteScalar
    '    Catch ex As Exception
    '        MsgBox(ex.Message, MsgBoxStyle.Information)
    '    Finally
    '        CON.Close()
    '        CON.Dispose()
    '    End Try
    'End Function

    '''''''''''''''''MÉTODO QUE DEVUELVE UN PARÁMETRO AL CUÁL SE LE PASA EL NOMBRE,TIPO DE DATO Y EL VALOR DE ESTE (PARAMETROS QUE NO REQUIEREN ESPECÍFICAR LONGITUD)
    'Public Shared Sub CreateMyParameter(ByVal prmNombre As String, ByRef prmTipo As SqlDbType, ByRef prmValor As Object)
    '    Dim par As New SqlParameter(prmNombre, prmTipo)
    '    par.Value = prmValor
    '    listParametros.Add(par)
    'End Sub

    '''''''''''''''''SOBRE-ESCRITURA DEL MÉTODO QUE DEVUELVE UN PARÁMETRO AL CUÁL SE LE PASA EL NOMBRE,TIPO DE DATO,TAMAÑO Y EL VALOR DE ESTE (PARAMETROS QUE REQUIEREN ESPECÍFICAR LONGITUD)
    'Public Shared Sub CreateMyParameter(ByVal prmNombre As String, ByRef prmTipo As SqlDbType, ByRef prmValor As Object, ByRef prmTamanio As Integer)
    '    Dim par As New SqlParameter(prmNombre, prmTipo, prmTamanio)
    '    par.Value = prmValor
    '    listParametros.Add(par)
    'End Sub

    'Public Shared Sub CreateMyParameter(ByVal prmNombre As String, ByRef prmDireccion As ParameterDirection, ByRef prmTipo As SqlDbType)
    '    Dim par As New SqlParameter(prmNombre, prmTipo)
    '    par.Direction = prmDireccion
    '    listParametros.Add(par)
    'End Sub

    '''''''''''''''''SOBRE-ESCRITURA DEL MÉTODO QUE DEVUELVE UN PARÁMETRO AL CUÁL SE LE PASA EL NOMBRE,TIPO DE DATO,TAMAÑO,DIRECCIÓN Y EL VALOR DE ESTE (PARAMETROS QUE REQUIEREN ESPECÍFICAR LONGITUD)
    'Public Shared Sub CreateMyParameter(ByVal prmNombre As String, ByRef prmTipo As SqlDbType, ByRef prmTamanio As Integer, ByRef prmDireccion As ParameterDirection, ByRef prmValor As Object)
    '    Dim par As New SqlParameter(prmNombre, prmTipo, prmTamanio)
    '    par.Direction = prmDireccion
    '    par.Value = prmValor
    '    listParametros.Add(par)
    'End Sub
    'Public Shared Sub CreateMyParameter(ByVal prmNombre As String, ByRef prmTipo As SqlDbType, ByRef prmDireccion As ParameterDirection)
    '    Dim par As New SqlParameter(prmNombre, prmTipo)
    '    par.Direction = prmDireccion
    '    listParametros.Add(par)
    'End Sub

    ''''''''''''''''''MÉTODO QUE DEVUELVE UN PARÁMETRO AL CUÁL SE LE PASA EL NOMBRE,TIPO DE DATO Y EL VALOR DE ESTE (PARAMETROS QUE NO REQUIEREN ESPECÍFICAR LONGITUD)
    ''Public Shared Sub CreateMyParameter(ByVal prmNombre As String, ByRef prmTipo As SqlDbType, ByRef prmValor As Object)
    ''    Dim par As New SqlParameter(prmNombre, prmTipo)
    ''    par.Value = prmValor
    ''    listParametros.Add(par)
    ''End Sub

    ''''''''''''''''''SOBRE-ESCRITURA DEL MÉTODO QUE DEVUELVE UN PARÁMETRO AL CUÁL SE LE PASA EL NOMBRE,TIPO DE DATO,TAMAÑO Y EL VALOR DE ESTE (PARAMETROS QUE REQUIEREN ESPECÍFICAR LONGITUD)
    ''Public Shared Sub CreateMyParameter(ByVal prmNombre As String, ByRef prmTipo As SqlDbType, ByRef prmValor As Object, ByRef prmTamanio As Integer)
    ''    Dim par As New SqlParameter(prmNombre, prmTipo, prmTamanio)
    ''    par.Value = prmValor
    ''    listParametros.Add(par)
    ''End Sub

    ''Public Shared Sub CreateMyParameter(ByVal prmNombre As String, ByRef prmDireccion As ParameterDirection, ByRef prmTipo As SqlDbType)
    ''    Dim par As New SqlParameter(prmNombre, prmTipo)
    ''    par.Direction = prmDireccion
    ''    listParametros.Add(par)
    ''End Sub

    ''''''''''''''''''SOBRE-ESCRITURA DEL MÉTODO QUE DEVUELVE UN PARÁMETRO AL CUÁL SE LE PASA EL NOMBRE,TIPO DE DATO,TAMAÑO,DIRECCIÓN Y EL VALOR DE ESTE (PARAMETROS QUE REQUIEREN ESPECÍFICAR LONGITUD)
    ''Public Shared Sub CreateMyParameter(ByVal prmNombre As String, ByRef prmTipo As SqlDbType, ByRef prmTamanio As Integer, ByRef prmDireccion As ParameterDirection, ByRef prmValor As Object)
    ''    Dim par As New SqlParameter(prmNombre, prmTipo, prmTamanio)
    ''    par.Direction = prmDireccion
    ''    par.Value = prmValor
    ''    listParametros.Add(par)
    ''End Sub

    ''''''''''''''''SOBRE-ESCRITURA DEL MÉTODO QUE DEVUELVE UN DATASET DE UN PROCEDIMIENTO CON PARÁMETROS Y ASIGNACIÓN DE NOMBRE A LAS TABLAS QUE DEVUELVE EL DATASET  (TABLE.NAME)
    'Public Shared Function ConsultaDS(ByVal Procedure As String, ByRef ltablesnames As List(Of String)) As DataSet
    '    Dim dc As New DataSet()
    '    Dim cnn As New SqlConnection(MiConexion)
    '    Dim cmd As New SqlCommand(Procedure, cnn)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.CommandTimeout = 0
    '    If listParametros.Count > 0 Then
    '        cmd.Parameters.AddRange(listParametros.ToArray())
    '    End If

    '    Dim da As New SqlDataAdapter(cmd)
    '    da.Fill(dc)
    '    Dim i As Integer = 0
    '    For i = 0 To ltablesnames.Count - 1
    '        dc.Tables(i).TableName = ltablesnames(i)
    '    Next
    '    Return dc
    'End Function

    '''''''''''''''MÉTODO QUE MANDA EL REPORTE A UN REPORT DOCUMENT Y SÓLO SE REQUIERE PASAR EL NOMRE DEL REPORTE
    'Public Shared Sub llamarReporteCentralizado(ByVal prmNombreReporte As String, ByRef prmdc As DataSet)

    '    Dim frmic As New FrmImprimirCentralizada()
    '    Dim rd As New ReportDocument()
    '    rd.Load(prmNombreReporte + ".rpt")
    '    rd.SetDataSource(prmdc)
    '    frmic.rd = rd
    '    frmic.ShowDialog()
    'End Sub

    '''''''''''''''MÉTODO QUE MANDA EL REPORTE A UN REPORT DOCUMENT Y SÓLO SE REQUIERE PASAR EL NOMRE DEL REPORTE Y EL ARREGLO DE FORMULAS (EN CASO DE HABERLAS)
    'Public Shared Sub llamarReporteCentralizado(ByVal prmNombreReporte As String, ByRef prmdc As DataSet, ByRef prmDicoFormulas As Dictionary(Of String, String))

    '    Dim frmic As New FrmImprimirCentralizada()
    '    Dim rd As New ReportDocument()
    '    rd.Load(prmNombreReporte + ".rpt")
    '    rd.SetDataSource(prmdc)

    '    For Each kvp As KeyValuePair(Of String, String) In prmDicoFormulas
    '        rd.DataDefinition.FormulaFields(kvp.Key).Text = "'" & kvp.Value & "'"
    '    Next kvp
    '    frmic.rd = rd
    '    frmic.Text = "Vista Previa para Imprimir"
    '    frmic.ShowDialog()
    'End Sub

    '''''''''''''''MÉTODO QUE MANDA EL REPORTE A UN REPORT DOCUMENT Y SÓLO SE REQUIERE PASAR EL NOMRE DEL REPORTE, EL ARREGLO DE PARÁMETROS, ARREGLO DE FORMULAS (EN CASO DE HABERLAS) O PARAMETROS
    'Public Shared Sub llamarReporteCentralizado(ByVal prmNombreReporte As String, ByRef prmdc As DataSet, ByRef prmDicoParametros As Dictionary(Of String, String), ByRef prmDicoFormulas As Dictionary(Of String, String))

    '    Dim frmic As New FrmImprimirCentralizada()
    '    Dim rd As New ReportDocument()
    '    rd.Load(prmNombreReporte + ".rpt")
    '    rd.SetDataSource(prmdc)
    '    For Each kvp As KeyValuePair(Of String, String) In prmDicoParametros
    '        rd.SetParameterValue(kvp.Key, kvp.Value)
    '    Next kvp

    '    For Each kvp As KeyValuePair(Of String, String) In prmDicoFormulas
    '        rd.DataDefinition.FormulaFields(kvp.Key).Text = "'" & kvp.Value & "'"
    '    Next kvp
    '    frmic.rd = rd
    '    frmic.ShowDialog()
    '    'frmic.rd = rd
    'End Sub
End Class


