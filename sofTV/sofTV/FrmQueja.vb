﻿
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic

Public Class FrmQueja
    Private customersByCityReport As ReportDocument
    Private Cadena As String
    Private LocFecEje As Boolean = False
    Private LocTec As Boolean = False
    Private LocTipQue As Boolean = False
    Private LocTipQue1 As Boolean = False
    Private LocSolucion As Boolean = False
    Dim bloq As Integer = 0
    Dim clvTecnicoDescarga As Integer
    Dim clvBitacoraDescarga As Integer
    Public GloClienteBloqueado As Boolean = False

    Private Sub ACTIVA(ByVal BND As Boolean)
        Me.ComboBox4.Enabled = BND
        If opcion <> "N" Then
            Me.ComboBox5.Enabled = False
        Else
            Me.ComboBox5.Enabled = True
        End If
        Me.BindingNavigatorDeleteItem.Enabled = BND
        RadioButton2.Enabled = BND
        RadioButton3.Enabled = BND
        '' Fecha_EjecucionMaskedTextBox.Enabled = BND
        SolucionTextBox.Enabled = BND
        Panel2.Enabled = BND
        Me.Fecha_EjecucionMaskedTextBox.Enabled = BND
        Me.Visita1MaskedTextBox.Enabled = BND
        Me.Visita2MaskedTextBox.Enabled = BND
        Me.Visita3MaskedTextBox.Enabled = BND
        If BND = True Then
            Me.RadioButton1.Enabled = False
            Me.ContratoTextBox.Enabled = False
            Me.Button1.Enabled = False
            Me.ComboBox1.Enabled = False
            Me.ProblemaTextBox.Enabled = False
            'Me.Fecha_EjecucionMaskedTextBox.Enabled = False
            'Me.Visita1MaskedTextBox.Enabled = False
            'Me.Visita2MaskedTextBox.Enabled = False
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        If dameStatusOrdenQueja(gloClave, "Q") = "P" And opcion = "M" Then
            Dim res = MsgBox("¿Deseas salir sin guardar la Descarga de Material?", MsgBoxStyle.YesNo)
            If res = MsgBoxResult.Yes Then
                softv_BorraDescarga(gloClave, "Q")
            Else
                Exit Sub
            End If
        End If
        FechaAgenda = ""
        HoraAgenda = ""
        Clv_HoraAgenda = 0
        ComentarioAgenda = ""
        Me.Close()
    End Sub

    Private Sub CONQUEJASBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)


    End Sub

    'Private Sub FillToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Me.CONQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.CONQUEJAS, New System.Nullable(Of Long)(CType(Clv_QuejaToolStripTextBox.Text, Long)), New System.Nullable(Of Integer)(CType(Clv_TipSerToolStripTextBox.Text, Integer)))
    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '   End Try

    'End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        FechaAgenda = ""
        HoraAgenda = ""
        Clv_HoraAgenda = 0
        ComentarioAgenda = ""
        Me.CONQUEJASBindingSource.CancelEdit()
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Ban", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.CreateMyParameter("@CLV_QUEJA", SqlDbType.BigInt, gloClave)
        Dim dico As Dictionary(Of String, Object) = BaseII.ProcedimientoOutPut("VALIDABORRARQUEJAS")
        If (CBool(dico("@Ban").ToString())) Then
            FechaAgenda = ""
            HoraAgenda = ""
            ComentarioAgenda = ""
            Clv_HoraAgenda = 0
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.CONQUEJASTableAdapter.Connection = CON
            Me.CONQUEJASTableAdapter.Delete(gloClave, GloClv_TipSer)
            CON.Close()
            GloBnd = True
            Me.Close()
        Else
            MessageBox.Show("No Se Puede Eliminar Una Queja Con Descarga De Material")
        End If
       
    End Sub

    Private Sub CONQUEJASBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONQUEJASBindingNavigatorSaveItem.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            Dim clv_prioridadQueja As Integer

            If Len(Trim(Me.ComboBox5.Text)) > 0 Then
                If IsNumeric(Me.ContratoTextBox.Text) = True Then
                    If Len(Trim(Me.ProblemaTextBox.Text)) > 0 Then
                        If Len(Trim(Me.ComboBox1.SelectedValue)) > 0 Then
                            If Len(Trim(Me.cmbPrioridadQueja.SelectedValue)) > 0 Then
                                clv_prioridadQueja = Me.cmbPrioridadQueja.SelectedValue
                                If Me.StatusTextBox.Text = "E" Then
                                    If Len(Trim(Me.ComboBox4.SelectedValue)) = 0 Then
                                        MsgBox("Se requiere el Tipo de Queja")
                                        Exit Sub
                                    End If
                                    If Len(Trim(Me.ComboBox4.Text)) = 0 Then
                                        MsgBox("Se requiere el Tipo de Queja")
                                        Exit Sub
                                    End If
                                    If Len(Trim(Me.SolucionTextBox.Text)) = 0 Then
                                        MsgBox("Se requiere que capture la Solucción ", MsgBoxStyle.Information)
                                        Exit Sub
                                    End If
                                    If Me.cmbClasifProblemas.SelectedValue = 0 Then
                                        MsgBox("Se requiere que capture la Clasificación del Problema", MsgBoxStyle.Information)
                                        Exit Sub
                                    End If

                                    Dim Pasa As Integer = 0
                                    If IsDate(Mid(Me.Fecha_EjecucionMaskedTextBox.Text, 1, 10)) = True Then
                                        Dim Fecha As Date = Mid(Me.Fecha_EjecucionMaskedTextBox.Text, 1, 10)
                                        If DateValue(Fecha) >= DateValue(Me.FechaTextBox.Text) And DateValue(Fecha) <= DateValue(Me.FechaDateTimePicker.Value) Then

                                            If FechaTextBox.Text.Substring(0, 10) = Fecha_EjecucionMaskedTextBox.Text Then
                                                If CDate(FechaTextBox.Text.Substring(11, 10)) < CDate(Hora_sol.Value.ToShortTimeString) Then

                                                Else
                                                    MsgBox("La Hora de Ejecución: " + Hora_sol.Value.ToShortTimeString + " no puede ser menor a la Hora de Solicitud: " + CDate(FechaTextBox.Text).ToShortTimeString, MsgBoxStyle.Information)
                                                    Me.Hora_sol.Value = DateTime.Now
                                                    Exit Sub
                                                End If
                                            End If

                                            Pasa = 1
                                        Else
                                            MsgBox("La Fecha de la ejecución no puede ser menor a la fecha de solicitud ni mayo a la fecha actual ", MsgBoxStyle.Information)
                                            Me.Fecha_EjecucionMaskedTextBox.Clear()
                                            Exit Sub
                                        End If
                                    Else
                                        MsgBox("La Fecha de Ejecución es Invalida")
                                        Me.Fecha_EjecucionMaskedTextBox.Clear()
                                        Exit Sub
                                    End If
                                    If IsDate(Me.Fecha_EjecucionMaskedTextBox.Text) = False Then
                                        MsgBox("Se requiere que capture la fecha y la hora de ejecución de forma correcta por favor", MsgBoxStyle.Information)
                                        Exit Sub
                                    End If

                                ElseIf Me.StatusTextBox.Text = "V" Then
                                    'Visita1 INICIO
                                    If Len(Me.ComboBox2.Text) = 0 Then
                                        MsgBox("Seleccione un técnico ", MsgBoxStyle.Information)
                                        Exit Sub
                                    End If
                                    Dim Pasa As Integer = 0
                                    If IsDate(Mid(Me.Visita1MaskedTextBox.Text, 1, 10)) = True Then
                                        Dim Fecha As Date = Mid(Me.Visita1MaskedTextBox.Text, 1, 10)
                                        If DateValue(Fecha) >= DateValue(Me.FechaTextBox.Text) And DateValue(Fecha) <= DateValue(Me.FechaDateTimePicker.Value) Then
                                            Pasa = 1
                                        Else
                                            MsgBox("La Fecha de la Visita no puede ser menor a la fecha de solicitud ni mayor a la fecha actual ", MsgBoxStyle.Information)
                                            Me.Visita1MaskedTextBox.Clear()
                                            Exit Sub
                                        End If
                                    Else
                                        MsgBox("La Fecha es Invalida")
                                        Me.Visita1MaskedTextBox.Clear()
                                        Exit Sub
                                    End If
                                    'Visita1 FIN              
                                    'Visita2 INICIO
                                    If IsDate(Mid(Me.Visita2MaskedTextBox.Text, 1, 10)) = True Then
                                        Dim Fecha As Date = Mid(Me.Visita2MaskedTextBox.Text, 1, 10)
                                        If DateValue(Fecha) >= DateValue(Me.FechaTextBox.Text) And DateValue(Fecha) <= DateValue(Me.FechaDateTimePicker.Value) Then
                                            Pasa = 1
                                        Else
                                            MsgBox("La Fecha de la Visita no puede ser menor a la fecha de solicitud ni mayor a la fecha actual ", MsgBoxStyle.Information)
                                            Me.Visita2MaskedTextBox.Clear()
                                            Exit Sub
                                        End If
                                    Else
                                        'MsgBox("La Fecha es Invalida")
                                        Me.Visita2MaskedTextBox.Clear()
                                    End If
                                    If IsDate(Me.Visita2MaskedTextBox.Text) = False Then
                                        'MsgBox("Se requiere que capture la fecha y la hora de Visita de forma correcta por favor", MsgBoxStyle.Information)
                                        Me.Visita2MaskedTextBox.Clear()
                                    End If
                                    'Visita 2 FIN
                                    'Visita3 INICIO
                                    If IsDate(Mid(Me.Visita3MaskedTextBox.Text, 1, 10)) = True Then
                                        Dim Fecha As Date = Mid(Me.Visita3MaskedTextBox.Text, 1, 10)
                                        If DateValue(Fecha) >= DateValue(Me.FechaTextBox.Text) And DateValue(Fecha) <= DateValue(Me.FechaDateTimePicker.Value) Then
                                            Pasa = 1
                                        Else
                                            MsgBox("La Fecha de la Visita no puede ser menor a la fecha de solicitud ni mayor a la fecha actual ", MsgBoxStyle.Information)
                                            Me.Visita3MaskedTextBox.Clear()
                                            Exit Sub
                                        End If
                                    Else
                                        'MsgBox("La Fecha  es Invalida")
                                        Me.Visita3MaskedTextBox.Clear()
                                    End If
                                    If IsDate(Me.Visita3MaskedTextBox.Text) = False Then
                                        'MsgBox("Se requiere que capture la fecha y la hora de Visita de forma correcta por favor", MsgBoxStyle.Information)
                                        Me.Visita3MaskedTextBox.Clear()
                                    End If
                                    'Visita3 FIN   

                                ElseIf Me.StatusTextBox.Text = "S" Then 'ESTO LO PUSO JUANJO PARA EL NUEVO STATUS EN PROCESO DE LA QUEJA (INICIO)
                                    If Len(Me.ComboBox2.Text) = 0 Then
                                        MsgBox("Seleccione un técnico ", MsgBoxStyle.Information)
                                        Exit Sub
                                    End If
                                    Dim Pasa As Integer = 0
                                    If IsDate(Mid(Me.EnProcesoMaskedTextBox.Text, 1, 10)) = True Then
                                        Dim Fecha As Date = Mid(Me.EnProcesoMaskedTextBox.Text, 1, 10)
                                        If DateValue(Fecha) >= DateValue(Me.FechaTextBox.Text) And DateValue(Fecha) <= DateValue(Me.FechaDateTimePicker.Value) Then
                                            Pasa = 1
                                        Else
                                            MsgBox("La Fecha no puede ser menor a la fecha de solicitud ni mayor a la fecha actual ", MsgBoxStyle.Information)
                                            Me.EnProcesoMaskedTextBox.Clear()
                                            Exit Sub
                                        End If
                                    Else
                                        MsgBox("La Fecha es Invalida")
                                        Me.EnProcesoMaskedTextBox.Clear()
                                        Exit Sub
                                    End If
                                    If IsDate(Me.EnProcesoMaskedTextBox.Text) = False Then
                                        'MsgBox("Se requiere que capture la fecha y la hora de Visita de forma correcta por favor", MsgBoxStyle.Information)
                                        Me.EnProcesoMaskedTextBox.Clear()
                                    End If
                                End If 'ESTO LO PUSO JUANJO PARA EL NUEVO STATUS EN PROCESO DE LA QUEJA (FIN)

                                'If Me.Button3.Visible = False Then
                                ' LocValida1 = True
                                'Else
                                '   Me.Dame_FolioTableAdapter.Fill(Me.DataSetarnoldo.Dame_Folio, 1, Locclv_folio)
                                '  Me.Inserta_Bitacora_tecTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Bitacora_tec, Locclv_folio, 1, Locclv_tec, GloUsuario, "P", "", LocNo_Bitacora)
                                ' Me.Inserta_DetBitTecTableAdapter.Fill(Me.DataSetarnoldo.Inserta_DetBitTec, LocNo_Bitacora, Locclv_folio, clv_sessionTecnico)
                                'Me.Inserta_Rel_Bitacora_QuejaTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Rel_Bitacora_Queja, LocNo_Bitacora, CInt(Me.Clv_QuejaTextBox.Text))
                                'End If

                                Me.Validate()
                                Me.CONQUEJASBindingSource.EndEdit()
                                Me.CONQUEJASTableAdapter.Connection = CON
                                Me.CONQUEJASTableAdapter.Update(Me.NewSofTvDataSet.CONQUEJAS)
                                If LocValida1 = True Then
                                    LocValida1 = False
                                    'gloClave = CLng(Me.Clv_QuejaTextBox.Text)
                                    Me.Dame_Folio_QTableAdapter.Connection = CON
                                    Me.Dame_Folio_QTableAdapter.Fill(Me.DataSetLidia.Dame_Folio_Q, Me.Clv_QuejaTextBox.Text, 1, Locclv_folio)
                                    Me.Inserta_Bitacora_tec_QTableAdapter.Connection = CON
                                    Me.Inserta_Bitacora_tec_QTableAdapter.Fill(Me.DataSetLidia.Inserta_Bitacora_tec_Q, clv_sessionTecnico, Me.Clv_QuejaTextBox.Text, Locclv_folio, 1, Locclv_tec, GloUsuario, "P", "", LocNo_Bitacora)
                                    Me.Inserta_Rel_Bitacora_QuejaTableAdapter.Connection = CON
                                    Me.Inserta_Rel_Bitacora_QuejaTableAdapter.Fill(Me.DataSetLidia.Inserta_Rel_Bitacora_Queja, LocNo_Bitacora, Me.Clv_QuejaTextBox.Text)
                                    Me.Inserta_RelCobraDescTableAdapter.Connection = CON
                                    Me.Inserta_RelCobraDescTableAdapter.Fill(Me.ProcedimientosArnoldo2.Inserta_RelCobraDesc, LocNo_Bitacora, "Q")
                                End If
                                Dim Clv_Cita As Long

                                Dim Clave As Long
                                FechaAgenda = Date.Today
                                HoraAgenda = "06:00"

                                If Len(Trim(FechaAgenda)) > 0 And Len(Trim(HoraAgenda)) Then
                                    Me.NUE_CITASTableAdapter.Connection = CON
                                    Me.NUE_CITASTableAdapter.Fill(Me.NewSofTvDataSet.NUE_CITAS, Me.ComboBox2.SelectedValue, New System.Nullable(Of Date)(CType(FechaAgenda, Date)), New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)), "", "", "Q", Clv_Cita)
                                    Me.CONDetCitasTableAdapter.Connection = CON
                                    Me.CONDetCitasTableAdapter.Delete(Clv_Cita)
                                    Me.CONDetCitasTableAdapter.Connection = CON
                                    Me.CONDetCitasTableAdapter.Insert(Clv_HoraAgenda, Clv_Cita, Clave)
                                    Me.NUEREL_CITAS_QUEJASTableAdapter.Connection = CON
                                    Me.NUEREL_CITAS_QUEJASTableAdapter.Fill(Me.NewSofTvDataSet.NUEREL_CITAS_QUEJAS, New System.Nullable(Of Long)(CType(Clv_Cita, Long)), New System.Nullable(Of Long)(CType(Me.Clv_QuejaTextBox.Text, Long)))
                                    NUEMOVREL_CITAS(Clv_Cita, ComentarioAgenda)
                                End If

                                'Eric RelQuejaUsuario
                                Me.NueRelQuejaUsuarioTableAdapter.Connection = CON
                                Me.NueRelQuejaUsuarioTableAdapter.Fill(Me.DataSetEric.NueRelQuejaUsuario, CLng(Me.Clv_QuejaTextBox.Text), GloClvUsuario, Me.StatusTextBox.Text)

                                'Cuadrilla---------------------------------------------------------------
                                If IsNumeric(Me.cbCuadrilla.SelectedValue) = True Then
                                    If cbCuadrilla.SelectedValue > 0 Then
                                        NueRelQuejaCuadrilla(Clv_QuejaTextBox.Text, cbCuadrilla.SelectedValue)
                                    End If
                                End If
                                '------------------------------------------------------------------------

                                'ALMACENA LA CLAVE DE LA CLASIFICACIÓN DEL PROBLEMA (INICIO) --JUANJO
                                Inserta(CLng(Me.Clv_QuejaTextBox.Text), CLng(Me.cmbClasifProblemas.SelectedValue))
                                'ALMACENA LA CLAVE DE LA CLASIFICACIÓN DEL PROBLEMA (FIN) --JUANJO

                                If opcion = "M" Then
                                    Checa_Si_Es_atenc_tel(CLng(Me.Clv_QuejaTextBox.Text), CLng(Me.ComboBox4.SelectedValue))
                                End If

                                'If opcion = "N" And IdSistema <> "VA" Then
                                'ConfigureCrystalReports_NewXml(0, "")
                                'End If

                                If Me.StatusTextBox.Text = "V" Then
                                    insertaVisitasQuejas(gloClave, IIf(Me.Visita1MaskedTextBox1.Text = "", "01/01/1900", Me.Visita1MaskedTextBox1.Text), IIf(Me.Visita2MaskedTextBox1.Text = "", "01/01/1900", Me.Visita2MaskedTextBox1.Text), IIf(Me.Visita3MaskedTextBox1.Text = "", "01/01/1900", Me.Visita3MaskedTextBox1.Text), FormatDateTime(Me.dtpHoraVisita1.Value, DateFormat.ShortTime), FormatDateTime(Me.dtpHoraVisita3.Value, DateFormat.ShortTime), FormatDateTime(Me.dtpHoraVisita3.Value, DateFormat.ShortTime))
                                End If

                                If Me.StatusTextBox.Text = "S" Then 'ESTO LO PUSO JUANJO PARA GUARDAR LA RELACIÓN DE LA QUEJA CON LA FECHA Y HORA DEL NUEVO STATUS (INICIO)
                                    spInsertaEnProcesoQuejas(gloClave, IIf(Me.EnProcesoMaskedTextBox.Text = "", "01/01/1900", Me.EnProcesoMaskedTextBox.Text), FormatDateTime(Me.dtpHoraVisita3.Value, DateFormat.ShortTime))
                                End If 'ESTO LO PUSO JUANJO PARA GUARDAR LA RELACIÓN DE LA QUEJA CON LA FECHA Y HORA DEL NUEVO STATUS (FIN)

                                BaseII.limpiaParametros()
                                BaseII.CreateMyParameter("@Clv_queja", SqlDbType.BigInt, Clv_QuejaTextBox.Text)
                                BaseII.CreateMyParameter("@clvPrioridadQueja", SqlDbType.Int, clv_prioridadQueja)
                                BaseII.Inserta("uspPrioridadQueja")

                                FechaAgenda = ""
                                HoraAgenda = ""
                                Clv_HoraAgenda = 0
                                ComentarioAgenda = ""
                                MsgBox(mensaje5)
                                If Me.StatusTextBox.Text = "E" Then
                                    Dim fechaejecucion As DateTime
                                    fechaejecucion = CDate(Fecha_EjecucionMaskedTextBox.Text)
                                    BaseII.limpiaParametros()
                                    BaseII.CreateMyParameter("@Clv_queja", SqlDbType.NVarChar, Clv_QuejaTextBox.Text)
                                    BaseII.CreateMyParameter("@Fecha", SqlDbType.NVarChar, fechaejecucion.ToString("yyyyMMdd") + " " + Hora_sol.Value.ToString("HH:mm:ss"))
                                    ' BaseII.CreateMyParameter("@Fecha", SqlDbType.NVarChar, Hora_sol.Value.Year.ToString + Hora_sol.Value.Month.ToString("MM") + Hora_sol.Value.Day.ToString("dd") + " " + Hora_sol.Value.ToShortTimeString)
                                    BaseII.Inserta("uspModificaFechaEjecucionQuejas")
                                End If
                                GloBnd = True
                                Me.Close()
                            Else
                                MsgBox("Se Requiere la Prioridad de la Queja", MsgBoxStyle.Information)
                            End If
                        Else
                            MsgBox("Se Requiere el Departamento Responsable", MsgBoxStyle.Information)
                        End If
                    Else
                        MsgBox("No a Capturado el Problema ", MsgBoxStyle.Information)
                    End If

                Else
                    MsgBox(mensaje7)
                End If
            Else
                MsgBox("Se Necesita El Tipo de Servicio De La Queja", MsgBoxStyle.Information)
            End If
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Checa_Si_Es_atenc_tel(ByVal clv_queja As Long, ByVal clv_trabajo As Long)
        Dim con1 As New SqlClient.SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()

        Try
            cmd = New SqlClient.SqlCommand()
            con1.Open()
            With cmd
                .CommandText = "Checa_Si_Es_atenc_tel"
                .CommandTimeout = 0
                .Connection = con1
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@clv_queja", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = clv_queja
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@clv_trabajo", SqlDbType.BigInt)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = clv_trabajo
                .Parameters.Add(prm1)

                Dim i As Integer = cmd.ExecuteNonQuery()

            End With
            con1.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub CREAARBOL()

        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim Y As Integer = 0
            Dim epasa As Boolean = True
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' msgbox(pRow("CustomerID").ToString())
            'Next

            If IsNumeric(Me.ContratoTextBox.Text) = True Then
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)))
            Else
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(0, Long)))
            End If
            Dim pasa As Boolean = False
            Dim Net As Boolean = False
            Dim dig As Boolean = False
            Dim jNet As Integer = -1
            Dim PasaJNet As Boolean = False
            Dim jDig As Integer = -1
            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.dameSerDELCli.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                'MsgBox(Mid(FilaRow("Servicio").ToString(), 1, 19))
                If Mid(FilaRow("Servicio").ToString(), 1, 3) = "---" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    Net = False
                    dig = False
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Servicio Basico" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 31) = "Servicios de Televisión Digital" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 21) = "Servicios de Internet" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    jNet = -1
                    jDig = -1
                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 22) = "Servicios de Teléfonia" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    jNet = -1
                    jDig = -1
                    pasa = True
                Else
                    If Mid(FilaRow("Servicio").ToString(), 1, 14) = "Mac Cablemodem" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jNet = jNet + 1
                        pasa = False
                        Net = True
                    ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Aparato Digital" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jDig = jDig + 1
                        pasa = False
                        dig = True
                    Else
                        If Net = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jNet).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        ElseIf dig = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jDig).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        Else
                            If epasa = True Then
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                pasa = False
                                epasa = False
                            Else
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                                epasa = False
                                pasa = False
                            End If

                        End If
                    End If
                End If
                If pasa = True Then I = I + 1
            Next
            CON.Close()
            'Me.TreeView1.Nodes(0).ExpandAll()
            For Y = 0 To (I - 1)
                Me.TreeView1.Nodes(Y).ExpandAll()
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub CREAARBOL88()

        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            Dim I As Integer = 0
            Dim X As Integer = 0
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' Console.WriteLine(pRow("CustomerID").ToString())
            'Next

            If IsNumeric(Me.ContratoTextBox.Text) = True Then
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)))
            Else
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(0, Long)))
            End If



            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.dameSerDELCli.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                Me.TreeView1.Nodes.Add(Trim(FilaRow("Servicio").ToString()))
                I += 1
            Next
            CON.Close()




        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub FrmQueja_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If GLOCONTRATOSEL > 0 Then
            Me.ContratoTextBox.Text = GLOCONTRATOSEL
            GLOCONTRATOSEL = 0

            'If (DABloqueoDeClientes.uspConsultaClientes_Bloqueados(Me.ContratoTextBox.Text)) Then
            '    FrmBloqueo.ShowDialog()
            '    Me.Close()
            '    Me.Dispose()
            'End If


        End If
        If GloBndTipSer = True Then
            GloBndTipSer = False
            Me.ComboBox5.SelectedValue = GloClv_TipSer
            Me.ComboBox5.Text = GloNom_TipSer
            Me.ComboBox5.FindString(GloNom_TipSer)
            Me.ComboBox5.Text = GloNom_TipSer
            GloBndTipSer = False
            Me.TextBox2.Text = GloNom_TipSer
            'Me.MUESTRATRABAJOSQUEJASTableAdapter.Connection = CON
            'Me.MUESTRATRABAJOSQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATRABAJOSQUEJAS, New System.Nullable(Of Integer)(CType(GloClv_TipSer, Integer)))
            Me.ComboBox4.Text = ""
        End If
        'If bloq = 1 Then
        '    bloq = 0
        '    eGloContrato = Me.ContratoTextBox.Text
        '    FrmBloqueo.ShowDialog()
        '    Me.Panel1.Enabled = False
        '    Me.Panel3.Enabled = False
        '    Me.Panel5.Enabled = False

        '    Me.Close()
        '    Me.Dispose()

        'End If
        CON.Close()



        checaBitacoraTecnico(gloClave, "Q")
        If clvBitacoraDescarga > 0 Then
            Me.ComboBox2.SelectedValue = clvTecnicoDescarga
            Me.ComboBox2.Enabled = False
        Else
            Me.ComboBox2.Enabled = True
        End If
    End Sub

    'Private Sub BUSCACLIENTES(ByVal OP As Integer)
    '    Try
    '        Dim CON As New SqlConnection(MiConexion)
    '        CON.Open()
    '        NUM = 0
    '        num2 = 0
    '        If IsNumeric(Me.ContratoTextBox.Text) = True Then
    '            Me.BUSCLIPORCONTRATOTableAdapter.Connection = CON
    '            Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.NewSofTvDataSet.BUSCLIPORCONTRATO, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), New System.Nullable(Of Integer)(CType(0, Integer)), GloClv_TipSer)
    '            Me.BuscaBloqueadoTableAdapter.Connection = CON
    '            Me.BuscaBloqueadoTableAdapter.Fill(Me.DataSetLidia.BuscaBloqueado, Me.ContratoTextBox.Text, NUM, num2)
    '            CREAARBOL()
    '            If num2 = 1 Then
    '                eGloContrato = Me.ContratoTextBox.Text
    '                bloq = 1
    '            End If
    '        End If
    '        CON.Close()
    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try

    'End Sub

    Private Sub BUSCACLIENTES(ByVal OP As Integer)
        Dim contrato1 As Long
        Dim CON As New SqlConnection(MiConexion)
        Dim com As SqlCommand = New SqlCommand("BUSCLIPORCONTRATO_2", CON)
        Dim tabla As DataTable = New DataTable
        Dim da As SqlDataAdapter = New SqlDataAdapter(com)
        If OP = 0 Then
            If IsNumeric(Me.ContratoTextBox.Text) = True Then
                contrato1 = Long.Parse(Me.ContratoTextBox.Text)
            Else
                MsgBox("El contrato introducido no es correcto, favor de verificarlo", MsgBoxStyle.Information)
                Return
            End If
        End If
        com.CommandType = CommandType.StoredProcedure
        com.Parameters.Add(New SqlParameter("@CONTRATO", contrato1))
        com.Parameters.Add(New SqlParameter("@NOMBRE", ""))
        com.Parameters.Add(New SqlParameter("@CALLE", ""))
        com.Parameters.Add(New SqlParameter("@NUMERO", ""))
        com.Parameters.Add(New SqlParameter("@CIUDAD", ""))
        com.Parameters.Add(New SqlParameter("@OP", OP))
        com.Parameters.Add(New SqlParameter("@Clv_TipSer", GloClv_TipSer))
        com.Parameters.Add(New SqlParameter("@Telefono", ""))
        Try
            CON.Open()
            da.Fill(tabla)
            'LLena  Controles 
            Me.NOMBRELabel1.Text = tabla.Rows(0)(1).ToString
            Me.CALLELabel1.Text = tabla.Rows(0)(2).ToString
            Me.COLONIALabel1.Text = tabla.Rows(0)(3).ToString
            Me.NUMEROLabel1.Text = tabla.Rows(0)(4).ToString
            Me.CIUDADLabel1.Text = tabla.Rows(0)(5).ToString
            Me.SOLOINTERNETCheckBox.Checked = Convert.ToBoolean(tabla.Rows(0)(6).ToString)
            Me.ESHOTELCheckBox.Checked = Convert.ToBoolean(tabla.Rows(0)(6).ToString)
            '
            'Me.BuscaBloqueadoTableAdapter.Connection = CON
            'Me.BuscaBloqueadoTableAdapter.Fill(Me.DataSetLidia.BuscaBloqueado, Me.ContratoTextBox.Text, NUM, num2)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            CON.Close()
        End Try
        TxtRefOxxo.Text = SP_REFERENCIA_OXXO(contrato1)
        CREAARBOL()
        'If num2 = 1 Then
        '    eGloContrato = Me.ContratoTextBox.Text
        '    bloq = 1
        'End If



    End Sub

    Private Sub BUSCA(ByVal CLAVE As Integer)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.CONQUEJASTableAdapter.Connection = CON
            Me.CONQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.CONQUEJAS, gloClave, GloClv_TipSer)
            CON.Close()
            CREAARBOL()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub FrmQueja_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If dameStatusOrdenQueja(gloClave, "Q") = "P" And opcion = "M" Then
            softv_BorraDescarga(gloClave, "Q")
        End If
    End Sub

    Private Sub FrmQueja_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Leave

    End Sub


    Private Sub FrmQueja_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cmbPrioridadQueja.DisplayMember = "Descripcion"
        cmbPrioridadQueja.ValueMember = "clvPrioridadQueja"
        cmbPrioridadQueja.DataSource = sofTV.BAL.PrioridadQueja.GetAll()

        Clv_TipoQuejaTextBox.Text = "A"
        'Hora_sol.CustomFormat = "dd/MM/yyy HH:mm:ss"
        Dim CON As New SqlConnection(MiConexion)
        If IdSistema = "LO" Or IdSistema = "YU" Then
            Me.Label6.Visible = False
            Me.ESHOTELCheckBox.Visible = False
        End If
        CON.Open()
        'TODO: esta línea de código carga datos en la tabla 'DataSetarnoldo.Dame_fecha_hora_serv' Puede moverla o quitarla según sea necesario.
        'Me.Dame_fecha_hora_servTableAdapter.Fill(Me.DataSetarnoldo.Dame_fecha_hora_serv)
        colorea(Me, Me.Name)
        'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerPrincipal' Puede moverla o quitarla según sea necesario.
        Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
        Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)
        'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MUESTRATIPOQUEJAS' Puede moverla o quitarla según sea necesario.
        'Me.MUESTRATIPOQUEJASTableAdapter.Connection = CON
        'Me.MUESTRATIPOQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATIPOQUEJAS)
        'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MUESTRATECNICOS' Puede moverla o quitarla según sea necesario.
        Me.Dame_fecha_hora_servTableAdapter.Connection = CON
        Me.Dame_fecha_hora_servTableAdapter.Fill(Me.DataSetarnoldo.Dame_fecha_hora_serv)
        'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MUESTRACLASIFICACIONQUEJAS' Puede moverla o quitarla según sea necesario.
        Me.MUESTRACLASIFICACIONQUEJASTableAdapter.Connection = CON
        Me.MUESTRACLASIFICACIONQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACLASIFICACIONQUEJAS)
        ' Me.MUESTRATECNICOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATECNICOS)

        Me.Muestra_Tecnicos_QuejasbuenoTableAdapter.Connection = CON
        Me.Muestra_Tecnicos_QuejasbuenoTableAdapter.Fill(Me.Procedimientosarnoldo4.Muestra_Tecnicos_Quejasbueno, gloClave)
        'Me.Muestra_Tecnicos_AlmacenTableAdapter.Connection = CON
        'Me.Muestra_Tecnicos_AlmacenTableAdapter.Fill(Me.DataSetarnoldo.Muestra_Tecnicos_Almacen, 0)

        'Me.CONDetOrdSerTableAdapter.Fill(Me.NewSofTvDataSet.CONDetOrdSer, New System.Nullable(Of Long)(CType(Me.Clv_OrdenTextBox.Text, Long)))
        Me.DameClv_Session_TecnicosTableAdapter.Connection = CON
        Me.DameClv_Session_TecnicosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Tecnicos, clv_sessionTecnico)
        Me.DameFechadelServidorHoraTableAdapter.Connection = CON
        Me.DameFechadelServidorHoraTableAdapter.Fill(Me.NewSofTvDataSet.DameFechadelServidorHora)
        Locclv_folio = 0
        LocNo_Bitacora = 0
        LocValida1 = False
        Me.Timer1.Enabled = False
        'If IdSistema = "SA" Then
        Me.Fecha_EjecucionMaskedTextBox.Mask = "00/00/0000"
        Visita1MaskedTextBox.Mask = "00/00/0000"
        Visita2MaskedTextBox.Mask = "00/00/0000"
        Visita3MaskedTextBox.Mask = "00/00/0000"
        EnProcesoMaskedTextBox.Mask = "00/00/0000"
        'Else
        '    Me.Fecha_EjecucionMaskedTextBox.Mask = "00/00/0000 00:00 a.m."
        '    Visita1MaskedTextBox.Mask = "00/00/0000 00:00 a.m."
        '    Visita2MaskedTextBox.Mask = "00/00/0000 00:00 a.m."
        '    Visita2MaskedTextBox.Mask = "00/00/0000 00:00 a.m."
        'End If

        FechaAgenda = ""
        HoraAgenda = ""
        Clv_HoraAgenda = 0
        ComentarioAgenda = ""

        'PARA LLENAR EL COMBO DE CLASIFICACIÓN DEL PROBLEMA (INICIO) --JUANJO
        llenaComboClasifProblemas()
        'PARA LLENAR EL COMBO DE CLASIFICACIÓN DEL PROBLEMA (FIN) --JUANJO
        If opcion = "N" Then

            GloControlaReloj = 0
            Me.Timer1.Enabled = False
            Me.Button3.Visible = False
            Me.Label3.Visible = False
            'Me.Panel11.Enabled = False
            Me.CONQUEJASBindingSource.AddNew()
            'FrmSelTipServicio.Show()
            GloClv_TipSer = 0
            ACTIVA(False)
            Me.StatusTextBox.Text = "P"
            Me.FechaTextBox.Text = Me.TextBox4.Text
            Me.ComboBox1.SelectedIndex = 1
            'Me.FechaTextBox.Enabled = False
            Me.ComboBox4.Text = ""
            Panel1.Enabled = True
            Me.Panel3.Enabled = True
            Me.Panel5.Enabled = True
            Me.Panel4.Enabled = True
            If IdSistema = "SA" Or IdSistema = "VA" Then
                Me.ComboBox2.Enabled = False
            End If

            'Cuadrilla-----------------------------------------------
            'MuestraRelQuejaCuadrilla(0, 0)
            '--------------------------------------------------------

        ElseIf opcion = "C" Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Queja", SqlDbType.BigInt, gloClave)
            BaseII.CreateMyParameter("@Comentario", SqlDbType.VarChar, 250, ParameterDirection.Output, "")
            Dim dico As Dictionary(Of String, Object) = BaseII.ProcedimientoOutPut("USP_ObtenerComentarioQueja")
            TextComentario.Text = dico("@Comentario").ToString()
            'Panel1.Enabled = False

            Me.Panel8.Enabled = False
            Me.Timer1.Enabled = False
            GloControlaReloj = 0
            Me.Panel3.Enabled = False
            Me.Panel5.Enabled = False
            'Me.Panel11.Enabled = False
            Me.ComboBox5.Enabled = False

            BUSCA(gloClave)

            ' CREAARBOL()
            Me.Button1.Enabled = False
            Me.Panel2.Enabled = False
            Me.Fecha_EjecucionMaskedTextBox.Enabled = False
            Me.Visita1MaskedTextBox.Enabled = False
            Me.Visita2MaskedTextBox.Enabled = False
            Me.Visita3MaskedTextBox.Enabled = False
            Me.CONQUEJASBindingNavigator.Enabled = False
            Me.cmbClasifProblemas.Enabled = False
            Me.ContratoTextBox.ReadOnly = True
            Me.FechaTextBox.ReadOnly = True
            Me.Label3.Visible = True 'Etiqueta Visible
            Me.Label3.Text = "Se generó el número de bitacora: " & Cadena 'Etiqueta Text concatenar    
            Me.Muestra_no_quejaTableAdapter.Connection = CON
            Me.Muestra_no_quejaTableAdapter.Fill(Me.DataSetarnoldo.Muestra_no_queja, CInt(gloClave))
            Muestra_usuario(gloClave)
        ElseIf opcion = "M" Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Queja", SqlDbType.BigInt, gloClave)
            BaseII.CreateMyParameter("@Comentario", SqlDbType.VarChar, 250, ParameterDirection.Output, "")
            Dim dico As Dictionary(Of String, Object) = BaseII.ProcedimientoOutPut("USP_ObtenerComentarioQueja")
            TextComentario.Text = dico("@Comentario").ToString()
            Panel1.Enabled = True
            Me.Panel3.Enabled = True
            Me.Panel5.Enabled = True
            Me.cmbClasifProblemas.Enabled = True
            'Me.Panel11.Enabled = True

            ACTIVA(True)

            BUSCA(gloClave)
            'CREAARBOL()
            'Me.MUESTRATRABAJOSQUEJASTableAdapter.Connection = CON
            'Me.MUESTRATRABAJOSQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATRABAJOSQUEJAS, New System.Nullable(Of Integer)(CType(GloClv_TipSer, Integer)))
            Muestra_usuario(gloClave)

            checaBitacoraTecnico(gloClave, "Q")
            If clvBitacoraDescarga > 0 Then
                Me.ComboBox2.SelectedValue = clvTecnicoDescarga
                Me.ComboBox2.Enabled = False
            Else
                Me.ComboBox2.Enabled = True
            End If

            If Me.StatusTextBox.Text = "E" Then
                Me.Panel8.Enabled = False
                Panel1.Enabled = False
                Me.Panel3.Enabled = False
                Me.Panel5.Enabled = False
                Me.cmbClasifProblemas.Enabled = False
                Me.Label3.Visible = True 'Etiqueta Visible
                Me.Label3.Text = "Se generó el número de bitacora: " & Cadena 'Etiqueta Text concatenar 
                Me.Muestra_no_quejaTableAdapter.Connection = CON
                Me.Muestra_no_quejaTableAdapter.Fill(Me.DataSetarnoldo.Muestra_no_queja, CInt(gloClave))
            ElseIf Me.StatusTextBox.Text = "P" Then
                ' Me.Panel4.Enabled = False
                Me.Panel8.Enabled = True
                Me.ComboBox4.Text = ""
                Me.StatusTextBox.Text = "E"
                GloControlaReloj = 1

                Me.Timer1.Enabled = True

                'Me.Label2.Visible = True
                'Me.Label2.Text = "Ejecución : "
                ''If IsDate(Me.Fecha_SoliciutudMaskedTextBox.Text) = True Then
                ''''Me.Fecha_EjecucionMaskedTextBox.MinDate = Me.Fecha_SoliciutudMaskedTextBox.Text
                ''''Me.Fecha_EjecucionMaskedTextBox.MaxDate = FechaDateTimePicker.Value
                ''End If
            ElseIf Me.StatusTextBox.Text = "V" Then
                Me.Fecha_EjecucionMaskedTextBox.Enabled = False
                Me.Hora_sol.Enabled = False
                consultaVisitasQuejas(gloClave)
                If IsDate(Me.Visita1MaskedTextBox.Text) = True Then
                    Me.Visita1MaskedTextBox.Enabled = False
                    Me.dtpHoraVisita1.Enabled = False
                End If
                If IsDate(Me.Visita2MaskedTextBox.Text) = True Then
                    Me.Visita2MaskedTextBox.Enabled = False
                    Me.dtpHoraVisita2.Enabled = False
                End If
                If IsDate(Me.Visita3MaskedTextBox.Text) = True Then
                    Me.Visita3MaskedTextBox.Enabled = False
                    Me.dtpHoraVisita3.Enabled = False
                End If
            ElseIf Me.StatusTextBox.Text = "S" Then 'ESTO LO PUSO JUANJO PARA CONSULTAR LA FECHA Y HORA DE LA QUEJA EN CASO DE QUE TENGA EL NUEVO STATUS(INICIO)
                Me.Fecha_EjecucionMaskedTextBox.Enabled = False
                Me.Hora_sol.Enabled = False
                Me.Visita1MaskedTextBox.Enabled = False
                Me.dtpHoraVisita1.Enabled = False
                Me.Visita2MaskedTextBox.Enabled = False
                Me.dtpHoraVisita2.Enabled = False
                Me.Visita3MaskedTextBox.Enabled = False
                Me.dtpHoraVisita3.Enabled = False

                spConsultaEnProcesoQuejas(gloClave)
                If IsDate(Me.EnProcesoMaskedTextBox.Text) = True Then
                    Me.EnProcesoMaskedTextBox.Enabled = False
                    Me.dtpHoraEnProceso.Enabled = False
                End If
            End If
        End If
        'Me.CONDetOrdSerTableAdapter.Fill(Me.NewSofTvDataSet.CONDetOrdSer, New System.Nullable(Of Long)(CType(Me.Clv_OrdenTextBox.Text, Long)))
        CON.Close()
        If opcion = "C" Or opcion = "M" Then
            Dim cone As New SqlClient.SqlConnection(MiConexion)
            cone.Open()
            NUM = 0
            num2 = 0
            Me.BuscaBloqueadoTableAdapter.Connection = cone
            Me.BuscaBloqueadoTableAdapter.Fill(Me.DataSetLidia.BuscaBloqueado, Me.ContratoTextBox.Text, NUM, num2)
            cone.Close()
            If num2 = 1 Then
                eGloContrato = Contrato
                bloq = 1
            End If
            ChecaRelQuejaUsuario(gloClave)

            'OBTIENE LA CLAVE DE LA CLASIFICACIÓN DEL PROBLEMA (INICIO) --JUANJO
            Me.cmbClasifProblemas.SelectedValue = Consulta(gloClave)
            Me.cmbClasifProblemas.Enabled = False
            'OBTIENE LA CLAVE DE LA CLASIFICACIÓN DEL PROBLEMA (FIN) --JUANJO

            'Cuadrilla-----------------------------------------------
            'MuestraRelQuejaCuadrilla(gloClave, 0)
            '--------------------------------------------------------

            'SE AÑADE EL COMENTARIO DE LA AGENDA EN LAS OBSEVACIONES (INICIO) --JUANJO
            Dim prmComentarioAgenda As String
            prmComentarioAgenda = uspConsultaComentarioAgenda(gloClave)
            If prmComentarioAgenda.Length > 0 Then
                Me.lblObsAgenda.Visible = True
                Me.lblObsAgendaMostrar.Visible = True
                lblObsAgendaMostrar.Text = prmComentarioAgenda
            Else
                Me.lblObsAgenda.Visible = False
                Me.lblObsAgendaMostrar.Visible = False
            End If
            'SE AÑADE EL COMENTARIO DE LA AGENDA EN LAS OBSEVACIONES (FIN) --JUANJO

        End If

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_queja", SqlDbType.BigInt, gloClave)
        BaseII.CreateMyParameter("@clvPrioridadQueja", ParameterDirection.Output, SqlDbType.Int)
        Dim lp As Dictionary(Of String, Object) = BaseII.ProcedimientoOutPut("uspGetPrioridadQueja")
        cmbPrioridadQueja.SelectedValue = CInt(lp("@clvPrioridadQueja").ToString())

        Me.BindingNavigatorDeleteItem.Visible = False
    End Sub
    Private Sub Muestra_usuario(ByVal clv_queja As Long)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Consulta_RelQuejaUsuarioTableAdapter.Connection = CON
        Me.Consulta_RelQuejaUsuarioTableAdapter.Fill(Me.DataSetEric2.Consulta_RelQuejaUsuario, clv_queja)
        Me.Label4.Visible = True
        Me.GeneroLabel1.Visible = True
        If Me.StatusTextBox.Text = "E" Then
            Me.Label5.Visible = True
            Me.EjecutoLabel1.Visible = True
        End If
        CON.Close()
    End Sub
    Private Sub bloqueado(ByRef contrato As Integer)

    End Sub
    Private Sub ComboBox4_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox4.LostFocus
        If Me.ComboBox4.SelectedIndex = -1 Then
            Me.Clv_TrabajoTextBox.Text = ""
        End If
    End Sub

    Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.SelectedIndexChanged
        Clv_TrabajoTextBox.Text = Me.ComboBox4.SelectedValue
        If IsNumeric(Me.Clv_TrabajoTextBox.Text) = False Then
            LocTipQue = False
        Else
            LocTipQue = True
        End If
    End Sub


    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        ClasificacionTextBox.Text = Me.ComboBox1.SelectedValue
    End Sub

    Private Sub ComboBox2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox2.LostFocus
        If Me.ComboBox2.SelectedIndex = -1 Then
            Me.Clave_TecnicoTextBox.Text = ""
        End If
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        Clave_TecnicoTextBox.Text = Me.ComboBox2.SelectedValue
        If IsNumeric(Me.ComboBox2.SelectedValue) = False Then
            LocTec = False
        Else
            MuestraRelQuejaCuadrilla(gloClave, Me.ComboBox2.SelectedValue)
            LocTec = True
        End If

        If IsNumeric(Me.ContratoTextBox.Text) = True And IsNumeric(Me.ComboBox2.SelectedValue) = True And Me.StatusTextBox.Text = "P" Then
            If Me.ContratoTextBox.Text > 0 And Me.ComboBox2.SelectedValue > 0 Then

                GLOCONTRATOSEL_agenda = Me.ContratoTextBox.Text
                GloClv_tecnico = Me.ComboBox2.SelectedValue
                GLONOM_TECNICO = Me.ComboBox2.Text

                'FrmAgendaRapida.Show()

            End If
        End If
    End Sub

    Private Sub ComboBox3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Clv_TipoQuejaTextBox.Text = "A"
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        GloClv_TipSer = Me.ComboBox5.SelectedValue
        If GloClv_TipSer = Nothing Or GloClv_TipSer = 0 Then
            MsgBox(" Por Favor Seleccione El Tipo de Servicio de la Queja", MsgBoxStyle.Information)
            Exit Sub
        End If
        GLOCONTRATOSEL = 0
        FrmSelCliente.Show()
    End Sub

    Private Sub ContratoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContratoTextBox.TextChanged
        Me.BUSCACLIENTES(0)
        If IsNumeric(Me.ContratoTextBox.Text) Then
            If ContratoTextBox.Text > 0 Then
                'Verificamos si es que el Cliente ha sido Bloqueado
                If GloClienteBloqueado = False Then
                    If (DABloqueoDeClientes.uspConsultaClientes_Bloqueados(ContratoTextBox.Text)) Then
                        GloClienteBloqueado = True
                        FrmBloqueo.ShowDialog()
                        Me.Close()
                        Me.Dispose()
                    End If
                End If
            End If
        End If

    End Sub


    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        If Me.StatusTextBox.Text <> "P" And Me.RadioButton1.Checked = True Then
            Me.StatusTextBox.Text = "P"
            'Me.Fecha_EjecucionMaskedTextBox.Enabled = False
            'Me.Label2.Visible = False
        End If

    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        If Me.StatusTextBox.Text <> "E" And Me.RadioButton2.Checked = True Then
            Me.StatusTextBox.Text = "E"
            Me.Fecha_EjecucionMaskedTextBox.Enabled = True
            Me.Hora_sol.Enabled = True
            Me.Visita1MaskedTextBox.Enabled = False
            Me.Visita2MaskedTextBox.Enabled = False
            Me.Visita3MaskedTextBox.Enabled = False
            Me.dtpHoraVisita1.Enabled = False
            Me.dtpHoraVisita2.Enabled = False
            Me.dtpHoraVisita3.Enabled = False
        ElseIf Me.StatusTextBox.Text = "E" Then
            Me.Fecha_EjecucionMaskedTextBox.Enabled = True
            Me.Hora_sol.Enabled = True
            Me.Visita1MaskedTextBox.Enabled = False
            Me.Visita2MaskedTextBox.Enabled = False
            Me.Visita3MaskedTextBox.Enabled = False
            Me.dtpHoraVisita1.Enabled = False
            Me.dtpHoraVisita2.Enabled = False
            Me.dtpHoraVisita3.Enabled = False
        End If
    End Sub

    Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton3.CheckedChanged
        If Me.StatusTextBox.Text <> "V" And Me.RadioButton3.Checked = True Then
            GloControlaReloj = 0
            Me.Visita1MaskedTextBox.Enabled = True
            Me.Visita2MaskedTextBox.Enabled = True
            Me.Visita3MaskedTextBox.Enabled = True
            Me.dtpHoraVisita1.Enabled = True
            Me.dtpHoraVisita2.Enabled = True
            Me.dtpHoraVisita3.Enabled = True
            Me.Fecha_EjecucionMaskedTextBox.Enabled = False
            Me.Hora_sol.Enabled = False
            Me.EnProcesoMaskedTextBox.Enabled = False
            Me.StatusTextBox.Text = "V"

        ElseIf Me.StatusTextBox.Text <> "V" Then
            GloControlaReloj = 0
            Me.Visita1MaskedTextBox.Enabled = True
            Me.Visita2MaskedTextBox.Enabled = True
            Me.Visita3MaskedTextBox.Enabled = True
            Me.dtpHoraVisita1.Enabled = False
            Me.dtpHoraVisita2.Enabled = False
            Me.dtpHoraVisita3.Enabled = False
            Me.Fecha_EjecucionMaskedTextBox.Enabled = False
            Me.Hora_sol.Enabled = False
            Me.StatusTextBox.Text = "V"
        End If

        If Me.StatusTextBox.Text = "V" Then
            If IsDate(Me.Visita1MaskedTextBox.Text) = True Then
                Me.Visita1MaskedTextBox.Enabled = False
                Me.dtpHoraVisita1.Enabled = False
            End If
            If IsDate(Me.Visita2MaskedTextBox.Text) = True Then
                Me.Visita2MaskedTextBox.Enabled = False
                Me.dtpHoraVisita2.Enabled = False
            End If
            If IsDate(Me.Visita3MaskedTextBox.Text) = True Then
                Me.Visita3MaskedTextBox.Enabled = False
                Me.dtpHoraVisita3.Enabled = False
            End If
        End If
    End Sub


    Private Sub Fecha_SoliciutudMaskedTextBox_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub



    Private Sub ComboBox5_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox5.SelectedIndexChanged
        Dim con As New SqlClient.SqlConnection(MiConexion)
        If Me.ComboBox5.SelectedValue <> Nothing Then
            GloClv_TipSer = Me.ComboBox5.SelectedValue
            Me.TextBox2.Text = Me.ComboBox5.Text
            GloNom_TipSer = Me.ComboBox5.Text
            con.Open()
            Me.MUESTRATRABAJOSQUEJASTableAdapter.Connection = con
            Me.MUESTRATRABAJOSQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATRABAJOSQUEJAS, New System.Nullable(Of Integer)(CType(GloClv_TipSer, Integer)))
            con.Close()
            Me.ComboBox4.Text = ""
        End If
    End Sub



    Private Sub Visita1MaskedTextBox_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim pasa As Integer = 0
        'If IsDate(DateValue(Me.Visita1MaskedTextBox.Text)) = True Then
        ' If DateValue(Me.Visita1MaskedTextBox.Text) >= DateValue(Me.Fecha_SoliciutudMaskedTextBox.Value) And DateValue(Me.Visita1MaskedTextBox.Text) <= DateValue(Me.FechaDateTimePicker.Value) Then
        ' pasa = 1
        ' Else
        ' MsgBox("La Fecha de la Visita no puede ser menor a la fecha de solicitud ni mayo a la fecha actual ", MsgBoxStyle.Information)
        ' Me.Visita1MaskedTextBox.Clear()
        ' End If
        ' End If
    End Sub

    Private Sub Fecha_EjecucionMaskedTextBox_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Fecha_EjecucionMaskedTextBox.GotFocus
        Me.Fecha_EjecucionMaskedTextBox.SelectionStart = 0
        Me.Fecha_EjecucionMaskedTextBox.SelectionLength = Len(Me.Fecha_EjecucionMaskedTextBox.Text)
    End Sub


    Private Sub Fecha_EjecucionMaskedTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Fecha_EjecucionMaskedTextBox.TextChanged
        If IsDate(Mid(Me.Fecha_EjecucionMaskedTextBox.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.Fecha_EjecucionMaskedTextBox.Text, 1, 10)
            If DateValue(Fecha) = DateValue("01/01/1900") Then
                Me.Fecha_EjecucionMaskedTextBox.Clear()
                LocFecEje = False
            End If
        End If
        If IsDate(Me.Fecha_EjecucionMaskedTextBox.Text) = True Then
            Me.Fecha_EjecucionMaskedTextBox1.Text = Me.Fecha_EjecucionMaskedTextBox.Text
        Else
            LocFecEje = False
        End If
    End Sub



    Private Sub Fecha_EjecucionMaskedTextBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.Fecha_EjecucionMaskedTextBox.Text = Me.Fecha_EjecucionMaskedTextBox1.Text
    End Sub



    Private Sub Visita1MaskedTextBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'Me.Fecha_EjecucionMaskedTextBox.Text = Me.Fecha_EjecucionMaskedTextBox1.Text
        Me.Visita1MaskedTextBox.Text = Me.Visita1MaskedTextBox1.Text
    End Sub



    Private Sub Visita2MaskedTextBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.Visita2MaskedTextBox.Text = Me.Visita2MaskedTextBox1.Text
    End Sub




    Private Sub Visita1MaskedTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Visita1MaskedTextBox.TextChanged

        If IsDate(Mid(Me.Visita1MaskedTextBox.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.Visita1MaskedTextBox.Text, 1, 10)
            If DateValue(Fecha) = DateValue("01/01/1900") Then
                Me.Visita1MaskedTextBox.Clear()
            End If
        End If
        If IsDate(Me.Visita1MaskedTextBox.Text) = True Then
            Me.Visita1MaskedTextBox1.Text = Me.Visita1MaskedTextBox.Text
        End If
    End Sub

    Private Sub Visita2MaskedTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Visita2MaskedTextBox.TextChanged
        If IsDate(Mid(Me.Visita2MaskedTextBox.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.Visita2MaskedTextBox.Text, 1, 10)
            If DateValue(Fecha) = DateValue("01/01/1900") Then
                Me.Visita2MaskedTextBox.Clear()
            End If
        End If
        If IsDate(Me.Visita2MaskedTextBox.Text) = True Then
            Me.Visita2MaskedTextBox1.Text = Me.Visita2MaskedTextBox.Text
        End If
    End Sub

    Private Sub Visita3MaskedTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Visita3MaskedTextBox.TextChanged
        If IsDate(Mid(Me.Visita3MaskedTextBox.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.Visita3MaskedTextBox.Text, 1, 10)
            If DateValue(Fecha) = DateValue("01/01/1900") Then
                Me.Visita3MaskedTextBox.Clear()
            End If
        End If
        If IsDate(Me.Visita3MaskedTextBox.Text) = True Then
            Me.Visita3MaskedTextBox1.Text = Me.Visita3MaskedTextBox.Text
        End If
    End Sub

    Private Sub StatusTextBox_TextChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StatusTextBox.TextChanged
        If Me.StatusTextBox.Text = "P" Then
            If Me.RadioButton1.Checked = False Then Me.RadioButton1.Checked = True
        ElseIf Me.StatusTextBox.Text = "E" Then
            If Me.RadioButton2.Checked = False Then Me.RadioButton2.Checked = True
        ElseIf Me.StatusTextBox.Text = "V" Then
            If Me.RadioButton3.Checked = False Then
                Me.RadioButton3.Checked = True
                Me.Visita1MaskedTextBox.Enabled = True
                Me.Visita2MaskedTextBox.Enabled = True
                Me.Visita3MaskedTextBox.Enabled = True
            End If
        ElseIf Me.StatusTextBox.Text = "S" Then
            If Me.rbEnProceso.Checked = False Then Me.rbEnProceso.Checked = True
            Me.EnProcesoMaskedTextBox.Enabled = True
            Me.dtpHoraEnProceso.Enabled = True
        End If
    End Sub

    Private Sub Visita1MaskedTextBox1_TextChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Visita1MaskedTextBox1.TextChanged
        Me.Visita1MaskedTextBox.Text = Me.Visita1MaskedTextBox1.Text
    End Sub

    Private Sub Fecha_EjecucionMaskedTextBox1_TextChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fecha_EjecucionMaskedTextBox1.TextChanged
        Me.Fecha_EjecucionMaskedTextBox.Text = Me.Fecha_EjecucionMaskedTextBox1.Text
        If IsDate(Me.Fecha_EjecucionMaskedTextBox.Text) = False Then
            LocFecEje = False
        Else
            If Len(Me.Fecha_EjecucionMaskedTextBox.Text) < 10 Then
                LocFecEje = False
            Else
                LocFecEje = True
            End If
        End If
    End Sub

    Private Sub Visita2MaskedTextBox1_TextChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Visita2MaskedTextBox1.TextChanged
        Me.Visita2MaskedTextBox.Text = Me.Visita2MaskedTextBox1.Text
    End Sub
    Private Sub Visita3MaskedTextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Visita3MaskedTextBox1.TextChanged
        Me.Visita3MaskedTextBox.Text = Me.Visita3MaskedTextBox1.Text
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Locclv_tec = Me.ComboBox2.SelectedValue
        gLOVERgUARDA = 0
        If Me.CONQUEJASBindingNavigator.Enabled = False Then
            gLOVERgUARDA = 1
        End If

        'My.Forms.FrmDescargaMateTecQuejas.Show()
        Dim frm As New SoftvNew.FrmDescargaMaterialTec()
        frm.Clv_Orden = gloClave
        frm.IdTecnico = Locclv_tec
        frm.tipoDescarga = "Q"
        frm.ShowDialog()
    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub
    Private Sub ConfigureCrystalReports_NewXml(ByVal op As Integer, ByVal Titulo As String)
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0", Op6 As String = "0"
            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0", StatusProc As String = "0"
            Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"
            Dim a As Integer = 0

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword
            Dim Impresora_Ordenes As String = Nothing
            Dim mySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"


            Dim reportPath As String = Nothing

            If IdSistema = "TO" Then
                reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoCabStar.rpt"
            ElseIf IdSistema = "AG" Then
                reportPath = RutaReportes + "\ReporteFormatoQuejasBueno.rpt"
            ElseIf IdSistema = "SA" Then
                reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoTvRey.rpt"
            ElseIf IdSistema = "VA" Then
                reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoCosmo.rpt"
            ElseIf IdSistema = "LO" Or IdSistema = "YU" Then
                reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoLogitel.rpt"
            End If

            Dim cnn As New SqlConnection(MiConexion)
            Dim cmd As New SqlCommand("ReporteAreaTecnicaQuejas1", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0

            Dim parametro1 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
            parametro1.Direction = ParameterDirection.Input
            parametro1.Value = CStr(GloClv_TipSer)
            cmd.Parameters.Add(parametro1)

            Dim parametro2 As New SqlParameter("@op1", SqlDbType.SmallInt)
            parametro2.Direction = ParameterDirection.Input
            parametro2.Value = 1
            cmd.Parameters.Add(parametro2)

            Dim parametro3 As New SqlParameter("@op2", SqlDbType.SmallInt)
            parametro3.Direction = ParameterDirection.Input
            parametro3.Value = Op2
            cmd.Parameters.Add(parametro3)

            Dim parametro4 As New SqlParameter("@op3", SqlDbType.SmallInt)
            parametro4.Direction = ParameterDirection.Input
            parametro4.Value = Op3
            cmd.Parameters.Add(parametro4)

            Dim parametro5 As New SqlParameter("@op4", SqlDbType.SmallInt)
            parametro5.Direction = ParameterDirection.Input
            parametro5.Value = Op4
            cmd.Parameters.Add(parametro5)

            Dim parametro6 As New SqlParameter("@op5", SqlDbType.SmallInt)
            parametro6.Direction = ParameterDirection.Input
            parametro6.Value = Op5
            cmd.Parameters.Add(parametro6)

            Dim parametro22 As New SqlParameter("@op6", SqlDbType.SmallInt)
            parametro22.Direction = ParameterDirection.Input
            parametro22.Value = Op6
            cmd.Parameters.Add(parametro22)

            Dim parametro7 As New SqlParameter("@StatusPen", SqlDbType.Bit)
            parametro7.Direction = ParameterDirection.Input
            If StatusPen = "1" Then
                parametro7.Value = True
            Else
                parametro7.Value = False
            End If
            cmd.Parameters.Add(parametro7)

            Dim parametro8 As New SqlParameter("@StatusEje", SqlDbType.Bit)
            parametro8.Direction = ParameterDirection.Input
            If StatusEje = "1" Then
                parametro8.Value = True
            Else
                parametro8.Value = False
            End If
            cmd.Parameters.Add(parametro8)

            Dim parametro9 As New SqlParameter("@StatusVis", SqlDbType.Bit)
            parametro9.Direction = ParameterDirection.Input
            If StatusVis = "1" Then
                parametro9.Value = True
            Else
                parametro9.Value = False
            End If
            cmd.Parameters.Add(parametro9)

            Dim parametro24 As New SqlParameter("@StatusProc", SqlDbType.Bit)
            parametro24.Direction = ParameterDirection.Input
            If StatusProc = "1" Then
                parametro24.Value = True
            Else
                parametro24.Value = False
            End If
            cmd.Parameters.Add(parametro24)

            Dim parametro10 As New SqlParameter("@Clv_OrdenIni", SqlDbType.BigInt)
            parametro10.Direction = ParameterDirection.Input
            parametro10.Value = CInt(Me.Clv_QuejaTextBox.Text)
            cmd.Parameters.Add(parametro10)

            Dim parametro11 As New SqlParameter("@Clv_OrdenFin", SqlDbType.BigInt)
            parametro11.Direction = ParameterDirection.Input
            parametro11.Value = CInt(Me.Clv_QuejaTextBox.Text)
            cmd.Parameters.Add(parametro11)

            Dim parametro12 As New SqlParameter("@Fec1Ini", SqlDbType.DateTime)
            parametro12.Direction = ParameterDirection.Input
            parametro12.Value = Fec1Ini
            cmd.Parameters.Add(parametro12)

            Dim parametro13 As New SqlParameter("@Fec1Fin", SqlDbType.DateTime)
            parametro13.Direction = ParameterDirection.Input
            parametro13.Value = Fec1Fin
            cmd.Parameters.Add(parametro13)

            Dim parametro14 As New SqlParameter("@Fec2Ini", SqlDbType.DateTime)
            parametro14.Direction = ParameterDirection.Input
            parametro14.Value = Fec2Ini
            cmd.Parameters.Add(parametro14)

            Dim parametro15 As New SqlParameter("@Fec2Fin", SqlDbType.DateTime)
            parametro15.Direction = ParameterDirection.Input
            parametro15.Value = Fec2Fin
            cmd.Parameters.Add(parametro15)

            Dim parametro16 As New SqlParameter("@Clv_Trabajo", SqlDbType.Int)
            parametro16.Direction = ParameterDirection.Input
            parametro16.Value = nclv_trabajo
            cmd.Parameters.Add(parametro16)

            Dim parametro17 As New SqlParameter("@Clv_Colonia", SqlDbType.Int)
            parametro17.Direction = ParameterDirection.Input
            parametro17.Value = nClv_colonia
            cmd.Parameters.Add(parametro17)

            Dim parametro18 As New SqlParameter("@OpOrden", SqlDbType.Int)
            parametro18.Direction = ParameterDirection.Input
            parametro18.Value = OpOrdenar
            cmd.Parameters.Add(parametro18)


            Dim parametro19 As New SqlParameter("@clv_Depto", SqlDbType.Char)
            parametro19.Direction = ParameterDirection.Input
            parametro19.Value = "0"
            cmd.Parameters.Add(parametro19)

            Dim parametro20 As New SqlParameter("@Op7", SqlDbType.SmallInt)
            parametro20.Direction = ParameterDirection.Input
            parametro20.Value = 0
            cmd.Parameters.Add(parametro20)


            Dim parametro21 As New SqlParameter("@Contrato", SqlDbType.BigInt)
            parametro21.Direction = ParameterDirection.Input
            parametro21.Value = 0
            cmd.Parameters.Add(parametro21)

            Dim parametro23 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
            parametro23.Direction = ParameterDirection.Input
            parametro23.Value = 0
            cmd.Parameters.Add(parametro23)


            Dim da As New SqlDataAdapter(cmd)

            Dim ds As New DataSet()


            da.Fill(ds)

            ds.Tables(0).TableName = "ReporteAreaTecnicaQuejas1"
            ds.Tables(1).TableName = "DameDatosGenerales_2"

            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(ds)

            If a = 1 Then
                MsgBox("No se ha asignado una Impresora de Quejas")
                'Exit Sub
            Else
                customersByCityReport.PrintOptions.PrinterName = Impresora_Ordenes
                customersByCityReport.PrintToPrinter(1, True, 1, 1)
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ConfigureCrystalReports(ByVal op As Integer, ByVal Titulo As String)
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0", Op6 As String = "0"
            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
            Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"
            Dim a As Integer = 0



            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword
            Dim Impresora_Ordenes As String = Nothing
            Dim mySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"


            Dim reportPath As String = Nothing

            If IdSistema = "TO" Then
                reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoCabStar.rpt"
            ElseIf IdSistema = "AG" Then
                reportPath = RutaReportes + "\ReporteFormatoQuejasBueno.rpt"
            ElseIf IdSistema = "SA" Then
                reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoTvRey.rpt"
            ElseIf IdSistema = "VA" Then
                reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoCosmo.rpt"
            ElseIf IdSistema = "LO" Or IdSistema = "YU" Then
                reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoLogitel.rpt"
            End If


            'MsgBox(reportPath)
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Clv_TipSer int
            customersByCityReport.SetParameterValue(0, CStr(GloClv_TipSer))
            ',@op1 smallint
            customersByCityReport.SetParameterValue(1, 1)
            ',@op2 smallint
            customersByCityReport.SetParameterValue(2, 0)
            ',@op3 smallint
            customersByCityReport.SetParameterValue(3, 0)
            ',@op4 smallint,
            customersByCityReport.SetParameterValue(4, 0)
            '@op5 smallint
            customersByCityReport.SetParameterValue(5, 0)
            '@op6 smallint
            customersByCityReport.SetParameterValue(6, 0)
            ',@StatusPen bit
            customersByCityReport.SetParameterValue(7, 0)
            ',@StatusEje bit
            customersByCityReport.SetParameterValue(8, 0)
            ',@StatusVis bit,
            customersByCityReport.SetParameterValue(9, 0)
            '@Clv_OrdenIni bigint
            customersByCityReport.SetParameterValue(10, CInt(Me.Clv_QuejaTextBox.Text))
            ',@Clv_OrdenFin bigint
            customersByCityReport.SetParameterValue(11, CInt(Me.Clv_QuejaTextBox.Text))
            ',@Fec1Ini Datetime
            customersByCityReport.SetParameterValue(12, "01/01/1900")
            ',@Fec1Fin Datetime,
            customersByCityReport.SetParameterValue(13, "01/01/1900")
            '@Fec2Ini Datetime
            customersByCityReport.SetParameterValue(14, "01/01/1900")
            ',@Fec2Fin Datetime
            customersByCityReport.SetParameterValue(15, "01/01/1900")
            ',@Clv_Trabajo int
            customersByCityReport.SetParameterValue(16, 0)
            ',@Clv_Colonia int
            customersByCityReport.SetParameterValue(17, 0)
            ',@OpOrden int
            customersByCityReport.SetParameterValue(18, OpOrdenar)
            '@Clv_Departamento
            customersByCityReport.SetParameterValue(19, 0)
            '@Op7
            customersByCityReport.SetParameterValue(20, 0)
            '@Contrato
            customersByCityReport.SetParameterValue(21, 0)

            'Titulos de Reporte
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            mySelectFormula = "Quejas " & Me.TextBox2.Text
            customersByCityReport.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"
            Me.Dame_Impresora_OrdenesTableAdapter.Connection = CON
            Me.Dame_Impresora_OrdenesTableAdapter.Fill(Me.DataSetarnoldo.Dame_Impresora_Ordenes, Impresora_Ordenes, a)
            If a = 1 Then
                MsgBox("No se ha asignado una Impresora de Quejas")
                'Exit Sub
            Else
                customersByCityReport.PrintOptions.PrinterName = Impresora_Ordenes
                customersByCityReport.PrintToPrinter(1, True, 1, 1)
            End If

            'CrystalReportViewer1.ReportSource = customersByCityReport

            CON.Close()
            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub Clv_QuejaTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_QuejaTextBox.TextChanged
        If IsNumeric(Me.Clv_QuejaTextBox.Text) = True Then
            gloClave = CLng(Me.Clv_QuejaTextBox.Text)
        End If
    End Sub


    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If GloControlaReloj = 1 Then
            If LocFecEje = False Then
                If Me.Panel6.BackColor = Color.WhiteSmoke Then
                    Me.Panel6.BackColor = Color.Gold
                    If Fecha_EjecucionMaskedTextBox.Focused = False Then
                        Fecha_EjecucionMaskedTextBox.Focus()
                    End If
                Else
                    Me.Panel6.BackColor = Color.WhiteSmoke
                End If
            ElseIf LocTec = False Then
                If Me.Panel10.BackColor = Color.WhiteSmoke Then
                    Me.Panel10.BackColor = Color.Gold
                    If Me.ComboBox2.Focused = False Then
                        Me.ComboBox2.Focus()
                    End If
                Else
                    Me.Panel10.BackColor = Color.WhiteSmoke
                End If
                'ElseIf LocTipQue1 = False Then
                '    If Me.Panel11.BackColor = Color.WhiteSmoke Then
                '        Me.Panel11.BackColor = Color.Gold
                '        If Me.ComboBox5.Focused = False Then
                '            Me.ComboBox5.Focus()
                '        End If
                '    Else
                '        Me.Panel11.BackColor = Color.WhiteSmoke
                'End If
            ElseIf LocTipQue = False Then
                If Me.Panel7.BackColor = Color.WhiteSmoke Then
                    Me.Panel7.BackColor = Color.Gold
                    If Me.ComboBox4.Focused = False Then
                        Me.ComboBox4.Focus()
                    End If
                Else
                    Me.Panel7.BackColor = Color.WhiteSmoke
                End If
            ElseIf LocSolucion = False Then
                If Me.Panel9.BackColor = Color.WhiteSmoke Then
                    Me.Panel9.BackColor = Color.Gold
                    If SolucionTextBox.Focused = False Then
                        SolucionTextBox.Focus()
                    End If
                Else
                    Me.Panel9.BackColor = Color.WhiteSmoke
                End If
            Else
                Me.Panel6.BackColor = Color.WhiteSmoke
                Me.Panel8.BackColor = Color.WhiteSmoke
                Me.Panel7.BackColor = Color.WhiteSmoke
                Me.Panel9.BackColor = Color.WhiteSmoke
                'Me.Panel11.BackColor = Color.WhiteSmoke
            End If
        End If
    End Sub


    Private Sub SolucionTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SolucionTextBox.TextChanged
        If Len(Trim(SolucionTextBox.Text)) = 0 Then
            LocSolucion = False
        Else
            LocSolucion = True
        End If
    End Sub



    Private Sub Panel8_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel8.Paint

    End Sub

    Private Sub ChecaRelQuejaUsuario(ByVal Clv_Queja As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ChecaRelQuejaUsuario", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Queja", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Queja
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            conexion.Dispose()
        Catch ex As Exception
            conexion.Close()
            conexion.Dispose()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub MuestraRelQuejaCuadrilla(ByVal Clv_Orden As Long, ByVal Clv_Tecnico As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraRelQuejaCuadrilla " + Clv_Orden.ToString() + ", " + Clv_Tecnico.ToString())


        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            cbCuadrilla.DataSource = bindingSource
        Catch ex As Exception
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub NueRelQuejaCuadrilla(ByVal Clv_Queja As Long, ByVal Clv_Tecnico As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueRelQuejaCuadrilla", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Clv_Queja", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = Clv_Queja
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Clv_Tecnico", SqlDbType.Int)
        par2.Direction = ParameterDirection.Input
        par2.Value = Clv_Tecnico
        comando.Parameters.Add(par2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub consultaVisitasQuejas(ByVal prmClvQueja As Long)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("consultaVisitasQuejas", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@ClvQueja", prmClvQueja)

        Dim READER As SqlDataReader

        Try
            CON.Open()
            READER = CMD.ExecuteReader()
            While READER.Read
                quejaGloFechaVisita1 = READER(0).ToString
                quejaGloFechaVisita2 = READER(1).ToString
                quejaGloFechaVisita3 = READER(2).ToString
                quejaGloHoraVisita1 = READER(0).ToString & " " & READER(3).ToString
                quejaGloHoraVisita2 = READER(1).ToString & " " & READER(4).ToString
                quejaGloHoraVisita3 = READER(2).ToString & " " & READER(5).ToString
                Me.Visita1MaskedTextBox.Text = quejaGloFechaVisita1
                Me.Visita2MaskedTextBox.Text = quejaGloFechaVisita2
                Me.Visita3MaskedTextBox.Text = quejaGloFechaVisita3
                If quejaGloFechaVisita1 <> "" Then
                    Me.dtpHoraVisita1.Value = quejaGloHoraVisita1
                End If
                If quejaGloFechaVisita2 <> "" Then
                    Me.dtpHoraVisita2.Value = quejaGloHoraVisita2
                End If
                If quejaGloFechaVisita3 <> "" Then
                    Me.dtpHoraVisita3.Value = quejaGloHoraVisita3
                End If
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub insertaVisitasQuejas(ByVal prmClvQueja As Long, ByVal prmFechaVisita1 As Date, ByVal prmFechaVisita2 As Date, ByVal prmFechaVisita3 As Date, _
                                      ByVal prmHoraVisita1 As String, ByVal prmHoraVisita2 As String, ByVal prmHoraVisita3 As String)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("insertaVisitasQuejas", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@clvQueja", prmClvQueja)
        CMD.Parameters.AddWithValue("@fechaVisita1", prmFechaVisita1)
        CMD.Parameters.AddWithValue("@fechaVisita2", prmFechaVisita2)
        CMD.Parameters.AddWithValue("@fechaVisita3", prmFechaVisita3)
        CMD.Parameters.AddWithValue("@horaVisita1", prmHoraVisita1)
        CMD.Parameters.AddWithValue("@horaVisita2", prmHoraVisita2)
        CMD.Parameters.AddWithValue("@horaVisita3", prmHoraVisita3)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub
    Private Sub checaBitacoraTecnico(ByVal prmClvQueja As Integer, ByVal prmTipoDescarga As String)
        Dim CON As New SqlConnection(Globals.DataAccess.GlobalConectionString)
        Dim CMD As New SqlCommand("checaBitacoraTecnico", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@clvOrden", prmClvQueja)
        CMD.Parameters.AddWithValue("@tipoDescarga", prmTipoDescarga)

        Dim READER As SqlDataReader

        Try
            CON.Open()
            READER = CMD.ExecuteReader()
            If READER.HasRows Then
                While (READER.Read)
                    clvTecnicoDescarga = READER(0).ToString
                    clvBitacoraDescarga = READER(1).ToString
                End While
            Else
                clvTecnicoDescarga = 0
                clvBitacoraDescarga = 0
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub softv_RecalculaCostosMaterial(ByVal prmClvQueja As Long)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("softv_RecalculaCostosMaterial", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@clvQueja", prmClvQueja)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub rbEnProceso_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbEnProceso.CheckedChanged
        If Me.StatusTextBox.Text <> "S" And Me.rbEnProceso.Checked = True Then
            GloControlaReloj = 0
            Me.Visita1MaskedTextBox.Enabled = False
            Me.Visita2MaskedTextBox.Enabled = False
            Me.Visita3MaskedTextBox.Enabled = False
            Me.dtpHoraVisita1.Enabled = False
            Me.dtpHoraVisita2.Enabled = False
            Me.dtpHoraVisita3.Enabled = False
            Me.Fecha_EjecucionMaskedTextBox.Enabled = False
            Me.Hora_sol.Enabled = False
            Me.EnProcesoMaskedTextBox.Enabled = True
            Me.StatusTextBox.Text = "S"

        ElseIf Me.StatusTextBox.Text <> "S" Then
            GloControlaReloj = 0
            Me.Visita1MaskedTextBox.Enabled = False
            Me.Visita2MaskedTextBox.Enabled = False
            Me.Visita3MaskedTextBox.Enabled = False
            Me.dtpHoraVisita1.Enabled = False
            Me.dtpHoraVisita2.Enabled = False
            Me.dtpHoraVisita3.Enabled = False
            Me.Fecha_EjecucionMaskedTextBox.Enabled = False
            Me.Hora_sol.Enabled = False
            Me.EnProcesoMaskedTextBox.Enabled = True
            Me.StatusTextBox.Text = "S"
        End If
    End Sub

    Private Sub spConsultaEnProcesoQuejas(ByVal prmClvQueja As Long)
        Using CON As New SqlConnection(MiConexion)
            CON.Open()

            Dim CMD As New SqlCommand("spConsultaEnProcesoQuejas", CON)
            CMD.CommandType = CommandType.StoredProcedure
            CMD.Parameters.AddWithValue("@ClvQueja", prmClvQueja)

            Dim READER As SqlDataReader

            Try
                READER = CMD.ExecuteReader()
                While READER.Read
                    quejaGloFechaEnProceso = READER(0).ToString
                    quejaGloHoraEnProceso = READER(0).ToString & " " & READER(1).ToString
                    Me.EnProcesoMaskedTextBox.Text = quejaGloFechaEnProceso
                    If quejaGloFechaEnProceso <> "" Then
                        Me.dtpHoraEnProceso.Value = quejaGloHoraEnProceso
                    End If
                End While
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information)
            End Try
        End Using
    End Sub

    Private Sub spInsertaEnProcesoQuejas(ByVal prmClvQueja As Long, ByVal prmFechaEnProceso As Date, ByVal prmHoraEnProceso As String)
        Using CON As New SqlConnection(MiConexion)
            CON.Open()

            Dim CMD As New SqlCommand("spInsertaEnProcesoQuejas", CON)
            CMD.CommandType = CommandType.StoredProcedure
            CMD.Parameters.AddWithValue("@clvQueja", prmClvQueja)
            CMD.Parameters.AddWithValue("@fechaEnProceso", prmFechaEnProceso)
            CMD.Parameters.AddWithValue("@horaEnProceso", prmHoraEnProceso)

            Dim TRANS As SqlTransaction = Nothing

            Try
                TRANS = CON.BeginTransaction
                CMD.Transaction = TRANS
                CMD.ExecuteNonQuery()
                TRANS.Commit()
            Catch ex As Exception
                TRANS.Rollback()
                MessageBox.Show(ex.Message)
            End Try
        End Using
    End Sub

#Region "Clasificación Problemas (JUANJO)"
    Private Sub llenaComboClasifProblemas()
        Dim Problema As New ClassClasificacionProblemas
        Problema.clvProblema = 0
        Problema.Descripcion = String.Empty
        Problema.OpBusqueda = 4
        Me.cmbClasifProblemas.DataSource = Problema.uspConsultaTblClasificacionProblemas()
    End Sub

    Private Sub Inserta(ByVal prmClvQueja As Long, ByVal prmClvProblema As Long) 'PARA MODIFICAR UN REGISTRO
        Dim Problema As New ClassClasificacionProblemas
        Problema.clvLlamada = 0
        Problema.clvQueja = prmClvQueja
        Problema.clvProblema = prmClvProblema
        Problema.OpAccion = 3
        Problema.uspInseraTblRelQuejaProblema()
    End Sub

    Private Function Consulta(ByVal prmclvQueja As Long) As Long 'PARA OBTENER EL VALOR DE LA CLASIFICACIÓN Y LLENAR EL COMBO
        Dim Problema As New ClassClasificacionProblemas
        Problema.clvLlamada = 0
        Problema.clvQueja = prmclvQueja
        Problema.OpAccion = 2
        Consulta = Problema.uspConsultaTblRelQuejaProblema()
    End Function

    Private Function uspConsultaComentarioAgenda(ByVal prmclvQueja As Long) As String 'OBTIENE EL COMENTARIO DE LA AGENDA DE LA QUEJA
        Dim PROBLEMA As New ClassClasificacionProblemas
        PROBLEMA.clvQueja = prmclvQueja
        uspConsultaComentarioAgenda = PROBLEMA.uspConsultaComentarioAgenda()
    End Function
#End Region
End Class