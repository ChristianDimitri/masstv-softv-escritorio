
Imports System.Data.SqlClient
Public Class BrwTap


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eOpcion = "N"
        FrmTap.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Me.ConTapDataGridView.RowCount > 0 Then
            eOpcion = "C"
            eClv_Sector = Me.Clv_SectorTextBox.Text
            eConsec = Me.ConsecutivoTextBox.Text
            FrmTap.Show()
        Else
            MsgBox("Selecciona un Tap a Consultar.", , "Atenci�n")
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.ConTapDataGridView.RowCount > 0 Then
            eOpcion = "M"
            eClv_Sector = Me.Clv_SectorTextBox.Text
            eConsec = Me.ConsecutivoTextBox.Text
            FrmTap.Show()
        Else
            MsgBox("Selecciona un Tap a Modificar.", , "Atenci�n")
        End If
    End Sub

    Private Sub BrwTap_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConTapTableAdapter.Connection = CON
        Me.ConTapTableAdapter.Fill(Me.DataSetEric.ConTap, "", 0, 0, "", 0, 0, "", "", 0)
        CON.Close()
    End Sub

    Private Sub BrwTap_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConTapTableAdapter.Connection = CON
        Me.ConTapTableAdapter.Fill(Me.DataSetEric.ConTap, "", 0, 0, "", 0, 0, "", "", 0)
        CON.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConTapTableAdapter.Connection = CON
        Me.ConTapTableAdapter.Fill(Me.DataSetEric.ConTap, Me.TextBox1.Text, 0, 0, "", 0, 0, "", "", 2)
        CON.Close()
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()

        If Asc(e.KeyChar) = 13 Then
            Me.ConTapTableAdapter.Connection = CON
            Me.ConTapTableAdapter.Fill(Me.DataSetEric.ConTap, Me.TextBox1.Text, 0, 0, "", 0, 0, "", "", 2)
        End If
        CON.Close()
    End Sub


    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConTapTableAdapter.Connection = CON
        Me.ConTapTableAdapter.Fill(Me.DataSetEric.ConTap, "", 0, 0, Me.TextBox2.Text, 0, 0, "", "", 3)
        CON.Close()
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress

        Dim CON As New SqlConnection(MiConexion)
        CON.Open()

        If Asc(e.KeyChar) = 13 Then
            Me.ConTapTableAdapter.Connection = CON
            Me.ConTapTableAdapter.Fill(Me.DataSetEric.ConTap, "", 0, 0, Me.TextBox2.Text, 0, 0, "", "", 3)
        End If
        CON.Close()
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.TextBox3.Text) = True Then
            Me.ConTapTableAdapter.Connection = CON
            Me.ConTapTableAdapter.Fill(Me.DataSetEric.ConTap, "", 0, Me.TextBox3.Text, "", 0, 0, "", "", 4)
        Else
            MsgBox("La B�squeda no se puede realizar con Dichos Par�metros.", , "Error")
        End If
        CON.Close()
    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Asc(e.KeyChar) = 13 Then
            If IsNumeric(Me.TextBox3.Text) = True Then
                Me.ConTapTableAdapter.Connection = CON
                Me.ConTapTableAdapter.Fill(Me.DataSetEric.ConTap, "", 0, Me.TextBox3.Text, "", 0, 0, "", "", 4)
            Else
                MsgBox("La B�squeda no se puede realizar con Dichos Par�metros.", , "Error")
            End If
        End If
        CON.Close()
    End Sub


    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.TextBox4.Text) = True Then
            Me.ConTapTableAdapter.Connection = CON
            Me.ConTapTableAdapter.Fill(Me.DataSetEric.ConTap, "", 0, 0, "", Me.TextBox4.Text, 0, "", "", 5)
        Else
            MsgBox("La B�squeda no se puede realizar con Dichos Par�metros.", , "Error")
        End If
        CON.Close()
    End Sub

    Private Sub TextBox4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox4.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Asc(e.KeyChar) = 13 Then
            If IsNumeric(Me.TextBox4.Text) = True Then
                Me.ConTapTableAdapter.Connection = CON
                Me.ConTapTableAdapter.Fill(Me.DataSetEric.ConTap, "", 0, 0, "", Me.TextBox4.Text, 0, "", "", 5)
            Else
                MsgBox("La B�squeda no se puede realizar con Dichos Par�metros.", , "Error")
            End If
        End If
        CON.Close()
    End Sub


    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.TextBox5.Text) = True Then
            Me.ConTapTableAdapter.Connection = CON
            Me.ConTapTableAdapter.Fill(Me.DataSetEric.ConTap, "", 0, 0, "", 0, Me.TextBox5.Text, "", "", 6)
        Else
            MsgBox("La B�squeda no se puede realizar con Dichos Par�metros.", , "Error")
        End If
        CON.Close()
    End Sub

    Private Sub TextBox5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox5.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Asc(e.KeyChar) = 13 Then
            If IsNumeric(Me.TextBox5.Text) = True Then
                Me.ConTapTableAdapter.Connection = CON
                Me.ConTapTableAdapter.Fill(Me.DataSetEric.ConTap, "", 0, 0, "", 0, Me.TextBox5.Text, "", "", 6)
            Else
                MsgBox("La B�squeda no se puede realizar con Dichos Par�metros.", , "Error")
            End If
        End If
        CON.Close()
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConTapTableAdapter.Connection = CON
        Me.ConTapTableAdapter.Fill(Me.DataSetEric.ConTap, "", 0, 0, "", 0, 0, Me.TextBox6.Text, "", 7)
        CON.Close()
    End Sub

    Private Sub TextBox6_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox6.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Asc(e.KeyChar) = 13 Then
            Me.ConTapTableAdapter.Connection = CON
            Me.ConTapTableAdapter.Fill(Me.DataSetEric.ConTap, "", 0, 0, "", 0, 0, Me.TextBox6.Text, "", 7)
        End If
        CON.Close()
    End Sub

    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConTapTableAdapter.Connection = CON
        Me.ConTapTableAdapter.Fill(Me.DataSetEric.ConTap, "", 0, 0, "", 0, 0, "", Me.TextBox7.Text, 8)
        CON.Close()
    End Sub

    Private Sub TextBox7_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox7.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Asc(e.KeyChar) = 13 Then
            Me.ConTapTableAdapter.Connection = CON
            Me.ConTapTableAdapter.Fill(Me.DataSetEric.ConTap, "", 0, 0, "", 0, 0, "", Me.TextBox7.Text, 8)
        End If
        CON.Close()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
    End Sub
End Class