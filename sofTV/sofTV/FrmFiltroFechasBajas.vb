﻿Public Class FrmFiltroFechasBajas

    Private Sub FrmFiltroFechasBajas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If CDate(Me.dtpFechaInicial.Value.ToShortDateString) > CDate(Me.dtpFechaFinal.Value.ToShortDateString) Then
            MsgBox("La Fecha Inicial no debe ser Mayor a la Final", MsgBoxStyle.Information)
            Exit Sub
        End If

        bndAvisos2 = True
        FrmImprimir.fechaInicialRecordatorios = Me.dtpFechaInicial.Value
        FrmImprimir.fechaFinalRecordatorios = Me.dtpFechaFinal.Value
        FrmImprimir.Show()
        Me.Close()
    End Sub
End Class