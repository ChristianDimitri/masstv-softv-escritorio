<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelEstado
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.CMBLabel3 = New System.Windows.Forms.Label()
        Me.CMBPanel1 = New System.Windows.Forms.Panel()
        Me.CMBCheckBox7 = New System.Windows.Forms.CheckBox()
        Me.CMBCheckBox6 = New System.Windows.Forms.CheckBox()
        Me.CMBCheckBox5 = New System.Windows.Forms.CheckBox()
        Me.CMBCheckBox4 = New System.Windows.Forms.CheckBox()
        Me.CMBCheckBox3 = New System.Windows.Forms.CheckBox()
        Me.CMBCheckBox2 = New System.Windows.Forms.CheckBox()
        Me.CMBCheckBox1 = New System.Windows.Forms.CheckBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.CMBCheckBox8 = New System.Windows.Forms.CheckBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.MuestraMesesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.CMBPanel2 = New System.Windows.Forms.Panel()
        Me.CaracterComboBox = New System.Windows.Forms.ComboBox()
        Me.Muestra_CaracteresBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CMBLabel4 = New System.Windows.Forms.Label()
        Me.Muestra_MesesTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_MesesTableAdapter()
        Me.InsertaRel_Reporte_Ciudad_Ultimo_mesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertaRel_Reporte_Ciudad_Ultimo_mesTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.InsertaRel_Reporte_Ciudad_Ultimo_mesTableAdapter()
        Me.ProcedimientosArnoldo2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Muestra_CaracteresTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_CaracteresTableAdapter()
        Me.CMBPanel3 = New System.Windows.Forms.Panel()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.MuestraMotCancReporteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CMBLabel7 = New System.Windows.Forms.Label()
        Me.Muestra_MotCanc_ReporteTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_MotCanc_ReporteTableAdapter()
        Me.Inserta_Rel_Motcan_Rep_CiudadBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Rel_Motcan_Rep_CiudadTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_Rel_Motcan_Rep_CiudadTableAdapter()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.CMBPanel1.SuspendLayout()
        CType(Me.MuestraMesesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CMBPanel2.SuspendLayout()
        CType(Me.Muestra_CaracteresBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertaRel_Reporte_Ciudad_Ultimo_mesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CMBPanel3.SuspendLayout()
        CType(Me.MuestraMotCancReporteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Rel_Motcan_Rep_CiudadBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBLabel3
        '
        Me.CMBLabel3.AutoSize = True
        Me.CMBLabel3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel3.Location = New System.Drawing.Point(26, 24)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(306, 24)
        Me.CMBLabel3.TabIndex = 5
        Me.CMBLabel3.Text = "Selecciona el Status de Cliente:"
        '
        'CMBPanel1
        '
        Me.CMBPanel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CMBPanel1.Controls.Add(Me.CMBCheckBox7)
        Me.CMBPanel1.Controls.Add(Me.CMBCheckBox6)
        Me.CMBPanel1.Controls.Add(Me.CMBCheckBox5)
        Me.CMBPanel1.Controls.Add(Me.CMBCheckBox4)
        Me.CMBPanel1.Controls.Add(Me.CMBCheckBox3)
        Me.CMBPanel1.Controls.Add(Me.CMBCheckBox2)
        Me.CMBPanel1.Controls.Add(Me.CMBCheckBox1)
        Me.CMBPanel1.Location = New System.Drawing.Point(30, 51)
        Me.CMBPanel1.Name = "CMBPanel1"
        Me.CMBPanel1.Size = New System.Drawing.Size(428, 86)
        Me.CMBPanel1.TabIndex = 6
        '
        'CMBCheckBox7
        '
        Me.CMBCheckBox7.AutoSize = True
        Me.CMBCheckBox7.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CMBCheckBox7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBCheckBox7.Location = New System.Drawing.Point(12, 63)
        Me.CMBCheckBox7.Name = "CMBCheckBox7"
        Me.CMBCheckBox7.Size = New System.Drawing.Size(193, 19)
        Me.CMBCheckBox7.TabIndex = 10
        Me.CMBCheckBox7.Text = "Suspendidos Temporales."
        Me.CMBCheckBox7.UseVisualStyleBackColor = False
        '
        'CMBCheckBox6
        '
        Me.CMBCheckBox6.AutoSize = True
        Me.CMBCheckBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBCheckBox6.Location = New System.Drawing.Point(150, 15)
        Me.CMBCheckBox6.Name = "CMBCheckBox6"
        Me.CMBCheckBox6.Size = New System.Drawing.Size(116, 19)
        Me.CMBCheckBox6.TabIndex = 2
        Me.CMBCheckBox6.Text = "Fuera de Area"
        Me.CMBCheckBox6.UseVisualStyleBackColor = True
        '
        'CMBCheckBox5
        '
        Me.CMBCheckBox5.AutoSize = True
        Me.CMBCheckBox5.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CMBCheckBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBCheckBox5.Location = New System.Drawing.Point(308, 15)
        Me.CMBCheckBox5.Name = "CMBCheckBox5"
        Me.CMBCheckBox5.Size = New System.Drawing.Size(109, 19)
        Me.CMBCheckBox5.TabIndex = 4
        Me.CMBCheckBox5.Text = "Suspendidos"
        Me.CMBCheckBox5.UseVisualStyleBackColor = False
        '
        'CMBCheckBox4
        '
        Me.CMBCheckBox4.AutoSize = True
        Me.CMBCheckBox4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CMBCheckBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBCheckBox4.Location = New System.Drawing.Point(150, 38)
        Me.CMBCheckBox4.Name = "CMBCheckBox4"
        Me.CMBCheckBox4.Size = New System.Drawing.Size(117, 19)
        Me.CMBCheckBox4.TabIndex = 3
        Me.CMBCheckBox4.Text = "Desconectado"
        Me.CMBCheckBox4.UseVisualStyleBackColor = False
        '
        'CMBCheckBox3
        '
        Me.CMBCheckBox3.AutoSize = True
        Me.CMBCheckBox3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CMBCheckBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBCheckBox3.Location = New System.Drawing.Point(12, 38)
        Me.CMBCheckBox3.Name = "CMBCheckBox3"
        Me.CMBCheckBox3.Size = New System.Drawing.Size(85, 19)
        Me.CMBCheckBox3.TabIndex = 1
        Me.CMBCheckBox3.Text = "Instalado"
        Me.CMBCheckBox3.UseVisualStyleBackColor = False
        '
        'CMBCheckBox2
        '
        Me.CMBCheckBox2.AutoSize = True
        Me.CMBCheckBox2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CMBCheckBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBCheckBox2.Location = New System.Drawing.Point(308, 38)
        Me.CMBCheckBox2.Name = "CMBCheckBox2"
        Me.CMBCheckBox2.Size = New System.Drawing.Size(101, 19)
        Me.CMBCheckBox2.TabIndex = 5
        Me.CMBCheckBox2.Text = "Cancelados"
        Me.CMBCheckBox2.UseVisualStyleBackColor = False
        '
        'CMBCheckBox1
        '
        Me.CMBCheckBox1.AutoSize = True
        Me.CMBCheckBox1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CMBCheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBCheckBox1.Location = New System.Drawing.Point(12, 13)
        Me.CMBCheckBox1.Name = "CMBCheckBox1"
        Me.CMBCheckBox1.Size = New System.Drawing.Size(96, 19)
        Me.CMBCheckBox1.TabIndex = 0
        Me.CMBCheckBox1.Text = "Contratado"
        Me.CMBCheckBox1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Orange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(573, 201)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(159, 42)
        Me.Button2.TabIndex = 8
        Me.Button2.Text = "IMPRIMIR &REPORTE"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.Orange
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(573, 301)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(159, 36)
        Me.Button3.TabIndex = 9
        Me.Button3.Text = "&CANCELAR"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Orange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(573, 251)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(159, 42)
        Me.Button1.TabIndex = 10
        Me.Button1.Text = "&IMPRIMIR ETIQUETAS"
        Me.Button1.UseVisualStyleBackColor = False
        Me.Button1.Visible = False
        '
        'CMBCheckBox8
        '
        Me.CMBCheckBox8.AutoSize = True
        Me.CMBCheckBox8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBCheckBox8.Location = New System.Drawing.Point(30, 177)
        Me.CMBCheckBox8.Name = "CMBCheckBox8"
        Me.CMBCheckBox8.Size = New System.Drawing.Size(277, 20)
        Me.CMBCheckBox8.TabIndex = 11
        Me.CMBCheckBox8.Text = "Buscar Por Mes Que Adeudan y Año"
        Me.CMBCheckBox8.UseVisualStyleBackColor = True
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.MuestraMesesBindingSource
        Me.ComboBox1.DisplayMember = "mes"
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(25, 94)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(189, 24)
        Me.ComboBox1.TabIndex = 12
        Me.ComboBox1.ValueMember = "clv_mes"
        '
        'MuestraMesesBindingSource
        '
        Me.MuestraMesesBindingSource.DataMember = "Muestra_Meses"
        Me.MuestraMesesBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(269, 94)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(114, 22)
        Me.TextBox1.TabIndex = 13
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(22, 75)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(127, 16)
        Me.CMBLabel1.TabIndex = 14
        Me.CMBLabel1.Text = "Mes Que Adeuda"
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(266, 75)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(35, 16)
        Me.CMBLabel2.TabIndex = 15
        Me.CMBLabel2.Text = "Año"
        '
        'CMBPanel2
        '
        Me.CMBPanel2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CMBPanel2.Controls.Add(Me.CaracterComboBox)
        Me.CMBPanel2.Controls.Add(Me.CMBLabel4)
        Me.CMBPanel2.Controls.Add(Me.ComboBox1)
        Me.CMBPanel2.Controls.Add(Me.CMBLabel2)
        Me.CMBPanel2.Controls.Add(Me.CMBLabel1)
        Me.CMBPanel2.Controls.Add(Me.TextBox1)
        Me.CMBPanel2.Enabled = False
        Me.CMBPanel2.Location = New System.Drawing.Point(25, 203)
        Me.CMBPanel2.Name = "CMBPanel2"
        Me.CMBPanel2.Size = New System.Drawing.Size(419, 128)
        Me.CMBPanel2.TabIndex = 16
        '
        'CaracterComboBox
        '
        Me.CaracterComboBox.DataSource = Me.Muestra_CaracteresBindingSource
        Me.CaracterComboBox.DisplayMember = "caracter"
        Me.CaracterComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CaracterComboBox.FormattingEnabled = True
        Me.CaracterComboBox.Location = New System.Drawing.Point(229, 24)
        Me.CaracterComboBox.Name = "CaracterComboBox"
        Me.CaracterComboBox.Size = New System.Drawing.Size(171, 24)
        Me.CaracterComboBox.TabIndex = 17
        Me.CaracterComboBox.ValueMember = "clv_caracter"
        '
        'Muestra_CaracteresBindingSource
        '
        Me.Muestra_CaracteresBindingSource.DataMember = "Muestra_Caracteres"
        Me.Muestra_CaracteresBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'CMBLabel4
        '
        Me.CMBLabel4.AutoSize = True
        Me.CMBLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel4.Location = New System.Drawing.Point(22, 25)
        Me.CMBLabel4.Name = "CMBLabel4"
        Me.CMBLabel4.Size = New System.Drawing.Size(201, 16)
        Me.CMBLabel4.TabIndex = 16
        Me.CMBLabel4.Text = "Buscar Por El Mes que Sea:"
        '
        'Muestra_MesesTableAdapter
        '
        Me.Muestra_MesesTableAdapter.ClearBeforeFill = True
        '
        'InsertaRel_Reporte_Ciudad_Ultimo_mesBindingSource
        '
        Me.InsertaRel_Reporte_Ciudad_Ultimo_mesBindingSource.DataMember = "InsertaRel_Reporte_Ciudad_Ultimo_mes"
        Me.InsertaRel_Reporte_Ciudad_Ultimo_mesBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'InsertaRel_Reporte_Ciudad_Ultimo_mesTableAdapter
        '
        Me.InsertaRel_Reporte_Ciudad_Ultimo_mesTableAdapter.ClearBeforeFill = True
        '
        'ProcedimientosArnoldo2BindingSource
        '
        Me.ProcedimientosArnoldo2BindingSource.DataSource = Me.ProcedimientosArnoldo2
        Me.ProcedimientosArnoldo2BindingSource.Position = 0
        '
        'Muestra_CaracteresTableAdapter
        '
        Me.Muestra_CaracteresTableAdapter.ClearBeforeFill = True
        '
        'CMBPanel3
        '
        Me.CMBPanel3.Controls.Add(Me.ComboBox2)
        Me.CMBPanel3.Controls.Add(Me.CMBLabel7)
        Me.CMBPanel3.Enabled = False
        Me.CMBPanel3.Location = New System.Drawing.Point(512, 51)
        Me.CMBPanel3.Name = "CMBPanel3"
        Me.CMBPanel3.Size = New System.Drawing.Size(319, 86)
        Me.CMBPanel3.TabIndex = 17
        '
        'ComboBox2
        '
        Me.ComboBox2.DataSource = Me.MuestraMotCancReporteBindingSource
        Me.ComboBox2.DisplayMember = "Motcan"
        Me.ComboBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(24, 52)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(271, 24)
        Me.ComboBox2.TabIndex = 18
        Me.ComboBox2.ValueMember = "clv_motcan"
        '
        'MuestraMotCancReporteBindingSource
        '
        Me.MuestraMotCancReporteBindingSource.DataMember = "Muestra_MotCanc_Reporte"
        Me.MuestraMotCancReporteBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'CMBLabel7
        '
        Me.CMBLabel7.AutoSize = True
        Me.CMBLabel7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel7.Location = New System.Drawing.Point(3, 19)
        Me.CMBLabel7.Name = "CMBLabel7"
        Me.CMBLabel7.Size = New System.Drawing.Size(293, 16)
        Me.CMBLabel7.TabIndex = 0
        Me.CMBLabel7.Text = "Selecciona El Motivo De La Cancelacion:"
        '
        'Muestra_MotCanc_ReporteTableAdapter
        '
        Me.Muestra_MotCanc_ReporteTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Rel_Motcan_Rep_CiudadBindingSource
        '
        Me.Inserta_Rel_Motcan_Rep_CiudadBindingSource.DataMember = "Inserta_Rel_Motcan_Rep_Ciudad"
        Me.Inserta_Rel_Motcan_Rep_CiudadBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Inserta_Rel_Motcan_Rep_CiudadTableAdapter
        '
        Me.Inserta_Rel_Motcan_Rep_CiudadTableAdapter.ClearBeforeFill = True
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.Orange
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.Black
        Me.Button4.Location = New System.Drawing.Point(573, 151)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(159, 42)
        Me.Button4.TabIndex = 18
        Me.Button4.Text = "IMPRIMIR &ESTADO DE CUENTA"
        Me.Button4.UseVisualStyleBackColor = False
        Me.Button4.Visible = False
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'FrmSelEstado
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(851, 359)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.CMBPanel3)
        Me.Controls.Add(Me.CMBPanel2)
        Me.Controls.Add(Me.CMBCheckBox8)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.CMBPanel1)
        Me.Controls.Add(Me.CMBLabel3)
        Me.MaximizeBox = False
        Me.Name = "FrmSelEstado"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selección de Status Cliente(s)"
        Me.TopMost = True
        Me.CMBPanel1.ResumeLayout(False)
        Me.CMBPanel1.PerformLayout()
        CType(Me.MuestraMesesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CMBPanel2.ResumeLayout(False)
        Me.CMBPanel2.PerformLayout()
        CType(Me.Muestra_CaracteresBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertaRel_Reporte_Ciudad_Ultimo_mesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CMBPanel3.ResumeLayout(False)
        Me.CMBPanel3.PerformLayout()
        CType(Me.MuestraMotCancReporteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Rel_Motcan_Rep_CiudadBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents CMBPanel1 As System.Windows.Forms.Panel
    Friend WithEvents CMBCheckBox4 As System.Windows.Forms.CheckBox
    Friend WithEvents CMBCheckBox3 As System.Windows.Forms.CheckBox
    Friend WithEvents CMBCheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents CMBCheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents CMBCheckBox6 As System.Windows.Forms.CheckBox
    Friend WithEvents CMBCheckBox5 As System.Windows.Forms.CheckBox
    Friend WithEvents CMBCheckBox7 As System.Windows.Forms.CheckBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents CMBCheckBox8 As System.Windows.Forms.CheckBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents CMBPanel2 As System.Windows.Forms.Panel
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents MuestraMesesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_MesesTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_MesesTableAdapter
    Friend WithEvents InsertaRel_Reporte_Ciudad_Ultimo_mesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertaRel_Reporte_Ciudad_Ultimo_mesTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.InsertaRel_Reporte_Ciudad_Ultimo_mesTableAdapter
    Friend WithEvents CMBLabel4 As System.Windows.Forms.Label
    Friend WithEvents ProcedimientosArnoldo2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_CaracteresBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_CaracteresTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_CaracteresTableAdapter
    Friend WithEvents CaracterComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents CMBPanel3 As System.Windows.Forms.Panel
    Friend WithEvents CMBLabel7 As System.Windows.Forms.Label
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents MuestraMotCancReporteBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_MotCanc_ReporteTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_MotCanc_ReporteTableAdapter
    Friend WithEvents Inserta_Rel_Motcan_Rep_CiudadBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Rel_Motcan_Rep_CiudadTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_Rel_Motcan_Rep_CiudadTableAdapter
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
