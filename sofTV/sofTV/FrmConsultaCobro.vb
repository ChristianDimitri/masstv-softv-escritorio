Imports System.Data.SqlClient
Public Class FrmConsultaCobro

    Dim clv_Session1 As Long = 0
    Dim bnderror As Integer = 0
    Dim msg As String = Nothing
    Dim PagoMens As Decimal
    Dim PagPen As Integer
    Dim EsEco As Integer

    Private Sub FrmConsultaCobro_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        Try
            'Me.Panel5.Visible = False
            bnderror = 0
            If IsNumeric(Me.Clv_SessionTextBox.Text) = True Then
                Me.BorraClv_SessionTableAdapter.Connection = CON
                Me.BorraClv_SessionTableAdapter.Fill(Me.DataSetEric.BorraClv_Session, Me.Clv_SessionTextBox.Text)
            End If
            If IsNumeric(eGloContrato) = True Then
                'GloContrato = Me.ContratoTextBox.Text
                Try
                    Me.BUSCLIPORCONTRATO_FACTableAdapter.Connection = CON
                    Me.BUSCLIPORCONTRATO_FACTableAdapter.Fill(Me.DataSetEric.BUSCLIPORCONTRATO_FAC, eGloContrato, 0)
                Catch ex As Exception

                End Try

                'Me.BuscaBloqueadoTableAdapter.Fill(Me.NewsoftvDataSet2.BuscaBloqueado, Me.ContratoTextBox.Text, NUM, num2)
                'If NUM = 0 Or num2 = 0 Then
                '    CREAARBOL()
                'Else
                '    bloqueado = 1
                '    clibloqueado()
                '    MsgBox("El Cliente " + Me.ContratoTextBox.Text + " Ha Sido Bloqueado por lo que no se Podr� Llevar a cabo la Queja ", MsgBoxStyle.Exclamation)
                '    Me.ContratoTextBox.Text = 0
                '    Bloque(False)
                '    'Glocontratosel = 0
                'End If
                Me.CobraTableAdapter.Connection = CON
                Me.CobraTableAdapter.Fill(Me.DataSetEric.Cobra, eGloContrato, IdSistema, 0, clv_Session1, bnderror, msg)
                Me.DameDetalleTableAdapter.Connection = CON
                Me.DameDetalleTableAdapter.Fill(Me.DataSetEric.DameDetalle, clv_Session1, 0)

                Usp_ClientesEco()

                If EsEco = 1 Then

                    DameImportePagSiguiente()
                    InsertaPagoParaAbonoACuenta(clv_Session1, PagoMens)

                End If

                Me.DameDetalleTableAdapter.Connection = CON
                Me.DameDetalleTableAdapter.Fill(Me.DataSetEric.DameDetalle, clv_Session1, 0)
                Me.SumaDetalleTableAdapter.Connection = CON
                Me.SumaDetalleTableAdapter.Fill(Me.DataSetEric.SumaDetalle, clv_Session1, False, 0)
                Me.Clv_SessionTextBox.Text = clv_Session1
                'If IsNumeric(bnderror) = False Then bnderror = 1
                'If bnderror = 1 Then

                'ElseIf bloqueado <> 1 Then

                '    'bloqueado = 0
                'End If


            Else
                eGloContrato = 0
                Me.BUSCLIPORCONTRATO_FACTableAdapter.Connection = CON
                Me.BUSCLIPORCONTRATO_FACTableAdapter.Fill(Me.DataSetEric.BUSCLIPORCONTRATO_FAC, 0, 0)
                Me.CobraTableAdapter.Connection = CON
                Me.CobraTableAdapter.Fill(Me.DataSetEric.Cobra, New System.Nullable(Of Long)(CType(0, Long)), "", 0, Me.Clv_SessionTextBox.Text, bnderror, msg)
                Me.Clv_SessionTextBox.Text = 0

            End If
            'DAMETIPOSCLIENTEDAME()
            'bloqueado = 0
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub


    Private Sub Clv_SessionTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_SessionTextBox.TextChanged
        'If IsNumeric(Me.Clv_SessionTextBox.Text) = True Then
        '    'Me.DameDetalleTableAdapter.Fill(Me.DataSetEric.DameDetalle, Me.Clv_SessionTextBox.Text, 0)
        '    Me.SumaDetalleTableAdapter.Fill(Me.DataSetEric.SumaDetalle, Me.Clv_SessionTextBox.Text, False, 0)
        'End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        If Me.DataGridView1.RowCount > 0 Then
            If DataGridView1.SelectedCells(7).Value = "Ver Detalle" Then

                FrmDetCobroDesc.Show()
            End If
        End If
    End Sub

    Private Sub DameImportePagSiguiente()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, eGloContrato)
        BaseII.CreateMyParameter("@PagoMensual", ParameterDirection.Output, SqlDbType.Money)
        BaseII.CreateMyParameter("@PagPen", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("DameImportePagSiguiente")
        PagoMens = CInt(BaseII.dicoPar("@PagoMensual").ToString)
        PagPen = CDbl(BaseII.dicoPar("@PagPen").ToString)
    End Sub

    Private Sub Usp_ClientesEco()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, eGloContrato)
        BaseII.CreateMyParameter("@EsEco", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("Usp_ClientesEco")
        EsEco = CInt(BaseII.dicoPar("@EsEco").ToString)

    End Sub

    Private Sub InsertaPagoParaAbonoACuenta(ByVal Clv_Session As Long, ByVal Importe As Double)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("InsertaPagoParaAbonarEconoPak", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Session
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Importe", SqlDbType.Decimal)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Importe
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = eGloContrato
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@CLVSESSIONABONO", SqlDbType.BigInt)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = 0
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@BND", SqlDbType.BigInt)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = 0
        comando.Parameters.Add(parametro5)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)

            'Log_Descripcion = ex.Message.ToString
            'Log_Formulario = Me.Name.ToString
            'Log_ProcedimientoAlmacenado = "InsertaPagoParaAbonarEconoPak"
            'GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub
End Class