<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmInternet
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Label10 As System.Windows.Forms.Label
        Dim CortesiaLabel2 As System.Windows.Forms.Label
        Dim CMBLabel7 As System.Windows.Forms.Label
        Dim ObsLabel2 As System.Windows.Forms.Label
        Dim EmailLabel1 As System.Windows.Forms.Label
        Dim Clv_VendedorLabel As System.Windows.Forms.Label
        Dim FacturaLabel As System.Windows.Forms.Label
        Dim PrimerMensualidadLabel As System.Windows.Forms.Label
        Dim FECHA_ULT_PAGOLabel1 As System.Windows.Forms.Label
        Dim Fecha_Fuera_AreaLabel As System.Windows.Forms.Label
        Dim Fecha_bajaLabel As System.Windows.Forms.Label
        Dim Fecha_suspensionLabel As System.Windows.Forms.Label
        Dim Fecha_instalacioLabel As System.Windows.Forms.Label
        Dim Fecha_solicitudLabel1 As System.Windows.Forms.Label
        Dim StatusLabel2 As System.Windows.Forms.Label
        Dim Fecha_BajaLabel1 As System.Windows.Forms.Label
        Dim SeRentaLabel As System.Windows.Forms.Label
        Dim ObsLabel1 As System.Windows.Forms.Label
        Dim Tipo_CablemodemLabel As System.Windows.Forms.Label
        Dim Fecha_TraspasoLabel As System.Windows.Forms.Label
        Dim Fecha_SuspencionLabel As System.Windows.Forms.Label
        Dim Fecha_ActivacionLabel As System.Windows.Forms.Label
        Dim Clv_TipoServicioLabel As System.Windows.Forms.Label
        Dim Ventacablemodem2Label As System.Windows.Forms.Label
        Dim Ventacablemodem1Label As System.Windows.Forms.Label
        Dim StatusLabel1 As System.Windows.Forms.Label
        Dim CMBLabel5 As System.Windows.Forms.Label
        Dim CMBLabel6 As System.Windows.Forms.Label
        Dim CMBTIPOAPARATOLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmInternet))
        Me.LblPlazoForzoso = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TipoCortesiaComboBox = New System.Windows.Forms.ComboBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.LblPlazoForzoso1 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Button26 = New System.Windows.Forms.Button()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.DescuentoLabel3 = New System.Windows.Forms.Label()
        Me.ConRelCteDescuentoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.ComboBox5 = New System.Windows.Forms.ComboBox()
        Me.CONSULTACONTNETBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.StatusNetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button23 = New System.Windows.Forms.Button()
        Me.ObsTextBox2 = New System.Windows.Forms.TextBox()
        Me.NOMBRELabel4 = New System.Windows.Forms.Label()
        Me.CONRel_ContNet_UsuariosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEDGAR = New sofTV.DataSetEDGAR()
        Me.Button18 = New System.Windows.Forms.Button()
        Me.CortesiaCheckBox2 = New System.Windows.Forms.CheckBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.BindingNavigator2 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton7 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton8 = New System.Windows.Forms.ToolStripButton()
        Me.ComboBox6 = New System.Windows.Forms.ComboBox()
        Me.MuestraPromotoresNetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CMBTextBox4 = New System.Windows.Forms.TextBox()
        Me.DESCRIPCIONLabel1 = New System.Windows.Forms.Label()
        Me.EmailTextBox1 = New System.Windows.Forms.TextBox()
        Me.FacturaTextBox = New System.Windows.Forms.TextBox()
        Me.Ultimo_anioTextBox1 = New System.Windows.Forms.TextBox()
        Me.Ultimo_mesTextBox1 = New System.Windows.Forms.TextBox()
        Me.PrimerMensualidadCheckBox = New System.Windows.Forms.CheckBox()
        Me.FECHA_ULT_PAGOTextBox1 = New System.Windows.Forms.TextBox()
        Me.Fecha_Fuera_AreaTextBox = New System.Windows.Forms.TextBox()
        Me.Fecha_bajaTextBox = New System.Windows.Forms.TextBox()
        Me.Fecha_suspensionTextBox = New System.Windows.Forms.TextBox()
        Me.Fecha_instalacioTextBox = New System.Windows.Forms.TextBox()
        Me.Fecha_solicitudTextBox1 = New System.Windows.Forms.TextBox()
        Me.Clv_ServicioTextBox = New System.Windows.Forms.TextBox()
        Me.ContratoNetTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_UnicaNetTextBox = New System.Windows.Forms.TextBox()
        Me.StatusTextBox2 = New System.Windows.Forms.TextBox()
        Me.Clv_PromocionTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_VendedorTextBox = New System.Windows.Forms.TextBox()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.txtMacCablModem = New System.Windows.Forms.TextBox()
        Me.VerAparatodelClienteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button1 = New System.Windows.Forms.Button()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.CONSULTACLIENTESNETBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLidia2 = New sofTV.DataSetLidia2()
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown()
        Me.ObsTextBox1 = New System.Windows.Forms.TextBox()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.StatusCableModemBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Fecha_BajaTextBox1 = New System.Windows.Forms.TextBox()
        Me.ContratoNetTextBox1 = New System.Windows.Forms.TextBox()
        Me.ContratoTextBox2 = New System.Windows.Forms.TextBox()
        Me.ComboBox4 = New System.Windows.Forms.ComboBox()
        Me.TipoCablemodemBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ComboBox3 = New System.Windows.Forms.ComboBox()
        Me.MuestraTipSerInternetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.StatusTextBox1 = New System.Windows.Forms.TextBox()
        Me.CMBTextBox3 = New System.Windows.Forms.TextBox()
        Me.SeRentaCheckBox = New System.Windows.Forms.CheckBox()
        Me.CMBTextBox2 = New System.Windows.Forms.TextBox()
        Me.Tipo_CablemodemTextBox = New System.Windows.Forms.TextBox()
        Me.Fecha_TraspasoTextBox = New System.Windows.Forms.TextBox()
        Me.Fecha_SuspencionTextBox = New System.Windows.Forms.TextBox()
        Me.Fecha_ActivacionTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_TipoServicioTextBox = New System.Windows.Forms.TextBox()
        Me.Ventacablemodem2CheckBox = New System.Windows.Forms.CheckBox()
        Me.Ventacablemodem1CheckBox = New System.Windows.Forms.CheckBox()
        Me.TIPOAPARATOLabel1 = New System.Windows.Forms.Label()
        Me.MARCALabel1 = New System.Windows.Forms.Label()
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton5 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton6 = New System.Windows.Forms.ToolStripButton()
        Me.MACCABLEMODEMLabel1 = New System.Windows.Forms.Label()
        Me.Procedimientosarnoldo4 = New sofTV.Procedimientosarnoldo4()
        Me.Inserta_Rel_cortesia_FechaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Rel_cortesia_FechaTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Inserta_Rel_cortesia_FechaTableAdapter()
        Me.CONRel_ContNet_UsuariosTableAdapter = New sofTV.DataSetEDGARTableAdapters.CONRel_ContNet_UsuariosTableAdapter()
        Me.ConRelCteDescuentoTableAdapter = New sofTV.DataSetEricTableAdapters.ConRelCteDescuentoTableAdapter()
        Me.ChecaRelCteDescuentoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ChecaRelCteDescuentoTableAdapter = New sofTV.DataSetEricTableAdapters.ChecaRelCteDescuentoTableAdapter()
        Me.MuestraPromotoresNetTableAdapter = New sofTV.DataSetEDGARTableAdapters.MuestraPromotoresNetTableAdapter()
        Me.GUARDARRel_ContNet_UsuariosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GUARDARRel_ContNet_UsuariosTableAdapter = New sofTV.DataSetEDGARTableAdapters.GUARDARRel_ContNet_UsuariosTableAdapter()
        Me.CONSULTACONTNETTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONSULTACONTNETTableAdapter()
        Me.VerAparatodelClienteTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.VerAparatodelClienteTableAdapter()
        Me.DataSetLidia = New sofTV.DataSetLidia()
        Me.DameFechaHabilitarBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameFechaHabilitarTableAdapter = New sofTV.DataSetLidiaTableAdapters.DameFechaHabilitarTableAdapter()
        Me.StatusNetTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.StatusNetTableAdapter()
        Me.StatusCableModemTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.StatusCableModemTableAdapter()
        Me.TipoCablemodemTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.TipoCablemodemTableAdapter()
        Me.MuestraTipSerInternetTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MuestraTipSerInternetTableAdapter()
        Me.HABILITACABLEMODEMBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.HABILITACABLEMODEMTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.HABILITACABLEMODEMTableAdapter()
        Me.PrimerMesCLIENTESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PrimerMesCLIENTESTableAdapter = New sofTV.DataSetLidiaTableAdapters.PrimerMesCLIENTESTableAdapter()
        Me.CONSULTACLIENTESNETTableAdapter = New sofTV.DataSetLidia2TableAdapters.CONSULTACLIENTESNETTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter5 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Label10 = New System.Windows.Forms.Label()
        CortesiaLabel2 = New System.Windows.Forms.Label()
        CMBLabel7 = New System.Windows.Forms.Label()
        ObsLabel2 = New System.Windows.Forms.Label()
        EmailLabel1 = New System.Windows.Forms.Label()
        Clv_VendedorLabel = New System.Windows.Forms.Label()
        FacturaLabel = New System.Windows.Forms.Label()
        PrimerMensualidadLabel = New System.Windows.Forms.Label()
        FECHA_ULT_PAGOLabel1 = New System.Windows.Forms.Label()
        Fecha_Fuera_AreaLabel = New System.Windows.Forms.Label()
        Fecha_bajaLabel = New System.Windows.Forms.Label()
        Fecha_suspensionLabel = New System.Windows.Forms.Label()
        Fecha_instalacioLabel = New System.Windows.Forms.Label()
        Fecha_solicitudLabel1 = New System.Windows.Forms.Label()
        StatusLabel2 = New System.Windows.Forms.Label()
        Fecha_BajaLabel1 = New System.Windows.Forms.Label()
        SeRentaLabel = New System.Windows.Forms.Label()
        ObsLabel1 = New System.Windows.Forms.Label()
        Tipo_CablemodemLabel = New System.Windows.Forms.Label()
        Fecha_TraspasoLabel = New System.Windows.Forms.Label()
        Fecha_SuspencionLabel = New System.Windows.Forms.Label()
        Fecha_ActivacionLabel = New System.Windows.Forms.Label()
        Clv_TipoServicioLabel = New System.Windows.Forms.Label()
        Ventacablemodem2Label = New System.Windows.Forms.Label()
        Ventacablemodem1Label = New System.Windows.Forms.Label()
        StatusLabel1 = New System.Windows.Forms.Label()
        CMBLabel5 = New System.Windows.Forms.Label()
        CMBLabel6 = New System.Windows.Forms.Label()
        CMBTIPOAPARATOLabel = New System.Windows.Forms.Label()
        Me.Panel6.SuspendLayout()
        CType(Me.ConRelCteDescuentoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONSULTACONTNETBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusNetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONRel_ContNet_UsuariosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingNavigator2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator2.SuspendLayout()
        CType(Me.MuestraPromotoresNetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        CType(Me.VerAparatodelClienteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONSULTACLIENTESNETBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusCableModemBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TipoCablemodemBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTipSerInternetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Rel_cortesia_FechaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ChecaRelCteDescuentoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GUARDARRel_ContNet_UsuariosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameFechaHabilitarBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.HABILITACABLEMODEMBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PrimerMesCLIENTESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label10
        '
        Label10.AutoSize = True
        Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label10.ForeColor = System.Drawing.Color.LightSlateGray
        Label10.Location = New System.Drawing.Point(11, 233)
        Label10.Name = "Label10"
        Label10.Size = New System.Drawing.Size(65, 15)
        Label10.TabIndex = 81
        Label10.Text = "Capturo :"
        '
        'CortesiaLabel2
        '
        CortesiaLabel2.AutoSize = True
        CortesiaLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CortesiaLabel2.ForeColor = System.Drawing.Color.LightSlateGray
        CortesiaLabel2.Location = New System.Drawing.Point(8, 152)
        CortesiaLabel2.Name = "CortesiaLabel2"
        CortesiaLabel2.Size = New System.Drawing.Size(64, 15)
        CortesiaLabel2.TabIndex = 55
        CortesiaLabel2.Text = "Cortesia:"
        '
        'CMBLabel7
        '
        CMBLabel7.AutoSize = True
        CMBLabel7.BackColor = System.Drawing.Color.Transparent
        CMBLabel7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabel7.ForeColor = System.Drawing.Color.Black
        CMBLabel7.Location = New System.Drawing.Point(5, 29)
        CMBLabel7.Name = "CMBLabel7"
        CMBLabel7.Size = New System.Drawing.Size(151, 18)
        CMBLabel7.TabIndex = 36
        CMBLabel7.Text = "Datos del Servicio "
        '
        'ObsLabel2
        '
        ObsLabel2.AutoSize = True
        ObsLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ObsLabel2.ForeColor = System.Drawing.Color.LightSlateGray
        ObsLabel2.Location = New System.Drawing.Point(11, 258)
        ObsLabel2.Name = "ObsLabel2"
        ObsLabel2.Size = New System.Drawing.Size(109, 15)
        ObsLabel2.TabIndex = 34
        ObsLabel2.Text = "Observaciones :"
        '
        'EmailLabel1
        '
        EmailLabel1.AutoSize = True
        EmailLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        EmailLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        EmailLabel1.Location = New System.Drawing.Point(7, 178)
        EmailLabel1.Name = "EmailLabel1"
        EmailLabel1.Size = New System.Drawing.Size(48, 15)
        EmailLabel1.TabIndex = 32
        EmailLabel1.Text = "Email:"
        '
        'Clv_VendedorLabel
        '
        Clv_VendedorLabel.AutoSize = True
        Clv_VendedorLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_VendedorLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_VendedorLabel.Location = New System.Drawing.Point(4, 206)
        Clv_VendedorLabel.Name = "Clv_VendedorLabel"
        Clv_VendedorLabel.Size = New System.Drawing.Size(76, 15)
        Clv_VendedorLabel.TabIndex = 28
        Clv_VendedorLabel.Text = "Vendedor :"
        '
        'FacturaLabel
        '
        FacturaLabel.AutoSize = True
        FacturaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FacturaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        FacturaLabel.Location = New System.Drawing.Point(1, 93)
        FacturaLabel.Name = "FacturaLabel"
        FacturaLabel.Size = New System.Drawing.Size(75, 15)
        FacturaLabel.TabIndex = 26
        FacturaLabel.Text = "# Factura :"
        '
        'PrimerMensualidadLabel
        '
        PrimerMensualidadLabel.AutoSize = True
        PrimerMensualidadLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PrimerMensualidadLabel.ForeColor = System.Drawing.Color.LightSlateGray
        PrimerMensualidadLabel.Location = New System.Drawing.Point(10, 123)
        PrimerMensualidadLabel.Name = "PrimerMensualidadLabel"
        PrimerMensualidadLabel.Size = New System.Drawing.Size(141, 15)
        PrimerMensualidadLabel.TabIndex = 20
        PrimerMensualidadLabel.Text = "Primer Mensualidad:"
        '
        'FECHA_ULT_PAGOLabel1
        '
        FECHA_ULT_PAGOLabel1.AutoSize = True
        FECHA_ULT_PAGOLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FECHA_ULT_PAGOLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        FECHA_ULT_PAGOLabel1.Location = New System.Drawing.Point(340, 219)
        FECHA_ULT_PAGOLabel1.Name = "FECHA_ULT_PAGOLabel1"
        FECHA_ULT_PAGOLabel1.Size = New System.Drawing.Size(94, 15)
        FECHA_ULT_PAGOLabel1.TabIndex = 18
        FECHA_ULT_PAGOLabel1.Text = "Ultimo Pago :"
        '
        'Fecha_Fuera_AreaLabel
        '
        Fecha_Fuera_AreaLabel.AutoSize = True
        Fecha_Fuera_AreaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_Fuera_AreaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Fecha_Fuera_AreaLabel.Location = New System.Drawing.Point(345, 193)
        Fecha_Fuera_AreaLabel.Name = "Fecha_Fuera_AreaLabel"
        Fecha_Fuera_AreaLabel.Size = New System.Drawing.Size(85, 15)
        Fecha_Fuera_AreaLabel.TabIndex = 16
        Fecha_Fuera_AreaLabel.Text = "Fuera Area :"
        '
        'Fecha_bajaLabel
        '
        Fecha_bajaLabel.AutoSize = True
        Fecha_bajaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_bajaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Fecha_bajaLabel.Location = New System.Drawing.Point(384, 167)
        Fecha_bajaLabel.Name = "Fecha_bajaLabel"
        Fecha_bajaLabel.Size = New System.Drawing.Size(44, 15)
        Fecha_bajaLabel.TabIndex = 14
        Fecha_bajaLabel.Text = "Baja :"
        '
        'Fecha_suspensionLabel
        '
        Fecha_suspensionLabel.AutoSize = True
        Fecha_suspensionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_suspensionLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Fecha_suspensionLabel.Location = New System.Drawing.Point(341, 141)
        Fecha_suspensionLabel.Name = "Fecha_suspensionLabel"
        Fecha_suspensionLabel.Size = New System.Drawing.Size(90, 15)
        Fecha_suspensionLabel.TabIndex = 12
        Fecha_suspensionLabel.Text = "Suspención :"
        '
        'Fecha_instalacioLabel
        '
        Fecha_instalacioLabel.AutoSize = True
        Fecha_instalacioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_instalacioLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Fecha_instalacioLabel.Location = New System.Drawing.Point(346, 114)
        Fecha_instalacioLabel.Name = "Fecha_instalacioLabel"
        Fecha_instalacioLabel.Size = New System.Drawing.Size(85, 15)
        Fecha_instalacioLabel.TabIndex = 10
        Fecha_instalacioLabel.Text = "Instalación :"
        '
        'Fecha_solicitudLabel1
        '
        Fecha_solicitudLabel1.AutoSize = True
        Fecha_solicitudLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_solicitudLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        Fecha_solicitudLabel1.Location = New System.Drawing.Point(338, 89)
        Fecha_solicitudLabel1.Name = "Fecha_solicitudLabel1"
        Fecha_solicitudLabel1.Size = New System.Drawing.Size(96, 15)
        Fecha_solicitudLabel1.TabIndex = 8
        Fecha_solicitudLabel1.Text = "Contratación :"
        '
        'StatusLabel2
        '
        StatusLabel2.AutoSize = True
        StatusLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        StatusLabel2.ForeColor = System.Drawing.Color.LightSlateGray
        StatusLabel2.Location = New System.Drawing.Point(21, 61)
        StatusLabel2.Name = "StatusLabel2"
        StatusLabel2.Size = New System.Drawing.Size(55, 15)
        StatusLabel2.TabIndex = 6
        StatusLabel2.Text = "Status :"
        '
        'Fecha_BajaLabel1
        '
        Fecha_BajaLabel1.AutoSize = True
        Fecha_BajaLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_BajaLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        Fecha_BajaLabel1.Location = New System.Drawing.Point(342, 190)
        Fecha_BajaLabel1.Name = "Fecha_BajaLabel1"
        Fecha_BajaLabel1.Size = New System.Drawing.Size(83, 15)
        Fecha_BajaLabel1.TabIndex = 54
        Fecha_BajaLabel1.Text = "Fecha Baja:"
        '
        'SeRentaLabel
        '
        SeRentaLabel.AutoSize = True
        SeRentaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SeRentaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        SeRentaLabel.Location = New System.Drawing.Point(410, 303)
        SeRentaLabel.Name = "SeRentaLabel"
        SeRentaLabel.Size = New System.Drawing.Size(74, 15)
        SeRentaLabel.TabIndex = 46
        SeRentaLabel.Text = "Se Renta :"
        SeRentaLabel.Visible = False
        '
        'ObsLabel1
        '
        ObsLabel1.AutoSize = True
        ObsLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ObsLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        ObsLabel1.Location = New System.Drawing.Point(30, 218)
        ObsLabel1.Name = "ObsLabel1"
        ObsLabel1.Size = New System.Drawing.Size(109, 15)
        ObsLabel1.TabIndex = 23
        ObsLabel1.Text = "Observaciones :"
        '
        'Tipo_CablemodemLabel
        '
        Tipo_CablemodemLabel.AutoSize = True
        Tipo_CablemodemLabel.BackColor = System.Drawing.Color.WhiteSmoke
        Tipo_CablemodemLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Tipo_CablemodemLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Tipo_CablemodemLabel.Location = New System.Drawing.Point(8, 170)
        Tipo_CablemodemLabel.Name = "Tipo_CablemodemLabel"
        Tipo_CablemodemLabel.Size = New System.Drawing.Size(132, 15)
        Tipo_CablemodemLabel.TabIndex = 21
        Tipo_CablemodemLabel.Text = "Tipo Cablemodem :"
        Tipo_CablemodemLabel.Visible = False
        '
        'Fecha_TraspasoLabel
        '
        Fecha_TraspasoLabel.AutoSize = True
        Fecha_TraspasoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_TraspasoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Fecha_TraspasoLabel.Location = New System.Drawing.Point(351, 163)
        Fecha_TraspasoLabel.Name = "Fecha_TraspasoLabel"
        Fecha_TraspasoLabel.Size = New System.Drawing.Size(74, 15)
        Fecha_TraspasoLabel.TabIndex = 19
        Fecha_TraspasoLabel.Text = "Traspaso :"
        '
        'Fecha_SuspencionLabel
        '
        Fecha_SuspencionLabel.AutoSize = True
        Fecha_SuspencionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_SuspencionLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Fecha_SuspencionLabel.Location = New System.Drawing.Point(339, 137)
        Fecha_SuspencionLabel.Name = "Fecha_SuspencionLabel"
        Fecha_SuspencionLabel.Size = New System.Drawing.Size(86, 15)
        Fecha_SuspencionLabel.TabIndex = 17
        Fecha_SuspencionLabel.Text = "Suspención:"
        '
        'Fecha_ActivacionLabel
        '
        Fecha_ActivacionLabel.AutoSize = True
        Fecha_ActivacionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_ActivacionLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Fecha_ActivacionLabel.Location = New System.Drawing.Point(350, 111)
        Fecha_ActivacionLabel.Name = "Fecha_ActivacionLabel"
        Fecha_ActivacionLabel.Size = New System.Drawing.Size(75, 15)
        Fecha_ActivacionLabel.TabIndex = 15
        Fecha_ActivacionLabel.Text = "Activación:"
        '
        'Clv_TipoServicioLabel
        '
        Clv_TipoServicioLabel.AutoSize = True
        Clv_TipoServicioLabel.BackColor = System.Drawing.Color.WhiteSmoke
        Clv_TipoServicioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_TipoServicioLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_TipoServicioLabel.Location = New System.Drawing.Point(41, 142)
        Clv_TipoServicioLabel.Name = "Clv_TipoServicioLabel"
        Clv_TipoServicioLabel.Size = New System.Drawing.Size(98, 15)
        Clv_TipoServicioLabel.TabIndex = 13
        Clv_TipoServicioLabel.Text = "Tipo Servicio :"
        '
        'Ventacablemodem2Label
        '
        Ventacablemodem2Label.AutoSize = True
        Ventacablemodem2Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Ventacablemodem2Label.ForeColor = System.Drawing.Color.LightSlateGray
        Ventacablemodem2Label.Location = New System.Drawing.Point(329, 279)
        Ventacablemodem2Label.Name = "Ventacablemodem2Label"
        Ventacablemodem2Label.Size = New System.Drawing.Size(153, 15)
        Ventacablemodem2Label.TabIndex = 11
        Ventacablemodem2Label.Text = "CableModem Pagado :"
        Ventacablemodem2Label.Visible = False
        '
        'Ventacablemodem1Label
        '
        Ventacablemodem1Label.AutoSize = True
        Ventacablemodem1Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Ventacablemodem1Label.ForeColor = System.Drawing.Color.LightSlateGray
        Ventacablemodem1Label.Location = New System.Drawing.Point(425, 258)
        Ventacablemodem1Label.Name = "Ventacablemodem1Label"
        Ventacablemodem1Label.Size = New System.Drawing.Size(57, 15)
        Ventacablemodem1Label.TabIndex = 9
        Ventacablemodem1Label.Text = "Propio :"
        Ventacablemodem1Label.Visible = False
        '
        'StatusLabel1
        '
        StatusLabel1.AutoSize = True
        StatusLabel1.BackColor = System.Drawing.Color.WhiteSmoke
        StatusLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        StatusLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        StatusLabel1.Location = New System.Drawing.Point(40, 117)
        StatusLabel1.Name = "StatusLabel1"
        StatusLabel1.Size = New System.Drawing.Size(99, 13)
        StatusLabel1.TabIndex = 7
        StatusLabel1.Text = "Status Aparato :"
        '
        'CMBLabel5
        '
        CMBLabel5.AutoSize = True
        CMBLabel5.BackColor = System.Drawing.Color.WhiteSmoke
        CMBLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabel5.ForeColor = System.Drawing.Color.Black
        CMBLabel5.Location = New System.Drawing.Point(3, 31)
        CMBLabel5.Name = "CMBLabel5"
        CMBLabel5.Size = New System.Drawing.Size(143, 18)
        CMBLabel5.TabIndex = 6
        CMBLabel5.Text = "Datos del Aparato"
        '
        'CMBLabel6
        '
        CMBLabel6.AutoSize = True
        CMBLabel6.BackColor = System.Drawing.Color.WhiteSmoke
        CMBLabel6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabel6.ForeColor = System.Drawing.Color.LightSlateGray
        CMBLabel6.Location = New System.Drawing.Point(84, 65)
        CMBLabel6.Name = "CMBLabel6"
        CMBLabel6.Size = New System.Drawing.Size(55, 15)
        CMBLabel6.TabIndex = 2
        CMBLabel6.Text = "Marca :"
        '
        'CMBTIPOAPARATOLabel
        '
        CMBTIPOAPARATOLabel.AutoSize = True
        CMBTIPOAPARATOLabel.BackColor = System.Drawing.Color.WhiteSmoke
        CMBTIPOAPARATOLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBTIPOAPARATOLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CMBTIPOAPARATOLabel.Location = New System.Drawing.Point(44, 89)
        CMBTIPOAPARATOLabel.Name = "CMBTIPOAPARATOLabel"
        CMBTIPOAPARATOLabel.Size = New System.Drawing.Size(96, 15)
        CMBTIPOAPARATOLabel.TabIndex = 4
        CMBTIPOAPARATOLabel.Text = "Tipo Aparato :"
        '
        'LblPlazoForzoso
        '
        Me.LblPlazoForzoso.AutoSize = True
        Me.LblPlazoForzoso.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPlazoForzoso.ForeColor = System.Drawing.Color.LightSlateGray
        Me.LblPlazoForzoso.Location = New System.Drawing.Point(327, 278)
        Me.LblPlazoForzoso.Name = "LblPlazoForzoso"
        Me.LblPlazoForzoso.Size = New System.Drawing.Size(106, 15)
        Me.LblPlazoForzoso.TabIndex = 86
        Me.LblPlazoForzoso.Text = "Plazo Forzoso :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label1.Location = New System.Drawing.Point(20, 195)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(119, 15)
        Me.Label1.TabIndex = 59
        Me.Label1.Text = "No. Extensiones :"
        Me.Label1.Visible = False
        '
        'Panel6
        '
        Me.Panel6.AutoScroll = True
        Me.Panel6.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel6.Controls.Add(Me.Button2)
        Me.Panel6.Controls.Add(Me.TipoCortesiaComboBox)
        Me.Panel6.Controls.Add(Me.ComboBox1)
        Me.Panel6.Controls.Add(Me.LblPlazoForzoso1)
        Me.Panel6.Controls.Add(Me.LblPlazoForzoso)
        Me.Panel6.Controls.Add(Me.Label49)
        Me.Panel6.Controls.Add(Me.Button26)
        Me.Panel6.Controls.Add(Me.Label44)
        Me.Panel6.Controls.Add(Me.TextBox5)
        Me.Panel6.Controls.Add(Me.DescuentoLabel3)
        Me.Panel6.Controls.Add(Me.ComboBox5)
        Me.Panel6.Controls.Add(Me.Button23)
        Me.Panel6.Controls.Add(Me.ObsTextBox2)
        Me.Panel6.Controls.Add(Me.NOMBRELabel4)
        Me.Panel6.Controls.Add(Label10)
        Me.Panel6.Controls.Add(Me.Button18)
        Me.Panel6.Controls.Add(CortesiaLabel2)
        Me.Panel6.Controls.Add(Me.CortesiaCheckBox2)
        Me.Panel6.Controls.Add(Me.Label11)
        Me.Panel6.Controls.Add(Me.Label19)
        Me.Panel6.Controls.Add(Me.Label20)
        Me.Panel6.Controls.Add(Me.BindingNavigator2)
        Me.Panel6.Controls.Add(Me.ComboBox6)
        Me.Panel6.Controls.Add(Me.CMBTextBox4)
        Me.Panel6.Controls.Add(Me.DESCRIPCIONLabel1)
        Me.Panel6.Controls.Add(CMBLabel7)
        Me.Panel6.Controls.Add(ObsLabel2)
        Me.Panel6.Controls.Add(EmailLabel1)
        Me.Panel6.Controls.Add(Me.EmailTextBox1)
        Me.Panel6.Controls.Add(Clv_VendedorLabel)
        Me.Panel6.Controls.Add(FacturaLabel)
        Me.Panel6.Controls.Add(Me.FacturaTextBox)
        Me.Panel6.Controls.Add(Me.Ultimo_anioTextBox1)
        Me.Panel6.Controls.Add(Me.Ultimo_mesTextBox1)
        Me.Panel6.Controls.Add(PrimerMensualidadLabel)
        Me.Panel6.Controls.Add(Me.PrimerMensualidadCheckBox)
        Me.Panel6.Controls.Add(FECHA_ULT_PAGOLabel1)
        Me.Panel6.Controls.Add(Me.FECHA_ULT_PAGOTextBox1)
        Me.Panel6.Controls.Add(Fecha_Fuera_AreaLabel)
        Me.Panel6.Controls.Add(Me.Fecha_Fuera_AreaTextBox)
        Me.Panel6.Controls.Add(Fecha_bajaLabel)
        Me.Panel6.Controls.Add(Me.Fecha_bajaTextBox)
        Me.Panel6.Controls.Add(Fecha_suspensionLabel)
        Me.Panel6.Controls.Add(Me.Fecha_suspensionTextBox)
        Me.Panel6.Controls.Add(Fecha_instalacioLabel)
        Me.Panel6.Controls.Add(Me.Fecha_instalacioTextBox)
        Me.Panel6.Controls.Add(Fecha_solicitudLabel1)
        Me.Panel6.Controls.Add(Me.Fecha_solicitudTextBox1)
        Me.Panel6.Controls.Add(StatusLabel2)
        Me.Panel6.Controls.Add(Me.Clv_ServicioTextBox)
        Me.Panel6.Controls.Add(Me.ContratoNetTextBox)
        Me.Panel6.Controls.Add(Me.Clv_UnicaNetTextBox)
        Me.Panel6.Controls.Add(Me.StatusTextBox2)
        Me.Panel6.Controls.Add(Me.Clv_PromocionTextBox)
        Me.Panel6.Controls.Add(Me.Clv_VendedorTextBox)
        Me.Panel6.Enabled = False
        Me.Panel6.Location = New System.Drawing.Point(1, 1)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(559, 326)
        Me.Panel6.TabIndex = 26
        Me.Panel6.Visible = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.SystemColors.Control
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(430, 244)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(114, 28)
        Me.Button2.TabIndex = 90
        Me.Button2.Text = "Datos Instalación"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'TipoCortesiaComboBox
        '
        Me.TipoCortesiaComboBox.DisplayMember = "DESCRIPCION"
        Me.TipoCortesiaComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TipoCortesiaComboBox.FormattingEnabled = True
        Me.TipoCortesiaComboBox.Location = New System.Drawing.Point(117, 146)
        Me.TipoCortesiaComboBox.Name = "TipoCortesiaComboBox"
        Me.TipoCortesiaComboBox.Size = New System.Drawing.Size(142, 20)
        Me.TipoCortesiaComboBox.TabIndex = 89
        Me.TipoCortesiaComboBox.ValueMember = "CLV_CORTESIA"
        '
        'ComboBox1
        '
        Me.ComboBox1.DisplayMember = "nombre"
        Me.ComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(82, 232)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(221, 21)
        Me.ComboBox1.TabIndex = 88
        Me.ComboBox1.ValueMember = "clave"
        '
        'LblPlazoForzoso1
        '
        Me.LblPlazoForzoso1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPlazoForzoso1.ForeColor = System.Drawing.Color.Red
        Me.LblPlazoForzoso1.Location = New System.Drawing.Point(435, 278)
        Me.LblPlazoForzoso1.Name = "LblPlazoForzoso1"
        Me.LblPlazoForzoso1.Size = New System.Drawing.Size(106, 21)
        Me.LblPlazoForzoso1.TabIndex = 87
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label49.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label49.Location = New System.Drawing.Point(326, 301)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(94, 15)
        Me.Label49.TabIndex = 85
        Me.Label49.Text = "Ultimo Pago :"
        '
        'Button26
        '
        Me.Button26.BackColor = System.Drawing.SystemColors.Control
        Me.Button26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button26.ForeColor = System.Drawing.Color.Black
        Me.Button26.Location = New System.Drawing.Point(304, 244)
        Me.Button26.Name = "Button26"
        Me.Button26.Size = New System.Drawing.Size(123, 28)
        Me.Button26.TabIndex = 33
        Me.Button26.Text = "Establecer Precios"
        Me.Button26.UseVisualStyleBackColor = False
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label44.Location = New System.Drawing.Point(320, 166)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(19, 15)
        Me.Label44.TabIndex = 34
        Me.Label44.Text = "%"
        Me.Label44.Visible = False
        '
        'TextBox5
        '
        Me.TextBox5.BackColor = System.Drawing.Color.White
        Me.TextBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox5.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox5.Location = New System.Drawing.Point(205, 111)
        Me.TextBox5.MaxLength = 3
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(50, 21)
        Me.TextBox5.TabIndex = 83
        Me.TextBox5.TabStop = False
        Me.TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DescuentoLabel3
        '
        Me.DescuentoLabel3.BackColor = System.Drawing.Color.White
        Me.DescuentoLabel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DescuentoLabel3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConRelCteDescuentoBindingSource, "Descuento", True))
        Me.DescuentoLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescuentoLabel3.Location = New System.Drawing.Point(272, 163)
        Me.DescuentoLabel3.Name = "DescuentoLabel3"
        Me.DescuentoLabel3.Size = New System.Drawing.Size(42, 20)
        Me.DescuentoLabel3.TabIndex = 33
        Me.DescuentoLabel3.Visible = False
        '
        'ConRelCteDescuentoBindingSource
        '
        Me.ConRelCteDescuentoBindingSource.DataMember = "ConRelCteDescuento"
        Me.ConRelCteDescuentoBindingSource.DataSource = Me.DataSetEric
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ComboBox5
        '
        Me.ComboBox5.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONSULTACONTNETBindingSource, "status", True))
        Me.ComboBox5.DataSource = Me.StatusNetBindingSource
        Me.ComboBox5.DisplayMember = "Concepto"
        Me.ComboBox5.Enabled = False
        Me.ComboBox5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox5.FormattingEnabled = True
        Me.ComboBox5.Location = New System.Drawing.Point(77, 59)
        Me.ComboBox5.Name = "ComboBox5"
        Me.ComboBox5.Size = New System.Drawing.Size(122, 23)
        Me.ComboBox5.TabIndex = 48
        Me.ComboBox5.TabStop = False
        Me.ComboBox5.ValueMember = "Clv_StatusNet"
        '
        'CONSULTACONTNETBindingSource
        '
        Me.CONSULTACONTNETBindingSource.DataMember = "CONSULTACONTNET"
        Me.CONSULTACONTNETBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'StatusNetBindingSource
        '
        Me.StatusNetBindingSource.DataMember = "StatusNet"
        Me.StatusNetBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'Button23
        '
        Me.Button23.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Button23.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button23.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button23.ForeColor = System.Drawing.Color.Black
        Me.Button23.Location = New System.Drawing.Point(262, 137)
        Me.Button23.Name = "Button23"
        Me.Button23.Size = New System.Drawing.Size(77, 22)
        Me.Button23.TabIndex = 84
        Me.Button23.Text = "Descuento"
        Me.Button23.UseVisualStyleBackColor = False
        '
        'ObsTextBox2
        '
        Me.ObsTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ObsTextBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTNETBindingSource, "Obs", True))
        Me.ObsTextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ObsTextBox2.Location = New System.Drawing.Point(16, 277)
        Me.ObsTextBox2.Multiline = True
        Me.ObsTextBox2.Name = "ObsTextBox2"
        Me.ObsTextBox2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.ObsTextBox2.Size = New System.Drawing.Size(304, 42)
        Me.ObsTextBox2.TabIndex = 28
        '
        'NOMBRELabel4
        '
        Me.NOMBRELabel4.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONRel_ContNet_UsuariosBindingSource, "NOMBRE", True))
        Me.NOMBRELabel4.Location = New System.Drawing.Point(83, 232)
        Me.NOMBRELabel4.Name = "NOMBRELabel4"
        Me.NOMBRELabel4.Size = New System.Drawing.Size(219, 19)
        Me.NOMBRELabel4.TabIndex = 82
        Me.NOMBRELabel4.Visible = False
        '
        'CONRel_ContNet_UsuariosBindingSource
        '
        Me.CONRel_ContNet_UsuariosBindingSource.DataMember = "CONRel_ContNet_Usuarios"
        Me.CONRel_ContNet_UsuariosBindingSource.DataSource = Me.DataSetEDGAR
        '
        'DataSetEDGAR
        '
        Me.DataSetEDGAR.DataSetName = "DataSetEDGAR"
        Me.DataSetEDGAR.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Button18
        '
        Me.Button18.BackColor = System.Drawing.SystemColors.Control
        Me.Button18.ForeColor = System.Drawing.Color.Black
        Me.Button18.Location = New System.Drawing.Point(94, 149)
        Me.Button18.Name = "Button18"
        Me.Button18.Size = New System.Drawing.Size(22, 20)
        Me.Button18.TabIndex = 26
        Me.Button18.Text = "..."
        Me.Button18.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button18.UseVisualStyleBackColor = False
        '
        'CortesiaCheckBox2
        '
        Me.CortesiaCheckBox2.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONSULTACONTNETBindingSource, "Cortesia", True))
        Me.CortesiaCheckBox2.Enabled = False
        Me.CortesiaCheckBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CortesiaCheckBox2.Location = New System.Drawing.Point(76, 149)
        Me.CortesiaCheckBox2.Name = "CortesiaCheckBox2"
        Me.CortesiaCheckBox2.Size = New System.Drawing.Size(19, 24)
        Me.CortesiaCheckBox2.TabIndex = 56
        Me.CortesiaCheckBox2.TabStop = False
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.Color.Silver
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Black
        Me.Label11.Location = New System.Drawing.Point(243, 83)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(75, 25)
        Me.Label11.TabIndex = 63
        Me.Label11.Text = " Año"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label19
        '
        Me.Label19.BackColor = System.Drawing.Color.Silver
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.Black
        Me.Label19.Location = New System.Drawing.Point(205, 83)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(47, 25)
        Me.Label19.TabIndex = 61
        Me.Label19.Text = " Mes"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label20
        '
        Me.Label20.BackColor = System.Drawing.Color.Silver
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.Black
        Me.Label20.Location = New System.Drawing.Point(205, 61)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(113, 25)
        Me.Label20.TabIndex = 62
        Me.Label20.Text = "Ultimo Pagado "
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BindingNavigator2
        '
        Me.BindingNavigator2.AddNewItem = Nothing
        Me.BindingNavigator2.CountItem = Nothing
        Me.BindingNavigator2.DeleteItem = Nothing
        Me.BindingNavigator2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripSeparator5, Me.ToolStripButton7, Me.ToolStripSeparator6, Me.ToolStripButton8})
        Me.BindingNavigator2.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigator2.MoveFirstItem = Nothing
        Me.BindingNavigator2.MoveLastItem = Nothing
        Me.BindingNavigator2.MoveNextItem = Nothing
        Me.BindingNavigator2.MovePreviousItem = Nothing
        Me.BindingNavigator2.Name = "BindingNavigator2"
        Me.BindingNavigator2.PositionItem = Nothing
        Me.BindingNavigator2.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.BindingNavigator2.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.BindingNavigator2.Size = New System.Drawing.Size(559, 25)
        Me.BindingNavigator2.TabIndex = 29
        Me.BindingNavigator2.TabStop = True
        Me.BindingNavigator2.Text = "BindingNavigator2"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripButton7
        '
        Me.ToolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton7.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton7.Name = "ToolStripButton7"
        Me.ToolStripButton7.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton7.Size = New System.Drawing.Size(76, 22)
        Me.ToolStripButton7.Text = "CA&NCELAR"
        Me.ToolStripButton7.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripButton8
        '
        Me.ToolStripButton8.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton8.Image = CType(resources.GetObject("ToolStripButton8.Image"), System.Drawing.Image)
        Me.ToolStripButton8.Name = "ToolStripButton8"
        Me.ToolStripButton8.Size = New System.Drawing.Size(88, 22)
        Me.ToolStripButton8.Text = "GUA&RDAR"
        '
        'ComboBox6
        '
        Me.ComboBox6.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONSULTACONTNETBindingSource, "Clv_Vendedor", True))
        Me.ComboBox6.DataSource = Me.MuestraPromotoresNetBindingSource
        Me.ComboBox6.DisplayMember = "Nombre"
        Me.ComboBox6.Enabled = False
        Me.ComboBox6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox6.FormattingEnabled = True
        Me.ComboBox6.Location = New System.Drawing.Point(86, 203)
        Me.ComboBox6.Name = "ComboBox6"
        Me.ComboBox6.Size = New System.Drawing.Size(216, 23)
        Me.ComboBox6.TabIndex = 50
        Me.ComboBox6.ValueMember = "Clave"
        '
        'MuestraPromotoresNetBindingSource
        '
        Me.MuestraPromotoresNetBindingSource.DataMember = "MuestraPromotoresNet"
        Me.MuestraPromotoresNetBindingSource.DataSource = Me.DataSetEDGAR
        '
        'CMBTextBox4
        '
        Me.CMBTextBox4.BackColor = System.Drawing.Color.Silver
        Me.CMBTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox4.ForeColor = System.Drawing.Color.Black
        Me.CMBTextBox4.Location = New System.Drawing.Point(328, 62)
        Me.CMBTextBox4.Name = "CMBTextBox4"
        Me.CMBTextBox4.ReadOnly = True
        Me.CMBTextBox4.Size = New System.Drawing.Size(216, 19)
        Me.CMBTextBox4.TabIndex = 47
        Me.CMBTextBox4.TabStop = False
        Me.CMBTextBox4.Text = "Fechas de "
        Me.CMBTextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DESCRIPCIONLabel1
        '
        Me.DESCRIPCIONLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTNETBindingSource, "DESCRIPCION", True))
        Me.DESCRIPCIONLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DESCRIPCIONLabel1.ForeColor = System.Drawing.Color.Navy
        Me.DESCRIPCIONLabel1.Location = New System.Drawing.Point(168, 29)
        Me.DESCRIPCIONLabel1.Name = "DESCRIPCIONLabel1"
        Me.DESCRIPCIONLabel1.Size = New System.Drawing.Size(373, 23)
        Me.DESCRIPCIONLabel1.TabIndex = 38
        '
        'EmailTextBox1
        '
        Me.EmailTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EmailTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTNETBindingSource, "Email", True))
        Me.EmailTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EmailTextBox1.Location = New System.Drawing.Point(56, 176)
        Me.EmailTextBox1.Name = "EmailTextBox1"
        Me.EmailTextBox1.Size = New System.Drawing.Size(203, 21)
        Me.EmailTextBox1.TabIndex = 27
        '
        'FacturaTextBox
        '
        Me.FacturaTextBox.BackColor = System.Drawing.Color.White
        Me.FacturaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FacturaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTNETBindingSource, "factura", True))
        Me.FacturaTextBox.Location = New System.Drawing.Point(77, 94)
        Me.FacturaTextBox.Name = "FacturaTextBox"
        Me.FacturaTextBox.ReadOnly = True
        Me.FacturaTextBox.Size = New System.Drawing.Size(100, 20)
        Me.FacturaTextBox.TabIndex = 27
        Me.FacturaTextBox.TabStop = False
        '
        'Ultimo_anioTextBox1
        '
        Me.Ultimo_anioTextBox1.BackColor = System.Drawing.Color.White
        Me.Ultimo_anioTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Ultimo_anioTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTNETBindingSource, "ultimo_anio", True))
        Me.Ultimo_anioTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Ultimo_anioTextBox1.Location = New System.Drawing.Point(260, 111)
        Me.Ultimo_anioTextBox1.Name = "Ultimo_anioTextBox1"
        Me.Ultimo_anioTextBox1.Size = New System.Drawing.Size(58, 21)
        Me.Ultimo_anioTextBox1.TabIndex = 25
        Me.Ultimo_anioTextBox1.TabStop = False
        Me.Ultimo_anioTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Ultimo_mesTextBox1
        '
        Me.Ultimo_mesTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Ultimo_mesTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTNETBindingSource, "ultimo_mes", True))
        Me.Ultimo_mesTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Ultimo_mesTextBox1.Location = New System.Drawing.Point(245, 111)
        Me.Ultimo_mesTextBox1.Name = "Ultimo_mesTextBox1"
        Me.Ultimo_mesTextBox1.ReadOnly = True
        Me.Ultimo_mesTextBox1.Size = New System.Drawing.Size(10, 21)
        Me.Ultimo_mesTextBox1.TabIndex = 23
        Me.Ultimo_mesTextBox1.TabStop = False
        '
        'PrimerMensualidadCheckBox
        '
        Me.PrimerMensualidadCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONSULTACONTNETBindingSource, "PrimerMensualidad", True))
        Me.PrimerMensualidadCheckBox.Enabled = False
        Me.PrimerMensualidadCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.PrimerMensualidadCheckBox.Location = New System.Drawing.Point(155, 120)
        Me.PrimerMensualidadCheckBox.Name = "PrimerMensualidadCheckBox"
        Me.PrimerMensualidadCheckBox.Size = New System.Drawing.Size(20, 24)
        Me.PrimerMensualidadCheckBox.TabIndex = 21
        Me.PrimerMensualidadCheckBox.TabStop = False
        '
        'FECHA_ULT_PAGOTextBox1
        '
        Me.FECHA_ULT_PAGOTextBox1.BackColor = System.Drawing.Color.White
        Me.FECHA_ULT_PAGOTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FECHA_ULT_PAGOTextBox1.CausesValidation = False
        Me.FECHA_ULT_PAGOTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTNETBindingSource, "FECHA_ULT_PAGO", True))
        Me.FECHA_ULT_PAGOTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FECHA_ULT_PAGOTextBox1.Location = New System.Drawing.Point(440, 217)
        Me.FECHA_ULT_PAGOTextBox1.Name = "FECHA_ULT_PAGOTextBox1"
        Me.FECHA_ULT_PAGOTextBox1.ReadOnly = True
        Me.FECHA_ULT_PAGOTextBox1.Size = New System.Drawing.Size(100, 21)
        Me.FECHA_ULT_PAGOTextBox1.TabIndex = 19
        Me.FECHA_ULT_PAGOTextBox1.TabStop = False
        '
        'Fecha_Fuera_AreaTextBox
        '
        Me.Fecha_Fuera_AreaTextBox.BackColor = System.Drawing.Color.White
        Me.Fecha_Fuera_AreaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Fecha_Fuera_AreaTextBox.CausesValidation = False
        Me.Fecha_Fuera_AreaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTNETBindingSource, "fecha_Fuera_Area", True))
        Me.Fecha_Fuera_AreaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fecha_Fuera_AreaTextBox.Location = New System.Drawing.Point(440, 191)
        Me.Fecha_Fuera_AreaTextBox.Name = "Fecha_Fuera_AreaTextBox"
        Me.Fecha_Fuera_AreaTextBox.ReadOnly = True
        Me.Fecha_Fuera_AreaTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Fecha_Fuera_AreaTextBox.TabIndex = 17
        Me.Fecha_Fuera_AreaTextBox.TabStop = False
        '
        'Fecha_bajaTextBox
        '
        Me.Fecha_bajaTextBox.BackColor = System.Drawing.Color.White
        Me.Fecha_bajaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Fecha_bajaTextBox.CausesValidation = False
        Me.Fecha_bajaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTNETBindingSource, "fecha_baja", True))
        Me.Fecha_bajaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fecha_bajaTextBox.Location = New System.Drawing.Point(440, 165)
        Me.Fecha_bajaTextBox.Name = "Fecha_bajaTextBox"
        Me.Fecha_bajaTextBox.ReadOnly = True
        Me.Fecha_bajaTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Fecha_bajaTextBox.TabIndex = 15
        Me.Fecha_bajaTextBox.TabStop = False
        '
        'Fecha_suspensionTextBox
        '
        Me.Fecha_suspensionTextBox.BackColor = System.Drawing.Color.White
        Me.Fecha_suspensionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Fecha_suspensionTextBox.CausesValidation = False
        Me.Fecha_suspensionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTNETBindingSource, "fecha_suspension", True))
        Me.Fecha_suspensionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fecha_suspensionTextBox.Location = New System.Drawing.Point(440, 139)
        Me.Fecha_suspensionTextBox.Name = "Fecha_suspensionTextBox"
        Me.Fecha_suspensionTextBox.ReadOnly = True
        Me.Fecha_suspensionTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Fecha_suspensionTextBox.TabIndex = 13
        Me.Fecha_suspensionTextBox.TabStop = False
        '
        'Fecha_instalacioTextBox
        '
        Me.Fecha_instalacioTextBox.BackColor = System.Drawing.Color.White
        Me.Fecha_instalacioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Fecha_instalacioTextBox.CausesValidation = False
        Me.Fecha_instalacioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTNETBindingSource, "fecha_instalacio", True))
        Me.Fecha_instalacioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fecha_instalacioTextBox.Location = New System.Drawing.Point(440, 113)
        Me.Fecha_instalacioTextBox.Name = "Fecha_instalacioTextBox"
        Me.Fecha_instalacioTextBox.ReadOnly = True
        Me.Fecha_instalacioTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Fecha_instalacioTextBox.TabIndex = 11
        Me.Fecha_instalacioTextBox.TabStop = False
        '
        'Fecha_solicitudTextBox1
        '
        Me.Fecha_solicitudTextBox1.BackColor = System.Drawing.Color.White
        Me.Fecha_solicitudTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Fecha_solicitudTextBox1.CausesValidation = False
        Me.Fecha_solicitudTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTNETBindingSource, "fecha_solicitud", True))
        Me.Fecha_solicitudTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fecha_solicitudTextBox1.Location = New System.Drawing.Point(440, 87)
        Me.Fecha_solicitudTextBox1.Name = "Fecha_solicitudTextBox1"
        Me.Fecha_solicitudTextBox1.ReadOnly = True
        Me.Fecha_solicitudTextBox1.Size = New System.Drawing.Size(100, 21)
        Me.Fecha_solicitudTextBox1.TabIndex = 9
        Me.Fecha_solicitudTextBox1.TabStop = False
        '
        'Clv_ServicioTextBox
        '
        Me.Clv_ServicioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTNETBindingSource, "Clv_Servicio", True))
        Me.Clv_ServicioTextBox.Location = New System.Drawing.Point(323, 2)
        Me.Clv_ServicioTextBox.Name = "Clv_ServicioTextBox"
        Me.Clv_ServicioTextBox.Size = New System.Drawing.Size(26, 20)
        Me.Clv_ServicioTextBox.TabIndex = 5
        '
        'ContratoNetTextBox
        '
        Me.ContratoNetTextBox.Location = New System.Drawing.Point(404, 3)
        Me.ContratoNetTextBox.Name = "ContratoNetTextBox"
        Me.ContratoNetTextBox.Size = New System.Drawing.Size(20, 20)
        Me.ContratoNetTextBox.TabIndex = 3
        Me.ContratoNetTextBox.Visible = False
        '
        'Clv_UnicaNetTextBox
        '
        Me.Clv_UnicaNetTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTNETBindingSource, "Clv_UnicaNet", True))
        Me.Clv_UnicaNetTextBox.Location = New System.Drawing.Point(363, 2)
        Me.Clv_UnicaNetTextBox.Name = "Clv_UnicaNetTextBox"
        Me.Clv_UnicaNetTextBox.Size = New System.Drawing.Size(26, 20)
        Me.Clv_UnicaNetTextBox.TabIndex = 1
        Me.Clv_UnicaNetTextBox.Visible = False
        '
        'StatusTextBox2
        '
        Me.StatusTextBox2.Location = New System.Drawing.Point(147, 60)
        Me.StatusTextBox2.Name = "StatusTextBox2"
        Me.StatusTextBox2.Size = New System.Drawing.Size(39, 20)
        Me.StatusTextBox2.TabIndex = 39
        Me.StatusTextBox2.TabStop = False
        '
        'Clv_PromocionTextBox
        '
        Me.Clv_PromocionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTNETBindingSource, "Clv_Promocion", True))
        Me.Clv_PromocionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_PromocionTextBox.Location = New System.Drawing.Point(257, 284)
        Me.Clv_PromocionTextBox.Name = "Clv_PromocionTextBox"
        Me.Clv_PromocionTextBox.Size = New System.Drawing.Size(22, 21)
        Me.Clv_PromocionTextBox.TabIndex = 31
        Me.Clv_PromocionTextBox.TabStop = False
        Me.Clv_PromocionTextBox.Visible = False
        '
        'Clv_VendedorTextBox
        '
        Me.Clv_VendedorTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTNETBindingSource, "Clv_Vendedor", True))
        Me.Clv_VendedorTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_VendedorTextBox.Location = New System.Drawing.Point(228, 281)
        Me.Clv_VendedorTextBox.Name = "Clv_VendedorTextBox"
        Me.Clv_VendedorTextBox.Size = New System.Drawing.Size(22, 21)
        Me.Clv_VendedorTextBox.TabIndex = 29
        Me.Clv_VendedorTextBox.TabStop = False
        '
        'Panel5
        '
        Me.Panel5.AutoScroll = True
        Me.Panel5.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel5.Controls.Add(Me.txtMacCablModem)
        Me.Panel5.Controls.Add(Me.Button1)
        Me.Panel5.Controls.Add(Me.CheckBox1)
        Me.Panel5.Controls.Add(Me.NumericUpDown1)
        Me.Panel5.Controls.Add(Me.Label1)
        Me.Panel5.Controls.Add(Me.ObsTextBox1)
        Me.Panel5.Controls.Add(Me.ComboBox2)
        Me.Panel5.Controls.Add(Me.Label5)
        Me.Panel5.Controls.Add(Fecha_BajaLabel1)
        Me.Panel5.Controls.Add(Me.Fecha_BajaTextBox1)
        Me.Panel5.Controls.Add(Me.ContratoNetTextBox1)
        Me.Panel5.Controls.Add(Me.ContratoTextBox2)
        Me.Panel5.Controls.Add(Me.ComboBox4)
        Me.Panel5.Controls.Add(Me.ComboBox3)
        Me.Panel5.Controls.Add(Me.StatusTextBox1)
        Me.Panel5.Controls.Add(Me.CMBTextBox3)
        Me.Panel5.Controls.Add(SeRentaLabel)
        Me.Panel5.Controls.Add(Me.SeRentaCheckBox)
        Me.Panel5.Controls.Add(Me.CMBTextBox2)
        Me.Panel5.Controls.Add(ObsLabel1)
        Me.Panel5.Controls.Add(Tipo_CablemodemLabel)
        Me.Panel5.Controls.Add(Me.Tipo_CablemodemTextBox)
        Me.Panel5.Controls.Add(Fecha_TraspasoLabel)
        Me.Panel5.Controls.Add(Me.Fecha_TraspasoTextBox)
        Me.Panel5.Controls.Add(Fecha_SuspencionLabel)
        Me.Panel5.Controls.Add(Me.Fecha_SuspencionTextBox)
        Me.Panel5.Controls.Add(Fecha_ActivacionLabel)
        Me.Panel5.Controls.Add(Me.Fecha_ActivacionTextBox)
        Me.Panel5.Controls.Add(Clv_TipoServicioLabel)
        Me.Panel5.Controls.Add(Me.Clv_TipoServicioTextBox)
        Me.Panel5.Controls.Add(Ventacablemodem2Label)
        Me.Panel5.Controls.Add(Me.Ventacablemodem2CheckBox)
        Me.Panel5.Controls.Add(Ventacablemodem1Label)
        Me.Panel5.Controls.Add(Me.Ventacablemodem1CheckBox)
        Me.Panel5.Controls.Add(StatusLabel1)
        Me.Panel5.Controls.Add(CMBLabel5)
        Me.Panel5.Controls.Add(Me.TIPOAPARATOLabel1)
        Me.Panel5.Controls.Add(Me.MARCALabel1)
        Me.Panel5.Controls.Add(CMBLabel6)
        Me.Panel5.Controls.Add(CMBTIPOAPARATOLabel)
        Me.Panel5.Controls.Add(Me.BindingNavigator1)
        Me.Panel5.Controls.Add(Me.MACCABLEMODEMLabel1)
        Me.Panel5.Enabled = False
        Me.Panel5.Location = New System.Drawing.Point(3, 3)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(560, 341)
        Me.Panel5.TabIndex = 26
        Me.Panel5.TabStop = True
        '
        'txtMacCablModem
        '
        Me.txtMacCablModem.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtMacCablModem.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtMacCablModem.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMacCablModem.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VerAparatodelClienteBindingSource, "MACCABLEMODEM", True))
        Me.txtMacCablModem.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.txtMacCablModem.ForeColor = System.Drawing.Color.Navy
        Me.txtMacCablModem.Location = New System.Drawing.Point(145, 30)
        Me.txtMacCablModem.MaxLength = 12
        Me.txtMacCablModem.Multiline = True
        Me.txtMacCablModem.Name = "txtMacCablModem"
        Me.txtMacCablModem.Size = New System.Drawing.Size(229, 22)
        Me.txtMacCablModem.TabIndex = 63
        '
        'VerAparatodelClienteBindingSource
        '
        Me.VerAparatodelClienteBindingSource.DataMember = "VerAparatodelCliente"
        Me.VerAparatodelClienteBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.SystemColors.Control
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(392, 31)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(153, 23)
        Me.Button1.TabIndex = 62
        Me.Button1.Text = "Cambio de Cablemodem"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBox1.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.CONSULTACLIENTESNETBindingSource, "Es_Telefonia", True))
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CheckBox1.Location = New System.Drawing.Point(441, 57)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(94, 19)
        Me.CheckBox1.TabIndex = 61
        Me.CheckBox1.Text = "Telefonía :"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'CONSULTACLIENTESNETBindingSource
        '
        Me.CONSULTACLIENTESNETBindingSource.DataMember = "CONSULTACLIENTESNET"
        Me.CONSULTACLIENTESNETBindingSource.DataSource = Me.DataSetLidia2
        '
        'DataSetLidia2
        '
        Me.DataSetLidia2.DataSetName = "DataSetLidia2"
        Me.DataSetLidia2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.CONSULTACLIENTESNETBindingSource, "No_Extensiones", True))
        Me.NumericUpDown1.Location = New System.Drawing.Point(145, 195)
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(46, 20)
        Me.NumericUpDown1.TabIndex = 60
        Me.NumericUpDown1.Visible = False
        '
        'ObsTextBox1
        '
        Me.ObsTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACLIENTESNETBindingSource, "Obs", True))
        Me.ObsTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ObsTextBox1.Location = New System.Drawing.Point(29, 235)
        Me.ObsTextBox1.MaxLength = 250
        Me.ObsTextBox1.Multiline = True
        Me.ObsTextBox1.Name = "ObsTextBox1"
        Me.ObsTextBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.ObsTextBox1.Size = New System.Drawing.Size(287, 51)
        Me.ObsTextBox1.TabIndex = 26
        '
        'ComboBox2
        '
        Me.ComboBox2.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONSULTACLIENTESNETBindingSource, "Status", True))
        Me.ComboBox2.DataSource = Me.StatusCableModemBindingSource
        Me.ComboBox2.DisplayMember = "Concepto"
        Me.ComboBox2.Enabled = False
        Me.ComboBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(145, 112)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(156, 23)
        Me.ComboBox2.TabIndex = 50
        Me.ComboBox2.TabStop = False
        Me.ComboBox2.ValueMember = "Clv_StatusCableModem"
        '
        'StatusCableModemBindingSource
        '
        Me.StatusCableModemBindingSource.DataMember = "StatusCableModem"
        Me.StatusCableModemBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Gold
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Red
        Me.Label5.Location = New System.Drawing.Point(30, 298)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(273, 15)
        Me.Label5.TabIndex = 58
        Me.Label5.Text = """El Servicio es de Prueba y Vence el Dia """
        Me.Label5.Visible = False
        '
        'Fecha_BajaTextBox1
        '
        Me.Fecha_BajaTextBox1.BackColor = System.Drawing.Color.White
        Me.Fecha_BajaTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Fecha_BajaTextBox1.CausesValidation = False
        Me.Fecha_BajaTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACLIENTESNETBindingSource, "Fecha_Baja", True))
        Me.Fecha_BajaTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fecha_BajaTextBox1.Location = New System.Drawing.Point(431, 188)
        Me.Fecha_BajaTextBox1.Name = "Fecha_BajaTextBox1"
        Me.Fecha_BajaTextBox1.ReadOnly = True
        Me.Fecha_BajaTextBox1.Size = New System.Drawing.Size(100, 21)
        Me.Fecha_BajaTextBox1.TabIndex = 55
        Me.Fecha_BajaTextBox1.TabStop = False
        '
        'ContratoNetTextBox1
        '
        Me.ContratoNetTextBox1.Location = New System.Drawing.Point(242, 247)
        Me.ContratoNetTextBox1.Name = "ContratoNetTextBox1"
        Me.ContratoNetTextBox1.Size = New System.Drawing.Size(41, 20)
        Me.ContratoNetTextBox1.TabIndex = 54
        Me.ContratoNetTextBox1.TabStop = False
        Me.ContratoNetTextBox1.Visible = False
        '
        'ContratoTextBox2
        '
        Me.ContratoTextBox2.Location = New System.Drawing.Point(260, 240)
        Me.ContratoTextBox2.Name = "ContratoTextBox2"
        Me.ContratoTextBox2.Size = New System.Drawing.Size(41, 20)
        Me.ContratoTextBox2.TabIndex = 53
        Me.ContratoTextBox2.TabStop = False
        Me.ContratoTextBox2.Visible = False
        '
        'ComboBox4
        '
        Me.ComboBox4.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONSULTACLIENTESNETBindingSource, "Tipo_Cablemodem", True))
        Me.ComboBox4.DataSource = Me.TipoCablemodemBindingSource
        Me.ComboBox4.DisplayMember = "Concepto"
        Me.ComboBox4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox4.FormattingEnabled = True
        Me.ComboBox4.Location = New System.Drawing.Point(145, 167)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(155, 23)
        Me.ComboBox4.TabIndex = 52
        Me.ComboBox4.TabStop = False
        Me.ComboBox4.ValueMember = "Clv_TipoCablemodem"
        Me.ComboBox4.Visible = False
        '
        'TipoCablemodemBindingSource
        '
        Me.TipoCablemodemBindingSource.DataMember = "TipoCablemodem"
        Me.TipoCablemodemBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'ComboBox3
        '
        Me.ComboBox3.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONSULTACLIENTESNETBindingSource, "Clv_TipoServicio", True))
        Me.ComboBox3.DataSource = Me.MuestraTipSerInternetBindingSource
        Me.ComboBox3.DisplayMember = "Concepto"
        Me.ComboBox3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox3.FormattingEnabled = True
        Me.ComboBox3.Location = New System.Drawing.Point(145, 139)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(155, 23)
        Me.ComboBox3.TabIndex = 51
        Me.ComboBox3.TabStop = False
        Me.ComboBox3.ValueMember = "Clv_TipSerInternet"
        '
        'MuestraTipSerInternetBindingSource
        '
        Me.MuestraTipSerInternetBindingSource.DataMember = "MuestraTipSerInternet"
        Me.MuestraTipSerInternetBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'StatusTextBox1
        '
        Me.StatusTextBox1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.StatusTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.StatusTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusTextBox1.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.StatusTextBox1.Location = New System.Drawing.Point(216, 115)
        Me.StatusTextBox1.Name = "StatusTextBox1"
        Me.StatusTextBox1.Size = New System.Drawing.Size(38, 14)
        Me.StatusTextBox1.TabIndex = 8
        Me.StatusTextBox1.TabStop = False
        '
        'CMBTextBox3
        '
        Me.CMBTextBox3.BackColor = System.Drawing.Color.Black
        Me.CMBTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox3.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox3.Location = New System.Drawing.Point(346, 228)
        Me.CMBTextBox3.Name = "CMBTextBox3"
        Me.CMBTextBox3.ReadOnly = True
        Me.CMBTextBox3.Size = New System.Drawing.Size(189, 19)
        Me.CMBTextBox3.TabIndex = 48
        Me.CMBTextBox3.TabStop = False
        Me.CMBTextBox3.Text = "Venta de Cablemodem"
        Me.CMBTextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.CMBTextBox3.Visible = False
        '
        'SeRentaCheckBox
        '
        Me.SeRentaCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONSULTACLIENTESNETBindingSource, "SeRenta", True))
        Me.SeRentaCheckBox.Location = New System.Drawing.Point(488, 300)
        Me.SeRentaCheckBox.Name = "SeRentaCheckBox"
        Me.SeRentaCheckBox.Size = New System.Drawing.Size(16, 24)
        Me.SeRentaCheckBox.TabIndex = 47
        Me.SeRentaCheckBox.TabStop = False
        Me.SeRentaCheckBox.Visible = False
        '
        'CMBTextBox2
        '
        Me.CMBTextBox2.BackColor = System.Drawing.Color.Black
        Me.CMBTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox2.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox2.Location = New System.Drawing.Point(346, 81)
        Me.CMBTextBox2.Name = "CMBTextBox2"
        Me.CMBTextBox2.ReadOnly = True
        Me.CMBTextBox2.Size = New System.Drawing.Size(189, 19)
        Me.CMBTextBox2.TabIndex = 46
        Me.CMBTextBox2.TabStop = False
        Me.CMBTextBox2.Text = "Fechas de "
        Me.CMBTextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Tipo_CablemodemTextBox
        '
        Me.Tipo_CablemodemTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Tipo_CablemodemTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Tipo_CablemodemTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Tipo_CablemodemTextBox.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Tipo_CablemodemTextBox.Location = New System.Drawing.Point(250, 167)
        Me.Tipo_CablemodemTextBox.Name = "Tipo_CablemodemTextBox"
        Me.Tipo_CablemodemTextBox.Size = New System.Drawing.Size(31, 13)
        Me.Tipo_CablemodemTextBox.TabIndex = 22
        Me.Tipo_CablemodemTextBox.TabStop = False
        '
        'Fecha_TraspasoTextBox
        '
        Me.Fecha_TraspasoTextBox.BackColor = System.Drawing.Color.White
        Me.Fecha_TraspasoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Fecha_TraspasoTextBox.CausesValidation = False
        Me.Fecha_TraspasoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACLIENTESNETBindingSource, "Fecha_Traspaso", True))
        Me.Fecha_TraspasoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fecha_TraspasoTextBox.Location = New System.Drawing.Point(431, 161)
        Me.Fecha_TraspasoTextBox.Name = "Fecha_TraspasoTextBox"
        Me.Fecha_TraspasoTextBox.ReadOnly = True
        Me.Fecha_TraspasoTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Fecha_TraspasoTextBox.TabIndex = 20
        Me.Fecha_TraspasoTextBox.TabStop = False
        '
        'Fecha_SuspencionTextBox
        '
        Me.Fecha_SuspencionTextBox.BackColor = System.Drawing.Color.White
        Me.Fecha_SuspencionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Fecha_SuspencionTextBox.CausesValidation = False
        Me.Fecha_SuspencionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fecha_SuspencionTextBox.Location = New System.Drawing.Point(431, 135)
        Me.Fecha_SuspencionTextBox.Name = "Fecha_SuspencionTextBox"
        Me.Fecha_SuspencionTextBox.ReadOnly = True
        Me.Fecha_SuspencionTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Fecha_SuspencionTextBox.TabIndex = 18
        Me.Fecha_SuspencionTextBox.TabStop = False
        '
        'Fecha_ActivacionTextBox
        '
        Me.Fecha_ActivacionTextBox.BackColor = System.Drawing.Color.White
        Me.Fecha_ActivacionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Fecha_ActivacionTextBox.CausesValidation = False
        Me.Fecha_ActivacionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACLIENTESNETBindingSource, "Fecha_Activacion", True))
        Me.Fecha_ActivacionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fecha_ActivacionTextBox.Location = New System.Drawing.Point(431, 109)
        Me.Fecha_ActivacionTextBox.Name = "Fecha_ActivacionTextBox"
        Me.Fecha_ActivacionTextBox.ReadOnly = True
        Me.Fecha_ActivacionTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Fecha_ActivacionTextBox.TabIndex = 16
        Me.Fecha_ActivacionTextBox.TabStop = False
        '
        'Clv_TipoServicioTextBox
        '
        Me.Clv_TipoServicioTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TipoServicioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_TipoServicioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_TipoServicioTextBox.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TipoServicioTextBox.Location = New System.Drawing.Point(216, 149)
        Me.Clv_TipoServicioTextBox.Name = "Clv_TipoServicioTextBox"
        Me.Clv_TipoServicioTextBox.Size = New System.Drawing.Size(31, 14)
        Me.Clv_TipoServicioTextBox.TabIndex = 14
        Me.Clv_TipoServicioTextBox.TabStop = False
        '
        'Ventacablemodem2CheckBox
        '
        Me.Ventacablemodem2CheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONSULTACLIENTESNETBindingSource, "ventacablemodem2", True))
        Me.Ventacablemodem2CheckBox.Location = New System.Drawing.Point(488, 276)
        Me.Ventacablemodem2CheckBox.Name = "Ventacablemodem2CheckBox"
        Me.Ventacablemodem2CheckBox.Size = New System.Drawing.Size(16, 24)
        Me.Ventacablemodem2CheckBox.TabIndex = 12
        Me.Ventacablemodem2CheckBox.TabStop = False
        Me.Ventacablemodem2CheckBox.Visible = False
        '
        'Ventacablemodem1CheckBox
        '
        Me.Ventacablemodem1CheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONSULTACLIENTESNETBindingSource, "ventacablemodem1", True))
        Me.Ventacablemodem1CheckBox.Location = New System.Drawing.Point(488, 255)
        Me.Ventacablemodem1CheckBox.Name = "Ventacablemodem1CheckBox"
        Me.Ventacablemodem1CheckBox.Size = New System.Drawing.Size(16, 24)
        Me.Ventacablemodem1CheckBox.TabIndex = 10
        Me.Ventacablemodem1CheckBox.TabStop = False
        Me.Ventacablemodem1CheckBox.Visible = False
        '
        'TIPOAPARATOLabel1
        '
        Me.TIPOAPARATOLabel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TIPOAPARATOLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VerAparatodelClienteBindingSource, "TIPOAPARATO", True))
        Me.TIPOAPARATOLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TIPOAPARATOLabel1.Location = New System.Drawing.Point(145, 86)
        Me.TIPOAPARATOLabel1.Name = "TIPOAPARATOLabel1"
        Me.TIPOAPARATOLabel1.Size = New System.Drawing.Size(123, 23)
        Me.TIPOAPARATOLabel1.TabIndex = 5
        '
        'MARCALabel1
        '
        Me.MARCALabel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.MARCALabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VerAparatodelClienteBindingSource, "MARCA", True))
        Me.MARCALabel1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.MARCALabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MARCALabel1.Location = New System.Drawing.Point(145, 59)
        Me.MARCALabel1.Name = "MARCALabel1"
        Me.MARCALabel1.Size = New System.Drawing.Size(123, 23)
        Me.MARCALabel1.TabIndex = 3
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Nothing
        Me.BindingNavigator1.CountItem = Nothing
        Me.BindingNavigator1.DeleteItem = Nothing
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripSeparator3, Me.ToolStripButton5, Me.ToolStripSeparator4, Me.ToolStripButton6})
        Me.BindingNavigator1.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigator1.MoveFirstItem = Nothing
        Me.BindingNavigator1.MoveLastItem = Nothing
        Me.BindingNavigator1.MoveNextItem = Nothing
        Me.BindingNavigator1.MovePreviousItem = Nothing
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Nothing
        Me.BindingNavigator1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.BindingNavigator1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.BindingNavigator1.Size = New System.Drawing.Size(560, 25)
        Me.BindingNavigator1.TabIndex = 27
        Me.BindingNavigator1.TabStop = True
        Me.BindingNavigator1.Text = "BindingNavigator1"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripButton5
        '
        Me.ToolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton5.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton5.Name = "ToolStripButton5"
        Me.ToolStripButton5.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton5.Size = New System.Drawing.Size(76, 22)
        Me.ToolStripButton5.Text = "CA&NCELAR"
        Me.ToolStripButton5.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripButton6
        '
        Me.ToolStripButton6.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton6.Image = CType(resources.GetObject("ToolStripButton6.Image"), System.Drawing.Image)
        Me.ToolStripButton6.Name = "ToolStripButton6"
        Me.ToolStripButton6.Size = New System.Drawing.Size(88, 22)
        Me.ToolStripButton6.Text = "GUA&RDAR"
        '
        'MACCABLEMODEMLabel1
        '
        Me.MACCABLEMODEMLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VerAparatodelClienteBindingSource, "MACCABLEMODEM", True))
        Me.MACCABLEMODEMLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MACCABLEMODEMLabel1.ForeColor = System.Drawing.Color.Navy
        Me.MACCABLEMODEMLabel1.Location = New System.Drawing.Point(141, 30)
        Me.MACCABLEMODEMLabel1.Name = "MACCABLEMODEMLabel1"
        Me.MACCABLEMODEMLabel1.Size = New System.Drawing.Size(233, 23)
        Me.MACCABLEMODEMLabel1.TabIndex = 1
        '
        'Procedimientosarnoldo4
        '
        Me.Procedimientosarnoldo4.DataSetName = "Procedimientosarnoldo4"
        Me.Procedimientosarnoldo4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Inserta_Rel_cortesia_FechaBindingSource
        '
        Me.Inserta_Rel_cortesia_FechaBindingSource.DataMember = "Inserta_Rel_cortesia_Fecha"
        Me.Inserta_Rel_cortesia_FechaBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Inserta_Rel_cortesia_FechaTableAdapter
        '
        Me.Inserta_Rel_cortesia_FechaTableAdapter.ClearBeforeFill = True
        '
        'CONRel_ContNet_UsuariosTableAdapter
        '
        Me.CONRel_ContNet_UsuariosTableAdapter.ClearBeforeFill = True
        '
        'ConRelCteDescuentoTableAdapter
        '
        Me.ConRelCteDescuentoTableAdapter.ClearBeforeFill = True
        '
        'ChecaRelCteDescuentoBindingSource
        '
        Me.ChecaRelCteDescuentoBindingSource.DataMember = "ChecaRelCteDescuento"
        Me.ChecaRelCteDescuentoBindingSource.DataSource = Me.DataSetEric
        '
        'ChecaRelCteDescuentoTableAdapter
        '
        Me.ChecaRelCteDescuentoTableAdapter.ClearBeforeFill = True
        '
        'MuestraPromotoresNetTableAdapter
        '
        Me.MuestraPromotoresNetTableAdapter.ClearBeforeFill = True
        '
        'GUARDARRel_ContNet_UsuariosBindingSource
        '
        Me.GUARDARRel_ContNet_UsuariosBindingSource.DataMember = "GUARDARRel_ContNet_Usuarios"
        Me.GUARDARRel_ContNet_UsuariosBindingSource.DataSource = Me.DataSetEDGAR
        '
        'GUARDARRel_ContNet_UsuariosTableAdapter
        '
        Me.GUARDARRel_ContNet_UsuariosTableAdapter.ClearBeforeFill = True
        '
        'CONSULTACONTNETTableAdapter
        '
        Me.CONSULTACONTNETTableAdapter.ClearBeforeFill = True
        '
        'VerAparatodelClienteTableAdapter
        '
        Me.VerAparatodelClienteTableAdapter.ClearBeforeFill = True
        '
        'DataSetLidia
        '
        Me.DataSetLidia.DataSetName = "DataSetLidia"
        Me.DataSetLidia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DameFechaHabilitarBindingSource
        '
        Me.DameFechaHabilitarBindingSource.DataMember = "DameFechaHabilitar"
        Me.DameFechaHabilitarBindingSource.DataSource = Me.DataSetLidia
        '
        'DameFechaHabilitarTableAdapter
        '
        Me.DameFechaHabilitarTableAdapter.ClearBeforeFill = True
        '
        'StatusNetTableAdapter
        '
        Me.StatusNetTableAdapter.ClearBeforeFill = True
        '
        'StatusCableModemTableAdapter
        '
        Me.StatusCableModemTableAdapter.ClearBeforeFill = True
        '
        'TipoCablemodemTableAdapter
        '
        Me.TipoCablemodemTableAdapter.ClearBeforeFill = True
        '
        'MuestraTipSerInternetTableAdapter
        '
        Me.MuestraTipSerInternetTableAdapter.ClearBeforeFill = True
        '
        'HABILITACABLEMODEMBindingSource
        '
        Me.HABILITACABLEMODEMBindingSource.DataMember = "HABILITACABLEMODEM"
        Me.HABILITACABLEMODEMBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'HABILITACABLEMODEMTableAdapter
        '
        Me.HABILITACABLEMODEMTableAdapter.ClearBeforeFill = True
        '
        'PrimerMesCLIENTESBindingSource
        '
        Me.PrimerMesCLIENTESBindingSource.DataMember = "PrimerMesCLIENTES"
        Me.PrimerMesCLIENTESBindingSource.DataSource = Me.DataSetLidia
        '
        'PrimerMesCLIENTESTableAdapter
        '
        Me.PrimerMesCLIENTESTableAdapter.ClearBeforeFill = True
        '
        'CONSULTACLIENTESNETTableAdapter
        '
        Me.CONSULTACLIENTESNETTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter5
        '
        Me.Muestra_ServiciosDigitalesTableAdapter5.ClearBeforeFill = True
        '
        'FrmInternet
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(555, 337)
        Me.Controls.Add(Me.Panel6)
        Me.Controls.Add(Me.Panel5)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Location = New System.Drawing.Point(445, 330)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmInternet"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Servicio Internet"
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        CType(Me.ConRelCteDescuentoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONSULTACONTNETBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusNetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONRel_ContNet_UsuariosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingNavigator2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator2.ResumeLayout(False)
        Me.BindingNavigator2.PerformLayout()
        CType(Me.MuestraPromotoresNetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.VerAparatodelClienteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONSULTACLIENTESNETBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusCableModemBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TipoCablemodemBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTipSerInternetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Rel_cortesia_FechaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChecaRelCteDescuentoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GUARDARRel_ContNet_UsuariosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameFechaHabilitarBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.HABILITACABLEMODEMBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PrimerMesCLIENTESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Button26 As System.Windows.Forms.Button
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents DescuentoLabel3 As System.Windows.Forms.Label
    Friend WithEvents ComboBox5 As System.Windows.Forms.ComboBox
    Friend WithEvents Button23 As System.Windows.Forms.Button
    Friend WithEvents ObsTextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents NOMBRELabel4 As System.Windows.Forms.Label
    Friend WithEvents Button18 As System.Windows.Forms.Button
    Friend WithEvents CortesiaCheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents BindingNavigator2 As System.Windows.Forms.BindingNavigator
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripButton7 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripButton8 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ComboBox6 As System.Windows.Forms.ComboBox
    Friend WithEvents CMBTextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents DESCRIPCIONLabel1 As System.Windows.Forms.Label
    Friend WithEvents EmailTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents FacturaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Ultimo_anioTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Ultimo_mesTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents PrimerMensualidadCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents FECHA_ULT_PAGOTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Fecha_Fuera_AreaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Fecha_bajaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Fecha_suspensionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Fecha_instalacioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Fecha_solicitudTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Clv_ServicioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ContratoNetTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_UnicaNetTextBox As System.Windows.Forms.TextBox
    Friend WithEvents StatusTextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Clv_PromocionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_VendedorTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents ObsTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Fecha_BajaTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents ContratoNetTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents ContratoTextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox4 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
    Friend WithEvents StatusTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents CMBTextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents SeRentaCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents CMBTextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Tipo_CablemodemTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Fecha_TraspasoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Fecha_SuspencionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Fecha_ActivacionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_TipoServicioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Ventacablemodem2CheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents Ventacablemodem1CheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents MACCABLEMODEMLabel1 As System.Windows.Forms.Label
    Friend WithEvents TIPOAPARATOLabel1 As System.Windows.Forms.Label
    Friend WithEvents MARCALabel1 As System.Windows.Forms.Label
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripButton5 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripButton6 As System.Windows.Forms.ToolStripButton
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONSULTACONTNETBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONSULTACONTNETTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONSULTACONTNETTableAdapter
    Friend WithEvents Procedimientosarnoldo4 As sofTV.Procedimientosarnoldo4
    Friend WithEvents Inserta_Rel_cortesia_FechaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Rel_cortesia_FechaTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Inserta_Rel_cortesia_FechaTableAdapter
    Friend WithEvents VerAparatodelClienteBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents VerAparatodelClienteTableAdapter As sofTV.NewSofTvDataSetTableAdapters.VerAparatodelClienteTableAdapter
    Friend WithEvents DataSetLidia As sofTV.DataSetLidia
    Friend WithEvents DameFechaHabilitarBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameFechaHabilitarTableAdapter As sofTV.DataSetLidiaTableAdapters.DameFechaHabilitarTableAdapter
    Friend WithEvents DataSetEDGAR As sofTV.DataSetEDGAR
    Friend WithEvents CONRel_ContNet_UsuariosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONRel_ContNet_UsuariosTableAdapter As sofTV.DataSetEDGARTableAdapters.CONRel_ContNet_UsuariosTableAdapter
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents ConRelCteDescuentoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConRelCteDescuentoTableAdapter As sofTV.DataSetEricTableAdapters.ConRelCteDescuentoTableAdapter
    Friend WithEvents ChecaRelCteDescuentoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ChecaRelCteDescuentoTableAdapter As sofTV.DataSetEricTableAdapters.ChecaRelCteDescuentoTableAdapter
    Friend WithEvents StatusNetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents StatusNetTableAdapter As sofTV.NewSofTvDataSetTableAdapters.StatusNetTableAdapter
    Friend WithEvents MuestraPromotoresNetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraPromotoresNetTableAdapter As sofTV.DataSetEDGARTableAdapters.MuestraPromotoresNetTableAdapter
    Friend WithEvents StatusCableModemBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents StatusCableModemTableAdapter As sofTV.NewSofTvDataSetTableAdapters.StatusCableModemTableAdapter
    Friend WithEvents TipoCablemodemBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TipoCablemodemTableAdapter As sofTV.NewSofTvDataSetTableAdapters.TipoCablemodemTableAdapter
    Friend WithEvents MuestraTipSerInternetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipSerInternetTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MuestraTipSerInternetTableAdapter
    Friend WithEvents GUARDARRel_ContNet_UsuariosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GUARDARRel_ContNet_UsuariosTableAdapter As sofTV.DataSetEDGARTableAdapters.GUARDARRel_ContNet_UsuariosTableAdapter
    Friend WithEvents HABILITACABLEMODEMBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents HABILITACABLEMODEMTableAdapter As sofTV.NewSofTvDataSetTableAdapters.HABILITACABLEMODEMTableAdapter
    Friend WithEvents PrimerMesCLIENTESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PrimerMesCLIENTESTableAdapter As sofTV.DataSetLidiaTableAdapters.PrimerMesCLIENTESTableAdapter
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents DataSetLidia2 As sofTV.DataSetLidia2
    Friend WithEvents CONSULTACLIENTESNETBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONSULTACLIENTESNETTableAdapter As sofTV.DataSetLidia2TableAdapters.CONSULTACLIENTESNETTableAdapter
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents txtMacCablModem As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents LblPlazoForzoso1 As System.Windows.Forms.Label
    Friend WithEvents LblPlazoForzoso As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents TipoCortesiaComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter5 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
