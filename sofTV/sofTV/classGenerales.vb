﻿Public Class classGenerales

#Region "PROPIEDADES"
    Private _imprimeOrdenesSoftv As Boolean
    Public Property imprimeOrdenesSoftv As Boolean
        Get
            Return _imprimeOrdenesSoftv
        End Get
        Set(value As Boolean)
            _imprimeOrdenesSoftv = value
        End Set
    End Property

    Private _imprimeOrdenesFac As Boolean
    Public Property imprimeOrdenesFac As Boolean
        Get
            Return _imprimeOrdenesFac
        End Get
        Set(value As Boolean)
            _imprimeOrdenesFac = value
        End Set
    End Property

    Private _imprimeQuejasSoftv As Boolean
    Public Property imprimeQuejasSoftv As Boolean
        Get
            Return _imprimeQuejasSoftv
        End Get
        Set(value As Boolean)
            _imprimeQuejasSoftv = VALUE
        End Set
    End Property

#End Region

#Region "CONSTRUCTORES"
    Public Sub New()
        _imprimeOrdenesSoftv = 0
        _imprimeOrdenesFac = 0
        _imprimeQuejasSoftv = 0
    End Sub
#End Region

#Region "MÉTODOS"

    Public Sub uspInsertaTblImpresionOrdenQueja()
        Try
            Dim impresionOrdenQueja As New BaseIII

            impresionOrdenQueja.limpiaParametros()
            impresionOrdenQueja.CreateMyParameter("@imprimeOrdenesSoftv", SqlDbType.Bit, CObj(_imprimeOrdenesSoftv))
            impresionOrdenQueja.CreateMyParameter("@imprimeOrdenesFact", SqlDbType.Bit, CObj(_imprimeOrdenesFac))
            impresionOrdenQueja.CreateMyParameter("@imprimeQuejasSoftv", SqlDbType.Bit, CObj(_imprimeQuejasSoftv))
            impresionOrdenQueja.Inserta("uspInsertaTblImpresionOrdenQueja")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub uspConsultaTblImpresionOrdenQueja()
        Try
            Dim impresionOrdenQueja As New BaseIII
            Dim dtOrdenQueja As New DataTable
            Dim rowOrdenQueja As DataRow

            impresionOrdenQueja.limpiaParametros()
            dtOrdenQueja = impresionOrdenQueja.ConsultaDT("uspConsultaTblImpresionOrdenQueja")

            For Each rowOrdenQueja In dtOrdenQueja.Rows
                _imprimeOrdenesSoftv = CBool(rowOrdenQueja("imprimeOrdenesSoftv").ToString())
                _imprimeOrdenesFac = CBool(rowOrdenQueja("imprimeOrdenesFac").ToString())
                _imprimeQuejasSoftv = CBool(rowOrdenQueja("imprimeQuejasSoftv").ToString)
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region
End Class
