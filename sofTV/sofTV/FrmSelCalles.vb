Imports System.Data.SqlClient

Public Class FrmSelCalles

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'Dim x As Integer = 0
        'Dim y As Integer
        'x = Me.ListBox2.Items.Count()
        'If x > 0 Then
        '    For y = 0 To (x - 1)
        '        Me.ListBox2.SelectedIndex = y
        '        If (Me.ListBox1.Text = Me.ListBox2.Text) Then
        '            MsgBox("La Calle ya esta en la lista", MsgBoxStyle.Information)
        '            Exit Sub
        '            'Else
        '            '    Me.ListBox2.Items.Add(Me.ListBox1.Text)
        '            '    Me.ListBox3.Items.Add(Me.ListBox1.SelectedValue.ToString)
        '            '    Exit Sub
        '        End If
        '    Next
        '    Me.ListBox2.Items.Add(Me.ListBox1.Text)
        '    Me.ListBox3.Items.Add(Me.ListBox1.SelectedValue.ToString)
        'Else
        '    Me.ListBox2.Items.Add(Me.ListBox1.Text)
        '    Me.ListBox3.Items.Add(Me.ListBox1.SelectedValue.ToString)
        'End If
        Me.Insertauno_Seleccion_CallesTableAdapter.Connection = CON
        Me.Insertauno_Seleccion_CallesTableAdapter.Fill(Me.ProcedimientosArnoldo2.Insertauno_Seleccion_Calles, LocClv_session, CInt(Me.ListBox1.SelectedValue))
        Me.MuestraSelecciona_CalleTmpCONSULTATableAdapter.Connection = CON
        Me.MuestraSelecciona_CalleTmpCONSULTATableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSelecciona_CalleTmpCONSULTA, LocClv_session)
        Me.MuestraSeleccion_CalleCONSULTATableAdapter.Connection = CON
        Me.MuestraSeleccion_CalleCONSULTATableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSeleccion_CalleCONSULTA, LocClv_session)
        CON.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Dim x As Integer
        'Dim y As Integer
        'x = Me.ListBox1.Items.Count()
        'If Me.ListBox2.Items.Count() > 0 Then
        '    MsgBox("Primero Borre las Calles Seleccionadas", MsgBoxStyle.Information)
        'Else
        '    Me.ListBox1.SelectedIndex = 0
        '    For y = 1 To x
        '        Me.ListBox1.SelectedIndex = (y - 1)
        '        Me.ListBox2.Items.Add(Me.ListBox1.Text)
        '        Me.ListBox3.Items.Add(Me.ListBox1.SelectedValue.ToString)
        '    Next
        'End If
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.InsertaTOSelecciona_CallesTableAdapter.Connection = CON
        Me.InsertaTOSelecciona_CallesTableAdapter.Fill(Me.ProcedimientosArnoldo2.InsertaTOSelecciona_Calles, LocClv_session)
        Me.MuestraSelecciona_CalleTmpCONSULTATableAdapter.Connection = CON
        Me.MuestraSelecciona_CalleTmpCONSULTATableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSelecciona_CalleTmpCONSULTA, LocClv_session)
        Me.MuestraSeleccion_CalleCONSULTATableAdapter.Connection = CON
        Me.MuestraSeleccion_CalleCONSULTATableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSeleccion_CalleCONSULTA, LocClv_session)
        CON.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'If (Me.ListBox2.SelectedIndex <> -1) Then
        '    Me.ListBox3.SelectedIndex = Me.ListBox2.SelectedIndex
        '    Me.ListBox2.Items.RemoveAt(Me.ListBox2.SelectedIndex)
        '    Me.ListBox3.Items.RemoveAt(Me.ListBox3.SelectedIndex)
        'Else
        '    MsgBox("Selecciona primero un valor a borrar")
        'End If
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Insertauno_Seleccion_CallestmpTableAdapter.Connection = CON
        Me.Insertauno_Seleccion_CallestmpTableAdapter.Fill(Me.ProcedimientosArnoldo2.Insertauno_Seleccion_Callestmp, LocClv_session, CInt(Me.ListBox2.SelectedValue))
        Me.MuestraSelecciona_CalleTmpCONSULTATableAdapter.Connection = CON
        Me.MuestraSelecciona_CalleTmpCONSULTATableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSelecciona_CalleTmpCONSULTA, LocClv_session)
        Me.MuestraSeleccion_CalleCONSULTATableAdapter.Connection = CON
        Me.MuestraSeleccion_CalleCONSULTATableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSeleccion_CalleCONSULTA, LocClv_session)
        CON.Close()

    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'Dim x As Integer
        'Dim y As Integer
        'x = Me.ListBox2.Items.Count()
        'For y = 0 To (x - 1)
        '    Me.ListBox2.Items.RemoveAt(0)
        '    Me.ListBox3.Items.RemoveAt(0)
        'Next
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.InsertaTOSelecciona_CallesTmpTableAdapter.Connection = CON
        Me.InsertaTOSelecciona_CallesTmpTableAdapter.Fill(Me.ProcedimientosArnoldo2.InsertaTOSelecciona_CallesTmp, LocClv_session)
        Me.MuestraSelecciona_CalleTmpCONSULTATableAdapter.Connection = CON
        Me.MuestraSelecciona_CalleTmpCONSULTATableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSelecciona_CalleTmpCONSULTA, LocClv_session)
        Me.MuestraSeleccion_CalleCONSULTATableAdapter.Connection = CON
        Me.MuestraSeleccion_CalleCONSULTATableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSeleccion_CalleCONSULTA, LocClv_session)
        CON.Close()
    End Sub

    Private Sub FrmSelCalles_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        'Me.Muestra_Calles_AsociadasTableAdapter.Fill(Me.DataSetarnoldo.Muestra_Calles_Asociadas, LocClv_session)
        Me.MuestraSeleccion_CallesTmpNUEVOTableAdapter.Connection = CON
        Me.MuestraSeleccion_CallesTmpNUEVOTableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSeleccion_CallesTmpNUEVO, LocClv_session)
        Me.MuestraSelecciona_CalleTmpCONSULTATableAdapter.Connection = CON
        Me.MuestraSelecciona_CalleTmpCONSULTATableAdapter.Fill(Me.ProcedimientosArnoldo2.MuestraSelecciona_CalleTmpCONSULTA, LocClv_session)
        CON.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Dim x As Integer = 0
        x = Me.ListBox2.Items.Count()
        Dim bandera As Boolean = sms
        'For y = 0 To (x - 1)
        '    Me.ListBox3.SelectedIndex = y
        '    Me.Inserta_Sel_CallesTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Sel_Calles, LocClv_session, CInt(Me.ListBox3.Text))
        'Next
        'Locbndpen1 = True
        If x = 0 Then
            MsgBox("Seleccione Al Menos una Calle", MsgBoxStyle.Information)
        ElseIf x > 0 Then
            If LocOp = 2000 And LocMasivaCapar = True Then
                LocMasivaCapar = False
                LocOp = 0
                FrmMuestraCantidadesGenerar.Show()
            End If
            If bandera = True Then
                FrmSms.Show()
                Me.Close()
                Return
            End If
            If LocOp = 30 Then
                FrmSelEdoReportes2.Show()
            ElseIf LocOp = 22 And eBndMenIns = True Then
                eBndMenIns = False
                FrmSelEstado_Mensajes.Show()
            ElseIf LocOp = 23 Then
                FrmCorreo.Show()
            ElseIf LocOp = 20 Or LocOp = 25 Then
                My.Forms.FrmSelEstado.Show()
            ElseIf LocOp = 22 And eBndMenIns = False Then
                FrmSelBanco.Show()
            End If
            If LEdo_Cuenta = True Or LEdo_Cuenta2 = True Then
                FrmEstado_Cuenta.Show()
            End If
            If LocOp = 90 Then
                If LocValidaHab = 0 Then
                    FrmSelPeriodo.Show()
                ElseIf LocValidaHab = 1 Then
                    FrmOpcionRepCombos.Show()
                End If
            End If
        End If
        Me.Close()

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        LEdo_Cuenta = False
        LEdo_Cuenta2 = False
        Me.Close()
    End Sub




End Class