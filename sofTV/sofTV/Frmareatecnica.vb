
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.Data.SqlClient
Imports System.Collections.Generic

Public Class Frmareatecnica
    Delegate Sub Reporte(ByVal op As String, ByVal Titulop As String)
    Public customersByCityReport As ReportDocument
    Dim op As String = Nothing
    Public bnd As Boolean = False
    Public bnd2 As Boolean = False
    Dim Titulo As String = Nothing
    Private opreporte As Integer = 0
    Dim NumeroVisitas As Integer
    Dim GenRepBlanco As Boolean = False

    Private Function ColocarCodigo() As DataSet
        Dim max As Long
        Dim LocError As Integer
        Dim locimporte As Decimal
        Dim DT As New DataTable
        Dim DS As New DataSet
        Dim oContrato As Long
        Dim prmOp As Integer
        Dim I As Integer
        prmOp = 0
        DS.Clear()
        DT.Clear()

        BaseII.limpiaParametros()
        DT = BaseII.ConsultaDT("UspChecarContratos")

        For I = 0 To DT.Rows.Count - 1
            oContrato = CLng(DT.Rows(I)(0).ToString)
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, oContrato)
            BaseII.CreateMyParameter("@Op", SqlDbType.Int, prmOp)
            Dim listatablas As New List(Of String)
            listatablas.Add("PREFacturas_EDO_SOFTV_OXXO_PrimerMensualidad")
            DS = BaseII.ConsultaDS("Genera_Codigo_OXXO_PorContrato_PrimerMensualidad", listatablas)
            Dim rd As ReportDocument
            Dim X As Long = 0
            Dim contador As Integer = 1
            max = DS.Tables(0).Rows.Count
            Dim prefijo As String = ""
            Dim Txt As String = Nothing

            For Each dr As DataRow In DS.Tables(0).Rows
                X = Long.Parse(dr(0).ToString())
                Txt = dr(10).ToString()
                If Len(Txt) > 0 Then
                    AgregaImgCodeBarEstadoDeCuentaTVPrimerMensualidad(oContrato, Txt, 0)
                End If

            Next
        Next
        BaseII.limpiaParametros()
        Dim lista As New List(Of String)
        lista.Add("ReporteAreaTecnicaOrdSer")
        lista.Add("DameDatosGenerales_2")
        lista.Add("Cosulta_OrdRetiro")
        lista.Add("TablaDatos")
        lista.Add("tabla5Queja")
        lista.Add("tblUltOrdQueja")
        lista.Add("tbl_DatosInstalacionInternet")
        ColocarCodigo = BaseII.ConsultaDS("Consulta_Codigo_OXXO_PorContrato_PrimerMensualidad", lista)

        BaseII.limpiaParametros()
        BaseII.Inserta("UspBorrarTabla")

    End Function

    Private Sub ConfigureCrystalReports_NewXml(ByVal op As String, ByVal Titulo As String)
        Try

            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            Dim clave_session As Long
            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0", Op6 As String = "0"

            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
            Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"
            Dim Status As String = Nothing

            If Me.PendientesCheckBox.CheckState = CheckState.Checked Then
                StatusPen = "1"
                If Me.EjecutadasCheckBox.Checked = True Or Me.VisitaCheckBox.Checked = True Then
                    Status = Status & "Pendientes,"
                Else
                    Status = "Pendientes"
                End If
            End If
            If Me.EjecutadasCheckBox.CheckState = CheckState.Checked Then
                StatusEje = "1"
                If Me.PendientesCheckBox.Checked = True Or Me.VisitaCheckBox.Checked = True Then
                    If Me.PendientesCheckBox.Checked = True Then
                        Status = Status & "Ejecutadas"
                    Else
                        Status = Status & "Ejecutadas,"
                    End If
                Else
                    Status = "Ejecutadas"
                End If
            End If
            If Me.VisitaCheckBox.CheckState = CheckState.Checked Then
                StatusVis = "1"
                If Me.PendientesCheckBox.Checked = True Or Me.EjecutadasCheckBox.Checked = True Then
                    Status = Status & "Visita"
                Else
                    Status = "Visita"
                End If
            End If


            If Me.NUMINILbl.Text.Length > 0 Then
                If IsNumeric(Me.NUMINILbl.Text) = True Then
                    If CLng(Me.NUMINILbl.Text) > 0 Then
                        Op1 = "1"
                    End If
                End If
            End If
            If Me.FECSOLINI.Text.Length > 0 Then

                Op2 = "1"
            End If
            If Me.FECEJEINI.Text.Length > 0 Then
                Op3 = "1"
            End If
            If Me.NOMTRABAJO.Text.Length > 0 Then
                Op4 = "1"
            End If
            If Me.NOMCOLONIA.Text.Length > 0 Then
                Op5 = "1"
            End If

            If CheckBox6.CheckState Then
                Op6 = "1"
                NumeroVisitas = NumericUpDown1.Value
            End If
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim ySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"
            If Me.RadioButton1.Checked = True Then
                OpOrdenar = "0"
            ElseIf Me.RadioButton2.Checked = True Then
                OpOrdenar = "1"
            ElseIf Me.RadioButton4.Checked = True Then
                OpOrdenar = "2"
            End If

            Dim reportPath As String = Nothing
            If NUMINILbl.Text <> "" And NUMFINLBL.Text <> "" And NUMINILbl.Text = NUMFINLBL.Text Then
                VerificaOrdRetiro()
            End If

            If op = 0 Then
                If IdSistema <> "SA" And IdSistema <> "VA" Then
                    reportPath = RutaReportes + "\ReporteOrdenesListadoNuevo.rpt"
                ElseIf IdSistema = "SA" Or IdSistema = "VA" Then
                    reportPath = RutaReportes + "\ReporteOrdenesListadoNuevo_SA.rpt"
                End If
            ElseIf op = 1 Then
                If IdSistema = "TO" Then
                    reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCabStar.rpt"
                ElseIf IdSistema = "AG" Then
                    If Me.CLV_TRABAJO.Text = "" Then
                        Me.CLV_TRABAJO.Text = 0


                    End If
                    If Me.CLV_TRABAJO.Text = 32 Or Me.CLV_TRABAJO.Text = 94 Or Me.CLV_TRABAJO.Text = 116 Or Me.CLV_TRABAJO.Text = 120 Or Me.CLV_TRABAJO.Text = 128 Or Me.CLV_TRABAJO.Text = 129 Or Me.CLV_TRABAJO.Text = 175 Or OrdRetiro = 1 Or Me.CLV_TRABAJO.Text = 262 Or Me.CLV_TRABAJO.Text = 307 Or Me.CLV_TRABAJO.Text = 322 Or Me.CLV_TRABAJO.Text = 323 Or Me.CLV_TRABAJO.Text = 331 Or Me.CLV_TRABAJO.Text = 332 Then
                        reportPath = RutaReportes + "\ReporteFormatoOrdenesServRetiro.rpt"
                    Else
                        reportPath = RutaReportes + "\ReporteFormatoOrdenesServBueno.rpt"
                    End If
                ElseIf IdSistema = "SA" Then
                    reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoTvRey.rpt"
                ElseIf IdSistema = "VA" Then
                    reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCosmo.rpt"
                ElseIf IdSistema = "LO" Or IdSistema = "YU" Then
                    reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoLogitel.rpt"
                End If

            End If

            Dim cnn As New SqlConnection(MiConexion)
            Dim cmd As New SqlCommand("ReporteAreaTecnicaOrdSer1", cnn)
           
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0

            Dim parametro1 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
            parametro1.Direction = ParameterDirection.Input
            Dim tipoServ As Integer = 0
            If GenRepBlanco Then
                tipoServ = 1
            End If
            parametro1.Value = tipoServ
            cmd.Parameters.Add(parametro1)

            Dim parametro2 As New SqlParameter("@op1", SqlDbType.SmallInt)
            parametro2.Direction = ParameterDirection.Input
            If GenRepBlanco Then
                Op1 = 1
            End If
            parametro2.Value = Op1
            cmd.Parameters.Add(parametro2)

            Dim parametro3 As New SqlParameter("@op2", SqlDbType.SmallInt)
            parametro3.Direction = ParameterDirection.Input
            parametro3.Value = Op2
            cmd.Parameters.Add(parametro3)

            Dim parametro4 As New SqlParameter("@op3", SqlDbType.SmallInt)
            parametro4.Direction = ParameterDirection.Input
            parametro4.Value = Op3
            cmd.Parameters.Add(parametro4)

            Dim parametro5 As New SqlParameter("@op4", SqlDbType.SmallInt)
            parametro5.Direction = ParameterDirection.Input
            parametro5.Value = Op4
            cmd.Parameters.Add(parametro5)

            Dim parametro6 As New SqlParameter("@op5", SqlDbType.SmallInt)
            parametro6.Direction = ParameterDirection.Input
            parametro6.Value = Op5
            cmd.Parameters.Add(parametro6)

            Dim parametro7 As New SqlParameter("@StatusPen", SqlDbType.Bit)
            parametro7.Direction = ParameterDirection.Input
            If StatusPen = "1" Then
                parametro7.Value = True
            Else
                parametro7.Value = False
            End If
            cmd.Parameters.Add(parametro7)

            Dim parametro8 As New SqlParameter("@StatusEje", SqlDbType.Bit)
            parametro8.Direction = ParameterDirection.Input
            If StatusEje = "1" Then
                parametro8.Value = True
            Else
                parametro8.Value = False
            End If
            cmd.Parameters.Add(parametro8)

            Dim parametro9 As New SqlParameter("@StatusVis", SqlDbType.Bit)
            parametro9.Direction = ParameterDirection.Input
            If StatusVis = "1" Then
                parametro9.Value = True
            Else
                parametro9.Value = False
            End If
            cmd.Parameters.Add(parametro9)
            If IsNumeric(Me.NUMINILbl.Text) = True Then Num1 = CStr(Me.NUMINILbl.Text)
            Dim parametro10 As New SqlParameter("@Clv_OrdenIni", SqlDbType.BigInt)
            parametro10.Direction = ParameterDirection.Input

            If GenRepBlanco Then
                Num1 = -1
            End If
            parametro10.Value = CLng(Num1)
            cmd.Parameters.Add(parametro10)
            If IsNumeric(Me.NUMFINLBL.Text) = True Then Num2 = CStr(Me.NUMFINLBL.Text)
            Dim parametro11 As New SqlParameter("@Clv_OrdenFin", SqlDbType.BigInt)
            parametro11.Direction = ParameterDirection.Input
            If GenRepBlanco Then
                Num2 = -1
            End If

            parametro11.Value = CLng(Num2)
            cmd.Parameters.Add(parametro11)
            If IsDate(Me.FECSOLINI.Text) = True Then Fec1Ini = Me.FECSOLINI.Text
            Dim parametro12 As New SqlParameter("@Fec1Ini", SqlDbType.DateTime)
            parametro12.Direction = ParameterDirection.Input
            parametro12.Value = Fec1Ini
            cmd.Parameters.Add(parametro12)
            If IsDate(Me.FECSOLFIN.Text) = True Then Fec1Fin = Me.FECSOLFIN.Text
            Dim parametro13 As New SqlParameter("@Fec1Fin", SqlDbType.DateTime)
            parametro13.Direction = ParameterDirection.Input
            parametro13.Value = Fec1Fin
            cmd.Parameters.Add(parametro13)
            If IsDate(Me.FECEJEINI.Text) = True Then Fec2Ini = Me.FECEJEINI.Text
            Dim parametro14 As New SqlParameter("@Fec2Ini", SqlDbType.DateTime)
            parametro14.Direction = ParameterDirection.Input
            parametro14.Value = Fec2Ini
            cmd.Parameters.Add(parametro14)
            If IsDate(Me.FECEJEFIN.Text) = True Then Fec2Fin = Me.FECEJEFIN.Text
            Dim parametro15 As New SqlParameter("@Fec2Fin", SqlDbType.DateTime)
            parametro15.Direction = ParameterDirection.Input
            parametro15.Value = Fec2Fin
            cmd.Parameters.Add(parametro15)
            If IsNumeric(Me.CLV_TRABAJO.Text) = True Then nclv_trabajo = CStr(Me.CLV_TRABAJO.Text)
            Dim parametro16 As New SqlParameter("@Clv_Trabajo", SqlDbType.Int)
            parametro16.Direction = ParameterDirection.Input
            parametro16.Value = nclv_trabajo
            cmd.Parameters.Add(parametro16)
            If IsNumeric(Me.CLV_COLONIA.Text) = True Then nClv_colonia = CStr(Me.CLV_COLONIA.Text)
            Dim parametro17 As New SqlParameter("@Clv_Colonia", SqlDbType.Int)
            parametro17.Direction = ParameterDirection.Input
            parametro17.Value = nClv_colonia
            cmd.Parameters.Add(parametro17)

            Dim parametro18 As New SqlParameter("@OpOrden", SqlDbType.Int)
            parametro18.Direction = ParameterDirection.Input
            parametro18.Value = OpOrdenar
            cmd.Parameters.Add(parametro18)

            Dim parametro22 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
            parametro22.Direction = ParameterDirection.Input
            parametro22.Value = LocClv_session
            cmd.Parameters.Add(parametro22)

            Dim parametro23 As New SqlParameter("@ResVisita", SqlDbType.Int)
            parametro23.Direction = ParameterDirection.Input
            parametro23.Value = NumeroVisitas
            cmd.Parameters.Add(parametro23)


            Dim parametro24 As New SqlParameter("@op6", SqlDbType.SmallInt)
            parametro24.Direction = ParameterDirection.Input
            parametro24.Value = Op6
            cmd.Parameters.Add(parametro24)

            Dim parametro25 As New SqlParameter("@clvDepto", SqlDbType.BigInt)
            parametro25.Direction = ParameterDirection.Input
            parametro25.Value = CLng(Me.cmbDepartamentos.SelectedValue)
            cmd.Parameters.Add(parametro25)

            Dim parametro26 As New SqlParameter("@clvTipoServicioDigital", SqlDbType.Int)
            parametro26.Direction = ParameterDirection.Input
            parametro26.Value = gloClvTipSerReportes
            cmd.Parameters.Add(parametro26)

            'clave_session = DAMESclv_Sessionporfavor()

            'Dim parametro25 As New SqlParameter("@clave_session", SqlDbType.BigInt)
            'parametro25.Direction = ParameterDirection.Input
            'parametro25.Value = clave_session
            'cmd.Parameters.Add(parametro25)

            If GenRepBlanco Then
                GenRepBlanco = False
            End If

            Dim da As New SqlDataAdapter(cmd)

            Dim ds As New DataSet()


            da.Fill(ds)
            ds.Clear()

            ds = ColocarCodigo()


            'ds.Tables(0).TableName = "ReporteAreaTecnicaOrdSer"
            'ds.Tables(1).TableName = "DameDatosGenerales_2"

            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(ds)


            If Me.CrystalReportViewer1.InvokeRequired Then
                Dim d As New Reporte(AddressOf ConfigureCrystalReports)
                Me.Invoke(d, New Object() {op, Titulo})
            Else
                CrystalReportViewer1.ReportSource = customersByCityReport
            End If
            CrystalReportViewer1.Zoom(75)
            BanderaAreaTecnica = False

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub ConfigureCrystalReports(ByVal op As String, ByVal Titulo As String)
        customersByCityReport = New ReportDocument
        Dim cnn As New SqlConnection(MiConexion)
        Try

            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0", Op6 As String = "0"
            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
            Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"
            Dim Status As String = Nothing
            Dim clave_session As Long


            NumeroVisitas = 0
            If NumeroVisitas > 3 Then
                MsgBox("El numero de Visitas no puede ser Mayor a 3", MsgBoxStyle.Exclamation)
                Exit Sub
            End If

            If Me.PendientesCheckBox.CheckState = CheckState.Checked Then
                StatusPen = "1"
                If Me.EjecutadasCheckBox.Checked = True Or Me.VisitaCheckBox.Checked = True Then
                    Status = Status & "Pendientes,"
                Else
                    Status = "Pendientes"
                End If
            End If
            If Me.EjecutadasCheckBox.CheckState = CheckState.Checked Then
                StatusEje = "1"
                If Me.PendientesCheckBox.Checked = True Or Me.VisitaCheckBox.Checked = True Then
                    If Me.PendientesCheckBox.Checked = True Then
                        Status = Status & "Ejecutadas"
                    Else
                        Status = Status & "Ejecutadas,"
                    End If
                Else
                    Status = "Ejecutadas"
                End If
            End If
            If Me.VisitaCheckBox.CheckState = CheckState.Checked Then
                StatusVis = "1"
                If Me.PendientesCheckBox.Checked = True Or Me.EjecutadasCheckBox.Checked = True Then
                    Status = Status & "Visita"
                Else
                    Status = "Visita"
                End If
            End If



            If Me.NUMINILbl.Text.Length > 0 Then
                If IsNumeric(Me.NUMINILbl.Text) = True Then
                    If CLng(Me.NUMINILbl.Text) > 0 Then
                        Op1 = "1"
                    End If
                End If
            End If
            If Me.FECSOLINI.Text.Length > 0 Then

                Op2 = "1"
            End If
            If Me.FECEJEINI.Text.Length > 0 Then
                Op3 = "1"
            End If
            If Me.NOMTRABAJO.Text.Length > 0 Then
                Op4 = "1"
            End If
            If Me.NOMCOLONIA.Text.Length > 0 Then
                Op5 = "1"
            End If

            If CheckBox6.CheckState Then
                Op6 = "1"
                NumeroVisitas = NumericUpDown1.Value
            End If

            Dim mySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"
            If Me.RadioButton1.Checked = True Then
                OpOrdenar = "0"
            ElseIf Me.RadioButton2.Checked = True Then
                OpOrdenar = "1"
            ElseIf Me.RadioButton4.Checked = True Then
                OpOrdenar = "2"
            End If

            Dim reportPath As String = Nothing


            If op = 0 Then
                If IdSistema <> "SA" And IdSistema <> "VA" Then
                    'reportPath = RutaReportes + "\ReporteOrdenesListadoNuevo.rpt"
                    'SAUL REPORTE
                    If Me.EjecutadasCheckBox.CheckState = CheckState.Checked Then
                        reportPath = RutaReportes + "\ReporteOrdenesListadoNuevo5.rpt"
                    Else
                        reportPath = RutaReportes + "\ReporteOrdenesListadoNuevo.rpt"
                    End If
                    'SAUL REPORTE(FIN)
                ElseIf IdSistema = "SA" Or IdSistema = "VA" Then
                    reportPath = RutaReportes + "\ReporteOrdenesListadoNuevo_SA.rpt"
                End If
            ElseIf op = 1 Then
                If IdSistema = "TO" Then
                    reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCabStar.rpt"
                ElseIf IdSistema = "AG" Then
                    reportPath = RutaReportes + "\ReporteFormatoOrdenesServBueno.rpt"
                ElseIf IdSistema = "SA" Then
                    reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoTvRey.rpt"
                ElseIf IdSistema = "VA" Then
                    reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCosmo.rpt"
                ElseIf IdSistema = "LO" Or IdSistema = "YU" Then
                    reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoLogitel.rpt"
                End If

            End If

            'SAUL REPORTE
            Dim Sp As String
            If Me.EjecutadasCheckBox.CheckState = CheckState.Checked Then
                Sp = "ReporteAreaTecnicaOrdSer_Prueba_Listado5"
            Else
                Sp = "ReporteAreaTecnicaOrdSer_Prueba_Listado"
            End If
            'SAUL REPORTE (FIN)

            Dim cmd As New SqlCommand(Sp, cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0

            '@Clv_TipSer int
            Dim parametro1 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
            parametro1.Direction = ParameterDirection.Input
            parametro1.Value = 0
            cmd.Parameters.Add(parametro1)
            '@op1 smallint
            Dim parametro2 As New SqlParameter("@op1", SqlDbType.SmallInt)
            parametro2.Direction = ParameterDirection.Input
            parametro2.Value = Op1
            cmd.Parameters.Add(parametro2)
            '@op2 smallint
            Dim parametro3 As New SqlParameter("@op2", SqlDbType.SmallInt)
            parametro3.Direction = ParameterDirection.Input
            parametro3.Value = Op2
            cmd.Parameters.Add(parametro3)
            '@op3 smallint
            Dim parametro4 As New SqlParameter("@op3", SqlDbType.SmallInt)
            parametro4.Direction = ParameterDirection.Input
            parametro4.Value = Op3
            cmd.Parameters.Add(parametro4)
            '@op4 smallint
            Dim parametro5 As New SqlParameter("@op4", SqlDbType.SmallInt)
            parametro5.Direction = ParameterDirection.Input
            parametro5.Value = Op4
            cmd.Parameters.Add(parametro5)
            '@op5 smallint
            Dim parametro6 As New SqlParameter("@op5", SqlDbType.SmallInt)
            parametro6.Direction = ParameterDirection.Input
            parametro6.Value = Op5
            cmd.Parameters.Add(parametro6)
            '@StatusPen bit
            Dim parametro7 As New SqlParameter("@StatusPen", SqlDbType.Bit)
            parametro7.Direction = ParameterDirection.Input
            If StatusPen = "1" Then
                parametro7.Value = True
            Else
                parametro7.Value = False
            End If
            cmd.Parameters.Add(parametro7)
            '@StatusEje bit
            Dim parametro8 As New SqlParameter("@StatusEje", SqlDbType.Bit)
            parametro8.Direction = ParameterDirection.Input
            If StatusEje = "1" Then
                parametro8.Value = True
            Else
                parametro8.Value = False
            End If
            cmd.Parameters.Add(parametro8)
            '@StatusVis bit
            Dim parametro9 As New SqlParameter("@StatusVis", SqlDbType.Bit)
            parametro9.Direction = ParameterDirection.Input
            If StatusVis = "1" Then
                parametro9.Value = True
            Else
                parametro9.Value = False
            End If
            cmd.Parameters.Add(parametro9)
            '@Clv_OrdenIni bigint
            If IsNumeric(Me.NUMINILbl.Text) = True Then Num1 = CStr(Me.NUMINILbl.Text)
            Dim parametro10 As New SqlParameter("@Clv_OrdenIni", SqlDbType.BigInt)
            parametro10.Direction = ParameterDirection.Input
            parametro10.Value = CLng(Num1)
            cmd.Parameters.Add(parametro10)
            '@Clv_OrdenFin bigint
            If IsNumeric(Me.NUMFINLBL.Text) = True Then Num2 = CStr(Me.NUMFINLBL.Text)
            Dim parametro11 As New SqlParameter("@Clv_OrdenFin", SqlDbType.BigInt)
            parametro11.Direction = ParameterDirection.Input
            parametro11.Value = CLng(Num2)
            cmd.Parameters.Add(parametro11)
            '@Fec1Ini Datetime
            If IsDate(Me.FECSOLINI.Text) = True Then Fec1Ini = Me.FECSOLINI.Text
            Dim parametro12 As New SqlParameter("@Fec1Ini", SqlDbType.DateTime)
            parametro12.Direction = ParameterDirection.Input
            parametro12.Value = Fec1Ini
            cmd.Parameters.Add(parametro12)
            If IsDate(Me.FECSOLFIN.Text) = True Then Fec1Fin = Me.FECSOLFIN.Text
            Dim parametro13 As New SqlParameter("@Fec1Fin", SqlDbType.DateTime)
            parametro13.Direction = ParameterDirection.Input
            parametro13.Value = Fec1Fin
            cmd.Parameters.Add(parametro13)
            If IsDate(Me.FECEJEINI.Text) = True Then Fec2Ini = Me.FECEJEINI.Text
            Dim parametro14 As New SqlParameter("@Fec2Ini", SqlDbType.DateTime)
            parametro14.Direction = ParameterDirection.Input
            parametro14.Value = Fec2Ini
            cmd.Parameters.Add(parametro14)
            If IsDate(Me.FECEJEFIN.Text) = True Then Fec2Fin = Me.FECEJEFIN.Text
            Dim parametro15 As New SqlParameter("@Fec2Fin", SqlDbType.DateTime)
            parametro15.Direction = ParameterDirection.Input
            parametro15.Value = Fec2Fin
            cmd.Parameters.Add(parametro15)
            '@Clv_Trabajo int
            If IsNumeric(Me.CLV_TRABAJO.Text) = True Then nclv_trabajo = CStr(Me.CLV_TRABAJO.Text)
            Dim parametro16 As New SqlParameter("@Clv_Trabajo", SqlDbType.Int)
            parametro16.Direction = ParameterDirection.Input
            parametro16.Value = nclv_trabajo
            cmd.Parameters.Add(parametro16)
            '@Clv_Colonia int
            If IsNumeric(Me.CLV_COLONIA.Text) = True Then nClv_colonia = CStr(Me.CLV_COLONIA.Text)
            Dim parametro17 As New SqlParameter("@Clv_Colonia", SqlDbType.Int)
            parametro17.Direction = ParameterDirection.Input
            parametro17.Value = nClv_colonia
            cmd.Parameters.Add(parametro17)
            '@OpOrden int
            Dim parametro18 As New SqlParameter("@OpOrden", SqlDbType.Int)
            parametro18.Direction = ParameterDirection.Input
            parametro18.Value = OpOrdenar
            cmd.Parameters.Add(parametro18)

            Dim parametro19 As New SqlParameter("@numeroVisitas", SqlDbType.Int)
            parametro19.Direction = ParameterDirection.Input
            parametro19.Value = NumeroVisitas
            cmd.Parameters.Add(parametro19)

            Dim parametro20 As New SqlParameter("@OP6", SqlDbType.SmallInt)
            parametro20.Direction = ParameterDirection.Input
            parametro20.Value = Op6
            cmd.Parameters.Add(parametro20)


            Dim parametro21 As New SqlParameter("@ResVisita", SqlDbType.Int)
            parametro21.Direction = ParameterDirection.Output
            cmd.Parameters.Add(parametro21)

            Dim parametro22 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
            parametro22.Direction = ParameterDirection.Input
            parametro22.Value = LocClv_session
            cmd.Parameters.Add(parametro22)

            Dim parametro23 As New SqlParameter("@clvDepto", SqlDbType.BigInt)
            parametro23.Direction = ParameterDirection.Input
            parametro23.Value = CLng(Me.cmbDepartamentos.SelectedValue)
            cmd.Parameters.Add(parametro23)

            Dim parametro24 As New SqlParameter("@clvTipoServicioDigital", SqlDbType.Int)
            parametro24.Direction = ParameterDirection.Input
            parametro24.Value = gloClvTipSerReportes
            cmd.Parameters.Add(parametro24)

            Dim NumVisitas As String

            cnn.Open()
            cmd.ExecuteNonQuery()
            NumVisitas = parametro21.Value

            clave_session = DAMESclv_Sessionporfavor()


            Dim da As New SqlDataAdapter(cmd)

            Dim ds As New DataSet()
            da.Fill(ds)

            ds.Tables(0).TableName = "ReporteAreaTecnicaOrdSer_Prueba_Listado"
            Dim variable As String
            variable = "Sin Filtro  #  Visitas de Tecnico"
            'ds.Tables.Add(data1)
            customersByCityReport.Load(reportPath)
            mySelectFormula = "Listado de Ordenes de Servicio"
            'If op = 0 Then
            customersByCityReport.DataDefinition.FormulaFields("empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloSucursal & "'"
            If NumeroVisitas > 0 Then
                customersByCityReport.DataDefinition.FormulaFields("NumeroVisitas").Text = "'" & NumVisitas & "'"
            Else
                customersByCityReport.DataDefinition.FormulaFields("NumeroVisitas").Text = "'" & variable & "'"
            End If

            SetDBReport(ds, customersByCityReport)

            If Me.CrystalReportViewer1.InvokeRequired Then
                Dim d As New Reporte(AddressOf ConfigureCrystalReports)
                Me.Invoke(d, New Object() {op, Titulo})
            Else
                CrystalReportViewer1.ReportSource = customersByCityReport
            End If
            CrystalReportViewer1.Zoom(75)

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            cnn.Close()
            cnn.Dispose()
        End Try
    End Sub

    Private Sub Frmareatecnica_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        ActivarForma()

    End Sub

    Public Sub ActivarForma()
        If GloBusca_NumOrden = True Then
            GloBusca_NumOrden = False
            Me.NUMFINLBL.Text = GLONUMCLV_ORDEN_FIN
            Me.NUMINILbl.Text = GLONUMCLV_ORDEN_INI
            GLONUMCLV_ORDEN_FIN = 0
            GLONUMCLV_ORDEN_INI = 0
        End If
        If op = "1" And GloBndSelFecha = True Then
            GloBndSelFecha = False
            Me.FECSOLINI.Text = GloFecha_Ini
            Me.FECSOLFIN.Text = GloFecha_Fin
            GloFecha_Ini = "01/01/1900"
            GloFecha_Fin = "01/01/1900"
        End If
        If op = "2" And GloBndSelFecha = True Then
            GloBndSelFecha = False
            Me.FECEJEINI.Text = GloFecha_Ini
            Me.FECEJEFIN.Text = GloFecha_Fin
            GloFecha_Ini = "01/01/1900"
            GloFecha_Fin = "01/01/1900"
        End If
        If GloBndTrabajo = True Then
            GloBndTrabajo = False
            Me.CLV_TRABAJO.Text = GloNumClv_Trabajo
            Me.NOMTRABAJO.Text = GLONOMTRABAJO
        End If
        If GlobndClv_Colonia = True Then
            GlobndClv_Colonia = False
            Me.CLV_COLONIA.Text = GloNumClv_Colonia
            Me.NOMCOLONIA.Text = GLONOMCOLONIA
        End If
        If PendientesCheckBox.CheckState = CheckState.Checked Then
            CheckBox6.CheckState = CheckState.Unchecked
        End If


        If BanderaAreaTecnica = True And opreporte = 1 And opcionarea = 1 Then
            ' FrmSelSector.Show()
            ConfigureCrystalReports(0, "")
            BanderaAreaTecnica = False
            opcionarea = 0

        End If

        If BanderaAreaTecnica = True And opreporte = 2 And opcionarea = 1 Then
            'FrmSelSector.Show()
            ConfigureCrystalReports_NewXml(1, "")
            BanderaAreaTecnica = False
            opcionarea = 0

        End If

        If BanderaAreaTecnica = True And opreporte = 3 Then
            'FrmSelSector.Show()
            ConfigureCrystalReports_NewXml(1, "")
            BanderaAreaTecnica = False
            opcionarea = 0

        End If
    End Sub




    Private Sub Frmareatecnica_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        PendientesCheckBox.Checked = False
        VisitaCheckBox.Checked = False

        EjecutadasCheckBox.Checked = False
        NumericUpDown1.Enabled = False

        'MANDAMOS LLENAR EL COMBO DE DEPARTAMENTOS (INICIO)
        llenaComboDepartamentos()
        'MANDAMOS LLENAR EL COMBO DE DEPARTAMENTOS (INICIO)

        ''TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerPrincipal' Puede moverla o quitarla seg�n sea necesario.
        'Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
        'Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)
        'If IsNumeric(Me.ComboBox4.SelectedValue) = True Then
        '    GloClv_TipSer = Me.ComboBox4.SelectedValue
        '    LIMPIA()
        'Else
        '    GloClv_TipSer = glotiposervicioppal
        'End If

        '--Me.CatalogodeReportesAreaTecnicaTableAdapter.Fill(Me.NewsoftvDataSet1.CatalogodeReportesAreaTecnica, Me.ComboBox4.SelectedValue, 0)
        '--Me.CatalogodeReportesAreaTecnicaTableAdapter.Fill(Me.NewsoftvDataSet1.CatalogodeReportesAreaTecnica, 1, 0)
        CON.Close()
    End Sub


    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub SetDBLogonForReport2(ByVal myConnectionInfo As ConnectionInfo)
        Dim myTableLogOnInfos As TableLogOnInfos = Me.CrystalReportViewer1.LogOnInfo
        For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
            myTableLogOnInfo.ConnectionInfo = myConnectionInfo
        Next
    End Sub

    Private Sub LIMPIA()
        Me.CheckBox1.CheckState = CheckState.Unchecked
        Me.CheckBox2.CheckState = CheckState.Unchecked
        Me.CheckBox3.CheckState = CheckState.Unchecked
        Me.CheckBox5.CheckState = CheckState.Unchecked
        Me.CheckBox4.CheckState = CheckState.Unchecked
        Me.CheckBox6.CheckState = CheckState.Unchecked
        Me.NUMFINLBL.Text = 0
        Me.NUMINILbl.Text = 0
        Me.FECSOLFIN.Text = ""
        Me.FECSOLINI.Text = ""
        Me.FECEJEFIN.Text = ""
        Me.FECEJEINI.Text = ""
        Me.CLV_TRABAJO.Text = 0
        Me.NOMTRABAJO.Text = ""
        Me.CLV_COLONIA.Text = 0
        Me.NOMCOLONIA.Text = ""
    End Sub

    'Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.SelectedIndexChanged
    '    If IsNumeric(Me.ComboBox4.SelectedValue) = True Then
    '        GloClv_TipSer = Me.ComboBox4.SelectedValue
    '        LIMPIA()
    '        '--Me.CatalogodeReportesAreaTecnicaTableAdapter.Fill(Me.NewsoftvDataSet1.CatalogodeReportesAreaTecnica, Me.ComboBox4.SelectedValue, 0)
    '    End If
    'End Sub



    Private Sub PendientesCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PendientesCheckBox.CheckedChanged
        If Me.PendientesCheckBox.CheckState = CheckState.Checked Then
            GloPendientes = 1
            Me.EjecutadasCheckBox.CheckState = CheckState.Unchecked
            Me.VisitaCheckBox.CheckState = CheckState.Unchecked
        Else
            GloPendientes = 0
        End If
    End Sub

    Private Sub EjecutadasCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EjecutadasCheckBox.CheckedChanged
        If Me.EjecutadasCheckBox.CheckState = CheckState.Checked Then
            GloEjecutadas = 1
            Me.PendientesCheckBox.CheckState = CheckState.Unchecked
            Me.VisitaCheckBox.CheckState = CheckState.Unchecked
        Else
            GloEjecutadas = 0
        End If
    End Sub

    Private Sub VisitaCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VisitaCheckBox.CheckedChanged
        If Me.VisitaCheckBox.CheckState = CheckState.Checked Then
            GloVisita = 1
            Me.EjecutadasCheckBox.CheckState = CheckState.Unchecked
            Me.PendientesCheckBox.CheckState = CheckState.Unchecked
        Else
            GloVisita = 0
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub


    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If Me.CheckBox1.CheckState = CheckState.Checked Then
            op = "0"
            FrmSelOrdSer.Show()
        Else
            Me.NUMINILbl.Text = 0
            Me.NUMFINLBL.Text = 0
        End If
    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If Me.CheckBox2.CheckState = CheckState.Checked Then
            op = "1"
            FrmSelFechas.Show()
        Else
            Me.FECSOLFIN.Text = ""
            Me.FECSOLINI.Text = ""
        End If
    End Sub

    Private Sub CheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox3.CheckedChanged
        If Me.CheckBox3.CheckState = CheckState.Checked Then
            op = "2"
            FrmSelFechas.Show()
        Else
            Me.FECEJEINI.Text = ""
            Me.FECEJEFIN.Text = ""
        End If
    End Sub

    Private Sub CheckBox4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox4.CheckedChanged
        If Me.CheckBox4.CheckState = CheckState.Checked Then
            op = "3"
            Dim frm As New FrmSelTrabajo
            frm.publicClvDepto = CLng(Me.cmbDepartamentos.SelectedValue)
            frm.ShowDialog()
        Else
            Me.CLV_TRABAJO.Text = 0
            Me.NOMTRABAJO.Text = ""
        End If
    End Sub

    Private Sub CheckBox5_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox5.CheckedChanged
        If Me.CheckBox5.CheckState = CheckState.Checked Then
            op = "4"
            FrmSelColonia.Show()
        Else
            Me.CLV_COLONIA.Text = 0
            Me.NOMCOLONIA.Text = ""
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        opreporte = 1
        'Me.BackgroundWorker1.RunWorkerAsync()
        'PantallaProcesando.Show()
        If frmFiltroTipoServicio.ShowDialog = DialogResult.OK Then
            gloClvTipSerReportes = frmFiltroTipoServicio.cmbTipoServicio.SelectedValue
            Reportes()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opreporte = 2
        OrdRetiro = 0
        'Me.BackgroundWorker1.RunWorkerAsync()
        'PantallaProcesando.Show()
        If frmFiltroTipoServicio.ShowDialog = DialogResult.OK Then
            gloClvTipSerReportes = frmFiltroTipoServicio.cmbTipoServicio.SelectedValue
            Reportes()
        End If
    End Sub
    Private Sub Reportes()
        Try

            BanderaAreaTecnica = True
            Select Case opreporte
                Case 1
                    LocClv_session = DAMESclv_Sessionporfavor()
                    FrmSelSector.Show()
                    'ConfigureCrystalReports(0, "")
                Case 2
                    LocClv_session = DAMESclv_Sessionporfavor()
                    FrmSelSector.Show()
                    'ConfigureCrystalReports_NewXml(1, "")
                Case 3
                    GenRepBlanco = True
                    ActivarForma()
            End Select
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork

    End Sub


    Private Sub BackgroundWorker1_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        PantallaProcesando.Close()
    End Sub

    Private Sub Label6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label6.Click

    End Sub

    Private Sub CheckBox6_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox6.CheckedChanged


        If Me.CheckBox6.CheckState = CheckState.Checked Then
            op = "6"
            NumericUpDown1.Enabled = True
        Else
            NumericUpDown1.Enabled = False
            Me.CheckBox6.CheckState = CheckState.Unchecked

        End If

        '  Me.CheckBox6.CheckState = CheckState.Unchecked



    End Sub

    Private Sub NumericUpDown1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NumericUpDown1.KeyPress
        NumeroVisitas = NumericUpDown1.Value
        If NumeroVisitas > 3 Then
            MsgBox("El numero de Visitas no puede ser Mayor a 3", MsgBoxStyle.Exclamation)
        End If
    End Sub

    Private Sub NumericUpDown1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumericUpDown1.ValueChanged
        NumeroVisitas = NumericUpDown1.Value
        If NumeroVisitas > 3 Then
            MsgBox("El numero de Visitas no puede ser Mayor a 3", MsgBoxStyle.Exclamation)
        End If


    End Sub

    Private Sub btnImprimirFormato_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimirFormato.Click
        opreporte = 3
        OrdRetiro = 0
        'Me.BackgroundWorker1.RunWorkerAsync()
        'PantallaProcesando.Show()
        Reportes()
    End Sub

    'LLENAMOS EL COMBO DE LOS DEPARTAMENTOS
    Private Sub llenaComboDepartamentos()
        Dim depto As New ClassDepartamentos

        Me.cmbDepartamentos.DataSource = depto.uspConsultaTblDepartamentos(0, String.Empty, 3)
    End Sub

    Private Sub cmbDepartamentos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbDepartamentos.SelectedIndexChanged
        Me.CheckBox4.Checked = False
        Me.CLV_TRABAJO.Text = ""
        Me.NOMTRABAJO.Text = ""
    End Sub

    Private Sub VerificaOrdRetiro()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_ordser", SqlDbType.Int, NUMINILbl.Text)
        BaseII.CreateMyParameter("@Retiro", ParameterDirection.Output, SqlDbType.Int)
        'ByVal prmNombre As String, ByRef prmTipo As SqlDbType, ByRef prmTamanio As Integer, ByRef prmDireccion As ParameterDirection, ByRef prmValor As Object
        BaseII.ProcedimientoOutPut("VerificaOrdRetiro")
        OrdRetiro = CInt(BaseII.dicoPar("@Retiro").ToString)
    End Sub

End Class