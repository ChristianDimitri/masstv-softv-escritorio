﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text
Imports System.Windows.Forms
Imports System.Collections
Imports System.Collections.Generic
Imports System.Xml
Imports System.IO

Public Class CatalogoMotivoNoPaga

    Dim opcion As Integer = 0
    Dim Codigo As Integer
    Dim NuevoCodigo As Integer
    Dim Respuesta As String
    Dim Mensaje As Integer = 0

    Private Sub CatalogoMotivoNoPaga_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Catalogo_PagoNoRealizado()
    End Sub

    Private Sub CatalogoMotivoNoPaga_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Me.Label1.ForeColor = Color.Black
        Me.Label2.ForeColor = Color.Black
        Me.Label3.ForeColor = Color.Black
        Me.Label4.ForeColor = Color.Black
        Me.Label6.ForeColor = Color.Black
        Me.Label7.ForeColor = Color.Black
        Me.Label8.ForeColor = Color.Black
        Me.Label9.ForeColor = Color.Black
        Me.Label10.ForeColor = Color.Black


    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        If TabControl1.SelectedTab Is TabPage1 Then

            opcion = 1
            If IsNumeric(TextBox1.Text) = True Then
                NuevoCodigo = TextBox1.Text
            Else
                MsgBox("El Codigo debe de ser Numerico", MsgBoxStyle.Exclamation)
                Exit Sub
            End If
            If TextBox2.Text = "" Then
                MsgBox("Escriba un valor para el campo Respuesta", MsgBoxStyle.Exclamation)
                Exit Sub
            Else
                Respuesta = TextBox2.Text
            End If


            ModifiCatalogRespNoPag()

            If Mensaje = 1 Then
                MsgBox("El numero de Codigo ya existe", MsgBoxStyle.Exclamation)
                Exit Sub
            Else
                MsgBox("Registro exitoso", MsgBoxStyle.Information)
            End If

        End If

        If TabControl1.SelectedTab Is TabPage2 Then

            opcion = 2
            If IsNumeric(TextBox3.Text) Then
                Codigo = TextBox3.Text
                Respuesta = ""
            Else
                MsgBox("El Codigo debe de ser Numerico", MsgBoxStyle.Exclamation)
                Exit Sub
            End If
            ModifiCatalogRespNoPag()

            If Mensaje = 1 Then
                MsgBox("El codigo a eliminar no existe", MsgBoxStyle.Exclamation)
                Exit Sub
            Else
                MsgBox("Se elimino Correctamente", MsgBoxStyle.Information)
            End If

        End If

        If TabControl1.SelectedTab Is TabPage3 Then

            opcion = 3
            Codigo = ComboBox1.SelectedValue

            If IsNumeric(TextBox4.Text) = True Then
                NuevoCodigo = TextBox4.Text
            Else
                MsgBox("El Codigo debe de ser Numerico", MsgBoxStyle.Exclamation)
                Exit Sub
            End If
            If TextBox5.Text = "" Then
                MsgBox("Escriba un valor para el campo Respuesta", MsgBoxStyle.Exclamation)
                Exit Sub
            Else
                Respuesta = TextBox5.Text
            End If

            ModifiCatalogRespNoPag()

            If Mensaje = 1 Then
                MsgBox("El Numero de Codigo Ya existe", MsgBoxStyle.Exclamation)
                Exit Sub
            Else
                MsgBox("Se Modifico Correctamente", MsgBoxStyle.Information)
            End If

        End If




    End Sub

    Private Sub Catalogo_PagoNoRealizado()
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("DameCatalogo_PagoNoRealizado", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Try
            Dim DA As New SqlDataAdapter(CMD)
            Dim DT As New DataTable
            DA.Fill(DT)
            Me.ComboBox1.DataSource = DT
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub ModifiCatalogRespNoPag()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Codigo", SqlDbType.VarChar, Codigo)
        BaseII.CreateMyParameter("@NuevoCodigo", SqlDbType.VarChar, NuevoCodigo)
        BaseII.CreateMyParameter("@Respuesta", SqlDbType.VarChar, Respuesta)
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, opcion)
        BaseII.CreateMyParameter("@error", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("UspModifiCatalogRespNoPag")
        Mensaje = BaseII.dicoPar("@error")
    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

End Class