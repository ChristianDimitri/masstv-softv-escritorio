Public Class FrmSelFechas2

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
    End Sub

    Private Sub FrmSelFechas2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If (Rpt_ProductividadTecnico = True) Then
            CMBlbl1.Visible = True
            CMBTipoTrabajo.Visible = True
            CMBTipoTrabajo.Items.Add("Ambos")
            CMBTipoTrabajo.Items.Add("Ordenes")
            CMBTipoTrabajo.Items.Add("Quejas")
            CMBTipoTrabajo.SelectedText = "Ambos"
        Else
            CMBLabel3.Visible = True
            RadioButton1.Visible = True
            RadioButton2.Visible = True
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        LocFechaTec1 = Me.DateTimePicker1.Value
        LocFechaTec2 = Me.DateTimePicker2.Value
        If (Rpt_ProductividadTecnico) Then
            Rpt_ProductividadTecnicoTipoTrabajo = CMBTipoTrabajo.Text
            bndreportetec = True
            FrmImprimirContrato.Show()
            Me.Close()
        Else
            If Me.RadioButton1.Checked = True Then
                LocTipRepTec = 0
            ElseIf Me.RadioButton1.Checked = False Then
                LocTipRepTec = 1
            End If
            bndreportetec = True
            FrmImprimirContrato.Show()
            Me.Close()
        End If


    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Rpt_ProductividadTecnico = False
        Me.Close()
    End Sub
End Class