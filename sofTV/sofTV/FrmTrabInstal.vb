﻿Imports System.Data.SqlClient
Imports System.Text

Public Class FrmTrabInstal

    Private Sub FrmTrabInstal_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        MuestraTipSerPrincipal()
        MuestraTrabajos()
    End Sub

    Private Sub MuestraTipSerPrincipal()
        Dim con As New SqlConnection(MiConexion)
        Dim str As New StringBuilder

        str.Append("Exec MuestraTipSerPrincipal ")

        Dim dataadapter As New SqlDataAdapter(str.ToString, con)
        Dim datatable As New DataTable
        Try
            con.Open()
            dataadapter.Fill(datatable)
            Me.cbxTipServ.DataSource = datatable

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
            con.Close()
            con.Dispose()
        End Try

    End Sub

    Private Sub MuestraTrabajos()
        '    Dim con As New SqlConnection(MiConexion)
        '    Dim str As New StringBuilder

        'str.Append("Exec uspConsultaTrabajosInst ")
        'str.Append(CStr(LocClv_session))
        'Dim dataadapter As New SqlDataAdapter(Str.ToString, con)
        Try
            Dim datatable As New DataTable

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_session", SqlDbType.BigInt, LocClv_session)
            BaseII.CreateMyParameter("@clvTipSer", SqlDbType.Int, gloClvTipSerReportes)
            datatable = BaseII.ConsultaDT("uspConsultaTrabajosInst")
            'con.Open()
            'dataadapter.Fill(datatable)
            lbxSeleccionar.DataSource = datatable
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
            'con.Close()
            'con.Dispose()
        End Try
    End Sub

    Private Sub MuestraSelecciona_TrabajoConsulta(ByVal clv_session As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim str As New StringBuilder

        str.Append("Exec MuestraSelecciona_TrabajoConsulta ")
        str.Append(CStr(clv_session))

        Dim dataadapter As New SqlDataAdapter(str.ToString, con)
        Dim datatable As New DataTable
        Try
            con.Open()
            dataadapter.Fill(datatable)
            lbxSeleccionado.DataSource = datatable
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
            con.Close()
            con.Dispose()
        End Try
    End Sub

    Private Sub MuestraSelecciona_Trabajo_tmpConsulta(ByVal clv_session As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim str As New StringBuilder

        str.Append("Exec MuestraSelecciona_Trabajo_tmpConsulta ")
        str.Append(CStr(clv_session))

        Dim dataadapter As New SqlDataAdapter(str.ToString, con)
        Dim datatable As New DataTable
        Try
            con.Open()
            dataadapter.Fill(datatable)
            lbxSeleccionar.DataSource = datatable
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
            con.Close()
            con.Dispose()
        End Try
    End Sub

    Private Sub Insertauno_Seleccion_Trabajo(ByVal clv_session As Long, ByVal clv_trabajo As Integer)
        Dim con As New SqlConnection(MiConexion)
        Dim str As New StringBuilder

        str.Append("Exec Insertauno_Seleccion_Trabajo ")
        str.Append(CStr(clv_session) & ", ")
        str.Append(CStr(clv_trabajo))

        Dim dataadapter As New SqlDataAdapter(str.ToString, con)
        Dim datatable As New DataTable
        Dim binding As New BindingSource
        Try
            con.Open()
            dataadapter.Fill(datatable)
            binding.DataSource = datatable

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
            con.Close()
            con.Dispose()
        End Try
    End Sub

    Private Sub btnM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnM.Click
        Insertauno_Seleccion_Trabajo(LocClv_session, CLng(lbxSeleccionar.SelectedValue))
        MuestraSelecciona_TrabajoConsulta(LocClv_session)
        MuestraSelecciona_Trabajo_tmpConsulta(LocClv_session)
    End Sub

    Private Sub InsertaTOSeleccion_trabajo(ByVal clv_session As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim str As New StringBuilder

        str.Append("Exec InsertaTOSeleccion_trabajo ")
        str.Append(CStr(clv_session))


        Dim dataadapter As New SqlDataAdapter(str.ToString, con)
        Dim datatable As New DataTable
        Dim binding As New BindingSource

        Try
            con.Open()
            dataadapter.Fill(datatable)
            binding.DataSource = datatable

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
            con.Close()
            con.Dispose()
        End Try
    End Sub

    Private Sub btnMM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMM.Click
        InsertaTOSeleccion_trabajo(LocClv_session)
        MuestraSelecciona_TrabajoConsulta(LocClv_session)
        MuestraSelecciona_Trabajo_tmpConsulta(LocClv_session)
    End Sub

    Private Sub Insertauno_Seleccion_Trabajo_tmp(ByVal clv_session As Long, ByVal clv_trabajo As Integer)
        Dim con As New SqlConnection(MiConexion)
        Dim str As New StringBuilder

        str.Append("Exec Insertauno_Seleccion_Trabajo_tmp ")
        str.Append(CStr(clv_session) & ", ")
        str.Append(CStr(clv_trabajo))

        Dim dataadapter As New SqlDataAdapter(str.ToString, con)
        Dim datatable As New DataTable
        Dim binding As New BindingSource

        Try
            con.Open()
            dataadapter.Fill(datatable)
            binding.DataSource = datatable

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
            con.Close()
            con.Dispose()
        End Try
    End Sub

    Private Sub btnL_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnL.Click
        Insertauno_Seleccion_Trabajo_tmp(LocClv_session, CLng(lbxSeleccionado.SelectedValue))
        MuestraSelecciona_TrabajoConsulta(LocClv_session)
        MuestraSelecciona_Trabajo_tmpConsulta(LocClv_session)
    End Sub

    Private Sub InsertaTOSeleccion_trabajo_tmp(ByVal clv_session As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim str As New StringBuilder

        str.Append("Exec InsertaTOSeleccion_trabajo_tmp ")
        str.Append(CStr(clv_session))


        Dim dataadapter As New SqlDataAdapter(str.ToString, con)
        Dim datatable As New DataTable
        Dim binding As New BindingSource

        Try
            con.Open()
            dataadapter.Fill(datatable)
            binding.DataSource = datatable

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
            con.Close()
            con.Dispose()
        End Try
    End Sub

    Private Sub btnLL_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLL.Click
        InsertaTOSeleccion_trabajo_tmp(LocClv_session)
        MuestraSelecciona_TrabajoConsulta(LocClv_session)
        MuestraSelecciona_Trabajo_tmpConsulta(LocClv_session)
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Dim cont As Integer = 0
        cont = lbxSeleccionado.Items.Count
        If cont = 0 Then
            MsgBox("Seleccione Al Menos un Trabajo", MsgBoxStyle.Information)
        ElseIf cont > 0 Then
            bndreportetec = False
            LocOp = 350
            FrmSelFechas.Show()
            Me.Close()
        End If
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub cbxTipServ_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxTipServ.SelectedIndexChanged

    End Sub
End Class