Imports System.Data.SqlClient
Public Class FrmCatalogoSeries

    Private Clave As String = Nothing
    Private Serie As String = Nothing
    Private NumeroFolios As String = Nothing
    Private UltimoFolio As String = Nothing
    Private Vendedor As String = Nothing

    Private Sub DameDatosBitacora()
        Try
            If opcion = "M" Then
                Clave = Me.ClaveTextBox.Text
                Serie = Me.SerieTextBox.Text
                NumeroFolios = Me.Folios_ImpresosTextBox.Text
                UltimoFolio = Me.UltimoFolio_UsadoTextBox.Text
                Vendedor = Me.ComboBox1.Text
            End If


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub GuardaDatosBitacora(ByVal Op As Integer)
        If op = 0 Then
            If opcion = "M" Then
                bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Clave", Clave, Me.ClaveTextBox.Text, LocClv_Ciudad)
                bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Serie", Serie, Me.SerieTextBox.Text, LocClv_Ciudad)
                bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "N�mero De Folios Impresos", NumeroFolios, Me.Folios_ImpresosTextBox.Text, LocClv_Ciudad)
                bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "�ltimo Folio Usado", UltimoFolio, Me.UltimoFolio_UsadoTextBox.Text, LocClv_Ciudad)
                bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Vendedor", Vendedor, Me.ComboBox1.Text, LocClv_Ciudad)
            ElseIf opcion = "N" Then
                bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Nueva Serie", "", "Serie:" + Me.SerieTextBox.Text + " / " + "Vendedor:" + Me.ComboBox1.Text, LocClv_Ciudad)
            End If
        ElseIf op = 1 Then
            bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Se Elimin� Serie", "Serie:" + Me.SerieTextBox.Text, "", LocClv_Ciudad)
        End If
    End Sub

    Private Sub FrmCatalogoSeries_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        Me.MUESTRAVENDORESTableAdapter.Connection = CON
        Me.MUESTRAVENDORESTableAdapter.Fill(Me.NewsoftvDataSet1.MUESTRAVENDORES, 0)
        If opcion = "N" Then
            Me.CONCatalogoSeriesBindingSource.AddNew()
        ElseIf opcion = "C" Then
            Me.ToolStripContainer1.Enabled = False
            BUSCA()
        ElseIf opcion = "M" Then
            BUSCA()
        End If
        CON.Close()
        DameDatosBitacora()
    End Sub

    Private Sub CONCatalogoSeriesBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONCatalogoSeriesBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.ComboBox1.SelectedValue) = False Then
            MsgBox("Seleccione un Vendedor ", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Len(Trim(Me.SerieTextBox.Text)) = 0 Then
            MsgBox("Tiene que Capturar la Serie ", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.UltimoFolio_UsadoTextBox.Text) = False Then
            Me.UltimoFolio_UsadoTextBox.Text = 0
            'MsgBox("Tiene que Capturar el Utlimo Folio Usado ", MsgBoxStyle.Information)
            'Exit Sub
        End If
        If IsNumeric(Me.Folios_ImpresosTextBox.Text) = False Then
            Me.Folios_ImpresosTextBox.Text = 0
            'MsgBox("Tiene que Capturar el Numero de Folios Impresos ", MsgBoxStyle.Information)
            'Exit Sub
        End If
        If CInt(Me.UltimoFolio_UsadoTextBox.Text) > CInt(Me.Folios_ImpresosTextBox.Text) And (CInt(Me.UltimoFolio_UsadoTextBox.Text) > 0 And CInt(Me.Folios_ImpresosTextBox.Text) > 0) Then
            MsgBox("El Numero de Folios Impresos tiene que ser mayor que el Utlimo Folio Usado ", MsgBoxStyle.Information)
            Exit Sub
        End If
            Dim MSG As String = Nothing
            Dim NUMERROR As Integer = 0
            Me.VALIDACatalogoSeriesTableAdapter.Connection = CON
            Me.VALIDACatalogoSeriesTableAdapter.Fill(Me.NewsoftvDataSet1.VALIDACatalogoSeries, gloClave, Me.SerieTextBox.Text, Me.UltimoFolio_UsadoTextBox.Text, Me.ComboBox1.SelectedValue, opcion, MSG, NUMERROR)
            If NUMERROR = 0 Then
                Me.Validate()
                Me.CONCatalogoSeriesBindingSource.EndEdit()
                Me.CONCatalogoSeriesTableAdapter.Connection = CON
                Me.CONCatalogoSeriesTableAdapter.Update(Me.NewsoftvDataSet1.CONCatalogoSeries)
                GloBnd = True
                GuardaDatosBitacora(0)
                MsgBox(mensaje5)
                Me.Close()
            Else

                MsgBox(MSG)
            End If
            CON.Close()

    End Sub

    Private Sub BUSCA()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.CONCatalogoSeriesTableAdapter.Connection = CON
            Me.CONCatalogoSeriesTableAdapter.Fill(Me.NewsoftvDataSet1.CONCatalogoSeries, gloClave)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.ClaveTextBox.Text) = True Then
            GuardaDatosBitacora(1)
            Me.CONCatalogoSeriesTableAdapter.Connection = CON
            Me.CONCatalogoSeriesTableAdapter.Delete(Me.ClaveTextBox.Text)
            GloBnd = True
            MsgBox(mensaje6)
            Me.Close()
        End If
        CON.Close()
    End Sub

    Private Sub VALIDA()
        Try

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
End Class