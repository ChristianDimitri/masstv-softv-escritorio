﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMuestraIFE
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgvIFE = New System.Windows.Forms.DataGridView()
        Me.Contrato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IFE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.bnGuardar = New System.Windows.Forms.Button()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.bnRechazar = New System.Windows.Forms.Button()
        CType(Me.dgvIFE, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvIFE
        '
        Me.dgvIFE.AllowUserToAddRows = False
        Me.dgvIFE.AllowUserToDeleteRows = False
        Me.dgvIFE.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvIFE.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvIFE.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvIFE.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Contrato, Me.Nombre, Me.IFE})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvIFE.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvIFE.Location = New System.Drawing.Point(163, 38)
        Me.dgvIFE.Name = "dgvIFE"
        Me.dgvIFE.ReadOnly = True
        Me.dgvIFE.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvIFE.Size = New System.Drawing.Size(794, 291)
        Me.dgvIFE.TabIndex = 0
        '
        'Contrato
        '
        Me.Contrato.DataPropertyName = "Contrato"
        Me.Contrato.HeaderText = "Contrato"
        Me.Contrato.Name = "Contrato"
        Me.Contrato.ReadOnly = True
        Me.Contrato.Width = 120
        '
        'Nombre
        '
        Me.Nombre.DataPropertyName = "Nombre"
        Me.Nombre.HeaderText = "Nombre"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.ReadOnly = True
        Me.Nombre.Width = 400
        '
        'IFE
        '
        Me.IFE.DataPropertyName = "IFE"
        Me.IFE.HeaderText = "IFE"
        Me.IFE.Name = "IFE"
        Me.IFE.ReadOnly = True
        Me.IFE.Width = 200
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.ForeColor = System.Drawing.Color.Maroon
        Me.CMBLabel2.Location = New System.Drawing.Point(440, 344)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(281, 24)
        Me.CMBLabel2.TabIndex = 22
        Me.CMBLabel2.Text = "¿Desea Agregar El Contrato?"
        '
        'bnGuardar
        '
        Me.bnGuardar.BackColor = System.Drawing.Color.Olive
        Me.bnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.bnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnGuardar.ForeColor = System.Drawing.Color.White
        Me.bnGuardar.Location = New System.Drawing.Point(391, 383)
        Me.bnGuardar.Name = "bnGuardar"
        Me.bnGuardar.Size = New System.Drawing.Size(177, 40)
        Me.bnGuardar.TabIndex = 21
        Me.bnGuardar.Text = "&GUARDAR"
        Me.bnGuardar.UseVisualStyleBackColor = False
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.Maroon
        Me.CMBLabel1.Location = New System.Drawing.Point(159, 15)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(245, 20)
        Me.CMBLabel1.TabIndex = 20
        Me.CMBLabel1.Text = "Contratos Con La Misma IFE:"
        '
        'bnRechazar
        '
        Me.bnRechazar.BackColor = System.Drawing.Color.Olive
        Me.bnRechazar.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.bnRechazar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnRechazar.ForeColor = System.Drawing.Color.White
        Me.bnRechazar.Location = New System.Drawing.Point(603, 383)
        Me.bnRechazar.Name = "bnRechazar"
        Me.bnRechazar.Size = New System.Drawing.Size(165, 40)
        Me.bnRechazar.TabIndex = 19
        Me.bnRechazar.Text = "&RECHAZAR"
        Me.bnRechazar.UseVisualStyleBackColor = False
        '
        'FrmMuestraIFE
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1116, 452)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.bnGuardar)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.bnRechazar)
        Me.Controls.Add(Me.dgvIFE)
        Me.Name = "FrmMuestraIFE"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Contratos con la misma IFE"
        CType(Me.dgvIFE, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvIFE As System.Windows.Forms.DataGridView
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents bnGuardar As System.Windows.Forms.Button
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents bnRechazar As System.Windows.Forms.Button
    Friend WithEvents Contrato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IFE As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
