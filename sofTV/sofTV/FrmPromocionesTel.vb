Imports System.Data.SqlClient
Imports System.Data
Imports System.Text
Public Class FrmPromocionesTel
    Private Descripcion As String = Nothing
    Private nomesesaplica As String = Nothing
    Private descuento As String = Nothing
    Private activa As String = Nothing
    Private fechaini As String = Nothing
    Private fechafin As String = Nothing

    Private Sub Llena_Tablas_Servicios(ByVal clv_tipser As Integer)
        Dim CON100 As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Try

            cmd = New SqlClient.SqlCommand()
            CON100.Open()
            With cmd
                .CommandText = "Muestra_Servicios_Promociones"
                .Connection = CON100
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                '@clv_promocion bigint,@clv_tipser int

                Dim prm As New SqlParameter("@clv_promocion", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = CLng(Me.CLAVETextBox.Text)
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@clv_tipser", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = clv_tipser
                .Parameters.Add(prm1)

                Dim reader As SqlDataReader = .ExecuteReader()

                Select Case clv_tipser
                    Case 2

                        Me.DGVServInt.Rows.Clear()
                        While reader.Read()
                            Me.DGVServInt.Rows.Add(reader(0), reader(1), reader(2), reader(3))
                        End While
                        Me.DGVServInt.Columns(0).Visible = False
                        Me.DGVServInt.Columns(1).Visible = False
                        Me.DGVServInt.Columns(3).Visible = False

                    Case 5

                        Me.DGVServTel.Rows.Clear()
                        While reader.Read()
                            Me.DGVServTel.Rows.Add(reader(0), reader(1), reader(2), reader(3))
                        End While
                        Me.DGVServTel.Columns(0).Visible = False
                        Me.DGVServTel.Columns(1).Visible = False
                        Me.DGVServTel.Columns(3).Visible = False

                    Case 0

                        Me.DGVCombo.Rows.Clear()
                        While reader.Read()
                            Me.DGVCombo.Rows.Add(reader(0), reader(1), reader(2), reader(3))
                        End While
                        Me.DGVCombo.Columns(0).Visible = False
                        Me.DGVCombo.Columns(1).Visible = False
                        Me.DGVCombo.Columns(3).Visible = False

                End Select
            End With
            CON100.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub GuardaDatos_PromocionesTel(ByVal clv_promocion As Long)
        Dim CON As New SqlClient.SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Dim cmd1 As New SqlClient.SqlCommand()
        Dim cmd2 As New SqlClient.SqlCommand()

        Try
            CON.Open()
            With cmd
                .CommandText = "Inserta_Rel_Promocion_Colonias_Ciudad"
                .Connection = CON
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@clv_promocion", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = CLng(clv_promocion)
                .Parameters.Add(prm)

                Dim i As Integer = cmd.ExecuteNonQuery()
            End With
            CON.Close()

            If (Me.CheckBox1.CheckState = CheckState.Checked) Then
                CON.Open()
                With cmd1
                    .CommandText = "Inserta_Rel_Promocion_MesesSinInte"
                    .Connection = CON
                    .CommandTimeout = 0
                    .CommandType = CommandType.StoredProcedure

                    Dim prm As New SqlParameter("@clv_promocion", SqlDbType.BigInt)
                    prm.Direction = ParameterDirection.Input
                    prm.Value = CLng(clv_promocion)
                    .Parameters.Add(prm)

                    Dim i1 As Integer = cmd1.ExecuteNonQuery()
                End With
                CON.Close()
            End If

            If (Me.CheckBox2.CheckState = CheckState.Checked) Then
                'If IsNumeric(Me.TextBox1.Text) = False Then
                '    MsgBox("Por Favor Capture el Porcentaje Inicial", MsgBoxStyle.Information)
                '    Exit Sub
                'ElseIf IsNumeric(Me.TextBox1.Text) = True Then
                '    If CLng(Me.TextBox1.Text) > 100 Then
                '        MsgBox("El Porcentaje Inicial No Debe Ser Mayor A 100%", MsgBoxStyle.Information)
                '        Exit Sub
                '    End If
                'End If

                CON.Open()
                With cmd2
                    .CommandText = "Inserta_Rel_Promocion_NoPagos"
                    .Connection = CON
                    .CommandTimeout = 0
                    .CommandType = CommandType.StoredProcedure

                    Dim prm As New SqlParameter("@clv_promocion", SqlDbType.BigInt)
                    prm.Direction = ParameterDirection.Input
                    prm.Value = CLng(clv_promocion)
                    .Parameters.Add(prm)

                    'Dim prm1 As New SqlParameter("@porcentajeinicial", SqlDbType.Int)
                    'prm1.Direction = ParameterDirection.Input
                    'prm1.Value = CInt(Me.TextBox1.Text)
                    '.Parameters.Add(prm1)

                    'Dim prm2 As New SqlParameter("@resto", SqlDbType.Int)
                    'prm2.Direction = ParameterDirection.Input
                    'prm2.Value = CLng(Me.NumericUpDown2.Value)
                    '.Parameters.Add(prm2)

                    'Dim prm3 As New SqlParameter("@fecha1", SqlDbType.DateTime)
                    'prm3.Direction = ParameterDirection.Input
                    'prm3.Value = Me.DateTimePicker3.Value
                    '.Parameters.Add(prm3)

                    'Dim prm4 As New SqlParameter("@fecha2", SqlDbType.DateTime)
                    'prm4.Direction = ParameterDirection.Input
                    'prm4.Value = Me.DateTimePicker4.Value
                    '.Parameters.Add(prm4)

                    Dim i2 As Integer = cmd2.ExecuteNonQuery()
                End With
                CON.Close()
            End If
            guardabitacora(2)
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CONTipoPromocionBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONTipoPromocionBindingNavigatorSaveItem.Click
        Dim activo As Boolean = False
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.CONTipoPromocionBindingSource.EndEdit()
        Me.CONTipoPromocionTableAdapter2.Connection = CON
        Me.CONTipoPromocionTableAdapter2.Update(Me.NewSofTvDataSet2.CONTipoPromocion)
        CON.Close()
        guardabitacora(0)
        MsgBox(mensaje5)
        If IsNumeric(Me.CLAVETextBox.Text) = True Then
            Try
                GuardaDatos_PromocionesTel(Me.CLAVETextBox.Text)
            Catch ex As Exception
                System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try
        End If
        GloBnd = True
        Me.Panel2.Enabled = True
        Me.Panel7.Enabled = True
        Me.Panel8.Enabled = True
        Me.Panel6.Enabled = True
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.CONTipoPromocionTableAdapter2.Connection = CON
        Me.CONTipoPromocionTableAdapter2.Delete(gloClave)
        Borra_REl_promociones(gloClave)
        CON.Close()
        guardabitacora(1)
        GloBnd = True
        Me.Close()
    End Sub
    Private Sub guardabitacora(ByVal op As Integer)
        Try
            Dim validacion1 As String = Nothing

            Select Case op
                Case 0
                    If opcion = "M" Then
                        'Descripcion = Me.DESCRIPCIONTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, "Promociones Tel�fonia", "Descripci�n Promoci�n", Descripcion, Me.DESCRIPCIONTextBox.Text, LocClv_Ciudad)
                    ElseIf opcion = "N" Then
                        bitsist(GloUsuario, 0, LocGloSistema, "Promociones Tel�fonia", "Se Agreg� Una Promoci�n", "", "Se Agreg� Una Promoci�n: " + Me.DESCRIPCIONTextBox.Text, LocClv_Ciudad)
                    End If
                Case 1
                    bitsist(GloUsuario, 0, LocGloSistema, "Promociones Tel�fonia", "Se Elimino Una Promoci�n", "", "Se Elimino Una Promoci�n: " + Me.DESCRIPCIONTextBox.Text, LocClv_Ciudad)
                Case 2
                    If opcion = "M" Then
                        bitsist(GloUsuario, 0, LocGloSistema, "Promociones Tel�fonia", "Se Modificaron Los Datos De La Promoci�n:", " ", Me.DESCRIPCIONTextBox.Text, LocClv_Ciudad)
                    ElseIf opcion = "N" Then
                        bitsist(GloUsuario, 0, LocGloSistema, "Promociones Tel�fonia", "Se Dieron De Alta Los Datos De La Promoci�n:", " ", Me.DESCRIPCIONTextBox.Text, LocClv_Ciudad)
                    End If
            End Select

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub damedatosbitacora()
        Try
            'If opcion = "M" Then
            '    Descripcion = Me.DESCRIPCIONTextBox.Text
            '    nomesesaplica = CStr(Me.MesesAplicaNumericUpDown.Value)
            '    descuento = Me.DescuentoTextBox.Text
            '    If Me.ActivoCheckBox.CheckState = CheckState.Checked Then
            '        activa = "True"
            '    Else
            '        activa = "False"
            '    End If
            '    fechaini = Me.Fecha1DateTimePicker.Text
            '    fechafin = Me.Fecha2DateTimePicker.Text
            'End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FrmPromocionesTel_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Dim con As New SqlConnection(MiConexion)
        If opcion = "N" Then
            Me.CONTipoPromocionBindingSource.AddNew()
            Panel1.Enabled = True
            con.Open()
            Me.Muestra_ciudadTableAdapter.Connection = con
            Me.Muestra_ciudadTableAdapter.Fill(Me.Procedimientosarnoldo4.Muestra_ciudad)
            con.Close()
            Me.ComboBox1.Text = ""
            Llena_tabs_New()
        ElseIf opcion = "C" Then
            Panel1.Enabled = False
            busca(gloClave)
            con.Open()
            Me.Muestra_ciudadTableAdapter.Connection = con
            Me.Muestra_ciudadTableAdapter.Fill(Me.Procedimientosarnoldo4.Muestra_ciudad)
            con.Close()
            Me.ComboBox1.Text = ""
            Me.Panel2.Enabled = False
            Me.Panel7.Enabled = False
            Me.Panel8.Enabled = False
            Me.Panel6.Enabled = False
        ElseIf opcion = "M" Then
            Panel1.Enabled = True
            busca(gloClave)
            con.Open()
            Me.Muestra_ciudadTableAdapter.Connection = con
            Me.Muestra_ciudadTableAdapter.Fill(Me.Procedimientosarnoldo4.Muestra_ciudad)
            con.Close()
            Me.ComboBox1.Text = ""
            Me.Panel2.Enabled = True
            Me.Panel7.Enabled = True
            Me.Panel8.Enabled = True
            Me.Panel6.Enabled = True
            Me.BindingNavigatorDeleteItem.Enabled = True
        End If
        Me.Clv_TipSerTextBox.Text = GloClv_TipSer
    End Sub

    Private Sub busca(ByVal clave As Integer)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.CONTipoPromocionTableAdapter2.Connection = CON
            Me.CONTipoPromocionTableAdapter2.Fill(Me.NewSofTvDataSet2.CONTipoPromocion, New System.Nullable(Of Integer)(CType(clave, Integer)))
            RefrescaGrid(0)
            Llena_temportales(clave)
            Llena_tabs_New()
            'Me.Consulta_Rel_Promocion_Meses_AplicaTableAdapter.Connection = CON
            'Me.Consulta_Rel_Promocion_Meses_AplicaTableAdapter.Fill(Me.ProcedimientosArnoldo2.Consulta_Rel_Promocion_Meses_Aplica, clave)
            damedatosbitacora()
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub DESCRIPCIONTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles DESCRIPCIONTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(DESCRIPCIONTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    'Private Sub DescuentoTextBox_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
    '    e.KeyChar = Chr((ValidaKey(Me.DescuentoTextBox, Asc(LCase(e.KeyChar)), "L")))
    'End Sub

    'Private Sub Fecha1DateTimePicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Me.Fecha2DateTimePicker.MinDate = Me.Fecha1DateTimePicker.Value
    'End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub CLAVETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CLAVETextBox.TextChanged
        gloClave = Me.CLAVETextBox.Text
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Me.CONTipoPromocionBindingSource.CancelEdit()
        GloBnd = True
        Me.Close()
    End Sub


    Private Sub CheckBox2_CheckedChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If Me.CheckBox2.CheckState = CheckState.Checked Then
            Me.Panel5.Enabled = True
        Else
            Me.Panel5.Enabled = False
        End If
    End Sub

    Private Sub CheckBox1_CheckedChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If Me.CheckBox1.CheckState = CheckState.Checked Then
            Me.Panel3.Enabled = True
        Else
            Me.Panel3.Enabled = False
        End If
    End Sub

    Private Sub ToolStripButton4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If IsNumeric(Me.CLAVETextBox.Text) = True Then
            Try
                GuardaDatos_PromocionesTel(Me.CLAVETextBox.Text)
            Catch ex As Exception
                System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub Button2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBButton3.Click
        Dim con1 As New SqlClient.SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Dim errors As Integer = Nothing
        Try
            If Len(Me.ComboBox1.Text) = 0 Then
                MsgBox("Seleccine Por Favor La Ciudad Para La Promoci�n", MsgBoxStyle.Information)
                Exit Sub
            End If

            If Len(Me.ComboBox2.Text) = 0 Then
                MsgBox("Seleccione Por Favor La Colonia Para La Promoci�n", MsgBoxStyle.Information)
                Exit Sub
            End If

            con1.Open()
            '(@clv_promocion bigint,@clv_ciudad bigint,@clv_colonia bigint,@error bigint output)
            With cmd
                .CommandText = "Inserta_Rel_Promocion_Colonias_Ciudad_tmp_Ind"
                .Connection = con1
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@clv_promocion", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = CLng(Me.CLAVETextBox.Text)
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@clv_ciudad", SqlDbType.BigInt)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = CLng(Me.ComboBox1.SelectedValue)
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@clv_colonia", SqlDbType.BigInt)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = CLng(Me.ComboBox2.SelectedValue)
                .Parameters.Add(prm2)

                Dim prm3 As New SqlParameter("@error", SqlDbType.Int)
                prm3.Direction = ParameterDirection.Output
                prm3.Value = 0
                .Parameters.Add(prm3)

                Dim x As Integer = cmd.ExecuteNonQuery()

                errors = prm3.Value
            End With
            con1.Close()

            If (errors) > 0 Then
                MsgBox("La Colonia Ya Existe En Esta Promocion", MsgBoxStyle.Information)
            Else
                RefrescaGrid(0)
            End If

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub DateTimePicker2_ValueChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker2.ValueChanged
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
    End Sub

    Private Sub DateTimePicker4_ValueChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker4.ValueChanged
        Me.DateTimePicker4.MinDate = Me.DateTimePicker3.Value
    End Sub

    Private Sub TextBox1_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox1, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBButton4.Click
        Dim Con20 As New SqlClient.SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Try
            If IsNumeric(Me.DataGridView2.SelectedCells(0).Value) = True Then
                Con20.Open()
                '@clv_promocion bigint,@clv_ciudad bigint,@clv_colonia bigint)
                With cmd
                    .CommandText = "Borra_Rel_Promocion_Colonias_Ciudad_tmp"
                    .Connection = Con20
                    .CommandTimeout = 0
                    .CommandType = CommandType.StoredProcedure

                    Dim prm As New SqlParameter("@clv_promocion", SqlDbType.BigInt)
                    prm.Direction = ParameterDirection.Input
                    prm.Value = CLng(Me.DataGridView2.SelectedCells(0).Value)
                    .Parameters.Add(prm)

                    Dim prm1 As New SqlParameter("@clv_ciudad", SqlDbType.BigInt)
                    prm1.Direction = ParameterDirection.Input
                    prm1.Value = CLng(Me.DataGridView2.SelectedCells(1).Value)
                    .Parameters.Add(prm1)

                    Dim prm2 As New SqlParameter("@clv_colonia", SqlDbType.BigInt)
                    prm2.Direction = ParameterDirection.Input
                    prm2.Value = CLng(Me.DataGridView2.SelectedCells(2).Value)
                    .Parameters.Add(prm2)

                    Dim yt As Integer = cmd.ExecuteNonQuery()
                End With
                Con20.Close()
                RefrescaGrid(0)
            Else
                MsgBox("No Se Puede Eliminar Los Datos En Blanco", MsgBoxStyle.Information)
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Dim con As New SqlClient.SqlConnection(MiConexion)
        If Len(Me.ComboBox1.Text) > 0 Then
            con.Open()
            Me.Muestra_colonias_segun_CiudadTableAdapter.Connection = con
            Me.Muestra_colonias_segun_CiudadTableAdapter.Fill(Me.Procedimientosarnoldo4.Muestra_colonias_segun_Ciudad, CLng(Me.ComboBox1.SelectedValue))
            con.Close()
        End If
    End Sub
    Private Sub RefrescaGrid(ByVal op As Integer)
        Dim con As New SqlClient.SqlConnection(MiConexion)
        Select Case op
            Case 0
                If IsNumeric(Me.CLAVETextBox.Text) = True Then
                    con.Open()
                    Me.Muestra_Rel_Promocion_Colonias_Ciudad_tmpTableAdapter.Connection = con
                    Me.Muestra_Rel_Promocion_Colonias_Ciudad_tmpTableAdapter.Fill(Me.Procedimientosarnoldo4.Muestra_Rel_Promocion_Colonias_Ciudad_tmp, CLng(Me.CLAVETextBox.Text))
                    con.Close()
                End If
            Case 1
                If IsNumeric(Me.CLAVETextBox.Text) = True Then
                    'con.Open()
                    'Me.Muestra_Rel_Promocion_MesesSinInte_TmpTableAdapter.Connection = con
                    'Me.Muestra_Rel_Promocion_MesesSinInte_TmpTableAdapter.Fill(Me.Procedimientosarnoldo4.Muestra_Rel_Promocion_MesesSinInte_Tmp, CLng(Me.CLAVETextBox.Text))
                    'con.Close()
                    MuestraRelPromocionMesesSinInte(CLng(Me.CLAVETextBox.Text))
                End If
            Case 2
                If IsNumeric(Me.CLAVETextBox.Text) = True Then
                    con.Open()
                    Me.Muestra_Rel_Promocion_Colonias_Ciudad_tmpTableAdapter.Connection = con
                    Me.Muestra_Rel_Promocion_Colonias_Ciudad_tmpTableAdapter.Fill(Me.Procedimientosarnoldo4.Muestra_Rel_Promocion_Colonias_Ciudad_tmp, CLng(Me.CLAVETextBox.Text))
                    'Me.Muestra_Rel_Promocion_MesesSinInte_TmpTableAdapter.Connection = con
                    'Me.Muestra_Rel_Promocion_MesesSinInte_TmpTableAdapter.Fill(Me.Procedimientosarnoldo4.Muestra_Rel_Promocion_MesesSinInte_Tmp, CLng(Me.CLAVETextBox.Text))
                    MuestraRelPromocionMesesSinInte(CLng(Me.CLAVETextBox.Text))
                    Me.Muestra_Rel_Promocion_NoPagos_tmpTableAdapter.Connection = con
                    Me.Muestra_Rel_Promocion_NoPagos_tmpTableAdapter.Fill(Me.Procedimientosarnoldo4.Muestra_Rel_Promocion_NoPagos_tmp, CLng(Me.CLAVETextBox.Text))
                    con.Close()
                End If
            Case 3
                If IsNumeric(Me.CLAVETextBox.Text) = True Then
                    con.Open()
                    Me.Muestra_Rel_Promocion_NoPagos_tmpTableAdapter.Connection = con
                    Me.Muestra_Rel_Promocion_NoPagos_tmpTableAdapter.Fill(Me.Procedimientosarnoldo4.Muestra_Rel_Promocion_NoPagos_tmp, CLng(Me.CLAVETextBox.Text))
                    con.Close()
                End If
        End Select
    End Sub
    Private Sub Borra_REl_promociones(ByVal clv_promocion As Long)
        Dim CON70 As New SqlClient.SqlConnection(MiConexion)
        Dim Cmd As New SqlClient.SqlCommand()
        Try
            CON70.Open()
            With Cmd
                .CommandText = "Borra_Rel_Promociones"
                .Connection = CON70
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0

                Dim prm As New SqlParameter("@clv_promocion", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = clv_promocion
                .Parameters.Add(prm)

                Dim q As Integer = Cmd.ExecuteNonQuery()

            End With
            CON70.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub CMBButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBButton1.Click
        Dim CON As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Dim errors As Integer = Nothing
        Try
            CON.Open()
            '(@clv_promocion bigint ,@numeroMeses int,@fecha1 varchar(20),@fecha2 varchar(20),@error int output)
            With cmd
                .CommandText = "Agregar_Rel_Promocion_MesesSinInte_Tmp"
                .Connection = CON
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@clv_promocion", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = CLng(Me.CLAVETextBox.Text)
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@numeroMeses", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = CLng(Me.NumericUpDown1.Value)
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@fecha1", SqlDbType.DateTime)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = Me.DateTimePicker1.Value
                .Parameters.Add(prm2)

                Dim prm3 As New SqlParameter("@fecha2", SqlDbType.DateTime)
                prm3.Direction = ParameterDirection.Input
                prm3.Value = Me.DateTimePicker2.Value
                .Parameters.Add(prm3)


                Dim prm4 As New SqlParameter("@error", SqlDbType.Int)
                prm4.Direction = ParameterDirection.Output
                prm4.Value = 0
                .Parameters.Add(prm4)

                Dim prm5 As New SqlParameter("@NoMesesForzoso", SqlDbType.Int)
                prm5.Direction = ParameterDirection.Input
                prm5.Value = CLng(Me.NumericUpDown3.Value)
                .Parameters.Add(prm5)

                Dim wx As Integer = cmd.ExecuteNonQuery()
                errors = prm4.Value
            End With
            CON.Close()
            If errors > 0 Then
                MsgBox("No Se Puede Agregar Ese Mes Ya Que Ya Existe En La Lista", MsgBoxStyle.Information)
            Else
                RefrescaGrid(1)
            End If

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CMBButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBButton2.Click
        Dim CON70 As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Try
            'If IsNumeric(Me.DataGridView1.SelectedCells(0).Value) = True Then
            ' MsgBox(Me.DataGridView1.SelectedCells.Item(1).Value)
            If Me.DataGridView1.RowCount() > 0 Then
                CON70.Open()
                '@clv_promocion bigint,@num_meses int
                With cmd
                    .CommandText = "Borra_Rel_Promocion_MesesSinInte_Tmp"
                    .Connection = CON70
                    .CommandTimeout = 0
                    .CommandType = CommandType.StoredProcedure

                    Dim prm As New SqlParameter("@clv_promocion", SqlDbType.BigInt)
                    prm.Direction = ParameterDirection.Input
                    prm.Value = CLng(Me.DataGridView1.SelectedCells(0).Value)
                    .Parameters.Add(prm)

                    Dim prm1 As New SqlParameter("@num_meses", SqlDbType.Int)
                    prm1.Direction = ParameterDirection.Input
                    prm1.Value = CLng(Me.DataGridView1.SelectedCells(1).Value)
                    .Parameters.Add(prm1)

                    Dim iu As Integer = cmd.ExecuteNonQuery()
                End With
                CON70.Close()
                RefrescaGrid(1)
            Else
                MsgBox("No Se Pueden Eliminar Datos En Blanco", MsgBoxStyle.Information)
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Llena_temportales(ByVal clv_promocion As Long)
        Dim CON80 As New SqlClient.SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Dim valida As Integer = Nothing
        Dim valida1 As Integer = Nothing
        Try
            ''cmd = New SqlCommand()
            ''CON80.Open()
            ''With cmd
            ''    .CommandText = ""
            ''    .Connection = CON80
            ''    .CommandTimeout = 0
            ''    .CommandType = CommandType.StoredProcedure

            ''    Dim prm As New SqlParameter("", )


            ''End With



            'CON80.Close()

            cmd = New SqlCommand()
            CON80.Open()
            '(@clv_promocion bigint,@error int output,@error1 int output)
            With cmd
                .CommandText = "Checa_Que_Tiene_Promocion"
                .Connection = CON80
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0

                Dim prm As New SqlParameter("@clv_promocion", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = clv_promocion
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@error", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@error1", SqlDbType.Int)
                prm2.Direction = ParameterDirection.Output
                prm2.Value = 0
                .Parameters.Add(prm2)

                Dim e As Integer = cmd.ExecuteNonQuery()
                valida = prm1.Value
                valida1 = prm2.Value
            End With
            CON80.Close()

            If valida = 1 Then 'tiene Meses sin intereses
                Me.CheckBox1.Checked = True
            End If
            If valida1 = 1 Then 'Tiene Pagos
                Me.CheckBox2.Checked = True
            End If

            cmd = New SqlClient.SqlCommand()
            CON80.Open()
            With cmd
                .CommandText = "Llena_Rel_Promocion_MesesSinInte_Tmp"
                .Connection = CON80
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim Prm As New SqlParameter("@clv_promocion", SqlDbType.BigInt)
                Prm.Direction = ParameterDirection.Input
                Prm.Value = clv_promocion
                .Parameters.Add(Prm)
                Dim i As Integer = cmd.ExecuteNonQuery()
            End With
            CON80.Close()
            RefrescaGrid(1)


            If Me.CheckBox2.CheckState = CheckState.Checked Then
                cmd = New SqlClient.SqlCommand()
                '@clv_promocion int,@Porcentaje int output,@Resto int output,@Fecha1 datetime output,@Fecha2 datetime output
                CON80.Open()
                With cmd
                    .CommandText = "Llena_Rel_Promocion_NoPagos_tmp"
                    .Connection = CON80
                    .CommandTimeout = 0
                    .CommandType = CommandType.StoredProcedure

                    Dim prm As New SqlParameter("@clv_promocion", SqlDbType.BigInt)
                    prm.Direction = ParameterDirection.Input
                    prm.Value = clv_promocion
                    .Parameters.Add(prm)

                    'Dim prm1 As New SqlParameter("@Porcentaje", SqlDbType.Int)
                    'prm1.Direction = ParameterDirection.Output
                    'prm1.Value = 0
                    '.Parameters.Add(prm1)

                    'Dim prm2 As New SqlParameter("@Resto", SqlDbType.Int)
                    'prm2.Direction = ParameterDirection.Output
                    'prm2.Value = 0
                    '.Parameters.Add(prm2)

                    'Dim prm3 As New SqlParameter("@Fecha1", SqlDbType.DateTime)
                    'prm3.Direction = ParameterDirection.Output
                    'prm3.Value = 0
                    '.Parameters.Add(prm3)

                    'Dim prm4 As New SqlParameter("@Fecha2", SqlDbType.DateTime)
                    'prm4.Direction = ParameterDirection.Output
                    'prm4.Value = 0
                    '.Parameters.Add(prm4)

                    Dim i As Integer = cmd.ExecuteNonQuery()

                    'Me.TextBox1.Text = prm1.Value
                    'Me.NumericUpDown2.Value = prm2.Value
                    'Me.DateTimePicker3.Value = prm3.Value
                    'Me.DateTimePicker4.Value = prm4.Value
                End With
                CON80.Close()
                RefrescaGrid(3)
            End If

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Llena_tabs_New()
        Dim i As Integer = 0
        If opcion = "M" Or opcion = "C" Then
            While i <= 2
                If i = 0 Then
                    'Llena Combos tabs Internet 
                    Llena_Combos(2)
                    'Lleno Grids Nuevos Internet
                    Llena_Tablas_Servicios(2)
                ElseIf i = 1 Then
                    'Llena Combos Telefonia
                    Llena_Combos(5)
                    'Lleno Grids Nuevos Telefonia
                    Llena_Tablas_Servicios(5)
                ElseIf i = 2 Then
                    'Llena Combos De Combos
                    Llena_Combos(0)
                    'Lleno Grids Nuevos Combos
                    Llena_Tablas_Servicios(0)
                End If
                i += 1
            End While
        ElseIf opcion = "N" Then
            While i <= 2
                If i = 0 Then
                    Llena_Combos(2)
                ElseIf i = 1 Then
                    'Llena Combos Telefonia
                    Llena_Combos(5)
                ElseIf i = 2 Then
                    'Llena Combos De Combos
                    Llena_Combos(0)
                End If
                i += 1
            End While
        End If
    End Sub

    Private Sub Borra_Servicios_Promociones(ByVal clv_servicio As Long, ByVal clv_tipser As Integer, ByVal op As Integer)
        '@clv_promocion bigint,@clv_servicio bigint,@clv_tipser int)
        Dim CON90 As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Try
            cmd = New SqlCommand()
            CON90.Open()
            With cmd
                .CommandText = "Borra_Servicios_Promociones"
                .Connection = CON90
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@clv_promocion", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = CLng(Me.CLAVETextBox.Text)
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@clv_servicio", SqlDbType.BigInt)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = clv_servicio
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@clv_tipser", SqlDbType.Int)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = clv_tipser
                .Parameters.Add(prm2)

                Dim reader As SqlDataReader = .ExecuteReader()

                Select Case op
                    Case 1
                        Me.DGVServInt.Rows.Clear()
                        While reader.Read()
                            Me.DGVServInt.Rows.Add(reader(0), reader(1), reader(2), reader(3))
                        End While
                        Me.DGVServInt.Columns(0).Visible = False
                        Me.DGVServInt.Columns(1).Visible = False
                        Me.DGVServInt.Columns(3).Visible = False
                    Case 2
                        Me.DGVServTel.Rows.Clear()
                        While reader.Read()
                            Me.DGVServTel.Rows.Add(reader(0), reader(1), reader(2), reader(3))
                        End While
                        Me.DGVServTel.Columns(0).Visible = False
                        Me.DGVServTel.Columns(1).Visible = False
                        Me.DGVServTel.Columns(3).Visible = False
                    Case 3
                        Me.DGVCombo.Rows.Clear()
                        While reader.Read()
                            Me.DGVCombo.Rows.Add(reader(0), reader(1), reader(2), reader(3))
                        End While
                        Me.DGVCombo.Columns(0).Visible = False
                        Me.DGVCombo.Columns(1).Visible = False
                        Me.DGVCombo.Columns(3).Visible = False
                End Select

            End With
            CON90.Close()


        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub Agregar_Servicios_Promociones(ByVal tipser As Integer, ByVal clv_servicio As Long, ByVal op As Integer)
        Dim error1 As Integer
        Dim CON80 As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Try

            cmd = New SqlCommand()
            CON80.Open()
            With cmd
                .CommandText = "Agregar_Servicios_Promociones"
                .CommandTimeout = 0
                .Connection = CON80
                .CommandType = CommandType.StoredProcedure
                '@clv_promocion bigint ,@clv_tipser int,@clv_servicio bigint,@error int output
                Dim prm As New SqlParameter("@clv_promocion", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = CLng(Me.CLAVETextBox.Text)
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@clv_tipser", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = tipser
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@clv_servicio", SqlDbType.BigInt)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = clv_servicio
                .Parameters.Add(prm2)

                Dim prm3 As New SqlParameter("@error", SqlDbType.Int)
                prm3.Direction = ParameterDirection.Output
                prm3.Value = 0
                .Parameters.Add(prm3)

                Dim reader As SqlDataReader = .ExecuteReader()
                error1 = prm3.Value()

                If error1 = 1 Then
                    MsgBox("El Servicio ya Existe Para Esta Promocion", MsgBoxStyle.Information)
                    Exit Sub
                ElseIf error1 = 0 Then
                    Select Case op
                        Case 1
                            Me.DGVServInt.Rows.Clear()
                            While reader.Read()
                                Me.DGVServInt.Rows.Add(reader(0), reader(1), reader(2), reader(3))
                            End While
                            Me.DGVServInt.Columns(0).Visible = False
                            Me.DGVServInt.Columns(1).Visible = False
                            Me.DGVServInt.Columns(3).Visible = False
                        Case 2

                            Me.DGVServTel.Rows.Clear()
                            While reader.Read()
                                Me.DGVServTel.Rows.Add(reader(0), reader(1), reader(2), reader(3))
                            End While
                            Me.DGVServTel.Columns(0).Visible = False
                            Me.DGVServTel.Columns(1).Visible = False
                            Me.DGVServTel.Columns(3).Visible = False

                        Case 3

                            Me.DGVCombo.Rows.Clear()
                            While reader.Read()
                                Me.DGVCombo.Rows.Add(reader(0), reader(1), reader(2), reader(3))
                            End While
                            Me.DGVCombo.Columns(0).Visible = False
                            Me.DGVCombo.Columns(1).Visible = False
                            Me.DGVCombo.Columns(3).Visible = False

                    End Select
                End If

            End With
            CON80.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub Llena_Combos(ByVal op As Integer)
        Dim CON20 As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlDataAdapter()
        Dim consulta As New StringBuilder
        If op <> 0 Then
            consulta.Append("EXEC MuestraServicios ")
        ElseIf op = 0 Then
            consulta.Append("EXEC Muestra_Combos_clContratan ")
        End If
        consulta.Append(CStr(op))
        'consulta = String.Concat("EXEC Muestra Servicios ", CStr(op))
        Try
            CON20.Open()
            cmd = New SqlDataAdapter(consulta.ToString(), CON20)

            Dim dt As New DataTable

            Dim bs As New BindingSource



            cmd.Fill(dt)
            bs.DataSource = dt

            If op = 2 Then
                Me.CmbBoxServInt.Items.Clear()
                With CmbBoxServInt
                    .DataSource = bs.DataSource
                End With
            ElseIf op = 5 Then
                Me.CmbBoxServTel.Items.Clear()
                With Me.CmbBoxServTel
                    .DataSource = bs.DataSource
                End With
            ElseIf op = 0 Then
                'MsgBox(consulta.ToString())
                Me.CmbBoxCombos.Items.Clear()
                With CmbBoxCombos
                    .DataSource = bs.DataSource
                End With
            End If
            CON20.Close()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CMBBtnPagosAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBBtnPagosAgregar.Click
        Dim CON90 As New SqlClient.SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Dim error01 As Integer = Nothing

        If IsNumeric(Me.TextBox1.Text) = True Then
            If CLng(Me.TextBox1.Text) = 0 Then
                MsgBox("El Porcentaje Inicial Debe Ser Mayor Que Cero", MsgBoxStyle.Information)
                Exit Sub
            End If
        End If

        Try
            '@clv_promocion bigint,@porcentaje_inicial decimal(18,2),@resto int,@fecha1 datetime,@fecha2 datetime,@error int output
            CON90.Open()
            With cmd
                .CommandText = "Agregar_Rel_Promocion_NoPagos_tmp"
                .Connection = CON90
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm1 As New SqlParameter("@clv_promocion", SqlDbType.BigInt)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = CLng(Me.CLAVETextBox.Text)
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@porcentaje_inicial", SqlDbType.Decimal)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = CLng(Me.TextBox1.Text)
                .Parameters.Add(prm2)

                Dim prm3 As New SqlParameter("@resto", SqlDbType.Int)
                prm3.Direction = ParameterDirection.Input
                prm3.Value = Me.NumericUpDown2.Value
                .Parameters.Add(prm3)

                Dim prm4 As New SqlParameter("@fecha1", SqlDbType.DateTime)
                prm4.Direction = ParameterDirection.Input
                prm4.Value = Me.DateTimePicker3.Value
                .Parameters.Add(prm4)

                Dim prm5 As New SqlParameter("@fecha2", SqlDbType.DateTime)
                prm5.Direction = ParameterDirection.Input
                prm5.Value = Me.DateTimePicker4.Value
                .Parameters.Add(prm5)

                Dim prm6 As New SqlParameter("@Plazo_Forzoso", SqlDbType.Int)
                prm6.Direction = ParameterDirection.Input
                prm6.Value = Me.NumericUpDown4.Value
                .Parameters.Add(prm6)

                Dim prm7 As New SqlParameter("@error", SqlDbType.Int)
                prm7.Direction = ParameterDirection.Output
                prm7.Value = 0
                .Parameters.Add(prm7)

                Dim i As Integer = cmd.ExecuteNonQuery()
                error01 = prm7.Value
            End With
            CON90.Close()

            If error01 > 0 Then
                MsgBox("Ya Existe Este Tipo De Pago", MsgBoxStyle.Information)
            End If
            RefrescaGrid(3)
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub



    Private Sub CMBBtnPagosEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBBtnPagosEliminar.Click
        Dim CON100 As New SqlClient.SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Try
            If Me.Muestra_Rel_Promocion_NoPagos_tmpDataGridView.RowCount() > 0 Then
                CON100.Open()
                With cmd
                    .CommandText = "Borra_Rel_Promocion_NoPagos_tmp_sel"
                    .Connection = CON100
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0

                    '@clv_promocion bigint,@porcentaje_ini decimal(18,2),@resto int,@fecha1 datetime,@fecha2 datetime
                    Dim prm As New SqlParameter("@clv_promocion", SqlDbType.BigInt)
                    prm.Direction = ParameterDirection.Input
                    prm.Value = CLng(Me.Muestra_Rel_Promocion_NoPagos_tmpDataGridView.SelectedCells(0).Value)
                    .Parameters.Add(prm)

                    Dim prm1 As New SqlParameter("@porcentaje_ini", SqlDbType.Decimal)
                    prm1.Direction = ParameterDirection.Input
                    prm1.Value = CLng(Me.Muestra_Rel_Promocion_NoPagos_tmpDataGridView.SelectedCells(1).Value)
                    .Parameters.Add(prm1)

                    Dim prm2 As New SqlParameter("@resto", SqlDbType.BigInt)
                    prm2.Direction = ParameterDirection.Input
                    prm2.Value = CLng(Me.Muestra_Rel_Promocion_NoPagos_tmpDataGridView.SelectedCells(2).Value)
                    .Parameters.Add(prm2)

                    Dim prm3 As New SqlParameter("@fecha1", SqlDbType.DateTime)
                    prm3.Direction = ParameterDirection.Input
                    prm3.Value = CDate(Me.Muestra_Rel_Promocion_NoPagos_tmpDataGridView.SelectedCells(3).Value)
                    .Parameters.Add(prm3)

                    Dim prm4 As New SqlParameter("@fecha2", SqlDbType.DateTime)
                    prm4.Direction = ParameterDirection.Input
                    prm4.Value = CDate(Me.Muestra_Rel_Promocion_NoPagos_tmpDataGridView.SelectedCells(4).Value)
                    .Parameters.Add(prm4)

                    Dim prm5 As New SqlParameter("@Plazo_Forsozo", SqlDbType.Int)
                    prm5.Direction = ParameterDirection.Input
                    prm5.Value = CLng(Me.Muestra_Rel_Promocion_NoPagos_tmpDataGridView.SelectedCells(5).Value)
                    .Parameters.Add(prm5)

                    Dim w As Integer = cmd.ExecuteNonQuery()
                End With
                CON100.Close()
                RefrescaGrid(3)
            Else
                MsgBox("No Hay Datos Que Eliminar", MsgBoxStyle.Information)
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub CMBBtnAddInt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBBtnAddInt.Click
        If IsNumeric(Me.CmbBoxServInt.SelectedValue) = True Then
            Me.Agregar_Servicios_Promociones(2, Me.CmbBoxServInt.SelectedValue, 1)
        End If
    End Sub

    Private Sub CMBBtnDelInt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBBtnDelInt.Click
        If IsNumeric(Me.DGVServInt.SelectedCells(1).Value) = True Then
            If IsNumeric(Me.DGVServInt.SelectedCells(3).Value) = True Then
                Borra_Servicios_Promociones(Me.DGVServInt.SelectedCells(1).Value, Me.DGVServInt.SelectedCells(3).Value, 1)
            End If
        End If
    End Sub

    Private Sub CMBBtnAddTel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBBtnAddTel.Click
        If IsNumeric(Me.CmbBoxServTel.SelectedValue) = True Then
            Me.Agregar_Servicios_Promociones(5, Me.CmbBoxServTel.SelectedValue, 2)
        End If
    End Sub

    Private Sub CMBBtnDelTel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBBtnDelTel.Click
        If Me.DGVServTel.Rows.Count() > 0 Then
            If IsNumeric(Me.DGVServTel.SelectedCells(1).Value) = True Then
                If IsNumeric(Me.DGVServTel.SelectedCells(3).Value) = True Then
                    Borra_Servicios_Promociones(Me.DGVServTel.SelectedCells(1).Value, Me.DGVServTel.SelectedCells(3).Value, 2)
                End If
            End If
        End If
    End Sub

    Private Sub CMBBtnAddCombo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBBtnAddCombo.Click
        If IsNumeric(Me.CmbBoxCombos.SelectedValue) = True Then
            Me.Agregar_Servicios_Promociones(0, Me.CmbBoxCombos.SelectedValue, 3)
        End If
    End Sub

    Private Sub CMBBtnDelCombo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBBtnDelCombo.Click
        If IsNumeric(Me.DGVCombo.SelectedCells(1).Value) = True Then
            If IsNumeric(Me.DGVCombo.SelectedCells(3).Value) = True Then
                Borra_Servicios_Promociones(Me.DGVCombo.SelectedCells(1).Value, Me.DGVCombo.SelectedCells(3).Value, 3)
            End If
        End If
    End Sub

    Private Sub MuestraRelPromocionMesesSinInte(ByVal Clv_Promocion As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC Muestra_Rel_Promocion_MesesSinInte_Tmp ")
        strSQL.Append(CStr(Clv_Promocion))

        Try
            conexion.Open()
            Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
            Dim dataTable As New DataTable
            Dim bindingSource As New BindingSource

            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.DataGridView1.DataSource = bindingSource
            conexion.Close()

            Me.DataGridView1.Columns(0).Visible = False
            Me.DataGridView1.Columns(1).Name = "No. De Meses"
            Me.DataGridView1.Columns(2).Name = "Fecha Inicial"
            Me.DataGridView1.Columns(3).Name = "Fecha Final"
            Me.DataGridView1.Columns(4).Name = "Meses Forzosos"

        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub
End Class