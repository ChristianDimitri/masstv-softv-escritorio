﻿Imports System.Data.SqlClient
Imports System.Text

Public Class FormMuestra_Que_Hize_Orden_Servicio

    Private Sub Usp_Muestra_Que_Hize_Orden_Servicio(ByVal eClv_Orden As Long, ByVal eClave As Long, ByVal eTrabajo As String, ByVal eClv_Trabajo As Integer, ByVal eOp As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder
        '// --@Clv_Orden bigint,@Clave int,@TRABAJO VARCHAR(50),@CLV_TRABAJO INT,@Op int
        StrSQL.Append("EXEC Usp_Muestra_Que_Hize_Orden_Servicio ")
        StrSQL.Append(CStr(eClv_Orden) & ",")
        StrSQL.Append(CStr(eClave) & ",")
        StrSQL.Append("'" & eTrabajo & "',")
        StrSQL.Append(CStr(eClv_Trabajo) & ",")
        StrSQL.Append(CStr(eOp))
        Dim DA As New SqlDataAdapter(StrSQL.ToString, CON)
        Dim DT As New DataTable
        Dim BS As New BindingSource
        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.DataGridView1.DataSource = BS.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub


    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub FormMuestra_Que_Hize_Orden_Servicio_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = 27 Then
            Me.Close()
        End If
    End Sub

    Private Sub FormMuestra_Que_Hize_Orden_Servicio_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Usp_Muestra_Que_Hize_Orden_Servicio(GloeClv_Orden, GloeClave, GloeTrabajo, GloeClv_Trabajo, GloeOp)
    End Sub

    Private Sub DataGridView1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DataGridView1.KeyDown
        If e.KeyCode = 27 Then
            Me.Close()
        End If
    End Sub
End Class