<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmClientesPorRecuperar
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim FechaSolicitudLabel As System.Windows.Forms.Label
        Dim FechaRecuperacionLabel As System.Windows.Forms.Label
        Dim UsuarioSolicitoLabel As System.Windows.Forms.Label
        Dim UsuarioRecuperoLabel As System.Windows.Forms.Label
        Dim DiasLabel As System.Windows.Forms.Label
        Dim RecuperoLabel As System.Windows.Forms.Label
        Dim ServicioLabel As System.Windows.Forms.Label
        Dim ConceptoLabel2 As System.Windows.Forms.Label
        Dim ContratoLabel As System.Windows.Forms.Label
        Dim ObservacionLabel As System.Windows.Forms.Label
        Dim NOMBRELabel As System.Windows.Forms.Label
        Dim CALLELabel As System.Windows.Forms.Label
        Dim COLONIALabel As System.Windows.Forms.Label
        Dim NUMEROLabel As System.Windows.Forms.Label
        Dim CIUDADLabel As System.Windows.Forms.Label
        Dim SOLOINTERNETLabel As System.Windows.Forms.Label
        Dim ESHOTELLabel As System.Windows.Forms.Label
        Dim TELEFONOLabel As System.Windows.Forms.Label
        Dim CELULARLabel As System.Windows.Forms.Label
        Dim EMAILLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmClientesPorRecuperar))
        Me.DataSetEric2 = New sofTV.DataSetEric2()
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.CONUSUARIOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONUSUARIOSTableAdapter = New sofTV.DataSetEricTableAdapters.CONUSUARIOSTableAdapter()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.NOMBRELabel2 = New System.Windows.Forms.Label()
        Me.BUSCLIPORCONTRATO3BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CALLELabel1 = New System.Windows.Forms.Label()
        Me.COLONIALabel1 = New System.Windows.Forms.Label()
        Me.NUMEROLabel1 = New System.Windows.Forms.Label()
        Me.CIUDADLabel1 = New System.Windows.Forms.Label()
        Me.SOLOINTERNETCheckBox = New System.Windows.Forms.CheckBox()
        Me.ESHOTELCheckBox = New System.Windows.Forms.CheckBox()
        Me.TELEFONOLabel1 = New System.Windows.Forms.Label()
        Me.CELULARLabel1 = New System.Windows.Forms.Label()
        Me.EMAILLabel1 = New System.Windows.Forms.Label()
        Me.ContratoTextBox = New System.Windows.Forms.TextBox()
        Me.MuestraClientePorRecuperarBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.NombreLabel1 = New System.Windows.Forms.Label()
        Me.ConceptoComboBox = New System.Windows.Forms.ComboBox()
        Me.MuestraServClientePorRecuperarBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ObservacionTextBox = New System.Windows.Forms.TextBox()
        Me.FechaSolicitudLabel2 = New System.Windows.Forms.Label()
        Me.PanelDias = New System.Windows.Forms.Panel()
        Me.DiasLabel2 = New System.Windows.Forms.Label()
        Me.PanelRecuperado = New System.Windows.Forms.Panel()
        Me.FechaRecuperacionLabel2 = New System.Windows.Forms.Label()
        Me.UsuarioRecuperoLabel2 = New System.Windows.Forms.Label()
        Me.PanelStatus = New System.Windows.Forms.Panel()
        Me.RecuperoLabel2 = New System.Windows.Forms.Label()
        Me.ConceptoLabel1 = New System.Windows.Forms.Label()
        Me.UsuarioSolicitoLabel2 = New System.Windows.Forms.Label()
        Me.DameSerDELCliBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameSerDELCliTableAdapter = New sofTV.DataSetEric2TableAdapters.dameSerDELCliTableAdapter()
        Me.MuestraServClientePorRecuperarTableAdapter = New sofTV.DataSetEric2TableAdapters.MuestraServClientePorRecuperarTableAdapter()
        Me.MuestraClientePorRecuperarTableAdapter = New sofTV.DataSetEric2TableAdapters.MuestraClientePorRecuperarTableAdapter()
        Me.ClaveTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_TipSerTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_UsuarioSolicitoTextBox = New System.Windows.Forms.TextBox()
        Me.SeRecuperoTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_UsuarioRecuperoTextBox = New System.Windows.Forms.TextBox()
        Me.ClienteTextBox = New System.Windows.Forms.TextBox()
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.BUSCLIPORCONTRATO3TableAdapter = New sofTV.DataSetEric2TableAdapters.BUSCLIPORCONTRATO3TableAdapter()
        Me.Button4 = New System.Windows.Forms.Button()
        FechaSolicitudLabel = New System.Windows.Forms.Label()
        FechaRecuperacionLabel = New System.Windows.Forms.Label()
        UsuarioSolicitoLabel = New System.Windows.Forms.Label()
        UsuarioRecuperoLabel = New System.Windows.Forms.Label()
        DiasLabel = New System.Windows.Forms.Label()
        RecuperoLabel = New System.Windows.Forms.Label()
        ServicioLabel = New System.Windows.Forms.Label()
        ConceptoLabel2 = New System.Windows.Forms.Label()
        ContratoLabel = New System.Windows.Forms.Label()
        ObservacionLabel = New System.Windows.Forms.Label()
        NOMBRELabel = New System.Windows.Forms.Label()
        CALLELabel = New System.Windows.Forms.Label()
        COLONIALabel = New System.Windows.Forms.Label()
        NUMEROLabel = New System.Windows.Forms.Label()
        CIUDADLabel = New System.Windows.Forms.Label()
        SOLOINTERNETLabel = New System.Windows.Forms.Label()
        ESHOTELLabel = New System.Windows.Forms.Label()
        TELEFONOLabel = New System.Windows.Forms.Label()
        CELULARLabel = New System.Windows.Forms.Label()
        EMAILLabel = New System.Windows.Forms.Label()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONUSUARIOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.BUSCLIPORCONTRATO3BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraClientePorRecuperarBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.MuestraServClientePorRecuperarBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelDias.SuspendLayout()
        Me.PanelRecuperado.SuspendLayout()
        Me.PanelStatus.SuspendLayout()
        CType(Me.DameSerDELCliBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        Me.SuspendLayout()
        '
        'FechaSolicitudLabel
        '
        FechaSolicitudLabel.AutoSize = True
        FechaSolicitudLabel.Location = New System.Drawing.Point(44, 67)
        FechaSolicitudLabel.Name = "FechaSolicitudLabel"
        FechaSolicitudLabel.Size = New System.Drawing.Size(110, 15)
        FechaSolicitudLabel.TabIndex = 10
        FechaSolicitudLabel.Text = "Fecha Solicitud:"
        '
        'FechaRecuperacionLabel
        '
        FechaRecuperacionLabel.AutoSize = True
        FechaRecuperacionLabel.Location = New System.Drawing.Point(42, 12)
        FechaRecuperacionLabel.Name = "FechaRecuperacionLabel"
        FechaRecuperacionLabel.Size = New System.Drawing.Size(163, 15)
        FechaRecuperacionLabel.TabIndex = 16
        FechaRecuperacionLabel.Text = "Fecha de Recuperación:"
        '
        'UsuarioSolicitoLabel
        '
        UsuarioSolicitoLabel.AutoSize = True
        UsuarioSolicitoLabel.Location = New System.Drawing.Point(13, 88)
        UsuarioSolicitoLabel.Name = "UsuarioSolicitoLabel"
        UsuarioSolicitoLabel.Size = New System.Drawing.Size(141, 15)
        UsuarioSolicitoLabel.TabIndex = 20
        UsuarioSolicitoLabel.Text = "Usuario que Solicitó:"
        '
        'UsuarioRecuperoLabel
        '
        UsuarioRecuperoLabel.AutoSize = True
        UsuarioRecuperoLabel.Location = New System.Drawing.Point(50, 35)
        UsuarioRecuperoLabel.Name = "UsuarioRecuperoLabel"
        UsuarioRecuperoLabel.Size = New System.Drawing.Size(155, 15)
        UsuarioRecuperoLabel.TabIndex = 22
        UsuarioRecuperoLabel.Text = "Usuario que Recuperó:"
        '
        'DiasLabel
        '
        DiasLabel.AutoSize = True
        DiasLabel.Location = New System.Drawing.Point(3, 17)
        DiasLabel.Name = "DiasLabel"
        DiasLabel.Size = New System.Drawing.Size(202, 15)
        DiasLabel.TabIndex = 24
        DiasLabel.Text = "Días restantes para recuperar:"
        '
        'RecuperoLabel
        '
        RecuperoLabel.AutoSize = True
        RecuperoLabel.Location = New System.Drawing.Point(64, 15)
        RecuperoLabel.Name = "RecuperoLabel"
        RecuperoLabel.Size = New System.Drawing.Size(141, 15)
        RecuperoLabel.TabIndex = 26
        RecuperoLabel.Text = "¿Se ha recuperado?:"
        '
        'ServicioLabel
        '
        ServicioLabel.AutoSize = True
        ServicioLabel.Location = New System.Drawing.Point(623, 15)
        ServicioLabel.Name = "ServicioLabel"
        ServicioLabel.Size = New System.Drawing.Size(69, 15)
        ServicioLabel.TabIndex = 29
        ServicioLabel.Text = "Servicios:"
        '
        'ConceptoLabel2
        '
        ConceptoLabel2.AutoSize = True
        ConceptoLabel2.Location = New System.Drawing.Point(92, 32)
        ConceptoLabel2.Name = "ConceptoLabel2"
        ConceptoLabel2.Size = New System.Drawing.Size(62, 15)
        ConceptoLabel2.TabIndex = 30
        ConceptoLabel2.Text = "Servicio:"
        '
        'ContratoLabel
        '
        ContratoLabel.AutoSize = True
        ContratoLabel.Location = New System.Drawing.Point(10, 32)
        ContratoLabel.Name = "ContratoLabel"
        ContratoLabel.Size = New System.Drawing.Size(65, 15)
        ContratoLabel.TabIndex = 32
        ContratoLabel.Text = "Contrato:"
        '
        'ObservacionLabel
        '
        ObservacionLabel.AutoSize = True
        ObservacionLabel.Location = New System.Drawing.Point(64, 113)
        ObservacionLabel.Name = "ObservacionLabel"
        ObservacionLabel.Size = New System.Drawing.Size(90, 15)
        ObservacionLabel.TabIndex = 46
        ObservacionLabel.Text = "Observacion:"
        '
        'NOMBRELabel
        '
        NOMBRELabel.AutoSize = True
        NOMBRELabel.Location = New System.Drawing.Point(13, 58)
        NOMBRELabel.Name = "NOMBRELabel"
        NOMBRELabel.Size = New System.Drawing.Size(62, 15)
        NOMBRELabel.TabIndex = 35
        NOMBRELabel.Text = "Nombre:"
        '
        'CALLELabel
        '
        CALLELabel.AutoSize = True
        CALLELabel.Location = New System.Drawing.Point(31, 82)
        CALLELabel.Name = "CALLELabel"
        CALLELabel.Size = New System.Drawing.Size(44, 15)
        CALLELabel.TabIndex = 37
        CALLELabel.Text = "Calle:"
        '
        'COLONIALabel
        '
        COLONIALabel.AutoSize = True
        COLONIALabel.Location = New System.Drawing.Point(39, 106)
        COLONIALabel.Name = "COLONIALabel"
        COLONIALabel.Size = New System.Drawing.Size(36, 15)
        COLONIALabel.TabIndex = 39
        COLONIALabel.Text = "Col.:"
        '
        'NUMEROLabel
        '
        NUMEROLabel.AutoSize = True
        NUMEROLabel.Location = New System.Drawing.Point(426, 82)
        NUMEROLabel.Name = "NUMEROLabel"
        NUMEROLabel.Size = New System.Drawing.Size(19, 15)
        NUMEROLabel.TabIndex = 41
        NUMEROLabel.Text = "#:"
        '
        'CIUDADLabel
        '
        CIUDADLabel.AutoSize = True
        CIUDADLabel.Location = New System.Drawing.Point(340, 106)
        CIUDADLabel.Name = "CIUDADLabel"
        CIUDADLabel.Size = New System.Drawing.Size(56, 15)
        CIUDADLabel.TabIndex = 43
        CIUDADLabel.Text = "Ciudad:"
        '
        'SOLOINTERNETLabel
        '
        SOLOINTERNETLabel.AutoSize = True
        SOLOINTERNETLabel.Location = New System.Drawing.Point(120, 154)
        SOLOINTERNETLabel.Name = "SOLOINTERNETLabel"
        SOLOINTERNETLabel.Size = New System.Drawing.Size(93, 15)
        SOLOINTERNETLabel.TabIndex = 45
        SOLOINTERNETLabel.Text = "Sólo Internet:"
        '
        'ESHOTELLabel
        '
        ESHOTELLabel.AutoSize = True
        ESHOTELLabel.Location = New System.Drawing.Point(321, 154)
        ESHOTELLabel.Name = "ESHOTELLabel"
        ESHOTELLabel.Size = New System.Drawing.Size(65, 15)
        ESHOTELLabel.TabIndex = 47
        ESHOTELLabel.Text = "Es Hotel:"
        '
        'TELEFONOLabel
        '
        TELEFONOLabel.AutoSize = True
        TELEFONOLabel.Location = New System.Drawing.Point(8, 130)
        TELEFONOLabel.Name = "TELEFONOLabel"
        TELEFONOLabel.Size = New System.Drawing.Size(67, 15)
        TELEFONOLabel.TabIndex = 49
        TELEFONOLabel.Text = "Teléfono:"
        '
        'CELULARLabel
        '
        CELULARLabel.AutoSize = True
        CELULARLabel.Location = New System.Drawing.Point(219, 130)
        CELULARLabel.Name = "CELULARLabel"
        CELULARLabel.Size = New System.Drawing.Size(57, 15)
        CELULARLabel.TabIndex = 51
        CELULARLabel.Text = "Celular:"
        '
        'EMAILLabel
        '
        EMAILLabel.AutoSize = True
        EMAILLabel.Location = New System.Drawing.Point(392, 130)
        EMAILLabel.Name = "EMAILLabel"
        EMAILLabel.Size = New System.Drawing.Size(53, 15)
        EMAILLabel.TabIndex = 53
        EMAILLabel.Text = "E-Mail:"
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric2"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CONUSUARIOSBindingSource
        '
        Me.CONUSUARIOSBindingSource.DataMember = "CONUSUARIOS"
        Me.CONUSUARIOSBindingSource.DataSource = Me.DataSetEric
        '
        'CONUSUARIOSTableAdapter
        '
        Me.CONUSUARIOSTableAdapter.ClearBeforeFill = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(NOMBRELabel)
        Me.GroupBox1.Controls.Add(Me.NOMBRELabel2)
        Me.GroupBox1.Controls.Add(CALLELabel)
        Me.GroupBox1.Controls.Add(Me.CALLELabel1)
        Me.GroupBox1.Controls.Add(COLONIALabel)
        Me.GroupBox1.Controls.Add(Me.COLONIALabel1)
        Me.GroupBox1.Controls.Add(NUMEROLabel)
        Me.GroupBox1.Controls.Add(Me.NUMEROLabel1)
        Me.GroupBox1.Controls.Add(CIUDADLabel)
        Me.GroupBox1.Controls.Add(Me.CIUDADLabel1)
        Me.GroupBox1.Controls.Add(SOLOINTERNETLabel)
        Me.GroupBox1.Controls.Add(Me.SOLOINTERNETCheckBox)
        Me.GroupBox1.Controls.Add(ESHOTELLabel)
        Me.GroupBox1.Controls.Add(Me.ESHOTELCheckBox)
        Me.GroupBox1.Controls.Add(TELEFONOLabel)
        Me.GroupBox1.Controls.Add(Me.TELEFONOLabel1)
        Me.GroupBox1.Controls.Add(CELULARLabel)
        Me.GroupBox1.Controls.Add(Me.CELULARLabel1)
        Me.GroupBox1.Controls.Add(EMAILLabel)
        Me.GroupBox1.Controls.Add(Me.EMAILLabel1)
        Me.GroupBox1.Controls.Add(ContratoLabel)
        Me.GroupBox1.Controls.Add(Me.ContratoTextBox)
        Me.GroupBox1.Controls.Add(ServicioLabel)
        Me.GroupBox1.Controls.Add(Me.TreeView1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 31)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(995, 188)
        Me.GroupBox1.TabIndex = 28
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Cliente"
        '
        'NOMBRELabel2
        '
        Me.NOMBRELabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATO3BindingSource, "NOMBRE", True))
        Me.NOMBRELabel2.Location = New System.Drawing.Point(77, 50)
        Me.NOMBRELabel2.Name = "NOMBRELabel2"
        Me.NOMBRELabel2.Size = New System.Drawing.Size(521, 23)
        Me.NOMBRELabel2.TabIndex = 36
        Me.NOMBRELabel2.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'BUSCLIPORCONTRATO3BindingSource
        '
        Me.BUSCLIPORCONTRATO3BindingSource.DataMember = "BUSCLIPORCONTRATO3"
        Me.BUSCLIPORCONTRATO3BindingSource.DataSource = Me.DataSetEric2
        '
        'CALLELabel1
        '
        Me.CALLELabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATO3BindingSource, "CALLE", True))
        Me.CALLELabel1.Location = New System.Drawing.Point(77, 74)
        Me.CALLELabel1.Name = "CALLELabel1"
        Me.CALLELabel1.Size = New System.Drawing.Size(343, 23)
        Me.CALLELabel1.TabIndex = 38
        Me.CALLELabel1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'COLONIALabel1
        '
        Me.COLONIALabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATO3BindingSource, "COLONIA", True))
        Me.COLONIALabel1.Location = New System.Drawing.Point(77, 98)
        Me.COLONIALabel1.Name = "COLONIALabel1"
        Me.COLONIALabel1.Size = New System.Drawing.Size(265, 23)
        Me.COLONIALabel1.TabIndex = 40
        Me.COLONIALabel1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'NUMEROLabel1
        '
        Me.NUMEROLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATO3BindingSource, "NUMERO", True))
        Me.NUMEROLabel1.Location = New System.Drawing.Point(451, 74)
        Me.NUMEROLabel1.Name = "NUMEROLabel1"
        Me.NUMEROLabel1.Size = New System.Drawing.Size(147, 23)
        Me.NUMEROLabel1.TabIndex = 42
        Me.NUMEROLabel1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'CIUDADLabel1
        '
        Me.CIUDADLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATO3BindingSource, "CIUDAD", True))
        Me.CIUDADLabel1.Location = New System.Drawing.Point(402, 98)
        Me.CIUDADLabel1.Name = "CIUDADLabel1"
        Me.CIUDADLabel1.Size = New System.Drawing.Size(196, 23)
        Me.CIUDADLabel1.TabIndex = 44
        Me.CIUDADLabel1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'SOLOINTERNETCheckBox
        '
        Me.SOLOINTERNETCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.BUSCLIPORCONTRATO3BindingSource, "SOLOINTERNET", True))
        Me.SOLOINTERNETCheckBox.Enabled = False
        Me.SOLOINTERNETCheckBox.Location = New System.Drawing.Point(219, 150)
        Me.SOLOINTERNETCheckBox.Name = "SOLOINTERNETCheckBox"
        Me.SOLOINTERNETCheckBox.Size = New System.Drawing.Size(17, 24)
        Me.SOLOINTERNETCheckBox.TabIndex = 46
        '
        'ESHOTELCheckBox
        '
        Me.ESHOTELCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.BUSCLIPORCONTRATO3BindingSource, "ESHOTEL", True))
        Me.ESHOTELCheckBox.Enabled = False
        Me.ESHOTELCheckBox.Location = New System.Drawing.Point(392, 150)
        Me.ESHOTELCheckBox.Name = "ESHOTELCheckBox"
        Me.ESHOTELCheckBox.Size = New System.Drawing.Size(17, 24)
        Me.ESHOTELCheckBox.TabIndex = 48
        '
        'TELEFONOLabel1
        '
        Me.TELEFONOLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATO3BindingSource, "TELEFONO", True))
        Me.TELEFONOLabel1.Location = New System.Drawing.Point(77, 122)
        Me.TELEFONOLabel1.Name = "TELEFONOLabel1"
        Me.TELEFONOLabel1.Size = New System.Drawing.Size(136, 23)
        Me.TELEFONOLabel1.TabIndex = 50
        Me.TELEFONOLabel1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'CELULARLabel1
        '
        Me.CELULARLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATO3BindingSource, "CELULAR", True))
        Me.CELULARLabel1.Location = New System.Drawing.Point(282, 122)
        Me.CELULARLabel1.Name = "CELULARLabel1"
        Me.CELULARLabel1.Size = New System.Drawing.Size(104, 23)
        Me.CELULARLabel1.TabIndex = 52
        Me.CELULARLabel1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'EMAILLabel1
        '
        Me.EMAILLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATO3BindingSource, "EMAIL", True))
        Me.EMAILLabel1.Location = New System.Drawing.Point(451, 122)
        Me.EMAILLabel1.Name = "EMAILLabel1"
        Me.EMAILLabel1.Size = New System.Drawing.Size(147, 23)
        Me.EMAILLabel1.TabIndex = 54
        Me.EMAILLabel1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'ContratoTextBox
        '
        Me.ContratoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraClientePorRecuperarBindingSource, "Contrato", True))
        Me.ContratoTextBox.Location = New System.Drawing.Point(77, 26)
        Me.ContratoTextBox.Name = "ContratoTextBox"
        Me.ContratoTextBox.Size = New System.Drawing.Size(110, 21)
        Me.ContratoTextBox.TabIndex = 33
        '
        'MuestraClientePorRecuperarBindingSource
        '
        Me.MuestraClientePorRecuperarBindingSource.DataMember = "MuestraClientePorRecuperar"
        Me.MuestraClientePorRecuperarBindingSource.DataSource = Me.DataSetEric2
        '
        'TreeView1
        '
        Me.TreeView1.Location = New System.Drawing.Point(605, 32)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(375, 142)
        Me.TreeView1.TabIndex = 30
        Me.TreeView1.TabStop = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.NombreLabel1)
        Me.GroupBox2.Controls.Add(Me.ConceptoComboBox)
        Me.GroupBox2.Controls.Add(Me.ObservacionTextBox)
        Me.GroupBox2.Controls.Add(ObservacionLabel)
        Me.GroupBox2.Controls.Add(Me.FechaSolicitudLabel2)
        Me.GroupBox2.Controls.Add(Me.PanelDias)
        Me.GroupBox2.Controls.Add(Me.PanelRecuperado)
        Me.GroupBox2.Controls.Add(Me.PanelStatus)
        Me.GroupBox2.Controls.Add(ConceptoLabel2)
        Me.GroupBox2.Controls.Add(FechaSolicitudLabel)
        Me.GroupBox2.Controls.Add(UsuarioSolicitoLabel)
        Me.GroupBox2.Controls.Add(Me.ConceptoLabel1)
        Me.GroupBox2.Controls.Add(Me.UsuarioSolicitoLabel2)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(12, 225)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(995, 183)
        Me.GroupBox2.TabIndex = 29
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Información del Servicio a Recuperar"
        '
        'NombreLabel1
        '
        Me.NombreLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONUSUARIOSBindingSource, "Nombre", True))
        Me.NombreLabel1.Location = New System.Drawing.Point(159, 81)
        Me.NombreLabel1.Name = "NombreLabel1"
        Me.NombreLabel1.Size = New System.Drawing.Size(294, 23)
        Me.NombreLabel1.TabIndex = 60
        Me.NombreLabel1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'ConceptoComboBox
        '
        Me.ConceptoComboBox.DataSource = Me.MuestraServClientePorRecuperarBindingSource
        Me.ConceptoComboBox.DisplayMember = "Concepto"
        Me.ConceptoComboBox.FormattingEnabled = True
        Me.ConceptoComboBox.Location = New System.Drawing.Point(160, 25)
        Me.ConceptoComboBox.Name = "ConceptoComboBox"
        Me.ConceptoComboBox.Size = New System.Drawing.Size(200, 23)
        Me.ConceptoComboBox.TabIndex = 31
        Me.ConceptoComboBox.ValueMember = "Clv_TipSer"
        '
        'MuestraServClientePorRecuperarBindingSource
        '
        Me.MuestraServClientePorRecuperarBindingSource.DataMember = "MuestraServClientePorRecuperar"
        Me.MuestraServClientePorRecuperarBindingSource.DataSource = Me.DataSetEric2
        '
        'ObservacionTextBox
        '
        Me.ObservacionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraClientePorRecuperarBindingSource, "Observacion", True))
        Me.ObservacionTextBox.Location = New System.Drawing.Point(160, 107)
        Me.ObservacionTextBox.MaxLength = 250
        Me.ObservacionTextBox.Multiline = True
        Me.ObservacionTextBox.Name = "ObservacionTextBox"
        Me.ObservacionTextBox.Size = New System.Drawing.Size(329, 61)
        Me.ObservacionTextBox.TabIndex = 47
        '
        'FechaSolicitudLabel2
        '
        Me.FechaSolicitudLabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraClientePorRecuperarBindingSource, "FechaSolicitud", True))
        Me.FechaSolicitudLabel2.Location = New System.Drawing.Point(160, 57)
        Me.FechaSolicitudLabel2.Name = "FechaSolicitudLabel2"
        Me.FechaSolicitudLabel2.Size = New System.Drawing.Size(200, 23)
        Me.FechaSolicitudLabel2.TabIndex = 39
        Me.FechaSolicitudLabel2.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'PanelDias
        '
        Me.PanelDias.Controls.Add(DiasLabel)
        Me.PanelDias.Controls.Add(Me.DiasLabel2)
        Me.PanelDias.Location = New System.Drawing.Point(495, 12)
        Me.PanelDias.Name = "PanelDias"
        Me.PanelDias.Size = New System.Drawing.Size(465, 36)
        Me.PanelDias.TabIndex = 34
        Me.PanelDias.Visible = False
        '
        'DiasLabel2
        '
        Me.DiasLabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraClientePorRecuperarBindingSource, "Dias", True))
        Me.DiasLabel2.Location = New System.Drawing.Point(211, 9)
        Me.DiasLabel2.Name = "DiasLabel2"
        Me.DiasLabel2.Size = New System.Drawing.Size(57, 23)
        Me.DiasLabel2.TabIndex = 55
        Me.DiasLabel2.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'PanelRecuperado
        '
        Me.PanelRecuperado.Controls.Add(FechaRecuperacionLabel)
        Me.PanelRecuperado.Controls.Add(UsuarioRecuperoLabel)
        Me.PanelRecuperado.Controls.Add(Me.FechaRecuperacionLabel2)
        Me.PanelRecuperado.Controls.Add(Me.UsuarioRecuperoLabel2)
        Me.PanelRecuperado.Location = New System.Drawing.Point(495, 102)
        Me.PanelRecuperado.Name = "PanelRecuperado"
        Me.PanelRecuperado.Size = New System.Drawing.Size(465, 66)
        Me.PanelRecuperado.TabIndex = 33
        Me.PanelRecuperado.Visible = False
        '
        'FechaRecuperacionLabel2
        '
        Me.FechaRecuperacionLabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraClientePorRecuperarBindingSource, "FechaRecuperacion", True))
        Me.FechaRecuperacionLabel2.Location = New System.Drawing.Point(211, 4)
        Me.FechaRecuperacionLabel2.Name = "FechaRecuperacionLabel2"
        Me.FechaRecuperacionLabel2.Size = New System.Drawing.Size(100, 23)
        Me.FechaRecuperacionLabel2.TabIndex = 45
        Me.FechaRecuperacionLabel2.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'UsuarioRecuperoLabel2
        '
        Me.UsuarioRecuperoLabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraClientePorRecuperarBindingSource, "UsuarioRecupero", True))
        Me.UsuarioRecuperoLabel2.Location = New System.Drawing.Point(211, 27)
        Me.UsuarioRecuperoLabel2.Name = "UsuarioRecuperoLabel2"
        Me.UsuarioRecuperoLabel2.Size = New System.Drawing.Size(251, 23)
        Me.UsuarioRecuperoLabel2.TabIndex = 53
        Me.UsuarioRecuperoLabel2.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'PanelStatus
        '
        Me.PanelStatus.Controls.Add(RecuperoLabel)
        Me.PanelStatus.Controls.Add(Me.RecuperoLabel2)
        Me.PanelStatus.Location = New System.Drawing.Point(495, 54)
        Me.PanelStatus.Name = "PanelStatus"
        Me.PanelStatus.Size = New System.Drawing.Size(465, 42)
        Me.PanelStatus.TabIndex = 32
        Me.PanelStatus.Visible = False
        '
        'RecuperoLabel2
        '
        Me.RecuperoLabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraClientePorRecuperarBindingSource, "Recupero", True))
        Me.RecuperoLabel2.Location = New System.Drawing.Point(211, 7)
        Me.RecuperoLabel2.Name = "RecuperoLabel2"
        Me.RecuperoLabel2.Size = New System.Drawing.Size(100, 23)
        Me.RecuperoLabel2.TabIndex = 57
        Me.RecuperoLabel2.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'ConceptoLabel1
        '
        Me.ConceptoLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraClientePorRecuperarBindingSource, "Concepto", True))
        Me.ConceptoLabel1.Location = New System.Drawing.Point(160, 24)
        Me.ConceptoLabel1.Name = "ConceptoLabel1"
        Me.ConceptoLabel1.Size = New System.Drawing.Size(200, 23)
        Me.ConceptoLabel1.TabIndex = 59
        Me.ConceptoLabel1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'UsuarioSolicitoLabel2
        '
        Me.UsuarioSolicitoLabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraClientePorRecuperarBindingSource, "UsuarioSolicito", True))
        Me.UsuarioSolicitoLabel2.Location = New System.Drawing.Point(160, 80)
        Me.UsuarioSolicitoLabel2.Name = "UsuarioSolicitoLabel2"
        Me.UsuarioSolicitoLabel2.Size = New System.Drawing.Size(294, 23)
        Me.UsuarioSolicitoLabel2.TabIndex = 51
        Me.UsuarioSolicitoLabel2.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'DameSerDELCliBindingSource
        '
        Me.DameSerDELCliBindingSource.DataMember = "dameSerDELCli"
        Me.DameSerDELCliBindingSource.DataSource = Me.DataSetEric2
        '
        'DameSerDELCliTableAdapter
        '
        Me.DameSerDELCliTableAdapter.ClearBeforeFill = True
        '
        'MuestraServClientePorRecuperarTableAdapter
        '
        Me.MuestraServClientePorRecuperarTableAdapter.ClearBeforeFill = True
        '
        'MuestraClientePorRecuperarTableAdapter
        '
        Me.MuestraClientePorRecuperarTableAdapter.ClearBeforeFill = True
        '
        'ClaveTextBox
        '
        Me.ClaveTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraClientePorRecuperarBindingSource, "Clave", True))
        Me.ClaveTextBox.Location = New System.Drawing.Point(144, 4)
        Me.ClaveTextBox.Name = "ClaveTextBox"
        Me.ClaveTextBox.Size = New System.Drawing.Size(10, 20)
        Me.ClaveTextBox.TabIndex = 31
        '
        'Clv_TipSerTextBox
        '
        Me.Clv_TipSerTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraClientePorRecuperarBindingSource, "Clv_TipSer", True))
        Me.Clv_TipSerTextBox.Location = New System.Drawing.Point(160, 4)
        Me.Clv_TipSerTextBox.Name = "Clv_TipSerTextBox"
        Me.Clv_TipSerTextBox.Size = New System.Drawing.Size(10, 20)
        Me.Clv_TipSerTextBox.TabIndex = 35
        '
        'Clv_UsuarioSolicitoTextBox
        '
        Me.Clv_UsuarioSolicitoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraClientePorRecuperarBindingSource, "Clv_UsuarioSolicito", True))
        Me.Clv_UsuarioSolicitoTextBox.Location = New System.Drawing.Point(174, 4)
        Me.Clv_UsuarioSolicitoTextBox.Name = "Clv_UsuarioSolicitoTextBox"
        Me.Clv_UsuarioSolicitoTextBox.Size = New System.Drawing.Size(10, 20)
        Me.Clv_UsuarioSolicitoTextBox.TabIndex = 37
        '
        'SeRecuperoTextBox
        '
        Me.SeRecuperoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraClientePorRecuperarBindingSource, "SeRecupero", True))
        Me.SeRecuperoTextBox.Location = New System.Drawing.Point(189, 4)
        Me.SeRecuperoTextBox.Name = "SeRecuperoTextBox"
        Me.SeRecuperoTextBox.Size = New System.Drawing.Size(10, 20)
        Me.SeRecuperoTextBox.TabIndex = 41
        '
        'Clv_UsuarioRecuperoTextBox
        '
        Me.Clv_UsuarioRecuperoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraClientePorRecuperarBindingSource, "Clv_UsuarioRecupero", True))
        Me.Clv_UsuarioRecuperoTextBox.Location = New System.Drawing.Point(200, 4)
        Me.Clv_UsuarioRecuperoTextBox.Name = "Clv_UsuarioRecuperoTextBox"
        Me.Clv_UsuarioRecuperoTextBox.Size = New System.Drawing.Size(10, 20)
        Me.Clv_UsuarioRecuperoTextBox.TabIndex = 43
        '
        'ClienteTextBox
        '
        Me.ClienteTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraClientePorRecuperarBindingSource, "Cliente", True))
        Me.ClienteTextBox.Location = New System.Drawing.Point(216, 4)
        Me.ClienteTextBox.Name = "ClienteTextBox"
        Me.ClienteTextBox.Size = New System.Drawing.Size(10, 20)
        Me.ClienteTextBox.TabIndex = 49
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Nothing
        Me.BindingNavigator1.CountItem = Nothing
        Me.BindingNavigator1.DeleteItem = Nothing
        Me.BindingNavigator1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.BindingNavigatorSaveItem})
        Me.BindingNavigator1.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigator1.MoveFirstItem = Nothing
        Me.BindingNavigator1.MoveLastItem = Nothing
        Me.BindingNavigator1.MoveNextItem = Nothing
        Me.BindingNavigator1.MovePreviousItem = Nothing
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Nothing
        Me.BindingNavigator1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.BindingNavigator1.Size = New System.Drawing.Size(1019, 25)
        Me.BindingNavigator1.TabIndex = 50
        Me.BindingNavigator1.TabStop = True
        Me.BindingNavigator1.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(74, 22)
        Me.BindingNavigatorDeleteItem.Text = "&Eliminar"
        '
        'BindingNavigatorSaveItem
        '
        Me.BindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorSaveItem.Image = CType(resources.GetObject("BindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem"
        Me.BindingNavigatorSaveItem.Size = New System.Drawing.Size(75, 22)
        Me.BindingNavigatorSaveItem.Text = "&Guardar"
        '
        'BUSCLIPORCONTRATO3TableAdapter
        '
        Me.BUSCLIPORCONTRATO3TableAdapter.ClearBeforeFill = True
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(871, 414)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(136, 36)
        Me.Button4.TabIndex = 51
        Me.Button4.Text = "&SALIR"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'FrmClientesPorRecuperar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1019, 455)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.BindingNavigator1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ClaveTextBox)
        Me.Controls.Add(Me.Clv_UsuarioSolicitoTextBox)
        Me.Controls.Add(Me.Clv_UsuarioRecuperoTextBox)
        Me.Controls.Add(Me.Clv_TipSerTextBox)
        Me.Controls.Add(Me.ClienteTextBox)
        Me.Controls.Add(Me.SeRecuperoTextBox)
        Me.Name = "FrmClientesPorRecuperar"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Clientes por Recuperar"
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONUSUARIOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.BUSCLIPORCONTRATO3BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraClientePorRecuperarBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.MuestraServClientePorRecuperarBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelDias.ResumeLayout(False)
        Me.PanelDias.PerformLayout()
        Me.PanelRecuperado.ResumeLayout(False)
        Me.PanelRecuperado.PerformLayout()
        Me.PanelStatus.ResumeLayout(False)
        Me.PanelStatus.PerformLayout()
        CType(Me.DameSerDELCliBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric2
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents CONUSUARIOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONUSUARIOSTableAdapter As sofTV.DataSetEricTableAdapters.CONUSUARIOSTableAdapter
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents DameSerDELCliBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameSerDELCliTableAdapter As sofTV.DataSetEric2TableAdapters.dameSerDELCliTableAdapter
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents MuestraServClientePorRecuperarBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraServClientePorRecuperarTableAdapter As sofTV.DataSetEric2TableAdapters.MuestraServClientePorRecuperarTableAdapter
    Friend WithEvents ConceptoComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents PanelDias As System.Windows.Forms.Panel
    Friend WithEvents PanelRecuperado As System.Windows.Forms.Panel
    Friend WithEvents PanelStatus As System.Windows.Forms.Panel
    Friend WithEvents MuestraClientePorRecuperarBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraClientePorRecuperarTableAdapter As sofTV.DataSetEric2TableAdapters.MuestraClientePorRecuperarTableAdapter
    Friend WithEvents NombreLabel1 As System.Windows.Forms.Label
    Friend WithEvents ClaveTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ContratoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_TipSerTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_UsuarioSolicitoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FechaSolicitudLabel2 As System.Windows.Forms.Label
    Friend WithEvents SeRecuperoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_UsuarioRecuperoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FechaRecuperacionLabel2 As System.Windows.Forms.Label
    Friend WithEvents ObservacionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ClienteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents UsuarioSolicitoLabel2 As System.Windows.Forms.Label
    Friend WithEvents UsuarioRecuperoLabel2 As System.Windows.Forms.Label
    Friend WithEvents DiasLabel2 As System.Windows.Forms.Label
    Friend WithEvents RecuperoLabel2 As System.Windows.Forms.Label
    Friend WithEvents ConceptoLabel1 As System.Windows.Forms.Label
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BUSCLIPORCONTRATO3BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BUSCLIPORCONTRATO3TableAdapter As sofTV.DataSetEric2TableAdapters.BUSCLIPORCONTRATO3TableAdapter
    Friend WithEvents NOMBRELabel2 As System.Windows.Forms.Label
    Friend WithEvents CALLELabel1 As System.Windows.Forms.Label
    Friend WithEvents COLONIALabel1 As System.Windows.Forms.Label
    Friend WithEvents NUMEROLabel1 As System.Windows.Forms.Label
    Friend WithEvents CIUDADLabel1 As System.Windows.Forms.Label
    Friend WithEvents SOLOINTERNETCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents ESHOTELCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents TELEFONOLabel1 As System.Windows.Forms.Label
    Friend WithEvents CELULARLabel1 As System.Windows.Forms.Label
    Friend WithEvents EMAILLabel1 As System.Windows.Forms.Label
    Friend WithEvents Button4 As System.Windows.Forms.Button
End Class
