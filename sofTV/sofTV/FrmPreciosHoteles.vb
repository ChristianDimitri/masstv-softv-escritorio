Imports System.Data.SqlClient
Public Class FrmPreciosHoteles
    Dim contratacion As Decimal = 0
    Dim mensualidad As Decimal = 0
    Dim Renta As Decimal = 0

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Dim cmd As New SqlClient.SqlCommand
        Dim con As New SqlClient.SqlConnection(MiConexion)
        con.Open()
        With cmd
            .CommandText = "Nuevo_CobroHoteles"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = con
            Dim prm As New SqlParameter("@Clv_servicio", SqlDbType.Int)
            prm.Direction = ParameterDirection.Input
            prm.Value = LClv_Servicio
            .Parameters.Add(prm)

            Dim prm2 As New SqlParameter("@Clv_txt", SqlDbType.VarChar, 100)
            prm2.Direction = ParameterDirection.Input
            prm2.Value = LServicio
            .Parameters.Add(prm2)

            Dim prm3 As New SqlParameter("@Clv_tipser", SqlDbType.Int)
            prm3.Direction = ParameterDirection.Input
            prm3.Value = LTip_Ser
            .Parameters.Add(prm3)

            Dim prm4 As New SqlParameter("@Contratacion", SqlDbType.Money)
            prm4.Direction = ParameterDirection.Input
            prm4.Value = CDec(Me.TextBox33.Text)
            .Parameters.Add(prm4)

            Dim prm5 As New SqlParameter("@Mensualidad", SqlDbType.Money)
            prm5.Direction = ParameterDirection.Input
            prm5.Value = CDec(Me.TextBox32.Text)
            .Parameters.Add(prm5)

            Dim prm6 As New SqlParameter("@Contrato", SqlDbType.BigInt)
            prm6.Direction = ParameterDirection.Input
            prm6.Value = Contrato
            .Parameters.Add(prm6)

            Dim prm7 As New SqlParameter("@Clv_Unicanet", SqlDbType.BigInt)
            prm7.Direction = ParameterDirection.Input
            prm7.Value = LClvUnica
            .Parameters.Add(prm7)

            Dim prm8 As New SqlParameter("@Renta", SqlDbType.Money)
            prm8.Direction = ParameterDirection.Input
            prm8.Value = CDec(Me.TxtRentaAparato.Text)
            .Parameters.Add(prm8)

            Dim i As Integer = .ExecuteNonQuery
        End With
        con.Close()
        MsgBox("Se Ha Guardado con �xito", MsgBoxStyle.Information)
        Me.Close()
    End Sub
    Private Sub Consultar_Precios()
        Dim cmd As New SqlClient.SqlCommand
        Dim reader As SqlDataReader
        Dim con2 As New SqlClient.SqlConnection(MiConexion)
        con2.Open()
        With cmd
            .CommandText = "Consulta_CobroHoteles"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = con2
            Dim prm As New SqlParameter("@Clv_servicio", SqlDbType.Int)
            prm.Direction = ParameterDirection.Input
            prm.Value = LClv_Servicio
            .Parameters.Add(prm)

            Dim prm2 As New SqlParameter("@Contrato", SqlDbType.BigInt)
            prm2.Direction = ParameterDirection.Input
            prm2.Value = Contrato
            .Parameters.Add(prm2)

            Dim prm3 As New SqlParameter("@clv_Unicanet", SqlDbType.BigInt)
            prm3.Direction = ParameterDirection.Input
            prm3.Value = LClvUnica
            .Parameters.Add(prm3)

            reader = .ExecuteReader()
            Using reader
                While reader.Read
                    ' Process SprocResults datareader here.
                    contratacion = reader.GetValue(0)
                    mensualidad = reader.GetValue(1)
                    Renta = reader.GetValue(2)
                End While
            End Using

        End With
        con2.Close()
        If contratacion > 0 Or mensualidad > 0 Then
            Me.Button2.Enabled = True
        End If
    End Sub
    Private Sub FrmPreciosHoteles_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.GroupBox1.Text = "Precios del Servicio a Hotel de " + LServicio
        Consultar_Precios()
        Me.TextBox32.Text = Format(mensualidad, "##,##0.00")
        Me.TextBox33.Text = Format(contratacion, "##,##0.00")
        Me.TxtRentaAparato.Text = Format(Renta, "##,##0.00")
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
    Private Sub Borrar_Precios()
        Dim ConSoftv As New SqlConnection(MiConexion)
        Dim Cmd As New SqlCommand
        ConSoftv.Open()
        With Cmd
            .CommandText = "Borra_CobroHoteles"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = ConSoftv
            Dim para As New SqlParameter("@clv_Servicio", SqlDbType.Int)
            Dim para2 As New SqlParameter("@clv_tipser", SqlDbType.Int)
            Dim para3 As New SqlParameter("@Contrato", SqlDbType.BigInt)
            Dim para4 As New SqlParameter("@Clv_Unicanet", SqlDbType.BigInt)
            para.Direction = ParameterDirection.Input
            para2.Direction = ParameterDirection.Input
            para3.Direction = ParameterDirection.Input
            para4.Direction = ParameterDirection.Input
            para.Value = LClv_Servicio
            para2.Value = LTip_Ser
            para3.Value = Contrato
            para4.Value = LClvUnica
            .Parameters.Add(para)
            .Parameters.Add(para2)
            .Parameters.Add(para3)
            .Parameters.Add(para4)
            .ExecuteNonQuery()
        End With
        ConSoftv.Close()
        MsgBox("Los Precios Han sido Eliminados con �xito", MsgBoxStyle.Information)
    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim resp As MsgBoxResult = MsgBoxResult.Cancel
        Borrar_Precios()
        Me.Close()
    End Sub

    Private Sub TextBox32_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox32.KeyPress
        e.KeyChar = Chr(ValidaKey(TextBox32, Asc(LCase(e.KeyChar)), "L"))
    End Sub

    Private Sub TextBox32_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox32.TextChanged

    End Sub

    Private Sub TextBox33_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox33.KeyPress
        e.KeyChar = Chr(ValidaKey(TextBox33, Asc(LCase(e.KeyChar)), "L"))
    End Sub

    Private Sub TxtRentaAparato_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtRentaAparato.KeyPress
        e.KeyChar = Chr(ValidaKey(TxtRentaAparato, Asc(LCase(e.KeyChar)), "L"))
    End Sub
End Class