<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCalles
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_CalleLabel As System.Windows.Forms.Label
        Dim NOMBRELabel As System.Windows.Forms.Label
        Dim NOMBRELabel1 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCalles))
        Me.Clv_CalleTextBox = New System.Windows.Forms.TextBox()
        Me.CONCALLESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.NOMBRETextBox = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.CONCALLESBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.CONCALLESBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CONCVECAROLBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONCALLESTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONCALLESTableAdapter()
        Me.MUESTRACALLESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACALLESTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACALLESTableAdapter()
        Me.CONCVECAROLTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONCVECAROLTableAdapter()
        Me.DAMECOLONIA_CALLEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DAMECOLONIA_CALLETableAdapter = New sofTV.NewSofTvDataSetTableAdapters.DAMECOLONIA_CALLETableAdapter()
        Me.Button5 = New System.Windows.Forms.Button()
        Clv_CalleLabel = New System.Windows.Forms.Label()
        NOMBRELabel = New System.Windows.Forms.Label()
        NOMBRELabel1 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        CType(Me.CONCALLESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.CONCALLESBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONCALLESBindingNavigator.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.CONCVECAROLBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACALLESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAMECOLONIA_CALLEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Clv_CalleLabel
        '
        Clv_CalleLabel.AutoSize = True
        Clv_CalleLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_CalleLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_CalleLabel.Location = New System.Drawing.Point(9, 45)
        Clv_CalleLabel.Name = "Clv_CalleLabel"
        Clv_CalleLabel.Size = New System.Drawing.Size(50, 15)
        Clv_CalleLabel.TabIndex = 2
        Clv_CalleLabel.Text = "Clave :"
        '
        'NOMBRELabel
        '
        NOMBRELabel.AutoSize = True
        NOMBRELabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NOMBRELabel.ForeColor = System.Drawing.Color.LightSlateGray
        NOMBRELabel.Location = New System.Drawing.Point(9, 81)
        NOMBRELabel.Name = "NOMBRELabel"
        NOMBRELabel.Size = New System.Drawing.Size(66, 15)
        NOMBRELabel.TabIndex = 4
        NOMBRELabel.Text = "Nombre :"
        '
        'NOMBRELabel1
        '
        NOMBRELabel1.AutoSize = True
        NOMBRELabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NOMBRELabel1.ForeColor = System.Drawing.Color.LightSlateGray
        NOMBRELabel1.Location = New System.Drawing.Point(13, 46)
        NOMBRELabel1.Name = "NOMBRELabel1"
        NOMBRELabel1.Size = New System.Drawing.Size(167, 15)
        NOMBRELabel1.TabIndex = 4
        NOMBRELabel1.Text = "Seleccione una Colonia :"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(3, 95)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(75, 15)
        Label2.TabIndex = 6
        Label2.Text = "Colonias  :"
        '
        'Clv_CalleTextBox
        '
        Me.Clv_CalleTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_CalleTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_CalleTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCALLESBindingSource, "Clv_Calle", True))
        Me.Clv_CalleTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_CalleTextBox.Location = New System.Drawing.Point(65, 45)
        Me.Clv_CalleTextBox.Name = "Clv_CalleTextBox"
        Me.Clv_CalleTextBox.ReadOnly = True
        Me.Clv_CalleTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Clv_CalleTextBox.TabIndex = 0
        Me.Clv_CalleTextBox.TabStop = False
        '
        'CONCALLESBindingSource
        '
        Me.CONCALLESBindingSource.DataMember = "CONCALLES"
        Me.CONCALLESBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'NOMBRETextBox
        '
        Me.NOMBRETextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NOMBRETextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.NOMBRETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCALLESBindingSource, "NOMBRE", True))
        Me.NOMBRETextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NOMBRETextBox.Location = New System.Drawing.Point(81, 81)
        Me.NOMBRETextBox.Name = "NOMBRETextBox"
        Me.NOMBRETextBox.Size = New System.Drawing.Size(412, 21)
        Me.NOMBRETextBox.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.CONCALLESBindingNavigator)
        Me.Panel1.Controls.Add(Me.NOMBRETextBox)
        Me.Panel1.Controls.Add(Clv_CalleLabel)
        Me.Panel1.Controls.Add(NOMBRELabel)
        Me.Panel1.Controls.Add(Me.Clv_CalleTextBox)
        Me.Panel1.Location = New System.Drawing.Point(12, 29)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(541, 128)
        Me.Panel1.TabIndex = 0
        Me.Panel1.TabStop = True
        '
        'CONCALLESBindingNavigator
        '
        Me.CONCALLESBindingNavigator.AddNewItem = Nothing
        Me.CONCALLESBindingNavigator.BindingSource = Me.CONCALLESBindingSource
        Me.CONCALLESBindingNavigator.CountItem = Nothing
        Me.CONCALLESBindingNavigator.DeleteItem = Nothing
        Me.CONCALLESBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripLabel1, Me.BindingNavigatorSeparator2, Me.CONCALLESBindingNavigatorSaveItem})
        Me.CONCALLESBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONCALLESBindingNavigator.MoveFirstItem = Nothing
        Me.CONCALLESBindingNavigator.MoveLastItem = Nothing
        Me.CONCALLESBindingNavigator.MoveNextItem = Nothing
        Me.CONCALLESBindingNavigator.MovePreviousItem = Nothing
        Me.CONCALLESBindingNavigator.Name = "CONCALLESBindingNavigator"
        Me.CONCALLESBindingNavigator.PositionItem = Nothing
        Me.CONCALLESBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONCALLESBindingNavigator.Size = New System.Drawing.Size(541, 25)
        Me.CONCALLESBindingNavigator.TabIndex = 200
        Me.CONCALLESBindingNavigator.Text = "BindingNavigator1"
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(72, 22)
        Me.ToolStripLabel1.Text = "&CANCELAR"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'CONCALLESBindingNavigatorSaveItem
        '
        Me.CONCALLESBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONCALLESBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONCALLESBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONCALLESBindingNavigatorSaveItem.Name = "CONCALLESBindingNavigatorSaveItem"
        Me.CONCALLESBindingNavigatorSaveItem.Size = New System.Drawing.Size(88, 22)
        Me.CONCALLESBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.TreeView1)
        Me.Panel2.Controls.Add(Me.ComboBox1)
        Me.Panel2.Controls.Add(Label2)
        Me.Panel2.Controls.Add(NOMBRELabel1)
        Me.Panel2.Controls.Add(Me.Button2)
        Me.Panel2.Controls.Add(Me.Button1)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Location = New System.Drawing.Point(12, 188)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(541, 329)
        Me.Panel2.TabIndex = 120
        Me.Panel2.TabStop = True
        '
        'TreeView1
        '
        Me.TreeView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView1.Location = New System.Drawing.Point(6, 113)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(408, 200)
        Me.TreeView1.TabIndex = 8
        Me.TreeView1.TabStop = False
        '
        'ComboBox1
        '
        Me.ComboBox1.DisplayMember = "CLV_COLONIA"
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(186, 46)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(320, 23)
        Me.ComboBox1.TabIndex = 120
        Me.ComboBox1.ValueMember = "CLV_COLONIA"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkRed
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(420, 149)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(96, 30)
        Me.Button2.TabIndex = 160
        Me.Button2.Text = "&Quitar"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkRed
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(420, 113)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(96, 30)
        Me.Button1.TabIndex = 140
        Me.Button1.Text = "&Agregar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.OrangeRed
        Me.Label1.Location = New System.Drawing.Point(3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(295, 20)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Relación de la Calle con la Colonias"
        '
        'CONCVECAROLBindingSource
        '
        Me.CONCVECAROLBindingSource.DataMember = "CONCVECAROL"
        Me.CONCVECAROLBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CONCALLESTableAdapter
        '
        Me.CONCALLESTableAdapter.ClearBeforeFill = True
        '
        'MUESTRACALLESBindingSource
        '
        Me.MUESTRACALLESBindingSource.DataMember = "MUESTRACALLES"
        Me.MUESTRACALLESBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRACALLESTableAdapter
        '
        Me.MUESTRACALLESTableAdapter.ClearBeforeFill = True
        '
        'CONCVECAROLTableAdapter
        '
        Me.CONCVECAROLTableAdapter.ClearBeforeFill = True
        '
        'DAMECOLONIA_CALLEBindingSource
        '
        Me.DAMECOLONIA_CALLEBindingSource.DataMember = "DAMECOLONIA_CALLE"
        Me.DAMECOLONIA_CALLEBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'DAMECOLONIA_CALLETableAdapter
        '
        Me.DAMECOLONIA_CALLETableAdapter.ClearBeforeFill = True
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(405, 523)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 300
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'FrmCalles
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(573, 586)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Button5)
        Me.Name = "FrmCalles"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Calles"
        CType(Me.CONCALLESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.CONCALLESBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONCALLESBindingNavigator.ResumeLayout(False)
        Me.CONCALLESBindingNavigator.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.CONCVECAROLBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACALLESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAMECOLONIA_CALLEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONCALLESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONCALLESTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONCALLESTableAdapter
    Friend WithEvents Clv_CalleTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NOMBRETextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents CONCALLESBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents ToolStripLabel1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CONCALLESBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents MUESTRACALLESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACALLESTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACALLESTableAdapter
    Friend WithEvents CONCVECAROLBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONCVECAROLTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONCVECAROLTableAdapter
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents DAMECOLONIA_CALLEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMECOLONIA_CALLETableAdapter As sofTV.NewSofTvDataSetTableAdapters.DAMECOLONIA_CALLETableAdapter
    Friend WithEvents Button5 As System.Windows.Forms.Button
End Class
