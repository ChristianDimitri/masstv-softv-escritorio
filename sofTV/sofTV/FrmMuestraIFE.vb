﻿Imports System.Text
Imports System.Data.SqlClient

Public Class FrmMuestraIFE
    Public Guardar As Boolean = False

    Private Sub MuestraClientesMismaIFE(ByVal IFE As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim sBuilder As New StringBuilder("EXEC MuestraClientesMismaIFE '" + IFE + "'")
        Dim dTable As New DataTable
        Dim bSource As New BindingSource
        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString, conexion)

        Try
            dAdapter.Fill(dTable)
            bSource.DataSource = dTable
            dgvIFE.DataSource = bSource
        Catch ex As Exception
            MsgBox(ex.ToString())
        End Try

    End Sub

    Private Sub FrmMuestraIFE_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        MuestraClientesMismaIFE(eIFE)
    End Sub

    Private Sub bnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnGuardar.Click
        If eBndAutorizacionIFERepetido = True Then
            Locbndguardar = True
            eBndEntraNombre = True
            eBndEntraDire = True
            FrmAutorizacion.IFEAutorizado = True
            Guardar = True
        Else
            FrmAutorizacion.OpcionValidar = "IFE"
            FrmAutorizacion.ShowDialog()
        End If
        Me.Close()
    End Sub

    Private Sub bnRechazar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnRechazar.Click
        Locbndguardar = False
        eBndEntraNombre = False
        eBndEntraDire = False
        Me.Close()
    End Sub

    Private Sub dgvIFE_CellContentDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvIFE.CellContentDoubleClick
        'Mandamos Mostrar los Datos Generales del Cliente
        If (Me.dgvIFE.RowCount > 0) Then
            Try
                'FrmDatosGeneralesClientes.Contrato = Me.dgvIFE.CurrentRow.Cells(0).Value
                'FrmDatosGeneralesClientes.ShowDialog()
                'FrmDatosGeneralesClientes.Contrato = 0
                'Me.DataGridView1.SelectedCells.Item(0).Value()
                If IsNumeric(Me.dgvIFE.CurrentRow.Cells(0).Value) = True Then
                    Contrato = Me.dgvIFE.CurrentRow.Cells(0).Value
                    OpcionCli = "C"
                    Dim PantallaClientes As New FrmClientes()
                    PantallaClientes.ShowDialog()
                Else
                    MsgBox("Seleccione un Cliente para poder Consultar ", MsgBoxStyle.Information)
                End If

            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

        End If
    End Sub

    Private Sub dgvIFE_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvIFE.CellDoubleClick
        'Mandamos Mostrar los Datos Generales del Cliente
        If (Me.dgvIFE.RowCount > 0) Then
            Try
                'FrmDatosGeneralesClientes.Contrato = Me.dgvIFE.CurrentRow.Cells(0).Value
                'FrmDatosGeneralesClientes.ShowDialog()
                'FrmDatosGeneralesClientes.Contrato = 0
                'Me.DataGridView1.SelectedCells.Item(0).Value()
                If IsNumeric(Me.dgvIFE.CurrentRow.Cells(0).Value) = True Then
                    Module1.Contrato = Me.dgvIFE.CurrentRow.Cells(0).Value
                    OpcionCli = "C"
                    Dim PantallaClientes As New FrmClientes()
                    colorea(PantallaClientes, PantallaClientes.Name)
                    PantallaClientes.ShowDialog()
                Else
                    MsgBox("Seleccione un Cliente para poder Consultar ", MsgBoxStyle.Information)
                End If

            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub
End Class