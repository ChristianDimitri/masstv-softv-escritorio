﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwClasificacionProblemas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.CMBlblBusqueda = New System.Windows.Forms.Label()
        Me.CMBlblBuscarDescripcion = New System.Windows.Forms.Label()
        Me.txtBuscarDescripcion = New System.Windows.Forms.TextBox()
        Me.dgvProblemas = New System.Windows.Forms.DataGridView()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnConsulta = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.gbxEspecificaciones = New System.Windows.Forms.GroupBox()
        Me.cbxActivo = New System.Windows.Forms.CheckBox()
        Me.CMBlblActivoEspecif = New System.Windows.Forms.Label()
        Me.txtDescripcionEspecif = New System.Windows.Forms.TextBox()
        Me.CMBlblDescripcionEspecif = New System.Windows.Forms.Label()
        Me.txtClaveEspecif = New System.Windows.Forms.TextBox()
        Me.CMBlblClaveEspecif = New System.Windows.Forms.Label()
        Me.CMBlblClave = New System.Windows.Forms.Label()
        Me.txtBuscarClave = New System.Windows.Forms.TextBox()
        Me.btnBuscarClave = New System.Windows.Forms.Button()
        Me.btnBuscarDescripcion = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Clave = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Activo = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        CType(Me.dgvProblemas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxEspecificaciones.SuspendLayout()
        Me.SuspendLayout()
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'CMBlblBusqueda
        '
        Me.CMBlblBusqueda.AutoSize = True
        Me.CMBlblBusqueda.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblBusqueda.Location = New System.Drawing.Point(12, 17)
        Me.CMBlblBusqueda.Name = "CMBlblBusqueda"
        Me.CMBlblBusqueda.Size = New System.Drawing.Size(107, 20)
        Me.CMBlblBusqueda.TabIndex = 0
        Me.CMBlblBusqueda.Text = "Buscar Por :"
        '
        'CMBlblBuscarDescripcion
        '
        Me.CMBlblBuscarDescripcion.AutoSize = True
        Me.CMBlblBuscarDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblBuscarDescripcion.Location = New System.Drawing.Point(12, 150)
        Me.CMBlblBuscarDescripcion.Name = "CMBlblBuscarDescripcion"
        Me.CMBlblBuscarDescripcion.Size = New System.Drawing.Size(91, 15)
        Me.CMBlblBuscarDescripcion.TabIndex = 1
        Me.CMBlblBuscarDescripcion.Text = "Descripción :"
        '
        'txtBuscarDescripcion
        '
        Me.txtBuscarDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBuscarDescripcion.Location = New System.Drawing.Point(5, 166)
        Me.txtBuscarDescripcion.Multiline = True
        Me.txtBuscarDescripcion.Name = "txtBuscarDescripcion"
        Me.txtBuscarDescripcion.Size = New System.Drawing.Size(155, 75)
        Me.txtBuscarDescripcion.TabIndex = 2
        '
        'dgvProblemas
        '
        Me.dgvProblemas.AllowUserToAddRows = False
        Me.dgvProblemas.AllowUserToDeleteRows = False
        Me.dgvProblemas.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgvProblemas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvProblemas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clave, Me.Descripcion, Me.Activo})
        Me.dgvProblemas.Location = New System.Drawing.Point(166, 9)
        Me.dgvProblemas.Name = "dgvProblemas"
        Me.dgvProblemas.ReadOnly = True
        Me.dgvProblemas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvProblemas.Size = New System.Drawing.Size(584, 625)
        Me.dgvProblemas.TabIndex = 4
        '
        'btnNuevo
        '
        Me.btnNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuevo.Location = New System.Drawing.Point(756, 12)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(113, 32)
        Me.btnNuevo.TabIndex = 5
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnConsulta
        '
        Me.btnConsulta.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConsulta.Location = New System.Drawing.Point(756, 51)
        Me.btnConsulta.Name = "btnConsulta"
        Me.btnConsulta.Size = New System.Drawing.Size(113, 32)
        Me.btnConsulta.TabIndex = 6
        Me.btnConsulta.Text = "&Consultar"
        Me.btnConsulta.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnModificar.Location = New System.Drawing.Point(756, 89)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(113, 32)
        Me.btnModificar.TabIndex = 7
        Me.btnModificar.Text = "&Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(756, 602)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(113, 32)
        Me.btnSalir.TabIndex = 9
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'gbxEspecificaciones
        '
        Me.gbxEspecificaciones.Controls.Add(Me.cbxActivo)
        Me.gbxEspecificaciones.Controls.Add(Me.CMBlblActivoEspecif)
        Me.gbxEspecificaciones.Controls.Add(Me.txtDescripcionEspecif)
        Me.gbxEspecificaciones.Controls.Add(Me.CMBlblDescripcionEspecif)
        Me.gbxEspecificaciones.Controls.Add(Me.txtClaveEspecif)
        Me.gbxEspecificaciones.Controls.Add(Me.CMBlblClaveEspecif)
        Me.gbxEspecificaciones.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxEspecificaciones.Location = New System.Drawing.Point(5, 405)
        Me.gbxEspecificaciones.Name = "gbxEspecificaciones"
        Me.gbxEspecificaciones.Size = New System.Drawing.Size(155, 229)
        Me.gbxEspecificaciones.TabIndex = 10
        Me.gbxEspecificaciones.TabStop = False
        Me.gbxEspecificaciones.Text = "Especificaciones"
        '
        'cbxActivo
        '
        Me.cbxActivo.AutoSize = True
        Me.cbxActivo.Enabled = False
        Me.cbxActivo.Location = New System.Drawing.Point(68, 199)
        Me.cbxActivo.Name = "cbxActivo"
        Me.cbxActivo.Size = New System.Drawing.Size(15, 14)
        Me.cbxActivo.TabIndex = 15
        Me.cbxActivo.UseVisualStyleBackColor = True
        '
        'CMBlblActivoEspecif
        '
        Me.CMBlblActivoEspecif.AutoSize = True
        Me.CMBlblActivoEspecif.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblActivoEspecif.Location = New System.Drawing.Point(8, 197)
        Me.CMBlblActivoEspecif.Name = "CMBlblActivoEspecif"
        Me.CMBlblActivoEspecif.Size = New System.Drawing.Size(52, 15)
        Me.CMBlblActivoEspecif.TabIndex = 16
        Me.CMBlblActivoEspecif.Text = "Activo :"
        '
        'txtDescripcionEspecif
        '
        Me.txtDescripcionEspecif.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescripcionEspecif.Location = New System.Drawing.Point(6, 104)
        Me.txtDescripcionEspecif.Multiline = True
        Me.txtDescripcionEspecif.Name = "txtDescripcionEspecif"
        Me.txtDescripcionEspecif.ReadOnly = True
        Me.txtDescripcionEspecif.Size = New System.Drawing.Size(143, 75)
        Me.txtDescripcionEspecif.TabIndex = 15
        '
        'CMBlblDescripcionEspecif
        '
        Me.CMBlblDescripcionEspecif.AutoSize = True
        Me.CMBlblDescripcionEspecif.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblDescripcionEspecif.Location = New System.Drawing.Point(8, 86)
        Me.CMBlblDescripcionEspecif.Name = "CMBlblDescripcionEspecif"
        Me.CMBlblDescripcionEspecif.Size = New System.Drawing.Size(91, 15)
        Me.CMBlblDescripcionEspecif.TabIndex = 15
        Me.CMBlblDescripcionEspecif.Text = "Descripción :"
        '
        'txtClaveEspecif
        '
        Me.txtClaveEspecif.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClaveEspecif.Location = New System.Drawing.Point(6, 49)
        Me.txtClaveEspecif.Name = "txtClaveEspecif"
        Me.txtClaveEspecif.ReadOnly = True
        Me.txtClaveEspecif.Size = New System.Drawing.Size(143, 21)
        Me.txtClaveEspecif.TabIndex = 15
        '
        'CMBlblClaveEspecif
        '
        Me.CMBlblClaveEspecif.AutoSize = True
        Me.CMBlblClaveEspecif.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblClaveEspecif.Location = New System.Drawing.Point(8, 31)
        Me.CMBlblClaveEspecif.Name = "CMBlblClaveEspecif"
        Me.CMBlblClaveEspecif.Size = New System.Drawing.Size(50, 15)
        Me.CMBlblClaveEspecif.TabIndex = 15
        Me.CMBlblClaveEspecif.Text = "Clave :"
        '
        'CMBlblClave
        '
        Me.CMBlblClave.AutoSize = True
        Me.CMBlblClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblClave.Location = New System.Drawing.Point(12, 70)
        Me.CMBlblClave.Name = "CMBlblClave"
        Me.CMBlblClave.Size = New System.Drawing.Size(50, 15)
        Me.CMBlblClave.TabIndex = 11
        Me.CMBlblClave.Text = "Clave :"
        '
        'txtBuscarClave
        '
        Me.txtBuscarClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBuscarClave.Location = New System.Drawing.Point(7, 88)
        Me.txtBuscarClave.Name = "txtBuscarClave"
        Me.txtBuscarClave.Size = New System.Drawing.Size(153, 21)
        Me.txtBuscarClave.TabIndex = 12
        '
        'btnBuscarClave
        '
        Me.btnBuscarClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscarClave.Location = New System.Drawing.Point(92, 114)
        Me.btnBuscarClave.Name = "btnBuscarClave"
        Me.btnBuscarClave.Size = New System.Drawing.Size(68, 22)
        Me.btnBuscarClave.TabIndex = 13
        Me.btnBuscarClave.Text = "&Buscar"
        Me.btnBuscarClave.UseVisualStyleBackColor = True
        '
        'btnBuscarDescripcion
        '
        Me.btnBuscarDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscarDescripcion.Location = New System.Drawing.Point(92, 247)
        Me.btnBuscarDescripcion.Name = "btnBuscarDescripcion"
        Me.btnBuscarDescripcion.Size = New System.Drawing.Size(68, 22)
        Me.btnBuscarDescripcion.TabIndex = 14
        Me.btnBuscarDescripcion.Text = "&Buscar"
        Me.btnBuscarDescripcion.UseVisualStyleBackColor = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Clave
        '
        Me.Clave.DataPropertyName = "clvProblema"
        Me.Clave.HeaderText = "Clave"
        Me.Clave.Name = "Clave"
        Me.Clave.ReadOnly = True
        '
        'Descripcion
        '
        Me.Descripcion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Descripcion.DataPropertyName = "descripcion"
        Me.Descripcion.HeaderText = "Descripción"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        '
        'Activo
        '
        Me.Activo.DataPropertyName = "activo"
        Me.Activo.HeaderText = "Activo"
        Me.Activo.Name = "Activo"
        Me.Activo.ReadOnly = True
        '
        'BrwClasificacionProblemas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(872, 646)
        Me.Controls.Add(Me.btnBuscarDescripcion)
        Me.Controls.Add(Me.btnBuscarClave)
        Me.Controls.Add(Me.txtBuscarClave)
        Me.Controls.Add(Me.CMBlblClave)
        Me.Controls.Add(Me.gbxEspecificaciones)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.btnConsulta)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.dgvProblemas)
        Me.Controls.Add(Me.txtBuscarDescripcion)
        Me.Controls.Add(Me.CMBlblBuscarDescripcion)
        Me.Controls.Add(Me.CMBlblBusqueda)
        Me.Name = "BrwClasificacionProblemas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Clasificación de Problemas"
        CType(Me.dgvProblemas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxEspecificaciones.ResumeLayout(False)
        Me.gbxEspecificaciones.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents CMBlblBusqueda As System.Windows.Forms.Label
    Friend WithEvents CMBlblBuscarDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtBuscarDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents dgvProblemas As System.Windows.Forms.DataGridView
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents btnConsulta As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents gbxEspecificaciones As System.Windows.Forms.GroupBox
    Friend WithEvents cbxActivo As System.Windows.Forms.CheckBox
    Friend WithEvents CMBlblActivoEspecif As System.Windows.Forms.Label
    Friend WithEvents txtDescripcionEspecif As System.Windows.Forms.TextBox
    Friend WithEvents CMBlblDescripcionEspecif As System.Windows.Forms.Label
    Friend WithEvents txtClaveEspecif As System.Windows.Forms.TextBox
    Friend WithEvents CMBlblClaveEspecif As System.Windows.Forms.Label
    Friend WithEvents CMBlblClave As System.Windows.Forms.Label
    Friend WithEvents txtBuscarClave As System.Windows.Forms.TextBox
    Friend WithEvents btnBuscarClave As System.Windows.Forms.Button
    Friend WithEvents btnBuscarDescripcion As System.Windows.Forms.Button
    Friend WithEvents Clave As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Activo As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
