<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelPeriodoCartera
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.ListBox2 = New System.Windows.Forms.ListBox()
        Me.MuestraSelperiodoCarteraConsultaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Procedimientosarnoldo4 = New sofTV.Procedimientosarnoldo4()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.MuestraSelperiodoCarteratmpConsultaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Muestra_Sel_periodoCarteratmpConsultaTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Muestra_Sel_periodoCarteratmpConsultaTableAdapter()
        Me.Muestra_Sel_periodoCarteraConsultaTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Muestra_Sel_periodoCarteraConsultaTableAdapter()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.CatalogoPeriodosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CatalogoPeriodosTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.CatalogoPeriodosTableAdapter()
        Me.InsertaTodo_Sel_PeriodoCarteraBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertaTodo_Sel_PeriodoCarteraTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.InsertaTodo_Sel_PeriodoCarteraTableAdapter()
        Me.InsertaTodo_Sel_PeriodoCarteratmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertaTodo_Sel_PeriodoCarteratmpTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.InsertaTodo_Sel_PeriodoCarteratmpTableAdapter()
        Me.InsertaUno_Sel_periodocarteraBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertaUno_Sel_periodocarteraTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.InsertaUno_Sel_periodocarteraTableAdapter()
        Me.InsertaUno_Sel_periodocarteratmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertaUno_Sel_periodocarteratmpTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.InsertaUno_Sel_periodocarteratmpTableAdapter()
        CType(Me.MuestraSelperiodoCarteraConsultaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraSelperiodoCarteratmpConsultaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CatalogoPeriodosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertaTodo_Sel_PeriodoCarteraBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertaTodo_Sel_PeriodoCarteratmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertaUno_Sel_periodocarteraBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertaUno_Sel_periodocarteratmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(410, 312)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 36)
        Me.Button5.TabIndex = 67
        Me.Button5.Text = "&Cancelar"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkOrange
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(230, 312)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(136, 36)
        Me.Button6.TabIndex = 66
        Me.Button6.Text = "&Aceptar"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'ListBox2
        '
        Me.ListBox2.DataSource = Me.MuestraSelperiodoCarteraConsultaBindingSource
        Me.ListBox2.DisplayMember = "Descripcion"
        Me.ListBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox2.FormattingEnabled = True
        Me.ListBox2.ItemHeight = 16
        Me.ListBox2.Location = New System.Drawing.Point(377, 45)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.ScrollAlwaysVisible = True
        Me.ListBox2.Size = New System.Drawing.Size(233, 244)
        Me.ListBox2.TabIndex = 69
        Me.ListBox2.TabStop = False
        Me.ListBox2.ValueMember = "clv_periodo"
        '
        'MuestraSelperiodoCarteraConsultaBindingSource
        '
        Me.MuestraSelperiodoCarteraConsultaBindingSource.DataMember = "Muestra_Sel_periodoCarteraConsulta"
        Me.MuestraSelperiodoCarteraConsultaBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Procedimientosarnoldo4
        '
        Me.Procedimientosarnoldo4.DataSetName = "Procedimientosarnoldo4"
        Me.Procedimientosarnoldo4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ListBox1
        '
        Me.ListBox1.DataSource = Me.MuestraSelperiodoCarteratmpConsultaBindingSource
        Me.ListBox1.DisplayMember = "Descripcion"
        Me.ListBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.ItemHeight = 16
        Me.ListBox1.Location = New System.Drawing.Point(15, 45)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.ScrollAlwaysVisible = True
        Me.ListBox1.Size = New System.Drawing.Size(239, 244)
        Me.ListBox1.TabIndex = 68
        Me.ListBox1.TabStop = False
        Me.ListBox1.ValueMember = "clv_periodo"
        '
        'MuestraSelperiodoCarteratmpConsultaBindingSource
        '
        Me.MuestraSelperiodoCarteratmpConsultaBindingSource.DataMember = "Muestra_Sel_periodoCarteratmpConsulta"
        Me.MuestraSelperiodoCarteratmpConsultaBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel2.Location = New System.Drawing.Point(424, 27)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(186, 15)
        Me.CMBLabel2.TabIndex = 71
        Me.CMBLabel2.Text = "Periodo(s) Seleccionado(s):"
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(12, 27)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(197, 15)
        Me.CMBLabel1.TabIndex = 70
        Me.CMBLabel1.Text = "Seleccione el(los) periodo(s):"
        '
        'Muestra_Sel_periodoCarteratmpConsultaTableAdapter
        '
        Me.Muestra_Sel_periodoCarteratmpConsultaTableAdapter.ClearBeforeFill = True
        '
        'Muestra_Sel_periodoCarteraConsultaTableAdapter
        '
        Me.Muestra_Sel_periodoCarteraConsultaTableAdapter.ClearBeforeFill = True
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkRed
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.White
        Me.Button4.Location = New System.Drawing.Point(269, 183)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(84, 30)
        Me.Button4.TabIndex = 122
        Me.Button4.Text = "<< "
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkRed
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.Location = New System.Drawing.Point(269, 109)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(84, 30)
        Me.Button3.TabIndex = 121
        Me.Button3.Text = " >>"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkRed
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(269, 146)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(84, 30)
        Me.Button2.TabIndex = 120
        Me.Button2.Text = "< "
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkRed
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(269, 73)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(84, 30)
        Me.Button1.TabIndex = 119
        Me.Button1.Text = " >"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'CatalogoPeriodosBindingSource
        '
        Me.CatalogoPeriodosBindingSource.DataMember = "CatalogoPeriodos"
        Me.CatalogoPeriodosBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'CatalogoPeriodosTableAdapter
        '
        Me.CatalogoPeriodosTableAdapter.ClearBeforeFill = True
        '
        'InsertaTodo_Sel_PeriodoCarteraBindingSource
        '
        Me.InsertaTodo_Sel_PeriodoCarteraBindingSource.DataMember = "InsertaTodo_Sel_PeriodoCartera"
        Me.InsertaTodo_Sel_PeriodoCarteraBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'InsertaTodo_Sel_PeriodoCarteraTableAdapter
        '
        Me.InsertaTodo_Sel_PeriodoCarteraTableAdapter.ClearBeforeFill = True
        '
        'InsertaTodo_Sel_PeriodoCarteratmpBindingSource
        '
        Me.InsertaTodo_Sel_PeriodoCarteratmpBindingSource.DataMember = "InsertaTodo_Sel_PeriodoCarteratmp"
        Me.InsertaTodo_Sel_PeriodoCarteratmpBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'InsertaTodo_Sel_PeriodoCarteratmpTableAdapter
        '
        Me.InsertaTodo_Sel_PeriodoCarteratmpTableAdapter.ClearBeforeFill = True
        '
        'InsertaUno_Sel_periodocarteraBindingSource
        '
        Me.InsertaUno_Sel_periodocarteraBindingSource.DataMember = "InsertaUno_Sel_periodocartera"
        Me.InsertaUno_Sel_periodocarteraBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'InsertaUno_Sel_periodocarteraTableAdapter
        '
        Me.InsertaUno_Sel_periodocarteraTableAdapter.ClearBeforeFill = True
        '
        'InsertaUno_Sel_periodocarteratmpBindingSource
        '
        Me.InsertaUno_Sel_periodocarteratmpBindingSource.DataMember = "InsertaUno_Sel_periodocarteratmp"
        Me.InsertaUno_Sel_periodocarteratmpBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'InsertaUno_Sel_periodocarteratmpTableAdapter
        '
        Me.InsertaUno_Sel_periodocarteratmpTableAdapter.ClearBeforeFill = True
        '
        'FrmSelPeriodoCartera
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(633, 360)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.ListBox2)
        Me.Controls.Add(Me.ListBox1)
        Me.Name = "FrmSelPeriodoCartera"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selección Periodo Cartera"
        CType(Me.MuestraSelperiodoCarteraConsultaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraSelperiodoCarteratmpConsultaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CatalogoPeriodosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertaTodo_Sel_PeriodoCarteraBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertaTodo_Sel_PeriodoCarteratmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertaUno_Sel_periodocarteraBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertaUno_Sel_periodocarteratmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents MuestraSelperiodoCarteratmpConsultaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Procedimientosarnoldo4 As sofTV.Procedimientosarnoldo4
    Friend WithEvents Muestra_Sel_periodoCarteratmpConsultaTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Muestra_Sel_periodoCarteratmpConsultaTableAdapter
    Friend WithEvents MuestraSelperiodoCarteraConsultaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Sel_periodoCarteraConsultaTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Muestra_Sel_periodoCarteraConsultaTableAdapter
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents CatalogoPeriodosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CatalogoPeriodosTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.CatalogoPeriodosTableAdapter
    Friend WithEvents InsertaTodo_Sel_PeriodoCarteraBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertaTodo_Sel_PeriodoCarteraTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.InsertaTodo_Sel_PeriodoCarteraTableAdapter
    Friend WithEvents InsertaTodo_Sel_PeriodoCarteratmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertaTodo_Sel_PeriodoCarteratmpTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.InsertaTodo_Sel_PeriodoCarteratmpTableAdapter
    Friend WithEvents InsertaUno_Sel_periodocarteraBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertaUno_Sel_periodocarteraTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.InsertaUno_Sel_periodocarteraTableAdapter
    Friend WithEvents InsertaUno_Sel_periodocarteratmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertaUno_Sel_periodocarteratmpTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.InsertaUno_Sel_periodocarteratmpTableAdapter
End Class
