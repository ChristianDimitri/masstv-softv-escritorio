﻿Imports System.Data.SqlClient
Imports sofTV.BAL
Imports System.Collections.Generic

Public Class BrwCompanias

#Region "Llenar Grid"
    Private Sub Consultar(ByVal prmClvCompania As Long, ByVal prmNombreCompania As String, ByVal prmRfc As String, ByVal prmOp As Integer)
        Dim Companias As New ClassCompanias
        Companias.clvCompania = prmClvCompania
        Companias.nombreCompania = prmNombreCompania
        Companias.rfc = prmRfc
        Companias.op = prmOp

        Me.dgvDatos.DataSource = Companias.uspConsultaTblCompanias
    End Sub
#End Region
    
#Region "Controles"

    Private Sub BrwCompanias_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Consultar(0, "", "", 1) 'PARA LLENAR EL GRID AL ARRANQUE DEL FORMULARIO
    End Sub

    Private Sub btnBuscarClave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscarClave.Click
        If Me.txtClave.Text.Length > 0 Then
            If IsNumeric(Me.txtClave.Text) = True Then
                Consultar(CInt(Me.txtClave.Text), "", "", 2)
            Else
                MsgBox("El valor debe ser numérico", MsgBoxStyle.Information)
                Exit Sub
            End If
        Else
            Consultar(0, "", "", 1)
        End If
    End Sub

    Private Sub btnBuscarNombreCompania_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscarNombreCompania.Click
        If Me.txtNombreCompania.Text.Length > 0 Then
            Consultar(0, Me.txtNombreCompania.Text, "", 3)
        Else
            Consultar(0, "", "", 1)
        End If
    End Sub

    Private Sub dgvDatos_CurrentCellChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvDatos.CurrentCellChanged
        Try
            If Me.dgvDatos.RowCount > 0 Then
                Me.id_companiaLbl.Text = CStr(Me.dgvDatos.CurrentRow.Cells(0).Value)
                Me.DescripcionLabel.Text = CStr(Me.dgvDatos.CurrentRow.Cells(1).Value)
                Me.CMBlblRfc.Text = CStr(Me.dgvDatos.CurrentRow.Cells(2).Value)
                Me.cbxActivo.Checked = CBool(Me.dgvDatos.CurrentRow.Cells(3).Value)
            End If
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "Botones ABC"
    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Dim frmCompania As New FrmCompanias

        frmCompania.CompaniasOpcion = "N"
        frmCompania.ShowDialog()
        Consultar(0, "", "", 1) 'PARA LLENAR EL GRID AL ARRANQUE DEL FORMULARIO
    End Sub

    Private Sub btnConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsultar.Click
        If Me.dgvDatos.RowCount = 0 Then
            MsgBox("¡No hay registros en la lista!", MsgBoxStyle.Information)
            Exit Sub
        End If

        If IsNumeric(Me.dgvDatos.SelectedCells(0).Value) = False Then
            MsgBox("¡Seleccione un registro!", MsgBoxStyle.Information)
            Exit Sub
        End If

        Dim frmCompania As New FrmCompanias

        frmCompania.CompaniasOpcion = "C"
        frmCompania.CompaniasClvCompania = CInt(Me.dgvDatos.SelectedCells(0).Value)
        frmCompania.CompaniasNombreCompania = CStr(Me.dgvDatos.SelectedCells(1).Value)
        frmCompania.CompaniasClvRfc = CInt(Me.dgvDatos.SelectedCells(4).Value)
        frmCompania.CompaniasActivo = CBool(Me.dgvDatos.CurrentRow.Cells(3).Value)
        frmCompania.ShowDialog()

        Consultar(0, "", "", 1) 'PARA LLENAR EL GRID AL ARRANQUE DEL FORMULARIO
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        If Me.dgvDatos.RowCount = 0 Then
            MsgBox("¡No hay registros en la lista!", MsgBoxStyle.Information)
            Exit Sub
        End If

        If IsNumeric(Me.dgvDatos.SelectedCells(0).Value) = False Then
            MsgBox("¡Seleccione un registro!", MsgBoxStyle.Information)
            Exit Sub
        End If

        Dim frmCompania As New FrmCompanias

        frmCompania.CompaniasOpcion = "M"
        frmCompania.CompaniasClvCompania = CInt(Me.dgvDatos.SelectedCells(0).Value)
        frmCompania.CompaniasNombreCompania = CStr(Me.dgvDatos.SelectedCells(1).Value)
        frmCompania.CompaniasClvRfc = CInt(Me.dgvDatos.SelectedCells(4).Value)
        frmCompania.CompaniasActivo = CBool(Me.dgvDatos.CurrentRow.Cells(3).Value)
        frmCompania.ShowDialog()

        Consultar(0, "", "", 1) 'PARA LLENAR EL GRID AL ARRANQUE DEL FORMULARIO
    End Sub
#End Region
End Class