﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmIngresosDesglozados
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.IdConceptoText = New System.Windows.Forms.TextBox()
        Me.CMBLabel5 = New System.Windows.Forms.Label()
        Me.IdGrupoCombo = New System.Windows.Forms.ComboBox()
        Me.SaveButton = New System.Windows.Forms.Button()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.gbxConcepto = New System.Windows.Forms.GroupBox()
        Me.ActivoCheckBox = New System.Windows.Forms.CheckBox()
        Me.CMBLabel3 = New System.Windows.Forms.Label()
        Me.ConceptoText = New System.Windows.Forms.TextBox()
        Me.PosicionText = New System.Windows.Forms.TextBox()
        Me.gbxDetalle = New System.Windows.Forms.GroupBox()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.dgvRelacion = New System.Windows.Forms.DataGridView()
        Me.Id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClaveCol = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Concepto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbxConceptoIngresos = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxGpoIngresos = New System.Windows.Forms.ComboBox()
        Me.txtClave = New System.Windows.Forms.TextBox()
        Me.txtID = New System.Windows.Forms.TextBox()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.gbxConcepto.SuspendLayout()
        Me.gbxDetalle.SuspendLayout()
        CType(Me.dgvRelacion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'IdConceptoText
        '
        Me.IdConceptoText.Enabled = False
        Me.IdConceptoText.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IdConceptoText.Location = New System.Drawing.Point(113, 30)
        Me.IdConceptoText.Name = "IdConceptoText"
        Me.IdConceptoText.Size = New System.Drawing.Size(124, 21)
        Me.IdConceptoText.TabIndex = 17
        '
        'CMBLabel5
        '
        Me.CMBLabel5.AutoSize = True
        Me.CMBLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel5.Location = New System.Drawing.Point(47, 31)
        Me.CMBLabel5.Name = "CMBLabel5"
        Me.CMBLabel5.Size = New System.Drawing.Size(60, 16)
        Me.CMBLabel5.TabIndex = 18
        Me.CMBLabel5.Text = "Clave : "
        '
        'IdGrupoCombo
        '
        Me.IdGrupoCombo.DisplayMember = "CONCEPTO"
        Me.IdGrupoCombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IdGrupoCombo.FormattingEnabled = True
        Me.IdGrupoCombo.Location = New System.Drawing.Point(113, 108)
        Me.IdGrupoCombo.Name = "IdGrupoCombo"
        Me.IdGrupoCombo.Size = New System.Drawing.Size(450, 23)
        Me.IdGrupoCombo.TabIndex = 37
        Me.IdGrupoCombo.ValueMember = "IdGrupoIngresos"
        '
        'SaveButton
        '
        Me.SaveButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SaveButton.Location = New System.Drawing.Point(806, 160)
        Me.SaveButton.Name = "SaveButton"
        Me.SaveButton.Size = New System.Drawing.Size(142, 35)
        Me.SaveButton.TabIndex = 49
        Me.SaveButton.Text = "&Guardar"
        Me.SaveButton.UseVisualStyleBackColor = True
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(21, 57)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(86, 16)
        Me.CMBLabel1.TabIndex = 19
        Me.CMBLabel1.Text = "Concepto : "
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(27, 83)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(80, 16)
        Me.CMBLabel2.TabIndex = 22
        Me.CMBLabel2.Text = "Posición : "
        '
        'gbxConcepto
        '
        Me.gbxConcepto.Controls.Add(Me.IdConceptoText)
        Me.gbxConcepto.Controls.Add(Me.SaveButton)
        Me.gbxConcepto.Controls.Add(Me.CMBLabel5)
        Me.gbxConcepto.Controls.Add(Me.IdGrupoCombo)
        Me.gbxConcepto.Controls.Add(Me.CMBLabel1)
        Me.gbxConcepto.Controls.Add(Me.CMBLabel2)
        Me.gbxConcepto.Controls.Add(Me.ActivoCheckBox)
        Me.gbxConcepto.Controls.Add(Me.CMBLabel3)
        Me.gbxConcepto.Controls.Add(Me.ConceptoText)
        Me.gbxConcepto.Controls.Add(Me.PosicionText)
        Me.gbxConcepto.Location = New System.Drawing.Point(12, 12)
        Me.gbxConcepto.Name = "gbxConcepto"
        Me.gbxConcepto.Size = New System.Drawing.Size(984, 213)
        Me.gbxConcepto.TabIndex = 51
        Me.gbxConcepto.TabStop = False
        '
        'ActivoCheckBox
        '
        Me.ActivoCheckBox.AutoSize = True
        Me.ActivoCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ActivoCheckBox.Location = New System.Drawing.Point(95, 135)
        Me.ActivoCheckBox.Name = "ActivoCheckBox"
        Me.ActivoCheckBox.Size = New System.Drawing.Size(70, 20)
        Me.ActivoCheckBox.TabIndex = 45
        Me.ActivoCheckBox.Text = "Activo"
        Me.ActivoCheckBox.UseVisualStyleBackColor = True
        '
        'CMBLabel3
        '
        Me.CMBLabel3.AutoSize = True
        Me.CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.Location = New System.Drawing.Point(49, 109)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(58, 16)
        Me.CMBLabel3.TabIndex = 38
        Me.CMBLabel3.Text = "Grupo :"
        '
        'ConceptoText
        '
        Me.ConceptoText.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConceptoText.Location = New System.Drawing.Point(113, 56)
        Me.ConceptoText.Name = "ConceptoText"
        Me.ConceptoText.Size = New System.Drawing.Size(551, 21)
        Me.ConceptoText.TabIndex = 20
        '
        'PosicionText
        '
        Me.PosicionText.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PosicionText.Location = New System.Drawing.Point(113, 82)
        Me.PosicionText.Name = "PosicionText"
        Me.PosicionText.Size = New System.Drawing.Size(124, 21)
        Me.PosicionText.TabIndex = 21
        '
        'gbxDetalle
        '
        Me.gbxDetalle.Controls.Add(Me.btnEliminar)
        Me.gbxDetalle.Controls.Add(Me.btnAgregar)
        Me.gbxDetalle.Controls.Add(Me.dgvRelacion)
        Me.gbxDetalle.Controls.Add(Me.Label2)
        Me.gbxDetalle.Controls.Add(Me.cbxConceptoIngresos)
        Me.gbxDetalle.Controls.Add(Me.Label1)
        Me.gbxDetalle.Controls.Add(Me.cbxGpoIngresos)
        Me.gbxDetalle.Controls.Add(Me.txtClave)
        Me.gbxDetalle.Controls.Add(Me.txtID)
        Me.gbxDetalle.Location = New System.Drawing.Point(12, 231)
        Me.gbxDetalle.Name = "gbxDetalle"
        Me.gbxDetalle.Size = New System.Drawing.Size(984, 446)
        Me.gbxDetalle.TabIndex = 52
        Me.gbxDetalle.TabStop = False
        '
        'btnEliminar
        '
        Me.btnEliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminar.Location = New System.Drawing.Point(587, 47)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(85, 27)
        Me.btnEliminar.TabIndex = 53
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnAgregar
        '
        Me.btnAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregar.Location = New System.Drawing.Point(496, 47)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(85, 27)
        Me.btnAgregar.TabIndex = 50
        Me.btnAgregar.Text = "&Agregar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'dgvRelacion
        '
        Me.dgvRelacion.AllowUserToAddRows = False
        Me.dgvRelacion.AllowUserToDeleteRows = False
        Me.dgvRelacion.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvRelacion.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvRelacion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRelacion.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Id, Me.ClaveCol, Me.Concepto})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvRelacion.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvRelacion.Location = New System.Drawing.Point(6, 88)
        Me.dgvRelacion.Name = "dgvRelacion"
        Me.dgvRelacion.ReadOnly = True
        Me.dgvRelacion.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvRelacion.Size = New System.Drawing.Size(972, 352)
        Me.dgvRelacion.TabIndex = 42
        '
        'Id
        '
        Me.Id.DataPropertyName = "Id"
        Me.Id.HeaderText = "Id"
        Me.Id.Name = "Id"
        Me.Id.ReadOnly = True
        Me.Id.Visible = False
        '
        'ClaveCol
        '
        Me.ClaveCol.DataPropertyName = "Clave"
        Me.ClaveCol.HeaderText = "Clave"
        Me.ClaveCol.Name = "ClaveCol"
        Me.ClaveCol.ReadOnly = True
        Me.ClaveCol.Width = 150
        '
        'Concepto
        '
        Me.Concepto.DataPropertyName = "Concepto"
        Me.Concepto.HeaderText = "Concepto"
        Me.Concepto.Name = "Concepto"
        Me.Concepto.ReadOnly = True
        Me.Concepto.Width = 600
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(21, 52)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(159, 16)
        Me.Label2.TabIndex = 41
        Me.Label2.Text = "Concepto de Polizas :"
        '
        'cbxConceptoIngresos
        '
        Me.cbxConceptoIngresos.DisplayMember = "CONCEPTO"
        Me.cbxConceptoIngresos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxConceptoIngresos.FormattingEnabled = True
        Me.cbxConceptoIngresos.Location = New System.Drawing.Point(195, 51)
        Me.cbxConceptoIngresos.Name = "cbxConceptoIngresos"
        Me.cbxConceptoIngresos.Size = New System.Drawing.Size(284, 23)
        Me.cbxConceptoIngresos.TabIndex = 40
        Me.cbxConceptoIngresos.ValueMember = "CLAVE"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(21, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(135, 16)
        Me.Label1.TabIndex = 39
        Me.Label1.Text = "Grupo de Polizas :"
        '
        'cbxGpoIngresos
        '
        Me.cbxGpoIngresos.DisplayMember = "Concepto"
        Me.cbxGpoIngresos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxGpoIngresos.FormattingEnabled = True
        Me.cbxGpoIngresos.Location = New System.Drawing.Point(165, 24)
        Me.cbxGpoIngresos.Name = "cbxGpoIngresos"
        Me.cbxGpoIngresos.Size = New System.Drawing.Size(284, 23)
        Me.cbxGpoIngresos.TabIndex = 0
        Me.cbxGpoIngresos.ValueMember = "IdGrupo"
        '
        'txtClave
        '
        Me.txtClave.Location = New System.Drawing.Point(371, 215)
        Me.txtClave.Name = "txtClave"
        Me.txtClave.Size = New System.Drawing.Size(39, 20)
        Me.txtClave.TabIndex = 52
        Me.txtClave.Visible = False
        '
        'txtID
        '
        Me.txtID.Location = New System.Drawing.Point(371, 180)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(39, 20)
        Me.txtID.TabIndex = 51
        Me.txtID.Visible = False
        '
        'btnSalir
        '
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(850, 683)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(140, 35)
        Me.btnSalir.TabIndex = 53
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'FrmIngresosDesglozados
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1008, 730)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.gbxDetalle)
        Me.Controls.Add(Me.gbxConcepto)
        Me.Name = "FrmIngresosDesglozados"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Conceptos de Desglose de Ingresos"
        Me.gbxConcepto.ResumeLayout(False)
        Me.gbxConcepto.PerformLayout()
        Me.gbxDetalle.ResumeLayout(False)
        Me.gbxDetalle.PerformLayout()
        CType(Me.dgvRelacion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents IdConceptoText As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel5 As System.Windows.Forms.Label
    Friend WithEvents IdGrupoCombo As System.Windows.Forms.ComboBox
    Friend WithEvents SaveButton As System.Windows.Forms.Button
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents gbxConcepto As System.Windows.Forms.GroupBox
    Friend WithEvents ActivoCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents ConceptoText As System.Windows.Forms.TextBox
    Friend WithEvents PosicionText As System.Windows.Forms.TextBox
    Friend WithEvents gbxDetalle As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbxConceptoIngresos As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbxGpoIngresos As System.Windows.Forms.ComboBox
    Friend WithEvents dgvRelacion As System.Windows.Forms.DataGridView
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents txtClave As System.Windows.Forms.TextBox
    Friend WithEvents txtID As System.Windows.Forms.TextBox
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents Id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClaveCol As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Concepto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
