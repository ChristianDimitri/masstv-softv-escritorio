Imports System.Data.SqlClient
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Text

Public Class FrmLlamadasTelefonicasReporte
    Private customersByCityReport As ReportDocument
    Dim op1 As Integer = 0
    Dim Titulo As String = Nothing
    Dim Op10 As String = "0", Op20 As String = "0", Op30 As String = "0", Op40 As String = "0", Op50 As String = "0", Op60 As String = "0", Op70 As String = "0"
    Dim eClv_Usuario As String
    Dim eClv_Colonia As Integer = 0
    Dim StatusQueja As String = Nothing
    Dim eClv_Trabajo As Integer = 0
    Dim idTurnoSeleccionado As Integer = 0
    Private Sub ConfigureCrystalReports(ByVal Titulo As String, ByVal op As Integer)
        'Try
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        Dim Subtitulo As String = Nothing
        Dim Pendiente As String = Nothing
        Dim clv_queja As String = "0"
        Dim clv_ordenini As String = "0", clv_ordenfin As String = "0"
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")

        Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900"
        Dim Num1 As String = 0, Num2 As String = 0

        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        If Me.CheckBox7.Checked = True Then
            Op70 = "1"
        End If

        If Gloclv_trabajo = "" Then
            Gloclv_trabajo = "0"
        End If
        Dim mySelectFormula As String = Titulo

        Dim reportPath As String = Nothing
        If op = 0 Then
            reportPath = RutaReportes + "\Reportellamadas.rpt"
        Else
            If IdSistema = "TO" Then
                reportPath = RutaReportes + "\FormatollamadasTelefonicasCabStar.rpt"
            ElseIf IdSistema = "AG" Then
                reportPath = RutaReportes + "\FormatollamadasTelefonicas.rpt"
            ElseIf IdSistema = "SA" Then
                reportPath = RutaReportes + "\FormatollamadasTelefonicasTvRey.rpt"
            ElseIf IdSistema = "VA" Then
                reportPath = RutaReportes + "\FormatollamadasTelefonicasCosmo.rpt"
            End If
        End If




        'MsgBox(reportPath)
        customersByCityReport.Load(reportPath)
        SetDBLogonForReport(connectionInfo, customersByCityReport)
        '@Clv_TipSer int
        customersByCityReport.SetParameterValue(0, CStr(GloClv_TipSer))
        ',@op0 smallint
        customersByCityReport.SetParameterValue(1, CInt(Op10))
        ',@op1 smallint
        customersByCityReport.SetParameterValue(2, CInt(Op20))
        ',@op2 smallint
        customersByCityReport.SetParameterValue(3, CInt(Op30))
        ',@op3 smallint
        customersByCityReport.SetParameterValue(4, CInt(Op40))
        ',@op4 smallint
        customersByCityReport.SetParameterValue(5, CInt(Op50))
        ',@op5 smallint
        customersByCityReport.SetParameterValue(6, CInt(Op60))
        ',@sinqueja bit
        customersByCityReport.SetParameterValue(7, GloPendientes)
        ',@conqueja bit 
        customersByCityReport.SetParameterValue(8, GloEjecutadas)
        ',@ambas bit,
        customersByCityReport.SetParameterValue(9, GloVisita)
        '@Clv_OrdenIni bigint
        If IsNumeric(Me.NUMINILbl.Text) = True Then clv_ordenini = Me.NUMINILbl.Text
        customersByCityReport.SetParameterValue(10, CInt(clv_ordenini))
        '@clv_OrdenFin bigint 
        If IsNumeric(Me.NUMFINLBL.Text) = True Then clv_ordenfin = Me.NUMFINLBL.Text
        customersByCityReport.SetParameterValue(11, CInt(clv_ordenfin))
        ',@Fec1Ini Datetime
        If IsDate(Me.FECSOLINI.Text) = True Then Fec1Ini = Me.FECSOLINI.Text
        customersByCityReport.SetParameterValue(12, Fec1Ini)
        ',@Fec2Fin Datetime
        If IsDate(Me.FECSOLFIN.Text) = True Then Fec1Fin = Me.FECSOLFIN.Text
        customersByCityReport.SetParameterValue(13, Fec1Fin)
        ',@Clv_Trabajo int
        customersByCityReport.SetParameterValue(14, CInt(Me.DescripcionComboBox.SelectedValue))
        '@clv_queja
        If IsNumeric(Me.Label8.Text) = True Then clv_queja = Me.Label8.Text
        customersByCityReport.SetParameterValue(15, CInt(clv_queja))
        ',@OpOrden int
        customersByCityReport.SetParameterValue(16, op1)
        ',@Nombre varchar(50)

        eClv_Usuario = CStr(Me.NOMBREComboBox.SelectedValue)

        If Len(eClv_Usuario) = 0 Then eClv_Usuario = "0"
        customersByCityReport.SetParameterValue(17, eClv_Usuario)



        'StatusQueja
        '@Op7
    

        If Me.EjecutadasCheckBox.Checked = True Then
            If Me.RadioButton5.Checked = True Then
                StatusQueja = "P"
            ElseIf Me.RadioButton6.Checked = True Then
                StatusQueja = "E"
            ElseIf Me.RadioButton7.Checked = True Then
                StatusQueja = "V"
            Else
                StatusQueja = "T"
            End If
        Else
            StatusQueja = "T"
        End If
        customersByCityReport.SetParameterValue(18, StatusQueja)
        ',@Clv_Colonia
        eClv_Colonia = Me.NombreComboBox1.SelectedValue
        customersByCityReport.SetParameterValue(19, eClv_Colonia)
        
        customersByCityReport.SetParameterValue(20, Op70)
        '@Contrato
        If IsNumeric(Me.TextBox1.Text) = False Then Me.TextBox1.Text = 0
        customersByCityReport.SetParameterValue(21, Me.TextBox1.Text)

        'Filtro de B�squeda por Turno
        customersByCityReport.SetParameterValue(22, idTurnoSeleccionado)

        'Titulos de Reporte
        If op = 0 Then

            mySelectFormula = "Reporte de " & Titulo & " V�a Telef�nica"
            Subtitulo = "Del: " & Fec1Ini & " Al:" & Fec1Fin
            Pendiente = "Clientes de Atenci�n Tel�fonica de " & Titulo & " a los que se le genero una queja"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Subtitulo & "'"
            customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & GloSucursal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Dependiente").Text = "'" & Pendiente & "'"
        Else
            mySelectFormula = "Orden "
            customersByCityReport.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"
        End If


        CrystalReportViewer1.ReportSource = customersByCityReport


        customersByCityReport = Nothing
        'Catch ex As System.Exception
        'System.Windows.Forms.MessageBox.Show(ex.Message)
        ' End Try
    End Sub
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub SetDBLogonForReport2(ByVal myConnectionInfo As ConnectionInfo)
        Dim myTableLogOnInfos As TableLogOnInfos = Me.CrystalReportViewer1.LogOnInfo
        For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
            myTableLogOnInfo.ConnectionInfo = myConnectionInfo
        Next
    End Sub
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub FrmLlamadasTelefonicasReporte_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If Op10 = "1" And GloBusca_Numllamada = True Then
            GloBusca_Numllamada = False
            Me.NUMFINLBL.Text = GlONUMCLVllamada_fin
            Me.NUMINILbl.Text = GlONUMCLVllamada_ini
            GlONUMCLVllamada_fin = 0
            GlONUMCLVllamada_ini = 0
        End If
        If Op20 = "1" And GloBndSelFecha = True Then
            GloBndSelFecha = False
            Me.FECSOLINI.Text = GloFecha_Ini
            Me.FECSOLFIN.Text = GloFecha_Fin
            GloFecha_Ini = "01/01/1900"
            GloFecha_Fin = "01/01/1900"
        End If
        If Op30 = "1" And GloBndTrabajo = True Then
            GloBndTrabajo = False
            Me.NOMTRABAJO.Text = GLONOMTRABAJO
            If Me.NOMTRABAJO.Text <> "" Then
                Me.CheckBox5.Visible = True
                Me.Label6.Visible = True
                Me.Label8.Visible = True
            End If
        End If
        If Op40 = "1" Then
            Me.Label8.Text = GLOCLV_oRDENbus
        End If
        'End If
    End Sub

    Private Sub FrmLlamadasTelefonicasReporte_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)




        Me.MuestraTipSerPrincipal2TableAdapter.Connection = CON
        Me.MuestraTipSerPrincipal2TableAdapter.Fill(Me.DataSetLidia2.MuestraTipSerPrincipal2)
        Me.MUESTRAUSUARIOSTableAdapter.Connection = CON
        Me.MUESTRAUSUARIOSTableAdapter.Fill(Me.DataSetEric2.MUESTRAUSUARIOS, 2)
        Me.MUESTRATRABAJOSQUEJASTableAdapter.Connection = CON
        Me.MUESTRATRABAJOSQUEJASTableAdapter.Fill(Me.DataSetEric2.MUESTRATRABAJOSQUEJAS, GloClv_TipSer)
        'Me.MUESTRACOLONIASTableAdapter.Connection = CON
        'Me.MUESTRACOLONIASTableAdapter.Fill(Me.DataSetEric.MUESTRACOLONIAS, 0)
        MuestraColonias(0, 0, 0)
        CON.Close()
        If IsNumeric(Me.ComboBox2.SelectedValue) = True Then
            GloClv_TipSer = Me.ComboBox2.SelectedValue
        Else
            GloClv_TipSer = glotiposervicioppal
        End If
        'GloClv_TipSer = 0

        'Cargamos los Turnos
        cmbxTurno.DataSource = spConsultaTurnos()
        cmbxTurno.ValueMember = "ID"
        cmbxTurno.DisplayMember = "TURNO"

        idTurnoSeleccionado = 0

    End Sub

    Private Sub MuestraColonias(ByVal opcion0 As Long, ByVal Opcion1 As Long, ByVal opcion As Long)
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder

        StrSQL.Append("EXEC MuestraColoniaSec ")
        StrSQL.Append(CStr(opcion0) & ", ")
        StrSQL.Append(CStr(Opcion1) & ", ")
        StrSQL.Append(CStr(opcion))


        Dim DA As New SqlDataAdapter(StrSQL.ToString, CON)
        Dim DT As New DataTable
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            NombreComboBox1.DataSource = BS.DataSource



        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try

    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If Me.EjecutadasCheckBox.CheckState = False And Me.VisitaCheckBox.CheckState = False And Me.PendientesCheckBox.CheckState = False Then
            MsgBox("Primero Seleccione si es con queja o sin queja", MsgBoxStyle.Information)
        Else
            If Me.CheckBox1.CheckState = CheckState.Checked Then
                Op10 = "1"
                FrmSelOrdSer.Show()
            Else
                Op10 = "0"
            End If
           
        End If
    End Sub

    Private Sub PendientesCheckBox_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles PendientesCheckBox.Click

        Me.PendientesCheckBox.Enabled = False
        Me.EjecutadasCheckBox.Enabled = True
        Me.EjecutadasCheckBox.Checked = False
        Me.VisitaCheckBox.Enabled = True
        Me.VisitaCheckBox.Checked = False

        'Midificado el 10 de Octubre de 2008
        'Me.CheckBox4.Visible = False
        'Me.Label18.Visible = False
        'Me.NOMTRABAJO.Visible = False
        'Me.DescripcionComboBox.Visible = False

        Me.CheckBox5.Visible = False
        Me.Label6.Visible = False
        Me.Label8.Visible = False

        Me.Label7.Visible = False
        Me.RadioButton5.Visible = False
        Me.RadioButton6.Visible = False
        Me.RadioButton7.Visible = False


        Me.RadioButton2.Visible = False
        Op30 = 0
        Op40 = 0
        GloPendientes = 1
        GloEjecutadas = 0
        GloVisita = 0

    End Sub

    Private Sub VisitaCheckBox_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles VisitaCheckBox.Click

        Me.VisitaCheckBox.Enabled = False
        Me.EjecutadasCheckBox.Enabled = True
        Me.EjecutadasCheckBox.Checked = False
        Me.PendientesCheckBox.Enabled = True
        Me.PendientesCheckBox.Checked = False


        'Me.CheckBox4.Visible = False
        'Me.Label18.Visible = False
        'Me.NOMTRABAJO.Visible = False
        'Me.DescripcionComboBox.Visible = False

        Me.CheckBox5.Visible = False
        Me.Label6.Visible = False
        Me.Label8.Visible = False

        Me.Label7.Visible = False
        Me.RadioButton5.Visible = False
        Me.RadioButton6.Visible = False
        Me.RadioButton7.Visible = False


        Me.RadioButton2.Visible = False
        Op30 = 0
        Op40 = 0
        GloPendientes = 0
        GloEjecutadas = 0
        GloVisita = 1
    End Sub

    Private Sub EjecutadasCheckBox_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles EjecutadasCheckBox.Click
        If Me.EjecutadasCheckBox.Checked = True Then

            Me.EjecutadasCheckBox.Enabled = False
            Me.PendientesCheckBox.Enabled = True
            Me.PendientesCheckBox.Checked = False
            Me.VisitaCheckBox.Enabled = True
            Me.VisitaCheckBox.Checked = False

            'Me.CheckBox4.Visible = True
            'Me.Label18.Visible = True
            'Me.NOMTRABAJO.Visible = True
            'Me.DescripcionComboBox.Visible = True

            Me.CheckBox5.Visible = True
            Me.Label6.Visible = True
            Me.Label8.Visible = True

            Me.Label7.Visible = True
            Me.RadioButton5.Visible = True
            Me.RadioButton6.Visible = True
            Me.RadioButton7.Visible = True


            Me.RadioButton2.Visible = True
            GloEjecutadas = 1
            GloPendientes = 0
            GloVisita = 0
        Else
            'Me.CheckBox4.Visible = False
            'Me.Label18.Visible = False
            'Me.NOMTRABAJO.Visible = False

            Me.CheckBox5.Visible = False
            Me.Label6.Visible = False
            Me.Label8.Visible = False

            Me.Label7.Visible = False
            Me.RadioButton5.Visible = False
            Me.RadioButton6.Visible = False
            Me.RadioButton7.Visible = False


            Me.RadioButton2.Visible = False
            GloEjecutadas = 0
            GloPendientes = 0
            GloVisita = 0
        End If
    End Sub

    'Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If IsNumeric(Me.ComboBox4.SelectedValue) = True Then
    '        GloClv_TipSer = Me.ComboBox4.SelectedValue
    '        Titulo = Me.ComboBox4.Text
    '        LIMPIA()
    '        Dim CON As New SqlConnection(MiConexion)
    '        CON.Open()
    '        Me.MUESTRATRABAJOSQUEJASTableAdapter.Connection = CON
    '        Me.MUESTRATRABAJOSQUEJASTableAdapter.Fill(Me.DataSetEric2.MUESTRATRABAJOSQUEJAS, GloClv_TipSer)
    '        CON.Close()
    '    End If
    'End Sub

    Private Sub NUMINILbl_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NUMINILbl.Click

    End Sub
    Private Sub LIMPIA()
        Me.CheckBox1.CheckState = CheckState.Unchecked
        Me.CheckBox2.CheckState = CheckState.Unchecked
        Me.CheckBox3.CheckState = CheckState.Unchecked
        Me.CheckBox5.CheckState = CheckState.Unchecked
        Me.CheckBox4.CheckState = CheckState.Unchecked
        Me.CheckBox7.CheckState = CheckState.Unchecked
        Me.TextBox1.Text = ""
        Me.NUMFINLBL.Text = 0
        Me.NUMINILbl.Text = 0
        Me.FECSOLFIN.Text = ""
        Me.FECSOLINI.Text = ""
        Me.NOMTRABAJO.Text = ""
    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If Me.CheckBox2.CheckState = CheckState.Checked Then
            Op20 = "1"
            FrmSelFechas.Show()
        Else
            Op20 = "0"
            Me.FECSOLINI.Text = ""
            Me.FECSOLFIN.Text = ""
        End If
    End Sub

    Private Sub CheckBox2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBox2.Click
       
    End Sub

    Private Sub CheckBox1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBox1.Click
        
    End Sub

    Private Sub CheckBox4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox4.CheckedChanged
        If Me.CheckBox4.CheckState = CheckState.Checked Then
            Op30 = "1"
            'FrmSelTrabajo.Show()
            Me.DescripcionComboBox.Enabled = True
        Else
            'Me.NOMTRABAJO.Text = ""
            Op30 = "0"
            Me.DescripcionComboBox.Enabled = False
        End If
    End Sub

    Private Sub CheckBox4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBox4.Click
       
    End Sub

    Private Sub Panel4_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel4.Paint

    End Sub


    Private Sub CheckBox5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBox5.Click
        
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Titulo = Me.ComboBox4.Text
        If Me.NUMINILbl.Text = "" And Me.NUMFINLBL.Text = "" And Me.Label8.Text = "" Then
            Me.NUMINILbl.Text = "0"
            Me.NUMFINLBL.Text = "0"
            Me.Label8.Text = "0"
            Me.ConfigureCrystalReports(Titulo, 0)
        Else
            Me.ConfigureCrystalReports(Titulo, 0)
        End If

    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged

    End Sub

    Private Sub RadioButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioButton1.Click
        op1 = 0
    End Sub

    Private Sub RadioButton4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton4.CheckedChanged

    End Sub

    Private Sub RadioButton4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioButton4.Click
        op1 = 1
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged

    End Sub

    Private Sub RadioButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioButton2.Click
        op1 = 2
    End Sub

    Private Sub CheckBox5_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox5.CheckedChanged
        If Me.CheckBox5.CheckState = CheckState.Checked Then
            FrmBuscaQuejas.Show()
            Op40 = "1"
        Else
            Op40 = "0"
            Me.Label8.Text = ""
        End If
    End Sub

    Private Sub Label8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label8.Click

    End Sub

    Private Sub Panel2_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel2.Paint

    End Sub

    Private Sub Label10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub NOMTRABAJO_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NOMTRABAJO.Click

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Titulo = Me.ComboBox4.Text
        If Me.NUMINILbl.Text = "" And Me.NUMFINLBL.Text = "" And Me.Label8.Text = "" Then
            Me.NUMINILbl.Text = "0"
            Me.NUMFINLBL.Text = "0"
            Me.Label8.Text = "0"
            Me.ConfigureCrystalReports(Titulo, 1)
        Else
            Me.ConfigureCrystalReports(Titulo, 1)
        End If

    End Sub

    Private Sub CheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox3.CheckedChanged
        If Me.CheckBox3.Checked = True Then
            Op50 = 1
            Me.NOMBREComboBox.Enabled = True
        Else
            Op50 = 0
            Me.NOMBREComboBox.Enabled = False
        End If
    End Sub

   
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        eBndAtenTelGraf = True
        FrmRepAtenTel.Show()
    End Sub

    
    Private Sub CheckBox6_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox6.CheckedChanged
        If CheckBox6.Checked = True Then
            Op60 = "1"
            Me.NombreComboBox1.Enabled = True

        Else
            Op60 = "0"
            Me.NombreComboBox1.Enabled = False
        End If
    End Sub

    
    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        If IsNumeric(Me.ComboBox2.SelectedValue) = True Then
            GloClv_TipSer = Me.ComboBox2.SelectedValue
            Titulo = Me.ComboBox2.Text
            LIMPIA()
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.MUESTRATRABAJOSQUEJASTableAdapter.Connection = CON
            Me.MUESTRATRABAJOSQUEJASTableAdapter.Fill(Me.DataSetEric2.MUESTRATRABAJOSQUEJAS, GloClv_TipSer)
            CON.Close()
        End If
    End Sub

    Private Sub CheckBox7_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox7.CheckedChanged

        If Me.CheckBox7.Checked = True Then
            Me.TextBox1.Enabled = True
        Else
            Me.TextBox1.Enabled = False
        End If


    End Sub

    Private Sub chkbxTurno_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkbxTurno.CheckStateChanged
        If (chkbxTurno.CheckState = CheckState.Checked) Then
            cmbxTurno.Enabled = True
            idTurnoSeleccionado = cmbxTurno.SelectedValue
        Else
            cmbxTurno.Enabled = False
            idTurnoSeleccionado = 0
        End If
    End Sub

    Private Sub cmbxTurno_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbxTurno.SelectedValueChanged
        Try
            If cmbxTurno.SelectedValue.ToString <> Nothing Then
                idTurnoSeleccionado = cmbxTurno.SelectedValue
            End If
        Catch ex As Exception
        End Try
    End Sub
End Class