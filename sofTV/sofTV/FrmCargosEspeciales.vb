Imports System.Data.SqlClient
Imports System.Text
Public Class FrmCargosEspeciales
    Private clv_cobro As Integer = 0
    Private servicio As String = Nothing
    Private cargo As String = Nothing
    Private eRes As Integer = 0
    Private eMsg As String = String.Empty
    Private eFecha As Date

    Private Sub damedatosbitracora()
        Try
            servicio = Me.TextBoxClvServicio.Text
            cargo = Me.TextBox1.Text
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub guardabitacora()
        Try
            'servicio = Me.ComboBox2.Text
            bitsist(GloUsuario, CLng(Me.ContratoTextBox.Text), LocGloSistema, Me.Name, "Servicio cobrado", servicio, Me.TextBoxClvServicio.Text, LocClv_Ciudad)
            'cargo = Me.TextBox1.Text
            bitsist(GloUsuario, CLng(Me.ContratoTextBox.Text), LocGloSistema, Me.Name, "Monto", cargo, Me.TextBox1.Text, LocClv_Ciudad)
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FrmCargosEspeciales_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'If locpagosespeciales = True Then
        '    locpagosespeciales = False

        '    If eBndCargoEsp = True Then
        '        Me.Text = "Cargos Especiales"
        '        Me.GroupBox1.Text = "Seleccione un Servicio para el Cargo Especial"
        '        Me.Label3.Text = "Cargo($):"
        '        'MuestraServiciosCargosEspeciales(GloClv_TipSer)
        '        Me.ComboBoxServicio.Visible = True
        '    ElseIf eBndBonifEsp = True Then
        '        Me.Text = "Bonificaciones Especiales"
        '        Me.GroupBox1.Text = "Seleccione un Servicio para la Bonificaci�n Especial"
        '        Me.Label3.Text = "Bonificaci�n($):"
        '        Me.TextBoxDescripcion.Visible = True
        '    End If
        '    Me.TextBox1.Text = ""
        '    If LocOpCargosEsp = "N" Then
        '        If eBndCargoEsp = True Then
        '            'Muestra_servicios_cargos(GloClv_TipSer, 0, 0)
        '            Me.ComboBoxServicio.Visible = True
        '        Else
        '            'MuestraServiciosCargosEspeciales(GloClv_TipSer)
        '            Me.TextBoxDescripcion.Visible = True
        '        End If
        '    End If
        'End If

        If Locbndcontrato3 = True Then
            Locbndcontrato3 = False
            Me.ContratoTextBox.Text = GLOCONTRATOSEL
            BUSCACLIENTES(0)
        End If

    End Sub

    Private Sub FrmCargosEspeciales_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Dim con As New SqlConnection(MiConexion)
        If IdSistema = "LO" Or IdSistema = "YU" Then
            Me.ESHOTELCheckBox.Visible = False
            Me.ESHOTELLabel1.Visible = False
        End If
        If eBndCargoEsp = True Then
            Me.Text = "Cargos Especiales"
            Me.GroupBox1.Text = "Servicio para Cargo Especial"
            Me.Label3.Text = "Cargo($):"
        Else
            Me.Text = "Bonificaciones Especiales"
            Me.GroupBox1.Text = "Servicio para Bonificaci�n Especial"
            Me.Label3.Text = "Bonificaci�n($):"
        End If
        Me.DateTimePicker1.Value = Today
        
        Select Case LocOpCargosEsp
            Case "N"
                DameFechaDelServidor()
            Case "C"
                Me.DateTimePicker1.Enabled = False
                Me.ContratoTextBox.Enabled = False
                Me.Button1.Enabled = False
                Me.TextBoxDescripcion.Enabled = False
                Me.TextBox1.Enabled = False
                Me.Inserta_Rel_cobroespBindingNavigatorSaveItem.Enabled = False
                Me.DateTimePicker1.Text = LocFechaesp
                Me.MOTIVOTextBox.Enabled = False

                con.Open()
                Me.Consulta_cobroespTableAdapter.Connection = con
                Me.Consulta_cobroespTableAdapter.Fill(Me.Procedimientosarnoldo4.consulta_cobroesp, Locclv_cobro)
                con.Close()
                If LocOpCargosEsp = "C" Or LocOpCargosEsp = "M" Then
                    'GloClv_TipSer = CLng(Me.Clv_tipserTextBox.Text)
                    Me.TextBox1.Text = Me.MontoTextBox.Text
                End If
                BUSCACLIENTES(0)

                'If eBndCargoEsp = True Then
                '    'Muestra_servicios_cargos(GloClv_TipSer, Locclv_cobro, 1)
                '    Me.ComboBoxServicio.Visible = True
                'Else
                '    'MuestraServiciosCargosEspeciales(GloClv_TipSer)
                '    Me.TextBoxDescripcion.Visible = True
                'End If
                Me.GroupBox1.Enabled = False
                Consulta_CobroEspFecha(Locclv_cobro)

            Case "M"
                Me.ContratoTextBox.Enabled = False
                Me.Button1.Enabled = False
                con.Open()
                Me.Consulta_cobroespTableAdapter.Connection = con
                Me.Consulta_cobroespTableAdapter.Fill(Me.Procedimientosarnoldo4.consulta_cobroesp, Locclv_cobro)
                con.Close()
                If LocOpCargosEsp = "C" Or LocOpCargosEsp = "M" Then
                    'GloClv_TipSer = CLng(Me.Clv_tipserTextBox.Text)
                    Me.TextBox1.Text = Me.MontoTextBox.Text
                End If
                BUSCACLIENTES(0)
                damedatosbitracora()
                Consulta_CobroEspFecha(Locclv_cobro)
                DameFechaMin()
                Me.DateTimePicker1.MinDate = eFecha
                'If eBndCargoEsp = True Then
                '    'Muestra_servicios_cargos(GloClv_TipSer, Locclv_cobro, 2)
                '    Me.ComboBoxServicio.Visible = True
                'Else
                '    'MuestraServiciosCargosEspeciales(GloClv_TipSer)
                '    Me.TextBoxDescripcion.Visible = True
                'End If

        End Select


    End Sub

    'Private Sub BUSCACLIENTES(ByVal OP As Integer)
    '    Try
    '        Dim CON As New SqlConnection(MiConexion)

    '        If IsNumeric(Me.ContratoTextBox.Text) = True Then
    '            CON.Open()
    '            '        Me.BUSCLIPORCONTRATOTableAdapter1.Connection = CON
    '            '        Me.BUSCLIPORCONTRATOTableAdapter1.Fill(Me.NewSofTvDataSet.BUSCLIPORCONTRATO, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), New System.Nullable(Of Integer)(CType(0, Integer)), GloClv_TipSer)
    '            Me.BUSCLIPORCONTRATOTableAdapter1.Connection = CON
    '            Me.BUSCLIPORCONTRATOTableAdapter1.Fill(Me.DataSetEric2.BUSCLIPORCONTRATO, CLng(Me.ContratoTextBox.Text), "", "", "", "", 0, 0)
    '            CON.Close()
    '            CREAARBOL()
    '        End If
    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try

    'End Sub

    Private Sub BUSCACLIENTES(ByVal OP As Integer)
        Dim contrato1 As Long
        Dim CON As New SqlConnection(MiConexion)
        Dim com As SqlCommand = New SqlCommand("BUSCLIPORCONTRATO_2", CON)
        Dim tabla As DataTable = New DataTable
        Dim da As SqlDataAdapter = New SqlDataAdapter(com)
        If OP = 0 Then
            If IsNumeric(Me.ContratoTextBox.Text) = True Then
                contrato1 = Long.Parse(Me.ContratoTextBox.Text)
            Else
                MsgBox("El contrato introducido no es correcto, favor de verificarlo", MsgBoxStyle.Information)
                Return
            End If
        End If
        com.CommandType = CommandType.StoredProcedure
        com.Parameters.Add(New SqlParameter("@CONTRATO", contrato1))
        com.Parameters.Add(New SqlParameter("@NOMBRE", ""))
        com.Parameters.Add(New SqlParameter("@CALLE", ""))
        com.Parameters.Add(New SqlParameter("@NUMERO", ""))
        com.Parameters.Add(New SqlParameter("@CIUDAD", ""))
        com.Parameters.Add(New SqlParameter("@OP", OP))
        com.Parameters.Add(New SqlParameter("@Clv_TipSer", GloClv_TipSer))
        com.Parameters.Add(New SqlParameter("@Telefono", ""))
        Try
            CON.Open()
            da.Fill(tabla)
            Me.NOMBRELabel1.Text = tabla.Rows(0)(1).ToString
            Me.CALLELabel1.Text = tabla.Rows(0)(2).ToString
            Me.COLONIALabel1.Text = tabla.Rows(0)(3).ToString
            Me.NUMEROLabel1.Text = tabla.Rows(0)(4).ToString
            Me.CIUDADLabel1.Text = tabla.Rows(0)(5).ToString
            Me.SOLOINTERNETCheckBox.Checked = Convert.ToBoolean(tabla.Rows(0)(6).ToString)
            Me.ESHOTELCheckBox.Checked = Convert.ToBoolean(tabla.Rows(0)(6).ToString)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            CON.Close()
        End Try
        CREAARBOL()
    End Sub

    Private Sub CREAARBOL()

        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim I As Integer = 0
            Dim X As Integer = 0


            If IsNumeric(Me.ContratoTextBox.Text) = True Then
                'Me.DameSerDELCliTableAdapter.Connection = CON
                'Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)))
                Me.DameSerDELCliTableAdapter1.Connection = CON
                Me.DameSerDELCliTableAdapter1.Fill(Me.DataSetEric2.dameSerDELCli, CLng(Me.ContratoTextBox.Text))
            Else
                'Me.DameSerDELCliTableAdapter.Connection = CON
                'Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(0, Long)))
                Me.DameSerDELCliTableAdapter1.Connection = CON
                Me.DameSerDELCliTableAdapter1.Fill(Me.DataSetEric2.dameSerDELCli, 0)
            End If



            Dim FilaRow As DataRow

            Me.TreeView1.Nodes.Clear()
            'For Each FilaRow In Me.NewSofTvDataSet.dameSerDELCli.Rows
            For Each FilaRow In Me.DataSetEric2.dameSerDELCli.Rows
                X = 0
                Me.TreeView1.Nodes.Add(Trim(FilaRow("Servicio").ToString()))
                I += 1
            Next

            CON.Close()

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try


    End Sub

    Private Sub ContratoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContratoTextBox.TextChanged
        BUSCACLIENTES(0)

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        GLOCONTRATOSEL = 0
        Locformulario = 3
        If IdSistema = "VA" Or IdSistema = "LO" Then
            FrmSelCliente2.Show()
        Else
            FrmSelCliente.Show()
        End If

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        GLOCONTRATOSEL = 0
        GloClv_TipSer = 0
        Me.Close()
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox1, Asc(LCase(e.KeyChar)), "L")))
    End Sub

    Private Sub Inserta_Rel_cobroespBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Inserta_Rel_cobroespBindingNavigatorSaveItem.Click
        Dim Monto As Single
        Dim Clv_Servicio As Integer
        Dim Con As New SqlConnection(MiConexion)
        Dim comando As SqlClient.SqlCommand
        Dim error1 As Integer = 0

        If IsNumeric(Me.ContratoTextBox.Text) = False Then
            MsgBox("Seleccione un Cliente.", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.MOTIVOTextBox.Text.Length = 0 Then
            MsgBox("Teclee el Motivo.", MsgBoxStyle.Information)
            Exit Sub
        End If

        If IsNumeric(Me.TextBox1.Text) = False Then
            MsgBox("Teclee un Monto.", MsgBoxStyle.Information)
            Exit Sub

        End If

        
        Clv_Servicio = 0

        'SI SE TRATA DE UNA BONIFICACION SE MULTIPLICAR� EL VALOR POR -1
        'LOS CARGOS SON POSITIVOS, POR ESO NO SE VER� AFECADO
        Monto = Me.TextBox1.Text
        If eBndBonifEsp = True Then
            If Monto > 0 Then Monto = Monto * -1
        End If

        Me.Validate()
        Me.ConsultacobroespBindingSource.EndEdit()
        If LocOpCargosEsp = "M" Then


            comando = New SqlClient.SqlCommand
            Con.Open()
            With comando
                .Connection = Con
                .CommandText = "modifica_cobroesp"
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
                ' Create a SqlParameter for each parameter in the stored procedure.
                Dim prm As New SqlParameter("@clv_cobro", SqlDbType.BigInt)
                Dim prm1 As New SqlParameter("@contrato", SqlDbType.BigInt)
                Dim prm2 As New SqlParameter("@Monto", SqlDbType.Decimal)
                Dim prm3 As New SqlParameter("@clv_servicio", SqlDbType.BigInt)
                Dim prm4 As New SqlParameter("@error", SqlDbType.Int)
                Dim prm5 As New SqlParameter("@Motivo", SqlDbType.VarChar, 250)
                Dim prm6 As New SqlParameter("@Fecha", SqlDbType.DateTime)

                '@clv_cobro bigint,@contrato bigint,@monto decimal(18,2),@clv_servicio bigint,@error int output


                prm.Direction = ParameterDirection.Input
                prm1.Direction = ParameterDirection.Input
                prm2.Direction = ParameterDirection.Input
                prm3.Direction = ParameterDirection.Input
                prm4.Direction = ParameterDirection.Output
                prm5.Direction = ParameterDirection.Input
                prm6.Direction = ParameterDirection.Input

                prm.Value = CLng(Me.TextBox2.Text)
                prm1.Value = CLng(Me.ContratoTextBox.Text)
                prm2.Value = Monto
                prm3.Value = Clv_Servicio
                prm4.Value = 0
                prm5.Value = Me.MOTIVOTextBox.Text
                prm6.Value = Me.DateTimePicker1.Value
                .Parameters.Add(prm)
                .Parameters.Add(prm1)
                .Parameters.Add(prm2)
                .Parameters.Add(prm3)
                .Parameters.Add(prm4)
                .Parameters.Add(prm5)
                .Parameters.Add(prm6)
                Dim i As Integer = comando.ExecuteNonQuery()
                error1 = prm4.Value
            End With

            Con.Close()
            If error1 = 0 Then
                MsgBox("Se Guardo con Exito", MsgBoxStyle.Information)
                guardabitacora()
                Locbndactualiza = True
                Me.Close()
            ElseIf error1 = 1 Then
                MsgBox("El Cliente Ya Tiene Ese Cobro Por El D�a De Hoy", MsgBoxStyle.Information)
                Exit Sub
            End If
        End If

        If LocOpCargosEsp = "N" Then

            ValidaCargosEspeciales(CLng(Me.ContratoTextBox.Text), Monto)
            If eRes = 1 Then
                MsgBox(eMsg, MsgBoxStyle.Information)
                Exit Sub
            End If

            comando = New SqlClient.SqlCommand
            Con.Open()
            With comando
                .Connection = Con
                .CommandText = "Inserta_Rel_cobroesp"
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0

                Dim prm As New SqlParameter("@clv_servicio", SqlDbType.BigInt)
                Dim prm1 As New SqlParameter("@contrato", SqlDbType.BigInt)
                Dim prm2 As New SqlParameter("@Monto", SqlDbType.Decimal)
                Dim prm3 As New SqlParameter("@clv_cobro", SqlDbType.BigInt)
                Dim prm4 As New SqlParameter("@Motivo", SqlDbType.VarChar, 250)
                Dim prm5 As New SqlParameter("Fecha", SqlDbType.DateTime)


                prm.Direction = ParameterDirection.Input
                prm1.Direction = ParameterDirection.Input
                prm2.Direction = ParameterDirection.Input
                prm3.Direction = ParameterDirection.Output
                prm4.Direction = ParameterDirection.Input
                prm5.Direction = ParameterDirection.Input

                prm.Value = Clv_Servicio
                prm1.Value = CLng(Me.ContratoTextBox.Text)
                prm2.Value = Monto
                prm3.Value = 0
                prm4.Value = Me.MOTIVOTextBox.Text
                prm5.Value = Me.DateTimePicker1.Value
                .Parameters.Add(prm)
                .Parameters.Add(prm1)
                .Parameters.Add(prm2)
                .Parameters.Add(prm3)
                .Parameters.Add(prm4)
                .Parameters.Add(prm5)
                Dim i As Integer = comando.ExecuteNonQuery()
                clv_cobro = prm3.Value
            End With

            bitsist(GloUsuario, CLng(Me.ContratoTextBox.Text), LocGloSistema, Me.Name, "Se Agrego un Cargo Especial", "Servicio: " + Me.TextBoxDescripcion.Text, "Monto: " + Me.TextBox1.Text, LocClv_Ciudad)
            Con.Close()
            MsgBox("Se Guardo con Exito", MsgBoxStyle.Information)
            Locbndactualiza = True
            Me.Close()


        End If
    End Sub

    Private Sub MuestraServiciosCargosEspeciales(ByVal Clv_TipSer As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("MuestraServiciosCargosEspeciales", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_TipSer
        comando.Parameters.Add(parametro)
        Dim reader As SqlDataReader
        Try
            conexion.Open()
            reader = comando.ExecuteReader(CommandBehavior.CloseConnection)
            While reader.Read
                Me.TextBoxClvServicio.Text = reader(0)
                Me.TextBoxDescripcion.Text = reader(1)
            End While
            conexion.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Muestra_servicios_cargos(ByVal Clv_TipSer As Integer, ByVal Clv_Cobro As Long, ByVal Op As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder()
        strSQL.Append("EXEC Muestra_servicios_cargos ")
        strSQL.Append(CStr(Clv_TipSer) & ", ")
        strSQL.Append(CStr(Clv_Cobro) & ", ")
        strSQL.Append(CStr(Op))

        Try
            conexion.Open()

            Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
            Dim dataTable As New DataTable
            Dim bindingSource As New BindingSource

            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.ComboBoxServicio.DataSource = bindingSource.DataSource

            conexion.Close()
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub ValidaCargosEspeciales(ByVal Contrato As Long, ByVal Monto As Decimal)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("Valida_cargos_especiales", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Monto", SqlDbType.Float)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Monto
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Res", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Output
        parametro3.Value = 0
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Msg", SqlDbType.VarChar, 150)
        parametro4.Direction = ParameterDirection.Output
        parametro4.Value = String.Empty
        comando.Parameters.Add(parametro4)

        Try
            eRes = 0
            eMsg = 0
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = CInt(parametro3.Value)
            eMsg = parametro4.Value
            conexion.Close()
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub Consulta_CobroEspFecha(ByVal Clv_Cobro As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("Consulta_CobroEspFecha", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Cobro", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Cobro
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("FechaCobro", SqlDbType.DateTime)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("FechaAplicacion", SqlDbType.DateTime)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            Me.DateTimePicker1.Value = parametro2.Value
            If eRealizado = "Si" And parametro3.Value <> "1900/01/01" Then

                Me.TextBox3.Text = parametro3.Value
                Me.TextBox3.Visible = True
                Me.Label6.Visible = True

            End If

        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub DameFechaDelServidor()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("DAMEFECHADELSERVIDOR", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Fecha", SqlDbType.DateTime)
        parametro.Direction = ParameterDirection.Output
        parametro.Value = Today
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            Me.DateTimePicker1.Value = parametro.Value
            Me.DateTimePicker1.MinDate = parametro.Value
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub DameFechaMin()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("DAMEFECHADELSERVIDOR", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Fecha", SqlDbType.DateTime)
        parametro.Direction = ParameterDirection.Output
        parametro.Value = Today
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            eFecha = parametro.Value
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub
    
End Class