Imports System.Data.SqlClient
Public Class BrwCablemodems

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub


    Private Sub BUSCA(ByVal OP As Integer)
        Me.Clv_calleLabel2.Text = "0"
        Me.CMBNombreTextBox.Text = ""
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            If OP = 0 Then
                If IsNumeric(Me.TextBox1.Text) = True Then
                    Me.BUSCAAparatosTableAdapter.Connection = CON
                    Me.BUSCAAparatosTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAAparatos, New System.Nullable(Of Integer)(CType(Me.TextBox1.Text, Integer)), "", New System.Nullable(Of Integer)(CType(0, Integer)), Me.ComboBox1.SelectedValue)
                Else
                    MsgBox(mensaje3)
                End If
            ElseIf OP = 1 Then
                If Len(Trim(Me.TextBox2.Text)) > 0 Then
                    Me.BUSCAAparatosTableAdapter.Connection = CON
                    Me.BUSCAAparatosTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAAparatos, New System.Nullable(Of Integer)(CType(0, Integer)), Me.TextBox2.Text, New System.Nullable(Of Integer)(CType(1, Integer)), Me.ComboBox1.SelectedValue)
                Else
                    MsgBox(mensaje4)
                End If
            Else
                Me.BUSCAAparatosTableAdapter.Connection = CON
                Me.BUSCAAparatosTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAAparatos, New System.Nullable(Of Integer)(CType(0, Integer)), "", New System.Nullable(Of Integer)(CType(2, Integer)), Me.ComboBox1.SelectedValue)
            End If
            Me.TextBox1.Clear()
            Me.TextBox2.Clear()
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            BUSCA(0)
        End If
    End Sub


    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            BUSCA(1)
        End If
    End Sub


    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        BUSCA(0)
    End Sub


    Private Sub BrwCablemodems_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            BUSCA(2)
        End If
    End Sub

    Private Sub BrwCablemodems_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        'Me.DamePermisosFormTableAdapter.Fill(Me.NewSofTvDataSet.DamePermisosForm, GloTipoUsuario, Me.Name, 1, glolec, gloescr, gloctr)
        If gloescr = 1 Then
            Me.Button2.Enabled = False
            Me.Button4.Enabled = False
        End If
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.TiposAparatos' Puede moverla o quitarla seg�n sea necesario.
        Me.TiposAparatosTableAdapter.Connection = CON
        Me.TiposAparatosTableAdapter.Fill(Me.NewSofTvDataSet.TiposAparatos)
        BUSCA(2)
        CON.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opcion = "N"
        GloTipoAparato = Me.ComboBox1.SelectedValue
        FrmCablemodems.Show()
    End Sub

    Private Sub consultar()
        If GloGClv_Cablemodem > 0 Then
            GloTipoAparato = Me.ComboBox1.SelectedValue
            opcion = "C"
            GloGClv_Cablemodem = Me.Clv_calleLabel2.Text
            FrmCablemodems.Show()
        Else
            MsgBox(mensaje2)
        End If
    End Sub


    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.DataGridView1.RowCount > 0 Then
            consultar()
        Else
            MsgBox(mensaje2)
        End If
    End Sub

    Private Sub modificar()
        If GloGClv_Cablemodem > 0 Then
            GloTipoAparato = Me.ComboBox1.SelectedValue
            opcion = "M"
            GloGClv_Cablemodem = Me.Clv_calleLabel2.Text
            FrmCablemodems.Show()
        Else
            MsgBox("Seleccione algun Tipo de Servicio")
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If Me.DataGridView1.RowCount > 0 Then
            modificar()
        Else
            MsgBox(mensaje1)
        End If

    End Sub


    Private Sub Clv_calleLabel2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Clv_calleLabel2.TextChanged

        GloGClv_Cablemodem = Me.Clv_calleLabel2.Text
        

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        GloTipoAparato = Me.ComboBox1.SelectedValue
        BUSCA(2)
    End Sub
    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If Button3.Enabled = True Then
            consultar()
        ElseIf Button4.Enabled = True Then
            modificar()
        End If
    End Sub

    
    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub



    Private Sub CMBNombreTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBNombreTextBox.TextChanged
        Dim CON As New SqlConnection(MiConexion)
        If Len(Me.CMBNombreTextBox.Text) = 0 Then Me.CMBNombreTextBox.Text = ""
        CON.Open()
        Me.MuestraInfoAparatoTableAdapter.Connection = CON
        Me.MuestraInfoAparatoTableAdapter.Fill(Me.DataSetEric2.MuestraInfoAparato, Me.CMBNombreTextBox.Text)
        CON.Close()
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        BUSCA(1)
    End Sub

    Private Sub TextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox2.TextChanged

    End Sub
End Class