Imports System.Data.SqlClient
Public Class FrmMetasIngresos
    Private Bnd As Boolean = False

    Private Sub FrmMetasIngresos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            colorea(Me, Me.Name)
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.MuestraAniosTableAdapter.Connection = CON
            Me.MuestraAniosTableAdapter.Fill(Me.DataSetEric2.MuestraAnios)
            CON.Close()
            Consultar()
            Bnd = True


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub Consultar()
        Try
            Dim CON2 As New SqlConnection(MiConexion)
            CON2.Open()
            Me.ConMetasIngresosTableAdapter.Connection = CON2
            Me.ConMetasIngresosTableAdapter.Fill(Me.DataSetEric2.ConMetasIngresos, CInt(Me.AnioComboBox.SelectedValue))
            CON2.Close()


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


    Private Sub AnioComboBox_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles AnioComboBox.SelectedValueChanged
        If Bnd = True Then
            Consultar()
        End If
    End Sub

    Private Sub ConMetasIngresosBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConMetasIngresosBindingNavigatorSaveItem.Click
        Try

            Dim CON3 As New SqlConnection(MiConexion)
            CON3.Open()
            Me.ConMetasIngresosTableAdapter.Connection = CON3

            Me.ConMetasIngresosTableAdapter.Update(Me.DataSetEric2.ConMetasIngresos)
            CON3.Close()
            MsgBox(mensaje5)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    

    

    

  

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim R As Integer = 0
        Try
            R = MsgBox("Se Eliminar�. �Deseas Continuar?", MsgBoxStyle.YesNo, "Atenci�n")
            If R = 6 Then
                Dim CON4 As New SqlConnection(MiConexion)
                CON4.Open()
                Me.ConMetasIngresosTableAdapter.Connection = CON4
                Me.ConMetasIngresosTableAdapter.Delete(CInt(Me.Clv_TipSerTextBox.Text), CInt(Me.AnioTextBox.Text))
                CON4.Close()
                MsgBox(mensaje6)
                Consultar()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub


End Class