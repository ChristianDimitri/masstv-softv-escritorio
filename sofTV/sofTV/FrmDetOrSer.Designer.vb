<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDetOrSer
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CMBLabel1 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim ClaveLabel As System.Windows.Forms.Label
        Dim Clv_OrdenLabel1 As System.Windows.Forms.Label
        Dim Clv_TrabajoLabel As System.Windows.Forms.Label
        Dim ObsLabel1 As System.Windows.Forms.Label
        Dim SeRealizaLabel As System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.CMBLabel7 = New System.Windows.Forms.Label()
        Me.ComboBox4 = New System.Windows.Forms.ComboBox()
        Me.DimeQueservicioTieneclienteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Procedimientosarnoldo4 = New sofTV.Procedimientosarnoldo4()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.MUESTRATRABAJOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.ObsTextBox1 = New System.Windows.Forms.TextBox()
        Me.Clv_OrdenTextBox1 = New System.Windows.Forms.TextBox()
        Me.Clv_TrabajoTextBox = New System.Windows.Forms.TextBox()
        Me.SeRealizaCheckBox = New System.Windows.Forms.CheckBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ContadorTextBox = New System.Windows.Forms.TextBox()
        Me.Dimesihay_ConexBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraTipSerPrincipalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRATRABAJOSTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRATRABAJOSTableAdapter()
        Me.CONDetOrdSerBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONDetOrdSerTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONDetOrdSerTableAdapter()
        Me.Dimesihay_ConexTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.Dimesihay_ConexTableAdapter()
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.GuardaMotivoCanServBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GuardaMotivoCanServTableAdapter = New sofTV.DataSetEricTableAdapters.GuardaMotivoCanServTableAdapter()
        Me.MuestraTipSerPrincipalTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MuestraTipSerPrincipalTableAdapter()
        Me.Dime_Que_servicio_Tiene_clienteTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Dime_Que_servicio_Tiene_clienteTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        CMBLabel1 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        ClaveLabel = New System.Windows.Forms.Label()
        Clv_OrdenLabel1 = New System.Windows.Forms.Label()
        Clv_TrabajoLabel = New System.Windows.Forms.Label()
        ObsLabel1 = New System.Windows.Forms.Label()
        SeRealizaLabel = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.DimeQueservicioTieneclienteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRATRABAJOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dimesihay_ConexBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTipSerPrincipalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONDetOrdSerBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GuardaMotivoCanServBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBLabel1
        '
        CMBLabel1.AutoSize = True
        CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabel1.ForeColor = System.Drawing.Color.DarkOrange
        CMBLabel1.Location = New System.Drawing.Point(3, 68)
        CMBLabel1.Name = "CMBLabel1"
        CMBLabel1.Size = New System.Drawing.Size(196, 20)
        CMBLabel1.TabIndex = 35
        CMBLabel1.Text = "Seleccione el Trabajo  :"
        CMBLabel1.Visible = False
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(25, 94)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(64, 15)
        Label2.TabIndex = 36
        Label2.Text = "Trabajo :"
        '
        'ClaveLabel
        '
        ClaveLabel.AutoSize = True
        ClaveLabel.BackColor = System.Drawing.Color.WhiteSmoke
        ClaveLabel.ForeColor = System.Drawing.Color.WhiteSmoke
        ClaveLabel.Location = New System.Drawing.Point(158, 3)
        ClaveLabel.Name = "ClaveLabel"
        ClaveLabel.Size = New System.Drawing.Size(37, 13)
        ClaveLabel.TabIndex = 22
        ClaveLabel.Text = "Clave:"
        '
        'Clv_OrdenLabel1
        '
        Clv_OrdenLabel1.AutoSize = True
        Clv_OrdenLabel1.BackColor = System.Drawing.Color.WhiteSmoke
        Clv_OrdenLabel1.ForeColor = System.Drawing.Color.WhiteSmoke
        Clv_OrdenLabel1.Location = New System.Drawing.Point(171, 1)
        Clv_OrdenLabel1.Name = "Clv_OrdenLabel1"
        Clv_OrdenLabel1.Size = New System.Drawing.Size(57, 13)
        Clv_OrdenLabel1.TabIndex = 25
        Clv_OrdenLabel1.Text = "Clv Orden:"
        '
        'Clv_TrabajoLabel
        '
        Clv_TrabajoLabel.AutoSize = True
        Clv_TrabajoLabel.BackColor = System.Drawing.Color.WhiteSmoke
        Clv_TrabajoLabel.ForeColor = System.Drawing.Color.WhiteSmoke
        Clv_TrabajoLabel.Location = New System.Drawing.Point(101, 2)
        Clv_TrabajoLabel.Name = "Clv_TrabajoLabel"
        Clv_TrabajoLabel.Size = New System.Drawing.Size(64, 13)
        Clv_TrabajoLabel.TabIndex = 28
        Clv_TrabajoLabel.Text = "Clv Trabajo:"
        '
        'ObsLabel1
        '
        ObsLabel1.AutoSize = True
        ObsLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ObsLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        ObsLabel1.Location = New System.Drawing.Point(25, 132)
        ObsLabel1.Name = "ObsLabel1"
        ObsLabel1.Size = New System.Drawing.Size(109, 15)
        ObsLabel1.TabIndex = 30
        ObsLabel1.Text = "Observaciones :"
        '
        'SeRealizaLabel
        '
        SeRealizaLabel.AutoSize = True
        SeRealizaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SeRealizaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        SeRealizaLabel.Location = New System.Drawing.Point(137, 208)
        SeRealizaLabel.Name = "SeRealizaLabel"
        SeRealizaLabel.Size = New System.Drawing.Size(154, 15)
        SeRealizaLabel.TabIndex = 32
        SeRealizaLabel.Text = "Se Realiza el Trabajo :"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.CMBLabel7)
        Me.Panel1.Controls.Add(Me.ComboBox4)
        Me.Panel1.Controls.Add(Me.Button5)
        Me.Panel1.Controls.Add(CMBLabel1)
        Me.Panel1.Controls.Add(Me.ComboBox2)
        Me.Panel1.Controls.Add(Me.ObsTextBox1)
        Me.Panel1.Controls.Add(Label2)
        Me.Panel1.Controls.Add(ClaveLabel)
        Me.Panel1.Controls.Add(Clv_OrdenLabel1)
        Me.Panel1.Controls.Add(Me.Clv_OrdenTextBox1)
        Me.Panel1.Controls.Add(Clv_TrabajoLabel)
        Me.Panel1.Controls.Add(Me.Clv_TrabajoTextBox)
        Me.Panel1.Controls.Add(ObsLabel1)
        Me.Panel1.Controls.Add(SeRealizaLabel)
        Me.Panel1.Controls.Add(Me.SeRealizaCheckBox)
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(Me.ContadorTextBox)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(603, 283)
        Me.Panel1.TabIndex = 0
        '
        'CMBLabel7
        '
        Me.CMBLabel7.AutoSize = True
        Me.CMBLabel7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel7.Location = New System.Drawing.Point(11, 15)
        Me.CMBLabel7.Name = "CMBLabel7"
        Me.CMBLabel7.Size = New System.Drawing.Size(154, 18)
        Me.CMBLabel7.TabIndex = 41
        Me.CMBLabel7.Text = "Tipo de Servicios  :"
        '
        'ComboBox4
        '
        Me.ComboBox4.DataSource = Me.DimeQueservicioTieneclienteBindingSource
        Me.ComboBox4.DisplayMember = "Concepto"
        Me.ComboBox4.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ComboBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox4.ForeColor = System.Drawing.Color.Red
        Me.ComboBox4.FormattingEnabled = True
        Me.ComboBox4.Location = New System.Drawing.Point(95, 41)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(196, 24)
        Me.ComboBox4.TabIndex = 40
        Me.ComboBox4.ValueMember = "clv_tipser"
        '
        'DimeQueservicioTieneclienteBindingSource
        '
        Me.DimeQueservicioTieneclienteBindingSource.DataMember = "Dime_Que_servicio_Tiene_cliente"
        Me.DimeQueservicioTieneclienteBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Procedimientosarnoldo4
        '
        Me.Procedimientosarnoldo4.DataSetName = "Procedimientosarnoldo4"
        Me.Procedimientosarnoldo4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(441, 234)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 4
        Me.Button5.Text = "&CERRAR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'ComboBox2
        '
        Me.ComboBox2.DataSource = Me.MUESTRATRABAJOSBindingSource
        Me.ComboBox2.DisplayMember = "Descripcion"
        Me.ComboBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(95, 91)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(482, 23)
        Me.ComboBox2.TabIndex = 0
        Me.ComboBox2.ValueMember = "Clv_Trabajo"
        '
        'MUESTRATRABAJOSBindingSource
        '
        Me.MUESTRATRABAJOSBindingSource.DataMember = "MUESTRATRABAJOS"
        Me.MUESTRATRABAJOSBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ObsTextBox1
        '
        Me.ObsTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ObsTextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ObsTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ObsTextBox1.Location = New System.Drawing.Point(140, 132)
        Me.ObsTextBox1.Multiline = True
        Me.ObsTextBox1.Name = "ObsTextBox1"
        Me.ObsTextBox1.Size = New System.Drawing.Size(437, 60)
        Me.ObsTextBox1.TabIndex = 1
        '
        'Clv_OrdenTextBox1
        '
        Me.Clv_OrdenTextBox1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_OrdenTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_OrdenTextBox1.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_OrdenTextBox1.Location = New System.Drawing.Point(195, 2)
        Me.Clv_OrdenTextBox1.Name = "Clv_OrdenTextBox1"
        Me.Clv_OrdenTextBox1.Size = New System.Drawing.Size(44, 13)
        Me.Clv_OrdenTextBox1.TabIndex = 27
        Me.Clv_OrdenTextBox1.TabStop = False
        '
        'Clv_TrabajoTextBox
        '
        Me.Clv_TrabajoTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TrabajoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_TrabajoTextBox.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TrabajoTextBox.Location = New System.Drawing.Point(270, 123)
        Me.Clv_TrabajoTextBox.Name = "Clv_TrabajoTextBox"
        Me.Clv_TrabajoTextBox.Size = New System.Drawing.Size(41, 13)
        Me.Clv_TrabajoTextBox.TabIndex = 29
        Me.Clv_TrabajoTextBox.TabStop = False
        '
        'SeRealizaCheckBox
        '
        Me.SeRealizaCheckBox.Checked = True
        Me.SeRealizaCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me.SeRealizaCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.SeRealizaCheckBox.Location = New System.Drawing.Point(297, 204)
        Me.SeRealizaCheckBox.Name = "SeRealizaCheckBox"
        Me.SeRealizaCheckBox.Size = New System.Drawing.Size(27, 24)
        Me.SeRealizaCheckBox.TabIndex = 2
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(297, 234)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 33)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "&ACEPTAR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'ContadorTextBox
        '
        Me.ContadorTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Dimesihay_ConexBindingSource, "Contador", True))
        Me.ContadorTextBox.Location = New System.Drawing.Point(348, 247)
        Me.ContadorTextBox.Name = "ContadorTextBox"
        Me.ContadorTextBox.Size = New System.Drawing.Size(44, 20)
        Me.ContadorTextBox.TabIndex = 39
        Me.ContadorTextBox.TabStop = False
        '
        'Dimesihay_ConexBindingSource
        '
        Me.Dimesihay_ConexBindingSource.DataMember = "Dimesihay_Conex"
        Me.Dimesihay_ConexBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MuestraTipSerPrincipalBindingSource
        '
        Me.MuestraTipSerPrincipalBindingSource.DataMember = "MuestraTipSerPrincipal"
        Me.MuestraTipSerPrincipalBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRATRABAJOSTableAdapter
        '
        Me.MUESTRATRABAJOSTableAdapter.ClearBeforeFill = True
        '
        'CONDetOrdSerBindingSource
        '
        Me.CONDetOrdSerBindingSource.DataMember = "CONDetOrdSer"
        Me.CONDetOrdSerBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CONDetOrdSerTableAdapter
        '
        Me.CONDetOrdSerTableAdapter.ClearBeforeFill = True
        '
        'Dimesihay_ConexTableAdapter
        '
        Me.Dimesihay_ConexTableAdapter.ClearBeforeFill = True
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GuardaMotivoCanServBindingSource
        '
        Me.GuardaMotivoCanServBindingSource.DataMember = "GuardaMotivoCanServ"
        Me.GuardaMotivoCanServBindingSource.DataSource = Me.DataSetEric
        '
        'GuardaMotivoCanServTableAdapter
        '
        Me.GuardaMotivoCanServTableAdapter.ClearBeforeFill = True
        '
        'MuestraTipSerPrincipalTableAdapter
        '
        Me.MuestraTipSerPrincipalTableAdapter.ClearBeforeFill = True
        '
        'Dime_Que_servicio_Tiene_clienteTableAdapter
        '
        Me.Dime_Que_servicio_Tiene_clienteTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'FrmDetOrSer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(628, 309)
        Me.Controls.Add(Me.Panel1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmDetOrSer"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Seleccione el Servicio al Cliente "
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DimeQueservicioTieneclienteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRATRABAJOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dimesihay_ConexBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTipSerPrincipalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONDetOrdSerBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GuardaMotivoCanServBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents ObsTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Clv_OrdenTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Clv_TrabajoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SeRealizaCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents MUESTRATRABAJOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents MUESTRATRABAJOSTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRATRABAJOSTableAdapter
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents CONDetOrdSerBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONDetOrdSerTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONDetOrdSerTableAdapter
    Friend WithEvents Dimesihay_ConexBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dimesihay_ConexTableAdapter As sofTV.NewSofTvDataSetTableAdapters.Dimesihay_ConexTableAdapter
    Friend WithEvents ContadorTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents GuardaMotivoCanServBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GuardaMotivoCanServTableAdapter As sofTV.DataSetEricTableAdapters.GuardaMotivoCanServTableAdapter
    Friend WithEvents CMBLabel7 As System.Windows.Forms.Label
    Friend WithEvents ComboBox4 As System.Windows.Forms.ComboBox
    Friend WithEvents MuestraTipSerPrincipalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipSerPrincipalTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MuestraTipSerPrincipalTableAdapter
    Friend WithEvents DimeQueservicioTieneclienteBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Procedimientosarnoldo4 As sofTV.Procedimientosarnoldo4
    Friend WithEvents Dime_Que_servicio_Tiene_clienteTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Dime_Que_servicio_Tiene_clienteTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
