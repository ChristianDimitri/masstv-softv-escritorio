Imports System.Data.SqlClient
Imports System.Text
Public Class FrmTap
    Dim eRes As Integer = 0
    Dim eMsg As String = Nothing
    Dim Salidas As String = Nothing
    Dim Entradas As String = Nothing
    Dim sector As String = Nothing
    Dim colonia As String = Nothing
    Dim Calle As String = Nothing
    Dim casas As String = Nothing
    Dim negocios As String = Nothing
    Dim lotes As String = Nothing
    Dim servicios As String = Nothing
    Dim frentenumero As String = Nothing
    Dim bnd1 As Boolean = False
    Dim bnd2 As Boolean = False
    Dim BNDTAP As Integer = Nothing
    Private Sub DameDatosBitacora()
        Try
            If eOpcion = "M" Then
                sector = Me.Clv_TxtComboBox.Text
                Entradas = Me.IngenieriaTextBox.Text
                Salidas = Me.SalidasTextBox.Text
                colonia = Me.NombreComboBox1.Text
                Calle = Me.NOMBREComboBox.Text
                casas = Me.NoCasasTextBox.Text
                negocios = Me.NoNegociosTextBox.Text
                lotes = Me.NoLotesTextBox.Text
                servicios = Me.NoServiciosTextBox.Text
                frentenumero = Me.FrenteANumeroTextBox.Text
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub guardabitacora(ByVal op As Integer)
        Select Case op
            Case 0
                bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Capturo Los Datos Del Tap", "", "Se Creo La Clave Del Tap", LocClv_Ciudad)
            Case 1
                If bnd1 = True Then
                    bnd1 = False
                    If bnd2 = False Then
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Capturo Los Datos Del Tap", "", "Se Capturo Los Datos Del Nuevo Tap", LocClv_Ciudad)
                        bnd2 = True
                    End If
                ElseIf bnd1 = False Then
                    'sector = Me.Clv_TxtComboBox.Text
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "clavesector", sector, Me.Clv_TxtComboBox.Text, LocClv_Ciudad)
                    'Entradas = Me.IngenieriaTextBox.Text
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.IngenieriaTextBox.Name, Entradas, Me.IngenieriaTextBox.Text, LocClv_Ciudad)
                    'Salidas = Me.SalidasTextBox.Text
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.SalidasTextBox.Name, Salidas, Me.SalidasTextBox.Text, LocClv_Ciudad)
                    'colonia = Me.NombreComboBox1.Text
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Coloniacombobox", colonia, Me.NombreComboBox1.Text, LocClv_Ciudad)
                    'Calle = Me.NOMBREComboBox.Text
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Callecombobox", Calle, Me.NOMBREComboBox.Text, LocClv_Ciudad)
                    'casas = Me.NoCasasTextBox.Text
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.NoCasasTextBox.Name, casas, Me.NoCasasTextBox.Text, LocClv_Ciudad)
                    'negocios = Me.NoNegociosTextBox.Text
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.NoNegociosTextBox.Name, negocios, Me.NoNegociosTextBox.Text, LocClv_Ciudad)
                    'lotes = Me.NoLotesTextBox.Text
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.NoLotesTextBox.Name, lotes, Me.NoLotesTextBox.Text, LocClv_Ciudad)
                    'servicios = Me.NoServiciosTextBox.Text
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.NoServiciosTextBox.Name, servicios, Me.NoServiciosTextBox.Text, LocClv_Ciudad)
                    'frentenumero = Me.FrenteANumeroTextBox.Text
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.FrenteANumeroTextBox.Name, frentenumero, Me.FrenteANumeroTextBox.Text, LocClv_Ciudad)
                End If
        End Select


    End Sub


    Private Sub FrmTap_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        MuestraDescPoste(1)
        If Me.PosteComboBox.Text.Length > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            colorea(Me, Me.Name)
            If eOpcion = "N" Then
                CON.Open()
                Me.ConSectorTableAdapter.Connection = CON
                Me.ConSectorTableAdapter.Fill(Me.DataSetEric.ConSector, 0, "", "", 0)
                CON.Close()
                'Me.MuestraColoniaSecTableAdapter.Connection = CON
                'Me.MuestraColoniaSecTableAdapter.Fill(Me.DataSetEric.MuestraColoniaSec, 0, Me.Clv_TxtComboBox.SelectedValue, 1)
                'Me.MuestraCalleSecTableAdapter.Connection = CON
                'Me.MuestraCalleSecTableAdapter.Fill(Me.DataSetEric.MuestraCalleSec, Me.Clv_TxtComboBox.SelectedValue, CLng(Me.NombreComboBox1.SelectedValue), 0, 0)
                MuestraDescPoste(1)
                Me.GroupBox2.Enabled = False
            End If

            If eOpcion = "C" Then
                Me.ConsecutivoAux.Text = eConsec
                CON.Open()
                Me.ConTapTableAdapter.Connection = CON
                Me.ConTapTableAdapter.Fill(Me.DataSetEric.ConTap, "", eClv_Sector, CLng(Me.ConsecutivoAux.Text), "", 0, 0, "", "", 1)
                Me.ConSectorTableAdapter.Connection = CON
                Me.ConSectorTableAdapter.Fill(Me.DataSetEric.ConSector, eClv_Sector, "", "", 3)
                CON.Close()
                'Me.MuestraColoniaSecTableAdapter.Connection = CON
                'Me.MuestraColoniaSecTableAdapter.Fill(Me.DataSetEric.MuestraColoniaSec, Me.Clv_ColoniaTextBox.Text, eClv_Sector, 2)
                MuestraColoniaSec(Me.Clv_ColoniaTextBox.Text, eClv_Sector, 2)
                'Me.MuestraCalleSecTableAdapter.Connection = CON
                'Me.MuestraCalleSecTableAdapter.Fill(Me.DataSetEric.MuestraCalleSec, eClv_Sector, Me.Clv_ColoniaTextBox.Text, Me.Clv_CalleTextBox.Text, 1)
                MuestraCalleSec(eClv_Sector, Me.Clv_ColoniaTextBox.Text, Me.Clv_CalleTextBox.Text, 1)
                'MuestraDescPoste(1)
                MuestraDescPoste_2(eClv_Sector, CLng(Me.ConsecutivoAux.Text))

                Me.GroupBox1.Enabled = False
                Me.GroupBox2.Enabled = False
            End If

            If eOpcion = "M" Then
                Me.ConsecutivoAux.Text = eConsec
                CON.Open()
                Me.ConTapTableAdapter.Connection = CON
                Me.ConTapTableAdapter.Fill(Me.DataSetEric.ConTap, "", eClv_Sector, CLng(Me.ConsecutivoAux.Text), "", 0, 0, "", "", 1)
                Me.ConSectorTableAdapter.Connection = CON
                Me.ConSectorTableAdapter.Fill(Me.DataSetEric.ConSector, eClv_Sector, "", "", 3)
                CON.Close()
                'Me.MuestraColoniaSecTableAdapter.Connection = CON
                'Me.MuestraColoniaSecTableAdapter.Fill(Me.DataSetEric.MuestraColoniaSec, Me.Clv_ColoniaTextBox.Text, eClv_Sector, 2)
                MuestraColoniaSec(Me.Clv_ColoniaTextBox.Text, eClv_Sector, 1)
                'Me.MuestraCalleSecTableAdapter.Connection = CON
                'Me.MuestraCalleSecTableAdapter.Fill(Me.DataSetEric.MuestraCalleSec, eClv_Sector, Me.Clv_ColoniaTextBox.Text, Me.Clv_CalleTextBox.Text, 1)
                MuestraCalleSec(eClv_Sector, Me.Clv_ColoniaTextBox.Text, Me.Clv_CalleTextBox.Text, 1)
                'MuestraDescPoste(1)
                MuestraDescPoste_2(eClv_Sector, CLng(Me.ConsecutivoAux.Text))
                Me.Clv_TxtComboBox.Enabled = False
                DameDatosBitacora()
            End If
        Else
            MsgBox("Debe De Haber Por Lo Menos Un Poste Dado De Alta")
            Me.Close()
        End If



    End Sub


    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        Dim CON As New SqlConnection(MiConexion)
        Try
            ChecaTextBox()
            'MODIFICAR
            If eOpcion = "M" Then
                eRes = 0

                If Me.Clv_SectorTextBox1.Text.Length = 0 Then
                    MsgBox("No Hay Sectores Registrados.", , "Atenci�n")
                    Exit Sub
                End If

                If Me.NombreComboBox1.Items.Count = 0 Then
                    MsgBox("No Existen Colonias Ligadas a �ste Sector.", , "Atenci�n")
                    Exit Sub
                End If
                If Me.NOMBREComboBox.Items.Count = 0 Then
                    MsgBox("No Existen Calles Ligadas a �sta Colonia.", , "Atenci�n")
                    Exit Sub
                End If
            'If Me.Clv_ColoniaTextBox1.Text.Length = 0 Then
            '    MsgBox("No Existen Colonias Ligadas a �ste Sector.", , "Atenci�n")
            '    Exit Sub
            'End If
            'If Me.Clv_CalleTextBox1.Text.Length = 0 Then
            '    MsgBox("No Existen Calles Ligadas a �sta Colonia.", , "Atenci�n")
            '    Exit Sub
            'End If
            GuardaRelacionTapPoste(CLng(Me.Clv_TxtComboBox.SelectedValue), CLng(Me.ConsecutivoAux.Text), CLng(Me.PosteComboBox.SelectedValue))
            CON.Open()
            Me.ModTapTableAdapter.Connection = CON
            Me.ModTapTableAdapter.Fill(Me.DataSetEric.ModTap, CLng(Me.Clv_TxtComboBox.SelectedValue), CLng(Me.ConsecutivoAux.Text), CInt(Me.IngenieriaTextBox.Text), CInt(Me.SalidasTextBox.Text), CLng(Me.NombreComboBox1.SelectedValue), CLng(Me.NOMBREComboBox.SelectedValue), CInt(Me.NoCasasTextBox.Text), CInt(Me.NoNegociosTextBox.Text), CInt(Me.NoLotesTextBox.Text), CInt(Me.NoServiciosTextBox.Text), Me.FrenteANumeroTextBox.Text, eRes, eMsg)
            CON.Close()
            If eRes = 1 Then
                MsgBox(eMsg)
            Else
                MsgBox("Se Guard� con �xito.")
                MuestraDescPoste_2(CLng(Me.Clv_TxtComboBox.SelectedValue), CLng(Me.ConsecutivoAux.Text))
                CON.Open()
                Me.ConTapTableAdapter.Connection = CON
                Me.ConTapTableAdapter.Fill(Me.DataSetEric.ConTap, "", CLng(Me.Clv_TxtComboBox.SelectedValue), CLng(Me.ConsecutivoAux.Text), "", 0, 0, "", "", 1)
                CON.Close()
                guardabitacora(1)
                DameDatosBitacora()
            End If
            End If


            'NUEVO
            If eOpcion = "N" Then
                eRes = 0
                CON.Open()
                ChecaTaps()
                If BNDTAP = 1 Then
                    MsgBox("El Tap Ya Existe", MsgBoxStyle.Information)
                    Exit Sub
                End If
                GuardaRelacionTapPoste(CLng(Me.Clv_TxtComboBox.SelectedValue), 0, CLng(Me.PosteComboBox.SelectedValue))

                Me.NueTapTableAdapter.Connection = CON
                Me.NueTapTableAdapter.Fill(Me.DataSetEric.NueTap, CLng(Me.Clv_TxtComboBox.SelectedValue), CInt(Me.IngenieriaTextBox.Text), CInt(Me.SalidasTextBox.Text), 0, 0, CInt(Me.NoCasasTextBox.Text), CInt(Me.NoNegociosTextBox.Text), CInt(Me.NoLotesTextBox.Text), CInt(Me.NoServiciosTextBox.Text), Me.FrenteANumeroTextBox.Text, eRes, eMsg, eConsec)
                CON.Close()
                If eRes = 1 Then
                    MsgBox(eMsg)
                Else
                    MsgBox("Se Guard� con �xito.")
                    Me.ConsecutivoAux.Text = eConsec
                    CON.Open()
                    Me.ConTapTableAdapter.Connection = CON
                    Me.ConTapTableAdapter.Fill(Me.DataSetEric.ConTap, "", CLng(Me.Clv_TxtComboBox.SelectedValue), CLng(Me.ConsecutivoAux.Text), "", 0, 0, "", "", 1)
                    CON.Close()
                    'Me.MuestraColoniaSecTableAdapter.Connection = CON
                    'Me.MuestraColoniaSecTableAdapter.Fill(Me.DataSetEric.MuestraColoniaSec, 0, Me.Clv_TxtComboBox.SelectedValue, 1)
                    MuestraColoniaSec(0, Me.Clv_TxtComboBox.SelectedValue, 1)
                    MuestraCalleSec(Me.Clv_TxtComboBox.SelectedValue, CLng(Me.NombreComboBox1.SelectedValue), 0, 0)
                    eOpcion = "M"
                    bnd1 = True
                    guardabitacora(0)
                    Me.Clv_TxtComboBox.Enabled = False
                    Me.GroupBox2.Enabled = True
                End If
            End If



        Catch
            MsgBox("Se ha Producido un Error.", , "Atenci�n")
        End Try

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub IngenieriaTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles IngenieriaTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.IngenieriaTextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub SalidasTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles SalidasTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.SalidasTextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub NoCasasTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NoCasasTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.NoCasasTextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub NoNegociosTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NoNegociosTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.NoNegociosTextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub NoLotesTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NoLotesTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.NoLotesTextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub NoServiciosTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NoServiciosTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.NoServiciosTextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub Clv_TxtComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_TxtComboBox.SelectedIndexChanged
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'Me.MuestraColoniaSecTableAdapter.Connection = CON
        'Me.MuestraColoniaSecTableAdapter.Fill(Me.DataSetEric.MuestraColoniaSec, 0, Me.Clv_TxtComboBox.SelectedValue, 1)
        'Me.MuestraCalleSecTableAdapter.Connection = CON
        'Me.MuestraCalleSecTableAdapter.Fill(Me.DataSetEric.MuestraCalleSec, Me.Clv_TxtComboBox.SelectedValue, CLng(Me.NombreComboBox1.SelectedValue), 0, 0)
        MuestraColoniaSec(0, Me.Clv_TxtComboBox.SelectedValue, 1)
        MuestraCalleSec(Me.Clv_TxtComboBox.SelectedValue, CLng(Me.NombreComboBox1.SelectedValue), 0, 0)
        'CON.Close()
    End Sub

    Private Sub NombreComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NombreComboBox1.SelectedIndexChanged
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'Me.MuestraCalleSecTableAdapter.Connection = CON
        'Me.MuestraCalleSecTableAdapter.Fill(Me.DataSetEric.MuestraCalleSec, Me.Clv_TxtComboBox.SelectedValue, Me.NombreComboBox1.SelectedValue, 0, 0)
        MuestraCalleSec(Me.Clv_TxtComboBox.SelectedValue, Me.NombreComboBox1.SelectedValue, 0, 0)
        'CON.Close()
    End Sub

    Private Sub ChecaTextBox()
        If Me.IngenieriaTextBox.Text.Length = 0 Then
            Me.IngenieriaTextBox.Text = 0
        End If
        If Me.SalidasTextBox.Text.Length = 0 Then
            Me.SalidasTextBox.Text = 0
        End If
        If Me.NoCasasTextBox.Text.Length = 0 Then
            Me.NoCasasTextBox.Text = 0
        End If
        If Me.NoNegociosTextBox.Text.Length = 0 Then
            Me.NoNegociosTextBox.Text = 0
        End If
        If Me.NoLotesTextBox.Text.Length = 0 Then
            Me.NoLotesTextBox.Text = 0
        End If
        If Me.NoServiciosTextBox.Text.Length = 0 Then
            Me.NoServiciosTextBox.Text = 0
        End If
        If Me.FrenteANumeroTextBox.Text.Length = 0 Then
            Me.FrenteANumeroTextBox.Text = "---"
        End If

    End Sub
    Private Sub MuestraDescPoste(ByVal op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraDescPoste ")
        strSQL.Append(CStr(op))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.PosteComboBox.DataSource = bindingSource

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub
    Private Sub MuestraDescPoste_2(ByVal clv_sector As Integer, ByVal consecutivo As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraDescPoste_2 ")
        strSQL.Append(CStr(clv_sector) & ",")
        strSQL.Append(CStr(consecutivo))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.PosteComboBox.DataSource = bindingSource

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub
    Private Sub GuardaRelacionTapPoste(ByVal clave As Long, ByVal descripcion As Long, ByVal poste As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("GuardaRelacionTapPoste", con)

        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@Clv_sector", SqlDbType.BigInt)
        Dim par2 As New SqlParameter("@Consecutivo", SqlDbType.BigInt)
        Dim par3 As New SqlParameter("@poste", SqlDbType.BigInt)

        par1.Direction = ParameterDirection.Input
        par2.Direction = ParameterDirection.Input
        par3.Direction = ParameterDirection.Input

        par1.Value = clave
        par2.Value = descripcion
        par3.Value = poste


        com.Parameters.Add(par1)
        com.Parameters.Add(par2)
        com.Parameters.Add(par3)

        Try
            con.Open()
            com.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()
        End Try

    End Sub

    Private Sub MuestraColoniaSec(ByVal Clv_Colonia As Long, ByVal Clv_Sector As Long, ByVal Op As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder

        StrSQL.Append("EXEC MuestraColoniaSec ")
        StrSQL.Append(CStr(Clv_Colonia) & ", ")
        StrSQL.Append(CStr(Clv_Sector) & ", ")
        StrSQL.Append(CStr(Op))

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(StrSQL.ToString(), CON)
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.NombreComboBox1.DataSource = BS.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub MuestraCalleSec(ByVal Clv_Sector As Long, ByVal Clv_Colonia As Long, ByVal Clv_Calle As Long, ByVal Op As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder

        StrSQL.Append("EXEC MuestraCalleSec ")
        StrSQL.Append(CStr(Clv_Sector) & ", ")
        StrSQL.Append(CStr(Clv_Colonia) & ", ")
        StrSQL.Append(CStr(Clv_Calle) & ", ")
        StrSQL.Append(CStr(Op))

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(StrSQL.ToString(), CON)
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.NOMBREComboBox.DataSource = BS.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub ChecaTaps()
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("ChecaTaps", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@CLV_SECTOR", SqlDbType.Int)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = Me.Clv_TxtComboBox.SelectedValue
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@CLV_POSTE", SqlDbType.Int)
        PRM2.Direction = ParameterDirection.Input
        PRM2.Value = Me.PosteComboBox.SelectedValue
        CMD.Parameters.Add(PRM2)

        Dim PRM3 As New SqlParameter("@BNDTAP", SqlDbType.Int)
        PRM3.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM3)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            BNDTAP = PRM3.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub
End Class