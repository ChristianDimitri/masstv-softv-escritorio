Imports System.Data.SqlClient


Public Class FrmConsultaCobro2

    Dim PagoMens As Decimal
    Dim PagPen As Integer
    Dim EsEco As Integer

    Private Sub TelMuestraDetalleCargos(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("TelMuestraDetalleCargos", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Try
            Me.DataGridView2.Rows.Clear()
            conexion.Open()
            Dim reader As SqlDataReader = comando.ExecuteReader()
            While (reader.Read())
                Me.DataGridView2.Rows.Add(reader(0), reader(1))
            End While
            conexion.Close()
            TelMuestraDetalleCargosAPagar(Contrato)

        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub TelMuestraDetalleCargosAPagar(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("TelMuestraDetalleCargosAPagar", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

       

        Try

            conexion.Open()
            Dim reader As SqlDataReader = comando.ExecuteReader()
            While reader.Read()
                Me.LabelSubTotal.Text = Microsoft.VisualBasic.FormatCurrency(reader(0).ToString, 2)
                Me.LabelIva.Text = Microsoft.VisualBasic.FormatCurrency(reader(1).ToString, 2)
                Me.LabelTotal.Text = Microsoft.VisualBasic.FormatCurrency(reader(2).ToString, 2)
                Me.LblCredito_Apagar.Text = Microsoft.VisualBasic.FormatCurrency(reader(3).ToString, 2)
                Me.LblImporte_Total.Text = reader(4).ToString
                Me.LabelGRan_Total.Text = Microsoft.VisualBasic.FormatCurrency(reader(4).ToString, 2)
                'Me.Clv_Session.Text = reader(5).ToString
                Me.LabelSaldoAnterior.Text = Microsoft.VisualBasic.FormatCurrency(reader(6).ToString, 2)
            End While
            conexion.Close()
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

       

    End Sub
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Private Sub FrmConsultaCobro2_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Text = "Consultar Pago"
        TelMuestraDetalleCargos(eGloContrato)
    End Sub

    Private Sub CMBLabel29_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub


    
End Class