﻿Public Class FrmClientes1Mes

    Private Sub FrmClientes1Mes_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
    End Sub


    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If cbxDesconectados.Checked = False And cbxInstalados.Checked = False Then
            MsgBox("Seleccione al menos una de las opciones", MsgBoxStyle.Information)
            Exit Sub
        End If
        If cbxDesconectados.Checked = True Or cbxInstalados.Checked = True Then
            Me.DialogResult = Windows.Forms.DialogResult.OK
        End If
        Me.Close()
    End Sub
End Class