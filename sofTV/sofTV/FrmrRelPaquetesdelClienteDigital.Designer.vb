<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmrRelPaquetesdelClienteDigital
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TreeView2 = New System.Windows.Forms.TreeView()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.Clv_Unicanet1 = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Clv_Unicanet = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.MacCableModem1 = New System.Windows.Forms.TextBox()
        Me.Contratonet1 = New System.Windows.Forms.TextBox()
        Me.MacCableModem = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Contratonet = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.MUESTRACABLEMODEMSDELCLI_porOpcionDIGBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACABLEMODEMSDELCLI_porOpcionDIGTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACABLEMODEMSDELCLI_porOpcionDIGTableAdapter()
        Me.MUESTRACONTDIG_PorOpcionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACONTDIG_PorOpcionTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACONTDIG_PorOpcionTableAdapter()
        Me.MUESTRAIPAQD_porSOLDigBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRAIPAQD_porSOLDigTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRAIPAQD_porSOLDigTableAdapter()
        Me.BORDetOrdSer_INTELIGENTEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BORDetOrdSer_INTELIGENTETableAdapter = New sofTV.NewSofTvDataSetTableAdapters.BORDetOrdSer_INTELIGENTETableAdapter()
        Me.DameContratoNetDigBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameContratoNetDigTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.DameContratoNetDigTableAdapter()
        Me.NUEIPAQD_SOLDIGBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NUEIPAQD_SOLDIGTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.NUEIPAQD_SOLDIGTableAdapter()
        Me.BorIPAQD_SOLDIGBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorIPAQD_SOLDIGTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.borIPAQD_SOLDIGTableAdapter()
        Me.MUESTRACONTDIGBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACONTDIGTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACONTDIGTableAdapter()
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.BorraMotivoCanServBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorraMotivoCanServTableAdapter = New sofTV.DataSetEricTableAdapters.BorraMotivoCanServTableAdapter()
        Me.GuardaMotivoCanServBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GuardaMotivoCanServTableAdapter = New sofTV.DataSetEricTableAdapters.GuardaMotivoCanServTableAdapter()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACABLEMODEMSDELCLI_porOpcionDIGBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACONTDIG_PorOpcionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAIPAQD_porSOLDigBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BORDetOrdSer_INTELIGENTEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameContratoNetDigBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NUEIPAQD_SOLDIGBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorIPAQD_SOLDIGBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACONTDIGBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorraMotivoCanServBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GuardaMotivoCanServBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TreeView2
        '
        Me.TreeView2.BackColor = System.Drawing.Color.Gainsboro
        Me.TreeView2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TreeView2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView2.ForeColor = System.Drawing.Color.Navy
        Me.TreeView2.Location = New System.Drawing.Point(506, 28)
        Me.TreeView2.Name = "TreeView2"
        Me.TreeView2.Size = New System.Drawing.Size(303, 269)
        Me.TreeView2.TabIndex = 43
        Me.TreeView2.TabStop = False
        '
        'TreeView1
        '
        Me.TreeView1.BackColor = System.Drawing.Color.Gainsboro
        Me.TreeView1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TreeView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView1.ForeColor = System.Drawing.Color.Navy
        Me.TreeView1.Location = New System.Drawing.Point(12, 28)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(292, 269)
        Me.TreeView1.TabIndex = 42
        Me.TreeView1.TabStop = False
        '
        'Clv_Unicanet1
        '
        Me.Clv_Unicanet1.Location = New System.Drawing.Point(550, 239)
        Me.Clv_Unicanet1.Name = "Clv_Unicanet1"
        Me.Clv_Unicanet1.Size = New System.Drawing.Size(127, 20)
        Me.Clv_Unicanet1.TabIndex = 59
        Me.Clv_Unicanet1.TabStop = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(74, 118)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(68, 13)
        Me.Label5.TabIndex = 58
        Me.Label5.Text = "Clv Unicanet"
        '
        'Clv_Unicanet
        '
        Me.Clv_Unicanet.Location = New System.Drawing.Point(162, 115)
        Me.Clv_Unicanet.Name = "Clv_Unicanet"
        Me.Clv_Unicanet.Size = New System.Drawing.Size(85, 20)
        Me.Clv_Unicanet.TabIndex = 57
        Me.Clv_Unicanet.TabStop = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(673, 319)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 4
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label4.Location = New System.Drawing.Point(503, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(247, 16)
        Me.Label4.TabIndex = 55
        Me.Label4.Text = "Instalar estos Servicios de Internet"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label3.Location = New System.Drawing.Point(12, 9)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(309, 16)
        Me.Label3.TabIndex = 54
        Me.Label3.Text = "Servicios de internet Pendientes de Instalar"
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkRed
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.White
        Me.Button4.Location = New System.Drawing.Point(331, 143)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(143, 30)
        Me.Button4.TabIndex = 3
        Me.Button4.Text = "<< "
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkRed
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.Location = New System.Drawing.Point(331, 105)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(143, 30)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = " >>"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'MacCableModem1
        '
        Me.MacCableModem1.Location = New System.Drawing.Point(550, 192)
        Me.MacCableModem1.Name = "MacCableModem1"
        Me.MacCableModem1.Size = New System.Drawing.Size(127, 20)
        Me.MacCableModem1.TabIndex = 51
        Me.MacCableModem1.TabStop = False
        '
        'Contratonet1
        '
        Me.Contratonet1.Location = New System.Drawing.Point(550, 217)
        Me.Contratonet1.Name = "Contratonet1"
        Me.Contratonet1.Size = New System.Drawing.Size(127, 20)
        Me.Contratonet1.TabIndex = 50
        Me.Contratonet1.TabStop = False
        '
        'MacCableModem
        '
        Me.MacCableModem.Location = New System.Drawing.Point(147, 190)
        Me.MacCableModem.Name = "MacCableModem"
        Me.MacCableModem.Size = New System.Drawing.Size(109, 20)
        Me.MacCableModem.TabIndex = 49
        Me.MacCableModem.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(49, 193)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(92, 13)
        Me.Label2.TabIndex = 48
        Me.Label2.Text = "Mac Cablemodem"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(77, 222)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 13)
        Me.Label1.TabIndex = 47
        Me.Label1.Text = "ContratoNet"
        '
        'Contratonet
        '
        Me.Contratonet.Location = New System.Drawing.Point(147, 219)
        Me.Contratonet.Name = "Contratonet"
        Me.Contratonet.Size = New System.Drawing.Size(127, 20)
        Me.Contratonet.TabIndex = 46
        Me.Contratonet.TabStop = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkRed
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(331, 69)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(143, 30)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "< "
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkRed
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(331, 33)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(143, 30)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = " >"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MUESTRACABLEMODEMSDELCLI_porOpcionDIGBindingSource
        '
        Me.MUESTRACABLEMODEMSDELCLI_porOpcionDIGBindingSource.DataMember = "MUESTRACABLEMODEMSDELCLI_porOpcionDIG"
        Me.MUESTRACABLEMODEMSDELCLI_porOpcionDIGBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRACABLEMODEMSDELCLI_porOpcionDIGTableAdapter
        '
        Me.MUESTRACABLEMODEMSDELCLI_porOpcionDIGTableAdapter.ClearBeforeFill = True
        '
        'MUESTRACONTDIG_PorOpcionBindingSource
        '
        Me.MUESTRACONTDIG_PorOpcionBindingSource.DataMember = "MUESTRACONTDIG_PorOpcion"
        Me.MUESTRACONTDIG_PorOpcionBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRACONTDIG_PorOpcionTableAdapter
        '
        Me.MUESTRACONTDIG_PorOpcionTableAdapter.ClearBeforeFill = True
        '
        'MUESTRAIPAQD_porSOLDigBindingSource
        '
        Me.MUESTRAIPAQD_porSOLDigBindingSource.DataMember = "MUESTRAIPAQD_porSOLDig"
        Me.MUESTRAIPAQD_porSOLDigBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRAIPAQD_porSOLDigTableAdapter
        '
        Me.MUESTRAIPAQD_porSOLDigTableAdapter.ClearBeforeFill = True
        '
        'BORDetOrdSer_INTELIGENTEBindingSource
        '
        Me.BORDetOrdSer_INTELIGENTEBindingSource.DataMember = "BORDetOrdSer_INTELIGENTE"
        Me.BORDetOrdSer_INTELIGENTEBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'BORDetOrdSer_INTELIGENTETableAdapter
        '
        Me.BORDetOrdSer_INTELIGENTETableAdapter.ClearBeforeFill = True
        '
        'DameContratoNetDigBindingSource
        '
        Me.DameContratoNetDigBindingSource.DataMember = "DameContratoNetDig"
        Me.DameContratoNetDigBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'DameContratoNetDigTableAdapter
        '
        Me.DameContratoNetDigTableAdapter.ClearBeforeFill = True
        '
        'NUEIPAQD_SOLDIGBindingSource
        '
        Me.NUEIPAQD_SOLDIGBindingSource.DataMember = "NUEIPAQD_SOLDIG"
        Me.NUEIPAQD_SOLDIGBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NUEIPAQD_SOLDIGTableAdapter
        '
        Me.NUEIPAQD_SOLDIGTableAdapter.ClearBeforeFill = True
        '
        'BorIPAQD_SOLDIGBindingSource
        '
        Me.BorIPAQD_SOLDIGBindingSource.DataMember = "borIPAQD_SOLDIG"
        Me.BorIPAQD_SOLDIGBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'BorIPAQD_SOLDIGTableAdapter
        '
        Me.BorIPAQD_SOLDIGTableAdapter.ClearBeforeFill = True
        '
        'MUESTRACONTDIGBindingSource
        '
        Me.MUESTRACONTDIGBindingSource.DataMember = "MUESTRACONTDIG"
        Me.MUESTRACONTDIGBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRACONTDIGTableAdapter
        '
        Me.MUESTRACONTDIGTableAdapter.ClearBeforeFill = True
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BorraMotivoCanServBindingSource
        '
        Me.BorraMotivoCanServBindingSource.DataMember = "BorraMotivoCanServ"
        Me.BorraMotivoCanServBindingSource.DataSource = Me.DataSetEric
        '
        'BorraMotivoCanServTableAdapter
        '
        Me.BorraMotivoCanServTableAdapter.ClearBeforeFill = True
        '
        'GuardaMotivoCanServBindingSource
        '
        Me.GuardaMotivoCanServBindingSource.DataMember = "GuardaMotivoCanServ"
        Me.GuardaMotivoCanServBindingSource.DataSource = Me.DataSetEric
        '
        'GuardaMotivoCanServTableAdapter
        '
        Me.GuardaMotivoCanServTableAdapter.ClearBeforeFill = True
        '
        'FrmrRelPaquetesdelClienteDigital
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(829, 391)
        Me.Controls.Add(Me.TreeView2)
        Me.Controls.Add(Me.TreeView1)
        Me.Controls.Add(Me.Clv_Unicanet1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Clv_Unicanet)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.MacCableModem1)
        Me.Controls.Add(Me.Contratonet1)
        Me.Controls.Add(Me.MacCableModem)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Contratonet)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Name = "FrmrRelPaquetesdelClienteDigital"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Relación de Paquetes del Cliente Digital"
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACABLEMODEMSDELCLI_porOpcionDIGBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACONTDIG_PorOpcionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAIPAQD_porSOLDigBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BORDetOrdSer_INTELIGENTEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameContratoNetDigBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NUEIPAQD_SOLDIGBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorIPAQD_SOLDIGBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACONTDIGBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorraMotivoCanServBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GuardaMotivoCanServBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TreeView2 As System.Windows.Forms.TreeView
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents Clv_Unicanet1 As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Clv_Unicanet As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents MacCableModem1 As System.Windows.Forms.TextBox
    Friend WithEvents Contratonet1 As System.Windows.Forms.TextBox
    Friend WithEvents MacCableModem As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Contratonet As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents MUESTRACABLEMODEMSDELCLI_porOpcionDIGBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACABLEMODEMSDELCLI_porOpcionDIGTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACABLEMODEMSDELCLI_porOpcionDIGTableAdapter
    Friend WithEvents MUESTRACONTDIG_PorOpcionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACONTDIG_PorOpcionTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACONTDIG_PorOpcionTableAdapter
    Friend WithEvents MUESTRAIPAQD_porSOLDigBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAIPAQD_porSOLDigTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRAIPAQD_porSOLDigTableAdapter
    Friend WithEvents BORDetOrdSer_INTELIGENTEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BORDetOrdSer_INTELIGENTETableAdapter As sofTV.NewSofTvDataSetTableAdapters.BORDetOrdSer_INTELIGENTETableAdapter
    Friend WithEvents DameContratoNetDigBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameContratoNetDigTableAdapter As sofTV.NewSofTvDataSetTableAdapters.DameContratoNetDigTableAdapter
    Friend WithEvents NUEIPAQD_SOLDIGBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NUEIPAQD_SOLDIGTableAdapter As sofTV.NewSofTvDataSetTableAdapters.NUEIPAQD_SOLDIGTableAdapter
    Friend WithEvents BorIPAQD_SOLDIGBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorIPAQD_SOLDIGTableAdapter As sofTV.NewSofTvDataSetTableAdapters.borIPAQD_SOLDIGTableAdapter
    Friend WithEvents MUESTRACONTDIGBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACONTDIGTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACONTDIGTableAdapter
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents BorraMotivoCanServBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorraMotivoCanServTableAdapter As sofTV.DataSetEricTableAdapters.BorraMotivoCanServTableAdapter
    Friend WithEvents GuardaMotivoCanServBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GuardaMotivoCanServTableAdapter As sofTV.DataSetEricTableAdapters.GuardaMotivoCanServTableAdapter
End Class
