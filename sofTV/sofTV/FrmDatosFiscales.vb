Imports System.Data.SqlClient
Public Class FrmDatosFiscales
    'Variables bitacora
    Private Razon_social As String = Nothing
    Private RFC As String = Nothing
    Private CURP As String = Nothing
    Private Calle As String = Nothing
    Private Numero As String = Nothing
    Private entrecalles As String = Nothing
    Private Colonia As String = Nothing
    Private CodigoPostal As String = Nothing
    Private Estado As String = Nothing
    Private Ciudad As String = Nothing
    Private Telefono As String = Nothing
    Private Fax As String = Nothing
    Private Pais As String = Nothing



    Private Sub Llena_Compa�ias()
        Dim oIdAsociado As Long = 0
        Dim oIdCompania As Long = 0
        Try
            BaseII.limpiaParametros()
            CmbCompania.DataSource = BaseII.ConsultaDT("UspCompaniasMizar")
            CmbCompania.ValueMember = "id_compania"
            CmbCompania.DisplayMember = "razon_social"
            oIdAsociado = UspEsNuevoRel_Cliente_AsociadoCFD(Contrato)
            If oIdAsociado > 0 Then
                oIdCompania = Query_Asociado(" Select x2.id_compania from asociados x1 inner join companias x2 on x1.id_compania=x2.id_compania  Where id_asociado = " + CStr(oIdAsociado))
                CmbCompania.SelectedValue = oIdCompania
            End If
        Catch ex As Exception
            MsgBox("El campo ID Grupo es Numerico", MsgBoxStyle.Exclamation, "Error")

        End Try
    End Sub
    'Fin Variables bitacora 
    Private Sub Guarda_Rel_DatosFiscales_Pais(ByVal contrato As Long, ByVal Pais As String, ByVal oEmail As String)
        Dim CON01 As New SqlConnection(MiConexion)
        Dim SQL As New SqlCommand()
        Try
            CON01.Open()
            SQL = New SqlCommand()
            With SQL
                .CommandText = "Guarda_Rel_DatosFiscales_Pais"
                .Connection = CON01
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@contrato", SqlDbType.BigInt)
                prm.Value = contrato
                prm.Direction = ParameterDirection.Input
                .Parameters.Add(prm)

                prm = New SqlParameter("@Pais", SqlDbType.VarChar, 150)
                prm.Value = Pais
                prm.Direction = ParameterDirection.Input
                .Parameters.Add(prm)

                prm = New SqlParameter("@Email", SqlDbType.VarChar, 100)
                prm.Value = oEmail
                prm.Direction = ParameterDirection.Input
                .Parameters.Add(prm)

                Dim i As Integer = .ExecuteNonQuery()
            End With
            CON01.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    


    Private Sub Consulta_Pais_Fiscal(ByVal contrato As Long)
        Dim CON01 As New SqlConnection(MiConexion)
        Dim SQL As New SqlCommand()
        Try
            CON01.Open()
            SQL = New SqlCommand()
            With SQL
                .CommandText = "Consulta_Pais_Fiscal"
                .CommandTimeout = 0
                .Connection = CON01
                .CommandType = CommandType.StoredProcedure

                Dim PRM As New SqlParameter("@contrato", SqlDbType.BigInt)
                PRM.Direction = ParameterDirection.Input
                PRM.Value = contrato
                .Parameters.Add(PRM)

                Dim reader As SqlDataReader = .ExecuteReader()

                While (reader.Read)
                    TxtPais.Text = reader.GetValue(1)
                    EMail_TextBox.Text = reader.GetValue(2)
                End While
            End With
            CON01.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub




    Private Sub damedatosbitacora()
        Try
            If OpcionCli = "M" Then
                Razon_social = Me.RAZON_SOCIALTextBox.Text
                RFC = Me.RFCTextBox.Text
                CURP = Me.CURPTextBox.Text
                Calle = Me.CALLE_RSTextBox.Text
                Numero = Me.NUMERO_RSTextBox.Text
                entrecalles = Me.ENTRECALLESTextBox.Text
                Colonia = Me.COLONIA_RSTextBox.Text
                CodigoPostal = Me.CP_RSTextBox.Text
                Estado = Me.ESTADO_RSTextBox.Text
                Ciudad = Me.CIUDAD_RSTextBox.Text
                Telefono = Me.TELEFONO_RSTextBox.Text
                Fax = Me.FAX_RSTextBox.Text
                Pais = Me.TxtPais.Text
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub guardabitacora(ByVal contrato As Integer)
        Try
            If OpcionCli = "M" Then
                'Razon_social = Me.RAZON_SOCIALTextBox.Text
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.RAZON_SOCIALTextBox.Name, Razon_social, Me.RAZON_SOCIALTextBox.Text, LocClv_Ciudad)
                'RFC = Me.RFCTextBox.Text
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.RFCTextBox.Name, RFC, Me.RFCTextBox.Text, LocClv_Ciudad)
                'CURP = Me.CURPTextBox.Text
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.CURPTextBox.Name, CURP, Me.CURPTextBox.Text, LocClv_Ciudad)
                'Calle = Me.CALLE_RSTextBox.Text
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.CALLE_RSTextBox.Name, Calle, Me.CALLE_RSTextBox.Text, LocClv_Ciudad)
                'Numero = Me.NUMERO_RSTextBox.Text
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.NUMERO_RSTextBox.Name, Numero, Me.NUMERO_RSTextBox.Text, LocClv_Ciudad)
                'entrecalles = Me.ENTRECALLESTextBox.Text
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.ENTRECALLESTextBox.Name, entrecalles, Me.ENTRECALLESTextBox.Text, LocClv_Ciudad)
                'Colonia = Me.COLONIA_RSTextBox.Text
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.COLONIA_RSTextBox.Name, Colonia, Me.COLONIA_RSTextBox.Text, LocClv_Ciudad)
                'CodigoPostal = Me.CP_RSTextBox.Text
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.CP_RSTextBox.Name, CodigoPostal, Me.CP_RSTextBox.Text, LocClv_Ciudad)
                'Estado = Me.ESTADO_RSTextBox.Text
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.ESTADO_RSTextBox.Name, Estado, Me.ESTADO_RSTextBox.Text, LocClv_Ciudad)
                'Ciudad = Me.CIUDAD_RSTextBox.Text
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.CIUDAD_RSTextBox.Name, Ciudad, Me.CIUDAD_RSTextBox.Text, LocClv_Ciudad)
                'Pais = Me.txtPais
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, "Pais Fiscal", Pais, Me.TxtPais.Text, LocClv_Ciudad)
                'Telefono = Me.TELEFONO_RSTextBox.Text
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.TELEFONO_RSTextBox.Name, Telefono, Me.TELEFONO_RSTextBox.Text, LocClv_Ciudad)
                'Fax = Me.FAX_RSTextBox.Text
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.FAX_RSTextBox.Name, Fax, Me.FAX_RSTextBox.Text, LocClv_Ciudad)
            ElseIf OpcionCli = "N" Then
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, "Se Capturaron Los Datos Fiscales Del Cliente", "", "Se Capturaron Los Datos Fiscales Del Cliente", LocClv_Ciudad)
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CONDatosFiscalesBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.CONDatosFiscalesBindingSource.EndEdit()
        Me.CONDatosFiscalesTableAdapter.Connection = CON
        Me.CONDatosFiscalesTableAdapter.Update(Me.NewSofTvDataSet.CONDatosFiscales)
        CON.Close()
    End Sub


    Private Sub CONDatosFiscalesBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONDatosFiscalesBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        Dim oIdAsociado As Long = 0
        CON.Open()
        Try
            If Len(RAZON_SOCIALTextBox.Text) = 0 Then
                MsgBox("Favor de Agregar la Raz�n Social")
                Exit Sub
            ElseIf Len(RFCTextBox.Text) = 0 Then
                MsgBox("Favor de Agregar el RFC")
                Exit Sub
            ElseIf Len(CURPTextBox.Text) = 0 Then
                MsgBox("Favor de Agregar la CURP")
                Exit Sub
            ElseIf Len(CALLE_RSTextBox.Text) = 0 Then
                MsgBox("Favor de Agregar la Calle")
                Exit Sub
            ElseIf Len(NUMERO_RSTextBox.Text) = 0 Then
                MsgBox("Favor de Agregar el n�mero ")
                Exit Sub
            ElseIf Len(COLONIA_RSTextBox.Text) = 0 Then
                MsgBox("Favor de Agregar la Colonia ")
                Exit Sub
            ElseIf Len(CP_RSTextBox.Text) = 0 Then
                MsgBox("Favor de Agregar el Codigo Postal ")
                Exit Sub
            ElseIf Len(ESTADO_RSTextBox.Text) = 0 Then
                MsgBox("Favor de Agregar el Estado")
                Exit Sub
            ElseIf Len(CIUDAD_RSTextBox.Text) = 0 Then
                MsgBox("Favor de Agregar la Ciudad")
                Exit Sub
            ElseIf Len(TxtPais.Text) = 0 Then
                MsgBox("Favor de Agregar el Pa�s")
                Exit Sub
            End If
            oIdAsociado = UspEsNuevoRel_Cliente_AsociadoCFD(RFCTextBox.Text)
            If oIdAsociado = 0 Then
                oIdAsociado = Nuevo_Asociado("Select Max(id_asociado) from asociados ")
            End If
            '--oIdAsociado = Contrato
            If oIdAsociado > 0 Then Alta_Datosfiscales_NEW(CmbCompania.SelectedValue, Me.RAZON_SOCIALTextBox.Text, Me.RFCTextBox.Text, CALLE_RSTextBox.Text, CStr(oIdAsociado), CP_RSTextBox.Text, COLONIA_RSTextBox.Text, EMail_TextBox.Text, ENTRECALLESTextBox.Text, ESTADO_RSTextBox.Text, CStr(oIdAsociado), CIUDAD_RSTextBox.Text, CIUDAD_RSTextBox.Text, NUMERO_RSTextBox.Text, TxtPais.Text, TELEFONO_RSTextBox.Text, FAX_RSTextBox.Text, CURPTextBox.Text, Contrato)
            'Alta_Datosfiscales(CmbCompania.SelectedValue, Contrato, Me.RAZON_SOCIALTextBox.Text, Me.RFCTextBox.Text, CALLE_RSTextBox.Text, CStr(oIdAsociado), CP_RSTextBox.Text, COLONIA_RSTextBox.Text, EMail_TextBox.Text, ENTRECALLESTextBox.Text, ESTADO_RSTextBox.Text, CStr(oIdAsociado), CIUDAD_RSTextBox.Text, CIUDAD_RSTextBox.Text, NUMERO_RSTextBox.Text, TxtPais.Text, TELEFONO_RSTextBox.Text, FAX_RSTextBox.Text, CURPTextBox.Text)
            If oAlta_Datosfiscales = True Then
                Me.Validate()
                Me.CONDatosFiscalesBindingSource.EndEdit()
                Me.CONDatosFiscalesTableAdapter.Connection = CON
                Me.CONDatosFiscalesTableAdapter.Update(Me.NewSofTvDataSet.CONDatosFiscales)
                Guarda_Rel_DatosFiscales_Pais(Contrato, Me.TxtPais.Text, EMail_TextBox.Text)
                'Guarda_Rel_DatosFiscales_Pais(Contrato, Me.TxtPais.Text)
                MsgBox(mensaje5)
                guardabitacora(Contrato)
                Me.Close()
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub Busca()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.CONDatosFiscalesTableAdapter.Connection = CON
            Me.CONDatosFiscalesTableAdapter.Fill(Me.NewSofTvDataSet.CONDatosFiscales, New System.Nullable(Of Long)(CType(Contrato, Long)))
            Consulta_Pais_Fiscal(Contrato)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()

    End Sub

    Private Sub CP_RSTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CP_RSTextBox.TextChanged

    End Sub

    Private Sub CP_RSLabel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub FrmDatosFiscales_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Llena_Compa�ias()
        'Me.Label1.Visible = False
        'Me.EMail_TextBox.Visible = False
        'colorea(Me)
        If OpcionCli = "C" Then
            Me.CONDatosFiscalesBindingNavigator.Enabled = False
            Me.Panel1.Enabled = False
        ElseIf OpcionCli = "M" Then
            Me.CONDatosFiscalesBindingNavigator.Enabled = True
            Me.Panel1.Enabled = True
        End If

        Busca()
        If IsNumeric(Me.ContratoTextBox.Text) = False Then
            Me.CONDatosFiscalesBindingSource.AddNew()
            Me.ContratoTextBox.Text = Contrato
            Me.IVADESGLOSADOTextBox.Text = 1
        End If
        damedatosbitacora()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.CONDatosFiscalesBindingSource.CancelEdit()
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        MsgBox(mensaje6)
        Me.CONDatosFiscalesTableAdapter.Connection = CON
        Me.CONDatosFiscalesTableAdapter.Delete(Contrato)
        UspBorrarDatosFiscalesPais()
        Me.TxtPais.Text = ""
        CON.Close()
        Borrar_Datosfiscales(Contrato)
        Usp_ED_BorraRel_Cliente_AsociadoCFD(Contrato)
        Me.Close()

    End Sub

    Private Sub UspBorrarDatosFiscalesPais()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, Contrato)
            BaseII.Inserta("UspBorrarDatosFiscalesPais")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub RAZON_SOCIALTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles RAZON_SOCIALTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.RAZON_SOCIALTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub RAZON_SOCIALTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RAZON_SOCIALTextBox.TextChanged

    End Sub

    Private Sub CALLE_RSTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles CALLE_RSTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.CALLE_RSTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub CALLE_RSTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CALLE_RSTextBox.TextChanged

    End Sub

    Private Sub NUMERO_RSTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NUMERO_RSTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.NUMERO_RSTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub NUMERO_RSTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NUMERO_RSTextBox.TextChanged

    End Sub

    Private Sub ENTRECALLESTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles ENTRECALLESTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.ENTRECALLESTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub ENTRECALLESTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ENTRECALLESTextBox.TextChanged

    End Sub

    Private Sub COLONIA_RSTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles COLONIA_RSTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.COLONIA_RSTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub COLONIA_RSTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles COLONIA_RSTextBox.TextChanged

    End Sub

    Private Sub ESTADO_RSTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles ESTADO_RSTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.ESTADO_RSTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub ESTADO_RSTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ESTADO_RSTextBox.TextChanged

    End Sub

    Private Sub CIUDAD_RSTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles CIUDAD_RSTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.CIUDAD_RSTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub CIUDAD_RSTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CIUDAD_RSTextBox.TextChanged

    End Sub

    Private Sub TELEFONO_RSTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TELEFONO_RSTextBox.TextChanged

    End Sub

    Private Sub CURPTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CURPTextBox.TextChanged

    End Sub
End Class