Imports System.Data.SqlClient
Imports System.Text
Public Class FrmBuscaQuejas

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Try


            If IsNumeric(Me.DataGridView1.SelectedCells(0).Value.ToString) = True Then
                GLOCLV_oRDENbus = Me.DataGridView1.SelectedCells(0).Value.ToString
                Me.Close()
            Else
                MsgBox("Seleccione la Queja ", MsgBoxStyle.Information)
            End If
        Catch
            MsgBox("No hay quejas que seleccionar", MsgBoxStyle.Information)
        End Try
    End Sub



    Private Sub Busca(ByVal op As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim sTATUS As String = "P"
        Try
            If IsNumeric(GloClv_TipSer) = True Then
                If op = 0 Then 'contrato
                    If IsNumeric(Me.TextBox1.Text) = True Then
                        'Me.BusquedaQuejasDynamicTableAdapter.Connection = CON
                        'Me.BusquedaQuejasDynamicTableAdapter.Fill(Me.DataSetarnoldo.BusquedaQuejasDynamic, GloClv_TipSer, GloPendientes, GloEjecutadas, GloVisita, 0, Me.TextBox1.Text, "", "", "", 0)
                        BuscaSeparado(GloClv_TipSer, GloPendientes, GloEjecutadas, GloVisita, 0, Me.TextBox1.Text, "", "", "", "", "", "", "", 0)
                    Else
                        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                    End If
                ElseIf op = 1 Then
                    If Me.TextBox2.Text.Length > 0 Or Len(Trim(Me.APaternoTextBox.Text)) > 0 Or Len(Trim(Me.AMaternoTextBox.Text)) > 0 Then
                        'Me.BusquedaQuejasDynamicTableAdapter.Connection = CON
                        'Me.BusquedaQuejasDynamicTableAdapter.Fill(Me.DataSetarnoldo.BusquedaQuejasDynamic, GloClv_TipSer, GloPendientes, GloEjecutadas, GloVisita, 0, 0, Me.TextBox2.Text, "", "", 1)
                        BuscaSeparado(GloClv_TipSer, GloPendientes, GloEjecutadas, GloVisita, 0, 0, Me.TextBox2.Text, Me.APaternoTextBox.Text, Me.AMaternoTextBox.Text, "", "", "", "", 1)
                    Else
                        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                    End If
                ElseIf op = 2 Then 'Calle y numero
                    'Me.BusquedaQuejasDynamicTableAdapter.Connection = CON
                    'Me.BusquedaQuejasDynamicTableAdapter.Fill(Me.DataSetarnoldo.BusquedaQuejasDynamic, GloClv_TipSer, GloPendientes, GloEjecutadas, GloVisita, 0, 0, "", Me.BCALLE.Text, Me.BNUMERO.Text, 2)
                    BuscaSeparado(0, GloPendientes, GloEjecutadas, GloVisita, 0, 0, "", "", "", Me.BCALLE.Text, Me.BNUMERO.Text, "", "", 2)
                ElseIf op = 3 Then 'clv_Queja
                    If IsNumeric(Me.TextBox3.Text) = True Then
                        'Me.BusquedaQuejasDynamicTableAdapter.Connection = CON
                        'Me.BusquedaQuejasDynamicTableAdapter.Fill(Me.DataSetarnoldo.BusquedaQuejasDynamic, GloClv_TipSer, GloPendientes, GloEjecutadas, GloVisita, Me.TextBox3.Text, 0, "", "", "", 3)
                        BuscaSeparado(GloClv_TipSer, GloPendientes, GloEjecutadas, GloVisita, Me.TextBox3.Text, 0, "", "", "", "", "", "", "", 3)
                    Else
                        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                    End If
                ElseIf op = 5 Then 'clv_QUEJA
                    If Me.TxtSetUpBox.Text.Length > 0 Or Len(Trim(Me.TxtTarjeta.Text)) > 0 Then
                        'Me.BusquedaOrdSerDynamicTableAdapter.Connection = CON
                        'Me.BusquedaOrdSerDynamicTableAdapter.Fill(Me.NewsoftvDataSet1.BusquedaOrdSerDynamic, 0, GloPendientes, GloEjecutadas, GloVisita, Me.TextBox3.Text, 0, "", "", "", op)
                        BuscaSeparado(GloClv_TipSer, GloPendientes, GloEjecutadas, GloVisita, 0, 0, "", "", "", "", "", TxtSetUpBox.Text, TxtTarjeta.Text, 5)

                    Else
                        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                    End If
                Else

                    'Me.BusquedaQuejasDynamicTableAdapter.Connection = CON
                    'Me.BusquedaQuejasDynamicTableAdapter.Fill(Me.DataSetarnoldo.BusquedaQuejasDynamic, GloClv_TipSer, GloPendientes, GloEjecutadas, GloVisita, 0, 0, Me.TextBox1.Text, "", "", 4)
                BuscaSeparado(0, GloPendientes, GloEjecutadas, GloVisita, 0, 0, Me.TextBox1.Text, "", "", "", "", "", "", 4)
                End If
                Me.TextBox1.Clear()
                Me.TextBox2.Clear()
                Me.TextBox3.Clear()
                Me.BNUMERO.Clear()
                Me.BCALLE.Clear()
            Else
                MsgBox("Seleccione el Tipo de Servicio")
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Busca(0)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Busca(1)
        TextBox2.Text = ""
        APaternoTextBox.Text = ""
        AMaternoTextBox.Text = ""
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(0)
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
        End If
    End Sub

    Private Sub FrmBuscaQuejas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerPrincipal' Puede moverla o quitarla seg�n sea necesario.
        Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
        Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)
        CON.Close()
        Busca(4)

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub



    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(3)
        End If
    End Sub

    Private Sub TextBox3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox3.TextChanged

    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        Busca(3)
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub Clv_calleLabel2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Busca(2)
        BCALLE.Text = ""
        BNUMERO.Text = ""
    End Sub

    Private Sub BCALLE_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BCALLE.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
        End If
    End Sub

    Private Sub BCALLE_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BCALLE.TextChanged

    End Sub

    Private Sub BNUMERO_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BNUMERO.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
        End If
    End Sub

    Private Sub BNUMERO_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BNUMERO.TextChanged

    End Sub

    Private Sub DataGridView1_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellDoubleClick
        If IsNumeric(Me.DataGridView1.SelectedCells(0).Value.ToString) = True Then
            GLOCLV_oRDENbus = Me.DataGridView1.SelectedCells(0).Value.ToString
            Me.Close()
        Else
            MsgBox("Seleccione la Queja", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub SplitContainer1_Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles SplitContainer1.Panel1.Paint

    End Sub
    Private Sub BuscaSeparado(ByVal Clv_TipSer As Integer, ByVal StatusPen As Boolean, ByVal StatusEje As Boolean, ByVal StatusVis As Boolean, ByVal Clv_Orden As Long, ByVal Contrato As Long, ByVal NOMBRE As String, ByVal ApePaterno As String, _
                         ByVal ApeMaterno As String, ByVal CALLE As String, ByVal NUMERO As String, ByVal SetUpBox As String, ByVal Tarjeta As String, ByVal OP As Integer)


        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder

        StrSQL.Append("EXEC BusquedaQuejasDynamicPORSEPARADO ")
        StrSQL.Append(CStr(Clv_TipSer) & ",")
        StrSQL.Append(CStr(StatusPen) & ",")
        StrSQL.Append(CStr(StatusEje) & ",")
        StrSQL.Append(CStr(StatusVis) & ",")
        StrSQL.Append(CStr(Clv_Orden) & ",")
        StrSQL.Append(CStr(Contrato) & ",")
        StrSQL.Append("'" & NOMBRE & "',")
        StrSQL.Append("'" & ApePaterno & "',")
        StrSQL.Append("'" & ApeMaterno & "',")
        StrSQL.Append("'" & CALLE & "',")
        StrSQL.Append("'" & NUMERO & "',")
        StrSQL.Append("'" & SetUpBox & "',")
        StrSQL.Append("'" & Tarjeta & "',")
        StrSQL.Append(CStr(OP))


        Dim DA As New SqlDataAdapter(StrSQL.ToString, CON)
        Dim DT As New DataTable
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.DataGridView1.DataSource = BS.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try

    End Sub

    Private Sub Button14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button14.Click
        Busca(5)
        TxtSetUpBox.Text = ""
        TxtTarjeta.Text = ""
    End Sub
End Class



