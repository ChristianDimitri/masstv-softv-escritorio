﻿Public Class brwTipoInstalaciones

#Region "Variables"
    Dim tipoInstalacion As classTipoInstalacion
    Dim dtTipoInstalacion As DataTable
    Dim frmTipoInstalacion As frmTipoInstalaciones
    Dim opcConsulta As Integer
#End Region

#Region "Controles"
    Private Sub brwTipoInstalaciones_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        consultar(0, String.Empty, 1) 'PARA LLENAR EL GRID Y OBTENER TODOS LOS REGISTROS 
    End Sub

    Private Sub btnBusquedaDescripcion_Click(sender As System.Object, e As System.EventArgs) Handles btnBusquedaDescripcion.Click
        If Me.txtBusquedaClave.Text.Length = 0 And Me.txtBusquedaDescripcion.Text.Length = 0 Then 'VALIDAMOS QUE HAYA INFORMACIÓN PARA REALIZAR LA BÚSQUEDA
            MsgBox("Inserte una Clave o una Descripción para realizar la Búsqueda", MsgBoxStyle.Information)
            Exit Sub
        End If

        dimeOpcionConsulta() 'PARA OBTENER LA OPCIÓN DE BÚSQUEDA

        If Me.txtBusquedaClave.Text.Length = 0 Then
            consultar(0, Me.txtBusquedaDescripcion.Text, opcConsulta) 'PARA LLENAR EL GRID
        ElseIf Me.txtBusquedaDescripcion.Text.Length = 0 Then
            consultar(CInt(Me.txtBusquedaClave.Text), String.Empty, opcConsulta) 'PARA LLENAR EL GRID
        End If

        Me.txtBusquedaClave.Clear()
        Me.txtBusquedaDescripcion.Clear()
    End Sub

    Private Sub btnEliminar_Click(sender As System.Object, e As System.EventArgs) Handles btnEliminar.Click
        If IsNumeric(Me.dgvTipoInstalaciones.SelectedCells(0).Value) = True Then
            If eliminar(CInt(Me.dgvTipoInstalaciones.SelectedCells(0).Value)) = False Then
                MsgBox("¡No se puede eliminar el registro seleccionado debido a que está asignado a un Cliente!", MsgBoxStyle.Information)
                Exit Sub
            Else
                MsgBox("¡Registro eliminado satisfactoriamente!", MsgBoxStyle.Information)
            End If
        Else
            MsgBox("¡Seleccione al menos un registro de la Lista!", MsgBoxStyle.Information)
            Exit Sub
        End If
        consultar(0, String.Empty, 1) 'PARA LLENAR EL GRID Y OBTENER TODOS LOS REGISTROS 
    End Sub

    Private Sub btnNuevo_Click(sender As System.Object, e As System.EventArgs) Handles btnNuevo.Click
        frmTipoInstalacion = New frmTipoInstalaciones

        frmTipoInstalacion.locOpcionTipo = "Nuevo"
        frmTipoInstalacion.ShowDialog()
        consultar(0, String.Empty, 1) 'PARA LLENAR EL GRID Y OBTENER TODOS LOS REGISTROS 
    End Sub

    Private Sub btnConsultar_Click(sender As System.Object, e As System.EventArgs) Handles btnConsultar.Click
        If IsNumeric(Me.dgvTipoInstalaciones.SelectedCells(0).Value) = True Then
            frmTipoInstalacion = New frmTipoInstalaciones

            frmTipoInstalacion.locClaveTipo = CInt(Me.dgvTipoInstalaciones.SelectedCells(0).Value)
            frmTipoInstalacion.locDescripcionTipo = Me.dgvTipoInstalaciones.SelectedCells(1).Value
            frmTipoInstalacion.locActivoTipo = CBool(Me.dgvTipoInstalaciones.SelectedCells(2).Value)
            frmTipoInstalacion.locOpcionTipo = "Consultar"
            frmTipoInstalacion.ShowDialog()

            consultar(0, String.Empty, 1) 'PARA LLENAR EL GRID Y OBTENER TODOS LOS REGISTROS 
        Else
            MsgBox("¡Seleccione al menos un registro de la Lista!", MsgBoxStyle.Information)
            Exit Sub
        End If
    End Sub

    Private Sub btnModificar_Click(sender As System.Object, e As System.EventArgs) Handles btnModificar.Click
        If IsNumeric(Me.dgvTipoInstalaciones.SelectedCells(0).Value) = True Then
            frmTipoInstalacion = New frmTipoInstalaciones

            frmTipoInstalacion.locClaveTipo = CInt(Me.dgvTipoInstalaciones.SelectedCells(0).Value)
            frmTipoInstalacion.locDescripcionTipo = Me.dgvTipoInstalaciones.SelectedCells(1).Value
            frmTipoInstalacion.locActivoTipo = CBool(Me.dgvTipoInstalaciones.SelectedCells(2).Value)
            frmTipoInstalacion.locOpcionTipo = "Modificar"
            frmTipoInstalacion.ShowDialog()

            consultar(0, String.Empty, 1) 'PARA LLENAR EL GRID Y OBTENER TODOS LOS REGISTROS 
        Else
            MsgBox("¡Seleccione al menos un registro de la Lista!", MsgBoxStyle.Information)
            Exit Sub
        End If
    End Sub

    Private Sub btnSalir_Click(sender As System.Object, e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub rbBusquedaActivos_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rbBusquedaActivos.CheckedChanged
        If Me.rbBusquedaActivos.Checked = True Then
            dimeOpcionConsulta()
            consultar(0, String.Empty, opcConsulta) 'PARA LLENAR EL GRID Y OBTENER TODOS LOS REGISTROS ACTIVOS
        End If
    End Sub

    Private Sub rbBusquedaNoActivos_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rbBusquedaNoActivos.CheckedChanged
        If Me.rbBusquedaNoActivos.Checked = True Then
            dimeOpcionConsulta()
            consultar(0, String.Empty, opcConsulta) 'PARA LLENAR EL GRID Y OBTENER TODOS LOS REGISTROS NO ACTIVOS
        End If
    End Sub

    Private Sub rbBusquedaTodos_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rbBusquedaTodos.CheckedChanged
        If Me.rbBusquedaTodos.Checked = True Then
            dimeOpcionConsulta()
            consultar(0, String.Empty, opcConsulta) 'PARA LLENAR EL GRID Y OBTENER TODOS LOS REGISTROS
        End If
    End Sub
#End Region

#Region "Métodos"

    Private Sub consultar(ByVal prmClaveTipo As Integer, ByVal prmDescripcionTipo As String, ByVal prmOpActivo As Integer)
        tipoInstalacion = New classTipoInstalacion
        dtTipoInstalacion = New DataTable

        tipoInstalacion.idTipoInstalacion = prmClaveTipo
        tipoInstalacion.descripcionTipo = prmDescripcionTipo
        tipoInstalacion.opActivo = prmOpActivo
        dtTipoInstalacion = tipoInstalacion.uspConsultaTblTipoInstalacion()

        Me.dgvTipoInstalaciones.DataSource = dtTipoInstalacion
    End Sub

    Private Function eliminar(ByVal prmClaveTipo As Integer) As Boolean
        tipoInstalacion = New classTipoInstalacion

        tipoInstalacion.idTipoInstalacion = prmClaveTipo
        eliminar = tipoInstalacion.uspEliminaTblTipoInstalacion()
    End Function

    Private Sub dimeOpcionConsulta()
        If Me.rbBusquedaActivos.Checked = True Then
            opcConsulta = 3
        ElseIf Me.rbBusquedaNoActivos.Checked = True Then
            opcConsulta = 2
        ElseIf Me.rbBusquedaTodos.Checked = True Then
            opcConsulta = 1
        End If
    End Sub
#End Region

    Private Sub dgvTipoInstalaciones_CurrentCellChanged(sender As Object, e As System.EventArgs) Handles dgvTipoInstalaciones.CurrentCellChanged
        Try
            Me.txtDetalleClave.Text = CInt(Me.dgvTipoInstalaciones.SelectedCells(0).Value)
            Me.txtDetalleDescripcion.Text = Me.dgvTipoInstalaciones.SelectedCells(1).Value
            Me.cbxActivoDetalle.Checked = CBool(Me.dgvTipoInstalaciones.SelectedCells(2).Value)
        Catch ex As Exception

        End Try
    End Sub
End Class