﻿
Imports System.Data.SqlClient

Public Class FormIAPARATOS_SELECCION

    Private Sub Llena_AparatosporDisponibles(oOp As String, oTrabajo As String, oContrato As Long, oClv_Tecnico As Long, oClv_Orden As Long, oClave As Long)
        Try
            '@Op BIGINT=0,@TIPO_APARATO VARCHAR(5)='',@TRABAJO VARCHAR(10)='',@CONTRATO BIGINT=0,@CLV_TECNICO INT=0
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@Op", SqlDbType.VarChar, oOp, 5)
            BaseII.CreateMyParameter("@TRABAJO", SqlDbType.VarChar, oTrabajo, 10)
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, oContrato)
            BaseII.CreateMyParameter("@CLV_TECNICO", SqlDbType.Int, oClv_Tecnico)
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
            BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)
            ComboBoxPorAsignar.DataSource = BaseII.ConsultaDT("MUESTRAAPARATOS_DISCPONIBLES")
            ComboBoxPorAsignar.DisplayMember = "Descripcion"
            ComboBoxPorAsignar.ValueMember = "ContratoAnt"

            If ComboBoxPorAsignar.Items.Count > 0 Then
                ComboBoxPorAsignar.SelectedIndex = 0
            End If
            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub



    Private Sub Llena_AparatosporAsignar(oOp As String, oTrabajo As String, oContrato As Long)
        Try
            '@Op BIGINT=0,@TIPO_APARATO VARCHAR(5)='',@TRABAJO VARCHAR(10)='',@CONTRATO BIGINT=0,@CLV_TECNICO INT=0
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@Op", SqlDbType.VarChar, oOp, 5)
            BaseII.CreateMyParameter("@TRABAJO", SqlDbType.VarChar, oTrabajo, 10)
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, oContrato)
            BaseII.CreateMyParameter("@CLV_TECNICO", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, 0)
            BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, 0)
            ComboBoxPorAsignar.DataSource = BaseII.ConsultaDT("MUESTRAAPARATOS_DISCPONIBLES")
            ComboBoxPorAsignar.DisplayMember = "Descripcion"
            ComboBoxPorAsignar.ValueMember = "ContratoAnt"

            If ComboBoxPorAsignar.Items.Count > 0 Then
                ComboBoxPorAsignar.SelectedIndex = 0

            End If
            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub FormIAPARATOS_SELECCION_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If GLOTRABAJO = "ECABL" Or GLOTRABAJO = "ECONT"  Then
            Me.Text = GLONOMTRABAJO
            Label4.Text = "Seleccione el Modelo :"
            If opcion = "N" Then
                Llena_AparatosporAsignar(opcion, GLOTRABAJO, FrmOrdSer.ContratoTextBox.Text)
            ElseIf opcion = "M" Or opcion = "C" Then
                Llena_AparatosporDisponibles("M", GLOTRABAJO, FrmOrdSer.ContratoTextBox.Text, FrmOrdSer.Tecnico.SelectedValue, gloClv_Orden, GloDetClave)
            End If
        ElseIf GLOTRABAJO = "RANTE" _
                Or GLOTRABAJO = "RELNB" _
                Or GLOTRABAJO = "RCONT" _
                Or GLOTRABAJO = "RCAJA" _
                Or GLOTRABAJO = "RAPAR" _
                Or GLOTRABAJO = "RANRA" _
                Or GLOTRABAJO = "RROUT" _
                Or GLOTRABAJO = "RCABL" Then
            Me.Text = GLONOMTRABAJO
            Label4.Text = "Seleccione el Aparato a Retirar :"
            If opcion = "N" Then
                Llena_AparatosporAsignar(opcion, GLOTRABAJO, FrmOrdSer.ContratoTextBox.Text)
            ElseIf opcion = "M" Or opcion = "C" Then
                Llena_AparatosporDisponibles("M", GLOTRABAJO, FrmOrdSer.ContratoTextBox.Text, FrmOrdSer.Tecnico.SelectedValue, gloClv_Orden, GloDetClave)
            End If
        Else
            If opcion = "N" Then
                Llena_AparatosporAsignar(opcion, GLOTRABAJO, FrmOrdSer.ContratoTextBox.Text)
            ElseIf Bloquea = True Or opcion = "M" Or opcion = "C" Then
                BtnAceptar.Enabled = False
            End If
        End If
        If opcion = "M" Or opcion = "C" Then
            If FrmOrdSer.Panel6.Enabled = False Then
                ComboBoxPorAsignar.Enabled = False
                BtnAceptar.Enabled = False
            End If
        End If
    End Sub

    Private Sub SP_GuardaIAPARATOS(oClave As Long, oTrabajo As String, oClv_Orden As Long, oContratonet As Long, oClv_Aparato As Long, oOpcion As String)
        '@Clave bigint=0,@Trabajo varchar(10)='',@Clv_Orden bigint=0,@Contratonet Bigint=0,@Clv_Aparato bigint=0,@Opcion varchar(10)=''
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)
        BaseII.CreateMyParameter("@Trabajo", SqlDbType.VarChar, oTrabajo, 10)
        BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
        BaseII.CreateMyParameter("@Contratonet", SqlDbType.BigInt, oContratonet)
        BaseII.CreateMyParameter("@Clv_Aparato", SqlDbType.BigInt, oClv_Aparato)
        BaseII.CreateMyParameter("@Opcion", SqlDbType.VarChar, oOpcion, 10)
        BaseII.Inserta("SP_GuardaIAPARATOS")
    End Sub

    Private Sub BtnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles BtnAceptar.Click
        If ComboBoxPorAsignar.SelectedIndex = -1 Then
            MsgBox("Seleccione el Aparato")
            Exit Sub
        End If
        If GLOTRABAJO = "ECONT" Or GLOTRABAJO = "ECABL" Then
            If opcion = "N" Then
                SP_GuardaIAPARATOS(GloDetClave, GLOTRABAJO, gloClv_Orden, ComboBoxPorAsignar.SelectedValue, 0, opcion)
            ElseIf opcion = "M" Then
                SP_GuardaIAPARATOS(GloDetClave, GLOTRABAJO, gloClv_Orden, 0, ComboBoxPorAsignar.SelectedValue, opcion)
            End If
        ElseIf GLOTRABAJO = "RANTE" Or GLOTRABAJO = "RELNB" _
            Or GLOTRABAJO = "RCABL" _
            Or GLOTRABAJO = "RCAJA" _
            Or GLOTRABAJO = "RCONT" _
            Or GLOTRABAJO = "RAPAR" _
            Or GLOTRABAJO = "RANRA" _
            Or GLOTRABAJO = "RROUT" _
            Then
            If opcion = "N" Or opcion = "M" Then
                SP_GuardaIAPARATOS(GloDetClave, GLOTRABAJO, gloClv_Orden, 0, ComboBoxPorAsignar.SelectedValue, opcion)
            End If
        Else
            SP_GuardaIAPARATOS(GloDetClave, GLOTRABAJO, gloClv_Orden, ComboBoxPorAsignar.SelectedValue, 0, opcion)
        End If

        Me.Close()
    End Sub

    Private Sub Button5_Click(sender As System.Object, e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
End Class