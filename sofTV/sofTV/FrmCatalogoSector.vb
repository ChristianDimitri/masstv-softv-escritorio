Imports System.Data.SqlClient
Public Class FrmCatalogoSector

    Private Sub Busqueda(ByVal Clave As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.BUSCASectoresTableAdapter.Connection = CON
            Me.BUSCASectoresTableAdapter.Fill(Me.DataSetLidia.BUSCASectores, New System.Nullable(Of Integer)(CType(Clave, Integer)), New System.Nullable(Of Integer)(CType(0, Integer)), New System.Nullable(Of Integer)(CType(0, Integer)), New System.Nullable(Of Integer)(CType(0, Integer)), 0, New System.Nullable(Of Integer)(CType(0, Integer)))
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.CONCatalogoSectorTableAdapter.Connection = CON
        Me.CONCatalogoSectorTableAdapter.Delete(gloClave)
        GloBnd = True
        CON.Close()
        Me.Close()
    End Sub

    Private Sub CONCatalogoSectorBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONCatalogoSectorBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If opcion = "N" Then
            If Len(Trim(Me.SectorComboBox.Text)) = 0 Then
                MsgBox("Se Requiere el Sector ", MsgBoxStyle.Information)
                Exit Sub
            End If
            'If Len(Trim(Me.DescripTextBox.Text)) = 0 Then
            '    MsgBox("Se Requiere la Descripcion ", MsgBoxStyle.Information)
            '    Exit Sub
            'End If
            If IsNumeric(Me.CALLEComboBox.SelectedValue) = False Then
                MsgBox("Seleccione la Calle", MsgBoxStyle.Information)
                Exit Sub
            End If
            If IsNumeric(Me.COLONIAComboBox.SelectedValue) = False Then
                MsgBox("Seleccione la Colonia", MsgBoxStyle.Information)
                Exit Sub
            End If
            If IsNumeric(Me.CIUDADComboBox.SelectedValue) = False Then
                MsgBox("Seleccione la Ciudad", MsgBoxStyle.Information)
                Exit Sub
            End If
            If IsNumeric(Me.SectorComboBox.SelectedValue) = False Then
                MsgBox("Seleccione el Sector", MsgBoxStyle.Information)
                Exit Sub
            End If
            If IsNumeric(Me.SalidaTextBox.Text) = False Then
                MsgBox("Seleccione la Salida", MsgBoxStyle.Information)
                Exit Sub
            End If
        End If
        Me.Validate()
        Me.CONCatalogoSectorBindingSource.EndEdit()
        Me.CONCatalogoSectorTableAdapter.Connection = CON
        Me.CONCatalogoSectorTableAdapter.Update(Me.DataSetLidia.CONCatalogoSector)
        MsgBox("Se ha Guardado con Ex�to", MsgBoxStyle.Information)
        CON.Close()
        Me.Close()

    End Sub

    Private Sub Button5_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub FrmCatalogoSector_Load_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        Try
            'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.CALLES' Puede moverla o quitarla seg�n sea necesario.
            Me.MUESTRACALLESTableAdapter.Connection = CON
            Me.MUESTRACALLESTableAdapter.Fill(Me.DataSetLidia.MUESTRACALLES)
            Me.DAMECOLONIA_CALLETableAdapter.Connection = CON
            Me.DAMECOLONIA_CALLETableAdapter.Fill(Me.DataSetLidia.DAMECOLONIA_CALLE, New System.Nullable(Of Integer)(CType(Me.CALLEComboBox.SelectedValue, Integer)))
            Me.MuestraCVECOLCIUTableAdapter.Connection = CON
            Me.MuestraCVECOLCIUTableAdapter.Fill(Me.DataSetLidia.MuestraCVECOLCIU, New System.Nullable(Of Integer)(CType(Me.COLONIAComboBox.SelectedValue, Integer)))
            Me.CONCatalogoSectorTableAdapter.Connection = CON
            Me.CONCatalogoSectorTableAdapter.Fill(Me.DataSetLidia.CONCatalogoSector, gloClave)
            Me.MUESTRASECTORESTableAdapter.Connection = CON
            Me.MUESTRASECTORESTableAdapter.Fill(Me.DataSetLidia.MUESTRASECTORES, 0)

            If opcion = "N" Then
                Me.CONCatalogoSectorBindingSource.AddNew()
                Me.CONCatalogoSectorBindingNavigator.Enabled = True
                Me.COLONIAComboBox.Enabled = False
                Me.CIUDADComboBox.Enabled = False
                Me.Panel1.Enabled = True

            ElseIf opcion = "C" Then
                Me.CONCatalogoSectorBindingNavigator.Enabled = False
                Me.Panel1.Enabled = False
                'Busqueda(gloClave)

            ElseIf opcion = "M" Then

                Me.CONCatalogoSectorBindingNavigator.Enabled = True
                Me.Panel1.Enabled = True
                'Busqueda(gloClave)

            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub CALLEComboBox_SelectedIndexChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CALLEComboBox.SelectedIndexChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.DAMECOLONIA_CALLETableAdapter.Connection = CON
            Me.DAMECOLONIA_CALLETableAdapter.Fill(Me.DataSetLidia.DAMECOLONIA_CALLE, New System.Nullable(Of Integer)(CType(Me.CALLEComboBox.SelectedValue, Integer)))
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        Me.COLONIAComboBox.Enabled = True
        Me.COLONIAComboBox.SelectedValue = 0
        CON.Close()
    End Sub

    Private Sub COLONIAComboBox_SelectedIndexChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles COLONIAComboBox.SelectedIndexChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            'If IsNumeric(Me.COLONIAComboBox.SelectedValue) = True And Me.COLONIAComboBox.SelectedValue > 0 Then
            Me.MuestraCVECOLCIUTableAdapter.Connection = CON
            Me.MuestraCVECOLCIUTableAdapter.Fill(Me.DataSetLidia.MuestraCVECOLCIU, New System.Nullable(Of Integer)(CType(Me.COLONIAComboBox.SelectedValue, Integer)))
            ' End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        Me.CIUDADComboBox.Enabled = True
        If opcion = "N" Then
            Me.CIUDADComboBox.Text = ""
        End If
        CON.Close()
    End Sub

    Private Sub SectorComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SectorComboBox.SelectedIndexChanged

    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub

    Private Sub SalidaTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles SalidaTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.SalidaTextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub
End Class