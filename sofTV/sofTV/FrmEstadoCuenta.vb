﻿
Imports System.Data.SqlClient

Public Class FrmEstadoCuenta

    Private Sub GeneraEstadoCuenta(ByVal Clv_Periodo As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("GeneraEstadoCuenta", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Clv_Periodo", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = Clv_Periodo
        comando.Parameters.Add(par1)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Generación de Estados de Cuenta", "", "Se generaron Estados de Cuenta del periodo " + eClv_Periodo.ToString(), LocClv_Ciudad)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub FrmEstadoCuenta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.BackgroundWorker1.RunWorkerAsync()
    End Sub

    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        GeneraEstadoCuenta(eClv_Periodo)
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(ByVal sender As System.Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        MsgBox("Se han generado con éxito.", MsgBoxStyle.Information)
        Me.Close()
    End Sub
End Class