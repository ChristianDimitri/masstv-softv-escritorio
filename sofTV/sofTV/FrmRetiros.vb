﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text
Imports System.Windows.Forms
Imports System.Collections
Imports System.Collections.Generic
Imports System.Xml
Imports System.IO

Public Class FrmRetiros
    Dim EquiposRetirados As String
    Dim fechasol As Integer
    Dim FechaProm As Date

    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label1.Click

    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Label5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub FrmRetiros_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Me.Label1.ForeColor = Color.Black
        Me.Label2.ForeColor = Color.Black
        Me.Label3.ForeColor = Color.Black
        Me.Label4.ForeColor = Color.Black
        Me.Label5.ForeColor = Color.Black
        Me.Label6.ForeColor = Color.Black
        Me.Label7.ForeColor = Color.Black
        Me.Label8.ForeColor = Color.Black
        Me.Label9.ForeColor = Color.Black
        Me.Label10.ForeColor = Color.Black
        Me.Label11.ForeColor = Color.Black

        FechaAgendaPag.Value = Date.Now
        FechaAgendaPag.MaxDate = DateTime.Today

        Catalogo_Recuperacion()
        Catalogo_PagoNoRealizado()

    End Sub
    Private Sub FechaSolicitud()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_ordser", SqlDbType.BigInt, gloClave)
        BaseII.CreateMyParameter("@fechasol", SqlDbType.Date, FechaAgendaPag.Value)
        BaseII.CreateMyParameter("@Result", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("Fechaminima")
        fechasol = CInt(BaseII.dicoPar("@Result").ToString)
    End Sub
    Private Sub GuardaRespuestas()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Orden", SqlDbType.Int, gloClave)
        BaseII.CreateMyParameter("@FechaPago", SqlDbType.Date, FechaAgendaPag.Value)
        BaseII.CreateMyParameter("@Señal", SqlDbType.Int, CboxSeñal.SelectedIndex)
        BaseII.CreateMyParameter("@Result_Recupera", SqlDbType.VarChar, CboxResultRecupera.SelectedIndex)
        BaseII.CreateMyParameter("@PagoNoRealiza", SqlDbType.VarChar, CboxPagoNoRe.SelectedValue)
        BaseII.CreateMyParameter("@Equipo_Recupera", SqlDbType.VarChar, EquiposRetirados)
        BaseII.CreateMyParameter("@Observaciones", SqlDbType.VarChar, TextBox1.Text)
        BaseII.CreateMyParameter("@HoraIni", SqlDbType.VarChar, MaskedTextBox1.Text)
        BaseII.CreateMyParameter("@HoraFin", SqlDbType.VarChar, MaskedTextBox2.Text)
        BaseII.CreateMyParameter("@Kmsalida", SqlDbType.VarChar, TextBox4.Text)
        BaseII.CreateMyParameter("@KmLlegada", SqlDbType.VarChar, TextBox5.Text)
        BaseII.CreateMyParameter("@HoraPag", SqlDbType.VarChar, MaskedTextBox3.Text)
        BaseII.CreateMyParameter("@FechaProm", SqlDbType.Date, FechaProm)
        BaseII.CreateMyParameter("@Cajas", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@Antenas", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@Control", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@Cable", SqlDbType.Int, 0)
        BaseII.ProcedimientoOutPut("uspGuardaConsulta_Retiro")
        'CInt(Me.ContratoLabel1.Text)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        If DTfechaProm.Visible = True And DTfechaProm.Enabled = True Then
            FechaProm = DTfechaProm.Value
        Else
            FechaProm = "01/01/1900"
        End If

        If CboxSeñal.SelectedIndex = -1 Or CboxResultRecupera.SelectedIndex = -1 Or TextBox1.Text = "" Or MaskedTextBox3.Text = "" Then
            If CboxResultRecupera.SelectedIndex <> 6 Then
                MessageBox.Show("Faltan campos por capturar")
                Exit Sub
            Else

                FechaSolicitud()
                If fechasol = 1 Then
                    MessageBox.Show("La fecha de Agenda no puede ser menor a la de Solicitud de la Orden")
                    Exit Sub
                End If

                Me.EquiposRetirados = 1

                GuardaRespuestas()

                FrmOrdSer.Show()
                Me.Close()

            End If

        Else

            FechaSolicitud()
            If fechasol = 1 Then
                MessageBox.Show("La fecha de Agenda no puede ser menor a la de Solicitud de la Orden")
                Exit Sub
            End If

            Me.EquiposRetirados = 1

            GuardaRespuestas()
            FrmOrdSer.Show()
            Me.Close()
        End If

    End Sub

    Private Sub Label7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label7.Click

    End Sub


    Private Sub CboxResultRecupera_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CboxResultRecupera.SelectedIndexChanged
        If CboxResultRecupera.SelectedIndex = 6 Then
            DTfechaProm.Visible = True
            Label11.Visible = True
        Else
            DTfechaProm.Visible = False
            Label11.Visible = False
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub


    Private Sub Catalogo_Recuperacion()
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("DameCatalogo_Recupera", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Try
            Dim DA As New SqlDataAdapter(CMD)
            Dim DT As New DataTable
            DA.Fill(DT)
            Me.CboxResultRecupera.DataSource = DT
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub Catalogo_PagoNoRealizado()
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("DameCatalogo_PagoNoRealizado", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Try
            Dim DA As New SqlDataAdapter(CMD)
            Dim DT As New DataTable
            DA.Fill(DT)
            Me.CboxPagoNoRe.DataSource = DT
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub


End Class