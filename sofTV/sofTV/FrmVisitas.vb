Imports System.Data.SqlClient
Imports sofTV.Base

Public Class FrmVisitas
    Private Clave As String = Nothing
    Private Descripcion As String = Nothing

    Public datos As New CBase
    Public vistaCliente As New CVisitaClientes

    Private Sub DameDatosBitacora()
        Try
            If LocOpVisita = "M" Then
                Clave = Me.Clv_visitaTextBox.Text
                Descripcion = Me.DescripcionTextBox.Text
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub GuardaDatosBitacora(ByVal Op As Integer)
        Try
            If Op = 0 Then
                If LocOpVisita = "M" Then
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Clave Visita", Clave, Me.Clv_visitaTextBox.Text, LocClv_Ciudad)
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Descripci�n", Descripcion, Me.DescripcionTextBox.Text, LocClv_Ciudad)
                ElseIf LocOpVisita = "N" Then
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Nueva Visita", "", "Visita: " + Me.DescripcionTextBox.Text, LocClv_Ciudad)
                End If
            ElseIf Op = 1 Then
                bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Se Elimin� Visita", "Visita: " + Me.DescripcionTextBox.Text, "", LocClv_Ciudad)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub FrmVisitas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        colorea(Me, Me.Name)
        vendedores()
        capturaVisitaCliente(LocOpVisita)
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Private Sub modificaVisitaCliente(ByVal cliente As CVisitaClientes)
        Using con As New SqlConnection(MiConexion)
            Using comando As New SqlCommand(MiConexion, con)

                con.Open()
                comando.Connection = con
                comando.CommandType = CommandType.StoredProcedure
                comando.CommandText = "Mod_CatalogoVisitas"

                comando.Parameters.Add("@clv_visita", SqlDbType.BigInt)
                comando.Parameters("@clv_visita").Value = cliente.Clv_Visita
                comando.Parameters.Add("@descripcion", SqlDbType.VarChar, 250)
                comando.Parameters("@descripcion").Value = cliente.Descripion
                comando.Parameters.Add("@fecha", SqlDbType.DateTime)
                comando.Parameters("@fecha").Value = cliente.Fecha
                comando.Parameters.Add("@vendedor", SqlDbType.NVarChar, 250)
                comando.Parameters("@vendedor").Value = cliente.Vendedor

                Dim trans As SqlTransaction = Nothing

                Try
                    trans = con.BeginTransaction()
                    comando.Transaction = trans

                    comando.ExecuteNonQuery()
                    trans.Commit()
                Catch ex As Exception

                    trans.Rollback()
                    MessageBox.Show(ex.Message)
                End Try
            End Using
        End Using
    End Sub

    Private Sub insertaVisitaCliente(ByVal cliente As CVisitaClientes)
        Using con As New SqlConnection(MiConexion)
            Using comando As New SqlCommand(MiConexion, con)

                con.Open()
                comando.Connection = con
                comando.CommandType = CommandType.StoredProcedure
                comando.CommandText = "Agrega_CatalogoVisitas"

                comando.Parameters.Add("@descripcion", SqlDbType.VarChar, 250)
                comando.Parameters("@descripcion").Value = cliente.Descripion
                comando.Parameters.Add("@fecha", SqlDbType.DateTime)
                comando.Parameters("@fecha").Value = cliente.Fecha
                comando.Parameters.Add("@vendedor", SqlDbType.NVarChar, 250)
                comando.Parameters("@vendedor").Value = cliente.Vendedor

                Dim trans As SqlTransaction = Nothing

                Try
                    trans = con.BeginTransaction()
                    comando.Transaction = trans

                    comando.ExecuteNonQuery()
                    trans.Commit()
                Catch ex As Exception

                    trans.Rollback()
                    MessageBox.Show(ex.Message)
                End Try
            End Using
        End Using
    End Sub

    Private Sub eliminaVisitaCliente(ByVal cliente As CVisitaClientes)
        Using con As New SqlConnection(MiConexion)
            Using comando As New SqlCommand(MiConexion, con)

                con.Open()
                comando.Connection = con
                comando.CommandType = CommandType.StoredProcedure
                comando.CommandText = "Bor_CatalogoVisitas"

                comando.Parameters.Add("@clv_visita", SqlDbType.BigInt)
                comando.Parameters("@clv_visita").Value = cliente.Clv_Visita

                Dim trans As SqlTransaction = Nothing

                Try
                    trans = con.BeginTransaction()
                    comando.Transaction = trans

                    comando.ExecuteNonQuery()
                    trans.Commit()
                Catch ex As Exception

                    trans.Rollback()
                    MessageBox.Show(ex.Message)
                End Try
            End Using
        End Using
    End Sub

    Public Function obtenerVisitaClientes(ByVal id As Integer) As CVisitaClientes
        Using con As New SqlConnection(MiConexion)
            Using comando As New SqlCommand(MiConexion, con)

                con.Open()
                comando.Connection = con
                comando.CommandText = "COnsulta_CatalogoVisitas"
                comando.CommandType = CommandType.StoredProcedure

                comando.Parameters.Add("@clv_visita", SqlDbType.BigInt)
                comando.Parameters("@clv_visita").Value = id

                Dim reader As SqlDataReader = comando.ExecuteReader()
                If reader.Read() Then
                    vistaCliente = cargarVisitaClientes(reader)
                End If
                reader.Close()

                Return vistaCliente
            End Using
        End Using
    End Function

    Private Function cargarVisitaClientes(ByVal registro As IDataReader) As CVisitaClientes
        Try
            vistaCliente.Clv_Visita = Convert.ToInt32(registro("clv_visita").ToString())
            vistaCliente.Descripion = registro("Descripcion").ToString()
            vistaCliente.Fecha = Convert.ToDateTime(registro("Fecha").ToString())
            vistaCliente.Vendedor = registro("Vendedor").ToString()

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        Return vistaCliente
    End Function

    Private Sub vendedores()
        Dim dt As DataTable = datos.consultarDT("SoftvSaltilloVistaVendedores", Nothing)

        VendedorComboBox.DataSource = dt
        VendedorComboBox.DisplayMember = "Nombre"
        VendedorComboBox.ValueMember = "Nombre"
    End Sub

    Private Sub capturaVisitaCliente(ByVal opcion As String)

        vistaCliente = obtenerVisitaClientes(Locclv_visita)
        BtnEliminar.Enabled = True
        BtnGuardar.Enabled = True
        Select Case opcion
            Case "N"

                BtnEliminar.Enabled = False

                Clv_visitaTextBox.Text = ""
                DescripcionTextBox.Text = ""
            Case "C"

                Me.Panel1.Enabled = False
                BtnGuardar.Enabled = False
                BtnEliminar.Enabled = False

                Clv_visitaTextBox.Text = vistaCliente.Clv_Visita
                DescripcionTextBox.Text = vistaCliente.Descripion
                VendedorComboBox.SelectedValue = vistaCliente.Vendedor
                FechaDTP.Value = vistaCliente.Fecha
            Case "M"

                Clv_visitaTextBox.Text = vistaCliente.Clv_Visita
                DescripcionTextBox.Text = vistaCliente.Descripion
                VendedorComboBox.SelectedValue = vistaCliente.Vendedor
                FechaDTP.Value = vistaCliente.Fecha

        End Select
    End Sub

    Private Sub Operacion(ByVal opcion As String)

        vistaCliente.Clv_Visita = Locclv_visita

        If Not String.IsNullOrEmpty(DescripcionTextBox.Text) Then
            vistaCliente.Descripion = DescripcionTextBox.Text
        Else
            MsgBox("Capture la Descripci�n de la Visita", MsgBoxStyle.Information)
        End If

        vistaCliente.Fecha = FechaDTP.Value
        vistaCliente.Vendedor = VendedorComboBox.SelectedValue

        Select Case LocOpVisita
            Case "N"
                insertaVisitaCliente(vistaCliente)
                MsgBox("Se Ha Guardado con Exito", MsgBoxStyle.Information)
                GuardaDatosBitacora(0)
            Case "M"
                modificaVisitaCliente(vistaCliente)
                MsgBox("Se Ha Guardado con Exito", MsgBoxStyle.Information)
                GuardaDatosBitacora(0)
            Case "E"
                eliminaVisitaCliente(vistaCliente)
                MsgBox("Se Elimino con Exito", MsgBoxStyle.Information)
                GuardaDatosBitacora(1)
        End Select

        DameDatosBitacora()
    End Sub

    Private Sub BtnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnEliminar.Click
        LocOpVisita = "E"
        Operacion(LocOpVisita)
        Me.Close()
    End Sub

    Private Sub BtnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGuardar.Click
        Operacion(LocOpVisita)
        Me.Close()
    End Sub
End Class