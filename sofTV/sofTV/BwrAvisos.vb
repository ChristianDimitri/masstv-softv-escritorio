Imports System.Data.SqlClient
Public Class BwrAvisos

    Private Sub Busca(ByVal op As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Select Case op
                Case 0
                    Me.Busca_AvisoTableAdapter.Connection = CON
                    Me.Busca_AvisoTableAdapter.Fill(Me.DataSetarnoldo.Busca_Aviso, "", "", 0)
                Case 1
                    Me.Busca_AvisoTableAdapter.Connection = CON
                    Me.Busca_AvisoTableAdapter.Fill(Me.DataSetarnoldo.Busca_Aviso, Me.BNOMBRE.Text, "", 1)
                    Me.BNOMBRE.Clear()
                    Me.bcONTRATO.Clear()
                Case 2
                    Me.Busca_AvisoTableAdapter.Connection = CON
                    Me.Busca_AvisoTableAdapter.Fill(Me.DataSetarnoldo.Busca_Aviso, "", Me.bcONTRATO.Text, 2)
                    Me.BNOMBRE.Clear()
                    Me.bcONTRATO.Clear()
            End Select
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()

    End Sub

    Private Sub BwrAvisos_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If bndAvisos = True Then
            bndAvisos = False
            Busca(0)
        End If
    End Sub

    Private Sub BwrAvisos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.Clv_txt_avisoTextBox.BackColor = Color.Gray
        Me.NombreTextBox.BackColor = Color.Gray
        Me.Clv_txt_avisoTextBox.ForeColor = Color.Black
        Me.NombreTextBox.ForeColor = Color.Black

        Busca(0)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Busca(2)
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Busca(1)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        LocOpAviso = "N"
        FrmCatAvisos.Show()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.Busca_AvisoDataGridView.RowCount > 0 Then
            LocOpAviso = "C"
            LocClaveaviso = Me.Busca_AvisoDataGridView.SelectedCells(0).Value
            FrmCatAvisos.Show()
        Else
            MsgBox(mensaje2)
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If Me.Busca_AvisoDataGridView.RowCount > 0 Then
            LocOpAviso = "M"
            LocClaveaviso = Me.Busca_AvisoDataGridView.SelectedCells(0).Value
            FrmCatAvisos.Show()
        Else
            MsgBox(mensaje1)
        End If
    End Sub

    Private Sub bcONTRATO_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles bcONTRATO.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
        End If
    End Sub

    Private Sub BNOMBRE_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BNOMBRE.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
        End If
    End Sub

    Private Sub Label2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBLabel2.Click

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label1.Click

    End Sub
End Class