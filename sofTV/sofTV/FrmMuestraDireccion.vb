Imports System.Data.SqlClient
Public Class FrmMuestraDireccion
    Public Guardar As Boolean = False
    Private Sub FrmMuestraDireccion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Locbnd2clientes = True Then
            Locbnd2clientes = False
        End If
        Dim CON As New SqlConnection(MiConexion)
        colorea(Me, Me.Name)
        CON.Open()
        Me.Muestra_Valida_DireccionTableAdapter.Connection = CON
        Me.Muestra_Valida_DireccionTableAdapter.Fill(Me.ProcedimientosArnoldo4.Muestra_Valida_Direccion, Locclv_session2)
        CON.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Locbndguardar = False
        Locbnd2clientes = False
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If (eBndAutorizacionDireccionRepetido = True) Then
            Locbndguardar = True
            Locbnd2clientes = True
            FrmAutorizacion.DireccionAutorizado = True
            Guardar = True
            Me.Close()
        Else
            FrmAutorizacion.OpcionValidar = "DIRECCION"
            FrmAutorizacion.ShowDialog()
            Me.Close()
        End If
        
    End Sub

    Private Sub Muestra_Valida_DireccionDataGridView_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Muestra_Valida_DireccionDataGridView.CellDoubleClick
        'Mandamos Mostrar los Datos Generales del Cliente
        If (Me.Muestra_Valida_DireccionDataGridView.RowCount > 0) Then
            Try
                'FrmDatosGeneralesClientes.Contrato = Me.dgvIFE.CurrentRow.Cells(0).Value
                'FrmDatosGeneralesClientes.ShowDialog()
                'FrmDatosGeneralesClientes.Contrato = 0
                'Me.DataGridView1.SelectedCells.Item(0).Value()
                If IsNumeric(Me.Muestra_Valida_DireccionDataGridView.CurrentRow.Cells(0).Value) = True Then
                    Module1.Contrato = Me.Muestra_Valida_DireccionDataGridView.CurrentRow.Cells(0).Value
                    OpcionCli = "C"
                    Dim PantallaClientes As New FrmClientes()
                    colorea(PantallaClientes, PantallaClientes.Name)
                    PantallaClientes.ShowDialog()
                Else
                    MsgBox("Seleccione un Cliente para poder Consultar ", MsgBoxStyle.Information)
                End If

            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub
End Class