<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDatosFiscales
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim RAZON_SOCIALLabel As System.Windows.Forms.Label
        Dim RFCLabel As System.Windows.Forms.Label
        Dim CALLE_RSLabel As System.Windows.Forms.Label
        Dim NUMERO_RSLabel As System.Windows.Forms.Label
        Dim ENTRECALLESLabel As System.Windows.Forms.Label
        Dim COLONIA_RSLabel As System.Windows.Forms.Label
        Dim CIUDAD_RSLabel As System.Windows.Forms.Label
        Dim ESTADO_RSLabel As System.Windows.Forms.Label
        Dim CP_RSLabel As System.Windows.Forms.Label
        Dim TELEFONO_RSLabel As System.Windows.Forms.Label
        Dim FAX_RSLabel As System.Windows.Forms.Label
        Dim CURPLabel As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmDatosFiscales))
        Me.CONDatosFiscalesBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.CONDatosFiscalesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.CONDatosFiscalesBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.ContratoTextBox = New System.Windows.Forms.TextBox()
        Me.IVADESGLOSADOTextBox = New System.Windows.Forms.TextBox()
        Me.RAZON_SOCIALTextBox = New System.Windows.Forms.TextBox()
        Me.RFCTextBox = New System.Windows.Forms.TextBox()
        Me.CALLE_RSTextBox = New System.Windows.Forms.TextBox()
        Me.NUMERO_RSTextBox = New System.Windows.Forms.TextBox()
        Me.ENTRECALLESTextBox = New System.Windows.Forms.TextBox()
        Me.COLONIA_RSTextBox = New System.Windows.Forms.TextBox()
        Me.CIUDAD_RSTextBox = New System.Windows.Forms.TextBox()
        Me.ESTADO_RSTextBox = New System.Windows.Forms.TextBox()
        Me.CP_RSTextBox = New System.Windows.Forms.TextBox()
        Me.TELEFONO_RSTextBox = New System.Windows.Forms.TextBox()
        Me.FAX_RSTextBox = New System.Windows.Forms.TextBox()
        Me.CURPTextBox = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.CmbCompania = New System.Windows.Forms.ComboBox()
        Me.EMail_TextBox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LblPais = New System.Windows.Forms.Label()
        Me.TxtPais = New System.Windows.Forms.TextBox()
        Me.CONDatosFiscalesTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONDatosFiscalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        RAZON_SOCIALLabel = New System.Windows.Forms.Label()
        RFCLabel = New System.Windows.Forms.Label()
        CALLE_RSLabel = New System.Windows.Forms.Label()
        NUMERO_RSLabel = New System.Windows.Forms.Label()
        ENTRECALLESLabel = New System.Windows.Forms.Label()
        COLONIA_RSLabel = New System.Windows.Forms.Label()
        CIUDAD_RSLabel = New System.Windows.Forms.Label()
        ESTADO_RSLabel = New System.Windows.Forms.Label()
        CP_RSLabel = New System.Windows.Forms.Label()
        TELEFONO_RSLabel = New System.Windows.Forms.Label()
        FAX_RSLabel = New System.Windows.Forms.Label()
        CURPLabel = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        CType(Me.CONDatosFiscalesBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONDatosFiscalesBindingNavigator.SuspendLayout()
        CType(Me.CONDatosFiscalesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'RAZON_SOCIALLabel
        '
        RAZON_SOCIALLabel.AutoSize = True
        RAZON_SOCIALLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        RAZON_SOCIALLabel.ForeColor = System.Drawing.Color.LightSlateGray
        RAZON_SOCIALLabel.Location = New System.Drawing.Point(15, 55)
        RAZON_SOCIALLabel.Name = "RAZON_SOCIALLabel"
        RAZON_SOCIALLabel.Size = New System.Drawing.Size(96, 15)
        RAZON_SOCIALLabel.TabIndex = 6
        RAZON_SOCIALLabel.Text = "Razon Social:"
        '
        'RFCLabel
        '
        RFCLabel.AutoSize = True
        RFCLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        RFCLabel.ForeColor = System.Drawing.Color.LightSlateGray
        RFCLabel.Location = New System.Drawing.Point(73, 106)
        RFCLabel.Name = "RFCLabel"
        RFCLabel.Size = New System.Drawing.Size(38, 15)
        RFCLabel.TabIndex = 8
        RFCLabel.Text = "RFC:"
        '
        'CALLE_RSLabel
        '
        CALLE_RSLabel.AutoSize = True
        CALLE_RSLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CALLE_RSLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CALLE_RSLabel.Location = New System.Drawing.Point(63, 158)
        CALLE_RSLabel.Name = "CALLE_RSLabel"
        CALLE_RSLabel.Size = New System.Drawing.Size(48, 15)
        CALLE_RSLabel.TabIndex = 10
        CALLE_RSLabel.Text = "Calle :"
        '
        'NUMERO_RSLabel
        '
        NUMERO_RSLabel.AutoSize = True
        NUMERO_RSLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NUMERO_RSLabel.ForeColor = System.Drawing.Color.LightSlateGray
        NUMERO_RSLabel.Location = New System.Drawing.Point(45, 184)
        NUMERO_RSLabel.Name = "NUMERO_RSLabel"
        NUMERO_RSLabel.Size = New System.Drawing.Size(66, 15)
        NUMERO_RSLabel.TabIndex = 12
        NUMERO_RSLabel.Text = "Numero :"
        '
        'ENTRECALLESLabel
        '
        ENTRECALLESLabel.AutoSize = True
        ENTRECALLESLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ENTRECALLESLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ENTRECALLESLabel.Location = New System.Drawing.Point(20, 210)
        ENTRECALLESLabel.Name = "ENTRECALLESLabel"
        ENTRECALLESLabel.Size = New System.Drawing.Size(91, 15)
        ENTRECALLESLabel.TabIndex = 14
        ENTRECALLESLabel.Text = "Entre calles :"
        '
        'COLONIA_RSLabel
        '
        COLONIA_RSLabel.AutoSize = True
        COLONIA_RSLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        COLONIA_RSLabel.ForeColor = System.Drawing.Color.LightSlateGray
        COLONIA_RSLabel.Location = New System.Drawing.Point(47, 236)
        COLONIA_RSLabel.Name = "COLONIA_RSLabel"
        COLONIA_RSLabel.Size = New System.Drawing.Size(64, 15)
        COLONIA_RSLabel.TabIndex = 16
        COLONIA_RSLabel.Text = "Colonia :"
        '
        'CIUDAD_RSLabel
        '
        CIUDAD_RSLabel.AutoSize = True
        CIUDAD_RSLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CIUDAD_RSLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CIUDAD_RSLabel.Location = New System.Drawing.Point(51, 314)
        CIUDAD_RSLabel.Name = "CIUDAD_RSLabel"
        CIUDAD_RSLabel.Size = New System.Drawing.Size(60, 15)
        CIUDAD_RSLabel.TabIndex = 18
        CIUDAD_RSLabel.Text = "Ciudad :"
        '
        'ESTADO_RSLabel
        '
        ESTADO_RSLabel.AutoSize = True
        ESTADO_RSLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ESTADO_RSLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ESTADO_RSLabel.Location = New System.Drawing.Point(52, 288)
        ESTADO_RSLabel.Name = "ESTADO_RSLabel"
        ESTADO_RSLabel.Size = New System.Drawing.Size(59, 15)
        ESTADO_RSLabel.TabIndex = 20
        ESTADO_RSLabel.Text = "Estado :"
        '
        'CP_RSLabel
        '
        CP_RSLabel.AutoSize = True
        CP_RSLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CP_RSLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CP_RSLabel.Location = New System.Drawing.Point(7, 262)
        CP_RSLabel.Name = "CP_RSLabel"
        CP_RSLabel.Size = New System.Drawing.Size(104, 15)
        CP_RSLabel.TabIndex = 22
        CP_RSLabel.Text = "Codigo Postal :"
        AddHandler CP_RSLabel.Click, AddressOf Me.CP_RSLabel_Click
        '
        'TELEFONO_RSLabel
        '
        TELEFONO_RSLabel.AutoSize = True
        TELEFONO_RSLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TELEFONO_RSLabel.ForeColor = System.Drawing.Color.LightSlateGray
        TELEFONO_RSLabel.Location = New System.Drawing.Point(40, 369)
        TELEFONO_RSLabel.Name = "TELEFONO_RSLabel"
        TELEFONO_RSLabel.Size = New System.Drawing.Size(71, 15)
        TELEFONO_RSLabel.TabIndex = 24
        TELEFONO_RSLabel.Text = "Teléfono :"
        '
        'FAX_RSLabel
        '
        FAX_RSLabel.AutoSize = True
        FAX_RSLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FAX_RSLabel.ForeColor = System.Drawing.Color.LightSlateGray
        FAX_RSLabel.Location = New System.Drawing.Point(73, 395)
        FAX_RSLabel.Name = "FAX_RSLabel"
        FAX_RSLabel.Size = New System.Drawing.Size(38, 15)
        FAX_RSLabel.TabIndex = 26
        FAX_RSLabel.Text = "Fax :"
        '
        'CURPLabel
        '
        CURPLabel.AutoSize = True
        CURPLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CURPLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CURPLabel.Location = New System.Drawing.Point(62, 132)
        CURPLabel.Name = "CURPLabel"
        CURPLabel.Size = New System.Drawing.Size(49, 15)
        CURPLabel.TabIndex = 32
        CURPLabel.Text = "CURP:"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(32, 27)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(76, 15)
        Label2.TabIndex = 38
        Label2.Text = "Compañia:"
        '
        'CONDatosFiscalesBindingNavigator
        '
        Me.CONDatosFiscalesBindingNavigator.AddNewItem = Nothing
        Me.CONDatosFiscalesBindingNavigator.BindingSource = Me.CONDatosFiscalesBindingSource
        Me.CONDatosFiscalesBindingNavigator.CountItem = Nothing
        Me.CONDatosFiscalesBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONDatosFiscalesBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.CONDatosFiscalesBindingNavigatorSaveItem})
        Me.CONDatosFiscalesBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONDatosFiscalesBindingNavigator.MoveFirstItem = Nothing
        Me.CONDatosFiscalesBindingNavigator.MoveLastItem = Nothing
        Me.CONDatosFiscalesBindingNavigator.MoveNextItem = Nothing
        Me.CONDatosFiscalesBindingNavigator.MovePreviousItem = Nothing
        Me.CONDatosFiscalesBindingNavigator.Name = "CONDatosFiscalesBindingNavigator"
        Me.CONDatosFiscalesBindingNavigator.PositionItem = Nothing
        Me.CONDatosFiscalesBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONDatosFiscalesBindingNavigator.Size = New System.Drawing.Size(722, 25)
        Me.CONDatosFiscalesBindingNavigator.TabIndex = 12
        Me.CONDatosFiscalesBindingNavigator.TabStop = True
        Me.CONDatosFiscalesBindingNavigator.Text = "BindingNavigator1"
        '
        'CONDatosFiscalesBindingSource
        '
        Me.CONDatosFiscalesBindingSource.DataMember = "CONDatosFiscales"
        Me.CONDatosFiscalesBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(77, 22)
        Me.BindingNavigatorDeleteItem.Text = "&Eliminar"
        '
        'CONDatosFiscalesBindingNavigatorSaveItem
        '
        Me.CONDatosFiscalesBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONDatosFiscalesBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONDatosFiscalesBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONDatosFiscalesBindingNavigatorSaveItem.Name = "CONDatosFiscalesBindingNavigatorSaveItem"
        Me.CONDatosFiscalesBindingNavigatorSaveItem.Size = New System.Drawing.Size(121, 22)
        Me.CONDatosFiscalesBindingNavigatorSaveItem.Text = "&Guardar datos"
        '
        'ContratoTextBox
        '
        Me.ContratoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDatosFiscalesBindingSource, "Contrato", True))
        Me.ContratoTextBox.Location = New System.Drawing.Point(219, 5)
        Me.ContratoTextBox.Name = "ContratoTextBox"
        Me.ContratoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ContratoTextBox.TabIndex = 300
        Me.ContratoTextBox.TabStop = False
        '
        'IVADESGLOSADOTextBox
        '
        Me.IVADESGLOSADOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDatosFiscalesBindingSource, "IVADESGLOSADO", True))
        Me.IVADESGLOSADOTextBox.Location = New System.Drawing.Point(361, 1)
        Me.IVADESGLOSADOTextBox.Name = "IVADESGLOSADOTextBox"
        Me.IVADESGLOSADOTextBox.Size = New System.Drawing.Size(100, 20)
        Me.IVADESGLOSADOTextBox.TabIndex = 50
        Me.IVADESGLOSADOTextBox.TabStop = False
        Me.IVADESGLOSADOTextBox.Text = "1"
        '
        'RAZON_SOCIALTextBox
        '
        Me.RAZON_SOCIALTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.RAZON_SOCIALTextBox.CausesValidation = False
        Me.RAZON_SOCIALTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDatosFiscalesBindingSource, "RAZON_SOCIAL", True))
        Me.RAZON_SOCIALTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RAZON_SOCIALTextBox.Location = New System.Drawing.Point(117, 54)
        Me.RAZON_SOCIALTextBox.MaxLength = 150
        Me.RAZON_SOCIALTextBox.Multiline = True
        Me.RAZON_SOCIALTextBox.Name = "RAZON_SOCIALTextBox"
        Me.RAZON_SOCIALTextBox.Size = New System.Drawing.Size(516, 45)
        Me.RAZON_SOCIALTextBox.TabIndex = 1
        '
        'RFCTextBox
        '
        Me.RFCTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.RFCTextBox.CausesValidation = False
        Me.RFCTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.RFCTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDatosFiscalesBindingSource, "RFC", True))
        Me.RFCTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RFCTextBox.Location = New System.Drawing.Point(117, 105)
        Me.RFCTextBox.MaxLength = 15
        Me.RFCTextBox.Name = "RFCTextBox"
        Me.RFCTextBox.Size = New System.Drawing.Size(128, 21)
        Me.RFCTextBox.TabIndex = 1
        '
        'CALLE_RSTextBox
        '
        Me.CALLE_RSTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CALLE_RSTextBox.CausesValidation = False
        Me.CALLE_RSTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDatosFiscalesBindingSource, "CALLE_RS", True))
        Me.CALLE_RSTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CALLE_RSTextBox.Location = New System.Drawing.Point(117, 157)
        Me.CALLE_RSTextBox.MaxLength = 150
        Me.CALLE_RSTextBox.Name = "CALLE_RSTextBox"
        Me.CALLE_RSTextBox.Size = New System.Drawing.Size(516, 21)
        Me.CALLE_RSTextBox.TabIndex = 3
        '
        'NUMERO_RSTextBox
        '
        Me.NUMERO_RSTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NUMERO_RSTextBox.CausesValidation = False
        Me.NUMERO_RSTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDatosFiscalesBindingSource, "NUMERO_RS", True))
        Me.NUMERO_RSTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NUMERO_RSTextBox.Location = New System.Drawing.Point(117, 183)
        Me.NUMERO_RSTextBox.MaxLength = 50
        Me.NUMERO_RSTextBox.Name = "NUMERO_RSTextBox"
        Me.NUMERO_RSTextBox.Size = New System.Drawing.Size(304, 21)
        Me.NUMERO_RSTextBox.TabIndex = 4
        '
        'ENTRECALLESTextBox
        '
        Me.ENTRECALLESTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ENTRECALLESTextBox.CausesValidation = False
        Me.ENTRECALLESTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDatosFiscalesBindingSource, "ENTRECALLES", True))
        Me.ENTRECALLESTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ENTRECALLESTextBox.Location = New System.Drawing.Point(117, 209)
        Me.ENTRECALLESTextBox.MaxLength = 150
        Me.ENTRECALLESTextBox.Name = "ENTRECALLESTextBox"
        Me.ENTRECALLESTextBox.Size = New System.Drawing.Size(516, 21)
        Me.ENTRECALLESTextBox.TabIndex = 5
        '
        'COLONIA_RSTextBox
        '
        Me.COLONIA_RSTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.COLONIA_RSTextBox.CausesValidation = False
        Me.COLONIA_RSTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDatosFiscalesBindingSource, "COLONIA_RS", True))
        Me.COLONIA_RSTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.COLONIA_RSTextBox.Location = New System.Drawing.Point(117, 235)
        Me.COLONIA_RSTextBox.MaxLength = 150
        Me.COLONIA_RSTextBox.Name = "COLONIA_RSTextBox"
        Me.COLONIA_RSTextBox.Size = New System.Drawing.Size(392, 21)
        Me.COLONIA_RSTextBox.TabIndex = 6
        '
        'CIUDAD_RSTextBox
        '
        Me.CIUDAD_RSTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CIUDAD_RSTextBox.CausesValidation = False
        Me.CIUDAD_RSTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDatosFiscalesBindingSource, "CIUDAD_RS", True))
        Me.CIUDAD_RSTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CIUDAD_RSTextBox.Location = New System.Drawing.Point(117, 313)
        Me.CIUDAD_RSTextBox.MaxLength = 150
        Me.CIUDAD_RSTextBox.Name = "CIUDAD_RSTextBox"
        Me.CIUDAD_RSTextBox.Size = New System.Drawing.Size(292, 21)
        Me.CIUDAD_RSTextBox.TabIndex = 9
        '
        'ESTADO_RSTextBox
        '
        Me.ESTADO_RSTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ESTADO_RSTextBox.CausesValidation = False
        Me.ESTADO_RSTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDatosFiscalesBindingSource, "ESTADO_RS", True))
        Me.ESTADO_RSTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ESTADO_RSTextBox.Location = New System.Drawing.Point(117, 287)
        Me.ESTADO_RSTextBox.MaxLength = 150
        Me.ESTADO_RSTextBox.Name = "ESTADO_RSTextBox"
        Me.ESTADO_RSTextBox.Size = New System.Drawing.Size(292, 21)
        Me.ESTADO_RSTextBox.TabIndex = 8
        '
        'CP_RSTextBox
        '
        Me.CP_RSTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CP_RSTextBox.CausesValidation = False
        Me.CP_RSTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.CP_RSTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDatosFiscalesBindingSource, "CP_RS", True))
        Me.CP_RSTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CP_RSTextBox.Location = New System.Drawing.Point(117, 261)
        Me.CP_RSTextBox.MaxLength = 20
        Me.CP_RSTextBox.Name = "CP_RSTextBox"
        Me.CP_RSTextBox.Size = New System.Drawing.Size(100, 21)
        Me.CP_RSTextBox.TabIndex = 7
        '
        'TELEFONO_RSTextBox
        '
        Me.TELEFONO_RSTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TELEFONO_RSTextBox.CausesValidation = False
        Me.TELEFONO_RSTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TELEFONO_RSTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDatosFiscalesBindingSource, "TELEFONO_RS", True))
        Me.TELEFONO_RSTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TELEFONO_RSTextBox.Location = New System.Drawing.Point(117, 368)
        Me.TELEFONO_RSTextBox.MaxLength = 30
        Me.TELEFONO_RSTextBox.Name = "TELEFONO_RSTextBox"
        Me.TELEFONO_RSTextBox.Size = New System.Drawing.Size(128, 21)
        Me.TELEFONO_RSTextBox.TabIndex = 11
        '
        'FAX_RSTextBox
        '
        Me.FAX_RSTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FAX_RSTextBox.CausesValidation = False
        Me.FAX_RSTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.FAX_RSTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDatosFiscalesBindingSource, "FAX_RS", True))
        Me.FAX_RSTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FAX_RSTextBox.Location = New System.Drawing.Point(117, 395)
        Me.FAX_RSTextBox.MaxLength = 30
        Me.FAX_RSTextBox.Name = "FAX_RSTextBox"
        Me.FAX_RSTextBox.Size = New System.Drawing.Size(128, 21)
        Me.FAX_RSTextBox.TabIndex = 12
        '
        'CURPTextBox
        '
        Me.CURPTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CURPTextBox.CausesValidation = False
        Me.CURPTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.CURPTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDatosFiscalesBindingSource, "CURP", True))
        Me.CURPTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CURPTextBox.Location = New System.Drawing.Point(117, 131)
        Me.CURPTextBox.MaxLength = 30
        Me.CURPTextBox.Name = "CURPTextBox"
        Me.CURPTextBox.Size = New System.Drawing.Size(241, 21)
        Me.CURPTextBox.TabIndex = 2
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(565, 483)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 13
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Label2)
        Me.Panel1.Controls.Add(Me.CmbCompania)
        Me.Panel1.Controls.Add(Me.EMail_TextBox)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.LblPais)
        Me.Panel1.Controls.Add(Me.TxtPais)
        Me.Panel1.Controls.Add(RAZON_SOCIALLabel)
        Me.Panel1.Controls.Add(RFCLabel)
        Me.Panel1.Controls.Add(CALLE_RSLabel)
        Me.Panel1.Controls.Add(NUMERO_RSLabel)
        Me.Panel1.Controls.Add(ENTRECALLESLabel)
        Me.Panel1.Controls.Add(Me.FAX_RSTextBox)
        Me.Panel1.Controls.Add(COLONIA_RSLabel)
        Me.Panel1.Controls.Add(CIUDAD_RSLabel)
        Me.Panel1.Controls.Add(ESTADO_RSLabel)
        Me.Panel1.Controls.Add(CP_RSLabel)
        Me.Panel1.Controls.Add(TELEFONO_RSLabel)
        Me.Panel1.Controls.Add(FAX_RSLabel)
        Me.Panel1.Controls.Add(CURPLabel)
        Me.Panel1.Controls.Add(Me.RAZON_SOCIALTextBox)
        Me.Panel1.Controls.Add(Me.RFCTextBox)
        Me.Panel1.Controls.Add(Me.CALLE_RSTextBox)
        Me.Panel1.Controls.Add(Me.NUMERO_RSTextBox)
        Me.Panel1.Controls.Add(Me.ENTRECALLESTextBox)
        Me.Panel1.Controls.Add(Me.COLONIA_RSTextBox)
        Me.Panel1.Controls.Add(Me.CIUDAD_RSTextBox)
        Me.Panel1.Controls.Add(Me.ESTADO_RSTextBox)
        Me.Panel1.Controls.Add(Me.CP_RSTextBox)
        Me.Panel1.Controls.Add(Me.TELEFONO_RSTextBox)
        Me.Panel1.Controls.Add(Me.CURPTextBox)
        Me.Panel1.Location = New System.Drawing.Point(30, 31)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(664, 446)
        Me.Panel1.TabIndex = 0
        Me.Panel1.TabStop = True
        '
        'CmbCompania
        '
        Me.CmbCompania.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CmbCompania.FormattingEnabled = True
        Me.CmbCompania.Location = New System.Drawing.Point(117, 24)
        Me.CmbCompania.Name = "CmbCompania"
        Me.CmbCompania.Size = New System.Drawing.Size(516, 24)
        Me.CmbCompania.TabIndex = 0
        '
        'EMail_TextBox
        '
        Me.EMail_TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EMail_TextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.EMail_TextBox.Location = New System.Drawing.Point(117, 422)
        Me.EMail_TextBox.MaxLength = 100
        Me.EMail_TextBox.Name = "EMail_TextBox"
        Me.EMail_TextBox.Size = New System.Drawing.Size(292, 21)
        Me.EMail_TextBox.TabIndex = 36
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label1.Location = New System.Drawing.Point(59, 422)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 15)
        Me.Label1.TabIndex = 35
        Me.Label1.Text = "EMail :"
        '
        'LblPais
        '
        Me.LblPais.AutoSize = True
        Me.LblPais.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.LblPais.ForeColor = System.Drawing.Color.LightSlateGray
        Me.LblPais.Location = New System.Drawing.Point(69, 341)
        Me.LblPais.Name = "LblPais"
        Me.LblPais.Size = New System.Drawing.Size(39, 15)
        Me.LblPais.TabIndex = 34
        Me.LblPais.Text = "Pais:"
        '
        'TxtPais
        '
        Me.TxtPais.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TxtPais.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.TxtPais.Location = New System.Drawing.Point(117, 340)
        Me.TxtPais.MaxLength = 150
        Me.TxtPais.Name = "TxtPais"
        Me.TxtPais.Size = New System.Drawing.Size(292, 21)
        Me.TxtPais.TabIndex = 10
        '
        'CONDatosFiscalesTableAdapter
        '
        Me.CONDatosFiscalesTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'FrmDatosFiscales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(722, 528)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.CONDatosFiscalesBindingNavigator)
        Me.Controls.Add(Me.IVADESGLOSADOTextBox)
        Me.Controls.Add(Me.ContratoTextBox)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmDatosFiscales"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Datos Fiscales del Cliente"
        CType(Me.CONDatosFiscalesBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONDatosFiscalesBindingNavigator.ResumeLayout(False)
        Me.CONDatosFiscalesBindingNavigator.PerformLayout()
        CType(Me.CONDatosFiscalesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONDatosFiscalesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONDatosFiscalesBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONDatosFiscalesBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ContratoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents IVADESGLOSADOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents RAZON_SOCIALTextBox As System.Windows.Forms.TextBox
    Friend WithEvents RFCTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CALLE_RSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NUMERO_RSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ENTRECALLESTextBox As System.Windows.Forms.TextBox
    Friend WithEvents COLONIA_RSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CIUDAD_RSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ESTADO_RSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CP_RSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TELEFONO_RSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FAX_RSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CURPTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents CONDatosFiscalesTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONDatosFiscalesTableAdapter
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents LblPais As System.Windows.Forms.Label
    Friend WithEvents TxtPais As System.Windows.Forms.TextBox
    Friend WithEvents EMail_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents CmbCompania As System.Windows.Forms.ComboBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
