Imports System.Data.SqlClient

Public Class FrmCajasAdicionales

    Private Sub Guarda_CajasDigitales(ByVal contrato As Long, ByVal Cajas As Integer)
        Try
            Dim CON As New SqlClient.SqlConnection(MiConexion)
            Dim cmd As New SqlClient.SqlCommand()
            CON.Open()
            With cmd
                .CommandText = "Guarda_CajasDigitales"
                .Connection = CON
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim Prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                Prm.Direction = ParameterDirection.Input
                Prm.Value = contrato
                .Parameters.Add(Prm)


                Dim prm2 As New SqlParameter("@NoCajas", SqlDbType.Int)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = Cajas
                .Parameters.Add(prm2)

                Dim i As Integer = cmd.ExecuteNonQuery()
            End With
            CON.Close()


        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FrmCajasAdicionales_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CAJASDIG = 0
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        CAJASDIG = Me.CajasUpDown.Value
        If IsNumeric(LocContratoLog) = True Then
            Guarda_CajasDigitales(LocContratoLog, Me.CajasUpDown.Value)
            FrmContratacionCombo.Guarda_Clientes_Combo(LocContratoLog, GloClv_Combo, Tipo, 0, 0, CAJASDIG)
            FrmClientes.CREAARBOLDIGITAL2()
        End If
        Me.Close()
    End Sub
End Class