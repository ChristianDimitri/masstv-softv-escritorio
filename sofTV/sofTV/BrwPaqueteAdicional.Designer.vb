<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwPaqueteAdicional
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim PaqueteAdicionalLabel As System.Windows.Forms.Label
        Dim ContratacionLabel As System.Windows.Forms.Label
        Dim MensualidadLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.ConPaqueteAdicionalDataGridView = New System.Windows.Forms.DataGridView
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button5 = New System.Windows.Forms.Button
        Me.Label4 = New System.Windows.Forms.Label
        Me.TipoCobroTextBox = New System.Windows.Forms.TextBox
        Me.PaqueteAdicionalLabel1 = New System.Windows.Forms.Label
        Me.NumeroLabel1 = New System.Windows.Forms.Label
        Me.ContratacionLabel1 = New System.Windows.Forms.Label
        Me.MensualidadLabel1 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.CMBLabel1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.ConPaqueteAdicionalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric2 = New sofTV.DataSetEric2
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ConPaqueteAdicionalTableAdapter = New sofTV.DataSetEric2TableAdapters.ConPaqueteAdicionalTableAdapter
        PaqueteAdicionalLabel = New System.Windows.Forms.Label
        ContratacionLabel = New System.Windows.Forms.Label
        MensualidadLabel = New System.Windows.Forms.Label
        CType(Me.ConPaqueteAdicionalDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.ConPaqueteAdicionalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PaqueteAdicionalLabel
        '
        PaqueteAdicionalLabel.AutoSize = True
        PaqueteAdicionalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PaqueteAdicionalLabel.Location = New System.Drawing.Point(15, 48)
        PaqueteAdicionalLabel.Name = "PaqueteAdicionalLabel"
        PaqueteAdicionalLabel.Size = New System.Drawing.Size(127, 15)
        PaqueteAdicionalLabel.TabIndex = 9
        PaqueteAdicionalLabel.Text = "Paquete Adicional:"
        '
        'ContratacionLabel
        '
        ContratacionLabel.AutoSize = True
        ContratacionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ContratacionLabel.Location = New System.Drawing.Point(12, 152)
        ContratacionLabel.Name = "ContratacionLabel"
        ContratacionLabel.Size = New System.Drawing.Size(51, 15)
        ContratacionLabel.TabIndex = 11
        ContratacionLabel.Text = "Costo :"
        '
        'MensualidadLabel
        '
        MensualidadLabel.AutoSize = True
        MensualidadLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        MensualidadLabel.Location = New System.Drawing.Point(12, 200)
        MensualidadLabel.Name = "MensualidadLabel"
        MensualidadLabel.Size = New System.Drawing.Size(66, 15)
        MensualidadLabel.TabIndex = 12
        MensualidadLabel.Text = "Minutos :"
        MensualidadLabel.Visible = False
        '
        'ConPaqueteAdicionalDataGridView
        '
        Me.ConPaqueteAdicionalDataGridView.AllowUserToAddRows = False
        Me.ConPaqueteAdicionalDataGridView.AllowUserToDeleteRows = False
        Me.ConPaqueteAdicionalDataGridView.AutoGenerateColumns = False
        Me.ConPaqueteAdicionalDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ConPaqueteAdicionalDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.ConPaqueteAdicionalDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5})
        Me.ConPaqueteAdicionalDataGridView.DataSource = Me.ConPaqueteAdicionalBindingSource
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ConPaqueteAdicionalDataGridView.DefaultCellStyle = DataGridViewCellStyle2
        Me.ConPaqueteAdicionalDataGridView.Location = New System.Drawing.Point(3, 0)
        Me.ConPaqueteAdicionalDataGridView.Name = "ConPaqueteAdicionalDataGridView"
        Me.ConPaqueteAdicionalDataGridView.ReadOnly = True
        Me.ConPaqueteAdicionalDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.ConPaqueteAdicionalDataGridView.Size = New System.Drawing.Size(491, 731)
        Me.ConPaqueteAdicionalDataGridView.TabIndex = 0
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(868, 5)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "&NUEVO"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(868, 47)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(136, 36)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = "&CONSULTA"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(868, 89)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(136, 36)
        Me.Button3.TabIndex = 3
        Me.Button3.Text = "&MODIFICA"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(868, 693)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(136, 36)
        Me.Button4.TabIndex = 4
        Me.Button4.Text = "&SALIR"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(18, 118)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(75, 23)
        Me.Button5.TabIndex = 5
        Me.Button5.Text = "&Buscar"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(15, 12)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(141, 16)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Datos del Paquete:"
        '
        'TipoCobroTextBox
        '
        Me.TipoCobroTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConPaqueteAdicionalBindingSource, "TipoCobro", True))
        Me.TipoCobroTextBox.Location = New System.Drawing.Point(21, 304)
        Me.TipoCobroTextBox.Name = "TipoCobroTextBox"
        Me.TipoCobroTextBox.ReadOnly = True
        Me.TipoCobroTextBox.Size = New System.Drawing.Size(152, 20)
        Me.TipoCobroTextBox.TabIndex = 9
        Me.TipoCobroTextBox.TabStop = False
        '
        'PaqueteAdicionalLabel1
        '
        Me.PaqueteAdicionalLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConPaqueteAdicionalBindingSource, "PaqueteAdicional", True))
        Me.PaqueteAdicionalLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PaqueteAdicionalLabel1.Location = New System.Drawing.Point(18, 63)
        Me.PaqueteAdicionalLabel1.Name = "PaqueteAdicionalLabel1"
        Me.PaqueteAdicionalLabel1.Size = New System.Drawing.Size(206, 26)
        Me.PaqueteAdicionalLabel1.TabIndex = 10
        '
        'NumeroLabel1
        '
        Me.NumeroLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConPaqueteAdicionalBindingSource, "Numero", True))
        Me.NumeroLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumeroLabel1.Location = New System.Drawing.Point(15, 115)
        Me.NumeroLabel1.Name = "NumeroLabel1"
        Me.NumeroLabel1.Size = New System.Drawing.Size(206, 23)
        Me.NumeroLabel1.TabIndex = 11
        '
        'ContratacionLabel1
        '
        Me.ContratacionLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConPaqueteAdicionalBindingSource, "Costo", True))
        Me.ContratacionLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContratacionLabel1.Location = New System.Drawing.Point(15, 167)
        Me.ContratacionLabel1.Name = "ContratacionLabel1"
        Me.ContratacionLabel1.Size = New System.Drawing.Size(206, 23)
        Me.ContratacionLabel1.TabIndex = 12
        '
        'MensualidadLabel1
        '
        Me.MensualidadLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConPaqueteAdicionalBindingSource, "Minutos", True))
        Me.MensualidadLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MensualidadLabel1.Location = New System.Drawing.Point(15, 215)
        Me.MensualidadLabel1.Name = "MensualidadLabel1"
        Me.MensualidadLabel1.Size = New System.Drawing.Size(206, 23)
        Me.MensualidadLabel1.TabIndex = 13
        Me.MensualidadLabel1.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(15, 100)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 15)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "Xxxx:"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(MensualidadLabel)
        Me.Panel1.Controls.Add(Me.MensualidadLabel1)
        Me.Panel1.Controls.Add(ContratacionLabel)
        Me.Panel1.Controls.Add(Me.ContratacionLabel1)
        Me.Panel1.Controls.Add(Me.NumeroLabel1)
        Me.Panel1.Controls.Add(PaqueteAdicionalLabel)
        Me.Panel1.Controls.Add(Me.PaqueteAdicionalLabel1)
        Me.Panel1.Controls.Add(Me.TipoCobroTextBox)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Location = New System.Drawing.Point(18, 400)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(271, 296)
        Me.Panel1.TabIndex = 0
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(9, 23)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(218, 16)
        Me.CMBLabel1.TabIndex = 0
        Me.CMBLabel1.Text = "Buscar Paquete Adicional por:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(15, 74)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(119, 15)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Nombre Paquete:"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(18, 92)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(244, 20)
        Me.TextBox1.TabIndex = 1
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Location = New System.Drawing.Point(3, 2)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button5)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.AutoScroll = True
        Me.SplitContainer1.Panel2.Controls.Add(Me.ConPaqueteAdicionalDataGridView)
        Me.SplitContainer1.Size = New System.Drawing.Size(805, 737)
        Me.SplitContainer1.SplitterDistance = 304
        Me.SplitContainer1.TabIndex = 0
        '
        'ConPaqueteAdicionalBindingSource
        '
        Me.ConPaqueteAdicionalBindingSource.DataMember = "ConPaqueteAdicional"
        Me.ConPaqueteAdicionalBindingSource.DataSource = Me.DataSetEric2
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric2"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Clv_PaqAdi"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Clave"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Clv_Tipo_Paquete_Adiocional"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Clv_Tipo_Paquete_Adiocional"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "TipoCobro"
        Me.DataGridViewTextBoxColumn3.HeaderText = "TipoCobro"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "PaqueteAdicional"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Paquete_Adicional"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 200
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Numero"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Numero"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'ConPaqueteAdicionalTableAdapter
        '
        Me.ConPaqueteAdicionalTableAdapter.ClearBeforeFill = True
        '
        'BrwPaqueteAdicional
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1016, 755)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Name = "BrwPaqueteAdicional"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Paquetes Adicionales"
        CType(Me.ConPaqueteAdicionalDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        CType(Me.ConPaqueteAdicionalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric2
    Friend WithEvents ConPaqueteAdicionalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConPaqueteAdicionalTableAdapter As sofTV.DataSetEric2TableAdapters.ConPaqueteAdicionalTableAdapter
    Friend WithEvents ConPaqueteAdicionalDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TipoCobroTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PaqueteAdicionalLabel1 As System.Windows.Forms.Label
    Friend WithEvents NumeroLabel1 As System.Windows.Forms.Label
    Friend WithEvents ContratacionLabel1 As System.Windows.Forms.Label
    Friend WithEvents MensualidadLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
End Class
