Imports System.Data.SqlClient
Public Class BRWCNRDIGX

    Private Sub BRWCNRDIGX_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'Me.BUSCACNRDIGTableAdapter.Connection = CON
        'Me.BUSCACNRDIGTableAdapter.Fill(Me.NewSofTvDataSet.BUSCACNRDIG, 0, 0, "", 0, 0, "", "01/01/1900", "01/01/1900", "01/01/1900", -1)
        BuscaCNRDigX(0, 0, "", 0, 0, "", "01/01/1900", "01/01/1900", "01/01/1900", -1)
        CON.Close()
    End Sub

    Private Sub BRWCNRDIGX_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            colorea(Me, Me.Name)
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            'Me.BUSCACNRDIGTableAdapter.Connection = CON
            'Me.BUSCACNRDIGTableAdapter.Fill(Me.NewSofTvDataSet.BUSCACNRDIG, 0, 0, "", 0, 0, "", "01/01/1900", "01/01/1900", "01/01/1900", -1)
            BuscaCNRDigX(0, 0, "", 0, 0, "", "01/01/1900", "01/01/1900", "01/01/1900", -1)
            CON.Close()


        Catch ex As Exception
            'MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub Busca(ByVal op As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If op = 0 Then
            '--Por Consecutivo
            If IsNumeric(Me.TextBox1.Text) = False Then Me.TextBox1.Text = 0
            'Me.BUSCACNRDIGTableAdapter.Connection = CON
            'Me.BUSCACNRDIGTableAdapter.Fill(Me.NewSofTvDataSet.BUSCACNRDIG, Me.TextBox1.Text, 0, "", 0, 0, "", "01/01/1900", "01/01/1900", "01/01/1900", op)
            BuscaCNRDigX(Me.TextBox1.Text, 0, "", 0, 0, "", "01/01/1900", "01/01/1900", "01/01/1900", op)
        ElseIf op = 1 Then
            '--Por numero_de_contrato
            If IsNumeric(Me.TextBox2.Text) = False Then Me.TextBox2.Text = 0
            'Me.BUSCACNRDIGTableAdapter.Connection = CON
            'Me.BUSCACNRDIGTableAdapter.Fill(Me.NewSofTvDataSet.BUSCACNRDIG, 0, Me.TextBox2.Text, "", 0, 0, "", "01/01/1900", "01/01/1900", "01/01/1900", op)
            BuscaCNRDigX(0, Me.TextBox2.Text, "", 0, 0, "", "01/01/1900", "01/01/1900", "01/01/1900", op)
        ElseIf op = 2 Then
            '--Por @mac_addres
            If Len(Trim(Me.TextBox3.Text)) = 0 Then Me.TextBox3.Text = ""
            'Me.BUSCACNRDIGTableAdapter.Connection = CON
            'Me.BUSCACNRDIGTableAdapter.Fill(Me.NewSofTvDataSet.BUSCACNRDIG, 0, 0, Me.TextBox3.Text, 0, 0, "", "01/01/1900", "01/01/1900", "01/01/1900", op)
            BuscaCNRDigX(0, 0, Me.TextBox3.Text, 0, 0, "", "01/01/1900", "01/01/1900", "01/01/1900", op)
        ElseIf op = 3 Then
            '--Por @resultado
            Dim Resultado As Integer = 0
            Resultado = CLng(Mid(Trim(Me.ComboBox1.Text), 1, 1))
            'Me.BUSCACNRDIGTableAdapter.Connection = CON
            'Me.BUSCACNRDIGTableAdapter.Fill(Me.NewSofTvDataSet.BUSCACNRDIG, 0, 0, "", Resultado, 0, "", "01/01/1900", "01/01/1900", "01/01/1900", op)
            BuscaCNRDigX(0, 0, "", Resultado, 0, "", "01/01/1900", "01/01/1900", "01/01/1900", op)
        ElseIf op = 4 Then
            '--Por @Clv_Orden
            If IsNumeric(Me.TextBox5.Text) = False Then Me.TextBox5.Text = 0
            'Me.BUSCACNRDIGTableAdapter.Connection = CON
            'Me.BUSCACNRDIGTableAdapter.Fill(Me.NewSofTvDataSet.BUSCACNRDIG, 0, 0, "", 0, Me.TextBox5.Text, "", "01/01/1900", "01/01/1900", "01/01/1900", op)
            BuscaCNRDigX(0, 0, "", 0, Me.TextBox5.Text, "", "01/01/1900", "01/01/1900", "01/01/1900", op)
        ElseIf op = 5 Then
            '--Por @Status
            Dim Status As String = "P"
            Status = CStr(Mid(Trim(Me.ComboBox2.Text), 1, 1))
            'Me.BUSCACNRDIGTableAdapter.Connection = CON
            'Me.BUSCACNRDIGTableAdapter.Fill(Me.NewSofTvDataSet.BUSCACNRDIG, 0, 0, "", 0, 0, Status, "01/01/1900", "01/01/1900", "01/01/1900", op)
            BuscaCNRDigX(0, 0, "", 0, 0, Status, "01/01/1900", "01/01/1900", "01/01/1900", op)
        ElseIf op = 6 Then
            '--Por @Fec_Sol
            If IsDate(Me.TextBox7.Text) = True Then
                'Me.BUSCACNRDIGTableAdapter.Connection = CON
                'Me.BUSCACNRDIGTableAdapter.Fill(Me.NewSofTvDataSet.BUSCACNRDIG, 0, 0, "", 0, 0, "", Me.TextBox7.Text, "01/01/1900", "01/01/1900", op)
                BuscaCNRDigX(0, 0, "", 0, 0, "", Me.TextBox7.Text, "01/01/1900", "01/01/1900", op)
            End If
        ElseIf op = 7 Then
            '--Por @Fec_Eje
            If IsDate(Me.TextBox8.Text) = True Then
                'Me.BUSCACNRDIGTableAdapter.Connection = CON
                'Me.BUSCACNRDIGTableAdapter.Fill(Me.NewSofTvDataSet.BUSCACNRDIG, 0, 0, "", 0, 0, "", "01/01/1900", Me.TextBox8.Text, "01/01/1900", op)
                BuscaCNRDigX(0, 0, "", 0, 0, "", "01/01/1900", Me.TextBox8.Text, "01/01/1900", op)
            End If
        ElseIf op = 8 Then
            '--Por @Fecha_habilitar
            If IsDate(Me.TextBox4.Text) = True Then
                'Me.BUSCACNRDIGTableAdapter.Connection = CON
                'Me.BUSCACNRDIGTableAdapter.Fill(Me.NewSofTvDataSet.BUSCACNRDIG, 0, 0, "", 0, 0, "", "01/01/1900", "01/01/1900", Me.TextBox4.Text, op)
                BuscaCNRDigX(0, 0, "", 0, 0, "", "01/01/1900", "01/01/1900", Me.TextBox4.Text, op)
            End If
        Else
            'Me.BUSCACNRDIGTableAdapter.Connection = CON
            'Me.BUSCACNRDIGTableAdapter.Fill(Me.NewSofTvDataSet.BUSCACNRDIG, 0, 0, "", 0, 0, "", "01/01/1900", "01/01/1900", "01/01/1900", -1)
            BuscaCNRDigX(0, 0, "", 0, 0, "", "01/01/1900", "01/01/1900", "01/01/1900", -1)
        End If
        Me.TextBox1.Text = ""
        Me.TextBox2.Text = ""
        Me.TextBox3.Text = ""
        Me.TextBox4.Text = ""
        Me.TextBox5.Text = ""
        Me.TextBox7.Text = ""
        Me.TextBox8.Text = ""
        Me.ComboBox1.Text = ""
        Me.ComboBox2.Text = ""
        CON.Close()
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Busca(0)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Busca(1)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Busca(2)
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Busca(3)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Busca(4)
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Busca(5)
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Busca(6)
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        Busca(7)
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        Busca(8)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(0)
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
        End If
    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
        End If
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Busca(3)
    End Sub


    Private Sub TextBox5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox5.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(4)
        End If
    End Sub
    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        Busca(5)
    End Sub

    Private Sub TextBox7_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox7.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(6)
        End If
    End Sub
    Private Sub TextBox8_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox8.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(7)
        End If
    End Sub


    Private Sub TextBox4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox4.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(8)
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        eOpcion = "N"
        FrmCNRDigX.Show()
    End Sub

    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click
        If Me.DataGridView1.RowCount > 0 Then
            eOpcion = "C"
            eConsecutivo = Me.ConsecutivoLabel1.Text
            FrmCNRDigX.Show()
        Else
            MsgBox(mensaje2)
        End If
    End Sub

    Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button13.Click
        If Me.DataGridView1.RowCount > 0 Then
            eOpcion = "M"
            eConsecutivo = Me.ConsecutivoLabel1.Text
            FrmCNRDigX.Show()
        Else
            MsgBox(mensaje1)
        End If
    End Sub

    Private Sub BuscaCNRDigX(ByVal prmConse As Integer, ByVal prmContrato As Integer, ByVal prmResul As String, ByVal prmMac As Integer, ByVal prmOrden As Integer, _
                                   ByVal prmStatus As String, ByVal prmFecsol As String, ByVal prmFecEje As String, ByVal prmfecHab As String, ByVal prmOp As Integer)

        Try
            Dim CNRDIGX As New BaseIII


            CNRDIGX.limpiaParametros()
            CNRDIGX.CreateMyParameter("@Consecutivo", SqlDbType.Int, prmConse)
            CNRDIGX.CreateMyParameter("@Numero_de_Contrato", SqlDbType.Int, prmContrato)
            CNRDIGX.CreateMyParameter("@mac_addres", SqlDbType.BigInt, prmMac)
            CNRDIGX.CreateMyParameter("@resultado", SqlDbType.VarChar, prmResul, 250)
            CNRDIGX.CreateMyParameter("@Clv_orden", SqlDbType.BigInt, prmOrden)
            CNRDIGX.CreateMyParameter("@Status", SqlDbType.VarChar, prmStatus, 1)
            CNRDIGX.CreateMyParameter("@Fec_sol", SqlDbType.DateTime, prmFecsol)
            CNRDIGX.CreateMyParameter("@Fec_Eje", SqlDbType.DateTime, prmFecEje)
            CNRDIGX.CreateMyParameter("@Fecha_habilitar", SqlDbType.DateTime, prmfecHab)
            CNRDIGX.CreateMyParameter("@Op", SqlDbType.Int, prmOp)
            Me.DataGridView1.DataSource = CNRDIGX.ConsultaDT("BUSCACNRDIGX")

            If Me.DataGridView1.RowCount > 0 Then
                Me.ConsecutivoLabel1.Text = CStr(Me.DataGridView1.SelectedCells(0).Value)
                Me.Numero_de_contratoLabel1.Text = CStr(Me.DataGridView1.SelectedCells(1).Value)
                Me.Mac_addresLabel1.Text = CStr(Me.DataGridView1.SelectedCells(3).Value)
                Me.c.Text = CStr(Me.DataGridView1.SelectedCells(4).Value)
                Me.ComandoLabel1.Text = CStr(Me.DataGridView1.SelectedCells(5).Value)
                Me.ResultadoLabel1.Text = CStr(Me.DataGridView1.SelectedCells(6).Value)
                Me.Descripcion_transaccionLabel1.Text = CStr(Me.DataGridView1.SelectedCells(7).Value)
                Me.Clv_OrdenLabel1.Text = CStr(Me.DataGridView1.SelectedCells(8).Value)
                Me.Fec_SolLabel1.Text = CStr(Me.DataGridView1.SelectedCells(9).Value)
                Me.Fec_EjeLabel1.Text = CStr(Me.DataGridView1.SelectedCells(10).Value)
                Me.Label13.Text = CStr(Me.DataGridView1.SelectedCells(12).Value)
                Me.StatusLabel1.Text = CStr(Me.DataGridView1.SelectedCells(11).Value)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub DataGridView1_CurrentCellChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.CurrentCellChanged
        Try
            If Me.DataGridView1.RowCount > 0 Then
                Me.ConsecutivoLabel1.Text = CStr(Me.DataGridView1.SelectedCells(0).Value)
                Me.Numero_de_contratoLabel1.Text = CStr(Me.DataGridView1.SelectedCells(1).Value)
                Me.Mac_addresLabel1.Text = CStr(Me.DataGridView1.SelectedCells(3).Value)
                Me.c.Text = CStr(Me.DataGridView1.SelectedCells(4).Value)
                Me.ComandoLabel1.Text = CStr(Me.DataGridView1.SelectedCells(5).Value)
                Me.ResultadoLabel1.Text = CStr(Me.DataGridView1.SelectedCells(6).Value)
                Me.Descripcion_transaccionLabel1.Text = CStr(Me.DataGridView1.SelectedCells(7).Value)
                Me.Clv_OrdenLabel1.Text = CStr(Me.DataGridView1.SelectedCells(8).Value)
                Me.Fec_SolLabel1.Text = CStr(Me.DataGridView1.SelectedCells(9).Value)
                Me.Fec_EjeLabel1.Text = CStr(Me.DataGridView1.SelectedCells(10).Value)
                Me.Label13.Text = CStr(Me.DataGridView1.SelectedCells(12).Value)
                Me.StatusLabel1.Text = CStr(Me.DataGridView1.SelectedCells(11).Value)
            End If
        Catch ex As Exception
        End Try
    End Sub
End Class