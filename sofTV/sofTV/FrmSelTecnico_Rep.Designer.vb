<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelTecnico_Rep
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.CMBLABEL = New System.Windows.Forms.Label()
        Me.CMB2Label1 = New System.Windows.Forms.Label()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ListBox2 = New System.Windows.Forms.ListBox()
        Me.MuestraSeleccionTecnicosConsultaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.MuestraSeleccionTecnicostmpConsultaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Muestra_Seleccion_Tecnicostmp_ConsultaTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_Seleccion_Tecnicostmp_ConsultaTableAdapter()
        Me.Muestra_Seleccion_Tecnicos_ConsultaTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_Seleccion_Tecnicos_ConsultaTableAdapter()
        Me.Muestra_Seleccion_Tecnicostmp_NuevoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Muestra_Seleccion_Tecnicostmp_NuevoTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_Seleccion_Tecnicostmp_NuevoTableAdapter()
        Me.Insertauno_Seleccion_TecnicoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Insertauno_Seleccion_TecnicoTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Insertauno_Seleccion_TecnicoTableAdapter()
        Me.Insertauno_Seleccion_Tecnico_tmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Insertauno_Seleccion_Tecnico_tmpTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Insertauno_Seleccion_Tecnico_tmpTableAdapter()
        Me.InsertaTOSeleccion_TecnicoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertaTOSeleccion_TecnicoTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.InsertaTOSeleccion_TecnicoTableAdapter()
        Me.InsertaTOSeleccion_Tecnico_tmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertaTOSeleccion_Tecnico_tmpTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.InsertaTOSeleccion_Tecnico_tmpTableAdapter()
        CType(Me.MuestraSeleccionTecnicosConsultaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraSeleccionTecnicostmpConsultaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Muestra_Seleccion_Tecnicostmp_NuevoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Insertauno_Seleccion_TecnicoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Insertauno_Seleccion_Tecnico_tmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertaTOSeleccion_TecnicoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertaTOSeleccion_Tecnico_tmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBLABEL
        '
        Me.CMBLABEL.AutoSize = True
        Me.CMBLABEL.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLABEL.ForeColor = System.Drawing.Color.Brown
        Me.CMBLABEL.Location = New System.Drawing.Point(366, 9)
        Me.CMBLABEL.Name = "CMBLABEL"
        Me.CMBLABEL.Size = New System.Drawing.Size(184, 16)
        Me.CMBLABEL.TabIndex = 41
        Me.CMBLABEL.Text = "Técnicos Seleccionados:"
        '
        'CMB2Label1
        '
        Me.CMB2Label1.AutoSize = True
        Me.CMB2Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMB2Label1.ForeColor = System.Drawing.Color.Brown
        Me.CMB2Label1.Location = New System.Drawing.Point(12, 9)
        Me.CMB2Label1.Name = "CMB2Label1"
        Me.CMB2Label1.Size = New System.Drawing.Size(199, 16)
        Me.CMB2Label1.TabIndex = 40
        Me.CMB2Label1.Text = "Técnicos para Seleccionar:"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(396, 351)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 36)
        Me.Button5.TabIndex = 37
        Me.Button5.Text = "&Cancelar"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkOrange
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(142, 351)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(136, 36)
        Me.Button6.TabIndex = 36
        Me.Button6.Text = "&Aceptar"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkOrange
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(279, 210)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 35
        Me.Button4.Text = "<<"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(279, 181)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 34
        Me.Button3.Text = "<"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(279, 113)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 33
        Me.Button2.Text = ">>"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(279, 84)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 32
        Me.Button1.Text = ">"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'ListBox2
        '
        Me.ListBox2.DataSource = Me.MuestraSeleccionTecnicosConsultaBindingSource
        Me.ListBox2.DisplayMember = "Nombre"
        Me.ListBox2.FormattingEnabled = True
        Me.ListBox2.Location = New System.Drawing.Point(369, 33)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.ScrollAlwaysVisible = True
        Me.ListBox2.Size = New System.Drawing.Size(248, 290)
        Me.ListBox2.TabIndex = 39
        Me.ListBox2.TabStop = False
        Me.ListBox2.ValueMember = "clv_tecnico"
        '
        'MuestraSeleccionTecnicosConsultaBindingSource
        '
        Me.MuestraSeleccionTecnicosConsultaBindingSource.DataMember = "Muestra_Seleccion_Tecnicos_Consulta"
        Me.MuestraSeleccionTecnicosConsultaBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ListBox1
        '
        Me.ListBox1.DataSource = Me.MuestraSeleccionTecnicostmpConsultaBindingSource
        Me.ListBox1.DisplayMember = "Nombre"
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Location = New System.Drawing.Point(15, 33)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.ScrollAlwaysVisible = True
        Me.ListBox1.Size = New System.Drawing.Size(241, 290)
        Me.ListBox1.TabIndex = 38
        Me.ListBox1.TabStop = False
        Me.ListBox1.ValueMember = "clv_tecnico"
        '
        'MuestraSeleccionTecnicostmpConsultaBindingSource
        '
        Me.MuestraSeleccionTecnicostmpConsultaBindingSource.DataMember = "Muestra_Seleccion_Tecnicostmp_Consulta"
        Me.MuestraSeleccionTecnicostmpConsultaBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Muestra_Seleccion_Tecnicostmp_ConsultaTableAdapter
        '
        Me.Muestra_Seleccion_Tecnicostmp_ConsultaTableAdapter.ClearBeforeFill = True
        '
        'Muestra_Seleccion_Tecnicos_ConsultaTableAdapter
        '
        Me.Muestra_Seleccion_Tecnicos_ConsultaTableAdapter.ClearBeforeFill = True
        '
        'Muestra_Seleccion_Tecnicostmp_NuevoBindingSource
        '
        Me.Muestra_Seleccion_Tecnicostmp_NuevoBindingSource.DataMember = "Muestra_Seleccion_Tecnicostmp_Nuevo"
        Me.Muestra_Seleccion_Tecnicostmp_NuevoBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Muestra_Seleccion_Tecnicostmp_NuevoTableAdapter
        '
        Me.Muestra_Seleccion_Tecnicostmp_NuevoTableAdapter.ClearBeforeFill = True
        '
        'Insertauno_Seleccion_TecnicoBindingSource
        '
        Me.Insertauno_Seleccion_TecnicoBindingSource.DataMember = "Insertauno_Seleccion_Tecnico"
        Me.Insertauno_Seleccion_TecnicoBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Insertauno_Seleccion_TecnicoTableAdapter
        '
        Me.Insertauno_Seleccion_TecnicoTableAdapter.ClearBeforeFill = True
        '
        'Insertauno_Seleccion_Tecnico_tmpBindingSource
        '
        Me.Insertauno_Seleccion_Tecnico_tmpBindingSource.DataMember = "Insertauno_Seleccion_Tecnico_tmp"
        Me.Insertauno_Seleccion_Tecnico_tmpBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Insertauno_Seleccion_Tecnico_tmpTableAdapter
        '
        Me.Insertauno_Seleccion_Tecnico_tmpTableAdapter.ClearBeforeFill = True
        '
        'InsertaTOSeleccion_TecnicoBindingSource
        '
        Me.InsertaTOSeleccion_TecnicoBindingSource.DataMember = "InsertaTOSeleccion_Tecnico"
        Me.InsertaTOSeleccion_TecnicoBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'InsertaTOSeleccion_TecnicoTableAdapter
        '
        Me.InsertaTOSeleccion_TecnicoTableAdapter.ClearBeforeFill = True
        '
        'InsertaTOSeleccion_Tecnico_tmpBindingSource
        '
        Me.InsertaTOSeleccion_Tecnico_tmpBindingSource.DataMember = "InsertaTOSeleccion_Tecnico_tmp"
        Me.InsertaTOSeleccion_Tecnico_tmpBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'InsertaTOSeleccion_Tecnico_tmpTableAdapter
        '
        Me.InsertaTOSeleccion_Tecnico_tmpTableAdapter.ClearBeforeFill = True
        '
        'FrmSelTecnico_Rep
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(629, 408)
        Me.Controls.Add(Me.CMBLABEL)
        Me.Controls.Add(Me.CMB2Label1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ListBox2)
        Me.Controls.Add(Me.ListBox1)
        Me.Name = "FrmSelTecnico_Rep"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Seleccion Tecnicos Reportes"
        CType(Me.MuestraSeleccionTecnicosConsultaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraSeleccionTecnicostmpConsultaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Muestra_Seleccion_Tecnicostmp_NuevoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Insertauno_Seleccion_TecnicoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Insertauno_Seleccion_Tecnico_tmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertaTOSeleccion_TecnicoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertaTOSeleccion_Tecnico_tmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBLABEL As System.Windows.Forms.Label
    Friend WithEvents CMB2Label1 As System.Windows.Forms.Label
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents MuestraSeleccionTecnicosConsultaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents MuestraSeleccionTecnicostmpConsultaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Seleccion_Tecnicostmp_ConsultaTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_Seleccion_Tecnicostmp_ConsultaTableAdapter
    Friend WithEvents Muestra_Seleccion_Tecnicos_ConsultaTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_Seleccion_Tecnicos_ConsultaTableAdapter
    Friend WithEvents Muestra_Seleccion_Tecnicostmp_NuevoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Seleccion_Tecnicostmp_NuevoTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_Seleccion_Tecnicostmp_NuevoTableAdapter
    Friend WithEvents Insertauno_Seleccion_TecnicoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Insertauno_Seleccion_TecnicoTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Insertauno_Seleccion_TecnicoTableAdapter
    Friend WithEvents Insertauno_Seleccion_Tecnico_tmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Insertauno_Seleccion_Tecnico_tmpTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Insertauno_Seleccion_Tecnico_tmpTableAdapter
    Friend WithEvents InsertaTOSeleccion_TecnicoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertaTOSeleccion_TecnicoTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.InsertaTOSeleccion_TecnicoTableAdapter
    Friend WithEvents InsertaTOSeleccion_Tecnico_tmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertaTOSeleccion_Tecnico_tmpTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.InsertaTOSeleccion_Tecnico_tmpTableAdapter
End Class
