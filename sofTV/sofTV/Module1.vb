Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports sofTV.Base
Imports System.IO
Imports CrystalDecisions.Shared

Module Module1
    Public GloClvCompania As Integer = 0
    Public GloClvGrupo As Integer = 0
    'Edgar 09/08/2011 Reporte Mezclas de Servicios
    Public GloMezclaMes1 As Integer = 0
    Public GloMezclaAno1 As Integer = 0
    Public GloMezclaMes2 As Integer = 0
    Public GloMezclaAno2 As Integer = 0
    Public GloeStatus As String = "P"
    '
    Public GloeClv_Orden As Long = 0
    Public GloeClave As Long = 0
    Public GloeTrabajo As String = Nothing
    Public GloeClv_Trabajo As Integer = 0
    Public GloeOp As Integer = 0
    '
    Public GloSeries As String = Nothing
    Public Bnd_Orden As Boolean = False
    Public opcionarea As Integer = 0
    Public Rpt_ProductividadTecnico As Boolean = False
    Public Rpt_ProductividadTecnicoTipoTrabajo As String = ""
    Public VISIBLE_IEPS As Boolean = False
    Public sms As Boolean = False
    Public ComentarioAgenda As String = Nothing
    Public Bnprocesa As Boolean = False
    Public MiConexion As String = Nothing
    Public LocOpTel As String = Nothing
    Public LocDescrip As String = Nothing
    Public LocClv_Tel As Long = Nothing
    Public LocBndTel As Boolean = False
    Public LocTipoPaquerebnd As Boolean = False
    Public LocGloTipoPaquete As Long = 0
    Public locbndrepcancelaciones As Boolean = False
    Public locMorosos As Integer = Nothing
    Public locMorososConSaldo As Integer = Nothing
    Public LocOpTipoPaqAdic As String = Nothing
    Public Locbndrepcontspago As Boolean = False
    Public LOCBNDREPDATOSBANC As Boolean = False
    Public ImpresoraContatos As String = Nothing
    Public LocMontoEsp As String = Nothing
    Public LocFechaesp As String = Nothing
    Public LocContratoesp As String = Nothing
    Public Locclv_cobro As Integer = 0
    Public Locbndactualiza As Boolean = False
    Public LocOpCargosEsp As String = Nothing
    Public Locbndcontrato3 As Boolean = False
    Public locpagosespeciales As Boolean = False
    Public locbndreportbit As Boolean = False
    Public loc1op As Integer = 0
    Public Loc1contrato As Integer = 0
    Public Loc1clv_sistema As String = Nothing
    Public Loc1Fechaini As String = Nothing
    Public Loc1Fechafin As String = Nothing
    Public Loc1clv_usuario As String = Nothing
    Public Loc1clv_pantalla As String = Nothing
    Public Loc1clv_control As String = Nothing
    Public Loc1horaini As String = Nothing
    Public Loc1horafin As String = Nothing
    Public LocbndProceso1 As Boolean = False
    Public Locbndvalcliente As Boolean = False
    Public Locbndcontrato1 As Boolean = False
    Public Locbndcontrato2 As Boolean = False
    Public Glocontratoini As Integer = 0
    Public Glocontratofin As Integer = 0
    Public Locformulario As Integer = 0
    Public LocopPoliza As String = Nothing
    Public LocbndPolizaCiudad As Boolean = False
    Public LocbndPolizaCiudad2 As Boolean = False
    Public Locbndactualizapoiza As Boolean = False
    Public LocGloClv_poliza As Integer = 0
    'Variables para el filtro de Agenda del T�cnico
    Public LocFecha1Agnd As String = Nothing
    Public LocFecha2Agnd As String = Nothing
    Public LocTurnoAgnd As String = ""
    'Fin de Variables para el filtro de Agenda del T�cnico
    Public LocClv_tecnico As Integer = 0
    Public LocopAgnd As Integer = 0
    Public Locbndagnd As Boolean = False
    Public LocbndProceso As Boolean = False
    Public LocProceso As Integer = 0
    Public LocBndFiltro1 As Integer = 0
    Public LocBndFiltro2 As Integer = 0
    Public TipoPersona As Integer
    Public clave_periodo As Integer = 0
    Public opcion_periodo As Integer = 0
    Public OpcionReporteLlamada As Integer = 0
    Public TipoServico_ReporteLlamada As Integer = 0
    Public Clave_Motivo As Integer = 0
    Public Fecha1 As DateTime
    Public fecha2 As DateTime
    Public opcionPeriodo As Integer
    Public eBndVendedorTecnico As Boolean = False
    Public eClaveVendedorTecnico As Integer = 0
    Public eNombreVendedorTecnico As String = Nothing

    Public BanderaAreaQuejas As Boolean = False
    Public BanderaAreaTecnica As Boolean = False

    Public eGloFecha As String = Nothing
    Public eGloHora As String = Nothing
    'Banderas de las autorizaciones cuando los datos de los Clientes se encuentren repetidos en la Base de Datos
    Public eBndAutorizacionNombreRepetido As Boolean = False
    Public eBndAutorizacionDireccionRepetido As Boolean = False
    Public eBndAutorizacionIFERepetido As Boolean = False
    '----------------------------------------------------
    Public eBndEntraNombre As Boolean = False
    Public eGloFecha_sol As String = Nothing
    Public eGloHora_sol As String = Nothing
    Public eGloFecha_eje As String = Nothing
    Public eGloHora_eje As String = Nothing

    'JUANJO FECHAS Y HORAS VSITAS ORDENES Y QUEJAS <INICIO>
    Public ordenGloFechaVisita1 As String = Nothing
    Public ordenGloFechaVisita2 As String = Nothing
    Public ordenGloFechaVisita3 As String = Nothing
    Public ordenGloHoraVisita1 As String = Nothing
    Public ordenGloHoraVisita2 As String = Nothing
    Public ordenGloHoraVisita3 As String = Nothing

    Public quejaGloFechaVisita1 As String = Nothing
    Public quejaGloFechaVisita2 As String = Nothing
    Public quejaGloFechaVisita3 As String = Nothing
    Public quejaGloHoraVisita1 As String = Nothing
    Public quejaGloHoraVisita2 As String = Nothing
    Public quejaGloHoraVisita3 As String = Nothing

    Public quejaGloFechaEnProceso As String = Nothing
    Public quejaGloHoraEnProceso As String = Nothing
    'JUANJO FECHAS Y HORAS VSITAS ORDENES Y QUEJAS <FIN>

    Public LocOpVisita As String = Nothing
    Public Locclv_visita As Integer = 0
    Public LocBndVisitas As Boolean = False
    Public RutaReportes As String
    Public glotiposervicioppal As Integer
    Public GloTipoCablemodem As Integer
    Public GloClvUsuario As Integer
    Public GloTickets As Boolean = False
    Public IdSistema As String = Nothing
    Public eGuardarCliente As Boolean = False
    Public LocFechaTec1 As Date = DateValue("01/01/1900")
    Public LocFechaTec2 As Date = DateValue("01/01/1900")
    Public LocTipRepTec As Integer = 0
    Public bndreportetec As Boolean = False
    Public eImprimeContrato As Boolean
    Public eErrorContrato As Integer
    Public eContrato As Long
    Public eClvMotivo As Long
    Public eReImpresion As Integer = 0
    Public eClv_Servicio As Integer = 0
    Public eCveRango As Integer = 0
    Public bndCCABM As Boolean = False
    Public eCveComision As Integer = 0
    Public eRangoInferior As Integer = 0
    Public eRangoSuperior As Integer = 0
    Public ePrecio As Single = 0
    Public eOpcion As Char = Nothing
    Public eValidaRango As Integer = 0
    Public eValidaRangoComision As Integer = 0
    Public eFechaIni As Date = Today
    Public eFechaFin As Date = Today
    Public eClv_Vendedor As Long = 0
    Public eOp As Integer = 0
    Public eRInferior As Integer = 0
    Public eRSuperior As Integer = 0
    Public eTituloComision As String = Nothing
    Public eOpVentas As Integer = 0
    Public gloClvTipSerReportes As Integer = 0
    Public gloClvTipSerOrdenes As Integer = 0
    Public eClv_Session As Long = 0
    Public eCont As Integer = 0
    Public eInst As Integer = 0
    Public eDesc As Integer = 0
    Public eSusp As Integer = 0
    Public eBaja As Integer = 0
    Public eFuera As Integer = 0
    Public eTempo As Integer = 0
    Public eTipSer As Integer = 0
    Public eServicio As String = Nothing
    Public eClv_Unica As Integer = 0
    Public eConsecutivo As Integer = 0
    Public eClv_Encargado As Integer = 0
    Public eStatusOrdSer As Char = Nothing
    Public eActTecnico As Boolean = True
    Public eEntraUM As Boolean = False
    Public eEntraUMB As Boolean = False
    Public eRespuesta As Integer = 0
    Public eCabModPropio As Boolean = False
    Public eGloContrato As Long = 0
    Public eGloContratoAux As Long = 0
    Public eGloSubContrato As Long = 0
    Public eGloSubContratoAux As Long = 0
    Public eGloClvSession As Long = 0
    Public eResAco As Integer = 0
    Public eBndReportePPE As Boolean = False
    Public eRobo As Boolean = False
    Public eEntraMaestro As Boolean = False
    Public eAccesoAdmin As Boolean = False
    Public eClv_Usuario As Integer = 0
    Public eOpPPE As Integer = 0
    Public eClv_Sector As Long = 0
    Public eConsec As Long = 0
    Public eContratoRec As Long = 0
    Public eClv_UnicaNetDig As Long = 0
    Public eClv_UnicaNet As Long = 0
    Public eGloDescuento As Integer = 0
    Public eGloTipSerDesc As Integer = 0
    Public eBndDesc As Boolean = False
    Public GloCiudad As String = Nothing
    Public GloClv_MotCan As Integer = 0
    Public eBndMenIns As Boolean = False
    Public eBndGraf As Boolean = False
    Public eClv_Sucursal As Integer = 0
    Public eNombre As String = Nothing
    Public eApellidoPaterno As String = Nothing
    Public eApellidoMaterno As String = Nothing
    Public eBndVen As Boolean = False
    Public eClv_MotAtenTel As Integer = 0
    Public eClave As Long = 0
    Public eMesIni As Integer = 0
    Public eMesFin As Integer = 0
    Public eAnioIni As Integer = 0
    Public eAnioFin As Integer = 0
    Public eStrMesIni As String = Nothing
    Public eStrMesFin As String = Nothing
    Public eBndOpVentas As Boolean = False
    Public eBndContratoF As Boolean = False
    Public eClv_Grupo As Integer = 0
    Public eGrupo As String = Nothing
    Public eBndCombo As Boolean = False
    Public eClv_Descuento As Integer = 0
    Public eNombreCombo As String = Nothing
    Public eClv_TipoCliente As Integer = 0
    Public eClv_PaqAdi As Integer = 0
    Public eBndPaqAdi As Boolean = False
    Public eBndAtenTelGraf As Boolean = False
    Public eOP1 As Integer = 0
    Public eOP2 As Integer = 0
    Public eOP3 As Integer = 0
    Public eOpIrdeto As Integer = 0
    Public eBndIrdeto As Boolean = False
    Public eOpContratoF As Integer = 0
    Public eClaveCorreo As Long = 0
    Public eOpCorreo As Integer = 0
    Public eClv_Tipo_Paquete_Adicional As Long = 0
    Public eOpCNRCNRDIG As Integer = 0
    Public eBndClv_CablemodemSelPropio As Boolean = False
    Public eBndMismoNombre As Boolean = False
    Public eBndEntraDire As Boolean = False
    Public eClavePorRecuperar As Long = 0
    Public eBndPorRecuperar As Boolean = False
    Public eBndCargoEsp As Boolean = False
    Public eBndBonifEsp As Boolean = False
    Public eClv_TipSer As Integer = 0
    Public eRealizado As String = Nothing
    Public eTipo As String = String.Empty
    Public eBndComisiones As Boolean = False
    Public eClv_Rango As Integer = 0
    Public eTitulo As String = String.Empty
    Public eSeVende As Boolean = False
    Public ePropio As Boolean = False
    Public eIdPaquete As Integer = 0
    Public eRes As Integer = 0
    Public eMsj As String = 0
    Public eIFE As String = String.Empty
    '-----------------------------------------
    Public LocGloOpRep As Integer = 0
    Public LocTarjNo_Contrato As Integer = 0
    Public GloTipo As String = Nothing
    Public GloContrato As Long = 0
    Public GloOpFacturas As Integer = 0
    'Public eRes As Integer
    Public eResString As Char
    Public opcFrm As Integer
    Public GloOpEtiqueta As String = "0"
    Public GloBndEtiqueta As Boolean = False
    Public GloClv_tipser2 As Integer
    Public GloClv_TSer As Integer
    Public Locorden As Integer = 0
    Public LocServicios As Boolean = False
    Public LocBndC As Boolean = False
    Public LocBndB As Boolean = False
    Public LocBndI As Boolean = False
    Public LocBndD As Boolean = False
    Public LocBndS As Boolean = False
    Public LocBndF As Boolean = False
    Public LocBndDT As Boolean = False
    Public LocBndRepMix As Boolean = False
    Public oprepetiq As Integer = 0
    Public LocClv_session As Integer
    Public Locclv_TipSer As Integer

    Public LocTipoClientes As Boolean = False
    Public LocOp As Integer
    Public LocMasivaCapar As Boolean = False
    Public GloPermisoCortesia As Integer = 0
    Public GloOpPermiso As Integer = 0
    Public GloCmdBanco As Integer = 0
    Public Locclv_tec As Integer = 0
    Public clv_sessionTecnico As Integer = 0
    Public LocValida1 As Boolean = False
    Public Locclv_folio As Integer = 0
    Public LocNo_Bitacora As Integer = 0
    Public GloContratoNet2 As Long = 0
    Public GloClvUnicaNet As Long = 0
    Public gLOVERgUARDA As Integer = 0
    '
    Public Locclv_txt As String = Nothing
    Public LocDescr As String = Nothing
    '
    Public GloOpRep As String
    Public GloBndSelBanco As Boolean = False
    Public GloSelBanco As Integer
    Public OpcionQuejaOrden As String

    Public glolec As Short
    Public gloescr As Short
    Public gloctr As Short
    Public GloTipoUsuario As Integer
    Public oOpcionMenu As ToolStripMenuItem
    '

    Public InstalaParcial As Boolean = False

    Public GloControlaReloj As Integer = 0
    Public GloBloqueaDetalle As Boolean = False
    Public GloSucursal As String = Nothing ' Sucursal : Aguascalientes,Ags. "
    '
    Public GloClv_UnicaDig_Nuevo As Long = 0
    Public GloContratoDig_Nuevo As Long = 0
    Public GloGuardarDig As Boolean = False
    '
    Public GloClv_UnicaNet_Nuevo As Long = 0
    Public GloContratonet_Nuevo As Long = 0
    Public GloGuardarNet As Boolean = False
    Public GloTrabajo_OrdSer As String
    Public Bloquea As Boolean = False
    Public GloBndTrabajo As Boolean = False
    Public GloObs_OrdSer As String
    Public GloSeRealiza_OrdSer As Boolean = True
    Public GloClv_Trabajo_OrdSer As Integer
    Public GloBndClv_CablemodemSel As Boolean = False
    Public GloClv_CablemodemSel As Long = 0
    Public GloMacCablemodemSel As String = Nothing
    Public GLOTRABAJO As String = Nothing
    Public gloClv_Orden As Long = 0
    Public GloDetClave As Long = 0
    Public GloBndTipSer As Boolean = False
    Public GloContratoVer As Long = 0
    Public GloGuardo As Boolean = False
    Public Clv_HoraAgenda As Long
    Public FechaAgenda As String = Nothing
    Public HoraAgenda As String = Nothing
    Public TurnoAgenda As String = Nothing
    Public Contrato As Long = 0
    Public GLOCONTRATOSEL As Long = 0
    Public GLOCONTRATOSEL_agenda As Long = 0
    Public GloClv_Cablemodem As Integer = 0
    Public OpcionCli As Char = Nothing
    Public GloClv_TipSer As Integer = 0
    Public GloNom_TipSer As String = Nothing
    Public GloClv_Servicio As Integer = 0
    Public GloContratonet As Long = 0
    Public Pantalla As String = Nothing
    Public LoContratonet As Long = 0
    Public LoClv_Unicanet As Long = 0
    Public GLOMOVNET As Integer = 0
    Public GLOMOVDIG As Integer = 0
    Public opcion As Char = Nothing
    Public GloClv_Calle As Integer = 0
    Public GloClv_COLONIA As Integer = 0
    Public GloClv_tecnico As Integer = 0
    Public GLONOM_TECNICO As String = Nothing
    Public GloBnd As Boolean
    Public opcionAgenda As Char = "N"
    Public GloClv_TipoServicio As Integer = 0
    Public GloGClv_Servicio As Integer = 0
    Public GloTipoAparato As Char = Nothing
    Public GloGClv_Cablemodem As Integer = 0
    Public gloClave As Integer = 0
    Public opcionprimera As Integer = 0

    '--Variable para Control de los Periodos en las Carteras
    Public gloPorClv_Periodo As Integer = 0
    '--Variables de Telefonia
    Public GlobndGuardaTelefonia As Boolean = False
    Public gLONET_1 As Integer = 0
    Public gLOTEL_1 As Integer = 0
    '--Variable de Combos
    Public Glo_Combo_Tipo As Integer = 0
    Public Servicio_Principal As Integer = 0
    Public HAB_TV As Integer = 1
    Public HAB_TVDIG As Integer = 1
    Public HAB_INTERNET As Integer = 1
    Public HAB_Telefonia As Integer = 1
    Public HAB_TVDIGCable As Integer = 1
    Public Var_reporteTiempo As Integer = 0

    '--Que Servicios contratto en combo
    Public Tiene_Internet As Integer = 0
    Public Tiene_Tv As Integer = 0
    Public Tiene_Dig As Integer = 0
    Public Tiene_Tel As Integer = 0
    '
    Public CliTiene_Internet As Integer = 0
    Public CliTiene_Tv As Integer = 0
    Public CliTiene_Dig As Integer = 0
    Public CliTiene_Tel As Integer = 0
    '
    Public CLITVSINPAGO As Integer = 0
    Public CLITVCONPAGO As Integer = 0
    Public CAJASDIG As Integer = 0
    Public GloClv_Combo As Integer = Nothing
    Public Tipo As Integer = Nothing
    '
    Public ContratoCombo As Boolean = False
    Public eResValida As Integer = 0
    Public eMsgValida As String = Nothing
    Public rvalida As Boolean = False

    Public Locclv_Alm As Integer = 0

    Public BndAsignacion As Boolean = False
    Public Bnd_CAPAR As Boolean = False

    'JUANJO COSTOS PAGARE�S
    Public OpPagare As String = Nothing
    Public IdPagare As Integer = 0
    Public BndActualizaPagare As Boolean = False
    Public OpICAJA As String = Nothing
    Public ContratoCajasDig As Integer = Nothing
    Public GloClvCajaDig As String = Nothing
    Public ContratoNetCajasDig As Integer = 0
    Public ContratoNetDetCajas As Integer = 0
    Public BNDCAJAS As Boolean = False
    Public BndGuardaCajas As Boolean = False
    Public BndEliminar As Boolean = False
    'JUANJO COSTOS PAGARE�S (FIN)

    'JUANJO ORDENES
    Public BNDORDACOMETIDA As Boolean = False
    Public TieneAcometida As Boolean = False

    '---------------------------------------------------------------
    'JUAN PABLO CONSULTAS
    Public consultas As New CBase
    Public _DataSet As DataSet = Nothing
    Public _DataTable As DataTable = Nothing
    '---------------------------------------------------------------

    'SAUL REPORTE TRABAJOS
    Public GloRepTecnicos As Integer = Nothing
    '----------------------------------------------------------------
    'SAUL Impresora de Ordenes
    Public ImpresoraOrd As String
    '----------------------------------------------------------------
    'SAUL Ticket
    Public GloCity As String = Nothing
    '----------------------------------------------------------------
    'Reporte Global
    Public rDocument As New ReportDocument()

    Public Function USPDameDetOrserClave(ByVal eClv_Orden As Long, ByVal eClave As Long) As Long
        Dim cON_x As New SqlConnection(MiConexion)
        Try
            'Regresa La Session
            USPDameDetOrserClave = 0
            Dim cmd As New SqlCommand
            cON_x.Open()
            With cmd
                .CommandText = "USPDameDetOrserClave"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = cON_x
                Dim prm1 As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = eClv_Orden
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@Clave", SqlDbType.BigInt)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = eClave
                .Parameters.Add(prm2)

                Dim prm3 As New SqlParameter("@ClaveRegresar", SqlDbType.BigInt)
                prm3.Direction = ParameterDirection.Output
                prm3.Value = 0
                .Parameters.Add(prm3)
                Dim i As Integer = .ExecuteNonQuery
                USPDameDetOrserClave = prm3.Value
            End With
            cON_x.Close()
        Catch ex As Exception
            cON_x.Close()

        End Try
    End Function

    Public Function SP_REFERENCIA_OXXO(ByVal eContrato As Long) As String
        Dim cON_x As New SqlConnection(MiConexion)
        Try
            'Regresa La Session
            SP_REFERENCIA_OXXO = 0
            Dim cmd As New SqlCommand
            cON_x.Open()
            With cmd
                .CommandText = "SP_REFERENCIA_OXXO"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = cON_x
                Dim prm1 As New SqlParameter("@contrato", SqlDbType.BigInt)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = eContrato
                .Parameters.Add(prm1)

                Dim prm3 As New SqlParameter("@Texto", SqlDbType.VarChar, 50)
                prm3.Direction = ParameterDirection.Output
                prm3.Value = 0
                .Parameters.Add(prm3)
                Dim i As Integer = .ExecuteNonQuery
                SP_REFERENCIA_OXXO = prm3.Value
            End With
            cON_x.Close()
        Catch ex As Exception
            cON_x.Close()

        End Try
    End Function

    Public Function UspVerAparatodelClientedig(ByVal eContratonet As Long) As String
        Dim cON_x As New SqlConnection(MiConexion)
        Try
            'Regresa La Session
            UspVerAparatodelClientedig = 0
            Dim cmd As New SqlCommand
            cON_x.Open()
            With cmd
                .CommandText = "UspVerAparatodelClientedig"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = cON_x
                Dim prm1 As New SqlParameter("@CONTRATONET", SqlDbType.BigInt)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = eContratonet
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@NoSerie", SqlDbType.VarChar, 100)
                prm2.Direction = ParameterDirection.Output
                prm2.Value = ""
                .Parameters.Add(prm2)
                Dim i As Integer = .ExecuteNonQuery
                UspVerAparatodelClientedig = prm2.Value
            End With
            cON_x.Close()
        Catch ex As Exception
            cON_x.Close()

        End Try
    End Function

    Public Function DAMESclv_Sessionporfavor() As Long
        Dim cON_x As New SqlConnection(MiConexion)
        Try
            'Regresa La Session
            DAMESclv_Sessionporfavor = 0
            Dim cmd As New SqlCommand
            cON_x.Open()
            With cmd
                .CommandText = "DameClv_Session_Servicios"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = cON_x
                Dim prm2 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                prm2.Direction = ParameterDirection.Output
                prm2.Value = 0
                .Parameters.Add(prm2)
                Dim i As Integer = .ExecuteNonQuery
                DAMESclv_Sessionporfavor = prm2.Value
            End With
            cON_x.Close()
        Catch ex As Exception
            cON_x.Close()
        End Try
    End Function

    Public Sub HazClienteDeTipoSoloInternet(ByVal Contrato As Integer)


        Dim CON80 As New SqlClient.SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlCommand()

        Try
            CMD = New SqlClient.SqlCommand()
            CON80.Open()
            With CMD
                .CommandText = "HazClienteDeTipoSoloInternet"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 0

                Dim prmContrato As New SqlParameter("@Contrato", SqlDbType.BigInt)
                prmContrato.Value = Contrato
                .Parameters.Add(prmContrato)

                Dim i As Integer = .ExecuteNonQuery()
                MsgBox("Se ha guardado el Cliente como: SOLO INTERNET con �xito.", MsgBoxStyle.Information, "CLIENTE GUARDADO COMO: SOLO INTERNET")

            End With
            CON80.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Function spConsultaTurnos(Optional ByVal prmIncluyeTodos As Boolean = False) As DataTable
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("spConsultaTurnos", CON)
        CMD.CommandType = CommandType.StoredProcedure
        'para darle chekin
        If prmIncluyeTodos Then
            Dim prmIncluTodos As New SqlParameter("@IncluyeTodos", SqlDbType.Bit)
            prmIncluTodos.Value = prmIncluyeTodos
            CMD.Parameters.Add(prmIncluTodos)
        End If

        Dim DA As New SqlDataAdapter(CMD)
        Dim DT As New DataTable

        Try
            CON.Open()
            DA.Fill(DT)
            spConsultaTurnos = DT
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Function

    Public Sub DimeSiAplicaIEPS(ByVal OP As Integer, ByVal CLV_TIPSER As Integer, ByVal CLV_SERVICIO As Integer, ByVal CLAVE As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim command As New SqlCommand("DimeSiAplicaIEPS", conexion)
        command.CommandType = CommandType.StoredProcedure

        Dim parametro1 As New SqlParameter("@OP", SqlDbType.Int)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = OP
        command.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@CLV_TIPSER", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = CLV_TIPSER
        command.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@CLV_SERVICIO", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = CLV_SERVICIO
        command.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@CLAVE", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = CLAVE
        command.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@APLICA", SqlDbType.Bit)
        parametro5.Direction = ParameterDirection.Output
        command.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@VISIBLE", SqlDbType.Bit)
        parametro6.Direction = ParameterDirection.Output
        command.Parameters.Add(parametro6)

        Try
            conexion.Open()
            command.ExecuteNonQuery()
            VISIBLE_IEPS = parametro6.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub


    Public Sub Dame_fecha_hora_serv_New()
        Dim CON90 As New SqlClient.SqlConnection(MiConexion)
        Dim Cmd As New SqlClient.SqlCommand()
        Try
            eGloFecha = ""
            eGloHora = ""
            CON90.Open()
            With Cmd
                .CommandText = "Dame_fecha_hora_serv_New"
                .Connection = CON90
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
                '@clv_combo bigint,@error int output

                Dim prm As New SqlParameter("@fecha", SqlDbType.VarChar, 10)
                prm.Direction = ParameterDirection.Output
                prm.Value = 0
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@hora", SqlDbType.VarChar, 10)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)


                Dim ia As Integer = .ExecuteNonQuery()
                eGloFecha = prm.Value
                eGloHora = prm1.Value
            End With
            CON90.Close()
        Catch ex As Exception
            If CON90.State = ConnectionState.Open Then
                CON90.Close()
            End If
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Public Sub Dame_La_HoraEje_Orden(ByVal eClv_Orden As Long)
        Dim CON90 As New SqlClient.SqlConnection(MiConexion)
        Dim Cmd As New SqlClient.SqlCommand()
        Try
            eGloFecha_sol = ""
            eGloHora_sol = ""
            eGloHora_eje = ""
            eGloFecha_eje = ""

            CON90.Open()
            With Cmd
                .CommandText = "Dame_La_HoraEje_Orden"
                .Connection = CON90
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
                '@clv_combo bigint,@error int output
                Dim prmx As New SqlParameter("@CLV_ORDEN", SqlDbType.BigInt)
                prmx.Direction = ParameterDirection.Input
                prmx.Value = eClv_Orden
                .Parameters.Add(prmx)


                Dim prm As New SqlParameter("@fecha_sol", SqlDbType.VarChar, 10)
                prm.Direction = ParameterDirection.Output
                prm.Value = 0
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@hora_sol", SqlDbType.VarChar, 10)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@fecha_eje", SqlDbType.VarChar, 10)
                prm2.Direction = ParameterDirection.Output
                prm2.Value = 0
                .Parameters.Add(prm2)

                Dim prm12 As New SqlParameter("@hora_eje", SqlDbType.VarChar, 10)
                prm12.Direction = ParameterDirection.Output
                prm12.Value = 0
                .Parameters.Add(prm12)


                Dim ia As Integer = .ExecuteNonQuery()
                eGloFecha_sol = prm.Value
                eGloHora_sol = prm1.Value
                eGloFecha_eje = prm2.Value
                eGloHora_eje = prm12.Value
            End With
            CON90.Close()
        Catch ex As Exception
            If CON90.State = ConnectionState.Open Then
                CON90.Close()
            End If
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub Dime_Checa_Servicios_Cliente(ByVal XContrato As Long)
        Dim CON90 As New SqlClient.SqlConnection(MiConexion)
        Dim Cmd As New SqlClient.SqlCommand()
        Try
            CliTiene_Internet = 0
            CliTiene_Tv = 0
            CliTiene_Dig = 0
            CliTiene_Tel = 0
            CON90.Open()
            With Cmd
                .CommandText = "Dime_Checa_Servicios_Cliente"
                .Connection = CON90
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
                '@clv_combo bigint,@error int output

                Dim prm As New SqlParameter("@contrato", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = XContrato
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@TV", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@INT", SqlDbType.Int)
                prm2.Direction = ParameterDirection.Output
                prm2.Value = 0
                .Parameters.Add(prm2)

                Dim prm3 As New SqlParameter("@DIG", SqlDbType.Int)
                prm3.Direction = ParameterDirection.Output
                prm3.Value = 0
                .Parameters.Add(prm3)

                Dim prm4 As New SqlParameter("@TEL", SqlDbType.Int)
                prm4.Direction = ParameterDirection.Output
                prm4.Value = 0
                .Parameters.Add(prm4)
                Dim ia As Integer = .ExecuteNonQuery()
                CliTiene_Tv = prm1.Value
                CliTiene_Internet = prm2.Value
                CliTiene_Dig = prm3.Value
                CliTiene_Tel = prm4.Value
            End With
            CON90.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub ChecaTelefonosDisponibles()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ChecaTelefonosDisponibles", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Res", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Msg", SqlDbType.VarChar, 150)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Try
            eResValida = 0
            eMsgValida = String.Empty
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            eResValida = CInt(parametro.Value)
            eMsgValida = parametro2.Value.ToString
        Catch ex As Exception
            conexion.Close()
        End Try
    End Sub
    Public Sub Genera_Cita_Ordenes(ByVal eClv_Orden As Integer)

        Dim CON As New SqlClient.SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlCommand()

        Try
            CMD = New SqlClient.SqlCommand()
            CON.Open()
            With CMD
                .CommandText = "Genera_Cita_Ordser"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON
                .CommandTimeout = 0

                Dim prmContrato As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
                prmContrato.Value = eClv_Orden
                .Parameters.Add(prmContrato)

                Dim i As Integer = .ExecuteNonQuery()
                'MsgBox("Se ha guardado el Cliente como: SOLO INTERNET con �xito.", MsgBoxStyle.Information, "CLIENTE GUARDADO COMO: SOLO INTERNET")

            End With
            CON.Close()
        Catch ex As Exception
            If CON.State = ConnectionState.Open Then CON.Close()
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub



    Public Sub NUEMOVREL_CITAS(ByVal opClv_Cita As Long, ByVal opComentario As String)
        Dim Con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        'Contrato
        If Contrato = Nothing Then Contrato = 0
        Try
            'Muestra_Plazo_Forzozo (@contrato bigint,@Clv_unicanet bigint,@Plazo varchar(10) output)
            cmd = New SqlCommand()
            Con.Open()
            With cmd
                .CommandText = "NUEMOVREL_CITAS"
                .Connection = Con
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@Clv_Cita", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = opClv_Cita
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@Comentario", SqlDbType.VarChar, 250)
                prm1.Direction = ParameterDirection.Input
                'If IsNumeric(Me.TextCosto.Text) = True Then
                prm1.Value = opComentario
                'Else
                'prm1.Value = 0
                'End If

                .Parameters.Add(prm1)

                Dim ia As Integer = .ExecuteNonQuery()

                'Me.TextComentario.Text = prm1.Value

            End With
            Con.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub DIME_SITELYNET(ByVal optcontrato As Integer)
        Dim cON_x As New SqlConnection(MiConexion)
        Try
            gLONET_1 = 0
            gLOTEL_1 = 0
            CliTiene_Tv = 0
            CliTiene_Dig = 0

            Dim cmd As New SqlCommand

            cON_x.Open()
            With cmd
                .CommandText = "DIME_SITELYNET"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = cON_x

                Dim prm As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
                Dim prm2 As New SqlParameter("@NUMNET", SqlDbType.Int)
                Dim prm3 As New SqlParameter("@NUMTEL", SqlDbType.Int)
                Dim prm4 As New SqlParameter("@TV", SqlDbType.Int)
                Dim prm5 As New SqlParameter("@Dig", SqlDbType.Int)

                prm.Direction = ParameterDirection.Input
                prm2.Direction = ParameterDirection.Output
                prm3.Direction = ParameterDirection.Output
                prm4.Direction = ParameterDirection.Output
                prm5.Direction = ParameterDirection.Output

                prm.Value = optcontrato
                prm2.Value = 0
                prm3.Value = 0
                prm4.Value = 0
                prm5.Value = 0

                .Parameters.Add(prm)
                .Parameters.Add(prm2)
                .Parameters.Add(prm3)
                .Parameters.Add(prm4)
                .Parameters.Add(prm5)

                Dim i As Integer = .ExecuteNonQuery
                gLONET_1 = prm2.Value
                gLOTEL_1 = prm3.Value
                CliTiene_Tv = prm4.Value
                CliTiene_Dig = prm5.Value
            End With
            cON_x.Close()
        Catch ex As Exception
            cON_x.Close()
            gLONET_1 = 0
            gLOTEL_1 = 0
        End Try
    End Sub

    Public Function TEL_TIENE_ADEUDOS(ByVal optcontrato As Integer) As Integer
        Dim cON_x As New SqlConnection(MiConexion)
        Try
            TEL_TIENE_ADEUDOS = 0
            Dim cmd As New SqlCommand
            cON_x.Open()
            With cmd
                .CommandText = "TEL_TIENE_ADEUDOS"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = cON_x
                Dim prm As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
                Dim prm2 As New SqlParameter("@BND", SqlDbType.Int)
                prm.Direction = ParameterDirection.Input
                prm2.Direction = ParameterDirection.Output
                prm.Value = optcontrato
                prm2.Value = 0
                .Parameters.Add(prm)
                .Parameters.Add(prm2)
                Dim i As Integer = .ExecuteNonQuery
                TEL_TIENE_ADEUDOS = prm2.Value
            End With
            cON_x.Close()
        Catch ex As Exception
            cON_x.Close()
        End Try
    End Function

    Public Function DimeTipSer_CualEsPrincipal() As Integer
        Dim con As New SqlConnection(MiConexion)
        Try
            DimeTipSer_CualEsPrincipal = 0

            Dim reader As SqlDataReader
            Dim cmd As New SqlCommand("DimeTipSer_CualEsPrincipal", con)
            cmd.CommandType = CommandType.StoredProcedure
            con.Open()
            reader = cmd.ExecuteReader()
            Using reader
                While reader.Read
                    DimeTipSer_CualEsPrincipal = reader.GetValue(0)
                End While
            End Using

            con.Close()
        Catch ex As Exception
            DimeTipSer_CualEsPrincipal = 1
            MsgBox(ex.Message)
            con.Close()
        End Try
    End Function



    Public Function DAMESTATUSHAB(ByVal CLV_TIPSER As Integer) As Integer
        Dim cON_x As New SqlConnection(MiConexion)
        Try
            DAMESTATUSHAB = 0
            Dim cmd As New SqlCommand
            cON_x.Open()
            With cmd
                .CommandText = "DAMESTATUSHAB"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = cON_x
                Dim prm As New SqlParameter("@CLV_TIPSER", SqlDbType.Int)
                Dim prm2 As New SqlParameter("@HABILITAR", SqlDbType.Int)
                prm.Direction = ParameterDirection.Input
                prm2.Direction = ParameterDirection.Output
                prm.Value = CLV_TIPSER
                prm2.Value = 0
                .Parameters.Add(prm)
                .Parameters.Add(prm2)
                Dim i As Integer = .ExecuteNonQuery
                DAMESTATUSHAB = prm2.Value
            End With
            cON_x.Close()
        Catch ex As Exception
            DAMESTATUSHAB = 1
            'MsgBox(ex.Message)
            cON_x.Close()
        End Try
    End Function


    Public Sub bitsist(ByVal usuario As String, ByVal contrato As Long, ByVal sistema As String, ByVal pantalla As String, ByVal control As String, ByVal valorant As String, ByVal valornuevo As String, ByVal clv_ciudad As String)
        Dim COn85 As New SqlConnection(MiConexion)
        COn85.Open()
        guardabitacora = New sofTV.ProcedimientosArnoldo2.Inserta_MovSistDataTable
        guardabitacorabuena = New sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_MovSistTableAdapter

        If valorant <> valornuevo Then
            guardabitacorabuena.Connection = COn85
            guardabitacorabuena.Fill(guardabitacora, usuario, contrato, sistema, pantalla, control, valorant, valornuevo, clv_ciudad)
        End If
        COn85.Close()

    End Sub


    Public Sub RecorrerEstructuraMenu(ByVal oMenu As MenuStrip)
        Dim menu As ToolStripMenuItem
        For Each oOpcionMenu As ToolStripMenuItem In oMenu.Items
            menu = New ToolStripMenuItem
            menu = oOpcionMenu
            menu.ForeColor = System.Drawing.Color.FromArgb(ColorMenuLetra)
            menu.Visible = TipoAccesoMenusPerfiles(GloTipoUsuario, oOpcionMenu.Name)
            menu = Nothing
            If oOpcionMenu.DropDownItems.Count > 0 Then
                RecorrerSubmenu(oOpcionMenu.DropDownItems, "----")
            End If
        Next
    End Sub

    Public Sub RecorrerSubmenu(ByVal oSubmenuItems As ToolStripItemCollection, ByVal sGuiones As String)
        Dim submenu As ToolStripItem
        For Each oSubitem As ToolStripItem In oSubmenuItems
            If oSubitem.GetType Is GetType(ToolStripMenuItem) Then
                submenu = New ToolStripMenuItem
                submenu = oSubitem
                submenu.Visible = TipoAccesoMenusPerfiles(GloTipoUsuario, oSubitem.Name)
                submenu.ForeColor = System.Drawing.Color.FromArgb(ColorMenuLetra)
                submenu = Nothing
                If CType(oSubitem, ToolStripMenuItem).DropDownItems.Count > 0 Then
                    RecorrerSubmenu(CType(oSubitem, ToolStripMenuItem).DropDownItems, sGuiones & "----")
                End If
            End If
        Next
    End Sub

    Public Sub bwrpanel(ByVal panel2 As Panel, ByVal formulario As Form) 'BUSCA PANEL----------------------------------
        Dim data As DataGridView
        Dim label As Label
        Dim boton As Button
        Dim split As SplitContainer
        Dim panel3 As Panel
        Dim text As TextBox
        Dim GROUP As GroupBox
        Dim var As String
        Dim TAB As TabControl
        Dim radio As RadioButton
        Dim CHECK As CheckBox
        Dim BN As BindingNavigator
        'Dim CON27 As New SqlConnection(MiConexion)
        'CON27.Open()
        'busca = New sofTV.DataSetLidia.BUSCAControlDataTable
        'buscaCtr = New sofTV.DataSetLidiaTableAdapters.BUSCAControlTableAdapter
        'borra = New sofTV.DataSetLidia.BORRARControlDataTable
        'borraCtr = New sofTV.DataSetLidiaTableAdapters.BORRARControlTableAdapter
        'nuevo = New sofTV.DataSetLidia.NUEVOControlDataTable
        'NuevoCtr = New sofTV.DataSetLidiaTableAdapters.NUEVOControlTableAdapter

        If panel2.BackColor <> Color.WhiteSmoke Then
            panel2.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
            'panel2.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
        End If

        For Each ctr As Control In panel2.Controls
            'SPLIT -----------------------------------------------------
            If ctr.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctr
                bwrpanel(split.Panel1, formulario)
                bwrpanel(split.Panel2, formulario)
                split = Nothing
                'CRYSTAL REPORTS ----------------------------
            ElseIf ctr.GetType Is GetType(CrystalDecisions.Windows.Forms.CrystalReportViewer) Then
                ctr.BackColor = Color.WhiteSmoke


                'DATA GRID----------------------------------
            ElseIf ctr.GetType Is GetType(System.Windows.Forms.DataGridView) Then
                data = New DataGridView
                data = ctr
                data.ColumnHeadersDefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                ' data.ColumnHeadersDefaultCellStyle.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                data.BackgroundColor = System.Drawing.Color.FromArgb(ColorGrid)
                'BUSCA PARA EVITAR REDUNDANCIA EN CONTROLES
                'buscaCtr.Connection = CON27
                'buscaCtr.Fill(busca, 1, formulario.Name, data.Name, data.Name, salida)
                'If salida = 0 Then
                '    'NuevoCtr.Fill(nuevo, 1, formulario.Name, data.Name, data.Name, GloTipoUsuario, data.Location.Y, data.Location.X)
                '    'ElseIf salida > 0 Then
                '    '    borraCtr.Fill(borra, data.Name, data.Name, formulario.Name, 1)
                'End If
                data = Nothing
            End If
            var = Mid(ctr.Name, 1, 3)
            'LABEL----------------------------------------
            If ctr.GetType Is GetType(System.Windows.Forms.Label) And var <> "CMB" And ctr.BackColor <> Color.WhiteSmoke Then
                label = New Label
                label = ctr
                label.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                label.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                label = Nothing
            ElseIf ctr.GetType Is GetType(System.Windows.Forms.Label) And var = "CMB" Then
                label = New Label
                label = ctr
                label.ForeColor = Color.Black
                label.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                label = Nothing

                'TEXT BOX --------------------------------------------
            ElseIf ctr.GetType Is GetType(System.Windows.Forms.TextBox) And var = "CMB" Then
                text = New TextBox
                text = ctr
                text.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                text.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                text = Nothing
                'BOTONES -------------------------------------------------
            ElseIf ctr.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctr
                boton.BackColor = System.Drawing.Color.FromArgb(ColorBut)
                boton.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
                'BUSCA PARA EVITAR REDUNDANCIA EN CONTROLES
                'If boton.Text <> "&Buscar" Then
                '    buscaCtr.Connection = CON27
                '    buscaCtr.Fill(busca, 1, formulario.Name, boton.Text, boton.Name, salida)
                '    If salida = 0 Then
                '        NuevoCtr.Connection = CON27
                '        NuevoCtr.Fill(nuevo, 1, formulario.Name, boton.Text, boton.Name, GloTipoUsuario, boton.Location.Y, boton.Location.X)
                '        'ElseIf salida > 0 Then
                '        '    borraCtr.Fill(borra, boton.Text, boton.Name, formulario.Name, 1)
                '    End If
                'End If
                boton = Nothing

                'PANEL ---------------------------------------------------
            ElseIf ctr.GetType Is GetType(System.Windows.Forms.Panel) And var = "CMB" Then
                panel3 = New Panel
                panel3 = ctr
                panel3.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                bwrpanel(panel3, formulario)
                panel3 = Nothing

                'GROUP BOX -----------------------------------------------
            ElseIf ctr.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GROUP = New GroupBox
                GROUP = ctr
                GROUP.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                GROUP.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                bwrgroup(GROUP, formulario)
                GROUP = Nothing

                'TAB CONTROL ------------------------------------------------
            ElseIf ctr.GetType Is GetType(System.Windows.Forms.TabControl) Then
                TAB = New TabControl
                TAB = ctr
                TAB.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                tabbwr(TAB, formulario)
                TAB = Nothing

                'RADIOBUTON--------------------------------------------------------
                'ElseIf ctr.GetType Is GetType(System.Windows.Forms.RadioButton) Then
                '    radio = New RadioButton
                '    radio = ctr
                '    radio.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                '    radio.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                '    radio = Nothing
            End If
            'PARA LLENAR NOMBRES DE LOS CONTROLES---------------------------------------------
            'If ctr.GetType Is GetType(System.Windows.Forms.TextBox) Then
            '    text = New TextBox
            '    text = ctr
            '    'BUSCA PARA EVITAR REDUNDANCIA EN CONTROLES
            '    buscaCtr.Connection = CON27
            '    buscaCtr.Fill(busca, 1, formulario.Name, text.Name, text.Name, salida)
            '    If salida = 0 Then
            '        NuevoCtr.Connection = CON27
            '        NuevoCtr.Fill(nuevo, 1, formulario.Name, text.Name, text.Name, GloTipoUsuario, text.Location.Y, text.Location.X)
            '        'ElseIf salida > 0 Then
            '        '    borraCtr.Fill(borra, text.Text, text.Name, formulario.Name, 1)
            '    End If
            '    text = Nothing
            'End If
            'If ctr.GetType Is GetType(System.Windows.Forms.CheckBox) Then
            '    CHECK = New CheckBox
            '    CHECK = ctr
            '    'BUSCA PARA EVITAR REDUNDANCIA EN CONTROLES
            '    buscaCtr.Connection = CON27
            '    buscaCtr.Fill(busca, 1, formulario.Name, CHECK.Name, CHECK.Name, salida)
            '    If salida = 0 Then
            '        NuevoCtr.Connection = CON27
            '        NuevoCtr.Fill(nuevo, 1, formulario.Name, CHECK.Name, CHECK.Name, GloTipoUsuario, CHECK.Location.Y, CHECK.Location.X)
            '        'ElseIf salida > 0 Then
            '        '    borraCtr.Fill(borra, CHECK.Name, CHECK.Name, formulario.Name, 1)
            '    End If
            '    CHECK = Nothing
            'End If

            'If ctr.GetType Is GetType(System.Windows.Forms.RadioButton) Then
            '    radio = New RadioButton
            '    radio = ctr
            '    'BUSCA PARA EVITAR REDUNDANCIA EN CONTROLES
            '    buscaCtr.Connection = CON27
            '    buscaCtr.Fill(busca, 1, formulario.Name, radio.Name, radio.Name, salida)
            '    If salida = 0 Then
            '        NuevoCtr.Connection = CON27
            '        NuevoCtr.Fill(nuevo, 1, formulario.Name, radio.Name, radio.Name, GloTipoUsuario, radio.Location.Y, radio.Location.X)
            '        'ElseIf salida > 0 Then
            '        '    borraCtr.Fill(borra, radio.Text, radio.Name, formulario.Name, 1)
            '    End If

            '    radio = Nothing
            'End If

            'If ctr.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
            '    BN = New BindingNavigator
            '    BN = ctr
            '    For Each BIN As ToolStripItem In BN.Items
            '        If BIN.GetType Is GetType(System.Windows.Forms.ToolStripButton) Then
            '            'BUSCA PARA EVITAR REDUNDANCIA EN CONTROLES
            '            buscaCtr.Connection = CON27
            '            buscaCtr.Fill(busca, 1, formulario.Name, BIN.Text, BIN.Name, salida)
            '            If salida = 0 Then
            '                NuevoCtr.Connection = CON27
            '                NuevoCtr.Fill(nuevo, 1, formulario.Name, BIN.Text, BIN.Name, GloTipoUsuario, 0, 0)
            '                'ElseIf salida > 0 Then
            '                '    borraCtr.Fill(borra, BIN.Text, BIN.Name, formulario.Name, 1)
            '            End If
            '        End If
            '    Next
            '    BN = Nothing
            'End If
            '-----------------------------------------------------------------------------------

            'CON27.Close()
            If ctr.GetType Is GetType(System.Windows.Forms.Panel) And var <> "CMB" Then
                bwrpanel(ctr, formulario)
            End If
        Next
    End Sub
    Public Sub bwrgroup(ByVal grup As GroupBox, ByVal formulario As Form) 'GROUP BOX--------------------------------
        Dim panel3 As Panel
        Dim label As Label
        Dim var As String
        Dim TEXT As TextBox
        Dim CHECK As CheckBox
        Dim RADIO As RadioButton
        Dim boton As Button

        'nuevo = New sofTV.DataSetLidia.NUEVOControlDataTable
        'NuevoCtr = New sofTV.DataSetLidiaTableAdapters.NUEVOControlTableAdapter
        'borra = New sofTV.DataSetLidia.BORRARControlDataTable
        'borraCtr = New sofTV.DataSetLidiaTableAdapters.BORRARControlTableAdapter
        'busca = New sofTV.DataSetLidia.BUSCAControlDataTable
        'buscaCtr = New sofTV.DataSetLidiaTableAdapters.BUSCAControlTableAdapter
        'Dim CON13 As New SqlConnection(MiConexion)
        'CON13.Open()
        For Each ctm As Control In grup.Controls
            var = Mid(ctm.Name, 1, 3)
            'PANEL------------------------------------------------------------- 
            If ctm.GetType Is GetType(System.Windows.Forms.Panel) And ctm.BackColor <> Color.WhiteSmoke Then
                panel3 = New Panel
                panel3 = ctm
                panel3.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                bwrpanel(panel3, formulario)
                panel3 = Nothing

                'LABEL--------------------------------------------------------------
            ElseIf ctm.GetType Is GetType(System.Windows.Forms.Label) And var <> "CMB" And ctm.BackColor <> Color.WhiteSmoke Then
                label = New Label
                label = ctm
                label.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                label.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                label = Nothing
            End If
            ''PARA LLENAR NOMBRES DE LOS CONTROLES
            'If ctm.GetType Is GetType(System.Windows.Forms.TextBox) Then
            '    TEXT = New TextBox
            '    TEXT = ctm
            '    'BUSCA PARA EVITAR REDUNDANCIA EN CONTROLES
            '    buscaCtr.Connection = CON13
            '    buscaCtr.Fill(busca, 1, formulario.Name, TEXT.Name, TEXT.Name, salida)
            '    If salida = 0 Then
            '        NuevoCtr.Connection = CON13
            '        NuevoCtr.Fill(nuevo, 1, formulario.Name, TEXT.Name, TEXT.Name, GloTipoUsuario, TEXT.Location.Y, TEXT.Location.X)
            '        'ElseIf salida > 0 Then
            '        '    borraCtr.Fill(borra, TEXT.Text, TEXT.Name, formulario.Name, 1)
            '    End If
            '    TEXT = Nothing
            'End If

            'If ctm.GetType Is GetType(System.Windows.Forms.CheckBox) Then
            '    CHECK = New CheckBox
            '    CHECK = ctm
            '    'BUSCA PARA EVITAR REDUNDANCIA EN CONTROLES
            '    buscaCtr.Connection = CON13
            '    buscaCtr.Fill(busca, 1, formulario.Name, CHECK.Name, CHECK.Name, salida)
            '    If salida = 0 Then
            '        NuevoCtr.Connection = CON13
            '        NuevoCtr.Fill(nuevo, 1, formulario.Name, CHECK.Name, CHECK.Name, GloTipoUsuario, CHECK.Location.Y, CHECK.Location.X)
            '        'ElseIf salida > 0 Then
            '        '    borraCtr.Fill(borra, CHECK.Name, CHECK.Name, formulario.Name, 1)
            '    End If
            '    CHECK = Nothing
            'End If
            'If ctm.GetType Is GetType(System.Windows.Forms.RadioButton) Then
            '    RADIO = New RadioButton
            '    RADIO = ctm
            '    'BUSCA PARA EVITAR REDUNDANCIA EN CONTROLES
            '    buscaCtr.Connection = CON13
            '    buscaCtr.Fill(busca, 1, formulario.Name, RADIO.Name, RADIO.Name, salida)
            '    If salida = 0 Then
            '        NuevoCtr.Connection = CON13
            '        NuevoCtr.Fill(nuevo, 1, formulario.Name, RADIO.Name, RADIO.Name, GloTipoUsuario, RADIO.Location.Y, RADIO.Location.X)
            '        'ElseIf salida > 0 Then
            '        '    borraCtr.Fill(borra, RADIO.Text, RADIO.Name, formulario.Name, 1)
            '    End If
            '    RADIO = Nothing
            'End If
            '-----------------------------------------------------------------------------------
            'If ctm.GetType Is GetType(System.Windows.Forms.Button) Then
            '    boton = New Button
            '    boton = ctm
            '    boton.BackColor = System.Drawing.Color.FromArgb(ColorBut)
            '    boton.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
            '    If boton.Text <> "&Buscar" Then
            '        'BUSCA PARA EVITAR REDUNDANCIA EN CONTROLES
            '        buscaCtr.Connection = CON13
            '        buscaCtr.Fill(busca, 1, formulario.Name, boton.Text, boton.Name, salida)
            '        If salida = 0 Then
            '            NuevoCtr.Connection = CON13
            '            NuevoCtr.Fill(nuevo, 1, formulario.Name, boton.Text, boton.Name, GloTipoUsuario, boton.Location.Y, boton.Location.X)
            '            'ElseIf salida > 0 Then
            '            '    borraCtr.Fill(borra, boton.Text, boton.Name, formulario.Name, 1)
            '        End If
            '    End If
            '    boton = Nothing
            'End If

        Next
        'CON13.Close()
    End Sub

    Public Sub tabbwr(ByVal tab As TabControl, ByVal formulario As Form) 'BUSCA EN UN TAB CONTROL
        Dim page As TabPage
        For Each ctn As Control In tab.Controls
            If ctn.GetType Is GetType(System.Windows.Forms.TabPage) Then
                page = New TabPage
                page = ctn
                page.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                pagebwr(page, formulario)
            End If
        Next

    End Sub
    Public Sub pagebwr(ByVal page As TabPage, ByVal formulario As Form) 'BUSCA EN UN TAB PAGE
        Dim panel As Panel
        Dim split As SplitContainer
        For Each ctn As Control In page.Controls
            'PANEL---------------------------------------------------------------
            If ctn.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctn
                panel.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                bwrpanel(page, formulario)
            End If
            'SPLIT-----------------------------------------------------------------
            If ctn.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctn
                bwrpanel(split.Panel1, formulario)
                bwrpanel(split.Panel2, formulario)
            End If
        Next

    End Sub


    ''''--- IMPLEMENTOS PARA ALMACEN WEB---------------********
    Function Dame_Folio(ByVal clv_orden As Long, ByVal clv_categoria As Integer, ByVal no_folio As Integer) As Integer

        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("Dame_Folio", con)
        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@clv_orden", SqlDbType.BigInt)
        Dim par2 As New SqlParameter("@clv_categoria", SqlDbType.Int)
        Dim par3 As New SqlParameter("@no_folio", SqlDbType.Int)

        par1.Direction = ParameterDirection.Input
        par2.Direction = ParameterDirection.Input
        par3.Direction = ParameterDirection.Output

        par1.Value = clv_orden
        par2.Value = clv_categoria

        com.Parameters.Add(par1)
        com.Parameters.Add(par2)
        com.Parameters.Add(par3)

        Try
            con.Open()
            com.ExecuteNonQuery()
            Return CInt(par3.Value)
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()
        End Try



    End Function

    Public Sub Inserta_Rel_Bitacora_Orden(ByVal clv_bitacora As Integer, ByVal clv_orden As Integer, ByVal tipo As String, ByVal contrato As Long)

        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("Inserta_Rel_Bitacora_Orden", con)
        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@Clv_bitacora", SqlDbType.BigInt)
        Dim par2 As New SqlParameter("@clv_Orden", SqlDbType.Int)
        Dim par3 As New SqlParameter("@Tipo", SqlDbType.VarChar, 1)
        Dim par4 As New SqlParameter("@contrato", SqlDbType.BigInt)

        par1.Direction = ParameterDirection.Input
        par2.Direction = ParameterDirection.Input
        par3.Direction = ParameterDirection.Input
        par4.Direction = ParameterDirection.Input

        par1.Value = clv_bitacora
        par2.Value = clv_orden
        par3.Value = tipo
        par4.Value = contrato

        com.Parameters.Add(par1)
        com.Parameters.Add(par2)
        com.Parameters.Add(par3)
        com.Parameters.Add(par4)

        Try
            con.Open()
            com.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()
        End Try
    End Sub
    ''''*********-------------    FIN   -----------****************

    Public Sub SetDBReport(ByVal ds As DataSet, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                myTable.SetDataSource(ds)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Function Dime_Si_Hay_Combos() As Integer

        Dim cON_x As New SqlConnection(MiConexion)
        Try
            'Regresa La Session
            Dime_Si_Hay_Combos = 0
            Dim cmd As New SqlCommand
            cON_x.Open()
            With cmd
                .CommandText = "Dime_Si_Hay_Combos"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = cON_x
                Dim prm2 As New SqlParameter("@Bnd", SqlDbType.BigInt)
                prm2.Direction = ParameterDirection.Output
                prm2.Value = 0
                .Parameters.Add(prm2)
                Dim i As Integer = .ExecuteNonQuery
                Dime_Si_Hay_Combos = prm2.Value
            End With
            cON_x.Close()
        Catch ex As Exception
            cON_x.Close()
        End Try
    End Function

    ''''***************** Ventas --- > Servicios vendidos por semana ***************************

    Public Function imprimirDataset(ByVal fechaIni As DateTime, ByVal fechaFin As DateTime, ByVal prmClvSession As Long, ByVal prmOp As Integer, ByVal prmClvTipSer As Integer) As DataSet
        Try

            Dim parametro(3) As SqlParameter

            parametro(0) = New SqlParameter("@fechaInicio", SqlDbType.DateTime)
            parametro(0).Value = fechaIni

            parametro(1) = New SqlParameter("@fechaFin", SqlDbType.DateTime)
            parametro(1).Value = fechaFin

            parametro(2) = New SqlParameter("@clvSession", SqlDbType.BigInt)
            parametro(2).Value = prmClvSession

            parametro(3) = New SqlParameter("@clvTipSer", SqlDbType.Int)
            parametro(3).Value = prmClvTipSer

            If prmOp = 1 Then
                _DataSet = consultas.consultarDS("uspReporteNivelesVentas", parametro) ''''COMISIONES DE VENTAS
            ElseIf prmOp = 2 Then
                _DataSet = consultas.consultarDS("uspReporteNivelesCajas", parametro) ''''COMISIONES DE CAJAS
            End If


            Return _DataSet
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
            Return Nothing
        End Try
    End Function

    ''''*******************************************************************************************


    Public Sub uspConsultaDatosParaPagare(ByVal Contrato As Integer)

        '@Contrato			INT	,
        '@Nombre				VARCHAR(800)	OUTPUT		,
        '@Direccion			VARCHAR(800)	OUTPUT		,
        '@Poblacion			VARCHAR(800)	OUTPUT		,	
        '@ImporteTotal		MONEY			OUTPUT		,
        '@CantidadConLetra	VARCHAR(800)	OUTPUT

        Dim conn As New SqlConnection(MiConexion)
        Dim Result As Integer = 0
        Try

            conn.Open()

            Dim comando As New SqlClient.SqlCommand("uspConsultaDatosParaPagare", conn)
            comando.CommandType = CommandType.StoredProcedure

            comando.Parameters.Add("@Contrato", SqlDbType.BigInt).Value = Contrato

            comando.Parameters.Add("@Nombre", SqlDbType.VarChar, 600).Value = ""
            comando.Parameters("@Nombre").Direction = ParameterDirection.Output

            comando.Parameters.Add("@Direccion", SqlDbType.VarChar, 600).Value = ""
            comando.Parameters("@Direccion").Direction = ParameterDirection.Output

            comando.Parameters.Add("@Poblacion", SqlDbType.VarChar, 600).Value = ""
            comando.Parameters("@Poblacion").Direction = ParameterDirection.Output

            comando.Parameters.Add("@ImporteTotal", SqlDbType.Money).Value = 0
            comando.Parameters("@ImporteTotal").Direction = ParameterDirection.Output

            comando.Parameters.Add("@CantidadConLetra", SqlDbType.VarChar, 600).Value = ""
            comando.Parameters("@CantidadConLetra").Direction = ParameterDirection.Output

            Result = comando.ExecuteNonQuery()
            Try
                'If (Result > 0) Then
                FrmDatosComplementariosContrato.Nombre = comando.Parameters("@Nombre").Value.ToString
                FrmDatosComplementariosContrato.Direccion = comando.Parameters("@Direccion").Value.ToString
                FrmDatosComplementariosContrato.Poblacion = comando.Parameters("@Poblacion").Value.ToString
                FrmDatosComplementariosContrato.ImporteTotal = comando.Parameters("@ImporteTotal").Value.ToString
                FrmDatosComplementariosContrato.CantidadConLetra = comando.Parameters("@CantidadConLetra").Value.ToString
                'Else
                'FrmDatosComplementariosContrato.Nombre = ""
                'FrmDatosComplementariosContrato.Direccion = ""
                'FrmDatosComplementariosContrato.Poblacion = ""
                'FrmDatosComplementariosContrato.ImporteTotal = 0
                'FrmDatosComplementariosContrato.CantidadConLetra = ""
                'End If

            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            End Try

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conn.Close()
            conn.Dispose()
        End Try


    End Sub

    'Carga los Datos del Contrato a Imprimir
    Public Function DatosGeneralesImpresionDeContrato(ByVal Contrato As Integer) As DataSet

        Dim conn As New SqlConnection(MiConexion)
        Dim Adaptador As New SqlDataAdapter
        Dim dtResultado As New DataSet

        Try

            Dim comando As New SqlClient.SqlCommand("ConContratoGiga", conn)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 5

            Adaptador.SelectCommand = comando
            Adaptador.SelectCommand.Parameters.Add("@Contrato", SqlDbType.BigInt)
            Adaptador.SelectCommand.Parameters("@Contrato").Value = Contrato
            Adaptador.SelectCommand.Parameters.Add("@Clv_sucursal", SqlDbType.BigInt).Value = GloClv_Sucursal

            conn.Open()
            Adaptador.Fill(dtResultado)
            comando.Dispose()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conn.Close()
            conn.Dispose()
        End Try

        Return dtResultado
    End Function

    Function ValidaDecimal(ByVal e As System.Windows.Forms.KeyPressEventArgs, ByVal Text As TextBox) As Integer

        Dim dig As Integer = Len(Text.Text & e.KeyChar)
        Dim a, esDecimal, NumDecimales As Integer
        Dim esDec As Boolean
        ' se verifica si es un digito o un punto 
        If e.KeyChar.IsDigit(e.KeyChar) Or e.KeyChar = "." Then
            e.Handled = False
        ElseIf e.KeyChar.IsControl(e.KeyChar) Then
            e.Handled = False
            Return a
        Else
            e.Handled = True
        End If
        ' se verifica que el primer digito ingresado no sea un punto al seleccionar
        If Text.SelectedText <> "" Then
            If e.KeyChar = "." Then
                e.Handled = True
                Return a
            End If
        End If
        If dig = 1 And e.KeyChar = "." Then
            e.Handled = True
            Return a
        End If
        'aqui se hace la verificacion cuando es seleccionado el valor del texto
        'y no sea considerado como la adicion de un digito mas al valor ya contenido en el textbox
        If Text.SelectedText = "" Then
            ' aqui se hace el for para controlar que el numero sea de dos digitos - contadose a partir del punto decimal.
            For a = 0 To dig - 1
                Dim car As String = CStr(Text.Text & e.KeyChar)
                If car.Substring(a, 1) = "." Then
                    esDecimal = esDecimal + 1
                    esDec = True
                End If
                If esDec = True Then
                    NumDecimales = NumDecimales + 1
                End If
                ' aqui se controla los digitos a partir del punto numdecimales = 4 si es de dos decimales 
                If NumDecimales >= 4 Or esDecimal >= 2 Then
                    e.Handled = True
                End If
            Next
        End If
    End Function
    Public Function ObtenerTurnoCita(ByVal prmClaveCita As Integer) As String
        Dim Con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()

        If Contrato = Nothing Then Contrato = 0
        Try
            cmd = New SqlCommand()
            Con.Open()
            With cmd
                .CommandText = "CONSULTARREL_CITAS_Turno"
                .Connection = Con
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@Clv_Cita", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = prmClaveCita
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@Turno", SqlDbType.NVarChar, 50)
                prm1.Direction = ParameterDirection.Output
                .Parameters.Add(prm1)

                Dim ia As Integer = .ExecuteNonQuery()

                Return prm1.Value

            End With
            Con.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Function

    '''''*********-------------    FIN   -----------****************
    Dim b As New BarcodeLib.Barcode()
    Public Function GeneraCodeBar128(ByVal Data As String) As Byte()
        Dim imgBarCode As Byte()
        Dim barcode As New PictureBox()
        Dim W As Integer = Convert.ToInt32(200)
        Dim H As Integer = Convert.ToInt32(100)
        'Dim Data As [String] = "31000016024201101070637005"
        Dim Align As BarcodeLib.AlignmentPositions = BarcodeLib.AlignmentPositions.CENTER
        Align = BarcodeLib.AlignmentPositions.CENTER
        Dim type As BarcodeLib.TYPE = BarcodeLib.TYPE.UNSPECIFIED
        type = BarcodeLib.TYPE.CODE128
        Try
            If type <> BarcodeLib.TYPE.UNSPECIFIED Then
                b.IncludeLabel = True
                b.Alignment = Align
                b.RotateFlipType = (RotateFlipType.RotateNoneFlipNone)
                b.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER
                barcode.Image = b.Encode(type, Data, Color.Black, Color.White, W, H)
            End If
            barcode.Width = barcode.Image.Width
            barcode.Height = barcode.Image.Height
            'File.WriteAllBytes(@"C:\XSD\1.jpg", imgbites);                
            Dim imgbites As Byte() = ImageToByte2(barcode.Image)
            imgBarCode = imgbites
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        Return imgBarCode
    End Function

    Public Function GeneraCodeBar128Nuevo(ByVal Data As String) As Byte()
        Dim imgBarCode As Byte()
        Dim barcode As New PictureBox()
        Dim W As Integer = Convert.ToInt32(250)
        Dim H As Integer = Convert.ToInt32(100)
        'Dim Data As [String] = "31000016024201101070637005"
        Dim Align As BarcodeLib.AlignmentPositions = BarcodeLib.AlignmentPositions.CENTER
        Align = BarcodeLib.AlignmentPositions.CENTER
        Dim type As BarcodeLib.TYPE = BarcodeLib.TYPE.UNSPECIFIED
        type = BarcodeLib.TYPE.CODE128
        Try
            If type <> BarcodeLib.TYPE.UNSPECIFIED Then
                b.IncludeLabel = True
                b.Alignment = Align
                b.RotateFlipType = (RotateFlipType.RotateNoneFlipNone)
                b.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER
                barcode.Image = b.Encode(type, Data, Color.Black, Color.White, W, H)
            End If
            barcode.Width = barcode.Image.Width
            barcode.Height = barcode.Image.Height
            'File.WriteAllBytes(@"C:\XSD\1.jpg", imgbites);                
            Dim imgbites As Byte() = ImageToByte2(barcode.Image)
            imgBarCode = imgbites
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        Return imgBarCode
    End Function
    Public Function ImageToByte2(ByVal img As Image) As Byte()
        Dim byteArray As Byte()
        Using stream As New MemoryStream()
            img.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg)
            stream.Close()
            byteArray = stream.ToArray()
        End Using
        Return byteArray
    End Function

    Public Function UspValidarClienteConCortesia(ByVal PRMCONTRATO As Long) As DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, PRMCONTRATO)
        UspValidarClienteConCortesia = BaseII.ConsultaDT("UspValidarClienteConCortesia")
    End Function

End Module
