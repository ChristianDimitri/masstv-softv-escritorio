﻿Public Class FrmSelCajeros
    Public clvSessionReporte As Long

    Private Sub FrmSelCajeros_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        clvSessionReporte = Dame_clv_session_Reportes()
        Me.cmbGruposVentas.DataSource = uspConsultaGruposVentas()
        uspConsultaCajeros(clvSessionReporte, 0)
    End Sub

    Private Sub uspConsultaCajeros(ByVal prmClvSession As Long, ByVal prmClvGrupo As Integer) '''''MÉTODO QUE LLENA EL LIST BOX DE LOS CAJEROS SIN AIGNAR
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clvSession", SqlDbType.BigInt, prmClvSession)
        BaseII.CreateMyParameter("@clvGrupo", SqlDbType.Int, prmClvGrupo)
        Me.lbxCajerosSinAsignar.DataSource = BaseII.ConsultaDT("uspConsultaCajeros")
    End Sub

    Private Sub uspConsultaCajerosComisionesTmp(ByVal prmClvSession As Long) '''''MÉTODO QUE LLENA EL LIST BOX DE LOS CAJEROS AIGNADOS
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clvSession", SqlDbType.BigInt, prmClvSession)
        Me.lbxCajerosAsignados.DataSource = BaseII.ConsultaDT("uspConsultaCajerosComisionesTmp")
    End Sub

    Private Sub uspInsertaCajerosComisionesTmp(ByVal prmClvSession As Long, ByVal prmClvUsuario As String, ByVal prmOp As Integer) '''MÉTODO QUE REALIZA LA INSERCIÓN DE LOS VALORES YA SEA UNO O TODOS
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clvSession", SqlDbType.BigInt, prmClvSession)
        BaseII.CreateMyParameter("@clvUsuario", SqlDbType.VarChar, prmClvUsuario, 5)
        BaseII.CreateMyParameter("@op", SqlDbType.Int, prmOp)
        BaseII.Inserta("uspInsertaCajerosComisionesTmp")
    End Sub

    Private Sub uspEliminaCajerosComisionesTmp(ByVal prmClvSession As Long, ByVal prmClvUsuario As String, ByVal prmOp As Integer) '''MÉTODO QUE REALIZA LA ELIMINACIÓN DE LOS VALORES YA SEA UNO O TODOS
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clvSession", SqlDbType.BigInt, prmClvSession)
        BaseII.CreateMyParameter("@clvUsuario", SqlDbType.VarChar, prmClvUsuario, 5)
        BaseII.CreateMyParameter("@op", SqlDbType.Int, prmOp)
        BaseII.Inserta("uspEliminaCajerosComisionesTmp")
    End Sub

    Private Function Dame_clv_session_Reportes() As Long ''''MÉTODO QUE OBTIENE UNA SESIÓN PARA MANDAR IMPRIMIR EL REPORTE
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", ParameterDirection.Output, SqlDbType.BigInt)
        BaseII.ProcedimientoOutPut("Dame_clv_session_Reportes")

        Dame_clv_session_Reportes = BaseII.dicoPar("@Clv_Session")
    End Function

    Private Sub actualizaListas(ByVal prmClvSession As Long)
        uspConsultaCajeros(prmClvSession, Me.cmbGruposVentas.SelectedValue) ''''SE ACTUALIZA EL LIST BOX DE LOS CAJEROS SIN ASIGNAR
        uspConsultaCajerosComisionesTmp(prmClvSession) ''''SE ACTUALIZA EL LIST BOX DE LOS CAJEROS ASIGNADOS
    End Sub

    Private Function uspConsultaGruposVentas() As DataTable
        BaseII.limpiaParametros()
        uspConsultaGruposVentas = BaseII.ConsultaDT("uspConsultaGruposVentas")
    End Function

    Private Sub btnAgregarUno_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregarUno.Click
        If Me.lbxCajerosSinAsignar.Items.Count > 0 Then
            If Me.lbxCajerosSinAsignar.SelectedValue.ToString.Length > 0 Then
                uspInsertaCajerosComisionesTmp(clvSessionReporte, Me.lbxCajerosSinAsignar.SelectedValue, 1) '''CON LA OPCIÓN 1 ES PARA MANDAR SÓLO UNO A LA LISTA
                actualizaListas(clvSessionReporte) ''''ACTUALIZAMOS LAS LISTAS
            Else
                MsgBox("Seleccione un Registro", MsgBoxStyle.Information)
            End If
        End If
    End Sub

    Private Sub btnAgregarTodos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregarTodos.Click
        uspInsertaCajerosComisionesTmp(clvSessionReporte, "TODOS", 2) '''CON LA OPCIÓN 2 ES PARA MANDAR TODOS A LA LISTA
        actualizaListas(clvSessionReporte) ''''ACTUALIZAMOS LAS LISTAS
    End Sub

    Private Sub btnQuitarUno_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQuitarUno.Click
        If Me.lbxCajerosAsignados.Items.Count > 0 Then
            If Me.lbxCajerosAsignados.SelectedValue.ToString.Length > 0 Then
                uspEliminaCajerosComisionesTmp(clvSessionReporte, Me.lbxCajerosAsignados.SelectedValue, 1) '''CON LA OPCIÓN 1 ES PARA QUITAR SÓLO UNO A LA LISTA
                actualizaListas(clvSessionReporte) ''''ACTUALIZAMOS LAS LISTAS
            Else
                MsgBox("Seleccione un Registro", MsgBoxStyle.Information)
            End If
        End If
    End Sub

    Private Sub btnQuitarTodos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQuitarTodos.Click
        uspEliminaCajerosComisionesTmp(clvSessionReporte, String.Empty, 2) '''CON LA OPCIÓN 2 ES PARA QUITAR TODOS DE LA LISTA
        actualizaListas(clvSessionReporte) ''''ACTUALIZAMOS LAS LISTAS
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If Me.lbxCajerosAsignados.Items.Count > 0 Then
            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
        Else
            Me.DialogResult = Windows.Forms.DialogResult.None
            MsgBox("Seleccione al Menos un Cajero de la Lsita", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub cmbGruposVentas_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbGruposVentas.SelectedIndexChanged
        Try
            uspConsultaCajeros(clvSessionReporte, Me.cmbGruposVentas.SelectedValue)
        Catch ex As Exception

        End Try
    End Sub
End Class