﻿
Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic
Imports System.IO
Imports System.Xml
Imports System.Collections


Public Class RepEquipoRecCajera

    Dim strXML As String = ""

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        'If (FrmFiltroFechas.ShowDialog() = Windows.Forms.DialogResult.OK) Then
        ReporteAparatoRecibidoCajero(generaXML(), dtpFechaInicio.Value, dtpFechaFin.Value)
        'End If
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub RepEquipoRecCajera_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        colorea(Me, Me.Name)

        BaseII.limpiaParametros()

        BaseII.CreateMyParameter("@OP", SqlDbType.Int, 5)

        Dim usuarios As DataTable = BaseII.ConsultaDT("MUESTRAUSUARIOS1")
        Me.cmbCajera.DisplayMember = "Nombre"
        Me.cmbCajera.ValueMember = "Clv_Usuario"
        Me.cmbCajera.DataSource = usuarios

    End Sub

    Private Function generaXML() As String

        strXML = ""

        Try

            Dim sw As New StringWriter()
            Dim w As New XmlTextWriter(sw)

            w.Formatting = Formatting.Indented

            w.WriteStartElement("Reporte")
            'w.WriteAttributeString("FechaInicio", dtpFechaInicio.Value)
            'w.WriteAttributeString("FechaFin", dtpFechaFin.Value)
            w.WriteAttributeString("Clv_usuario", cmbCajera.SelectedValue)
            w.WriteEndElement()
            w.Close()
            strXML = sw.ToString()

        Catch ex As Exception
            MsgBox(ex.Message)
            strXML = ""
        End Try

        Return strXML

    End Function

End Class