﻿Public Class FrmDatosComplementariosContrato
    Public ContratoAImprimir As Integer = 0
    Public Nombre As String
    Public Direccion As String
    Public Poblacion As String
    Public ImporteTotal As Double
    Public CantidadConLetra As String

   

#Region "Controles y Eventos"

    'btnSalir
    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        eErrorContrato = 1
        Me.Close()
        Me.Dispose()
    End Sub

    'btnGuardar
    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click

        Try
            If (ValidaCamposObligatorios()) Then
                If (Softv.BAL.DatosComplementariosContrato.Add(0, ContratoAImprimir, txtNoFolio.Text, txtCantidadNumerica.Text, txtCantidadConLetra.Text, txtSerieNumerada.Text, Now, txtComplementario01.Text, txtPorcentaje.Text, 0, 0)) > 0 Then
                    MsgBox("Guardado correctamente.", MsgBoxStyle.Information)
                    Me.Close()
                    Me.Dispose()
                End If
            Else
                MsgBox("Existen datos complementarios que son obligatorios.", MsgBoxStyle.Exclamation, "CAPTURA TODOS LOS DATOS")
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    'Load
    Private Sub FrmDatosComplementariosContrato_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        colorea(Me, Me.Name)
        Me.lblPanel.ForeColor = Color.Black

        'Cargamos los Datos del Cliente
        uspConsultaDatosParaPagare(ContratoAImprimir)

        Try
            txtNoFolio.Text = ContratoAImprimir
            txtNombreDelCliente.Text = GloEmpresa
            txtNombreDelDeudor.Text = Me.Nombre
            txtDireccionDelDeudor.Text = Me.Direccion
            txtPoblacion.Text = Me.Poblacion
            txtCantidadNumerica.Text = Me.ImporteTotal
            txtCantidadConLetra.Text = Me.CantidadConLetra
            txtDatosDeLaEmpresa.Text = GloDireccionEmpresa.ToUpper & ", " & GloCiudadEmpresa.ToUpper


        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

#End Region

#Region "GUI"

    'Función que debe de validar los campos Obligatorios del Formulario
    Private Function ValidaCamposObligatorios() As Boolean

        Dim Valido As Boolean = True

        Try
            txtComplementario01.BackColor = Color.White
            txtNoFolio.BackColor = Color.White
            txtPorcentaje.BackColor = Color.White
            txtSerieNumerada.BackColor = Color.White

            If (txtComplementario01.Text = "[Dato Obligatorio]" Or txtComplementario01.Text = "") Then
                txtComplementario01.BackColor = Color.Red
                txtComplementario01.Text = ""
                txtComplementario01.Focus()
                Valido = False
            End If
            If (txtNoFolio.Text = "[Dato Obligatorio]" Or txtNoFolio.Text = "") Then
                txtNoFolio.BackColor = Color.Red
                txtNoFolio.Text = ""
                txtNoFolio.Focus()
                Valido = False
            End If
            If (txtPorcentaje.Text = "[Dato Obligatorio]" Or txtPorcentaje.Text = "") Then
                txtPorcentaje.BackColor = Color.Red
                txtPorcentaje.Text = ""
                txtPorcentaje.Focus()
                Valido = False
            End If
            If (txtSerieNumerada.Text = "[Dato Obligatorio]" Or txtSerieNumerada.Text = "") Then
                txtSerieNumerada.BackColor = Color.Red
                txtSerieNumerada.Text = ""
                txtSerieNumerada.Focus()
                Valido = False
            End If




        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Valido = False
        End Try


        Return Valido

    End Function

    'Solo decimales
    Private Sub txtPorcentaje_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPorcentaje.KeyPress
        ValidaDecimal(e, Me.txtPorcentaje)
    End Sub

    'Solo enteros [txtSerieNumerada]
    Private Sub txtSerieNumerada_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSerieNumerada.KeyPress
        If e.KeyChar.IsDigit(e.KeyChar) Then
            e.Handled = False
        ElseIf e.KeyChar.IsControl(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub
    Private Sub txtComplementario01_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtComplementario01.Click
        txtComplementario01.SelectionStart = 0
        txtComplementario01.SelectionLength = txtComplementario01.Text.Length
    End Sub
    Private Sub txtSerieNumerada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSerieNumerada.Click
        txtSerieNumerada.SelectionStart = 0
        txtSerieNumerada.SelectionLength = txtSerieNumerada.Text.Length
    End Sub
    Private Sub txtPorcentaje_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPorcentaje.Click
        txtPorcentaje.SelectionStart = 0
        txtPorcentaje.SelectionLength = txtPorcentaje.Text.Length
    End Sub
    Private Sub txtNoFolio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNoFolio.Click
        txtNoFolio.SelectionStart = 0
        txtNoFolio.SelectionLength = txtNoFolio.Text.Length
    End Sub

    'Solo enteros [ txtNoFolio ]
    Private Sub txtNoFolio_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNoFolio.KeyPress
        If e.KeyChar.IsDigit(e.KeyChar) Then
            e.Handled = False
        ElseIf e.KeyChar.IsControl(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

#End Region


End Class