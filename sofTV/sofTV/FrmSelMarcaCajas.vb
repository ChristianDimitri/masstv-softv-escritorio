﻿Imports System.Data.SqlClient
Imports System.Text
Imports Microsoft.VisualBasic
Imports System.IO

Public Class FrmSelMarcaCajas

    Private Sub FrmSelMarcaCajas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.CMBLabel3.ForeColor = Color.LightSlateGray
        Me.Label1.ForeColor = Color.LightSlateGray
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None
        Me.SalirButton.Visible = False
        DimeSiTieneHd()
    End Sub

    Private Sub DimeSiTieneHd()
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("DimeSiTieneHd", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@CONTRATONET", SqlDbType.Int)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = ContratoNetCajasDig
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@TIENEHD", SqlDbType.Bit)
        PRM2.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM2)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            If PRM2.Value = True Then
                Me.HDRadioBut.Checked = True
                Me.NormalRadioBut.Enabled = False
                Me.HDRadioBut.Enabled = False
                MuestraMarcasAparatosPagare(3, 2)
            ElseIf PRM2.Value = False Then
                Me.NormalRadioBut.Checked = True
                Me.NormalRadioBut.Enabled = True
                Me.HDRadioBut.Enabled = True
                MuestraMarcasAparatosPagare(3, 1)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub MuestraMarcasAparatosPagare(ByVal TipServCajas As Integer, ByVal OpTipoCaja As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder

        StrSQL.Append("EXEC MuestraMarcasAparatosPagare ")
        StrSQL.Append(CStr(TipServCajas) & ", ")
        StrSQL.Append(CStr(OpTipoCaja))

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(StrSQL.ToString(), CON)
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            AparatosComboBox.DataSource = BS.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub ActualizaRelClientesAparatos()
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("ActualizaRelClientesAparatos", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@CONTRATONET", SqlDbType.Int)
        Dim PRM2 As New SqlParameter("@CLV_APARATO", SqlDbType.Int)

        PRM1.Direction = ParameterDirection.Input
        PRM2.Direction = ParameterDirection.Input

        PRM1.Value = ContratoNetCajasDig
        PRM2.Value = Me.AparatosComboBox.SelectedValue

        CMD.Parameters.Add(PRM1)
        CMD.Parameters.Add(PRM2)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            'MsgBox("Registro Actualizado Correctamente", MsgBoxStyle.Information)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub NormalRadioBut_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NormalRadioBut.CheckedChanged
        Me.AparatosComboBox.SelectedValue = 0
        MuestraMarcasAparatosPagare(3, 1)
    End Sub

    Private Sub HDRadioBut_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles HDRadioBut.CheckedChanged
        Me.AparatosComboBox.SelectedValue = 0
        MuestraMarcasAparatosPagare(3, 2)
    End Sub

    Private Sub SalirButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalirButton.Click
        GloBloqueaDetalle = True
        BNDCAJAS = True
        Me.Close()
    End Sub

    Private Sub AceptarCButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AceptarCButton.Click
        If IsNumeric(Me.AparatosComboBox.SelectedValue) = False Then
            MsgBox("¡Seleccione al menos una marca!", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If Me.AparatosComboBox.Text.Length = 0 Then
            MsgBox("¡Seleccione al menos una marca!", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        ActualizaRelClientesAparatos()

        If (GLOTRABAJO = "CCAJA" Or GLOTRABAJO = "IAPAR" Or GLOTRABAJO = "RIAPA") Or (GLOTRABAJO = "CCAJA6" Or GLOTRABAJO = "IAPAR6" Or GLOTRABAJO = "RIAPA6") Then
            FrmSelCablemodemsDisponibles.ShowDialog()
        ElseIf GLOTRABAJO = "ICAJA" Or GLOTRABAJO = "ICAJA6" Then
            'FrmSelCajasDisponiblesvb.Show()
            BNDCAJAS = True
        End If
        Me.Close()
    End Sub
End Class