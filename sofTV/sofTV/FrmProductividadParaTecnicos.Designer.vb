﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmProductividadParaTecnicos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim CMBRangoSuperiorLabel As System.Windows.Forms.Label
        Dim CMBRangoInferiorLabel As System.Windows.Forms.Label
        Dim CMBImporte As System.Windows.Forms.Label
        Me.PuntosFinalesTextBox = New System.Windows.Forms.TextBox()
        Me.PuntosInicialesTextBox = New System.Windows.Forms.TextBox()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.ImporteTextBox = New System.Windows.Forms.TextBox()
        Me.btnGuardar = New System.Windows.Forms.Button()
        CMBRangoSuperiorLabel = New System.Windows.Forms.Label()
        CMBRangoInferiorLabel = New System.Windows.Forms.Label()
        CMBImporte = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'CMBRangoSuperiorLabel
        '
        CMBRangoSuperiorLabel.AutoSize = True
        CMBRangoSuperiorLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBRangoSuperiorLabel.Location = New System.Drawing.Point(77, 58)
        CMBRangoSuperiorLabel.Name = "CMBRangoSuperiorLabel"
        CMBRangoSuperiorLabel.Size = New System.Drawing.Size(106, 15)
        CMBRangoSuperiorLabel.TabIndex = 132
        CMBRangoSuperiorLabel.Text = "Puntos Finales:"
        '
        'CMBRangoInferiorLabel
        '
        CMBRangoInferiorLabel.AutoSize = True
        CMBRangoInferiorLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBRangoInferiorLabel.Location = New System.Drawing.Point(77, 31)
        CMBRangoInferiorLabel.Name = "CMBRangoInferiorLabel"
        CMBRangoInferiorLabel.Size = New System.Drawing.Size(113, 15)
        CMBRangoInferiorLabel.TabIndex = 131
        CMBRangoInferiorLabel.Text = "Puntos Iniciales:"
        '
        'PuntosFinalesTextBox
        '
        Me.PuntosFinalesTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PuntosFinalesTextBox.Location = New System.Drawing.Point(196, 52)
        Me.PuntosFinalesTextBox.Name = "PuntosFinalesTextBox"
        Me.PuntosFinalesTextBox.Size = New System.Drawing.Size(146, 21)
        Me.PuntosFinalesTextBox.TabIndex = 2
        '
        'PuntosInicialesTextBox
        '
        Me.PuntosInicialesTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PuntosInicialesTextBox.Location = New System.Drawing.Point(196, 25)
        Me.PuntosInicialesTextBox.Name = "PuntosInicialesTextBox"
        Me.PuntosInicialesTextBox.Size = New System.Drawing.Size(146, 21)
        Me.PuntosInicialesTextBox.TabIndex = 1
        '
        'btnSalir
        '
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(271, 120)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(136, 36)
        Me.btnSalir.TabIndex = 5
        Me.btnSalir.Text = "&SALIR"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'ImporteTextBox
        '
        Me.ImporteTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ImporteTextBox.Location = New System.Drawing.Point(196, 79)
        Me.ImporteTextBox.Name = "ImporteTextBox"
        Me.ImporteTextBox.Size = New System.Drawing.Size(146, 21)
        Me.ImporteTextBox.TabIndex = 3
        '
        'CMBImporte
        '
        CMBImporte.AutoSize = True
        CMBImporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBImporte.Location = New System.Drawing.Point(77, 85)
        CMBImporte.Name = "CMBImporte"
        CMBImporte.Size = New System.Drawing.Size(60, 15)
        CMBImporte.TabIndex = 137
        CMBImporte.Text = "Importe:"
        '
        'btnGuardar
        '
        Me.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.Location = New System.Drawing.Point(129, 120)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(136, 36)
        Me.btnGuardar.TabIndex = 4
        Me.btnGuardar.Text = "&GUARDAR"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'FrmProductividadParaTecnicos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(419, 168)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.ImporteTextBox)
        Me.Controls.Add(CMBImporte)
        Me.Controls.Add(Me.PuntosFinalesTextBox)
        Me.Controls.Add(CMBRangoSuperiorLabel)
        Me.Controls.Add(Me.PuntosInicialesTextBox)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(CMBRangoInferiorLabel)
        Me.Name = "FrmProductividadParaTecnicos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Puntos De Productividad Para Tecnicos"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PuntosFinalesTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PuntosInicialesTextBox As System.Windows.Forms.TextBox
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents ImporteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
End Class
