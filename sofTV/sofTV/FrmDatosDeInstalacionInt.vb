﻿Public Class FrmDatosDeInstalacionInt

    Private Sub FrmDatosDeInstalacionInt_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        SP_DameDatosInstalacionInternet(0, 0, GloContratonet)
        If GloTipoUsuario = 40 Then
            GroupBox1.Enabled = True
            Button1.Enabled = True
        Else
            GroupBox1.Enabled = False
            Button1.Enabled = False
        End If
    End Sub

    Private Sub SP_DameDatosInstalacionInternet(oClv_Orden As Long, oClave As Long, oContratonet As Long)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
        BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)
        BaseII.CreateMyParameter("@Contratonet", SqlDbType.BigInt, oContratonet)
        BaseII.CreateMyParameter("@Ip", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.CreateMyParameter("@Pin", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.CreateMyParameter("@DistanciaConfig", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.CreateMyParameter("@TorreEnlace", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.CreateMyParameter("@SectorialEnlace", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.CreateMyParameter("@IntesidadSeñal", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.CreateMyParameter("@VelSubida", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.CreateMyParameter("@VelBajada", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.ProcedimientoOutPut("SP_DameDatosInstalacionInternet")
        TextBox1.Text = BaseII.dicoPar("@Ip").ToString
        TextBox2.Text = BaseII.dicoPar("@Pin").ToString
        TextBox3.Text = BaseII.dicoPar("@DistanciaConfig").ToString
        TextBox5.Text = BaseII.dicoPar("@TorreEnlace").ToString
        TextBox4.Text = BaseII.dicoPar("@SectorialEnlace").ToString
        TextBox8.Text = BaseII.dicoPar("@IntesidadSeñal").ToString
        TextBox7.Text = BaseII.dicoPar("@VelSubida").ToString
        TextBox6.Text = BaseII.dicoPar("@VelBajada").ToString
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        If TextBox1.Text = "" Or TextBox2.Text = "" Or TextBox3.Text = "" Or TextBox4.Text = "" Or TextBox5.Text = "" Or TextBox6.Text = "" Or TextBox7.Text = "" Or TextBox8.Text = "" Then
            MsgBox("Debe llenar todos los campos", MsgBoxStyle.Information, "Información")
            Exit Sub
        End If
        SP_GuardaDatosInstalacionInternet(0, 0, "", GloContratonet)
        MsgBox("Modificado con exitó")
        Me.Close()
    End Sub

    Private Sub SP_GuardaDatosInstalacionInternet(oClv_Orden As Long, oClave As Long, oTrabajo As String, oContratonet As Long)
        '@Clave bigint=0,@Trabajo varchar(10)='',@Clv_Orden bigint=0,@Contratonet Bigint=0,@Clv_Aparato bigint=0,@Opcion varchar(10)=''
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
        BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)
        BaseII.CreateMyParameter("@Trabajo", SqlDbType.VarChar, oTrabajo, 50)
        BaseII.CreateMyParameter("@Contratonet", SqlDbType.BigInt, oContratonet)
        BaseII.CreateMyParameter("@Ip", SqlDbType.VarChar, TextBox1.Text, 50)
        BaseII.CreateMyParameter("@Pin", SqlDbType.VarChar, TextBox2.Text, 50)
        BaseII.CreateMyParameter("@DistanciaConfig", SqlDbType.VarChar, TextBox3.Text, 50)
        BaseII.CreateMyParameter("@TorreEnlace", SqlDbType.VarChar, TextBox5.Text, 50)
        BaseII.CreateMyParameter("@SectorialEnlace", SqlDbType.VarChar, TextBox4.Text, 50)
        BaseII.CreateMyParameter("@IntesidadSeñal", SqlDbType.VarChar, TextBox8.Text, 50)
        BaseII.CreateMyParameter("@VelSubida", SqlDbType.VarChar, TextBox7.Text, 50)
        BaseII.CreateMyParameter("@VelBajada", SqlDbType.VarChar, TextBox6.Text, 50)
        BaseII.Inserta("SP_GuardaDatosInstalacionInternet")
    End Sub

    Private Sub TextBox1_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        EventArgs.Handled = Fg_SoloNumeros(EventArgs.KeyChar, TextBox1.Text & CChar(EventArgs.KeyChar))
    End Sub

    Private Sub TextBox1_TextChanged(sender As System.Object, e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub

    Function Fg_SoloNumeros(ByVal Digito As String, ByVal Texto As String) As Boolean
        Dim Dt_Entero As Integer = CInt(Asc(Digito))
        If Dt_Entero = 8 Then
            Fg_SoloNumeros = False
        Else
            If InStr("1234567890.", Digito) = 0 Then
                Fg_SoloNumeros = True
                'ElseIf IsNumeric(Texto) = True Then
                '    Fg_SoloNumeros = False
                'ElseIf IsNumeric(Texto) = False Then
                '    Fg_SoloNumeros = True
            End If
        End If
        Return Fg_SoloNumeros
    End Function

End Class