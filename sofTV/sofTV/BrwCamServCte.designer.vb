<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwCamServCte
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ContratoLabel As System.Windows.Forms.Label
        Dim NombreLabel As System.Windows.Forms.Label
        Dim StatusLabel As System.Windows.Forms.Label
        Dim Fecha_SolLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.TxtTarjeta = New System.Windows.Forms.TextBox()
        Me.TxtSetUpBox = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.AMaternoTextBox = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.APaternoTextBox = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ConCambioServClienteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Fecha_SolLabel1 = New System.Windows.Forms.Label()
        Me.StatusLabel2 = New System.Windows.Forms.Label()
        Me.Servicio_ActualLabel2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ContratoLabel2 = New System.Windows.Forms.Label()
        Me.NombreLabel2 = New System.Windows.Forms.Label()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.ConCambioServClienteDataGridView = New System.Windows.Forms.DataGridView()
        Me.Clave = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Contrato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Servicio_Anterior = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Servicio_Actual = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fecha_Sol = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Realizar = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Monto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Servicio_AnteriorLabel2 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.ClaveTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_TipSerTextBox = New System.Windows.Forms.TextBox()
        Me.RealizarTextBox = New System.Windows.Forms.TextBox()
        Me.ConCambioServClienteTableAdapter = New sofTV.DataSetEricTableAdapters.ConCambioServClienteTableAdapter()
        Me.BorCambioServClienteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorCambioServClienteTableAdapter = New sofTV.DataSetEricTableAdapters.BorCambioServClienteTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        ContratoLabel = New System.Windows.Forms.Label()
        NombreLabel = New System.Windows.Forms.Label()
        StatusLabel = New System.Windows.Forms.Label()
        Fecha_SolLabel = New System.Windows.Forms.Label()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.ConCambioServClienteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConCambioServClienteDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorCambioServClienteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ContratoLabel
        '
        ContratoLabel.AutoSize = True
        ContratoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ContratoLabel.ForeColor = System.Drawing.Color.White
        ContratoLabel.Location = New System.Drawing.Point(11, 31)
        ContratoLabel.Name = "ContratoLabel"
        ContratoLabel.Size = New System.Drawing.Size(65, 15)
        ContratoLabel.TabIndex = 13
        ContratoLabel.Text = "Contrato:"
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.ForeColor = System.Drawing.Color.White
        NombreLabel.Location = New System.Drawing.Point(11, 63)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(62, 15)
        NombreLabel.TabIndex = 14
        NombreLabel.Text = "Nombre:"
        '
        'StatusLabel
        '
        StatusLabel.AutoSize = True
        StatusLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        StatusLabel.ForeColor = System.Drawing.Color.White
        StatusLabel.Location = New System.Drawing.Point(11, 235)
        StatusLabel.Name = "StatusLabel"
        StatusLabel.Size = New System.Drawing.Size(67, 15)
        StatusLabel.TabIndex = 18
        StatusLabel.Text = "Proceso :"
        '
        'Fecha_SolLabel
        '
        Fecha_SolLabel.AutoSize = True
        Fecha_SolLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_SolLabel.ForeColor = System.Drawing.Color.White
        Fecha_SolLabel.Location = New System.Drawing.Point(8, 274)
        Fecha_SolLabel.Name = "Fecha_SolLabel"
        Fecha_SolLabel.Size = New System.Drawing.Size(134, 15)
        Fecha_SolLabel.TabIndex = 12
        Fecha_SolLabel.Text = "Fecha de Solicitud :"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Location = New System.Drawing.Point(-8, -3)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TxtTarjeta)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TxtSetUpBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label15)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label14)
        Me.SplitContainer1.Panel1.Controls.Add(Me.AMaternoTextBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label9)
        Me.SplitContainer1.Panel1.Controls.Add(Me.APaternoTextBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label12)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button6)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.AutoScroll = True
        Me.SplitContainer1.Panel2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer1.Panel2.Controls.Add(Me.ConCambioServClienteDataGridView)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Servicio_AnteriorLabel2)
        Me.SplitContainer1.Size = New System.Drawing.Size(872, 761)
        Me.SplitContainer1.SplitterDistance = 274
        Me.SplitContainer1.TabIndex = 0
        '
        'Button3
        '
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(165, 386)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(80, 27)
        Me.Button3.TabIndex = 69
        Me.Button3.Text = "Buscar"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'TxtTarjeta
        '
        Me.TxtTarjeta.BackColor = System.Drawing.SystemColors.Window
        Me.TxtTarjeta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TxtTarjeta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtTarjeta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtTarjeta.Location = New System.Drawing.Point(31, 359)
        Me.TxtTarjeta.Name = "TxtTarjeta"
        Me.TxtTarjeta.Size = New System.Drawing.Size(216, 21)
        Me.TxtTarjeta.TabIndex = 68
        '
        'TxtSetUpBox
        '
        Me.TxtSetUpBox.BackColor = System.Drawing.SystemColors.Window
        Me.TxtSetUpBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TxtSetUpBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtSetUpBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtSetUpBox.Location = New System.Drawing.Point(31, 315)
        Me.TxtSetUpBox.Name = "TxtSetUpBox"
        Me.TxtSetUpBox.Size = New System.Drawing.Size(216, 21)
        Me.TxtSetUpBox.TabIndex = 67
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(27, 341)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(124, 15)
        Me.Label15.TabIndex = 66
        Me.Label15.Text = "No. Serie Tarjeta :"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(24, 297)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(86, 15)
        Me.Label14.TabIndex = 65
        Me.Label14.Text = "Set Up Box :"
        '
        'AMaternoTextBox
        '
        Me.AMaternoTextBox.BackColor = System.Drawing.SystemColors.Window
        Me.AMaternoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AMaternoTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.AMaternoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AMaternoTextBox.Location = New System.Drawing.Point(30, 249)
        Me.AMaternoTextBox.Name = "AMaternoTextBox"
        Me.AMaternoTextBox.Size = New System.Drawing.Size(215, 21)
        Me.AMaternoTextBox.TabIndex = 60
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(28, 231)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(124, 15)
        Me.Label9.TabIndex = 62
        Me.Label9.Text = "Apellido Materno :"
        '
        'APaternoTextBox
        '
        Me.APaternoTextBox.BackColor = System.Drawing.SystemColors.Window
        Me.APaternoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.APaternoTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.APaternoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.APaternoTextBox.Location = New System.Drawing.Point(29, 207)
        Me.APaternoTextBox.Name = "APaternoTextBox"
        Me.APaternoTextBox.Size = New System.Drawing.Size(216, 21)
        Me.APaternoTextBox.TabIndex = 59
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(28, 189)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(121, 15)
        Me.Label12.TabIndex = 61
        Me.Label12.Text = "Apellido Paterno :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(25, 145)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(63, 16)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "Nombre"
        '
        'Button6
        '
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(165, 276)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(80, 27)
        Me.Button6.TabIndex = 11
        Me.Button6.Text = "Buscar"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'TextBox2
        '
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(28, 164)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(214, 22)
        Me.TextBox2.TabIndex = 10
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(24, 62)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(66, 16)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Contrato"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Fecha_SolLabel1)
        Me.Panel1.Controls.Add(Fecha_SolLabel)
        Me.Panel1.Controls.Add(StatusLabel)
        Me.Panel1.Controls.Add(Me.StatusLabel2)
        Me.Panel1.Controls.Add(Me.Servicio_ActualLabel2)
        Me.Panel1.Controls.Add(NombreLabel)
        Me.Panel1.Controls.Add(ContratoLabel)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.ContratoLabel2)
        Me.Panel1.Controls.Add(Me.NombreLabel2)
        Me.Panel1.Location = New System.Drawing.Point(17, 419)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(255, 339)
        Me.Panel1.TabIndex = 8
        '
        'Label1
        '
        Me.Label1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Servicio_Actual", True))
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(11, 197)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(236, 23)
        Me.Label1.TabIndex = 20
        '
        'ConCambioServClienteBindingSource
        '
        Me.ConCambioServClienteBindingSource.DataMember = "ConCambioServCliente"
        Me.ConCambioServClienteBindingSource.DataSource = Me.DataSetEric
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(10, 173)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(120, 15)
        Me.Label6.TabIndex = 19
        Me.Label6.Text = "Servicio Anterior :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(11, 114)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(109, 15)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "Servicio Actual :"
        '
        'Fecha_SolLabel1
        '
        Me.Fecha_SolLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Fecha_Sol", True))
        Me.Fecha_SolLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fecha_SolLabel1.ForeColor = System.Drawing.Color.White
        Me.Fecha_SolLabel1.Location = New System.Drawing.Point(3, 293)
        Me.Fecha_SolLabel1.Name = "Fecha_SolLabel1"
        Me.Fecha_SolLabel1.Size = New System.Drawing.Size(240, 23)
        Me.Fecha_SolLabel1.TabIndex = 13
        '
        'StatusLabel2
        '
        Me.StatusLabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Status", True))
        Me.StatusLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusLabel2.ForeColor = System.Drawing.Color.White
        Me.StatusLabel2.Location = New System.Drawing.Point(83, 232)
        Me.StatusLabel2.Name = "StatusLabel2"
        Me.StatusLabel2.Size = New System.Drawing.Size(155, 23)
        Me.StatusLabel2.TabIndex = 12
        Me.StatusLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Servicio_ActualLabel2
        '
        Me.Servicio_ActualLabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Servicio_Actual", True))
        Me.Servicio_ActualLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Servicio_ActualLabel2.ForeColor = System.Drawing.Color.White
        Me.Servicio_ActualLabel2.Location = New System.Drawing.Point(11, 138)
        Me.Servicio_ActualLabel2.Name = "Servicio_ActualLabel2"
        Me.Servicio_ActualLabel2.Size = New System.Drawing.Size(236, 23)
        Me.Servicio_ActualLabel2.TabIndex = 10
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(6, 4)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(147, 20)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Datos del Cliente"
        '
        'ContratoLabel2
        '
        Me.ContratoLabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Contrato", True))
        Me.ContratoLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContratoLabel2.ForeColor = System.Drawing.Color.White
        Me.ContratoLabel2.Location = New System.Drawing.Point(91, 28)
        Me.ContratoLabel2.Name = "ContratoLabel2"
        Me.ContratoLabel2.Size = New System.Drawing.Size(147, 23)
        Me.ContratoLabel2.TabIndex = 2
        Me.ContratoLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'NombreLabel2
        '
        Me.NombreLabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Nombre", True))
        Me.NombreLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreLabel2.ForeColor = System.Drawing.Color.White
        Me.NombreLabel2.Location = New System.Drawing.Point(11, 78)
        Me.NombreLabel2.Name = "NombreLabel2"
        Me.NombreLabel2.Size = New System.Drawing.Size(241, 27)
        Me.NombreLabel2.TabIndex = 4
        '
        'Button5
        '
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(27, 109)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(80, 27)
        Me.Button5.TabIndex = 7
        Me.Button5.Text = "Buscar"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(13, 26)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(195, 24)
        Me.CMBLabel1.TabIndex = 2
        Me.CMBLabel1.Text = "Buscar Cliente Por :"
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(27, 81)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(142, 22)
        Me.TextBox1.TabIndex = 3
        '
        'ConCambioServClienteDataGridView
        '
        Me.ConCambioServClienteDataGridView.AllowUserToAddRows = False
        Me.ConCambioServClienteDataGridView.AllowUserToDeleteRows = False
        Me.ConCambioServClienteDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ConCambioServClienteDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.ConCambioServClienteDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clave, Me.Contrato, Me.Nombre, Me.Servicio_Anterior, Me.Servicio_Actual, Me.Fecha_Sol, Me.Realizar, Me.Monto})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ConCambioServClienteDataGridView.DefaultCellStyle = DataGridViewCellStyle3
        Me.ConCambioServClienteDataGridView.GridColor = System.Drawing.Color.WhiteSmoke
        Me.ConCambioServClienteDataGridView.Location = New System.Drawing.Point(0, 3)
        Me.ConCambioServClienteDataGridView.Name = "ConCambioServClienteDataGridView"
        Me.ConCambioServClienteDataGridView.ReadOnly = True
        Me.ConCambioServClienteDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.ConCambioServClienteDataGridView.Size = New System.Drawing.Size(591, 755)
        Me.ConCambioServClienteDataGridView.TabIndex = 0
        '
        'Clave
        '
        Me.Clave.DataPropertyName = "Clave"
        Me.Clave.HeaderText = "Clave"
        Me.Clave.Name = "Clave"
        Me.Clave.ReadOnly = True
        '
        'Contrato
        '
        Me.Contrato.DataPropertyName = "Contrato"
        Me.Contrato.HeaderText = "Contrato"
        Me.Contrato.Name = "Contrato"
        Me.Contrato.ReadOnly = True
        '
        'Nombre
        '
        Me.Nombre.DataPropertyName = "Nombre"
        Me.Nombre.HeaderText = "Nombre"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.ReadOnly = True
        Me.Nombre.Width = 200
        '
        'Servicio_Anterior
        '
        Me.Servicio_Anterior.DataPropertyName = "Servicio_Anterior"
        Me.Servicio_Anterior.HeaderText = "Servicio Anterior"
        Me.Servicio_Anterior.Name = "Servicio_Anterior"
        Me.Servicio_Anterior.ReadOnly = True
        Me.Servicio_Anterior.Width = 150
        '
        'Servicio_Actual
        '
        Me.Servicio_Actual.DataPropertyName = "Servicio_Actual"
        Me.Servicio_Actual.HeaderText = "Servicio Actual"
        Me.Servicio_Actual.Name = "Servicio_Actual"
        Me.Servicio_Actual.ReadOnly = True
        Me.Servicio_Actual.Width = 150
        '
        'Fecha_Sol
        '
        Me.Fecha_Sol.DataPropertyName = "Fecha_Sol"
        Me.Fecha_Sol.HeaderText = "Fecha Sol"
        Me.Fecha_Sol.Name = "Fecha_Sol"
        Me.Fecha_Sol.ReadOnly = True
        '
        'Realizar
        '
        Me.Realizar.DataPropertyName = "Realizar"
        Me.Realizar.HeaderText = "Realizar"
        Me.Realizar.Name = "Realizar"
        Me.Realizar.ReadOnly = True
        '
        'Monto
        '
        Me.Monto.DataPropertyName = "Monto"
        DataGridViewCellStyle2.Format = "C2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.Monto.DefaultCellStyle = DataGridViewCellStyle2
        Me.Monto.HeaderText = "Monto"
        Me.Monto.Name = "Monto"
        Me.Monto.ReadOnly = True
        '
        'Servicio_AnteriorLabel2
        '
        Me.Servicio_AnteriorLabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Servicio_Anterior", True))
        Me.Servicio_AnteriorLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Servicio_AnteriorLabel2.ForeColor = System.Drawing.Color.White
        Me.Servicio_AnteriorLabel2.Location = New System.Drawing.Point(-134, 531)
        Me.Servicio_AnteriorLabel2.Name = "Servicio_AnteriorLabel2"
        Me.Servicio_AnteriorLabel2.Size = New System.Drawing.Size(111, 23)
        Me.Servicio_AnteriorLabel2.TabIndex = 8
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(870, 17)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "&NUEVO"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(870, 59)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(136, 36)
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "&ELIMINAR"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(870, 673)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(136, 36)
        Me.Button4.TabIndex = 6
        Me.Button4.Text = "&SALIR"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'ClaveTextBox
        '
        Me.ClaveTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Clave", True))
        Me.ClaveTextBox.Location = New System.Drawing.Point(923, 70)
        Me.ClaveTextBox.Name = "ClaveTextBox"
        Me.ClaveTextBox.ReadOnly = True
        Me.ClaveTextBox.Size = New System.Drawing.Size(10, 20)
        Me.ClaveTextBox.TabIndex = 2
        Me.ClaveTextBox.TabStop = False
        '
        'Clv_TipSerTextBox
        '
        Me.Clv_TipSerTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Clv_TipSer", True))
        Me.Clv_TipSerTextBox.Location = New System.Drawing.Point(939, 70)
        Me.Clv_TipSerTextBox.Name = "Clv_TipSerTextBox"
        Me.Clv_TipSerTextBox.ReadOnly = True
        Me.Clv_TipSerTextBox.Size = New System.Drawing.Size(10, 20)
        Me.Clv_TipSerTextBox.TabIndex = 4
        Me.Clv_TipSerTextBox.TabStop = False
        '
        'RealizarTextBox
        '
        Me.RealizarTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCambioServClienteBindingSource, "Realizar", True))
        Me.RealizarTextBox.Location = New System.Drawing.Point(955, 70)
        Me.RealizarTextBox.Name = "RealizarTextBox"
        Me.RealizarTextBox.ReadOnly = True
        Me.RealizarTextBox.Size = New System.Drawing.Size(10, 20)
        Me.RealizarTextBox.TabIndex = 6
        Me.RealizarTextBox.TabStop = False
        '
        'ConCambioServClienteTableAdapter
        '
        Me.ConCambioServClienteTableAdapter.ClearBeforeFill = True
        '
        'BorCambioServClienteBindingSource
        '
        Me.BorCambioServClienteBindingSource.DataMember = "BorCambioServCliente"
        Me.BorCambioServClienteBindingSource.DataSource = Me.DataSetEric
        '
        'BorCambioServClienteTableAdapter
        '
        Me.BorCambioServClienteTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'BrwCamServCte
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1016, 758)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.RealizarTextBox)
        Me.Controls.Add(Me.Clv_TipSerTextBox)
        Me.Controls.Add(Me.ClaveTextBox)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Name = "BrwCamServCte"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cambio de Servicio"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.ConCambioServClienteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConCambioServClienteDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorCambioServClienteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents ConCambioServClienteBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConCambioServClienteTableAdapter As sofTV.DataSetEricTableAdapters.ConCambioServClienteTableAdapter
    Friend WithEvents ConCambioServClienteDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents ClaveTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_TipSerTextBox As System.Windows.Forms.TextBox
    Friend WithEvents RealizarTextBox As System.Windows.Forms.TextBox
    Friend WithEvents StatusLabel2 As System.Windows.Forms.Label
    Friend WithEvents Servicio_ActualLabel2 As System.Windows.Forms.Label
    Friend WithEvents Servicio_AnteriorLabel2 As System.Windows.Forms.Label
    Friend WithEvents ContratoLabel2 As System.Windows.Forms.Label
    Friend WithEvents NombreLabel2 As System.Windows.Forms.Label
    Friend WithEvents Fecha_SolLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents BorCambioServClienteBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorCambioServClienteTableAdapter As sofTV.DataSetEricTableAdapters.BorCambioServClienteTableAdapter
    Friend WithEvents AMaternoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents APaternoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents TxtTarjeta As System.Windows.Forms.TextBox
    Friend WithEvents TxtSetUpBox As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Clave As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Contrato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Servicio_Anterior As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Servicio_Actual As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fecha_Sol As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Realizar As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Monto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
