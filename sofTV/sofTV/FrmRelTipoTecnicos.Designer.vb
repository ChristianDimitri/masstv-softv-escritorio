﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRelTipoTecnicos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.btnBuscarClave = New System.Windows.Forms.Button()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.CMBlblNombre = New System.Windows.Forms.Label()
        Me.txtClave = New System.Windows.Forms.TextBox()
        Me.CMBlblClave = New System.Windows.Forms.Label()
        Me.CMBlblBuscarTecnico = New System.Windows.Forms.Label()
        Me.dgvTipoTecnicos = New System.Windows.Forms.DataGridView()
        Me.colIdRel = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colClvTecnico = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colNombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colEsCable = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.colEsAntena = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.cbxCable = New System.Windows.Forms.CheckBox()
        Me.cbxAntena = New System.Windows.Forms.CheckBox()
        Me.CMBlblLeyenda = New System.Windows.Forms.Label()
        CType(Me.dgvTipoTecnicos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnBuscarClave
        '
        Me.btnBuscarClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscarClave.Location = New System.Drawing.Point(580, 40)
        Me.btnBuscarClave.Name = "btnBuscarClave"
        Me.btnBuscarClave.Size = New System.Drawing.Size(89, 31)
        Me.btnBuscarClave.TabIndex = 29
        Me.btnBuscarClave.Text = "Buscar"
        Me.btnBuscarClave.UseVisualStyleBackColor = True
        '
        'txtNombre
        '
        Me.txtNombre.BackColor = System.Drawing.Color.LightGray
        Me.txtNombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNombre.Location = New System.Drawing.Point(262, 47)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(250, 24)
        Me.txtNombre.TabIndex = 26
        '
        'CMBlblNombre
        '
        Me.CMBlblNombre.AutoSize = True
        Me.CMBlblNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblNombre.Location = New System.Drawing.Point(261, 29)
        Me.CMBlblNombre.Name = "CMBlblNombre"
        Me.CMBlblNombre.Size = New System.Drawing.Size(66, 15)
        Me.CMBlblNombre.TabIndex = 25
        Me.CMBlblNombre.Text = "Nombre :"
        '
        'txtClave
        '
        Me.txtClave.BackColor = System.Drawing.Color.LightGray
        Me.txtClave.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtClave.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClave.Location = New System.Drawing.Point(51, 47)
        Me.txtClave.Name = "txtClave"
        Me.txtClave.Size = New System.Drawing.Size(124, 24)
        Me.txtClave.TabIndex = 23
        '
        'CMBlblClave
        '
        Me.CMBlblClave.AutoSize = True
        Me.CMBlblClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblClave.Location = New System.Drawing.Point(48, 29)
        Me.CMBlblClave.Name = "CMBlblClave"
        Me.CMBlblClave.Size = New System.Drawing.Size(50, 15)
        Me.CMBlblClave.TabIndex = 22
        Me.CMBlblClave.Text = "Clave :"
        '
        'CMBlblBuscarTecnico
        '
        Me.CMBlblBuscarTecnico.AutoSize = True
        Me.CMBlblBuscarTecnico.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblBuscarTecnico.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBlblBuscarTecnico.Location = New System.Drawing.Point(48, 8)
        Me.CMBlblBuscarTecnico.Name = "CMBlblBuscarTecnico"
        Me.CMBlblBuscarTecnico.Size = New System.Drawing.Size(152, 16)
        Me.CMBlblBuscarTecnico.TabIndex = 1
        Me.CMBlblBuscarTecnico.Text = "Buscar Técnico Por :"
        '
        'dgvTipoTecnicos
        '
        Me.dgvTipoTecnicos.AllowUserToAddRows = False
        Me.dgvTipoTecnicos.AllowUserToDeleteRows = False
        Me.dgvTipoTecnicos.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvTipoTecnicos.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvTipoTecnicos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTipoTecnicos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIdRel, Me.colClvTecnico, Me.colNombre, Me.colEsCable, Me.colEsAntena})
        Me.dgvTipoTecnicos.Location = New System.Drawing.Point(49, 122)
        Me.dgvTipoTecnicos.Name = "dgvTipoTecnicos"
        Me.dgvTipoTecnicos.Size = New System.Drawing.Size(620, 472)
        Me.dgvTipoTecnicos.TabIndex = 1
        '
        'colIdRel
        '
        Me.colIdRel.DataPropertyName = "IdRel"
        Me.colIdRel.HeaderText = "Id"
        Me.colIdRel.Name = "colIdRel"
        Me.colIdRel.Visible = False
        '
        'colClvTecnico
        '
        Me.colClvTecnico.DataPropertyName = "Clave"
        Me.colClvTecnico.HeaderText = "Id Técnico"
        Me.colClvTecnico.Name = "colClvTecnico"
        Me.colClvTecnico.ReadOnly = True
        '
        'colNombre
        '
        Me.colNombre.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colNombre.DataPropertyName = "nombre"
        Me.colNombre.HeaderText = "Nombre"
        Me.colNombre.Name = "colNombre"
        Me.colNombre.ReadOnly = True
        '
        'colEsCable
        '
        Me.colEsCable.DataPropertyName = "EsCable"
        Me.colEsCable.HeaderText = "Cable"
        Me.colEsCable.Name = "colEsCable"
        Me.colEsCable.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colEsCable.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'colEsAntena
        '
        Me.colEsAntena.DataPropertyName = "EsAntena"
        Me.colEsAntena.HeaderText = "Antena"
        Me.colEsAntena.Name = "colEsAntena"
        '
        'btnGuardar
        '
        Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.Location = New System.Drawing.Point(49, 624)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(123, 43)
        Me.btnGuardar.TabIndex = 2
        Me.btnGuardar.Text = "ACEPTAR"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(546, 624)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(123, 43)
        Me.btnSalir.TabIndex = 3
        Me.btnSalir.Text = "CANCELAR"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'cbxCable
        '
        Me.cbxCable.AutoSize = True
        Me.cbxCable.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxCable.Location = New System.Drawing.Point(261, 624)
        Me.cbxCable.Name = "cbxCable"
        Me.cbxCable.Size = New System.Drawing.Size(182, 19)
        Me.cbxCable.TabIndex = 32
        Me.cbxCable.Text = "Seleccionar todos Cable"
        Me.cbxCable.UseVisualStyleBackColor = True
        '
        'cbxAntena
        '
        Me.cbxAntena.AutoSize = True
        Me.cbxAntena.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxAntena.Location = New System.Drawing.Point(261, 648)
        Me.cbxAntena.Name = "cbxAntena"
        Me.cbxAntena.Size = New System.Drawing.Size(196, 19)
        Me.cbxAntena.TabIndex = 33
        Me.cbxAntena.Text = "Seleccionar todos Antenas"
        Me.cbxAntena.UseVisualStyleBackColor = True
        '
        'CMBlblLeyenda
        '
        Me.CMBlblLeyenda.AutoSize = True
        Me.CMBlblLeyenda.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblLeyenda.Location = New System.Drawing.Point(164, 93)
        Me.CMBlblLeyenda.Name = "CMBlblLeyenda"
        Me.CMBlblLeyenda.Size = New System.Drawing.Size(388, 16)
        Me.CMBlblLeyenda.TabIndex = 34
        Me.CMBlblLeyenda.Text = "Seleccione un tipo para cada Técnico (cable o antena)"
        '
        'FrmRelTipoTecnicos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(720, 699)
        Me.Controls.Add(Me.CMBlblLeyenda)
        Me.Controls.Add(Me.cbxAntena)
        Me.Controls.Add(Me.cbxCable)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnBuscarClave)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.dgvTipoTecnicos)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.CMBlblNombre)
        Me.Controls.Add(Me.CMBlblBuscarTecnico)
        Me.Controls.Add(Me.txtClave)
        Me.Controls.Add(Me.CMBlblClave)
        Me.Name = "FrmRelTipoTecnicos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Seleccione un tipo para cada Técnico (cable o antena)"
        CType(Me.dgvTipoTecnicos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBlblBuscarTecnico As System.Windows.Forms.Label
    Friend WithEvents txtClave As System.Windows.Forms.TextBox
    Friend WithEvents CMBlblClave As System.Windows.Forms.Label
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents CMBlblNombre As System.Windows.Forms.Label
    Friend WithEvents dgvTipoTecnicos As System.Windows.Forms.DataGridView
    Friend WithEvents btnBuscarClave As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents colIdRel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colClvTecnico As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colNombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colEsCable As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colEsAntena As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents cbxCable As System.Windows.Forms.CheckBox
    Friend WithEvents cbxAntena As System.Windows.Forms.CheckBox
    Friend WithEvents CMBlblLeyenda As System.Windows.Forms.Label
End Class
