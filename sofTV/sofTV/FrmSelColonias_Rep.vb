Imports System.Data.SqlClient

Public Class FrmSelColonias_Rep

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim x As Integer = 0
        Dim y As Integer
        x = Me.ListBox2.Items.Count()
        If x > 0 Then
            For y = 0 To (x - 1)
                Me.ListBox2.SelectedIndex = y
                If (Me.ListBox1.Text = Me.ListBox2.Text) Then
                    MsgBox("La Colonia ya esta en la lista", MsgBoxStyle.Information)
                    Exit Sub
                    'Else
                    '    Me.ListBox2.Items.Add(Me.ListBox1.Text)
                    '    Me.ListBox3.Items.Add(Me.ListBox1.SelectedValue.ToString)
                    '    Exit Sub
                End If
            Next
            Me.ListBox2.Items.Add(Me.ListBox1.Text)
            Me.ListBox3.Items.Add(Me.ListBox1.SelectedValue.ToString)
        Else
            Me.ListBox2.Items.Add(Me.ListBox1.Text)
            Me.ListBox3.Items.Add(Me.ListBox1.SelectedValue.ToString)
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim x As Integer
        Dim y As Integer
        x = Me.ListBox1.Items.Count()
        If Me.ListBox2.Items.Count() > 0 Then
            MsgBox("Primero Borre las Colonias Seleccionadas", MsgBoxStyle.Information)
        Else
            Me.ListBox1.SelectedIndex = 0
            For y = 1 To x
                Me.ListBox1.SelectedIndex = (y - 1)
                Me.ListBox2.Items.Add(Me.ListBox1.Text)
                Me.ListBox3.Items.Add(Me.ListBox1.SelectedValue.ToString)
            Next
        End If

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If (Me.ListBox2.SelectedIndex <> -1) Then
            Me.ListBox3.SelectedIndex = Me.ListBox2.SelectedIndex
            Me.ListBox2.Items.RemoveAt(Me.ListBox2.SelectedIndex)
            Me.ListBox3.Items.RemoveAt(Me.ListBox3.SelectedIndex)
        Else
            MsgBox("Selecciona primero un valor a borrar")
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim x As Integer
        Dim y As Integer
        x = Me.ListBox2.Items.Count()
        For y = 0 To (x - 1)
            Me.ListBox2.Items.RemoveAt(0)
            Me.ListBox3.Items.RemoveAt(0)
        Next
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Dim x As Integer = 0
        Dim y As Integer
        x = Me.ListBox3.Items.Count()
        If x = 0 Then

            MsgBox("Seleccione al menos una Colonia por favor", MsgBoxStyle.Information)

        Else
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            For y = 0 To (x - 1)
                Me.ListBox3.SelectedIndex = y
                Me.Inserta_Seleccion_ColoniaTableAdapter.Connection = CON
                Me.Inserta_Seleccion_ColoniaTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Seleccion_Colonia, LocClv_session, CInt(Me.ListBox3.Text))
            Next
            CON.Close()

            If LocOp = 3 Then
                If (Me.ListBox2.Items.Count()) > 1 Then
                    LocDescr3 = "Varias Colonias"
                    My.Forms.FrmSelFechas.Show()
                Else
                    LocDescr3 = Me.ListBox2.Text.ToString
                    My.Forms.FrmSelFechas.Show()

                End If
            End If
            If LocOp = 4 Then
                If (Me.ListBox2.Items.Count()) > 1 Then
                    LocDescr3 = "Varias Colonias"
                    My.Forms.FrmTelsi.Show()
                Else
                    LocDescr3 = Me.ListBox2.Text.ToString
                    My.Forms.FrmTelsi.Show()
                End If
            End If
            If LocOp = 5 Then
                If (Me.ListBox2.Items.Count()) > 1 Then
                    LocDescr3 = "Varias Colonias"
                    If LocValidaHab = 0 Then
                        FrmSelPeriodo.Show()
                    Else
                        bndReport2 = True
                    End If
                Else
                    LocDescr3 = Me.ListBox2.Text.ToString
                    If LocValidaHab = 0 Then
                        FrmSelPeriodo.Show()
                    Else
                        bndReport2 = True
                    End If

                End If
            End If
            If LocOp = 1 Then
                If (Me.ListBox2.Items.Count()) > 1 Then
                    LocDescr3 = "Varias Colonias"
                    If LocValidaHab = 0 Then
                        FrmSelPeriodo.Show()
                    Else
                        bndReportC = True
                    End If
                Else
                    LocDescr3 = Me.ListBox2.Text.ToString
                    If LocValidaHab = 0 Then
                        FrmSelPeriodo.Show()
                    Else
                        bndReportC = True
                    End If
                End If
            End If
            If LocOp = 9 Then
                If (Me.ListBox2.Items.Count()) > 1 Then
                    LocDescr3 = "Varias Colonias"
                    If LocValidaHab = 0 Then
                        FrmSelPeriodo.Show()
                    Else
                        bndReportA = True
                    End If
                Else
                    LocDescr3 = Me.ListBox2.Text.ToString
                    If LocValidaHab = 0 Then
                        FrmSelPeriodo.Show()
                    Else
                        bndReportA = True
                    End If
                End If
            End If

            If LocOp = 6 Or LocOp = 7 Then
                If (Me.ListBox2.Items.Count()) > 1 Then
                    LocDescr3 = "Varias Colonias"
                    My.Forms.FrmEtiquetas.Show()

                Else
                    LocDescr3 = Me.ListBox2.Text.ToString
                    My.Forms.FrmEtiquetas.Show()
                End If
            End If
            If LocOp = 10 Then
                If (Me.ListBox2.Items.Count()) > 1 Then
                    LocDescr3 = "Varias Colonias"
                    My.Forms.FrmSelPromocion_Rep.Show()
                Else
                    LocDescr3 = Me.ListBox2.Text.ToString
                    My.Forms.FrmSelPromocion_Rep.Show()

                End If
            End If

            If LocOp = 11 Then
                If (Me.ListBox2.Items.Count()) > 1 Then
                    LocDescr3 = "Varias Colonias"
                    My.Forms.FrmSelCalles.Show()
                Else
                    LocDescr3 = Me.ListBox2.Text.ToString
                    My.Forms.FrmSelCalles.Show()
                End If
            End If
        End If
        Me.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub FrmSelColonias_Rep_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetarnoldo.Muestra_Colonia_Rep' Puede moverla o quitarla seg�n sea necesario.
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Muestra_Colonia_RepTableAdapter.Connection = CON
        Me.Muestra_Colonia_RepTableAdapter.Fill(Me.DataSetarnoldo.Muestra_Colonia_Rep)
        CON.Close()
        colorea(Me, Me.Name)
    End Sub


End Class