<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPaqueteActivado
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim MAcCableModemLabel As System.Windows.Forms.Label
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.DamePaquetesDigBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DamePaquetesDigTableAdapter = New sofTV.DataSetEricTableAdapters.DamePaquetesDigTableAdapter()
        Me.DameTarjetasClientesDigBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameTarjetasClientesDigTableAdapter = New sofTV.DataSetEricTableAdapters.DameTarjetasClientesDigTableAdapter()
        Me.MANDA_CNR_DIG_2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MANDA_CNR_DIG_2TableAdapter = New sofTV.DataSetEricTableAdapters.MANDA_CNR_DIG_2TableAdapter()
        Me.MAcCableModemComboBox = New System.Windows.Forms.ComboBox()
        Me.DescripcionComboBox = New System.Windows.Forms.ComboBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ChecaPaqueteActivadoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ChecaPaqueteActivadoTableAdapter = New sofTV.DataSetEricTableAdapters.ChecaPaqueteActivadoTableAdapter()
        Me.btnBusCliente = New System.Windows.Forms.Button()
        MAcCableModemLabel = New System.Windows.Forms.Label()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DamePaquetesDigBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameTarjetasClientesDigBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MANDA_CNR_DIG_2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ChecaPaqueteActivadoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MAcCableModemLabel
        '
        MAcCableModemLabel.AutoSize = True
        MAcCableModemLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        MAcCableModemLabel.Location = New System.Drawing.Point(81, 76)
        MAcCableModemLabel.Name = "MAcCableModemLabel"
        MAcCableModemLabel.Size = New System.Drawing.Size(116, 16)
        MAcCableModemLabel.TabIndex = 22
        MAcCableModemLabel.Text = "No. de Tarjeta :"
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DamePaquetesDigBindingSource
        '
        Me.DamePaquetesDigBindingSource.DataMember = "DamePaquetesDig"
        Me.DamePaquetesDigBindingSource.DataSource = Me.DataSetEric
        '
        'DamePaquetesDigTableAdapter
        '
        Me.DamePaquetesDigTableAdapter.ClearBeforeFill = True
        '
        'DameTarjetasClientesDigBindingSource
        '
        Me.DameTarjetasClientesDigBindingSource.DataMember = "DameTarjetasClientesDig"
        Me.DameTarjetasClientesDigBindingSource.DataSource = Me.DataSetEric
        '
        'DameTarjetasClientesDigTableAdapter
        '
        Me.DameTarjetasClientesDigTableAdapter.ClearBeforeFill = True
        '
        'MANDA_CNR_DIG_2BindingSource
        '
        Me.MANDA_CNR_DIG_2BindingSource.DataMember = "MANDA_CNR_DIG_2"
        Me.MANDA_CNR_DIG_2BindingSource.DataSource = Me.DataSetEric
        '
        'MANDA_CNR_DIG_2TableAdapter
        '
        Me.MANDA_CNR_DIG_2TableAdapter.ClearBeforeFill = True
        '
        'MAcCableModemComboBox
        '
        Me.MAcCableModemComboBox.DataSource = Me.DameTarjetasClientesDigBindingSource
        Me.MAcCableModemComboBox.DisplayMember = "MAcCableModem"
        Me.MAcCableModemComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MAcCableModemComboBox.FormattingEnabled = True
        Me.MAcCableModemComboBox.Location = New System.Drawing.Point(203, 68)
        Me.MAcCableModemComboBox.Name = "MAcCableModemComboBox"
        Me.MAcCableModemComboBox.Size = New System.Drawing.Size(237, 24)
        Me.MAcCableModemComboBox.TabIndex = 1
        Me.MAcCableModemComboBox.ValueMember = "MAcCableModem"
        '
        'DescripcionComboBox
        '
        Me.DescripcionComboBox.DataSource = Me.DamePaquetesDigBindingSource
        Me.DescripcionComboBox.DisplayMember = "Descripcion"
        Me.DescripcionComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionComboBox.FormattingEnabled = True
        Me.DescripcionComboBox.Location = New System.Drawing.Point(203, 107)
        Me.DescripcionComboBox.Name = "DescripcionComboBox"
        Me.DescripcionComboBox.Size = New System.Drawing.Size(237, 24)
        Me.DescripcionComboBox.TabIndex = 2
        Me.DescripcionComboBox.ValueMember = "Clv_Txt"
        '
        'Button2
        '
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(336, 221)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(136, 36)
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "&CANCELAR"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(113, 221)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "&ACEPTAR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(203, 31)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(117, 22)
        Me.TextBox1.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(123, 37)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(74, 16)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Contrato :"
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown1.Location = New System.Drawing.Point(203, 143)
        Me.NumericUpDown1.Maximum = New Decimal(New Integer() {183, 0, 0, 0})
        Me.NumericUpDown1.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(117, 22)
        Me.NumericUpDown1.TabIndex = 3
        Me.NumericUpDown1.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(59, 149)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(138, 16)
        Me.Label4.TabIndex = 20
        Me.Label4.Text = "Número de Día(s) :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(127, 115)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(70, 16)
        Me.Label2.TabIndex = 21
        Me.Label2.Text = "Paquete:"
        '
        'ChecaPaqueteActivadoBindingSource
        '
        Me.ChecaPaqueteActivadoBindingSource.DataMember = "ChecaPaqueteActivado"
        Me.ChecaPaqueteActivadoBindingSource.DataSource = Me.DataSetEric
        '
        'ChecaPaqueteActivadoTableAdapter
        '
        Me.ChecaPaqueteActivadoTableAdapter.ClearBeforeFill = True
        '
        'btnBusCliente
        '
        Me.btnBusCliente.BackColor = System.Drawing.Color.DarkOrange
        Me.btnBusCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBusCliente.Location = New System.Drawing.Point(326, 31)
        Me.btnBusCliente.Name = "btnBusCliente"
        Me.btnBusCliente.Size = New System.Drawing.Size(30, 22)
        Me.btnBusCliente.TabIndex = 23
        Me.btnBusCliente.Text = "..."
        Me.btnBusCliente.UseVisualStyleBackColor = False
        '
        'FrmPaqueteActivado
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(554, 284)
        Me.Controls.Add(Me.btnBusCliente)
        Me.Controls.Add(MAcCableModemLabel)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.NumericUpDown1)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.DescripcionComboBox)
        Me.Controls.Add(Me.MAcCableModemComboBox)
        Me.Name = "FrmPaqueteActivado"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Activar Paquete"
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DamePaquetesDigBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameTarjetasClientesDigBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MANDA_CNR_DIG_2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChecaPaqueteActivadoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents DamePaquetesDigBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DamePaquetesDigTableAdapter As sofTV.DataSetEricTableAdapters.DamePaquetesDigTableAdapter
    Friend WithEvents DameTarjetasClientesDigBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameTarjetasClientesDigTableAdapter As sofTV.DataSetEricTableAdapters.DameTarjetasClientesDigTableAdapter
    Friend WithEvents MANDA_CNR_DIG_2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MANDA_CNR_DIG_2TableAdapter As sofTV.DataSetEricTableAdapters.MANDA_CNR_DIG_2TableAdapter
    Friend WithEvents MAcCableModemComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents DescripcionComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ChecaPaqueteActivadoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ChecaPaqueteActivadoTableAdapter As sofTV.DataSetEricTableAdapters.ChecaPaqueteActivadoTableAdapter
    Friend WithEvents btnBusCliente As System.Windows.Forms.Button
End Class
