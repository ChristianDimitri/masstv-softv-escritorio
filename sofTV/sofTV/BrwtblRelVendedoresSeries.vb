﻿Public Class BrwtblRelVendedoresSeries

    Dim FOLIO As New ClassBrwtblRelVendedoresSeries
#Region "EVENTOS"
    Private Sub BrwtblRelVendedoresSeries_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        VendedoresSeries(0, 0, "")
    End Sub
#End Region
   
#Region "METODOS"
    Private Sub VendedoresSeries(ByVal PRMOP As Integer, ByVal PRMID As Integer, ByVal PRMNOMBRE As String)
        FOLIO.OP = PRMOP
        FOLIO.ID = PRMID
        FOLIO.NOMBRE = PRMNOMBRE
        Me.dgvVendedores.DataSource = FOLIO.CONtblRelVendedoresSeries
    End Sub
#End Region

#Region "CONTROLES"
    Private Sub bnBuscarNombre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnBuscarNombre.Click
        VendedoresSeries(1, 0, tbNombre.Text)
        Me.tbNombre.Text = ""
    End Sub

    Private Sub bnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub

    Private Sub bnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnNuevo.Click
        opcion = "N"
        FrmtblRelVendedoresSeries.Show()
    End Sub
#End Region
   
   
    
    
End Class