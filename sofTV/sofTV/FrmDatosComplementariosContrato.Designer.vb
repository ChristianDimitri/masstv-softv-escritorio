﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDatosComplementariosContrato
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtPorcentaje = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtComplementario01 = New System.Windows.Forms.TextBox()
        Me.lblTexto03 = New System.Windows.Forms.Label()
        Me.txtPoblacion = New System.Windows.Forms.TextBox()
        Me.txtDireccionDelDeudor = New System.Windows.Forms.TextBox()
        Me.txtNombreDelDeudor = New System.Windows.Forms.TextBox()
        Me.lblTexto11 = New System.Windows.Forms.Label()
        Me.lblTexto10 = New System.Windows.Forms.Label()
        Me.lblTexto09 = New System.Windows.Forms.Label()
        Me.lblTexto07 = New System.Windows.Forms.Label()
        Me.lblTexto08 = New System.Windows.Forms.Label()
        Me.txtSerieNumerada = New System.Windows.Forms.TextBox()
        Me.lblTexto06 = New System.Windows.Forms.Label()
        Me.lblTexto05 = New System.Windows.Forms.Label()
        Me.txtCantidadConLetra = New System.Windows.Forms.TextBox()
        Me.lblTexto04 = New System.Windows.Forms.Label()
        Me.txtDatosDeLaEmpresa = New System.Windows.Forms.TextBox()
        Me.lblTexto02 = New System.Windows.Forms.Label()
        Me.txtNombreDelCliente = New System.Windows.Forms.TextBox()
        Me.lblTexto01 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblNo = New System.Windows.Forms.Label()
        Me.txtCantidadNumerica = New System.Windows.Forms.TextBox()
        Me.txtNoFolio = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.lblPanel = New System.Windows.Forms.Label()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtPorcentaje)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtComplementario01)
        Me.GroupBox1.Controls.Add(Me.lblTexto03)
        Me.GroupBox1.Controls.Add(Me.txtPoblacion)
        Me.GroupBox1.Controls.Add(Me.txtDireccionDelDeudor)
        Me.GroupBox1.Controls.Add(Me.txtNombreDelDeudor)
        Me.GroupBox1.Controls.Add(Me.lblTexto11)
        Me.GroupBox1.Controls.Add(Me.lblTexto10)
        Me.GroupBox1.Controls.Add(Me.lblTexto09)
        Me.GroupBox1.Controls.Add(Me.lblTexto07)
        Me.GroupBox1.Controls.Add(Me.lblTexto08)
        Me.GroupBox1.Controls.Add(Me.txtSerieNumerada)
        Me.GroupBox1.Controls.Add(Me.lblTexto06)
        Me.GroupBox1.Controls.Add(Me.lblTexto05)
        Me.GroupBox1.Controls.Add(Me.txtCantidadConLetra)
        Me.GroupBox1.Controls.Add(Me.lblTexto04)
        Me.GroupBox1.Controls.Add(Me.txtDatosDeLaEmpresa)
        Me.GroupBox1.Controls.Add(Me.lblTexto02)
        Me.GroupBox1.Controls.Add(Me.txtNombreDelCliente)
        Me.GroupBox1.Controls.Add(Me.lblTexto01)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.lblNo)
        Me.GroupBox1.Controls.Add(Me.txtCantidadNumerica)
        Me.GroupBox1.Controls.Add(Me.txtNoFolio)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 33)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(832, 455)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Ingresa los Datos Complementarios Del Pagaré para la Impresión del Contrato"
        '
        'txtPorcentaje
        '
        Me.txtPorcentaje.BackColor = System.Drawing.Color.White
        Me.txtPorcentaje.Enabled = False
        Me.txtPorcentaje.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPorcentaje.Location = New System.Drawing.Point(548, 273)
        Me.txtPorcentaje.Name = "txtPorcentaje"
        Me.txtPorcentaje.Size = New System.Drawing.Size(122, 23)
        Me.txtPorcentaje.TabIndex = 3
        Me.txtPorcentaje.Text = "3.5"
        Me.txtPorcentaje.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(669, 276)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(16, 18)
        Me.Label2.TabIndex = 138
        Me.Label2.Text = "%"
        '
        'txtComplementario01
        '
        Me.txtComplementario01.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtComplementario01.Location = New System.Drawing.Point(419, 172)
        Me.txtComplementario01.Name = "txtComplementario01"
        Me.txtComplementario01.Size = New System.Drawing.Size(398, 23)
        Me.txtComplementario01.TabIndex = 1
        Me.txtComplementario01.Text = "[Dato Obligatorio]"
        Me.txtComplementario01.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblTexto03
        '
        Me.lblTexto03.AutoSize = True
        Me.lblTexto03.Location = New System.Drawing.Point(395, 175)
        Me.lblTexto03.Name = "lblTexto03"
        Me.lblTexto03.Size = New System.Drawing.Size(18, 18)
        Me.lblTexto03.TabIndex = 136
        Me.lblTexto03.Text = "el"
        '
        'txtPoblacion
        '
        Me.txtPoblacion.BackColor = System.Drawing.Color.White
        Me.txtPoblacion.Enabled = False
        Me.txtPoblacion.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPoblacion.Location = New System.Drawing.Point(163, 405)
        Me.txtPoblacion.Name = "txtPoblacion"
        Me.txtPoblacion.Size = New System.Drawing.Size(654, 23)
        Me.txtPoblacion.TabIndex = 135
        Me.txtPoblacion.Text = "Población del Cliente"
        Me.txtPoblacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtDireccionDelDeudor
        '
        Me.txtDireccionDelDeudor.BackColor = System.Drawing.Color.White
        Me.txtDireccionDelDeudor.Enabled = False
        Me.txtDireccionDelDeudor.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDireccionDelDeudor.Location = New System.Drawing.Point(163, 376)
        Me.txtDireccionDelDeudor.Name = "txtDireccionDelDeudor"
        Me.txtDireccionDelDeudor.Size = New System.Drawing.Size(654, 23)
        Me.txtDireccionDelDeudor.TabIndex = 134
        Me.txtDireccionDelDeudor.Text = "Dirección del Cliente"
        Me.txtDireccionDelDeudor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtNombreDelDeudor
        '
        Me.txtNombreDelDeudor.BackColor = System.Drawing.Color.White
        Me.txtNombreDelDeudor.Enabled = False
        Me.txtNombreDelDeudor.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNombreDelDeudor.Location = New System.Drawing.Point(163, 347)
        Me.txtNombreDelDeudor.Name = "txtNombreDelDeudor"
        Me.txtNombreDelDeudor.Size = New System.Drawing.Size(654, 23)
        Me.txtNombreDelDeudor.TabIndex = 133
        Me.txtNombreDelDeudor.Text = "Nombre del Cliente"
        Me.txtNombreDelDeudor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblTexto11
        '
        Me.lblTexto11.AutoSize = True
        Me.lblTexto11.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTexto11.Location = New System.Drawing.Point(17, 408)
        Me.lblTexto11.Name = "lblTexto11"
        Me.lblTexto11.Size = New System.Drawing.Size(83, 18)
        Me.lblTexto11.TabIndex = 132
        Me.lblTexto11.Text = "POBLACIÓN:"
        '
        'lblTexto10
        '
        Me.lblTexto10.AutoSize = True
        Me.lblTexto10.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTexto10.Location = New System.Drawing.Point(17, 379)
        Me.lblTexto10.Name = "lblTexto10"
        Me.lblTexto10.Size = New System.Drawing.Size(78, 18)
        Me.lblTexto10.TabIndex = 131
        Me.lblTexto10.Text = "DIRECCIÓN:"
        '
        'lblTexto09
        '
        Me.lblTexto09.AutoSize = True
        Me.lblTexto09.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTexto09.Location = New System.Drawing.Point(17, 350)
        Me.lblTexto09.Name = "lblTexto09"
        Me.lblTexto09.Size = New System.Drawing.Size(143, 18)
        Me.lblTexto09.TabIndex = 130
        Me.lblTexto09.Text = "NOMBRE DEL DEUDOR:"
        '
        'lblTexto07
        '
        Me.lblTexto07.AutoSize = True
        Me.lblTexto07.Location = New System.Drawing.Point(17, 305)
        Me.lblTexto07.Name = "lblTexto07"
        Me.lblTexto07.Size = New System.Drawing.Size(244, 18)
        Me.lblTexto07.TabIndex = 129
        Me.lblTexto07.Text = "esta ciudad juntamente con el principal."
        '
        'lblTexto08
        '
        Me.lblTexto08.AutoSize = True
        Me.lblTexto08.Location = New System.Drawing.Point(685, 276)
        Me.lblTexto08.Name = "lblTexto08"
        Me.lblTexto08.Size = New System.Drawing.Size(141, 18)
        Me.lblTexto08.TabIndex = 128
        Me.lblTexto08.Text = "mensual, pagadero en "
        '
        'txtSerieNumerada
        '
        Me.txtSerieNumerada.BackColor = System.Drawing.Color.White
        Me.txtSerieNumerada.Enabled = False
        Me.txtSerieNumerada.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSerieNumerada.Location = New System.Drawing.Point(658, 243)
        Me.txtSerieNumerada.Name = "txtSerieNumerada"
        Me.txtSerieNumerada.Size = New System.Drawing.Size(159, 23)
        Me.txtSerieNumerada.TabIndex = 2
        Me.txtSerieNumerada.Text = "1"
        Me.txtSerieNumerada.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblTexto06
        '
        Me.lblTexto06.AutoSize = True
        Me.lblTexto06.Location = New System.Drawing.Point(17, 276)
        Me.lblTexto06.Name = "lblTexto06"
        Me.lblTexto06.Size = New System.Drawing.Size(532, 18)
        Me.lblTexto06.TabIndex = 125
        Me.lblTexto06.Text = "y todos están sujetos a la condición de que, al no pagarse intereses moratorios a" & _
            "l tipo de"
        '
        'lblTexto05
        '
        Me.lblTexto05.AutoSize = True
        Me.lblTexto05.Location = New System.Drawing.Point(17, 248)
        Me.lblTexto05.Name = "lblTexto05"
        Me.lblTexto05.Size = New System.Drawing.Size(635, 18)
        Me.lblTexto05.TabIndex = 123
        Me.lblTexto05.Text = "Valor Recibido a mi (nuestra) entera satisfacción. Este pagaré forma parte de una" & _
            " serie numerada del 1 al "
        '
        'txtCantidadConLetra
        '
        Me.txtCantidadConLetra.BackColor = System.Drawing.Color.White
        Me.txtCantidadConLetra.Enabled = False
        Me.txtCantidadConLetra.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCantidadConLetra.Location = New System.Drawing.Point(119, 212)
        Me.txtCantidadConLetra.Name = "txtCantidadConLetra"
        Me.txtCantidadConLetra.Size = New System.Drawing.Size(698, 23)
        Me.txtCantidadConLetra.TabIndex = 122
        Me.txtCantidadConLetra.Text = "Cantidad con letra"
        Me.txtCantidadConLetra.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblTexto04
        '
        Me.lblTexto04.AutoSize = True
        Me.lblTexto04.Location = New System.Drawing.Point(17, 215)
        Me.lblTexto04.Name = "lblTexto04"
        Me.lblTexto04.Size = New System.Drawing.Size(96, 18)
        Me.lblTexto04.TabIndex = 121
        Me.lblTexto04.Text = "La Cantidad de"
        '
        'txtDatosDeLaEmpresa
        '
        Me.txtDatosDeLaEmpresa.BackColor = System.Drawing.Color.White
        Me.txtDatosDeLaEmpresa.Enabled = False
        Me.txtDatosDeLaEmpresa.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDatosDeLaEmpresa.Location = New System.Drawing.Point(45, 172)
        Me.txtDatosDeLaEmpresa.Name = "txtDatosDeLaEmpresa"
        Me.txtDatosDeLaEmpresa.Size = New System.Drawing.Size(344, 23)
        Me.txtDatosDeLaEmpresa.TabIndex = 120
        Me.txtDatosDeLaEmpresa.Text = "Dirección de la Empresa"
        Me.txtDatosDeLaEmpresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblTexto02
        '
        Me.lblTexto02.AutoSize = True
        Me.lblTexto02.Location = New System.Drawing.Point(17, 175)
        Me.lblTexto02.Name = "lblTexto02"
        Me.lblTexto02.Size = New System.Drawing.Size(22, 18)
        Me.lblTexto02.TabIndex = 119
        Me.lblTexto02.Text = "En"
        '
        'txtNombreDelCliente
        '
        Me.txtNombreDelCliente.BackColor = System.Drawing.Color.White
        Me.txtNombreDelCliente.Enabled = False
        Me.txtNombreDelCliente.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNombreDelCliente.Location = New System.Drawing.Point(20, 138)
        Me.txtNombreDelCliente.Name = "txtNombreDelCliente"
        Me.txtNombreDelCliente.Size = New System.Drawing.Size(797, 23)
        Me.txtNombreDelCliente.TabIndex = 118
        Me.txtNombreDelCliente.Text = "Nombre del Cliente"
        Me.txtNombreDelCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblTexto01
        '
        Me.lblTexto01.AutoSize = True
        Me.lblTexto01.Location = New System.Drawing.Point(17, 117)
        Me.lblTexto01.Name = "lblTexto01"
        Me.lblTexto01.Size = New System.Drawing.Size(457, 18)
        Me.lblTexto01.TabIndex = 117
        Me.lblTexto01.Text = "Debe(mos) y pagare(mos) incondicionalmente por este pagaré a la orden de:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(662, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(71, 18)
        Me.Label1.TabIndex = 116
        Me.Label1.Text = "Bueno por:"
        '
        'lblNo
        '
        Me.lblNo.AutoSize = True
        Me.lblNo.Location = New System.Drawing.Point(441, 32)
        Me.lblNo.Name = "lblNo"
        Me.lblNo.Size = New System.Drawing.Size(28, 18)
        Me.lblNo.TabIndex = 115
        Me.lblNo.Text = "No."
        '
        'txtCantidadNumerica
        '
        Me.txtCantidadNumerica.BackColor = System.Drawing.Color.White
        Me.txtCantidadNumerica.Enabled = False
        Me.txtCantidadNumerica.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCantidadNumerica.Location = New System.Drawing.Point(661, 53)
        Me.txtCantidadNumerica.Name = "txtCantidadNumerica"
        Me.txtCantidadNumerica.Size = New System.Drawing.Size(156, 26)
        Me.txtCantidadNumerica.TabIndex = 113
        Me.txtCantidadNumerica.Text = "$XXX.XX "
        Me.txtCantidadNumerica.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtNoFolio
        '
        Me.txtNoFolio.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNoFolio.Location = New System.Drawing.Point(444, 53)
        Me.txtNoFolio.Name = "txtNoFolio"
        Me.txtNoFolio.Size = New System.Drawing.Size(156, 26)
        Me.txtNoFolio.TabIndex = 0
        Me.txtNoFolio.Text = "[Dato Obligatorio]"
        Me.txtNoFolio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(646, 54)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(19, 22)
        Me.Label3.TabIndex = 139
        Me.Label3.Text = "$"
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.DarkOrange
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.ForeColor = System.Drawing.Color.Black
        Me.btnSalir.Location = New System.Drawing.Point(719, 506)
        Me.btnSalir.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(125, 42)
        Me.btnSalir.TabIndex = 103
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'lblPanel
        '
        Me.lblPanel.AutoSize = True
        Me.lblPanel.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPanel.Location = New System.Drawing.Point(263, 0)
        Me.lblPanel.Name = "lblPanel"
        Me.lblPanel.Size = New System.Drawing.Size(321, 24)
        Me.lblPanel.TabIndex = 116
        Me.lblPanel.Text = "Datos Complementarios del Pagaré"
        '
        'btnGuardar
        '
        Me.btnGuardar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardar.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.ForeColor = System.Drawing.Color.Black
        Me.btnGuardar.Location = New System.Drawing.Point(578, 506)
        Me.btnGuardar.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(125, 42)
        Me.btnGuardar.TabIndex = 0
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = False
        '
        'FrmDatosComplementariosContrato
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 18.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(856, 560)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.lblPanel)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.Name = "FrmDatosComplementariosContrato"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Datos Complementarios Contrato"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents txtCantidadNumerica As System.Windows.Forms.TextBox
    Friend WithEvents txtNoFolio As System.Windows.Forms.TextBox
    Friend WithEvents txtDatosDeLaEmpresa As System.Windows.Forms.TextBox
    Friend WithEvents lblTexto02 As System.Windows.Forms.Label
    Friend WithEvents txtNombreDelCliente As System.Windows.Forms.TextBox
    Friend WithEvents lblTexto01 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblNo As System.Windows.Forms.Label
    Friend WithEvents lblPanel As System.Windows.Forms.Label
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents txtComplementario01 As System.Windows.Forms.TextBox
    Friend WithEvents lblTexto03 As System.Windows.Forms.Label
    Friend WithEvents txtPoblacion As System.Windows.Forms.TextBox
    Friend WithEvents txtDireccionDelDeudor As System.Windows.Forms.TextBox
    Friend WithEvents txtNombreDelDeudor As System.Windows.Forms.TextBox
    Friend WithEvents lblTexto11 As System.Windows.Forms.Label
    Friend WithEvents lblTexto10 As System.Windows.Forms.Label
    Friend WithEvents lblTexto09 As System.Windows.Forms.Label
    Friend WithEvents lblTexto07 As System.Windows.Forms.Label
    Friend WithEvents lblTexto08 As System.Windows.Forms.Label
    Friend WithEvents txtSerieNumerada As System.Windows.Forms.TextBox
    Friend WithEvents txtPorcentaje As System.Windows.Forms.TextBox
    Friend WithEvents lblTexto06 As System.Windows.Forms.Label
    Friend WithEvents lblTexto05 As System.Windows.Forms.Label
    Friend WithEvents txtCantidadConLetra As System.Windows.Forms.TextBox
    Friend WithEvents lblTexto04 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
End Class
