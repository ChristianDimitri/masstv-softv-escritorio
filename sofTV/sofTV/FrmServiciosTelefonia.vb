Imports System.Data.SqlClient
Imports System.Diagnostics
Imports Microsoft.VisualBasic
Imports System.Windows.Forms
Public Class FrmServiciosTelefonia
    Dim ban As Boolean = False
    Dim trazo As TraceListener
    Dim Clv_ServicioDigital As Integer
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
    Private Sub Guarda_Lineas(ByVal opx As Integer)
        Dim ConLineas As New SqlConnection(MiConexion)
        Dim CMd As New SqlCommand
        ConLineas.Open()
        trazo = New TextWriterTraceListener("...\Res.log")

        Try
            With CMd
                '[Guarda_RelPlanLineas](@Clv_servicio int,@Lineas int,@opc int)
                .CommandText = "Guarda_RelPlanLineas"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = ConLineas
                Dim pmt As New SqlParameter("@clv_servicio", SqlDbType.BigInt)
                Dim Pmt2 As New SqlParameter("@Lineas", SqlDbType.Int)
                Dim Pmt3 As New SqlParameter("@opc", SqlDbType.Int)

                pmt.Direction = ParameterDirection.Input
                Pmt2.Direction = ParameterDirection.InputOutput
                Pmt3.Direction = ParameterDirection.Input

                pmt.Value = Me.Clv_ServicioTextBox.Text
                Pmt2.Value = Me.NumericUpDown5.Value
                Pmt3.Value = opx

                .Parameters.Add(pmt)
                .Parameters.Add(Pmt2)
                .Parameters.Add(Pmt3)

                .ExecuteNonQuery()
                If opx = 3 Then
                    Me.NumericUpDown5.Value = Pmt2.Value
                End If

            End With
            ConLineas.Close()
        Catch ex As Exception
            trazo.WriteLine("Error ")
            trazo.WriteLine(ex.Message)
            trazo.Flush()
        End Try

    End Sub
    Private Sub CONSERVICIOSBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONSERVICIOSBindingNavigatorSaveItem.Click
        Dim Cone7 As New SqlClient.SqlConnection(MiConexion)
        If Me.DescripcionTextBox.Text.Trim.Length = 0 Then
            MsgBox("Se Debe Capturar una Descripci�n del Servicio", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.Clv_TxtTextBox.Text.Trim.Length = 0 Then
            MsgBox("Se Debe Capturar una Clave para el Servicio", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.Genera_OrdenCheckBox.CheckState <> CheckState.Checked Then
            BorrarRel_Trabajos_NoCobroMensual()
        Else
            GUARDARel_Trabajos_NoCobroMensualGUARDA()
        End If
        Me.Validate()
        Cone7.Open()
        Me.CONSERVICIOSBindingSource.EndEdit()
        Me.CONSERVICIOSTableAdapter.Connection = Cone7
        Me.CONSERVICIOSTableAdapter.Update(Me.DataSetLidia2.CONSERVICIOS)
        Cone7.Close()
        Guarda_Lineas(0)
        MsgBox("Se Ha Guardado con �xito", MsgBoxStyle.Information)
        Me.Panel9.Enabled = True
        activa(True)
        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Guardar de Servicios", "Se Dio de Alta Servicio", "Clve del Servicio" + CStr(Me.Clv_ServicioTextBox.Text), GloCiudad)
        GloBnd = True
    End Sub
    Private Sub GUARDARel_Trabajos_NoCobroMensualGUARDA()
        Try
            Dim cmd As New SqlClient.SqlCommand
            Dim CON4 As New SqlConnection(MiConexion)
            CON4.Open()
            If IsNumeric(Me.Clv_ServicioTextBox.Text) = True Then
                If Len(Trim(Me.ComboBox5.Text)) > 0 And IsNumeric(Me.ComboBox5.SelectedValue) = True Then
                    With cmd
                        .CommandText = "GuardaRel_trabajos_NoCobroMensual"
                        .CommandTimeout = 0
                        .CommandType = CommandType.StoredProcedure
                        .Connection = CON4
                        Dim prm As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
                        Dim prm2 As New SqlParameter("@Clv_Trabajo", SqlDbType.Int)
                        prm.Direction = ParameterDirection.Input
                        prm2.Direction = ParameterDirection.Input
                        prm.Value = Me.Clv_ServicioTextBox.Text
                        prm2.Value = Me.ComboBox5.SelectedValue
                        .Parameters.Add(prm)
                        .Parameters.Add(prm2)
                        Dim i As Integer = .ExecuteNonQuery

                    End With
                   
                End If
            End If
            CON4.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub BorrarRel_Trabajos_NoCobroMensual()
        Dim cmd As New SqlClient.SqlCommand
        Dim CON5 As New SqlConnection(MiConexion)
        CON5.Open()
        Try
            If IsNumeric(Me.Clv_ServicioTextBox.Text) = True Then
                With cmd
                    .CommandText = "BORRel_Trabajos_NoCobroMensual"
                    .CommandTimeout = 0
                    .CommandType = CommandType.StoredProcedure
                    .Connection = CON5
                    Dim prm As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
                    prm.Direction = ParameterDirection.Input
                    prm.Value = Me.Clv_ServicioTextBox.Text
                    .Parameters.Add(prm)
                    Dim i As Integer = .ExecuteNonQuery
                End With

            End If
            CON5.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub cobromensual()
        If Me.Sale_en_CarteraCheckBox1.Checked = True Then
            Me.Panel2.Visible = False
            Me.Sale_en_CarteraCheckBox.Checked = False
            Me.Sale_en_CarteraCheckBox.Visible = False
            Me.Genera_OrdenCheckBox.Checked = False
            Me.Genera_OrdenCheckBox.Visible = False
            Me.Panel3.Visible = False
            Me.TabControl1.Enabled = True
            Me.NumericUpDown5.Visible = True
            Me.Label6.Visible = True
        Else
            Me.NumericUpDown5.Visible = False
            Me.Label6.Visible = False
            Me.Panel2.Visible = True
            Me.Sale_en_CarteraCheckBox.Visible = True
            Me.Genera_OrdenCheckBox.Visible = True
            Me.Panel3.Visible = True
            Me.TabControl1.Enabled = False
        End If
    End Sub

    Private Sub FrmServiciosTelefonia_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        

        coloreaSer_Tel()
        Dim cone As New SqlClient.SqlConnection(MiConexion)
        cone.Open()
        Me.MuestraTipoClientesTableAdapter.Connection = cone
        Me.MuestraTipoClientesTableAdapter.Fill(Me.DataSetLidia2.MuestraTipoClientes, 0, 0)
        Me.MUESTRATRABAJOS_NOCOBROMENSUALTableAdapter.Connection = cone
        Me.MUESTRATRABAJOS_NOCOBROMENSUALTableAdapter.Fill(Me.DataSetEDGAR.MUESTRATRABAJOS_NOCOBROMENSUAL, GloClv_TipSer, 0)
        Me.Muestra_ServiciosDigitalesTableAdapter.Connection = cone
        Me.Muestra_ServiciosDigitalesTableAdapter.Fill(Me.DataSetyahve.Muestra_ServiciosDigitales)
        Me.Muestra_Dias_FinalTableAdapter.Connection = cone
        Me.Muestra_Dias_FinalTableAdapter.Fill(Me.DataSetyahve.Muestra_Dias_Final)
        Me.Muestra_Dias_inicialTableAdapter.Connection = cone
        Me.Muestra_Dias_inicialTableAdapter.Fill(Me.DataSetyahve.Muestra_Dias_inicial)
        cone.Close()
        Me.ComboBox4.Text = Nothing
        Me.BindingNavigatorAddNewItem.Enabled = False
        Me.ToolStripButton4.Enabled = False
        Me.CheckBox4.CheckState = CheckState.Unchecked

        Me.DameClv_Session()

       If opcion = "N" Then
            Me.Panel9.Enabled = False
            Me.CONSERVICIOSBindingSource.AddNew()
            Me.CLV_SERVICIOTextBox1.Text = GloClv_TipSer
            Me.Sale_en_CarteraCheckBox1.Checked = False
            Me.Es_PrincipalCheckBox.Checked = False
            Me.CheckBox1.Checked = False
            Me.CheckBox2.Checked = False
            Me.CheckBox3.Checked = False
            Me.CheckBox4.Checked = False
            Me.CheckBox5.Checked = False

        End If
        If opcion = "C" Or opcion = "M" Then
            Me.Panel9.Enabled = True
            cone.Open()
            Me.CONSERVICIOSTableAdapter.Connection = cone
            Me.CONSERVICIOSTableAdapter.Fill(Me.DataSetLidia2.CONSERVICIOS, GloClv_Servicio)
            cone.Close()
            LLena_Puntos()
            Guarda_Lineas(3)
            activa(True)
            If Me.Genera_OrdenCheckBox.Checked = True Then
                cone.Open()
                Me.CONRel_Trabajos_NoCobroMensualTableAdapter.Connection = cone
                Me.CONRel_Trabajos_NoCobroMensualTableAdapter.Fill(Me.DataSetEDGAR.CONRel_Trabajos_NoCobroMensual, Me.Clv_ServicioTextBox.Text)
                cone.Close()
            End If
        End If
        If opcion = "C" Then
            Me.ConServiciosTelBindingNavigator.Enabled = False
        End If
       
    End Sub

    Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.SelectedIndexChanged
        Dim Cone2 As New SqlClient.SqlConnection(MiConexion)
        If IsNumeric(Me.ComboBox4.SelectedValue) = True Then
            Me.BindingNavigatorAddNewItem.Enabled = False
            Me.ToolStripButton4.Enabled = False
            Cone2.Open()

            CONSULTA_Dias_Aplica()

            Me.Consultar_Rel_ServiciosDigitales_Tel_TMPTableAdapter.Connection = Cone2

            If Me.Clv_ServicioTextBox.Text = "" Then
                Me.Consultar_Rel_ServiciosDigitales_Tel_TMPTableAdapter.Fill(Me.DataSetyahve.Consultar_Rel_ServiciosDigitales_Tel_TMP, CInt(Me.TxtClv_Session.Text), 0, CInt(Me.ComboBox4.SelectedValue))
            Else
                Me.Consultar_Rel_ServiciosDigitales_Tel_TMPTableAdapter.Fill(Me.DataSetyahve.Consultar_Rel_ServiciosDigitales_Tel_TMP, CInt(Me.TxtClv_Session.Text), CInt(Me.Clv_ServicioTextBox.Text), CInt(Me.ComboBox4.SelectedValue))
            End If

            Me.ConServiciosTelTableAdapter.Connection = Cone2
            Me.ConServiciosTelTableAdapter.Fill(Me.DataSetLidia2.ConServiciosTel, Me.Clv_ServicioTextBox.Text, Me.ComboBox4.SelectedValue)
            Cone2.Close()
            If IsNumeric(Me.TextBox5.Text) = True Then
                If Me.TextBox5.Text > 0 Then
                    Me.BindingNavigatorAddNewItem.Enabled = False
                    Me.ToolStripButton4.Enabled = True
                Else
                    activa(True)
                End If
      
            End If
            Me.TextBox5.Text = Me.Clv_ServicioTextBox.Text
            Me.Clv_TipoClienteTextBox.Text = Me.ComboBox4.SelectedValue
            Me.Panel21.Visible = True

        Else
            Cone2.Open()
            Me.ConServiciosTelTableAdapter.Connection = Cone2
            Me.ConServiciosTelTableAdapter.Fill(Me.DataSetLidia2.ConServiciosTel, 0, 0)
            Cone2.Close()
            Me.BindingNavigatorAddNewItem.Enabled = True
            Me.ToolStripButton4.Enabled = True
            Me.Panel21.Visible = False
        End If

    End Sub

    Private Sub CONSULTA_Dias_Aplica()

        Dim sw As Integer = 0
        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("CONSULTA_Dias_Aplica", con)

        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New Data.SqlClient.SqlParameter( _
                "@Clv_Servicio", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.Clv_ServicioTextBox.Text)
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                  "@Clv_Tipocobro", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.ComboBox4.SelectedValue)
        cmd.Parameters.Add(prm)

        con.Open()

        Dim reader As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Try
            While reader.Read()

                'Llenamos los TxtBox con los datos respectivos
                Me.ComboBox2.SelectedValue = CInt(reader(0).ToString)
                Me.ComboBox3.SelectedValue = CInt(reader(1).ToString)
                sw = 1

            End While
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            reader.Close()
        End Try
        con.Close()
        If sw = 0 Then
            Me.ComboBox2.SelectedValue = 0
            Me.ComboBox3.SelectedValue = 0
        End If

    End Sub


    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim cone5 As New SqlClient.SqlConnection(MiConexion)
        cone5.Open()
        Me.CONSERVICIOSTableAdapter.Connection = cone5
        Me.CONSERVICIOSTableAdapter.Delete(Me.Clv_ServicioTextBox.Text)
        cone5.Close()
        Borra_Datos()
        Guarda_Lineas(1)
        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Eliminar en Servicios", "Se Borr� Servicio de Telefonia", "Clave del Servicio" + CStr(Me.Clv_ServicioTextBox.Text), GloCiudad)
        MsgBox("Se Ha Borrado con �xito", MsgBoxStyle.Information)
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Me.CONSERVICIOSBindingSource.CancelEdit()

    End Sub

    Private Sub Sale_en_CarteraCheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Sale_en_CarteraCheckBox1.CheckedChanged
        cobromensual()
    End Sub
    Private Sub LLena_Puntos()
        Dim reader As SqlDataReader
        Dim CON As New SqlConnection(MiConexion)
        Dim previousConnectionState As ConnectionState = CON.State
        Try
            If CON.State = ConnectionState.Closed Then
                CON.Open()
            End If
            Dim comando As SqlClient.SqlCommand
            comando = New SqlClient.SqlCommand
            If Me.Clv_ServicioTextBox.Text = "" Then Me.Clv_ServicioTextBox.Text = 0
            With comando
                .Connection = CON
                .CommandText = "exec BUSCAPuntos_Pago_Adelantado " & Me.Clv_ServicioTextBox.Text & ",0"
                .CommandType = CommandType.Text
                .CommandTimeout = 0
                reader = comando.ExecuteReader()
                Using reader
                    While reader.Read
                        ' Process SprocResults datareader here.
                        Me.Puntos3NumericUpDown.Value = reader.GetValue(1)
                        Me.Puntos6NumericUpDown.Value = reader.GetValue(2)
                        Me.Puntos11NumericUpDown.Value = reader.GetValue(3)
                        Me.Punto_Pronto_PagoNumericUpDown.Value = reader.GetValue(4)
                    End While
                End Using
            End With
            CON.Close()

        Finally
            If previousConnectionState = ConnectionState.Closed Then
                CON.Close()
            End If
        End Try

    End Sub


    Private Sub Genera_OrdenCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Genera_OrdenCheckBox.CheckedChanged
        If Me.Genera_OrdenCheckBox.CheckState = CheckState.Checked Then
            Panel18.Visible = True
        Else
            Panel18.Visible = False
        End If
    End Sub

    Private Sub BindingNavigatorAddNewItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorAddNewItem.Click
        Me.Panel9.Enabled = True
        activa(False)
        Me.TabControl2.Enabled = True
        Me.ConServiciosTelBindingNavigatorSaveItem.Enabled = True
        Me.CMBPanel4.Enabled = True
        Me.ConServiciosTelTableAdapter.Fill(Me.DataSetLidia2.ConServiciosTel, 0, 0)
        Me.TextBox5.Text = Me.Clv_ServicioTextBox.Text
        Me.Clv_TipoClienteTextBox.Text = Me.ComboBox4.SelectedValue
        Me.BindingNavigatorAddNewItem.Enabled = True
        Me.ToolStripButton3.Enabled = True
        Me.Panel15.Enabled = False
        Me.Panel21.Enabled = True
    End Sub
    Private Sub activa(ByVal opt As Boolean)
        Me.Button1.Enabled = Not (opt)
        Me.Button2.Enabled = Not (opt)
        Me.TxtBoxCosto.ReadOnly = opt
        Me.ComboBox1.Enabled = Not (opt)
        Me.ComboBox2.Enabled = Not (opt)
        Me.ComboBox3.Enabled = Not (opt)


        Me.TextBox1.ReadOnly = opt
        Me.TextBox2.ReadOnly = opt
        Me.TextBox3.ReadOnly = opt
        Me.TextBox4.ReadOnly = opt
        Me.TextBox7.ReadOnly = opt
        Me.TextBox8.ReadOnly = opt
        Me.CheckBox1.Enabled = Not (opt)
        Me.CheckBox2.Enabled = Not (opt)
        Me.CheckBox3.Enabled = Not (opt)
        Me.CheckBox4.Enabled = Not (opt)
        Me.CheckBox5.Enabled = Not (opt)

        'Me.TextBox9.ReadOnly = opt
        'Me.TextBox10.ReadOnly = opt
        Me.TextBox17.ReadOnly = opt
        Me.TextBox18.ReadOnly = opt
        Me.TextBox12.ReadOnly = opt
        Me.TextBox11.ReadOnly = opt
        Me.TextBox14.ReadOnly = opt
        'Me.TextBox15.ReadOnly = opt
        Me.NumericUpDown1.ReadOnly = opt
        'Me.NumericUpDown6.ReadOnly = opt
        'Me.NumericUpDown5.ReadOnly = opt
        Me.NumericUpDown4.ReadOnly = opt
        Me.NumericUpDown3.ReadOnly = opt
        Me.NumericUpDown2.ReadOnly = opt
        Me.NumericUpDown8.ReadOnly = opt
        Me.NumericUpDown7.ReadOnly = opt
        'Me.NumericUpDown9.ReadOnly = opt
    End Sub
    Private Sub ToolStripButton4_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton4.Click
        activa(False)
        Me.Panel21.Enabled = True
        Me.ConServiciosTelBindingNavigatorSaveItem.Enabled = True
        Me.CMBPanel4.Enabled = True
        Me.BindingNavigatorAddNewItem.Enabled = False
        Me.ToolStripButton3.Enabled = True
        Me.Panel15.Enabled = False
        ban = True
    End Sub

    Private Sub ConServiciosTelBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConServiciosTelBindingNavigatorSaveItem.Click
        Dim cone6 As New SqlClient.SqlConnection(MiConexion)
        If Me.TextBox8.Text.Trim.Length > 0 And IsNumeric(Me.TextBox8.Text) = False Then
            MsgBox("Se Debe de Capturar un Costo de Contrataci�n", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.TextBox8.Text.Trim.Length = 0 Then
            Me.TextBox8.Text = 0
        End If
        If Me.TextBox3.Text.Trim.Length > 0 And IsNumeric(Me.TextBox3.Text) = False Then
            MsgBox("Se Debe de Capturar un Costo de Mensualidad", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.TextBox3.Text.Trim.Length = 0 Then
            Me.TextBox3.Text = 0
        End If
        If Me.TextBox11.Text.Trim.Length > 0 And IsNumeric(Me.TextBox11.Text) = False Then
            MsgBox("Se Debe de Capturar un Costo de Instalaci�n", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.TextBox11.Text.Trim.Length = 0 Then
            Me.TextBox11.Text = 0
        End If
        If Me.TextBox12.Text.Trim.Length > 0 And IsNumeric(Me.TextBox12.Text) = False Then
            MsgBox("Se Debe de Capturar un Costo de Extensi�n Adicional", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.TextBox12.Text.Trim.Length = 0 Then
            Me.TextBox12.Text = 0
        End If
        If Me.TextBox17.Text.Trim.Length > 0 And IsNumeric(Me.TextBox17.Text) = False Then
            MsgBox("Se Debe de Capturar un Costo de Inal�mbrico", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.TextBox17.Text.Trim.Length = 0 Then
            Me.TextBox17.Text = 0
        End If
        If Me.TextBox18.Text.Trim.Length > 0 And IsNumeric(Me.TextBox18.Text) = False Then
            MsgBox("Se Debe de Capturar un Costo de Al�mbrico", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.TextBox18.Text.Trim.Length = 0 Then
            Me.TextBox18.Text = 0
        End If
        If Me.TextBox14.Text.Trim.Length > 0 And IsNumeric(Me.TextBox14.Text) = False Then
            MsgBox("Se Debe de Capturar un Costo de Llamada Adicional a Mismo Fraccionamiento", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.TextBox14.Text.Trim.Length = 0 Then
            Me.TextBox14.Text = 0
        End If
        'If Me.TextBox9.Text.Trim.Length > 0 And IsNumeric(Me.TextBox9.Text) = False Then
        '    MsgBox("Se Debe de Capturar un Costo de Minuto Adicional a Celular", MsgBoxStyle.Information)
        '    Exit Sub
        'ElseIf Me.TextBox9.Text.Trim.Length = 0 Then
        '    Me.TextBox9.Text = 0
        'End If
        'If Me.TextBox10.Text.Trim.Length > 0 And IsNumeric(Me.TextBox10.Text) = False Then
        '    MsgBox("Se Debe de Capturar un Costo de Minuto Adicional a 045", MsgBoxStyle.Information)
        '    Exit Sub
        'ElseIf Me.TextBox10.Text.Trim.Length = 0 Then
        '    Me.TextBox10.Text = 0
        'End If
        If Me.TextBox1.Text.Trim.Length > 0 And IsNumeric(Me.TextBox1.Text) = False Then
            MsgBox("Se Debe de Capturar un Costo de Llamada Adicional De Llamada Local", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.TextBox1.Text.Trim.Length = 0 Then
            Me.TextBox1.Text = 0
        End If
        If Me.TextBox2.Text.Trim.Length > 0 And IsNumeric(Me.TextBox2.Text) = False Then
            MsgBox("Se Debe de Capturar un Costo de Minuto Adicional De Larga Distancia Nacional", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.TextBox2.Text.Trim.Length = 0 Then
            Me.TextBox2.Text = 0
        End If
        If Me.TextBox4.Text.Trim.Length > 0 And IsNumeric(Me.TextBox4.Text) = False Then
            MsgBox("Se Debe de Capturar un Costo de Minuto Adicional De Larga Distancia Internacional", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.TextBox4.Text.Trim.Length = 0 Then
            Me.TextBox4.Text = 0
        End If
        If Me.TextBox7.Text.Trim.Length > 0 And IsNumeric(Me.TextBox7.Text) = False Then
            MsgBox("Se Debe de Capturar un Costo de Minuto Adicional a Estados Unidos y C�nada", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.TextBox7.Text.Trim.Length = 0 Then
            Me.TextBox7.Text = 0
        End If

        'If Me.TextBox15.Text.Trim.Length > 0 And IsNumeric(Me.TextBox15.Text) = False Then
        '    MsgBox("Se Debe de Capturar un Costo de Llamada Adicional a Numeros 01-900", MsgBoxStyle.Information)
        '    Exit Sub
        'ElseIf Me.TextBox15.Text.Trim.Length = 0 Then
        '    Me.TextBox15.Text = 0
        'End If


        If Me.ComboBox2.SelectedValue = 0 Then
            MsgBox("Se Debe de Seleccionar un D�a inicial", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.ComboBox3.SelectedValue = 0 Then
            MsgBox("Se Debe de Seleccionar un D�a Final", MsgBoxStyle.Information)
            Exit Sub
        End If

        If ComboBox2.SelectedValue > Me.ComboBox3.SelectedValue Then
            MsgBox("Se Debe de Seleccionar un D�a Final Menor al Inicial", MsgBoxStyle.Information)
            Exit Sub
        End If

        Guarda_ServiciosDigitales_Tel()

        Me.TextBox5.Text = Me.Clv_ServicioTextBox.Text
        Me.Clv_TipoClienteTextBox.Text = Me.ComboBox4.SelectedValue
        Me.Validate()
        If ban = False Then
            cone6.Open()
            Me.ConServiciosTelTableAdapter.Connection = cone6
            Me.ConServiciosTelTableAdapter.Insert(Me.TextBox5.Text, Me.Clv_TipoClienteTextBox.Text, "", Me.TextBox8.Text, Me.TextBox3.Text, Me.NumericUpDown2.Value, Me.NumericUpDown1.Value, Me.NumericUpDown7.Value, Me.NumericUpDown3.Value, Me.NumericUpDown4.Value, Me.NumericUpDown8.Value, Me.CheckBox1.CheckState, Me.TextBox1.Text, Me.TextBox2.Text, Me.TextBox4.Text, Me.CheckBox2.CheckState, Me.TextBox7.Text, Me.TextBox14.Text, Me.CheckBox3.CheckState, Me.TextBox11.Text, Me.TextBox12.Text, Me.CheckBox5.CheckState, Me.CheckBox4.CheckState)
            cone6.Close()
            bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Guardar En Precios por Tipo de Cobro", "Se Dio de Alta Cobro de Servicio" + CStr(Me.Clv_ServicioTextBox.Text), "En el Cobro de " + CStr(Me.ComboBox4.Text), GloCiudad)
            Nuevo_Dias_Aplica()
        ElseIf ban = True Then
            cone6.Open()
            Me.CONSERVICIOSBindingSource.EndEdit()
            Me.ConServiciosTelTableAdapter.Connection = cone6
            'MsgBox(Me.TextBox5.Text & "," & Me.Clv_TipoClienteTextBox.Text & "," & "" & "," & Me.TextBox8.Text & "," & Me.TextBox3.Text & "," & Me.NumericUpDown2.Value & "," & Me.NumericUpDown1.Value & "," & Me.NumericUpDown7.Value & "," & Me.NumericUpDown3.Value & "," & Me.NumericUpDown4.Value & "," & Me.NumericUpDown8.Value & "," & Me.CheckBox1.CheckState & "," & Me.TextBox1.Text & "," & Me.TextBox2.Text & "," & Me.TextBox4.Text & "," & Me.CheckBox2.CheckState & "," & Me.TextBox7.Text & "," & Me.TextBox14.Text & "," & Me.CheckBox3.CheckState & "," & Me.TextBox11.Text & "," & Me.TextBox12.Text & "," & Me.CheckBox5.CheckState & "," & Me.CheckBox4.CheckState)
            Me.ConServiciosTelTableAdapter.Update(Me.TextBox5.Text, Me.Clv_TipoClienteTextBox.Text, "", Me.TextBox8.Text, Me.TextBox3.Text, Me.NumericUpDown2.Value, Me.NumericUpDown1.Value, Me.NumericUpDown7.Value, Me.NumericUpDown3.Value, Me.NumericUpDown4.Value, Me.NumericUpDown8.Value, Me.CheckBox1.CheckState, Me.TextBox1.Text, Me.TextBox2.Text, Me.TextBox4.Text, Me.CheckBox2.CheckState, Me.TextBox7.Text, Me.TextBox14.Text, Me.CheckBox3.CheckState, Me.TextBox11.Text, Me.TextBox12.Text, Me.CheckBox5.CheckState, Me.CheckBox4.CheckState)
            cone6.Close()
            bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Guardar En Precios por Tipo de Cobro", "Se Modific� Cobro de Servicio " + CStr(Me.Clv_ServicioTextBox.Text), "En el Cobro de " + CStr(Me.ComboBox4.Text), GloCiudad)
            Modifica_Dias_Aplica()
        End If
        If Me.CheckBox6.CheckState = CheckState.Checked Then
            Me.Inserta_Todos()
        End If
        Me.BindingNavigatorAddNewItem.Enabled = True
        MsgBox("Se Ha Guardado con �xito", MsgBoxStyle.Information)
        activa(True)
        Me.Panel21.Enabled = False
        Me.Panel15.Enabled = True
        Me.CMBPanel4.Enabled = False
        Me.ConServiciosTelBindingNavigatorSaveItem.Enabled = False
        Limpia()

    End Sub

    Private Sub limpia()
        Me.TxtBoxCosto.Text = ""
        Me.ComboBox1.SelectedValue = 0

    End Sub

    Private Sub Inserta_Todos()
        Dim Cmd As New SqlCommand
        Dim ConAll As New SqlConnection(MiConexion)
        ConAll.Open()
        With Cmd
            .CommandText = "NueServiciosTel_Todos"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = ConAll
            Dim Pmt As New SqlParameter("@Clv_servicio ", SqlDbType.BigInt)
            Pmt.Direction = ParameterDirection.Input
            Pmt.Value = Me.TextBox5.Text
            .Parameters.Add(Pmt)
            Dim Pmt2 As New SqlParameter("@Clv_tipocliente", SqlDbType.Int)
            Pmt2.Direction = ParameterDirection.Input
            Pmt2.Value = Me.Clv_TipoClienteTextBox.Text
            .Parameters.Add(Pmt2)
            Dim Pmt3 As New SqlParameter("@Descripcion", SqlDbType.VarChar, 800)
            Pmt3.Direction = ParameterDirection.Input
            Pmt3.Value = ""
            .Parameters.Add(Pmt3)
            Dim Pmt4 As New SqlParameter("@Precio_Contratacion", SqlDbType.Money)
            Pmt4.Direction = ParameterDirection.Input
            Pmt4.Value = Me.TextBox8.Text
            .Parameters.Add(Pmt4)
            Dim Pmt5 As New SqlParameter("@Mensualidad", SqlDbType.Money)
            Pmt5.Direction = ParameterDirection.Input
            Pmt5.Value = Me.TextBox3.Text
            .Parameters.Add(Pmt5)
            Dim Pmt6 As New SqlParameter("@Extensiones_SinCosto", SqlDbType.Int)
            Pmt6.Direction = ParameterDirection.Input
            Pmt6.Value = Me.NumericUpDown2.Value
            .Parameters.Add(Pmt6)
            Dim Pmt7 As New SqlParameter("@Llamadas_Locales", SqlDbType.Int)
            Pmt7.Direction = ParameterDirection.Input
            Pmt7.Value = Me.NumericUpDown1.Value
            .Parameters.Add(Pmt7)
            Dim Pmt8 As New SqlParameter("@Minutos_EUA_Can", SqlDbType.Int)
            Pmt8.Direction = ParameterDirection.Input
            Pmt8.Value = Me.NumericUpDown7.Value
            .Parameters.Add(Pmt8)
            Dim Pmt9 As New SqlParameter("@Minutos_LDN", SqlDbType.Int)
            Pmt9.Direction = ParameterDirection.Input
            Pmt9.Value = Me.NumericUpDown3.Value
            .Parameters.Add(Pmt9)
            Dim Pmt10 As New SqlParameter("@Minutos_LDI", SqlDbType.Int)
            Pmt10.Direction = ParameterDirection.Input
            Pmt10.Value = Me.NumericUpDown4.Value
            .Parameters.Add(Pmt10)
            Dim Pmt11 As New SqlParameter("@Minutos_Fraccionamiento", SqlDbType.Int)
            Pmt11.Direction = ParameterDirection.Input
            Pmt11.Value = Me.NumericUpDown8.Value
            .Parameters.Add(Pmt11)
            Dim Pmt12 As New SqlParameter("@Ilimitadas_Loc", SqlDbType.Bit)
            Pmt12.Direction = ParameterDirection.Input
            Pmt12.Value = Me.CheckBox1.CheckState
            .Parameters.Add(Pmt12)
            Dim Pmt13 As New SqlParameter("@Costo_Lamada_Adic", SqlDbType.Money)
            Pmt13.Direction = ParameterDirection.Input
            Pmt13.Value = Me.TextBox1.Text
            .Parameters.Add(Pmt13)
            Dim Pmt14 As New SqlParameter("@Costo_Min_LDN_Adic", SqlDbType.Money)
            Pmt14.Direction = ParameterDirection.Input
            Pmt14.Value = Me.TextBox2.Text
            .Parameters.Add(Pmt14)
            Dim Pmt15 As New SqlParameter("@Costo_Min_LDI_Adic", SqlDbType.Money)
            Pmt15.Direction = ParameterDirection.Input
            Pmt15.Value = Me.TextBox4.Text
            .Parameters.Add(Pmt15)
            Dim Pmt16 As New SqlParameter("@Ilimitadas_Frac", SqlDbType.Bit)
            Pmt16.Direction = ParameterDirection.Input
            Pmt16.Value = Me.CheckBox2.CheckState
            .Parameters.Add(Pmt16)
            Dim Pmt17 As New SqlParameter("@Costo_Min_EUA_Can", SqlDbType.Money)
            Pmt17.Direction = ParameterDirection.Input
            Pmt17.Value = Me.TextBox7.Text
            .Parameters.Add(Pmt17)
            Dim Pmt18 As New SqlParameter("@Costo_fraccionamiento", SqlDbType.Money)
            Pmt18.Direction = ParameterDirection.Input
            Pmt18.Value = Me.TextBox14.Text
            .Parameters.Add(Pmt18)
            Dim Pmt19 As New SqlParameter("@Ilimitadas_LDN", SqlDbType.Bit)
            Pmt19.Direction = ParameterDirection.Input
            Pmt19.Value = Me.CheckBox3.CheckState
            .Parameters.Add(Pmt19)
            Dim Pmt20 As New SqlParameter("@Costo_Instalacion", SqlDbType.Money)
            Pmt20.Direction = ParameterDirection.Input
            Pmt20.Value = Me.TextBox11.Text
            .Parameters.Add(Pmt20)
            Dim Pmt21 As New SqlParameter("@Costo_Extension_Adic", SqlDbType.Money)
            Pmt21.Direction = ParameterDirection.Input
            Pmt21.Value = Me.TextBox12.Text
            .Parameters.Add(Pmt21)
            Dim Pmt22 As New SqlParameter("@Ilimitadas_LDI ", SqlDbType.Bit)
            Pmt22.Direction = ParameterDirection.Input
            Pmt22.Value = Me.CheckBox5.CheckState
            .Parameters.Add(Pmt22)
            Dim Pmt23 As New SqlParameter("@Ilimitadas_Eua", SqlDbType.Bit)
            Pmt23.Direction = ParameterDirection.Input
            Pmt23.Value = Me.CheckBox4.CheckState
            .Parameters.Add(Pmt23)
            .ExecuteNonQuery()
        End With
        ConAll.Close()
    End Sub


    Private Sub Inserta_Uno()
        Dim Cmd As New SqlCommand
        Dim ConAll As New SqlConnection(MiConexion)
        ConAll.Open()
        With Cmd
            .CommandText = "NueServiciosTel_Todos"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = ConAll
            Dim Pmt As New SqlParameter("@Clv_servicio ", SqlDbType.BigInt)
            Pmt.Direction = ParameterDirection.Input
            Pmt.Value = Me.TextBox5.Text
            .Parameters.Add(Pmt)
            Dim Pmt2 As New SqlParameter("@Clv_tipocliente", SqlDbType.Int)
            Pmt2.Direction = ParameterDirection.Input
            Pmt2.Value = Me.Clv_TipoClienteTextBox.Text
            .Parameters.Add(Pmt2)
            Dim Pmt3 As New SqlParameter("@Descripcion", SqlDbType.VarChar, 800)
            Pmt3.Direction = ParameterDirection.Input
            Pmt3.Value = ""
            .Parameters.Add(Pmt3)
            Dim Pmt4 As New SqlParameter("@Precio_Contratacion", SqlDbType.Money)
            Pmt4.Direction = ParameterDirection.Input
            Pmt4.Value = Me.TextBox8.Text
            .Parameters.Add(Pmt4)
            Dim Pmt5 As New SqlParameter("@Mensualidad", SqlDbType.Money)
            Pmt5.Direction = ParameterDirection.Input
            Pmt5.Value = Me.TextBox3.Text
            .Parameters.Add(Pmt5)
            Dim Pmt6 As New SqlParameter("@Extensiones_SinCosto", SqlDbType.Int)
            Pmt6.Direction = ParameterDirection.Input
            Pmt6.Value = Me.NumericUpDown2.Value
            .Parameters.Add(Pmt6)
            Dim Pmt7 As New SqlParameter("@Llamadas_Locales", SqlDbType.Int)
            Pmt7.Direction = ParameterDirection.Input
            Pmt7.Value = Me.NumericUpDown1.Value
            .Parameters.Add(Pmt7)
            Dim Pmt8 As New SqlParameter("@Minutos_EUA_Can", SqlDbType.Int)
            Pmt8.Direction = ParameterDirection.Input
            Pmt8.Value = Me.NumericUpDown7.Value
            .Parameters.Add(Pmt8)
            Dim Pmt9 As New SqlParameter("@Minutos_LDN", SqlDbType.Int)
            Pmt9.Direction = ParameterDirection.Input
            Pmt9.Value = Me.NumericUpDown3.Value
            .Parameters.Add(Pmt9)
            Dim Pmt10 As New SqlParameter("@Minutos_LDI", SqlDbType.Int)
            Pmt10.Direction = ParameterDirection.Input
            Pmt10.Value = Me.NumericUpDown4.Value
            .Parameters.Add(Pmt10)
            Dim Pmt11 As New SqlParameter("@Minutos_Fraccionamiento", SqlDbType.Int)
            Pmt11.Direction = ParameterDirection.Input
            Pmt11.Value = Me.NumericUpDown8.Value
            .Parameters.Add(Pmt11)
            Dim Pmt12 As New SqlParameter("@Ilimitadas_Loc", SqlDbType.Bit)
            Pmt12.Direction = ParameterDirection.Input
            Pmt12.Value = Me.CheckBox1.CheckState
            .Parameters.Add(Pmt12)
            Dim Pmt13 As New SqlParameter("@Costo_Lamada_Adic", SqlDbType.Money)
            Pmt13.Direction = ParameterDirection.Input
            Pmt13.Value = Me.TextBox1.Text
            .Parameters.Add(Pmt13)
            Dim Pmt14 As New SqlParameter("@Costo_Min_LDN_Adic", SqlDbType.Money)
            Pmt14.Direction = ParameterDirection.Input
            Pmt14.Value = Me.TextBox2.Text
            .Parameters.Add(Pmt14)
            Dim Pmt15 As New SqlParameter("@Costo_Min_LDI_Adic", SqlDbType.Money)
            Pmt15.Direction = ParameterDirection.Input
            Pmt15.Value = Me.TextBox4.Text
            .Parameters.Add(Pmt15)
            Dim Pmt16 As New SqlParameter("@Ilimitadas_Frac", SqlDbType.Bit)
            Pmt16.Direction = ParameterDirection.Input
            Pmt16.Value = Me.CheckBox2.CheckState
            .Parameters.Add(Pmt16)
            Dim Pmt17 As New SqlParameter("@Costo_Min_EUA_Can", SqlDbType.Money)
            Pmt17.Direction = ParameterDirection.Input
            Pmt17.Value = Me.TextBox7.Text
            .Parameters.Add(Pmt17)
            Dim Pmt18 As New SqlParameter("@Costo_fraccionamiento", SqlDbType.Money)
            Pmt18.Direction = ParameterDirection.Input
            Pmt18.Value = Me.TextBox14.Text
            .Parameters.Add(Pmt18)
            Dim Pmt19 As New SqlParameter("@Ilimitadas_LDN", SqlDbType.Bit)
            Pmt19.Direction = ParameterDirection.Input
            Pmt19.Value = Me.CheckBox3.CheckState
            .Parameters.Add(Pmt19)
            Dim Pmt20 As New SqlParameter("@Costo_Instalacion", SqlDbType.Money)
            Pmt20.Direction = ParameterDirection.Input
            Pmt20.Value = Me.TextBox11.Text
            .Parameters.Add(Pmt20)
            Dim Pmt21 As New SqlParameter("@Costo_Extension_Adic", SqlDbType.Money)
            Pmt21.Direction = ParameterDirection.Input
            Pmt21.Value = Me.TextBox12.Text
            .Parameters.Add(Pmt21)
            Dim Pmt22 As New SqlParameter("@Ilimitadas_LDI ", SqlDbType.Bit)
            Pmt22.Direction = ParameterDirection.Input
            Pmt22.Value = Me.CheckBox5.CheckState
            .Parameters.Add(Pmt22)
            Dim Pmt23 As New SqlParameter("@Ilimitadas_Eua", SqlDbType.Bit)
            Pmt23.Direction = ParameterDirection.Input
            Pmt23.Value = Me.CheckBox4.CheckState
            .Parameters.Add(Pmt23)
            .ExecuteNonQuery()
        End With
        ConAll.Close()
    End Sub

    Private Sub ToolStripButton3_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton3.Click
        Me.ConServiciosTelBindingSource.CancelEdit()
        Me.ConServiciosTelBindingNavigatorSaveItem.Enabled = False
        Me.CMBPanel4.Enabled = False
        Me.ComboBox4.Text = Nothing
        Me.BindingNavigatorAddNewItem.Enabled = True
        Me.ToolStripButton3.Enabled = False
        Me.Panel15.Enabled = True
        Me.Panel21.Enabled = False
        activa(True)
    End Sub

    Private Sub ToolStripButton2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        Dim cone1 As New SqlClient.SqlConnection(MiConexion)
        If IsNumeric(Me.ComboBox4.SelectedValue) = True And Me.ComboBox4.SelectedValue > 0 Then
            cone1.Open()
            elimina_serv()
            elimina_Dias()
            Me.ConServiciosTelTableAdapter.Connection = cone1
            Me.ConServiciosTelTableAdapter.Delete(Me.Clv_ServicioTextBox.Text, Me.ComboBox4.SelectedValue)
            cone1.Close()
            bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Eliminar en Precios por Tipo de Cobro", "Se Elimin� Cobro de Servicio " + CStr(Me.Clv_ServicioTextBox.Text), "En el Cobro de " + CStr(Me.ComboBox4.Text), GloCiudad)
            MsgBox("Se Ha Borrado con �xito", MsgBoxStyle.Information)
            Me.Panel15.Enabled = True
            Me.Panel21.Enabled = False
            activa(True)
        End If
    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If Me.CheckBox2.CheckState = CheckState.Checked Then
            Me.NumericUpDown8.Enabled = False
            'Me.CheckBox2.Checked = True
        Else
            'Me.CheckBox2.Checked = False
            Me.NumericUpDown8.Enabled = True
        End If
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If Me.CheckBox1.CheckState = CheckState.Checked Then
            ' Me.CheckBox1.Checked = False
            Me.NumericUpDown1.Enabled = False

        Else
            ' Me.CheckBox1.Checked = True
            Me.NumericUpDown1.Enabled = True
        End If
    End Sub

    Private Sub CheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox3.CheckedChanged
        If Me.CheckBox3.CheckState = CheckState.Checked Then
            'Me.CheckBox3.Checked = False
            Me.NumericUpDown3.Enabled = False
        Else
            'Me.CheckBox3.Checked = True
            Me.NumericUpDown3.Enabled = True
        End If
    End Sub

    Private Sub CheckBox4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox4.CheckedChanged
        If Me.CheckBox4.CheckState = CheckState.Checked Then
            'Me.CheckBox4.Checked = False
            Me.NumericUpDown7.Enabled = False
        Else
            'Me.CheckBox4.Checked = True
            Me.NumericUpDown7.Enabled = True
        End If
    End Sub

    Private Sub CheckBox5_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox5.CheckedChanged
        If Me.CheckBox5.CheckState = CheckState.Checked Then
            'Me.CheckBox5.Checked = False
            Me.NumericUpDown4.Enabled = False
        Else
            'Me.CheckBox5.Checked = True
            Me.NumericUpDown4.Enabled = True
        End If
    End Sub

    '**************************
    Protected Sub DameClv_Session()
        'Se genera una nueva clave de session 
        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("DAME_Session_ServiciosDigitales", con)
        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New SqlParameter( _
                  "@Clv_Session", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Output
        prm.Value = 0
        cmd.Parameters.Add(prm)

        Try
            con.Open()
            Dim i As Integer = cmd.ExecuteNonQuery()
            Dim gloclave As Integer = 0
            'Asignamos el valor al TxtClv_Session
            TxtClv_Session.Text = prm.Value
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            If con IsNot Nothing Then
                con.Close()
            End If
        End Try
        con.Close()
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged

        If CInt(Me.ComboBox1.SelectedValue) = 0 Then
            'Limpio los textBox
            Me.TxtBoxCosto.Text = ""
        Else
            'Llena los TextBox
            DameDatos()

        End If

    End Sub

    Protected Sub Modifica_Dias_Aplica()

        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("MODIFICA_Dias_Aplica", con)

        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New Data.SqlClient.SqlParameter( _
                 "@Clv_Servicio", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.Clv_ServicioTextBox.Text)
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                  "@Clv_Tipocobro", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.ComboBox4.SelectedValue)
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
               "@Dia_Inicio", SqlDbType.Int)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.ComboBox2.SelectedValue)
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
              "@Dia_Final", SqlDbType.Int)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.ComboBox3.SelectedValue)
        cmd.Parameters.Add(prm)

        Try
            con.Open()

            Dim i As Integer = cmd.ExecuteNonQuery()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            If con IsNot Nothing Then
                con.Close()
            End If
        End Try
        con.Close()

    End Sub

    Protected Sub Nuevo_Dias_Aplica()

        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("NUEVO_Dias_Aplica", con)

        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New Data.SqlClient.SqlParameter( _
                 "@Clv_Servicio", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.Clv_ServicioTextBox.Text)
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                  "@Clv_Tipocobro", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.ComboBox4.SelectedValue)
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
               "@Dia_Inicio", SqlDbType.Int)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.ComboBox2.SelectedValue)
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
              "@Dia_Final", SqlDbType.Int)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.ComboBox3.SelectedValue)
        cmd.Parameters.Add(prm)

        Try
            con.Open()

            Dim i As Integer = cmd.ExecuteNonQuery()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            If con IsNot Nothing Then
                con.Close()
            End If
        End Try
        con.Close()

    End Sub
    Protected Sub DameDatos()

        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("Consulta_ServiciosDigitales", con)

        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New Data.SqlClient.SqlParameter( _
                  "@clv_ServicioDigital", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.ComboBox1.SelectedValue)
        cmd.Parameters.Add(prm)

        con.Open()

        Dim reader As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Try
            While reader.Read()

                'Llenamos los TxtBox con los datos respectivos
                Me.TxtBoxCosto.Text = reader(2).ToString 'Costo

            End While
        Finally
            reader.Close()
        End Try
        con.Close()

    End Sub
    Private Sub Agregar()
        '******** BOTON DE AGREGAR ********
        'Validaci�n de los camp�s a Agregar
        If CInt(Me.ComboBox4.SelectedValue) = 0 Then
            MsgBox("SELECCIONE EL TIPO DE COBRO", MsgBoxStyle.Information)
            Me.ComboBox4.Focus()
            Exit Sub
        End If

        If CInt(Me.ComboBox1.SelectedValue) = 0 Then
            MsgBox("SELECCIONE EL SERVICIO DIGITAL", MsgBoxStyle.Information)
            Me.ComboBox1.Focus()
            Exit Sub
        End If

        If IsNumeric(Me.TxtBoxCosto.Text) = False Then
            MsgBox("INGRESE EL IMPORTE", MsgBoxStyle.Information)
            Me.TxtBoxCosto.Text = ""
            Me.TxtBoxCosto.Focus()
            Exit Sub
        End If

        If CDec(Me.TxtBoxCosto.Text) <= 0 Then
            MsgBox("INGRESE EL IMPORTE", MsgBoxStyle.Information)
            Me.TxtBoxCosto.Text = ""
            Me.TxtBoxCosto.Focus()
            Exit Sub
        End If

        'Cuando AGREGA Inserta los datos en la tabla PreServiciosDigitales_Telefonia
        'y todavia NO GUARDA en ServiciosDigitales_Telefonia

        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("Nuevo_Rel_ServiciosDigitales_Tel_TMP", con)
        cmd.CommandType = CommandType.StoredProcedure

        '@Clv_Session bigint, @Clv_Servicio bigint, @Clv_Tipocobro bigint, @Clv_ServicioDigital bigint, @Costo MONEY

        Dim prm As New SqlParameter( _
                  "@Clv_Session", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        If Me.TxtClv_Session.Text = "" Then
            prm.Value = 0
        Else
            prm.Value = CInt(Me.TxtClv_Session.Text)
        End If
        cmd.Parameters.Add(prm)


        prm = New SqlParameter( _
                  "@Clv_Servicio", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        If Me.Clv_ServicioTextBox.Text = "" Then
            prm.Value = 0
        Else
            prm.Value = CInt(Me.Clv_ServicioTextBox.Text)
        End If
        cmd.Parameters.Add(prm)


        prm = New SqlParameter( _
                 "@Clv_Tipocobro", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.ComboBox4.SelectedValue)
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                "@Clv_ServicioDigital", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.ComboBox1.SelectedValue)
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                 "@Costo", SqlDbType.Money)
        prm.Direction = ParameterDirection.Input
        prm.Value = CDec(Me.TxtBoxCosto.Text)
        cmd.Parameters.Add(prm)

        Try
            con.Open()

            Dim i As Integer = cmd.ExecuteNonQuery()

            'Limpio los campos.
            Me.TxtBoxCosto.Text = ""
            Me.ComboBox1.SelectedValue = 0

            'Llamado a busca para llenar el Grid
            busca_Servicios()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            If con IsNot Nothing Then
                con.Close()
            End If
        End Try
        con.Close()

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Agregar()
    End Sub

    Protected Sub busca_Servicios()

        Dim con As New SqlClient.SqlConnection(MiConexion)
        con.Open()

        Me.Consultar_Rel_ServiciosDigitales_Tel_TMPTableAdapter.Connection = con

        If Me.Clv_ServicioTextBox.Text = "" Then
            Me.Consultar_Rel_ServiciosDigitales_Tel_TMPTableAdapter.Fill(Me.DataSetyahve.Consultar_Rel_ServiciosDigitales_Tel_TMP, CInt(Me.TxtClv_Session.Text), 0, CInt(Me.ComboBox4.SelectedValue))
        Else
            Me.Consultar_Rel_ServiciosDigitales_Tel_TMPTableAdapter.Fill(Me.DataSetyahve.Consultar_Rel_ServiciosDigitales_Tel_TMP, CInt(Me.TxtClv_Session.Text), CInt(Me.Clv_ServicioTextBox.Text), CInt(Me.ComboBox4.SelectedValue))
        End If

        con.Close()

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        Borra_PreServicios()
    End Sub

    Protected Sub Borra_Datos()
    
        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("Borra_Datos_Tel", con)
        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New SqlParameter( _
                  "@Clv_Servicio", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        If Me.Clv_ServicioTextBox.Text = "" Then
            prm.Value = 0
        Else
            prm.Value = CInt(Me.Clv_ServicioTextBox.Text)
        End If
        cmd.Parameters.Add(prm)


        Try
            con.Open()

            Dim i As Integer = cmd.ExecuteNonQuery()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            If con IsNot Nothing Then
                con.Close()
            End If
        End Try
        con.Close()

        CONSULTA_Dias_Aplica()
        'Llamado a busca para llenar el Grid
        busca_Servicios()

    End Sub

    Protected Sub Borra_PreServicios()
        Try
            Dim con As New SqlConnection(MiConexion)
            con.Open()

            Me.Borrar_Rel_ServiciosDigitales_Tel_TMPTableAdapter.Connection = con
            If IsNumeric(Me.Clv_ServicioTextBox.Text) = False Then
                Me.Clv_ServicioTextBox.Text = 0
            End If

            If IsNumeric(Me.Clv_ServicioDigitalTextBox.Text) = True Then
                Me.Borrar_Rel_ServiciosDigitales_Tel_TMPTableAdapter.Fill(Me.DataSetyahve.Borrar_Rel_ServiciosDigitales_Tel_TMP, CInt(Me.TxtClv_Session.Text), CInt(Me.Clv_ServicioTextBox.Text), CInt(Me.ComboBox4.SelectedValue), CInt(Me.Clv_ServicioDigitalTextBox.Text))
            End If
            con.Close()
            'Llamado a busca para llenar el Grid
            busca_Servicios()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Protected Sub Guarda_ServiciosDigitales_Tel()
        'Cuando GUARDA Inserta los datos en la tabla ServiciosDigitales_Telefonia

        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("Guarda_Rel_ServiciosDigitales_Tel", con)
        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New SqlParameter( _
                  "@Clv_session", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        If Me.TxtClv_Session.Text = "" Then
            prm.Value = 0
        Else
            prm.Value = CInt(Me.TxtClv_Session.Text)
        End If
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                  "@Clv_Servicio", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        If Me.Clv_ServicioTextBox.Text = "" Then
            prm.Value = 0
        Else
            prm.Value = CInt(Me.Clv_ServicioTextBox.Text)
        End If
        cmd.Parameters.Add(prm)

        Try
            con.Open()

            Dim i As Integer = cmd.ExecuteNonQuery()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            If con IsNot Nothing Then
                con.Close()
            End If
        End Try
        con.Close()
    End Sub
    Protected Sub elimina_Dias()

        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("Elimina_Dias_Aplica", con)
        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New SqlParameter( _
                  "@Clv_Servicio", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        If Me.Clv_ServicioTextBox.Text = "" Then
            prm.Value = 0
        Else
            prm.Value = CInt(Me.Clv_ServicioTextBox.Text)
        End If
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                 "@Clv_Tipocobro", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.ComboBox4.SelectedValue)
        cmd.Parameters.Add(prm)

        Try
            con.Open()

            Dim i As Integer = cmd.ExecuteNonQuery()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            If con IsNot Nothing Then
                con.Close()
            End If
        End Try
        con.Close()
        CONSULTA_Dias_Aplica()
    End Sub

    Protected Sub elimina_serv()
        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("Elimina_ServiciosDigitales_tel", con)
        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New SqlParameter( _
                  "@Clv_Servicio", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        If Me.Clv_ServicioTextBox.Text = "" Then
            prm.Value = 0
        Else
            prm.Value = CInt(Me.Clv_ServicioTextBox.Text)
        End If
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                 "@Clv_Tipocobro", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.ComboBox4.SelectedValue)
        cmd.Parameters.Add(prm)

        Try
            con.Open()

            Dim i As Integer = cmd.ExecuteNonQuery()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            If con IsNot Nothing Then
                con.Close()
            End If
        End Try
        con.Close()

        'Llamado a busca para llenar el Grid
        busca_Servicios()
    End Sub

    Private Sub TabPage5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TabPage5.Click

    End Sub

    Private Sub TxtBoxCosto_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtBoxCosto.KeyPress
        e.KeyChar = ChrW(ValidaKey(TxtBoxCosto, Asc(e.KeyChar), "L"))
        If Asc(e.KeyChar) = 13 Then
            Agregar()
        End If
    End Sub

    Private Sub CostoTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CostoTextBox.TextChanged
        If Me.ComboBox1.Enabled = True Then
            Me.ComboBox1.SelectedValue = CInt(Me.Clv_ServicioDigitalTextBox.Text)
            Me.TxtBoxCosto.Text = Me.CostoTextBox.Text
        End If
    End Sub

End Class

