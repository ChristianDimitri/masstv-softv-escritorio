Imports System.Data.SqlClient
Public Class FrmSelRepAgendaTecnico

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub FrmSelRepAgendaTecnico_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()  
        'Me.Muestra_Tecnicos_AlmacenTableAdapter.Connection = CON
        'Me.Muestra_Tecnicos_AlmacenTableAdapter.Fill(Me.DataSetarnoldo.Muestra_Tecnicos_Almacen, 0)
        'Me.ComboBox1.Text = ""
        'CON.Close()
        colorea(Me, Me.Name)
        lblTurno.ForeColor = Color.Black
        uspMuestraTecnicosAlmacenReportes(0, gloClvTipSerReportes)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        LocFecha1Agnd = Me.DateTimePicker1.Text
        LocFecha2Agnd = Me.DateTimePicker2.Text
        If Me.ComboBox1.Text = "" Then
            LocClv_tecnico = 0
            LocopAgnd = 0
        ElseIf Me.ComboBox1.Text <> "" Then
            LocClv_tecnico = Me.ComboBox1.SelectedValue
            LocopAgnd = 1
        End If
        Locbndagnd = True
        FrmImprimirContrato.Show()
        Me.Close()
    End Sub

    Private Sub cmbxTurno_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbxTurno.SelectedValueChanged
        Try
            LocTurnoAgnd = cmbxTurno.Text
        Catch ex As Exception
        End Try
    End Sub

    Private Sub uspMuestraTecnicosAlmacenReportes(ByVal prmOp As Integer, ByVal prmClvTipSer As Integer)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@op", SqlDbType.Int, prmOp)
            BaseII.CreateMyParameter("@clvTipSer", SqlDbType.Int, prmClvTipSer)
            Me.ComboBox1.DataSource = BaseII.ConsultaDT("uspMuestraTecnicosAlmacenReportes")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class