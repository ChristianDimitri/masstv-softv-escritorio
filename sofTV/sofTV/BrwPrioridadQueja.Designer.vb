﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwPrioridadQueja
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.lblDatosConcepto = New System.Windows.Forms.Label()
        Me.lblDatosClave = New System.Windows.Forms.Label()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.txtConcepto = New System.Windows.Forms.TextBox()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.txtClave = New System.Windows.Forms.TextBox()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnConsulta = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnBuscarConcepto = New System.Windows.Forms.Button()
        Me.btnBuscarClave = New System.Windows.Forms.Button()
        Me.pnlDatos = New System.Windows.Forms.Panel()
        Me.lblInfConcepto = New System.Windows.Forms.TextBox()
        Me.lblDatos = New System.Windows.Forms.Label()
        Me.lblInfClave = New System.Windows.Forms.Label()
        Me.dgvPrioridades = New System.Windows.Forms.DataGridView()
        Me.Clave = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Concepto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblConcepto = New System.Windows.Forms.Label()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.lblTitulo = New System.Windows.Forms.Label()
        Me.lblClave = New System.Windows.Forms.Label()
        Me.pnlDatos.SuspendLayout()
        CType(Me.dgvPrioridades, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblDatosConcepto
        '
        Me.lblDatosConcepto.AutoSize = True
        Me.lblDatosConcepto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDatosConcepto.ForeColor = System.Drawing.Color.White
        Me.lblDatosConcepto.Location = New System.Drawing.Point(17, 68)
        Me.lblDatosConcepto.Name = "lblDatosConcepto"
        Me.lblDatosConcepto.Size = New System.Drawing.Size(75, 15)
        Me.lblDatosConcepto.TabIndex = 3
        Me.lblDatosConcepto.Text = "Concepto :"
        '
        'lblDatosClave
        '
        Me.lblDatosClave.AutoSize = True
        Me.lblDatosClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDatosClave.ForeColor = System.Drawing.Color.White
        Me.lblDatosClave.Location = New System.Drawing.Point(17, 36)
        Me.lblDatosClave.Name = "lblDatosClave"
        Me.lblDatosClave.Size = New System.Drawing.Size(50, 15)
        Me.lblDatosClave.TabIndex = 1
        Me.lblDatosClave.Text = "Clave :"
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.DarkOrange
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.ForeColor = System.Drawing.Color.Black
        Me.btnSalir.Location = New System.Drawing.Point(868, 667)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(136, 36)
        Me.btnSalir.TabIndex = 22
        Me.btnSalir.Text = "&SALIR"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'txtConcepto
        '
        Me.txtConcepto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtConcepto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtConcepto.Location = New System.Drawing.Point(17, 151)
        Me.txtConcepto.Name = "txtConcepto"
        Me.txtConcepto.Size = New System.Drawing.Size(243, 21)
        Me.txtConcepto.TabIndex = 7
        '
        'btnModificar
        '
        Me.btnModificar.BackColor = System.Drawing.Color.Orange
        Me.btnModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnModificar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnModificar.ForeColor = System.Drawing.Color.Black
        Me.btnModificar.Location = New System.Drawing.Point(868, 96)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(136, 36)
        Me.btnModificar.TabIndex = 20
        Me.btnModificar.Text = "&MODIFICAR"
        Me.btnModificar.UseVisualStyleBackColor = False
        '
        'txtClave
        '
        Me.txtClave.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClave.Location = New System.Drawing.Point(17, 72)
        Me.txtClave.Name = "txtClave"
        Me.txtClave.Size = New System.Drawing.Size(88, 21)
        Me.txtClave.TabIndex = 5
        '
        'btnImprimir
        '
        Me.btnImprimir.BackColor = System.Drawing.Color.DarkOrange
        Me.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImprimir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImprimir.ForeColor = System.Drawing.Color.Black
        Me.btnImprimir.Location = New System.Drawing.Point(868, 616)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(136, 36)
        Me.btnImprimir.TabIndex = 21
        Me.btnImprimir.Text = "&IMPRIMIR"
        Me.btnImprimir.UseVisualStyleBackColor = False
        Me.btnImprimir.Visible = False
        '
        'btnConsulta
        '
        Me.btnConsulta.BackColor = System.Drawing.Color.Orange
        Me.btnConsulta.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnConsulta.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConsulta.ForeColor = System.Drawing.Color.Black
        Me.btnConsulta.Location = New System.Drawing.Point(868, 54)
        Me.btnConsulta.Name = "btnConsulta"
        Me.btnConsulta.Size = New System.Drawing.Size(136, 36)
        Me.btnConsulta.TabIndex = 19
        Me.btnConsulta.Text = "&CONSULTAR"
        Me.btnConsulta.UseVisualStyleBackColor = False
        '
        'btnNuevo
        '
        Me.btnNuevo.BackColor = System.Drawing.Color.Orange
        Me.btnNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuevo.ForeColor = System.Drawing.Color.Black
        Me.btnNuevo.Location = New System.Drawing.Point(868, 12)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(136, 36)
        Me.btnNuevo.TabIndex = 18
        Me.btnNuevo.Text = "&NUEVO"
        Me.btnNuevo.UseVisualStyleBackColor = False
        '
        'btnBuscarConcepto
        '
        Me.btnBuscarConcepto.BackColor = System.Drawing.Color.DarkOrange
        Me.btnBuscarConcepto.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuscarConcepto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscarConcepto.ForeColor = System.Drawing.Color.Black
        Me.btnBuscarConcepto.Location = New System.Drawing.Point(17, 178)
        Me.btnBuscarConcepto.Name = "btnBuscarConcepto"
        Me.btnBuscarConcepto.Size = New System.Drawing.Size(88, 23)
        Me.btnBuscarConcepto.TabIndex = 8
        Me.btnBuscarConcepto.Text = "&Buscar"
        Me.btnBuscarConcepto.UseVisualStyleBackColor = False
        '
        'btnBuscarClave
        '
        Me.btnBuscarClave.BackColor = System.Drawing.Color.DarkOrange
        Me.btnBuscarClave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuscarClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscarClave.ForeColor = System.Drawing.Color.Black
        Me.btnBuscarClave.Location = New System.Drawing.Point(17, 99)
        Me.btnBuscarClave.Name = "btnBuscarClave"
        Me.btnBuscarClave.Size = New System.Drawing.Size(88, 23)
        Me.btnBuscarClave.TabIndex = 6
        Me.btnBuscarClave.Text = "&Buscar"
        Me.btnBuscarClave.UseVisualStyleBackColor = False
        '
        'pnlDatos
        '
        Me.pnlDatos.BackColor = System.Drawing.Color.DarkOrange
        Me.pnlDatos.Controls.Add(Me.lblInfConcepto)
        Me.pnlDatos.Controls.Add(Me.lblDatos)
        Me.pnlDatos.Controls.Add(Me.lblDatosConcepto)
        Me.pnlDatos.Controls.Add(Me.lblDatosClave)
        Me.pnlDatos.Controls.Add(Me.lblInfClave)
        Me.pnlDatos.Location = New System.Drawing.Point(17, 373)
        Me.pnlDatos.Name = "pnlDatos"
        Me.pnlDatos.Size = New System.Drawing.Size(243, 299)
        Me.pnlDatos.TabIndex = 8
        '
        'lblInfConcepto
        '
        Me.lblInfConcepto.BackColor = System.Drawing.Color.Gray
        Me.lblInfConcepto.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblInfConcepto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInfConcepto.ForeColor = System.Drawing.Color.Black
        Me.lblInfConcepto.Location = New System.Drawing.Point(20, 86)
        Me.lblInfConcepto.Multiline = True
        Me.lblInfConcepto.Name = "lblInfConcepto"
        Me.lblInfConcepto.ReadOnly = True
        Me.lblInfConcepto.Size = New System.Drawing.Size(207, 78)
        Me.lblInfConcepto.TabIndex = 6
        Me.lblInfConcepto.TabStop = False
        '
        'lblDatos
        '
        Me.lblDatos.AutoSize = True
        Me.lblDatos.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDatos.ForeColor = System.Drawing.Color.White
        Me.lblDatos.Location = New System.Drawing.Point(3, 0)
        Me.lblDatos.Name = "lblDatos"
        Me.lblDatos.Size = New System.Drawing.Size(177, 20)
        Me.lblDatos.TabIndex = 5
        Me.lblDatos.Text = "Datos de la Prioridad"
        '
        'lblInfClave
        '
        Me.lblInfClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInfClave.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lblInfClave.Location = New System.Drawing.Point(75, 36)
        Me.lblInfClave.Name = "lblInfClave"
        Me.lblInfClave.Size = New System.Drawing.Size(152, 23)
        Me.lblInfClave.TabIndex = 2
        '
        'dgvPrioridades
        '
        Me.dgvPrioridades.AllowUserToAddRows = False
        Me.dgvPrioridades.AllowUserToDeleteRows = False
        Me.dgvPrioridades.AllowUserToOrderColumns = True
        Me.dgvPrioridades.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Chocolate
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPrioridades.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvPrioridades.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPrioridades.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clave, Me.Concepto})
        Me.dgvPrioridades.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvPrioridades.Location = New System.Drawing.Point(0, 0)
        Me.dgvPrioridades.Name = "dgvPrioridades"
        Me.dgvPrioridades.ReadOnly = True
        Me.dgvPrioridades.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPrioridades.Size = New System.Drawing.Size(554, 694)
        Me.dgvPrioridades.TabIndex = 1
        Me.dgvPrioridades.TabStop = False
        '
        'Clave
        '
        Me.Clave.DataPropertyName = "clvPrioridadQueja"
        Me.Clave.HeaderText = "Clave"
        Me.Clave.Name = "Clave"
        Me.Clave.ReadOnly = True
        '
        'Concepto
        '
        Me.Concepto.DataPropertyName = "Descripcion"
        Me.Concepto.HeaderText = "Concepto"
        Me.Concepto.Name = "Concepto"
        Me.Concepto.ReadOnly = True
        Me.Concepto.Width = 408
        '
        'lblConcepto
        '
        Me.lblConcepto.AutoSize = True
        Me.lblConcepto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblConcepto.Location = New System.Drawing.Point(14, 133)
        Me.lblConcepto.Name = "lblConcepto"
        Me.lblConcepto.Size = New System.Drawing.Size(75, 15)
        Me.lblConcepto.TabIndex = 6
        Me.lblConcepto.Text = "Concepto :"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Location = New System.Drawing.Point(12, 12)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnBuscarConcepto)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnBuscarClave)
        Me.SplitContainer1.Panel1.Controls.Add(Me.pnlDatos)
        Me.SplitContainer1.Panel1.Controls.Add(Me.txtConcepto)
        Me.SplitContainer1.Panel1.Controls.Add(Me.txtClave)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lblTitulo)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lblClave)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lblConcepto)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.dgvPrioridades)
        Me.SplitContainer1.Size = New System.Drawing.Size(836, 694)
        Me.SplitContainer1.SplitterDistance = 278
        Me.SplitContainer1.TabIndex = 23
        Me.SplitContainer1.TabStop = False
        '
        'lblTitulo
        '
        Me.lblTitulo.AutoSize = True
        Me.lblTitulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblTitulo.Location = New System.Drawing.Point(13, 12)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(214, 24)
        Me.lblTitulo.TabIndex = 1
        Me.lblTitulo.Text = "Buscar Prioridad Por :"
        '
        'lblClave
        '
        Me.lblClave.AutoSize = True
        Me.lblClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClave.Location = New System.Drawing.Point(14, 54)
        Me.lblClave.Name = "lblClave"
        Me.lblClave.Size = New System.Drawing.Size(50, 15)
        Me.lblClave.TabIndex = 4
        Me.lblClave.Text = "Clave :"
        '
        'BrwPrioridadQueja
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1028, 753)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.btnConsulta)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Name = "BrwPrioridadQueja"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Prioridades de Quejas"
        Me.pnlDatos.ResumeLayout(False)
        Me.pnlDatos.PerformLayout()
        CType(Me.dgvPrioridades, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents txtConcepto As System.Windows.Forms.TextBox
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents txtClave As System.Windows.Forms.TextBox
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents btnConsulta As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents btnBuscarConcepto As System.Windows.Forms.Button
    Friend WithEvents btnBuscarClave As System.Windows.Forms.Button
    Friend WithEvents pnlDatos As System.Windows.Forms.Panel
    Friend WithEvents lblInfConcepto As System.Windows.Forms.TextBox
    Friend WithEvents lblDatos As System.Windows.Forms.Label
    Friend WithEvents lblInfClave As System.Windows.Forms.Label
    Friend WithEvents dgvPrioridades As System.Windows.Forms.DataGridView
    Friend WithEvents lblConcepto As System.Windows.Forms.Label
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents lblTitulo As System.Windows.Forms.Label
    Friend WithEvents lblClave As System.Windows.Forms.Label
    Friend WithEvents lblDatosConcepto As System.Windows.Forms.Label
    Friend WithEvents lblDatosClave As System.Windows.Forms.Label
    Friend WithEvents clvPrioridadQueja As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clave As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Concepto As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
