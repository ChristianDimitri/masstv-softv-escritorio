﻿Imports System.Data.SqlClient
Public Class FrmSucursales
    'VARIABLES COMPAÑÍAS -INICIO- <JUANJO>
    Dim bndSerieCompania,
        folioUsadoExiste As Integer
    Dim bndFolioCompania,
        bndGuarda As Boolean
    'VARIABLES COMPAÑÍAS -FIN- <JUANJO>

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub CONSUCURSALESBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONSUCURSALESBindingNavigatorSaveItem.Click
        Dim locerror As Integer
        Try
            'VALIDACIONES PARA GUARDAR UNA SUCURSAL -INICIO- <JUANJO>
            If opcion = "M" Then
                If Me.dgvCompanias.Rows.Count = 0 Then
                    MsgBox("¡Debe tener dada de alta al menos una Compañía para esta Sucursal!" & vbNewLine & "(Datos Facturas y Tickets)", MsgBoxStyle.Information)
                    Exit Sub
                ElseIf Me.txtCalle.Text.Length = 0 Then
                    MsgBox("¡Capture la Calle de la Sucursal!" & vbNewLine & " (Dirección Sucursal)", MsgBoxStyle.Information)
                    Exit Sub
                ElseIf Me.txtNumero.Text.Length = 0 Then
                    MsgBox("¡Capture el Número de la Sucursal!" & vbNewLine & " (Dirección Sucursal)", MsgBoxStyle.Information)
                    Exit Sub
                ElseIf Me.txtColonia.Text.Length = 0 Then
                    MsgBox("¡Capture la Colonia de la Sucursal!" & vbNewLine & " (Dirección Sucursal)", MsgBoxStyle.Information)
                    Exit Sub
                ElseIf Me.txtCP.Text.Length = 0 Then
                    MsgBox("¡Capture el CP de la Sucursal!" & vbNewLine & " (Dirección Sucursal)", MsgBoxStyle.Information)
                    Exit Sub
                ElseIf Me.txtMunicipio.Text.Length = 0 Then
                    MsgBox("¡Capture el Municipio de la Sucursal!" & vbNewLine & " (Dirección Sucursal)", MsgBoxStyle.Information)
                    Exit Sub
                ElseIf Me.txtCiudad.Text.Length = 0 Then
                    MsgBox("¡Capture la Ciudad de la Sucursal!" & vbNewLine & " (Dirección Sucursal)", MsgBoxStyle.Information)
                    Exit Sub
                Else
                    insertaDatosGeneralesSucursal(gloClave, Me.txtCalle.Text, Me.txtNumero.Text, Me.txtColonia.Text, CInt(Me.txtCP.Text), Me.txtMunicipio.Text, _
                                                  Me.txtCiudad.Text, Me.txtTelefono.Text)
                End If
            End If
            'VALIDACIONES PARA GUARDAR UNA SUCURSAL -FIN- <JUANJO>

            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If IsNumeric(Me.No_folioTextBox.Text) = False Then Me.No_folioTextBox.Text = 0
            Me.Inserta_Generales_FacturaGlobalTableAdapter.Connection = CON
            Me.Inserta_Generales_FacturaGlobalTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Generales_FacturaGlobal, CInt(Me.Clv_SucursalTextBox.Text), Me.SerieTextBox1.Text, CInt(Me.No_folioTextBox.Text), locerror)
            If locerror = 0 Then
                If IsNumeric(Me.UltimoFolioUsadoTextBox.Text) = False Then Me.UltimoFolioUsadoTextBox.Text = 0
                Me.Validate()
                Me.CONSUCURSALESBindingSource.EndEdit()
                Me.CONSUCURSALESTableAdapter.Connection = CON
                Me.CONSUCURSALESTableAdapter.Update(Me.NewSofTvDataSet.CONSUCURSALES)
                Me.Inserta_impresora_sucursalTableAdapter1.Connection = CON
                Me.Inserta_impresora_sucursalTableAdapter1.Fill(Me.ProcedimientosArnoldo2.inserta_impresora_sucursal, CInt(Me.Clv_SucursalTextBox.Text), Me.Impresora_ContratosTextBox.Text, Me.Impresora_TarjetasTextBox.Text, Me.TextBox1.Text)
                Me.Inserta_Generales_FacturaGlobalTableAdapter.Connection = CON
                Me.Inserta_Generales_FacturaGlobalTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Generales_FacturaGlobal, CInt(Me.Clv_SucursalTextBox.Text), Me.SerieTextBox1.Text, CInt(Me.No_folioTextBox.Text), locerror)
                bitsist(GloUsuario, 0, LocGloSistema, "Catálogo de Sucursales ", "", "Creación Sucursal", "Clave de la Sucursal: " + Me.Clv_SucursalTextBox.Text, LocClv_Ciudad)
                MsgBox(mensaje5)
                GloBnd = True
                CON.Close()
                'Me.Close()
            Else
                MsgBox("La Serie de Factura Global ya Existe no se puede Guardar", MsgBoxStyle.Information)
            End If

            'HABILITA Y DESHABILITA LOS PANELES SEGÚN SEA EL CASO A REALIZAR -INICIO- <JUANJO>
            If opcion = "N" Then
                opcion = "M"
                Me.pnlDatosFiscales.Enabled = True
                Me.pnlDatosGenerales.Enabled = True
            End If
            'HABILITA Y DESHABILITA LOS PANELES SEGÚN SEA EL CASO A REALIZAR -INICIO- <JUANJO>
        Catch ex As System.Exception
            'MsgBox("La Serie ya Existe no se Puede Guardar")
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub BUSCA(ByVal CLAVE As Integer)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.CONSUCURSALESTableAdapter.Connection = CON
            Me.CONSUCURSALESTableAdapter.Fill(Me.NewSofTvDataSet.CONSUCURSALES, New System.Nullable(Of Integer)(CType(CLAVE, Integer)))
            Me.Consulta_Impresora_SucursalTableAdapter1.Connection = CON
            Me.Consulta_Impresora_SucursalTableAdapter1.Fill(Me.ProcedimientosArnoldo2.Consulta_Impresora_Sucursal, CLAVE)
            Me.Consulta_Generales_FacturasGlobalesTableAdapter.Connection = CON
            Me.Consulta_Generales_FacturasGlobalesTableAdapter.Fill(Me.DataSetarnoldo.Consulta_Generales_FacturasGlobales, CLAVE)
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub FrmSucursales_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If opcion = "N" Then
            Me.CONSUCURSALESBindingSource.AddNew()
            Panel1.Enabled = True
            Me.pnlDatosFiscales.Enabled = False
            Me.pnlDatosGenerales.Enabled = False
        ElseIf opcion = "C" Then
            Panel1.Enabled = False
            BUSCA(gloClave)
            consultaDatosFiscalesSucursal(gloClave)
            Me.pnlDatosFiscales.Enabled = False
            Me.pnlDatosGenerales.Enabled = False
        ElseIf opcion = "M" Then
            Panel1.Enabled = True
            BUSCA(gloClave)
            consultaDatosFiscalesSucursal(gloClave)
            Me.pnlDatosFiscales.Enabled = True
            Me.pnlDatosGenerales.Enabled = True
        End If
        llenaComboCompanias()
    End Sub

    Private Sub Clv_SucursalTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_SucursalTextBox.TextChanged
        gloClave = Me.Clv_SucursalTextBox.Text
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.CONSUCURSALESTableAdapter.Connection = CON
        Me.CONSUCURSALESTableAdapter.Delete(gloClave)
        Me.Borra_Impresora_SucursalesTableAdapter.Connection = CON
        Me.Borra_Impresora_SucursalesTableAdapter.Fill(Me.DataSetarnoldo.Borra_Impresora_Sucursales, gloClave)
        Me.Borra_Generales_FacturasGlobalesTableAdapter.Connection = CON
        Me.Borra_Generales_FacturasGlobalesTableAdapter.Fill(Me.DataSetarnoldo.Borra_Generales_FacturasGlobales, gloClave)
        bitsist(GloUsuario, 0, LocGloSistema, "Catálogo de Sucursales ", "", "Eliminó Sucursal", "Clave de la Sucursal: " + Me.Clv_SucursalTextBox.Text, LocClv_Ciudad)
        CON.Close()
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub NombreTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NombreTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(NombreTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

#Region "Controles Nueva Parte Sucursales"
    Private Sub tabDatosSucursal_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tabDatosSucursal.SelectedIndexChanged
        If tabDatosSucursal.SelectedTab.Name = "tpgDatos" Then 'PARA LLENAR LOS COMBOS CUANDO SE SELECCIONE EL TAB DE DATOS FISCALES
            consultaDatosGeneralesSucursal(gloClave)
        ElseIf tabDatosSucursal.SelectedTab.Name = "tpgFiscal" Then 'PARA LLENAR LOS DATOS DE DIRECCIÓN CUANDO SE SELECCIONE EL TAB DATOS GENERALES
            llenaComboCompanias()
            consultaDatosFiscalesSucursal(gloClave)
        End If
    End Sub

    Private Sub cmbCompanias_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCompanias.SelectedIndexChanged
        Try 'PARA LLENAR EL COMBO DE LAS SERIES DEPENDIENDO LA COMPAÑÍA SELECCIONADA
            If IsNumeric(Me.cmbCompanias.SelectedValue) = True Then
                llenaCombosSeries(Me.cmbCompanias.SelectedValue)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtCP_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If InStr(1, "0123456789,-" & Chr(8), e.KeyChar) = 0 Then 'VALIDACIÓN PARA QUE SÓLO ADMITA VALORES NUMÉRICOS EN EL TEXTBOX (txtCP)
            e.KeyChar = ""
        End If
    End Sub

    Private Sub txtUltFolioUsado_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If InStr(1, "0123456789,-" & Chr(8), e.KeyChar) = 0 Then 'VALIDACIÓN PARA QUE SÓLO ADMITA VALORES NUMÉRICOS EN EL TEXTBOX (txtUltFolioUsado)
            e.KeyChar = ""
        End If
    End Sub

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        'VALIDACIONES PREVIAS A LA ADICIÓN DE UNA COMPAÑÍA A UNA SUCURSAL -INICIO- <JUANJO>
        If IsNumeric(Me.cmbCompanias.SelectedValue) = False Then
            MsgBox("¡Debe Seleccionar al menos una Compañía!", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.cmbCompanias.SelectedValue = 0 Then
            MsgBox("¡Debe Seleccionar al menos una Compañía!", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.txtSerieTicket.Text.Length = 0 Then
            MsgBox("¡Capture la Serie a la que pertenece la Compañía seleccionada!", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.txtUltFolioUsado.Text.Length = 0 Then
            MsgBox("¡Capture el último folio usado para la Compañía seleccionada!", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.cmbFacturaFiscal.SelectedValue.ToString = "0" Then
            MsgBox("¡Seleccione la serie para la Factura Fiscal!", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.cmbFacturaGlobal.SelectedValue.ToString = "0" Then
            MsgBox("¡Seleccione la serie para la Factura Global!", MsgBoxStyle.Information)
            Exit Sub
        End If
        'VALIDACIONES PREVIAS A LA ADICIÓN DE UNA COMPAÑÍA A UNA SUCURSAL -FIN- <JUANJO>

        'REALIZA LA INSERCIÓN A LA TABLA -INICIO- <JUANJO>
        insertaDatosFiscalesSucursal(gloClave, CInt(Me.cmbCompanias.SelectedValue), Me.txtSerieTicket.Text, CInt(Me.txtUltFolioUsado.Text), _
                                     Me.cmbFacturaFiscal.SelectedValue, Me.cmbFacturaGlobal.SelectedValue)
        'REALIZA LA INSERCIÓN A LA TABLA -FIN- <JUANJO>

        If bndGuarda = True Then 'VARIABLE EN TRUE CUANDO LA COMPAÑÍA A DAR DE ALTA YA SE ENCUENTRA EN LA LISTA
            MsgBox("¡La Compañía ya se encuentra en la lista!", MsgBoxStyle.Information)
            Exit Sub
        ElseIf bndSerieCompania > 0 Then 'VARIABLE MAYOR A CERO CUANDO LA SERIE YA EXISTE EN ALGUNA SUCURSAL
            MsgBox("¡La serie ya ha sido dada de alta en otra Compañía!", MsgBoxStyle.Information)
            Exit Sub
        ElseIf bndFolioCompania = True Then 'CUANDO LA SERIE NO EXISTE EN OTRA SUCURSAL PERO YA HAY REGISTROS DE ELLA, REGRESA EL FOLIO ULTIMO EXISTENTE
            MsgBox("¡El folio debe ser mayor a " & CStr(folioUsadoExiste) & " para esta serie!", MsgBoxStyle.Information)
            Exit Sub
        End If

        consultaDatosFiscalesSucursal(gloClave) 'RECARGA EL GRID VIEW DE LAS COMPAÑÍAS DADAS DE ALTA (dgvCompanias)
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        If Me.dgvCompanias.Rows.Count > 0 Then
            If CInt(Me.dgvCompanias.SelectedCells(0).Value) > 0 Then
                Dim res = MsgBox("¿Está seguro de eliminar este registro?" & vbNewLine & "Este movimiento afectará el Proceso de Cobro de Facturas y Tickets de esta Sucursal", MsgBoxStyle.YesNo)
                If res = MsgBoxResult.Yes Then
                    eliminaDatosFiscalesSucursal(CInt(Me.dgvCompanias.SelectedCells(0).Value))
                    consultaDatosFiscalesSucursal(gloClave)
                Else
                    Exit Sub
                End If
            Else
                MsgBox("¡Seleccione un registro para eliminar!", MsgBoxStyle.Information)
                Exit Sub
            End If
        Else
            MsgBox("¡No hay registros para Eliminar!", MsgBoxStyle.Information)
            Exit Sub
        End If
    End Sub
#End Region

#Region "Métodos Nueva Parte Sucursales"
    Private Sub llenaCombosSeries(ByVal prmClvCompaniaSoftv As Integer) 'PARA LLENAR LOS COMBOS cmbFacturaFiscal Y cmbFacturaGlobal
        Dim sucursales As New classSucursales
        sucursales.clvCompaniaSoftv = prmClvCompaniaSoftv

        Me.cmbFacturaFiscal.DataSource = sucursales.uspMuestraSerieFolioMizar
        Me.cmbFacturaGlobal.DataSource = sucursales.uspMuestraSerieFolioMizar
    End Sub

    Private Sub llenaComboCompanias() 'PARA LLENAR EL COMBO DE LAS COMPAÑÍAS
        Dim sucursales As New classSucursales

        Me.cmbCompanias.DataSource = sucursales.uspConsultaCompaniasActivas
    End Sub

    Private Sub insertaDatosGeneralesSucursal(ByVal prmClvSucursal As Integer, ByVal prmCalle As String, ByVal prmNumero As String, ByVal prmColonia As String, ByVal prmCp As Integer, _
                                              ByVal prmMunicipio As String, ByVal prmCiudad As String, ByVal prmTelefono As String)
        Dim sucursales As New classSucursales
        sucursales.clvSucursal = prmClvSucursal
        sucursales.calle = prmCalle
        sucursales.numero = prmNumero
        sucursales.colonia = prmColonia
        sucursales.cp = prmCp
        sucursales.municipio = prmMunicipio
        sucursales.ciudad = prmCiudad
        sucursales.telefono = prmTelefono

        sucursales.uspInsertaTblRelSucursalDatosGenerales()
    End Sub

    Private Sub consultaDatosGeneralesSucursal(ByVal prmClvSucursal As Integer)
        Dim sucursales As New classSucursales
        sucursales.clvSucursal = prmClvSucursal

        Dim dtDatosGenerales As New DataTable
        dtDatosGenerales = sucursales.uspConsultaTblRelSucursalDatosGenerales

        If dtDatosGenerales.Rows.Count > 0 Then
            Me.txtCalle.Text = dtDatosGenerales.Rows(0)("calle").ToString
            Me.txtNumero.Text = dtDatosGenerales.Rows(0)("numero").ToString
            Me.txtColonia.Text = dtDatosGenerales.Rows(0)("colonia").ToString
            Me.txtCP.Text = CInt(dtDatosGenerales.Rows(0)("cp").ToString)
            Me.txtMunicipio.Text = dtDatosGenerales.Rows(0)("municipio").ToString
            Me.txtCiudad.Text = dtDatosGenerales.Rows(0)("ciudad").ToString
            Me.txtTelefono.Text = dtDatosGenerales.Rows(0)("telefono").ToString
        End If
    End Sub

    Private Sub insertaDatosFiscalesSucursal(ByVal prmClvSucursal As Integer, ByVal prmClvCompania As Integer, ByVal prmSerieTicket As String, ByVal prmUltFolio As Integer, _
                                             ByVal prmClvSerieFacFiscal As String, ByVal prmClvSerieFacGlobal As String)
        Dim sucursales As New classSucursales
        sucursales.clvSucursal = prmClvSucursal
        sucursales.clvCompaniaSoftv = prmClvCompania
        sucursales.serieTicket = prmSerieTicket
        sucursales.ultFolioUsado = prmUltFolio
        sucursales.clvSerieFacFiscal = prmClvSerieFacFiscal
        sucursales.clvSerieFacGlobal = prmClvSerieFacGlobal

        Dim dtDatosFiscales As New DataTable
        dtDatosFiscales = sucursales.uspInsertatblRelSucursalDatosFiscales

        bndSerieCompania = CInt(dtDatosFiscales.Rows(0)("bndSerie").ToString)
        bndFolioCompania = CBool(dtDatosFiscales.Rows(0)("bndFolio").ToString)
        folioUsadoExiste = CInt(dtDatosFiscales.Rows(0)("folioUsadoExiste").ToString)
        bndGuarda = CBool(dtDatosFiscales.Rows(0)("bndGuarda").ToString)
    End Sub

    Private Sub consultaDatosFiscalesSucursal(ByVal prmClvSucursal As Integer)
        Dim sucursales As New classSucursales
        sucursales.clvSucursal = prmClvSucursal

        Me.dgvCompanias.DataSource = sucursales.uspConsultatblRelSucursalDatosFiscales
    End Sub

    Private Sub eliminaDatosFiscalesSucursal(ByVal prmIdRelacion As Integer)
        Dim sucursales As New classSucursales
        sucursales.idRelacion = prmIdRelacion
        sucursales.uspEliminatblRelSucursalDatosFiscales()
    End Sub
#End Region
End Class