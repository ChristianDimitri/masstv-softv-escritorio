<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmGenerales_Sistema
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim PortLabel As System.Windows.Forms.Label
        Dim HostLabel As System.Windows.Forms.Label
        Dim PasswordLabel As System.Windows.Forms.Label
        Dim CuentaLabel As System.Windows.Forms.Label
        Dim MesesLabel As System.Windows.Forms.Label
        Dim ConceptoLabel As System.Windows.Forms.Label
        Dim Label20 As System.Windows.Forms.Label
        Dim Label19 As System.Windows.Forms.Label
        Dim Label22 As System.Windows.Forms.Label
        Dim Label16 As System.Windows.Forms.Label
        Dim Label21 As System.Windows.Forms.Label
        Dim Label15 As System.Windows.Forms.Label
        Dim Label12 As System.Windows.Forms.Label
        Dim ImportePorExtrasLabel As System.Windows.Forms.Label
        Dim CargaDeTrabajoLabel As System.Windows.Forms.Label
        Dim Carga_trab_insLabel As System.Windows.Forms.Label
        Dim NumeroExtLabel As System.Windows.Forms.Label
        Dim CostoExtLabel As System.Windows.Forms.Label
        Dim ImpresoraOrdenesLabel As System.Windows.Forms.Label
        Dim Puntos1Label As System.Windows.Forms.Label
        Dim Label24 As System.Windows.Forms.Label
        Dim Label25 As System.Windows.Forms.Label
        Dim Label28 As System.Windows.Forms.Label
        Dim Label27 As System.Windows.Forms.Label
        Dim Label26 As System.Windows.Forms.Label
        Dim Label31 As System.Windows.Forms.Label
        Dim Label30 As System.Windows.Forms.Label
        Dim Label29 As System.Windows.Forms.Label
        Dim Label34 As System.Windows.Forms.Label
        Dim Label33 As System.Windows.Forms.Label
        Dim Label32 As System.Windows.Forms.Label
        Dim Si_se_generaLabel As System.Windows.Forms.Label
        Dim MensajeLabel As System.Windows.Forms.Label
        Dim ActivoLabel As System.Windows.Forms.Label
        Dim Label38 As System.Windows.Forms.Label
        Dim Label41 As System.Windows.Forms.Label
        Dim Label39 As System.Windows.Forms.Label
        Dim CablemodemsLabel As System.Windows.Forms.Label
        Dim PregcablLabel As System.Windows.Forms.Label
        Dim DecodersLabel As System.Windows.Forms.Label
        Dim PregdecoLabel As System.Windows.Forms.Label
        Dim TELefonosLabel As System.Windows.Forms.Label
        Dim RfcLabel As System.Windows.Forms.Label
        Dim CiudadLabel As System.Windows.Forms.Label
        Dim ColoniaLabel As System.Windows.Forms.Label
        Dim DireccionLabel As System.Windows.Forms.Label
        Dim NombreLabel As System.Windows.Forms.Label
        Dim Id_sucursalLabel As System.Windows.Forms.Label
        Dim Fecha_InicioLabel As System.Windows.Forms.Label
        Dim Numero_InicioLabel As System.Windows.Forms.Label
        Dim FormatoLabel As System.Windows.Forms.Label
        Dim RutaLabel As System.Windows.Forms.Label
        Dim Numero_LocalLabel As System.Windows.Forms.Label
        Dim Pais_LocalLabel As System.Windows.Forms.Label
        Dim Clave044Label As System.Windows.Forms.Label
        Dim Clave045Label As System.Windows.Forms.Label
        Dim Clave01800Label As System.Windows.Forms.Label
        Dim Clave01900Label As System.Windows.Forms.Label
        Dim Label46 As System.Windows.Forms.Label
        Dim Nom_ArchivoLabel As System.Windows.Forms.Label
        Dim Fecha_FacturasLabel As System.Windows.Forms.Label
        Dim Periodo_inicialLabel As System.Windows.Forms.Label
        Dim ModuloLabel As System.Windows.Forms.Label
        Dim ConstanteLabel As System.Windows.Forms.Label
        Dim ReferenciaLabel As System.Windows.Forms.Label
        Dim Label48 As System.Windows.Forms.Label
        Dim Label50 As System.Windows.Forms.Label
        Dim Label52 As System.Windows.Forms.Label
        Dim Label53 As System.Windows.Forms.Label
        Dim LblCdClvAdic As System.Windows.Forms.Label
        Dim LblEdoClvAdic As System.Windows.Forms.Label
        Dim Label69 As System.Windows.Forms.Label
        Dim Label84 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim Label95 As System.Windows.Forms.Label
        Dim Label96 As System.Windows.Forms.Label
        Dim Label97 As System.Windows.Forms.Label
        Dim Label98 As System.Windows.Forms.Label
        Me.ConsultaCpGeneBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetarnoldo = New sofTV.DataSetarnoldo()
        Me.Consulta_ImpresorasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraTipServEricBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.ConMesesCobroAdeudoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConGeneralCorreoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Consulta_ImpresorasTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Consulta_ImpresorasTableAdapter()
        Me.Inserta_Mod_Cp_GenBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Mod_Cp_GenTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Inserta_Mod_Cp_GenTableAdapter()
        Me.Consulta_Cp_GeneTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Consulta_Cp_GeneTableAdapter()
        Me.ConMesesCobroAdeudoTableAdapter = New sofTV.DataSetEricTableAdapters.ConMesesCobroAdeudoTableAdapter()
        Me.MuestraTipServEricTableAdapter = New sofTV.DataSetEricTableAdapters.MuestraTipServEricTableAdapter()
        Me.ConGeneralCorreoTableAdapter = New sofTV.DataSetEricTableAdapters.ConGeneralCorreoTableAdapter()
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2()
        Me.Consulta_Generales_Filtros_OrdenesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Consulta_Generales_Filtros_OrdenesTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Consulta_Generales_Filtros_OrdenesTableAdapter()
        Me.Inserta_Generales_Filtros_OrdenesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Generales_Filtros_OrdenesTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_Generales_Filtros_OrdenesTableAdapter()
        Me.COnsultaGralfacturaglobalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.COnsulta_Gral_factura_globalTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.COnsulta_Gral_factura_globalTableAdapter()
        Me.Inserta_Gral_factura_globalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Gral_factura_globalTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_Gral_factura_globalTableAdapter()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.CuentaTextBox = New System.Windows.Forms.TextBox()
        Me.PasswordTextBox = New System.Windows.Forms.TextBox()
        Me.HostTextBox = New System.Windows.Forms.TextBox()
        Me.PortTextBox = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Clv_TipSerTextBox = New System.Windows.Forms.TextBox()
        Me.ConceptoComboBox = New System.Windows.Forms.ComboBox()
        Me.MesesTextBox = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.MUESTRAPERIODOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Procedimientosarnoldo4 = New sofTV.Procedimientosarnoldo4()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.TextBox10 = New System.Windows.Forms.TextBox()
        Me.CONSULTAGENERALESDESCBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TextBox15 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TextBox11 = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TextBox14 = New System.Windows.Forms.TextBox()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.TextBox16 = New System.Windows.Forms.TextBox()
        Me.TextBox13 = New System.Windows.Forms.TextBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.ImpresoraFiscalTextBox = New System.Windows.Forms.TextBox()
        Me.ImpresoraTarjetasTextBox = New System.Windows.Forms.TextBox()
        Me.ImpresoraTicketsTextBox = New System.Windows.Forms.TextBox()
        Me.ImpresoraContratosTextBox = New System.Windows.Forms.TextBox()
        Me.PanelBasico = New System.Windows.Forms.Panel()
        Me.bpxImpresiones = New System.Windows.Forms.GroupBox()
        Me.cbxImprimeOrdenesFac = New System.Windows.Forms.CheckBox()
        Me.lblAvisoImpresiones = New System.Windows.Forms.Label()
        Me.cbxImprimeOrdenesSoftv = New System.Windows.Forms.CheckBox()
        Me.cbxImprimeQuejasSoftv = New System.Windows.Forms.CheckBox()
        Me.lblEncabezadoImpresion = New System.Windows.Forms.Label()
        Me.CheckBox3 = New System.Windows.Forms.CheckBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.NumericUpDown2 = New System.Windows.Forms.NumericUpDown()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown4 = New System.Windows.Forms.NumericUpDown()
        Me.ImportePorExtrasTextBox = New System.Windows.Forms.TextBox()
        Me.ConGeneralBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label4 = New System.Windows.Forms.Label()
        Me.NumericUpDown7 = New System.Windows.Forms.NumericUpDown()
        Me.Puntos10TextBox = New System.Windows.Forms.TextBox()
        Me.ConGeneralAntiguedadBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NumericUpDown3 = New System.Windows.Forms.NumericUpDown()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Carga_trab_insTextBox = New System.Windows.Forms.TextBox()
        Me.CargaDeTrabajoTextBox = New System.Windows.Forms.TextBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.NumericUpDown8 = New System.Windows.Forms.NumericUpDown()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.CostoExtTextBox = New System.Windows.Forms.TextBox()
        Me.NumeroExtTextBox = New System.Windows.Forms.TextBox()
        Me.Puntos5TextBox = New System.Windows.Forms.TextBox()
        Me.ImpresoraOrdenesTextBox = New System.Windows.Forms.TextBox()
        Me.Puntos2TextBox = New System.Windows.Forms.TextBox()
        Me.NumericUpDown6 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown5 = New System.Windows.Forms.NumericUpDown()
        Me.Puntos1TextBox = New System.Windows.Forms.TextBox()
        Me.Consulta_cobrodepositoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.Si_se_generaCheckBox = New System.Windows.Forms.CheckBox()
        Me.Button15 = New System.Windows.Forms.Button()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.ConGeneralAlertaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLidia2 = New sofTV.DataSetLidia2()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.ReferenciaTextBox1 = New System.Windows.Forms.TextBox()
        Me.CONSULTA_General_HsbcBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetyahve = New sofTV.DataSetyahve()
        Me.ModuloTextBox1 = New System.Windows.Forms.TextBox()
        Me.Button31 = New System.Windows.Forms.Button()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.ReferenciaTextBox = New System.Windows.Forms.TextBox()
        Me.CONSULTA_General_SantanderBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConstanteTextBox = New System.Windows.Forms.TextBox()
        Me.ModuloTextBox = New System.Windows.Forms.TextBox()
        Me.Button30 = New System.Windows.Forms.Button()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.PanelDatosXml = New System.Windows.Forms.Panel()
        Me.TxtCdClvAdic = New System.Windows.Forms.TextBox()
        Me.TxtEdoClvAdic = New System.Windows.Forms.TextBox()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.TextBoxfolio = New System.Windows.Forms.TextBox()
        Me.TextBoxserie = New System.Windows.Forms.TextBox()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Nom_ArchivoTextBox = New System.Windows.Forms.TextBox()
        Me.CONSULTA_General_XmlBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Fecha_InicioDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.Fecha_FacturasDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.Periodo_inicialTextBox = New System.Windows.Forms.TextBox()
        Me.Periodo_finalTextBox = New System.Windows.Forms.TextBox()
        Me.Numero_InicioTextBox = New System.Windows.Forms.TextBox()
        Me.FormatoTextBox = New System.Windows.Forms.TextBox()
        Me.RutaTextBox = New System.Windows.Forms.TextBox()
        Me.Numero_LocalTextBox = New System.Windows.Forms.TextBox()
        Me.Pais_LocalTextBox = New System.Windows.Forms.TextBox()
        Me.Clave044TextBox = New System.Windows.Forms.TextBox()
        Me.Clave045TextBox = New System.Windows.Forms.TextBox()
        Me.Clave01800TextBox = New System.Windows.Forms.TextBox()
        Me.Clave01900TextBox = New System.Windows.Forms.TextBox()
        Me.Button28 = New System.Windows.Forms.Button()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.PanelDatos = New System.Windows.Forms.Panel()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.TextBoxNumeroExt = New System.Windows.Forms.TextBox()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.TextBoxNumeroInt = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.CiudadTextBox = New System.Windows.Forms.TextBox()
        Me.ColoniaTextBox = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Id_sucursalTextBox = New System.Windows.Forms.TextBox()
        Me.RfcTextBox = New System.Windows.Forms.TextBox()
        Me.DireccionTextBox = New System.Windows.Forms.TextBox()
        Me.NombreTextBox = New System.Windows.Forms.TextBox()
        Me.TELefonosTextBox = New System.Windows.Forms.TextBox()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.Button21 = New System.Windows.Forms.Button()
        Me.Button20 = New System.Windows.Forms.Button()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.CheckBox4 = New System.Windows.Forms.CheckBox()
        Me.Button19 = New System.Windows.Forms.Button()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.MensajeTextBox = New System.Windows.Forms.TextBox()
        Me.ConGeneralMsjTicketsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric2 = New sofTV.DataSetEric2()
        Me.ActivoCheckBox = New System.Windows.Forms.CheckBox()
        Me.Button16 = New System.Windows.Forms.Button()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.CablemodemsCheckBox = New System.Windows.Forms.CheckBox()
        Me.Consulta_cobrodepositobuenoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PregcablTextBox = New System.Windows.Forms.TextBox()
        Me.DecodersCheckBox = New System.Windows.Forms.CheckBox()
        Me.PregdecoTextBox = New System.Windows.Forms.TextBox()
        Me.Button23 = New System.Windows.Forms.Button()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Button26 = New System.Windows.Forms.Button()
        Me.Button25 = New System.Windows.Forms.Button()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.NumericUpDown9 = New System.Windows.Forms.NumericUpDown()
        Me.Consulta_Rel_PaquetesDigTelDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Consulta_Rel_PaquetesDigTelBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConsecutivoTextBox = New System.Windows.Forms.TextBox()
        Me.ConGeneralMsjTicketsTableAdapter = New sofTV.DataSetEric2TableAdapters.ConGeneralMsjTicketsTableAdapter()
        Me.NueGeneralMsjTicketsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NueGeneralMsjTicketsTableAdapter = New sofTV.DataSetEric2TableAdapters.NueGeneralMsjTicketsTableAdapter()
        Me.ConGeneralAlertaTableAdapter = New sofTV.DataSetLidia2TableAdapters.ConGeneralAlertaTableAdapter()
        Me.NueGeneralAlertaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NueGeneralAlertaTableAdapter = New sofTV.DataSetLidia2TableAdapters.NueGeneralAlertaTableAdapter()
        Me.Dame_Direcciones_IpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_Direcciones_IpTableAdapter = New sofTV.DataSetLidia2TableAdapters.Dame_Direcciones_IpTableAdapter()
        Me.Consulta_cobrodepositoTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Consulta_cobrodepositoTableAdapter()
        Me.Consulta_cobrodepositobuenoTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Consulta_cobrodepositobuenoTableAdapter()
        Me.Consulta_Rel_PaquetesDigTelTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Consulta_Rel_PaquetesDigTelTableAdapter()
        Me.CONSULTA_General_XmlTableAdapter = New sofTV.DataSetyahveTableAdapters.CONSULTA_General_XmlTableAdapter()
        Me.CONSULTA_General_SantanderTableAdapter = New sofTV.DataSetyahveTableAdapters.CONSULTA_General_SantanderTableAdapter()
        Me.CONSULTA_General_HsbcTableAdapter = New sofTV.DataSetyahveTableAdapters.CONSULTA_General_HsbcTableAdapter()
        Me.Muestra_ciudadTableAdapter1 = New sofTV.Procedimientosarnoldo4TableAdapters.Muestra_ciudadTableAdapter()
        Me.TabPage14 = New System.Windows.Forms.TabPage()
        Me.TabPage13 = New System.Windows.Forms.TabPage()
        Me.TabPage12 = New System.Windows.Forms.TabPage()
        Me.TabPage11 = New System.Windows.Forms.TabPage()
        Me.TabPage10 = New System.Windows.Forms.TabPage()
        Me.TabPage9 = New System.Windows.Forms.TabPage()
        Me.TabPage8 = New System.Windows.Forms.TabPage()
        Me.TabPage7 = New System.Windows.Forms.TabPage()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.tbcAccesoriosInstalacion = New System.Windows.Forms.TabControl()
        Me.TabPage15 = New System.Windows.Forms.TabPage()
        Me.PnQuejas = New System.Windows.Forms.Panel()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.TreeViewOrdenes = New System.Windows.Forms.TreeView()
        Me.LblNotas = New System.Windows.Forms.Label()
        Me.LblFamilia = New System.Windows.Forms.Label()
        Me.LblTecnico = New System.Windows.Forms.Label()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.TreViewTecnicos = New System.Windows.Forms.TreeView()
        Me.CmbTecnicos = New System.Windows.Forms.ComboBox()
        Me.MuestraTecnicosByFamiliBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CmBDepto = New System.Windows.Forms.ComboBox()
        Me.MuestratecnicosDepartamentosAlmacenBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TabPage16 = New System.Windows.Forms.TabPage()
        Me.LabelNota = New System.Windows.Forms.Label()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Clv_Grupo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Grupo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Porcentaje = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Activo = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.TabPage17 = New System.Windows.Forms.TabPage()
        Me.TextIEPS = New System.Windows.Forms.TextBox()
        Me.TextIva = New System.Windows.Forms.TextBox()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.LblPorIeps = New System.Windows.Forms.Label()
        Me.NumericUpDownIeps = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDownIva = New System.Windows.Forms.NumericUpDown()
        Me.Button14 = New System.Windows.Forms.Button()
        Me.ChkCalculo1 = New System.Windows.Forms.CheckBox()
        Me.LabelIEPS = New System.Windows.Forms.Label()
        Me.CtaIepsText = New System.Windows.Forms.TextBox()
        Me.LabelCtaIEPS = New System.Windows.Forms.Label()
        Me.CheckIEPS = New System.Windows.Forms.CheckBox()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.tbEstadoCuenta = New System.Windows.Forms.TabPage()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.tbConvenioBancomer = New System.Windows.Forms.TextBox()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.tbPrefijoOxxo = New System.Windows.Forms.TextBox()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.tbLetraInicialBancomer = New System.Windows.Forms.TextBox()
        Me.bnGenerarEstado = New System.Windows.Forms.Button()
        Me.bnEstadoCuentaGuardar = New System.Windows.Forms.Button()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.Label72 = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.tbDiaGeneracion = New System.Windows.Forms.TextBox()
        Me.tbDiaEnvio = New System.Windows.Forms.TextBox()
        Me.tbDiaFin = New System.Windows.Forms.TextBox()
        Me.tbDiaIni = New System.Windows.Forms.TextBox()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.cbPeriodo = New System.Windows.Forms.ComboBox()
        Me.tbpAccesoriosInstalacion = New System.Windows.Forms.TabPage()
        Me.Label79 = New System.Windows.Forms.Label()
        Me.cmbTipoDeCaja = New System.Windows.Forms.ComboBox()
        Me.Label81 = New System.Windows.Forms.Label()
        Me.cmbClasificacionArticulos = New System.Windows.Forms.ComboBox()
        Me.Label80 = New System.Windows.Forms.Label()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.dgvRelArticuloAccesorioInst = New System.Windows.Forms.DataGridView()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.idTipoCaja = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Accesorio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Articulo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmbAccesorioInst = New System.Windows.Forms.ComboBox()
        Me.Label78 = New System.Windows.Forms.Label()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.cmbDescArticulo = New System.Windows.Forms.ComboBox()
        Me.cmbClave = New System.Windows.Forms.ComboBox()
        Me.cmbClasifMaterial = New System.Windows.Forms.ComboBox()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.lblClave = New System.Windows.Forms.Label()
        Me.lblClasificacion = New System.Windows.Forms.Label()
        Me.tpCobroMaterial = New System.Windows.Forms.TabPage()
        Me.Panel14 = New System.Windows.Forms.Panel()
        Me.dgvRangosMaterial = New System.Windows.Forms.DataGridView()
        Me.identity = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RangoIni = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RangoFinal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NumPagos = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnEliminarRango = New System.Windows.Forms.Button()
        Me.btnAgregarRango = New System.Windows.Forms.Button()
        Me.lblRangoFin = New System.Windows.Forms.Label()
        Me.lblRangoIni = New System.Windows.Forms.Label()
        Me.txtRango2 = New System.Windows.Forms.TextBox()
        Me.txtRango1 = New System.Windows.Forms.TextBox()
        Me.nudPagos = New System.Windows.Forms.NumericUpDown()
        Me.lblPagos = New System.Windows.Forms.Label()
        Me.TextBox17 = New System.Windows.Forms.TextBox()
        Me.Mizar = New System.Windows.Forms.TabPage()
        Me.cbxMizarS = New System.Windows.Forms.CheckBox()
        Me.lblLeyenda = New System.Windows.Forms.Label()
        Me.cbxMizar = New System.Windows.Forms.CheckBox()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.lblTituloF = New System.Windows.Forms.Label()
        Me.lblUltFolioF = New System.Windows.Forms.Label()
        Me.txtUltFolioF = New System.Windows.Forms.TextBox()
        Me.lblSerieF = New System.Windows.Forms.Label()
        Me.txtSerieF = New System.Windows.Forms.TextBox()
        Me.lblTituloG = New System.Windows.Forms.Label()
        Me.lblUltFolioG = New System.Windows.Forms.Label()
        Me.txtUltFolioG = New System.Windows.Forms.TextBox()
        Me.lblSerieG = New System.Windows.Forms.Label()
        Me.txtSerieG = New System.Windows.Forms.TextBox()
        Me.cmbCompanias = New System.Windows.Forms.ComboBox()
        Me.lblCompania = New System.Windows.Forms.Label()
        Me.TabOxxo = New System.Windows.Forms.TabPage()
        Me.txtPrefijo = New System.Windows.Forms.TextBox()
        Me.Label83 = New System.Windows.Forms.Label()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.txtComisionOxxo = New System.Windows.Forms.TextBox()
        Me.Label82 = New System.Windows.Forms.Label()
        Me.ConsultaRelTecnicosQuejasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConsultaRelQuejasTecFamiliaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ConGeneralTableAdapter1 = New sofTV.Procedimientosarnoldo4TableAdapters.ConGeneralTableAdapter()
        Me.ConGeneralAntiguedadTableAdapter1 = New sofTV.Procedimientosarnoldo4TableAdapters.ConGeneralAntiguedadTableAdapter()
        Me.MUESTRAPERIODOSTableAdapter1 = New sofTV.Procedimientosarnoldo4TableAdapters.MUESTRAPERIODOSTableAdapter()
        Me.CONSULTAGENERALESDESCTableAdapter1 = New sofTV.Procedimientosarnoldo4TableAdapters.CONSULTAGENERALESDESCTableAdapter()
        Me.MODIFCAGENERALESDESCBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.MODIFCAGENERALESDESCTableAdapter1 = New sofTV.DataSetEric2TableAdapters.MODIFCAGENERALESDESCTableAdapter()
        Me.NueGeneralBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.NueGeneralTableAdapter1 = New sofTV.DataSetEric2TableAdapters.NueGeneralTableAdapter()
        Me.NUEGeneralAntiguedadBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.NUEGeneralAntiguedadTableAdapter1 = New sofTV.DataSetEric2TableAdapters.NUEGeneralAntiguedadTableAdapter()
        Me.ConsultaRel_Quejas_Tec_FamiliaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConsultaRel_Quejas_Tec_FamiliaTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.ConsultaRel_Quejas_Tec_FamiliaTableAdapter()
        Me.ConsultaRel_Tecnicos_QuejasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConsultaRel_Tecnicos_QuejasTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.ConsultaRel_Tecnicos_QuejasTableAdapter()
        Me.Muestra_tecnicosDepartamentos_AlmacenTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Muestra_tecnicosDepartamentos_AlmacenTableAdapter()
        Me.Muestra_TecnicosByFamiliTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Muestra_TecnicosByFamiliTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.TabPageCorteSistema = New System.Windows.Forms.TabPage()
        Me.TextBoxMensajeCorte = New System.Windows.Forms.TextBox()
        Me.TextBoxMensajeAvisoCorte = New System.Windows.Forms.TextBox()
        Me.TextBoxDiasAvisoCorte = New System.Windows.Forms.TextBox()
        Me.CheckBoxCorteSATV = New System.Windows.Forms.CheckBox()
        Me.TextBoxFecCorteSatv = New System.Windows.Forms.MaskedTextBox()
        Me.Button22 = New System.Windows.Forms.Button()
        Me.Label99 = New System.Windows.Forms.Label()
        PortLabel = New System.Windows.Forms.Label()
        HostLabel = New System.Windows.Forms.Label()
        PasswordLabel = New System.Windows.Forms.Label()
        CuentaLabel = New System.Windows.Forms.Label()
        MesesLabel = New System.Windows.Forms.Label()
        ConceptoLabel = New System.Windows.Forms.Label()
        Label20 = New System.Windows.Forms.Label()
        Label19 = New System.Windows.Forms.Label()
        Label22 = New System.Windows.Forms.Label()
        Label16 = New System.Windows.Forms.Label()
        Label21 = New System.Windows.Forms.Label()
        Label15 = New System.Windows.Forms.Label()
        Label12 = New System.Windows.Forms.Label()
        ImportePorExtrasLabel = New System.Windows.Forms.Label()
        CargaDeTrabajoLabel = New System.Windows.Forms.Label()
        Carga_trab_insLabel = New System.Windows.Forms.Label()
        NumeroExtLabel = New System.Windows.Forms.Label()
        CostoExtLabel = New System.Windows.Forms.Label()
        ImpresoraOrdenesLabel = New System.Windows.Forms.Label()
        Puntos1Label = New System.Windows.Forms.Label()
        Label24 = New System.Windows.Forms.Label()
        Label25 = New System.Windows.Forms.Label()
        Label28 = New System.Windows.Forms.Label()
        Label27 = New System.Windows.Forms.Label()
        Label26 = New System.Windows.Forms.Label()
        Label31 = New System.Windows.Forms.Label()
        Label30 = New System.Windows.Forms.Label()
        Label29 = New System.Windows.Forms.Label()
        Label34 = New System.Windows.Forms.Label()
        Label33 = New System.Windows.Forms.Label()
        Label32 = New System.Windows.Forms.Label()
        Si_se_generaLabel = New System.Windows.Forms.Label()
        MensajeLabel = New System.Windows.Forms.Label()
        ActivoLabel = New System.Windows.Forms.Label()
        Label38 = New System.Windows.Forms.Label()
        Label41 = New System.Windows.Forms.Label()
        Label39 = New System.Windows.Forms.Label()
        CablemodemsLabel = New System.Windows.Forms.Label()
        PregcablLabel = New System.Windows.Forms.Label()
        DecodersLabel = New System.Windows.Forms.Label()
        PregdecoLabel = New System.Windows.Forms.Label()
        TELefonosLabel = New System.Windows.Forms.Label()
        RfcLabel = New System.Windows.Forms.Label()
        CiudadLabel = New System.Windows.Forms.Label()
        ColoniaLabel = New System.Windows.Forms.Label()
        DireccionLabel = New System.Windows.Forms.Label()
        NombreLabel = New System.Windows.Forms.Label()
        Id_sucursalLabel = New System.Windows.Forms.Label()
        Fecha_InicioLabel = New System.Windows.Forms.Label()
        Numero_InicioLabel = New System.Windows.Forms.Label()
        FormatoLabel = New System.Windows.Forms.Label()
        RutaLabel = New System.Windows.Forms.Label()
        Numero_LocalLabel = New System.Windows.Forms.Label()
        Pais_LocalLabel = New System.Windows.Forms.Label()
        Clave044Label = New System.Windows.Forms.Label()
        Clave045Label = New System.Windows.Forms.Label()
        Clave01800Label = New System.Windows.Forms.Label()
        Clave01900Label = New System.Windows.Forms.Label()
        Label46 = New System.Windows.Forms.Label()
        Nom_ArchivoLabel = New System.Windows.Forms.Label()
        Fecha_FacturasLabel = New System.Windows.Forms.Label()
        Periodo_inicialLabel = New System.Windows.Forms.Label()
        ModuloLabel = New System.Windows.Forms.Label()
        ConstanteLabel = New System.Windows.Forms.Label()
        ReferenciaLabel = New System.Windows.Forms.Label()
        Label48 = New System.Windows.Forms.Label()
        Label50 = New System.Windows.Forms.Label()
        Label52 = New System.Windows.Forms.Label()
        Label53 = New System.Windows.Forms.Label()
        LblCdClvAdic = New System.Windows.Forms.Label()
        LblEdoClvAdic = New System.Windows.Forms.Label()
        Label69 = New System.Windows.Forms.Label()
        Label84 = New System.Windows.Forms.Label()
        Label95 = New System.Windows.Forms.Label()
        Label96 = New System.Windows.Forms.Label()
        Label97 = New System.Windows.Forms.Label()
        Label98 = New System.Windows.Forms.Label()
        CType(Me.ConsultaCpGeneBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_ImpresorasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTipServEricBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConMesesCobroAdeudoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConGeneralCorreoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Mod_Cp_GenBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_Generales_Filtros_OrdenesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Generales_Filtros_OrdenesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.COnsultaGralfacturaglobalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Gral_factura_globalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.MUESTRAPERIODOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONSULTAGENERALESDESCBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.PanelBasico.SuspendLayout()
        Me.bpxImpresiones.SuspendLayout()
        CType(Me.NumericUpDown2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConGeneralBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConGeneralAntiguedadBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.NumericUpDown8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.NumericUpDown6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_cobrodepositoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel6.SuspendLayout()
        CType(Me.ConGeneralAlertaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel12.SuspendLayout()
        CType(Me.CONSULTA_General_HsbcBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetyahve, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel11.SuspendLayout()
        CType(Me.CONSULTA_General_SantanderBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelDatosXml.SuspendLayout()
        CType(Me.CONSULTA_General_XmlBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelDatos.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel7.SuspendLayout()
        CType(Me.ConGeneralMsjTicketsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel9.SuspendLayout()
        CType(Me.Consulta_cobrodepositobuenoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel10.SuspendLayout()
        CType(Me.NumericUpDown9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_Rel_PaquetesDigTelDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_Rel_PaquetesDigTelBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NueGeneralMsjTicketsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NueGeneralAlertaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_Direcciones_IpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage14.SuspendLayout()
        Me.TabPage13.SuspendLayout()
        Me.TabPage12.SuspendLayout()
        Me.TabPage11.SuspendLayout()
        Me.TabPage10.SuspendLayout()
        Me.TabPage9.SuspendLayout()
        Me.TabPage8.SuspendLayout()
        Me.TabPage7.SuspendLayout()
        Me.TabPage6.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.tbcAccesoriosInstalacion.SuspendLayout()
        Me.TabPage15.SuspendLayout()
        Me.PnQuejas.SuspendLayout()
        CType(Me.MuestraTecnicosByFamiliBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestratecnicosDepartamentosAlmacenBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage16.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage17.SuspendLayout()
        CType(Me.NumericUpDownIeps, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDownIva, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbEstadoCuenta.SuspendLayout()
        Me.tbpAccesoriosInstalacion.SuspendLayout()
        CType(Me.dgvRelArticuloAccesorioInst, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpCobroMaterial.SuspendLayout()
        Me.Panel14.SuspendLayout()
        CType(Me.dgvRangosMaterial, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudPagos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Mizar.SuspendLayout()
        Me.TabOxxo.SuspendLayout()
        CType(Me.ConsultaRelTecnicosQuejasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsultaRelQuejasTecFamiliaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MODIFCAGENERALESDESCBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NueGeneralBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NUEGeneralAntiguedadBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsultaRel_Quejas_Tec_FamiliaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsultaRel_Tecnicos_QuejasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPageCorteSistema.SuspendLayout()
        Me.SuspendLayout()
        '
        'PortLabel
        '
        PortLabel.AutoSize = True
        PortLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PortLabel.Location = New System.Drawing.Point(336, 210)
        PortLabel.Name = "PortLabel"
        PortLabel.Size = New System.Drawing.Size(37, 15)
        PortLabel.TabIndex = 35
        PortLabel.Text = "Port:"
        '
        'HostLabel
        '
        HostLabel.AutoSize = True
        HostLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        HostLabel.Location = New System.Drawing.Point(333, 183)
        HostLabel.Name = "HostLabel"
        HostLabel.Size = New System.Drawing.Size(40, 15)
        HostLabel.TabIndex = 33
        HostLabel.Text = "Host:"
        '
        'PasswordLabel
        '
        PasswordLabel.AutoSize = True
        PasswordLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PasswordLabel.Location = New System.Drawing.Point(300, 156)
        PasswordLabel.Name = "PasswordLabel"
        PasswordLabel.Size = New System.Drawing.Size(73, 15)
        PasswordLabel.TabIndex = 31
        PasswordLabel.Text = "Password:"
        '
        'CuentaLabel
        '
        CuentaLabel.AutoSize = True
        CuentaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CuentaLabel.Location = New System.Drawing.Point(317, 129)
        CuentaLabel.Name = "CuentaLabel"
        CuentaLabel.Size = New System.Drawing.Size(56, 15)
        CuentaLabel.TabIndex = 29
        CuentaLabel.Text = "Cuenta:"
        '
        'MesesLabel
        '
        MesesLabel.AutoSize = True
        MesesLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        MesesLabel.Location = New System.Drawing.Point(312, 201)
        MesesLabel.Name = "MesesLabel"
        MesesLabel.Size = New System.Drawing.Size(132, 15)
        MesesLabel.TabIndex = 4
        MesesLabel.Text = "Número de Meses :"
        '
        'ConceptoLabel
        '
        ConceptoLabel.BackColor = System.Drawing.Color.Black
        ConceptoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ConceptoLabel.ForeColor = System.Drawing.Color.White
        ConceptoLabel.Location = New System.Drawing.Point(287, 115)
        ConceptoLabel.Name = "ConceptoLabel"
        ConceptoLabel.Size = New System.Drawing.Size(299, 24)
        ConceptoLabel.TabIndex = 5
        ConceptoLabel.Text = "Tipo de Servicio :"
        ConceptoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label20
        '
        Label20.AutoSize = True
        Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label20.Location = New System.Drawing.Point(336, 189)
        Label20.Name = "Label20"
        Label20.Size = New System.Drawing.Size(98, 15)
        Label20.TabIndex = 20
        Label20.Text = "Desde el Dia :"
        '
        'Label19
        '
        Label19.AutoSize = True
        Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label19.Location = New System.Drawing.Point(340, 216)
        Label19.Name = "Label19"
        Label19.Size = New System.Drawing.Size(94, 15)
        Label19.TabIndex = 21
        Label19.Text = "Hasta el Dia :"
        '
        'Label22
        '
        Label22.AutoSize = True
        Label22.Location = New System.Drawing.Point(599, 292)
        Label22.Name = "Label22"
        Label22.Size = New System.Drawing.Size(52, 15)
        Label22.TabIndex = 29
        Label22.Text = "Precio:"
        Label22.Visible = False
        '
        'Label16
        '
        Label16.AutoSize = True
        Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label16.Location = New System.Drawing.Point(310, 339)
        Label16.Name = "Label16"
        Label16.Size = New System.Drawing.Size(124, 15)
        Label16.TabIndex = 20
        Label16.Text = "Por Pronto Pago : "
        '
        'Label21
        '
        Label21.AutoSize = True
        Label21.Location = New System.Drawing.Point(588, 330)
        Label21.Name = "Label21"
        Label21.Size = New System.Drawing.Size(132, 15)
        Label21.TabIndex = 30
        Label21.Text = "Precio Suspendido:"
        Label21.Visible = False
        '
        'Label15
        '
        Label15.AutoSize = True
        Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label15.Location = New System.Drawing.Point(322, 367)
        Label15.Name = "Label15"
        Label15.Size = New System.Drawing.Size(113, 15)
        Label15.TabIndex = 21
        Label15.Text = "Por Antiguedad :"
        '
        'Label12
        '
        Label12.Location = New System.Drawing.Point(371, 481)
        Label12.Name = "Label12"
        Label12.Size = New System.Drawing.Size(100, 23)
        Label12.TabIndex = 41
        AddHandler Label12.Click, AddressOf Me.Label12_Click
        '
        'ImportePorExtrasLabel
        '
        ImportePorExtrasLabel.AutoSize = True
        ImportePorExtrasLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ImportePorExtrasLabel.Location = New System.Drawing.Point(286, 61)
        ImportePorExtrasLabel.Name = "ImportePorExtrasLabel"
        ImportePorExtrasLabel.Size = New System.Drawing.Size(306, 15)
        ImportePorExtrasLabel.TabIndex = 16
        ImportePorExtrasLabel.Text = "Importe de la mensualidad de la TV Adicional :"
        ImportePorExtrasLabel.Visible = False
        '
        'CargaDeTrabajoLabel
        '
        CargaDeTrabajoLabel.AutoSize = True
        CargaDeTrabajoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CargaDeTrabajoLabel.Location = New System.Drawing.Point(32, 24)
        CargaDeTrabajoLabel.Name = "CargaDeTrabajoLabel"
        CargaDeTrabajoLabel.Size = New System.Drawing.Size(66, 15)
        CargaDeTrabajoLabel.TabIndex = 0
        CargaDeTrabajoLabel.Text = "General :"
        '
        'Carga_trab_insLabel
        '
        Carga_trab_insLabel.AutoSize = True
        Carga_trab_insLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Carga_trab_insLabel.Location = New System.Drawing.Point(13, 51)
        Carga_trab_insLabel.Name = "Carga_trab_insLabel"
        Carga_trab_insLabel.Size = New System.Drawing.Size(85, 15)
        Carga_trab_insLabel.TabIndex = 2
        Carga_trab_insLabel.Text = "Instalación :"
        '
        'NumeroExtLabel
        '
        NumeroExtLabel.AutoSize = True
        NumeroExtLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NumeroExtLabel.Location = New System.Drawing.Point(17, 21)
        NumeroExtLabel.Name = "NumeroExtLabel"
        NumeroExtLabel.Size = New System.Drawing.Size(168, 15)
        NumeroExtLabel.TabIndex = 0
        NumeroExtLabel.Text = "Numero de Extensiones :"
        NumeroExtLabel.Visible = False
        '
        'CostoExtLabel
        '
        CostoExtLabel.AutoSize = True
        CostoExtLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CostoExtLabel.Location = New System.Drawing.Point(35, 50)
        CostoExtLabel.Name = "CostoExtLabel"
        CostoExtLabel.Size = New System.Drawing.Size(151, 15)
        CostoExtLabel.TabIndex = 2
        CostoExtLabel.Text = "Costo por  Extensión : "
        CostoExtLabel.Visible = False
        '
        'ImpresoraOrdenesLabel
        '
        ImpresoraOrdenesLabel.AutoSize = True
        ImpresoraOrdenesLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ImpresoraOrdenesLabel.Location = New System.Drawing.Point(217, 192)
        ImpresoraOrdenesLabel.Name = "ImpresoraOrdenesLabel"
        ImpresoraOrdenesLabel.Size = New System.Drawing.Size(175, 15)
        ImpresoraOrdenesLabel.TabIndex = 21
        ImpresoraOrdenesLabel.Text = "Impresora Recone&xiones :"
        '
        'Puntos1Label
        '
        Puntos1Label.AutoSize = True
        Puntos1Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Puntos1Label.Location = New System.Drawing.Point(37, 279)
        Puntos1Label.Name = "Puntos1Label"
        Puntos1Label.Size = New System.Drawing.Size(55, 15)
        Puntos1Label.TabIndex = 23
        Puntos1Label.Text = "Puntos "
        '
        'Label24
        '
        Label24.AutoSize = True
        Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label24.Location = New System.Drawing.Point(147, 279)
        Label24.Name = "Label24"
        Label24.Size = New System.Drawing.Size(15, 15)
        Label24.TabIndex = 30
        Label24.Text = "a"
        '
        'Label25
        '
        Label25.AutoSize = True
        Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label25.Location = New System.Drawing.Point(223, 279)
        Label25.Name = "Label25"
        Label25.Size = New System.Drawing.Size(46, 15)
        Label25.TabIndex = 32
        Label25.Text = "años :"
        '
        'Label28
        '
        Label28.AutoSize = True
        Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label28.Location = New System.Drawing.Point(36, 306)
        Label28.Name = "Label28"
        Label28.Size = New System.Drawing.Size(55, 15)
        Label28.TabIndex = 33
        Label28.Text = "Puntos "
        '
        'Label27
        '
        Label27.AutoSize = True
        Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label27.Location = New System.Drawing.Point(147, 306)
        Label27.Name = "Label27"
        Label27.Size = New System.Drawing.Size(15, 15)
        Label27.TabIndex = 35
        Label27.Text = "a"
        '
        'Label26
        '
        Label26.AutoSize = True
        Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label26.Location = New System.Drawing.Point(223, 306)
        Label26.Name = "Label26"
        Label26.Size = New System.Drawing.Size(46, 15)
        Label26.TabIndex = 37
        Label26.Text = "años :"
        '
        'Label31
        '
        Label31.AutoSize = True
        Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label31.Location = New System.Drawing.Point(36, 332)
        Label31.Name = "Label31"
        Label31.Size = New System.Drawing.Size(55, 15)
        Label31.TabIndex = 38
        Label31.Text = "Puntos "
        '
        'Label30
        '
        Label30.AutoSize = True
        Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label30.Location = New System.Drawing.Point(147, 332)
        Label30.Name = "Label30"
        Label30.Size = New System.Drawing.Size(15, 15)
        Label30.TabIndex = 40
        Label30.Text = "a"
        '
        'Label29
        '
        Label29.AutoSize = True
        Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label29.Location = New System.Drawing.Point(223, 332)
        Label29.Name = "Label29"
        Label29.Size = New System.Drawing.Size(46, 15)
        Label29.TabIndex = 42
        Label29.Text = "años :"
        '
        'Label34
        '
        Label34.AutoSize = True
        Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label34.Location = New System.Drawing.Point(36, 358)
        Label34.Name = "Label34"
        Label34.Size = New System.Drawing.Size(55, 15)
        Label34.TabIndex = 43
        Label34.Text = "Puntos "
        '
        'Label33
        '
        Label33.AutoSize = True
        Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label33.Location = New System.Drawing.Point(147, 358)
        Label33.Name = "Label33"
        Label33.Size = New System.Drawing.Size(15, 15)
        Label33.TabIndex = 45
        Label33.Text = "a"
        '
        'Label32
        '
        Label32.AutoSize = True
        Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label32.Location = New System.Drawing.Point(223, 358)
        Label32.Name = "Label32"
        Label32.Size = New System.Drawing.Size(46, 15)
        Label32.TabIndex = 47
        Label32.Text = "años :"
        '
        'Si_se_generaLabel
        '
        Si_se_generaLabel.AutoSize = True
        Si_se_generaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Si_se_generaLabel.Location = New System.Drawing.Point(276, 97)
        Si_se_generaLabel.Name = "Si_se_generaLabel"
        Si_se_generaLabel.Size = New System.Drawing.Size(174, 16)
        Si_se_generaLabel.TabIndex = 52
        Si_se_generaLabel.Text = "Generar Factura Global "
        '
        'MensajeLabel
        '
        MensajeLabel.AutoSize = True
        MensajeLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        MensajeLabel.Location = New System.Drawing.Point(210, 120)
        MensajeLabel.Name = "MensajeLabel"
        MensajeLabel.Size = New System.Drawing.Size(66, 15)
        MensajeLabel.TabIndex = 54
        MensajeLabel.Text = "Mensaje:"
        '
        'ActivoLabel
        '
        ActivoLabel.AutoSize = True
        ActivoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ActivoLabel.Location = New System.Drawing.Point(210, 299)
        ActivoLabel.Name = "ActivoLabel"
        ActivoLabel.Size = New System.Drawing.Size(48, 15)
        ActivoLabel.TabIndex = 56
        ActivoLabel.Text = "Activo:"
        '
        'Label38
        '
        Label38.AutoSize = True
        Label38.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label38.Location = New System.Drawing.Point(197, 158)
        Label38.Name = "Label38"
        Label38.Size = New System.Drawing.Size(70, 15)
        Label38.TabIndex = 54
        Label38.Text = "Mensaje :"
        '
        'Label41
        '
        Label41.AutoSize = True
        Label41.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label41.Location = New System.Drawing.Point(197, 102)
        Label41.Name = "Label41"
        Label41.Size = New System.Drawing.Size(99, 15)
        Label41.TabIndex = 58
        Label41.Text = "Monto Límite :"
        '
        'Label39
        '
        Label39.AutoSize = True
        Label39.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label39.Location = New System.Drawing.Point(202, 275)
        Label39.Name = "Label39"
        Label39.Size = New System.Drawing.Size(27, 15)
        Label39.TabIndex = 60
        Label39.Text = "Ip :"
        '
        'CablemodemsLabel
        '
        CablemodemsLabel.AutoSize = True
        CablemodemsLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CablemodemsLabel.Location = New System.Drawing.Point(208, 128)
        CablemodemsLabel.Name = "CablemodemsLabel"
        CablemodemsLabel.Size = New System.Drawing.Size(112, 16)
        CablemodemsLabel.TabIndex = 73
        CablemodemsLabel.Text = "Cablemodems:"
        '
        'PregcablLabel
        '
        PregcablLabel.AutoSize = True
        PregcablLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PregcablLabel.Location = New System.Drawing.Point(458, 160)
        PregcablLabel.Name = "PregcablLabel"
        PregcablLabel.Size = New System.Drawing.Size(194, 16)
        PregcablLabel.TabIndex = 75
        PregcablLabel.Text = "Número Máximo de Pagos:"
        '
        'DecodersLabel
        '
        DecodersLabel.AutoSize = True
        DecodersLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DecodersLabel.Location = New System.Drawing.Point(179, 163)
        DecodersLabel.Name = "DecodersLabel"
        DecodersLabel.Size = New System.Drawing.Size(141, 16)
        DecodersLabel.TabIndex = 77
        DecodersLabel.Text = "Aparatos Digitales:"
        '
        'PregdecoLabel
        '
        PregdecoLabel.AutoSize = True
        PregdecoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PregdecoLabel.Location = New System.Drawing.Point(176, 258)
        PregdecoLabel.Name = "PregdecoLabel"
        PregdecoLabel.Size = New System.Drawing.Size(194, 16)
        PregdecoLabel.TabIndex = 79
        PregdecoLabel.Text = "Número Máximo de Pagos:"
        PregdecoLabel.Visible = False
        '
        'TELefonosLabel
        '
        TELefonosLabel.AutoSize = True
        TELefonosLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TELefonosLabel.Location = New System.Drawing.Point(182, 435)
        TELefonosLabel.Name = "TELefonosLabel"
        TELefonosLabel.Size = New System.Drawing.Size(78, 15)
        TELefonosLabel.TabIndex = 13
        TELefonosLabel.Text = "Teléfonos :"
        '
        'RfcLabel
        '
        RfcLabel.AutoSize = True
        RfcLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        RfcLabel.Location = New System.Drawing.Point(218, 408)
        RfcLabel.Name = "RfcLabel"
        RfcLabel.Size = New System.Drawing.Size(42, 15)
        RfcLabel.TabIndex = 11
        RfcLabel.Text = "RFC :"
        '
        'CiudadLabel
        '
        CiudadLabel.AutoSize = True
        CiudadLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CiudadLabel.Location = New System.Drawing.Point(204, 381)
        CiudadLabel.Name = "CiudadLabel"
        CiudadLabel.Size = New System.Drawing.Size(56, 15)
        CiudadLabel.TabIndex = 9
        CiudadLabel.Text = "Ciudad:"
        '
        'ColoniaLabel
        '
        ColoniaLabel.AutoSize = True
        ColoniaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ColoniaLabel.Location = New System.Drawing.Point(196, 327)
        ColoniaLabel.Name = "ColoniaLabel"
        ColoniaLabel.Size = New System.Drawing.Size(64, 15)
        ColoniaLabel.TabIndex = 7
        ColoniaLabel.Text = "Colonia :"
        '
        'DireccionLabel
        '
        DireccionLabel.AutoSize = True
        DireccionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DireccionLabel.Location = New System.Drawing.Point(188, 204)
        DireccionLabel.Name = "DireccionLabel"
        DireccionLabel.Size = New System.Drawing.Size(76, 15)
        DireccionLabel.TabIndex = 5
        DireccionLabel.Text = "Dirección :"
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.Location = New System.Drawing.Point(188, 184)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(72, 15)
        NombreLabel.TabIndex = 3
        NombreLabel.Text = "Empresa :"
        '
        'Id_sucursalLabel
        '
        Id_sucursalLabel.AutoSize = True
        Id_sucursalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Id_sucursalLabel.Location = New System.Drawing.Point(134, 153)
        Id_sucursalLabel.Name = "Id_sucursalLabel"
        Id_sucursalLabel.Size = New System.Drawing.Size(126, 15)
        Id_sucursalLabel.TabIndex = 0
        Id_sucursalLabel.Text = "Clave de la Plaza :"
        '
        'Fecha_InicioLabel
        '
        Fecha_InicioLabel.AutoSize = True
        Fecha_InicioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_InicioLabel.Location = New System.Drawing.Point(63, 134)
        Fecha_InicioLabel.Name = "Fecha_InicioLabel"
        Fecha_InicioLabel.Size = New System.Drawing.Size(85, 13)
        Fecha_InicioLabel.TabIndex = 21
        Fecha_InicioLabel.Text = "Fecha Inicio :"
        '
        'Numero_InicioLabel
        '
        Numero_InicioLabel.AutoSize = True
        Numero_InicioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Numero_InicioLabel.Location = New System.Drawing.Point(63, 216)
        Numero_InicioLabel.Name = "Numero_InicioLabel"
        Numero_InicioLabel.Size = New System.Drawing.Size(93, 13)
        Numero_InicioLabel.TabIndex = 23
        Numero_InicioLabel.Text = "Número Inicio :"
        '
        'FormatoLabel
        '
        FormatoLabel.AutoSize = True
        FormatoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FormatoLabel.Location = New System.Drawing.Point(63, 242)
        FormatoLabel.Name = "FormatoLabel"
        FormatoLabel.Size = New System.Drawing.Size(60, 13)
        FormatoLabel.TabIndex = 25
        FormatoLabel.Text = "Formato :"
        '
        'RutaLabel
        '
        RutaLabel.AutoSize = True
        RutaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        RutaLabel.Location = New System.Drawing.Point(63, 268)
        RutaLabel.Name = "RutaLabel"
        RutaLabel.Size = New System.Drawing.Size(42, 13)
        RutaLabel.TabIndex = 27
        RutaLabel.Text = "Ruta :"
        '
        'Numero_LocalLabel
        '
        Numero_LocalLabel.AutoSize = True
        Numero_LocalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Numero_LocalLabel.Location = New System.Drawing.Point(63, 294)
        Numero_LocalLabel.Name = "Numero_LocalLabel"
        Numero_LocalLabel.Size = New System.Drawing.Size(93, 13)
        Numero_LocalLabel.TabIndex = 29
        Numero_LocalLabel.Text = "Número Local :"
        '
        'Pais_LocalLabel
        '
        Pais_LocalLabel.AutoSize = True
        Pais_LocalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Pais_LocalLabel.Location = New System.Drawing.Point(63, 320)
        Pais_LocalLabel.Name = "Pais_LocalLabel"
        Pais_LocalLabel.Size = New System.Drawing.Size(74, 13)
        Pais_LocalLabel.TabIndex = 31
        Pais_LocalLabel.Text = "Pais Local :"
        '
        'Clave044Label
        '
        Clave044Label.AutoSize = True
        Clave044Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clave044Label.Location = New System.Drawing.Point(63, 346)
        Clave044Label.Name = "Clave044Label"
        Clave044Label.Size = New System.Drawing.Size(72, 13)
        Clave044Label.TabIndex = 33
        Clave044Label.Text = "Clave 044 :"
        '
        'Clave045Label
        '
        Clave045Label.AutoSize = True
        Clave045Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clave045Label.Location = New System.Drawing.Point(63, 372)
        Clave045Label.Name = "Clave045Label"
        Clave045Label.Size = New System.Drawing.Size(72, 13)
        Clave045Label.TabIndex = 35
        Clave045Label.Text = "Clave 045 :"
        '
        'Clave01800Label
        '
        Clave01800Label.AutoSize = True
        Clave01800Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clave01800Label.Location = New System.Drawing.Point(63, 398)
        Clave01800Label.Name = "Clave01800Label"
        Clave01800Label.Size = New System.Drawing.Size(86, 13)
        Clave01800Label.TabIndex = 37
        Clave01800Label.Text = "Clave 01800 :"
        '
        'Clave01900Label
        '
        Clave01900Label.AutoSize = True
        Clave01900Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clave01900Label.Location = New System.Drawing.Point(63, 424)
        Clave01900Label.Name = "Clave01900Label"
        Clave01900Label.Size = New System.Drawing.Size(86, 13)
        Clave01900Label.TabIndex = 39
        Clave01900Label.Text = "Clave 01900 :"
        '
        'Label46
        '
        Label46.AutoSize = True
        Label46.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label46.Location = New System.Drawing.Point(63, 189)
        Label46.Name = "Label46"
        Label46.Size = New System.Drawing.Size(85, 13)
        Label46.TabIndex = 41
        Label46.Text = "Periodos del :"
        '
        'Nom_ArchivoLabel
        '
        Nom_ArchivoLabel.AutoSize = True
        Nom_ArchivoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Nom_ArchivoLabel.Location = New System.Drawing.Point(63, 107)
        Nom_ArchivoLabel.Name = "Nom_ArchivoLabel"
        Nom_ArchivoLabel.Size = New System.Drawing.Size(127, 13)
        Nom_ArchivoLabel.TabIndex = 19
        Nom_ArchivoLabel.Text = "Nombre de  Archivo :"
        '
        'Fecha_FacturasLabel
        '
        Fecha_FacturasLabel.AutoSize = True
        Fecha_FacturasLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_FacturasLabel.Location = New System.Drawing.Point(63, 162)
        Fecha_FacturasLabel.Name = "Fecha_FacturasLabel"
        Fecha_FacturasLabel.Size = New System.Drawing.Size(181, 13)
        Fecha_FacturasLabel.TabIndex = 47
        Fecha_FacturasLabel.Text = "Fecha de emisión de Facturas:"
        '
        'Periodo_inicialLabel
        '
        Periodo_inicialLabel.AutoSize = True
        Periodo_inicialLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Periodo_inicialLabel.Location = New System.Drawing.Point(305, 188)
        Periodo_inicialLabel.Name = "Periodo_inicialLabel"
        Periodo_inicialLabel.Size = New System.Drawing.Size(17, 13)
        Periodo_inicialLabel.TabIndex = 49
        Periodo_inicialLabel.Text = "al"
        '
        'ModuloLabel
        '
        ModuloLabel.AutoSize = True
        ModuloLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ModuloLabel.Location = New System.Drawing.Point(297, 202)
        ModuloLabel.Name = "ModuloLabel"
        ModuloLabel.Size = New System.Drawing.Size(63, 15)
        ModuloLabel.TabIndex = 21
        ModuloLabel.Text = "Módulo :"
        '
        'ConstanteLabel
        '
        ConstanteLabel.AutoSize = True
        ConstanteLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ConstanteLabel.Location = New System.Drawing.Point(281, 171)
        ConstanteLabel.Name = "ConstanteLabel"
        ConstanteLabel.Size = New System.Drawing.Size(79, 15)
        ConstanteLabel.TabIndex = 19
        ConstanteLabel.Text = "Constante :"
        '
        'ReferenciaLabel
        '
        ReferenciaLabel.AutoSize = True
        ReferenciaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ReferenciaLabel.Location = New System.Drawing.Point(232, 140)
        ReferenciaLabel.Name = "ReferenciaLabel"
        ReferenciaLabel.Size = New System.Drawing.Size(128, 15)
        ReferenciaLabel.TabIndex = 17
        ReferenciaLabel.Text = "Referencia Inicial :"
        '
        'Label48
        '
        Label48.AutoSize = True
        Label48.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label48.Location = New System.Drawing.Point(279, 132)
        Label48.Name = "Label48"
        Label48.Size = New System.Drawing.Size(128, 15)
        Label48.TabIndex = 17
        Label48.Text = "Referencia Inicial :"
        '
        'Label50
        '
        Label50.AutoSize = True
        Label50.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label50.Location = New System.Drawing.Point(344, 166)
        Label50.Name = "Label50"
        Label50.Size = New System.Drawing.Size(63, 15)
        Label50.TabIndex = 21
        Label50.Text = "Módulo :"
        '
        'Label52
        '
        Label52.AutoSize = True
        Label52.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label52.Location = New System.Drawing.Point(543, 229)
        Label52.Name = "Label52"
        Label52.Size = New System.Drawing.Size(44, 13)
        Label52.TabIndex = 72
        Label52.Text = "Serie :"
        '
        'Label53
        '
        Label53.AutoSize = True
        Label53.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label53.Location = New System.Drawing.Point(543, 255)
        Label53.Name = "Label53"
        Label53.Size = New System.Drawing.Size(42, 13)
        Label53.TabIndex = 73
        Label53.Text = "Folio :"
        '
        'LblCdClvAdic
        '
        LblCdClvAdic.AutoSize = True
        LblCdClvAdic.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        LblCdClvAdic.Location = New System.Drawing.Point(542, 346)
        LblCdClvAdic.Name = "LblCdClvAdic"
        LblCdClvAdic.Size = New System.Drawing.Size(118, 13)
        LblCdClvAdic.TabIndex = 78
        LblCdClvAdic.Text = "Ciudad / Municipio:"
        '
        'LblEdoClvAdic
        '
        LblEdoClvAdic.AutoSize = True
        LblEdoClvAdic.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        LblEdoClvAdic.Location = New System.Drawing.Point(542, 320)
        LblEdoClvAdic.Name = "LblEdoClvAdic"
        LblEdoClvAdic.Size = New System.Drawing.Size(54, 13)
        LblEdoClvAdic.TabIndex = 77
        LblEdoClvAdic.Text = "Estado :"
        '
        'Label69
        '
        Label69.AutoSize = True
        Label69.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label69.Location = New System.Drawing.Point(201, 139)
        Label69.Name = "Label69"
        Label69.Size = New System.Drawing.Size(47, 15)
        Label69.TabIndex = 81
        Label69.Text = "I.V.A. :"
        '
        'Label84
        '
        Label84.BackColor = System.Drawing.Color.Black
        Label84.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label84.ForeColor = System.Drawing.Color.White
        Label84.Location = New System.Drawing.Point(150, 64)
        Label84.Name = "Label84"
        Label84.Size = New System.Drawing.Size(510, 24)
        Label84.TabIndex = 128
        Label84.Text = "Configuración de Rangos de Montos por Cobro de Material Diferido"
        Label84.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ConsultaCpGeneBindingSource
        '
        Me.ConsultaCpGeneBindingSource.DataMember = "Consulta_Cp_Gene"
        Me.ConsultaCpGeneBindingSource.DataSource = Me.DataSetarnoldo
        '
        'DataSetarnoldo
        '
        Me.DataSetarnoldo.DataSetName = "DataSetarnoldo"
        Me.DataSetarnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Consulta_ImpresorasBindingSource
        '
        Me.Consulta_ImpresorasBindingSource.DataMember = "Consulta_Impresoras"
        Me.Consulta_ImpresorasBindingSource.DataSource = Me.DataSetarnoldo
        '
        'MuestraTipServEricBindingSource
        '
        Me.MuestraTipServEricBindingSource.DataMember = "MuestraTipServEric"
        Me.MuestraTipServEricBindingSource.DataSource = Me.DataSetEric
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ConMesesCobroAdeudoBindingSource
        '
        Me.ConMesesCobroAdeudoBindingSource.DataMember = "ConMesesCobroAdeudo"
        Me.ConMesesCobroAdeudoBindingSource.DataSource = Me.DataSetEric
        '
        'ConGeneralCorreoBindingSource
        '
        Me.ConGeneralCorreoBindingSource.DataMember = "ConGeneralCorreo"
        Me.ConGeneralCorreoBindingSource.DataSource = Me.DataSetEric
        '
        'Consulta_ImpresorasTableAdapter
        '
        Me.Consulta_ImpresorasTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Mod_Cp_GenBindingSource
        '
        Me.Inserta_Mod_Cp_GenBindingSource.DataMember = "Inserta_Mod_Cp_Gen"
        Me.Inserta_Mod_Cp_GenBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Inserta_Mod_Cp_GenTableAdapter
        '
        Me.Inserta_Mod_Cp_GenTableAdapter.ClearBeforeFill = True
        '
        'Consulta_Cp_GeneTableAdapter
        '
        Me.Consulta_Cp_GeneTableAdapter.ClearBeforeFill = True
        '
        'ConMesesCobroAdeudoTableAdapter
        '
        Me.ConMesesCobroAdeudoTableAdapter.ClearBeforeFill = True
        '
        'MuestraTipServEricTableAdapter
        '
        Me.MuestraTipServEricTableAdapter.ClearBeforeFill = True
        '
        'ConGeneralCorreoTableAdapter
        '
        Me.ConGeneralCorreoTableAdapter.ClearBeforeFill = True
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Consulta_Generales_Filtros_OrdenesBindingSource
        '
        Me.Consulta_Generales_Filtros_OrdenesBindingSource.DataMember = "Consulta_Generales_Filtros_Ordenes"
        Me.Consulta_Generales_Filtros_OrdenesBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Consulta_Generales_Filtros_OrdenesTableAdapter
        '
        Me.Consulta_Generales_Filtros_OrdenesTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Generales_Filtros_OrdenesBindingSource
        '
        Me.Inserta_Generales_Filtros_OrdenesBindingSource.DataMember = "Inserta_Generales_Filtros_Ordenes"
        Me.Inserta_Generales_Filtros_OrdenesBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Inserta_Generales_Filtros_OrdenesTableAdapter
        '
        Me.Inserta_Generales_Filtros_OrdenesTableAdapter.ClearBeforeFill = True
        '
        'COnsultaGralfacturaglobalBindingSource
        '
        Me.COnsultaGralfacturaglobalBindingSource.DataMember = "COnsulta_Gral_factura_global"
        Me.COnsultaGralfacturaglobalBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'COnsulta_Gral_factura_globalTableAdapter
        '
        Me.COnsulta_Gral_factura_globalTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Gral_factura_globalBindingSource
        '
        Me.Inserta_Gral_factura_globalBindingSource.DataMember = "Inserta_Gral_factura_global"
        Me.Inserta_Gral_factura_globalBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Inserta_Gral_factura_globalTableAdapter
        '
        Me.Inserta_Gral_factura_globalTableAdapter.ClearBeforeFill = True
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Transparent
        Me.Panel4.Controls.Add(Me.Button12)
        Me.Panel4.Controls.Add(CuentaLabel)
        Me.Panel4.Controls.Add(Me.CuentaTextBox)
        Me.Panel4.Controls.Add(PasswordLabel)
        Me.Panel4.Controls.Add(Me.PasswordTextBox)
        Me.Panel4.Controls.Add(HostLabel)
        Me.Panel4.Controls.Add(Me.HostTextBox)
        Me.Panel4.Controls.Add(PortLabel)
        Me.Panel4.Controls.Add(Me.PortTextBox)
        Me.Panel4.Controls.Add(Me.Label17)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel4.Location = New System.Drawing.Point(0, 0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(865, 592)
        Me.Panel4.TabIndex = 32
        '
        'Button12
        '
        Me.Button12.BackColor = System.Drawing.SystemColors.Control
        Me.Button12.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button12.ForeColor = System.Drawing.Color.Black
        Me.Button12.Location = New System.Drawing.Point(694, 543)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(136, 36)
        Me.Button12.TabIndex = 29
        Me.Button12.Text = "&GUARDAR"
        Me.Button12.UseVisualStyleBackColor = False
        '
        'CuentaTextBox
        '
        Me.CuentaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralCorreoBindingSource, "Cuenta", True))
        Me.CuentaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CuentaTextBox.Location = New System.Drawing.Point(379, 126)
        Me.CuentaTextBox.Name = "CuentaTextBox"
        Me.CuentaTextBox.Size = New System.Drawing.Size(208, 21)
        Me.CuentaTextBox.TabIndex = 30
        '
        'PasswordTextBox
        '
        Me.PasswordTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralCorreoBindingSource, "Password", True))
        Me.PasswordTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PasswordTextBox.Location = New System.Drawing.Point(379, 153)
        Me.PasswordTextBox.Name = "PasswordTextBox"
        Me.PasswordTextBox.Size = New System.Drawing.Size(208, 21)
        Me.PasswordTextBox.TabIndex = 32
        '
        'HostTextBox
        '
        Me.HostTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralCorreoBindingSource, "Host", True))
        Me.HostTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.HostTextBox.Location = New System.Drawing.Point(379, 180)
        Me.HostTextBox.Name = "HostTextBox"
        Me.HostTextBox.Size = New System.Drawing.Size(208, 21)
        Me.HostTextBox.TabIndex = 34
        '
        'PortTextBox
        '
        Me.PortTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralCorreoBindingSource, "Port", True))
        Me.PortTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PortTextBox.Location = New System.Drawing.Point(379, 207)
        Me.PortTextBox.Name = "PortTextBox"
        Me.PortTextBox.Size = New System.Drawing.Size(127, 21)
        Me.PortTextBox.TabIndex = 36
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.BackColor = System.Drawing.Color.Black
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.White
        Me.Label17.Location = New System.Drawing.Point(50, 32)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(63, 20)
        Me.Label17.TabIndex = 29
        Me.Label17.Text = "Correo"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.Transparent
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Controls.Add(Me.Button11)
        Me.Panel3.Controls.Add(Me.Clv_TipSerTextBox)
        Me.Panel3.Controls.Add(ConceptoLabel)
        Me.Panel3.Controls.Add(Me.ConceptoComboBox)
        Me.Panel3.Controls.Add(MesesLabel)
        Me.Panel3.Controls.Add(Me.MesesTextBox)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(0, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(865, 592)
        Me.Panel3.TabIndex = 31
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Black
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(37, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(149, 20)
        Me.Label1.TabIndex = 22
        Me.Label1.Text = "Cobro de Adeudo"
        '
        'Button11
        '
        Me.Button11.BackColor = System.Drawing.SystemColors.Control
        Me.Button11.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button11.ForeColor = System.Drawing.Color.Black
        Me.Button11.Location = New System.Drawing.Point(694, 543)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(136, 36)
        Me.Button11.TabIndex = 21
        Me.Button11.Text = "&GUARDAR"
        Me.Button11.UseVisualStyleBackColor = False
        '
        'Clv_TipSerTextBox
        '
        Me.Clv_TipSerTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraTipServEricBindingSource, "Clv_TipSer", True))
        Me.Clv_TipSerTextBox.Location = New System.Drawing.Point(755, 551)
        Me.Clv_TipSerTextBox.Name = "Clv_TipSerTextBox"
        Me.Clv_TipSerTextBox.ReadOnly = True
        Me.Clv_TipSerTextBox.Size = New System.Drawing.Size(10, 21)
        Me.Clv_TipSerTextBox.TabIndex = 7
        Me.Clv_TipSerTextBox.TabStop = False
        '
        'ConceptoComboBox
        '
        Me.ConceptoComboBox.DataSource = Me.MuestraTipServEricBindingSource
        Me.ConceptoComboBox.DisplayMember = "Concepto"
        Me.ConceptoComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConceptoComboBox.FormattingEnabled = True
        Me.ConceptoComboBox.Location = New System.Drawing.Point(290, 142)
        Me.ConceptoComboBox.Name = "ConceptoComboBox"
        Me.ConceptoComboBox.Size = New System.Drawing.Size(296, 23)
        Me.ConceptoComboBox.TabIndex = 6
        Me.ConceptoComboBox.ValueMember = "Clv_TipSer"
        '
        'MesesTextBox
        '
        Me.MesesTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConMesesCobroAdeudoBindingSource, "Meses", True))
        Me.MesesTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MesesTextBox.Location = New System.Drawing.Point(450, 198)
        Me.MesesTextBox.Name = "MesesTextBox"
        Me.MesesTextBox.Size = New System.Drawing.Size(113, 21)
        Me.MesesTextBox.TabIndex = 5
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.Controls.Add(Me.CheckBox1)
        Me.Panel1.Controls.Add(Me.Label18)
        Me.Panel1.Controls.Add(Me.TextBox10)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.TextBox15)
        Me.Panel1.Controls.Add(Label21)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.TextBox11)
        Me.Panel1.Controls.Add(Label20)
        Me.Panel1.Controls.Add(Label15)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Label16)
        Me.Panel1.Controls.Add(Me.TextBox14)
        Me.Panel1.Controls.Add(Me.Button6)
        Me.Panel1.Controls.Add(Label12)
        Me.Panel1.Controls.Add(Me.TextBox16)
        Me.Panel1.Controls.Add(Me.TextBox13)
        Me.Panel1.Controls.Add(Me.ComboBox1)
        Me.Panel1.Controls.Add(Me.TextBox7)
        Me.Panel1.Controls.Add(Label22)
        Me.Panel1.Controls.Add(Label19)
        Me.Panel1.Controls.Add(Me.Label23)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(865, 592)
        Me.Panel1.TabIndex = 29
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.MUESTRAPERIODOSBindingSource, "Habilitar", True))
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.Location = New System.Drawing.Point(510, 115)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(104, 19)
        Me.CheckBox1.TabIndex = 11
        Me.CheckBox1.Text = "Deshabilitar"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'MUESTRAPERIODOSBindingSource
        '
        Me.MUESTRAPERIODOSBindingSource.DataMember = "MUESTRAPERIODOS"
        Me.MUESTRAPERIODOSBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Procedimientosarnoldo4
        '
        Me.Procedimientosarnoldo4.DataSetName = "Procedimientosarnoldo4"
        Me.Procedimientosarnoldo4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.BackColor = System.Drawing.Color.Black
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.White
        Me.Label18.Location = New System.Drawing.Point(34, 29)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(144, 20)
        Me.Label18.TabIndex = 16
        Me.Label18.Text = "Periodo de Corte"
        '
        'TextBox10
        '
        Me.TextBox10.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTAGENERALESDESCBindingSource, "DiasPuntosAnt1", True))
        Me.TextBox10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox10.Location = New System.Drawing.Point(440, 364)
        Me.TextBox10.MaxLength = 3
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.Size = New System.Drawing.Size(100, 21)
        Me.TextBox10.TabIndex = 16
        '
        'CONSULTAGENERALESDESCBindingSource
        '
        Me.CONSULTAGENERALESDESCBindingSource.DataMember = "CONSULTAGENERALESDESC"
        Me.CONSULTAGENERALESDESCBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.Black
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(252, 301)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(330, 23)
        Me.Label7.TabIndex = 39
        Me.Label7.Text = "Días Limite para Puntos  :"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox15
        '
        Me.TextBox15.Location = New System.Drawing.Point(591, 348)
        Me.TextBox15.MaxLength = 3
        Me.TextBox15.Name = "TextBox15"
        Me.TextBox15.ReadOnly = True
        Me.TextBox15.Size = New System.Drawing.Size(110, 21)
        Me.TextBox15.TabIndex = 32
        Me.TextBox15.TabStop = False
        Me.TextBox15.Visible = False
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.Black
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(252, 238)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(330, 23)
        Me.Label6.TabIndex = 38
        Me.Label6.Text = "Proceso de Desconexión :"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox11
        '
        Me.TextBox11.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTAGENERALESDESCBindingSource, "DiasPuntosPP1", True))
        Me.TextBox11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox11.Location = New System.Drawing.Point(440, 337)
        Me.TextBox11.MaxLength = 3
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.Size = New System.Drawing.Size(100, 21)
        Me.TextBox11.TabIndex = 15
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.Black
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(253, 153)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(329, 23)
        Me.Label5.TabIndex = 37
        Me.Label5.Text = "Clientes que Contraten :"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox14
        '
        Me.TextBox14.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTAGENERALESDESCBindingSource, "DiaInicial1", True))
        Me.TextBox14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox14.Location = New System.Drawing.Point(440, 186)
        Me.TextBox14.MaxLength = 3
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.Size = New System.Drawing.Size(100, 21)
        Me.TextBox14.TabIndex = 12
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.SystemColors.Control
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.Black
        Me.Button6.Location = New System.Drawing.Point(694, 543)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(136, 36)
        Me.Button6.TabIndex = 17
        Me.Button6.Text = "&GUARDAR"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'TextBox16
        '
        Me.TextBox16.Location = New System.Drawing.Point(592, 308)
        Me.TextBox16.MaxLength = 3
        Me.TextBox16.Name = "TextBox16"
        Me.TextBox16.ReadOnly = True
        Me.TextBox16.Size = New System.Drawing.Size(109, 21)
        Me.TextBox16.TabIndex = 31
        Me.TextBox16.TabStop = False
        Me.TextBox16.Visible = False
        '
        'TextBox13
        '
        Me.TextBox13.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTAGENERALESDESCBindingSource, "DiaFinal1", True))
        Me.TextBox13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox13.Location = New System.Drawing.Point(440, 213)
        Me.TextBox13.MaxLength = 3
        Me.TextBox13.Name = "TextBox13"
        Me.TextBox13.Size = New System.Drawing.Size(100, 21)
        Me.TextBox13.TabIndex = 13
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.MUESTRAPERIODOSBindingSource
        Me.ComboBox1.DisplayMember = "Descripcion"
        Me.ComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(256, 115)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(205, 23)
        Me.ComboBox1.TabIndex = 10
        Me.ComboBox1.ValueMember = "Clv_Periodo"
        '
        'TextBox7
        '
        Me.TextBox7.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTAGENERALESDESCBindingSource, "DiaCorte", True))
        Me.TextBox7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox7.Location = New System.Drawing.Point(440, 271)
        Me.TextBox7.MaxLength = 3
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(100, 21)
        Me.TextBox7.TabIndex = 14
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.BackColor = System.Drawing.Color.Black
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.White
        Me.Label23.Location = New System.Drawing.Point(252, 89)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(75, 20)
        Me.Label23.TabIndex = 34
        Me.Label23.Text = "Periodo:"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Transparent
        Me.Panel2.Controls.Add(Me.Label13)
        Me.Panel2.Controls.Add(Me.Button8)
        Me.Panel2.Controls.Add(Me.Label11)
        Me.Panel2.Controls.Add(Me.Label10)
        Me.Panel2.Controls.Add(Me.Label9)
        Me.Panel2.Controls.Add(Me.Label8)
        Me.Panel2.Controls.Add(Me.ImpresoraFiscalTextBox)
        Me.Panel2.Controls.Add(Me.ImpresoraTarjetasTextBox)
        Me.Panel2.Controls.Add(Me.ImpresoraTicketsTextBox)
        Me.Panel2.Controls.Add(Me.ImpresoraContratosTextBox)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(865, 592)
        Me.Panel2.TabIndex = 30
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.Black
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(49, 35)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(88, 20)
        Me.Label13.TabIndex = 44
        Me.Label13.Text = "Impresión"
        '
        'Button8
        '
        Me.Button8.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.Button8.BackColor = System.Drawing.SystemColors.Control
        Me.Button8.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.Black
        Me.Button8.Location = New System.Drawing.Point(694, 543)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(136, 36)
        Me.Button8.TabIndex = 14
        Me.Button8.Text = "&GUARDAR"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.Color.Black
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(291, 370)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(330, 23)
        Me.Label11.TabIndex = 42
        Me.Label11.Text = "Impresora de Contratos:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.Color.Black
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(291, 277)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(330, 23)
        Me.Label10.TabIndex = 41
        Me.Label10.Text = "Impresora de Tickets:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.Color.Black
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(291, 191)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(330, 23)
        Me.Label9.TabIndex = 40
        Me.Label9.Text = "Impresora de Tarjetas:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.Color.Black
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(285, 107)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(330, 23)
        Me.Label8.TabIndex = 39
        Me.Label8.Text = "Impresora Fiscal:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ImpresoraFiscalTextBox
        '
        Me.ImpresoraFiscalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ImpresorasBindingSource, "ImpresoraFiscal", True))
        Me.ImpresoraFiscalTextBox.Location = New System.Drawing.Point(290, 136)
        Me.ImpresoraFiscalTextBox.Name = "ImpresoraFiscalTextBox"
        Me.ImpresoraFiscalTextBox.Size = New System.Drawing.Size(325, 21)
        Me.ImpresoraFiscalTextBox.TabIndex = 10
        '
        'ImpresoraTarjetasTextBox
        '
        Me.ImpresoraTarjetasTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ImpresorasBindingSource, "ImpresoraTarjetas", True))
        Me.ImpresoraTarjetasTextBox.Location = New System.Drawing.Point(291, 223)
        Me.ImpresoraTarjetasTextBox.Name = "ImpresoraTarjetasTextBox"
        Me.ImpresoraTarjetasTextBox.Size = New System.Drawing.Size(330, 21)
        Me.ImpresoraTarjetasTextBox.TabIndex = 11
        '
        'ImpresoraTicketsTextBox
        '
        Me.ImpresoraTicketsTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ImpresorasBindingSource, "ImpresoraTickets", True))
        Me.ImpresoraTicketsTextBox.Location = New System.Drawing.Point(295, 307)
        Me.ImpresoraTicketsTextBox.Name = "ImpresoraTicketsTextBox"
        Me.ImpresoraTicketsTextBox.Size = New System.Drawing.Size(326, 21)
        Me.ImpresoraTicketsTextBox.TabIndex = 12
        '
        'ImpresoraContratosTextBox
        '
        Me.ImpresoraContratosTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ImpresorasBindingSource, "ImpresoraContratos", True))
        Me.ImpresoraContratosTextBox.Location = New System.Drawing.Point(299, 400)
        Me.ImpresoraContratosTextBox.Name = "ImpresoraContratosTextBox"
        Me.ImpresoraContratosTextBox.Size = New System.Drawing.Size(325, 21)
        Me.ImpresoraContratosTextBox.TabIndex = 13
        '
        'PanelBasico
        '
        Me.PanelBasico.BackColor = System.Drawing.Color.Transparent
        Me.PanelBasico.Controls.Add(Me.bpxImpresiones)
        Me.PanelBasico.Controls.Add(Me.lblEncabezadoImpresion)
        Me.PanelBasico.Controls.Add(Me.CheckBox3)
        Me.PanelBasico.Controls.Add(Me.Label3)
        Me.PanelBasico.Controls.Add(Me.NumericUpDown2)
        Me.PanelBasico.Controls.Add(Me.CheckBox2)
        Me.PanelBasico.Controls.Add(Label25)
        Me.PanelBasico.Controls.Add(Label24)
        Me.PanelBasico.Controls.Add(Me.Label35)
        Me.PanelBasico.Controls.Add(Label28)
        Me.PanelBasico.Controls.Add(Me.Panel5)
        Me.PanelBasico.Controls.Add(Me.NumericUpDown1)
        Me.PanelBasico.Controls.Add(Label32)
        Me.PanelBasico.Controls.Add(Me.NumericUpDown4)
        Me.PanelBasico.Controls.Add(Me.ImportePorExtrasTextBox)
        Me.PanelBasico.Controls.Add(Me.Label4)
        Me.PanelBasico.Controls.Add(Me.NumericUpDown7)
        Me.PanelBasico.Controls.Add(Label27)
        Me.PanelBasico.Controls.Add(ImportePorExtrasLabel)
        Me.PanelBasico.Controls.Add(Me.Puntos10TextBox)
        Me.PanelBasico.Controls.Add(Label33)
        Me.PanelBasico.Controls.Add(Me.NumericUpDown3)
        Me.PanelBasico.Controls.Add(Me.GroupBox2)
        Me.PanelBasico.Controls.Add(Me.Button3)
        Me.PanelBasico.Controls.Add(Me.NumericUpDown8)
        Me.PanelBasico.Controls.Add(Label26)
        Me.PanelBasico.Controls.Add(Me.GroupBox3)
        Me.PanelBasico.Controls.Add(Me.Puntos5TextBox)
        Me.PanelBasico.Controls.Add(Label34)
        Me.PanelBasico.Controls.Add(Label31)
        Me.PanelBasico.Controls.Add(Me.ImpresoraOrdenesTextBox)
        Me.PanelBasico.Controls.Add(Me.Puntos2TextBox)
        Me.PanelBasico.Controls.Add(Label29)
        Me.PanelBasico.Controls.Add(Me.NumericUpDown6)
        Me.PanelBasico.Controls.Add(ImpresoraOrdenesLabel)
        Me.PanelBasico.Controls.Add(Puntos1Label)
        Me.PanelBasico.Controls.Add(Me.NumericUpDown5)
        Me.PanelBasico.Controls.Add(Label30)
        Me.PanelBasico.Controls.Add(Me.Puntos1TextBox)
        Me.PanelBasico.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelBasico.Location = New System.Drawing.Point(3, 3)
        Me.PanelBasico.Name = "PanelBasico"
        Me.PanelBasico.Size = New System.Drawing.Size(859, 586)
        Me.PanelBasico.TabIndex = 3
        '
        'bpxImpresiones
        '
        Me.bpxImpresiones.Controls.Add(Me.cbxImprimeOrdenesFac)
        Me.bpxImpresiones.Controls.Add(Me.lblAvisoImpresiones)
        Me.bpxImpresiones.Controls.Add(Me.cbxImprimeOrdenesSoftv)
        Me.bpxImpresiones.Controls.Add(Me.cbxImprimeQuejasSoftv)
        Me.bpxImpresiones.Location = New System.Drawing.Point(20, 444)
        Me.bpxImpresiones.Name = "bpxImpresiones"
        Me.bpxImpresiones.Size = New System.Drawing.Size(372, 129)
        Me.bpxImpresiones.TabIndex = 57
        Me.bpxImpresiones.TabStop = False
        '
        'cbxImprimeOrdenesFac
        '
        Me.cbxImprimeOrdenesFac.AutoSize = True
        Me.cbxImprimeOrdenesFac.Location = New System.Drawing.Point(6, 20)
        Me.cbxImprimeOrdenesFac.Name = "cbxImprimeOrdenesFac"
        Me.cbxImprimeOrdenesFac.Size = New System.Drawing.Size(363, 19)
        Me.cbxImprimeOrdenesFac.TabIndex = 53
        Me.cbxImprimeOrdenesFac.Text = "Imprimir automáticamente órdenes al generar nueva"
        Me.cbxImprimeOrdenesFac.UseVisualStyleBackColor = True
        '
        'lblAvisoImpresiones
        '
        Me.lblAvisoImpresiones.AutoSize = True
        Me.lblAvisoImpresiones.Location = New System.Drawing.Point(13, 107)
        Me.lblAvisoImpresiones.Name = "lblAvisoImpresiones"
        Me.lblAvisoImpresiones.Size = New System.Drawing.Size(338, 15)
        Me.lblAvisoImpresiones.TabIndex = 56
        Me.lblAvisoImpresiones.Text = "**** No aplican órdenes de proceso automático ****"
        '
        'cbxImprimeOrdenesSoftv
        '
        Me.cbxImprimeOrdenesSoftv.AutoSize = True
        Me.cbxImprimeOrdenesSoftv.Location = New System.Drawing.Point(6, 48)
        Me.cbxImprimeOrdenesSoftv.Name = "cbxImprimeOrdenesSoftv"
        Me.cbxImprimeOrdenesSoftv.Size = New System.Drawing.Size(320, 19)
        Me.cbxImprimeOrdenesSoftv.TabIndex = 54
        Me.cbxImprimeOrdenesSoftv.Text = "Imprimir automáticamente órdenes al facturar"
        Me.cbxImprimeOrdenesSoftv.UseVisualStyleBackColor = True
        '
        'cbxImprimeQuejasSoftv
        '
        Me.cbxImprimeQuejasSoftv.AutoSize = True
        Me.cbxImprimeQuejasSoftv.Location = New System.Drawing.Point(6, 76)
        Me.cbxImprimeQuejasSoftv.Name = "cbxImprimeQuejasSoftv"
        Me.cbxImprimeQuejasSoftv.Size = New System.Drawing.Size(354, 19)
        Me.cbxImprimeQuejasSoftv.TabIndex = 55
        Me.cbxImprimeQuejasSoftv.Text = "Imprimir automáticamente quejas al generar nueva"
        Me.cbxImprimeQuejasSoftv.UseVisualStyleBackColor = True
        '
        'lblEncabezadoImpresion
        '
        Me.lblEncabezadoImpresion.BackColor = System.Drawing.Color.Black
        Me.lblEncabezadoImpresion.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEncabezadoImpresion.ForeColor = System.Drawing.Color.White
        Me.lblEncabezadoImpresion.Location = New System.Drawing.Point(24, 421)
        Me.lblEncabezadoImpresion.Name = "lblEncabezadoImpresion"
        Me.lblEncabezadoImpresion.Size = New System.Drawing.Size(358, 20)
        Me.lblEncabezadoImpresion.TabIndex = 52
        Me.lblEncabezadoImpresion.Text = "Impresión de Órdenes y Quejas"
        '
        'CheckBox3
        '
        Me.CheckBox3.AutoSize = True
        Me.CheckBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox3.Location = New System.Drawing.Point(483, 293)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(263, 19)
        Me.CheckBox3.TabIndex = 50
        Me.CheckBox3.Text = "Instalación De Filtro De Solo Internet"
        Me.CheckBox3.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Black
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(24, 23)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(190, 20)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "Servicios de Televisión"
        '
        'NumericUpDown2
        '
        Me.NumericUpDown2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown2.Location = New System.Drawing.Point(167, 276)
        Me.NumericUpDown2.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.NumericUpDown2.Name = "NumericUpDown2"
        Me.NumericUpDown2.Size = New System.Drawing.Size(52, 21)
        Me.NumericUpDown2.TabIndex = 17
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox2.Location = New System.Drawing.Point(484, 267)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(232, 19)
        Me.CheckBox2.TabIndex = 49
        Me.CheckBox2.Text = "Retiro De Filtro De Solo Internet"
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'Label35
        '
        Me.Label35.BackColor = System.Drawing.Color.Black
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.ForeColor = System.Drawing.Color.White
        Me.Label35.Location = New System.Drawing.Point(475, 244)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(339, 20)
        Me.Label35.TabIndex = 48
        Me.Label35.Text = "Generar Orden De Cambio De Clientes"
        '
        'Panel5
        '
        Me.Panel5.Location = New System.Drawing.Point(761, 69)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(72, 100)
        Me.Panel5.TabIndex = 51
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown1.Location = New System.Drawing.Point(92, 276)
        Me.NumericUpDown1.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(52, 21)
        Me.NumericUpDown1.TabIndex = 16
        '
        'NumericUpDown4
        '
        Me.NumericUpDown4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown4.Location = New System.Drawing.Point(169, 303)
        Me.NumericUpDown4.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.NumericUpDown4.Name = "NumericUpDown4"
        Me.NumericUpDown4.Size = New System.Drawing.Size(50, 21)
        Me.NumericUpDown4.TabIndex = 20
        '
        'ImportePorExtrasTextBox
        '
        Me.ImportePorExtrasTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "ImportePorExtras", True))
        Me.ImportePorExtrasTextBox.Location = New System.Drawing.Point(593, 55)
        Me.ImportePorExtrasTextBox.MaxLength = 10
        Me.ImportePorExtrasTextBox.Name = "ImportePorExtrasTextBox"
        Me.ImportePorExtrasTextBox.Size = New System.Drawing.Size(100, 21)
        Me.ImportePorExtrasTextBox.TabIndex = 10
        Me.ImportePorExtrasTextBox.Visible = False
        '
        'ConGeneralBindingSource
        '
        Me.ConGeneralBindingSource.AllowNew = False
        Me.ConGeneralBindingSource.DataMember = "ConGeneral"
        Me.ConGeneralBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Black
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(24, 244)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(358, 20)
        Me.Label4.TabIndex = 28
        Me.Label4.Text = "Puntos Por Antigüedad"
        '
        'NumericUpDown7
        '
        Me.NumericUpDown7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown7.Location = New System.Drawing.Point(93, 355)
        Me.NumericUpDown7.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.NumericUpDown7.Name = "NumericUpDown7"
        Me.NumericUpDown7.Size = New System.Drawing.Size(51, 21)
        Me.NumericUpDown7.TabIndex = 25
        '
        'Puntos10TextBox
        '
        Me.Puntos10TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralAntiguedadBindingSource, "Puntos10", True))
        Me.Puntos10TextBox.Location = New System.Drawing.Point(275, 355)
        Me.Puntos10TextBox.MaxLength = 3
        Me.Puntos10TextBox.Name = "Puntos10TextBox"
        Me.Puntos10TextBox.Size = New System.Drawing.Size(100, 21)
        Me.Puntos10TextBox.TabIndex = 27
        '
        'ConGeneralAntiguedadBindingSource
        '
        Me.ConGeneralAntiguedadBindingSource.DataMember = "ConGeneralAntiguedad"
        Me.ConGeneralAntiguedadBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'NumericUpDown3
        '
        Me.NumericUpDown3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown3.Location = New System.Drawing.Point(92, 303)
        Me.NumericUpDown3.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.NumericUpDown3.Name = "NumericUpDown3"
        Me.NumericUpDown3.Size = New System.Drawing.Size(51, 21)
        Me.NumericUpDown3.TabIndex = 19
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Carga_trab_insLabel)
        Me.GroupBox2.Controls.Add(Me.Carga_trab_insTextBox)
        Me.GroupBox2.Controls.Add(CargaDeTrabajoLabel)
        Me.GroupBox2.Controls.Add(Me.CargaDeTrabajoTextBox)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(290, 85)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(302, 84)
        Me.GroupBox2.TabIndex = 13
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Carga de trabajo"
        '
        'Carga_trab_insTextBox
        '
        Me.Carga_trab_insTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "carga_trab_ins", True))
        Me.Carga_trab_insTextBox.Location = New System.Drawing.Point(104, 48)
        Me.Carga_trab_insTextBox.MaxLength = 2
        Me.Carga_trab_insTextBox.Name = "Carga_trab_insTextBox"
        Me.Carga_trab_insTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Carga_trab_insTextBox.TabIndex = 14
        '
        'CargaDeTrabajoTextBox
        '
        Me.CargaDeTrabajoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "CargaDeTrabajo", True))
        Me.CargaDeTrabajoTextBox.Location = New System.Drawing.Point(104, 21)
        Me.CargaDeTrabajoTextBox.MaxLength = 2
        Me.CargaDeTrabajoTextBox.Name = "CargaDeTrabajoTextBox"
        Me.CargaDeTrabajoTextBox.Size = New System.Drawing.Size(100, 21)
        Me.CargaDeTrabajoTextBox.TabIndex = 13
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.SystemColors.Control
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(607, 499)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(136, 36)
        Me.Button3.TabIndex = 20
        Me.Button3.Text = "&GUARDAR"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'NumericUpDown8
        '
        Me.NumericUpDown8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown8.Location = New System.Drawing.Point(169, 355)
        Me.NumericUpDown8.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.NumericUpDown8.Name = "NumericUpDown8"
        Me.NumericUpDown8.Size = New System.Drawing.Size(50, 21)
        Me.NumericUpDown8.TabIndex = 26
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(CostoExtLabel)
        Me.GroupBox3.Controls.Add(Me.CostoExtTextBox)
        Me.GroupBox3.Controls.Add(NumeroExtLabel)
        Me.GroupBox3.Controls.Add(Me.NumeroExtTextBox)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(504, 334)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(295, 83)
        Me.GroupBox3.TabIndex = 11
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Hoteles"
        Me.GroupBox3.Visible = False
        '
        'CostoExtTextBox
        '
        Me.CostoExtTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "CostoExt", True))
        Me.CostoExtTextBox.Location = New System.Drawing.Point(183, 47)
        Me.CostoExtTextBox.MaxLength = 10
        Me.CostoExtTextBox.Name = "CostoExtTextBox"
        Me.CostoExtTextBox.Size = New System.Drawing.Size(100, 21)
        Me.CostoExtTextBox.TabIndex = 12
        Me.CostoExtTextBox.Visible = False
        '
        'NumeroExtTextBox
        '
        Me.NumeroExtTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "NumeroExt", True))
        Me.NumeroExtTextBox.Location = New System.Drawing.Point(183, 18)
        Me.NumeroExtTextBox.MaxLength = 3
        Me.NumeroExtTextBox.Name = "NumeroExtTextBox"
        Me.NumeroExtTextBox.Size = New System.Drawing.Size(100, 21)
        Me.NumeroExtTextBox.TabIndex = 11
        Me.NumeroExtTextBox.Visible = False
        '
        'Puntos5TextBox
        '
        Me.Puntos5TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralAntiguedadBindingSource, "Puntos5", True))
        Me.Puntos5TextBox.Location = New System.Drawing.Point(275, 329)
        Me.Puntos5TextBox.MaxLength = 3
        Me.Puntos5TextBox.Name = "Puntos5TextBox"
        Me.Puntos5TextBox.Size = New System.Drawing.Size(100, 21)
        Me.Puntos5TextBox.TabIndex = 24
        '
        'ImpresoraOrdenesTextBox
        '
        Me.ImpresoraOrdenesTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "ImpresoraOrdenes", True))
        Me.ImpresoraOrdenesTextBox.Location = New System.Drawing.Point(398, 191)
        Me.ImpresoraOrdenesTextBox.MaxLength = 150
        Me.ImpresoraOrdenesTextBox.Name = "ImpresoraOrdenesTextBox"
        Me.ImpresoraOrdenesTextBox.Size = New System.Drawing.Size(345, 21)
        Me.ImpresoraOrdenesTextBox.TabIndex = 15
        '
        'Puntos2TextBox
        '
        Me.Puntos2TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralAntiguedadBindingSource, "Puntos2", True))
        Me.Puntos2TextBox.Location = New System.Drawing.Point(275, 303)
        Me.Puntos2TextBox.MaxLength = 3
        Me.Puntos2TextBox.Name = "Puntos2TextBox"
        Me.Puntos2TextBox.Size = New System.Drawing.Size(100, 21)
        Me.Puntos2TextBox.TabIndex = 21
        '
        'NumericUpDown6
        '
        Me.NumericUpDown6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown6.Location = New System.Drawing.Point(169, 329)
        Me.NumericUpDown6.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.NumericUpDown6.Name = "NumericUpDown6"
        Me.NumericUpDown6.Size = New System.Drawing.Size(50, 21)
        Me.NumericUpDown6.TabIndex = 23
        '
        'NumericUpDown5
        '
        Me.NumericUpDown5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown5.Location = New System.Drawing.Point(92, 329)
        Me.NumericUpDown5.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.NumericUpDown5.Name = "NumericUpDown5"
        Me.NumericUpDown5.Size = New System.Drawing.Size(51, 21)
        Me.NumericUpDown5.TabIndex = 22
        '
        'Puntos1TextBox
        '
        Me.Puntos1TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralAntiguedadBindingSource, "Puntos1", True))
        Me.Puntos1TextBox.Location = New System.Drawing.Point(275, 276)
        Me.Puntos1TextBox.MaxLength = 3
        Me.Puntos1TextBox.Name = "Puntos1TextBox"
        Me.Puntos1TextBox.Size = New System.Drawing.Size(100, 21)
        Me.Puntos1TextBox.TabIndex = 18
        '
        'Consulta_cobrodepositoBindingSource
        '
        Me.Consulta_cobrodepositoBindingSource.DataMember = "Consulta_cobrodeposito"
        Me.Consulta_cobrodepositoBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.Transparent
        Me.Panel6.Controls.Add(Me.RadioButton2)
        Me.Panel6.Controls.Add(Me.RadioButton1)
        Me.Panel6.Controls.Add(Si_se_generaLabel)
        Me.Panel6.Controls.Add(Me.Si_se_generaCheckBox)
        Me.Panel6.Controls.Add(Me.Button15)
        Me.Panel6.Controls.Add(Me.Label36)
        Me.Panel6.Controls.Add(Me.TextBox2)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel6.Location = New System.Drawing.Point(0, 0)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(865, 592)
        Me.Panel6.TabIndex = 52
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton2.Location = New System.Drawing.Point(462, 145)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(80, 19)
        Me.RadioButton2.TabIndex = 55
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "Mensual"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton1.Location = New System.Drawing.Point(341, 145)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(64, 19)
        Me.RadioButton1.TabIndex = 54
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Diaria"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'Si_se_generaCheckBox
        '
        Me.Si_se_generaCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.COnsultaGralfacturaglobalBindingSource, "si_se_genera", True))
        Me.Si_se_generaCheckBox.Location = New System.Drawing.Point(256, 94)
        Me.Si_se_generaCheckBox.Name = "Si_se_generaCheckBox"
        Me.Si_se_generaCheckBox.Size = New System.Drawing.Size(51, 24)
        Me.Si_se_generaCheckBox.TabIndex = 53
        '
        'Button15
        '
        Me.Button15.BackColor = System.Drawing.SystemColors.Control
        Me.Button15.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button15.ForeColor = System.Drawing.Color.Black
        Me.Button15.Location = New System.Drawing.Point(694, 543)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(136, 36)
        Me.Button15.TabIndex = 50
        Me.Button15.Text = "&GUARDAR"
        Me.Button15.UseVisualStyleBackColor = False
        '
        'Label36
        '
        Me.Label36.BackColor = System.Drawing.Color.Black
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.Color.White
        Me.Label36.Location = New System.Drawing.Point(42, 27)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(281, 20)
        Me.Label36.TabIndex = 49
        Me.Label36.Text = "Generales de Facturas Globales."
        '
        'TextBox2
        '
        Me.TextBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.COnsultaGralfacturaglobalBindingSource, "Tipo", True))
        Me.TextBox2.Location = New System.Drawing.Point(595, 145)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(112, 21)
        Me.TextBox2.TabIndex = 54
        '
        'ConGeneralAlertaBindingSource
        '
        Me.ConGeneralAlertaBindingSource.DataMember = "ConGeneralAlerta"
        Me.ConGeneralAlertaBindingSource.DataSource = Me.DataSetLidia2
        '
        'DataSetLidia2
        '
        Me.DataSetLidia2.DataSetName = "DataSetLidia2"
        Me.DataSetLidia2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Panel12
        '
        Me.Panel12.BackColor = System.Drawing.Color.Transparent
        Me.Panel12.Controls.Add(Me.ReferenciaTextBox1)
        Me.Panel12.Controls.Add(Me.ModuloTextBox1)
        Me.Panel12.Controls.Add(Label48)
        Me.Panel12.Controls.Add(Label50)
        Me.Panel12.Controls.Add(Me.Button31)
        Me.Panel12.Controls.Add(Me.Label51)
        Me.Panel12.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel12.Location = New System.Drawing.Point(0, 0)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(865, 592)
        Me.Panel12.TabIndex = 71
        '
        'ReferenciaTextBox1
        '
        Me.ReferenciaTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_HsbcBindingSource, "Referencia", True))
        Me.ReferenciaTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ReferenciaTextBox1.Location = New System.Drawing.Point(425, 126)
        Me.ReferenciaTextBox1.MaxLength = 50
        Me.ReferenciaTextBox1.Name = "ReferenciaTextBox1"
        Me.ReferenciaTextBox1.Size = New System.Drawing.Size(178, 21)
        Me.ReferenciaTextBox1.TabIndex = 22
        '
        'CONSULTA_General_HsbcBindingSource
        '
        Me.CONSULTA_General_HsbcBindingSource.DataMember = "CONSULTA_General_Hsbc"
        Me.CONSULTA_General_HsbcBindingSource.DataSource = Me.DataSetyahve
        '
        'DataSetyahve
        '
        Me.DataSetyahve.DataSetName = "DataSetyahve"
        Me.DataSetyahve.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ModuloTextBox1
        '
        Me.ModuloTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_HsbcBindingSource, "Modulo", True))
        Me.ModuloTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ModuloTextBox1.Location = New System.Drawing.Point(425, 160)
        Me.ModuloTextBox1.Name = "ModuloTextBox1"
        Me.ModuloTextBox1.Size = New System.Drawing.Size(62, 21)
        Me.ModuloTextBox1.TabIndex = 24
        '
        'Button31
        '
        Me.Button31.BackColor = System.Drawing.SystemColors.Control
        Me.Button31.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button31.ForeColor = System.Drawing.Color.Black
        Me.Button31.Location = New System.Drawing.Point(694, 543)
        Me.Button31.Name = "Button31"
        Me.Button31.Size = New System.Drawing.Size(136, 36)
        Me.Button31.TabIndex = 17
        Me.Button31.Text = "&GUARDAR"
        Me.Button31.UseVisualStyleBackColor = False
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.BackColor = System.Drawing.Color.Black
        Me.Label51.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label51.ForeColor = System.Drawing.Color.White
        Me.Label51.Location = New System.Drawing.Point(62, 40)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(248, 20)
        Me.Label51.TabIndex = 15
        Me.Label51.Text = "Datos de la Referencia HSBC"
        '
        'Panel11
        '
        Me.Panel11.BackColor = System.Drawing.Color.Transparent
        Me.Panel11.Controls.Add(ReferenciaLabel)
        Me.Panel11.Controls.Add(Me.ReferenciaTextBox)
        Me.Panel11.Controls.Add(ConstanteLabel)
        Me.Panel11.Controls.Add(Me.ConstanteTextBox)
        Me.Panel11.Controls.Add(ModuloLabel)
        Me.Panel11.Controls.Add(Me.ModuloTextBox)
        Me.Panel11.Controls.Add(Me.Button30)
        Me.Panel11.Controls.Add(Me.Label62)
        Me.Panel11.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel11.Location = New System.Drawing.Point(0, 0)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(865, 592)
        Me.Panel11.TabIndex = 70
        '
        'ReferenciaTextBox
        '
        Me.ReferenciaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_SantanderBindingSource, "Referencia", True))
        Me.ReferenciaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ReferenciaTextBox.Location = New System.Drawing.Point(375, 140)
        Me.ReferenciaTextBox.MaxLength = 50
        Me.ReferenciaTextBox.Name = "ReferenciaTextBox"
        Me.ReferenciaTextBox.Size = New System.Drawing.Size(226, 21)
        Me.ReferenciaTextBox.TabIndex = 18
        '
        'CONSULTA_General_SantanderBindingSource
        '
        Me.CONSULTA_General_SantanderBindingSource.DataMember = "CONSULTA_General_Santander"
        Me.CONSULTA_General_SantanderBindingSource.DataSource = Me.DataSetyahve
        '
        'ConstanteTextBox
        '
        Me.ConstanteTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_SantanderBindingSource, "Constante", True))
        Me.ConstanteTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConstanteTextBox.Location = New System.Drawing.Point(375, 168)
        Me.ConstanteTextBox.Name = "ConstanteTextBox"
        Me.ConstanteTextBox.Size = New System.Drawing.Size(74, 21)
        Me.ConstanteTextBox.TabIndex = 20
        '
        'ModuloTextBox
        '
        Me.ModuloTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_SantanderBindingSource, "Modulo", True))
        Me.ModuloTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ModuloTextBox.Location = New System.Drawing.Point(375, 196)
        Me.ModuloTextBox.Name = "ModuloTextBox"
        Me.ModuloTextBox.Size = New System.Drawing.Size(74, 21)
        Me.ModuloTextBox.TabIndex = 22
        '
        'Button30
        '
        Me.Button30.BackColor = System.Drawing.SystemColors.Control
        Me.Button30.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button30.ForeColor = System.Drawing.Color.Black
        Me.Button30.Location = New System.Drawing.Point(694, 543)
        Me.Button30.Name = "Button30"
        Me.Button30.Size = New System.Drawing.Size(136, 36)
        Me.Button30.TabIndex = 17
        Me.Button30.Text = "&GUARDAR"
        Me.Button30.UseVisualStyleBackColor = False
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.BackColor = System.Drawing.Color.Black
        Me.Label62.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label62.ForeColor = System.Drawing.Color.White
        Me.Label62.Location = New System.Drawing.Point(57, 37)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(283, 20)
        Me.Label62.TabIndex = 15
        Me.Label62.Text = "Datos de la Referencia Santander"
        '
        'PanelDatosXml
        '
        Me.PanelDatosXml.BackColor = System.Drawing.Color.Transparent
        Me.PanelDatosXml.Controls.Add(Me.TxtCdClvAdic)
        Me.PanelDatosXml.Controls.Add(Me.TxtEdoClvAdic)
        Me.PanelDatosXml.Controls.Add(LblCdClvAdic)
        Me.PanelDatosXml.Controls.Add(LblEdoClvAdic)
        Me.PanelDatosXml.Controls.Add(Me.Label54)
        Me.PanelDatosXml.Controls.Add(Me.TextBoxfolio)
        Me.PanelDatosXml.Controls.Add(Me.TextBoxserie)
        Me.PanelDatosXml.Controls.Add(Label53)
        Me.PanelDatosXml.Controls.Add(Label52)
        Me.PanelDatosXml.Controls.Add(Me.Label49)
        Me.PanelDatosXml.Controls.Add(Me.Nom_ArchivoTextBox)
        Me.PanelDatosXml.Controls.Add(Me.Fecha_InicioDateTimePicker)
        Me.PanelDatosXml.Controls.Add(Fecha_FacturasLabel)
        Me.PanelDatosXml.Controls.Add(Me.Fecha_FacturasDateTimePicker)
        Me.PanelDatosXml.Controls.Add(Periodo_inicialLabel)
        Me.PanelDatosXml.Controls.Add(Me.Periodo_inicialTextBox)
        Me.PanelDatosXml.Controls.Add(Me.Periodo_finalTextBox)
        Me.PanelDatosXml.Controls.Add(Me.Numero_InicioTextBox)
        Me.PanelDatosXml.Controls.Add(Me.FormatoTextBox)
        Me.PanelDatosXml.Controls.Add(Me.RutaTextBox)
        Me.PanelDatosXml.Controls.Add(Me.Numero_LocalTextBox)
        Me.PanelDatosXml.Controls.Add(Me.Pais_LocalTextBox)
        Me.PanelDatosXml.Controls.Add(Me.Clave044TextBox)
        Me.PanelDatosXml.Controls.Add(Me.Clave045TextBox)
        Me.PanelDatosXml.Controls.Add(Me.Clave01800TextBox)
        Me.PanelDatosXml.Controls.Add(Me.Clave01900TextBox)
        Me.PanelDatosXml.Controls.Add(Label46)
        Me.PanelDatosXml.Controls.Add(Nom_ArchivoLabel)
        Me.PanelDatosXml.Controls.Add(Fecha_InicioLabel)
        Me.PanelDatosXml.Controls.Add(Numero_InicioLabel)
        Me.PanelDatosXml.Controls.Add(FormatoLabel)
        Me.PanelDatosXml.Controls.Add(RutaLabel)
        Me.PanelDatosXml.Controls.Add(Numero_LocalLabel)
        Me.PanelDatosXml.Controls.Add(Pais_LocalLabel)
        Me.PanelDatosXml.Controls.Add(Clave044Label)
        Me.PanelDatosXml.Controls.Add(Clave045Label)
        Me.PanelDatosXml.Controls.Add(Clave01800Label)
        Me.PanelDatosXml.Controls.Add(Clave01900Label)
        Me.PanelDatosXml.Controls.Add(Me.Button28)
        Me.PanelDatosXml.Controls.Add(Me.Label47)
        Me.PanelDatosXml.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelDatosXml.Location = New System.Drawing.Point(0, 0)
        Me.PanelDatosXml.Name = "PanelDatosXml"
        Me.PanelDatosXml.Size = New System.Drawing.Size(865, 592)
        Me.PanelDatosXml.TabIndex = 69
        '
        'TxtCdClvAdic
        '
        Me.TxtCdClvAdic.Location = New System.Drawing.Point(723, 348)
        Me.TxtCdClvAdic.MaxLength = 2
        Me.TxtCdClvAdic.Name = "TxtCdClvAdic"
        Me.TxtCdClvAdic.Size = New System.Drawing.Size(100, 21)
        Me.TxtCdClvAdic.TabIndex = 80
        '
        'TxtEdoClvAdic
        '
        Me.TxtEdoClvAdic.Location = New System.Drawing.Point(723, 321)
        Me.TxtEdoClvAdic.MaxLength = 2
        Me.TxtEdoClvAdic.Name = "TxtEdoClvAdic"
        Me.TxtEdoClvAdic.Size = New System.Drawing.Size(100, 21)
        Me.TxtEdoClvAdic.TabIndex = 79
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.BackColor = System.Drawing.Color.Black
        Me.Label54.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label54.ForeColor = System.Drawing.Color.White
        Me.Label54.Location = New System.Drawing.Point(501, 294)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(131, 20)
        Me.Label54.TabIndex = 76
        Me.Label54.Text = "Clave Adicional"
        '
        'TextBoxfolio
        '
        Me.TextBoxfolio.Location = New System.Drawing.Point(723, 255)
        Me.TextBoxfolio.MaxLength = 15
        Me.TextBoxfolio.Name = "TextBoxfolio"
        Me.TextBoxfolio.Size = New System.Drawing.Size(100, 21)
        Me.TextBoxfolio.TabIndex = 75
        '
        'TextBoxserie
        '
        Me.TextBoxserie.Location = New System.Drawing.Point(723, 229)
        Me.TextBoxserie.MaxLength = 15
        Me.TextBoxserie.Name = "TextBoxserie"
        Me.TextBoxserie.Size = New System.Drawing.Size(100, 21)
        Me.TextBoxserie.TabIndex = 74
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.BackColor = System.Drawing.Color.Black
        Me.Label49.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label49.ForeColor = System.Drawing.Color.White
        Me.Label49.Location = New System.Drawing.Point(498, 188)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(292, 20)
        Me.Label49.TabIndex = 71
        Me.Label49.Text = "Serie y Folio de Estados de Cuenta"
        '
        'Nom_ArchivoTextBox
        '
        Me.Nom_ArchivoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_XmlBindingSource, "Nom_Archivo", True))
        Me.Nom_ArchivoTextBox.Location = New System.Drawing.Point(244, 107)
        Me.Nom_ArchivoTextBox.MaxLength = 250
        Me.Nom_ArchivoTextBox.Name = "Nom_ArchivoTextBox"
        Me.Nom_ArchivoTextBox.Size = New System.Drawing.Size(200, 21)
        Me.Nom_ArchivoTextBox.TabIndex = 44
        '
        'CONSULTA_General_XmlBindingSource
        '
        Me.CONSULTA_General_XmlBindingSource.DataMember = "CONSULTA_General_Xml"
        Me.CONSULTA_General_XmlBindingSource.DataSource = Me.DataSetyahve
        '
        'Fecha_InicioDateTimePicker
        '
        Me.Fecha_InicioDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.CONSULTA_General_XmlBindingSource, "Fecha_Inicio", True))
        Me.Fecha_InicioDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Fecha_InicioDateTimePicker.Location = New System.Drawing.Point(244, 133)
        Me.Fecha_InicioDateTimePicker.Name = "Fecha_InicioDateTimePicker"
        Me.Fecha_InicioDateTimePicker.Size = New System.Drawing.Size(100, 21)
        Me.Fecha_InicioDateTimePicker.TabIndex = 46
        '
        'Fecha_FacturasDateTimePicker
        '
        Me.Fecha_FacturasDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.CONSULTA_General_XmlBindingSource, "Fecha_Facturas", True))
        Me.Fecha_FacturasDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Fecha_FacturasDateTimePicker.Location = New System.Drawing.Point(244, 159)
        Me.Fecha_FacturasDateTimePicker.Name = "Fecha_FacturasDateTimePicker"
        Me.Fecha_FacturasDateTimePicker.Size = New System.Drawing.Size(100, 21)
        Me.Fecha_FacturasDateTimePicker.TabIndex = 48
        '
        'Periodo_inicialTextBox
        '
        Me.Periodo_inicialTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_XmlBindingSource, "Periodo_inicial", True))
        Me.Periodo_inicialTextBox.Location = New System.Drawing.Point(244, 185)
        Me.Periodo_inicialTextBox.Name = "Periodo_inicialTextBox"
        Me.Periodo_inicialTextBox.Size = New System.Drawing.Size(50, 21)
        Me.Periodo_inicialTextBox.TabIndex = 50
        '
        'Periodo_finalTextBox
        '
        Me.Periodo_finalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_XmlBindingSource, "Periodo_final", True))
        Me.Periodo_finalTextBox.Location = New System.Drawing.Point(338, 185)
        Me.Periodo_finalTextBox.Name = "Periodo_finalTextBox"
        Me.Periodo_finalTextBox.Size = New System.Drawing.Size(49, 21)
        Me.Periodo_finalTextBox.TabIndex = 52
        '
        'Numero_InicioTextBox
        '
        Me.Numero_InicioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_XmlBindingSource, "Numero_Inicio", True))
        Me.Numero_InicioTextBox.Location = New System.Drawing.Point(244, 212)
        Me.Numero_InicioTextBox.Name = "Numero_InicioTextBox"
        Me.Numero_InicioTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Numero_InicioTextBox.TabIndex = 54
        '
        'FormatoTextBox
        '
        Me.FormatoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_XmlBindingSource, "Formato", True))
        Me.FormatoTextBox.Location = New System.Drawing.Point(244, 238)
        Me.FormatoTextBox.Name = "FormatoTextBox"
        Me.FormatoTextBox.Size = New System.Drawing.Size(100, 21)
        Me.FormatoTextBox.TabIndex = 56
        '
        'RutaTextBox
        '
        Me.RutaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_XmlBindingSource, "Ruta", True))
        Me.RutaTextBox.Location = New System.Drawing.Point(244, 264)
        Me.RutaTextBox.MaxLength = 250
        Me.RutaTextBox.Name = "RutaTextBox"
        Me.RutaTextBox.Size = New System.Drawing.Size(143, 21)
        Me.RutaTextBox.TabIndex = 58
        '
        'Numero_LocalTextBox
        '
        Me.Numero_LocalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_XmlBindingSource, "Numero_Local", True))
        Me.Numero_LocalTextBox.Location = New System.Drawing.Point(244, 290)
        Me.Numero_LocalTextBox.MaxLength = 50
        Me.Numero_LocalTextBox.Name = "Numero_LocalTextBox"
        Me.Numero_LocalTextBox.Size = New System.Drawing.Size(143, 21)
        Me.Numero_LocalTextBox.TabIndex = 60
        '
        'Pais_LocalTextBox
        '
        Me.Pais_LocalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_XmlBindingSource, "Pais_Local", True))
        Me.Pais_LocalTextBox.Location = New System.Drawing.Point(244, 316)
        Me.Pais_LocalTextBox.MaxLength = 50
        Me.Pais_LocalTextBox.Name = "Pais_LocalTextBox"
        Me.Pais_LocalTextBox.Size = New System.Drawing.Size(143, 21)
        Me.Pais_LocalTextBox.TabIndex = 62
        '
        'Clave044TextBox
        '
        Me.Clave044TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_XmlBindingSource, "Clave044", True))
        Me.Clave044TextBox.Location = New System.Drawing.Point(244, 342)
        Me.Clave044TextBox.MaxLength = 50
        Me.Clave044TextBox.Name = "Clave044TextBox"
        Me.Clave044TextBox.Size = New System.Drawing.Size(143, 21)
        Me.Clave044TextBox.TabIndex = 64
        '
        'Clave045TextBox
        '
        Me.Clave045TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_XmlBindingSource, "Clave045", True))
        Me.Clave045TextBox.Location = New System.Drawing.Point(244, 368)
        Me.Clave045TextBox.MaxLength = 50
        Me.Clave045TextBox.Name = "Clave045TextBox"
        Me.Clave045TextBox.Size = New System.Drawing.Size(143, 21)
        Me.Clave045TextBox.TabIndex = 66
        '
        'Clave01800TextBox
        '
        Me.Clave01800TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_XmlBindingSource, "Clave01800", True))
        Me.Clave01800TextBox.Location = New System.Drawing.Point(244, 394)
        Me.Clave01800TextBox.MaxLength = 50
        Me.Clave01800TextBox.Name = "Clave01800TextBox"
        Me.Clave01800TextBox.Size = New System.Drawing.Size(143, 21)
        Me.Clave01800TextBox.TabIndex = 68
        '
        'Clave01900TextBox
        '
        Me.Clave01900TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_XmlBindingSource, "Clave01900", True))
        Me.Clave01900TextBox.Location = New System.Drawing.Point(244, 420)
        Me.Clave01900TextBox.MaxLength = 50
        Me.Clave01900TextBox.Name = "Clave01900TextBox"
        Me.Clave01900TextBox.Size = New System.Drawing.Size(143, 21)
        Me.Clave01900TextBox.TabIndex = 70
        '
        'Button28
        '
        Me.Button28.BackColor = System.Drawing.SystemColors.Control
        Me.Button28.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button28.ForeColor = System.Drawing.Color.Black
        Me.Button28.Location = New System.Drawing.Point(694, 543)
        Me.Button28.Name = "Button28"
        Me.Button28.Size = New System.Drawing.Size(136, 36)
        Me.Button28.TabIndex = 17
        Me.Button28.Text = "&GUARDAR"
        Me.Button28.UseVisualStyleBackColor = False
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.BackColor = System.Drawing.Color.Black
        Me.Label47.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.ForeColor = System.Drawing.Color.White
        Me.Label47.Location = New System.Drawing.Point(43, 27)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(83, 20)
        Me.Label47.TabIndex = 15
        Me.Label47.Text = "Telefonía"
        '
        'PanelDatos
        '
        Me.PanelDatos.AutoScroll = True
        Me.PanelDatos.BackColor = System.Drawing.Color.Transparent
        Me.PanelDatos.Controls.Add(Me.Label56)
        Me.PanelDatos.Controls.Add(Me.TextBoxNumeroExt)
        Me.PanelDatos.Controls.Add(Me.Label55)
        Me.PanelDatos.Controls.Add(Me.TextBoxNumeroInt)
        Me.PanelDatos.Controls.Add(Me.Label14)
        Me.PanelDatos.Controls.Add(Me.Label2)
        Me.PanelDatos.Controls.Add(ColoniaLabel)
        Me.PanelDatos.Controls.Add(Me.TextBox1)
        Me.PanelDatos.Controls.Add(Me.CiudadTextBox)
        Me.PanelDatos.Controls.Add(Me.ColoniaTextBox)
        Me.PanelDatos.Controls.Add(Me.Button5)
        Me.PanelDatos.Controls.Add(CiudadLabel)
        Me.PanelDatos.Controls.Add(Me.Id_sucursalTextBox)
        Me.PanelDatos.Controls.Add(DireccionLabel)
        Me.PanelDatos.Controls.Add(Id_sucursalLabel)
        Me.PanelDatos.Controls.Add(Me.RfcTextBox)
        Me.PanelDatos.Controls.Add(TELefonosLabel)
        Me.PanelDatos.Controls.Add(Me.DireccionTextBox)
        Me.PanelDatos.Controls.Add(Me.NombreTextBox)
        Me.PanelDatos.Controls.Add(RfcLabel)
        Me.PanelDatos.Controls.Add(Me.TELefonosTextBox)
        Me.PanelDatos.Controls.Add(NombreLabel)
        Me.PanelDatos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelDatos.Location = New System.Drawing.Point(3, 3)
        Me.PanelDatos.Name = "PanelDatos"
        Me.PanelDatos.Size = New System.Drawing.Size(859, 566)
        Me.PanelDatos.TabIndex = 68
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Location = New System.Drawing.Point(162, 273)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(103, 15)
        Me.Label56.TabIndex = 23
        Me.Label56.Text = "Num. Exterior :"
        '
        'TextBoxNumeroExt
        '
        Me.TextBoxNumeroExt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxNumeroExt.Location = New System.Drawing.Point(266, 267)
        Me.TextBoxNumeroExt.MaxLength = 20
        Me.TextBoxNumeroExt.Name = "TextBoxNumeroExt"
        Me.TextBoxNumeroExt.Size = New System.Drawing.Size(204, 21)
        Me.TextBoxNumeroExt.TabIndex = 22
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Location = New System.Drawing.Point(161, 300)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(99, 15)
        Me.Label55.TabIndex = 21
        Me.Label55.Text = "Num. Interior :"
        '
        'TextBoxNumeroInt
        '
        Me.TextBoxNumeroInt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxNumeroInt.Location = New System.Drawing.Point(266, 294)
        Me.TextBoxNumeroInt.MaxLength = 20
        Me.TextBoxNumeroInt.Name = "TextBoxNumeroInt"
        Me.TextBoxNumeroInt.Size = New System.Drawing.Size(204, 21)
        Me.TextBoxNumeroInt.TabIndex = 20
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(164, 354)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(100, 15)
        Me.Label14.TabIndex = 19
        Me.Label14.Text = "Código Postal:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Black
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(26, 21)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(182, 20)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "Datos de la Empresa "
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConsultaCpGeneBindingSource, "Codigo_Postal", True))
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.TextBox1.Location = New System.Drawing.Point(266, 348)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(204, 21)
        Me.TextBox1.TabIndex = 18
        '
        'CiudadTextBox
        '
        Me.CiudadTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CiudadTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "ciudad", True))
        Me.CiudadTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CiudadTextBox.Location = New System.Drawing.Point(266, 375)
        Me.CiudadTextBox.MaxLength = 180
        Me.CiudadTextBox.Name = "CiudadTextBox"
        Me.CiudadTextBox.Size = New System.Drawing.Size(331, 21)
        Me.CiudadTextBox.TabIndex = 14
        '
        'ColoniaTextBox
        '
        Me.ColoniaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ColoniaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "colonia", True))
        Me.ColoniaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ColoniaTextBox.Location = New System.Drawing.Point(266, 321)
        Me.ColoniaTextBox.MaxLength = 150
        Me.ColoniaTextBox.Name = "ColoniaTextBox"
        Me.ColoniaTextBox.Size = New System.Drawing.Size(331, 21)
        Me.ColoniaTextBox.TabIndex = 13
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.SystemColors.Control
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(694, 543)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 36)
        Me.Button5.TabIndex = 17
        Me.Button5.Text = "&GUARDAR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Id_sucursalTextBox
        '
        Me.Id_sucursalTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Id_sucursalTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Id_sucursalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "id_sucursal", True))
        Me.Id_sucursalTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Id_sucursalTextBox.Location = New System.Drawing.Point(266, 151)
        Me.Id_sucursalTextBox.MaxLength = 2
        Me.Id_sucursalTextBox.Name = "Id_sucursalTextBox"
        Me.Id_sucursalTextBox.ReadOnly = True
        Me.Id_sucursalTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Id_sucursalTextBox.TabIndex = 10
        '
        'RfcTextBox
        '
        Me.RfcTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.RfcTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.RfcTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "rfc", True))
        Me.RfcTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RfcTextBox.Location = New System.Drawing.Point(266, 402)
        Me.RfcTextBox.MaxLength = 20
        Me.RfcTextBox.Name = "RfcTextBox"
        Me.RfcTextBox.Size = New System.Drawing.Size(204, 21)
        Me.RfcTextBox.TabIndex = 15
        '
        'DireccionTextBox
        '
        Me.DireccionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DireccionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "direccion", True))
        Me.DireccionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DireccionTextBox.Location = New System.Drawing.Point(266, 204)
        Me.DireccionTextBox.MaxLength = 250
        Me.DireccionTextBox.Multiline = True
        Me.DireccionTextBox.Name = "DireccionTextBox"
        Me.DireccionTextBox.Size = New System.Drawing.Size(488, 57)
        Me.DireccionTextBox.TabIndex = 12
        '
        'NombreTextBox
        '
        Me.NombreTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NombreTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "nombre", True))
        Me.NombreTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreTextBox.Location = New System.Drawing.Point(266, 178)
        Me.NombreTextBox.MaxLength = 150
        Me.NombreTextBox.Name = "NombreTextBox"
        Me.NombreTextBox.Size = New System.Drawing.Size(488, 21)
        Me.NombreTextBox.TabIndex = 11
        '
        'TELefonosTextBox
        '
        Me.TELefonosTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TELefonosTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "TELefonos", True))
        Me.TELefonosTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TELefonosTextBox.Location = New System.Drawing.Point(266, 429)
        Me.TELefonosTextBox.MaxLength = 50
        Me.TELefonosTextBox.Name = "TELefonosTextBox"
        Me.TELefonosTextBox.Size = New System.Drawing.Size(204, 21)
        Me.TELefonosTextBox.TabIndex = 16
        '
        'Panel8
        '
        Me.Panel8.BackColor = System.Drawing.Color.Transparent
        Me.Panel8.Controls.Add(Me.TreeView1)
        Me.Panel8.Controls.Add(Me.TextBox6)
        Me.Panel8.Controls.Add(Me.Button21)
        Me.Panel8.Controls.Add(Me.Button20)
        Me.Panel8.Controls.Add(Label39)
        Me.Panel8.Controls.Add(Me.TextBox5)
        Me.Panel8.Controls.Add(Label41)
        Me.Panel8.Controls.Add(Me.TextBox4)
        Me.Panel8.Controls.Add(Label38)
        Me.Panel8.Controls.Add(Me.TextBox3)
        Me.Panel8.Controls.Add(Me.CheckBox4)
        Me.Panel8.Controls.Add(Me.Button19)
        Me.Panel8.Controls.Add(Me.Label40)
        Me.Panel8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel8.Location = New System.Drawing.Point(0, 0)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(865, 592)
        Me.Panel8.TabIndex = 54
        '
        'TreeView1
        '
        Me.TreeView1.Location = New System.Drawing.Point(415, 291)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(312, 127)
        Me.TreeView1.TabIndex = 62
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(505, 332)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(100, 21)
        Me.TextBox6.TabIndex = 74
        '
        'Button21
        '
        Me.Button21.BackColor = System.Drawing.SystemColors.Control
        Me.Button21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button21.ForeColor = System.Drawing.Color.Black
        Me.Button21.Location = New System.Drawing.Point(212, 370)
        Me.Button21.Name = "Button21"
        Me.Button21.Size = New System.Drawing.Size(176, 26)
        Me.Button21.TabIndex = 64
        Me.Button21.Text = "&Quitar Ip de la Lista"
        Me.Button21.UseVisualStyleBackColor = False
        '
        'Button20
        '
        Me.Button20.BackColor = System.Drawing.SystemColors.Control
        Me.Button20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button20.ForeColor = System.Drawing.Color.Black
        Me.Button20.Location = New System.Drawing.Point(212, 332)
        Me.Button20.Name = "Button20"
        Me.Button20.Size = New System.Drawing.Size(174, 26)
        Me.Button20.TabIndex = 63
        Me.Button20.Text = "&Agregar Ip a la Lista"
        Me.Button20.UseVisualStyleBackColor = False
        '
        'TextBox5
        '
        Me.TextBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox5.Location = New System.Drawing.Point(210, 293)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox5.Size = New System.Drawing.Size(110, 21)
        Me.TextBox5.TabIndex = 61
        '
        'TextBox4
        '
        Me.TextBox4.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralAlertaBindingSource, "Monto", True))
        Me.TextBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox4.Location = New System.Drawing.Point(205, 120)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox4.Size = New System.Drawing.Size(110, 21)
        Me.TextBox4.TabIndex = 59
        '
        'TextBox3
        '
        Me.TextBox3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralAlertaBindingSource, "Mensaje", True))
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(205, 176)
        Me.TextBox3.Multiline = True
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox3.Size = New System.Drawing.Size(517, 88)
        Me.TextBox3.TabIndex = 55
        '
        'CheckBox4
        '
        Me.CheckBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox4.Location = New System.Drawing.Point(409, 110)
        Me.CheckBox4.Name = "CheckBox4"
        Me.CheckBox4.Size = New System.Drawing.Size(161, 24)
        Me.CheckBox4.TabIndex = 57
        Me.CheckBox4.Text = "Activar Alerta"
        '
        'Button19
        '
        Me.Button19.BackColor = System.Drawing.SystemColors.Control
        Me.Button19.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button19.ForeColor = System.Drawing.Color.Black
        Me.Button19.Location = New System.Drawing.Point(694, 543)
        Me.Button19.Name = "Button19"
        Me.Button19.Size = New System.Drawing.Size(136, 36)
        Me.Button19.TabIndex = 54
        Me.Button19.Text = "&GUARDAR"
        Me.Button19.UseVisualStyleBackColor = False
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.BackColor = System.Drawing.Color.Black
        Me.Label40.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.ForeColor = System.Drawing.Color.White
        Me.Label40.Location = New System.Drawing.Point(46, 26)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(66, 20)
        Me.Label40.TabIndex = 52
        Me.Label40.Text = "Alertas"
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.Transparent
        Me.Panel7.Controls.Add(MensajeLabel)
        Me.Panel7.Controls.Add(Me.MensajeTextBox)
        Me.Panel7.Controls.Add(ActivoLabel)
        Me.Panel7.Controls.Add(Me.ActivoCheckBox)
        Me.Panel7.Controls.Add(Me.Button16)
        Me.Panel7.Controls.Add(Me.Label37)
        Me.Panel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel7.Location = New System.Drawing.Point(0, 0)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(865, 592)
        Me.Panel7.TabIndex = 53
        '
        'MensajeTextBox
        '
        Me.MensajeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralMsjTicketsBindingSource, "Mensaje", True))
        Me.MensajeTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MensajeTextBox.Location = New System.Drawing.Point(218, 138)
        Me.MensajeTextBox.Multiline = True
        Me.MensajeTextBox.Name = "MensajeTextBox"
        Me.MensajeTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.MensajeTextBox.Size = New System.Drawing.Size(517, 153)
        Me.MensajeTextBox.TabIndex = 55
        '
        'ConGeneralMsjTicketsBindingSource
        '
        Me.ConGeneralMsjTicketsBindingSource.DataMember = "ConGeneralMsjTickets"
        Me.ConGeneralMsjTicketsBindingSource.DataSource = Me.DataSetEric2
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric2"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ActivoCheckBox
        '
        Me.ActivoCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.ConGeneralMsjTicketsBindingSource, "Activo", True))
        Me.ActivoCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ActivoCheckBox.Location = New System.Drawing.Point(263, 295)
        Me.ActivoCheckBox.Name = "ActivoCheckBox"
        Me.ActivoCheckBox.Size = New System.Drawing.Size(104, 24)
        Me.ActivoCheckBox.TabIndex = 57
        '
        'Button16
        '
        Me.Button16.BackColor = System.Drawing.SystemColors.Control
        Me.Button16.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button16.ForeColor = System.Drawing.Color.Black
        Me.Button16.Location = New System.Drawing.Point(694, 543)
        Me.Button16.Name = "Button16"
        Me.Button16.Size = New System.Drawing.Size(136, 36)
        Me.Button16.TabIndex = 54
        Me.Button16.Text = "&GUARDAR"
        Me.Button16.UseVisualStyleBackColor = False
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.BackColor = System.Drawing.Color.Black
        Me.Label37.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.ForeColor = System.Drawing.Color.White
        Me.Label37.Location = New System.Drawing.Point(55, 32)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(163, 20)
        Me.Label37.TabIndex = 52
        Me.Label37.Text = "Mensaje en Tickets"
        '
        'Panel9
        '
        Me.Panel9.BackColor = System.Drawing.Color.Transparent
        Me.Panel9.Controls.Add(CablemodemsLabel)
        Me.Panel9.Controls.Add(Me.CablemodemsCheckBox)
        Me.Panel9.Controls.Add(PregcablLabel)
        Me.Panel9.Controls.Add(Me.PregcablTextBox)
        Me.Panel9.Controls.Add(DecodersLabel)
        Me.Panel9.Controls.Add(Me.DecodersCheckBox)
        Me.Panel9.Controls.Add(PregdecoLabel)
        Me.Panel9.Controls.Add(Me.PregdecoTextBox)
        Me.Panel9.Controls.Add(Me.Button23)
        Me.Panel9.Controls.Add(Me.Label42)
        Me.Panel9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel9.Location = New System.Drawing.Point(0, 0)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(865, 592)
        Me.Panel9.TabIndex = 65
        '
        'CablemodemsCheckBox
        '
        Me.CablemodemsCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.Consulta_cobrodepositobuenoBindingSource, "cablemodems", True))
        Me.CablemodemsCheckBox.Location = New System.Drawing.Point(333, 123)
        Me.CablemodemsCheckBox.Name = "CablemodemsCheckBox"
        Me.CablemodemsCheckBox.Size = New System.Drawing.Size(23, 24)
        Me.CablemodemsCheckBox.TabIndex = 74
        '
        'Consulta_cobrodepositobuenoBindingSource
        '
        Me.Consulta_cobrodepositobuenoBindingSource.DataMember = "Consulta_cobrodepositobueno"
        Me.Consulta_cobrodepositobuenoBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'PregcablTextBox
        '
        Me.PregcablTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_cobrodepositobuenoBindingSource, "pregcabl", True))
        Me.PregcablTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PregcablTextBox.Location = New System.Drawing.Point(669, 158)
        Me.PregcablTextBox.Name = "PregcablTextBox"
        Me.PregcablTextBox.Size = New System.Drawing.Size(104, 21)
        Me.PregcablTextBox.TabIndex = 76
        '
        'DecodersCheckBox
        '
        Me.DecodersCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.Consulta_cobrodepositobuenoBindingSource, "decoders", True))
        Me.DecodersCheckBox.Location = New System.Drawing.Point(333, 159)
        Me.DecodersCheckBox.Name = "DecodersCheckBox"
        Me.DecodersCheckBox.Size = New System.Drawing.Size(18, 24)
        Me.DecodersCheckBox.TabIndex = 78
        '
        'PregdecoTextBox
        '
        Me.PregdecoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_cobrodepositobuenoBindingSource, "pregdeco", True))
        Me.PregdecoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PregdecoTextBox.Location = New System.Drawing.Point(387, 257)
        Me.PregdecoTextBox.Name = "PregdecoTextBox"
        Me.PregdecoTextBox.Size = New System.Drawing.Size(104, 21)
        Me.PregdecoTextBox.TabIndex = 80
        Me.PregdecoTextBox.Visible = False
        '
        'Button23
        '
        Me.Button23.BackColor = System.Drawing.SystemColors.Control
        Me.Button23.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button23.ForeColor = System.Drawing.Color.Black
        Me.Button23.Location = New System.Drawing.Point(694, 543)
        Me.Button23.Name = "Button23"
        Me.Button23.Size = New System.Drawing.Size(136, 36)
        Me.Button23.TabIndex = 72
        Me.Button23.Text = "&GUARDAR"
        Me.Button23.UseVisualStyleBackColor = False
        '
        'Label42
        '
        Me.Label42.BackColor = System.Drawing.Color.Black
        Me.Label42.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.ForeColor = System.Drawing.Color.White
        Me.Label42.Location = New System.Drawing.Point(51, 38)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(287, 20)
        Me.Label42.TabIndex = 66
        Me.Label42.Text = "Se Cobran Deposito de Aparatos:"
        '
        'Panel10
        '
        Me.Panel10.BackColor = System.Drawing.Color.Transparent
        Me.Panel10.Controls.Add(Me.Label45)
        Me.Panel10.Controls.Add(Me.Button26)
        Me.Panel10.Controls.Add(Me.Button25)
        Me.Panel10.Controls.Add(Me.Label44)
        Me.Panel10.Controls.Add(Me.Label43)
        Me.Panel10.Controls.Add(Me.TextBox8)
        Me.Panel10.Controls.Add(Me.NumericUpDown9)
        Me.Panel10.Controls.Add(Me.Consulta_Rel_PaquetesDigTelDataGridView)
        Me.Panel10.Controls.Add(Me.ConsecutivoTextBox)
        Me.Panel10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel10.Location = New System.Drawing.Point(0, 0)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(865, 592)
        Me.Panel10.TabIndex = 52
        '
        'Label45
        '
        Me.Label45.BackColor = System.Drawing.Color.Black
        Me.Label45.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.ForeColor = System.Drawing.Color.White
        Me.Label45.Location = New System.Drawing.Point(41, 34)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(339, 20)
        Me.Label45.TabIndex = 52
        Me.Label45.Text = "Precios Paquetes Teléfonia"
        '
        'Button26
        '
        Me.Button26.BackColor = System.Drawing.SystemColors.Control
        Me.Button26.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button26.ForeColor = System.Drawing.Color.Black
        Me.Button26.Location = New System.Drawing.Point(560, 233)
        Me.Button26.Name = "Button26"
        Me.Button26.Size = New System.Drawing.Size(124, 30)
        Me.Button26.TabIndex = 53
        Me.Button26.Text = "&BORRAR"
        Me.Button26.UseVisualStyleBackColor = False
        '
        'Button25
        '
        Me.Button25.BackColor = System.Drawing.SystemColors.Control
        Me.Button25.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button25.ForeColor = System.Drawing.Color.Black
        Me.Button25.Location = New System.Drawing.Point(560, 141)
        Me.Button25.Name = "Button25"
        Me.Button25.Size = New System.Drawing.Size(124, 30)
        Me.Button25.TabIndex = 52
        Me.Button25.Text = "&AGREGAR"
        Me.Button25.UseVisualStyleBackColor = False
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label44.Location = New System.Drawing.Point(329, 163)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(79, 16)
        Me.Label44.TabIndex = 4
        Me.Label44.Text = "Precio ($):"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.Location = New System.Drawing.Point(291, 134)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(114, 16)
        Me.Label43.TabIndex = 3
        Me.Label43.Text = "# De Paquetes:"
        '
        'TextBox8
        '
        Me.TextBox8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox8.Location = New System.Drawing.Point(424, 160)
        Me.TextBox8.MaxLength = 15
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New System.Drawing.Size(109, 22)
        Me.TextBox8.TabIndex = 2
        '
        'NumericUpDown9
        '
        Me.NumericUpDown9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown9.Location = New System.Drawing.Point(424, 133)
        Me.NumericUpDown9.Minimum = New Decimal(New Integer() {2, 0, 0, 0})
        Me.NumericUpDown9.Name = "NumericUpDown9"
        Me.NumericUpDown9.Size = New System.Drawing.Size(59, 22)
        Me.NumericUpDown9.TabIndex = 1
        Me.NumericUpDown9.Value = New Decimal(New Integer() {2, 0, 0, 0})
        '
        'Consulta_Rel_PaquetesDigTelDataGridView
        '
        Me.Consulta_Rel_PaquetesDigTelDataGridView.AllowUserToAddRows = False
        Me.Consulta_Rel_PaquetesDigTelDataGridView.AllowUserToDeleteRows = False
        Me.Consulta_Rel_PaquetesDigTelDataGridView.AllowUserToOrderColumns = True
        Me.Consulta_Rel_PaquetesDigTelDataGridView.AutoGenerateColumns = False
        Me.Consulta_Rel_PaquetesDigTelDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.DarkOrange
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Consulta_Rel_PaquetesDigTelDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.Consulta_Rel_PaquetesDigTelDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3})
        Me.Consulta_Rel_PaquetesDigTelDataGridView.DataSource = Me.Consulta_Rel_PaquetesDigTelBindingSource
        Me.Consulta_Rel_PaquetesDigTelDataGridView.Location = New System.Drawing.Point(300, 233)
        Me.Consulta_Rel_PaquetesDigTelDataGridView.Name = "Consulta_Rel_PaquetesDigTelDataGridView"
        Me.Consulta_Rel_PaquetesDigTelDataGridView.ReadOnly = True
        Me.Consulta_Rel_PaquetesDigTelDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Consulta_Rel_PaquetesDigTelDataGridView.Size = New System.Drawing.Size(254, 220)
        Me.Consulta_Rel_PaquetesDigTelDataGridView.TabIndex = 0
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "consecutivo"
        Me.DataGridViewTextBoxColumn1.HeaderText = "consecutivo"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "nopaquetes"
        Me.DataGridViewTextBoxColumn2.HeaderText = "# Paquetes"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "precio"
        DataGridViewCellStyle2.Format = "C2"
        DataGridViewCellStyle2.NullValue = "0"
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridViewTextBoxColumn3.HeaderText = "Precio"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'Consulta_Rel_PaquetesDigTelBindingSource
        '
        Me.Consulta_Rel_PaquetesDigTelBindingSource.DataMember = "Consulta_Rel_PaquetesDigTel"
        Me.Consulta_Rel_PaquetesDigTelBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'ConsecutivoTextBox
        '
        Me.ConsecutivoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_Rel_PaquetesDigTelBindingSource, "consecutivo", True))
        Me.ConsecutivoTextBox.Location = New System.Drawing.Point(388, 372)
        Me.ConsecutivoTextBox.Name = "ConsecutivoTextBox"
        Me.ConsecutivoTextBox.Size = New System.Drawing.Size(100, 21)
        Me.ConsecutivoTextBox.TabIndex = 54
        '
        'ConGeneralMsjTicketsTableAdapter
        '
        Me.ConGeneralMsjTicketsTableAdapter.ClearBeforeFill = True
        '
        'NueGeneralMsjTicketsBindingSource
        '
        Me.NueGeneralMsjTicketsBindingSource.DataMember = "NueGeneralMsjTickets"
        Me.NueGeneralMsjTicketsBindingSource.DataSource = Me.DataSetEric2
        '
        'NueGeneralMsjTicketsTableAdapter
        '
        Me.NueGeneralMsjTicketsTableAdapter.ClearBeforeFill = True
        '
        'ConGeneralAlertaTableAdapter
        '
        Me.ConGeneralAlertaTableAdapter.ClearBeforeFill = True
        '
        'NueGeneralAlertaBindingSource
        '
        Me.NueGeneralAlertaBindingSource.DataMember = "NueGeneralAlerta"
        Me.NueGeneralAlertaBindingSource.DataSource = Me.DataSetLidia2
        '
        'NueGeneralAlertaTableAdapter
        '
        Me.NueGeneralAlertaTableAdapter.ClearBeforeFill = True
        '
        'Dame_Direcciones_IpBindingSource
        '
        Me.Dame_Direcciones_IpBindingSource.DataMember = "Dame_Direcciones_Ip"
        Me.Dame_Direcciones_IpBindingSource.DataSource = Me.DataSetLidia2
        '
        'Dame_Direcciones_IpTableAdapter
        '
        Me.Dame_Direcciones_IpTableAdapter.ClearBeforeFill = True
        '
        'Consulta_cobrodepositoTableAdapter
        '
        Me.Consulta_cobrodepositoTableAdapter.ClearBeforeFill = True
        '
        'Consulta_cobrodepositobuenoTableAdapter
        '
        Me.Consulta_cobrodepositobuenoTableAdapter.ClearBeforeFill = True
        '
        'Consulta_Rel_PaquetesDigTelTableAdapter
        '
        Me.Consulta_Rel_PaquetesDigTelTableAdapter.ClearBeforeFill = True
        '
        'CONSULTA_General_XmlTableAdapter
        '
        Me.CONSULTA_General_XmlTableAdapter.ClearBeforeFill = True
        '
        'CONSULTA_General_SantanderTableAdapter
        '
        Me.CONSULTA_General_SantanderTableAdapter.ClearBeforeFill = True
        '
        'CONSULTA_General_HsbcTableAdapter
        '
        Me.CONSULTA_General_HsbcTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ciudadTableAdapter1
        '
        Me.Muestra_ciudadTableAdapter1.ClearBeforeFill = True
        '
        'TabPage14
        '
        Me.TabPage14.Controls.Add(Me.Panel12)
        Me.TabPage14.Location = New System.Drawing.Point(4, 64)
        Me.TabPage14.Name = "TabPage14"
        Me.TabPage14.Size = New System.Drawing.Size(865, 592)
        Me.TabPage14.TabIndex = 13
        Me.TabPage14.Text = "Referencia HSBC"
        Me.TabPage14.UseVisualStyleBackColor = True
        '
        'TabPage13
        '
        Me.TabPage13.Controls.Add(Me.Panel11)
        Me.TabPage13.Location = New System.Drawing.Point(4, 64)
        Me.TabPage13.Name = "TabPage13"
        Me.TabPage13.Size = New System.Drawing.Size(865, 592)
        Me.TabPage13.TabIndex = 12
        Me.TabPage13.Text = "Referencia Santander"
        Me.TabPage13.UseVisualStyleBackColor = True
        '
        'TabPage12
        '
        Me.TabPage12.Controls.Add(Me.Panel2)
        Me.TabPage12.Location = New System.Drawing.Point(4, 64)
        Me.TabPage12.Name = "TabPage12"
        Me.TabPage12.Size = New System.Drawing.Size(865, 592)
        Me.TabPage12.TabIndex = 11
        Me.TabPage12.Text = "Impresión"
        Me.TabPage12.UseVisualStyleBackColor = True
        '
        'TabPage11
        '
        Me.TabPage11.Controls.Add(Me.Panel10)
        Me.TabPage11.Location = New System.Drawing.Point(4, 64)
        Me.TabPage11.Name = "TabPage11"
        Me.TabPage11.Size = New System.Drawing.Size(865, 592)
        Me.TabPage11.TabIndex = 10
        Me.TabPage11.Text = "Paquetes Digitales Telefonía"
        Me.TabPage11.UseVisualStyleBackColor = True
        '
        'TabPage10
        '
        Me.TabPage10.Controls.Add(Me.Panel9)
        Me.TabPage10.Location = New System.Drawing.Point(4, 64)
        Me.TabPage10.Name = "TabPage10"
        Me.TabPage10.Size = New System.Drawing.Size(865, 592)
        Me.TabPage10.TabIndex = 9
        Me.TabPage10.Text = "Depósitos"
        Me.TabPage10.UseVisualStyleBackColor = True
        '
        'TabPage9
        '
        Me.TabPage9.Controls.Add(Me.PanelDatosXml)
        Me.TabPage9.Location = New System.Drawing.Point(4, 64)
        Me.TabPage9.Name = "TabPage9"
        Me.TabPage9.Size = New System.Drawing.Size(865, 592)
        Me.TabPage9.TabIndex = 8
        Me.TabPage9.Text = "Telefonía"
        Me.TabPage9.UseVisualStyleBackColor = True
        '
        'TabPage8
        '
        Me.TabPage8.Controls.Add(Me.Panel8)
        Me.TabPage8.Location = New System.Drawing.Point(4, 64)
        Me.TabPage8.Name = "TabPage8"
        Me.TabPage8.Size = New System.Drawing.Size(865, 592)
        Me.TabPage8.TabIndex = 7
        Me.TabPage8.Text = "Alertas"
        Me.TabPage8.UseVisualStyleBackColor = True
        '
        'TabPage7
        '
        Me.TabPage7.Controls.Add(Me.Panel7)
        Me.TabPage7.Location = New System.Drawing.Point(4, 64)
        Me.TabPage7.Name = "TabPage7"
        Me.TabPage7.Size = New System.Drawing.Size(865, 592)
        Me.TabPage7.TabIndex = 6
        Me.TabPage7.Text = "Mensaje en Tickets"
        Me.TabPage7.UseVisualStyleBackColor = True
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.Panel6)
        Me.TabPage6.Location = New System.Drawing.Point(4, 64)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Size = New System.Drawing.Size(865, 592)
        Me.TabPage6.TabIndex = 5
        Me.TabPage6.Text = "Facturas Globales"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.Panel4)
        Me.TabPage5.Location = New System.Drawing.Point(4, 64)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(865, 592)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "Correo"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.Panel3)
        Me.TabPage4.Location = New System.Drawing.Point(4, 64)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(865, 592)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Cobro de Adeudo"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.Panel1)
        Me.TabPage3.Location = New System.Drawing.Point(4, 64)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(865, 592)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Periodo de Corte"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.PanelBasico)
        Me.TabPage2.Location = New System.Drawing.Point(4, 64)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(865, 592)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Servicios de Televisión"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage1.Controls.Add(Me.PanelDatos)
        Me.TabPage1.Location = New System.Drawing.Point(4, 84)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(865, 572)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Datos de la Empresa"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'tbcAccesoriosInstalacion
        '
        Me.tbcAccesoriosInstalacion.Controls.Add(Me.TabPage1)
        Me.tbcAccesoriosInstalacion.Controls.Add(Me.TabPage2)
        Me.tbcAccesoriosInstalacion.Controls.Add(Me.TabPage3)
        Me.tbcAccesoriosInstalacion.Controls.Add(Me.TabPage4)
        Me.tbcAccesoriosInstalacion.Controls.Add(Me.TabPage5)
        Me.tbcAccesoriosInstalacion.Controls.Add(Me.TabPage6)
        Me.tbcAccesoriosInstalacion.Controls.Add(Me.TabPage7)
        Me.tbcAccesoriosInstalacion.Controls.Add(Me.TabPage8)
        Me.tbcAccesoriosInstalacion.Controls.Add(Me.TabPage9)
        Me.tbcAccesoriosInstalacion.Controls.Add(Me.TabPage10)
        Me.tbcAccesoriosInstalacion.Controls.Add(Me.TabPage11)
        Me.tbcAccesoriosInstalacion.Controls.Add(Me.TabPage12)
        Me.tbcAccesoriosInstalacion.Controls.Add(Me.TabPage13)
        Me.tbcAccesoriosInstalacion.Controls.Add(Me.TabPage14)
        Me.tbcAccesoriosInstalacion.Controls.Add(Me.TabPage15)
        Me.tbcAccesoriosInstalacion.Controls.Add(Me.TabPage16)
        Me.tbcAccesoriosInstalacion.Controls.Add(Me.TabPage17)
        Me.tbcAccesoriosInstalacion.Controls.Add(Me.tbEstadoCuenta)
        Me.tbcAccesoriosInstalacion.Controls.Add(Me.tbpAccesoriosInstalacion)
        Me.tbcAccesoriosInstalacion.Controls.Add(Me.tpCobroMaterial)
        Me.tbcAccesoriosInstalacion.Controls.Add(Me.Mizar)
        Me.tbcAccesoriosInstalacion.Controls.Add(Me.TabOxxo)
        Me.tbcAccesoriosInstalacion.Controls.Add(Me.TabPageCorteSistema)
        Me.tbcAccesoriosInstalacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbcAccesoriosInstalacion.Location = New System.Drawing.Point(12, 12)
        Me.tbcAccesoriosInstalacion.Multiline = True
        Me.tbcAccesoriosInstalacion.Name = "tbcAccesoriosInstalacion"
        Me.tbcAccesoriosInstalacion.SelectedIndex = 0
        Me.tbcAccesoriosInstalacion.Size = New System.Drawing.Size(873, 660)
        Me.tbcAccesoriosInstalacion.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight
        Me.tbcAccesoriosInstalacion.TabIndex = 72
        '
        'TabPage15
        '
        Me.TabPage15.AutoScroll = True
        Me.TabPage15.Controls.Add(Me.PnQuejas)
        Me.TabPage15.Location = New System.Drawing.Point(4, 64)
        Me.TabPage15.Name = "TabPage15"
        Me.TabPage15.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage15.Size = New System.Drawing.Size(865, 592)
        Me.TabPage15.TabIndex = 14
        Me.TabPage15.Text = "Órdenes y Quejas"
        Me.TabPage15.UseVisualStyleBackColor = True
        '
        'PnQuejas
        '
        Me.PnQuejas.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PnQuejas.Controls.Add(Me.Label59)
        Me.PnQuejas.Controls.Add(Me.Button10)
        Me.PnQuejas.Controls.Add(Me.Button9)
        Me.PnQuejas.Controls.Add(Me.TreeViewOrdenes)
        Me.PnQuejas.Controls.Add(Me.LblNotas)
        Me.PnQuejas.Controls.Add(Me.LblFamilia)
        Me.PnQuejas.Controls.Add(Me.LblTecnico)
        Me.PnQuejas.Controls.Add(Me.Label57)
        Me.PnQuejas.Controls.Add(Me.TreViewTecnicos)
        Me.PnQuejas.Controls.Add(Me.CmbTecnicos)
        Me.PnQuejas.Controls.Add(Me.CmBDepto)
        Me.PnQuejas.Controls.Add(Me.Button4)
        Me.PnQuejas.Controls.Add(Me.Button2)
        Me.PnQuejas.Location = New System.Drawing.Point(6, 6)
        Me.PnQuejas.Name = "PnQuejas"
        Me.PnQuejas.Size = New System.Drawing.Size(850, 581)
        Me.PnQuejas.TabIndex = 0
        '
        'Label59
        '
        Me.Label59.BackColor = System.Drawing.Color.Black
        Me.Label59.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label59.ForeColor = System.Drawing.Color.White
        Me.Label59.Location = New System.Drawing.Point(25, 120)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(304, 23)
        Me.Label59.TabIndex = 66
        Me.Label59.Text = "Técnicos a mostrar en Órdenes"
        Me.Label59.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(335, 184)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(76, 32)
        Me.Button10.TabIndex = 65
        Me.Button10.Text = "&Eliminar"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(335, 146)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(76, 32)
        Me.Button9.TabIndex = 64
        Me.Button9.Text = "&Agregar"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'TreeViewOrdenes
        '
        Me.TreeViewOrdenes.Location = New System.Drawing.Point(25, 146)
        Me.TreeViewOrdenes.Name = "TreeViewOrdenes"
        Me.TreeViewOrdenes.Size = New System.Drawing.Size(304, 365)
        Me.TreeViewOrdenes.TabIndex = 63
        '
        'LblNotas
        '
        Me.LblNotas.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblNotas.ForeColor = System.Drawing.Color.Red
        Me.LblNotas.Location = New System.Drawing.Point(300, 525)
        Me.LblNotas.Name = "LblNotas"
        Me.LblNotas.Size = New System.Drawing.Size(296, 50)
        Me.LblNotas.TabIndex = 62
        Me.LblNotas.Text = "Nota: Si requiere agregar TODOS los Técnicos de un Departamento, NO seleccione ni" & _
    "ngún Técnico."
        Me.LblNotas.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblFamilia
        '
        Me.LblFamilia.AutoSize = True
        Me.LblFamilia.Location = New System.Drawing.Point(196, 37)
        Me.LblFamilia.Name = "LblFamilia"
        Me.LblFamilia.Size = New System.Drawing.Size(98, 15)
        Me.LblFamilia.TabIndex = 61
        Me.LblFamilia.Text = "Departamento"
        '
        'LblTecnico
        '
        Me.LblTecnico.AutoSize = True
        Me.LblTecnico.Location = New System.Drawing.Point(237, 66)
        Me.LblTecnico.Name = "LblTecnico"
        Me.LblTecnico.Size = New System.Drawing.Size(57, 15)
        Me.LblTecnico.TabIndex = 60
        Me.LblTecnico.Text = "Técnico"
        '
        'Label57
        '
        Me.Label57.BackColor = System.Drawing.Color.Black
        Me.Label57.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label57.ForeColor = System.Drawing.Color.White
        Me.Label57.Location = New System.Drawing.Point(449, 120)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(304, 23)
        Me.Label57.TabIndex = 59
        Me.Label57.Text = "Técnicos a mostrar en Quejas"
        Me.Label57.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TreViewTecnicos
        '
        Me.TreViewTecnicos.Location = New System.Drawing.Point(449, 146)
        Me.TreViewTecnicos.Name = "TreViewTecnicos"
        Me.TreViewTecnicos.Size = New System.Drawing.Size(304, 365)
        Me.TreViewTecnicos.TabIndex = 58
        '
        'CmbTecnicos
        '
        Me.CmbTecnicos.DataSource = Me.MuestraTecnicosByFamiliBindingSource
        Me.CmbTecnicos.DisplayMember = "Tecnico"
        Me.CmbTecnicos.FormattingEnabled = True
        Me.CmbTecnicos.Location = New System.Drawing.Point(300, 58)
        Me.CmbTecnicos.Name = "CmbTecnicos"
        Me.CmbTecnicos.Size = New System.Drawing.Size(296, 23)
        Me.CmbTecnicos.TabIndex = 57
        Me.CmbTecnicos.ValueMember = "Clv_tecnico"
        '
        'MuestraTecnicosByFamiliBindingSource
        '
        Me.MuestraTecnicosByFamiliBindingSource.DataMember = "Muestra_TecnicosByFamili"
        Me.MuestraTecnicosByFamiliBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'CmBDepto
        '
        Me.CmBDepto.DataSource = Me.MuestratecnicosDepartamentosAlmacenBindingSource
        Me.CmBDepto.DisplayMember = "Puesto"
        Me.CmBDepto.FormattingEnabled = True
        Me.CmBDepto.Location = New System.Drawing.Point(300, 29)
        Me.CmBDepto.Name = "CmBDepto"
        Me.CmBDepto.Size = New System.Drawing.Size(296, 23)
        Me.CmBDepto.TabIndex = 56
        Me.CmBDepto.ValueMember = "Clv_Puesto"
        '
        'MuestratecnicosDepartamentosAlmacenBindingSource
        '
        Me.MuestratecnicosDepartamentosAlmacenBindingSource.DataMember = "Muestra_tecnicosDepartamentos_Almacen"
        Me.MuestratecnicosDepartamentosAlmacenBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(759, 184)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(76, 32)
        Me.Button4.TabIndex = 55
        Me.Button4.Text = "&Eliminar"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(759, 146)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(76, 32)
        Me.Button2.TabIndex = 54
        Me.Button2.Text = "&Agregar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'TabPage16
        '
        Me.TabPage16.Controls.Add(Me.LabelNota)
        Me.TabPage16.Controls.Add(Me.Label58)
        Me.TabPage16.Controls.Add(Me.Button7)
        Me.TabPage16.Controls.Add(Me.DataGridView1)
        Me.TabPage16.Location = New System.Drawing.Point(4, 64)
        Me.TabPage16.Name = "TabPage16"
        Me.TabPage16.Size = New System.Drawing.Size(865, 592)
        Me.TabPage16.TabIndex = 15
        Me.TabPage16.Text = "Cartera Ejecutiva"
        Me.TabPage16.UseVisualStyleBackColor = True
        '
        'LabelNota
        '
        Me.LabelNota.ForeColor = System.Drawing.Color.Red
        Me.LabelNota.Location = New System.Drawing.Point(226, 399)
        Me.LabelNota.Name = "LabelNota"
        Me.LabelNota.Size = New System.Drawing.Size(452, 22)
        Me.LabelNota.TabIndex = 55
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.BackColor = System.Drawing.Color.Black
        Me.Label58.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label58.ForeColor = System.Drawing.Color.White
        Me.Label58.Location = New System.Drawing.Point(28, 25)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(260, 20)
        Me.Label58.TabIndex = 54
        Me.Label58.Text = "Generales de Cartera Ejecutiva"
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.SystemColors.Control
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.ForeColor = System.Drawing.Color.Black
        Me.Button7.Location = New System.Drawing.Point(694, 543)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(136, 36)
        Me.Button7.TabIndex = 18
        Me.Button7.Text = "&GUARDAR"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_Grupo, Me.Grupo, Me.Porcentaje, Me.Activo})
        Me.DataGridView1.Location = New System.Drawing.Point(226, 119)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(452, 268)
        Me.DataGridView1.TabIndex = 0
        '
        'Clv_Grupo
        '
        Me.Clv_Grupo.DataPropertyName = "Clv_Grupo"
        Me.Clv_Grupo.HeaderText = "Clv_Grupo"
        Me.Clv_Grupo.Name = "Clv_Grupo"
        Me.Clv_Grupo.Visible = False
        '
        'Grupo
        '
        Me.Grupo.DataPropertyName = "Grupo"
        Me.Grupo.HeaderText = "Grupo"
        Me.Grupo.Name = "Grupo"
        Me.Grupo.Width = 150
        '
        'Porcentaje
        '
        Me.Porcentaje.DataPropertyName = "Porcentaje"
        Me.Porcentaje.HeaderText = "Porcentaje (%)"
        Me.Porcentaje.Name = "Porcentaje"
        Me.Porcentaje.Width = 150
        '
        'Activo
        '
        Me.Activo.DataPropertyName = "Activo"
        Me.Activo.HeaderText = "Activo"
        Me.Activo.Name = "Activo"
        '
        'TabPage17
        '
        Me.TabPage17.Controls.Add(Me.TextIEPS)
        Me.TabPage17.Controls.Add(Me.TextIva)
        Me.TabPage17.Controls.Add(Me.Label68)
        Me.TabPage17.Controls.Add(Me.LblPorIeps)
        Me.TabPage17.Controls.Add(Me.NumericUpDownIeps)
        Me.TabPage17.Controls.Add(Me.NumericUpDownIva)
        Me.TabPage17.Controls.Add(Me.Button14)
        Me.TabPage17.Controls.Add(Me.ChkCalculo1)
        Me.TabPage17.Controls.Add(Me.LabelIEPS)
        Me.TabPage17.Controls.Add(Me.CtaIepsText)
        Me.TabPage17.Controls.Add(Me.LabelCtaIEPS)
        Me.TabPage17.Controls.Add(Me.CheckIEPS)
        Me.TabPage17.Controls.Add(Label69)
        Me.TabPage17.Controls.Add(Me.Label67)
        Me.TabPage17.Location = New System.Drawing.Point(4, 64)
        Me.TabPage17.Name = "TabPage17"
        Me.TabPage17.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage17.Size = New System.Drawing.Size(865, 592)
        Me.TabPage17.TabIndex = 16
        Me.TabPage17.Text = "Impuestos"
        Me.TabPage17.UseVisualStyleBackColor = True
        '
        'TextIEPS
        '
        Me.TextIEPS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextIEPS.Enabled = False
        Me.TextIEPS.Location = New System.Drawing.Point(331, 265)
        Me.TextIEPS.Name = "TextIEPS"
        Me.TextIEPS.Size = New System.Drawing.Size(47, 21)
        Me.TextIEPS.TabIndex = 93
        Me.TextIEPS.Visible = False
        '
        'TextIva
        '
        Me.TextIva.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextIva.Enabled = False
        Me.TextIva.Location = New System.Drawing.Point(331, 137)
        Me.TextIva.Name = "TextIva"
        Me.TextIva.Size = New System.Drawing.Size(47, 21)
        Me.TextIva.TabIndex = 92
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.Location = New System.Drawing.Point(306, 139)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(19, 15)
        Me.Label68.TabIndex = 91
        Me.Label68.Text = "%"
        '
        'LblPorIeps
        '
        Me.LblPorIeps.AutoSize = True
        Me.LblPorIeps.Location = New System.Drawing.Point(306, 267)
        Me.LblPorIeps.Name = "LblPorIeps"
        Me.LblPorIeps.Size = New System.Drawing.Size(19, 15)
        Me.LblPorIeps.TabIndex = 90
        Me.LblPorIeps.Text = "%"
        Me.LblPorIeps.Visible = False
        '
        'NumericUpDownIeps
        '
        Me.NumericUpDownIeps.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NumericUpDownIeps.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDownIeps.Location = New System.Drawing.Point(254, 265)
        Me.NumericUpDownIeps.Name = "NumericUpDownIeps"
        Me.NumericUpDownIeps.Size = New System.Drawing.Size(46, 22)
        Me.NumericUpDownIeps.TabIndex = 89
        Me.NumericUpDownIeps.Visible = False
        '
        'NumericUpDownIva
        '
        Me.NumericUpDownIva.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NumericUpDownIva.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDownIva.Location = New System.Drawing.Point(254, 137)
        Me.NumericUpDownIva.Name = "NumericUpDownIva"
        Me.NumericUpDownIva.Size = New System.Drawing.Size(46, 22)
        Me.NumericUpDownIva.TabIndex = 88
        '
        'Button14
        '
        Me.Button14.BackColor = System.Drawing.SystemColors.Control
        Me.Button14.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button14.ForeColor = System.Drawing.Color.Black
        Me.Button14.Location = New System.Drawing.Point(376, 418)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(136, 36)
        Me.Button14.TabIndex = 87
        Me.Button14.Text = "&GUARDAR"
        Me.Button14.UseVisualStyleBackColor = False
        '
        'ChkCalculo1
        '
        Me.ChkCalculo1.AutoSize = True
        Me.ChkCalculo1.Location = New System.Drawing.Point(254, 220)
        Me.ChkCalculo1.Name = "ChkCalculo1"
        Me.ChkCalculo1.Size = New System.Drawing.Size(205, 19)
        Me.ChkCalculo1.TabIndex = 86
        Me.ChkCalculo1.Text = "Calcular Iva Incluyendo Ieps"
        Me.ChkCalculo1.UseVisualStyleBackColor = True
        Me.ChkCalculo1.Visible = False
        '
        'LabelIEPS
        '
        Me.LabelIEPS.AutoSize = True
        Me.LabelIEPS.Location = New System.Drawing.Point(202, 267)
        Me.LabelIEPS.Name = "LabelIEPS"
        Me.LabelIEPS.Size = New System.Drawing.Size(46, 15)
        Me.LabelIEPS.TabIndex = 85
        Me.LabelIEPS.Text = "IEPS :"
        '
        'CtaIepsText
        '
        Me.CtaIepsText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CtaIepsText.Location = New System.Drawing.Point(254, 309)
        Me.CtaIepsText.Name = "CtaIepsText"
        Me.CtaIepsText.Size = New System.Drawing.Size(136, 21)
        Me.CtaIepsText.TabIndex = 84
        Me.CtaIepsText.Visible = False
        '
        'LabelCtaIEPS
        '
        Me.LabelCtaIEPS.AutoSize = True
        Me.LabelCtaIEPS.Location = New System.Drawing.Point(173, 311)
        Me.LabelCtaIEPS.Name = "LabelCtaIEPS"
        Me.LabelCtaIEPS.Size = New System.Drawing.Size(75, 15)
        Me.LabelCtaIEPS.TabIndex = 83
        Me.LabelCtaIEPS.Text = "Cta. IEPS :"
        Me.LabelCtaIEPS.Visible = False
        '
        'CheckIEPS
        '
        Me.CheckIEPS.AutoSize = True
        Me.CheckIEPS.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckIEPS.ForeColor = System.Drawing.Color.Red
        Me.CheckIEPS.Location = New System.Drawing.Point(254, 184)
        Me.CheckIEPS.Name = "CheckIEPS"
        Me.CheckIEPS.Size = New System.Drawing.Size(124, 20)
        Me.CheckIEPS.TabIndex = 82
        Me.CheckIEPS.Text = "Habilitar IEPS"
        Me.CheckIEPS.UseVisualStyleBackColor = True
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.BackColor = System.Drawing.Color.Black
        Me.Label67.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label67.ForeColor = System.Drawing.SystemColors.Window
        Me.Label67.Location = New System.Drawing.Point(133, 79)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(115, 18)
        Me.Label67.TabIndex = 80
        Me.Label67.Text = "IMPUESTOS: "
        '
        'tbEstadoCuenta
        '
        Me.tbEstadoCuenta.Controls.Add(Me.Label76)
        Me.tbEstadoCuenta.Controls.Add(Me.tbConvenioBancomer)
        Me.tbEstadoCuenta.Controls.Add(Me.Label75)
        Me.tbEstadoCuenta.Controls.Add(Me.Label74)
        Me.tbEstadoCuenta.Controls.Add(Me.tbPrefijoOxxo)
        Me.tbEstadoCuenta.Controls.Add(Me.Label73)
        Me.tbEstadoCuenta.Controls.Add(Me.tbLetraInicialBancomer)
        Me.tbEstadoCuenta.Controls.Add(Me.bnGenerarEstado)
        Me.tbEstadoCuenta.Controls.Add(Me.bnEstadoCuentaGuardar)
        Me.tbEstadoCuenta.Controls.Add(Me.Label64)
        Me.tbEstadoCuenta.Controls.Add(Me.Label63)
        Me.tbEstadoCuenta.Controls.Add(Me.Label72)
        Me.tbEstadoCuenta.Controls.Add(Me.Label65)
        Me.tbEstadoCuenta.Controls.Add(Me.Label66)
        Me.tbEstadoCuenta.Controls.Add(Me.Label70)
        Me.tbEstadoCuenta.Controls.Add(Me.Label71)
        Me.tbEstadoCuenta.Controls.Add(Me.Label61)
        Me.tbEstadoCuenta.Controls.Add(Me.tbDiaGeneracion)
        Me.tbEstadoCuenta.Controls.Add(Me.tbDiaEnvio)
        Me.tbEstadoCuenta.Controls.Add(Me.tbDiaFin)
        Me.tbEstadoCuenta.Controls.Add(Me.tbDiaIni)
        Me.tbEstadoCuenta.Controls.Add(Me.Label60)
        Me.tbEstadoCuenta.Controls.Add(Me.cbPeriodo)
        Me.tbEstadoCuenta.Location = New System.Drawing.Point(4, 64)
        Me.tbEstadoCuenta.Name = "tbEstadoCuenta"
        Me.tbEstadoCuenta.Size = New System.Drawing.Size(865, 592)
        Me.tbEstadoCuenta.TabIndex = 17
        Me.tbEstadoCuenta.Text = "Estado de Cuenta"
        Me.tbEstadoCuenta.UseVisualStyleBackColor = True
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.Location = New System.Drawing.Point(267, 496)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(139, 15)
        Me.Label76.TabIndex = 53
        Me.Label76.Text = "Convenio Bancomer:"
        '
        'tbConvenioBancomer
        '
        Me.tbConvenioBancomer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbConvenioBancomer.Location = New System.Drawing.Point(412, 490)
        Me.tbConvenioBancomer.MaxLength = 50
        Me.tbConvenioBancomer.Name = "tbConvenioBancomer"
        Me.tbConvenioBancomer.Size = New System.Drawing.Size(100, 21)
        Me.tbConvenioBancomer.TabIndex = 6
        '
        'Label75
        '
        Me.Label75.BackColor = System.Drawing.Color.Black
        Me.Label75.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label75.ForeColor = System.Drawing.Color.White
        Me.Label75.Location = New System.Drawing.Point(270, 416)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(329, 23)
        Me.Label75.TabIndex = 51
        Me.Label75.Text = "Referencias"
        Me.Label75.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label74
        '
        Me.Label74.AutoSize = True
        Me.Label74.Location = New System.Drawing.Point(317, 523)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(89, 15)
        Me.Label74.TabIndex = 50
        Me.Label74.Text = "Prefijo Oxxo:"
        '
        'tbPrefijoOxxo
        '
        Me.tbPrefijoOxxo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbPrefijoOxxo.Location = New System.Drawing.Point(412, 517)
        Me.tbPrefijoOxxo.MaxLength = 5
        Me.tbPrefijoOxxo.Name = "tbPrefijoOxxo"
        Me.tbPrefijoOxxo.Size = New System.Drawing.Size(100, 21)
        Me.tbPrefijoOxxo.TabIndex = 7
        '
        'Label73
        '
        Me.Label73.AutoSize = True
        Me.Label73.Location = New System.Drawing.Point(250, 466)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(156, 15)
        Me.Label73.TabIndex = 48
        Me.Label73.Text = "Letra inicial Bancomer:"
        '
        'tbLetraInicialBancomer
        '
        Me.tbLetraInicialBancomer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbLetraInicialBancomer.Location = New System.Drawing.Point(412, 463)
        Me.tbLetraInicialBancomer.MaxLength = 5
        Me.tbLetraInicialBancomer.Name = "tbLetraInicialBancomer"
        Me.tbLetraInicialBancomer.Size = New System.Drawing.Size(100, 21)
        Me.tbLetraInicialBancomer.TabIndex = 5
        '
        'bnGenerarEstado
        '
        Me.bnGenerarEstado.Location = New System.Drawing.Point(627, 90)
        Me.bnGenerarEstado.Name = "bnGenerarEstado"
        Me.bnGenerarEstado.Size = New System.Drawing.Size(75, 23)
        Me.bnGenerarEstado.TabIndex = 46
        Me.bnGenerarEstado.Text = "&Generar"
        Me.bnGenerarEstado.UseVisualStyleBackColor = True
        '
        'bnEstadoCuentaGuardar
        '
        Me.bnEstadoCuentaGuardar.BackColor = System.Drawing.SystemColors.Control
        Me.bnEstadoCuentaGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnEstadoCuentaGuardar.ForeColor = System.Drawing.Color.Black
        Me.bnEstadoCuentaGuardar.Location = New System.Drawing.Point(706, 533)
        Me.bnEstadoCuentaGuardar.Name = "bnEstadoCuentaGuardar"
        Me.bnEstadoCuentaGuardar.Size = New System.Drawing.Size(136, 36)
        Me.bnEstadoCuentaGuardar.TabIndex = 8
        Me.bnEstadoCuentaGuardar.Text = "&GUARDAR"
        Me.bnEstadoCuentaGuardar.UseVisualStyleBackColor = False
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Location = New System.Drawing.Point(373, 380)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(33, 15)
        Me.Label64.TabIndex = 44
        Me.Label64.Text = "Día:"
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Location = New System.Drawing.Point(373, 288)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(33, 15)
        Me.Label63.TabIndex = 43
        Me.Label63.Text = "Día:"
        '
        'Label72
        '
        Me.Label72.BackColor = System.Drawing.Color.Black
        Me.Label72.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label72.ForeColor = System.Drawing.Color.White
        Me.Label72.Location = New System.Drawing.Point(270, 325)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(329, 23)
        Me.Label72.TabIndex = 42
        Me.Label72.Text = "Día del envío de Estados de Cuenta a Decos"
        Me.Label72.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label65
        '
        Me.Label65.BackColor = System.Drawing.Color.Black
        Me.Label65.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label65.ForeColor = System.Drawing.Color.White
        Me.Label65.Location = New System.Drawing.Point(270, 238)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(329, 23)
        Me.Label65.TabIndex = 41
        Me.Label65.Text = "Día de la generación de Estados de Cuenta"
        Me.Label65.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.BackColor = System.Drawing.Color.Black
        Me.Label66.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label66.ForeColor = System.Drawing.Color.White
        Me.Label66.Location = New System.Drawing.Point(56, 39)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(163, 20)
        Me.Label66.TabIndex = 38
        Me.Label66.Text = "Estados de Cuenta"
        '
        'Label70
        '
        Me.Label70.BackColor = System.Drawing.Color.Black
        Me.Label70.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label70.ForeColor = System.Drawing.Color.White
        Me.Label70.Location = New System.Drawing.Point(270, 133)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(329, 23)
        Me.Label70.TabIndex = 40
        Me.Label70.Text = "Periodo del Estado de Cuenta"
        Me.Label70.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label71
        '
        Me.Label71.AutoSize = True
        Me.Label71.BackColor = System.Drawing.Color.Black
        Me.Label71.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label71.ForeColor = System.Drawing.Color.White
        Me.Label71.Location = New System.Drawing.Point(274, 68)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(75, 20)
        Me.Label71.TabIndex = 39
        Me.Label71.Text = "Periodo:"
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.Location = New System.Drawing.Point(337, 205)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(69, 15)
        Me.Label61.TabIndex = 6
        Me.Label61.Text = "Día Final:"
        '
        'tbDiaGeneracion
        '
        Me.tbDiaGeneracion.Location = New System.Drawing.Point(412, 282)
        Me.tbDiaGeneracion.Name = "tbDiaGeneracion"
        Me.tbDiaGeneracion.Size = New System.Drawing.Size(100, 21)
        Me.tbDiaGeneracion.TabIndex = 3
        '
        'tbDiaEnvio
        '
        Me.tbDiaEnvio.Location = New System.Drawing.Point(412, 374)
        Me.tbDiaEnvio.Name = "tbDiaEnvio"
        Me.tbDiaEnvio.Size = New System.Drawing.Size(100, 21)
        Me.tbDiaEnvio.TabIndex = 4
        '
        'tbDiaFin
        '
        Me.tbDiaFin.Location = New System.Drawing.Point(412, 199)
        Me.tbDiaFin.Name = "tbDiaFin"
        Me.tbDiaFin.Size = New System.Drawing.Size(100, 21)
        Me.tbDiaFin.TabIndex = 2
        '
        'tbDiaIni
        '
        Me.tbDiaIni.Location = New System.Drawing.Point(412, 172)
        Me.tbDiaIni.Name = "tbDiaIni"
        Me.tbDiaIni.Size = New System.Drawing.Size(100, 21)
        Me.tbDiaIni.TabIndex = 1
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Location = New System.Drawing.Point(330, 178)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(76, 15)
        Me.Label60.TabIndex = 1
        Me.Label60.Text = "Día Inicial:"
        '
        'cbPeriodo
        '
        Me.cbPeriodo.DisplayMember = "Descripcion"
        Me.cbPeriodo.FormattingEnabled = True
        Me.cbPeriodo.Location = New System.Drawing.Point(273, 91)
        Me.cbPeriodo.Name = "cbPeriodo"
        Me.cbPeriodo.Size = New System.Drawing.Size(326, 23)
        Me.cbPeriodo.TabIndex = 0
        Me.cbPeriodo.ValueMember = "Clv_Periodo"
        '
        'tbpAccesoriosInstalacion
        '
        Me.tbpAccesoriosInstalacion.Controls.Add(Me.Label79)
        Me.tbpAccesoriosInstalacion.Controls.Add(Me.cmbTipoDeCaja)
        Me.tbpAccesoriosInstalacion.Controls.Add(Me.Label81)
        Me.tbpAccesoriosInstalacion.Controls.Add(Me.cmbClasificacionArticulos)
        Me.tbpAccesoriosInstalacion.Controls.Add(Me.Label80)
        Me.tbpAccesoriosInstalacion.Controls.Add(Me.btnAgregar)
        Me.tbpAccesoriosInstalacion.Controls.Add(Me.btnEliminar)
        Me.tbpAccesoriosInstalacion.Controls.Add(Me.dgvRelArticuloAccesorioInst)
        Me.tbpAccesoriosInstalacion.Controls.Add(Me.cmbAccesorioInst)
        Me.tbpAccesoriosInstalacion.Controls.Add(Me.Label78)
        Me.tbpAccesoriosInstalacion.Controls.Add(Me.Label77)
        Me.tbpAccesoriosInstalacion.Controls.Add(Me.cmbDescArticulo)
        Me.tbpAccesoriosInstalacion.Controls.Add(Me.cmbClave)
        Me.tbpAccesoriosInstalacion.Controls.Add(Me.cmbClasifMaterial)
        Me.tbpAccesoriosInstalacion.Controls.Add(Me.lblDescripcion)
        Me.tbpAccesoriosInstalacion.Controls.Add(Me.lblClave)
        Me.tbpAccesoriosInstalacion.Controls.Add(Me.lblClasificacion)
        Me.tbpAccesoriosInstalacion.Location = New System.Drawing.Point(4, 64)
        Me.tbpAccesoriosInstalacion.Name = "tbpAccesoriosInstalacion"
        Me.tbpAccesoriosInstalacion.Padding = New System.Windows.Forms.Padding(3)
        Me.tbpAccesoriosInstalacion.Size = New System.Drawing.Size(865, 592)
        Me.tbpAccesoriosInstalacion.TabIndex = 18
        Me.tbpAccesoriosInstalacion.Text = "Accesorios de Instalación"
        Me.tbpAccesoriosInstalacion.UseVisualStyleBackColor = True
        '
        'Label79
        '
        Me.Label79.AutoSize = True
        Me.Label79.BackColor = System.Drawing.Color.Black
        Me.Label79.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label79.ForeColor = System.Drawing.Color.White
        Me.Label79.Location = New System.Drawing.Point(29, 275)
        Me.Label79.Name = "Label79"
        Me.Label79.Size = New System.Drawing.Size(454, 18)
        Me.Label79.TabIndex = 90
        Me.Label79.Text = "Relación de Accesorios de Instalación por caja tipo de caja"
        '
        'cmbTipoDeCaja
        '
        Me.cmbTipoDeCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbTipoDeCaja.FormattingEnabled = True
        Me.cmbTipoDeCaja.Location = New System.Drawing.Point(164, 217)
        Me.cmbTipoDeCaja.Name = "cmbTipoDeCaja"
        Me.cmbTipoDeCaja.Size = New System.Drawing.Size(309, 21)
        Me.cmbTipoDeCaja.TabIndex = 89
        '
        'Label81
        '
        Me.Label81.AutoSize = True
        Me.Label81.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label81.Location = New System.Drawing.Point(161, 198)
        Me.Label81.Name = "Label81"
        Me.Label81.Size = New System.Drawing.Size(151, 16)
        Me.Label81.TabIndex = 88
        Me.Label81.Text = "Tipo de Caja Digital:"
        '
        'cmbClasificacionArticulos
        '
        Me.cmbClasificacionArticulos.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbClasificacionArticulos.FormattingEnabled = True
        Me.cmbClasificacionArticulos.Location = New System.Drawing.Point(338, 69)
        Me.cmbClasificacionArticulos.Name = "cmbClasificacionArticulos"
        Me.cmbClasificacionArticulos.Size = New System.Drawing.Size(461, 21)
        Me.cmbClasificacionArticulos.TabIndex = 87
        '
        'Label80
        '
        Me.Label80.AutoSize = True
        Me.Label80.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label80.Location = New System.Drawing.Point(141, 70)
        Me.Label80.Name = "Label80"
        Me.Label80.Size = New System.Drawing.Size(187, 16)
        Me.Label80.TabIndex = 86
        Me.Label80.Text = "Clasificación de Articulos:"
        '
        'btnAgregar
        '
        Me.btnAgregar.BackColor = System.Drawing.SystemColors.Control
        Me.btnAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregar.ForeColor = System.Drawing.Color.Black
        Me.btnAgregar.Location = New System.Drawing.Point(602, 262)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(110, 30)
        Me.btnAgregar.TabIndex = 85
        Me.btnAgregar.Text = "&AGREGAR"
        Me.btnAgregar.UseVisualStyleBackColor = False
        '
        'btnEliminar
        '
        Me.btnEliminar.BackColor = System.Drawing.SystemColors.Control
        Me.btnEliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminar.ForeColor = System.Drawing.Color.Black
        Me.btnEliminar.Location = New System.Drawing.Point(718, 262)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(110, 30)
        Me.btnEliminar.TabIndex = 84
        Me.btnEliminar.Text = "&ELIMINAR"
        Me.btnEliminar.UseVisualStyleBackColor = False
        '
        'dgvRelArticuloAccesorioInst
        '
        Me.dgvRelArticuloAccesorioInst.AllowUserToAddRows = False
        Me.dgvRelArticuloAccesorioInst.AllowUserToDeleteRows = False
        Me.dgvRelArticuloAccesorioInst.AllowUserToResizeColumns = False
        Me.dgvRelArticuloAccesorioInst.AllowUserToResizeRows = False
        Me.dgvRelArticuloAccesorioInst.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRelArticuloAccesorioInst.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.idTipoCaja, Me.Accesorio, Me.Articulo})
        Me.dgvRelArticuloAccesorioInst.Location = New System.Drawing.Point(33, 298)
        Me.dgvRelArticuloAccesorioInst.Name = "dgvRelArticuloAccesorioInst"
        Me.dgvRelArticuloAccesorioInst.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvRelArticuloAccesorioInst.Size = New System.Drawing.Size(795, 166)
        Me.dgvRelArticuloAccesorioInst.TabIndex = 82
        '
        'ID
        '
        Me.ID.DataPropertyName = "ID"
        Me.ID.HeaderText = "ID"
        Me.ID.Name = "ID"
        Me.ID.ReadOnly = True
        Me.ID.Visible = False
        '
        'idTipoCaja
        '
        Me.idTipoCaja.DataPropertyName = "Caja"
        Me.idTipoCaja.HeaderText = "Tipo de Caja"
        Me.idTipoCaja.Name = "idTipoCaja"
        Me.idTipoCaja.ReadOnly = True
        Me.idTipoCaja.Width = 250
        '
        'Accesorio
        '
        Me.Accesorio.DataPropertyName = "Articulo"
        Me.Accesorio.HeaderText = "Accesorio de Instalación"
        Me.Accesorio.Name = "Accesorio"
        Me.Accesorio.ReadOnly = True
        Me.Accesorio.Width = 250
        '
        'Articulo
        '
        Me.Articulo.DataPropertyName = "Accesorio"
        Me.Articulo.HeaderText = "Articulo Del Almacen"
        Me.Articulo.Name = "Articulo"
        Me.Articulo.ReadOnly = True
        Me.Articulo.Width = 250
        '
        'cmbAccesorioInst
        '
        Me.cmbAccesorioInst.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbAccesorioInst.FormattingEnabled = True
        Me.cmbAccesorioInst.Location = New System.Drawing.Point(490, 217)
        Me.cmbAccesorioInst.Name = "cmbAccesorioInst"
        Me.cmbAccesorioInst.Size = New System.Drawing.Size(309, 21)
        Me.cmbAccesorioInst.TabIndex = 81
        '
        'Label78
        '
        Me.Label78.AutoSize = True
        Me.Label78.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label78.Location = New System.Drawing.Point(487, 198)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(183, 16)
        Me.Label78.TabIndex = 80
        Me.Label78.Text = "Accesorio de Instalación:"
        '
        'Label77
        '
        Me.Label77.AutoSize = True
        Me.Label77.BackColor = System.Drawing.Color.Black
        Me.Label77.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label77.ForeColor = System.Drawing.Color.White
        Me.Label77.Location = New System.Drawing.Point(288, 10)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(356, 20)
        Me.Label77.TabIndex = 79
        Me.Label77.Text = "Configuración de Accesorios de Instalación"
        '
        'cmbDescArticulo
        '
        Me.cmbDescArticulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbDescArticulo.FormattingEnabled = True
        Me.cmbDescArticulo.Location = New System.Drawing.Point(338, 146)
        Me.cmbDescArticulo.Name = "cmbDescArticulo"
        Me.cmbDescArticulo.Size = New System.Drawing.Size(461, 21)
        Me.cmbDescArticulo.TabIndex = 78
        '
        'cmbClave
        '
        Me.cmbClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbClave.FormattingEnabled = True
        Me.cmbClave.Location = New System.Drawing.Point(6, 26)
        Me.cmbClave.Name = "cmbClave"
        Me.cmbClave.Size = New System.Drawing.Size(21, 21)
        Me.cmbClave.TabIndex = 77
        Me.cmbClave.Visible = False
        '
        'cmbClasifMaterial
        '
        Me.cmbClasifMaterial.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbClasifMaterial.FormattingEnabled = True
        Me.cmbClasifMaterial.Location = New System.Drawing.Point(338, 109)
        Me.cmbClasifMaterial.Name = "cmbClasifMaterial"
        Me.cmbClasifMaterial.Size = New System.Drawing.Size(461, 21)
        Me.cmbClasifMaterial.TabIndex = 76
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescripcion.Location = New System.Drawing.Point(174, 147)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(154, 16)
        Me.lblDescripcion.TabIndex = 74
        Me.lblDescripcion.Text = "Artículo del Almacen:"
        '
        'lblClave
        '
        Me.lblClave.AutoSize = True
        Me.lblClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClave.Location = New System.Drawing.Point(6, 7)
        Me.lblClave.Name = "lblClave"
        Me.lblClave.Size = New System.Drawing.Size(48, 16)
        Me.lblClave.TabIndex = 73
        Me.lblClave.Text = "Clave"
        Me.lblClave.Visible = False
        '
        'lblClasificacion
        '
        Me.lblClasificacion.AutoSize = True
        Me.lblClasificacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClasificacion.Location = New System.Drawing.Point(141, 110)
        Me.lblClasificacion.Name = "lblClasificacion"
        Me.lblClasificacion.Size = New System.Drawing.Size(183, 16)
        Me.lblClasificacion.TabIndex = 72
        Me.lblClasificacion.Text = "Clasificación de Material:"
        '
        'tpCobroMaterial
        '
        Me.tpCobroMaterial.Controls.Add(Me.Panel14)
        Me.tpCobroMaterial.Controls.Add(Me.btnEliminarRango)
        Me.tpCobroMaterial.Controls.Add(Me.btnAgregarRango)
        Me.tpCobroMaterial.Controls.Add(Me.lblRangoFin)
        Me.tpCobroMaterial.Controls.Add(Me.lblRangoIni)
        Me.tpCobroMaterial.Controls.Add(Me.txtRango2)
        Me.tpCobroMaterial.Controls.Add(Me.txtRango1)
        Me.tpCobroMaterial.Controls.Add(Me.nudPagos)
        Me.tpCobroMaterial.Controls.Add(Me.lblPagos)
        Me.tpCobroMaterial.Controls.Add(Me.TextBox17)
        Me.tpCobroMaterial.Controls.Add(Label84)
        Me.tpCobroMaterial.Location = New System.Drawing.Point(4, 64)
        Me.tpCobroMaterial.Name = "tpCobroMaterial"
        Me.tpCobroMaterial.Padding = New System.Windows.Forms.Padding(3)
        Me.tpCobroMaterial.Size = New System.Drawing.Size(865, 592)
        Me.tpCobroMaterial.TabIndex = 19
        Me.tpCobroMaterial.Text = "Cobro de Material"
        Me.tpCobroMaterial.UseVisualStyleBackColor = True
        '
        'Panel14
        '
        Me.Panel14.Controls.Add(Me.dgvRangosMaterial)
        Me.Panel14.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel14.Location = New System.Drawing.Point(125, 228)
        Me.Panel14.Name = "Panel14"
        Me.Panel14.Size = New System.Drawing.Size(490, 164)
        Me.Panel14.TabIndex = 138
        '
        'dgvRangosMaterial
        '
        Me.dgvRangosMaterial.AllowUserToAddRows = False
        Me.dgvRangosMaterial.AllowUserToDeleteRows = False
        Me.dgvRangosMaterial.AllowUserToOrderColumns = True
        Me.dgvRangosMaterial.AllowUserToResizeColumns = False
        Me.dgvRangosMaterial.AllowUserToResizeRows = False
        Me.dgvRangosMaterial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRangosMaterial.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.identity, Me.RangoIni, Me.RangoFinal, Me.NumPagos})
        Me.dgvRangosMaterial.Location = New System.Drawing.Point(7, 7)
        Me.dgvRangosMaterial.Margin = New System.Windows.Forms.Padding(3, 6, 3, 6)
        Me.dgvRangosMaterial.Name = "dgvRangosMaterial"
        Me.dgvRangosMaterial.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvRangosMaterial.Size = New System.Drawing.Size(475, 149)
        Me.dgvRangosMaterial.TabIndex = 103
        '
        'identity
        '
        Me.identity.DataPropertyName = "ident"
        Me.identity.HeaderText = "ident"
        Me.identity.Name = "identity"
        Me.identity.ReadOnly = True
        Me.identity.Visible = False
        '
        'RangoIni
        '
        Me.RangoIni.DataPropertyName = "Inicio"
        Me.RangoIni.HeaderText = "Rango Inicial"
        Me.RangoIni.Name = "RangoIni"
        Me.RangoIni.ReadOnly = True
        Me.RangoIni.Width = 120
        '
        'RangoFinal
        '
        Me.RangoFinal.DataPropertyName = "Final"
        Me.RangoFinal.HeaderText = "Rango Final"
        Me.RangoFinal.Name = "RangoFinal"
        Me.RangoFinal.ReadOnly = True
        Me.RangoFinal.Width = 120
        '
        'NumPagos
        '
        Me.NumPagos.DataPropertyName = "NMaximoDiferidos"
        Me.NumPagos.HeaderText = "Número Máximo de Pagos Diferidos"
        Me.NumPagos.Name = "NumPagos"
        Me.NumPagos.ReadOnly = True
        Me.NumPagos.Width = 190
        '
        'btnEliminarRango
        '
        Me.btnEliminarRango.BackColor = System.Drawing.SystemColors.Control
        Me.btnEliminarRango.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminarRango.ForeColor = System.Drawing.Color.Black
        Me.btnEliminarRango.Location = New System.Drawing.Point(646, 274)
        Me.btnEliminarRango.Name = "btnEliminarRango"
        Me.btnEliminarRango.Size = New System.Drawing.Size(99, 30)
        Me.btnEliminarRango.TabIndex = 137
        Me.btnEliminarRango.Text = "&Eliminar Rango"
        Me.btnEliminarRango.UseVisualStyleBackColor = False
        '
        'btnAgregarRango
        '
        Me.btnAgregarRango.BackColor = System.Drawing.SystemColors.Control
        Me.btnAgregarRango.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregarRango.ForeColor = System.Drawing.Color.Black
        Me.btnAgregarRango.Location = New System.Drawing.Point(646, 232)
        Me.btnAgregarRango.Name = "btnAgregarRango"
        Me.btnAgregarRango.Size = New System.Drawing.Size(99, 36)
        Me.btnAgregarRango.TabIndex = 136
        Me.btnAgregarRango.Text = "&Agregar Rango"
        Me.btnAgregarRango.UseVisualStyleBackColor = False
        '
        'lblRangoFin
        '
        Me.lblRangoFin.AutoSize = True
        Me.lblRangoFin.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRangoFin.Location = New System.Drawing.Point(286, 176)
        Me.lblRangoFin.Name = "lblRangoFin"
        Me.lblRangoFin.Size = New System.Drawing.Size(77, 18)
        Me.lblRangoFin.TabIndex = 135
        Me.lblRangoFin.Text = "Rango Final:"
        '
        'lblRangoIni
        '
        Me.lblRangoIni.AutoSize = True
        Me.lblRangoIni.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRangoIni.Location = New System.Drawing.Point(137, 176)
        Me.lblRangoIni.Name = "lblRangoIni"
        Me.lblRangoIni.Size = New System.Drawing.Size(83, 18)
        Me.lblRangoIni.TabIndex = 134
        Me.lblRangoIni.Text = "Rango Inicial:"
        '
        'txtRango2
        '
        Me.txtRango2.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRango2.Location = New System.Drawing.Point(268, 197)
        Me.txtRango2.Name = "txtRango2"
        Me.txtRango2.Size = New System.Drawing.Size(118, 26)
        Me.txtRango2.TabIndex = 133
        Me.txtRango2.Text = "0.0"
        Me.txtRango2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtRango1
        '
        Me.txtRango1.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRango1.Location = New System.Drawing.Point(125, 197)
        Me.txtRango1.Name = "txtRango1"
        Me.txtRango1.Size = New System.Drawing.Size(118, 26)
        Me.txtRango1.TabIndex = 132
        Me.txtRango1.Text = "0.0"
        Me.txtRango1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'nudPagos
        '
        Me.nudPagos.BackColor = System.Drawing.Color.White
        Me.nudPagos.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudPagos.Location = New System.Drawing.Point(461, 196)
        Me.nudPagos.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.nudPagos.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.nudPagos.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudPagos.Name = "nudPagos"
        Me.nudPagos.ReadOnly = True
        Me.nudPagos.Size = New System.Drawing.Size(110, 30)
        Me.nudPagos.TabIndex = 131
        Me.nudPagos.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudPagos.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblPagos
        '
        Me.lblPagos.AutoSize = True
        Me.lblPagos.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPagos.Location = New System.Drawing.Point(411, 176)
        Me.lblPagos.Name = "lblPagos"
        Me.lblPagos.Size = New System.Drawing.Size(208, 18)
        Me.lblPagos.TabIndex = 130
        Me.lblPagos.Text = "Número Máximo de Pagos Diferidos"
        '
        'TextBox17
        '
        Me.TextBox17.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraTipServEricBindingSource, "Clv_TipSer", True))
        Me.TextBox17.Location = New System.Drawing.Point(580, 344)
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.ReadOnly = True
        Me.TextBox17.Size = New System.Drawing.Size(10, 21)
        Me.TextBox17.TabIndex = 129
        Me.TextBox17.TabStop = False
        '
        'Mizar
        '
        Me.Mizar.Controls.Add(Me.cbxMizarS)
        Me.Mizar.Controls.Add(Me.lblLeyenda)
        Me.Mizar.Controls.Add(Me.cbxMizar)
        Me.Mizar.Controls.Add(Me.btnGuardar)
        Me.Mizar.Controls.Add(Me.lblTituloF)
        Me.Mizar.Controls.Add(Me.lblUltFolioF)
        Me.Mizar.Controls.Add(Me.txtUltFolioF)
        Me.Mizar.Controls.Add(Me.lblSerieF)
        Me.Mizar.Controls.Add(Me.txtSerieF)
        Me.Mizar.Controls.Add(Me.lblTituloG)
        Me.Mizar.Controls.Add(Me.lblUltFolioG)
        Me.Mizar.Controls.Add(Me.txtUltFolioG)
        Me.Mizar.Controls.Add(Me.lblSerieG)
        Me.Mizar.Controls.Add(Me.txtSerieG)
        Me.Mizar.Controls.Add(Me.cmbCompanias)
        Me.Mizar.Controls.Add(Me.lblCompania)
        Me.Mizar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Mizar.Location = New System.Drawing.Point(4, 64)
        Me.Mizar.Name = "Mizar"
        Me.Mizar.Padding = New System.Windows.Forms.Padding(3)
        Me.Mizar.Size = New System.Drawing.Size(865, 592)
        Me.Mizar.TabIndex = 20
        Me.Mizar.Text = "Facturas"
        Me.Mizar.UseVisualStyleBackColor = True
        '
        'cbxMizarS
        '
        Me.cbxMizarS.AutoSize = True
        Me.cbxMizarS.BackColor = System.Drawing.Color.Black
        Me.cbxMizarS.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxMizarS.ForeColor = System.Drawing.Color.White
        Me.cbxMizarS.Location = New System.Drawing.Point(634, 41)
        Me.cbxMizarS.Name = "cbxMizarS"
        Me.cbxMizarS.Size = New System.Drawing.Size(41, 20)
        Me.cbxMizarS.TabIndex = 34
        Me.cbxMizarS.Text = "SI"
        Me.cbxMizarS.UseVisualStyleBackColor = False
        '
        'lblLeyenda
        '
        Me.lblLeyenda.AutoSize = True
        Me.lblLeyenda.BackColor = System.Drawing.Color.Black
        Me.lblLeyenda.ForeColor = System.Drawing.Color.White
        Me.lblLeyenda.Location = New System.Drawing.Point(278, 41)
        Me.lblLeyenda.Name = "lblLeyenda"
        Me.lblLeyenda.Size = New System.Drawing.Size(300, 20)
        Me.lblLeyenda.TabIndex = 33
        Me.lblLeyenda.Text = "Mizar generará las facturas digitales"
        '
        'cbxMizar
        '
        Me.cbxMizar.AutoSize = True
        Me.cbxMizar.BackColor = System.Drawing.Color.Black
        Me.cbxMizar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxMizar.ForeColor = System.Drawing.Color.White
        Me.cbxMizar.Location = New System.Drawing.Point(710, 41)
        Me.cbxMizar.Name = "cbxMizar"
        Me.cbxMizar.Size = New System.Drawing.Size(49, 20)
        Me.cbxMizar.TabIndex = 32
        Me.cbxMizar.Text = "NO"
        Me.cbxMizar.UseVisualStyleBackColor = False
        '
        'btnGuardar
        '
        Me.btnGuardar.BackColor = System.Drawing.SystemColors.Control
        Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.ForeColor = System.Drawing.Color.Black
        Me.btnGuardar.Location = New System.Drawing.Point(337, 425)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(136, 36)
        Me.btnGuardar.TabIndex = 31
        Me.btnGuardar.Text = "&GUARDAR"
        Me.btnGuardar.UseVisualStyleBackColor = False
        '
        'lblTituloF
        '
        Me.lblTituloF.AutoSize = True
        Me.lblTituloF.BackColor = System.Drawing.Color.Black
        Me.lblTituloF.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTituloF.ForeColor = System.Drawing.Color.White
        Me.lblTituloF.Location = New System.Drawing.Point(199, 307)
        Me.lblTituloF.Name = "lblTituloF"
        Me.lblTituloF.Size = New System.Drawing.Size(125, 18)
        Me.lblTituloF.TabIndex = 30
        Me.lblTituloF.Text = "Factura Fiscal :"
        '
        'lblUltFolioF
        '
        Me.lblUltFolioF.AutoSize = True
        Me.lblUltFolioF.BackColor = System.Drawing.Color.Black
        Me.lblUltFolioF.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUltFolioF.ForeColor = System.Drawing.Color.White
        Me.lblUltFolioF.Location = New System.Drawing.Point(394, 339)
        Me.lblUltFolioF.Name = "lblUltFolioF"
        Me.lblUltFolioF.Size = New System.Drawing.Size(149, 16)
        Me.lblUltFolioF.TabIndex = 29
        Me.lblUltFolioF.Text = "Último Folio Usado :"
        '
        'txtUltFolioF
        '
        Me.txtUltFolioF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtUltFolioF.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUltFolioF.Location = New System.Drawing.Point(546, 336)
        Me.txtUltFolioF.Name = "txtUltFolioF"
        Me.txtUltFolioF.Size = New System.Drawing.Size(116, 21)
        Me.txtUltFolioF.TabIndex = 27
        '
        'lblSerieF
        '
        Me.lblSerieF.AutoSize = True
        Me.lblSerieF.BackColor = System.Drawing.Color.Black
        Me.lblSerieF.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSerieF.ForeColor = System.Drawing.Color.White
        Me.lblSerieF.Location = New System.Drawing.Point(192, 339)
        Me.lblSerieF.Name = "lblSerieF"
        Me.lblSerieF.Size = New System.Drawing.Size(49, 15)
        Me.lblSerieF.TabIndex = 28
        Me.lblSerieF.Text = "Serie :"
        '
        'txtSerieF
        '
        Me.txtSerieF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSerieF.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSerieF.Location = New System.Drawing.Point(243, 336)
        Me.txtSerieF.MaxLength = 5
        Me.txtSerieF.Name = "txtSerieF"
        Me.txtSerieF.Size = New System.Drawing.Size(116, 21)
        Me.txtSerieF.TabIndex = 26
        '
        'lblTituloG
        '
        Me.lblTituloG.AutoSize = True
        Me.lblTituloG.BackColor = System.Drawing.Color.Black
        Me.lblTituloG.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTituloG.ForeColor = System.Drawing.Color.White
        Me.lblTituloG.Location = New System.Drawing.Point(197, 197)
        Me.lblTituloG.Name = "lblTituloG"
        Me.lblTituloG.Size = New System.Drawing.Size(129, 18)
        Me.lblTituloG.TabIndex = 25
        Me.lblTituloG.Text = "Factura Global :"
        '
        'lblUltFolioG
        '
        Me.lblUltFolioG.AutoSize = True
        Me.lblUltFolioG.BackColor = System.Drawing.Color.Black
        Me.lblUltFolioG.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUltFolioG.ForeColor = System.Drawing.Color.White
        Me.lblUltFolioG.Location = New System.Drawing.Point(392, 229)
        Me.lblUltFolioG.Name = "lblUltFolioG"
        Me.lblUltFolioG.Size = New System.Drawing.Size(149, 16)
        Me.lblUltFolioG.TabIndex = 24
        Me.lblUltFolioG.Text = "Último Folio Usado :"
        '
        'txtUltFolioG
        '
        Me.txtUltFolioG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtUltFolioG.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUltFolioG.Location = New System.Drawing.Point(544, 226)
        Me.txtUltFolioG.Name = "txtUltFolioG"
        Me.txtUltFolioG.Size = New System.Drawing.Size(116, 21)
        Me.txtUltFolioG.TabIndex = 22
        '
        'lblSerieG
        '
        Me.lblSerieG.AutoSize = True
        Me.lblSerieG.BackColor = System.Drawing.Color.Black
        Me.lblSerieG.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSerieG.ForeColor = System.Drawing.Color.White
        Me.lblSerieG.Location = New System.Drawing.Point(186, 229)
        Me.lblSerieG.Name = "lblSerieG"
        Me.lblSerieG.Size = New System.Drawing.Size(53, 16)
        Me.lblSerieG.TabIndex = 23
        Me.lblSerieG.Text = "Serie :"
        '
        'txtSerieG
        '
        Me.txtSerieG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSerieG.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSerieG.Location = New System.Drawing.Point(241, 226)
        Me.txtSerieG.MaxLength = 5
        Me.txtSerieG.Name = "txtSerieG"
        Me.txtSerieG.Size = New System.Drawing.Size(116, 21)
        Me.txtSerieG.TabIndex = 20
        '
        'cmbCompanias
        '
        Me.cmbCompanias.DisplayMember = "nombreCompania"
        Me.cmbCompanias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbCompanias.FormattingEnabled = True
        Me.cmbCompanias.Location = New System.Drawing.Point(279, 120)
        Me.cmbCompanias.Name = "cmbCompanias"
        Me.cmbCompanias.Size = New System.Drawing.Size(384, 24)
        Me.cmbCompanias.TabIndex = 19
        Me.cmbCompanias.ValueMember = "clvCompania"
        '
        'lblCompania
        '
        Me.lblCompania.AutoSize = True
        Me.lblCompania.BackColor = System.Drawing.Color.Black
        Me.lblCompania.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompania.ForeColor = System.Drawing.Color.White
        Me.lblCompania.Location = New System.Drawing.Point(186, 120)
        Me.lblCompania.Name = "lblCompania"
        Me.lblCompania.Size = New System.Drawing.Size(86, 16)
        Me.lblCompania.TabIndex = 21
        Me.lblCompania.Text = "Compañía :"
        '
        'TabOxxo
        '
        Me.TabOxxo.Controls.Add(Me.txtPrefijo)
        Me.TabOxxo.Controls.Add(Me.Label83)
        Me.TabOxxo.Controls.Add(Me.Button13)
        Me.TabOxxo.Controls.Add(Me.txtComisionOxxo)
        Me.TabOxxo.Controls.Add(Me.Label82)
        Me.TabOxxo.Location = New System.Drawing.Point(4, 64)
        Me.TabOxxo.Name = "TabOxxo"
        Me.TabOxxo.Padding = New System.Windows.Forms.Padding(3)
        Me.TabOxxo.Size = New System.Drawing.Size(865, 592)
        Me.TabOxxo.TabIndex = 21
        Me.TabOxxo.Text = "OXXO"
        Me.TabOxxo.UseVisualStyleBackColor = True
        '
        'txtPrefijo
        '
        Me.txtPrefijo.Location = New System.Drawing.Point(459, 158)
        Me.txtPrefijo.Name = "txtPrefijo"
        Me.txtPrefijo.Size = New System.Drawing.Size(100, 21)
        Me.txtPrefijo.TabIndex = 16
        '
        'Label83
        '
        Me.Label83.AutoSize = True
        Me.Label83.Location = New System.Drawing.Point(303, 164)
        Me.Label83.Name = "Label83"
        Me.Label83.Size = New System.Drawing.Size(150, 15)
        Me.Label83.TabIndex = 15
        Me.Label83.Text = "Prefijo del Proveedor :"
        '
        'Button13
        '
        Me.Button13.BackColor = System.Drawing.SystemColors.Control
        Me.Button13.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button13.ForeColor = System.Drawing.Color.Black
        Me.Button13.Location = New System.Drawing.Point(723, 550)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(136, 36)
        Me.Button13.TabIndex = 14
        Me.Button13.Text = "&GUARDAR"
        Me.Button13.UseVisualStyleBackColor = False
        '
        'txtComisionOxxo
        '
        Me.txtComisionOxxo.Location = New System.Drawing.Point(459, 201)
        Me.txtComisionOxxo.Name = "txtComisionOxxo"
        Me.txtComisionOxxo.Size = New System.Drawing.Size(100, 21)
        Me.txtComisionOxxo.TabIndex = 13
        '
        'Label82
        '
        Me.Label82.AutoSize = True
        Me.Label82.Location = New System.Drawing.Point(378, 204)
        Me.Label82.Name = "Label82"
        Me.Label82.Size = New System.Drawing.Size(75, 15)
        Me.Label82.TabIndex = 12
        Me.Label82.Text = "Comisión :"
        '
        'ConsultaRelTecnicosQuejasBindingSource
        '
        Me.ConsultaRelTecnicosQuejasBindingSource.DataMember = "ConsultaRel_Tecnicos_Quejas"
        Me.ConsultaRelTecnicosQuejasBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'ConsultaRelQuejasTecFamiliaBindingSource
        '
        Me.ConsultaRelQuejasTecFamiliaBindingSource.DataMember = "ConsultaRel_Quejas_Tec_Familia"
        Me.ConsultaRelQuejasTecFamiliaBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.SystemColors.Control
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(392, 693)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 74
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'ConGeneralTableAdapter1
        '
        Me.ConGeneralTableAdapter1.ClearBeforeFill = True
        '
        'ConGeneralAntiguedadTableAdapter1
        '
        Me.ConGeneralAntiguedadTableAdapter1.ClearBeforeFill = True
        '
        'MUESTRAPERIODOSTableAdapter1
        '
        Me.MUESTRAPERIODOSTableAdapter1.ClearBeforeFill = True
        '
        'CONSULTAGENERALESDESCTableAdapter1
        '
        Me.CONSULTAGENERALESDESCTableAdapter1.ClearBeforeFill = True
        '
        'MODIFCAGENERALESDESCBindingSource1
        '
        Me.MODIFCAGENERALESDESCBindingSource1.DataMember = "MODIFCAGENERALESDESC"
        Me.MODIFCAGENERALESDESCBindingSource1.DataSource = Me.DataSetEric2
        '
        'MODIFCAGENERALESDESCTableAdapter1
        '
        Me.MODIFCAGENERALESDESCTableAdapter1.ClearBeforeFill = True
        '
        'NueGeneralBindingSource1
        '
        Me.NueGeneralBindingSource1.DataMember = "NueGeneral"
        Me.NueGeneralBindingSource1.DataSource = Me.DataSetEric2
        '
        'NueGeneralTableAdapter1
        '
        Me.NueGeneralTableAdapter1.ClearBeforeFill = True
        '
        'NUEGeneralAntiguedadBindingSource1
        '
        Me.NUEGeneralAntiguedadBindingSource1.DataMember = "NUEGeneralAntiguedad"
        Me.NUEGeneralAntiguedadBindingSource1.DataSource = Me.DataSetEric2
        '
        'NUEGeneralAntiguedadTableAdapter1
        '
        Me.NUEGeneralAntiguedadTableAdapter1.ClearBeforeFill = True
        '
        'ConsultaRel_Quejas_Tec_FamiliaBindingSource
        '
        Me.ConsultaRel_Quejas_Tec_FamiliaBindingSource.DataMember = "ConsultaRel_Quejas_Tec_Familia"
        Me.ConsultaRel_Quejas_Tec_FamiliaBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'ConsultaRel_Quejas_Tec_FamiliaTableAdapter
        '
        Me.ConsultaRel_Quejas_Tec_FamiliaTableAdapter.ClearBeforeFill = True
        '
        'ConsultaRel_Tecnicos_QuejasBindingSource
        '
        Me.ConsultaRel_Tecnicos_QuejasBindingSource.DataMember = "ConsultaRel_Tecnicos_Quejas"
        Me.ConsultaRel_Tecnicos_QuejasBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'ConsultaRel_Tecnicos_QuejasTableAdapter
        '
        Me.ConsultaRel_Tecnicos_QuejasTableAdapter.ClearBeforeFill = True
        '
        'Muestra_tecnicosDepartamentos_AlmacenTableAdapter
        '
        Me.Muestra_tecnicosDepartamentos_AlmacenTableAdapter.ClearBeforeFill = True
        '
        'Muestra_TecnicosByFamiliTableAdapter
        '
        Me.Muestra_TecnicosByFamiliTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'TabPageCorteSistema
        '
        Me.TabPageCorteSistema.Controls.Add(Me.TextBoxMensajeCorte)
        Me.TabPageCorteSistema.Controls.Add(Me.TextBoxMensajeAvisoCorte)
        Me.TabPageCorteSistema.Controls.Add(Label95)
        Me.TabPageCorteSistema.Controls.Add(Label96)
        Me.TabPageCorteSistema.Controls.Add(Label97)
        Me.TabPageCorteSistema.Controls.Add(Me.TextBoxDiasAvisoCorte)
        Me.TabPageCorteSistema.Controls.Add(Label98)
        Me.TabPageCorteSistema.Controls.Add(Me.CheckBoxCorteSATV)
        Me.TabPageCorteSistema.Controls.Add(Me.TextBoxFecCorteSatv)
        Me.TabPageCorteSistema.Controls.Add(Me.Button22)
        Me.TabPageCorteSistema.Controls.Add(Me.Label99)
        Me.TabPageCorteSistema.Location = New System.Drawing.Point(4, 84)
        Me.TabPageCorteSistema.Name = "TabPageCorteSistema"
        Me.TabPageCorteSistema.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageCorteSistema.Size = New System.Drawing.Size(865, 572)
        Me.TabPageCorteSistema.TabIndex = 22
        Me.TabPageCorteSistema.Text = "Corte del Sistema por SATV"
        Me.TabPageCorteSistema.UseVisualStyleBackColor = True
        '
        'TextBoxMensajeCorte
        '
        Me.TextBoxMensajeCorte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxMensajeCorte.Location = New System.Drawing.Point(418, 275)
        Me.TextBoxMensajeCorte.Multiline = True
        Me.TextBoxMensajeCorte.Name = "TextBoxMensajeCorte"
        Me.TextBoxMensajeCorte.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBoxMensajeCorte.Size = New System.Drawing.Size(323, 86)
        Me.TextBoxMensajeCorte.TabIndex = 152
        '
        'TextBoxMensajeAvisoCorte
        '
        Me.TextBoxMensajeAvisoCorte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxMensajeAvisoCorte.Location = New System.Drawing.Point(418, 181)
        Me.TextBoxMensajeAvisoCorte.Multiline = True
        Me.TextBoxMensajeAvisoCorte.Name = "TextBoxMensajeAvisoCorte"
        Me.TextBoxMensajeAvisoCorte.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBoxMensajeAvisoCorte.Size = New System.Drawing.Size(323, 86)
        Me.TextBoxMensajeAvisoCorte.TabIndex = 151
        '
        'Label95
        '
        Label95.AutoSize = True
        Label95.Location = New System.Drawing.Point(206, 282)
        Label95.Name = "Label95"
        Label95.Size = New System.Drawing.Size(120, 15)
        Label95.TabIndex = 150
        Label95.Text = "Mensaje de Corte"
        '
        'Label96
        '
        Label96.AutoSize = True
        Label96.Location = New System.Drawing.Point(206, 185)
        Label96.Name = "Label96"
        Label96.Size = New System.Drawing.Size(119, 15)
        Label96.TabIndex = 149
        Label96.Text = "Mensaje de Aviso"
        '
        'Label97
        '
        Label97.AutoSize = True
        Label97.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label97.Location = New System.Drawing.Point(206, 157)
        Label97.Name = "Label97"
        Label97.Size = New System.Drawing.Size(93, 15)
        Label97.TabIndex = 148
        Label97.Text = "Dias de Aviso"
        '
        'TextBoxDiasAvisoCorte
        '
        Me.TextBoxDiasAvisoCorte.Location = New System.Drawing.Point(418, 153)
        Me.TextBoxDiasAvisoCorte.Name = "TextBoxDiasAvisoCorte"
        Me.TextBoxDiasAvisoCorte.Size = New System.Drawing.Size(154, 21)
        Me.TextBoxDiasAvisoCorte.TabIndex = 147
        '
        'Label98
        '
        Label98.AutoSize = True
        Label98.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label98.Location = New System.Drawing.Point(206, 128)
        Label98.Name = "Label98"
        Label98.Size = New System.Drawing.Size(184, 15)
        Label98.TabIndex = 146
        Label98.Text = "Fecha de Corte del Sistema"
        '
        'CheckBoxCorteSATV
        '
        Me.CheckBoxCorteSATV.AutoSize = True
        Me.CheckBoxCorteSATV.Location = New System.Drawing.Point(418, 89)
        Me.CheckBoxCorteSATV.Name = "CheckBoxCorteSATV"
        Me.CheckBoxCorteSATV.Size = New System.Drawing.Size(63, 19)
        Me.CheckBoxCorteSATV.TabIndex = 145
        Me.CheckBoxCorteSATV.Text = "Activo"
        Me.CheckBoxCorteSATV.UseVisualStyleBackColor = True
        '
        'TextBoxFecCorteSatv
        '
        Me.TextBoxFecCorteSatv.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxFecCorteSatv.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxFecCorteSatv.Location = New System.Drawing.Point(418, 126)
        Me.TextBoxFecCorteSatv.Mask = "00/00/0000"
        Me.TextBoxFecCorteSatv.Name = "TextBoxFecCorteSatv"
        Me.TextBoxFecCorteSatv.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.TextBoxFecCorteSatv.Size = New System.Drawing.Size(107, 21)
        Me.TextBoxFecCorteSatv.TabIndex = 144
        Me.TextBoxFecCorteSatv.ValidatingType = GetType(Date)
        '
        'Button22
        '
        Me.Button22.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button22.Location = New System.Drawing.Point(691, 509)
        Me.Button22.Name = "Button22"
        Me.Button22.Size = New System.Drawing.Size(136, 36)
        Me.Button22.TabIndex = 143
        Me.Button22.Text = "&GUARDAR"
        Me.Button22.UseVisualStyleBackColor = True
        '
        'Label99
        '
        Me.Label99.AutoSize = True
        Me.Label99.BackColor = System.Drawing.Color.Black
        Me.Label99.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label99.ForeColor = System.Drawing.Color.White
        Me.Label99.Location = New System.Drawing.Point(37, 27)
        Me.Label99.Name = "Label99"
        Me.Label99.Size = New System.Drawing.Size(234, 20)
        Me.Label99.TabIndex = 142
        Me.Label99.Text = "Corte del Sistema por SATV"
        '
        'FrmGenerales_Sistema
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(901, 741)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.tbcAccesoriosInstalacion)
        Me.MaximizeBox = False
        Me.Name = "FrmGenerales_Sistema"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Generales del Sistema"
        CType(Me.ConsultaCpGeneBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_ImpresorasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTipServEricBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConMesesCobroAdeudoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConGeneralCorreoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Mod_Cp_GenBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_Generales_Filtros_OrdenesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Generales_Filtros_OrdenesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.COnsultaGralfacturaglobalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Gral_factura_globalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.MUESTRAPERIODOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONSULTAGENERALESDESCBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.PanelBasico.ResumeLayout(False)
        Me.PanelBasico.PerformLayout()
        Me.bpxImpresiones.ResumeLayout(False)
        Me.bpxImpresiones.PerformLayout()
        CType(Me.NumericUpDown2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConGeneralBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConGeneralAntiguedadBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.NumericUpDown8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.NumericUpDown6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_cobrodepositoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        CType(Me.ConGeneralAlertaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel12.ResumeLayout(False)
        Me.Panel12.PerformLayout()
        CType(Me.CONSULTA_General_HsbcBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetyahve, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel11.ResumeLayout(False)
        Me.Panel11.PerformLayout()
        CType(Me.CONSULTA_General_SantanderBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelDatosXml.ResumeLayout(False)
        Me.PanelDatosXml.PerformLayout()
        CType(Me.CONSULTA_General_XmlBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelDatos.ResumeLayout(False)
        Me.PanelDatos.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        CType(Me.ConGeneralMsjTicketsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        CType(Me.Consulta_cobrodepositobuenoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        CType(Me.NumericUpDown9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_Rel_PaquetesDigTelDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_Rel_PaquetesDigTelBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NueGeneralMsjTicketsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NueGeneralAlertaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_Direcciones_IpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage14.ResumeLayout(False)
        Me.TabPage13.ResumeLayout(False)
        Me.TabPage12.ResumeLayout(False)
        Me.TabPage11.ResumeLayout(False)
        Me.TabPage10.ResumeLayout(False)
        Me.TabPage9.ResumeLayout(False)
        Me.TabPage8.ResumeLayout(False)
        Me.TabPage7.ResumeLayout(False)
        Me.TabPage6.ResumeLayout(False)
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.tbcAccesoriosInstalacion.ResumeLayout(False)
        Me.TabPage15.ResumeLayout(False)
        Me.PnQuejas.ResumeLayout(False)
        Me.PnQuejas.PerformLayout()
        CType(Me.MuestraTecnicosByFamiliBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestratecnicosDepartamentosAlmacenBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage16.ResumeLayout(False)
        Me.TabPage16.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage17.ResumeLayout(False)
        Me.TabPage17.PerformLayout()
        CType(Me.NumericUpDownIeps, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDownIva, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbEstadoCuenta.ResumeLayout(False)
        Me.tbEstadoCuenta.PerformLayout()
        Me.tbpAccesoriosInstalacion.ResumeLayout(False)
        Me.tbpAccesoriosInstalacion.PerformLayout()
        CType(Me.dgvRelArticuloAccesorioInst, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpCobroMaterial.ResumeLayout(False)
        Me.tpCobroMaterial.PerformLayout()
        Me.Panel14.ResumeLayout(False)
        CType(Me.dgvRangosMaterial, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudPagos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Mizar.ResumeLayout(False)
        Me.Mizar.PerformLayout()
        Me.TabOxxo.ResumeLayout(False)
        Me.TabOxxo.PerformLayout()
        CType(Me.ConsultaRelTecnicosQuejasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsultaRelQuejasTecFamiliaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MODIFCAGENERALESDESCBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NueGeneralBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NUEGeneralAntiguedadBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsultaRel_Quejas_Tec_FamiliaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsultaRel_Tecnicos_QuejasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPageCorteSistema.ResumeLayout(False)
        Me.TabPageCorteSistema.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents ConGeneralBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConGeneralTableAdapter As sofTV.NewSofTvDataSetTableAdapters.ConGeneralTableAdapter
    Friend WithEvents NueGeneralBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NueGeneralTableAdapter As sofTV.NewSofTvDataSetTableAdapters.NueGeneralTableAdapter
    Friend WithEvents ConGeneralAntiguedadBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConGeneralAntiguedadTableAdapter As sofTV.NewSofTvDataSetTableAdapters.ConGeneralAntiguedadTableAdapter
    Friend WithEvents NUEGeneralAntiguedadBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NUEGeneralAntiguedadTableAdapter As sofTV.NewSofTvDataSetTableAdapters.NUEGeneralAntiguedadTableAdapter
    Friend WithEvents MUESTRAPERIODOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAPERIODOSTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRAPERIODOSTableAdapter
    Friend WithEvents CONSULTAGENERALESDESCBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONSULTAGENERALESDESCTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONSULTAGENERALESDESCTableAdapter
    Friend WithEvents MODIFCAGENERALESDESCBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MODIFCAGENERALESDESCTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MODIFCAGENERALESDESCTableAdapter
    Friend WithEvents DataSetLidia As sofTV.DataSetLidia
    Friend WithEvents MODIFCAPeriodosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MODIFCAPeriodosTableAdapter As sofTV.DataSetLidiaTableAdapters.MODIFCAPeriodosTableAdapter
    Friend WithEvents DataSetarnoldo As sofTV.DataSetarnoldo
    Friend WithEvents Consulta_ImpresorasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_ImpresorasTableAdapter As sofTV.DataSetarnoldoTableAdapters.Consulta_ImpresorasTableAdapter
    Friend WithEvents Inserta_Mod_Cp_GenBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Mod_Cp_GenTableAdapter As sofTV.DataSetarnoldoTableAdapters.Inserta_Mod_Cp_GenTableAdapter
    Friend WithEvents ConsultaCpGeneBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_Cp_GeneTableAdapter As sofTV.DataSetarnoldoTableAdapters.Consulta_Cp_GeneTableAdapter
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents ConMesesCobroAdeudoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConMesesCobroAdeudoTableAdapter As sofTV.DataSetEricTableAdapters.ConMesesCobroAdeudoTableAdapter
    Friend WithEvents MuestraTipServEricBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipServEricTableAdapter As sofTV.DataSetEricTableAdapters.MuestraTipServEricTableAdapter
    Friend WithEvents ConGeneralCorreoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConGeneralCorreoTableAdapter As sofTV.DataSetEricTableAdapters.ConGeneralCorreoTableAdapter
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents Consulta_Generales_Filtros_OrdenesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_Generales_Filtros_OrdenesTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Consulta_Generales_Filtros_OrdenesTableAdapter
    Friend WithEvents Inserta_Generales_Filtros_OrdenesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Generales_Filtros_OrdenesTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_Generales_Filtros_OrdenesTableAdapter
    'Friend WithEvents COnsulta_Gral_factura_globalBindingSource As System.Windows.Forms.BindingSource
    'Friend WithEvents COnsulta_Gral_factura_globalTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.COnsulta_Gral_factura_globalTableAdapter
    Friend WithEvents COnsultaGralfacturaglobalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents COnsulta_Gral_factura_globalTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.COnsulta_Gral_factura_globalTableAdapter
    Friend WithEvents Inserta_Gral_factura_globalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Gral_factura_globalTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_Gral_factura_globalTableAdapter
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents CuentaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PasswordTextBox As System.Windows.Forms.TextBox
    Friend WithEvents HostTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PortTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Clv_TipSerTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ConceptoComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents MesesTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents TextBox10 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox15 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox11 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox16 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox13 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox14 As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents ImpresoraFiscalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ImpresoraTarjetasTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ImpresoraTicketsTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ImpresoraContratosTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PanelBasico As System.Windows.Forms.Panel
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown7 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown8 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown5 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown6 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown3 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown4 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown2 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Puntos10TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Puntos5TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Puntos2TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Puntos1TextBox As System.Windows.Forms.TextBox
    Friend WithEvents ImpresoraOrdenesTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents CostoExtTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NumeroExtTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Carga_trab_insTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CargaDeTrabajoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ImportePorExtrasTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Si_se_generaCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents Button15 As System.Windows.Forms.Button
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Button16 As System.Windows.Forms.Button
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric2
    Friend WithEvents ConGeneralMsjTicketsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConGeneralMsjTicketsTableAdapter As sofTV.DataSetEric2TableAdapters.ConGeneralMsjTicketsTableAdapter
    Friend WithEvents MensajeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ActivoCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents NueGeneralMsjTicketsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NueGeneralMsjTicketsTableAdapter As sofTV.DataSetEric2TableAdapters.NueGeneralMsjTicketsTableAdapter
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents CheckBox4 As System.Windows.Forms.CheckBox
    Friend WithEvents Button19 As System.Windows.Forms.Button
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Button20 As System.Windows.Forms.Button
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents DataSetLidia2 As sofTV.DataSetLidia2
    Friend WithEvents ConGeneralAlertaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConGeneralAlertaTableAdapter As sofTV.DataSetLidia2TableAdapters.ConGeneralAlertaTableAdapter
    Friend WithEvents NueGeneralAlertaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NueGeneralAlertaTableAdapter As sofTV.DataSetLidia2TableAdapters.NueGeneralAlertaTableAdapter
    Friend WithEvents Dame_Direcciones_IpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_Direcciones_IpTableAdapter As sofTV.DataSetLidia2TableAdapters.Dame_Direcciones_IpTableAdapter
    Friend WithEvents Button21 As System.Windows.Forms.Button
    Friend WithEvents Procedimientosarnoldo4 As sofTV.Procedimientosarnoldo4
    Friend WithEvents Consulta_cobrodepositoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_cobrodepositoTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Consulta_cobrodepositoTableAdapter
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents Button23 As System.Windows.Forms.Button
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Consulta_cobrodepositobuenoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_cobrodepositobuenoTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Consulta_cobrodepositobuenoTableAdapter
    Friend WithEvents CablemodemsCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents PregcablTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DecodersCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents PregdecoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents Consulta_Rel_PaquetesDigTelBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_Rel_PaquetesDigTelTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Consulta_Rel_PaquetesDigTelTableAdapter
    Friend WithEvents Consulta_Rel_PaquetesDigTelDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents Button26 As System.Windows.Forms.Button
    Friend WithEvents Button25 As System.Windows.Forms.Button
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents NumericUpDown9 As System.Windows.Forms.NumericUpDown
    Friend WithEvents ConsecutivoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PanelDatos As System.Windows.Forms.Panel
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TELefonosTextBox As System.Windows.Forms.TextBox
    Friend WithEvents RfcTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CiudadTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ColoniaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DireccionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NombreTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Id_sucursalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PanelDatosXml As System.Windows.Forms.Panel
    Friend WithEvents Button28 As System.Windows.Forms.Button
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents DataSetyahve As sofTV.DataSetyahve
    Friend WithEvents CONSULTA_General_XmlBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONSULTA_General_XmlTableAdapter As sofTV.DataSetyahveTableAdapters.CONSULTA_General_XmlTableAdapter
    Friend WithEvents Nom_ArchivoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Fecha_InicioDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Fecha_FacturasDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Periodo_inicialTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Periodo_finalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Numero_InicioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FormatoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents RutaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Numero_LocalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Pais_LocalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clave044TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clave045TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clave01800TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clave01900TextBox As System.Windows.Forms.TextBox
    Friend WithEvents CONSULTA_General_SantanderBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONSULTA_General_SantanderTableAdapter As sofTV.DataSetyahveTableAdapters.CONSULTA_General_SantanderTableAdapter
    Friend WithEvents Panel11 As System.Windows.Forms.Panel
    Friend WithEvents ReferenciaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ConstanteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ModuloTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button30 As System.Windows.Forms.Button
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents Panel12 As System.Windows.Forms.Panel
    Friend WithEvents Button31 As System.Windows.Forms.Button
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents CONSULTA_General_HsbcBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONSULTA_General_HsbcTableAdapter As sofTV.DataSetyahveTableAdapters.CONSULTA_General_HsbcTableAdapter
    Friend WithEvents ReferenciaTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents ModuloTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents TextBoxfolio As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxserie As System.Windows.Forms.TextBox
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents TxtCdClvAdic As System.Windows.Forms.TextBox
    Friend WithEvents TxtEdoClvAdic As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Muestra_ciudadTableAdapter1 As sofTV.Procedimientosarnoldo4TableAdapters.Muestra_ciudadTableAdapter
    Friend WithEvents TabPage14 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage13 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage12 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage11 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage10 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage9 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage8 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage7 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents tbcAccesoriosInstalacion As System.Windows.Forms.TabControl
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents TextBoxNumeroExt As System.Windows.Forms.TextBox
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents TextBoxNumeroInt As System.Windows.Forms.TextBox
    Friend WithEvents ConGeneralTableAdapter1 As sofTV.Procedimientosarnoldo4TableAdapters.ConGeneralTableAdapter
    Friend WithEvents ConGeneralAntiguedadTableAdapter1 As sofTV.Procedimientosarnoldo4TableAdapters.ConGeneralAntiguedadTableAdapter
    Friend WithEvents MUESTRAPERIODOSTableAdapter1 As sofTV.Procedimientosarnoldo4TableAdapters.MUESTRAPERIODOSTableAdapter
    Friend WithEvents CONSULTAGENERALESDESCTableAdapter1 As sofTV.Procedimientosarnoldo4TableAdapters.CONSULTAGENERALESDESCTableAdapter
    Friend WithEvents MODIFCAGENERALESDESCBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents MODIFCAGENERALESDESCTableAdapter1 As sofTV.DataSetEric2TableAdapters.MODIFCAGENERALESDESCTableAdapter
    Friend WithEvents NueGeneralBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents NueGeneralTableAdapter1 As sofTV.DataSetEric2TableAdapters.NueGeneralTableAdapter
    Friend WithEvents NUEGeneralAntiguedadBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents NUEGeneralAntiguedadTableAdapter1 As sofTV.DataSetEric2TableAdapters.NUEGeneralAntiguedadTableAdapter
    Friend WithEvents TabPage15 As System.Windows.Forms.TabPage
    Friend WithEvents PnQuejas As System.Windows.Forms.Panel
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents TreViewTecnicos As System.Windows.Forms.TreeView
    Friend WithEvents CmbTecnicos As System.Windows.Forms.ComboBox
    Friend WithEvents CmBDepto As System.Windows.Forms.ComboBox
    Friend WithEvents LblFamilia As System.Windows.Forms.Label
    Friend WithEvents LblTecnico As System.Windows.Forms.Label
    Friend WithEvents LblNotas As System.Windows.Forms.Label
    Friend WithEvents ConsultaRel_Quejas_Tec_FamiliaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConsultaRel_Quejas_Tec_FamiliaTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.ConsultaRel_Quejas_Tec_FamiliaTableAdapter
    Friend WithEvents ConsultaRel_Tecnicos_QuejasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConsultaRel_Tecnicos_QuejasTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.ConsultaRel_Tecnicos_QuejasTableAdapter
    Friend WithEvents ConsultaRelTecnicosQuejasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConsultaRelQuejasTecFamiliaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTecnicosByFamiliBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestratecnicosDepartamentosAlmacenBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_tecnicosDepartamentos_AlmacenTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Muestra_tecnicosDepartamentos_AlmacenTableAdapter
    Friend WithEvents Muestra_TecnicosByFamiliTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Muestra_TecnicosByFamiliTableAdapter
    Friend WithEvents TabPage16 As System.Windows.Forms.TabPage
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Clv_Grupo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Grupo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Porcentaje As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Activo As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents LabelNota As System.Windows.Forms.Label
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents TreeViewOrdenes As System.Windows.Forms.TreeView
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents TabPage17 As System.Windows.Forms.TabPage
    Friend WithEvents TextIEPS As System.Windows.Forms.TextBox
    Friend WithEvents TextIva As System.Windows.Forms.TextBox
    Friend WithEvents Label68 As System.Windows.Forms.Label
    Friend WithEvents LblPorIeps As System.Windows.Forms.Label
    Friend WithEvents NumericUpDownIeps As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDownIva As System.Windows.Forms.NumericUpDown
    Friend WithEvents Button14 As System.Windows.Forms.Button
    Friend WithEvents ChkCalculo1 As System.Windows.Forms.CheckBox
    Friend WithEvents LabelIEPS As System.Windows.Forms.Label
    Friend WithEvents CtaIepsText As System.Windows.Forms.TextBox
    Friend WithEvents LabelCtaIEPS As System.Windows.Forms.Label
    Friend WithEvents CheckIEPS As System.Windows.Forms.CheckBox
    Friend WithEvents Label67 As System.Windows.Forms.Label
    Friend WithEvents tbEstadoCuenta As System.Windows.Forms.TabPage
    Friend WithEvents bnEstadoCuentaGuardar As System.Windows.Forms.Button
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents Label72 As System.Windows.Forms.Label
    Friend WithEvents Label65 As System.Windows.Forms.Label
    Friend WithEvents Label66 As System.Windows.Forms.Label
    Friend WithEvents Label70 As System.Windows.Forms.Label
    Friend WithEvents Label71 As System.Windows.Forms.Label
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents tbDiaGeneracion As System.Windows.Forms.TextBox
    Friend WithEvents tbDiaEnvio As System.Windows.Forms.TextBox
    Friend WithEvents tbDiaFin As System.Windows.Forms.TextBox
    Friend WithEvents tbDiaIni As System.Windows.Forms.TextBox
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents cbPeriodo As System.Windows.Forms.ComboBox
    Friend WithEvents bnGenerarEstado As System.Windows.Forms.Button
    Friend WithEvents Label74 As System.Windows.Forms.Label
    Friend WithEvents tbPrefijoOxxo As System.Windows.Forms.TextBox
    Friend WithEvents Label73 As System.Windows.Forms.Label
    Friend WithEvents tbLetraInicialBancomer As System.Windows.Forms.TextBox
    Friend WithEvents Label75 As System.Windows.Forms.Label
    Friend WithEvents Label76 As System.Windows.Forms.Label
    Friend WithEvents tbConvenioBancomer As System.Windows.Forms.TextBox
    Friend WithEvents tbpAccesoriosInstalacion As System.Windows.Forms.TabPage
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents dgvRelArticuloAccesorioInst As System.Windows.Forms.DataGridView
    Friend WithEvents cmbAccesorioInst As System.Windows.Forms.ComboBox
    Friend WithEvents Label78 As System.Windows.Forms.Label
    Friend WithEvents Label77 As System.Windows.Forms.Label
    Friend WithEvents cmbDescArticulo As System.Windows.Forms.ComboBox
    Friend WithEvents cmbClave As System.Windows.Forms.ComboBox
    Friend WithEvents cmbClasifMaterial As System.Windows.Forms.ComboBox
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents lblClave As System.Windows.Forms.Label
    Friend WithEvents lblClasificacion As System.Windows.Forms.Label
    Friend WithEvents cmbClasificacionArticulos As System.Windows.Forms.ComboBox
    Friend WithEvents Label80 As System.Windows.Forms.Label
    Friend WithEvents Label79 As System.Windows.Forms.Label
    Friend WithEvents cmbTipoDeCaja As System.Windows.Forms.ComboBox
    Friend WithEvents Label81 As System.Windows.Forms.Label
    Friend WithEvents ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents idTipoCaja As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Accesorio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Articulo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tpCobroMaterial As System.Windows.Forms.TabPage
    Friend WithEvents Panel14 As System.Windows.Forms.Panel
    Friend WithEvents dgvRangosMaterial As System.Windows.Forms.DataGridView
    Friend WithEvents identity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RangoIni As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RangoFinal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NumPagos As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnEliminarRango As System.Windows.Forms.Button
    Friend WithEvents btnAgregarRango As System.Windows.Forms.Button
    Friend WithEvents lblRangoFin As System.Windows.Forms.Label
    Friend WithEvents lblRangoIni As System.Windows.Forms.Label
    Friend WithEvents txtRango2 As System.Windows.Forms.TextBox
    Friend WithEvents txtRango1 As System.Windows.Forms.TextBox
    Friend WithEvents nudPagos As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblPagos As System.Windows.Forms.Label
    Friend WithEvents TextBox17 As System.Windows.Forms.TextBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Mizar As System.Windows.Forms.TabPage
    Friend WithEvents lblUltFolioG As System.Windows.Forms.Label
    Friend WithEvents txtUltFolioG As System.Windows.Forms.TextBox
    Friend WithEvents lblSerieG As System.Windows.Forms.Label
    Friend WithEvents txtSerieG As System.Windows.Forms.TextBox
    Friend WithEvents cmbCompanias As System.Windows.Forms.ComboBox
    Friend WithEvents lblCompania As System.Windows.Forms.Label
    Friend WithEvents lblTituloG As System.Windows.Forms.Label
    Friend WithEvents lblTituloF As System.Windows.Forms.Label
    Friend WithEvents lblUltFolioF As System.Windows.Forms.Label
    Friend WithEvents txtUltFolioF As System.Windows.Forms.TextBox
    Friend WithEvents lblSerieF As System.Windows.Forms.Label
    Friend WithEvents txtSerieF As System.Windows.Forms.TextBox
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents cbxMizar As System.Windows.Forms.CheckBox
    Friend WithEvents cbxMizarS As System.Windows.Forms.CheckBox
    Friend WithEvents lblLeyenda As System.Windows.Forms.Label
    Friend WithEvents TabOxxo As System.Windows.Forms.TabPage
    Friend WithEvents txtPrefijo As System.Windows.Forms.TextBox
    Friend WithEvents Label83 As System.Windows.Forms.Label
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents txtComisionOxxo As System.Windows.Forms.TextBox
    Friend WithEvents Label82 As System.Windows.Forms.Label
    Friend WithEvents bpxImpresiones As System.Windows.Forms.GroupBox
    Friend WithEvents cbxImprimeOrdenesFac As System.Windows.Forms.CheckBox
    Friend WithEvents lblAvisoImpresiones As System.Windows.Forms.Label
    Friend WithEvents cbxImprimeOrdenesSoftv As System.Windows.Forms.CheckBox
    Friend WithEvents cbxImprimeQuejasSoftv As System.Windows.Forms.CheckBox
    Friend WithEvents lblEncabezadoImpresion As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents TabPageCorteSistema As System.Windows.Forms.TabPage
    Friend WithEvents TextBoxMensajeCorte As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxMensajeAvisoCorte As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxDiasAvisoCorte As System.Windows.Forms.TextBox
    Friend WithEvents CheckBoxCorteSATV As System.Windows.Forms.CheckBox
    Friend WithEvents TextBoxFecCorteSatv As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Button22 As System.Windows.Forms.Button
    Friend WithEvents Label99 As System.Windows.Forms.Label
    'Friend WithEvents Inserta_Gral_factura_globalBindingSource As System.Windows.Forms.BindingSource
    'Friend WithEvents Inserta_Gral_factura_globalTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_Gral_factura_globalTableAdapter
End Class
