﻿Imports System.Data.SqlClient
Public Class FrmCabModPropio

    Private Sub FrmCabModPropio_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        Try
            Me.MUESTRACABLEDELCLIbajaTableAdapter.Connection = CON
            Me.MUESTRACABLEDELCLIbajaTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACABLEDELCLIbaja, New System.Nullable(Of Long)(CType(Contrato, Long)))
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        GloClv_Cablemodem = 0
        Me.Close()
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        Me.Panel1.Visible = False
        Me.Panel3.Visible = True
    End Sub

    Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Panel1.Visible = True
        Me.Panel3.Visible = False
    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        'Me.Panel1.Visible = True
        'Me.Panel3.Visible = False
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.RadioButton1.Checked = True Then
            If IsNumeric(Me.ComboBox1.SelectedValue) = True Then
                GLOMOVNET = 2
                GloClv_Cablemodem = 0
                GloContratonet = Me.ComboBox1.SelectedValue
                GloClv_Servicio = 0
                GloClv_TipSer = 2
                FrmSelServicios.Show()
                Me.Close()
            Else
                MsgBox("No a Seleccionado la Mac de algún Aparato")
            End If
        ElseIf Me.RadioButton2.Checked = True Then

        End If

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Dim resp As MsgBoxResult = MsgBoxResult.Cancel
            Dim Respuesta As Integer = 0
            Me.ValidaCablemodemTableAdapter.Connection = CON
            Me.ValidaCablemodemTableAdapter.Fill(Me.NewSofTvDataSet.ValidaCablemodem, Me.mac.Text, New System.Nullable(Of Long)(CType(Contrato, Long)), GloClv_Cablemodem, Respuesta)
            'Respuesta = 1
            'No Existe en el Catalogo se requiere dar de Alta
            'Respuesta = 2
            'Existe en el Catalogo pero no esta asignado a ningun Cliente y esta Disponible 
            'Respuesta = 3
            'Existe en el Catalogo pero no esta asignado a ningun Cliente y esta Instalado
            'Respuesta = 4
            'Existe en el Catalogo pero no esta asignado a ningun Cliente y esta Baja
            'Respuesta = 5
            'Existe en el Catalogo pero no esta asignado a ningun Cliente y esta Dañado
            'Respuesta = 6
            'Existe en el Catalogo y Esta Asignado a un Cliente y es el mismo Contrato
            'Respuesta = 7
            'Existe en el Catalogo y Esta Asignado a un Cliente y es diferente Cliente y el Status del Aparato esta en Baja
            'Respuesta = 8
            'Existe en el Catalogo y Esta Asignado a un Cliente y es diferente Cliente y el Status del Aparato es Diferente de Baja
            If Me.mac.Text.Length = 14 Then
                Select Case Respuesta
                    Case 1
                        resp = MsgBox("La Mac que Tecleo no Existe en nuestro Catalogo ¿ Desea dar de Alta esa Mac ? ", MsgBoxStyle.YesNo)
                        If resp = MsgBoxResult.Yes Then
                            Me.AGREGAMACTableAdapter.Connection = CON
                            Me.AGREGAMACTableAdapter.Fill(Me.NewSofTvDataSet.AGREGAMAC, Me.mac.Text, GloClv_Cablemodem)
                            GLOMOVNET = 1
                            De_Internet = True
                            FrmSelServicios.Show()
                            Me.Close()
                        End If
                    Case 2
                        resp = MsgBox("La Mac que Tecleo esta Disponible ¿ Desea Asignar esta Mac al Cliente ? ", MsgBoxStyle.YesNo)
                        If resp = MsgBoxResult.Yes Then
                            If resp = MsgBoxResult.Yes Then
                                GLOMOVNET = 1
                                De_Internet = True
                                FrmSelServicios.Show()

                                Me.Close()
                            End If
                        End If
                    Case 3
                        MsgBox("Actualmente esa Mac no esta Asignada a ningun Cliente, Pero aparece como Instalado en el Catalogo. ! Por lo cual no se le Puede Asignar ¡")
                    Case 4
                        MsgBox("Actualmente esa Mac no esta Asignada a ningun Cliente, Pero aparece como Baja en el Catalogo. ! Por lo cual no se le Puede Asignar ¡")
                    Case 5
                        MsgBox("Actualmente esa Mac no esta Asignada a ningun Cliente, Pero aparece como Dañado ó en Reparación en el Catalogo. ! Por lo cual no se le Puede Asignar ¡")
                    Case 6
                        MsgBox("Actualmente esa Mac ya esta asignada a este Cliente")
                    Case 7
                        resp = MsgBox("La Mac que Tecleo esta Asignada a Otro Cliente pero el Cliente tiene la Mac en Baja ¿ Desea Asignar esta Mac al Cliente ? ", MsgBoxStyle.YesNo)
                        If resp = MsgBoxResult.Yes Then
                            If resp = MsgBoxResult.Yes Then
                                GLOMOVNET = 1
                                De_Internet = True
                                FrmSelServicios.Show()
                                De_Internet = True
                                Me.Close()
                            End If
                        End If
                    Case 8
                        MsgBox("La Mac que Tecleo esta Asignada a Otro Cliente y tiene esa Mac Activa. ! Por lo cual no se le Puede Asignar ¡ ")
                End Select
            Else
                MsgBox("Captura los 12 Dígitos de la MAC", , "Atención")
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub FillToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)


    End Sub

    Private Sub FillToolStripButton_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)


    End Sub

    Private Sub RadioButton3_CheckedChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton3.CheckedChanged
        Me.Panel1.Visible = False
        Me.Panel3.Visible = True
    End Sub


End Class