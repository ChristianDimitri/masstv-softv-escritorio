Imports System.Data.SqlClient
Imports System.Text

Public Class FrmContratoMaestro

    Private Sub Button1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eOpcion = "N"
        FrmSubContrato.Show()
    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        Try
            If Me.ConContratoMaestroDataGridView.RowCount > 0 Then
                eOpcion = "C"
                eGloContratoAux = ConContratoMaestroDataGridView.SelectedCells(0).Value
                FrmSubContrato.Show()
            Else
                MsgBox("No Existen Contratos Maestros a Consultar.", , "Atenci�n")
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
        
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Try
            If Me.ConContratoMaestroDataGridView.RowCount > 0 Then
                eOpcion = "M"
                eGloContratoAux = ConContratoMaestroDataGridView.SelectedCells(0).Value
                FrmSubContrato.Show()
            Else
                MsgBox("No Existen Contratos Maestros a Modificar.", , "Atenci�n")
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
       
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
    End Sub

    Private Sub FrmContratoMaestro_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'Me.ConContratoMaestroTableAdapter.Connection = CON
        'Me.ConContratoMaestroTableAdapter.Fill(Me.DataSetEric.ConContratoMaestro, 0, 0, "", "", "", "", 3)
        CON.Close()
    End Sub

    Private Sub FrmContratoMaestro_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Label1.ForeColor = Color.Black
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        'Me.ConContratoMaestroTableAdapter.Connection = CON
        'Me.ConContratoMaestroTableAdapter.Fill(Me.DataSetEric.ConContratoMaestro, 0, 0, "", "", "", "", 3)
        BUSCACONTRATOPORSEPARADO(0, 0, "", "", "", "", "", "", "", "", 3)
        CON.Close()
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.TextBox1.Text) = True Then
            'Me.ConContratoMaestroTableAdapter.Connection = CON
            'Me.ConContratoMaestroTableAdapter.Fill(Me.DataSetEric.ConContratoMaestro, Me.TextBox1.Text, 0, "", "", "", "", 0)
            BUSCACONTRATOPORSEPARADO(Me.TextBox1.Text, 0, "", "", "", "", "", "", "", "", 0)
        End If
        CON.Close()
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Asc(e.KeyChar) = 13 Then
            If IsNumeric(Me.TextBox1.Text) = True Then
                'Me.ConContratoMaestroTableAdapter.Connection = CON
                'Me.ConContratoMaestroTableAdapter.Fill(Me.DataSetEric.ConContratoMaestro, Me.TextBox1.Text, 0, "", "", "", "", 0)
                BUSCACONTRATOPORSEPARADO(Me.TextBox1.Text, 0, "", "", "", "", "", "", "", "", 0)
            End If
        End If
        CON.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
 
        If Me.TextBox2.Text.Length > 0 Or Len(Trim(Me.APaternoTextBox.Text)) > 0 Or Len(Trim(Me.AMaternoTextBox.Text)) > 0 Then
            BUSCACONTRATOPORSEPARADO(0, 0, Me.TextBox2.Text, Me.APaternoTextBox.Text, Me.AMaternoTextBox.Text, "", "", "", "", "", 1)
        End If
        CON.Close()
        TextBox2.Text = ""
        APaternoTextBox.Text = ""
        AMaternoTextBox.Text = ""
        CON.Close()
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Asc(e.KeyChar) = 13 Then
        If Me.TextBox2.Text.Length > 0 Or Len(Trim(Me.APaternoTextBox.Text)) > 0 Or Len(Trim(Me.AMaternoTextBox.Text)) > 0 Then
                BUSCACONTRATOPORSEPARADO(0, 0, Me.TextBox2.Text, Me.APaternoTextBox.Text, Me.AMaternoTextBox.Text, "", "", "", "", "", 1)
            End If
        End If
        CON.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
        If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
        If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
        'Me.ConContratoMaestroTableAdapter.Connection = CON
        'Me.ConContratoMaestroTableAdapter.Fill(Me.DataSetEric.ConContratoMaestro, 0, 0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2)
        BUSCACONTRATOPORSEPARADO(0, 0, "", "", "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, "", "", 2)
        CON.Close()
    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Asc(e.KeyChar) = 13 Then
            If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
            If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
            If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
            'Me.ConContratoMaestroTableAdapter.Connection = CON
            'Me.ConContratoMaestroTableAdapter.Fill(Me.DataSetEric.ConContratoMaestro, 0, 0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2)
            BUSCACONTRATOPORSEPARADO(0, 0, "", "", "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, "", "", 2)
        End If
        CON.Close()
    End Sub

    Private Sub TextBox4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox4.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Asc(e.KeyChar) = 13 Then
            If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
            If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
            If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
            'Me.ConContratoMaestroTableAdapter.Connection = CON
            'Me.ConContratoMaestroTableAdapter.Fill(Me.DataSetEric.ConContratoMaestro, 0, 0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2)
            BUSCACONTRATOPORSEPARADO(0, 0, "", "", "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, "", "", 2)
        End If
        CON.Close()
    End Sub

    Private Sub TextBox5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox5.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Asc(e.KeyChar) = 13 Then
            If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
            If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
            If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
            'Me.ConContratoMaestroTableAdapter.Connection = CON
            'Me.ConContratoMaestroTableAdapter.Fill(Me.DataSetEric.ConContratoMaestro, 0, 0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2)
            BUSCACONTRATOPORSEPARADO(0, 0, "", "", "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, "", "", 2)
        End If
        CON.Close()
    End Sub
    Private Sub BUSCACONTRATOPORSEPARADO(ByVal ContratoCli As Integer, ByVal SubContrato As Integer, ByVal NombreCli As String, ByVal APaternoCli As String, ByVal AMaternoCli As String, _
                                         ByVal CalleCli As String, ByVal NumCli As String, ByVal Ciudad As String, ByVal SetUpBox As String, ByVal Tarjeta As String, ByVal OpCli As Integer)

        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder

        StrSQL.Append("EXEC ConContratoMaestroPORSEPARADO ")
        StrSQL.Append(CStr(ContratoCli) & ",")
        StrSQL.Append(CStr(SubContrato) & ",")
        StrSQL.Append("'" & NombreCli & "',")
        StrSQL.Append("'" & APaternoCli & "',")
        StrSQL.Append("'" & AMaternoCli & "',")
        StrSQL.Append("'" & CalleCli & "',")
        StrSQL.Append("'" & NumCli & "',")
        StrSQL.Append("'" & Ciudad & "',")
        StrSQL.Append("'" & SetUpBox & "', ")
        StrSQL.Append("'" & Tarjeta & "', ")
        StrSQL.Append(CStr(OpCli))


        Dim DA As New SqlDataAdapter(StrSQL.ToString, CON)
        Dim DT As New DataTable
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.ConContratoMaestroDataGridView.DataSource = BS.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub ConContratoMaestroDataGridView_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles ConContratoMaestroDataGridView.CellClick
        CONTRATOLabel1.Text = Me.ConContratoMaestroDataGridView.SelectedCells(0).Value
        NOMBRELabel1.Text = Me.ConContratoMaestroDataGridView.SelectedCells(1).Value
        CALLELabel1.Text = Me.ConContratoMaestroDataGridView.SelectedCells(2).Value
        COLONIALabel1.Text = Me.ConContratoMaestroDataGridView.SelectedCells(3).Value
        NUMEROLabel1.Text = Me.ConContratoMaestroDataGridView.SelectedCells(4).Value
        CIUDADLabel1.Text = Me.ConContratoMaestroDataGridView.SelectedCells(5).Value

    End Sub


    Private Sub ConContratoMaestroDataGridView_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles ConContratoMaestroDataGridView.CellContentClick

    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        If Me.TxtSetUpBox.Text.Length > 0 Or Len(Trim(Me.TxtTarjeta.Text)) > 0 Then
            BUSCACONTRATOPORSEPARADO(0, 0, "", "", "", "", "", "", Me.TxtSetUpBox.Text, Me.TxtTarjeta.Text, 4)
        End If
        TxtSetUpBox.Text = ""
        TxtTarjeta.Text = ""

    End Sub
End Class