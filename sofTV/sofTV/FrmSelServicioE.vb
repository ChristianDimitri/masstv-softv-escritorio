Imports System.Data.SqlClient
Public Class FrmSelServicioE

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim CON As New SqlConnection(MiConexion)
        If Me.DescripcionListBox.Items.Count > 0 Then
            CON.Open()
            Me.InsertarServiciosTmpTableAdapter.Connection = CON
            Me.InsertarServiciosTmpTableAdapter.Fill(Me.DataSetEric2.InsertarServiciosTmp, CInt(Me.DescripcionListBox.SelectedValue), eClv_Session, 0)
            Me.ConServiciosProTableAdapter.Connection = CON
            Me.ConServiciosProTableAdapter.Fill(Me.DataSetEric2.ConServiciosPro, 0, eClv_Session, 1)
            Me.ConServiciosTmpTableAdapter.Connection = CON
            Me.ConServiciosTmpTableAdapter.Fill(Me.DataSetEric2.ConServiciosTmp, eClv_Session)
            CON.Close()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim CON As New SqlConnection(MiConexion)
        If Me.DescripcionListBox.Items.Count > 0 Then
            CON.Open()
            Me.InsertarServiciosTmpTableAdapter.Connection = CON
            Me.InsertarServiciosTmpTableAdapter.Fill(Me.DataSetEric2.InsertarServiciosTmp, 0, eClv_Session, 1)
            Me.ConServiciosProTableAdapter.Connection = CON
            Me.ConServiciosProTableAdapter.Fill(Me.DataSetEric2.ConServiciosPro, 0, eClv_Session, 1)
            Me.ConServiciosTmpTableAdapter.Connection = CON
            Me.ConServiciosTmpTableAdapter.Fill(Me.DataSetEric2.ConServiciosTmp, eClv_Session)
            CON.Close()
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim CON As New SqlConnection(MiConexion)
        If Me.DescripcionListBox1.Items.Count > 0 Then
            CON.Open()
            Me.BorrarServiciosTmpTableAdapter.Connection = CON
            Me.BorrarServiciosTmpTableAdapter.Fill(Me.DataSetEric2.BorrarServiciosTmp, CInt(Me.DescripcionListBox1.SelectedValue), eClv_Session, 0)
            Me.ConServiciosProTableAdapter.Connection = CON
            Me.ConServiciosProTableAdapter.Fill(Me.DataSetEric2.ConServiciosPro, 0, eClv_Session, 1)
            Me.ConServiciosTmpTableAdapter.Connection = CON
            Me.ConServiciosTmpTableAdapter.Fill(Me.DataSetEric2.ConServiciosTmp, eClv_Session)
            CON.Close()
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim CON As New SqlConnection(MiConexion)
        If Me.DescripcionListBox1.Items.Count > 0 Then
            CON.Open()
            Me.BorrarServiciosTmpTableAdapter.Connection = CON
            Me.BorrarServiciosTmpTableAdapter.Fill(Me.DataSetEric2.BorrarServiciosTmp, 0, eClv_Session, 1)
            Me.ConServiciosProTableAdapter.Connection = CON
            Me.ConServiciosProTableAdapter.Fill(Me.DataSetEric2.ConServiciosPro, 0, eClv_Session, 1)
            Me.ConServiciosTmpTableAdapter.Connection = CON
            Me.ConServiciosTmpTableAdapter.Fill(Me.DataSetEric2.ConServiciosTmp, eClv_Session)
            CON.Close()
        End If
    End Sub

    Private Sub FrmSelServicioE_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        If gloClvTipSerReportes > 0 Then
            eTipSer = gloClvTipSerReportes
            gloClvTipSerReportes = 0
        End If
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConServiciosProTableAdapter.Connection = CON
        Me.ConServiciosProTableAdapter.Fill(Me.DataSetEric2.ConServiciosPro, eTipSer, eClv_Session, 0)
        CON.Close()
        'eTipSer = 0
    End Sub

    Private Sub Aceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Aceptar.Click

        If (FrmFiltroSector.Clv_Sector > 0) Then
            Me.Close()
            Me.Dispose()
            Exit Sub
        End If

        If Me.DescripcionListBox1.Items.Count > 0 Then 'And (eOpVentas = 2 Or eOpVentas = 3 Or eOpVentas = 20 Or eOpVentas = 21) Then
            FrmImprimirComision.Show()
            Me.Close()
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
End Class