﻿Imports System.Collections.Generic

Public Class classTipoInstalacion

#Region "Propiedades"
    Private _idTipoInstalacion As Integer
    Public Property idTipoInstalacion As Integer
        Get
            Return _idTipoInstalacion
        End Get
        Set(value As Integer)
            _idTipoInstalacion = value
        End Set
    End Property

    Private _opActivo As Integer
    Public Property opActivo As Integer
        Get
            Return _opActivo
        End Get
        Set(value As Integer)
            _opActivo = value
        End Set
    End Property

    Private _descripcionTipo As String
    Public Property descripcionTipo As String
        Get
            Return _descripcionTipo
        End Get
        Set(value As String)
            _descripcionTipo = value
        End Set
    End Property

    Private _activoTipo As Boolean
    Public Property activoTipo As Boolean
        Get
            Return _activoTipo
        End Get
        Set(value As Boolean)
            _activoTipo = value
        End Set
    End Property

    Private _opReporte As Integer
    Public Property opReporte As Integer
        Get
            Return _opReporte
        End Get
        Set(value As Integer)
            _opReporte = value
        End Set
    End Property

    Private _nomReporte As String
    Public Property nomReporte As String
        Get
            Return _nomReporte
        End Get
        Set(value As String)
            _nomReporte = value
        End Set
    End Property

    Private _fechaInicial As Date
    Public Property fechaInicial As Date
        Get
            Return _fechaInicial
        End Get
        Set(value As Date)
            _fechaInicial = value
        End Set
    End Property

    Private _fechaFinal As Date
    Public Property fechaFinal As Date
        Get
            Return _fechaFinal
        End Get
        Set(value As Date)
            _fechaFinal = value
        End Set
    End Property

    Private _contrato As Long
    Public Property contrato As Long
        Get
            Return _contrato
        End Get
        Set(value As Long)
            _contrato = value
        End Set
    End Property
#End Region

#Region "Constructores"
    Public Sub New()
        _descripcionTipo = ""
        _activoTipo = False
        _idTipoInstalacion = 0
        _opActivo = 0
        _opReporte = 0
        _nomReporte = ""
        _fechaInicial = "01/01/1900"
        _fechaFinal = "01/01/1900"
    End Sub
#End Region

#Region "Métodos"
    Dim tipoInstalacion As BaseIII
    Dim dataSet As DataSet
    Dim listaTablas As List(Of String)

    Public Function uspInsertaTblTipoInstalacion() As DataTable
        Try
            tipoInstalacion = New BaseIII
            tipoInstalacion.limpiaParametros()
            tipoInstalacion.CreateMyParameter("@descripcion", SqlDbType.VarChar, _descripcionTipo, 250)
            tipoInstalacion.CreateMyParameter("@activo", SqlDbType.Bit, _activoTipo)
            uspInsertaTblTipoInstalacion = tipoInstalacion.ConsultaDT("uspInsertaTblTipoInstalacion")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub uspModificaTblTipoInstalacion()
        Try
            tipoInstalacion = New BaseIII
            tipoInstalacion.limpiaParametros()
            tipoInstalacion.CreateMyParameter("@idTipoInstalacion", SqlDbType.Int, _idTipoInstalacion)
            tipoInstalacion.CreateMyParameter("@descripcion", SqlDbType.VarChar, _descripcionTipo, 250)
            tipoInstalacion.CreateMyParameter("@activo", SqlDbType.Bit, _activoTipo)
            tipoInstalacion.Inserta("uspModificaTblTipoInstalacion")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function uspConsultaTblTipoInstalacion() As DataTable
        Try
            tipoInstalacion = New BaseIII
            tipoInstalacion.limpiaParametros()
            tipoInstalacion.CreateMyParameter("@idTipoInstalacion", SqlDbType.Int, _idTipoInstalacion)
            tipoInstalacion.CreateMyParameter("@descripcion", SqlDbType.VarChar, _descripcionTipo, 250)
            tipoInstalacion.CreateMyParameter("@opActivo", SqlDbType.Int, _opActivo)
            uspConsultaTblTipoInstalacion = tipoInstalacion.ConsultaDT("uspConsultaTblTipoInstalacion")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function uspEliminaTblTipoInstalacion() As Boolean
        Try
            tipoInstalacion = New BaseIII
            tipoInstalacion.limpiaParametros()
            tipoInstalacion.CreateMyParameter("@idTipoInstalacion", SqlDbType.Int, _idTipoInstalacion)
            tipoInstalacion.CreateMyParameter("@bndEliminar", ParameterDirection.Output, SqlDbType.Bit)
            tipoInstalacion.ProcedimientoOutPut("uspEliminaTblTipoInstalacion")
            uspEliminaTblTipoInstalacion = CBool(tipoInstalacion.dicoPar("@bndEliminar").ToString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function uspConsultaTipoInstalacionCombo() As DataTable
        Try
            tipoInstalacion = New BaseIII
            tipoInstalacion.limpiaParametros()
            tipoInstalacion.CreateMyParameter("@op", SqlDbType.Int, _opActivo)
            uspConsultaTipoInstalacionCombo = tipoInstalacion.ConsultaDT("uspConsultaTipoInstalacionCombo")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub uspReporteTblTipoInstalacion() 'REPORTE INTERFAZ DIGITAL
        Try
            tipoInstalacion = New BaseIII
            dataSet = New DataSet
            listaTablas = New List(Of String)
            listaTablas.Add("ReporteTblTipoInstalacion")

            tipoInstalacion.limpiaParametros()
            tipoInstalacion.CreateMyParameter("@opTipo", SqlDbType.Int, _opReporte)
            tipoInstalacion.CreateMyParameter("@fechaInicial", SqlDbType.DateTime, _fechaInicial)
            tipoInstalacion.CreateMyParameter("@fechaFinal", SqlDbType.DateTime, _fechaFinal)
            dataSet = tipoInstalacion.ConsultaDS("uspReporteTblTipoInstalacion", listaTablas)

            tipoInstalacion.llamarReporteCentralizado(_nomReporte, dataSet)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub uspInsertaTblRelClientesTipoInstalacion()
        Try
            tipoInstalacion = New BaseIII
            tipoInstalacion.limpiaParametros()
            tipoInstalacion.CreateMyParameter("@contrato", SqlDbType.Int, _contrato)
            tipoInstalacion.CreateMyParameter("@clvTipoInstalacion", SqlDbType.Int, _idTipoInstalacion)
            tipoInstalacion.Inserta("uspInsertaTblRelClientesTipoInstalacion")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function uspConsultaTblRelClientesTipoInstalacion() As Integer
        Try
            tipoInstalacion = New BaseIII
            tipoInstalacion.limpiaParametros()
            tipoInstalacion.CreateMyParameter("@contrato", SqlDbType.Int, _contrato)
            tipoInstalacion.CreateMyParameter("@clvTipoInstalacion", ParameterDirection.Output, SqlDbType.Int)
            tipoInstalacion.ProcedimientoOutPut("uspConsultaTblRelClientesTipoInstalacion")
            uspConsultaTblRelClientesTipoInstalacion = CInt(tipoInstalacion.dicoPar("@clvTipoInstalacion").ToString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region
End Class
