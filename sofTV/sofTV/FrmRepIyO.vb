﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Net
Imports System.Net.Sockets
Imports System.IO
Imports System
Imports System.IO.StreamReader
Imports System.IO.File
Imports System.Collections.Generic


Public Class FrmRepIyO
    Private customersByCityReport As ReportDocument

    Private Sub BtnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAceptar.Click

    End Sub


    Private Sub ConfigureCrystalReportsNewPPE()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing


            Dim eFecha As String = Nothing
            eFecha = "Del " & eFechaIni & " al " & eFechaFin
            reportPath = RutaReportes + "\ReportServiciosPPE.rpt"
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            'FechaIni
            customersByCityReport.SetParameterValue(0, eFechaIni)
            'FechaFin
            customersByCityReport.SetParameterValue(1, eFechaFin)


            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait

            'CrystalReportViewer1.ReportSource = customersByCityReport
            'CrystalReportViewer1.Zoom(75)




            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & GloSucursal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFecha & "'"
            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class