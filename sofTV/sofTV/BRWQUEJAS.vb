﻿Imports System.Data.SqlClient
Imports System.Text
Imports sofTV.Base

Public Class BRWQUEJAS
    Public consultas As New CBase
    'SAUL
    Private eRespuesta As Integer = 0
    Private eMensaje As String = Nothing
    Dim bndAcceso As Integer = 0

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opcion = "N"
        GloClv_TipSer = 0
        FrmQueja.Show()
    End Sub

    Private Sub consultar()
        If Me.DataGridView1.SelectedCells.Item(0).Value > 0 Then
            opcion = "C"
            gloClave = Me.DataGridView1.SelectedCells.Item(1).Value
            GloClv_TipSer = Me.DataGridView1.SelectedCells(0).Value
            FrmQueja.Show()
        Else
            MsgBox(mensaje2)
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If DataGridView1.RowCount = 0 Then
            MessageBox.Show("Selecciona una Queja.")
            Exit Sub
        End If
        consultar()
    End Sub

    Private Sub modificar()
        If Me.DataGridView1.SelectedCells.Item(0).Value > 0 Then
            opcion = "M"
            gloClave = Me.DataGridView1.SelectedCells.Item(1).Value
            GloClv_TipSer = Me.DataGridView1.SelectedCells(0).Value
            FrmQueja.Show()
        Else
            MsgBox("Seleccione algun Tipo de Servicio")
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click

        If DataGridView1.RowCount = 0 Then
            MessageBox.Show("Selecciona una Queja.")
            Exit Sub
        End If

        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'If IsNumeric(Me.ContratoLabel1.Text) Then
        Me.BuscaBloqueadoTableAdapter.Connection = CON
        Me.BuscaBloqueadoTableAdapter.Fill(Me.DataSetLidia.BuscaBloqueado, CInt(DataGridView1.SelectedCells(3).Value.ToString), NUM, num2)
        If NUM = 0 Then
            'SAUL
            bndAcceso = 0
            uspAutorizadoQuejasOrdenes()
            If bndAcceso = 0 Then
                eRespuesta = 0
                eMensaje = ""
                uspValidaOrdenes()
                If eRespuesta = 1 Then
                    MsgBox("El Status del Cliente con el Serv. Digital es Moroso ó Baja!. No se Puede Ejecutar las Quejas!")
                    Exit Sub
                Else
                    modificar()
                End If
            Else
                modificar()
            End If
            eRespuesta = 0
            eMensaje = ""
            '(Fin)
            'modificar()
        ElseIf num2 = 1 Then
            MsgBox("El Cliente " + Me.ContratoLabel1.Text + " Ha Sido Bloqueado por lo que no se Podrá Ejecutar la Orden ", MsgBoxStyle.Exclamation)
        End If
        CON.Close()
        'Else
        '    MsgBox("Seleccione al menos un Contrato")
        'End If


    End Sub

    Private Sub Busca(ByVal op As Integer)
        Dim sTATUS As String = "P"
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            If Me.RadioButton1.Checked = True Then
                sTATUS = "P"
            ElseIf Me.RadioButton2.Checked = True Then
                sTATUS = "E"
            ElseIf Me.RadioButton3.Checked = True Then
                sTATUS = "V"
            ElseIf Me.rbEnProceso.Checked = True Then
                sTATUS = "S"
            End If

            If op = 0 Then 'contrato
                If IsNumeric(Me.TextBox1.Text) = True Then
                    'Me.BUSCAQUEJASTableAdapter.Connection = CON
                    'Me.BUSCAQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAQUEJAS, New System.Nullable(Of Integer)(CType(Me.ComboBox1.SelectedValue, Integer)), 0, Me.TextBox1.Text, "", "", "", New System.Nullable(Of Integer)(CType(0, Integer)))
                    'Me.BUSCAQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAQUEJAS, 0, 0, Me.TextBox1.Text, "", "", "", New System.Nullable(Of Integer)(CType(0, Integer)))
                    BuscaQuejasSeparado(Me.ComboBox1.SelectedValue, 0, Me.TextBox1.Text, "", "", "", "", "", "", "", 0, 0)
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If

            ElseIf op = 1 Then
                If Len(Trim(Me.TextBox2.Text)) > 0 Or Len(Trim(Me.APaternoTextBox.Text)) > 0 Or Len(Trim(Me.AMaternoTextBox.Text)) > 0 Then
                    'Me.BUSCAQUEJASTableAdapter.Connection = CON
                    'Me.BUSCAQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAQUEJAS, New System.Nullable(Of Integer)(CType(Me.ComboBox1.SelectedValue, Integer)), 0, 0, Me.TextBox2.Text, "", "", New System.Nullable(Of Integer)(CType(1, Integer)))
                    'Me.BUSCAQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAQUEJAS, 0, 0, 0, Me.TextBox2.Text, "", "", New System.Nullable(Of Integer)(CType(1, Integer)))
                    BuscaQuejasSeparado(Me.ComboBox1.SelectedValue, 0, 0, Me.TextBox2.Text, Me.APaternoTextBox.Text, Me.AMaternoTextBox.Text, "", "", "", "", 1, 0)
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If
            ElseIf op = 2 Then 'Calle y numero
                'Me.BUSCAQUEJASTableAdapter.Connection = CON
                'Me.BUSCAQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAQUEJAS, New System.Nullable(Of Integer)(CType(Me.ComboBox1.SelectedValue, Integer)), 0, 0, "", Me.BCALLE.Text, Me.BNUMERO.Text, New System.Nullable(Of Integer)(CType(2, Integer)))
                BuscaQuejasSeparado(Me.ComboBox1.SelectedValue, 0, 0, "", "", "", Me.BCALLE.Text, Me.BNUMERO.Text, "", "", 2, 0)
                ' Me.BUSCAQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAQUEJAS, 0, 0, 0, "", Me.BCALLE.Text, Me.BNUMERO.Text, New System.Nullable(Of Integer)(CType(2, Integer)))
            ElseIf op = 199 Then 'Status
                'Me.BUSCAQUEJASTableAdapter.Connection = CON
                'Me.BUSCAQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAQUEJAS, New System.Nullable(Of Integer)(CType(Me.ComboBox1.SelectedValue, Integer)), 0, 0, sTATUS, "", "", New System.Nullable(Of Integer)(CType(199, Integer)))
                'Me.BUSCAQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAQUEJAS, 0, 0, 0, sTATUS, "", "", New System.Nullable(Of Integer)(CType(199, Integer)))
                BuscaQuejasSeparado(Me.ComboBox1.SelectedValue, 0, 0, sTATUS, "", "", "", "", "", "", 199, 0)
            ElseIf op = 3 Then 'clv_Orden
                If IsNumeric(Me.TextBox3.Text) = True Then
                    'Me.BUSCAQUEJASTableAdapter.Connection = CON
                    'Me.BUSCAQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAQUEJAS, New System.Nullable(Of Integer)(CType(Me.ComboBox1.SelectedValue, Integer)), Me.TextBox3.Text, 0, "", "", "", New System.Nullable(Of Integer)(CType(3, Integer)))
                    'Me.BUSCAQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAQUEJAS, 0, Me.TextBox3.Text, 0, "", "", "", New System.Nullable(Of Integer)(CType(3, Integer)))
                    BuscaQuejasSeparado(Me.ComboBox1.SelectedValue, Me.TextBox3.Text, 0, "", "", "", "", "", "", "", 3, 0)
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If

                'ElseIf op = 3 Then 'clv_Orden
                '    If IsNumeric(Me.TextBox3.Text) = True Then
                '        'Me.BUSCAQUEJASTableAdapter.Connection = CON
                '        'Me.BUSCAQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAQUEJAS, New System.Nullable(Of Integer)(CType(Me.ComboBox1.SelectedValue, Integer)), Me.TextBox3.Text, 0, "", "", "", New System.Nullable(Of Integer)(CType(3, Integer)))
                '        'Me.BUSCAQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAQUEJAS, 0, Me.TextBox3.Text, 0, "", "", "", New System.Nullable(Of Integer)(CType(3, Integer)))
                '        BuscaQuejasSeparado(Me.ComboBox1.SelectedValue, Me.TextBox3.Text, 0, "", "", "", "", "", "", "", 3, 0)
                '    Else
                '        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                '    End If


            ElseIf op = 6 Then

                If Len(Trim(Me.TxtSetUpBox.Text)) > 0 Or Len(Trim(Me.TxtTarjeta.Text)) > 0 Then
                    BuscaQuejasSeparado(Me.ComboBox1.SelectedValue, 0, 0, "", "", "", "", "", Me.TxtSetUpBox.Text, Me.TxtTarjeta.Text, 6, 0)
                    'BuscaQuejasSeparado(Me.ComboBox1.SelectedValue, 0, 0, "", "", "", "", "", Me.TxtSetUpBox.Text, Me.TxtTarjeta.Text, 6, 0)
                Else
                    MsgBox("La Busqueda no se puede realizar con datos Invalidos", MsgBoxStyle.Information)
                End If

            ElseIf op = 10 Then
                If Me.cmbSectores.Items.Count <> 0 Then
                    BuscaQuejasSeparado(Me.ComboBox1.SelectedValue, 0, 0, sTATUS, "", "", "", "", "", "", op, Me.cmbSectores.SelectedValue)
                End If
            Else
                'Me.BUSCAQUEJASTableAdapter.Connection = CON
                'Me.BUSCAQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAQUEJAS, New System.Nullable(Of Integer)(CType(Me.ComboBox1.SelectedValue, Integer)), 0, 0, "", "", "", New System.Nullable(Of Integer)(CType(4, Integer)))
                'Me.BUSCAQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAQUEJAS, 0, 0, 0, "", "", "", New System.Nullable(Of Integer)(CType(4, Integer)))
                BuscaQuejasSeparado(Me.ComboBox1.SelectedValue, 0, 0, "", "", "", "", "", "", "", 4, 0)
            End If
            Me.TextBox1.Clear()
            Me.TextBox2.Clear()
            Me.TextBox3.Clear()
            Me.BNUMERO.Clear()
            Me.BCALLE.Clear()

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()

    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Busca(0)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Busca(1)
        TextBox2.Text = ""
        APaternoTextBox.Text = ""
        AMaternoTextBox.Text = ""
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(0)
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
        End If
    End Sub


    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(3)
        End If
    End Sub



    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        Busca(3)
    End Sub



    Private Sub Clv_calleLabel2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Clv_calleLabel2.TextChanged
        gloClave = Me.Clv_calleLabel2.Text
    End Sub

    Private Sub BRWQUEJAS_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            Me.ComboBox1.SelectedValue = GloClv_TipSer
            Me.ComboBox1.Text = GloNom_TipSer
            Me.ComboBox1.FindString(GloNom_TipSer)
            Me.ComboBox1.Text = GloNom_TipSer
            Busca(4)
        End If
    End Sub

    Private Sub BRWQUEJAS_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load 'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerPrincipal' Puede moverla o quitarla según sea necesario.
        Dim CON As New SqlConnection(MiConexion)

        colorea(Me, Me.Name)
        'Me.DamePermisosFormTableAdapter.Fill(Me.NewSofTvDataSet.DamePermisosForm, GloTipoUsuario, Me.Name, 1, glolec, gloescr, gloctr)
        If gloescr = 1 Then
            If GloTipoUsuario = 3 Then
                Me.Button2.Enabled = False
                Me.Button4.Enabled = True
            Else
                Me.Button2.Enabled = False
                Me.Button4.Enabled = False
            End If
        End If
        'TODO: esta línea de código carga datos en la tabla 'DataSetLidia2.MuestraTipSerPrincipal2' Puede moverla o quitarla según sea necesario.
        CON.Open()
        Me.MuestraTipSerPrincipal2TableAdapter.Connection = CON
        Me.MuestraTipSerPrincipal2TableAdapter.Fill(Me.DataSetLidia2.MuestraTipSerPrincipal2)
        CON.Close()

        GloClv_TipSer = Me.ComboBox1.SelectedValue
        Busca(4)
        GloBnd = False

        'PARA LLENAR EL RESUMEN DE ORDENES (INICIO) --JUANJO
        uspChecaCuantasOrdenesQuejas(GloClv_TipSer)
        'PARA LLENAR EL RESUMEN DE ORDENES (FIN) --JUANJO

        Softv_MuestraSectores()
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Busca(2)
    End Sub

    Private Sub BCALLE_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BCALLE.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
        End If
    End Sub


    Private Sub BNUMERO_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BNUMERO.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
        End If
    End Sub

    Private Sub DataGridView1_CurrentCellChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.CurrentCellChanged
        Try
            If Me.DataGridView1.SelectedCells.Item(0).Value > 0 Then
                Clv_calleLabel2.Text = Me.DataGridView1.SelectedCells.Item(1).Value
                Me.ContratoLabel1.Text = Me.DataGridView1.SelectedCells.Item(3).Value
                Me.CMBNombreTextBox.Text = Me.DataGridView1.SelectedCells.Item(4).Value
                CALLELabel1.Text = Me.DataGridView1.SelectedCells.Item(5).Value
                NUMEROLabel1.Text = Me.DataGridView1.SelectedCells.Item(6).Value
                Me.Label9.Text = Me.DataGridView1.SelectedCells.Item(6).Value
            End If
        Catch ex As Exception

        End Try
    End Sub


    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If Button4.Enabled = True Then
            modificar()
        ElseIf Button3.Enabled = True Then
            consultar()
        End If
    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        Busca(199)
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        Busca(199)
    End Sub

    Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton3.CheckedChanged
        Busca(199)
    End Sub


    Private Sub TextBox3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox3.TextChanged

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        If Me.ComboBox1.Items.Count > 0 Then
            If IsNumeric(Me.ComboBox1.SelectedValue) = True Then
                Busca(4)
                'PARA LLENAR EL RESUMEN DE ORDENES (INICIO) --JUANJO
                uspChecaCuantasOrdenesQuejas(CInt(Me.ComboBox1.SelectedValue))
                'PARA LLENAR EL RESUMEN DE ORDENES (FIN) --JUANJO
            End If
        End If
    End Sub
    Private Sub BuscaQuejasSeparado(ByVal Clv_TipSerQ As Integer, ByVal Clv_QuejaQ As Long, ByVal ContratoQ As Long, ByVal NombreQ As String, ByVal ApePaternoQ As String, _
                                ByVal ApeMaternoQ As String, ByVal CalleQ As String, ByVal NumeroQ As String, ByVal SetUpBox As String, ByVal Tarjeta As String, _
                                ByVal OpQ As Integer, ByVal prmClvSector As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder

        StrSQL.Append("EXEC BuscaQuejasSeparado ")
        StrSQL.Append(Clv_TipSerQ & ", ")
        StrSQL.Append(Clv_QuejaQ & ", ")
        StrSQL.Append(ContratoQ & ", ")
        StrSQL.Append("'" & NombreQ & "', ")
        StrSQL.Append("'" & ApePaternoQ & "', ")
        StrSQL.Append("'" & ApeMaternoQ & "', ")
        StrSQL.Append("'" & CalleQ & "', ")
        StrSQL.Append("'" & NumeroQ & "', ")
        StrSQL.Append("'" & SetUpBox & "', ")
        StrSQL.Append("'" & Tarjeta & "', ")
        StrSQL.Append(OpQ & ", ")
        StrSQL.Append(prmClvSector)

        Dim DA As New SqlDataAdapter(StrSQL.ToString, CON)
        Dim DT As New DataTable
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.DataGridView1.DataSource = BS.DataSource
            Try
                If Me.DataGridView1.SelectedCells.Item(0).Value > 0 Then
                    Clv_calleLabel2.Text = Me.DataGridView1.SelectedCells.Item(0).Value
                    Me.ContratoLabel1.Text = Me.DataGridView1.SelectedCells.Item(2).Value
                    Me.CMBNombreTextBox.Text = Me.DataGridView1.SelectedCells.Item(3).Value
                    CALLELabel1.Text = Me.DataGridView1.SelectedCells.Item(4).Value
                    NUMEROLabel1.Text = Me.DataGridView1.SelectedCells.Item(5).Value
                    Me.Label9.Text = Me.DataGridView1.SelectedCells.Item(6).Value
                End If
            Catch ex As Exception

            End Try
            'gloClave = Me.DataGridView1.SelectedCells.Item(0).Value
            'GloClv_TipSer = Me.DataGridView1.SelectedCells(6).Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub Button14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button14.Click
        Busca(6)
        TxtSetUpBox.Text = ""
        TxtTarjeta.Text = ""

    End Sub

    Private Sub Softv_MuestraSectores()
        'Dim parametro(2) As SqlParameter
        'parametro(0) = New SqlParameter("@id", SqlDbType.Int)
        'parametro(0).Value = 1
        'parametro(1) = New SqlParameter("@id2", SqlDbType.Int)
        'parametro(1).Value = 1
        'Dim DT As DataTable = consultas.consultarDT("Softv_MuestraSectores", parametro)
        Dim DT As DataTable = consultas.consultarDT("Softv_MuestraSectores", Nothing)
        Me.cmbSectores.DisplayMember = "Sector"
        Me.cmbSectores.ValueMember = "clv_sector"
        Me.cmbSectores.DataSource = DT
    End Sub

    Private Sub rbEnProceso_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbEnProceso.CheckedChanged
        Busca(199)
    End Sub

    Private Sub cmbSectores_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSectores.SelectedIndexChanged
        Busca(10)
    End Sub

    Public Sub borraQuejas(ByVal prmClvQueja As Long, ByVal prmTipo As String)
        Dim claseQuejas As New OrdenesDeServicioClass

        claseQuejas.uspBorraQuejasOrdenes(prmClvQueja, prmTipo)
        If CBool(claseQuejas.Diccionario("@bndOrdenQueja")) = True Then
            MsgBox("No se pueden eliminar Quejas con status Ejecutada", MsgBoxStyle.Information)
            Exit Sub
        ElseIf CBool(claseQuejas.Diccionario("@bndDescarga")) = True Then
            MsgBox("No se puede eliminar la Queja ya que tiene Descarga de Material Asignada ", MsgBoxStyle.Information)
            Exit Sub
        End If
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        If Me.DataGridView1.RowCount > 0 Then
            If IsNumeric(Me.DataGridView1.SelectedCells(0).Value) Then
                Dim Res = MsgBox("Realmente Desea Eliminar la Queja # " + CStr(Me.DataGridView1.SelectedCells(1).Value), MsgBoxStyle.YesNo)

                If Res = MsgBoxResult.Yes Then
                    borraQuejas(Me.DataGridView1.SelectedCells(1).Value, "Q")
                    bitsist(GloUsuario, CLng(Me.DataGridView1.SelectedCells(3).Value), LocGloSistema, "BrwQuejas", "Se eliminó una Queja", "", "El Usuario " + GloUsuario + " eliminó la Queja # " + CStr(Me.DataGridView1.SelectedCells(1).Value), LocClv_Ciudad)
                    Busca(4)
                Else
                    Exit Sub
                End If
            Else
                MsgBox("Seleccione un Registro Válido", MsgBoxStyle.Information)
                Exit Sub
            End If
        Else
            MsgBox("No Hay Registros en la Lista", MsgBoxStyle.Information)
            Exit Sub
        End If
    End Sub

#Region "Total Quejas JUANJO"
    Private Sub uspChecaCuantasOrdenesQuejas(ByVal prmClvTipSer As Integer)
        Dim Problema As New ClassClasificacionProblemas
        Dim DT As New DataTable

        Problema.OpAccion = 1
        Problema.clvTipSer = prmClvTipSer
        DT = Problema.uspChecaCuantasOrdenesQuejas()

        For Each fila As DataRow In DT.Rows
            Me.lblTotalPendientes.Text = fila("pendietnes").ToString()
            Me.lblTotalConVisita.Text = fila("visitas").ToString()
            Me.lblTotalEnProceso.Text = fila("proceso").ToString()
        Next
    End Sub
#End Region

    'SAUL
    Private Sub uspAutorizadoQuejasOrdenes()
        'GloUsuario-bndOxxo
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Usuario", SqlDbType.VarChar, GloUsuario, 10)
        BaseII.CreateMyParameter("@bnd", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("uspAutorizadoQuejasOrdenes")
        bndAcceso = CInt(BaseII.dicoPar("@bnd").ToString)
    End Sub

    Private Sub uspValidaOrdenes()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CInt(DataGridView1.SelectedCells.Item(3).Value))
        BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@Res", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@Msg", SqlDbType.VarChar, 150, ParameterDirection.Output, "")
        'ByVal prmNombre As String, ByRef prmTipo As SqlDbType, ByRef prmTamanio As Integer, ByRef prmDireccion As ParameterDirection, ByRef prmValor As Object
        BaseII.ProcedimientoOutPut("uspValidaOrdenes")
        eRespuesta = CInt(BaseII.dicoPar("@Res").ToString)
        eMensaje = BaseII.dicoPar("@Msg").ToString
    End Sub
    '(Fin)
End Class