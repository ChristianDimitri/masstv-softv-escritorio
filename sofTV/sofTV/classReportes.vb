﻿Imports System.Data.SqlClient
Imports System.Collections.Generic

Public Class classReportes
#Region "Propiedades"
    Private _contrato As Integer
    Public Property contrato As Integer
        Get
            Return _contrato
        End Get
        Set(value As Integer)
            _contrato = value
        End Set
    End Property

    Private _fechaInicial As Date
    Public Property fechaInicial As Date
        Get
            Return _fechaInicial
        End Get
        Set(value As Date)
            _fechaInicial = value
        End Set
    End Property

    Private _fechaFinal As Date
    Public Property fechaFinal As Date
        Get
            Return _fechaFinal
        End Get
        Set(value As Date)
            _fechaFinal = value
        End Set
    End Property

    Private _nomReporteInterfaz As String
    Public Property nomReporteInterfaz As String
        Get
            Return _nomReporteInterfaz
        End Get
        Set(value As String)
            _nomReporteInterfaz = value
        End Set
    End Property

    Private _clvSession As Long
    Public Property clvSession As Long
        Get
            Return _clvSession
        End Get
        Set(value As Long)
            _clvSession = value
        End Set
    End Property

    Private _idMes As Integer
    Public Property idMes As Integer
        Get
            Return _idMes
        End Get
        Set(value As Integer)
            _idMes = value
        End Set
    End Property

    Private _clvTipSer As Integer
    Public Property clvTipSer As Integer
        Get
            Return _clvTipSer
        End Get
        Set(value As Integer)
            _clvTipSer = value
        End Set
    End Property

    Private _tituloReporte As String
    Public Property tituloReporte As String
        Get
            Return _tituloReporte
        End Get
        Set(value As String)
            _tituloReporte = value
        End Set
    End Property

    Private _nombreEmpresa As String
    Public Property nombreEmpresa As String
        Get
            Return _nombreEmpresa
        End Get
        Set(value As String)
            _nombreEmpresa = value
        End Set
    End Property

    Private _ciudadEmpresa As String
    Public Property ciudadEmpresa As String
        Get
            Return _ciudadEmpresa
        End Get
        Set(value As String)
            _ciudadEmpresa = value
        End Set
    End Property
#End Region

#Region "Constructores"
    Public Sub New()
        _contrato = 0
        _fechaInicial = "01/01/1900"
        _fechaFinal = "01/01/1900"
        _nomReporteInterfaz = ""
        _clvSession = 0
        _idMes = 0
        _clvTipSer = 0
        _tituloReporte = ""
        _nombreEmpresa = ""
        _ciudadEmpresa = ""
    End Sub
#End Region

#Region "MÉTODOS REPORTE INTERFAZ DIGITAL"
    Dim cnr As BaseIII

    Public Sub uspConsultaMovimientosCnr() 'REPORTE INTERFAZ DIGITAL
        cnr = New BaseIII
        Dim dataSet As New  DataSet
        Dim listaTablas As New List(Of String)
        listaTablas.Add("ConsultaMovimientosCnr")

        cnr.limpiaParametros()
        cnr.CreateMyParameter("@contrato", SqlDbType.Int, _contrato)
        cnr.CreateMyParameter("@fechaInicial", SqlDbType.DateTime, _fechaInicial)
        cnr.CreateMyParameter("@fechaFinal", SqlDbType.DateTime, _fechaFinal)
        dataSet = cnr.ConsultaDS("uspConsultaMovimientosCnr", listaTablas)

        cnr.llamarReporteCentralizado(_nomReporteInterfaz, dataSet)
    End Sub
#End Region

#Region "MÉTODOS REPORTE DE RENOVACIÓN DE ANUALIDADES"
    Dim anualidad As BaseIII

    Public Function uspGeneraClvSessionAnualidad() As Long
        Try
            anualidad = New BaseIII
            anualidad.CreateMyParameter("@clvSession", ParameterDirection.Output, SqlDbType.BigInt)
            anualidad.ProcedimientoOutPut("uspGeneraClvSessionAnualidad")
            uspGeneraClvSessionAnualidad = CLng(anualidad.dicoPar("@clvSession").ToString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub uspGeneraClientesConAnualidad()
        Try
            anualidad = New BaseIII
            anualidad.CreateMyParameter("@clvSession", SqlDbType.BigInt, _clvSession)
            anualidad.Inserta("uspGeneraClientesConAnualidad")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function uspChecaFechasAnualidad() As DataTable
        Try
            anualidad = New BaseIII
            anualidad.CreateMyParameter("@clvSession", SqlDbType.BigInt, _clvSession)
            uspChecaFechasAnualidad = anualidad.ConsultaDT("uspChecaFechasAnualidad")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub uspReporteAnualidadesVencidas()
        anualidad = New BaseIII
        Dim dsAnualidad As New DataSet
        Dim listAnualidad As New List(Of String)
        Dim dicoFormulasAnualidad As Dictionary(Of String, String)
        listAnualidad.Add("ReporteAnualidadesVencidas")

        anualidad.limpiaParametros()
        anualidad.CreateMyParameter("@idMes", SqlDbType.Int, _idMes)
        anualidad.CreateMyParameter("@clvSession", SqlDbType.BigInt, _clvSession)
        anualidad.CreateMyParameter("@clvTipSer", SqlDbType.Int, _clvTipSer)
        dsAnualidad = anualidad.ConsultaDS("uspReporteAnualidadesVencidas", listAnualidad)

        dicoFormulasAnualidad = New Dictionary(Of String, String)
        dicoFormulasAnualidad.Add("Empresa", _nombreEmpresa)
        dicoFormulasAnualidad.Add("Subtitulo", _ciudadEmpresa)
        dicoFormulasAnualidad.Add("TituloReporte", _tituloReporte)

        anualidad.llamarReporteCentralizado(RutaReportes + "\rptReporteAnualidadesVencidas", dsAnualidad, dicoFormulasAnualidad)
    End Sub
#End Region

End Class
