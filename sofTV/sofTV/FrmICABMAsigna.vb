Imports System.Data.SqlClient
Public Class FrmICABMAsigna

    Public Sub CREAARBOL2()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            'Eric
            Bloquea = False
            Dim I As Integer = 0
            Dim X As Integer = 0
            If GLOTRABAJO = "ICABM" Or GLOTRABAJO = "ICABMT" Then
                Me.MUESTRAICAM_porSOLTableAdapter.Connection = CON
                Me.MUESTRAICAM_porSOLTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRAICAM_porSOL, GloDetClave, gloClv_Orden, 0)
                Dim FilaRow As DataRow
                Dim FilacontNet As DataRow
                Me.TreeView2.Nodes.Clear()
                For Each FilaRow In Me.NewSofTvDataSet.MUESTRAICAM_porSOL.Rows
                    'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                    X = 0
                    Me.TreeView2.Nodes.Add(Trim(FilaRow("CONTRATONET").ToString()), Trim(FilaRow("MACCABLEMODEM").ToString()))
                    Me.TreeView2.Nodes(I).Tag = Trim(FilaRow("CONTRATONET").ToString())
                    If GLOTRABAJO = "ICABM" Then
                        Me.MUESTRACONTNETTableAdapter.Connection = CON
                        Me.MUESTRACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTNET, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)))
                        Me.TreeView2.Nodes(I).ForeColor = Color.Black
                        For Each FilacontNet In Me.NewSofTvDataSet.MUESTRACONTNET.Rows
                            Me.TreeView2.Nodes(I).Nodes.Add(Trim(FilacontNet("CLV_UNICANET").ToString()), Trim(FilacontNet("DESCRIPCION").ToString()) & Trim(FilacontNet("STATUS").ToString()))
                            Me.TreeView2.Nodes(I).Nodes(X).Tag = Trim(FilacontNet("CLV_UNICANET").ToString())
                            If Trim(FilacontNet("STATUS").ToString()) = "Suspendido" Then
                                Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Olive
                            ElseIf Trim(FilacontNet("STATUS").ToString()) = "Instalado" Or Trim(FilacontNet("STATUS").ToString()) = "Contratado" Then
                                Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Navy
                            Else
                                Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Red
                            End If
                            X += 1
                        Next
                    ElseIf GLOTRABAJO = "ICABMT" Then
                        'Me.MUESTRACONTNETTableAdapter.Connection = CON
                        'Me.MUESTRACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTNET, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)))
                        Me.MUESTRACONTTELTableAdapter.Connection = CON
                        Me.MUESTRACONTTELTableAdapter.Fill(Me.Procedimientosarnoldo4.MUESTRACONTTEL, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)))
                        Me.TreeView2.Nodes(I).ForeColor = Color.Black
                        For Each FilacontNet In Me.Procedimientosarnoldo4.MUESTRACONTTEL.Rows
                            Me.TreeView2.Nodes(I).Nodes.Add(Trim(FilacontNet("CLV_UNICA").ToString()), Trim(FilacontNet("DESCRIPCION").ToString()) & Trim(FilacontNet("STATUS").ToString()))
                            Me.TreeView2.Nodes(I).Nodes(X).Tag = Trim(FilacontNet("CLV_UNICA").ToString())
                            If Trim(FilacontNet("STATUS").ToString()) = "Suspendido" Then
                                Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Olive
                            ElseIf Trim(FilacontNet("STATUS").ToString()) = "Instalado" Or Trim(FilacontNet("STATUS").ToString()) = "Contratado" Then
                                Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Navy
                            Else
                                Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Red
                            End If
                            X += 1
                        Next
                    End If
                    I += 1
                Next
            ElseIf GLOTRABAJO = "CCABM" Then
                'Me.MUESTRAICAM_porSOLTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRAICAM_porSOL, GloDetClave, gloClv_Orden, 2)
                'If opcion = "M" Then Bloquea = True
                'eric-------------------------------
                If Bloquea = False Then
                    Me.MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter.Connection = CON
                    Me.MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACABLEMODEMSDELCLI_porOpcion, New System.Nullable(Of Long)(CType(Contrato, Long)), "P", 16)
                    Dim FilaRow As DataRow
                    Dim FilacontNet As DataRow
                    Me.TreeView2.Nodes.Clear()
                    For Each FilaRow In Me.NewSofTvDataSet.MUESTRACABLEMODEMSDELCLI_porOpcion.Rows
                        'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                        X = 0
                        Me.TreeView2.Nodes.Add(Trim(FilaRow("CONTRATONET").ToString()), Trim(FilaRow("MACCABLEMODEM").ToString()))
                        Me.TreeView2.Nodes(I).Tag = Trim(FilaRow("CONTRATONET").ToString())
                        Me.MUESTRACONTNETTableAdapter.Connection = CON
                        Me.MUESTRACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTNET, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)))
                        Me.TreeView2.Nodes(I).ForeColor = Color.Black
                        For Each FilacontNet In Me.NewSofTvDataSet.MUESTRACONTNET.Rows
                            Me.TreeView2.Nodes(I).Nodes.Add(Trim(FilacontNet("CLV_UNICANET").ToString()), Trim(FilacontNet("DESCRIPCION").ToString()) & Trim(FilacontNet("STATUS").ToString()))
                            Me.TreeView2.Nodes(I).Nodes(X).Tag = Trim(FilacontNet("CLV_UNICANET").ToString())
                            If Trim(FilacontNet("STATUS").ToString()) = "Suspendido" Then
                                Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Olive
                            ElseIf Trim(FilacontNet("STATUS").ToString()) = "Instalado" Or Trim(FilacontNet("STATUS").ToString()) = "Contratado" Then
                                Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Navy
                            Else
                                Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Red
                            End If
                            X += 1
                        Next
                        I += 1
                    Next
                Else
                    Me.MUESTRAICAM_porSOLTableAdapter.Connection = CON
                    Me.MUESTRAICAM_porSOLTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRAICAM_porSOL, GloDetClave, gloClv_Orden, 17)
                    Dim FilaRow As DataRow
                    Dim FilacontNet As DataRow
                    Me.TreeView2.Nodes.Clear()
                    For Each FilaRow In Me.NewSofTvDataSet.MUESTRAICAM_porSOL.Rows
                        'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                        X = 0
                        Me.TreeView2.Nodes.Add(Trim(FilaRow("CONTRATONET").ToString()), Trim(FilaRow("MACCABLEMODEM").ToString()))
                        Me.TreeView2.Nodes(I).Tag = Trim(FilaRow("CONTRATONET").ToString())
                        Me.MUESTRACONTNETTableAdapter.Connection = CON
                        Me.MUESTRACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTNET, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)))
                        Me.TreeView2.Nodes(I).ForeColor = Color.Black
                        For Each FilacontNet In Me.NewSofTvDataSet.MUESTRACONTNET.Rows
                            Me.TreeView2.Nodes(I).Nodes.Add(Trim(FilacontNet("CLV_UNICANET").ToString()), Trim(FilacontNet("DESCRIPCION").ToString()) & Trim(FilacontNet("STATUS").ToString()))
                            Me.TreeView2.Nodes(I).Nodes(X).Tag = Trim(FilacontNet("CLV_UNICANET").ToString())
                            If Trim(FilacontNet("STATUS").ToString()) = "Suspendido" Then
                                Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Olive
                            ElseIf Trim(FilacontNet("STATUS").ToString()) = "Instalado" Or Trim(FilacontNet("STATUS").ToString()) = "Contratado" Then
                                Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Navy
                            Else
                                Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Red
                            End If
                            X += 1
                        Next
                        I += 1
                    Next
                End If
            End If
            Me.TreeView2.ExpandAll()
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub



    Private Sub busca()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If GLOTRABAJO = "ICABM" Or GLOTRABAJO = "ICABMT" Then
                Me.CONICABMTableAdapter.Connection = CON
                Me.CONICABMTableAdapter.Fill(Me.NewSofTvDataSet.CONICABM, New System.Nullable(Of Long)(CType(GloDetClave, Long)), New System.Nullable(Of Long)(CType(gloClv_Orden, Long)), New System.Nullable(Of Long)(CType(Contratonet1.Text, Long)))
            ElseIf GLOTRABAJO = "CCABM" Then

            End If
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub TreeView2_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView2.AfterSelect
        Try
            If e.Node.Level = 0 Then
                If IsNumeric(e.Node.Tag) = True Then
                    'Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))
                    Contratonet1.Text = e.Node.Tag
                    MacCablemodem1.Text = e.Node.Text
                    Contratonet2.Text = e.Node.Tag
                    MacCablemodem2.Text = e.Node.Text
                    busca()
                    '    Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))
                    'Else
                    '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                    'End If
                    'Else
                    'If IsNumeric(e.Node.Tag) Then
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                    'Else
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                    'End If
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FrmICABMAsigna_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBndClv_CablemodemSel = True Then
            If GLOTRABAJO = "CCABM" Or GLOTRABAJO = "CCABT" Then
                Me.CLV_CABLEMODEMNewTextBox.Text = GloClv_CablemodemSel
                Me.MaccablemodemNew.Text = GloMacCablemodemSel
            ElseIf GLOTRABAJO = "ICABM" Or GLOTRABAJO = "ICABMT" Then
                Me.CLV_CABLEMODEMTextBox.Text = GloClv_CablemodemSel
                Me.MacCableModemTextBox.Text = GloMacCablemodemSel
            End If
            GloBndClv_CablemodemSel = False
        End If
    End Sub

    Private Sub FrmICABMAsigna_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            'If GLOTRABAJO = "CCABM" Then
            Me.BORDetOrdSer_INTELIGENTETableAdapter.Connection = CON
            Me.BORDetOrdSer_INTELIGENTETableAdapter.Fill(Me.NewSofTvDataSet.BORDetOrdSer_INTELIGENTE, New System.Nullable(Of Long)(CType(GloDetClave, Long)))
            GloBndTrabajo = True
            GloBloqueaDetalle = True
            'End If
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FrmICABMAsigna_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MUESTRACABLEMODEMS_disponiblesTableAdapter.Connection = CON
        Me.MUESTRACABLEMODEMS_disponiblesTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACABLEMODEMS_disponibles, 0)
        Me.CREAARBOL2()
        If opcion = "N" Then Bloquea = True

        Me.Button3.Visible = False
        Me.Button4.Visible = False

        If IdSistema = "LO" Or IdSistema = "YU" Then
            Me.Text = "Asiganción de ATAS"
        End If

        If GLOTRABAJO = "ICABM" Or GLOTRABAJO = "ICABMT" Then
            Me.Panel1.Visible = True
            Me.Panel2.Visible = False
            If opcion = "C" Then
                Me.Button2.Enabled = False
                Me.Button3.Enabled = False
                CONICABMBindingNavigator.Enabled = False
            End If
            If GLOTRABAJO = "ICABM" Then
                If IdSistema <> "LO" And IdSistema <> "YU" Then
                    Me.Label4.Text = "Por Asignar Cablemodems"
                    Me.Button2.Text = "CABLEMODEMS DISPONIBLES"
                    Me.Button3.Text = "CABLEMODEM PROPIO"
                    Me.ToolStripButton1.Text = "BORRAR CABLEMODEM"

                ElseIf IdSistema = "LO" Or IdSistema = "YU" Then
                    Me.Label4.Text = "Por Asignar ATA"
                    Me.Button2.Text = "ATAS DISPONIBLES"
                    Me.Button3.Text = "ATA PROPIO"
                    Me.ToolStripButton1.Text = "BORRAR ATA"


                End If
            ElseIf GLOTRABAJO = "ICABMT" Then
                Me.Label4.Text = "Por Asignar ATA"
                Me.Button2.Text = "ATAS DISPONIBLES"
                Me.Button3.Text = "ATA PROPIO"
                Me.ToolStripButton1.Text = "BORRAR ATA"

            End If

            'If Bloquea = True Then
            ' CONCCABMBindingNavigator.Enabled = False
            ' Me.CONICABMBindingNavigator.Enabled = False
            'End If
        ElseIf GLOTRABAJO = "CCABM" Or GLOTRABAJO = "CCABT" Then
            Me.Panel2.Visible = True
            Me.Panel1.Visible = False
            If Bloquea = True Or opcion = "N" Then
                CONCCABMBindingNavigator.Enabled = True
                Me.CONICABMBindingNavigator.Enabled = True
            End If
        End If
        Me.Button3.Visible = False
        Me.Button4.Visible = False
        CON.Close()
        eBndClv_CablemodemSelPropio = False
    End Sub

    Private Sub CONICABMBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONICABMBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.CLV_CABLEMODEMTextBox.Text) = True Then
            If IsNumeric(Me.CLV_CABLEMODEMTextBox.Text) = False Then Me.CLV_CABLEMODEMTextBox.Text = 0
            If Me.CLV_CABLEMODEMTextBox.Text > 0 Then
                Me.Validate()
                GloGuardo = True
                Me.CONICABMBindingSource.EndEdit()
                Me.CONICABMTableAdapter.Connection = CON
                Me.CONICABMTableAdapter.Update(Me.NewSofTvDataSet.CONICABM)
                Me.MUESTRACABLEMODEMS_disponiblesTableAdapter.Connection = CON
                Me.MUESTRACABLEMODEMS_disponiblesTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACABLEMODEMS_disponibles, 0)
                If eBndClv_CablemodemSelPropio = True Then
                    eBndClv_CablemodemSelPropio = False
                    CablemodemPropio()
                End If
                Me.CREAARBOL2()
                Me.Close()
            Else
                If IdSistema <> "LO" And IdSistema <> "YU" Then
                    MsgBox("Ya tiene asignado un Cablemodem ", MsgBoxStyle.Information)
                ElseIf IdSistema = "LO" Or IdSistema = "YU" Then
                    MsgBox("Ya tiene asignado un ATA ", MsgBoxStyle.Information)
                End If
            End If
        Else
            If IdSistema <> "LO" And IdSistema <> "YU" Then
                MsgBox("Seleccione el Cablemodem")
            ElseIf IdSistema = "LO" Or IdSistema = "YU" Then
                MsgBox("Seleccione el ATA")
            End If
        End If
        CON.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub


    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(GloDetClave) = True And IsNumeric(gloClv_Orden) = True And IsNumeric(Me.Contratonet1.Text) = True And IsNumeric(Me.CLV_CABLEMODEMTextBox.Text) = True Then
            If GloDetClave > 0 And gloClv_Orden > 0 And Me.Contratonet1.Text > 0 And Me.CLV_CABLEMODEMTextBox.Text > 0 Then
                Me.CONICABMTableAdapter.Connection = CON
                Me.CONICABMTableAdapter.Delete(GloDetClave, gloClv_Orden, Me.Contratonet1.Text, Me.CLV_CABLEMODEMTextBox.Text)
                Me.MUESTRACABLEMODEMS_disponiblesTableAdapter.Connection = CON
                Me.MUESTRACABLEMODEMS_disponiblesTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACABLEMODEMS_disponibles, 0)
                Me.CREAARBOL2()
                Me.CLV_CABLEMODEMTextBox.Text = 0
                Me.MacCableModemTextBox.Text = ""
                Me.Close()
            End If
        End If
        CON.Close()
    End Sub

    Private Sub ToolStripButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If IsNumeric(GloDetClave) = True And IsNumeric(gloClv_Orden) = True And IsNumeric(Me.Contratonet2.Text) = True And IsNumeric(Me.CLV_CABLEMODEMNewTextBox.Text) > 0 Then
                Me.CONCCABMTableAdapter.Connection = CON
                Me.CONCCABMTableAdapter.Fill(Me.NewSofTvDataSet.CONCCABM, GloDetClave, gloClv_Orden, Me.Contratonet2.Text)
            Else
                MsgBox("Seleccione el Cablemode Nuevo ", MsgBoxStyle.Information)
            End If
            CON.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub MacCablemodem2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MacCablemodem2.TextChanged

    End Sub

    Private Sub Panel2_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel2.Paint

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        FrmSelCablemodemsDisponibles.Show()
    End Sub

    Private Sub Contratonet2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Contratonet2.TextChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.Contratonet2.Text) = True Then
            Me.CONCCABMTableAdapter.Connection = CON
            Me.CONCCABMTableAdapter.Fill(Me.NewSofTvDataSet.CONCCABM, GloDetClave, gloClv_Orden, Me.Contratonet2.Text)
        End If
        CON.Close()
    End Sub




    Private Sub CONCCABMBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONCCABMBindingNavigatorSaveItem.Click
        Dim BND As Boolean = False
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Me.CheckBox1.Checked = True Then BND = True Else BND = False
        If IsNumeric(Me.CLV_CABLEMODEMNewTextBox.Text) = False Then Me.CLV_CABLEMODEMNewTextBox.Text = 0
        If IsNumeric(GloDetClave) = True And IsNumeric(gloClv_Orden) = True And IsNumeric(Me.Contratonet2.Text) = True And Me.CLV_CABLEMODEMNewTextBox.Text > 0 Then
            GloGuardo = True
            Me.Validate()
            Me.NUECCABMTableAdapter.Connection = CON
            Me.NUECCABMTableAdapter.Fill(Me.NewSofTvDataSet.NUECCABM, GloDetClave, gloClv_Orden, Me.Contratonet2.Text, Me.CLV_CABLEMODEMNewTextBox.Text, BND)
            'Me.CONCCABMTableAdapter.Insert(GloDetClave, gloClv_Orden, Me.Contratonet2.Text, Me.CLV_CABLEMODEMNewTextBox.Text, BND)
            Me.MUESTRACABLEMODEMS_disponiblesTableAdapter.Connection = CON
            Me.MUESTRACABLEMODEMS_disponiblesTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACABLEMODEMS_disponibles, 0)
            If eBndClv_CablemodemSelPropio = True Then
                eBndClv_CablemodemSelPropio = False
                CablemodemPropio()
            End If
            MsgBox(mensaje5)
            Me.Close()
        Else
            If IdSistema <> "LO" And IdSistema <> "YU" Then
                MsgBox("Seleccione el Cablemodem Nuevo ", MsgBoxStyle.Information)
            ElseIf IdSistema = "LO" Or IdSistema = "YU" Then
                MsgBox("Seleccione el ATA Nuevo ", MsgBoxStyle.Information)
            End If
        End If
        CON.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.CLV_CABLEMODEMNewTextBox.Text) = False Then Me.CLV_CABLEMODEMNewTextBox.Text = 0
        If IsNumeric(GloDetClave) = True And IsNumeric(gloClv_Orden) = True And IsNumeric(Me.Contratonet2.Text) = True And Me.CLV_CABLEMODEMNewTextBox.Text > 0 Then
            Me.BORCCABMTableAdapter.Connection = CON
            Me.BORCCABMTableAdapter.Fill(Me.NewSofTvDataSet.BORCCABM, GloDetClave, gloClv_Orden, Me.Contratonet2.Text, Me.CLV_CABLEMODEMNewTextBox.Text)
            Me.MUESTRACABLEMODEMS_disponiblesTableAdapter.Connection = CON
            Me.MUESTRACABLEMODEMS_disponiblesTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACABLEMODEMS_disponibles, 0)
            Me.MaccablemodemNew.Text = ""
            Me.CLV_CABLEMODEMNewTextBox.Text = 0
            If IdSistema <> "LO" And IdSistema <> "YU" Then
                MsgBox("Se quito el Cablemodem Nuevo ")
            ElseIf IdSistema = "LO" Or IdSistema = "YU" Then
                MsgBox("Se quito el ATA Nuevo ")
            End If
            Me.Close()
        Else
            If IdSistema <> "LO" And IdSistema <> "YU" Then
                MsgBox("No tiene asginado un Cablemodem Nuevo")
            ElseIf IdSistema = "LO" Or IdSistema = "YU" Then
                MsgBox("No tiene asginado un ATA Nuevo")
            End If
        End If
            CON.Close()
    End Sub

    Private Sub NoFuncionaCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NoFuncionaCheckBox.CheckedChanged
        Me.CheckBox1.Checked = Me.NoFuncionaCheckBox.Checked
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged

    End Sub

    Private Sub CLV_CABLEMODEMNewTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CLV_CABLEMODEMNewTextBox.TextChanged
        If IsNumeric(Me.CLV_CABLEMODEMNewTextBox.Text) = False Then Me.CLV_CABLEMODEMNewTextBox.Text = 0
        If Me.CLV_CABLEMODEMNewTextBox.Text = 0 Then
            Me.Button1.Enabled = True
            Me.Button4.Enabled = True
        ElseIf Me.CLV_CABLEMODEMNewTextBox.Text > 0 Then
            Me.Button1.Enabled = False
            Me.Button4.Enabled = False
        End If
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub CLV_CABLEMODEMTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CLV_CABLEMODEMTextBox.TextChanged
        If IsNumeric(Me.CLV_CABLEMODEMTextBox.Text) = False Then Me.CLV_CABLEMODEMTextBox.Text = 0
        If Me.CLV_CABLEMODEMTextBox.Text = 0 Then
            If opcion = "C" Then
                Me.Button2.Enabled = False
                Me.Button3.Enabled = False
                CONICABMBindingNavigator.Enabled = False
            Else
                Me.Button2.Enabled = True
                Me.Button3.Enabled = True
            End If
            
        ElseIf Me.CLV_CABLEMODEMTextBox.Text > 0 Then
            Me.Button2.Enabled = False
            Me.Button3.Enabled = False
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        FrmSelCablemodemsDisponibles.Show()
    End Sub


    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        FrmCabModPropio2.Show()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        FrmCabModPropio2.Show()
    End Sub

    Private Sub CablemodemPropio()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("CablemodemPropio", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@ContratoNet", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Me.Contratonet1.Text
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    
End Class