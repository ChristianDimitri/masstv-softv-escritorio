<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMuestraDireccion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ProcedimientosArnoldo4 = New sofTV.Procedimientosarnoldo4()
        Me.Muestra_Valida_DireccionDataGridView = New System.Windows.Forms.DataGridView()
        Me.ClvsessionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ContratoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NombreDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DireccionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StatusDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MuestraValidaDireccionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.Muestra_Valida_DireccionTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Muestra_Valida_DireccionTableAdapter()
        CType(Me.ProcedimientosArnoldo4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Muestra_Valida_DireccionDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraValidaDireccionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ProcedimientosArnoldo4
        '
        Me.ProcedimientosArnoldo4.DataSetName = "ProcedimientosArnoldo4"
        Me.ProcedimientosArnoldo4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Muestra_Valida_DireccionDataGridView
        '
        Me.Muestra_Valida_DireccionDataGridView.AllowUserToAddRows = False
        Me.Muestra_Valida_DireccionDataGridView.AllowUserToDeleteRows = False
        Me.Muestra_Valida_DireccionDataGridView.AllowUserToOrderColumns = True
        Me.Muestra_Valida_DireccionDataGridView.AllowUserToResizeColumns = False
        Me.Muestra_Valida_DireccionDataGridView.AllowUserToResizeRows = False
        Me.Muestra_Valida_DireccionDataGridView.AutoGenerateColumns = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Muestra_Valida_DireccionDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.Muestra_Valida_DireccionDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ClvsessionDataGridViewTextBoxColumn, Me.ContratoDataGridViewTextBoxColumn, Me.NombreDataGridViewTextBoxColumn, Me.DireccionDataGridViewTextBoxColumn, Me.StatusDataGridViewTextBoxColumn})
        Me.Muestra_Valida_DireccionDataGridView.DataSource = Me.MuestraValidaDireccionBindingSource
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Muestra_Valida_DireccionDataGridView.DefaultCellStyle = DataGridViewCellStyle2
        Me.Muestra_Valida_DireccionDataGridView.Location = New System.Drawing.Point(0, 0)
        Me.Muestra_Valida_DireccionDataGridView.Name = "Muestra_Valida_DireccionDataGridView"
        Me.Muestra_Valida_DireccionDataGridView.ReadOnly = True
        Me.Muestra_Valida_DireccionDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Muestra_Valida_DireccionDataGridView.Size = New System.Drawing.Size(1188, 277)
        Me.Muestra_Valida_DireccionDataGridView.TabIndex = 2
        '
        'ClvsessionDataGridViewTextBoxColumn
        '
        Me.ClvsessionDataGridViewTextBoxColumn.DataPropertyName = "clv_session"
        Me.ClvsessionDataGridViewTextBoxColumn.HeaderText = "clv_session"
        Me.ClvsessionDataGridViewTextBoxColumn.Name = "ClvsessionDataGridViewTextBoxColumn"
        Me.ClvsessionDataGridViewTextBoxColumn.ReadOnly = True
        Me.ClvsessionDataGridViewTextBoxColumn.Visible = False
        '
        'ContratoDataGridViewTextBoxColumn
        '
        Me.ContratoDataGridViewTextBoxColumn.DataPropertyName = "contrato"
        Me.ContratoDataGridViewTextBoxColumn.HeaderText = "contrato"
        Me.ContratoDataGridViewTextBoxColumn.Name = "ContratoDataGridViewTextBoxColumn"
        Me.ContratoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'NombreDataGridViewTextBoxColumn
        '
        Me.NombreDataGridViewTextBoxColumn.DataPropertyName = "nombre"
        Me.NombreDataGridViewTextBoxColumn.HeaderText = "Nombre del Cliente"
        Me.NombreDataGridViewTextBoxColumn.Name = "NombreDataGridViewTextBoxColumn"
        Me.NombreDataGridViewTextBoxColumn.ReadOnly = True
        Me.NombreDataGridViewTextBoxColumn.Width = 400
        '
        'DireccionDataGridViewTextBoxColumn
        '
        Me.DireccionDataGridViewTextBoxColumn.DataPropertyName = "direccion"
        Me.DireccionDataGridViewTextBoxColumn.HeaderText = "Dirección"
        Me.DireccionDataGridViewTextBoxColumn.Name = "DireccionDataGridViewTextBoxColumn"
        Me.DireccionDataGridViewTextBoxColumn.ReadOnly = True
        Me.DireccionDataGridViewTextBoxColumn.Width = 400
        '
        'StatusDataGridViewTextBoxColumn
        '
        Me.StatusDataGridViewTextBoxColumn.DataPropertyName = "status"
        Me.StatusDataGridViewTextBoxColumn.HeaderText = "Status"
        Me.StatusDataGridViewTextBoxColumn.Name = "StatusDataGridViewTextBoxColumn"
        Me.StatusDataGridViewTextBoxColumn.ReadOnly = True
        '
        'MuestraValidaDireccionBindingSource
        '
        Me.MuestraValidaDireccionBindingSource.DataMember = "Muestra_Valida_Direccion"
        Me.MuestraValidaDireccionBindingSource.DataSource = Me.ProcedimientosArnoldo4
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Muestra_Valida_DireccionDataGridView)
        Me.Panel1.Location = New System.Drawing.Point(12, 53)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1047, 280)
        Me.Panel1.TabIndex = 3
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.Olive
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.White
        Me.Button5.Location = New System.Drawing.Point(522, 389)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(165, 40)
        Me.Button5.TabIndex = 15
        Me.Button5.Text = "&RECHAZAR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.Maroon
        Me.CMBLabel1.Location = New System.Drawing.Point(28, 20)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(284, 20)
        Me.CMBLabel1.TabIndex = 16
        Me.CMBLabel1.Text = "Contratos Con El Mismo Domicilio:"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Olive
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(310, 389)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(177, 40)
        Me.Button1.TabIndex = 17
        Me.Button1.Text = "&GUARDAR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.ForeColor = System.Drawing.Color.Maroon
        Me.CMBLabel2.Location = New System.Drawing.Point(359, 350)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(281, 24)
        Me.CMBLabel2.TabIndex = 18
        Me.CMBLabel2.Text = "¿Desea Agregar El Contrato?"
        '
        'Muestra_Valida_DireccionTableAdapter
        '
        Me.Muestra_Valida_DireccionTableAdapter.ClearBeforeFill = True
        '
        'FrmMuestraDireccion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1116, 452)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "FrmMuestraDireccion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Contratos Con Domicilio Igual"
        CType(Me.ProcedimientosArnoldo4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Muestra_Valida_DireccionDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraValidaDireccionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ProcedimientosArnoldo4 As sofTV.Procedimientosarnoldo4
    'Friend WithEvents Muestra_Valida_DireccionTableAdapter As sofTV.ProcedimientosArnoldo4TableAdapters.Muestra_Valida_DireccionTableAdapter
    Friend WithEvents Muestra_Valida_DireccionDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MuestraValidaDireccionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Valida_DireccionTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Muestra_Valida_DireccionTableAdapter
    Friend WithEvents ClvsessionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ContratoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NombreDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DireccionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents StatusDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
