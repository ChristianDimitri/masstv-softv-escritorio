Imports System.Data.SqlClient
Public Class FrmAgendaRapida

    Public MensajeAnualidad As String = ""

    Private Sub FrmAgendaRapida_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

    End Sub

    Private Sub FrmAgendaRapida_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'Checa()
    End Sub


    Private Sub FrmAgendaRapida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)


        If opcion = "N" Then
            Me.TextComentario.Text = ""
            Me.TextComentario.Text = MensajeAnualidad
            ''Me.ComboBox1.Text = "6:00"
            ''Me.ComboBox1.Enabled = False

            Dim CON As New SqlConnection(MiConexion)
            Me.TextBox1.Text = GLONOM_TECNICO
            'Me.Clv_TecnicoTextBox.Text = GloClv_tecnico
            Me.Clv_TecnicoTextBox.Text = 0
            CON.Open()
            Me.Muestra_Tecnicos_AlmacenTableAdapter.Connection = CON
            Me.Muestra_Tecnicos_AlmacenTableAdapter.Fill(Me.DataSetLidia2.Muestra_Tecnicos_Almacen, 1)
            Me.MUESTRAHORAS_CITASTableAdapter.Connection = CON
            Me.MUESTRAHORAS_CITASTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRAHORAS_CITAS, Me.DateTimePicker1.Text, GloClv_tecnico)
            CON.Close()
            'Me.ComboBox2.SelectedValue = GloClv_tecnico
            Me.ComboBox2.SelectedValue = 0
            If Len(Trim(FechaAgenda)) > 0 Then
                Me.DateTimePicker1.Value = CDate(FechaAgenda)
            End If
            'If Len(Trim(HoraAgenda)) > 0 Then
            '    Me.ComboBox1.Text = HoraAgenda
            '    Me.ComboBox1.FindStringExact(HoraAgenda, 0)
            '    Me.ComboBox1.SelectedValue = Clv_HoraAgenda
            '    Me.TextComentario.Text = ComentarioAgenda
            'End If
            Me.DateTimePicker1.Value = Today
            Me.ComboBox1.DataSource = spConsultaTurnos()
            ''Extra
            Me.TextComentario.Text = ""
        Else
            Me.ComboBox2.SelectedValue = GloClv_tecnico
            If Len(Trim(FechaAgenda)) > 0 Then
                Me.DateTimePicker1.Value = CDate(FechaAgenda)
            End If
            If Len(Trim(HoraAgenda)) > 0 Then
                Me.ComboBox1.Text = HoraAgenda
                Me.ComboBox1.FindStringExact(HoraAgenda, 0)
                Me.ComboBox1.SelectedValue = Clv_HoraAgenda
                Me.TextComentario.Text = ComentarioAgenda
            End If
            opcion = ""
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    'Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
    '    Checa()
    'End Sub

    Private Sub DateTimePicker1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DateTimePicker1.TextChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsDate(Me.DateTimePicker1.Text) = True And IsNumeric(Me.Clv_TecnicoTextBox.Text) = True Then
            Me.MUESTRAHORAS_CITASTableAdapter.Connection = CON
            Me.MUESTRAHORAS_CITASTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRAHORAS_CITAS, Me.DateTimePicker1.Text, GloClv_tecnico)
        End If
        CON.Close()
    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsDate(Me.DateTimePicker1.Text) = True And IsNumeric(Me.Clv_TecnicoTextBox.Text) = True Then
            Me.MUESTRAHORAS_CITASTableAdapter.Connection = CON
            Me.MUESTRAHORAS_CITASTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRAHORAS_CITAS, Me.DateTimePicker1.Text, GloClv_tecnico)
        End If
        CON.Close()
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        Dim CON As New SqlConnection(MiConexion)
        If IsNumeric(Me.ComboBox2.SelectedValue) = True And Me.ComboBox2.SelectedValue > 0 Then
            GloClv_tecnico = Me.ComboBox2.SelectedValue
            If IsDate(Me.DateTimePicker1.Text) = True And IsNumeric(Me.Clv_TecnicoTextBox.Text) = True Then
                CON.Open()
                Me.MUESTRAHORAS_CITASTableAdapter.Connection = CON
                Me.MUESTRAHORAS_CITASTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRAHORAS_CITAS, Me.DateTimePicker1.Text, GloClv_tecnico)
                CON.Close()
            End If
        End If
    End Sub

    Private Sub Checa()
        If GloClv_tecnico = Nothing Or GloClv_tecnico = 0 Or Me.ComboBox2.Text = "" Then
            MsgBox("Seleccione El T�cnico Por Favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Me.ComboBox1.SelectedValue = 0 Or Me.ComboBox1.SelectedValue = Nothing Then
            MsgBox("Seleccione el Turno Por Favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        FechaAgenda = Me.DateTimePicker1.Text
        HoraAgenda = "6:00"
        TurnoAgenda = Me.ComboBox1.Text
        Clv_HoraAgenda = Me.ComboBox1.SelectedValue
        ComentarioAgenda = Me.TextComentario.Text
        Me.DialogResult = DialogResult.OK
        Me.Close()
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged

    End Sub

    Private Sub TextComentario_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TextComentario.KeyPress


    End Sub

    Private Sub FrmAgendaRapida_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If (e.KeyValue = 13) Then
            e.SuppressKeyPress = True
        End If
    End Sub

    Private Sub Button1_Click_1(sender As System.Object, e As System.EventArgs)

    End Sub

    

    Private Sub Button1_Click_2(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Checa()
    End Sub
End Class