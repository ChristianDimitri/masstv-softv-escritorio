﻿Public Class FrmtblRelVendedoresSeries

    Dim FOLIO As New ClassBrwtblRelVendedoresSeries
    Public IdRelVendedoresSeries As Integer

#Region "EVENTOS"
    Private Sub FrmtblRelVendedoresSeries_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        tblCompanias(0, 0)

        If opcion = "N" Then
            VendedoresSeries(2, 0, "")
            tsbEliminar.Enabled = False
            Bloquear(False)
        ElseIf opcion = "M" Then
            VendedoresSeries(2, IdRelVendedoresSeries, "")
            DetalleVendedoresSeries(IdRelVendedoresSeries)
            cbVendedores.Enabled = False
        ElseIf opcion = "C" Then
            VendedoresSeries(2, IdRelVendedoresSeries, "")
            DetalleVendedoresSeries(IdRelVendedoresSeries)
            bnRelVendedoresSeries.Enabled = False
            cbVendedores.Enabled = False
            Bloquear(False)
        End If
    End Sub
#End Region

#Region "METODOS"
    Private Sub tblCompanias(ByVal PRMOP As Integer, ByVal PRMCLVCOMPANY As Integer)
        FOLIO.OP = PRMOP
        FOLIO.CLVCOMPANY = PRMCLVCOMPANY
        cbCompanias.DataSource = FOLIO.MUESTRAtblCompanias
    End Sub

    Private Sub VendedoresSeries(ByVal PRMOP As Integer, ByVal PRMID As Integer, ByVal PRMNOMBRE As String)
        FOLIO.OP = PRMOP
        FOLIO.ID = PRMID
        FOLIO.NOMBRE = PRMNOMBRE
        Me.cbVendedores.DataSource = FOLIO.CONtblRelVendedoresSeries
    End Sub

    Private Sub DetalleVendedoresSeries(ByVal PRMID As Integer)
        FOLIO.ID = PRMID
        Me.dgvRelVendedoresSeries.DataSource = FOLIO.CONtblDetRelVendedoresSeries
    End Sub

    Private Sub Bloquear(ByVal bnd As Boolean)
        cbCompanias.Enabled = bnd
        tbFoliosUsados.Enabled = bnd
        tbSerie.Enabled = bnd
        tbUltimoFolioUsado.Enabled = bnd
        bnAgregar.Enabled = bnd
        bnModificar.Enabled = bnd
        bnGuardar.Enabled = bnd
        bnEliminar.Enabled = bnd
        bnCancelar.Enabled = bnd
        dgvRelVendedoresSeries.Enabled = bnd
    End Sub

    Private Sub NuevoRelVendedoresSeries(ByVal PRMID As Integer, ByVal PRMCLVCOMPANY As Integer, ByVal PRMFOLIOUSADO As Integer, ByVal PRMSERIE As String, ByVal PRMULTIMOFOLIO As Integer)
        Dim DT As New DataTable
        Dim MENSAJES As String
        FOLIO.ID = PRMID
        FOLIO.CLVCOMPANY = PRMCLVCOMPANY
        FOLIO.FOLIOUSADOS = PRMFOLIOUSADO
        FOLIO.SERIE = PRMSERIE
        FOLIO.ULTIMOFOLIOUSADO = PRMULTIMOFOLIO
        DT = FOLIO.NUEtblDetRelVendedoresSeries()
        If Len(DT.Rows(0)(0).ToString) > 0 Then
            MENSAJES = DT.Rows(0)(0).ToString
            MsgBox(MENSAJES, MsgBoxStyle.Information, "Catálogos Series")
        End If

    End Sub

    Private Sub ModificarRelVendedoresSeries(ByVal PRMIDDETALLE As Integer, ByVal PRMID As Integer, ByVal PRMCLVCOMPANY As Integer, ByVal PRMFOLIOUSADO As Integer, ByVal PRMSERIE As String, ByVal PRMULTIMOFOLIO As Integer)
        Dim DT As New DataTable
        Dim MENSAJES As String
        FOLIO.IDDETALLE = PRMIDDETALLE
        FOLIO.ID = PRMID
        FOLIO.CLVCOMPANY = PRMCLVCOMPANY
        FOLIO.FOLIOUSADOS = PRMFOLIOUSADO
        FOLIO.SERIE = PRMSERIE
        FOLIO.ULTIMOFOLIOUSADO = PRMULTIMOFOLIO
        DT = FOLIO.NUEtblDetRelVendedoresSeries()
        If Len(DT.Rows(0)(0).ToString) > 0 Then
            MENSAJES = DT.Rows(0)(0).ToString
            MsgBox(MENSAJES, MsgBoxStyle.Information, "Catálogos Series")
        End If

    End Sub

    Private Sub habilitar()
        tblCompanias(0, 0)
        tbFoliosUsados.Text = ""
        tbSerie.Text = ""
        tbUltimoFolioUsado.Text = ""

        bnAgregar.Enabled = True
        bnModificar.Enabled = True
        bnGuardar.Enabled = False
        bnEliminar.Enabled = True
        bnCancelar.Enabled = True
        dgvRelVendedoresSeries.Enabled = True
    End Sub

    Private Sub deshabilitar()
        tblCompanias(1, CInt(dgvRelVendedoresSeries.SelectedCells(1).Value.ToString()))
        tbFoliosUsados.Text = dgvRelVendedoresSeries.SelectedCells(3).Value.ToString()
        tbSerie.Text = dgvRelVendedoresSeries.SelectedCells(4).Value.ToString()
        tbUltimoFolioUsado.Text = dgvRelVendedoresSeries.SelectedCells(5).Value.ToString()

        bnAgregar.Enabled = False
        bnModificar.Enabled = False
        bnGuardar.Enabled = True
        bnEliminar.Enabled = False
        bnCancelar.Enabled = True
        dgvRelVendedoresSeries.Enabled = False
    End Sub

    Private Sub BorrarDetalleVendedoresSeries(ByVal PRMIDDETALLE As Integer)
        FOLIO.IDDETALLE = PRMIDDETALLE
        FOLIO.BORtblDetRelVendedoresSeries()
    End Sub


#End Region

#Region "CONTROLES"
    Private Sub bnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub

    Private Sub bnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnAgregar.Click
        If cbCompanias.Text.Length = 0 Then
            MsgBox("Selecciona una Compañía.", MsgBoxStyle.Information, "Catálogos Series")
            Exit Sub
        ElseIf tbFoliosUsados.Text.Length = 0 Then
            MsgBox("Captura el número de Folios Impresos.", MsgBoxStyle.Information, "Catálogos Series")
            Exit Sub
        ElseIf IsNumeric(tbFoliosUsados.Text) = False Then
            MsgBox("Captura un número válido de Folios Impresos.", MsgBoxStyle.Information, "Catálogos Series")
            Exit Sub
        ElseIf tbSerie.Text.Length = 0 Then
            MsgBox("Captura una Serie.", MsgBoxStyle.Information, "Catálogos Series")
            Exit Sub
        ElseIf tbUltimoFolioUsado.Text.Length = 0 Then
            MsgBox("Captura el número del Último Folio Usado.", MsgBoxStyle.Information, "Catálogos Series")
            Exit Sub
        ElseIf IsNumeric(tbUltimoFolioUsado.Text) = False Then
            MsgBox("Captura un número válido del Último Folio Usado.", MsgBoxStyle.Information, "Catálogos Series")
            Exit Sub
        ElseIf cbCompanias.SelectedValue = 0 Then
            MsgBox("Selecciona una Compañía.", MsgBoxStyle.Information, "Catálogos Series")
            Exit Sub
        End If

        NuevoRelVendedoresSeries(IdRelVendedoresSeries, CInt(cbCompanias.SelectedValue.ToString()), CInt(tbFoliosUsados.Text), tbSerie.Text, CInt(tbUltimoFolioUsado.Text))
        DetalleVendedoresSeries(IdRelVendedoresSeries)
        habilitar()

    End Sub

    Private Sub bnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnModificar.Click
        If dgvRelVendedoresSeries.Rows.Count = 0 Then
            MsgBox("Selecciona un registro.", MsgBoxStyle.Information, "Catálogos Series")
            Exit Sub
        End If
        deshabilitar()
    End Sub

    Private Sub bnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnGuardar.Click
        If cbCompanias.Text.Length = 0 Then
            MsgBox("Selecciona una Compañía.", MsgBoxStyle.Information, "Catálogos Series")
            Exit Sub
        ElseIf tbFoliosUsados.Text.Length = 0 Then
            MsgBox("Captura el número de Folios Impresos.", MsgBoxStyle.Information, "Catálogos Series")
            Exit Sub
        ElseIf IsNumeric(tbFoliosUsados.Text) = False Then
            MsgBox("Captura un número válido de Folios Impresos.", MsgBoxStyle.Information, "Catálogos Series")
            Exit Sub
        ElseIf tbSerie.Text.Length = 0 Then
            MsgBox("Captura una Serie.", MsgBoxStyle.Information, "Catálogos Series")
            Exit Sub
        ElseIf tbUltimoFolioUsado.Text.Length = 0 Then
            MsgBox("Captura el número del Último Folio Usado.", MsgBoxStyle.Information, "Catálogos Series")
            Exit Sub
        ElseIf IsNumeric(tbUltimoFolioUsado.Text) = False Then
            MsgBox("Captura un número válido del Último Folio Usado.", MsgBoxStyle.Information, "Catálogos Series")
            Exit Sub
        ElseIf cbCompanias.SelectedValue = 0 Then
            MsgBox("Selecciona una Compañía.", MsgBoxStyle.Information, "Catálogos Series")
            Exit Sub
        End If
        ModificarRelVendedoresSeries(CInt(dgvRelVendedoresSeries.SelectedCells(0).Value.ToString()), IdRelVendedoresSeries, CInt(cbCompanias.SelectedValue.ToString()), CInt(tbFoliosUsados.Text), tbSerie.Text, CInt(tbUltimoFolioUsado.Text))
        DetalleVendedoresSeries(IdRelVendedoresSeries)
        habilitar()
    End Sub

    Private Sub bnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnEliminar.Click
        If dgvRelVendedoresSeries.Rows.Count = 0 Then
            MsgBox("Selecciona un registro.", MsgBoxStyle.Information, "Catálogos Series")
            Exit Sub
        End If
        BorrarDetalleVendedoresSeries(CInt(dgvRelVendedoresSeries.SelectedCells(0).Value.ToString()))
        DetalleVendedoresSeries(IdRelVendedoresSeries)
        habilitar()
    End Sub

    Private Sub bnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnCancelar.Click
        habilitar()
    End Sub
#End Region








    Private Sub tsbGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbGuardar.Click
        If opcion = "N" Then
            If (cbVendedores.Text.Length = 0) Then
                MsgBox("Selecciona un Vendedor.", MsgBoxStyle.Information, "Catálogos Series")
                Exit Sub
            ElseIf CInt(cbVendedores.SelectedValue.ToString()) = 0 Then
                MsgBox("Selecciona un Vendedor.", MsgBoxStyle.Information, "Catálogos Series")
                Exit Sub
            End If

        End If
    End Sub
End Class