Imports System.Data.SqlClient
Public Class FrmServiciosDigitales

    Private Sub Consulta_ServiciosDigitalesBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Consulta_ServiciosDigitalesBindingNavigatorSaveItem.Click
        If opcion = "N" Then
            Guardar(1)
        ElseIf opcion = "M" Then
            Guardar(2)
        End If
    End Sub
    Private Sub Guardar(ByVal opt As Integer)
        Dim cone As New SqlConnection(MiConexion)
        Dim Cmd As New SqlCommand
        Dim nombre As String = Nothing
        If opt = 1 Then
            nombre = "Nuevo_ServiciosDigitales"
        ElseIf opt = 2 Then
            nombre = "Modifica_ServiciosDigitales"
        End If
        cone.Open()
        With Cmd
            .CommandText = nombre
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = cone
            Dim prm As New SqlParameter("@Clv_ServicioDigital", SqlDbType.Int)
            Dim prm2 As New SqlParameter("@Nombre", SqlDbType.VarChar, 450)
            Dim prm3 As New SqlParameter("@Precio", SqlDbType.Money)
            If opt = 1 Then
                prm.Direction = ParameterDirection.Output
            ElseIf opt = 2 Then
                prm.Direction = ParameterDirection.Input
            End If
            prm.Value = Me.Clv_ServicioDigitalTextBox.Text
            prm2.Direction = ParameterDirection.Input
            prm3.Direction = ParameterDirection.Input
            prm2.Value = Me.NombreTextBox.Text
            If Me.Precio_UnitarioTextBox.Text = "" Then
                prm3.Value = 0
            Else
                prm3.Value = Me.Precio_UnitarioTextBox.Text
            End If
            .Parameters.Add(prm)
            .Parameters.Add(prm2)
            .Parameters.Add(prm3)
            Dim i As Integer = .ExecuteNonQuery
            If opt = 1 Then
                Me.Clv_ServicioDigitalTextBox.Text = prm.Value
            End If
        End With
        cone.Close()
        MsgBox("Se Ha Guardado con �xito", MsgBoxStyle.Information)
        bec_bnd = True
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        elimina()
        bec_bnd = True
        MsgBox("Se Ha Eliminado con �xito", MsgBoxStyle.Information)
        Me.Close()
    End Sub
    Private Sub elimina()
        Dim cone2 As New SqlConnection(MiConexion)
        Dim Cmd2 As New SqlCommand
     
        cone2.Open()
        With Cmd2
            .CommandText = "Borrar_ServiciosDigitales"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = cone2
            Dim prm As New SqlParameter("@Clv_ServicioDigital", SqlDbType.Int)  
            prm.Direction = ParameterDirection.Input
            prm.Value = Me.Clv_ServicioDigitalTextBox.Text
            .Parameters.Add(prm)
            Dim i As Integer = .ExecuteNonQuery

        End With
        cone2.Close()
    End Sub

    Private Sub FrmServiciosDigitales_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim cone4 As New SqlConnection(MiConexion)
        colorea(Me, Me.Name)
        If opcion = "C" Or opcion = "M" Then
            cone4.Open()
            Me.Consulta_ServiciosDigitalesTableAdapter.Connection = cone4
            Me.Consulta_ServiciosDigitalesTableAdapter.Fill(Me.DataSetLidia2.Consulta_ServiciosDigitales, gloclv_servicioDigital)
            cone4.Close()
        End If
        If opcion = "C" Then
            Me.Consulta_ServiciosDigitalesBindingNavigator.Enabled = False
            Me.NombreTextBox.ReadOnly = True
            Me.Precio_UnitarioTextBox.ReadOnly = True
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
End Class