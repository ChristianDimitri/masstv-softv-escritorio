﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwGrupoIngresos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim NombreLabel As System.Windows.Forms.Label
        Dim Clv_calleLabel1 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ConsultarButton = New System.Windows.Forms.Button()
        Me.SalirButton = New System.Windows.Forms.Button()
        Me.ModificarButton = New System.Windows.Forms.Button()
        Me.NuevoButton = New System.Windows.Forms.Button()
        Me.GrupoConceptosDataGrid = New System.Windows.Forms.DataGridView()
        Me.IdGrupo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Concepto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Posicion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdProgramacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ConceptoButton = New System.Windows.Forms.Button()
        Me.ConceptoTextBox = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.IdGrupoLabel1 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ConceptoLabel1 = New System.Windows.Forms.Label()
        Me.IdGrupoButton = New System.Windows.Forms.Button()
        Me.IdGrupoTextBox = New System.Windows.Forms.TextBox()
        Me.CMBLabel10 = New System.Windows.Forms.Label()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.CMBLabel1478 = New System.Windows.Forms.Label()
        Me.cbCompania = New System.Windows.Forms.ComboBox()
        NombreLabel = New System.Windows.Forms.Label()
        Clv_calleLabel1 = New System.Windows.Forms.Label()
        CType(Me.GrupoConceptosDataGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.ForeColor = System.Drawing.Color.White
        NombreLabel.Location = New System.Drawing.Point(11, 76)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(75, 15)
        NombreLabel.TabIndex = 3
        NombreLabel.Text = "Concepto :"
        '
        'Clv_calleLabel1
        '
        Clv_calleLabel1.AutoSize = True
        Clv_calleLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_calleLabel1.ForeColor = System.Drawing.Color.White
        Clv_calleLabel1.Location = New System.Drawing.Point(11, 39)
        Clv_calleLabel1.Name = "Clv_calleLabel1"
        Clv_calleLabel1.Size = New System.Drawing.Size(50, 15)
        Clv_calleLabel1.TabIndex = 1
        Clv_calleLabel1.Text = "Clave :"
        '
        'ConsultarButton
        '
        Me.ConsultarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ConsultarButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConsultarButton.Location = New System.Drawing.Point(884, 71)
        Me.ConsultarButton.Name = "ConsultarButton"
        Me.ConsultarButton.Size = New System.Drawing.Size(112, 34)
        Me.ConsultarButton.TabIndex = 6
        Me.ConsultarButton.Text = "&Consultar"
        Me.ConsultarButton.UseVisualStyleBackColor = True
        '
        'SalirButton
        '
        Me.SalirButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.SalirButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SalirButton.Location = New System.Drawing.Point(884, 668)
        Me.SalirButton.Name = "SalirButton"
        Me.SalirButton.Size = New System.Drawing.Size(112, 34)
        Me.SalirButton.TabIndex = 9
        Me.SalirButton.Text = "&Salir"
        Me.SalirButton.UseVisualStyleBackColor = True
        '
        'ModificarButton
        '
        Me.ModificarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ModificarButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ModificarButton.Location = New System.Drawing.Point(884, 110)
        Me.ModificarButton.Name = "ModificarButton"
        Me.ModificarButton.Size = New System.Drawing.Size(112, 34)
        Me.ModificarButton.TabIndex = 7
        Me.ModificarButton.Text = "&Modificar"
        Me.ModificarButton.UseVisualStyleBackColor = True
        '
        'NuevoButton
        '
        Me.NuevoButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.NuevoButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NuevoButton.Location = New System.Drawing.Point(884, 30)
        Me.NuevoButton.Name = "NuevoButton"
        Me.NuevoButton.Size = New System.Drawing.Size(112, 35)
        Me.NuevoButton.TabIndex = 5
        Me.NuevoButton.Text = "&Nuevo"
        Me.NuevoButton.UseVisualStyleBackColor = True
        '
        'GrupoConceptosDataGrid
        '
        Me.GrupoConceptosDataGrid.AllowUserToAddRows = False
        Me.GrupoConceptosDataGrid.AllowUserToDeleteRows = False
        Me.GrupoConceptosDataGrid.BackgroundColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GrupoConceptosDataGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.GrupoConceptosDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GrupoConceptosDataGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdGrupo, Me.Concepto, Me.Posicion, Me.IdProgramacion})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GrupoConceptosDataGrid.DefaultCellStyle = DataGridViewCellStyle2
        Me.GrupoConceptosDataGrid.GridColor = System.Drawing.SystemColors.Window
        Me.GrupoConceptosDataGrid.Location = New System.Drawing.Point(226, 30)
        Me.GrupoConceptosDataGrid.Name = "GrupoConceptosDataGrid"
        Me.GrupoConceptosDataGrid.ReadOnly = True
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GrupoConceptosDataGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.GrupoConceptosDataGrid.RowTemplate.Height = 24
        Me.GrupoConceptosDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.GrupoConceptosDataGrid.Size = New System.Drawing.Size(652, 672)
        Me.GrupoConceptosDataGrid.TabIndex = 17
        '
        'IdGrupo
        '
        Me.IdGrupo.DataPropertyName = "IdGrupoIngresos"
        Me.IdGrupo.HeaderText = "Clave"
        Me.IdGrupo.Name = "IdGrupo"
        Me.IdGrupo.ReadOnly = True
        Me.IdGrupo.Width = 67
        '
        'Concepto
        '
        Me.Concepto.DataPropertyName = "Concepto"
        Me.Concepto.HeaderText = "Concepto"
        Me.Concepto.Name = "Concepto"
        Me.Concepto.ReadOnly = True
        Me.Concepto.Width = 300
        '
        'Posicion
        '
        Me.Posicion.DataPropertyName = "Pos"
        Me.Posicion.HeaderText = "Posición"
        Me.Posicion.Name = "Posicion"
        Me.Posicion.ReadOnly = True
        Me.Posicion.Visible = False
        '
        'IdProgramacion
        '
        Me.IdProgramacion.DataPropertyName = "IdProgramacion"
        Me.IdProgramacion.HeaderText = "Id Programación"
        Me.IdProgramacion.Name = "IdProgramacion"
        Me.IdProgramacion.ReadOnly = True
        Me.IdProgramacion.Width = 120
        '
        'ConceptoButton
        '
        Me.ConceptoButton.BackColor = System.Drawing.Color.DarkOrange
        Me.ConceptoButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ConceptoButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConceptoButton.ForeColor = System.Drawing.Color.Black
        Me.ConceptoButton.Location = New System.Drawing.Point(14, 198)
        Me.ConceptoButton.Name = "ConceptoButton"
        Me.ConceptoButton.Size = New System.Drawing.Size(88, 23)
        Me.ConceptoButton.TabIndex = 4
        Me.ConceptoButton.Text = "&Buscar"
        Me.ConceptoButton.UseVisualStyleBackColor = False
        '
        'ConceptoTextBox
        '
        Me.ConceptoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ConceptoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConceptoTextBox.Location = New System.Drawing.Point(14, 172)
        Me.ConceptoTextBox.Name = "ConceptoTextBox"
        Me.ConceptoTextBox.Size = New System.Drawing.Size(207, 21)
        Me.ConceptoTextBox.TabIndex = 3
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel1.Controls.Add(Me.IdGrupoLabel1)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(NombreLabel)
        Me.Panel1.Controls.Add(Me.ConceptoLabel1)
        Me.Panel1.Controls.Add(Clv_calleLabel1)
        Me.Panel1.Location = New System.Drawing.Point(14, 228)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(206, 474)
        Me.Panel1.TabIndex = 26
        '
        'IdGrupoLabel1
        '
        Me.IdGrupoLabel1.Location = New System.Drawing.Point(67, 39)
        Me.IdGrupoLabel1.Name = "IdGrupoLabel1"
        Me.IdGrupoLabel1.Size = New System.Drawing.Size(100, 23)
        Me.IdGrupoLabel1.TabIndex = 20
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(0, 4)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(141, 20)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Datos del Grupo"
        '
        'ConceptoLabel1
        '
        Me.ConceptoLabel1.Location = New System.Drawing.Point(16, 91)
        Me.ConceptoLabel1.Name = "ConceptoLabel1"
        Me.ConceptoLabel1.Size = New System.Drawing.Size(176, 58)
        Me.ConceptoLabel1.TabIndex = 22
        '
        'IdGrupoButton
        '
        Me.IdGrupoButton.BackColor = System.Drawing.Color.DarkOrange
        Me.IdGrupoButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.IdGrupoButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IdGrupoButton.ForeColor = System.Drawing.Color.Black
        Me.IdGrupoButton.Location = New System.Drawing.Point(118, 115)
        Me.IdGrupoButton.Name = "IdGrupoButton"
        Me.IdGrupoButton.Size = New System.Drawing.Size(88, 23)
        Me.IdGrupoButton.TabIndex = 2
        Me.IdGrupoButton.Text = "&Buscar"
        Me.IdGrupoButton.UseVisualStyleBackColor = False
        '
        'IdGrupoTextBox
        '
        Me.IdGrupoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.IdGrupoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IdGrupoTextBox.Location = New System.Drawing.Point(14, 118)
        Me.IdGrupoTextBox.Name = "IdGrupoTextBox"
        Me.IdGrupoTextBox.Size = New System.Drawing.Size(95, 21)
        Me.IdGrupoTextBox.TabIndex = 1
        '
        'CMBLabel10
        '
        Me.CMBLabel10.AutoSize = True
        Me.CMBLabel10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel10.Location = New System.Drawing.Point(10, 153)
        Me.CMBLabel10.Name = "CMBLabel10"
        Me.CMBLabel10.Size = New System.Drawing.Size(82, 16)
        Me.CMBLabel10.TabIndex = 23
        Me.CMBLabel10.Text = "Concepto :"
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(10, 98)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(56, 16)
        Me.CMBLabel2.TabIndex = 22
        Me.CMBLabel2.Text = "Clave :"
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(9, 9)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(106, 20)
        Me.CMBLabel1.TabIndex = 21
        Me.CMBLabel1.Text = "Buscar por :"
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(884, 604)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(112, 34)
        Me.Button1.TabIndex = 8
        Me.Button1.Text = "&Eliminar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'CMBLabel1478
        '
        Me.CMBLabel1478.AutoSize = True
        Me.CMBLabel1478.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1478.Location = New System.Drawing.Point(11, 38)
        Me.CMBLabel1478.Name = "CMBLabel1478"
        Me.CMBLabel1478.Size = New System.Drawing.Size(78, 16)
        Me.CMBLabel1478.TabIndex = 40
        Me.CMBLabel1478.Text = "Compañía"
        '
        'cbCompania
        '
        Me.cbCompania.DisplayMember = "nombreCompania"
        Me.cbCompania.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbCompania.FormattingEnabled = True
        Me.cbCompania.Location = New System.Drawing.Point(14, 57)
        Me.cbCompania.Name = "cbCompania"
        Me.cbCompania.Size = New System.Drawing.Size(207, 23)
        Me.cbCompania.TabIndex = 0
        Me.cbCompania.ValueMember = "ClvCompania"
        '
        'BrwGrupoIngresos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1008, 730)
        Me.Controls.Add(Me.CMBLabel1478)
        Me.Controls.Add(Me.cbCompania)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ConceptoButton)
        Me.Controls.Add(Me.ConceptoTextBox)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.IdGrupoButton)
        Me.Controls.Add(Me.IdGrupoTextBox)
        Me.Controls.Add(Me.CMBLabel10)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.GrupoConceptosDataGrid)
        Me.Controls.Add(Me.ConsultarButton)
        Me.Controls.Add(Me.SalirButton)
        Me.Controls.Add(Me.ModificarButton)
        Me.Controls.Add(Me.NuevoButton)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "BrwGrupoIngresos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catalago de Grupo de Ingresos"
        CType(Me.GrupoConceptosDataGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ConsultarButton As System.Windows.Forms.Button
    Friend WithEvents SalirButton As System.Windows.Forms.Button
    Friend WithEvents ModificarButton As System.Windows.Forms.Button
    Friend WithEvents NuevoButton As System.Windows.Forms.Button
    Friend WithEvents GrupoConceptosDataGrid As System.Windows.Forms.DataGridView
    Friend WithEvents ConceptoButton As System.Windows.Forms.Button
    Friend WithEvents ConceptoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents IdGrupoLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents ConceptoLabel1 As System.Windows.Forms.Label
    Friend WithEvents IdGrupoButton As System.Windows.Forms.Button
    Friend WithEvents IdGrupoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel10 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents CMBLabel1478 As System.Windows.Forms.Label
    Friend WithEvents cbCompania As System.Windows.Forms.ComboBox
    Friend WithEvents IdGrupo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Concepto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Posicion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdProgramacion As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
