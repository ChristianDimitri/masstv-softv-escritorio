﻿Imports System.Data.SqlClient
Imports System.Collections.Generic
Public Class FrmServicios
    Public MiOpLoc As Char = "N"
    Public MICLAVE As Integer = 0

    

    Private Sub FrmServicios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ColoreaServicios()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IdSistema = "AG" And (GloClv_TipSer = 3 Or GloClv_TipSer = 6) Then
            Me.TextBox3.Visible = True
            Me.Label11.Visible = True
        End If
        Me.ComboBox6.Text = Nothing
        'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MUESTRASOLOTARIFADOS' Puede moverla o quitarla según sea necesario.
        Me.MUESTRASOLOTARIFADOSTableAdapter.Connection = CON
        Me.MUESTRASOLOTARIFADOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRASOLOTARIFADOS)
        Me.MuestraTipoPromocionTableAdapter.Connection = CON
        Me.MuestraTipoPromocionTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipoPromocion, GloClv_TipSer)
        Me.MUESTRATRABAJOS_NOCOBROMENSUALTableAdapter.Connection = CON
        Me.MUESTRATRABAJOS_NOCOBROMENSUALTableAdapter.Fill(Me.DataSetEDGAR.MUESTRATRABAJOS_NOCOBROMENSUAL, GloClv_TipSer, 0)
        Me.MUESTRA_TIPOCLIENTESTableAdapter.Connection = CON
        Me.MUESTRA_TIPOCLIENTESTableAdapter.Fill(Me.DataSetEDGAR.MUESTRA_TIPOCLIENTES, 0)
        Me.MUESTRA_TIPOCLIENTES_2TableAdapter.Connection = CON
        Me.MUESTRA_TIPOCLIENTES_2TableAdapter.Fill(Me.DataSetEdgarRev2.MUESTRA_TIPOCLIENTES_2, 0)
        Me.MuestraTipSerPrincipalDescuentNetTableAdapter.Connection = CON
        Me.MuestraTipSerPrincipalDescuentNetTableAdapter.Fill(Me.DataSetEDGAR.MuestraTipSerPrincipalDescuentNet, 0)
        BaseII.limpiaParametros()
        cbClaveProducto.DataSource = BaseII.ConsultaDT("MuestraClaveProdServHL")

        gloClave = GloClv_Servicio
        If opcion = "N" Then
            Me.CONSERVICIOSBindingSource.AddNew()
            Me.Sale_en_CarteraCheckBox1.Checked = True
            Me.Es_PrincipalCheckBox.Checked = True
            Me.Sale_en_CarteraCheckBox.Checked = True
            Me.Genera_OrdenCheckBox.Checked = True
            Me.Sale_en_CarteraCheckBox1.Checked = False
            Me.Genera_OrdenCheckBox.Checked = False
            Me.Es_PrincipalCheckBox.Checked = False
            Me.Sale_en_CarteraCheckBox.Checked = False
            Me.ComboBox5.Text = Nothing
            Panel1.Enabled = True
            'Lineas de Eric
            If GloClv_TipSer = 2 Then
                Me.CMBLabel10.Visible = True
                Me.CheckBox1.Visible = True
                Me.CheckBox1.TabStop = True
                Me.PRECIOLabel1.Text = "Alámbrico"
                Me.Label14.Text = "Inalambrico"
            End If
            '----------------------
        ElseIf opcion = "C" Then
            Panel1.Enabled = False
            Me.Clv_ServicioTextBox.Text = GloClv_Servicio
            bUSCASERVICIOS()
            BUSCAREL_TARIFADOS()
            'Me.BUSCAPuntos_Pago_AdelantadoTableAdapter.Connection = CON
            'Me.BUSCAPuntos_Pago_AdelantadoTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAPuntos_Pago_Adelantado, Me.Clv_ServicioTextBox.Text, 0)
            Me.LLena_Puntos()
            If Me.Genera_OrdenCheckBox.Checked = True Then
                Me.CONRel_Trabajos_NoCobroMensualTableAdapter.Connection = CON
                Me.CONRel_Trabajos_NoCobroMensualTableAdapter.Fill(Me.DataSetEDGAR.CONRel_Trabajos_NoCobroMensual, Me.Clv_ServicioTextBox.Text)
            End If
            MuestraDesCuentoNet()
            'Lineas de Eric
            If GloClv_TipSer = 2 Then
                Me.CMBLabel10.Visible = True
                Me.CheckBox1.Visible = True
                Me.PRECIOLabel1.Text = "Alámbrico"

                Me.Label14.Text = "Inalambrico"

                ValidaAplicaSoloInternet()
            End If
            If IdSistema = "AG" And (GloClv_TipSer = 3 Or GloClv_TipSer = 6) Then
                Me.CONSULTAClv_EquiTableAdapter.Connection = CON
                Me.CONSULTAClv_EquiTableAdapter.Fill(Me.DataSetLidia.CONSULTAClv_Equi, Me.Clv_TxtTextBox.Text)
            End If

            '---------------
            cbClaveProducto.Enabled = False
            ObtieneClaveProductoHL()
        ElseIf opcion = "M" Then
            Panel1.Enabled = True
            Me.Clv_ServicioTextBox.Text = GloClv_Servicio
            bUSCASERVICIOS()
            BUSCAREL_TARIFADOS()
            'Me.BUSCAPuntos_Pago_AdelantadoTableAdapter.Connection = CON
            'Me.BUSCAPuntos_Pago_AdelantadoTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAPuntos_Pago_Adelantado, Me.Clv_ServicioTextBox.Text, 0)
            Me.LLena_Puntos()

          
            If Me.Genera_OrdenCheckBox.Checked = True Then
                Me.CONRel_Trabajos_NoCobroMensualTableAdapter.Connection = CON
                Me.CONRel_Trabajos_NoCobroMensualTableAdapter.Fill(Me.DataSetEDGAR.CONRel_Trabajos_NoCobroMensual, Me.Clv_ServicioTextBox.Text)
            End If
            MuestraDesCuentoNet()
            'Lineas de Eric
            If GloClv_TipSer = 2 Then
                Me.CMBLabel10.Visible = True
                Me.CheckBox1.Visible = True
                Me.CheckBox1.TabStop = True
                Me.PRECIOLabel1.Text = "Alámbrico"
                Me.Label14.Text = "Inalambrico"
                ValidaAplicaSoloInternet()
            End If
            If IdSistema = "AG" And (GloClv_TipSer = 3 Or GloClv_TipSer = 6) Then
                Me.CONSULTAClv_EquiTableAdapter.Connection = CON
                Me.CONSULTAClv_EquiTableAdapter.Fill(Me.DataSetLidia.CONSULTAClv_Equi, Me.Clv_TxtTextBox.Text)
            End If
            '---------------
            ObtieneClaveProductoHL()
        End If
        If Me.Genera_OrdenCheckBox.CheckState <> CheckState.Checked Then
            Me.ComboBox5.Text = Nothing
        End If
        Me.Clv_TipSerTextBox.Text = GloClv_TipSer
        Me.CLAVETextBox.Text = Me.ComboBox1.SelectedValue

        If Me.Clv_TipSerTextBox.Text = "1" Then
            Me.Panel13.Visible = False
            Me.Panel6.Visible = False
        End If
        If IdSistema = "VA" Then
            Me.Panel16.Visible = False
        End If
        Me.MUESTRATRABAJOSTableAdapter.Connection = CON
        Me.MUESTRATRABAJOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATRABAJOS, New System.Nullable(Of Integer)(CType(GloClv_TipSer, Integer)))
        CON.Close()

        ''''''''''''''''''''''''''''''''''''JUAN JOSÉ
        DimeSiAplicaIEPS(2, 0, CInt(GloClv_Servicio), 0)
        Me.AplicaIEPSCheckBox.Visible = VISIBLE_IEPS
        Me.IEPS2CheckBox.Visible = VISIBLE_IEPS

        If Me.Sale_en_CarteraCheckBox1.Checked = True Then
            Me.AplicaIEPSCheckBox.Visible = False
        Else
            Me.IEPS2CheckBox.Visible = False
        End If
        ''''''''''''''''''''''''''''''''''''fin JUAN JOSÉ
        Me.AplicaIEPSCheckBox.Checked = ConsultaIepsServicios(0)

        ''''''''''''''''''''''''''''''''''JUAN JOSÉ SERVICIO HD
        ConsultaServicioHD(CInt(Me.Clv_ServicioTextBox.Text))
        ''''''''''''''''''''''''''''''''''JUAN JOSÉ SERVICIO HD
        tbPrecio.Visible = False
        LabelPrecio.Visible = False
        LabelPrecioAdic.Visible = False
        tbPrecioAdic.Visible = False

        MuestraCuenta(Me.Clv_TxtTextBox.Text, "", 0)

    End Sub

    Private Sub ObtieneClaveProductoHL()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Servicio", SqlDbType.Int, GloClv_Servicio)
        BaseII.CreateMyParameter("@id_ClaveProdServ", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("ObtieneClaveProdServicio")
        If BaseII.dicoPar("@id_ClaveProdServ") > 0 Then
            cbClaveProducto.SelectedValue = BaseII.dicoPar("@id_ClaveProdServ")
        End If
    End Sub

    Private Sub GuardaClaveProductoHL()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Servicio", SqlDbType.BigInt, GloClv_Servicio)
            BaseII.CreateMyParameter("@id_ClaveProdServ", SqlDbType.BigInt, cbClaveProducto.SelectedValue)
            BaseII.Inserta("GuardaClaveProdServicioHL")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub LLena_Puntos()
        Dim reader As SqlDataReader
        Dim CON As New SqlConnection(MiConexion)
        Dim previousConnectionState As ConnectionState = CON.State
        Try
            
            If CON.State = ConnectionState.Closed Then
                CON.Open()
            End If
            Dim comando As SqlClient.SqlCommand
            comando = New SqlClient.SqlCommand
            With comando
                .Connection = CON
                .CommandText = "exec BUSCAPuntos_Pago_Adelantado " & Me.Clv_ServicioTextBox.Text & ",0"
                .CommandType = CommandType.Text
                .CommandTimeout = 0
                reader = comando.ExecuteReader()
                Using reader
                    While reader.Read
                        ' Process SprocResults datareader here.
                        Me.Puntos3NumericUpDown.Value = reader.GetValue(1)
                        Me.Puntos6NumericUpDown.Value = reader.GetValue(2)
                        Me.Puntos11NumericUpDown.Value = reader.GetValue(3)
                        Me.Punto_Pronto_PagoNumericUpDown.Value = reader.GetValue(4)
                    End While
                End Using
            End With
            CON.Close()

        Finally
            If previousConnectionState = ConnectionState.Closed Then
                CON.Close()
            End If
        End Try



    End Sub

    Private Sub bUSCASERVICIOS()
        Try
            Dim CON2 As New SqlConnection(MiConexion)
            CON2.Open()
            Me.CONSERVICIOSTableAdapter.Connection = CON2
            Me.CONSERVICIOSTableAdapter.Fill(Me.NewSofTvDataSet.CONSERVICIOS, New System.Nullable(Of Integer)(CType(GloClv_Servicio, Integer)))
            CON2.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Me.CONSERVICIOSBindingSource.CancelEdit()
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        Dim error1 As Integer = 0
        Dim mensaje As String = Nothing
        CON.Open()
        Me.Valida_borra_servicioTableAdapter.Connection = CON
        Me.Valida_borra_servicioTableAdapter.Fill(Me.ProcedimientosArnoldo2.Valida_borra_servicio, Me.Clv_TxtTextBox.Text, error1, mensaje)
        If error1 = 1 Then
            MsgBox(mensaje, MsgBoxStyle.Information)
            Me.Close()
            Exit Sub
        Else
            MsgBox("Realmente desea eliminar el servicio", MsgBoxStyle.YesNo)
            If MsgBoxResult.Yes Then
                Me.CONSERVICIOSTableAdapter.Connection = CON
                Me.CONSERVICIOSTableAdapter.Delete(GloClv_Servicio)
                bitsist(GloUsuario, 0, LocGloSistema, Me.Name.ToString, "Eliminación de servicio", "", "Eliminó el servicio: " + Me.DescripcionTextBox.Text, LocClv_Ciudad)
                GloBnd = True
                Me.Close()
            End If
        End If
        'If GloClv_TipSer = 2 Then
        '    Me.ValidaAplicaSoloInternetTableAdapter.Connection = CON
        '    Me.ValidaAplicaSoloInternetTableAdapter.Fill(Me.DataSetEric.ValidaAplicaSoloInternet, Me.Clv_ServicioTextBox.Text, eRespuesta)
        '    If eRespuesta = 1 Then
        '        BorAplicaSoloInternet()
        '    End If
        'End If

    End Sub

    Private Sub CONSERVICIOSBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONSERVICIOSBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            If Me.Genera_OrdenCheckBox.CheckState = CheckState.Checked Then
                If Len(Trim(Me.ComboBox5.Text)) = 0 Then
                    MsgBox("Seleccione el Servicios que se le va realizar al Cliente ", MsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            Me.Validate()
            GuardaIepsServicios(Me.AplicaIEPSCheckBox.CheckState, 0)
            GuardaServicioHD(CInt(Me.Clv_ServicioTextBox.Text))
            Me.CONSERVICIOSBindingSource.EndEdit()
            Me.CONSERVICIOSTableAdapter.Connection = CON
            Me.CONSERVICIOSTableAdapter.Update(Me.NewSofTvDataSet.CONSERVICIOS)
            GuardaClaveProductoHL()
            '--Me.NUEPuntos_Pago_AdelantadoTableAdapter.Connection = CON
            '--Me.NUEPuntos_Pago_AdelantadoTableAdapter.Fill(Me.NewSofTvDataSet.NUEPuntos_Pago_Adelantado, New System.Nullable(Of Integer)(CType(Me.Clv_ServicioTextBox.Text, Integer)), New System.Nullable(Of Integer)(CType(Me.Puntos3NumericUpDown.Value, Integer)), New System.Nullable(Of Integer)(CType(Me.Puntos6NumericUpDown.Value, Integer)), New System.Nullable(Of Integer)(CType(Me.Puntos11NumericUpDown.Value, Integer)), New System.Nullable(Of Integer)(CType(Me.Punto_Pronto_PagoNumericUpDown.Value, Integer)))
            '--
            Dim comando As SqlClient.SqlCommand
            comando = New SqlClient.SqlCommand
            With comando
                .Connection = CON
                .CommandText = "NUEPuntos_Pago_Adelantado "
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
                ' Create a SqlParameter for each parameter in the stored procedure.
                Dim prm As New SqlParameter("@CLV_SERVICIO", SqlDbType.Int)
                prm.Direction = ParameterDirection.Input
                prm.Value = Me.Clv_ServicioTextBox.Text
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@Puntos3", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = Me.Puntos3NumericUpDown.Value
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@Puntos6", SqlDbType.Int)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = Me.Puntos6NumericUpDown.Value
                .Parameters.Add(prm2)

                Dim prm3 As New SqlParameter("@Puntos11", SqlDbType.Int)
                prm3.Direction = ParameterDirection.Input
                prm3.Value = Me.Puntos11NumericUpDown.Value
                .Parameters.Add(prm3)

                Dim prm4 As New SqlParameter("@Punto_Pronto_Pago", SqlDbType.Int)
                prm4.Direction = ParameterDirection.Input
                prm4.Value = Me.Punto_Pronto_PagoNumericUpDown.Value
                .Parameters.Add(prm4)


                Dim i As Integer = comando.ExecuteNonQuery()

            End With
            '--

            If IsNumeric(Me.ContDescuentoNetTextBox.Text) = False Then Me.ContDescuentoNetTextBox.Text = 0
            If IsNumeric(Me.MensDescuentoNetTextBox.Text) = False Then Me.MensDescuentoNetTextBox.Text = 0
            If IsNumeric(Me.ComboBox6.SelectedValue) = False Then Me.ComboBox6.SelectedValue = 1

            'Me.Guarda_GeneralDescuentoNetTableAdapter.Fill(Me.DataSetEDGAR.Guarda_GeneralDescuentoNet, Me.Clv_ServicioTextBox.Text, Me.ContDescuentoNetTextBox.Text, Me.MensDescuentoNetTextBox.Text, Me.ComboBox6.SelectedValue)

            If Me.Genera_OrdenCheckBox.CheckState <> CheckState.Checked Then
                Me.BorrarRel_Trabajos_NoCobroMensual()
            Else
                GUARDARel_Trabajos_NoCobroMensualGUARDA()
            End If

            If GloClv_TipSer = 2 Then
                If Me.CheckBox1.Checked = True Then
                    Me.ValidaAplicaSoloInternetTableAdapter.Connection = CON
                    Me.ValidaAplicaSoloInternetTableAdapter.Fill(Me.DataSetEric.ValidaAplicaSoloInternet, Me.Clv_ServicioTextBox.Text, eRespuesta)
                    If eRespuesta = 0 Then
                        NueAplicaSoloInternet()
                    End If
                Else
                    Me.ValidaAplicaSoloInternetTableAdapter.Connection = CON
                    Me.ValidaAplicaSoloInternetTableAdapter.Fill(Me.DataSetEric.ValidaAplicaSoloInternet, Me.Clv_ServicioTextBox.Text, eRespuesta)
                    If eRespuesta = 1 Then
                        BorAplicaSoloInternet()
                    End If
                End If
            End If
            If IdSistema = "AG" And (GloClv_TipSer = 3 Or GloClv_TipSer = 6) Then
                Me.NUEVOClv_EquiTableAdapter.Connection = CON
                Me.NUEVOClv_EquiTableAdapter.Fill(Me.DataSetLidia.NUEVOClv_Equi, Me.Clv_TxtTextBox.Text, Me.TextBox3.Text)
            End If
            GuardaCuenta(Me.txtCuentaContable.Text, IIf(IsNumeric(Me.txtPosicion.Text), Me.txtPosicion.Text, 0), Me.Clv_TxtTextBox.Text, GloClv_TipSer)
            MsgBox(mensaje5)
            GloBnd = True
            CON.Close()
            If opcion <> "N" Then

                Me.Close()
            Else
                opcion = "M"
            End If


        Catch ex As System.Exception
            MsgBox("Ya Existe esa Clave cambiela por favor")
        End Try
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub BUSCAREL_TARIFADOS()
        Try
            Dim CON2 As New SqlConnection(MiConexion)
            CON2.Open()
            If IsNumeric(Me.Clv_ServicioTextBox.Text) = True Then
                Me.Clv_TipoClienteTextBox.Text = Me.ComboBox4.SelectedValue
                Me.BUSCAREL_TARIFADOS_SERVICIOSTableAdapter.Connection = CON2
                Me.BUSCAREL_TARIFADOS_SERVICIOSTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAREL_TARIFADOS_SERVICIOS, New System.Nullable(Of Integer)(CType(Me.Clv_ServicioTextBox.Text, Integer)), 0, Me.ComboBox4.SelectedValue)
                CON2.Close()

                'Renta de Aparato
                If (Clv_TipSerTextBox.Text = 3 Or Clv_TipSerTextBox.Text = 2) And ComboBox1.SelectedValue = 2 Then
                    ConRentaAparato(Clv_TipoClienteTextBox.Text, Clv_ServicioTextBox.Text)
                    LabelPrecio.Visible = True
                    tbPrecio.Visible = False
                    LabelPrecioAdic.Visible = False
                    tbPrecioAdic.Visible = True
                Else
                    LabelPrecio.Visible = False
                    tbPrecio.Visible = False
                    LabelPrecioAdic.Visible = False
                    tbPrecioAdic.Visible = False
                End If


            End If
            If Me.ComboBox4.SelectedValue = 1 Then
                Me.CheckBox2.Visible = True
            Else
                Me.CheckBox2.Checked = False
                Me.CheckBox2.Visible = False
            End If
            Cambios_GastosInstalacion(1)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Cambios_GastosInstalacion(ByVal opt As Integer)
        Dim Com As New SqlCommand
        Dim ConIns As New SqlConnection(MiConexion)
        ConIns.Open()
        With Com
            .CommandText = "Actualiza_Instalacion"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = ConIns
            Dim Pmt As New SqlParameter("@Clv_Llave", SqlDbType.BigInt)
            Dim Pmt2 As New SqlParameter("@Clv_TipoCliente", SqlDbType.Int)
            Dim Pmt3 As New SqlParameter("@GAstos_Instalacion", SqlDbType.Money)
            Dim pmt4 As New SqlParameter("@opc", SqlDbType.Int)

            Pmt.Direction = ParameterDirection.Input
            If (IsNumeric(Me.CLV_LLAVETextBox.Text) = False) Then
                Pmt.Value = 0
            Else
                Pmt.Value = Me.CLV_LLAVETextBox.Text
            End If
            .Parameters.Add(Pmt)
            Pmt2.Direction = ParameterDirection.Input
            If IsNumeric(Me.ComboBox4.SelectedValue) = False Then
                Pmt2.Value = 0
            Else
                Pmt2.Value = Me.ComboBox4.SelectedValue
            End If

            .Parameters.Add(Pmt2)
            Pmt3.Direction = ParameterDirection.InputOutput
            If IsNumeric(Me.TextBox7.Text) = False Then
                Me.TextBox7.Text = 0
            End If
            Pmt3.Value = Me.TextBox7.Text
            .Parameters.Add(Pmt3)
            pmt4.Direction = ParameterDirection.Input
            pmt4.Value = opt
            .Parameters.Add(pmt4)
            .ExecuteNonQuery()
            If opt = 1 Then
                Me.TextBox7.Text = Pmt3.Value
            End If
            '(@CLV_LLAVE INT,@Clv_TipoCliente int,@Gastos_Instalacion decimal(18,2) output,@opc int)
        End With
        ConIns.Close()
    End Sub


    Private Sub Clv_ServicioTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_ServicioTextBox.TextChanged
        Me.CLV_SERVICIOTextBox1.Text = Me.Clv_ServicioTextBox.Text
        If Me.Clv_ServicioTextBox.Text > 0 Then
            CONREL_TARIFADOS_SERVICIOSBindingNavigator.Enabled = True
        End If
    End Sub

    Private Sub limpia()
        Dim CON2 As New SqlConnection(MiConexion)
        CON2.Open()
        Me.CONREL_TARIFADOS_SERVICIOSTableAdapter.Connection = CON2
        Me.CONREL_TARIFADOS_SERVICIOSTableAdapter.Fill(Me.NewSofTvDataSet.CONREL_TARIFADOS_SERVICIOS, 0, 0)
        Me.ComboBox1.Text = Nothing
        Me.PRECIOTextBox1.Text = 0
        Me.Precio_AdicionalTextBox.Text = 0
        Me.DIA_INICIALNumericUpDown.Value = 1
        Me.DIA_FINALNumericUpDown.Minimum = 1
        Me.DIA_FINALNumericUpDown.Value = 1
        Me.Periodo_InicialDateTimePicker.MinDate = CDate(Now)
        Me.Periodo_FinalDateTimePicker.MinDate = CDate(Now)
        Me.Periodo_InicialDateTimePicker.Value = CDate(Now)
        Me.Periodo_FinalDateTimePicker.Value = CDate(Now)
        Me.Porcetaje_DescuentoTextBox.Text = ""
        'Me.Punto_Pronto_PagoNumericUpDown.Value = 0
        'Me.Puntos3NumericUpDown.Value = 0
        'M.Puntos6NumericUpDown.Value = 0
        'Me.Puntos11NumericUpDown.Value = 0
        Me.Genera_OrdenCheckBox1.Checked = False
        Me.Aplica_ComisionCheckBox.Checked = False
        Me.BRINCA_MESCheckBox.Checked = False
        'Me.VigenteCheckBox.Checked = True
        CON2.Close()
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Me.CLAVETextBox.Text = Me.ComboBox1.SelectedValue
        If Me.ComboBox1.Text = "MENSUALIDAD" Then
            Me.BRINCA_MESCheckBox.Checked = False
            Me.BRINCA_MESCheckBox.Visible = False
            Me.BrincaMes_Label.Visible = False
        End If
        'If Me.ComboBox1.Text = "CONTRATACIÓN" Then
        'Me.Panel22.Visible = True
        'Else
        Me.Panel22.Visible = False
        'End If
        If Me.ComboBox1.Text = "CONTRATACIÓN" Or Me.ComboBox1.Text = "RECONEXION" Then
            Me.BRINCA_MESCheckBox.Visible = True
            Me.BrincaMes_Label.Visible = True
        End If
        If Me.Clv_TipSerTextBox.Text = "1" Then
            Me.Panel13.Visible = False
            Me.Panel6.Visible = False
        End If
        Me.IEPS2CheckBox.Checked = ConsultaIepsServicios(Me.ComboBox1.SelectedValue)

        'Renta de Aparato
        If (Clv_TipSerTextBox.Text = 3 Or Clv_TipSerTextBox.Text = 2) And ComboBox1.SelectedValue = 2 Then
            ConRentaAparato(Clv_TipoClienteTextBox.Text, Clv_ServicioTextBox.Text)
            LabelPrecio.Visible = True
            tbPrecio.Visible = False
            LabelPrecioAdic.Visible = False
            tbPrecioAdic.Visible = True
        Else
            LabelPrecio.Visible = False
            tbPrecio.Visible = False
            LabelPrecioAdic.Visible = False
            tbPrecioAdic.Visible = False
        End If


    End Sub
    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        eliminar()
    End Sub

    Private Sub cobromensual()
        If Me.Sale_en_CarteraCheckBox1.Checked = True Then
            Me.SplitContainer1.Enabled = True
            Me.Panel2.Visible = False
            Me.Sale_en_CarteraCheckBox.Checked = False
            Me.Sale_en_CarteraCheckBox.Visible = False
            Me.Genera_OrdenCheckBox.Checked = False
            Me.Genera_OrdenCheckBox.Visible = False
            Me.Panel3.Visible = False
            Me.TabControl1.Enabled = True
            Me.PnlClaveEquivalente.Visible = True
            Me.pnlContable.Visible = False
        Else
            Me.SplitContainer1.Enabled = False
            Me.Panel2.Visible = True
            Me.Sale_en_CarteraCheckBox.Visible = True
            Me.Genera_OrdenCheckBox.Visible = True
            Me.Panel3.Visible = True
            Me.TabControl1.Enabled = False
            Me.PnlClaveEquivalente.Visible = False
            Me.pnlContable.Visible = False
        End If
    End Sub




    'Private Sub GUARDAR()
    '    Dim LOCCLV_LLAVE As Integer = 0
    '    If IsNumeric(CLV_LLAVETextBox.Text) = True And CLV_LLAVETextBox.Text > 0 Then
    '        MICLAVE = CLV_LLAVETextBox.Text
    '    End If

    '    If IsNumeric(Me.PRECIOTextBox1.Text) = False Then Me.PRECIOTextBox1.Text = 0
    '    'Me.CONREL_TARIFADOS_SERVICIOSTableAdapter.Insert(Me.Clv_ServicioTextBox.Text, Me.CLAVETextBox.Text, Me.PRECIOTextBox1.Text, Me.DIA_INICIALNumericUpDown.Value, Me.DIA_FINALNumericUpDown.Value, Me.BRINCA_MESCheckBox.Checked, Me.Periodo_InicialDateTimePicker.Value, Me.Periodo_FinalDateTimePicker.Value, Me.Puntos3NumericUpDown.Value, Me.Puntos6NumericUpDown.Value, Me.Puntos11NumericUpDown.Value, Me.Porcetaje_DescuentoNumericUpDown.Value, Me.AplicanComCheckBox.Checked, Me.Genera_OrdenCheckBox1.Checked, Me.Precio_AdicionalTextBox.Text, Me.Punto_Pronto_PagoNumericUpDown.Value, LOCCLV_LLAVE)
    '    Me.Validate()
    '    Me.CONREL_TARIFADOS_SERVICIOSTableAdapter.Update(Me.NewSofTvDataSet.CONREL_TARIFADOS_SERVICIOS)
    '    BUSCAREL_TARIFADOS()
    '    If MICLAVE > 0 Then
    '        CLV_LLAVETextBox.Text = MICLAVE
    '    End If
    'End Sub

    Private Sub CLV_LLAVETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CLV_LLAVETextBox.TextChanged
        Try
            If IsNumeric(CLV_LLAVETextBox.Text) = True Then
                If CLV_LLAVETextBox.Text > 0 Then
                    '-- Me.CONREL_TARIFADOS_SERVICIOSTableAdapter.Fill(Me.NewSofTvDataSet.CONREL_TARIFADOS_SERVICIOS, CLV_LLAVETextBox.Text)
                    ToolStripButton2.Enabled = True
                    ToolStripButton4.Enabled = True
                Else
                    ToolStripButton2.Enabled = False
                    ToolStripButton4.Enabled = False
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub modificar()
        Try
            Dim CON3 As New SqlConnection(MiConexion)
            CON3.Open()
            If Me.Clv_TipSerTextBox.Text = "1" Then
                Me.Panel13.Visible = False
                Me.Panel6.Visible = False
            End If

            If Len(Trim(Me.ComboBox4.Text)) > 0 Then
                If IsNumeric(Me.ComboBox4.SelectedValue) = True Then Me.Clv_TipoClienteTextBox.Text = Me.ComboBox4.SelectedValue
                If IsNumeric(Me.Clv_TipoClienteTextBox.Text) = True Then

                    If IsNumeric(CLV_LLAVETextBox.Text) = True Then
                        If CLV_LLAVETextBox.Text > 0 Then



                            MiOpLoc = "M"
                            Me.Panel15.Enabled = False
                            Me.Panel4.Enabled = True
                            Me.Panel21.Enabled = True
                            Me.BindingNavigatorAddNewItem.Enabled = False
                            Me.CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem.Enabled = True
                            Me.ToolStripButton2.Enabled = False
                            Me.ToolStripButton2.Enabled = True
                            Me.ToolStripButton3.Enabled = True
                            Me.CONREL_TARIFADOS_SERVICIOSTableAdapter.Connection = CON3
                            Me.CONREL_TARIFADOS_SERVICIOSTableAdapter.Fill(Me.NewSofTvDataSet.CONREL_TARIFADOS_SERVICIOS, CLV_LLAVETextBox.Text, Me.ComboBox4.SelectedValue)
                            Me.DataGridView1.Enabled = False

                            'Renta de Aparato
                            If (Clv_TipSerTextBox.Text = 3 Or Clv_TipSerTextBox.Text = 2) And ComboBox1.SelectedValue = 2 Then
                                ConRentaAparato(Clv_TipoClienteTextBox.Text, Clv_ServicioTextBox.Text)
                                LabelPrecio.Visible = True
                                tbPrecio.Visible = False
                                LabelPrecioAdic.Visible = False
                                tbPrecioAdic.Visible = True
                            Else
                                LabelPrecio.Visible = False
                                tbPrecio.Visible = False
                                LabelPrecioAdic.Visible = False
                                tbPrecioAdic.Visible = False
                            End If


                        Else
                            ToolStripButton2.Enabled = False
                        End If
                    End If
                Else
                    MsgBox("Seleccione el Tipo de Cobro ", MsgBoxStyle.Information)
                End If
            Else
                MsgBox("Seleccione el Tipo de Cobro ", MsgBoxStyle.Information)
            End If
            CON3.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub guardar()
        Dim LOCCLV_LLAVE As Integer = 0
        Dim bnd As Boolean
        Try
            Dim CON3 As New SqlConnection(MiConexion)
            CON3.Open()
            If Len(Trim(Me.ComboBox4.Text)) > 0 Then
                If IsNumeric(Me.Clv_TipoClienteTextBox.Text) = True Then

                    If IsNumeric(Me.CLAVETextBox.Text) = True Then 'And IsNumeric(Me.DataGridView1.SelectedCells(0).Value) = True Then
                        If Me.Genera_OrdenCheckBox1.CheckState = CheckState.Checked Then
                            If Len(Trim(Me.ComboBox3.Text)) = 0 Then
                                MsgBox("Seleccione el servicio al Cliente que se va realizar,. Ya que esta activada la casilla de genera orden.")
                                Exit Sub
                            End If
                            If IsNumeric(Me.ComboBox3.SelectedValue) = False Then
                                MsgBox("Seleccione el servicio al Cliente que se va realizar,. Ya que esta activada la casilla de genera orden.")
                                Exit Sub
                            End If
                        End If

                        Me.Clv_TipoPromocionTextBox.Text = Me.ComboBox2.SelectedValue
                        If MiOpLoc = "N" Then
                            Me.ValidaPeriodosTableAdapter.Connection = CON3
                            Me.ValidaPeriodosTableAdapter.Fill(Me.NewSofTvDataSet.ValidaPeriodos, 0, New System.Nullable(Of Date)(CType(Me.Periodo_InicialDateTimePicker.Value, Date)), New System.Nullable(Of Date)(CType(Me.Periodo_FinalDateTimePicker.Value, Date)), CInt(Me.DIA_INICIALNumericUpDown.Value), CInt(Me.DIA_FINALNumericUpDown.Value), New System.Nullable(Of Long)(CType(Me.Clv_ServicioTextBox.Text, Long)), New System.Nullable(Of Integer)(CType(Me.CLAVETextBox.Text, Integer)), Me.Clv_TipoClienteTextBox.Text, bnd)
                        Else
                            Me.ValidaPeriodosTableAdapter.Connection = CON3
                            Me.ValidaPeriodosTableAdapter.Fill(Me.NewSofTvDataSet.ValidaPeriodos, Me.CLV_LLAVETextBox.Text, New System.Nullable(Of Date)(CType(Me.Periodo_InicialDateTimePicker.Value, Date)), New System.Nullable(Of Date)(CType(Me.Periodo_FinalDateTimePicker.Value, Date)), CInt(Me.DIA_INICIALNumericUpDown.Value), CInt(Me.DIA_FINALNumericUpDown.Value), New System.Nullable(Of Long)(CType(Me.Clv_ServicioTextBox.Text, Long)), New System.Nullable(Of Integer)(CType(Me.CLAVETextBox.Text, Integer)), Me.Clv_TipoClienteTextBox.Text, bnd)
                        End If
                        If bnd = True Then
                            MsgBox("Los Peridos de Vigencia no son Validos")
                            Exit Sub
                        End If

                        If IsNumeric(Clv_TipoPromocionTextBox.Text) = False Then Clv_TipoPromocionTextBox.Text = 0
                        If IsNumeric(Me.PRECIOTextBox1.Text) = False Then Me.PRECIOTextBox1.Text = 0
                        If IsNumeric(Me.Clv_TrabajoTextBox) = False Then Me.Clv_TrabajoTextBox.Text = 0
                        If IsNumeric(Me.Numero_CortesiasTextBox.Text) = False Then Me.Numero_CortesiasTextBox.Text = 0
                        If IsNumeric(Me.Precio_InalambricoTextBox.Text) = False Then Me.Precio_InalambricoTextBox.Text = 0
                        If IsNumeric(Me.Porcetaje_DescuentoTextBox.Text) = False Then Me.Porcetaje_DescuentoTextBox.Text = 0
                        If IsNumeric(Me.Precio_AdicionalTextBox.Text) = False Then Me.Precio_AdicionalTextBox.Text = 0
                        'Me.CONREL_TARIFADOS_SERVICIOSTableAdapter.Insert(Me.Clv_ServicioTextBox.Text, Me.CLAVETextBox.Text, Me.PRECIOTextBox1.Text, Me.DIA_INICIALNumericUpDown.Value, Me.DIA_FINALNumericUpDown.Value, Me.BRINCA_MESCheckBox.Checked, Me.Periodo_InicialDateTimePicker.Value, Me.Periodo_FinalDateTimePicker.Value, Me.Puntos3NumericUpDown.Value, Me.Puntos6NumericUpDown.Value, Me.Puntos11NumericUpDown.Value, Me.Porcetaje_DescuentoNumericUpDown.Value, Me.AplicanComCheckBox.Checked, Me.Genera_OrdenCheckBox1.Checked, Me.Precio_AdicionalTextBox.Text, Me.Punto_Pronto_PagoNumericUpDown.Value, LOCCLV_LLAVE)
                        Me.CLV_SERVICIOTextBox1.Text = Me.Clv_ServicioTextBox.Text
                        Me.Validate()
                        If MiOpLoc = "N" Then
                            Me.CONREL_TARIFADOS_SERVICIOSTableAdapter.Connection = CON3
                            Me.CONREL_TARIFADOS_SERVICIOSTableAdapter.Insert(Me.Clv_ServicioTextBox.Text, Me.CLAVETextBox.Text, Me.PRECIOTextBox1.Text, Me.DIA_INICIALNumericUpDown.Value, Me.DIA_FINALNumericUpDown.Value, Me.BRINCA_MESCheckBox.Checked, Me.Periodo_InicialDateTimePicker.Value, Me.Periodo_FinalDateTimePicker.Value, Me.Porcetaje_DescuentoTextBox.Text, Me.Aplica_ComisionCheckBox.Checked, Me.Genera_OrdenCheckBox1.Checked, Me.Precio_AdicionalTextBox.Text, Me.VigenteCheckBox.CheckState, 0, Me.Clv_TipoPromocionTextBox.Text, Me.ComboBox3.SelectedValue, Me.Numero_CortesiasTextBox.Text, Me.Precio_InalambricoTextBox.Text, CInt(Me.Clv_TipoClienteTextBox.Text), Me.Se_Cobra_ProporcionalCheckBox.CheckState, LOCCLV_LLAVE)
                            Me.CLV_LLAVETextBox.Text = LOCCLV_LLAVE
                            If Me.CheckBox2.Checked = True Then
                                Me.NUEREL_TARIFADOS_SERVICIOSALLTableAdapter.Connection = CON3
                                Me.NUEREL_TARIFADOS_SERVICIOSALLTableAdapter.Fill(Me.DataSetEric2.NUEREL_TARIFADOS_SERVICIOSALL, Me.Clv_ServicioTextBox.Text, Me.CLAVETextBox.Text, Me.PRECIOTextBox1.Text, Me.DIA_INICIALNumericUpDown.Value, Me.DIA_FINALNumericUpDown.Value, Me.BRINCA_MESCheckBox.Checked, Me.Periodo_InicialDateTimePicker.Value, Me.Periodo_FinalDateTimePicker.Value, Me.Porcetaje_DescuentoTextBox.Text, Me.Aplica_ComisionCheckBox.Checked, Me.Genera_OrdenCheckBox1.Checked, Me.Precio_AdicionalTextBox.Text, Me.VigenteCheckBox.CheckState, 0, Me.Clv_TipoPromocionTextBox.Text, Me.ComboBox3.SelectedValue, Me.Numero_CortesiasTextBox.Text, Me.Precio_InalambricoTextBox.Text, CInt(Me.Clv_TipoClienteTextBox.Text), Me.Se_Cobra_ProporcionalCheckBox.CheckState)
                            End If
                        Else
                            Me.MODREL_TARIFADOS_SERVICIOSTableAdapter.Connection = CON3
                            Me.MODREL_TARIFADOS_SERVICIOSTableAdapter.Fill(Me.NewSofTvDataSet.MODREL_TARIFADOS_SERVICIOS, New System.Nullable(Of Integer)(CType(Me.CLV_LLAVETextBox.Text, Integer)), New System.Nullable(Of Integer)(CType(Me.Clv_ServicioTextBox.Text, Integer)), New System.Nullable(Of Integer)(CType(Me.CLAVETextBox.Text, Integer)), New System.Nullable(Of Decimal)(CType(Me.PRECIOTextBox1.Text, Decimal)), New System.Nullable(Of Integer)(CType(Me.DIA_INICIALNumericUpDown.Value, Integer)), New System.Nullable(Of Integer)(CType(Me.DIA_FINALNumericUpDown.Value, Integer)), New System.Nullable(Of Boolean)(CType(Me.BRINCA_MESCheckBox.Checked, Boolean)), New System.Nullable(Of Date)(CType(Me.Periodo_InicialDateTimePicker.Value, Date)), New System.Nullable(Of Date)(CType(Me.Periodo_FinalDateTimePicker.Value, Date)), Me.Porcetaje_DescuentoTextBox.Text, New System.Nullable(Of Boolean)(CType(Me.Aplica_ComisionCheckBox.Checked, Boolean)), New System.Nullable(Of Boolean)(CType(Me.Genera_OrdenCheckBox1.Checked, Boolean)), New System.Nullable(Of Decimal)(CType(Me.Precio_AdicionalTextBox.Text, Decimal)), New System.Nullable(Of Boolean)(CType(Me.VigenteCheckBox.CheckState, Boolean)), 0, Me.Clv_TipoPromocionTextBox.Text, Me.ComboBox3.SelectedValue, Me.Numero_CortesiasTextBox.Text, Me.Precio_InalambricoTextBox.Text, Me.Clv_TipoClienteTextBox.Text, Me.Se_Cobra_ProporcionalCheckBox.CheckState)
                            If Me.CheckBox2.Checked = True Then
                                Me.MODREL_TARIFADOS_SERVICIOSALLTableAdapter.Connection = CON3
                                Me.MODREL_TARIFADOS_SERVICIOSALLTableAdapter.Fill(Me.DataSetEric2.MODREL_TARIFADOS_SERVICIOSALL, New System.Nullable(Of Integer)(CType(Me.CLV_LLAVETextBox.Text, Integer)), New System.Nullable(Of Integer)(CType(Me.Clv_ServicioTextBox.Text, Integer)), New System.Nullable(Of Integer)(CType(Me.CLAVETextBox.Text, Integer)), New System.Nullable(Of Decimal)(CType(Me.PRECIOTextBox1.Text, Decimal)), New System.Nullable(Of Integer)(CType(Me.DIA_INICIALNumericUpDown.Value, Integer)), New System.Nullable(Of Integer)(CType(Me.DIA_FINALNumericUpDown.Value, Integer)), New System.Nullable(Of Boolean)(CType(Me.BRINCA_MESCheckBox.Checked, Boolean)), New System.Nullable(Of Date)(CType(Me.Periodo_InicialDateTimePicker.Value, Date)), New System.Nullable(Of Date)(CType(Me.Periodo_FinalDateTimePicker.Value, Date)), Me.Porcetaje_DescuentoTextBox.Text, New System.Nullable(Of Boolean)(CType(Me.Aplica_ComisionCheckBox.Checked, Boolean)), New System.Nullable(Of Boolean)(CType(Me.Genera_OrdenCheckBox1.Checked, Boolean)), New System.Nullable(Of Decimal)(CType(Me.Precio_AdicionalTextBox.Text, Decimal)), New System.Nullable(Of Boolean)(CType(Me.VigenteCheckBox.CheckState, Boolean)), 0, Me.Clv_TipoPromocionTextBox.Text, Me.ComboBox3.SelectedValue, Me.Numero_CortesiasTextBox.Text, Me.Precio_InalambricoTextBox.Text, Me.Clv_TipoClienteTextBox.Text, Me.Se_Cobra_ProporcionalCheckBox.CheckState)
                            End If
                        End If

                        If IsNumeric(tbPrecio.Text) = False Then tbPrecio.Text = 0
                        If IsNumeric(tbPrecioAdic.Text) = False Then tbPrecioAdic.Text = 0
                        ModRentaAparato(Clv_TipoClienteTextBox.Text, Clv_ServicioTextBox.Text, tbPrecio.Text, tbPrecioAdic.Text)

                        Cambios_GastosInstalacion(2)
                        BUSCAREL_TARIFADOS()
                        GuardaIepsServicios(Me.IEPS2CheckBox.CheckState, Me.ComboBox1.SelectedValue)
                        MsgBox(mensaje5)
                        limpia()

                        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem.Enabled = False
                        Me.Panel4.Enabled = False
                        Me.Panel21.Enabled = False
                        Me.Panel15.Enabled = True
                        Me.BindingNavigatorAddNewItem.Enabled = True
                        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem.Enabled = False
                        Me.ToolStripButton2.Enabled = False
                        Me.ToolStripButton3.Enabled = False
                        Me.ToolStripButton4.Enabled = True
                        Me.DataGridView1.Enabled = True
                    Else
                        MsgBox("Se requiere que seleccione un Concepto")
                    End If
                Else
                    MsgBox("Seleccione el Tipo de Cobro ", MsgBoxStyle.Information)
                End If
            Else
                MsgBox("Seleccione el Tipo de Cobro ", MsgBoxStyle.Information)
            End If

            CON3.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub cancelar()
        limpia()
        Me.CONREL_TARIFADOS_SERVICIOSBindingSource.CancelEdit()
        Me.Panel4.Enabled = False
        Me.Panel21.Enabled = False
        Me.Panel15.Enabled = True
        Me.BindingNavigatorAddNewItem.Enabled = True
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem.Enabled = False
        Me.ToolStripButton2.Enabled = False
        Me.ToolStripButton3.Enabled = False
        Me.ToolStripButton4.Enabled = True
        Me.DataGridView1.Enabled = True
    End Sub

    Private Sub eliminar()
        Dim CON3 As New SqlConnection(MiConexion)
        CON3.Open()
        If Len(Trim(Me.ComboBox4.Text)) > 0 Then
            If IsNumeric(Me.Clv_TipoClienteTextBox.Text) = True Then
                limpia()
                Me.CONREL_TARIFADOS_SERVICIOSTableAdapter.Connection = CON3
                Me.CONREL_TARIFADOS_SERVICIOSTableAdapter.Delete(Me.CLV_LLAVETextBox.Text, Me.ComboBox4.SelectedValue)
                BUSCAREL_TARIFADOS()
                Me.Panel4.Enabled = False
                Me.Panel21.Enabled = False
                Me.BindingNavigatorAddNewItem.Enabled = True
                Me.CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem.Enabled = False
                Me.ToolStripButton2.Enabled = False
                Me.ToolStripButton3.Enabled = False
                Me.ToolStripButton4.Enabled = True
                Me.DataGridView1.Enabled = True
            Else
                MsgBox("Seleccione el Tipo de Cobro ", MsgBoxStyle.Information)
            End If
        Else
            MsgBox("Seleccione el Tipo de Cobro ", MsgBoxStyle.Information)
        End If
        CON3.Close()
    End Sub

    Private Sub nuevo()

        If Len(Trim(Me.ComboBox4.Text)) > 0 Then
            If IsNumeric(Me.ComboBox4.SelectedValue) = True Then Me.Clv_TipoClienteTextBox.Text = Me.ComboBox4.SelectedValue
            If IsNumeric(Me.Clv_TipoClienteTextBox.Text) = True Then

                If ComboBox1.SelectedValue = 2 Then
                    LabelPrecio.Visible = True
                    LabelPrecioAdic.Visible = True
                    tbPrecio.Visible = False
                    tbPrecioAdic.Visible = False
                Else
                    LabelPrecio.Visible = False
                    LabelPrecioAdic.Visible = False
                    tbPrecio.Visible = False
                    tbPrecioAdic.Visible = False
                End If

                Me.CONREL_TARIFADOS_SERVICIOSBindingSource.AddNew()
                Me.Panel15.Enabled = False
                Me.Panel4.Enabled = True
                Me.Panel21.Enabled = True
                Me.CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem.Enabled = True
                Me.ToolStripButton2.Enabled = False
                Me.ToolStripButton4.Enabled = False
                Me.ToolStripButton3.Enabled = True
                BindingNavigatorAddNewItem.Enabled = False
                Me.DataGridView1.Enabled = False
                limpia()
                MiOpLoc = "N"
                Me.Clv_TipoClienteTextBox.Text = Me.ComboBox4.SelectedValue


                'Renta de Aparato
                If (Clv_TipSerTextBox.Text = 3 Or Clv_TipSerTextBox.Text = 2) And ComboBox1.SelectedValue = 2 Then
                    ConRentaAparato(Clv_TipoClienteTextBox.Text, Clv_ServicioTextBox.Text)
                    LabelPrecio.Visible = True
                    tbPrecio.Visible = False
                    LabelPrecioAdic.Visible = False
                    tbPrecioAdic.Visible = True
                Else
                    LabelPrecio.Visible = False
                    tbPrecio.Visible = False
                    LabelPrecioAdic.Visible = False
                    tbPrecioAdic.Visible = False
                End If

            Else
                MsgBox("Seleccione el Tipo de Cobro ", MsgBoxStyle.Information)
            End If
        Else
            MsgBox("Seleccione el Tipo de Cobro ", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub CLV_LLAVETextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CLV_LLAVETextBox2.TextChanged
        If IsNumeric(CLV_LLAVETextBox2.Text) = True Then
            If CLV_LLAVETextBox2.Text > 0 Then
                'Me.CLV_LLAVETextBox.Text = CLV_LLAVETextBox2.Text
            End If
        End If
    End Sub



    Private Sub DIA_INICIALNumericUpDown_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DIA_INICIALNumericUpDown.ValueChanged
        Me.DIA_FINALNumericUpDown.Minimum = Me.DIA_INICIALNumericUpDown.Value + 1
    End Sub

    Private Sub Periodo_InicialDateTimePicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Periodo_InicialDateTimePicker.ValueChanged
        Me.Periodo_FinalDateTimePicker.MinDate = Me.Periodo_InicialDateTimePicker.Value
    End Sub

    Private Sub BindingNavigatorAddNewItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorAddNewItem.Click
        nuevo()
    End Sub

    Private Sub ToolStripButton4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton4.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim selec As String
        modificar()
        selec = Me.DataGridView1.SelectedCells.Item(1).Value.ToString
        If selec = "CONTRATACIÓN" Then
            Me.Panel19.Visible = True
            Me.Dame_PagosParcialesTableAdapter.Connection = CON
            Me.Dame_PagosParcialesTableAdapter.Fill(Me.DataSetLidia.Dame_PagosParciales)
            Me.ComboBox9.Text = ""
            
        End If

        If selec = "CONTRATACIÓN" And GloClv_TipSer = 2 Then
            'Me.Panel22.Visible = True
            
            If IdSistema = "LO" Or IdSistema = "YU" Then
                Me.PRECIOLabel1.Text = "Gtos Inst. Alambrico"
                Me.Label14.Text = "Gtos Inst. Inalámbrico"
            End If
            Cambios_GastosInstalacion(1)
        Else
            Me.Panel22.Visible = False
            Me.LabelPrecio.Visible = False
            If GloClv_TipSer = 2 Then
                Me.PRECIOLabel1.Text = "Alambrico"
                Me.Label14.Text = "Inalámbrico"
            End If
        End If

        CON.Close()
    End Sub

    Private Sub CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem.Click
        guardar()
    End Sub

    Private Sub ToolStripButton2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        eliminar()
    End Sub

    Private Sub ToolStripButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton3.Click
        cancelar()
    End Sub

    Private Sub CLAVETextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CLAVETextBox.TextChanged
        If IsNumeric(Me.CLAVETextBox.Text) = True Then
            If Me.Se_Cobra_ProporcionalCheckBox.CheckState = CheckState.Indeterminate Then Me.Se_Cobra_ProporcionalCheckBox.CheckState = CheckState.Unchecked
            Me.Se_Cobra_ProporcionalCheckBox.Visible = False
            Me.Label7.Visible = False
            'Me.Se_Cobra_ProporcionalCheckBox.CheckState = CheckState.Unchecked
            'If Me.CLAVETextBox.Text = 1 Then
            'PanelPagoAdelantado.Visible = False
            'If Me.CLAVETextBox.Text = 2 Then
            'PanelPagoAdelantado.Visible = True
            Me.Panel6.Visible = False
            Me.BRINCA_MESCheckBox.Enabled = True
            If Me.CLAVETextBox.Text = 3 Then
                'PanelPagoAdelantado.Visible = False
                Me.BRINCA_MESCheckBox.Enabled = False
                If MiOpLoc = "N" Then
                    Me.BRINCA_MESCheckBox.Checked = True
                End If
                'Else
                '   PanelPagoAdelantado.Visible = False
            ElseIf Me.CLAVETextBox.Text = 2 Then
                'Me.Panel6.Visible = True
                'If IdSistema = "VA" Then
                Me.Panel16.Visible = False
                'End If
            ElseIf Me.CLAVETextBox.Text = 1 Then
                Me.Se_Cobra_ProporcionalCheckBox.Visible = True
                Me.Label7.Visible = True
            End If
        End If
    End Sub

    Private Sub Clv_TipSerTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_TipSerTextBox.TextChanged
        Me.Panel16.Visible = False
        'Me.Panel17.Visible = False
        Me.Panel6.Visible = False
        If IsNumeric(Me.Clv_TipSerTextBox.Text) = True Then
            If Me.Clv_TipSerTextBox.Text = 2 Then
                'Me.Panel11adicional.Visible = False
                Me.Label2.Text = " Paquetes Internet :"
                Me.Panel6.Visible = True
                If IdSistema = "VA" Then
                    Me.Panel16.Visible = False
                End If
                Me.Panel11adicional2.Visible = False
                Me.Panel14.Visible = True
                Me.Panel16.Visible = True
                'Me.Panel17.Visible = True
                Me.DataGridView1.Columns(3).HeaderText = "Precio Inalàmbrico"
            ElseIf Me.Clv_TipSerTextBox.Text = 3 Or Me.Clv_TipSerTextBox.Text = 1 Then
                Me.Panel13.Visible = True
                If Me.Clv_TipSerTextBox.Text = 3 Then
                    Me.Panel6.Visible = True
                    Me.Label2.Text = " Paquetes Digitales :"
                End If
            End If
        End If
    End Sub





    Private Sub DataGridView1_CellContentClick_1(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        If IsNumeric(Me.DataGridView1.SelectedCells(0).Value) = True Then
            Me.CLV_LLAVETextBox.Text = Me.DataGridView1.SelectedCells(0).Value
        End If
    End Sub

    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If IsNumeric(Me.DataGridView1.SelectedCells(0).Value) = True Then
            Me.CLV_LLAVETextBox.Text = Me.DataGridView1.SelectedCells(0).Value
            If IsNumeric(Me.CLV_LLAVETextBox.Text) = True Then
                modificar()
            End If
        End If
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        Me.Clv_TipoPromocionTextBox.Text = Me.ComboBox2.SelectedValue
    End Sub



    Private Sub Genera_OrdenCheckBox1_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Genera_OrdenCheckBox1.CheckStateChanged
        If Me.Genera_OrdenCheckBox1.Checked = True Then
            Me.Panel12.Visible = True
            Me.Panel5.Width = 311
            Me.Panel5.Height = 152
        Else
            Me.Panel12.Visible = False
            Me.Panel5.Width = 311
            Me.Panel5.Height = 97

        End If
    End Sub

    Private Sub ComboBox3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox3.SelectedIndexChanged
        Me.Clv_TrabajoTextBox.Text = Me.ComboBox3.SelectedValue
    End Sub


    Private Sub Sale_en_CarteraCheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Sale_en_CarteraCheckBox1.CheckedChanged
        cobromensual()
        ''''''''''''''''''''''''''''''''''''JUAN JOSÉ
        If Me.Sale_en_CarteraCheckBox1.Checked = True Then
            Me.AplicaIEPSCheckBox.Visible = False
            DimeSiAplicaIEPS(2, 0, CInt(GloClv_Servicio), 0)
            Me.IEPS2CheckBox.Visible = VISIBLE_IEPS
        Else
            Me.IEPS2CheckBox.Visible = False
            DimeSiAplicaIEPS(2, 0, CInt(GloClv_Servicio), 0)
            Me.AplicaIEPSCheckBox.Visible = VISIBLE_IEPS
        End If
        ''''''''''''''''''''''''''''''''''''JUAN JOSÉ
    End Sub


    Private Sub DescripcionTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles DescripcionTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(DescripcionTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub



    Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.SelectedIndexChanged
        BUSCAREL_TARIFADOS()
    End Sub

    Private Sub Panel4_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel4.Paint

    End Sub

    Private Sub MuestraDesCuentoNet()
        Try
            Dim CON4 As New SqlConnection(MiConexion)

            If IsNumeric(Me.Clv_ServicioTextBox.Text) = True Then
                CON4.Open()
                Me.CONGeneralDescuentoNetTableAdapter.Connection = CON4
                Me.CONGeneralDescuentoNetTableAdapter.Fill(Me.DataSetEric2.CONGeneralDescuentoNet, Me.Clv_ServicioTextBox.Text, Me.ComboBox8.SelectedValue)
                CON4.Close()
            End If

            If Me.ComboBox8.SelectedValue = 1 Then
                Me.CheckBox3.Visible = True

            Else
                Me.CheckBox3.Visible = False
                Me.CheckBox3.Checked = False
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub



    Private Sub Genera_OrdenCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Genera_OrdenCheckBox.CheckedChanged
        If Me.Genera_OrdenCheckBox.CheckState = CheckState.Checked Then
            Panel18.Visible = True
        Else
            Panel18.Visible = False
        End If
    End Sub



    Private Sub GUARDARel_Trabajos_NoCobroMensualGUARDA()
        Try
            Dim CON4 As New SqlConnection(MiConexion)
            CON4.Open()
            If IsNumeric(Me.Clv_ServicioTextBox.Text) = True Then
                If Len(Trim(Me.ComboBox5.Text)) > 0 And IsNumeric(Me.ComboBox5.SelectedValue) = True Then
                    Me.GUARDARel_Trabajos_NoCobroMensualTableAdapter.Connection = CON4
                    Me.GUARDARel_Trabajos_NoCobroMensualTableAdapter.Fill(Me.DataSetEDGAR.GUARDARel_Trabajos_NoCobroMensual, Me.Clv_ServicioTextBox.Text, Me.ComboBox5.SelectedValue)
                End If
            End If
            CON4.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub BorrarRel_Trabajos_NoCobroMensual()
        Dim CON5 As New SqlConnection(MiConexion)
        CON5.Open()
        Try
            If IsNumeric(Me.Clv_ServicioTextBox.Text) = True Then
                Me.BORRel_Trabajos_NoCobroMensualTableAdapter.Connection = CON5
                Me.BORRel_Trabajos_NoCobroMensualTableAdapter.Fill(Me.DataSetEDGAR.BORRel_Trabajos_NoCobroMensual, Me.Clv_ServicioTextBox.Text)

            End If
            CON5.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub



    Private Sub ComboBox6_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox6.SelectedIndexChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Len(Trim(Me.ComboBox6.Text)) > 0 And IsNumeric(Me.ComboBox6.SelectedValue) = True Then
            Me.MuesteServiciosDescuentoNetTableAdapter.Connection = CON
            Me.MuesteServiciosDescuentoNetTableAdapter.Fill(Me.DataSetEDGAR.MuesteServiciosDescuentoNet, Me.ComboBox6.SelectedValue, 0)
        End If
        CON.Close()
    End Sub



    Private Sub BotonNuevo()
        Me.CONGeneralDescuentoNetDataGridView.Enabled = False
        PanelCapDesNet.Enabled = True
        Me.ComboBox6.Enabled = True
        Me.ComboBox7.Enabled = True
        Me.ComboBox6.Text = ""
        Me.ComboBox7.Text = ""

        Me.MensDescuentoNetTextBox.Enabled = True
        Me.ContDescuentoNetTextBox.Enabled = True
        Me.PuntosTextBox.Enabled = True
        Me.MensDescuentoNetTextBox.Text = 0
        Me.ContDescuentoNetTextBox.Text = 0
        Me.PuntosTextBox.Text = 0
        Me.ContDescuentoNetTextBox.Visible = True
        Me.MensDescuentoNetTextBox.Visible = True
        Me.PuntosTextBox.Visible = True

        Me.Agregar1.Enabled = True
        Me.Modificar1.Enabled = False
        Me.Eliminar1.Enabled = False
        Me.Guardar1.Enabled = True
        Me.Cancelar1.Enabled = True


        Me.ConceptoComboBox.Visible = False
        Me.DescripcionComboBox.Visible = False
        '--


    End Sub


    Private Sub BotonModificar()
        PanelCapDesNet.Enabled = True
        Me.MensDescuentoNetTextBox.Enabled = True
        Me.ContDescuentoNetTextBox.Enabled = True
        Me.PuntosTextBox.Enabled = True
        Me.ComboBox6.Enabled = False
        Me.ComboBox7.Enabled = False
        PanelCapDesNet.Enabled = True
        Me.Agregar1.Enabled = False
        Me.Modificar1.Enabled = True
        Me.Eliminar1.Enabled = False
        Me.Guardar1.Enabled = True
        Me.Cancelar1.Enabled = True
        Me.ConceptoComboBox.Visible = True
        Me.DescripcionComboBox.Visible = True
        Me.ContDescuentoNetTextBox.Visible = True
        Me.MensDescuentoNetTextBox.Visible = True
        Me.PuntosTextBox.Visible = True
    End Sub

    Private Sub BotonGuardar()
        If eResString = "N" Then
            If IsNumeric(Me.ComboBox8.SelectedValue) = False Then
                MsgBox("Selecciona el Tipo de Cliente.")
                Exit Sub
            End If
            If IsNumeric(Me.ComboBox6.SelectedValue) = False Then
                MsgBox("Selecciona el Tipo de Servicio.")
                Exit Sub
            End If
            If IsNumeric(Me.ComboBox7.SelectedValue) = False Then
                MsgBox("Selecciona el Servicio.")
                Exit Sub
            End If
        End If
        If IsNumeric(Me.ContDescuentoNetTextBox.Text) = False Then
            MsgBox("El Descuento de Contratación debe ser Numérico.", , "Atención")
            Exit Sub
        End If
        If IsNumeric(Me.MensDescuentoNetTextBox.Text) = False Then
            MsgBox("El Descuento de Mensualidad debe ser Numérico.", , "Atención")
            Exit Sub
        End If
        If IsNumeric(Me.PuntosTextBox.Text) = False Then
            MsgBox("Los Puntos debe ser Numérico.", , "Atención")
            Exit Sub

        End If
        Me.CONGeneralDescuentoNetDataGridView.Enabled = True
        Me.Agregar1.Enabled = True
        Me.Modificar1.Enabled = True
        Me.Eliminar1.Enabled = True
        Me.Guardar1.Enabled = False
        Me.Cancelar1.Enabled = True
        PanelCapDesNet.Enabled = False
        Guarda_GeneralDescuentoNetguarda()
        MuestraDesCuentoNet()
        Me.ComboBox6.Text = Nothing
        Me.ComboBox7.Text = Nothing
        Me.ConceptoComboBox.Visible = False
        Me.DescripcionComboBox.Visible = False
        '-
        Me.MensDescuentoNetTextBox.Enabled = False
        Me.ContDescuentoNetTextBox.Enabled = False
        Me.PuntosTextBox.Enabled = False
        Me.ContDescuentoNetTextBox.Visible = False
        Me.MensDescuentoNetTextBox.Visible = False
        Me.PuntosTextBox.Visible = False
    End Sub

    Private Sub BotonEliminar()
        Me.Agregar1.Enabled = True
        Me.Modificar1.Enabled = True
        Me.Eliminar1.Enabled = True

        Me.Guardar1.Enabled = False
        Me.Cancelar1.Enabled = True
        PanelCapDesNet.Enabled = False
        BorrarGenDesnet()
        MuestraDesCuentoNet()
        MsgBox(mensaje6)
        Me.ComboBox6.Text = Nothing
        Me.ComboBox7.Text = Nothing
        Me.ConceptoComboBox.Visible = False
        Me.DescripcionComboBox.Visible = False
        '-
        Me.ContDescuentoNetTextBox.Visible = False
        Me.MensDescuentoNetTextBox.Visible = False
        Me.PuntosTextBox.Visible = False
    End Sub

    Private Sub BotonCancelar()
        Me.CONGeneralDescuentoNetDataGridView.Enabled = True
        Me.Agregar1.Enabled = True
        Me.MensDescuentoNetTextBox.Enabled = False
        Me.ContDescuentoNetTextBox.Enabled = False
        Me.PuntosTextBox.Enabled = False
        Me.Modificar1.Enabled = True
        Me.Eliminar1.Enabled = True
        Me.Guardar1.Enabled = False
        Me.Cancelar1.Enabled = True
        Me.ComboBox6.Enabled = False
        Me.ComboBox6.Text = ""
        Me.ComboBox7.Enabled = False
        Me.ComboBox7.Text = ""
        Me.ConceptoComboBox.Visible = False
        Me.DescripcionComboBox.Visible = False
        '-
        Me.ConceptoComboBox.Visible = False
        Me.DescripcionComboBox.Visible = False
        Me.ContDescuentoNetTextBox.Visible = False
        Me.MensDescuentoNetTextBox.Visible = False
        Me.PuntosTextBox.Visible = False
        PanelCapDesNet.Enabled = False
        MuestraDesCuentoNet()
    End Sub

    Private Sub Guarda_GeneralDescuentoNetguarda()

        Try


            '    If Len(Trim(Me.ComboBox6.Text)) = 0 Then
            '        MsgBox("Seleccione el Tipo Servicio")
            '        Exit Sub
            '    End If
            '    If Len(Trim(Me.ComboBox7.Text)) = 0 Then
            '        MsgBox("Seleccione el Servicio")
            '        Exit Sub
            '    End If
            '    If IsNumeric(Me.ComboBox6.SelectedValue) = False Then
            '        MsgBox("Seleccione el Servicio")
            '        Exit Sub
            '    End If




            Dim CON5 As New SqlConnection(MiConexion)

            If eResString = "M" Then
                CON5.Open()
                Me.Guarda_GeneralDescuentoNetTableAdapter.Connection = CON5
                Me.Guarda_GeneralDescuentoNetTableAdapter.Fill(Me.DataSetEric2.Guarda_GeneralDescuentoNet, Me.Clv_ServicioTextBox.Text, Me.ContDescuentoNetTextBox.Text, New System.Nullable(Of Decimal)(CType(Me.MensDescuentoNetTextBox.Text, Decimal)), Me.Clv_TipSerTextBox1.Text, Me.Clv_Servicio_Con_AplicaTextBox.Text, Me.ComboBox8.SelectedValue, CType(Me.PuntosTextBox.Text, Decimal))
                CON5.Close()
                MsgBox(mensaje5)
            End If

            If eResString = "N" Then
                CON5.Open()
                Me.Valida_Guarda_GeneralDescuentoNetTableAdapter.Connection = CON5
                Me.Valida_Guarda_GeneralDescuentoNetTableAdapter.Fill(Me.DataSetEDGAR.Valida_Guarda_GeneralDescuentoNet, Me.Clv_ServicioTextBox.Text, Me.ComboBox6.SelectedValue, Me.ComboBox7.SelectedValue, Me.ComboBox8.SelectedValue, eRes)
                CON5.Close()
                If eRes = 1 Then
                    MsgBox("No puedes Agregar el Descuento debido a que Ya se encuentra Registrado. Selecciona la opción Modificar.")
                Else
                    CON5.Open()
                    Me.Guarda_GeneralDescuentoNetTableAdapter.Connection = CON5
                    Me.Guarda_GeneralDescuentoNetTableAdapter.Fill(Me.DataSetEric2.Guarda_GeneralDescuentoNet, Me.Clv_ServicioTextBox.Text, Me.ContDescuentoNetTextBox.Text, New System.Nullable(Of Decimal)(CType(Me.MensDescuentoNetTextBox.Text, Decimal)), Me.ComboBox6.SelectedValue, Me.ComboBox7.SelectedValue, Me.ComboBox8.SelectedValue, CType(Me.PuntosTextBox.Text, Decimal))
                    If Me.CheckBox3.Checked = True Then
                        Me.Guarda_GeneralDescuentoNetAllTableAdapter.Connection = CON5
                        Me.Guarda_GeneralDescuentoNetAllTableAdapter.Fill(Me.DataSetEric2.Guarda_GeneralDescuentoNetAll, Me.Clv_ServicioTextBox.Text, Me.ContDescuentoNetTextBox.Text, New System.Nullable(Of Decimal)(CType(Me.MensDescuentoNetTextBox.Text, Decimal)), Me.ComboBox6.SelectedValue, Me.ComboBox7.SelectedValue, Me.ComboBox8.SelectedValue, CType(Me.PuntosTextBox.Text, Decimal))
                    End If
                    CON5.Close()
                    MsgBox(mensaje5)
                End If
            End If


        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub Agregar1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Agregar1.Click
        eResString = "N"
        BotonNuevo()
        Me.MensDescuentoNetTextBox.Text = 0
        Me.ContDescuentoNetTextBox.Text = 0
        Me.PuntosTextBox.Text = 0
    End Sub

    Private Sub Modificar1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Modificar1.Click
        If Me.CONGeneralDescuentoNetDataGridView.RowCount > 0 Then
            eResString = "M"
            Me.CONGeneralDescuentoNetDataGridView.Enabled = False
            Me.BotonModificar()
        Else
            MsgBox("No existen Descuentos registrados para Modificar", , "Atención")
        End If
    End Sub

    Private Sub Guardar1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Guardar1.Click
        Me.BotonGuardar()
    End Sub

    Private Sub Eliminar1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Eliminar1.Click
        If Me.CONGeneralDescuentoNetDataGridView.RowCount > 0 Then
            Me.BotonEliminar()
        Else
            MsgBox("No existen Descuentos registrados para Eliminar.", , "Atención")
        End If
    End Sub

    Private Sub Cancelar1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancelar1.Click
        Me.BotonCancelar()
    End Sub


    Private Sub BorrarGenDesnet()
        Try
            Dim CON5 As New SqlConnection(MiConexion)
            CON5.Open()
            Me.BORGeneralDescuentoNetTableAdapter.Connection = CON5
            Me.BORGeneralDescuentoNetTableAdapter.Fill(Me.DataSetEDGAR.BORGeneralDescuentoNet, Me.Clv_TipSerTextBox1.Text, Me.Clv_ServicioTextBox.Text, Me.Clv_Servicio_Con_AplicaTextBox.Text, Me.ComboBox8.SelectedValue)
            CON5.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub Panel16_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel16.Paint

    End Sub

    Private Sub MensDescuentoNetTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.KeyChar = Chr((ValidaKey(Me.MensDescuentoNetTextBox, Asc(LCase(e.KeyChar)), "L")))
    End Sub

    Private Sub MensDescuentoNetTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ContDescuentoNetTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.KeyChar = Chr((ValidaKey(Me.ContDescuentoNetTextBox, Asc(LCase(e.KeyChar)), "L")))
    End Sub


    Private Sub ComboBox8_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox8.SelectedIndexChanged
        Me.MuestraDesCuentoNet()
    End Sub

    Private Sub Sale_en_CarteraCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Sale_en_CarteraCheckBox.CheckedChanged

    End Sub

    Private Sub NueAplicaSoloInternet()
        Try
            Dim CON5 As New SqlConnection(MiConexion)
            CON5.Open()
            Me.NueAplicaSoloInternetTableAdapter.Connection = CON5
            Me.NueAplicaSoloInternetTableAdapter.Fill(Me.DataSetEric.NueAplicaSoloInternet, Me.Clv_ServicioTextBox.Text)
            CON5.Close()
        Catch
            MsgBox("Se ha producido un Error.", , "Atención")
        End Try
    End Sub

    Private Sub BorAplicaSoloInternet()
        Try
            Dim CON5 As New SqlConnection(MiConexion)
            CON5.Open()
            Me.BorAplicaSoloInternetTableAdapter.Connection = CON5
            Me.BorAplicaSoloInternetTableAdapter.Fill(Me.DataSetEric.BorAplicaSoloInternet, Me.Clv_ServicioTextBox.Text)
            CON5.Close()
        Catch
            MsgBox("Se ha producido un Error.", , "Atención")
        End Try
    End Sub

    Private Sub ValidaAplicaSoloInternet()
        Dim CON6 As New SqlConnection(MiConexion)
        CON6.Open()
        Me.ValidaAplicaSoloInternetTableAdapter.Connection = CON6
        Me.ValidaAplicaSoloInternetTableAdapter.Fill(Me.DataSetEric.ValidaAplicaSoloInternet, Me.Clv_ServicioTextBox.Text, eRespuesta)
        CON6.Close()
        If eRespuesta = 1 Then
            Me.CheckBox1.Checked = True
        Else
            Me.CheckBox1.Checked = False
        End If
    End Sub


    Private Sub ComboBox9_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox9.SelectedIndexChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Dame_ContratacionParcialTableAdapter.Connection = CON
        Me.Dame_ContratacionParcialTableAdapter.Fill(Me.DataSetLidia.Dame_ContratacionParcial, CInt(Me.ComboBox9.SelectedValue))
        CON.Close()
    End Sub



    Private Sub TabControl1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabControl1.SelectedIndexChanged
        Me.ComboBox6.Text = ""
    End Sub

    Private Sub GuardaIepsServicios(ByVal APLICA As Boolean, ByVal CLAVE_IEPS As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim command As New SqlCommand("GuardaIepsServicios", conexion)
        command.CommandType = CommandType.StoredProcedure

        Dim parametro1 As New SqlParameter("@Clv_Servicio", SqlDbType.BigInt)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = CInt(Me.Clv_ServicioTextBox.Text)
        command.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@Aplica_Ieps", SqlDbType.Bit)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = APLICA
        command.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@CLAVE", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = CLAVE_IEPS
        command.Parameters.Add(parametro3)

        Try
            conexion.Open()
            command.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Function ConsultaIepsServicios(ByVal CLAVE_IEPS As Integer) As Boolean
        Dim conexion As New SqlConnection(MiConexion)
        Dim command As New SqlCommand("ConsultaIepsServicios", conexion)
        command.CommandType = CommandType.StoredProcedure

        Dim parametro1 As New SqlParameter("@Clv_Servicio", SqlDbType.BigInt)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = CInt(Me.Clv_ServicioTextBox.Text)
        command.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@APLICA_IEPS", SqlDbType.Bit)
        parametro2.Direction = ParameterDirection.Output
        command.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@CLAVE", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = CLAVE_IEPS
        command.Parameters.Add(parametro3)

        Try
            conexion.Open()
            command.ExecuteNonQuery()
            Return parametro2.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Function

    Private Sub ConRentaAparato(ByVal Clv_TipoCliente As Integer, ByVal Clv_Servicio As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ConRentaAparato", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0
        Dim reader As SqlDataReader

        Dim par1 As New SqlParameter("@Clv_TipoCliente", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = Clv_TipoCliente
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        par2.Direction = ParameterDirection.Input
        par2.Value = Clv_Servicio
        comando.Parameters.Add(par2)

        Try
            conexion.Open()
            reader = comando.ExecuteReader
            While (reader.Read())
                tbPrecio.Text = reader(2).ToString()
                tbPrecioAdic.Text = reader(3).ToString()
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub ModRentaAparato(ByVal Clv_TipoCliente As Integer, ByVal Clv_Servicio As Integer, ByVal Precio As Decimal, ByVal PrecioAdic As Decimal)

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ModRentaAparato", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Clv_TipoCliente", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = Clv_TipoCliente
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        par2.Direction = ParameterDirection.Input
        par2.Value = Clv_Servicio
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Precio", SqlDbType.Decimal)
        par3.Direction = ParameterDirection.Input
        par3.Value = Precio
        comando.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@PrecioAdic", SqlDbType.Decimal)
        par4.Direction = ParameterDirection.Input
        par4.Value = PrecioAdic
        comando.Parameters.Add(par4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub GuardaServicioHD(ByVal ClaveServicio As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("GuardaServicioHD", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@CLV_SERVICIO", ClaveServicio)
        CMD.Parameters.AddWithValue("@HD", Me.HDCheckBox.CheckState)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub ConsultaServicioHD(ByVal ClaveServicio As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("ConsultaServicioHD", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@CLV_SERVICIO", SqlDbType.Int)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = ClaveServicio
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@HD", SqlDbType.Bit)
        PRM2.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM2)

        Dim PRM3 As New SqlParameter("@BNDPRINCIPAL", SqlDbType.Int)
        PRM3.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM3)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()

            If PRM3.Value = 0 Then
                If PRM2.Value = True Then
                    Me.HDCheckBox.CheckState = CheckState.Checked
                Else
                    Me.HDCheckBox.CheckState = CheckState.Unchecked
                End If
            Else
                Me.HDCheckBox.Visible = False
            End If
            If PRM2.Value = True Then
                Me.HDCheckBox.CheckState = CheckState.Checked
            Else
                Me.HDCheckBox.CheckState = CheckState.Unchecked
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub


    Private Sub GuardaCuenta(ByVal prmCuenta As String, ByVal prmPosicion As Integer, ByVal prmClavetxt As String, ByVal prmClvTipServ As Integer)
        BaseII.limpiaParametros()

        'Se Insertan los parametros de cuenta posicion, clave del servicio y tipo del servicio.
        BaseII.CreateMyParameter("@cuenta", SqlDbType.VarChar, prmCuenta, 100)
        BaseII.CreateMyParameter("@posicion", SqlDbType.Int, prmPosicion)
        BaseII.CreateMyParameter("@clv_txt", SqlDbType.VarChar, prmClavetxt, 20)
        BaseII.CreateMyParameter("@clv_tipserv", SqlDbType.Int, prmClvTipServ)

        'Guardamos el registro de la cuenta contable.
        BaseII.Inserta("Guarda_Servicios_Poliza")
    End Sub

    Private Sub MuestraCuenta(ByVal prmClavetxt As String, ByVal prmCuenta As String, ByVal prmPosicion As Integer)
        BaseII.limpiaParametros()

        'Se Insertan los parametros de cuenta posicion, clave del servicio y tipo del servicio.
        BaseII.CreateMyParameter("@clv_text", SqlDbType.VarChar, prmClavetxt, 20)
        BaseII.CreateMyParameter("@cuenta", SqlDbType.VarChar, 100, ParameterDirection.Output, prmCuenta)
        BaseII.CreateMyParameter("@posicion",ParameterDirection.Output, SqlDbType.Int )

        Dim diccionario As Dictionary(Of String, Object) = BaseII.ProcedimientoOutPut("Muestra_Cuenta")

        Me.txtCuentaContable.Text = diccionario("@cuenta").ToString
        Me.txtPosicion.Text = diccionario("@posicion").ToString
    End Sub

    Private Sub txtCuentaContable_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCuentaContable.TextChanged

    End Sub
End Class