Imports System.Data.SqlClient
Imports System.Text
Imports sofTV.BAL

Public Class FrmAtenTel
    Dim clave_colonia As Integer
    Dim Clv_TipSer As Integer
    Dim banderaGuarda As String
    Dim clv_usuario As Integer
    Dim Clv_Colonia As Integer
    Dim Clv_Motivo As Integer

    Private Sub FrmAtenTel_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GLOCONTRATOSEL > 0 Then
            Me.ContratoTextBox.Text = GLOCONTRATOSEL
            Me.TextBox3.Text = CLng(Me.ContratoTextBox.Text)
            GLOCONTRATOSEL = 0
            GloClv_TipSer = 0

            If Me.RadioButton1.Checked = True Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.MuestraTipServEricTableAdapter.Connection = CON
                Me.MuestraTipServEricTableAdapter.Fill(Me.DataSetEric.MuestraTipServEric, CLng(Me.ContratoTextBox.Text), 10)
                CON.Close()
            End If

            BuscaCte()
        End If
    End Sub
    Private Sub LlenaComboMotAtenTel()
        DescripcionComboBox.DataSource = MotAtenTel.GetAll()
    End Sub

    'Private WithEvents Documento As New Printing.PrintDocument
    Private Sub FrmAtenTel_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Me.GroupBox1.BackColor = Color.Gray
        Me.GroupBox2.BackColor = Color.Gray
        Me.GroupBox3.BackColor = Color.Gray
        Me.GroupBox4.BackColor = Color.Gray
        LlenaComboMotAtenTel()
        If eOpcion = "N" Then
            Me.BindingNavigatorAddNewItem.Enabled = False
            Nuevo()
        End If

        If eOpcion = "C" Then
            Consultar()
        End If
    End Sub



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        TextBox4.Text = ""
        ObservacionTextBox.Text = ""
        GLOCONTRATOSEL = 0
        GloClv_TipSer = 0

        FrmSelCliente.Show()
        'BuscaCte()
    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        If Me.RadioButton1.Checked = True Then
            Me.TextBox3.Enabled = True
            Me.Button1.Enabled = True
            Me.TextBox4.ReadOnly = True
            Me.TextBox5.ReadOnly = True
            Me.NombreComboBox.Enabled = False
            Me.GroupBox2.Text = "Datos del Cliente"
        End If
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        TextBox4.Text = ""
        ObservacionTextBox.Text = ""
        Dim CON As New SqlConnection(MiConexion)
        If Me.RadioButton2.Checked = True Then
            Me.TextBox3.Enabled = False
            Me.Button1.Enabled = False
            Me.TextBox4.ReadOnly = False
            Me.TextBox5.ReadOnly = False
            Me.NombreComboBox.Enabled = True
            Me.TextBox3.Clear()
            Me.TextBox4.Clear()
            Me.TextBox5.Clear()
            CON.Open()
            Me.MuestraTipServEricTableAdapter.Connection = CON
            Me.MuestraTipServEricTableAdapter.Fill(Me.DataSetEric.MuestraTipServEric, 0, 0)
            CON.Close()
            MuestraColonias(0, 0, 0)
            Me.GroupBox2.Text = "Datos de la Persona"
        End If
    End Sub
    Private Sub BuscaCte()
        Dim CON As New SqlConnection(MiConexion)
        Dim eRes As Integer = 0
        Dim eMsg As String = Nothing
        Dim eRes2 As Integer = 0
        Dim eMsg2 As String = Nothing
        If IsNumeric(Me.TextBox3.Text) = True Then
            CON.Open()
            Me.ConAtenTelCteTableAdapter.Connection = CON
            Me.ConAtenTelCteTableAdapter.Fill(Me.DataSetEric.ConAtenTelCte, CLng(Me.TextBox3.Text), eRes, eMsg)
            CON.Close()
            If eRes = 1 Then
                Me.TextBox3.Clear()
                Me.TextBox4.Clear()
                Me.TextBox5.Clear()
                CON.Open()
                'Me.MuestraColoniaSecTableAdapter.Connection = CON
                'Me.MuestraColoniaSecTableAdapter.Fill(Me.DataSetEric.MuestraColoniaSec, 0, 0, 0)
                MuestraColonias(0, 0, 0)
                CON.Close()

                MsgBox(eMsg)
            Else

                If eOpcion = "N" Then
                    CON.Open()
                    Me.BuscaSiTieneQuejaTableAdapter.Connection = CON
                    Me.BuscaSiTieneQuejaTableAdapter.Fill(Me.DataSetEric2.BuscaSiTieneQueja, 0, CLng(Me.TextBox3.Text), eRes2, eMsg2)
                    CON.Close()
                    If eRes2 = 1 Then
                        Me.ObservacionTextBox.Text = eMsg2
                        Me.ObservacionTextBox.ForeColor = Color.Red
                    Else
                        Me.ObservacionTextBox.Text = ""
                        Me.ObservacionTextBox.ForeColor = Color.Black
                    End If
                End If

                Me.TextBox4.Text = Me.NombreTextBox2.Text
                Me.TextBox5.Text = Me.TelefonoTextBox1.Text
                CON.Open()
                If IsNumeric(Me.Clv_ColoniaTextBox1.Text) Then
                    clave_colonia = CLng(Me.Clv_ColoniaTextBox1.Text)
                Else
                    clave_colonia = 0
                End If

                MuestraColonias(clave_colonia, 0, 3)

                CON.Close()
            End If
        End If
    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If Me.RadioButton1.Checked = True Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.MuestraTipServEricTableAdapter.Connection = CON
                Me.MuestraTipServEricTableAdapter.Fill(Me.DataSetEric.MuestraTipServEric, CLng(Me.TextBox3.Text), 10)
                CON.Close()
            End If
            BuscaCte()
        End If
    End Sub


    Private Sub ConAtenTelBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConAtenTelBindingNavigatorSaveItem.Click
        ' Dim CON As New SqlConnection(MiConexion)
        Try
            If IsNumeric(Me.TextBox3.Text) = False Then
                Me.TextBox3.Text = "0"
            End If

            If Me.TextBox4.Text.Length = 0 Then
                MsgBox("Captura el Nombre de la Persona.")
                Exit Sub
            End If

            If Me.TextBox5.Text.Length = 0 Then
                Me.TextBox5.Text = "Tel�fono No Registrado."
            End If


            sofTV.BAL.AtenTel.Add(Nothing, CLng(TextBox3.Text), CLng(ConceptoComboBox.SelectedValue), CInt(NombreComboBox.SelectedValue), TextBox5.Text, CInt(DescripcionComboBox.SelectedValue), Me.HoraDateTimePicker1.Value, Me.FechaDateTimePicker1.Value, Me.ObservacionTextBox.Text, CInt(eClv_Usuario), Me.TextBox4.Text)

            MsgBox(mensaje5)
            Me.BindingNavigatorAddNewItem.Enabled = True
            Me.ConAtenTelBindingNavigatorSaveItem.Enabled = False
            Me.Close()
        Catch ex As Exception
            MsgBox("Error")

        End Try
    End Sub
    Private Sub Nuevo()
        Dim CON As New SqlConnection(MiConexion)


        CON.Open()
        Me.DameFechaHoraTableAdapter.Connection = CON
        Me.DameFechaHoraTableAdapter.Fill(Me.DataSetEric.DameFechaHora)
        Me.CONUSUARIOSTableAdapter.Connection = CON
        Me.CONUSUARIOSTableAdapter.Fill(Me.DataSetEric.CONUSUARIOS, eClv_Usuario)
        Me.MuestraTipServEricTableAdapter.Connection = CON
        Me.MuestraTipServEricTableAdapter.Fill(Me.DataSetEric.MuestraTipServEric, 0, 0)
        'Me.MuestraColoniaSecTableAdapter.Connection = CON
        'Me.MuestraColoniaSecTableAdapter.Fill(Me.DataSetEric.MuestraColoniaSec, 0, 0, 0)
        MuestraColonias(0, 0, 0)
        Me.ConMotAtenTelTableAdapter.Connection = CON
        Me.ConMotAtenTelTableAdapter.Fill(Me.DataSetEric.ConMotAtenTel, 0, "", 0)

        Me.TextBox1.Text = CStr(Me.FechaDateTimePicker1.Value.Date)
        Me.TextBox2.Text = CStr(Me.HoraDateTimePicker1.Text)

        Me.TextBox3.Clear()
        Me.TextBox4.Clear()
        Me.TextBox5.Clear()

        CON.Close()

        Me.RadioButton2.Checked = True

    End Sub
    Private Sub MuestraColonias(ByVal opcion0 As Long, ByVal Opcion1 As Long, ByVal opcion As Long)
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder

        StrSQL.Append("EXEC MuestraColoniaSec ")
        StrSQL.Append(CStr(opcion0) & ", ")
        StrSQL.Append(CStr(Opcion1) & ", ")
        StrSQL.Append(CStr(opcion))


        Dim DA As New SqlDataAdapter(StrSQL.ToString, CON)
        Dim DT As New DataTable
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            NombreComboBox.DataSource = BS.DataSource



        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try

    End Sub

    Private Sub BindingNavigatorAddNewItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorAddNewItem.Click
        Nuevo()
        Me.BindingNavigatorAddNewItem.Enabled = False
        Me.ConAtenTelBindingNavigatorSaveItem.Enabled = True
    End Sub

    Private Sub Consultar()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim frm As AtenTel = New AtenTel
        frm = AtenTel.GetOne(eClave)

        clv_usuario = frm.Clv_Usuario
        llenaTextBoxUsuario(clv_usuario)
        Me.TextBox3.Text = frm.Contrato
        Me.TextBox4.Text = frm.Nombre
        Clv_Colonia = frm.Clv_Colonia
        LlenaComboColonia(Clv_Colonia)
        Me.TextBox5.Text = frm.Telefono
        Me.TextBox1.Text = frm.Fecha
        Me.TextBox2.Text = FormatDateTime(frm.Hora, DateFormat.ShortTime)
        Me.ObservacionTextBox.Text = frm.Observacion
        Clv_TipSer = frm.Clv_TipSer
        LlenaComboTipServicio(Clv_TipSer)

        Clv_Motivo = frm.Clv_Motivo
        LlenaComBoTipoMotivo(Clv_Motivo)
        Me.DescripcionComboBox.SelectedValue = frm.Clv_Motivo

        CON.Close()

        Me.GroupBox1.Enabled = False
        Me.GroupBox2.Enabled = False
        Me.GroupBox4.Enabled = False
        Me.GroupBox3.Enabled = False
        Me.ConAtenTelBindingNavigator.Enabled = False

    End Sub
    Private Sub llenaTextBoxUsuario(ByVal clv_usuario As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("MUESTRAUSUARIOS_LlamadasTelefonica", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@Clave_Usuario", clv_usuario)
        Dim Reader As SqlDataReader

        Try
            CON.Open()
            Reader = CMD.ExecuteReader
            While (Reader.Read)
                Me.NombreTextBox1.Text = Reader(0).ToString
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub
    Private Sub LlenaComBoTipoMotivo(ByVal Clv_Motivo As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("MUESTRAMotivo_AtencionTel", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@Clv_Motivo", Clv_Motivo)
        Dim Reader As SqlDataReader

        Try
            CON.Open()
            Reader = CMD.ExecuteReader
            While (Reader.Read)
                Me.DescripcionComboBox.Text = Reader(0).ToString
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub
    Private Sub LlenaComboTipServicio(ByVal Clv_TipSer As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("MuestraTipSer_AtencionDeLlamadas", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@Clv_TipSer", Clv_TipSer)
        Dim Reader As SqlDataReader

        Try
            CON.Open()
            Reader = CMD.ExecuteReader
            While (Reader.Read)
                Me.ConceptoComboBox.Text = Reader(0).ToString
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub
    Private Sub LlenaComboColonia(ByVal Clv_Colonia As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("MUECOLONIAS_LlamadasTelefonica", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@Clv_Colonia", Clv_Colonia)

        Dim Reader As SqlDataReader

        Try
            CON.Open()
            Reader = CMD.ExecuteReader
            While (Reader.Read)
                Me.NombreComboBox.Text = Reader(0).ToString
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub ConceptoComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConceptoComboBox.SelectedIndexChanged
        If Me.ConceptoComboBox.SelectedValue = 1 Then
            Me.RichTextBox1.LoadFile(RutaReportes + "\Basico.rtf")
        End If

        If Me.ConceptoComboBox.SelectedValue = 2 Then
            Me.RichTextBox1.LoadFile(RutaReportes + "\Internet.rtf")
        End If

        If Me.ConceptoComboBox.SelectedValue = 5 Then
            Me.RichTextBox1.LoadFile(RutaReportes + "\Digital.rtf")
        End If
    End Sub


    Private Sub TextBox3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox3.TextChanged

    End Sub
End Class