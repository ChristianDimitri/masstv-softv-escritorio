Imports System.Data.SqlClient
Imports sofTV.Base

Public Class BrwVisitas

    Public consulta As New CBase
    Public clv_visita As Integer, descripcion As String, opcion As Integer

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
    Private Sub Busqueda(ByVal op As Integer)

        Select Case op
            Case 0
                clv_visita = 0 : descripcion = "" : opcion = 0
            Case 1
                If IsNumeric(Me.TextBox1.Text) Then
                    clv_visita = CLng(Me.TextBox1.Text)
                Else
                    MsgBox("Capture un dato entero", MsgBoxStyle.Information)
                End If

                descripcion = "" : opcion = op
            Case 2
                clv_visita = 0 : descripcion = Me.TextBox2.Text : opcion = op
        End Select

        Dim parametro(3) As SqlParameter
        parametro(0) = New SqlParameter("@clv_visita", SqlDbType.Int)
        parametro(0).Value = clv_visita
        parametro(1) = New SqlParameter("@descripcion", SqlDbType.VarChar, 250)
        parametro(1).Value = descripcion
        parametro(2) = New SqlParameter("@op", SqlDbType.Int)
        parametro(2).Value = opcion

        Dim ds As DataTable = consultas.consultarDS("Busqueda_CatalogoVisitas", parametro).Tables(0)
        DataGridView1.DataSource = ds

        Me.TextBox1.Clear()
        Me.TextBox2.Clear()
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox1, Asc(LCase(e.KeyChar)), "N")))
        If Asc(e.KeyChar) = 13 Then
            Busqueda(1)
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox2, Asc(LCase(e.KeyChar)), "S")))
        If Asc(e.KeyChar) = 13 Then
            Busqueda(2)
        End If
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Busqueda(1)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Busqueda(2)
    End Sub

    Private Sub BrwVisitas_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'If LocBndVisitas = True Then
        '    LocBndVisitas = False
        Busqueda(0)
        'End If
    End Sub

    Private Sub BrwVisitas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Busqueda(0)

        If DataGridView1.RowCount > 0 Then
            Clv_calleLabel2.Text = DataGridView1.SelectedRows(0).Cells(0).Value.ToString()
            LblDescricpion.Text = DataGridView1.SelectedRows(0).Cells(1).Value.ToString()
            LblVendedor.Text = DataGridView1.SelectedRows(0).Cells(3).Value.ToString()
            LblFecha.Text = DataGridView1.SelectedRows(0).Cells(2).Value.ToString()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        LocOpVisita = "N"
        FrmVisitas.Show()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        LocOpVisita = "C"
        If DataGridView1.Rows.Count > 0 Then
            Locclv_visita = Convert.ToInt32(DataGridView1.SelectedRows(0).Cells(0).Value.ToString())
            FrmVisitas.Show()
        Else
            MsgBox("No existen registros a consultar", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        LocOpVisita = "M"
        If DataGridView1.Rows.Count > 0 Then
            Locclv_visita = Convert.ToInt32(DataGridView1.SelectedRows(0).Cells(0).Value.ToString())
            FrmVisitas.Show()
        Else
            MsgBox("No existen registros a modificar", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub DataGridView1_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick

        If e.RowIndex >= 0 Then
            Dim filas As Integer = DataGridView1.Rows.GetRowCount(DataGridViewElementStates.Selected) 'Obtengo cuantas filas se han seleccionado

            If filas > 0 Then
                Clv_calleLabel2.Text = DataGridView1.Rows(e.RowIndex).Cells(0).Value.ToString()
                LblDescricpion.Text = DataGridView1.Rows(e.RowIndex).Cells(1).Value.ToString()
                LblVendedor.Text = DataGridView1.Rows(e.RowIndex).Cells(3).Value.ToString()
                LblFecha.Text = DataGridView1.Rows(e.RowIndex).Cells(2).Value.ToString()
            End If
        End If
    End Sub

End Class