Imports System.Data.SqlClient
Public Class FrmDetOrSer
    Dim op_cabl As Integer = 0
    Dim errorDuplicado As Integer = 0
    Private Iante As Integer = 0

    Private Sub FrmDetOrSer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        HayConex(0)
        'Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
        'Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)
        Me.Dime_Que_servicio_Tiene_clienteTableAdapter.Connection = CON
        Me.Dime_Que_servicio_Tiene_clienteTableAdapter.Fill(Me.Procedimientosarnoldo4.Dime_Que_servicio_Tiene_cliente, GloContratoord)

        Me.MUESTRATRABAJOSTableAdapter.Connection = CON
        Me.MUESTRATRABAJOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATRABAJOS, CInt(Me.ComboBox4.SelectedValue))
        Me.ComboBox2.Text = ""
        CON.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim clavedetordSer As Long = 0
        Dim CON As New SqlConnection(MiConexion)
        eClv_TipSer = Me.ComboBox4.SelectedValue
        CON.Open()
        If IsNumeric(Me.ComboBox2.SelectedValue) = True And Len(Trim(Me.ComboBox2.Text)) > 0 Then
            validaDetOrdserDuplicado(gloClave, Me.ComboBox2.SelectedValue, 2)
            If errorDuplicado = 0 Then
                MsgBox("No Puedes Mezclar �rdenes De Proceso Autom�tico Y Proceso Manual", MsgBoxStyle.Information)
                Exit Sub
            End If
            If Mid(Trim(Me.ComboBox2.Text), 1, 5) = "BCABM" Then
                MsgBox("La baja de Cablemodem se genera de forma automatica en el momento que todos los Servicios pase a Baja ", MsgBoxStyle.Information)
                Exit Sub
            End If
            If Mid(Trim(Me.ComboBox2.Text), 1, 5) = "BAPAR" Then
                MsgBox("La baja de aparato digital se genera de forma automatica en el momento que todos los servicios se pase a baja ", MsgBoxStyle.Information)
                Exit Sub
            End If
            If Mid(Trim(Me.ComboBox2.Text), 1, 5) = "CANEX" Then
                HayConex(0)
                If Me.ContadorTextBox.Text > 0 Then
                    MsgBox("Existe una Orden de Contrataci�n de Extenci�n por lo cual se puede agregar este concepto ", MsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            If Mid(Trim(Me.ComboBox2.Text), 1, 5) = "CONEX" Then
                HayConex(1)
                If Me.ContadorTextBox.Text > 0 Then
                    MsgBox("Existe una Orden de Cancelaci�n de Extensi�n por lo cual se puede agregar este concepto ", MsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            If Mid(Trim(Me.ComboBox2.Text), 1, 5) = "IANTE" Then
                Iante = 1
                BuscaIANTE()
                If Iante = 0 Then
                    MsgBox("No existe Antena por asignar ", MsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            GloClv_Trabajo_OrdSer = Me.ComboBox2.SelectedValue
            GloTrabajo_OrdSer = Me.ComboBox2.Text
            If Len(Trim(Me.ObsTextBox1.Text)) > 0 Then
                GloObs_OrdSer = Me.ObsTextBox1.Text
            Else
                GloObs_OrdSer = ""
            End If
            GloSeRealiza_OrdSer = Me.SeRealizaCheckBox.Checked
            GloBndTrabajo = True
            GLOTRABAJO = Mid(Trim(GloTrabajo_OrdSer), 1, 5)
            Me.CONDetOrdSerTableAdapter.Connection = CON
            Me.CONDetOrdSerTableAdapter.Insert(gloClv_Orden, GloClv_Trabajo_OrdSer, GloObs_OrdSer, GloSeRealiza_OrdSer, GloDetClave)
            If Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "CAMDO" Or Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "CANET" Or Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "CADIG" Or Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "CADIG6" Then
                FrmCAMDO.ShowDialog()
            ElseIf Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "CONEX" Then
                FrmCONEX.ShowDialog()
            ElseIf Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "CEXTE" Then
                FrmCEXTE.ShowDialog()
            ElseIf Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "CANEX" Then
                FrmCANEX.ShowDialog()
            ElseIf Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "ICABM" Then
                FrmRelCablemodemClientes.ShowDialog()
            ElseIf Mid(Trim(Me.ComboBox2.Text), 1, 5) = "BCABM" Then
                FrmRelCablemodemClientes.ShowDialog()
            ElseIf Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "CCABM" Then
                'FrmRelCablemodemClientes.ShowDialog()
                'Eric----------------
                'FrmICABMAsigna.ShowDialog()
                op_cabl = MsgBox("�El Cablemodem Que Se Va Instalar es Inal�mbrico?", MsgBoxStyle.YesNo)
                If op_cabl = 6 Then
                    LoctipoCablemdm = 2
                    bndCCABM = True
                ElseIf op_cabl = 7 Then
                    LoctipoCablemdm = 1
                    bndCCABM = True
                End If
            ElseIf Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "CAPAR" Or Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "RAPAR" Then
                Bnd_CAPAR = True
                'FrmRelCablemodemClientes.ShowDialog()
                'Eric----------------
                'FrmIAPARAsigna.ShowDialog()
            ElseIf (Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "IPAQU" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "BPAQU" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "DPAQU" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "RPAQU") Then
                FrmrRelPaquetesdelCliente.ShowDialog()
            ElseIf (Mid(GloTrabajo_OrdSer, 1, 6) = "IPAQUT" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "BPAQT" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "DPAQT" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "RPAQT") Then
                FrmrRelPaquetesdelCliente.ShowDialog()
            ElseIf (Mid(GloTrabajo_OrdSer, 1, 5) = "BPAAD" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "BSEDI") Then
                FrmrRelPaquetesdelCliente.ShowDialog()
            ElseIf (Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "IPAQD" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "BPAQD" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "DPAQD" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "RPAQD") Then
                FrmrRelPaquetesdelClienteDigital.ShowDialog()
            ElseIf Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "IAPAR" Then
                FrmRelCablemodemClientesDigital.ShowDialog()
            ElseIf Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "IANRA" Or Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "IROUT" Then
                'FrmRelCablemodemClientes.Show()
                Dim FrM1 As New FormIAPARATOS_SELECCION
                FrM1.ShowDialog()
            End If
            'validaDetOrdserDuplicado(gloClave, Me.ComboBox2.SelectedValue, 1)
            'If errorDuplicado = 1 Then
            '    MsgBox("El Trabajo Ya Existe En La Lista", MsgBoxStyle.Information)
            '    Exit Sub
            'End If

            If GLOTRABAJO = "RETLI" Then
                Me.GuardaMotivoCanServTableAdapter.Connection = CON
                Me.GuardaMotivoCanServTableAdapter.Fill(Me.DataSetEric.GuardaMotivoCanServ, gloClv_Orden, 1, 0, 0, 0)
            End If
            Me.Close()
        Else
            MsgBox("No a Seleccionado un Servicio al Cliente")
        End If
        CON.Close()

    End Sub
    Private Sub BuscaIANTE()
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("TieneIANTE", con)
        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = GloContratoord
        com.Parameters.Add(par1)

        Dim par3 As New SqlParameter("@EXISTE", SqlDbType.Bit)
        par3.Direction = ParameterDirection.Output
        com.Parameters.Add(par3)
        Try
            con.Open()
            com.ExecuteNonQuery()
            Iante = par3.Value

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()
        End Try
    End Sub

    Private Sub HayConex(ByVal op As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.Dimesihay_ConexTableAdapter.Connection = CON
            Me.Dimesihay_ConexTableAdapter.Fill(Me.NewSofTvDataSet.Dimesihay_Conex, New System.Nullable(Of Long)(CType(gloClv_Orden, Long)), op)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()

    End Sub



    Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.SelectedIndexChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
        'Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)

        Me.ComboBox2.Text = ""
        Me.MUESTRATRABAJOSTableAdapter.Connection = CON
        Me.MUESTRATRABAJOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATRABAJOS, CInt(Me.ComboBox4.SelectedValue))
        Me.ComboBox2.Text = ""
        CON.Close()
    End Sub
    Private Sub validaDetOrdserDuplicado(ByVal clv_orden As Long, ByVal clv_trabajo As Long, ByVal OP As Integer)
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("validaDetOrdserDuplicado", con)

        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@clv_orden", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = clv_orden
        com.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@clv_trabajo", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Input
        par2.Value = clv_trabajo
        com.Parameters.Add(par2)

        Dim par4 As New SqlParameter("@OP", SqlDbType.BigInt)
        par4.Direction = ParameterDirection.Input
        par4.Value = OP
        com.Parameters.Add(par4)

        Dim par3 As New SqlParameter("@error", SqlDbType.Int)
        par3.Direction = ParameterDirection.Output
        com.Parameters.Add(par3)


        Try
            con.Open()
            com.ExecuteNonQuery()

            errorDuplicado = par3.Value

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()
        End Try
    End Sub

    
End Class