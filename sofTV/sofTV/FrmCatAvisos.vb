Imports System.Data.SqlClient
Public Class FrmCatAvisos
    Private clv_txtaviso As String = Nothing
    Private nombre As String = Nothing
    Private descripcion As String = Nothing

    Private Sub damedatosbitacora()
        Try
            If LocOpAviso = "M" Then
                clv_txtaviso = Me.Clv_txt_avisoTextBox.Text
                nombre = Me.NombreTextBox.Text
                descripcion = Me.DescripcionTextBox.Text
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub guardabitacora(ByVal op As Integer)
        Try
            Select Case op
                Case 0
                    If LocOpAviso = "N" Then
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Agrego Un Aviso", "", "Se Agrego Un Aviso: " + Me.NombreTextBox.Text, LocClv_Ciudad)
                    ElseIf LocOpAviso = "M" Then
                        'clv_txtaviso = Me.Clv_txt_avisoTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.Clv_txt_avisoTextBox.Name + " , " + Me.Clv_txt_avisoTextBox.Text, clv_txtaviso, Me.Clv_txt_avisoTextBox.Text, LocClv_Ciudad)
                        'nombre = Me.NombreTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.NombreTextBox.Name + " , " + Me.Clv_txt_avisoTextBox.Text, nombre, Me.NombreTextBox.Text, LocClv_Ciudad)
                        'descripcion = Me.DescripcionTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.DescripcionTextBox.Name + "," + Me.Clv_txt_avisoTextBox.Text, descripcion, Me.DescripcionTextBox.Text, LocClv_Ciudad)
                    End If
                Case 1
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Elimino Un Aviso", "", "Se Elimino Un Aviso: " + Me.NombreTextBox.Text, LocClv_Ciudad)
            End Select
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub FrmCatAvisos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Select Case LocOpAviso
            Case "N"
                Me.Con_AvisoBindingSource.AddNew()
                Me.DescripcionTextBox.Clear()
                Me.Clv_txt_avisoTextBox.Clear()
            Case "M"
                Me.Con_AvisoTableAdapter.Connection = CON
                Me.Con_AvisoTableAdapter.Fill(Me.DataSetarnoldo.Con_Aviso, LocClaveaviso)
                damedatosbitacora()
            Case "C"
                Me.Con_AvisoTableAdapter.Connection = CON
                Me.Con_AvisoTableAdapter.Fill(Me.DataSetarnoldo.Con_Aviso, LocClaveaviso)
                Me.CMBPanel1.Enabled = False
                Me.DescripcionTextBox.ReadOnly = True
                Me.Con_AvisoBindingNavigatorSaveItem.Enabled = False
                Me.BindingNavigatorDeleteItem.Enabled = False
        End Select
        CON.Close()


    End Sub

    Private Sub Con_AvisoBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Con_AvisoBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim valida As Integer = 0
        If Me.Clv_txt_avisoTextBox.Text = "" Then
            MsgBox("Por favor Capture la clave de Aviso", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.DescripcionTextBox.Text = "" Then
            MsgBox("Por favor Capture La Descripcion del Aviso", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.NombreTextBox.Text = "" Then
            MsgBox("Por favor Capture el Nombre del Aviso", MsgBoxStyle.Information)
            Exit Sub
        End If
        If LocOpAviso = "N" Then
            Me.Valida_clv_avisoTableAdapter.Connection = CON
            Me.Valida_clv_avisoTableAdapter.Fill(Me.DataSetarnoldo.Valida_clv_aviso, Me.Clv_txt_avisoTextBox.Text, valida)
        End If
        If valida = 0 Then
            Me.Validate()
            Me.Con_AvisoBindingSource.EndEdit()
            Me.Con_AvisoTableAdapter.Connection = CON
            Me.Con_AvisoTableAdapter.Update(Me.DataSetarnoldo.Con_Aviso)
            MsgBox("Se guardo con exito", MsgBoxStyle.Information)
            guardabitacora(0)
            bndAvisos = True
            Me.Close()
        Else
            MsgBox("La clave del Aviso ya existe por favor capture otra nuevamente", MsgBoxStyle.Information)
            Me.Clv_txt_avisoTextBox.Clear()
        End If
        CON.Close()
    End Sub


    Private Sub Button2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub


    Private Sub Con_AvisoBindingNavigator_RefreshItems(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Con_AvisoBindingNavigator.RefreshItems

    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Con_AvisoTableAdapter.Connection = CON
        Me.Con_AvisoTableAdapter.Delete(LocClaveaviso)
        guardabitacora(1)
        bndAvisos = True
        CON.Close()
        Me.Close()
    End Sub

    Private Sub DescripcionTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DescripcionTextBox.TextChanged

    End Sub
End Class