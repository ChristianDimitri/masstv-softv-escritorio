﻿Imports System.IO
Imports System.Text
Imports System.Collections
Imports System.Collections.Generic
Imports System.Data.SqlClient
Public Class DAEquipoExtra

    Public Shared StatusOrdSer As String = ""
    
    'Procedimiento que nos indica si es que la Orden que se va a ejecutar contiene la entraga de Material Extra ( ECTRL y/o ICRCA )
    Public Shared Function ValidaSiTieneMaterialExtraLaOrden_Instalacion(ByVal Clv_Orden As Integer) As Boolean

        Dim conn As New SqlConnection(MiConexion)
        Dim MaterialExtra As Boolean = False
        Try

            conn.Open()

            Dim comando As New SqlClient.SqlCommand("ValidaSiTieneMaterialExtraLaOrden_Instalacion", conn)
            comando.CommandType = CommandType.StoredProcedure

            comando.Parameters.Add("@Clv_Orden", SqlDbType.BigInt).Value = Clv_Orden
            comando.Parameters.Add("@MaterialExtra", SqlDbType.Bit).Value = 0

            comando.Parameters("@MaterialExtra").Direction = ParameterDirection.Output

            comando.ExecuteNonQuery()
            MaterialExtra = comando.Parameters("@MaterialExtra").Value

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conn.Close()
            conn.Dispose()
        End Try

        Return MaterialExtra
    End Function

    'Procedimiento que nos indica si es que la Orden que se va a ejecutar contiene la entraga de Material Extra ( RCTRL y/o RCRCA )
    Public Shared Function ValidaSiTieneMaterialExtraLaOrden_Retiro(ByVal Clv_Orden As Integer) As Boolean

        Dim conn As New SqlConnection(MiConexion)
        Dim MaterialExtra As Boolean = False
        Try

            conn.Open()

            Dim comando As New SqlClient.SqlCommand("ValidaSiTieneMaterialExtraLaOrden_Retiro", conn)
            comando.CommandType = CommandType.StoredProcedure

            comando.Parameters.Add("@Clv_Orden", SqlDbType.BigInt).Value = Clv_Orden
            comando.Parameters.Add("@MaterialExtra", SqlDbType.Bit).Value = 0

            comando.Parameters("@MaterialExtra").Direction = ParameterDirection.Output

            comando.ExecuteNonQuery()
            MaterialExtra = comando.Parameters("@MaterialExtra").Value

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conn.Close()
            conn.Dispose()
        End Try

        Return MaterialExtra
    End Function

    'Carga el Detalle de la Orden al nuevo GridView
    Public Shared Function BUSCADetOrdSer(ByVal Clv_Orden As Integer) As DataTable

        Dim conn As New SqlConnection(MiConexion)
        Dim Adaptador As New SqlDataAdapter
        Dim dtResultado As New DataTable

        Try

            Dim comando As New SqlClient.SqlCommand("CargaDetOrdSer", conn)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 5

            Adaptador.SelectCommand = comando
            Adaptador.SelectCommand.Parameters.Add("@Clv_Orden", SqlDbType.BigInt)
            Adaptador.SelectCommand.Parameters("@Clv_Orden").Value = Clv_Orden

            Adaptador.SelectCommand.Parameters.Add("@StatusOrdSer", SqlDbType.VarChar, 1)
            Adaptador.SelectCommand.Parameters("@StatusOrdSer").Direction = ParameterDirection.Output
            Adaptador.SelectCommand.Parameters("@StatusOrdSer").Value = ""

            conn.Open()
            Adaptador.Fill(dtResultado)
            StatusOrdSer = Adaptador.SelectCommand.Parameters("@StatusOrdSer").Value
            comando.Dispose()

        Catch ex As Exception

            MsgBox(ex.Message, MsgBoxStyle.Exclamation)

        Finally

            conn.Close()
            conn.Dispose()

        End Try

        Return dtResultado
    End Function

    'Carga los Trabajos que tiene relación con el Retiro de Aparato
    Public Shared Function CargaRelTrabajosRetiroAparato() As List(Of TrabajosRetiroAparato)

        Dim diccionario As Dictionary(Of String, TrabajosRetiroAparato) = New Dictionary(Of String, TrabajosRetiroAparato)
        Dim listaDeTrabajos As New List(Of TrabajosRetiroAparato)

        Dim I As Integer = 0
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder

        Try

            Dim cmd As New SqlCommand("CargaRelTrabajosRetiroAparato", conexion)
            cmd.CommandType = CommandType.StoredProcedure

            Dim listaCargos As New List(Of TrabajosRetiroAparato)
            Dim Trabajo As TrabajosRetiroAparato

            Dim dr As SqlDataReader
            conexion.Open()
            dr = cmd.ExecuteReader()
            listaDeTrabajos.Clear()

            Using dr
                While dr.Read
                    If diccionario.ContainsKey(dr("ident").ToString().Trim()) Then
                        If Not listaDeTrabajos.Contains(diccionario(dr("ident").ToString().Trim())) Then
                            listaDeTrabajos.Add(diccionario(dr("ident").ToString().Trim()))
                        End If
                    Else

                        Trabajo = New TrabajosRetiroAparato()
                        Trabajo.ident = dr("ident").ToString().Trim()
                        Trabajo.Trabajo = dr("Trabajo").ToString().Trim()
                        Trabajo.Descripcion = dr("Descripcion").ToString().Trim()
                        Trabajo.AplicaRetiroAparato = dr("AplicaRetiroAparato").ToString().Trim()

                        diccionario.Add(Trabajo.ident, Trabajo)
                        listaDeTrabajos.Add(Trabajo)

                    End If
                End While

                dr.Close()

            End Using

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

        Return listaDeTrabajos

    End Function

    'Procedimiento que guarda la Relación del Detalle de la Orden con el Retiro de Aparatos
    Public Shared Function AgregaRelDetOrdSerRetiroAparato(ByRef strXML As String, ByVal Contrato As Integer, ByVal Status As String) As Boolean

        Dim conn As New SqlConnection(MiConexion)
        Dim Dataset As New DataSet
        Dim MaterialPendienteDeRetirar As Boolean = False

        Try

            Dim comando As New SqlClient.SqlCommand("AgregaRelDetOrdSerRetiroAparato", conn)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 0

            comando.Parameters.Add("@RelDetOrdSerRetiroAparato", SqlDbType.Xml)
            comando.Parameters("@RelDetOrdSerRetiroAparato").Direction = ParameterDirection.Input
            comando.Parameters("@RelDetOrdSerRetiroAparato").Value = strXML

            comando.Parameters.Add("@Contrato", SqlDbType.int)
            comando.Parameters("@Contrato").Direction = ParameterDirection.Input
            comando.Parameters("@Contrato").Value = Contrato

            comando.Parameters.Add("@Status", SqlDbType.VarChar, 1)
            comando.Parameters("@Status").Direction = ParameterDirection.Input
            comando.Parameters("@Status").Value = Status

            comando.Parameters.Add("@MaterialPendienteDeRetirar", SqlDbType.Bit)
            comando.Parameters("@MaterialPendienteDeRetirar").Direction = ParameterDirection.Output
            comando.Parameters("@MaterialPendienteDeRetirar").Value = False

            comando.Parameters.Add("@Msg", SqlDbType.VarChar, 400)
            comando.Parameters("@Msg").Direction = ParameterDirection.Output
            comando.Parameters("@Msg").Value = ""

            conn.Open()
            comando.ExecuteNonQuery()
            MaterialPendienteDeRetirar = comando.Parameters("@MaterialPendienteDeRetirar").Value
            'MsgBox("Se ha actualizado la relación del material retirado." + Chr(13) + "" & comando.Parameters("@Msg").Value.ToString, MsgBoxStyle.Information, "Se actualizó correctamente.")

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conn.Close()
            conn.Dispose()
        End Try

        Return MaterialPendienteDeRetirar
    End Function

    'Procedimiento que guarda la relación del USUARIO CON LA ORDEN
    Public Shared Sub Usp_ED_Guarda_RelOrdenUsuario(ByVal oClv_Orden As Long, oClv_UsuarioGenera As Integer, oClv_UsuarioEjecuta As Integer)

        Dim conn As New SqlConnection(MiConexion)
        Dim Dataset As New DataSet


        Try

            Dim comando As New SqlClient.SqlCommand("Usp_ED_Guarda_RelOrdenUsuario", conn)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 0

            comando.Parameters.Add("@Clv_Orden", SqlDbType.BigInt)
            comando.Parameters("@Clv_Orden").Direction = ParameterDirection.Input
            comando.Parameters("@Clv_Orden").Value = oClv_Orden


            comando.Parameters.Add("@Clv_UsuarioGenera", SqlDbType.Int)
            comando.Parameters("@Clv_UsuarioGenera").Direction = ParameterDirection.Input
            comando.Parameters("@Clv_UsuarioGenera").Value = oClv_UsuarioGenera

            comando.Parameters.Add("@Clv_UsuarioEjecuta", SqlDbType.Int)
            comando.Parameters("@Clv_UsuarioEjecuta").Direction = ParameterDirection.Input
            comando.Parameters("@Clv_UsuarioEjecuta").Value = oClv_UsuarioEjecuta


            conn.Open()
            comando.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conn.Close()
            conn.Dispose()
        End Try


    End Sub


    'Procedimiento que guarda la Relación del Detalle de la Orden con el Retiro de Aparatos
    Public Shared Function EjecutarRelDetOrdSerRetiroAparato(ByRef strXML As String, ByVal Contrato As Integer, ByVal Status As String, ByVal Clv_tecnico As Integer)

        Dim conn As New SqlConnection(MiConexion)

        Try

            Dim comando As New SqlClient.SqlCommand("EjecutarRelDetOrdSerRetiroAparato", conn)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 0

            comando.Parameters.Add("@RelDetOrdSerRetiroAparato", SqlDbType.Xml)
            comando.Parameters("@RelDetOrdSerRetiroAparato").Direction = ParameterDirection.Input
            comando.Parameters("@RelDetOrdSerRetiroAparato").Value = strXML

            comando.Parameters.Add("@Contrato", SqlDbType.Int)
            comando.Parameters("@Contrato").Direction = ParameterDirection.Input
            comando.Parameters("@Contrato").Value = Contrato

            comando.Parameters.Add("@Status", SqlDbType.VarChar, 1)
            comando.Parameters("@Status").Direction = ParameterDirection.Input
            comando.Parameters("@Status").Value = Status

            comando.Parameters.Add("@Clv_tecnico", SqlDbType.Int)
            comando.Parameters("@Clv_tecnico").Direction = ParameterDirection.Input
            comando.Parameters("@Clv_tecnico").Value = Clv_tecnico

            comando.Parameters.Add("@MaterialPendienteDeRetirar", SqlDbType.Bit)
            comando.Parameters("@MaterialPendienteDeRetirar").Direction = ParameterDirection.Output
            comando.Parameters("@MaterialPendienteDeRetirar").Value = False

            comando.Parameters.Add("@Msg", SqlDbType.VarChar, 400)
            comando.Parameters("@Msg").Direction = ParameterDirection.Output
            comando.Parameters("@Msg").Value = ""

            comando.Parameters.Add("@OrdenGenerada", SqlDbType.BigInt)
            comando.Parameters("@OrdenGenerada").Direction = ParameterDirection.Output
            comando.Parameters("@OrdenGenerada").Value = ""

            conn.Open()
            comando.ExecuteNonQuery()


            Usp_ED_Guarda_RelOrdenUsuario(comando.Parameters("@OrdenGenerada").Value.ToString, GloClvUsuario, GloClvUsuario)

            If InstalaParcial = True Then
                MsgBox("Se ha ejecutado correctamente la instalación parcial del Material." + Chr(13) + "Orden generada: " & comando.Parameters("@OrdenGenerada").Value.ToString, MsgBoxStyle.Information, "Se ejecutó correctamente.")
                InstalaParcial = False
            Else
                MsgBox("Se ha ejecutado correctamente el retiro parcial del Material." + Chr(13) + "Orden generada: " & comando.Parameters("@OrdenGenerada").Value.ToString, MsgBoxStyle.Information, "Se ejecutó correctamente.")
            End If


        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conn.Close()
            conn.Dispose()
        End Try


    End Function

    'Procedimiento que guarda la relación de los trabajos con el retiro de aparato.
    Public Shared Function GuardaConfiguracionesRelTrabajosRetiroAparato(ByRef strXML As String) As Boolean

        Dim conn As New SqlConnection(MiConexion)
        Dim Dataset As New DataSet
        Dim MaterialPendienteDeRetirar As Boolean = False

        Try

            Dim comando As New SqlClient.SqlCommand("GuardaConfiguracionesRelTrabajosRetiroAparato", conn)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 0

            comando.Parameters.Add("@xml", SqlDbType.Xml)
            comando.Parameters("@xml").Direction = ParameterDirection.Input
            comando.Parameters("@xml").Value = strXML

            conn.Open()
            comando.ExecuteNonQuery()
            MsgBox("Se actualizó correctamente.", MsgBoxStyle.Information, "Se actualizó correctamente.")

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conn.Close()
            conn.Dispose()
        End Try

        Return MaterialPendienteDeRetirar
    End Function

    'uspConsultaExistenciasTecnicoPorIDArticulo
    Public Shared Function uspConsultaExistenciasTecnicoMaterialExtra(ByVal idTecnico As Integer, ByVal Clv_Orden As Integer) As Boolean

        Dim conn As New SqlConnection(MiConexion)
        Dim MaterialDisponible As Boolean = False
        Try

            Dim comando As New SqlClient.SqlCommand("uspConsultaExistenciasTecnicoMaterialExtra", conn)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 0

            comando.Parameters.Add("@idTecnico", SqlDbType.Int)
            comando.Parameters("@idTecnico").Direction = ParameterDirection.Input
            comando.Parameters("@idTecnico").Value = idTecnico

            comando.Parameters.Add("@Clv_Orden", SqlDbType.BigInt)
            comando.Parameters("@Clv_Orden").Direction = ParameterDirection.Input
            comando.Parameters("@Clv_Orden").Value = Clv_Orden

            comando.Parameters.Add("@MaterialDisponible", SqlDbType.Bit)
            comando.Parameters("@MaterialDisponible").Direction = ParameterDirection.Output
            comando.Parameters("@MaterialDisponible").Value = MaterialDisponible

            conn.Open()
            comando.ExecuteNonQuery()
            MaterialDisponible = comando.Parameters("@MaterialDisponible").Value

            If Not (MaterialDisponible) Then
                MsgBox("El Técnico no cuenta con el(los) Accesorio(s) de Instalación suficiente para ejecutar la Orden. Puedes generarle una salida de material desde el sistema de Almacen.", MsgBoxStyle.Information, "Faltan Accesorios de Instalación")
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conn.Close()
            conn.Dispose()
        End Try

        Return MaterialDisponible
    End Function

    'Carga el Arbol del Material del Almacen ( uspConsultaArbolMaterial )
    Public Shared Function uspConsultaArbolMaterial() As DataSet

        Dim ds As New DataSet
        Dim conn As New SqlConnection(MiConexion)
        Try

            conn.Open()

            Dim comando As New SqlClient.SqlCommand("uspConsultaArbolMaterial", conn)
            comando.CommandType = CommandType.StoredProcedure

            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando

            Adaptador.Fill(ds)

        Catch ex As Exception

            MsgBox(ex.Message, MsgBoxStyle.Exclamation)

        Finally

            conn.Close()
            conn.Dispose()

        End Try

        Return ds
    End Function

    'Verifica si es que existen Accesorios de Instalación sin configurar (Esta función se cambió por la que valida por cada tipo de caja)
    Public Shared Function uspConsultaConfiguracionAccesorioInstalacion(ByVal Clv_Orden As Integer) As Boolean

        Dim conn As New SqlConnection(MiConexion)
        Dim Configurados As Boolean = False

        Try

            Dim comando As New SqlClient.SqlCommand("uspConsultaConfiguracionAccesorioInstalacion", conn)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 0

            comando.Parameters.Add("@Clv_Orden", SqlDbType.Int)
            comando.Parameters("@Clv_Orden").Direction = ParameterDirection.Input
            comando.Parameters("@Clv_Orden").Value = Clv_Orden

            comando.Parameters.Add("@Configurados", SqlDbType.Bit)
            comando.Parameters("@Configurados").Direction = ParameterDirection.Output
            comando.Parameters("@Configurados").Value = Configurados

            conn.Open()
            comando.ExecuteNonQuery()
            Configurados = comando.Parameters("@Configurados").Value

            If Not (Configurados) Then
                MsgBox("En la Orden hay Accesorios de Instalación que aún no se han ligado a un artículo del Almacen." + Chr(13) + Chr(13) + "Ingresa al menu: 'Generales - Sistema - Accesorios de Instalalción' para agregar las relaciones correspondientes.", MsgBoxStyle.Information, "Faltan Accesorios de Instalación por configurar.")
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conn.Close()
            conn.Dispose()
        End Try

        Return Configurados
    End Function

    'Verifica que para todos los accesorios de instalación en la Orden haya una relación con el artículo del almacen. (Para poder generar la bitácora del artpiculo exacto)
    Public Shared Function uspValidaConfiguracionesRelTipoCajaAccesorio(ByVal Clv_Orden As Integer) As Boolean

        Dim conn As New SqlConnection(MiConexion)
        Dim Consigurado As Boolean = False

        Try

            Dim comando As New SqlClient.SqlCommand("uspValidaConfiguracionesRelTipoCajaAccesorio", conn)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 0

            comando.Parameters.Add("@Clv_Orden", SqlDbType.Int)
            comando.Parameters("@Clv_Orden").Direction = ParameterDirection.Input
            comando.Parameters("@Clv_Orden").Value = Clv_Orden

            comando.Parameters.Add("@NOConsigurado", SqlDbType.Int)
            comando.Parameters("@NOConsigurado").Direction = ParameterDirection.Output
            comando.Parameters("@NOConsigurado").Value = 0

            conn.Open()
            comando.ExecuteNonQuery()

            If (comando.Parameters("@NOConsigurado").Value) = 0 Then
                Consigurado = True
            Else
                MsgBox("En la Orden hay Accesorios de Instalación que aún no se han ligado a un artículo del Almacen." + Chr(13) + Chr(13) + "Ingresa al menu: 'Generales - Sistema - Accesorios de Instalalción' para agregar las relaciones correspondientes.", MsgBoxStyle.Information, "Faltan Accesorios de Instalación por configurar.")
            End If


        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conn.Close()
            conn.Dispose()
        End Try

        Return Consigurado
    End Function

    'Valida que el técnico que ejecuta la Orden cuente con el Material necesario del tipo de Accesorio de Instalación.
    Public Shared Function uspValidaExistenciasTecnicoAccesoriosDeInstalacion(ByVal idTecnico As Integer, ByVal Clv_Orden As Integer) As Boolean

        Dim conn As New SqlConnection(MiConexion)
        Dim ExistenciasSuficientes As Boolean = False
        Try

            Dim comando As New SqlClient.SqlCommand("uspValidaExistenciasTecnicoAccesoriosDeInstalacion", conn)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 0

            comando.Parameters.Add("@Clv_Orden", SqlDbType.Int)
            comando.Parameters("@Clv_Orden").Direction = ParameterDirection.Input
            comando.Parameters("@Clv_Orden").Value = Clv_Orden

            comando.Parameters.Add("@idTecnico", SqlDbType.BigInt)
            comando.Parameters("@idTecnico").Direction = ParameterDirection.Input
            comando.Parameters("@idTecnico").Value = idTecnico

            comando.Parameters.Add("@ExistenciasSuficientes", SqlDbType.Bit)
            comando.Parameters("@ExistenciasSuficientes").Direction = ParameterDirection.Output
            comando.Parameters("@ExistenciasSuficientes").Value = 0

            conn.Open()
            comando.ExecuteNonQuery()
            ExistenciasSuficientes = comando.Parameters("@ExistenciasSuficientes").Value

            If Not (ExistenciasSuficientes) Then
                MsgBox("El Técnico seleccionado no cuenta con los Accesorios de Instalación ( Control Remoto o Cable RCA ) suficiente para ejecutar la Orden. Puedes generarle una salida de material desde el Sistema de Almacen.", MsgBoxStyle.Information, "Faltan Accesorios de Instalación")
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conn.Close()
            conn.Dispose()
        End Try

        Return ExistenciasSuficientes
    End Function

End Class

'Nuestra Clase de Retiro Aparato con Trabajos
Public Class TrabajosRetiroAparato

    Private _ident As Integer
    Private _Trabajo As String
    Private _Descripcion As String
    Private _AplicaRetiroAparato As Boolean

    Public Property ident() As Integer
        Get
            Return _ident
        End Get
        Set(ByVal Value As Integer)
            _ident = Value
        End Set
    End Property
    Public Property Trabajo() As String
        Get
            Return _Trabajo
        End Get
        Set(ByVal Value As String)
            _Trabajo = Value
        End Set
    End Property
    Public Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal Value As String)
            _Descripcion = Value
        End Set
    End Property
    Public Property AplicaRetiroAparato() As Boolean
        Get
            Return _AplicaRetiroAparato
        End Get
        Set(ByVal Value As Boolean)
            _AplicaRetiroAparato = Value
        End Set
    End Property

End Class
