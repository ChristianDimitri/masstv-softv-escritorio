<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelCliente2
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim lblServicioAsignado As System.Windows.Forms.Label
        Dim lblEsHotelDatos As System.Windows.Forms.Label
        Dim lblSoloInternetDatos As System.Windows.Forms.Label
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.TxtTelefono = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.btnLimpiarB = New System.Windows.Forms.Button
        Me.TxtApeM = New System.Windows.Forms.TextBox
        Me.lblApeM = New System.Windows.Forms.Label
        Me.TxtApeP = New System.Windows.Forms.TextBox
        Me.lblApeP = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.TrVDatosCliente = New System.Windows.Forms.TreeView
        Me.ckEsHotelDatos = New System.Windows.Forms.CheckBox
        Me.BuscaCliPorApellidosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Procedimientosarnoldo4 = New sofTV.Procedimientosarnoldo4
        Me.ckSoloInternetDatos = New System.Windows.Forms.CheckBox
        Me.lblColoniaDatos = New System.Windows.Forms.Label
        Me.lblNumeroDatos = New System.Windows.Forms.Label
        Me.lblDireccionDatos = New System.Windows.Forms.Label
        Me.lblNombreDatos = New System.Windows.Forms.Label
        Me.lblContatoDatos = New System.Windows.Forms.Label
        Me.lblContratoDatosD = New System.Windows.Forms.Label
        Me.lblNombreDatosD = New System.Windows.Forms.Label
        Me.lblCalleDatosD = New System.Windows.Forms.Label
        Me.lblColoniaDatosD = New System.Windows.Forms.Label
        Me.lblNumeroDatosD = New System.Windows.Forms.Label
        Me.CIUDADLabel1 = New System.Windows.Forms.Label
        Me.btnBuscar = New System.Windows.Forms.Button
        Me.TxtCiudad = New System.Windows.Forms.TextBox
        Me.lblciudad = New System.Windows.Forms.Label
        Me.TxtNumero = New System.Windows.Forms.TextBox
        Me.lblNum = New System.Windows.Forms.Label
        Me.TxtCalle = New System.Windows.Forms.TextBox
        Me.lblCalle = New System.Windows.Forms.Label
        Me.TxtContrato = New System.Windows.Forms.TextBox
        Me.lblContrato = New System.Windows.Forms.Label
        Me.TxtNombre = New System.Windows.Forms.TextBox
        Me.LblNombre = New System.Windows.Forms.Label
        Me.CMBLabel1 = New System.Windows.Forms.Label
        Me.DGDatosCliente = New System.Windows.Forms.DataGridView
        Me.CONTRATODataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.NOMBREDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CALLEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.COLONIADataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.NUMERODataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Telefono = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CIUDADDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.SOLOINTERNETDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.ESHOTELDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.btnCerrar = New System.Windows.Forms.Button
        Me.btnAceptar = New System.Windows.Forms.Button
        Me.BuscaCliPorApellidosTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.BuscaCliPorApellidosTableAdapter
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet
        Me.DameSerDELCliTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.dameSerDELCliTableAdapter
        Me.DameSerDELCliBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        lblServicioAsignado = New System.Windows.Forms.Label
        lblEsHotelDatos = New System.Windows.Forms.Label
        lblSoloInternetDatos = New System.Windows.Forms.Label
        Me.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.BuscaCliPorApellidosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGDatosCliente, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameSerDELCliBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblServicioAsignado
        '
        lblServicioAsignado.AutoSize = True
        lblServicioAsignado.Location = New System.Drawing.Point(6, 155)
        lblServicioAsignado.Name = "lblServicioAsignado"
        lblServicioAsignado.Size = New System.Drawing.Size(151, 16)
        lblServicioAsignado.TabIndex = 18
        lblServicioAsignado.Text = "Servicio Asignados :"
        '
        'lblEsHotelDatos
        '
        lblEsHotelDatos.AutoSize = True
        lblEsHotelDatos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        lblEsHotelDatos.ForeColor = System.Drawing.Color.White
        lblEsHotelDatos.Location = New System.Drawing.Point(143, 138)
        lblEsHotelDatos.Name = "lblEsHotelDatos"
        lblEsHotelDatos.Size = New System.Drawing.Size(67, 15)
        lblEsHotelDatos.TabIndex = 17
        lblEsHotelDatos.Text = "Es hotel :"
        '
        'lblSoloInternetDatos
        '
        lblSoloInternetDatos.AutoSize = True
        lblSoloInternetDatos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        lblSoloInternetDatos.ForeColor = System.Drawing.Color.White
        lblSoloInternetDatos.Location = New System.Drawing.Point(3, 138)
        lblSoloInternetDatos.Name = "lblSoloInternetDatos"
        lblSoloInternetDatos.Size = New System.Drawing.Size(97, 15)
        lblSoloInternetDatos.TabIndex = 16
        lblSoloInternetDatos.Text = "Solo Internet :"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.SplitContainer1)
        Me.Panel1.Location = New System.Drawing.Point(12, 9)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(839, 717)
        Me.Panel1.TabIndex = 14
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.Controls.Add(Me.TxtTelefono)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnLimpiarB)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TxtApeM)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lblApeM)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TxtApeP)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lblApeP)
        Me.SplitContainer1.Panel1.Controls.Add(Me.GroupBox1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnBuscar)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TxtCiudad)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lblciudad)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TxtNumero)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lblNum)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TxtCalle)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lblCalle)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TxtContrato)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lblContrato)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TxtNombre)
        Me.SplitContainer1.Panel1.Controls.Add(Me.LblNombre)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.DGDatosCliente)
        Me.SplitContainer1.Size = New System.Drawing.Size(839, 717)
        Me.SplitContainer1.SplitterDistance = 279
        Me.SplitContainer1.TabIndex = 0
        Me.SplitContainer1.TabStop = False
        '
        'TxtTelefono
        '
        Me.TxtTelefono.BackColor = System.Drawing.Color.LightGray
        Me.TxtTelefono.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TxtTelefono.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtTelefono.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtTelefono.Location = New System.Drawing.Point(12, 374)
        Me.TxtTelefono.Name = "TxtTelefono"
        Me.TxtTelefono.Size = New System.Drawing.Size(250, 24)
        Me.TxtTelefono.TabIndex = 19
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(13, 356)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(71, 15)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Teléfono :"
        '
        'btnLimpiarB
        '
        Me.btnLimpiarB.BackColor = System.Drawing.Color.DarkOrange
        Me.btnLimpiarB.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLimpiarB.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLimpiarB.ForeColor = System.Drawing.Color.Black
        Me.btnLimpiarB.Location = New System.Drawing.Point(125, 405)
        Me.btnLimpiarB.Name = "btnLimpiarB"
        Me.btnLimpiarB.Size = New System.Drawing.Size(97, 31)
        Me.btnLimpiarB.TabIndex = 11
        Me.btnLimpiarB.Text = "&Limpiar"
        Me.btnLimpiarB.UseVisualStyleBackColor = False
        '
        'TxtApeM
        '
        Me.TxtApeM.BackColor = System.Drawing.Color.LightGray
        Me.TxtApeM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TxtApeM.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtApeM.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtApeM.Location = New System.Drawing.Point(12, 185)
        Me.TxtApeM.Name = "TxtApeM"
        Me.TxtApeM.Size = New System.Drawing.Size(250, 24)
        Me.TxtApeM.TabIndex = 6
        '
        'lblApeM
        '
        Me.lblApeM.AutoSize = True
        Me.lblApeM.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApeM.Location = New System.Drawing.Point(13, 167)
        Me.lblApeM.Name = "lblApeM"
        Me.lblApeM.Size = New System.Drawing.Size(124, 15)
        Me.lblApeM.TabIndex = 17
        Me.lblApeM.Text = "Apellido Materno :"
        '
        'TxtApeP
        '
        Me.TxtApeP.BackColor = System.Drawing.Color.LightGray
        Me.TxtApeP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TxtApeP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtApeP.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtApeP.Location = New System.Drawing.Point(11, 140)
        Me.TxtApeP.Name = "TxtApeP"
        Me.TxtApeP.Size = New System.Drawing.Size(250, 24)
        Me.TxtApeP.TabIndex = 5
        '
        'lblApeP
        '
        Me.lblApeP.AutoSize = True
        Me.lblApeP.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApeP.Location = New System.Drawing.Point(12, 122)
        Me.lblApeP.Name = "lblApeP"
        Me.lblApeP.Size = New System.Drawing.Size(121, 15)
        Me.lblApeP.TabIndex = 15
        Me.lblApeP.Text = "Apellido Paterno :"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.DarkOrange
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.TrVDatosCliente)
        Me.GroupBox1.Controls.Add(lblServicioAsignado)
        Me.GroupBox1.Controls.Add(lblEsHotelDatos)
        Me.GroupBox1.Controls.Add(Me.ckEsHotelDatos)
        Me.GroupBox1.Controls.Add(lblSoloInternetDatos)
        Me.GroupBox1.Controls.Add(Me.ckSoloInternetDatos)
        Me.GroupBox1.Controls.Add(Me.lblColoniaDatos)
        Me.GroupBox1.Controls.Add(Me.lblNumeroDatos)
        Me.GroupBox1.Controls.Add(Me.lblDireccionDatos)
        Me.GroupBox1.Controls.Add(Me.lblNombreDatos)
        Me.GroupBox1.Controls.Add(Me.lblContatoDatos)
        Me.GroupBox1.Controls.Add(Me.lblContratoDatosD)
        Me.GroupBox1.Controls.Add(Me.lblNombreDatosD)
        Me.GroupBox1.Controls.Add(Me.lblCalleDatosD)
        Me.GroupBox1.Controls.Add(Me.lblColoniaDatosD)
        Me.GroupBox1.Controls.Add(Me.lblNumeroDatosD)
        Me.GroupBox1.Controls.Add(Me.CIUDADLabel1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.White
        Me.GroupBox1.Location = New System.Drawing.Point(3, 446)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(273, 264)
        Me.GroupBox1.TabIndex = 14
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Cliente"
        '
        'TrVDatosCliente
        '
        Me.TrVDatosCliente.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TrVDatosCliente.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TrVDatosCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TrVDatosCliente.ForeColor = System.Drawing.Color.Black
        Me.TrVDatosCliente.Location = New System.Drawing.Point(0, 174)
        Me.TrVDatosCliente.Name = "TrVDatosCliente"
        Me.TrVDatosCliente.Size = New System.Drawing.Size(273, 222)
        Me.TrVDatosCliente.TabIndex = 121
        Me.TrVDatosCliente.TabStop = False
        '
        'ckEsHotelDatos
        '
        Me.ckEsHotelDatos.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.BuscaCliPorApellidosBindingSource, "ESHOTEL", True))
        Me.ckEsHotelDatos.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ckEsHotelDatos.Location = New System.Drawing.Point(216, 134)
        Me.ckEsHotelDatos.Name = "ckEsHotelDatos"
        Me.ckEsHotelDatos.Size = New System.Drawing.Size(21, 24)
        Me.ckEsHotelDatos.TabIndex = 106
        Me.ckEsHotelDatos.TabStop = False
        '
        'BuscaCliPorApellidosBindingSource
        '
        Me.BuscaCliPorApellidosBindingSource.DataMember = "BuscaCliPorApellidos"
        Me.BuscaCliPorApellidosBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Procedimientosarnoldo4
        '
        Me.Procedimientosarnoldo4.DataSetName = "Procedimientosarnoldo4"
        Me.Procedimientosarnoldo4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ckSoloInternetDatos
        '
        Me.ckSoloInternetDatos.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.BuscaCliPorApellidosBindingSource, "SOLOINTERNET", True))
        Me.ckSoloInternetDatos.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ckSoloInternetDatos.Location = New System.Drawing.Point(106, 134)
        Me.ckSoloInternetDatos.Name = "ckSoloInternetDatos"
        Me.ckSoloInternetDatos.Size = New System.Drawing.Size(21, 24)
        Me.ckSoloInternetDatos.TabIndex = 105
        Me.ckSoloInternetDatos.TabStop = False
        '
        'lblColoniaDatos
        '
        Me.lblColoniaDatos.AutoSize = True
        Me.lblColoniaDatos.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblColoniaDatos.Location = New System.Drawing.Point(86, 115)
        Me.lblColoniaDatos.Name = "lblColoniaDatos"
        Me.lblColoniaDatos.Size = New System.Drawing.Size(31, 13)
        Me.lblColoniaDatos.TabIndex = 16
        Me.lblColoniaDatos.Text = "Col. :"
        '
        'lblNumeroDatos
        '
        Me.lblNumeroDatos.AutoSize = True
        Me.lblNumeroDatos.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNumeroDatos.Location = New System.Drawing.Point(12, 115)
        Me.lblNumeroDatos.Name = "lblNumeroDatos"
        Me.lblNumeroDatos.Size = New System.Drawing.Size(20, 13)
        Me.lblNumeroDatos.TabIndex = 15
        Me.lblNumeroDatos.Text = "# :"
        '
        'lblDireccionDatos
        '
        Me.lblDireccionDatos.AutoSize = True
        Me.lblDireccionDatos.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDireccionDatos.Location = New System.Drawing.Point(9, 75)
        Me.lblDireccionDatos.Name = "lblDireccionDatos"
        Me.lblDireccionDatos.Size = New System.Drawing.Size(58, 13)
        Me.lblDireccionDatos.TabIndex = 14
        Me.lblDireccionDatos.Text = "Dirección :"
        '
        'lblNombreDatos
        '
        Me.lblNombreDatos.AutoSize = True
        Me.lblNombreDatos.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombreDatos.Location = New System.Drawing.Point(9, 35)
        Me.lblNombreDatos.Name = "lblNombreDatos"
        Me.lblNombreDatos.Size = New System.Drawing.Size(50, 13)
        Me.lblNombreDatos.TabIndex = 13
        Me.lblNombreDatos.Text = "Nombre :"
        '
        'lblContatoDatos
        '
        Me.lblContatoDatos.AutoSize = True
        Me.lblContatoDatos.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContatoDatos.Location = New System.Drawing.Point(6, 18)
        Me.lblContatoDatos.Name = "lblContatoDatos"
        Me.lblContatoDatos.Size = New System.Drawing.Size(63, 13)
        Me.lblContatoDatos.TabIndex = 12
        Me.lblContatoDatos.Text = "# Contrato :"
        '
        'lblContratoDatosD
        '
        Me.lblContratoDatosD.BackColor = System.Drawing.Color.DarkOrange
        Me.lblContratoDatosD.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BuscaCliPorApellidosBindingSource, "CONTRATO", True))
        Me.lblContratoDatosD.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContratoDatosD.Location = New System.Drawing.Point(75, 16)
        Me.lblContratoDatosD.Name = "lblContratoDatosD"
        Me.lblContratoDatosD.Size = New System.Drawing.Size(135, 15)
        Me.lblContratoDatosD.TabIndex = 100
        '
        'lblNombreDatosD
        '
        Me.lblNombreDatosD.AutoEllipsis = True
        Me.lblNombreDatosD.BackColor = System.Drawing.Color.DarkOrange
        Me.lblNombreDatosD.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BuscaCliPorApellidosBindingSource, "NOMBRE", True))
        Me.lblNombreDatosD.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombreDatosD.Location = New System.Drawing.Point(62, 31)
        Me.lblNombreDatosD.Name = "lblNombreDatosD"
        Me.lblNombreDatosD.Size = New System.Drawing.Size(205, 17)
        Me.lblNombreDatosD.TabIndex = 101
        '
        'lblCalleDatosD
        '
        Me.lblCalleDatosD.AutoEllipsis = True
        Me.lblCalleDatosD.BackColor = System.Drawing.Color.DarkOrange
        Me.lblCalleDatosD.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BuscaCliPorApellidosBindingSource, "CALLE", True))
        Me.lblCalleDatosD.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCalleDatosD.Location = New System.Drawing.Point(12, 92)
        Me.lblCalleDatosD.Name = "lblCalleDatosD"
        Me.lblCalleDatosD.Size = New System.Drawing.Size(255, 23)
        Me.lblCalleDatosD.TabIndex = 102
        '
        'lblColoniaDatosD
        '
        Me.lblColoniaDatosD.AutoEllipsis = True
        Me.lblColoniaDatosD.BackColor = System.Drawing.Color.DarkOrange
        Me.lblColoniaDatosD.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BuscaCliPorApellidosBindingSource, "COLONIA", True))
        Me.lblColoniaDatosD.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblColoniaDatosD.Location = New System.Drawing.Point(123, 115)
        Me.lblColoniaDatosD.Name = "lblColoniaDatosD"
        Me.lblColoniaDatosD.Size = New System.Drawing.Size(135, 23)
        Me.lblColoniaDatosD.TabIndex = 104
        '
        'lblNumeroDatosD
        '
        Me.lblNumeroDatosD.AutoEllipsis = True
        Me.lblNumeroDatosD.BackColor = System.Drawing.Color.DarkOrange
        Me.lblNumeroDatosD.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BuscaCliPorApellidosBindingSource, "NUMERO", True))
        Me.lblNumeroDatosD.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNumeroDatosD.Location = New System.Drawing.Point(33, 115)
        Me.lblNumeroDatosD.Name = "lblNumeroDatosD"
        Me.lblNumeroDatosD.Size = New System.Drawing.Size(47, 23)
        Me.lblNumeroDatosD.TabIndex = 103
        '
        'CIUDADLabel1
        '
        Me.CIUDADLabel1.AutoEllipsis = True
        Me.CIUDADLabel1.BackColor = System.Drawing.Color.DarkOrange
        Me.CIUDADLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CIUDADLabel1.Location = New System.Drawing.Point(13, 155)
        Me.CIUDADLabel1.Name = "CIUDADLabel1"
        Me.CIUDADLabel1.Size = New System.Drawing.Size(224, 23)
        Me.CIUDADLabel1.TabIndex = 11
        '
        'btnBuscar
        '
        Me.btnBuscar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuscar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscar.ForeColor = System.Drawing.Color.Black
        Me.btnBuscar.Location = New System.Drawing.Point(12, 405)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(91, 31)
        Me.btnBuscar.TabIndex = 10
        Me.btnBuscar.Text = "&Buscar"
        Me.btnBuscar.UseVisualStyleBackColor = False
        '
        'TxtCiudad
        '
        Me.TxtCiudad.BackColor = System.Drawing.Color.LightGray
        Me.TxtCiudad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TxtCiudad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtCiudad.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtCiudad.Location = New System.Drawing.Point(11, 329)
        Me.TxtCiudad.Name = "TxtCiudad"
        Me.TxtCiudad.Size = New System.Drawing.Size(250, 24)
        Me.TxtCiudad.TabIndex = 9
        '
        'lblciudad
        '
        Me.lblciudad.AutoSize = True
        Me.lblciudad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblciudad.Location = New System.Drawing.Point(12, 311)
        Me.lblciudad.Name = "lblciudad"
        Me.lblciudad.Size = New System.Drawing.Size(60, 15)
        Me.lblciudad.TabIndex = 9
        Me.lblciudad.Text = "Ciudad :"
        '
        'TxtNumero
        '
        Me.TxtNumero.BackColor = System.Drawing.Color.LightGray
        Me.TxtNumero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TxtNumero.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtNumero.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtNumero.Location = New System.Drawing.Point(11, 284)
        Me.TxtNumero.Name = "TxtNumero"
        Me.TxtNumero.Size = New System.Drawing.Size(250, 24)
        Me.TxtNumero.TabIndex = 8
        '
        'lblNum
        '
        Me.lblNum.AutoSize = True
        Me.lblNum.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNum.Location = New System.Drawing.Point(12, 266)
        Me.lblNum.Name = "lblNum"
        Me.lblNum.Size = New System.Drawing.Size(66, 15)
        Me.lblNum.TabIndex = 7
        Me.lblNum.Text = "Numero :"
        '
        'TxtCalle
        '
        Me.TxtCalle.BackColor = System.Drawing.Color.LightGray
        Me.TxtCalle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TxtCalle.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtCalle.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtCalle.Location = New System.Drawing.Point(11, 239)
        Me.TxtCalle.Name = "TxtCalle"
        Me.TxtCalle.Size = New System.Drawing.Size(250, 24)
        Me.TxtCalle.TabIndex = 7
        '
        'lblCalle
        '
        Me.lblCalle.AutoSize = True
        Me.lblCalle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCalle.Location = New System.Drawing.Point(12, 221)
        Me.lblCalle.Name = "lblCalle"
        Me.lblCalle.Size = New System.Drawing.Size(48, 15)
        Me.lblCalle.TabIndex = 5
        Me.lblCalle.Text = "Calle :"
        '
        'TxtContrato
        '
        Me.TxtContrato.BackColor = System.Drawing.Color.LightGray
        Me.TxtContrato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TxtContrato.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtContrato.Location = New System.Drawing.Point(12, 50)
        Me.TxtContrato.Name = "TxtContrato"
        Me.TxtContrato.Size = New System.Drawing.Size(91, 24)
        Me.TxtContrato.TabIndex = 3
        '
        'lblContrato
        '
        Me.lblContrato.AutoSize = True
        Me.lblContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContrato.Location = New System.Drawing.Point(9, 29)
        Me.lblContrato.Name = "lblContrato"
        Me.lblContrato.Size = New System.Drawing.Size(69, 15)
        Me.lblContrato.TabIndex = 3
        Me.lblContrato.Text = "Contrato :"
        '
        'TxtNombre
        '
        Me.TxtNombre.BackColor = System.Drawing.Color.LightGray
        Me.TxtNombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TxtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtNombre.Location = New System.Drawing.Point(12, 95)
        Me.TxtNombre.Name = "TxtNombre"
        Me.TxtNombre.Size = New System.Drawing.Size(250, 24)
        Me.TxtNombre.TabIndex = 4
        '
        'LblNombre
        '
        Me.LblNombre.AutoSize = True
        Me.LblNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblNombre.Location = New System.Drawing.Point(13, 77)
        Me.LblNombre.Name = "LblNombre"
        Me.LblNombre.Size = New System.Drawing.Size(137, 15)
        Me.LblNombre.TabIndex = 1
        Me.LblNombre.Text = "Nombre Del Cliente:"
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(5, 5)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(195, 24)
        Me.CMBLabel1.TabIndex = 0
        Me.CMBLabel1.Text = "Buscar Cliente Por :"
        '
        'DGDatosCliente
        '
        Me.DGDatosCliente.AllowUserToAddRows = False
        Me.DGDatosCliente.AllowUserToDeleteRows = False
        Me.DGDatosCliente.AllowUserToOrderColumns = True
        Me.DGDatosCliente.AutoGenerateColumns = False
        Me.DGDatosCliente.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.Chocolate
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGDatosCliente.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.DGDatosCliente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGDatosCliente.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CONTRATODataGridViewTextBoxColumn, Me.NOMBREDataGridViewTextBoxColumn, Me.CALLEDataGridViewTextBoxColumn, Me.COLONIADataGridViewTextBoxColumn, Me.NUMERODataGridViewTextBoxColumn, Me.Telefono, Me.CIUDADDataGridViewTextBoxColumn, Me.SOLOINTERNETDataGridViewCheckBoxColumn, Me.ESHOTELDataGridViewCheckBoxColumn})
        Me.DGDatosCliente.DataSource = Me.BuscaCliPorApellidosBindingSource
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGDatosCliente.DefaultCellStyle = DataGridViewCellStyle6
        Me.DGDatosCliente.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DGDatosCliente.Location = New System.Drawing.Point(0, 0)
        Me.DGDatosCliente.MultiSelect = False
        Me.DGDatosCliente.Name = "DGDatosCliente"
        Me.DGDatosCliente.ReadOnly = True
        Me.DGDatosCliente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGDatosCliente.Size = New System.Drawing.Size(556, 717)
        Me.DGDatosCliente.TabIndex = 0
        Me.DGDatosCliente.TabStop = False
        '
        'CONTRATODataGridViewTextBoxColumn
        '
        Me.CONTRATODataGridViewTextBoxColumn.DataPropertyName = "CONTRATO"
        Me.CONTRATODataGridViewTextBoxColumn.HeaderText = "Contrato"
        Me.CONTRATODataGridViewTextBoxColumn.Name = "CONTRATODataGridViewTextBoxColumn"
        Me.CONTRATODataGridViewTextBoxColumn.ReadOnly = True
        '
        'NOMBREDataGridViewTextBoxColumn
        '
        Me.NOMBREDataGridViewTextBoxColumn.DataPropertyName = "NOMBRE"
        Me.NOMBREDataGridViewTextBoxColumn.HeaderText = "Nombre"
        Me.NOMBREDataGridViewTextBoxColumn.Name = "NOMBREDataGridViewTextBoxColumn"
        Me.NOMBREDataGridViewTextBoxColumn.ReadOnly = True
        Me.NOMBREDataGridViewTextBoxColumn.Width = 200
        '
        'CALLEDataGridViewTextBoxColumn
        '
        Me.CALLEDataGridViewTextBoxColumn.DataPropertyName = "CALLE"
        Me.CALLEDataGridViewTextBoxColumn.HeaderText = "Calle"
        Me.CALLEDataGridViewTextBoxColumn.Name = "CALLEDataGridViewTextBoxColumn"
        Me.CALLEDataGridViewTextBoxColumn.ReadOnly = True
        Me.CALLEDataGridViewTextBoxColumn.Width = 200
        '
        'COLONIADataGridViewTextBoxColumn
        '
        Me.COLONIADataGridViewTextBoxColumn.DataPropertyName = "COLONIA"
        Me.COLONIADataGridViewTextBoxColumn.HeaderText = "Colonia"
        Me.COLONIADataGridViewTextBoxColumn.Name = "COLONIADataGridViewTextBoxColumn"
        Me.COLONIADataGridViewTextBoxColumn.ReadOnly = True
        '
        'NUMERODataGridViewTextBoxColumn
        '
        Me.NUMERODataGridViewTextBoxColumn.DataPropertyName = "NUMERO"
        Me.NUMERODataGridViewTextBoxColumn.HeaderText = "Numero"
        Me.NUMERODataGridViewTextBoxColumn.Name = "NUMERODataGridViewTextBoxColumn"
        Me.NUMERODataGridViewTextBoxColumn.ReadOnly = True
        '
        'Telefono
        '
        Me.Telefono.DataPropertyName = "Telefono"
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Telefono.DefaultCellStyle = DataGridViewCellStyle5
        Me.Telefono.HeaderText = "Telefono"
        Me.Telefono.Name = "Telefono"
        Me.Telefono.ReadOnly = True
        '
        'CIUDADDataGridViewTextBoxColumn
        '
        Me.CIUDADDataGridViewTextBoxColumn.DataPropertyName = "CIUDAD"
        Me.CIUDADDataGridViewTextBoxColumn.HeaderText = "Ciudad"
        Me.CIUDADDataGridViewTextBoxColumn.Name = "CIUDADDataGridViewTextBoxColumn"
        Me.CIUDADDataGridViewTextBoxColumn.ReadOnly = True
        '
        'SOLOINTERNETDataGridViewCheckBoxColumn
        '
        Me.SOLOINTERNETDataGridViewCheckBoxColumn.DataPropertyName = "SOLOINTERNET"
        Me.SOLOINTERNETDataGridViewCheckBoxColumn.HeaderText = "Solo Internet"
        Me.SOLOINTERNETDataGridViewCheckBoxColumn.Name = "SOLOINTERNETDataGridViewCheckBoxColumn"
        Me.SOLOINTERNETDataGridViewCheckBoxColumn.ReadOnly = True
        '
        'ESHOTELDataGridViewCheckBoxColumn
        '
        Me.ESHOTELDataGridViewCheckBoxColumn.DataPropertyName = "ESHOTEL"
        Me.ESHOTELDataGridViewCheckBoxColumn.HeaderText = "Es Hotel"
        Me.ESHOTELDataGridViewCheckBoxColumn.Name = "ESHOTELDataGridViewCheckBoxColumn"
        Me.ESHOTELDataGridViewCheckBoxColumn.ReadOnly = True
        '
        'btnCerrar
        '
        Me.btnCerrar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnCerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCerrar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCerrar.ForeColor = System.Drawing.Color.Black
        Me.btnCerrar.Location = New System.Drawing.Point(868, 661)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(136, 36)
        Me.btnCerrar.TabIndex = 13
        Me.btnCerrar.Text = "&CERRAR"
        Me.btnCerrar.UseVisualStyleBackColor = False
        '
        'btnAceptar
        '
        Me.btnAceptar.BackColor = System.Drawing.Color.Orange
        Me.btnAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.ForeColor = System.Drawing.Color.Black
        Me.btnAceptar.Location = New System.Drawing.Point(868, 9)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(136, 36)
        Me.btnAceptar.TabIndex = 12
        Me.btnAceptar.Text = "&ACEPTAR"
        Me.btnAceptar.UseVisualStyleBackColor = False
        '
        'BuscaCliPorApellidosTableAdapter
        '
        Me.BuscaCliPorApellidosTableAdapter.ClearBeforeFill = True
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DameSerDELCliTableAdapter
        '
        Me.DameSerDELCliTableAdapter.ClearBeforeFill = True
        '
        'DameSerDELCliBindingSource
        '
        Me.DameSerDELCliBindingSource.DataMember = "dameSerDELCli"
        Me.DameSerDELCliBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(9, 53)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(25, 13)
        Me.Label2.TabIndex = 122
        Me.Label2.Text = "Tel:"
        '
        'Label3
        '
        Me.Label3.AutoEllipsis = True
        Me.Label3.BackColor = System.Drawing.Color.DarkOrange
        Me.Label3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BuscaCliPorApellidosBindingSource, "Telefono", True))
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(40, 52)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(205, 14)
        Me.Label3.TabIndex = 123
        '
        'FrmSelCliente2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1016, 734)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.btnAceptar)
        Me.MaximizeBox = False
        Me.Name = "FrmSelCliente2"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selecciona Cliente"
        Me.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.BuscaCliPorApellidosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGDatosCliente, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameSerDELCliBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents btnLimpiarB As System.Windows.Forms.Button
    Friend WithEvents TxtApeM As System.Windows.Forms.TextBox
    Friend WithEvents lblApeM As System.Windows.Forms.Label
    Friend WithEvents TxtApeP As System.Windows.Forms.TextBox
    Friend WithEvents lblApeP As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TrVDatosCliente As System.Windows.Forms.TreeView
    Friend WithEvents ckEsHotelDatos As System.Windows.Forms.CheckBox
    Friend WithEvents ckSoloInternetDatos As System.Windows.Forms.CheckBox
    Friend WithEvents lblColoniaDatos As System.Windows.Forms.Label
    Friend WithEvents lblNumeroDatos As System.Windows.Forms.Label
    Friend WithEvents lblDireccionDatos As System.Windows.Forms.Label
    Friend WithEvents lblNombreDatos As System.Windows.Forms.Label
    Friend WithEvents lblContatoDatos As System.Windows.Forms.Label
    Friend WithEvents lblContratoDatosD As System.Windows.Forms.Label
    Friend WithEvents lblNombreDatosD As System.Windows.Forms.Label
    Friend WithEvents lblCalleDatosD As System.Windows.Forms.Label
    Friend WithEvents lblColoniaDatosD As System.Windows.Forms.Label
    Friend WithEvents lblNumeroDatosD As System.Windows.Forms.Label
    Friend WithEvents CIUDADLabel1 As System.Windows.Forms.Label
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents TxtCiudad As System.Windows.Forms.TextBox
    Friend WithEvents lblciudad As System.Windows.Forms.Label
    Friend WithEvents TxtNumero As System.Windows.Forms.TextBox
    Friend WithEvents lblNum As System.Windows.Forms.Label
    Friend WithEvents TxtCalle As System.Windows.Forms.TextBox
    Friend WithEvents lblCalle As System.Windows.Forms.Label
    Friend WithEvents TxtContrato As System.Windows.Forms.TextBox
    Friend WithEvents lblContrato As System.Windows.Forms.Label
    Friend WithEvents TxtNombre As System.Windows.Forms.TextBox
    Friend WithEvents LblNombre As System.Windows.Forms.Label
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents DGDatosCliente As System.Windows.Forms.DataGridView
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents BuscaCliPorApellidosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Procedimientosarnoldo4 As sofTV.Procedimientosarnoldo4
    Friend WithEvents BuscaCliPorApellidosTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.BuscaCliPorApellidosTableAdapter
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents DameSerDELCliTableAdapter As sofTV.NewSofTvDataSetTableAdapters.dameSerDELCliTableAdapter
    Friend WithEvents DameSerDELCliBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TxtTelefono As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents CONTRATODataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NOMBREDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CALLEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents COLONIADataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NUMERODataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Telefono As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CIUDADDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SOLOINTERNETDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents ESHOTELDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
