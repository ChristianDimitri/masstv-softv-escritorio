﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwClientes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ESHOTELLabel1 As System.Windows.Forms.Label
        Dim ServicioLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Button14 = New System.Windows.Forms.Button()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.TxtTarjeta = New System.Windows.Forms.TextBox()
        Me.TxtSetUpBox = New System.Windows.Forms.TextBox()
        Me.AMaternoTextBox = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.APaternoTextBox = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.ESHOTELCheckBox = New System.Windows.Forms.CheckBox()
        Me.BUSCLIPORCONTRATO2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.SOLOINTERNETCheckBox = New System.Windows.Forms.CheckBox()
        Me.BUSCLIPORCONTRATO2BindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Procedimientosarnoldo4 = New sofTV.Procedimientosarnoldo4()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.CONTRATOLabel1 = New System.Windows.Forms.Label()
        Me.NOMBRELabel1 = New System.Windows.Forms.Label()
        Me.CALLELabel1 = New System.Windows.Forms.Label()
        Me.COLONIALabel1 = New System.Windows.Forms.Label()
        Me.NUMEROLabel1 = New System.Windows.Forms.Label()
        Me.CIUDADLabel1 = New System.Windows.Forms.Label()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.BCIUDAD = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.BNUMERO = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.BCALLE = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.bcONTRATO = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.BNOMBRE = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.btnBtelefono = New System.Windows.Forms.Button()
        Me.txtTelefono = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.CONTRATO1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NOMBRE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CALLE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COLONIA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NUMERO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CIUDAD = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TELEFONO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SOLOINTERNET = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ESHOTEL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.DataSetEDGAR = New sofTV.DataSetEDGAR()
        Me.ChecaServicioContratadoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ChecaServicioContratadoTableAdapter = New sofTV.DataSetEDGARTableAdapters.ChecaServicioContratadoTableAdapter()
        Me.DataSetarnoldo = New sofTV.DataSetarnoldo()
        Me.Selecciona_ImpresoraBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Selecciona_ImpresoraTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Selecciona_ImpresoraTableAdapter()
        Me.Selecciona_Impresora_SucursalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Selecciona_Impresora_SucursalTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Selecciona_Impresora_SucursalTableAdapter()
        Me.Dame_clv_EmpresaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_clv_EmpresaTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Dame_clv_EmpresaTableAdapter()
        Me.Valida_Contrato_ServBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Valida_Contrato_ServTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Valida_Contrato_ServTableAdapter()
        Me.Valida_si_facturoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Valida_si_facturoTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Valida_si_facturoTableAdapter()
        Me.Hora_insBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Hora_insTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Hora_insTableAdapter()
        Me.Inserta_Comentario2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Comentario2TableAdapter = New sofTV.DataSetarnoldoTableAdapters.Inserta_Comentario2TableAdapter()
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2()
        Me.Valida_Factura_contratoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Valida_Factura_contratoTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Valida_Factura_contratoTableAdapter()
        Me.Checa_servicios_clienteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Checa_servicios_clienteTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Checa_servicios_clienteTableAdapter()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.DataSetEdgarRev2 = New sofTV.DataSetEdgarRev2()
        Me.Dime_Si_BoletasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dime_Si_BoletasTableAdapter = New sofTV.DataSetEdgarRev2TableAdapters.Dime_Si_BoletasTableAdapter()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.DataSetEric2 = New sofTV.DataSetEric2()
        Me.ConGeneralRelBDBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConGeneralRelBDTableAdapter = New sofTV.DataSetEric2TableAdapters.ConGeneralRelBDTableAdapter()
        Me.DameSerDELCliBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACALLESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACALLESTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACALLESTableAdapter()
        Me.DameSerDELCliTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.dameSerDELCliTableAdapter()
        Me.BUSCLIPORCONTRATO2TableAdapter = New sofTV.NewSofTvDataSetTableAdapters.BUSCLIPORCONTRATO2TableAdapter()
        Me.DamePermisosFormBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DamePermisosFormTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.DamePermisosFormTableAdapter()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.BUSCLIPORCONTRATO2TableAdapter1 = New sofTV.Procedimientosarnoldo4TableAdapters.BUSCLIPORCONTRATO2TableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter5 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Button15 = New System.Windows.Forms.Button()
        Me.BtnHistorialTarjetas = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter6 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Button16 = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.Muestra_ServiciosDigitalesTableAdapter7 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter8 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter9 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter10 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter11 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter12 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter13 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter14 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        ESHOTELLabel1 = New System.Windows.Forms.Label()
        ServicioLabel = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.BUSCLIPORCONTRATO2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BUSCLIPORCONTRATO2BindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ChecaServicioContratadoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Selecciona_ImpresoraBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Selecciona_Impresora_SucursalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_clv_EmpresaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Valida_Contrato_ServBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Valida_si_facturoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Hora_insBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Comentario2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Valida_Factura_contratoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Checa_servicios_clienteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dime_Si_BoletasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConGeneralRelBDBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameSerDELCliBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACALLESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DamePermisosFormBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ESHOTELLabel1
        '
        ESHOTELLabel1.AutoSize = True
        ESHOTELLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ESHOTELLabel1.ForeColor = System.Drawing.Color.White
        ESHOTELLabel1.Location = New System.Drawing.Point(143, 250)
        ESHOTELLabel1.Name = "ESHOTELLabel1"
        ESHOTELLabel1.Size = New System.Drawing.Size(67, 15)
        ESHOTELLabel1.TabIndex = 17
        ESHOTELLabel1.Text = "Es hotel :"
        AddHandler ESHOTELLabel1.Click, AddressOf Me.ESHOTELLabel1_Click
        '
        'ServicioLabel
        '
        ServicioLabel.AutoSize = True
        ServicioLabel.Location = New System.Drawing.Point(3, 121)
        ServicioLabel.Name = "ServicioLabel"
        ServicioLabel.Size = New System.Drawing.Size(151, 16)
        ServicioLabel.TabIndex = 18
        ServicioLabel.Text = "Servicio Asignados :"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.SplitContainer1)
        Me.Panel1.Location = New System.Drawing.Point(1, 2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(868, 698)
        Me.Panel1.TabIndex = 0
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label15)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button14)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label14)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TxtTarjeta)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TxtSetUpBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.AMaternoTextBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label12)
        Me.SplitContainer1.Panel1.Controls.Add(Me.APaternoTextBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label13)
        Me.SplitContainer1.Panel1.Controls.Add(Me.GroupBox1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button8)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button7)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.BCIUDAD)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label6)
        Me.SplitContainer1.Panel1.Controls.Add(Me.BNUMERO)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.BCALLE)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label4)
        Me.SplitContainer1.Panel1.Controls.Add(Me.bcONTRATO)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.BNOMBRE)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnBtelefono)
        Me.SplitContainer1.Panel1.Controls.Add(Me.txtTelefono)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label27)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label28)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label31)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.DataGridView1)
        Me.SplitContainer1.Size = New System.Drawing.Size(868, 698)
        Me.SplitContainer1.SplitterDistance = 288
        Me.SplitContainer1.TabIndex = 0
        Me.SplitContainer1.TabStop = False
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(12, 434)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(124, 15)
        Me.Label15.TabIndex = 46
        Me.Label15.Text = "No. Serie Tarjeta :"
        '
        'Button14
        '
        Me.Button14.BackColor = System.Drawing.Color.DarkOrange
        Me.Button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button14.ForeColor = System.Drawing.Color.Black
        Me.Button14.Location = New System.Drawing.Point(174, 479)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(88, 23)
        Me.Button14.TabIndex = 45
        Me.Button14.Text = "&Buscar"
        Me.Button14.UseVisualStyleBackColor = False
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(9, 390)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(86, 15)
        Me.Label14.TabIndex = 44
        Me.Label14.Text = "Set Up Box :"
        '
        'TxtTarjeta
        '
        Me.TxtTarjeta.BackColor = System.Drawing.Color.LightGray
        Me.TxtTarjeta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TxtTarjeta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtTarjeta.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtTarjeta.Location = New System.Drawing.Point(12, 451)
        Me.TxtTarjeta.Name = "TxtTarjeta"
        Me.TxtTarjeta.Size = New System.Drawing.Size(250, 24)
        Me.TxtTarjeta.TabIndex = 43
        '
        'TxtSetUpBox
        '
        Me.TxtSetUpBox.BackColor = System.Drawing.Color.LightGray
        Me.TxtSetUpBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TxtSetUpBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtSetUpBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtSetUpBox.Location = New System.Drawing.Point(10, 408)
        Me.TxtSetUpBox.Name = "TxtSetUpBox"
        Me.TxtSetUpBox.Size = New System.Drawing.Size(250, 24)
        Me.TxtSetUpBox.TabIndex = 42
        '
        'AMaternoTextBox
        '
        Me.AMaternoTextBox.BackColor = System.Drawing.Color.LightGray
        Me.AMaternoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AMaternoTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.AMaternoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AMaternoTextBox.Location = New System.Drawing.Point(9, 177)
        Me.AMaternoTextBox.Name = "AMaternoTextBox"
        Me.AMaternoTextBox.Size = New System.Drawing.Size(250, 24)
        Me.AMaternoTextBox.TabIndex = 39
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(8, 159)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(124, 15)
        Me.Label12.TabIndex = 41
        Me.Label12.Text = "Apellido Materno :"
        '
        'APaternoTextBox
        '
        Me.APaternoTextBox.BackColor = System.Drawing.Color.LightGray
        Me.APaternoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.APaternoTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.APaternoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.APaternoTextBox.Location = New System.Drawing.Point(10, 132)
        Me.APaternoTextBox.Name = "APaternoTextBox"
        Me.APaternoTextBox.Size = New System.Drawing.Size(250, 24)
        Me.APaternoTextBox.TabIndex = 38
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(9, 114)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(121, 15)
        Me.Label13.TabIndex = 40
        Me.Label13.Text = "Apellido Paterno :"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.DarkOrange
        Me.GroupBox1.Controls.Add(Me.TreeView1)
        Me.GroupBox1.Controls.Add(ServicioLabel)
        Me.GroupBox1.Controls.Add(ESHOTELLabel1)
        Me.GroupBox1.Controls.Add(Me.ESHOTELCheckBox)
        Me.GroupBox1.Controls.Add(Me.SOLOINTERNETCheckBox)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.CONTRATOLabel1)
        Me.GroupBox1.Controls.Add(Me.NOMBRELabel1)
        Me.GroupBox1.Controls.Add(Me.CALLELabel1)
        Me.GroupBox1.Controls.Add(Me.COLONIALabel1)
        Me.GroupBox1.Controls.Add(Me.NUMEROLabel1)
        Me.GroupBox1.Controls.Add(Me.CIUDADLabel1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.White
        Me.GroupBox1.Location = New System.Drawing.Point(10, 508)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(265, 341)
        Me.GroupBox1.TabIndex = 14
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Cliente"
        '
        'TreeView1
        '
        Me.TreeView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView1.Location = New System.Drawing.Point(12, 140)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(243, 184)
        Me.TreeView1.TabIndex = 19
        Me.TreeView1.TabStop = False
        '
        'ESHOTELCheckBox
        '
        Me.ESHOTELCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.BUSCLIPORCONTRATO2BindingSource, "ESHOTEL", True))
        Me.ESHOTELCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ESHOTELCheckBox.Location = New System.Drawing.Point(189, 241)
        Me.ESHOTELCheckBox.Name = "ESHOTELCheckBox"
        Me.ESHOTELCheckBox.Size = New System.Drawing.Size(21, 24)
        Me.ESHOTELCheckBox.TabIndex = 18
        Me.ESHOTELCheckBox.TabStop = False
        '
        'BUSCLIPORCONTRATO2BindingSource
        '
        Me.BUSCLIPORCONTRATO2BindingSource.DataMember = "BUSCLIPORCONTRATO2"
        Me.BUSCLIPORCONTRATO2BindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SOLOINTERNETCheckBox
        '
        Me.SOLOINTERNETCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.BUSCLIPORCONTRATO2BindingSource1, "SOLOINTERNET", True))
        Me.SOLOINTERNETCheckBox.Enabled = False
        Me.SOLOINTERNETCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.SOLOINTERNETCheckBox.Location = New System.Drawing.Point(238, 96)
        Me.SOLOINTERNETCheckBox.Name = "SOLOINTERNETCheckBox"
        Me.SOLOINTERNETCheckBox.Size = New System.Drawing.Size(21, 24)
        Me.SOLOINTERNETCheckBox.TabIndex = 17
        Me.SOLOINTERNETCheckBox.TabStop = False
        '
        'BUSCLIPORCONTRATO2BindingSource1
        '
        Me.BUSCLIPORCONTRATO2BindingSource1.DataMember = "BUSCLIPORCONTRATO2"
        Me.BUSCLIPORCONTRATO2BindingSource1.DataSource = Me.Procedimientosarnoldo4
        '
        'Procedimientosarnoldo4
        '
        Me.Procedimientosarnoldo4.DataSetName = "Procedimientosarnoldo4"
        Me.Procedimientosarnoldo4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(13, 100)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(31, 13)
        Me.Label11.TabIndex = 16
        Me.Label11.Text = "Col. :"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(173, 81)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(20, 13)
        Me.Label10.TabIndex = 15
        Me.Label10.Text = "# :"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(10, 77)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(58, 13)
        Me.Label9.TabIndex = 14
        Me.Label9.Text = "Dirección :"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(9, 41)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(50, 13)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Nombre :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(6, 18)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(63, 13)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "# Contrato :"
        '
        'CONTRATOLabel1
        '
        Me.CONTRATOLabel1.BackColor = System.Drawing.Color.DarkOrange
        Me.CONTRATOLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATO2BindingSource1, "CONTRATO", True))
        Me.CONTRATOLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONTRATOLabel1.Location = New System.Drawing.Point(75, 18)
        Me.CONTRATOLabel1.Name = "CONTRATOLabel1"
        Me.CONTRATOLabel1.Size = New System.Drawing.Size(162, 25)
        Me.CONTRATOLabel1.TabIndex = 1
        '
        'NOMBRELabel1
        '
        Me.NOMBRELabel1.AutoEllipsis = True
        Me.NOMBRELabel1.BackColor = System.Drawing.Color.DarkOrange
        Me.NOMBRELabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATO2BindingSource1, "NOMBRE", True))
        Me.NOMBRELabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NOMBRELabel1.Location = New System.Drawing.Point(65, 45)
        Me.NOMBRELabel1.Name = "NOMBRELabel1"
        Me.NOMBRELabel1.Size = New System.Drawing.Size(181, 17)
        Me.NOMBRELabel1.TabIndex = 3
        '
        'CALLELabel1
        '
        Me.CALLELabel1.AutoEllipsis = True
        Me.CALLELabel1.BackColor = System.Drawing.Color.DarkOrange
        Me.CALLELabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATO2BindingSource1, "CALLE", True))
        Me.CALLELabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CALLELabel1.Location = New System.Drawing.Point(65, 73)
        Me.CALLELabel1.Name = "CALLELabel1"
        Me.CALLELabel1.Size = New System.Drawing.Size(102, 19)
        Me.CALLELabel1.TabIndex = 5
        '
        'COLONIALabel1
        '
        Me.COLONIALabel1.AutoEllipsis = True
        Me.COLONIALabel1.BackColor = System.Drawing.Color.DarkOrange
        Me.COLONIALabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATO2BindingSource1, "COLONIA", True))
        Me.COLONIALabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.COLONIALabel1.Location = New System.Drawing.Point(44, 96)
        Me.COLONIALabel1.Name = "COLONIALabel1"
        Me.COLONIALabel1.Size = New System.Drawing.Size(89, 25)
        Me.COLONIALabel1.TabIndex = 7
        '
        'NUMEROLabel1
        '
        Me.NUMEROLabel1.AutoEllipsis = True
        Me.NUMEROLabel1.BackColor = System.Drawing.Color.DarkOrange
        Me.NUMEROLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATO2BindingSource1, "NUMERO", True))
        Me.NUMEROLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NUMEROLabel1.Location = New System.Drawing.Point(199, 75)
        Me.NUMEROLabel1.Name = "NUMEROLabel1"
        Me.NUMEROLabel1.Size = New System.Drawing.Size(47, 19)
        Me.NUMEROLabel1.TabIndex = 9
        '
        'CIUDADLabel1
        '
        Me.CIUDADLabel1.AutoEllipsis = True
        Me.CIUDADLabel1.BackColor = System.Drawing.Color.DarkOrange
        Me.CIUDADLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATO2BindingSource, "CIUDAD", True))
        Me.CIUDADLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CIUDADLabel1.Location = New System.Drawing.Point(13, 155)
        Me.CIUDADLabel1.Name = "CIUDADLabel1"
        Me.CIUDADLabel1.Size = New System.Drawing.Size(224, 23)
        Me.CIUDADLabel1.TabIndex = 11
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.DarkOrange
        Me.Button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.Black
        Me.Button8.Location = New System.Drawing.Point(171, 309)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(88, 23)
        Me.Button8.TabIndex = 27
        Me.Button8.Text = "&Buscar"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.DarkOrange
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.ForeColor = System.Drawing.Color.Black
        Me.Button7.Location = New System.Drawing.Point(171, 207)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(88, 23)
        Me.Button7.TabIndex = 23
        Me.Button7.Text = "&Buscar"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(171, 42)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(88, 23)
        Me.Button1.TabIndex = 21
        Me.Button1.Text = "&Buscar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'BCIUDAD
        '
        Me.BCIUDAD.BackColor = System.Drawing.Color.LightGray
        Me.BCIUDAD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BCIUDAD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.BCIUDAD.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BCIUDAD.Location = New System.Drawing.Point(99, 279)
        Me.BCIUDAD.Name = "BCIUDAD"
        Me.BCIUDAD.Size = New System.Drawing.Size(160, 24)
        Me.BCIUDAD.TabIndex = 26
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(103, 261)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(60, 15)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Ciudad :"
        '
        'BNUMERO
        '
        Me.BNUMERO.BackColor = System.Drawing.Color.LightGray
        Me.BNUMERO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BNUMERO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.BNUMERO.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BNUMERO.Location = New System.Drawing.Point(12, 279)
        Me.BNUMERO.Name = "BNUMERO"
        Me.BNUMERO.Size = New System.Drawing.Size(67, 24)
        Me.BNUMERO.TabIndex = 25
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(12, 261)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(66, 15)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Numero :"
        '
        'BCALLE
        '
        Me.BCALLE.BackColor = System.Drawing.Color.LightGray
        Me.BCALLE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BCALLE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.BCALLE.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BCALLE.Location = New System.Drawing.Point(12, 237)
        Me.BCALLE.Name = "BCALLE"
        Me.BCALLE.Size = New System.Drawing.Size(247, 24)
        Me.BCALLE.TabIndex = 24
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(12, 219)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 15)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Calle :"
        '
        'bcONTRATO
        '
        Me.bcONTRATO.BackColor = System.Drawing.Color.LightGray
        Me.bcONTRATO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.bcONTRATO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.bcONTRATO.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bcONTRATO.Location = New System.Drawing.Point(11, 42)
        Me.bcONTRATO.Name = "bcONTRATO"
        Me.bcONTRATO.Size = New System.Drawing.Size(124, 24)
        Me.bcONTRATO.TabIndex = 20
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(8, 24)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(69, 15)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Contrato :"
        '
        'BNOMBRE
        '
        Me.BNOMBRE.BackColor = System.Drawing.Color.LightGray
        Me.BNOMBRE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BNOMBRE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.BNOMBRE.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BNOMBRE.Location = New System.Drawing.Point(10, 87)
        Me.BNOMBRE.Name = "BNOMBRE"
        Me.BNOMBRE.Size = New System.Drawing.Size(250, 24)
        Me.BNOMBRE.TabIndex = 22
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(9, 69)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(66, 15)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Nombre :"
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(5, 5)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(195, 24)
        Me.CMBLabel1.TabIndex = 0
        Me.CMBLabel1.Text = "Buscar Cliente Por :"
        '
        'btnBtelefono
        '
        Me.btnBtelefono.BackColor = System.Drawing.Color.DarkOrange
        Me.btnBtelefono.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBtelefono.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBtelefono.ForeColor = System.Drawing.Color.Black
        Me.btnBtelefono.Location = New System.Drawing.Point(174, 376)
        Me.btnBtelefono.Name = "btnBtelefono"
        Me.btnBtelefono.Size = New System.Drawing.Size(88, 23)
        Me.btnBtelefono.TabIndex = 17
        Me.btnBtelefono.Text = "&Buscar"
        Me.btnBtelefono.UseVisualStyleBackColor = False
        '
        'txtTelefono
        '
        Me.txtTelefono.BackColor = System.Drawing.Color.LightGray
        Me.txtTelefono.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTelefono.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTelefono.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTelefono.Location = New System.Drawing.Point(12, 346)
        Me.txtTelefono.Name = "txtTelefono"
        Me.txtTelefono.Size = New System.Drawing.Size(250, 24)
        Me.txtTelefono.TabIndex = 15
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(9, 325)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(71, 15)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Telefono :"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Location = New System.Drawing.Point(13, 260)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(66, 15)
        Me.Label27.TabIndex = 7
        Me.Label27.Text = "Numero :"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(13, 215)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(48, 15)
        Me.Label28.TabIndex = 5
        Me.Label28.Text = "Calle :"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label31.Location = New System.Drawing.Point(5, 5)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(195, 24)
        Me.Label31.TabIndex = 0
        Me.Label31.Text = "Buscar Cliente Por :"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Chocolate
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CONTRATO1, Me.NOMBRE, Me.CALLE, Me.COLONIA, Me.NUMERO, Me.CIUDAD, Me.TELEFONO, Me.SOLOINTERNET, Me.ESHOTEL})
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(576, 698)
        Me.DataGridView1.TabIndex = 0
        Me.DataGridView1.TabStop = False
        '
        'CONTRATO1
        '
        Me.CONTRATO1.DataPropertyName = "CONTRATO"
        Me.CONTRATO1.HeaderText = "CONTRATO"
        Me.CONTRATO1.Name = "CONTRATO1"
        Me.CONTRATO1.ReadOnly = True
        '
        'NOMBRE
        '
        Me.NOMBRE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.NOMBRE.DataPropertyName = "NOMBRE"
        Me.NOMBRE.HeaderText = "NOMBRE"
        Me.NOMBRE.Name = "NOMBRE"
        Me.NOMBRE.ReadOnly = True
        Me.NOMBRE.Width = 106
        '
        'CALLE
        '
        Me.CALLE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.CALLE.DataPropertyName = "CALLE"
        Me.CALLE.HeaderText = "CALLE"
        Me.CALLE.Name = "CALLE"
        Me.CALLE.ReadOnly = True
        Me.CALLE.Width = 84
        '
        'COLONIA
        '
        Me.COLONIA.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.COLONIA.DataPropertyName = "COLONIA"
        Me.COLONIA.HeaderText = "COLONIA"
        Me.COLONIA.Name = "COLONIA"
        Me.COLONIA.ReadOnly = True
        Me.COLONIA.Width = 106
        '
        'NUMERO
        '
        Me.NUMERO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.NUMERO.DataPropertyName = "NUMERO"
        Me.NUMERO.HeaderText = "NUMERO"
        Me.NUMERO.Name = "NUMERO"
        Me.NUMERO.ReadOnly = True
        Me.NUMERO.Width = 107
        '
        'CIUDAD
        '
        Me.CIUDAD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.CIUDAD.DataPropertyName = "CIUDAD"
        Me.CIUDAD.HeaderText = "CIUDAD"
        Me.CIUDAD.Name = "CIUDAD"
        Me.CIUDAD.ReadOnly = True
        Me.CIUDAD.Width = 95
        '
        'TELEFONO
        '
        Me.TELEFONO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.TELEFONO.DataPropertyName = "TELEFONO"
        Me.TELEFONO.HeaderText = "TELEFONO"
        Me.TELEFONO.Name = "TELEFONO"
        Me.TELEFONO.ReadOnly = True
        Me.TELEFONO.Width = 122
        '
        'SOLOINTERNET
        '
        Me.SOLOINTERNET.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.SOLOINTERNET.DataPropertyName = "SOLOINTERNET"
        Me.SOLOINTERNET.HeaderText = "SOLOINTERNET"
        Me.SOLOINTERNET.Name = "SOLOINTERNET"
        Me.SOLOINTERNET.ReadOnly = True
        Me.SOLOINTERNET.Visible = False
        '
        'ESHOTEL
        '
        Me.ESHOTEL.DataPropertyName = "ESHOTEL"
        Me.ESHOTEL.HeaderText = "ESHOTEL"
        Me.ESHOTEL.Name = "ESHOTEL"
        Me.ESHOTEL.ReadOnly = True
        Me.ESHOTEL.Visible = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Orange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(884, 15)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(120, 36)
        Me.Button2.TabIndex = 0
        Me.Button2.Text = "&NUEVO"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.Orange
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(884, 57)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(120, 36)
        Me.Button3.TabIndex = 11
        Me.Button3.Text = "&CONSULTAR"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.Orange
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.Black
        Me.Button4.Location = New System.Drawing.Point(884, 99)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(120, 36)
        Me.Button4.TabIndex = 12
        Me.Button4.Text = "&MODIFICAR"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(884, 664)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(120, 36)
        Me.Button5.TabIndex = 16
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkOrange
        Me.Button6.Enabled = False
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.Black
        Me.Button6.Location = New System.Drawing.Point(884, 608)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(120, 36)
        Me.Button6.TabIndex = 15
        Me.Button6.Text = "&IMPRIMIR"
        Me.Button6.UseVisualStyleBackColor = False
        Me.Button6.Visible = False
        '
        'Button9
        '
        Me.Button9.BackColor = System.Drawing.Color.DarkOrange
        Me.Button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button9.ForeColor = System.Drawing.Color.Black
        Me.Button9.Location = New System.Drawing.Point(884, 551)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(120, 43)
        Me.Button9.TabIndex = 14
        Me.Button9.Text = "IMPRIMIR &TARJETA"
        Me.Button9.UseVisualStyleBackColor = False
        '
        'Button10
        '
        Me.Button10.BackColor = System.Drawing.Color.DarkOrange
        Me.Button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button10.ForeColor = System.Drawing.Color.Black
        Me.Button10.Location = New System.Drawing.Point(884, 601)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(120, 43)
        Me.Button10.TabIndex = 13
        Me.Button10.Text = "IMPRIMIR C&ONTRATO"
        Me.Button10.UseVisualStyleBackColor = False
        '
        'DataSetEDGAR
        '
        Me.DataSetEDGAR.DataSetName = "DataSetEDGAR"
        Me.DataSetEDGAR.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ChecaServicioContratadoBindingSource
        '
        Me.ChecaServicioContratadoBindingSource.DataMember = "ChecaServicioContratado"
        Me.ChecaServicioContratadoBindingSource.DataSource = Me.DataSetEDGAR
        '
        'ChecaServicioContratadoTableAdapter
        '
        Me.ChecaServicioContratadoTableAdapter.ClearBeforeFill = True
        '
        'DataSetarnoldo
        '
        Me.DataSetarnoldo.DataSetName = "DataSetarnoldo"
        Me.DataSetarnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Selecciona_ImpresoraBindingSource
        '
        Me.Selecciona_ImpresoraBindingSource.DataMember = "Selecciona_Impresora"
        Me.Selecciona_ImpresoraBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Selecciona_ImpresoraTableAdapter
        '
        Me.Selecciona_ImpresoraTableAdapter.ClearBeforeFill = True
        '
        'Selecciona_Impresora_SucursalBindingSource
        '
        Me.Selecciona_Impresora_SucursalBindingSource.DataMember = "Selecciona_Impresora_Sucursal"
        Me.Selecciona_Impresora_SucursalBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Selecciona_Impresora_SucursalTableAdapter
        '
        Me.Selecciona_Impresora_SucursalTableAdapter.ClearBeforeFill = True
        '
        'Dame_clv_EmpresaBindingSource
        '
        Me.Dame_clv_EmpresaBindingSource.DataMember = "Dame_clv_Empresa"
        Me.Dame_clv_EmpresaBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Dame_clv_EmpresaTableAdapter
        '
        Me.Dame_clv_EmpresaTableAdapter.ClearBeforeFill = True
        '
        'Valida_Contrato_ServBindingSource
        '
        Me.Valida_Contrato_ServBindingSource.DataSource = Me.DataSetarnoldo
        Me.Valida_Contrato_ServBindingSource.Position = 0
        '
        'Valida_Contrato_ServTableAdapter
        '
        Me.Valida_Contrato_ServTableAdapter.ClearBeforeFill = True
        '
        'Valida_si_facturoBindingSource
        '
        Me.Valida_si_facturoBindingSource.DataMember = "Valida_si_facturo"
        Me.Valida_si_facturoBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Valida_si_facturoTableAdapter
        '
        Me.Valida_si_facturoTableAdapter.ClearBeforeFill = True
        '
        'Hora_insBindingSource
        '
        Me.Hora_insBindingSource.DataMember = "Hora_ins"
        Me.Hora_insBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Hora_insTableAdapter
        '
        Me.Hora_insTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Comentario2BindingSource
        '
        Me.Inserta_Comentario2BindingSource.DataMember = "Inserta_Comentario2"
        Me.Inserta_Comentario2BindingSource.DataSource = Me.DataSetarnoldo
        '
        'Inserta_Comentario2TableAdapter
        '
        Me.Inserta_Comentario2TableAdapter.ClearBeforeFill = True
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Valida_Factura_contratoBindingSource
        '
        Me.Valida_Factura_contratoBindingSource.DataMember = "Valida_Factura_contrato"
        Me.Valida_Factura_contratoBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Valida_Factura_contratoTableAdapter
        '
        Me.Valida_Factura_contratoTableAdapter.ClearBeforeFill = True
        '
        'Checa_servicios_clienteBindingSource
        '
        Me.Checa_servicios_clienteBindingSource.DataMember = "Checa_servicios_cliente"
        Me.Checa_servicios_clienteBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Checa_servicios_clienteTableAdapter
        '
        Me.Checa_servicios_clienteTableAdapter.ClearBeforeFill = True
        '
        'Button11
        '
        Me.Button11.BackColor = System.Drawing.Color.DarkOrange
        Me.Button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button11.ForeColor = System.Drawing.Color.Black
        Me.Button11.Location = New System.Drawing.Point(884, 177)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(120, 67)
        Me.Button11.TabIndex = 17
        Me.Button11.Text = "IMPRIMIR BOLETA DE PAGO"
        Me.Button11.UseVisualStyleBackColor = False
        '
        'DataSetEdgarRev2
        '
        Me.DataSetEdgarRev2.DataSetName = "DataSetEdgarRev2"
        Me.DataSetEdgarRev2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Dime_Si_BoletasBindingSource
        '
        Me.Dime_Si_BoletasBindingSource.DataMember = "Dime_Si_Boletas"
        Me.Dime_Si_BoletasBindingSource.DataSource = Me.DataSetEdgarRev2
        '
        'Dime_Si_BoletasTableAdapter
        '
        Me.Dime_Si_BoletasTableAdapter.ClearBeforeFill = True
        '
        'Button12
        '
        Me.Button12.BackColor = System.Drawing.Color.DarkOrange
        Me.Button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button12.ForeColor = System.Drawing.Color.Black
        Me.Button12.Location = New System.Drawing.Point(884, 168)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(120, 67)
        Me.Button12.TabIndex = 18
        Me.Button12.Text = "IMPRIMIR ETIQUETA DE PLACA"
        Me.Button12.UseVisualStyleBackColor = False
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric2"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ConGeneralRelBDBindingSource
        '
        Me.ConGeneralRelBDBindingSource.DataMember = "ConGeneralRelBD"
        Me.ConGeneralRelBDBindingSource.DataSource = Me.DataSetEric2
        '
        'ConGeneralRelBDTableAdapter
        '
        Me.ConGeneralRelBDTableAdapter.ClearBeforeFill = True
        '
        'DameSerDELCliBindingSource
        '
        Me.DameSerDELCliBindingSource.DataMember = "dameSerDELCli"
        Me.DameSerDELCliBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRACALLESBindingSource
        '
        Me.MUESTRACALLESBindingSource.DataMember = "MUESTRACALLES"
        Me.MUESTRACALLESBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRACALLESTableAdapter
        '
        Me.MUESTRACALLESTableAdapter.ClearBeforeFill = True
        '
        'DameSerDELCliTableAdapter
        '
        Me.DameSerDELCliTableAdapter.ClearBeforeFill = True
        '
        'BUSCLIPORCONTRATO2TableAdapter
        '
        Me.BUSCLIPORCONTRATO2TableAdapter.ClearBeforeFill = True
        '
        'DamePermisosFormBindingSource
        '
        Me.DamePermisosFormBindingSource.DataMember = "DamePermisosForm"
        Me.DamePermisosFormBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'DamePermisosFormTableAdapter
        '
        Me.DamePermisosFormTableAdapter.ClearBeforeFill = True
        '
        'Button13
        '
        Me.Button13.BackColor = System.Drawing.Color.DarkOrange
        Me.Button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button13.ForeColor = System.Drawing.Color.Black
        Me.Button13.Location = New System.Drawing.Point(884, 169)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(120, 67)
        Me.Button13.TabIndex = 19
        Me.Button13.Text = "IMPRIMIR ESTADO DE CUENTA"
        Me.Button13.UseVisualStyleBackColor = False
        Me.Button13.Visible = False
        '
        'BUSCLIPORCONTRATO2TableAdapter1
        '
        Me.BUSCLIPORCONTRATO2TableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter5
        '
        Me.Muestra_ServiciosDigitalesTableAdapter5.ClearBeforeFill = True
        '
        'Button15
        '
        Me.Button15.BackColor = System.Drawing.Color.DarkOrange
        Me.Button15.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button15.ForeColor = System.Drawing.Color.Black
        Me.Button15.Location = New System.Drawing.Point(884, 479)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(120, 67)
        Me.Button15.TabIndex = 22
        Me.Button15.Text = "IMPRIMIR TARJETAS MASIVAS"
        Me.Button15.UseVisualStyleBackColor = False
        '
        'BtnHistorialTarjetas
        '
        Me.BtnHistorialTarjetas.BackColor = System.Drawing.Color.DarkOrange
        Me.BtnHistorialTarjetas.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnHistorialTarjetas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnHistorialTarjetas.ForeColor = System.Drawing.Color.Black
        Me.BtnHistorialTarjetas.Location = New System.Drawing.Point(884, 318)
        Me.BtnHistorialTarjetas.Name = "BtnHistorialTarjetas"
        Me.BtnHistorialTarjetas.Size = New System.Drawing.Size(120, 84)
        Me.BtnHistorialTarjetas.TabIndex = 21
        Me.BtnHistorialTarjetas.Text = "HISTORIAL DE TARJETAS IMPRESAS"
        Me.BtnHistorialTarjetas.UseVisualStyleBackColor = False
        '
        'Muestra_ServiciosDigitalesTableAdapter6
        '
        Me.Muestra_ServiciosDigitalesTableAdapter6.ClearBeforeFill = True
        '
        'Button16
        '
        Me.Button16.BackColor = System.Drawing.Color.DarkOrange
        Me.Button16.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button16.ForeColor = System.Drawing.Color.Black
        Me.Button16.Location = New System.Drawing.Point(884, 406)
        Me.Button16.Name = "Button16"
        Me.Button16.Size = New System.Drawing.Size(120, 67)
        Me.Button16.TabIndex = 23
        Me.Button16.Text = "IMPRIMIR TARJETAS LAYOUT"
        Me.Button16.UseVisualStyleBackColor = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'Muestra_ServiciosDigitalesTableAdapter7
        '
        Me.Muestra_ServiciosDigitalesTableAdapter7.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter8
        '
        Me.Muestra_ServiciosDigitalesTableAdapter8.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter9
        '
        Me.Muestra_ServiciosDigitalesTableAdapter9.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter10
        '
        Me.Muestra_ServiciosDigitalesTableAdapter10.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter11
        '
        Me.Muestra_ServiciosDigitalesTableAdapter11.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter12
        '
        Me.Muestra_ServiciosDigitalesTableAdapter12.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter13
        '
        Me.Muestra_ServiciosDigitalesTableAdapter13.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter14
        '
        Me.Muestra_ServiciosDigitalesTableAdapter14.ClearBeforeFill = True
        '
        'BrwClientes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1021, 734)
        Me.Controls.Add(Me.Button16)
        Me.Controls.Add(Me.BtnHistorialTarjetas)
        Me.Controls.Add(Me.Button15)
        Me.Controls.Add(Me.Button13)
        Me.Controls.Add(Me.Button12)
        Me.Controls.Add(Me.Button10)
        Me.Controls.Add(Me.Button9)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Button11)
        Me.Name = "BrwClientes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Buscar Cliente"
        Me.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.BUSCLIPORCONTRATO2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BUSCLIPORCONTRATO2BindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChecaServicioContratadoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Selecciona_ImpresoraBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Selecciona_Impresora_SucursalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_clv_EmpresaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Valida_Contrato_ServBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Valida_si_facturoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Hora_insBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Comentario2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Valida_Factura_contratoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Checa_servicios_clienteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dime_Si_BoletasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConGeneralRelBDBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameSerDELCliBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACALLESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DamePermisosFormBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents BNOMBRE As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents BCIUDAD As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents BNUMERO As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents BCALLE As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents bcONTRATO As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents MUESTRACALLESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACALLESTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACALLESTableAdapter
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents CONTRATOLabel1 As System.Windows.Forms.Label
    Friend WithEvents NOMBRELabel1 As System.Windows.Forms.Label
    Friend WithEvents CALLELabel1 As System.Windows.Forms.Label
    Friend WithEvents COLONIALabel1 As System.Windows.Forms.Label
    Friend WithEvents NUMEROLabel1 As System.Windows.Forms.Label
    Friend WithEvents CIUDADLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents ESHOTELCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents SOLOINTERNETCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents DameSerDELCliBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameSerDELCliTableAdapter As sofTV.NewSofTvDataSetTableAdapters.dameSerDELCliTableAdapter
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents BUSCLIPORCONTRATO2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BUSCLIPORCONTRATO2TableAdapter As sofTV.NewSofTvDataSetTableAdapters.BUSCLIPORCONTRATO2TableAdapter
    Friend WithEvents DamePermisosFormBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DamePermisosFormTableAdapter As sofTV.NewSofTvDataSetTableAdapters.DamePermisosFormTableAdapter
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents DataSetEDGAR As sofTV.DataSetEDGAR
    Friend WithEvents ChecaServicioContratadoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ChecaServicioContratadoTableAdapter As sofTV.DataSetEDGARTableAdapters.ChecaServicioContratadoTableAdapter
    Friend WithEvents DataSetarnoldo As sofTV.DataSetarnoldo
    Friend WithEvents Selecciona_ImpresoraBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Selecciona_ImpresoraTableAdapter As sofTV.DataSetarnoldoTableAdapters.Selecciona_ImpresoraTableAdapter
    Friend WithEvents Selecciona_Impresora_SucursalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Selecciona_Impresora_SucursalTableAdapter As sofTV.DataSetarnoldoTableAdapters.Selecciona_Impresora_SucursalTableAdapter
    Friend WithEvents Dame_clv_EmpresaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_clv_EmpresaTableAdapter As sofTV.DataSetarnoldoTableAdapters.Dame_clv_EmpresaTableAdapter
    Friend WithEvents Valida_Contrato_ServBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Valida_Contrato_ServTableAdapter As sofTV.DataSetarnoldoTableAdapters.Valida_Contrato_ServTableAdapter
    Friend WithEvents Valida_si_facturoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Valida_si_facturoTableAdapter As sofTV.DataSetarnoldoTableAdapters.Valida_si_facturoTableAdapter
    Friend WithEvents Hora_insBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Hora_insTableAdapter As sofTV.DataSetarnoldoTableAdapters.Hora_insTableAdapter
    Friend WithEvents Inserta_Comentario2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Comentario2TableAdapter As sofTV.DataSetarnoldoTableAdapters.Inserta_Comentario2TableAdapter
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents Valida_Factura_contratoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Valida_Factura_contratoTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Valida_Factura_contratoTableAdapter
    Friend WithEvents Checa_servicios_clienteBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Checa_servicios_clienteTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Checa_servicios_clienteTableAdapter
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents DataSetEdgarRev2 As sofTV.DataSetEdgarRev2
    Friend WithEvents Dime_Si_BoletasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dime_Si_BoletasTableAdapter As sofTV.DataSetEdgarRev2TableAdapters.Dime_Si_BoletasTableAdapter
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric2
    Friend WithEvents ConGeneralRelBDBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConGeneralRelBDTableAdapter As sofTV.DataSetEric2TableAdapters.ConGeneralRelBDTableAdapter
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents BUSCLIPORCONTRATO2BindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents Procedimientosarnoldo4 As sofTV.Procedimientosarnoldo4
    Friend WithEvents BUSCLIPORCONTRATO2TableAdapter1 As sofTV.Procedimientosarnoldo4TableAdapters.BUSCLIPORCONTRATO2TableAdapter
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnBtelefono As System.Windows.Forms.Button
    Friend WithEvents txtTelefono As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents GDCONTRATO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CONTRATO1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NOMBRE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CALLE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents COLONIA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NUMERO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CIUDAD As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TELEFONO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SOLOINTERNET As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ESHOTEL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AMaternoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents APaternoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents TxtTarjeta As System.Windows.Forms.TextBox
    Friend WithEvents TxtSetUpBox As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Button14 As System.Windows.Forms.Button
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter5 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Button15 As System.Windows.Forms.Button
    Friend WithEvents BtnHistorialTarjetas As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter6 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Button16 As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter7 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter8 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter9 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter10 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter11 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter12 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter13 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter14 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
