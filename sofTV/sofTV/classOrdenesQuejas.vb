﻿Public Class classOrdenesQuejas
    Dim ordenQueja As BaseIII

#Region "PROPIEDADES"

    Private _contrato As Long
    Public Property contrato As Long
        Get
            Return _contrato
        End Get
        Set(value As Long)
            _contrato = value
        End Set
    End Property
#End Region

#Region "CONSTRUCTORES"
    Public Sub New()
        _contrato = 0
    End Sub
#End Region

#Region "MÉTODOS"
    Public Function uspChecaSiClienteActivo() As Boolean
        Try
            ordenQueja = New BaseIII
            ordenQueja.limpiaParametros()
            ordenQueja.CreateMyParameter("@contrato", SqlDbType.BigInt, CObj(_contrato))
            ordenQueja.CreateMyParameter("@bndInstalado", ParameterDirection.Output, SqlDbType.Bit)
            ordenQueja.ProcedimientoOutPut("uspChecaSiClienteActivo")

            uspChecaSiClienteActivo = CBool(ordenQueja.dicoPar("@bndInstalado").ToString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region
End Class
