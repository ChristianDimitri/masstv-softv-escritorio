﻿Imports System.Collections.Generic
Imports CrystalDecisions.CrystalReports.Engine
Imports System.IO

Public Class FrmImpresionMasivaTarjetas
    Dim obj_Impresora As Object
    Dim contrato1 As Boolean
    Dim contrato2 As Boolean
    Private LocOpTarjetas As Integer = 0
    Private LocTipoTarjeta As String = Nothing
    Dim dsrpt As DataSet
    Public DstReporte As New DataSet
    Public DstReportePrueba As New DataSet
    Public DstReporteVerdes As New DataSet
    Public DstReporteRojas As New DataSet
    Public DstReporteRojasTMP As New DataTable
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        GloClv_TipSer = 1000
        GLOCONTRATOSEL = 0
        contrato1 = True
        FrmSelCliente.Show()
    End Sub

    Private Sub FrmImpresionMasivaTarjetas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        GloClv_TipSer = 1000
        GLOCONTRATOSEL = 0
        contrato2 = True
        FrmSelCliente.Show()
    End Sub

    Private Sub FrmImpresionMasivaTarjetas_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        If GLOCONTRATOSEL > 0 Then
            If contrato1 = True Then
                contrato1 = False
                Me.TextBox1.Text = GLOCONTRATOSEL
            ElseIf contrato2 = True Then
                contrato2 = False
                Me.TextBox2.Text = GLOCONTRATOSEL
            End If
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()

    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        If CInt(Me.TextBox1.Text) > CInt(Me.TextBox2.Text) Then
            MsgBox("El contrato inicial debe ser menor al contrato final", MsgBoxStyle.Information, "Tarjetas Masivas")
            Exit Sub
        End If

        Dim DT As New DataTable
        Dim Faltantes As Integer
        Dim I, X, Y, Z, cont, A, B, C, CONTRATOTARJETAS As Integer
        Dim resp As Integer = 0
        Dim fila As Data.DataRow
        Dim RutaPDF As String
        obj_Impresora = CreateObject("WScript.Network")
        obj_Impresora.setdefaultprinter("Tarjetas")

        If FolderBrowserDialog1.ShowDialog = Windows.Forms.DialogResult.Cancel Then
            Exit Sub
        End If

        RutaPDF = FolderBrowserDialog1.SelectedPath


        DstReporteVerdes = New DataSet
        DstReporte = New DataSet
        DstReporteRojas = New DataSet
        DstReporteRojasTMP = New DataTable
        For I = CInt(Me.TextBox1.Text) To CInt(Me.TextBox2.Text)
            LocOpTarjetas = 1
            LocTipoTarjeta = "P"
            GenerarEstados(I, 1)
            'LocOpTarjetas = 1
            'LocTipoTarjeta = "R"
            'GenerarEstados(I, 1)
        Next

        Faltantes = 24 - (DstReporte.Tables(0).Rows.Count Mod 24)
        If Faltantes = 24 Then
            Faltantes = 0
        End If

        Dim contador1 As Integer

        For contador1 = 0 To Faltantes
            dsrpt.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, 0)
            BaseII.CreateMyParameter("@Op", SqlDbType.Int, 1)
            Dim listatablas3 As New List(Of String)
            listatablas3.Add("PREFacturas_EDO_SOFTV")
            dsrpt = BaseII.ConsultaDS("Consulta_Codigo_OXXO_PorContrato", listatablas3)
            DstReporte.Merge(dsrpt)
        Next

        If DstReporte.Tables.Count = 0 Then
            MsgBox("No se pueden imprimir las Tarjetas, los contratos tienen estatus diferente a instalado o es cortesía")
            Exit Sub
        End If

        Dim X1, X2, X3, NUM, X5 As Integer

        DstReporteVerdes.Merge(DstReporte)
        DstReporteVerdes.Clear()
        'X2 = DstReporte.Tables(0).Rows.Count / 24 
        X2 = DstReporte.Tables(0).Rows.Count
        'NUM = 0
        'For X1 = 0 To X2 - 1
        '    X5 = NUM
        'For X3 = 0 To 23
        For X3 = 0 To X2 - 1
            DstReporteVerdes.Tables(0).Rows.Add(DstReporte.Tables(0).Rows(X5).ItemArray)
            X5 = X5 + 1
            'X5 = X5 + X2
        Next
        'NUM = NUM + 1
        'Next





        Dim frmic As New FrmImprimirCentralizada()
        Dim rd As New ReportDocument()
        rd.Load(RutaReportes + "\rptOxxoSaltilloMasivoVerdes" + ".rpt")

        rd.SetDataSource(DstReporteVerdes)

        rd.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, RutaPDF + "\Tarjetas_OXXO_PrePago.pdf")

        DT = New DataTable
        DT.Columns.Add("contratos")
        'DT.Merge(DstReporte.Tables(0))


        Dim BND As Boolean = False

        C = 0
        While (BND = False)
            CONTRATOTARJETAS = DstReporteVerdes.Tables(0).Rows(C)(1).ToString
            DT.Rows.Add(New Object() {CONTRATOTARJETAS})
            If C = DstReporteVerdes.Tables(0).Rows.Count - 1 Then
                A = DstReporteVerdes.Tables(0).Rows.Count
                resp = A Mod 3
                If resp <> 0 Then
                    A = A + 1
                    resp = A Mod 3
                    If resp <> 0 Then
                        DT.Rows.Add(New Object() {0})
                    End If
                    DT.Rows.Add(New Object() {0})
                    'DT.Rows.Add(New Object() {DstReporte.Tables(0).Rows(C + 1)(1).ToString})
                    BND = True
                Else
                    'DT.Rows.Add(New Object() {DstReporte.Tables(0).Rows(C + 1)(1).ToString})
                    BND = True
                End If
            End If
            C = C + 1
        End While


        frmic.rd = rd
        frmic.ShowDialog()
        DstReporte = New DataSet

        'Comentada impresion masiva Tarjetas Rojas

        'For I = 0 To DT.Rows.Count - 1
        '    'LocOpTarjetas = 0
        '    'LocTipoTarjeta = "V"
        '    'GenerarEstados(I, 0)
        '    LocOpTarjetas = 0
        '    LocTipoTarjeta = "N"
        '    GenerarEstados(CLng(DT.Rows(I)(0).ToString), 0)
        'Next
        'DstReporteRojas.Merge(DstReporte)
        'DstReporteRojas.Clear()
        'cont = DstReporte.Tables(0).Rows.Count / 3
        'X = 2
        'For I = 0 To cont
        '    Y = X
        '    For Z = 0 To 2
        '        If Y < DstReporte.Tables(0).Rows.Count Then
        '            DstReporteRojas.Tables(0).Rows.Add(DstReporte.Tables(0).Rows(Y).ItemArray)
        '        End If
        '        Y = Y - 1
        '    Next
        '    X = X + 3
        'Next

        'rd = New ReportDocument()
        'rd.Load(RutaReportes + "\rptOxxoSaltilloMasivoRojas" + ".rpt")
        'rd.SetDataSource(DstReporteRojas)

        'rd.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, RutaPDF + "\Tarjetas_OXXO_PagoNormal.pdf")

        'frmic.rd = rd
        'frmic.ShowDialog()

        'Comentada impresion masiva Tarjetas Rojas

        RutaPDF = ""
        Me.Close()
        'DataTableLeer2.Rows.Add(DataTableLeer.Rows(0).ItemArray)
    End Sub
    Private Sub UspTarjetasOXXO(ByVal PRMCLVRECIBO As Long, ByVal PRMCONTRATO As Long, ByVal PRMFECHAIMPRESION As Date, ByVal PRMIMPORTE As Decimal, ByVal PRMCANCELADA As Integer, _
                          ByVal PRMTXT As String, ByVal PRMNOMBRE As String, ByVal PRMDIRECCION As String, ByVal PRMCOLONIA As String, ByVal PRMUNICIPIO As String, _
                          ByVal PRMESTADO As String, ByVal PRMCP As String, ByVal PRMENTRECALLES As String, ByVal PRMTELEFONO As String, ByVal PRMCANTIDAD As String, _
                          ByVal PRMTIPO As String, ByVal PRMOP As Integer)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@ClvRECIBO", SqlDbType.BigInt, PRMCLVRECIBO)
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, PRMCONTRATO)
        BaseII.CreateMyParameter("@FechaImpresion", SqlDbType.Date, PRMFECHAIMPRESION)
        BaseII.CreateMyParameter("@importe", SqlDbType.Decimal, PRMIMPORTE)
        BaseII.CreateMyParameter("@cancelada", SqlDbType.Int, PRMCANCELADA)
        BaseII.CreateMyParameter("@TXT", SqlDbType.VarChar, PRMTXT, 150)
        BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, PRMNOMBRE, 250)
        BaseII.CreateMyParameter("@Direccion", SqlDbType.VarChar, PRMDIRECCION, 250)
        BaseII.CreateMyParameter("@Colonia", SqlDbType.VarChar, PRMCOLONIA, 250)
        BaseII.CreateMyParameter("@Municipio", SqlDbType.VarChar, PRMUNICIPIO, 250)
        BaseII.CreateMyParameter("@Estado", SqlDbType.VarChar, PRMESTADO, 250)
        BaseII.CreateMyParameter("@CP", SqlDbType.VarChar, PRMCP, 250)
        BaseII.CreateMyParameter("@EntreCalle", SqlDbType.VarChar, PRMENTRECALLES, 250)
        BaseII.CreateMyParameter("@Telefono", SqlDbType.VarChar, PRMTELEFONO, 150)
        BaseII.CreateMyParameter("@CantidadLetra", SqlDbType.VarChar, PRMCANTIDAD, 250)
        BaseII.CreateMyParameter("@Tipo", SqlDbType.VarChar, PRMTIPO, 1)
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, PRMOP)
        BaseII.Inserta("UspTarjetasOXXO")
    End Sub

    Public Sub GenerarEstados(ByVal oContrato As Long, ByVal prmOp As Integer)
        Try

            Dim max As Long
            Dim LocError As Integer
            Dim locimporte As Decimal
            Dim DT As New DataTable
            Dim DS As New DataSet
            Dim CORTESIA As New DataTable

            CORTESIA = UspValidarClienteConCortesia(oContrato)
            If CORTESIA.Rows(0)(0).ToString = "1" Then
                Exit Sub
            End If

            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, oContrato)
            BaseII.CreateMyParameter("@Op", SqlDbType.Int, prmOp)
            Dim listatablas As New List(Of String)
            listatablas.Add("PREFacturas_EDO_SOFTV")
            DS = BaseII.ConsultaDS("Genera_Codigo_OXXO_PorContrato", listatablas)
            Dim rd As ReportDocument
            Dim i As Long = 0
            Dim contador As Integer = 1
            max = DS.Tables(0).Rows.Count
            Dim prefijo As String = ""
            Dim Txt As String = Nothing

            If max = 0 Then
                Exit Sub
                'Else
                '    If Len(DS.Tables(0).Rows(0)(4).ToString) = 0 Then
                '        Exit Sub
                '    End If
            End If
            'If Len(DS.Tables(0).Rows(0)(8).ToString) > 0 Then
            '    LocError = 1
            'Else
            '    LocError = 0
            'End If
            'If LocError = 1 Then
            '    Exit Sub
            'End If

            For Each dr As DataRow In DS.Tables(0).Rows
                i = Long.Parse(dr(0).ToString())
                Txt = dr(10).ToString()
                If Len(Txt) > 0 Then
                    AgregaImgCodeBarEstadoDeCuentaTV(oContrato, Txt, LocOpTarjetas)
                End If
                'rd = New ReportDocument()
                'rd.Load(RutaReportes + "\rptEstadoDeCuentaTV.rpt")

                'dsrpt.Clear()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, oContrato)
                BaseII.CreateMyParameter("@Op", SqlDbType.Int, prmOp)
                Dim listatablas2 As New List(Of String)
                listatablas2.Add("PREFacturas_EDO_SOFTV")
                dsrpt = BaseII.ConsultaDS("Consulta_Codigo_OXXO_PorContrato", listatablas2)
                'rd.SetDataSource(dsrpt)
                DT = dsrpt.Tables(0)
                If LocTipoTarjeta = "P" Then
                    If DT.Rows(0)(31).ToString <> "" Then
                        locimporte = CDec(DT.Rows(0)(31).ToString)
                    Else
                        locimporte = 0
                    End If
                Else
                    locimporte = CDec(DT.Rows(0)(4).ToString)
                End If
                UspTarjetasOXXO(CInt(DT.Rows(0)(0).ToString), CInt(DT.Rows(0)(1).ToString), CDate(DT.Rows(0)(2).ToString), locimporte, 0, DT.Rows(0)(10).ToString, DT.Rows(0)(14).ToString, DT.Rows(0)(16).ToString, DT.Rows(0)(17).ToString, DT.Rows(0)(18).ToString, DT.Rows(0)(19).ToString, DT.Rows(0)(20).ToString, DT.Rows(0)(21).ToString, DT.Rows(0)(23).ToString, DT.Rows(0)(27).ToString, LocTipoTarjeta, 1)

                DstReporte.Merge(dsrpt)

                'BaseII.llamarReporteCentralizado(RutaReportes + "\rptOxxoSaltillo", dsrpt)


                'If chbGenerarConsecutivo.CheckState = CheckState.Checked Then
                '    rd.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, txtDirectorio.Text + "\" + contador.ToString() + "_" + i.ToString() + ".pdf")
                'Else
                '    rd.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, txtDirectorio.Text + "\" + i.ToString() + ".pdf")
                'End If

                contador = contador + 1
                GC.Collect()
            Next
        Catch ex As Exception
            Throw ex
        End Try

    End Sub
End Class