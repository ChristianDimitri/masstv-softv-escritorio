﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwtblRelVendedoresSeries
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.bnImprimirFolios = New System.Windows.Forms.Button()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.CMBlabel100 = New System.Windows.Forms.Label()
        Me.bnBuscarNombre = New System.Windows.Forms.Button()
        Me.tbNombre = New System.Windows.Forms.TextBox()
        Me.bnModificar = New System.Windows.Forms.Button()
        Me.bnConsultar = New System.Windows.Forms.Button()
        Me.bnNuevo = New System.Windows.Forms.Button()
        Me.CMBlabel2 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.dgvVendedores = New System.Windows.Forms.DataGridView()
        Me.Id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgvVendedores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'bnImprimirFolios
        '
        Me.bnImprimirFolios.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnImprimirFolios.Location = New System.Drawing.Point(855, 153)
        Me.bnImprimirFolios.Name = "bnImprimirFolios"
        Me.bnImprimirFolios.Size = New System.Drawing.Size(136, 72)
        Me.bnImprimirFolios.TabIndex = 45
        Me.bnImprimirFolios.Text = "&IMPRIMIR FOLIOS"
        Me.bnImprimirFolios.UseVisualStyleBackColor = True
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(855, 684)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(136, 36)
        Me.bnSalir.TabIndex = 44
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'CMBlabel100
        '
        Me.CMBlabel100.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlabel100.Location = New System.Drawing.Point(18, 11)
        Me.CMBlabel100.Name = "CMBlabel100"
        Me.CMBlabel100.Size = New System.Drawing.Size(227, 53)
        Me.CMBlabel100.TabIndex = 39
        Me.CMBlabel100.Text = "Buscar Vendedor:"
        '
        'bnBuscarNombre
        '
        Me.bnBuscarNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnBuscarNombre.Location = New System.Drawing.Point(21, 115)
        Me.bnBuscarNombre.Name = "bnBuscarNombre"
        Me.bnBuscarNombre.Size = New System.Drawing.Size(75, 23)
        Me.bnBuscarNombre.TabIndex = 37
        Me.bnBuscarNombre.Text = "&Buscar"
        Me.bnBuscarNombre.UseVisualStyleBackColor = True
        '
        'tbNombre
        '
        Me.tbNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNombre.Location = New System.Drawing.Point(21, 88)
        Me.tbNombre.MaxLength = 750
        Me.tbNombre.Name = "tbNombre"
        Me.tbNombre.Size = New System.Drawing.Size(223, 21)
        Me.tbNombre.TabIndex = 38
        '
        'bnModificar
        '
        Me.bnModificar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnModificar.Location = New System.Drawing.Point(855, 111)
        Me.bnModificar.Name = "bnModificar"
        Me.bnModificar.Size = New System.Drawing.Size(136, 36)
        Me.bnModificar.TabIndex = 43
        Me.bnModificar.Text = "&MODIFICAR"
        Me.bnModificar.UseVisualStyleBackColor = True
        '
        'bnConsultar
        '
        Me.bnConsultar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnConsultar.Location = New System.Drawing.Point(855, 70)
        Me.bnConsultar.Name = "bnConsultar"
        Me.bnConsultar.Size = New System.Drawing.Size(136, 36)
        Me.bnConsultar.TabIndex = 42
        Me.bnConsultar.Text = "&CONSULTAR"
        Me.bnConsultar.UseVisualStyleBackColor = True
        '
        'bnNuevo
        '
        Me.bnNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnNuevo.Location = New System.Drawing.Point(855, 28)
        Me.bnNuevo.Name = "bnNuevo"
        Me.bnNuevo.Size = New System.Drawing.Size(136, 36)
        Me.bnNuevo.TabIndex = 41
        Me.bnNuevo.Text = "&NUEVO"
        Me.bnNuevo.UseVisualStyleBackColor = True
        '
        'CMBlabel2
        '
        Me.CMBlabel2.AutoSize = True
        Me.CMBlabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlabel2.Location = New System.Drawing.Point(18, 70)
        Me.CMBlabel2.Name = "CMBlabel2"
        Me.CMBlabel2.Size = New System.Drawing.Size(68, 15)
        Me.CMBlabel2.TabIndex = 36
        Me.CMBlabel2.Text = "Vendedor"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(855, 231)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 72)
        Me.Button1.TabIndex = 46
        Me.Button1.Text = "&CANCELAR FOLIOS"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'dgvVendedores
        '
        Me.dgvVendedores.AllowUserToAddRows = False
        Me.dgvVendedores.AllowUserToDeleteRows = False
        Me.dgvVendedores.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvVendedores.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvVendedores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvVendedores.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Id, Me.Nombre})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvVendedores.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgvVendedores.Location = New System.Drawing.Point(263, 28)
        Me.dgvVendedores.Name = "dgvVendedores"
        Me.dgvVendedores.ReadOnly = True
        Me.dgvVendedores.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvVendedores.Size = New System.Drawing.Size(568, 692)
        Me.dgvVendedores.TabIndex = 40
        '
        'Id
        '
        Me.Id.DataPropertyName = "Id"
        Me.Id.HeaderText = "Id"
        Me.Id.Name = "Id"
        Me.Id.ReadOnly = True
        Me.Id.Visible = False
        '
        'Nombre
        '
        Me.Nombre.DataPropertyName = "Nombre"
        Me.Nombre.HeaderText = "Vendedor"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.ReadOnly = True
        Me.Nombre.Width = 350
        '
        'BrwtblRelVendedoresSeries
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1008, 730)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.bnImprimirFolios)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.CMBlabel100)
        Me.Controls.Add(Me.bnBuscarNombre)
        Me.Controls.Add(Me.tbNombre)
        Me.Controls.Add(Me.bnModificar)
        Me.Controls.Add(Me.bnConsultar)
        Me.Controls.Add(Me.bnNuevo)
        Me.Controls.Add(Me.dgvVendedores)
        Me.Controls.Add(Me.CMBlabel2)
        Me.Name = "BrwtblRelVendedoresSeries"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "BrwtblRelVendedoresSeries"
        CType(Me.dgvVendedores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents bnImprimirFolios As System.Windows.Forms.Button
    Private WithEvents bnSalir As System.Windows.Forms.Button
    Private WithEvents CMBlabel100 As System.Windows.Forms.Label
    Private WithEvents bnBuscarNombre As System.Windows.Forms.Button
    Private WithEvents tbNombre As System.Windows.Forms.TextBox
    Private WithEvents bnModificar As System.Windows.Forms.Button
    Private WithEvents bnConsultar As System.Windows.Forms.Button
    Private WithEvents bnNuevo As System.Windows.Forms.Button
    Private WithEvents CMBlabel2 As System.Windows.Forms.Label
    Private WithEvents Button1 As System.Windows.Forms.Button
    Private WithEvents dgvVendedores As System.Windows.Forms.DataGridView
    Private WithEvents Id As System.Windows.Forms.DataGridViewTextBoxColumn
    Private WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
