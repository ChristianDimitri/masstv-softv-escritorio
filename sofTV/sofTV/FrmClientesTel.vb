Imports System.Data.SqlClient
Public Class FrmClientesTel
    Dim contador As Integer
    Dim conlidia As New SqlClient.SqlConnection(MiConexion)
    Private Sub Checa_Forzoso(ByVal Clv_unicanet As Long, ByVal Contrato As Long)
        Dim Con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        'Contrato
        If Contrato = Nothing Then Contrato = 0
        Try
            'Muestra_Plazo_Forzozo (@contrato bigint,@Clv_unicanet bigint,@Plazo varchar(10) output)
            cmd = New SqlCommand()
            Con.Open()
            With cmd
                .CommandText = "Muestra_Plazo_Forzozo"
                .Connection = Con
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@contrato", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = Contrato
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@Clv_unicanet", SqlDbType.BigInt)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = Clv_unicanet
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@Plazo", SqlDbType.VarChar, 10)
                prm2.Direction = ParameterDirection.Output
                prm2.Value = 0
                .Parameters.Add(prm2)

                Dim ia As Integer = .ExecuteNonQuery()

                Me.LblPlazoForzoso1.Text = prm2.Value

            End With
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub GuardarToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GuardarToolStripButton.Click
        If Contrato > 0 Then
            Me.Panel3.Show()
            Me.Panel4.Hide()
            Me.Panel5.Hide()
            Me.BindingNavigator2.Enabled = True
            optTel = 2
            FrmSeleccionaTel.Show()
        End If
    End Sub
   

    Private Sub GuardarToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GuardarToolStripButton2.Click
    
        If Me.ComboBox1.Text = "Baja" Then
            If IsDate(Me.BajaTelTextBox1.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Baja", MsgBoxStyle.Information, "Atenci�n")
            ElseIf (CDate(Me.ActivacionTextBox2.Text) <= CDate(Me.BajaTelTextBox1.Text)) And (CDate(Me.BajaTelTextBox1.Text) <= CDate(fecha_Hoy)) Then
                Guardar_ClientesTel(2)
            Else
                MsgBox("La Fecha de Baja debe Encontrarse entre la Fecha de Activaci�n y la Fecha Actual", MsgBoxStyle.Exclamation, "Atenci�n")
            End If
        End If

        If Me.ComboBox1.Text = "Instalado" Then
            If IsDate(Me.ActivacionTextBox2.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Activaci�n", MsgBoxStyle.Information, "Atenci�n")
            Else
                Guardar_ClientesTel(2)
            End If
        End If
        If Me.ComboBox1.Text = "Por Asignar" Then
            If IsDate(Me.ActivacionTextBox2.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Activaci�n", MsgBoxStyle.Information, "Atenci�n")
            Else
                Guardar_ClientesTel(2)
            End If
        End If


    End Sub
    Public Sub Guardar_ClientesTel(ByVal opt As Integer)
        Dim ConTel As New SqlClient.SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand
        Dim Nombre As String = Nothing
        Dim ccNet As Integer = Nothing
        If opt = 1 Then
            Nombre = "Nuevo_ClientesTel"
        ElseIf opt = 2 Then
            Nombre = "Modifica_ClientesTel"
        End If
        ConTel.Open()
        With cmd
            '(@contratonet bigint output,@contrato bigint,@no_extensiones int,@status varchar(1),@clv_cablemodem bigint,
            '@clv_telefono bigint,@fecha_Activacion datetime,@fecha_Suspension datetime,@fecha_baja datetime,@Obs varchar(max))
            .CommandText = Nombre
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = ConTel
            Dim prm As New SqlParameter("@ContratoNet", SqlDbType.BigInt)
            Dim prm2 As New SqlParameter("@Contrato", SqlDbType.BigInt)
            prm2.Direction = ParameterDirection.Input
            Dim prm3 As New SqlParameter("@No_extensiones", SqlDbType.Int)
            prm3.Direction = ParameterDirection.Input
            Dim prm4 As New SqlParameter("@Status", SqlDbType.VarChar, 1)
            prm4.Direction = ParameterDirection.Input
            Dim prm5 As New SqlParameter("@Clv_Cablemodem", SqlDbType.BigInt)
            prm5.Direction = ParameterDirection.Input
            Dim prm6 As New SqlParameter("@Clv_Telefono", SqlDbType.BigInt)
            prm6.Direction = ParameterDirection.Input
            Dim prm7 As New SqlParameter("@Fecha_Activacion", SqlDbType.DateTime)
            prm7.Direction = ParameterDirection.Input
            Dim prm8 As New SqlParameter("@Fecha_Suspension", SqlDbType.DateTime)
            prm8.Direction = ParameterDirection.Input
            Dim prm9 As New SqlParameter("@Fecha_Baja", SqlDbType.DateTime)
            prm9.Direction = ParameterDirection.Input
            Dim prm10 As New SqlParameter("@Obs", SqlDbType.VarChar, 9000)
            prm10.Direction = ParameterDirection.Input

            If opt = 1 Then
                prm.Direction = ParameterDirection.Output
                prm.Value = 0
                prm2.Value = Contrato
                prm3.Value = 0
                prm4.Value = "P"
                prm5.Value = Clv_EquipoTel
                prm6.Value = Clv_NumTelefonico
                prm7.Value = CDate(fecha_Hoy)
                prm8.Value = CDate("01/01/1900")
                prm9.Value = CDate("01/01/1900")
                prm10.Value = ""
            ElseIf opt = 2 Then
                prm.Direction = ParameterDirection.Input
                prm.Value = Me.ContratonetTextBox.Text
                prm2.Value = Contrato
                prm3.Value = Me.No_extensionesNumericUpDown.Value
                prm4.Value = Me.ComboBox1.SelectedValue
                prm5.Value = Me.Clv_cablemodemTextBox.Text
                prm6.Value = 0
                If Me.ActivacionTextBox2.Text = "  /  /" Then
                    prm7.Value = CDate("01/01/1900")
                Else
                    prm7.Value = Me.ActivacionTextBox2.Text
                End If
                If Me.SuspensionTelTextBox5.Text = "  /  /" Then
                    prm8.Value = CDate("01/01/1900")
                Else
                    prm8.Value = Me.SuspensionTelTextBox5.Text
                End If
                If Me.BajaTelTextBox1.Text = "  /  /" Then
                    prm9.Value = CDate("01/01/1900")
                Else
                    prm9.Value = Me.BajaTelTextBox1.Text
                End If
                prm10.Value = Me.ObsTextBox.Text
            End If
            .Parameters.Add(prm)
            .Parameters.Add(prm2)
            .Parameters.Add(prm3)
            .Parameters.Add(prm4)
            .Parameters.Add(prm5)
            .Parameters.Add(prm6)
            .Parameters.Add(prm7)
            .Parameters.Add(prm8)
            .Parameters.Add(prm9)
            .Parameters.Add(prm10)
            Dim i As Integer = .ExecuteNonQuery
            If opt = 1 Then
                Me.ContratonetTextBox.Text = prm.Value
            End If
        End With
        ConTel.Close()
        If opt = 2 Then
            MsgBox("Se Guardo Con �xito", MsgBoxStyle.Information, "Telefon�a")
        End If

    End Sub

    Private Sub GuardarToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GuardarToolStripButton1.Click
        'SE ELIGE EL PLAN Y NUMERO TELEFONICO 
        optTel = 1
        FrmSelServicios.Show()
    End Sub
    Public Sub Guardar_ContTel(ByVal opt As Integer)
        Dim ConTel1 As New SqlClient.SqlConnection(MiConexion)
        Dim cmd1 As New SqlClient.SqlCommand
        Dim Nombre As String = Nothing
        Dim ccNet As Integer = Nothing
        If (GloClv_servTel > 0 And GloContratonet > 0 And opt = 1) Or (GloContratonet > 0 And opt = 2) Then
            GlobndGuardaTelefonia = True
            If opt = 1 Then
                Nombre = "Nuevo_ContTel"
            ElseIf opt = 2 Then
                Nombre = "Modifica_ContTel"
                'ConTel1.Open()
                'Me.Consulta_ContTelTableAdapter.Connection = ConTel1
                'Me.Consulta_ContTelTableAdapter.Fill(Me.DataSetLidia2.Consulta_ContTel, Clv_UnicaTel, GloContratonet)
                'ConTel1.Close()
            End If

            ConTel1.Open()
            With cmd1
                .CommandText = Nombre
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = ConTel1
                Dim prm As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
                Dim prm2 As New SqlParameter("@contratonet", SqlDbType.BigInt)
                prm2.Direction = ParameterDirection.Input
                Dim prm25 As New SqlParameter("@clv_telefono", SqlDbType.BigInt)
                prm25.Direction = ParameterDirection.Input
                Dim prm3 As New SqlParameter("@referencia_Pago", SqlDbType.BigInt)
                prm3.Direction = ParameterDirection.Input
                Dim prm4 As New SqlParameter("@clv_servicio", SqlDbType.BigInt)
                prm4.Direction = ParameterDirection.Input
                Dim prm5 As New SqlParameter("@status", SqlDbType.VarChar, 1)
                prm5.Direction = ParameterDirection.Input
                Dim prm6 As New SqlParameter("@fecha_solicitud", SqlDbType.DateTime)
                prm6.Direction = ParameterDirection.Input
                Dim prm7 As New SqlParameter("@fecha_instalacion", SqlDbType.DateTime)
                prm7.Direction = ParameterDirection.Input
                Dim prm8 As New SqlParameter("@Fecha_Suspension", SqlDbType.DateTime)
                prm8.Direction = ParameterDirection.Input
                Dim prm9 As New SqlParameter("@Fecha_Baja", SqlDbType.DateTime)
                prm9.Direction = ParameterDirection.Input
                Dim prm10 As New SqlParameter("@fecha_Ult_Pago", SqlDbType.DateTime)
                prm10.Direction = ParameterDirection.Input
                Dim prm11 As New SqlParameter("@PrimerMensualidad", SqlDbType.Int)
                prm11.Direction = ParameterDirection.Input
                Dim prm12 As New SqlParameter("@ultimo_mes", SqlDbType.Int)
                prm12.Direction = ParameterDirection.Input
                Dim prm13 As New SqlParameter("@ultimo_anio", SqlDbType.Int)
                prm13.Direction = ParameterDirection.Input
                Dim prm14 As New SqlParameter("@factura", SqlDbType.VarChar, 20)
                prm14.Direction = ParameterDirection.Input
                Dim prm15 As New SqlParameter("@clv_promocion", SqlDbType.Int)
                prm15.Direction = ParameterDirection.Input
                Dim prm16 As New SqlParameter("@obs", SqlDbType.VarChar, 250)
                prm16.Direction = ParameterDirection.Input
                Dim prm17 As New SqlParameter("@clv_motcan", SqlDbType.Int)
                prm17.Direction = ParameterDirection.Input
                Dim prm18 As New SqlParameter("@cortesia", SqlDbType.Bit)
                prm18.Direction = ParameterDirection.Input
                Dim prm19 As New SqlParameter("@clv_telefonoXtra", SqlDbType.BigInt)
                prm19.Direction = ParameterDirection.Input

                If opt = 1 Then
                    prm.Direction = ParameterDirection.Output
                    prm.Value = 0
                    prm25.Value = 0
                    prm2.Value = GloContratonet
                    prm3.Value = 0
                    prm4.Value = GloClv_servTel
                    prm5.Value = "C"
                    prm6.Value = CDate(fecha_Hoy)
                    prm7.Value = CDate("01/01/1900")
                    prm8.Value = CDate("01/01/1900")
                    prm9.Value = CDate("01/01/1900")
                    prm10.Value = CDate("01/01/1900")
                    prm11.Value = 1
                    prm12.Value = 0
                    prm13.Value = 0
                    prm14.Value = ""
                    prm15.Value = 0
                    prm16.Value = ""
                    prm17.Value = 0
                    prm18.Value = False
                    prm19.Value = 0
                    '(@clv_unicanet bigint output,@contratonet bigint,@referencia_Pago bigint,@clv_servicio bigint,@status varchar(1),
                    '@fecha_solicitud datetime,@fecha_instalacion datetime,@fecha_suspension datetime,@fecha_baja datetime,@fecha_Ult_Pago datetime,
                    '@PrimerMensualidad int,@ultimo_mes int,@ultimo_anio int,@factura varchar(20),@clv_promocion bigint,@obs varchar(max),@clv_motcan bigint,@cortesia bit)             
                ElseIf opt = 2 Then
                    prm.Direction = ParameterDirection.Input
                    prm.Value = Clv_UnicaTel
                    If IsNumeric(Me.Clv_telefonoTextBox.Text) = True Then
                        prm25.Value = Me.Clv_telefonoTextBox.Text
                    Else
                        prm25.Value = 0
                    End If
                    prm2.Value = GloContratonet
                    If IsNumeric(Me.Referencia_pagoTextBox.Text) = False Then
                        prm3.Value = 0
                    Else
                        prm3.Value = Me.Referencia_pagoTextBox.Text
                    End If
                    GloClv_servTel = Me.Clv_servicioTextBox.Text
                    prm4.Value = Me.Clv_servicioTextBox.Text
                    If Me.ComboBox2.SelectedValue Is Nothing Then
                        prm5.Value = "C"
                    Else
                        prm5.Value = Me.ComboBox2.SelectedValue
                    End If

                    If Me.SolicitudTextBox1.Text = "  /  /" Then
                        prm6.Value = CDate("01/01/1900")
                    Else
                        prm6.Value = Me.SolicitudTextBox1.Text
                    End If

                    If Me.InstalacionTextBox1.Text = "  /  /" Then
                        prm7.Value = CDate("01/01/1900")
                    Else
                        prm7.Value = Me.InstalacionTextBox1.Text
                    End If

                    If Me.SuspensionTextBox4.Text = "  /  /" Then
                        prm8.Value = CDate("01/01/1900")
                    Else
                        prm8.Value = Me.SuspensionTextBox4.Text
                    End If

                    If Me.BajaTextBox6.Text = "  /  /" Then
                        prm9.Value = CDate("01/01/1900")
                    Else
                        prm9.Value = Me.BajaTextBox6.Text
                    End If

                    If Me.Ult_PAgoTextBox3.Text = "  /  /" Then
                        prm10.Value = CDate("01/01/1900")
                    Else
                        prm10.Value = Me.Ult_PAgoTextBox3.Text
                    End If

                    If Me.CheckBox1.Checked = True Then
                        prm11.Value = 1
                    Else
                        prm11.Value = 0
                    End If
                    If Me.Ultimo_MesTextBox.Text = "" Then
                        prm12.Value = 0
                    Else
                        prm12.Value = CInt(Me.Ultimo_MesTextBox.Text)
                    End If
                    If Me.Ultimo_anioTextBox.Text = "" Then
                        prm13.Value = 0
                    Else
                        prm13.Value = CInt(Me.Ultimo_anioTextBox.Text)
                    End If

                    prm14.Value = Me.FacturaTextBox.Text
                    If IsNumeric(Me.ComboBox5.SelectedValue) = False Then
                        prm15.Value = 0
                    Else
                        prm15.Value = Me.ComboBox5.SelectedValue
                    End If
                    prm16.Value = Me.ObsTextBox1.Text

                    If IsNumeric(Me.ComboBox3.SelectedValue) = False Then
                        prm17.Value = 0
                    Else
                        prm17.Value = Me.ComboBox3.SelectedValue
                    End If
                    prm18.Value = Me.CortesiaCheckBox.Checked
                    If IsNumeric(Me.Clv_telefonoXtraTextBox.Text) = False Then
                        prm19.Value = 0
                    Else
                        prm19.Value = Me.Clv_telefonoXtraTextBox.Text
                    End If
                End If
                .Parameters.Add(prm)
                .Parameters.Add(prm2)
                .Parameters.Add(prm25)
                .Parameters.Add(prm3)
                .Parameters.Add(prm4)
                .Parameters.Add(prm5)
                .Parameters.Add(prm6)
                .Parameters.Add(prm7)
                .Parameters.Add(prm8)
                .Parameters.Add(prm9)
                .Parameters.Add(prm10)
                .Parameters.Add(prm11)
                .Parameters.Add(prm12)
                .Parameters.Add(prm13)
                .Parameters.Add(prm14)
                .Parameters.Add(prm15)
                .Parameters.Add(prm16)
                .Parameters.Add(prm17)
                .Parameters.Add(prm18)
                .Parameters.Add(prm19)
                Dim i As Integer = .ExecuteNonQuery
                If opt = 1 Then
                    ccNet = prm.Value
                End If
            End With
            '

            If opt = 1 Then
                GloClv_servTel = 0
                GloContratonet = GloContratonet_Nuevo
            Else
                ccNet = Clv_UnicaTel
            End If
            Me.Consulta_ContTelTableAdapter.Connection = ConTel1
            Me.Consulta_ContTelTableAdapter.Fill(Me.DataSetLidia2.Consulta_ContTel, ccNet, GloContratonet)
            ConTel1.Close()
        End If
        'Equip_tel = False
        'globndTel = True

        'If FrmClientes.Button28.Visible = True Then
        '    FrmClientes.Button8.Enabled = True
        '    FrmClientes.Button28.Enabled = False
        'End If
        If opt = 2 Then
            MsgBox("Se Guardo Con �xito", MsgBoxStyle.Information, "Telefon�a")
        End If
        'CREAARBOL()
    End Sub

    Private Sub GuardarToolStripButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GuardarToolStripButton3.Click
        If Me.ComboBox2.Text = "Instalado" Then
            If IsDate(Me.InstalacionTextBox1.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Instalaci�n", MsgBoxStyle.Information, "Atenci�n")
                Exit Sub
            ElseIf Me.Ultimo_MesTextBox.Text = "" Or CInt(Me.Ultimo_MesTextBox.Text) > 12 Or CInt(Me.Ultimo_MesTextBox.Text) = 0 Then
                MsgBox("Se Requiere que Capture el �ltimo Mes", MsgBoxStyle.Exclamation, "Atenci�n")
                Exit Sub
            ElseIf (Me.Ultimo_anioTextBox.Text).Length <> 4 Or IsNumeric(Me.Ultimo_anioTextBox.Text) = False Then
                MsgBox("Se Requiere que Capture el �ltimo A�o", MsgBoxStyle.Exclamation, "Atenci�n")
                Exit Sub
            End If
        ElseIf Me.ComboBox2.Text = "Desconectado" Or Me.ComboBox2.SelectedValue = "Suspendido" Then
            If IsDate(Me.SuspensionTextBox4.Text) = False Then
                MsgBox("Es Necesario que se Capture una Fecha de Suspensi�n", MsgBoxStyle.Information, "Atenci�n")
                Exit Sub
            End If
        ElseIf Me.ComboBox2.Text = "Baja" Then
            If IsDate(Me.BajaTextBox6.Text) = False Then
                MsgBox("Es Necesario que se Capture una Fecha de Baja", MsgBoxStyle.Information, "Atenci�n")
                Exit Sub
            End If
        End If
        Guardar_ContTel(2)

        If IsNumeric(Me.Clv_unicanetTextBox.Text) = True Then
            InsertaRelClv_UnicaNetExt(Me.Clv_unicanetTextBox.Text, Me.NumericUpDown1.Value)
        End If
    End Sub
    Public Function Checar_Equipos(ByVal optcontrato As Integer, ByVal opp As Integer) As Integer
        Dim cmd As New SqlCommand
        Dim ConLidia As New SqlConnection(MiConexion)
        ConLidia.Open()
        With cmd
            .CommandText = "Contarclientes"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = ConLidia
            Dim prm As New SqlParameter("@Contratonet", SqlDbType.BigInt)
            Dim prm2 As New SqlParameter("@Opc", SqlDbType.Int)
            Dim prm3 As New SqlParameter("@Cont", SqlDbType.Int)
            prm.Direction = ParameterDirection.Input
            prm2.Direction = ParameterDirection.Input
            prm3.Direction = ParameterDirection.Output
            prm.Value = GloContratonet 'optcontrato
            prm2.Value = opp
            prm3.Value = 0
            .Parameters.Add(prm)
            .Parameters.Add(prm2)
            .Parameters.Add(prm3)
            Dim i As Integer = .ExecuteNonQuery
            If IsNumeric(prm3.Value) = True Then
                Checar_Equipos = prm3.Value
            Else
                Checar_Equipos = 0
            End If
        End With
        ConLidia.Close()
        'CREAARBOL()
    End Function
    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        'checar que se pueda eliminar
        If Contrato > 0 Then
            contador = Checar_Equipos(Contrato, 8)
            If contador = 0 Then
                Elimina_Aparato()
            Else
                MsgBox("Primero se debe de eliminar el Plan Tarifario", MsgBoxStyle.Information, "Telefon�a")
            End If
        End If
    End Sub
    Public Sub Elimina_Aparato()
        Dim coneTel3 As New SqlClient.SqlConnection(MiConexion)
        Dim cmd2 As New SqlClient.SqlCommand
        coneTel3.Open()
        With cmd2
            .CommandText = "Borra_ClientesTel"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = coneTel3
            Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
            Dim prm2 As New SqlParameter("@contrato", SqlDbType.BigInt)
            prm.Direction = ParameterDirection.Input
            prm2.Direction = ParameterDirection.Input
            prm.Value = Me.ContratonetTextBox.Text
            prm2.Value = Me.ContratoTextBox.Text
            .Parameters.Add(prm)
            .Parameters.Add(prm2)
            Dim i As Integer = .ExecuteNonQuery
        End With
        coneTel3.Close()
        'CREAARBOL()
    End Sub

    Private Sub BindingNavigatorDeleteItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem1.Click
        'checar que se pueda eliminar
        If Clv_UnicaTel > 0 Then
            contador = Checar_Equipos(Clv_UnicaTel, 9)
            If contador = 0 Then
                Elimina_Plan()
            Else
                MsgBox("Primero se debe de eliminar el Plan Adicional", MsgBoxStyle.Information, "Telefon�a")
            End If
        End If

    End Sub
    Public Sub Elimina_Plan()
        Dim coneTel As New SqlClient.SqlConnection(MiConexion)
        Dim cmd2 As New SqlClient.SqlCommand
        coneTel.Open()
        With cmd2
            .CommandText = "Borra_conttel"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = coneTel
            Dim prm As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
            Dim prm2 As New SqlParameter("@contratonet", SqlDbType.BigInt)
            prm.Direction = ParameterDirection.Input
            prm2.Direction = ParameterDirection.Input
            If IsNumeric(Me.Clv_unicanetTextBox.Text) = False Then Me.Clv_unicanetTextBox.Text = 0
            prm.Value = Me.Clv_unicanetTextBox.Text
            prm2.Value = GloContratonet
            .Parameters.Add(prm)
            .Parameters.Add(prm2)
            Dim i As Integer = .ExecuteNonQuery
        End With
        coneTel.Close()
        GLOMOVNET = 0
        Me.Clv_unicanetTextBox.Text = 0

        'CREAARBOL()
    End Sub

    Private Sub FrmClientesTel_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
     
        If GloBndSer = True Then
            GloBndSer = False
            Me.BindingNavigator5.Enabled = True
            Me.Clv_servicioTextBox.Text = GloClv_servTel
            optTel = 1
            FrmSeleccionaTel.Show()
        End If
        If GloBndNum = True Then
            GloBndNum = False
            Me.Clv_telefonoTextBox.Text = Clv_NumTelefonico
            Guardar_ContTel(1)
        End If
        If GloBndAdic = True Then
            GloBndAdic = False
            If optTel = 3 Then
                Me.Clv_servicioTextBox1.Text = clv_paqAdic
            ElseIf optTel = 4 Then
                Me.Clv_servicioTextBox1.Text = gloclv_servicioDigital
            End If
            Guarda_Paq_Adic(1)
        End If
        If GloTipoUsuario = 40 Then
            Me.CortesiaCheckBox.Enabled = True
        Else
            Me.CortesiaCheckBox.Enabled = False

        End If
        'frmTelefonia()
        If GloOpPermiso = 5 And GloPermisoCortesia = 1 Then
            Me.CortesiaCheckBox.Enabled = True
            Me.CortesiaCheckBox.Focus()
        End If
        If eGloDescuento = 1 And eGloTipSerDesc = 5 Then
            eGloDescuento = 0
            eGloTipSerDesc = 0
            eTipSer = 5
            eClv_UnicaNetDig = Clv_UnicaTel
            FrmRelCteDescuento.Show()
            carga()
        End If
        If eBndDesc = True Then
            eBndDesc = False
            BuscaDescTel()
        End If
    End Sub

    Public Sub carga()
        If eBndDesc = True Then
            eBndDesc = False
            BuscaDescTel()
        End If
        Dim contel As New SqlClient.SqlConnection(MiConexion)
        If globndclitel = True Then
            globndclitel = False
            contel.Open()
            Me.Consulta_ClientesTelTableAdapter.Connection = contel
            Me.Consulta_ClientesTelTableAdapter.Fill(Me.DataSetLidia2.Consulta_ClientesTel, GloContratonet, Contrato)
            contel.Close()
        End If
        If globndcotel = True Then
            globndcotel = False
            contel.Open()
            Me.Consulta_ContTelTableAdapter.Connection = contel
            Me.Consulta_ContTelTableAdapter.Fill(Me.DataSetLidia2.Consulta_ContTel, Clv_UnicaTel, GloContratonet)
            contel.Close()
            frmInternet2.Show()
            BuscaDescTel()
            
        End If
        If globndpaqtel = True Then
            globndpaqtel = False
            contel.Open()
            Me.Consulta_ConTTelAdicTableAdapter.Connection = contel
            Me.Consulta_ConTTelAdicTableAdapter.Fill(Me.DataSetLidia2.Consulta_ConTTelAdic, Clv_UnicaTel, GloClv_servTel, Tipo_Servicio)
            contel.Close()
        End If

        ConsultaRelClv_UnicaNetExt(Clv_UnicaTel)
        'Aqui
        Checa_Forzoso(Clv_UnicaTel, Contrato)

        Me.Focus()
    End Sub
    Private Sub FrmClientesTel_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'colorea(Me, Me.Name)
        Dim Contel As New SqlClient.SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand
        GloClv_TipSer = 5
        Contel.Open()        
        Me.MuestraMotivoCancelacionTableAdapter.Connection = Contel
        Me.MuestraMotivoCancelacionTableAdapter.Fill(Me.DataSetLidia.MuestraMotivoCancelacion)
        Me.StatusTelTableAdapter.Connection = Contel
        Me.StatusTelTableAdapter.Fill(Me.DataSetLidia2.StatusTel)
        'Me.StatusMTATableAdapter.Connection = Contel
        'Me.StatusMTATableAdapter.Fill(Me.DataSetLidia2.StatusMTA)
        Me.MuestraTipoPromocionTableAdapter.Connection = Contel
        Me.MuestraTipoPromocionTableAdapter.Fill(Me.DataSetLidia2.MuestraTipoPromocion, GloClv_TipSer)
        'Me.Consulta_ClientesTelTableAdapter.Connection = Contel
        'Me.Consulta_ClientesTelTableAdapter.Fill(Me.DataSetLidia2.Consulta_ClientesTel, contratoNetTel, Contrato)
        Me.MUESTRA_TiposPaqTableAdapter.Connection = Contel
        Me.MUESTRA_TiposPaqTableAdapter.Fill(Me.DataSetLidia2.MUESTRA_TiposPaq, 0)
        Contel.Close()
        Me.BindingNavigatorDeleteItem.Enabled = True
        Me.BindingNavigatorDeleteItem1.Enabled = True
        Me.BindingNavigatorDeleteItem2.Enabled = True
        Me.BindingNavigatorDeleteItem3.Enabled = True
        Me.ToolStripButton1.Enabled = True
        BuscaDescTel()

        If OpcionCli = "C" Then
            Me.BindingNavigator4.Enabled = False
            Me.BindingNavigator6.Enabled = False
            Me.Button1.Enabled = False
            Me.Button2.Enabled = False
            Me.ComboBox2.Enabled = False
            Me.ComboBox3.Enabled = False
            Me.ComboBox4.Enabled = False
            Me.ComboBox6.Enabled = False
        Else
            Me.BindingNavigator4.Enabled = True
            Me.BindingNavigator6.Enabled = True
            Me.Button1.Enabled = True
            Me.Button2.Enabled = True
            Me.ComboBox2.Enabled = True
            Me.ComboBox3.Enabled = True
            Me.ComboBox4.Enabled = True
            Me.ComboBox6.Enabled = True
        End If

    End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        'SE ELIGE PAQUETE ADICIONAL
        optTel = 3
        FrmSeleccionaTel.Show()
    End Sub
    Public Sub Guarda_Paq_Adic(ByVal opt As Integer)
        Dim ConTel1 As New SqlClient.SqlConnection(MiConexion)
        Dim cmd1 As New SqlClient.SqlCommand
        Dim Nombre As String = Nothing
        Dim ccNet As Integer = Nothing
        If opt = 1 Then
            Nombre = "Nuevo_ConTTelAdic"
        ElseIf opt = 2 Then
            Nombre = "Modifica_ContTelAdic"
        End If
        ConTel1.Open()
        With cmd1
            '(@clv_unicanet bigint,@clv_servicio bigint,@status varchar(1),@fecha_solicitud datetime,@fecha_instalacion datetime,
            '@fecha_suspension datetime,@fecha_baja datetime)
            .CommandText = Nombre
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = ConTel1
            Dim prm As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
            prm.Direction = ParameterDirection.Input
            Dim prm2 As New SqlParameter("@clv_servicio", SqlDbType.BigInt)
            prm2.Direction = ParameterDirection.Input
            Dim prm3 As New SqlParameter("@status", SqlDbType.VarChar, 1)
            prm3.Direction = ParameterDirection.Input
            Dim prm4 As New SqlParameter("@fecha_solicitud", SqlDbType.DateTime)
            prm4.Direction = ParameterDirection.Input
            Dim prm5 As New SqlParameter("@fecha_instalacion", SqlDbType.DateTime)
            prm5.Direction = ParameterDirection.Input
            Dim prm6 As New SqlParameter("@Fecha_Suspension", SqlDbType.DateTime)
            prm6.Direction = ParameterDirection.Input
            Dim prm7 As New SqlParameter("@Fecha_Baja", SqlDbType.DateTime)
            prm7.Direction = ParameterDirection.Input
            Dim prm8 As New SqlParameter("@Tipo_Servicio", SqlDbType.VarChar, 1)
            prm8.Direction = ParameterDirection.Input

            If opt = 1 Then
                prm.Value = Me.Clv_unicanetTextBox.Text
                prm2.Value = Me.Clv_servicioTextBox1.Text
                prm3.Value = "C"
                prm4.Value = CDate(fecha_Hoy)
                prm5.Value = CDate("01/01/1900")
                prm6.Value = CDate("01/01/1900")
                prm7.Value = CDate("01/01/1900")
                prm8.Value = Tipo_Servicio

            ElseIf opt = 2 Then
                prm.Value = Me.Clv_unicanetTextBox.Text
                prm2.Value = Me.Clv_servicioTextBox1.Text
                prm3.Value = Me.ComboBox4.SelectedValue
                If Me.SolicitudAdicTextBox2.Text = "  /  /" Then
                    prm4.Value = "01/01/1900"
                Else
                    prm4.Value = Me.SolicitudAdicTextBox2.Text
                End If
                If Me.InstalacionAdicTextBox3.Text = "  /  /" Then
                    prm5.Value = "01/01/1900"
                Else
                    prm5.Value = Me.InstalacionAdicTextBox3.Text
                End If
                If Me.SuspensionAdicTextBox4.Text = "  /  /" Then
                    prm6.Value = "01/01/1900"
                Else
                    prm6.Value = Me.SuspensionAdicTextBox4.Text
                End If
                If Me.BajaAdicTextBox1.Text = "  /  /" Then
                    prm7.Value = "01/01/1900"
                Else
                    prm7.Value = Me.BajaAdicTextBox1.Text
                End If
                prm8.Value = Tipo_Servicio
            End If
            .Parameters.Add(prm)
            .Parameters.Add(prm2)
            .Parameters.Add(prm3)
            .Parameters.Add(prm4)
            .Parameters.Add(prm5)
            .Parameters.Add(prm6)
            .Parameters.Add(prm7)
            .Parameters.Add(prm8)

            Dim i As Integer = .ExecuteNonQuery

        End With
        ConTel1.Close()
        If opt = 2 Then
            MsgBox("Se Guardo Con �xito", MsgBoxStyle.Information, "Telefon�a")
        End If
    End Sub

    Private Sub ToolStripButton4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton4.Click
        Guarda_Paq_Adic(2)
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        If Me.Clv_unicanetTextBox1.Text <> "" Then
            Elimina_Paq_Adic()
        End If

    End Sub
    Public Sub Elimina_Paq_Adic()
        Dim coneTel5 As New SqlClient.SqlConnection(MiConexion)
        Dim cmd2 As New SqlClient.SqlCommand
        coneTel5.Open()
        With cmd2
            .CommandText = "Borra_ContTelAdic"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = coneTel5
            Dim prm As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
            Dim prm2 As New SqlParameter("@clv_servicio", SqlDbType.BigInt)
            Dim prm3 As New SqlParameter("@Tipo_Servicio", SqlDbType.VarChar, 1)
            prm.Direction = ParameterDirection.Input
            prm2.Direction = ParameterDirection.Input
            prm3.Direction = ParameterDirection.Input
            prm.Value = Me.Clv_unicanetTextBox1.Text
            prm2.Value = Me.Clv_servicioTextBox1.Text
            prm3.Value = Tipo_Servicio
            .Parameters.Add(prm)
            .Parameters.Add(prm2)
            .Parameters.Add(prm3)
            Dim i As Integer = .ExecuteNonQuery
        End With
        coneTel5.Close()
        'CREAARBOL()
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Descuento
        If Me.CortesiaCheckBox.Checked = False Then
            eTipSer = 5
            If eAccesoAdmin = True Then
                'eClv_UnicaNetDig = Clv_UnicaTel
                FrmRelCteDescuento.Show()
            Else
                eGloDescuento = 0
                eGloTipSerDesc = 0
                FrmAccesopoUsuario.Show()
            End If
        Else
            MsgBox("No Puede Aplicar un Descuento, ya que cuenta con una Cortes�a", , "Atenci�n")
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Me.Clv_UsuarioTextBox.Text = ""
        'Me.PasaporteTextBox.Text = ""
        GloOpPermiso = 5
        GloPermisoCortesia = 0
        'Me.GroupBox1.Visible = True
        FrmAccesopoUsuario.Show()
    End Sub
    Private Sub BuscaDescTel()
        Dim CON2 As New SqlConnection(MiConexion)
        Dim eRes As Integer = Nothing
        Dim eMsg As String = Nothing
        If Clv_UnicaTel > 0 Then
            CON2.Open()
            Me.ChecaRelCteDescuentoTableAdapter.Connection = CON2
            Me.ChecaRelCteDescuentoTableAdapter.Fill(Me.DataSetEric.ChecaRelCteDescuento, Clv_UnicaTel, 5, eRes, eMsg)
            CON2.Close()
            If eRes = 1 Then
                CON2.Open()
                Me.ConRelCteDescuentoTableAdapter.Connection = CON2
                Me.ConRelCteDescuentoTableAdapter.Fill(Me.DataSetEric.ConRelCteDescuento, Clv_UnicaTel, 5)
                CON2.Close()
                Me.TextBox7.Visible = True
                Me.Label39.Visible = True
            Else
                Me.TextBox7.Visible = False
                Me.Label39.Visible = False
            End If
        End If
    End Sub

    Private Sub ToolStripButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton3.Click
        'cancelar Adicional
        Me.Consulta_ConTTelAdicBindingSource.CancelEdit()
    End Sub

    Private Sub BindingNavigatorDeleteItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem3.Click
        'cancelar conttel
        Me.Consulta_ContTelBindingSource.CancelEdit()
    End Sub

    Private Sub BindingNavigatorDeleteItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem2.Click
        'cancelar clientetel
        Me.Consulta_ClientesTelBindingSource.CancelEdit()
    End Sub

    Private Sub Clv_cablemodemTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Clv_cablemodemTextBox.TextChanged
        'If IsNumeric(Me.Clv_cablemodemTextBox.Text) Then
        '    GloClv_Cablemodem = Me.Clv_cablemodemTextBox.Text
        '    Equip_tel = True
        'End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        eEntraUMB = True
        eEntraUM = False
    End Sub

    Private Sub TextBox2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox2.LostFocus
        
        If Me.TextBox2.Text.Length < 3 Then
            MsgBox("Captura el Mes.", , "Atenci�n")
            Me.TextBox2.Focus()
        End If
    End Sub


    Private Sub TextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox2.TextChanged
        If eEntraUMB = True Then
            If Me.TextBox2.Text.Length = 3 Then
                If Me.TextBox2.Text = "ENE" Then
                    Me.Ultimo_MesTextBox.Text = 1
                ElseIf Me.TextBox2.Text = "FEB" Then
                    Me.Ultimo_MesTextBox.Text = 2
                ElseIf Me.TextBox2.Text = "MAR" Then
                    Me.Ultimo_MesTextBox.Text = 3
                ElseIf Me.TextBox2.Text = "ABR" Then
                    Me.Ultimo_MesTextBox.Text = 4
                ElseIf Me.TextBox2.Text = "MAY" Then
                    Me.Ultimo_MesTextBox.Text = 5
                ElseIf Me.TextBox2.Text = "JUN" Then
                    Me.Ultimo_MesTextBox.Text = 6
                ElseIf Me.TextBox2.Text = "JUL" Then
                    Me.Ultimo_MesTextBox.Text = 7
                ElseIf Me.TextBox2.Text = "AGO" Then
                    Me.Ultimo_MesTextBox.Text = 8
                ElseIf Me.TextBox2.Text = "SEP" Then
                    Me.Ultimo_MesTextBox.Text = 9
                ElseIf Me.TextBox2.Text = "OCT" Then
                    Me.Ultimo_MesTextBox.Text = 10
                ElseIf Me.TextBox2.Text = "NOV" Then
                    Me.Ultimo_MesTextBox.Text = 11
                ElseIf Me.TextBox2.Text = "DIC" Then
                    Me.Ultimo_MesTextBox.Text = 12
                Else
                    MsgBox("El Mes Tecleado es Incorrecto.", , "Error")
                    Me.TextBox2.Clear()
                End If
            End If
        End If
    End Sub

    Private Sub Ultimo_MesTextBox_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Ultimo_MesTextBox.Click
        eEntraUM = True
        eEntraUMB = False
    End Sub

    Private Sub Ultimo_MesTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ultimo_MesTextBox.TextChanged
        If eEntraUM = True Then
            If IsNumeric(Ultimo_MesTextBox.Text) = True Then
                If Me.Ultimo_MesTextBox.Text = 1 Then
                    Me.TextBox2.Text = "ENE"
                ElseIf Me.Ultimo_MesTextBox.Text = 2 Then
                    Me.TextBox2.Text = "FEB"
                ElseIf Me.Ultimo_MesTextBox.Text = 3 Then
                    Me.TextBox2.Text = "MAR"
                ElseIf Me.Ultimo_MesTextBox.Text = 4 Then
                    Me.TextBox2.Text = "ABR"
                ElseIf Me.Ultimo_MesTextBox.Text = 5 Then
                    Me.TextBox2.Text = "MAY"
                ElseIf Me.Ultimo_MesTextBox.Text = 6 Then
                    Me.TextBox2.Text = "JUN"
                ElseIf Me.Ultimo_MesTextBox.Text = 7 Then
                    Me.TextBox2.Text = "JUL"
                ElseIf Me.Ultimo_MesTextBox.Text = 8 Then
                    Me.TextBox2.Text = "AGO"
                ElseIf Me.Ultimo_MesTextBox.Text = 9 Then
                    Me.TextBox2.Text = "SEP"
                ElseIf Me.Ultimo_MesTextBox.Text = 10 Then
                    Me.TextBox2.Text = "OCT"
                ElseIf Me.Ultimo_MesTextBox.Text = 11 Then
                    Me.TextBox2.Text = "NOV"
                ElseIf Me.Ultimo_MesTextBox.Text = 12 Then
                    Me.TextBox2.Text = "DIC"
                Else
                    Me.TextBox2.Text = "---"
                End If
            End If
        End If
    End Sub
    Private Sub Clv_UsuarioTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        'If Asc(e.KeyChar) = 13 Then
        '    Permiso()
        'End If
    End Sub

    Private Sub Clv_UsuarioTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub PasaporteTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        'If Asc(e.KeyChar) = 13 Then
        '    Permiso()
        'End If
    End Sub

    'Private Sub Permiso()
    '    Dim CON As New SqlConnection(MiConexion)
    '    CON.Open()
    '    Try
    '        'Me.CortesiaCheckBox.Enabled = False
    '        Dim bnd As Integer = 0
    '        Dim clv_usuario As Integer = 0
    '        Me.Dame_clvtipusuarioTableAdapter.Connection = CON
    '        Me.Dame_clvtipusuarioTableAdapter.Fill(Me.ProcedimientosArnoldo2.Dame_clvtipusuario, Me.Clv_UsuarioTextBox.Text, Me.PasaporteTextBox.Text, clv_usuario)
    '        Select Case clv_usuario
    '            Case 1
    '                Me.VerAcceso_ChecaTableAdapter.Connection = CON
    '                Me.VerAcceso_ChecaTableAdapter.Fill(Me.DataSetEdgarRev2.VerAcceso_Checa, Me.Clv_UsuarioTextBox.Text, Me.PasaporteTextBox.Text, New System.Nullable(Of Integer)(CType(1, Integer)), bnd)
    '            Case 40
    '                Me.VerAcceso_ChecaTableAdapter.Connection = CON
    '                Me.VerAcceso_ChecaTableAdapter.Fill(Me.DataSetEdgarRev2.VerAcceso_Checa, Me.Clv_UsuarioTextBox.Text, Me.PasaporteTextBox.Text, New System.Nullable(Of Integer)(CType(40, Integer)), bnd)
    '            Case Else
    '                bnd = 0
    '        End Select

    '        If bnd = 1 Then
    '            'GloPermisoCortesia = 1
    '            'eGloDescuento = 1
    '            'Me.CortesiaCheckBox.Enabled = True
    '            Me.Close()
    '        Else
    '            'GloPermisoCortesia = 0
    '            'eGloDescuento = 0
    '            MsgBox("No Tiene Acceso ", MsgBoxStyle.Information)
    '            Me.Close()
    '        End If
    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try
    '    CON.Close()
    'End Sub

    

    
    Private Sub OK_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Permiso()
        'Me.GroupBox1.Visible = False
    End Sub

    Private Sub Cancel_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Me.GroupBox1.Visible = False
    End Sub

    Private Sub InsertaRelClv_UnicaNetExt(ByVal Clv_UnicaNet As Long, ByVal NoExtensiones As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("InsertaRelClv_UnicaNetExt", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_UnicaNet", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_UnicaNet
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@NoExtensiones", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = NoExtensiones
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub ConsultaRelClv_UnicaNetExt(ByVal Clv_UnicaNet As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ConsultaRelClv_UnicaNetExt", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_UnicaNet", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_UnicaNet
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@NoExtensiones", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Output
        parametro2.Value = 0
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            Me.NumericUpDown1.Value = CInt(parametro2.Value.ToString)
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try

    End Sub

    Private Sub SolicitudAdicTextBox2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SolicitudAdicTextBox2.TextChanged
        If SolicitudAdicTextBox2.Text = "01/01/1900" Then
            SolicitudAdicTextBox2.Text = "  /  /"
        End If
    End Sub

    Private Sub InstalacionAdicTextBox3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles InstalacionAdicTextBox3.TextChanged
        If InstalacionAdicTextBox3.Text = "01/01/1900" Then
            InstalacionAdicTextBox3.Text = "  /  /"
        End If
    End Sub

    Private Sub SuspensionAdicTextBox4_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SuspensionAdicTextBox4.TextChanged
        If SuspensionAdicTextBox4.Text = "01/01/1900" Then
            SuspensionAdicTextBox4.Text = "  /  /"
        End If
    End Sub

    Private Sub BajaAdicTextBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles BajaAdicTextBox1.TextChanged
        If BajaAdicTextBox1.Text = "01/01/1900" Then
            BajaAdicTextBox1.Text = "  /  /"
        End If
    End Sub
End Class