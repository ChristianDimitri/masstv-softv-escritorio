<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmrRelPaquetesdelCliente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.TreeView2 = New System.Windows.Forms.TreeView()
        Me.MacCableModem1 = New System.Windows.Forms.TextBox()
        Me.Contratonet1 = New System.Windows.Forms.TextBox()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.MacCableModem = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Contratonet = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Clv_Unicanet = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Clv_Unicanet1 = New System.Windows.Forms.TextBox()
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.BorraMotivoCanServBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorraMotivoCanServTableAdapter = New sofTV.DataSetEricTableAdapters.BorraMotivoCanServTableAdapter()
        Me.GuardaMotivoCanServBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GuardaMotivoCanServTableAdapter = New sofTV.DataSetEricTableAdapters.GuardaMotivoCanServTableAdapter()
        Me.MUESTRACONTNETPorOpcionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.MUESTRACABLEMODEMSDELCLIporOpcionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter()
        Me.MUESTRACONTNET_PorOpcionTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACONTNET_PorOpcionTableAdapter()
        Me.MUESTRACONTNETBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACONTNETTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACONTNETTableAdapter()
        Me.MUESTRACONTNETPorOpcionBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRAIPAQUporSOLBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRAIPAQU_porSOLTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRAIPAQU_porSOLTableAdapter()
        Me.NUEIPAQUSOLBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NUEIPAQU_SOLTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.NUEIPAQU_SOLTableAdapter()
        Me.BorIPAQUSOLBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorIPAQU_SOLTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.borIPAQU_SOLTableAdapter()
        Me.DameContratoNetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameContratoNetTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.DameContratoNetTableAdapter()
        Me.BORDetOrdSerBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BORDetOrdSerTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.BORDetOrdSerTableAdapter()
        Me.BORDetOrdSer_INTELIGENTEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BORDetOrdSer_INTELIGENTETableAdapter = New sofTV.NewSofTvDataSetTableAdapters.BORDetOrdSer_INTELIGENTETableAdapter()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorraMotivoCanServBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GuardaMotivoCanServBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACONTNETPorOpcionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACABLEMODEMSDELCLIporOpcionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACONTNETBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACONTNETPorOpcionBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAIPAQUporSOLBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NUEIPAQUSOLBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorIPAQUSOLBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameContratoNetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BORDetOrdSerBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BORDetOrdSer_INTELIGENTEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(689, 330)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 4
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label4.Location = New System.Drawing.Point(503, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(247, 16)
        Me.Label4.TabIndex = 37
        Me.Label4.Text = "Instalar estos Servicios de Internet"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label3.Location = New System.Drawing.Point(12, 9)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(309, 16)
        Me.Label3.TabIndex = 36
        Me.Label3.Text = "Servicios de internet Pendientes de Instalar"
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkRed
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.White
        Me.Button4.Location = New System.Drawing.Point(345, 192)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(143, 30)
        Me.Button4.TabIndex = 3
        Me.Button4.Text = "<< "
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkRed
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.Location = New System.Drawing.Point(345, 154)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(143, 30)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = " >>"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'TreeView2
        '
        Me.TreeView2.BackColor = System.Drawing.Color.Gainsboro
        Me.TreeView2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TreeView2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView2.ForeColor = System.Drawing.Color.Navy
        Me.TreeView2.Location = New System.Drawing.Point(502, 28)
        Me.TreeView2.Name = "TreeView2"
        Me.TreeView2.Size = New System.Drawing.Size(323, 269)
        Me.TreeView2.TabIndex = 25
        Me.TreeView2.TabStop = False
        '
        'MacCableModem1
        '
        Me.MacCableModem1.Location = New System.Drawing.Point(550, 192)
        Me.MacCableModem1.Name = "MacCableModem1"
        Me.MacCableModem1.Size = New System.Drawing.Size(127, 20)
        Me.MacCableModem1.TabIndex = 33
        Me.MacCableModem1.TabStop = False
        '
        'Contratonet1
        '
        Me.Contratonet1.Location = New System.Drawing.Point(550, 217)
        Me.Contratonet1.Name = "Contratonet1"
        Me.Contratonet1.Size = New System.Drawing.Size(127, 20)
        Me.Contratonet1.TabIndex = 32
        Me.Contratonet1.TabStop = False
        '
        'TreeView1
        '
        Me.TreeView1.BackColor = System.Drawing.Color.Gainsboro
        Me.TreeView1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TreeView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView1.ForeColor = System.Drawing.Color.Navy
        Me.TreeView1.Location = New System.Drawing.Point(12, 28)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(313, 269)
        Me.TreeView1.TabIndex = 24
        Me.TreeView1.TabStop = False
        '
        'MacCableModem
        '
        Me.MacCableModem.Location = New System.Drawing.Point(147, 190)
        Me.MacCableModem.Name = "MacCableModem"
        Me.MacCableModem.Size = New System.Drawing.Size(109, 20)
        Me.MacCableModem.TabIndex = 31
        Me.MacCableModem.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(49, 193)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(92, 13)
        Me.Label2.TabIndex = 30
        Me.Label2.Text = "Mac Cablemodem"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(77, 222)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 13)
        Me.Label1.TabIndex = 29
        Me.Label1.Text = "ContratoNet"
        '
        'Contratonet
        '
        Me.Contratonet.Location = New System.Drawing.Point(147, 219)
        Me.Contratonet.Name = "Contratonet"
        Me.Contratonet.Size = New System.Drawing.Size(127, 20)
        Me.Contratonet.TabIndex = 28
        Me.Contratonet.TabStop = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkRed
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(345, 118)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(143, 30)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "<"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkRed
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(345, 82)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(143, 30)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = " >"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Clv_Unicanet
        '
        Me.Clv_Unicanet.Location = New System.Drawing.Point(162, 115)
        Me.Clv_Unicanet.Name = "Clv_Unicanet"
        Me.Clv_Unicanet.Size = New System.Drawing.Size(85, 20)
        Me.Clv_Unicanet.TabIndex = 39
        Me.Clv_Unicanet.TabStop = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(74, 118)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(68, 13)
        Me.Label5.TabIndex = 40
        Me.Label5.Text = "Clv Unicanet"
        '
        'Clv_Unicanet1
        '
        Me.Clv_Unicanet1.Location = New System.Drawing.Point(550, 239)
        Me.Clv_Unicanet1.Name = "Clv_Unicanet1"
        Me.Clv_Unicanet1.Size = New System.Drawing.Size(127, 20)
        Me.Clv_Unicanet1.TabIndex = 41
        Me.Clv_Unicanet1.TabStop = False
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BorraMotivoCanServBindingSource
        '
        Me.BorraMotivoCanServBindingSource.DataMember = "BorraMotivoCanServ"
        Me.BorraMotivoCanServBindingSource.DataSource = Me.DataSetEric
        '
        'BorraMotivoCanServTableAdapter
        '
        Me.BorraMotivoCanServTableAdapter.ClearBeforeFill = True
        '
        'GuardaMotivoCanServBindingSource
        '
        Me.GuardaMotivoCanServBindingSource.DataMember = "GuardaMotivoCanServ"
        Me.GuardaMotivoCanServBindingSource.DataSource = Me.DataSetEric
        '
        'GuardaMotivoCanServTableAdapter
        '
        Me.GuardaMotivoCanServTableAdapter.ClearBeforeFill = True
        '
        'MUESTRACONTNETPorOpcionBindingSource
        '
        Me.MUESTRACONTNETPorOpcionBindingSource.DataMember = "MUESTRACONTNET_PorOpcion"
        Me.MUESTRACONTNETPorOpcionBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MUESTRACABLEMODEMSDELCLIporOpcionBindingSource
        '
        Me.MUESTRACABLEMODEMSDELCLIporOpcionBindingSource.DataMember = "MUESTRACABLEMODEMSDELCLI_porOpcion"
        Me.MUESTRACABLEMODEMSDELCLIporOpcionBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter
        '
        Me.MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter.ClearBeforeFill = True
        '
        'MUESTRACONTNET_PorOpcionTableAdapter
        '
        Me.MUESTRACONTNET_PorOpcionTableAdapter.ClearBeforeFill = True
        '
        'MUESTRACONTNETBindingSource
        '
        Me.MUESTRACONTNETBindingSource.DataMember = "MUESTRACONTNET"
        Me.MUESTRACONTNETBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRACONTNETTableAdapter
        '
        Me.MUESTRACONTNETTableAdapter.ClearBeforeFill = True
        '
        'MUESTRACONTNETPorOpcionBindingSource1
        '
        Me.MUESTRACONTNETPorOpcionBindingSource1.DataMember = "MUESTRACONTNET_PorOpcion"
        Me.MUESTRACONTNETPorOpcionBindingSource1.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRAIPAQUporSOLBindingSource
        '
        Me.MUESTRAIPAQUporSOLBindingSource.DataMember = "MUESTRAIPAQU_porSOL"
        Me.MUESTRAIPAQUporSOLBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRAIPAQU_porSOLTableAdapter
        '
        Me.MUESTRAIPAQU_porSOLTableAdapter.ClearBeforeFill = True
        '
        'NUEIPAQUSOLBindingSource
        '
        Me.NUEIPAQUSOLBindingSource.DataMember = "NUEIPAQU_SOL"
        Me.NUEIPAQUSOLBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NUEIPAQU_SOLTableAdapter
        '
        Me.NUEIPAQU_SOLTableAdapter.ClearBeforeFill = True
        '
        'BorIPAQUSOLBindingSource
        '
        Me.BorIPAQUSOLBindingSource.DataMember = "borIPAQU_SOL"
        Me.BorIPAQUSOLBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'BorIPAQU_SOLTableAdapter
        '
        Me.BorIPAQU_SOLTableAdapter.ClearBeforeFill = True
        '
        'DameContratoNetBindingSource
        '
        Me.DameContratoNetBindingSource.DataMember = "DameContratoNet"
        Me.DameContratoNetBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'DameContratoNetTableAdapter
        '
        Me.DameContratoNetTableAdapter.ClearBeforeFill = True
        '
        'BORDetOrdSerBindingSource
        '
        Me.BORDetOrdSerBindingSource.DataMember = "BORDetOrdSer"
        Me.BORDetOrdSerBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'BORDetOrdSerTableAdapter
        '
        Me.BORDetOrdSerTableAdapter.ClearBeforeFill = True
        '
        'BORDetOrdSer_INTELIGENTEBindingSource
        '
        Me.BORDetOrdSer_INTELIGENTEBindingSource.DataMember = "BORDetOrdSer_INTELIGENTE"
        Me.BORDetOrdSer_INTELIGENTEBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'BORDetOrdSer_INTELIGENTETableAdapter
        '
        Me.BORDetOrdSer_INTELIGENTETableAdapter.ClearBeforeFill = True
        '
        'FrmrRelPaquetesdelCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(837, 375)
        Me.Controls.Add(Me.TreeView2)
        Me.Controls.Add(Me.TreeView1)
        Me.Controls.Add(Me.Clv_Unicanet1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Clv_Unicanet)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.MacCableModem1)
        Me.Controls.Add(Me.Contratonet1)
        Me.Controls.Add(Me.MacCableModem)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Contratonet)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Name = "FrmrRelPaquetesdelCliente"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Instalación de Servicios de Internet"
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorraMotivoCanServBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GuardaMotivoCanServBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACONTNETPorOpcionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACABLEMODEMSDELCLIporOpcionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACONTNETBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACONTNETPorOpcionBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAIPAQUporSOLBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NUEIPAQUSOLBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorIPAQUSOLBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameContratoNetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BORDetOrdSerBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BORDetOrdSer_INTELIGENTEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents TreeView2 As System.Windows.Forms.TreeView
    Friend WithEvents MacCableModem1 As System.Windows.Forms.TextBox
    Friend WithEvents Contratonet1 As System.Windows.Forms.TextBox
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents MacCableModem As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Contratonet As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents MUESTRACABLEMODEMSDELCLIporOpcionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter
    Friend WithEvents MUESTRACONTNETPorOpcionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACONTNET_PorOpcionTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACONTNET_PorOpcionTableAdapter
    Friend WithEvents MUESTRACONTNETBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACONTNETTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACONTNETTableAdapter
    Friend WithEvents MUESTRACONTNETPorOpcionBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAIPAQUporSOLBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAIPAQU_porSOLTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRAIPAQU_porSOLTableAdapter
    Friend WithEvents Clv_Unicanet As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents NUEIPAQUSOLBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NUEIPAQU_SOLTableAdapter As sofTV.NewSofTvDataSetTableAdapters.NUEIPAQU_SOLTableAdapter
    Friend WithEvents Clv_Unicanet1 As System.Windows.Forms.TextBox
    Friend WithEvents BorIPAQUSOLBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorIPAQU_SOLTableAdapter As sofTV.NewSofTvDataSetTableAdapters.borIPAQU_SOLTableAdapter
    Friend WithEvents DameContratoNetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameContratoNetTableAdapter As sofTV.NewSofTvDataSetTableAdapters.DameContratoNetTableAdapter
    Friend WithEvents BORDetOrdSerBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BORDetOrdSerTableAdapter As sofTV.NewSofTvDataSetTableAdapters.BORDetOrdSerTableAdapter
    Friend WithEvents BORDetOrdSer_INTELIGENTEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BORDetOrdSer_INTELIGENTETableAdapter As sofTV.NewSofTvDataSetTableAdapters.BORDetOrdSer_INTELIGENTETableAdapter
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents BorraMotivoCanServBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorraMotivoCanServTableAdapter As sofTV.DataSetEricTableAdapters.BorraMotivoCanServTableAdapter
    Friend WithEvents GuardaMotivoCanServBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GuardaMotivoCanServTableAdapter As sofTV.DataSetEricTableAdapters.GuardaMotivoCanServTableAdapter
End Class
