Imports System.Data.SqlClient
Imports System.Text
Public Class FrmDescargaMaterialTec
    Dim Ini_metraje As Long = 0
    Dim Fin_Metraje As Long = 0
    Dim cantidad As Integer = 0
    Dim Articulo As Integer = 0
    Dim cantidad_total_tec As Integer = 0

    Private Sub FrmDescargaMaterialTec_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If IsNumeric(ComboBox1.SelectedValue) = True Then
            Clv_Tipo.Text = Me.ComboBox1.SelectedValue
        End If
        'If Me.Clv_Tipo.Text = "1" Then
        '    Me.Label9.Visible = True
        '    Me.Label10.Visible = True
        '    Me.Label11.Visible = True
        '    Me.MetrajeFinTxt.Visible = True
        '    Me.MetrajeIniTxt.Visible = True
        '    Me.Label5.Visible = False
        '    Me.TextBox1.Visible = False
        'Else
        '    Me.Label9.Visible = False
        '    Me.Label10.Visible = False
        '    Me.Label11.Visible = False
        '    Me.MetrajeFinTxt.Visible = False
        '    Me.MetrajeIniTxt.Visible = False
        '    Me.Label5.Visible = True
        '    Me.TextBox1.Visible = True
        'End If

        If ConRel_Session_TecnicosDataGridView.RowCount = 0 Then
            Me.Almacen.Enabled = True
        Else
            Me.Almacen.Enabled = False
        End If

    End Sub




    Private Sub FrmDescargaMaterialTec_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Dim CON7 As New SqlConnection(MiConexion)

        Me.Label9.Visible = False
        Me.Label10.Visible = False
        Me.Label11.Visible = False
        Me.MetrajeFinTxt.Visible = False
        Me.MetrajeIniTxt.Visible = False

        CON7.Open()

        Me.DimeSiTieneunaBitacoraTableAdapter.Connection = CON7
        Me.DimeSiTieneunaBitacoraTableAdapter.Fill(Me.DataSetEdgarRev2.DimeSiTieneunaBitacora, gloClave)


        'Me.Muestra_Detalle_BitacoraTableAdapter.Connection = CON7
        'Me.Muestra_Detalle_BitacoraTableAdapter.Fill(Me.DataSetarnoldo.Muestra_Detalle_Bitacora, Locclv_tec)

        'If IsNumeric(Locclv_tec) = True And IsNumeric(ComboBox1.SelectedValue) And IsNumeric(Locclv_Alm) = True Then
        '    Muestra_Descripcion_Articulo_2(Locclv_tec, CInt(Me.ComboBox1.SelectedValue), Locclv_Alm)
        '    'Me.Muestra_Descripcion_ArticuloTableAdapter.Connection = CON7
        '    'Me.Muestra_Descripcion_ArticuloTableAdapter.Fill(Me.DataSetarnoldo.Muestra_Descripcion_Articulo, Locclv_tec, Me.ComboBox1.SelectedValue)
        'End If

        CON7.Close()
        llena_Detalle()
        Me.OK.Visible = True
        If opcion <> "C" Then
            Me.Button4.Visible = False
            Me.TextBox1.Enabled = True
            Me.ComboBox1.Enabled = True
            Me.ComboBox2.Enabled = True
            Me.ComboBox3.Enabled = True
            MuestraAlmacenes(0)
            If gLOVERgUARDA = 1 Then
                Me.OK.Visible = False
                Me.Button4.Visible = True

            End If
            'Muestra_Metraje()
        End If
        If opcion = "C" Then
            Me.OK.Visible = False
            Me.Button4.Visible = False
            Me.Button1.Enabled = False
            Me.Button3.Enabled = False
            Me.TextBox1.Enabled = False
            Me.ComboBox1.Enabled = False
            Me.ComboBox2.Enabled = False
            Me.ComboBox3.Enabled = False
            MuestraAlmacenes(gloClave)
        End If


        

    End Sub

    Private Sub ComboBox1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.Click

    End Sub

    Private Sub ComboBox1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.LostFocus
        If Me.ComboBox1.SelectedIndex = -1 Then
            Clv_Tipo.Text = 0
        End If

       

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        If IsNumeric(ComboBox1.SelectedValue) = True Then
            Clv_Tipo.Text = Me.ComboBox1.SelectedValue
            Me.ComboBox2.Text = ""
            Me.ComboBox3.Text = ""
            Muestra_Descripcion_Articulo_2(Locclv_tec, Clv_Tipo.Text, Locclv_Alm)
        End If
       
        'If Me.Clv_Tipo.Text = "1" Then
        'Me.Label9.Visible = True
        'Me.Label10.Visible = True
        'Me.Label11.Visible = True
        'Me.MetrajeFinTxt.Visible = True
        'Me.MetrajeIniTxt.Visible = True
        'Me.Label5.Visible = False
        'Me.TextBox1.Visible = False
        'Else

        'Me.Label5.Visible = True
        'Me.TextBox1.Visible = True
        'End If
    End Sub

    Private Sub ComboBox3_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox3.LostFocus
        If Me.ComboBox3.SelectedIndex = -1 Then
            Me.ComboBox2.ResetText()
        End If
    End Sub

    Private Sub ComboBox3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox3.SelectedIndexChanged
        If Me.ComboBox3.SelectedIndex >= 1 Then
            Me.ComboBox2.SelectedIndex = Me.ComboBox3.SelectedIndex
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click

        Dim CON As New SqlConnection(MiConexion)

        Dim num As Integer
        Dim RESP As String
        CON.Open()
        Me.Revisa_Tempo_OrdTableAdapter.Connection = CON
        Me.Revisa_Tempo_OrdTableAdapter.Fill(Me.DataSetLidia.Revisa_Tempo_Ord, clv_sessionTecnico, num)
        CON.Close()
        'CUANDO EL BOTON ACEPTAR ES VISIBLE=================================================
        If num > 0 And Me.Button4.Visible = True Then
            RESP = MsgBox("No se han Guardado los Datos Desea Guardar", MsgBoxStyle.YesNo).ToString
            If RESP = "Yes" Then
                'boton guardar
                CON.Open()

                If eStatusOrdSer = "E" Then

                    Me.Quita_SEBORROART_SALTECTableAdapter.Connection = CON
                    Me.Quita_SEBORROART_SALTECTableAdapter.Fill(Me.DataSetEdgarRev2.Quita_SEBORROART_SALTEC, clv_sessionTecnico)

                    Me.Dame_FolioTableAdapter.Connection = CON
                    Me.Dame_FolioTableAdapter.Fill(Me.DataSetarnoldo.Dame_Folio, gloClave, 1, Locclv_folio)
                    ''----  ALMACENWEB ------*-******
                    'Locclv_folio = Dame_Folio(gloClave, 1, 0)
                    Inserta_Bitacora_tec_2(clv_sessionTecnico, gloClave, Locclv_folio, 1, Locclv_tec, GloUsuario, "P", "", Locclv_Alm)
                    'Me.Inserta_Bitacora_tecTableAdapter.Connection = CON
                    'Me.Inserta_Bitacora_tecTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Bitacora_tec, clv_sessionTecnico, gloClave, Locclv_folio, 1, Locclv_tec, GloUsuario, "P", "", LocNo_Bitacora)

                    ''Inserta_Rel_Bitacora_Orden(LocNo_Bitacora, CInt(gloClave), "O", Contrato)
                    Me.Inserta_Rel_Bitacora_OrdenTableAdapter.Connection = CON
                    Me.Inserta_Rel_Bitacora_OrdenTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Rel_Bitacora_Orden, LocNo_Bitacora, CInt(gloClave))

                    Me.Inserta_RelCobraDescTableAdapter.Connection = CON
                    Me.Inserta_RelCobraDescTableAdapter.Fill(Me.ProcedimientosArnoldo2.Inserta_RelCobraDesc, LocNo_Bitacora, "O")

                End If
                CON.Close()
                MsgBox("Se Guardo con �xito", MsgBoxStyle.Information)
                If Me.ConRel_Session_TecnicosDataGridView.RowCount > 0 Then
                    eActTecnico = False
                Else
                    eActTecnico = True
                End If
            ElseIf RESP = "No" Then
                CON.Open()
                Me.Borra_Inserta_TempoTableAdapter.Connection = CON
                Me.Borra_Inserta_TempoTableAdapter.Fill(Me.DataSetarnoldo.Borra_Inserta_Tempo, clv_sessionTecnico)
                CON.Close()
                'Eric lo hizo, sirve para habilitar o deshabilitar el combo e Vendedores; Lleno de nuevo el grid por
                'Puede que hayan agregado Art pero no quieren salvar.
                llena_Detalle()
                If Me.ConRel_Session_TecnicosDataGridView.RowCount > 0 Then
                    eActTecnico = False
                Else
                    eActTecnico = True
                End If
            End If
        End If
        'CUANDO EL BOTON GUARDAR ES VISIBLE==================================================
        If num > 0 And Me.OK.Visible = True Then
            RESP = MsgBox("No se han Guardado los Datos Desea Guardar", MsgBoxStyle.YesNo).ToString()
            If RESP = "Yes" Then
                'boton Aceptar
                LocValida1 = True
                If Me.ConRel_Session_TecnicosDataGridView.RowCount > 0 Then
                    eActTecnico = False
                Else
                    eActTecnico = True
                End If
            ElseIf RESP = "No" Then
                CON.Open()
                Me.Borra_Inserta_TempoTableAdapter.Connection = CON
                Me.Borra_Inserta_TempoTableAdapter.Fill(Me.DataSetarnoldo.Borra_Inserta_Tempo, clv_sessionTecnico)
                CON.Close()
                'Eric lo hizo, sirve para habilitar o deshabilitar el combo e Vendedores; Lleno de nuevo el grid por
                'Puede que hayan agregado Art pero no quieren salvar.
                llena_Detalle()
                If Me.ConRel_Session_TecnicosDataGridView.RowCount > 0 Then
                    eActTecnico = False
                Else
                    eActTecnico = True
                End If
            End If
        End If

        ' Me.Borra_Inserta_TempoTableAdapter.Fill(Me.DataSetarnoldo.Borra_Inserta_Tempo, clv_sessionTecnico)
        Me.Close()

    End Sub


    Private Sub AGREGAR()

        Dim CON As New SqlConnection(MiConexion)
        Dim bndduplicados As Integer = 0



        Try

            'If Me.Clv_Tipo.Text = "1" Then
            '    If IsNumeric(Me.MetrajeFinTxt.Text) = True And IsNumeric(Me.MetrajeIniTxt.Text) = True Then
            '        Ini_metraje = CLng(Me.MetrajeIniTxt.Text)
            '        Fin_Metraje = CLng(Me.MetrajeFinTxt.Text)

            '        If Ini_metraje > Fin_Metraje Then
            '            MsgBox("El Valor Inicial Debe ser Menor al Final")
            '            Exit Sub
            '        End If
            '        cantidad = Fin_Metraje - Ini_metraje

            '    End If
            'Else
            If IsNumeric(TextBox1.Text) = True Then
                cantidad = CInt(TextBox1.Text)
            Else
                Me.TextBox1.Text = ""
                MsgBox("Capture datos validos ", MsgBoxStyle.Information)
                Exit Sub
            End If
            'End If



            CON.Open()
            Me.ValidaDuplicadoRel_Session_TecnicosTableAdapter.Connection = CON
            Me.ValidaDuplicadoRel_Session_TecnicosTableAdapter.Fill(Me.DataSetEDGAR.ValidaDuplicadoRel_Session_Tecnicos, gloClave, New System.Nullable(Of Long)(CType(clv_sessionTecnico, Long)), CInt(Me.ComboBox2.SelectedValue), bndduplicados)
            CON.Close()
            If bndduplicados = 0 Then


                'CON.Open()
                'Me.ValidaExistenciasTecnicosTableAdapter.Connection = CON
                'Me.ValidaExistenciasTecnicosTableAdapter.Fill(Me.DataSetEDGAR.ValidaExistenciasTecnicos, New System.Nullable(Of Long)(CType(Locclv_tec, Long)), New System.Nullable(Of Long)(CType(Me.TextBox2.Text, Long)), cantidad)
                'CON.Close()
                'If Me.RespuestaTextBox.Text = "4" Then
                SP_existenciasTecnico()
                If cantidad_total_tec >= cantidad Then

                    CON.Open()
                    Me.Inserta_Valores_TempoTableAdapter.Connection = CON
                    Me.Inserta_Valores_TempoTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Valores_Tempo, clv_sessionTecnico, CInt(Me.ComboBox2.SelectedValue), CInt(Me.ComboBox4.SelectedValue), CInt(Me.ComboBox5.SelectedValue), 1, Me.ComboBox3.Text, cantidad, Locclv_tec)
                    CON.Close()
                    Articulo = CInt(Me.ComboBox2.SelectedValue)
                    Me.TextBox1.Clear()
                    llena_Detalle()
                Else
                    Me.TextBox1.Text = ""
                    cantidad = 0
                    MsgBox("No tiene existencias suficientes de este material", MsgBoxStyle.Information)
                End If
            Else
                Me.TextBox1.Text = ""
                cantidad = 0
                MsgBox("El Articulo ya esta en la lista ", MsgBoxStyle.Information)
            End If

            If ConRel_Session_TecnicosDataGridView.RowCount = 0 Then
                Me.Almacen.Enabled = True
            Else
                Me.Almacen.Enabled = False
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        Dim bndduplicados As Integer = 0
        Try
            e.KeyChar = Chr(ValidaKey(Me.TextBox1, Asc(LCase(e.KeyChar)), "N"))
            If e.KeyChar = Chr(13) Then
                AGREGAR()
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        Dim CON As New SqlConnection(MiConexion)
        LocValida1 = False
        If Me.ConRel_Session_TecnicosDataGridView.RowCount > 0 Then
            LocValida1 = True
            'Eric lo hizo, sirve para habilitar o deshabilitar el combo e Vendedores
            If Me.ConRel_Session_TecnicosDataGridView.RowCount > 0 Then
                eActTecnico = False
            Else
                eActTecnico = True
            End If

            'Inserta_Bitacora_Tecnico()
            'Me.Dame_FolioTableAdapter.Fill(Me.DataSetarnoldo.Dame_Folio, gloClave, 1, Locclv_folio)
            ''----  ALMACENWEB ------*-******
            'Locclv_folio = Dame_Folio(gloClave, 1, 0)
            'CON.Open()
            'Me.Inserta_Bitacora_tecTableAdapter.Connection = CON
            'Me.Inserta_Bitacora_tecTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Bitacora_tec, clv_sessionTecnico, gloClave, Locclv_folio, 1, Locclv_tec, GloUsuario, "P", "", LocNo_Bitacora)
            'Me.Inserta_Rel_Bitacora_OrdenTableAdapter.Connection = CON
            'Me.Inserta_Rel_Bitacora_OrdenTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Rel_Bitacora_Orden, LocNo_Bitacora, CInt(gloClave))
            'CON.Close()
            Me.Close()
            Locclv_Alm = CInt(Me.Almacen.SelectedValue)
            'Guarda_Rel_Metraje()
        Else
            MsgBox("La Lista esta Vacia por lo cual no se puede Guardar ", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub llena_Detalle()
        Dim CON As New SqlConnection(MiConexion)
        Try
            CON.Open()
            Me.ConRel_Session_TecnicosTableAdapter.Connection = CON
            Me.ConRel_Session_TecnicosTableAdapter.Fill(Me.DataSetEDGAR.ConRel_Session_Tecnicos, gloClave, New System.Nullable(Of Integer)(CType(Locclv_tec, Integer)), clv_sessionTecnico)
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try


    End Sub

    Private Sub validadupl()


    End Sub

    'Private Sub FillToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FillToolStripButton.Click
    '    Try
    'Dim bnd As Object = BndToolStripTextBox.Text
    '        Me.ValidaDuplicadoRel_Session_TecnicosTableAdapter.Fill(Me.DataSetEDGAR.ValidaDuplicadoRel_Session_Tecnicos, New System.Nullable(Of Long)(CType(Clv_sessionToolStripTextBox.Text, Long)), New System.Nullable(Of Integer)(CType(NoArticuloToolStripTextBox.Text, Integer)), bnd)
    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try

    'End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        AGREGAR()


    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()

        If eStatusOrdSer = "E" Then

            Me.Quita_SEBORROART_SALTECTableAdapter.Connection = CON
            Me.Quita_SEBORROART_SALTECTableAdapter.Fill(Me.DataSetEdgarRev2.Quita_SEBORROART_SALTEC, clv_sessionTecnico)

            Me.Dame_FolioTableAdapter.Connection = CON
            Me.Dame_FolioTableAdapter.Fill(Me.DataSetarnoldo.Dame_Folio, gloClave, 1, Locclv_folio)
            ''----  ALMACENWEB ------*-******
            'Locclv_folio = Dame_Folio(gloClave, 1, 0)
            Inserta_Bitacora_tec_2(clv_sessionTecnico, gloClave, Locclv_folio, 1, Locclv_tec, GloUsuario, "P", "", Locclv_Alm)
            'Me.Inserta_Bitacora_tecTableAdapter.Connection = CON
            'Me.Inserta_Bitacora_tecTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Bitacora_tec, clv_sessionTecnico, gloClave, Locclv_folio, 1, Locclv_tec, GloUsuario, "P", "", LocNo_Bitacora)

            ''Inserta_Rel_Bitacora_Orden(LocNo_Bitacora, CInt(gloClave), "O", Contrato)
            Me.Inserta_Rel_Bitacora_OrdenTableAdapter.Connection = CON
            Me.Inserta_Rel_Bitacora_OrdenTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Rel_Bitacora_Orden, LocNo_Bitacora, CInt(gloClave))

            Me.Inserta_RelCobraDescTableAdapter.Connection = CON
            Me.Inserta_RelCobraDescTableAdapter.Fill(Me.ProcedimientosArnoldo2.Inserta_RelCobraDesc, LocNo_Bitacora, "O")

        End If

        MsgBox("Se Guardo con �xito", MsgBoxStyle.Information)
        CON.Close()
        Me.Close()
    End Sub

    Private Sub ConRel_Session_TecnicosDataGridView_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles ConRel_Session_TecnicosDataGridView.CellClick

    End Sub

    Private Sub ConRel_Session_TecnicosDataGridView_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles ConRel_Session_TecnicosDataGridView.CellContentClick

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If IsNumeric(Me.NoArticuloTextBox.Text) = True Then
            If Me.MovimientoTextBox.Text = "N" Then
                BorraConcepto()
            ElseIf IsNumeric(Me.NoBitacoraTextBox.Text) = True Then
                If Me.NoBitacoraTextBox.Text > 0 Then
                    BorraExistente()
                End If
            End If
            llena_Detalle()
        End If
        'Eric lo hizo, sirve para habilitar o deshabilitar el combo e Vendedores
        If Me.ConRel_Session_TecnicosDataGridView.RowCount > 0 Then
            eActTecnico = False
        Else
            eActTecnico = True
        End If
    End Sub

    Private Sub BorraConcepto()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.Borra_Inserta_Tempo_Por_ServicioTableAdapter.Connection = CON
            Me.Borra_Inserta_Tempo_Por_ServicioTableAdapter.Fill(Me.DataSetEdgarRev2.Borra_Inserta_Tempo_Por_Servicio, New System.Nullable(Of Integer)(CType(clv_sessionTecnico, Integer)), New System.Nullable(Of Long)(CType(Me.NoArticuloTextBox.Text, Long)))
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub BorraExistente()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.INSERTA_Borra_Articulo_BitacoraTableAdapter.Connection = CON
            Me.INSERTA_Borra_Articulo_BitacoraTableAdapter.Fill(Me.DataSetEdgarRev2.INSERTA_Borra_Articulo_Bitacora, clv_sessionTecnico, New System.Nullable(Of Long)(CType(gloClave, Long)), New System.Nullable(Of Long)(CType(Me.NoArticuloTextBox.Text, Long)), New System.Nullable(Of Long)(CType(Locclv_tec, Long)), New System.Nullable(Of Long)(CType(Me.NoBitacoraTextBox.Text, Long)))
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.ConRel_Session_TecnicosDataGridView.Enabled = False
        Me.Panel1.Visible = True
    End Sub


    Private Sub Clv_Tipo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_Tipo.TextChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Locclv_tec) = True And IsNumeric(Clv_Tipo.Text) And IsNumeric(Locclv_Alm) = True Then
            Muestra_Descripcion_Articulo_2(Locclv_tec, Clv_Tipo.Text, Locclv_Alm)
            'Me.Muestra_Descripcion_ArticuloTableAdapter.Connection = CON
            'Me.Muestra_Descripcion_ArticuloTableAdapter.Fill(Me.DataSetarnoldo.Muestra_Descripcion_Articulo, Locclv_tec, Clv_Tipo.Text)
        End If
        CON.Close()
    End Sub

    Private Sub ComboBox2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox2.LostFocus
        If Me.ComboBox2.SelectedIndex = -1 Then
            Me.ComboBox3.ResetText()
        End If
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        Me.ComboBox3.SelectedIndex = Me.ComboBox2.SelectedIndex
    End Sub
    Private Sub Guarda_Rel_Metraje()

        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("Guarda_Rel_Metraje", con)
        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@clv_orden", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = gloClv_Orden
        com.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@contrato", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Input
        par2.Value = Contrato
        com.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@clv_tecnico", SqlDbType.Int)
        par3.Direction = ParameterDirection.Input
        par3.Value = Locclv_tec
        com.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@metrajeIni", SqlDbType.BigInt)
        par4.Direction = ParameterDirection.Input
        par4.Value = Ini_metraje
        com.Parameters.Add(par4)

        Dim par5 As New SqlParameter("@metrajeFin", SqlDbType.BigInt)
        par5.Direction = ParameterDirection.Input
        par5.Value = Fin_Metraje
        com.Parameters.Add(par5)

        Dim par6 As New SqlParameter("@NoArticulo", SqlDbType.Int)
        par6.Direction = ParameterDirection.Input
        par6.Value = Articulo
        com.Parameters.Add(par6)


        Try
            con.Open()
            com.ExecuteNonQuery()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            con.Close()
            con.Dispose()

        End Try

    End Sub
    Private Sub Muestra_Metraje()

        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("Muestra_Metraje", con)
        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@clv_orden", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = gloClv_Orden
        com.Parameters.Add(par1)

        Dim par4 As New SqlParameter("@metrajeIni", SqlDbType.BigInt)
        par4.Direction = ParameterDirection.Output
        com.Parameters.Add(par4)

        Dim par5 As New SqlParameter("@metrajeFin", SqlDbType.BigInt)
        par5.Direction = ParameterDirection.Output
        com.Parameters.Add(par5)

        Try
            con.Open()
            com.ExecuteNonQuery()
            Me.MetrajeIniTxt.Text = par4.Value.ToString
            Me.MetrajeFinTxt.Text = par5.Value.ToString
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            con.Close()
            con.Dispose()

        End Try

    End Sub
    ''''--- IMPLEMENTOS PARA ALMACEN WEB---------------********
    Private Sub SP_existenciasTecnico()

        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("almacenWebDB.dbo.SP_existenciasTecnico", con)
        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@idTecnico", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = Locclv_tec
        com.Parameters.Add(par1)

        Dim par4 As New SqlParameter("@clv_Articulo", SqlDbType.Int)
        par4.Direction = ParameterDirection.Input
        par4.Value = CLng(Me.ComboBox2.SelectedValue)
        com.Parameters.Add(par4)

        Dim par5 As New SqlParameter("@idDevolucion", SqlDbType.Int)
        par5.Direction = ParameterDirection.Input
        par5.Value = 0
        com.Parameters.Add(par5)

        Dim par6 As New SqlParameter("@idBitacora", SqlDbType.Int)
        par6.Direction = ParameterDirection.Input
        par6.Value = 0
        com.Parameters.Add(par6)

        Dim par7 As New SqlParameter("@idModificando", SqlDbType.Int)
        par7.Direction = ParameterDirection.Input
        par7.Value = 0
        com.Parameters.Add(par7)

        Dim par8 As New SqlParameter("@identificador", SqlDbType.Int)
        par8.Direction = ParameterDirection.Output
        com.Parameters.Add(par8)

        Dim par9 As New SqlParameter("@idAlmacenEmpresa", SqlDbType.Int)
        par9.Direction = ParameterDirection.Input
        par9.Value = CInt(Me.Almacen.SelectedValue)
        com.Parameters.Add(par9)


        Try
            con.Open()
            com.ExecuteNonQuery()
            'MsgBox(par8.Value.ToString)
            cantidad_total_tec = CInt(par8.Value)
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            con.Close()
            con.Dispose()

        End Try



    End Sub
    Public Sub Muestra_Descripcion_Articulo_2(ByVal tecnico As Integer, ByVal tipo As Integer, ByVal almacen As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC Muestra_Descripcion_Articulo_2 ")
        strSQL.Append(CStr(tecnico) & ",")
        strSQL.Append(CStr(tipo) & ",")
        strSQL.Append(CStr(almacen))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable

            ComboBox2.Refresh()
            ComboBox3.Refresh()

            ComboBox3.DisplayMember = "Concepto"
            ComboBox3.ValueMember = "realno_articulo"
            ComboBox3.DataSource = bindingSource

            ComboBox2.DisplayMember = "No_Articulo"
            ComboBox2.ValueMember = "realno_articulo"
            ComboBox2.DataSource = bindingSource


        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub
    Private Sub MuestraAlmacenes(ByVal Clv_Orden As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraAlmacenes ")
        strSQL.Append(CStr(Clv_Orden))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.Almacen.DisplayMember = "almacen"
            Me.Almacen.ValueMember = "id"
            Me.Almacen.DataSource = bindingSource

            Locclv_Alm = Me.Almacen.SelectedValue

        Catch ex As Exception
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub
    Public Sub Muestra_Detalle_Bitacora_2(ByVal tecnico As Integer, ByVal almacen As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC Muestra_Detalle_Bitacora_2 ")
        strSQL.Append(CStr(tecnico) & ",")
        strSQL.Append(CStr(almacen))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable


            'ComboBox1.Refresh()

            ComboBox1.DisplayMember = "Concepto"
            ComboBox1.ValueMember = "Clv_Tipo"
            ComboBox1.DataSource = bindingSource

            ComboBox4.DisplayMember = "Clv_Categoria"
            ComboBox4.ValueMember = "Clv_Tipo"
            ComboBox4.DataSource = bindingSource

            ComboBox5.DisplayMember = "Clv_Tipo"
            ComboBox5.ValueMember = "Clv_Tipo"
            ComboBox5.DataSource = bindingSource




        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub
    Private Sub Almacen_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Almacen.SelectedIndexChanged
        If IsNumeric(Locclv_tec) = True And IsNumeric(Me.Almacen.SelectedValue) = True Then
            Me.ComboBox1.Text = ""
            Me.ComboBox2.Text = ""
            Me.ComboBox3.Text = ""
            Locclv_Alm = Me.Almacen.SelectedValue
            Muestra_Detalle_Bitacora_2(Locclv_tec, Me.Almacen.SelectedValue)
            'Me.Muestra_Descripcion_ArticuloTableAdapter.Connection = CON7
            'Me.Muestra_Descripcion_ArticuloTableAdapter.Fill(Me.DataSetarnoldo.Muestra_Descripcion_Articulo, Locclv_tec, Me.ComboBox1.SelectedValue)
        End If
    End Sub

    Private Sub Inserta_Bitacora_tec_2(ByVal Clv_Session As Long, ByVal Clv_Orden As Long, ByRef clv_folio As Integer, ByVal clv_categoria As Integer, ByVal clv_tecnico As Integer, ByVal clv_usuario As String, ByVal Status As String, ByVal obs As String, ByVal idAlmacen As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("Inserta_Bitacora_tec_2", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Session
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_Orden
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@clv_folio", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = clv_folio
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@clv_categoria", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = clv_categoria
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@clv_tecnico", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = clv_tecnico
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@clv_usuario", SqlDbType.VarChar, 50)
        parametro6.Direction = ParameterDirection.Input
        parametro6.Value = clv_usuario
        comando.Parameters.Add(parametro6)

        Dim parametro7 As New SqlParameter("@Status", SqlDbType.VarChar, 50)
        parametro7.Direction = ParameterDirection.Input
        parametro7.Value = Status
        comando.Parameters.Add(parametro7)

        Dim parametro8 As New SqlParameter("@obs", SqlDbType.VarChar, 200)
        parametro8.Direction = ParameterDirection.Input
        parametro8.Value = obs
        comando.Parameters.Add(parametro8)

        Dim parametro9 As New SqlParameter("@no_bitacora", SqlDbType.BigInt)
        parametro9.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro9)

        Dim parametro10 As New SqlParameter("@idAlmacen", SqlDbType.Int)
        parametro10.Direction = ParameterDirection.Input
        parametro10.Value = idAlmacen
        comando.Parameters.Add(parametro10)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            LocNo_Bitacora = CLng(parametro9.Value)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub
End Class
