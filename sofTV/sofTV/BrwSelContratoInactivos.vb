Imports System.Data.SqlClient
Imports System.Text

Public Class BrwSelContratoInactivos


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.DameClientesInactivosDataGridView.RowCount > 0 Then
            If IsNumeric(Me.CONTRATOLabel1.Text) = True Then
                'eContratoRec = Me.CONTRATOLabel1.Text
                tempocontrato = Me.CONTRATOLabel1.Text
            Else
                tempocontrato = 0
                eContratoRec = 0
            End If
            My.Forms.FrmSelPaquete_Reactiva.Show()
            Me.Close()
        Else
            MsgBox("Selecciona un Contrato.", , "Atenci�n")
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub


    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        If Asc(e.KeyChar) = 13 Then
            If IsNumeric(Me.TextBox1.Text) = True Then
                CON.Open()
                'Me.DameClientesInactivosTableAdapter.Connection = CON
                'Me.DameClientesInactivosTableAdapter.Fill(Me.DataSetEric.DameClientesInactivos, Me.TextBox1.Text, "", "", "", "", 0)
                DAMECLIENTESINACTIVOSPORSEPARADO(Me.TextBox1.Text, "", "", "", "", "", "", "", "", 0)
            End If
        End If
        CON.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim CON As New SqlConnection(MiConexion)
        If IsNumeric(Me.TextBox1.Text) = True Then
            CON.Open()
            'Me.DameClientesInactivosTableAdapter.Connection = CON
            'Me.DameClientesInactivosTableAdapter.Fill(Me.DataSetEric.DameClientesInactivos, Me.TextBox1.Text, "", "", "", "", 0)
            DAMECLIENTESINACTIVOSPORSEPARADO(Me.TextBox1.Text, "", "", "", "", "", "", "", "", 0)
        End If
        CON.Close()
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        If Asc(e.KeyChar) = 13 Then
            If Me.TextBox2.Text.Length > 0 Or Len(Trim(Me.APaternoTextBox.Text)) > 0 Or Len(Trim(Me.AMaternoTextBox.Text)) > 0 Then
                CON.Open()
                'Me.DameClientesInactivosTableAdapter.Connection = CON
                'Me.DameClientesInactivosTableAdapter.Fill(Me.DataSetEric.DameClientesInactivos, 0, Me.TextBox2.Text, "", "", "", 1)

                DAMECLIENTESINACTIVOSPORSEPARADO(0, Me.TextBox2.Text, Me.APaternoTextBox.Text, Me.AMaternoTextBox.Text, "", "", "", "", "", 1)
            End If
        End If
        CON.Close()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim CON As New SqlConnection(MiConexion)
        If Me.TextBox2.Text.Length > 0 Or Len(Trim(Me.APaternoTextBox.Text)) > 0 Or Len(Trim(Me.AMaternoTextBox.Text)) > 0 Then
            CON.Open()
            'Me.DameClientesInactivosTableAdapter.Connection = CON
            'Me.DameClientesInactivosTableAdapter.Fill(Me.DataSetEric.DameClientesInactivos, 0, Me.TextBox2.Text, "", "", "", 1)
            DAMECLIENTESINACTIVOSPORSEPARADO(0, Me.TextBox2.Text, Me.APaternoTextBox.Text, Me.AMaternoTextBox.Text, "", "", "", "", "", 1)
        End If
        CON.Close()
    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        If Asc(e.KeyChar) = 13 Then
            If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
            If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
            If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
            CON.Open()
            'Me.DameClientesInactivosTableAdapter.Connection = CON
            'Me.DameClientesInactivosTableAdapter.Fill(Me.DataSetEric.DameClientesInactivos, 0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2)
            DAMECLIENTESINACTIVOSPORSEPARADO(0, "", "", "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, "", "", 2)
        End If
        CON.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Dim CON As New SqlConnection(MiConexion)
        If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
        If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
        If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
        'CON.Open()
        'Me.DameClientesInactivosTableAdapter.Connection = CON
        'Me.DameClientesInactivosTableAdapter.Fill(Me.DataSetEric.DameClientesInactivos, 0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2)
        DAMECLIENTESINACTIVOSPORSEPARADO(0, "", "", "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, "", "", 2)
        'CON.Close()
    End Sub


    Private Sub TextBox4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox4.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        If Asc(e.KeyChar) = 13 Then
            If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
            If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
            If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
            'CON.Open()
            'Me.DameClientesInactivosTableAdapter.Connection = CON
            'Me.DameClientesInactivosTableAdapter.Fill(Me.DataSetEric.DameClientesInactivos, 0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2)
            DAMECLIENTESINACTIVOSPORSEPARADO(0, "", "", "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, "", "", 2)
        End If
        'CON.Close()
    End Sub

    Private Sub TextBox5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox5.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        If Asc(e.KeyChar) = 13 Then
            If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
            If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
            If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
            CON.Open()
            'Me.DameClientesInactivosTableAdapter.Connection = CON
            'Me.DameClientesInactivosTableAdapter.Fill(Me.DataSetEric.DameClientesInactivos, 0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2)
            DAMECLIENTESINACTIVOSPORSEPARADO(0, "", "", "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, "", "", 2)
        End If
        CON.Close()
    End Sub

    Private Sub BrwSelContratoInactivos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CON As New SqlConnection(MiConexion)
        colorea(Me, Me.Name)
        CON.Open()
        'Me.DameClientesInactivosTableAdapter.Connection = CON
        'Me.DameClientesInactivosTableAdapter.Fill(Me.DataSetEric.DameClientesInactivos, 0, "", "", "", "", 3)
        DAMECLIENTESINACTIVOSPORSEPARADO(0, "", "", "", "", "", "", "", "", 3)
        CON.Close()
    End Sub

    Private Sub DameClientesInactivosDataGridView_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DameClientesInactivosDataGridView.CellClick
        CONTRATOLabel1.Text = Me.DameClientesInactivosDataGridView.SelectedCells(0).Value
        NOMBRELabel1.Text = Me.DameClientesInactivosDataGridView.SelectedCells(1).Value
        CALLELabel1.Text = Me.DameClientesInactivosDataGridView.SelectedCells(2).Value
        COLONIALabel1.Text = Me.DameClientesInactivosDataGridView.SelectedCells(3).Value
        NUMEROLabel1.Text = Me.DameClientesInactivosDataGridView.SelectedCells(4).Value
        CIUDADLabel1.Text = Me.DameClientesInactivosDataGridView.SelectedCells(5).Value
    End Sub


    Private Sub DameClientesInactivosDataGridView_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DameClientesInactivosDataGridView.DoubleClick
        If Me.DameClientesInactivosDataGridView.RowCount > 0 Then
            If IsNumeric(Me.CONTRATOLabel1.Text) = True Then
                eContratoRec = Me.CONTRATOLabel1.Text
            Else
                eContratoRec = 0
            End If
            My.Forms.FrmSelPaquete_Reactiva.Show()
            Me.Close()
        Else
            MsgBox("Selecciona un Contrato.", , "Atenci�n")
        End If
    End Sub
    Private Sub DAMECLIENTESINACTIVOSPORSEPARADO(ByVal ContratoCli As Long, ByVal NombreCli As String, ByVal APaternoCli As String, ByVal AMaternoCli As String, _
                                      ByVal CalleCli As String, ByVal NumeroCli As String, ByVal CiudadCli As String, ByVal SetUpBox As String, ByVal Tarjeta As String, ByVal OpCli As Integer)



        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder

        StrSQL.Append("EXEC DameClientesInactivosPORSEPARADO ")
        StrSQL.Append(CStr(ContratoCli) & ",")
        StrSQL.Append("'" & NombreCli & "',")
        StrSQL.Append("'" & APaternoCli & "',")
        StrSQL.Append("'" & AMaternoCli & "',")
        StrSQL.Append("'" & CalleCli & "',")
        StrSQL.Append("'" & NumeroCli & "',")
        StrSQL.Append("'" & CiudadCli & "',")
        StrSQL.Append("'" & SetUpBox & "', ")
        StrSQL.Append("'" & Tarjeta & "', ")
        StrSQL.Append(CStr(OpCli))


        Dim DA As New SqlDataAdapter(StrSQL.ToString, CON)
        Dim DT As New DataTable
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.DameClientesInactivosDataGridView.DataSource = BS.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub DameClientesInactivosDataGridView_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DameClientesInactivosDataGridView.CellContentClick

    End Sub

    Private Sub SOLOINTERNETCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SOLOINTERNETCheckBox.CheckedChanged

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        DAMECLIENTESINACTIVOSPORSEPARADO(0, "", "", "", "", "", "", Me.TxtSetUpBox.Text, Me.TxtTarjeta.Text, 4)
        TxtSetUpBox.Text = ""
        TxtTarjeta.Text = ""

    End Sub
End Class