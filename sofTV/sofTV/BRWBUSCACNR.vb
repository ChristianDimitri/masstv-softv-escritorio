Imports System.Data.SqlClient
Public Class BRWBUSCACNR

    Private Sub BRWBUSCACNR_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        'Me.DamePermisosFormTableAdapter.Fill(Me.NewSofTvDataSet.DamePermisosForm, GloTipoUsuario, Me.Name, 1, glolec, gloescr, gloctr)
        If gloescr = 1 Then
            Me.Button2.Enabled = False
            Me.Button4.Enabled = False
        End If
        Me.BUSCACNRTableAdapter.Connection = CON
        Me.BUSCACNRTableAdapter.Fill(Me.NewSofTvDataSet.BUSCACNR, 0, 0, "", 0, 0, "", "01/01/1900", "01/01/1900", -1)
        CON.Close()
    End Sub

    Private Sub Busca(ByVal op As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If op = 0 Then
            '--Por Consecutivo
            If IsNumeric(Me.TextBox1.Text) = False Then Me.TextBox1.Text = 0
            Me.BUSCACNRTableAdapter.Connection = CON
            Me.BUSCACNRTableAdapter.Fill(Me.NewSofTvDataSet.BUSCACNR, Me.TextBox1.Text, 0, "", 0, 0, "", "01/01/1900", "01/01/1900", op)
        ElseIf op = 1 Then
            '--Por numero_de_contrato
            If IsNumeric(Me.TextBox2.Text) = False Then Me.TextBox2.Text = 0
            Me.BUSCACNRTableAdapter.Connection = CON
            Me.BUSCACNRTableAdapter.Fill(Me.NewSofTvDataSet.BUSCACNR, 0, Me.TextBox2.Text, "", 0, 0, "", "01/01/1900", "01/01/1900", op)
        ElseIf op = 2 Then
            '--Por @mac_addres
            If Len(Trim(Me.TextBox3.Text)) = 0 Then Me.TextBox3.Text = ""
            Me.BUSCACNRTableAdapter.Connection = CON
            Me.BUSCACNRTableAdapter.Fill(Me.NewSofTvDataSet.BUSCACNR, 0, 0, Me.TextBox3.Text, 0, 0, "", "01/01/1900", "01/01/1900", op)
        ElseIf op = 3 Then
            '--Por @resultado
            Dim Resultado As Integer = 0
            Resultado = CLng(Mid(Trim(Me.ComboBox1.Text), 1, 1))
            Me.BUSCACNRTableAdapter.Connection = CON
            Me.BUSCACNRTableAdapter.Fill(Me.NewSofTvDataSet.BUSCACNR, 0, 0, "", Resultado, 0, "", "01/01/1900", "01/01/1900", op)
        ElseIf op = 4 Then
            '--Por @Clv_Orden
            If IsNumeric(Me.TextBox5.Text) = False Then Me.TextBox5.Text = 0
            Me.BUSCACNRTableAdapter.Connection = CON
            Me.BUSCACNRTableAdapter.Fill(Me.NewSofTvDataSet.BUSCACNR, 0, 0, "", 0, Me.TextBox5.Text, "", "01/01/1900", "01/01/1900", op)
        ElseIf op = 5 Then
            '--Por @Status
            Dim Status As String = "P"
            Status = CStr(Mid(Trim(Me.ComboBox2.Text), 1, 1))
            Me.BUSCACNRTableAdapter.Connection = CON
            Me.BUSCACNRTableAdapter.Fill(Me.NewSofTvDataSet.BUSCACNR, 0, 0, "", 0, 0, Status, "01/01/1900", "01/01/1900", op)
        ElseIf op = 6 Then
            '--Por @Fec_Sol
            If IsDate(Me.TextBox7.Text) = True Then
                Me.BUSCACNRTableAdapter.Connection = CON
                Me.BUSCACNRTableAdapter.Fill(Me.NewSofTvDataSet.BUSCACNR, 0, 0, "", 0, 0, "", Me.TextBox7.Text, "01/01/1900", op)
            End If
        ElseIf op = 7 Then
            '--Por @Fec_Eje
            If IsDate(Me.TextBox8.Text) = True Then
                Me.BUSCACNRTableAdapter.Connection = CON
                Me.BUSCACNRTableAdapter.Fill(Me.NewSofTvDataSet.BUSCACNR, 0, 0, "", 0, 0, "", "01/01/1900", Me.TextBox8.Text, op)
            End If
        Else
            Me.BUSCACNRTableAdapter.Connection = CON
            Me.BUSCACNRTableAdapter.Fill(Me.NewSofTvDataSet.BUSCACNR, 0, 0, "", 0, 0, "", "01/01/1900", "01/01/1900", -1)
        End If

        Me.TextBox1.Text = ""
        Me.TextBox2.Text = ""
        Me.TextBox3.Text = ""
        Me.TextBox5.Text = ""
        Me.TextBox7.Text = ""
        Me.TextBox8.Text = ""
        Me.ComboBox1.Text = ""
        Me.ComboBox2.Text = ""
        CON.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Busca(0)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(0)
        End If
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Busca(1)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Busca(2)
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Busca(3)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Busca(4)
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Busca(5)
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Busca(6)
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        Busca(7)
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
        End If
    End Sub

    Private Sub TextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox2.TextChanged

    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
        End If
    End Sub

    Private Sub TextBox3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox3.TextChanged

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Busca(3)
    End Sub

    Private Sub TextBox5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox5.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(4)
        End If
    End Sub

    Private Sub TextBox5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox5.TextChanged

    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        Busca(5)
    End Sub

    Private Sub TextBox7_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox7.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(6)
        End If
    End Sub

    Private Sub TextBox7_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox7.TextChanged

    End Sub

    Private Sub TextBox8_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox8.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(7)
        End If
    End Sub

    Private Sub TextBox8_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox8.TextChanged

    End Sub
End Class