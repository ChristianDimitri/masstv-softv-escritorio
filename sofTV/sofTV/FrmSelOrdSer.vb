Public Class FrmSelOrdSer

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        GloBusca_Numllamada = False
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Select Case GloRepAreaTec
            Case "O"
                If IsNumeric(Me.TextBox1.Text) = False Then GLONUMCLV_ORDEN_INI = 0 Else GLONUMCLV_ORDEN_INI = Me.TextBox1.Text
                If IsNumeric(Me.TextBox2.Text) = False Then GLONUMCLV_ORDEN_FIN = 0 Else GLONUMCLV_ORDEN_FIN = Me.TextBox2.Text
                If GLONUMCLV_ORDEN_FIN > 0 Or GLONUMCLV_ORDEN_INI > 0 Then
                    GloBusca_NumOrden = True
                    Me.TextBox1.Text = ""
                    Me.TextBox2.Text = ""
                    Me.Close()
                Else
                    MsgBox("Captura los Datos Solicitados ", MsgBoxStyle.Information)
                    GloBusca_NumOrden = False
                End If
            Case "Q"
                If IsNumeric(Me.TextBox1.Text) = False Then GLONUMCLVQUEJAS_INI = 0 Else GLONUMCLVQUEJAS_INI = Me.TextBox1.Text
                If IsNumeric(Me.TextBox2.Text) = False Then GLONUMCLVQUEJAS_FIN = 0 Else GLONUMCLVQUEJAS_FIN = Me.TextBox2.Text
                If GLONUMCLVQUEJAS_FIN > 0 Or GLONUMCLVQUEJAS_INI > 0 Then
                    GloBusca_NumQueja = True
                    Me.TextBox1.Text = ""
                    Me.TextBox2.Text = ""
                    Me.Close()
                Else
                    MsgBox("Captura los Datos Solicitados ", MsgBoxStyle.Information)
                    GloBusca_NumQueja = False
                End If
            Case "L"
                If IsNumeric(Me.TextBox1.Text) = False Then GLONUMCLVllamada_INI = 0 Else GLONUMCLVllamada_INI = Me.TextBox1.Text
                If IsNumeric(Me.TextBox2.Text) = False Then GLONUMCLVllamada_FIN = 0 Else GLONUMCLVllamada_FIN = Me.TextBox2.Text
                If GLONUMCLVllamada_FIN > 0 Or GLONUMCLVllamada_INI > 0 Then
                    GloBusca_Numllamada = True
                    Me.TextBox1.Text = ""
                    Me.TextBox2.Text = ""
                    Me.Close()
                Else
                    MsgBox("Captura los Datos Solicitados ", MsgBoxStyle.Information)
                    GloBusca_Numllamada = False
                End If

        End Select

       
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Select Case GloRepAreaTec
            Case "O"
                GLOBNDCLV_ORDEN_INI = True
                GLOBNDCLV_ORDEN_FIN = False
                GLOCLV_oRDENbus = 0
                FrmBuscaOrdSer.Show()
            Case "Q"
                GLOBNDQUEJA_ini = True
                GLOBNDQueja_fin = False
                GLOCLV_oRDENbus = 0
                FrmBuscaQuejas.Show()
            Case "L"
                GLOBNDLLamada_ini = True
                GLOBNDLlamada_fin = False
                GLOCLV_oRDENbus = 0
                My.Forms.BwrLlamadas.Show()
        End Select

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
     
        Select Case GloRepAreaTec
            Case "O"
                GLOBNDCLV_ORDEN_INI = False
                GLOBNDCLV_ORDEN_FIN = True
                GLOCLV_oRDENbus = 0
                FrmBuscaOrdSer.Show()
            Case "Q"
                GLOBNDQUEJA_ini = False
                GLOBNDQueja_fin = True
                GLOCLV_oRDENbus = 0
                GloEjecutadas = 1
                FrmBuscaQuejas.Show()
            Case "L"
                GLOBNDLLamada_ini = False
                GLOBNDLlamada_fin = True
                GLOCLV_oRDENbus = 0
                My.Forms.BwrLlamadas.Show()
        End Select

    End Sub

    Private Sub FrmSelOrdSer_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloRepAreaTec = "O" Then
            Me.Text = "B�squeda por N�mero de Orden De Servicio"
            Me.Label1.Text = "Desde el N�mero de Orden:"
            Me.Label2.Text = "Hasta el N�mero de Orden:"
            If GLOBNDCLV_ORDEN_INI = True Then
                GLOBNDCLV_ORDEN_INI = False
                Me.TextBox1.Text = GLOCLV_oRDENbus
                GLOCLV_oRDENbus = 0
            ElseIf GLOBNDCLV_ORDEN_FIN = True Then
                GLOBNDCLV_ORDEN_FIN = False
                Me.TextBox2.Text = GLOCLV_oRDENbus
                GLOCLV_oRDENbus = 0
            End If
        Else
            If GloRepAreaTec = "Q" Then
                Me.Text = "B�squeda por N�mero de Queja"
                Me.Label1.Text = "Desde el N�mero de Queja:"
                Me.Label2.Text = "Hasta el N�mero de Queja:"
                If GLOBNDQUEJA_ini = True Then
                    GLOBNDQUEJA_ini = False
                    Me.TextBox1.Text = GLOCLV_oRDENbus
                    GLOCLV_oRDENbus = 0
                ElseIf GLOBNDQueja_fin = True Then
                    GLOBNDQueja_fin = False
                    Me.TextBox2.Text = GLOCLV_oRDENbus
                    GLOCLV_oRDENbus = 0
                End If
            End If
        End If
        If GloRepAreaTec = "L" Then
            Me.Text = "B�squeda por N�mero de Llamada"
            Me.Label1.Text = "Desde el N�mero de Llamada:"
            Me.Label2.Text = "Hasta el N�mero de Llamada:"
            If GLOBNDLLamada_ini = True Then
                GLOBNDLLamada_ini = False
                Me.TextBox1.Text = GLOCLV_oRDENbus
                GLOCLV_oRDENbus = 0
            ElseIf GLOBNDLlamada_fin = True Then
                GLOBNDLlamada_fin = False
                Me.TextBox2.Text = GLOCLV_oRDENbus
                GLOCLV_oRDENbus = 0
            End If

        End If
    End Sub

    Private Sub FrmSelOrdSer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        TextBox1.Text = ""
        TextBox2.Text = ""
    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub
End Class