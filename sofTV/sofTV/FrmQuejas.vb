
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Windows.Forms
Imports System.IO

Public Class FrmQuejas
    Private customersByCityReport As ReportDocument
    Dim op As String = Nothing
    Dim Titulo As String = Nothing
    Dim OpcionReporte As Integer
    Dim ImpRepBco As Boolean = False
    Public LocClvClasifProblema As Long = 0
    Public prmDescClasifProblema As String = Nothing


    Private Sub ConfigureCrystalReports(ByVal op As Integer, ByVal Titulo As String)
        'Try
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0", Op6 As String = "0", Op7 As String = "0"
        Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
        Dim Fec1Ini As Date = "01/01/1900", Fec1Fin As Date = "01/01/1900", Fec2Ini As Date = "01/01/1900", Fec2Fin As Date = "01/01/1900"
        Dim Num1 As String = 0, Num2 As String = 0
        Dim nclv_trabajo As String = "0"
        Dim nClv_colonia As String = "0"

        If Me.PendientesCheckBox.CheckState = CheckState.Checked Then
            StatusPen = "1"
        End If
        If Me.EjecutadasCheckBox.CheckState = CheckState.Checked Then
            StatusEje = "1"
        End If
        If Me.VisitaCheckBox.CheckState = CheckState.Checked Then
            StatusVis = "1"
        End If

        If Me.CheckBox1.CheckState = CheckState.Checked Then
            Op1 = "1"
        End If
        If Me.CheckBox2.CheckState = CheckState.Checked Then
            Op2 = "1"
        End If
        If Me.CheckBox3.CheckState = CheckState.Checked Then
            Op3 = "1"
        End If
        If Me.CheckBox4.CheckState = CheckState.Checked Then
            Op4 = "1"
        End If
        If Me.CheckBox5.CheckState = CheckState.Checked Then
            Op5 = "1"
        End If
        If Me.CheckBox6.CheckState = CheckState.Checked Then
            Op6 = "1"
        End If
        If Me.CheckBox7.Checked = True Then
            Op7 = "1"
        End If


        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim mySelectFormula As String = Titulo
        Dim OpOrdenar As String = "0"
        If Me.RadioButton1.Checked = True Then
            OpOrdenar = "0"
        ElseIf Me.RadioButton2.Checked = True Then
            OpOrdenar = "1"
        ElseIf Me.RadioButton4.Checked = True Then
            OpOrdenar = "2"
        End If


        Dim reportPath As String = Nothing
        If op = 0 Then
            reportPath = RutaReportes + "\ReporteQuejasD.rpt"
        ElseIf op = 1 Then
            If IdSistema = "TO" Then
                reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoCabStar.rpt"
            ElseIf IdSistema = "AG" Then
                reportPath = RutaReportes + "\ReporteFormatoQuejasBueno.rpt"
            ElseIf IdSistema = "SA" Then
                reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoTvRey.rpt"
            ElseIf IdSistema = "VA" Then
                reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoCosmo.rpt"
            ElseIf IdSistema = "LO" Or IdSistema = "YU" Then
                reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoLogitel.rpt"
            End If
        ElseIf op = 2 Then
            reportPath = RutaReportes + "\ReporteQuejasD_ListadoNuevo.rpt"
        End If
        'MsgBox(reportPath)
        customersByCityReport.Load(reportPath)
        SetDBLogonForReport(connectionInfo, customersByCityReport)
        '@Clv_TipSer int
        customersByCityReport.SetParameterValue(0, GloClv_TipSer)
        ',@op1 smallint
        customersByCityReport.SetParameterValue(1, Op1)
        ',@op2 smallint
        customersByCityReport.SetParameterValue(2, Op2)
        ',@op3 smallint
        customersByCityReport.SetParameterValue(3, Op3)
        ',@op4 smallint,
        customersByCityReport.SetParameterValue(4, Op4)
        '@op5 smallint
        customersByCityReport.SetParameterValue(5, Op5)
        '@op6 smallint
        customersByCityReport.SetParameterValue(6, Op6)
        ',@StatusPen bit
        customersByCityReport.SetParameterValue(7, StatusPen)
        ',@StatusEje bit
        customersByCityReport.SetParameterValue(8, StatusEje)
        ',@StatusVis bit,
        customersByCityReport.SetParameterValue(9, StatusVis)
        '@Clv_OrdenIni bigint
        If IsNumeric(Me.NUMINILbl.Text) = True Then Num1 = CStr(Me.NUMINILbl.Text)
        customersByCityReport.SetParameterValue(10, Num1)
        ',@Clv_OrdenFin bigint
        If IsNumeric(Me.NUMFINLBL.Text) = True Then Num2 = CStr(Me.NUMFINLBL.Text)
        customersByCityReport.SetParameterValue(11, Num2)
        ',@Fec1Ini Datetime
        If IsDate(Me.FECSOLINI.Text) = True Then Fec1Ini = Me.FECSOLINI.Text
        customersByCityReport.SetParameterValue(12, Fec1Ini)
        ',@Fec1Fin Datetime,
        If IsDate(Me.FECSOLFIN.Text) = True Then Fec1Fin = Me.FECSOLFIN.Text
        customersByCityReport.SetParameterValue(13, Fec1Fin)
        '@Fec2Ini Datetime
        If IsDate(Me.FECEJEINI.Text) = True Then Fec2Ini = Me.FECEJEINI.Text
        customersByCityReport.SetParameterValue(14, Fec2Ini)
        ',@Fec2Fin Datetime
        If IsDate(Me.FECEJEFIN.Text) = True Then Fec2Fin = Me.FECEJEFIN.Text
        customersByCityReport.SetParameterValue(15, Fec2Fin)
        ',@Clv_Trabajo int
        If IsNumeric(Me.CLV_TRABAJO.Text) = True Then nclv_trabajo = CStr(Me.CLV_TRABAJO.Text)
        customersByCityReport.SetParameterValue(16, nclv_trabajo)
        ',@Clv_Colonia int
        If IsNumeric(Me.CLV_COLONIA.Text) = True Then nClv_colonia = CStr(Me.CLV_COLONIA.Text)
        customersByCityReport.SetParameterValue(17, nClv_colonia)
        ',@OpOrden int
        customersByCityReport.SetParameterValue(18, OpOrdenar)
        '@Clv_Departamento
        If IsNumeric(LocclvDepto) = False Then LocclvDepto = "0"
        customersByCityReport.SetParameterValue(19, LocclvDepto)
        '@Op7
        customersByCityReport.SetParameterValue(20, Op7)
        '@Contrato
        If IsNumeric(Me.TextBox1.Text) = False Then Me.TextBox1.Text = 0
        customersByCityReport.SetParameterValue(21, Me.TextBox1.Text)

        'Titulos de Reporte
        If op = 0 Or op = 2 Then
            mySelectFormula = "Listado de Quejas"
            customersByCityReport.DataDefinition.FormulaFields("empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloSucursal & "'"
            'customersByCityReport.DataDefinition.FormulaFields("nomservicio").Text = "'" & Me.ComboBox4.Text & "'"
        ElseIf op = 1 Then
            mySelectFormula = "Quejas " + Me.ComboBox1.Text
            customersByCityReport.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"
        End If
        CrystalReportViewer1.ReportSource = customersByCityReport


        customersByCityReport = Nothing
        'Catch ex As System.Exception
        'System.Windows.Forms.MessageBox.Show(ex.Message)
        ' End Try
    End Sub

    Private Sub ConfigureCrystalReports_NewXml(ByVal op As Integer, ByVal Titulo As String)
        'Try
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        Dim Op1 As Integer = 0, Op2 As Integer = 0, Op3 As Integer = 0, Op4 As Integer = 0, Op5 As Integer = 0, Op6 As Integer = 0, Op7 As Integer = 0
        Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0", StatusProc As String = "0"
        Dim Fec1Ini As Date = "01/01/1900", Fec1Fin As Date = "01/01/1900", Fec2Ini As Date = "01/01/1900", Fec2Fin As Date = "01/01/1900"
        Dim Num1 As String = "0", Num2 As String = "0"
        Dim nclv_trabajo As String = "0"
        Dim nClv_colonia As String = "0"
        Dim eContrato As String = "0"

        If Me.PendientesCheckBox.CheckState = CheckState.Checked Then
            StatusPen = "1"
        End If
        If Me.EjecutadasCheckBox.CheckState = CheckState.Checked Then
            StatusEje = "1"
        End If
        If Me.VisitaCheckBox.CheckState = CheckState.Checked Then
            StatusVis = "1"
        End If
        If Me.cbEnProceso.CheckState = CheckState.Checked Then
            StatusProc = "1"
        End If

        If Me.CheckBox1.CheckState = CheckState.Checked Then
            Op1 = "1"
        End If
        If Me.CheckBox2.CheckState = CheckState.Checked Then
            Op2 = "1"
        End If
        If Me.CheckBox3.CheckState = CheckState.Checked Then
            Op3 = "1"
        End If
        If Me.CheckBox4.CheckState = CheckState.Checked Then
            Op4 = "1"
        End If
        If Me.CheckBox5.CheckState = CheckState.Checked Then
            Op5 = "1"
        End If
        If Me.CheckBox6.CheckState = CheckState.Checked Then
            Op6 = "1"
        End If
        If Me.CheckBox7.Checked = True Then
            Op7 = "1"
        End If


        Dim mySelectFormula As String = Titulo
        Dim OpOrdenar As String = "0"
        If Me.RadioButton1.Checked = True Then
            OpOrdenar = "0"
        ElseIf Me.RadioButton2.Checked = True Then
            OpOrdenar = "1"
        ElseIf Me.RadioButton4.Checked = True Then
            OpOrdenar = "2"
        End If


        Dim reportPath As String = Nothing
        If op = 0 Then
            reportPath = RutaReportes + "\ReporteQuejasD.rpt"

            Dim cnn As New SqlConnection(MiConexion)
            Dim cmd As New SqlCommand("ReporteAreaTecnicaQuejas_ListadoNuevo1", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0

            Dim parametro As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
            parametro.Direction = ParameterDirection.Input
            parametro.Value = GloClv_TipSer
            cmd.Parameters.Add(parametro)

            Dim parametro1 As New SqlParameter("@op1", SqlDbType.SmallInt)
            parametro1.Direction = ParameterDirection.Input
            parametro1.Value = Op1
            cmd.Parameters.Add(parametro1)

            Dim parametro2 As New SqlParameter("@op2", SqlDbType.SmallInt)
            parametro2.Direction = ParameterDirection.Input
            parametro2.Value = Op2
            cmd.Parameters.Add(parametro2)

            Dim parametro3 As New SqlParameter("@op3", SqlDbType.SmallInt)
            parametro3.Direction = ParameterDirection.Input
            parametro3.Value = Op3
            cmd.Parameters.Add(parametro3)

            Dim parametro4 As New SqlParameter("@op4 ", SqlDbType.SmallInt)
            parametro4.Direction = ParameterDirection.Input
            parametro4.Value = Op4
            cmd.Parameters.Add(parametro4)

            Dim parametro5 As New SqlParameter("@op5 ", SqlDbType.SmallInt)
            parametro5.Direction = ParameterDirection.Input
            parametro5.Value = Op5
            cmd.Parameters.Add(parametro5)

            Dim parametro6 As New SqlParameter("@op6", SqlDbType.SmallInt)
            parametro6.Direction = ParameterDirection.Input
            parametro6.Value = Op6
            cmd.Parameters.Add(parametro6)

            Dim parametro7 As New SqlParameter("@StatusPen", SqlDbType.Bit)
            parametro7.Direction = ParameterDirection.Input
            parametro7.Value = CByte(StatusPen)
            cmd.Parameters.Add(parametro7)

            Dim parametro8 As New SqlParameter("@StatusEje", SqlDbType.Bit)
            parametro8.Direction = ParameterDirection.Input
            parametro8.Value = CByte(StatusEje)
            cmd.Parameters.Add(parametro8)

            Dim parametro9 As New SqlParameter("@StatusVis", SqlDbType.Bit)
            parametro9.Direction = ParameterDirection.Input
            parametro9.Value = CByte(StatusVis)
            cmd.Parameters.Add(parametro9)

            Dim parametro23 As New SqlParameter("@StatusProc", SqlDbType.Bit)
            parametro23.Direction = ParameterDirection.Input
            parametro23.Value = CByte(StatusProc)
            cmd.Parameters.Add(parametro23)

            If IsNumeric(Me.NUMINILbl.Text) = True Then Num1 = CStr(Me.NUMINILbl.Text)
            Dim parametro10 As New SqlParameter("@Clv_OrdenIni", SqlDbType.BigInt)
            parametro10.Direction = ParameterDirection.Input
            If ImpRepBco Then
                ImpRepBco = False
                Num1 = -1
            End If
            parametro10.Value = Num1
            cmd.Parameters.Add(parametro10)
            If IsNumeric(Me.NUMFINLBL.Text) = True Then Num2 = CStr(Me.NUMFINLBL.Text)
            Dim parametro11 As New SqlParameter("@Clv_OrdenFin", SqlDbType.BigInt)
            parametro11.Direction = ParameterDirection.Input
            If ImpRepBco Then
                ImpRepBco = False
                Num2 = -1
            End If
            parametro11.Value = Num2
            cmd.Parameters.Add(parametro11)

            If IsDate(Me.FECSOLINI.Text) = True Then Fec1Ini = Me.FECSOLINI.Text
            Dim parametro12 As New SqlParameter("@Fec1Ini", SqlDbType.DateTime)
            parametro12.Direction = ParameterDirection.Input
            parametro12.Value = Fec1Ini
            cmd.Parameters.Add(parametro12)

            If IsDate(Me.FECSOLFIN.Text) = True Then Fec1Fin = Me.FECSOLFIN.Text
            Dim parametro13 As New SqlParameter("@Fec1Fin", SqlDbType.DateTime)
            parametro13.Direction = ParameterDirection.Input
            parametro13.Value = Fec1Fin
            cmd.Parameters.Add(parametro13)

            If IsDate(Me.FECEJEINI.Text) = True Then Fec2Ini = Me.FECEJEINI.Text
            Dim parametro14 As New SqlParameter("@Fec2Ini", SqlDbType.DateTime)
            parametro14.Direction = ParameterDirection.Input
            parametro14.Value = Fec2Ini
            cmd.Parameters.Add(parametro14)

            If IsDate(Me.FECEJEFIN.Text) = True Then Fec2Fin = Me.FECEJEFIN.Text
            Dim parametro15 As New SqlParameter("@Fec2Fin", SqlDbType.DateTime)
            parametro15.Direction = ParameterDirection.Input
            parametro15.Value = Fec2Fin
            cmd.Parameters.Add(parametro15)

            If IsNumeric(Me.CLV_TRABAJO.Text) = True Then nclv_trabajo = CStr(Me.CLV_TRABAJO.Text)
            Dim parametro16 As New SqlParameter("@Clv_Trabajo", SqlDbType.Int)
            parametro16.Direction = ParameterDirection.Input
            parametro16.Value = nclv_trabajo
            cmd.Parameters.Add(parametro16)

            If IsNumeric(Me.CLV_COLONIA.Text) = True Then nClv_colonia = CStr(Me.CLV_COLONIA.Text)
            Dim parametro17 As New SqlParameter("@Clv_Colonia", SqlDbType.Int)
            parametro17.Direction = ParameterDirection.Input
            parametro17.Value = nClv_colonia
            cmd.Parameters.Add(parametro17)

            Dim parametro18 As New SqlParameter("@OpOrden", SqlDbType.Int)
            parametro18.Direction = ParameterDirection.Input
            parametro18.Value = OpOrdenar
            cmd.Parameters.Add(parametro18)

            If IsNumeric(LocclvDepto) = False Then LocclvDepto = "0"
            Dim parametro19 As New SqlParameter("@clv_Depto", SqlDbType.NVarChar)
            parametro19.Direction = ParameterDirection.Input
            parametro19.Value = LocclvDepto
            cmd.Parameters.Add(parametro19)

            Dim parametro20 As New SqlParameter("@Op7", SqlDbType.SmallInt)
            parametro20.Direction = ParameterDirection.Input
            parametro20.Value = Op7
            cmd.Parameters.Add(parametro20)

            If IsNumeric(Me.TextBox1.Text) = False Then Me.TextBox1.Text = 0
            Dim parametro21 As New SqlParameter("@Contrato", SqlDbType.BigInt)
            parametro21.Direction = ParameterDirection.Input
            parametro21.Value = CInt(TextBox1.Text)
            cmd.Parameters.Add(parametro21)

            Dim parametro22 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
            parametro22.Direction = ParameterDirection.Input
            parametro22.Value = LocClv_session
            cmd.Parameters.Add(parametro22)

            Dim parametro24 As New SqlParameter("@clvProblema", SqlDbType.BigInt)
            parametro24.Direction = ParameterDirection.Input
            parametro24.Value = LocClvClasifProblema
            cmd.Parameters.Add(parametro24)

            Dim da As New SqlDataAdapter(cmd)

            Dim ds As New DataSet()
            da.Fill(ds)

            ds.Tables(0).TableName = "ReporteAreaTecnicaQuejas"
            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(ds)
            CrystalReportViewer1.ReportSource = customersByCityReport

            Dim tipoQueja As String
            If StatusEje = "1" Then
                tipoQueja = "0"
            Else
                tipoQueja = "1"
            End If

            'Titulos de Reporte
            If op = 0 Or op = 2 Then
                'If StatusEje = "1" Then
                '    mySelectFormula = "Listado de Quejas Ejecutadas"
                'Else
                '    mySelectFormula = "Listado de Quejas Pendientes"
                'End If
                mySelectFormula = "Listado de Quejas"
                customersByCityReport.DataDefinition.FormulaFields("empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
                customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloSucursal & "'"
                customersByCityReport.DataDefinition.FormulaFields("tipoQueja").Text = "'" & tipoQueja & "'"

                Dim msj = MsgBox("�Desea Exportar el Archivo a Excel?", MsgBoxStyle.YesNo)

                If msj = MsgBoxResult.Yes Then
                    exportaExcel(customersByCityReport)
                End If
            ElseIf op = 1 Then
                mySelectFormula = "Quejas " + Me.ComboBox1.Text
                customersByCityReport.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"
            End If
            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)
        ElseIf op = 1 Then
            If IdSistema = "TO" Then
                reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoCabStar.rpt"
            ElseIf IdSistema = "AG" Then
                reportPath = RutaReportes + "\ReporteFormatoQuejasBueno.rpt"

                If IsDate(Me.FECSOLINI.Text) = True Then Fec1Ini = Me.FECSOLINI.Text
                If IsDate(Me.FECSOLFIN.Text) = True Then Fec1Fin = Me.FECSOLFIN.Text
                If IsDate(Me.FECEJEINI.Text) = True Then Fec2Ini = Me.FECEJEINI.Text
                If IsDate(Me.FECEJEFIN.Text) = True Then Fec2Fin = Me.FECEJEFIN.Text
                If IsNumeric(Me.CLV_TRABAJO.Text) = True Then nclv_trabajo = CStr(Me.CLV_TRABAJO.Text)
                If IsNumeric(Me.CLV_COLONIA.Text) = True Then nClv_colonia = CStr(Me.CLV_COLONIA.Text)
                If IsNumeric(Me.NUMINILbl.Text) = True Then Num1 = CStr(Me.NUMINILbl.Text)
                If IsNumeric(Me.NUMFINLBL.Text) = True Then Num2 = CStr(Me.NUMFINLBL.Text)
                If IsNumeric(Me.TextBox1.Text) = False Then Me.TextBox1.Text = 0
                If IsNumeric(LocclvDepto) = False Then LocclvDepto = "0"
                If IsNumeric(Me.TextBox1.Text) = False Then Me.TextBox1.Text = 0
                If IsNumeric(Me.TextBox1.Text) Then eContrato = CInt(Me.TextBox1.Text)

                If ImpRepBco = True Then
                    ImpRepBco = False
                    Op7 = 1
                    eContrato = 0
                End If

                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@op1", SqlDbType.SmallInt, Op1)
                BaseII.CreateMyParameter("@op2", SqlDbType.SmallInt, Op2)
                BaseII.CreateMyParameter("@op3", SqlDbType.SmallInt, Op3)
                BaseII.CreateMyParameter("@op4", SqlDbType.SmallInt, Op4)
                BaseII.CreateMyParameter("@op5", SqlDbType.SmallInt, Op5)
                BaseII.CreateMyParameter("@op6", SqlDbType.SmallInt, Op6)
                BaseII.CreateMyParameter("@Op7", SqlDbType.SmallInt, Op7)
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, GloClv_TipSer)
                BaseII.CreateMyParameter("@Clv_Trabajo", SqlDbType.Int, nclv_trabajo)
                BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, nClv_colonia)
                BaseII.CreateMyParameter("@OpOrden", SqlDbType.Int, OpOrdenar)
                BaseII.CreateMyParameter("@Fec1Ini", SqlDbType.DateTime, Fec1Ini)
                BaseII.CreateMyParameter("@Fec1Fin", SqlDbType.DateTime, Fec1Fin)
                BaseII.CreateMyParameter("@Fec2Ini", SqlDbType.DateTime, Fec2Ini)
                BaseII.CreateMyParameter("@Fec2Fin", SqlDbType.DateTime, Fec2Fin)
                BaseII.CreateMyParameter("@StatusPen", SqlDbType.Bit, IIf(StatusPen = 1, True, False))
                BaseII.CreateMyParameter("@StatusEje", SqlDbType.Bit, IIf(StatusEje = "1", True, False))
                BaseII.CreateMyParameter("@StatusVis", SqlDbType.Bit, IIf(StatusVis = "1", True, False))
                BaseII.CreateMyParameter("@StatusProc", SqlDbType.Bit, IIf(StatusProc = "1", True, False))
                BaseII.CreateMyParameter("@Clv_OrdenIni", SqlDbType.BigInt, Num1)
                BaseII.CreateMyParameter("@Clv_OrdenFin", SqlDbType.BigInt, Num2)
                BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, eContrato)
                BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, LocClv_session)
                BaseII.CreateMyParameter("@clv_Depto", SqlDbType.VarChar, LocclvDepto, 1)
                BaseII.CreateMyParameter("@clvProblema", SqlDbType.BigInt, LocClvClasifProblema)


                Dim lp As List(Of String) = New List(Of String)
                lp.Add("ReporteAreaTecnicaQuejas1")
                lp.Add("DameDatosGenerales_2")
                Dim ds As DataSet = BaseII.ConsultaDS("ReporteAreaTecnicaQuejas1", lp)

                'ds.Tables(0).TableName = "ReporteAreaTecnicaQuejas1"
                'ds.Tables(1).TableName = "DameDatosGenerales_2"

                'Dim cnn As New SqlConnection(MiConexion)
                'Dim cmd As New SqlCommand("ReporteAreaTecnicaQuejas1", cnn)
                'cmd.CommandType = CommandType.StoredProcedure
                'cmd.CommandTimeout = 0

                'Dim parametro1 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
                'parametro1.Direction = ParameterDirection.Input
                'parametro1.Value = GloClv_TipSer
                'cmd.Parameters.Add(parametro1)
                'Dim parametro2 As New SqlParameter("@op1", SqlDbType.SmallInt)
                'parametro2.Direction = ParameterDirection.Input
                'parametro2.Value = Op1
                'cmd.Parameters.Add(parametro2)

                'Dim parametro3 As New SqlParameter("@op2", SqlDbType.SmallInt)
                'parametro3.Direction = ParameterDirection.Input
                'parametro3.Value = Op2
                'cmd.Parameters.Add(parametro3)

                'Dim parametro4 As New SqlParameter("@op3", SqlDbType.SmallInt)
                'parametro4.Direction = ParameterDirection.Input
                'parametro4.Value = Op3
                'cmd.Parameters.Add(parametro4)

                'Dim parametro5 As New SqlParameter("@op4", SqlDbType.SmallInt)
                'parametro5.Direction = ParameterDirection.Input
                'parametro5.Value = Op4
                'cmd.Parameters.Add(parametro5)

                'Dim parametro6 As New SqlParameter("@op5", SqlDbType.SmallInt)
                'parametro6.Direction = ParameterDirection.Input
                'parametro6.Value = Op5
                'cmd.Parameters.Add(parametro6)

                'Dim parametro22 As New SqlParameter("@op6", SqlDbType.SmallInt)
                'parametro22.Direction = ParameterDirection.Input
                'parametro22.Value = Op6
                'cmd.Parameters.Add(parametro22)

                'Dim parametro7 As New SqlParameter("@StatusPen", SqlDbType.Bit)
                'parametro7.Direction = ParameterDirection.Input
                'If StatusPen = "1" Then
                '    parametro7.Value = True
                'Else
                '    parametro7.Value = False
                'End If
                'cmd.Parameters.Add(parametro7)

                'Dim parametro8 As New SqlParameter("@StatusEje", SqlDbType.Bit)
                'parametro8.Direction = ParameterDirection.Input
                'If StatusEje = "1" Then
                '    parametro8.Value = True
                'Else
                '    parametro8.Value = False
                'End If
                'cmd.Parameters.Add(parametro8)

                'Dim parametro9 As New SqlParameter("@StatusVis", SqlDbType.Bit)
                'parametro9.Direction = ParameterDirection.Input
                'If StatusVis = "1" Then
                '    parametro9.Value = True
                'Else
                '    parametro9.Value = False
                'End If
                'cmd.Parameters.Add(parametro9)
                'Dim parametro24 As New SqlParameter("@StatusProc", SqlDbType.Bit)
                'parametro24.Direction = ParameterDirection.Input
                'If StatusProc = "1" Then
                '    parametro24.Value = True
                'Else
                '    parametro24.Value = False
                'End If
                'cmd.Parameters.Add(parametro24)
                'If IsNumeric(Me.NUMINILbl.Text) = True Then Num1 = CStr(Me.NUMINILbl.Text)
                'Dim parametro10 As New SqlParameter("@Clv_OrdenIni", SqlDbType.BigInt)
                'parametro10.Direction = ParameterDirection.Input
                'parametro10.Value = CLng(Num1)
                'cmd.Parameters.Add(parametro10)
                'If IsNumeric(Me.NUMFINLBL.Text) = True Then Num2 = CStr(Me.NUMFINLBL.Text)
                'Dim parametro11 As New SqlParameter("@Clv_OrdenFin", SqlDbType.BigInt)
                'parametro11.Direction = ParameterDirection.Input
                'parametro11.Value = CLng(Num2)
                'cmd.Parameters.Add(parametro11)
                'If IsDate(Me.FECSOLINI.Text) = True Then Fec1Ini = Me.FECSOLINI.Text
                'Dim parametro12 As New SqlParameter("@Fec1Ini", SqlDbType.DateTime)
                'parametro12.Direction = ParameterDirection.Input
                'parametro12.Value = Fec1Ini
                'cmd.Parameters.Add(parametro12)
                'If IsDate(Me.FECSOLFIN.Text) = True Then Fec1Fin = Me.FECSOLFIN.Text
                'Dim parametro13 As New SqlParameter("@Fec1Fin", SqlDbType.DateTime)
                'parametro13.Direction = ParameterDirection.Input
                'parametro13.Value = Fec1Fin
                'cmd.Parameters.Add(parametro13)
                'If IsDate(Me.FECEJEINI.Text) = True Then Fec2Ini = Me.FECEJEINI.Text
                'Dim parametro14 As New SqlParameter("@Fec2Ini", SqlDbType.DateTime)
                'parametro14.Direction = ParameterDirection.Input
                'parametro14.Value = Fec2Ini
                'cmd.Parameters.Add(parametro14)
                'If IsDate(Me.FECEJEFIN.Text) = True Then Fec2Fin = Me.FECEJEFIN.Text
                'Dim parametro15 As New SqlParameter("@Fec2Fin", SqlDbType.DateTime)
                'parametro15.Direction = ParameterDirection.Input
                'parametro15.Value = Fec2Fin
                'cmd.Parameters.Add(parametro15)
                'If IsNumeric(Me.CLV_TRABAJO.Text) = True Then nclv_trabajo = CStr(Me.CLV_TRABAJO.Text)
                'Dim parametro16 As New SqlParameter("@Clv_Trabajo", SqlDbType.Int)
                'parametro16.Direction = ParameterDirection.Input
                'parametro16.Value = nclv_trabajo
                'cmd.Parameters.Add(parametro16)
                'If IsNumeric(Me.CLV_COLONIA.Text) = True Then nClv_colonia = CStr(Me.CLV_COLONIA.Text)
                'Dim parametro17 As New SqlParameter("@Clv_Colonia", SqlDbType.Int)
                'parametro17.Direction = ParameterDirection.Input
                'parametro17.Value = nClv_colonia
                'cmd.Parameters.Add(parametro17)

                'Dim parametro18 As New SqlParameter("@OpOrden", SqlDbType.Int)
                'parametro18.Direction = ParameterDirection.Input
                'parametro18.Value = OpOrdenar
                'cmd.Parameters.Add(parametro18)
                'If IsNumeric(LocclvDepto) = False Then LocclvDepto = "0"

                'Dim parametro19 As New SqlParameter("@clv_Depto", SqlDbType.Char)
                'parametro19.Direction = ParameterDirection.Input
                'parametro19.Value = LocclvDepto
                'cmd.Parameters.Add(parametro19)

                'Dim parametro20 As New SqlParameter("@Op7", SqlDbType.SmallInt)
                'parametro20.Direction = ParameterDirection.Input
                'parametro20.Value = Op7
                'cmd.Parameters.Add(parametro20)

                'If IsNumeric(Me.TextBox1.Text) = False Then Me.TextBox1.Text = 0
                'Dim parametro21 As New SqlParameter("@Contrato", SqlDbType.BigInt)
                'parametro21.Direction = ParameterDirection.Input
                'parametro21.Value = CLng(eContrato)
                'cmd.Parameters.Add(parametro21)


                'Dim parametro23 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                'parametro23.Direction = ParameterDirection.Input
                'parametro23.Value = LocClv_session
                'cmd.Parameters.Add(parametro23)

                'Dim da As New SqlDataAdapter(cmd)

                'Dim ds As New DataSet()

                'Try
                '    da.Fill(ds)
                'Catch ex As Exception
                '    Dim err As String = ex.Message
                'End Try


                'ds.Tables(0).TableName = "ReporteAreaTecnicaQuejas1"
                'ds.Tables(1).TableName = "DameDatosGenerales_2"

                customersByCityReport.Load(reportPath)
                customersByCityReport.SetDataSource(ds)



            ElseIf IdSistema = "SA" Then
                reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoTvRey.rpt"
            ElseIf IdSistema = "VA" Then
                reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoCosmo.rpt"
            ElseIf IdSistema = "LO" Or IdSistema = "YU" Then
                reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoLogitel.rpt"
            End If
        ElseIf op = 2 Then
            reportPath = RutaReportes + "\ReporteQuejasD_ListadoNuevo.rpt"

            If IsDate(Me.FECSOLINI.Text) = True Then Fec1Ini = Me.FECSOLINI.Text
            If IsDate(Me.FECSOLFIN.Text) = True Then Fec1Fin = Me.FECSOLFIN.Text
            If IsDate(Me.FECEJEINI.Text) = True Then Fec2Ini = Me.FECEJEINI.Text
            If IsDate(Me.FECEJEFIN.Text) = True Then Fec2Fin = Me.FECEJEFIN.Text
            If IsNumeric(Me.CLV_TRABAJO.Text) = True Then nclv_trabajo = CStr(Me.CLV_TRABAJO.Text)
            If IsNumeric(Me.CLV_COLONIA.Text) = True Then nClv_colonia = CStr(Me.CLV_COLONIA.Text)
            If IsNumeric(Me.NUMINILbl.Text) = True Then Num1 = CStr(Me.NUMINILbl.Text)
            If IsNumeric(Me.NUMFINLBL.Text) = True Then Num2 = CStr(Me.NUMFINLBL.Text)
            If IsNumeric(LocclvDepto) = False Then LocclvDepto = "0"
            If IsNumeric(Me.TextBox1.Text) = False Then Me.TextBox1.Text = 0
            If IsNumeric(Me.TextBox1.Text) Then eContrato = CInt(Me.TextBox1.Text)

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@op1", SqlDbType.SmallInt, Op1)
            BaseII.CreateMyParameter("@op2", SqlDbType.SmallInt, Op2)
            BaseII.CreateMyParameter("@op3", SqlDbType.SmallInt, Op3)
            BaseII.CreateMyParameter("@op4", SqlDbType.SmallInt, Op4)
            BaseII.CreateMyParameter("@op5", SqlDbType.SmallInt, Op5)
            BaseII.CreateMyParameter("@op6", SqlDbType.SmallInt, Op6)
            BaseII.CreateMyParameter("@Op7", SqlDbType.SmallInt, Op7)
            BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, GloClv_TipSer)
            BaseII.CreateMyParameter("@Clv_Trabajo", SqlDbType.Int, nclv_trabajo)
            BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, nClv_colonia)
            BaseII.CreateMyParameter("@OpOrden", SqlDbType.Int, OpOrdenar)
            BaseII.CreateMyParameter("@Fec1Ini", SqlDbType.DateTime, Fec1Ini)
            BaseII.CreateMyParameter("@Fec1Fin", SqlDbType.DateTime, Fec1Fin)
            BaseII.CreateMyParameter("@Fec2Ini", SqlDbType.DateTime, Fec2Ini)
            BaseII.CreateMyParameter("@Fec2Fin", SqlDbType.DateTime, Fec2Fin)
            BaseII.CreateMyParameter("@StatusPen", SqlDbType.Bit, CByte(StatusPen))
            BaseII.CreateMyParameter("@StatusEje", SqlDbType.Bit, CByte(StatusEje))
            BaseII.CreateMyParameter("@StatusVis", SqlDbType.Bit, CByte(StatusVis))
            BaseII.CreateMyParameter("@StatusProc", SqlDbType.Bit, CByte(StatusProc))
            BaseII.CreateMyParameter("@Clv_OrdenIni", SqlDbType.BigInt, Num1)
            BaseII.CreateMyParameter("@Clv_OrdenFin", SqlDbType.BigInt, Num2)
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CInt(TextBox1.Text))
            BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, LocClv_session)
            BaseII.CreateMyParameter("@clv_Depto", SqlDbType.VarChar, LocclvDepto)
            BaseII.CreateMyParameter("@clvProblema", SqlDbType.BigInt, LocClvClasifProblema)
            Dim lp As List(Of String) = New List(Of String)
            lp.Add("ReporteAreaTecnicaQuejas_ListadoNuevo")
            Dim ds As DataSet = BaseII.ConsultaDS("ReporteAreaTecnicaQuejas_ListadoNuevo", lp)

            'Dim cnn As New SqlConnection(MiConexion)
            'Dim cmd As New SqlCommand("ReporteAreaTecnicaQuejas_ListadoNuevo", cnn)
            'cmd.CommandType = CommandType.StoredProcedure
            'cmd.CommandTimeout = 0
            'Dim parametro As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
            'parametro.Direction = ParameterDirection.Input
            'parametro.Value = GloClv_TipSer
            'cmd.Parameters.Add(parametro)

            'Dim parametro1 As New SqlParameter("@op1", SqlDbType.SmallInt)
            'parametro1.Direction = ParameterDirection.Input
            'parametro1.Value = Op1
            'cmd.Parameters.Add(parametro1)

            'Dim parametro2 As New SqlParameter("@op2", SqlDbType.SmallInt)
            'parametro2.Direction = ParameterDirection.Input
            'parametro2.Value = Op2
            'cmd.Parameters.Add(parametro2)

            'Dim parametro3 As New SqlParameter("@op3", SqlDbType.SmallInt)
            'parametro3.Direction = ParameterDirection.Input
            'parametro3.Value = Op3
            'cmd.Parameters.Add(parametro3)

            'Dim parametro4 As New SqlParameter("@op4 ", SqlDbType.SmallInt)
            'parametro4.Direction = ParameterDirection.Input
            'parametro4.Value = Op4
            'cmd.Parameters.Add(parametro4)

            'Dim parametro5 As New SqlParameter("@op5 ", SqlDbType.SmallInt)
            'parametro5.Direction = ParameterDirection.Input
            'parametro5.Value = Op5
            'cmd.Parameters.Add(parametro5)

            'Dim parametro6 As New SqlParameter("@op6", SqlDbType.SmallInt)
            'parametro6.Direction = ParameterDirection.Input
            'parametro6.Value = Op6
            'cmd.Parameters.Add(parametro6)

            'Dim parametro7 As New SqlParameter("@StatusPen", SqlDbType.Bit)
            'parametro7.Direction = ParameterDirection.Input
            'parametro7.Value = CByte(StatusPen)
            'cmd.Parameters.Add(parametro7)

            'Dim parametro8 As New SqlParameter("@StatusEje", SqlDbType.Bit)
            'parametro8.Direction = ParameterDirection.Input
            'parametro8.Value = CByte(StatusEje)
            'cmd.Parameters.Add(parametro8)

            'Dim parametro9 As New SqlParameter("@StatusVis", SqlDbType.Bit)
            'parametro9.Direction = ParameterDirection.Input
            'parametro9.Value = CByte(StatusVis)
            'cmd.Parameters.Add(parametro9)

            'Dim parametro23 As New SqlParameter("@StatusProc", SqlDbType.Bit)
            'parametro23.Direction = ParameterDirection.Input
            'parametro23.Value = CByte(StatusProc)
            'cmd.Parameters.Add(parametro23)

            'If IsNumeric(Me.NUMINILbl.Text) = True Then Num1 = CStr(Me.NUMINILbl.Text)
            'Dim parametro10 As New SqlParameter("@Clv_OrdenIni", SqlDbType.BigInt)
            'parametro10.Direction = ParameterDirection.Input
            'parametro10.Value = Num1
            'cmd.Parameters.Add(parametro10)
            'If IsNumeric(Me.NUMFINLBL.Text) = True Then Num2 = CStr(Me.NUMFINLBL.Text)
            'Dim parametro11 As New SqlParameter("@Clv_OrdenFin", SqlDbType.BigInt)
            'parametro11.Direction = ParameterDirection.Input
            'parametro11.Value = Num2
            'cmd.Parameters.Add(parametro11)

            'If IsDate(Me.FECSOLINI.Text) = True Then Fec1Ini = Me.FECSOLINI.Text
            'Dim parametro12 As New SqlParameter("@Fec1Ini", SqlDbType.DateTime)
            'parametro12.Direction = ParameterDirection.Input
            'parametro12.Value = Fec1Ini
            'cmd.Parameters.Add(parametro12)

            'If IsDate(Me.FECSOLFIN.Text) = True Then Fec1Fin = Me.FECSOLFIN.Text
            'Dim parametro13 As New SqlParameter("@Fec1Fin", SqlDbType.DateTime)
            'parametro13.Direction = ParameterDirection.Input
            'parametro13.Value = Fec1Fin
            'cmd.Parameters.Add(parametro13)

            'If IsDate(Me.FECEJEINI.Text) = True Then Fec2Ini = Me.FECEJEINI.Text
            'Dim parametro14 As New SqlParameter("@Fec2Ini", SqlDbType.DateTime)
            'parametro14.Direction = ParameterDirection.Input
            'parametro14.Value = Fec2Ini
            'cmd.Parameters.Add(parametro14)

            'If IsDate(Me.FECEJEFIN.Text) = True Then Fec2Fin = Me.FECEJEFIN.Text
            'Dim parametro15 As New SqlParameter("@Fec2Fin", SqlDbType.DateTime)
            'parametro15.Direction = ParameterDirection.Input
            'parametro15.Value = Fec2Fin
            'cmd.Parameters.Add(parametro15)

            'If IsNumeric(Me.CLV_TRABAJO.Text) = True Then nclv_trabajo = CStr(Me.CLV_TRABAJO.Text)
            'Dim parametro16 As New SqlParameter("@Clv_Trabajo", SqlDbType.Int)
            'parametro16.Direction = ParameterDirection.Input
            'parametro16.Value = nclv_trabajo
            'cmd.Parameters.Add(parametro16)

            'If IsNumeric(Me.CLV_COLONIA.Text) = True Then nClv_colonia = CStr(Me.CLV_COLONIA.Text)
            'Dim parametro17 As New SqlParameter("@Clv_Colonia", SqlDbType.Int)
            'parametro17.Direction = ParameterDirection.Input
            'parametro17.Value = nClv_colonia
            'cmd.Parameters.Add(parametro17)

            'Dim parametro18 As New SqlParameter("@OpOrden", SqlDbType.Int)
            'parametro18.Direction = ParameterDirection.Input
            'parametro18.Value = OpOrdenar
            'cmd.Parameters.Add(parametro18)

            'If IsNumeric(LocclvDepto) = False Then LocclvDepto = "0"

            'Dim parametro19 As New SqlParameter("@clv_Depto", SqlDbType.VarChar)
            'parametro19.Direction = ParameterDirection.Input
            'parametro19.Value = LocclvDepto
            'cmd.Parameters.Add(parametro19)

            'Dim parametro20 As New SqlParameter("@Op7", SqlDbType.SmallInt)
            'parametro20.Direction = ParameterDirection.Input
            'parametro20.Value = Op7
            'cmd.Parameters.Add(parametro20)


            'If IsNumeric(Me.TextBox1.Text) = False Then Me.TextBox1.Text = 0
            'Dim parametro21 As New SqlParameter("@Contrato", SqlDbType.BigInt)
            'parametro21.Direction = ParameterDirection.Input
            'parametro21.Value = CInt(TextBox1.Text)
            'cmd.Parameters.Add(parametro21)

            'Dim parametro22 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
            'parametro22.Direction = ParameterDirection.Input
            'parametro22.Value = LocClv_session
            'cmd.Parameters.Add(parametro22)

            'Dim da As New SqlDataAdapter(cmd)
            'Dim ds As New DataSet()
            'Dim data1 As New DataTable()

            'da.Fill(data1)
            'data1.TableName = "ReporteAreaTecnicaQuejas_ListadoNuevo"
            'ds.Tables.Add(data1)


            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(ds)
            'CrystalReportViewer1.ReportSource = customersByCityReport
            'CrystalReportViewer1.Zoom(75)

        End If


        'Titulos de Reporte
        If op = 0 Or op = 2 Then
            mySelectFormula = "Listado de Quejas"
            customersByCityReport.DataDefinition.FormulaFields("empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloSucursal & "'"

        End If
        CrystalReportViewer1.ReportSource = customersByCityReport
        CrystalReportViewer1.Zoom(75)
        ' End Try
    End Sub
    Private Sub FrmQuejas_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Activar()
    End Sub

    Private Sub Activar()
        If GloBusca_NumQueja = True Then
            GloBusca_NumQueja = False
            Me.NUMFINLBL.Text = GLONUMCLVQUEJAS_FIN
            Me.NUMINILbl.Text = GLONUMCLVQUEJAS_INI
            GLONUMCLVQUEJAS_FIN = 0
            GLONUMCLVQUEJAS_INI = 0
        End If
        If op = "1" And GloBndSelFecha = True Then
            GloBndSelFecha = False
            Me.FECSOLINI.Text = GloFecha_Ini
            Me.FECSOLFIN.Text = GloFecha_Fin
            GloFecha_Ini = "01/01/1900"
            GloFecha_Fin = "01/01/1900"
        End If
        If op = "2" And GloBndSelFecha = True Then
            GloBndSelFecha = False
            Me.FECEJEINI.Text = GloFecha_Ini
            Me.FECEJEFIN.Text = GloFecha_Fin
            GloFecha_Ini = "01/01/1900"
            GloFecha_Fin = "01/01/1900"
        End If
        If GloBndTrabajo = True Then
            GloBndTrabajo = False
            Me.CLV_TRABAJO.Text = GloNumClv_Trabajo
            Me.NOMTRABAJO.Text = GLONOMTRABAJO
        End If
        If GlobndClv_Colonia = True Then
            GlobndClv_Colonia = False
            Me.CLV_COLONIA.Text = GloNumClv_Colonia
            Me.NOMCOLONIA.Text = GLONOMCOLONIA
        End If
        If LocGlobndDpto = True Then
            LocGlobndDpto = False
            Me.Label8.Text = LocNomDepto
        End If

        If BanderaAreaQuejas = True Then
            ' FrmSelSector.Show()
            BanderaAreaQuejas = False
            ConfigureCrystalReports_NewXml(OpcionReporte, "")
        End If
        '    Case 2
        'If BanderaAreaQuejas = False Then
        '    'FrmSelSector.Show()
        '    ConfigureCrystalReports_NewXml(0, "")
        'End If
        'End Select
    End Sub

    Private Sub FrmQuejas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.Label9.ForeColor = Color.Black
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MuestraTipSerPrincipal2TableAdapter.Connection = CON
        Me.MuestraTipSerPrincipal2TableAdapter.Fill(Me.DataSetLidia2.MuestraTipSerPrincipal2)
        CON.Close()
        If IsNumeric(Me.ComboBox1.SelectedValue) = True Then
            GloClv_TipSer = Me.ComboBox1.SelectedValue
            LIMPIA()
        End If
    End Sub


    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub SetDBLogonForReport2(ByVal myConnectionInfo As ConnectionInfo)
        Dim myTableLogOnInfos As TableLogOnInfos = Me.CrystalReportViewer1.LogOnInfo
        For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
            myTableLogOnInfo.ConnectionInfo = myConnectionInfo
        Next
    End Sub

    Private Sub LIMPIA()
        Me.CheckBox1.CheckState = CheckState.Unchecked
        Me.CheckBox2.CheckState = CheckState.Unchecked
        Me.CheckBox3.CheckState = CheckState.Unchecked
        Me.CheckBox5.CheckState = CheckState.Unchecked
        Me.CheckBox4.CheckState = CheckState.Unchecked
        Me.CheckBox6.CheckState = CheckState.Unchecked
        Me.NUMFINLBL.Text = 0
        Me.NUMINILbl.Text = 0
        Me.FECSOLFIN.Text = ""
        Me.FECSOLINI.Text = ""
        Me.FECEJEFIN.Text = ""
        Me.FECEJEINI.Text = ""
        Me.CLV_TRABAJO.Text = 0
        Me.NOMTRABAJO.Text = ""
        Me.CLV_COLONIA.Text = 0
        Me.NOMCOLONIA.Text = ""
        Me.Label8.Text = ""
    End Sub

    'Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.SelectedIndexChanged
    '    If IsNumeric(Me.ComboBox4.SelectedValue) = True Then
    '        GloClv_TipSer = Me.ComboBox4.SelectedValue
    '        LIMPIA()
    '        '--Me.CatalogodeReportesAreaTecnicaTableAdapter.Fill(Me.NewsoftvDataSet1.CatalogodeReportesAreaTecnica, Me.ComboBox4.SelectedValue, 0)
    '    End If
    'End Sub

    Private Sub PendientesCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PendientesCheckBox.CheckedChanged
        If Me.PendientesCheckBox.CheckState = CheckState.Checked Then
            GloPendientes = 1
            Me.EjecutadasCheckBox.CheckState = CheckState.Unchecked
            Me.VisitaCheckBox.CheckState = CheckState.Unchecked
            Me.cbEnProceso.CheckState = CheckState.Unchecked
        Else
            GloPendientes = 0
        End If
    End Sub

    Private Sub EjecutadasCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EjecutadasCheckBox.CheckedChanged
        If Me.EjecutadasCheckBox.CheckState = CheckState.Checked Then
            GloEjecutadas = 1
            Me.PendientesCheckBox.CheckState = CheckState.Unchecked
            Me.VisitaCheckBox.CheckState = CheckState.Unchecked
            Me.cbEnProceso.CheckState = CheckState.Unchecked
        Else
            GloEjecutadas = 0
        End If
    End Sub

    Private Sub VisitaCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VisitaCheckBox.CheckedChanged
        If Me.VisitaCheckBox.CheckState = CheckState.Checked Then
            GloVisita = 1
            Me.PendientesCheckBox.CheckState = CheckState.Unchecked
            Me.EjecutadasCheckBox.CheckState = CheckState.Unchecked
            Me.cbEnProceso.CheckState = CheckState.Unchecked
        Else
            GloVisita = 0
        End If
    End Sub


    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If Me.CheckBox1.CheckState = CheckState.Checked Then
            op = "0"
            FrmSelOrdSer.Show()
        Else
            Me.NUMINILbl.Text = 0
            Me.NUMFINLBL.Text = 0
        End If
    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If Me.CheckBox2.CheckState = CheckState.Checked Then
            op = "1"
            FrmSelFechas.Show()
        Else
            Me.FECSOLFIN.Text = ""
            Me.FECSOLINI.Text = ""
        End If
    End Sub

    Private Sub CheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox3.CheckedChanged
        If Me.CheckBox3.CheckState = CheckState.Checked Then
            op = "2"
            FrmSelFechas.Show()
        Else
            Me.FECEJEINI.Text = ""
            Me.FECEJEFIN.Text = ""
        End If
    End Sub

    Private Sub CheckBox4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox4.CheckedChanged
        If Me.CheckBox4.CheckState = CheckState.Checked Then
            op = "3"
            FrmSelTrabajo.Show()
        Else
            Me.CLV_TRABAJO.Text = 0
            Me.NOMTRABAJO.Text = ""
        End If
    End Sub

    Private Sub CheckBox5_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox5.CheckedChanged
        If Me.CheckBox5.CheckState = CheckState.Checked Then
            op = "4"
            FrmSelColonia.Show()
        Else
            Me.CLV_COLONIA.Text = 0
            Me.NOMCOLONIA.Text = ""
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'ConfigureCrystalReports_NewXml(0, "")
        OpcionReporte = 0
        LocClv_session = DAMESclv_Sessionporfavor()
        BanderaAreaQuejas = True
        FrmSelSector.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'ConfigureCrystalReports_NewXml(1, "")
        OpcionReporte = 1
        LocClv_session = DAMESclv_Sessionporfavor()
        BanderaAreaQuejas = True
        FrmSelSector.Show()
    End Sub

    Private Sub CheckBox6_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox6.CheckedChanged
        If Me.CheckBox6.CheckState = CheckState.Checked Then
            op = "5"
            FrmDepartamento.Show()
        Else
            Me.Label8.Text = ""
        End If
    End Sub


    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'ConfigureCrystalReports(2, "")
        OpcionReporte = 2
        LocClv_session = DAMESclv_Sessionporfavor()
        BanderaAreaQuejas = True
        FrmSelSector.Show()

    End Sub

    Private Sub CheckBox7_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox7.CheckedChanged
        If Me.CheckBox7.Checked = True Then
            Me.TextBox1.Enabled = True

        Else
            Me.TextBox1.Enabled = False

        End If
    End Sub


    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        If IsNumeric(Me.ComboBox1.SelectedValue) = True Then
            GloClv_TipSer = Me.ComboBox1.SelectedValue
            LIMPIA()
        End If
    End Sub

    Private Sub cbEnProceso_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbEnProceso.CheckedChanged
        If Me.cbEnProceso.CheckState = CheckState.Checked Then
            GloEnProceso = 1
            Me.PendientesCheckBox.CheckState = CheckState.Unchecked
            Me.EjecutadasCheckBox.CheckState = CheckState.Unchecked
            Me.VisitaCheckBox.CheckState = CheckState.Unchecked
        Else
            GloEnProceso = 0
        End If
    End Sub

    Private Sub btnImpFormato_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImpFormato.Click
        OpcionReporte = 1
        ImpRepBco = True
        LocClv_session = DAMESclv_Sessionporfavor()
        BanderaAreaQuejas = True
        Activar()
    End Sub

    Private Sub cbxClasifPorblema_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxClasifPorblema.CheckedChanged
        If Me.cbxClasifPorblema.CheckState = CheckState.Checked Then
            FrmProblema.Show()
        Else
            Me.lblProblemaSeleccionado.Text = ""
            LocClvClasifProblema = 0
        End If
    End Sub

    Private Sub exportaExcel(ByRef prmReporte As ReportDocument)
        Try
            Dim saveFileDialog1 As New SaveFileDialog()
            Dim CrExportOptions As ExportOptions
            Dim CrDiskFileDestinationOptions As New DiskFileDestinationOptions()
            Dim CrFormatTypeOptions As New ExcelFormatOptions

            saveFileDialog1.Filter = "Excel File|*.xls"
            saveFileDialog1.Title = "Exportar Reporte a Excel"
            saveFileDialog1.FilterIndex = 2
            saveFileDialog1.RestoreDirectory = True

            If saveFileDialog1.ShowDialog() = DialogResult.OK Then
                'CrDiskFileDestinationOptions.DiskFileName = "D:\EXES\crystalExport.xls"
                CrDiskFileDestinationOptions.DiskFileName = saveFileDialog1.FileName
                CrExportOptions = prmReporte.ExportOptions
                With CrExportOptions
                    .ExportDestinationType = ExportDestinationType.DiskFile
                    .ExportFormatType = ExportFormatType.Excel
                    .DestinationOptions = CrDiskFileDestinationOptions
                    .FormatOptions = CrFormatTypeOptions
                End With
                prmReporte.Export()

                If Comprobar("Excel.Application") = True Then
                    System.Diagnostics.Process.Start(saveFileDialog1.FileName)
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Function Comprobar(ByVal Clase_Application As String) As Boolean
        Dim Objeto As Object

        ' Deshabilitar errores temporalmente  
        On Error Resume Next

        ' -- Crear una referencia al objeto  
        Objeto = CreateObject(Clase_Application)

        ' -- No d�o error  
        If Err.Number <> 0 Then
            Comprobar = False
        Else
            ' .. error  
            Comprobar = True
            ' -- Eliminar  referencia  
            Objeto = Nothing
        End If
        ' -- Limpiar error  
        On Error GoTo 0
    End Function
End Class