<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelColonia_Rep21
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.ListBox3 = New System.Windows.Forms.ListBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ListBox2 = New System.Windows.Forms.ListBox()
        Me.MuestraSeleccionColoniaCONSULTABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.MuestraSeleccionaColoniasTmpCONSULTABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraColoniaRep2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetarnoldo = New sofTV.DataSetarnoldo()
        Me.Inserta_Seleccion_ColoniaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Seleccion_ColoniaTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Inserta_Seleccion_ColoniaTableAdapter()
        Me.Muestra_Colonia_Rep2TableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_Colonia_Rep2TableAdapter()
        Me.MuestraSelecciona_ColoniaTmpNUEVOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraSelecciona_ColoniaTmpNUEVOTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.MuestraSelecciona_ColoniaTmpNUEVOTableAdapter()
        Me.MuestraSelecciona_ColoniasTmpCONSULTATableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.MuestraSelecciona_ColoniasTmpCONSULTATableAdapter()
        Me.MuestraSeleccion_ColoniaCONSULTATableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.MuestraSeleccion_ColoniaCONSULTATableAdapter()
        Me.InsertaTOSelecciona_ColoniasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertaTOSelecciona_ColoniasTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.InsertaTOSelecciona_ColoniasTableAdapter()
        Me.InsertaTOSelecciona_ColoniasTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertaTOSelecciona_ColoniasTmpTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.InsertaTOSelecciona_ColoniasTmpTableAdapter()
        Me.Insertauno_Seleccion_ColoniaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Insertauno_Seleccion_ColoniaTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Insertauno_Seleccion_ColoniaTableAdapter()
        Me.Insertauno_Seleccion_ColoniatmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Insertauno_Seleccion_ColoniatmpTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Insertauno_Seleccion_ColoniatmpTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        CType(Me.MuestraSeleccionColoniaCONSULTABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraSeleccionaColoniasTmpCONSULTABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraColoniaRep2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Seleccion_ColoniaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraSelecciona_ColoniaTmpNUEVOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertaTOSelecciona_ColoniasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertaTOSelecciona_ColoniasTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Insertauno_Seleccion_ColoniaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Insertauno_Seleccion_ColoniatmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel2.Location = New System.Drawing.Point(364, 9)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(185, 15)
        Me.CMBLabel2.TabIndex = 64
        Me.CMBLabel2.Text = "Colonia(s) Seleccionada(s):"
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(12, 9)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(183, 15)
        Me.CMBLabel1.TabIndex = 63
        Me.CMBLabel1.Text = "Seleccione la(s) colonia(s):"
        '
        'ListBox3
        '
        Me.ListBox3.FormattingEnabled = True
        Me.ListBox3.Location = New System.Drawing.Point(11, 260)
        Me.ListBox3.Name = "ListBox3"
        Me.ListBox3.ScrollAlwaysVisible = True
        Me.ListBox3.Size = New System.Drawing.Size(202, 43)
        Me.ListBox3.TabIndex = 62
        Me.ListBox3.TabStop = False
        Me.ListBox3.Visible = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(471, 267)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 36)
        Me.Button5.TabIndex = 59
        Me.Button5.Text = "&Cancelar"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkOrange
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(292, 267)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(136, 36)
        Me.Button6.TabIndex = 58
        Me.Button6.Text = "&Aceptar"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkOrange
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(258, 154)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 57
        Me.Button4.Text = "<<"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(258, 125)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 56
        Me.Button3.Text = "<"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(258, 57)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 55
        Me.Button2.Text = ">>"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(258, 28)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 54
        Me.Button1.Text = ">"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'ListBox2
        '
        Me.ListBox2.DisplayMember = "Nombre"
        Me.ListBox2.FormattingEnabled = True
        Me.ListBox2.Location = New System.Drawing.Point(365, 28)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.ScrollAlwaysVisible = True
        Me.ListBox2.Size = New System.Drawing.Size(209, 225)
        Me.ListBox2.TabIndex = 61
        Me.ListBox2.TabStop = False
        Me.ListBox2.ValueMember = "clv_colonia"
        '
        'MuestraSeleccionColoniaCONSULTABindingSource
        '
        Me.MuestraSeleccionColoniaCONSULTABindingSource.DataMember = "MuestraSeleccion_ColoniaCONSULTA"
        Me.MuestraSeleccionColoniaCONSULTABindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ListBox1
        '
        Me.ListBox1.DisplayMember = "Nombre"
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Location = New System.Drawing.Point(11, 27)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.ScrollAlwaysVisible = True
        Me.ListBox1.Size = New System.Drawing.Size(202, 225)
        Me.ListBox1.TabIndex = 60
        Me.ListBox1.TabStop = False
        Me.ListBox1.ValueMember = "clv_colonia"
        '
        'MuestraSeleccionaColoniasTmpCONSULTABindingSource
        '
        Me.MuestraSeleccionaColoniasTmpCONSULTABindingSource.DataMember = "MuestraSelecciona_ColoniasTmpCONSULTA"
        Me.MuestraSeleccionaColoniasTmpCONSULTABindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'MuestraColoniaRep2BindingSource
        '
        Me.MuestraColoniaRep2BindingSource.DataMember = "Muestra_Colonia_Rep2"
        Me.MuestraColoniaRep2BindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'DataSetarnoldo
        '
        Me.DataSetarnoldo.DataSetName = "DataSetarnoldo"
        Me.DataSetarnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Inserta_Seleccion_ColoniaBindingSource
        '
        Me.Inserta_Seleccion_ColoniaBindingSource.DataMember = "Inserta_Seleccion_Colonia"
        Me.Inserta_Seleccion_ColoniaBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Inserta_Seleccion_ColoniaTableAdapter
        '
        Me.Inserta_Seleccion_ColoniaTableAdapter.ClearBeforeFill = True
        '
        'Muestra_Colonia_Rep2TableAdapter
        '
        Me.Muestra_Colonia_Rep2TableAdapter.ClearBeforeFill = True
        '
        'MuestraSelecciona_ColoniaTmpNUEVOBindingSource
        '
        Me.MuestraSelecciona_ColoniaTmpNUEVOBindingSource.DataMember = "MuestraSelecciona_ColoniaTmpNUEVO"
        Me.MuestraSelecciona_ColoniaTmpNUEVOBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'MuestraSelecciona_ColoniaTmpNUEVOTableAdapter
        '
        Me.MuestraSelecciona_ColoniaTmpNUEVOTableAdapter.ClearBeforeFill = True
        '
        'MuestraSelecciona_ColoniasTmpCONSULTATableAdapter
        '
        Me.MuestraSelecciona_ColoniasTmpCONSULTATableAdapter.ClearBeforeFill = True
        '
        'MuestraSeleccion_ColoniaCONSULTATableAdapter
        '
        Me.MuestraSeleccion_ColoniaCONSULTATableAdapter.ClearBeforeFill = True
        '
        'InsertaTOSelecciona_ColoniasBindingSource
        '
        Me.InsertaTOSelecciona_ColoniasBindingSource.DataMember = "InsertaTOSelecciona_Colonias"
        Me.InsertaTOSelecciona_ColoniasBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'InsertaTOSelecciona_ColoniasTableAdapter
        '
        Me.InsertaTOSelecciona_ColoniasTableAdapter.ClearBeforeFill = True
        '
        'InsertaTOSelecciona_ColoniasTmpBindingSource
        '
        Me.InsertaTOSelecciona_ColoniasTmpBindingSource.DataMember = "InsertaTOSelecciona_ColoniasTmp"
        Me.InsertaTOSelecciona_ColoniasTmpBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'InsertaTOSelecciona_ColoniasTmpTableAdapter
        '
        Me.InsertaTOSelecciona_ColoniasTmpTableAdapter.ClearBeforeFill = True
        '
        'Insertauno_Seleccion_ColoniaBindingSource
        '
        Me.Insertauno_Seleccion_ColoniaBindingSource.DataMember = "Insertauno_Seleccion_Colonia"
        Me.Insertauno_Seleccion_ColoniaBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Insertauno_Seleccion_ColoniaTableAdapter
        '
        Me.Insertauno_Seleccion_ColoniaTableAdapter.ClearBeforeFill = True
        '
        'Insertauno_Seleccion_ColoniatmpBindingSource
        '
        Me.Insertauno_Seleccion_ColoniatmpBindingSource.DataMember = "Insertauno_Seleccion_Coloniatmp"
        Me.Insertauno_Seleccion_ColoniatmpBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Insertauno_Seleccion_ColoniatmpTableAdapter
        '
        Me.Insertauno_Seleccion_ColoniatmpTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'FrmSelColonia_Rep21
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(616, 328)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.ListBox3)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ListBox2)
        Me.Controls.Add(Me.ListBox1)
        Me.Name = "FrmSelColonia_Rep21"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selección Colonia."
        CType(Me.MuestraSeleccionColoniaCONSULTABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraSeleccionaColoniasTmpCONSULTABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraColoniaRep2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Seleccion_ColoniaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraSelecciona_ColoniaTmpNUEVOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertaTOSelecciona_ColoniasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertaTOSelecciona_ColoniasTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Insertauno_Seleccion_ColoniaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Insertauno_Seleccion_ColoniatmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents ListBox3 As System.Windows.Forms.ListBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents DataSetarnoldo As sofTV.DataSetarnoldo
    Friend WithEvents Inserta_Seleccion_ColoniaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Seleccion_ColoniaTableAdapter As sofTV.DataSetarnoldoTableAdapters.Inserta_Seleccion_ColoniaTableAdapter
    Friend WithEvents MuestraColoniaRep2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents Muestra_Colonia_Rep2TableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_Colonia_Rep2TableAdapter
    Friend WithEvents MuestraSelecciona_ColoniaTmpNUEVOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraSelecciona_ColoniaTmpNUEVOTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.MuestraSelecciona_ColoniaTmpNUEVOTableAdapter
    Friend WithEvents MuestraSeleccionColoniaCONSULTABindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraSeleccionaColoniasTmpCONSULTABindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraSelecciona_ColoniasTmpCONSULTATableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.MuestraSelecciona_ColoniasTmpCONSULTATableAdapter
    Friend WithEvents MuestraSeleccion_ColoniaCONSULTATableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.MuestraSeleccion_ColoniaCONSULTATableAdapter
    Friend WithEvents InsertaTOSelecciona_ColoniasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertaTOSelecciona_ColoniasTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.InsertaTOSelecciona_ColoniasTableAdapter
    Friend WithEvents InsertaTOSelecciona_ColoniasTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertaTOSelecciona_ColoniasTmpTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.InsertaTOSelecciona_ColoniasTmpTableAdapter
    Friend WithEvents Insertauno_Seleccion_ColoniaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Insertauno_Seleccion_ColoniaTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Insertauno_Seleccion_ColoniaTableAdapter
    Friend WithEvents Insertauno_Seleccion_ColoniatmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Insertauno_Seleccion_ColoniatmpTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Insertauno_Seleccion_ColoniatmpTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
