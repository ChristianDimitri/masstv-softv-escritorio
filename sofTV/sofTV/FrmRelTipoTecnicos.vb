﻿Public Class FrmRelTipoTecnicos
    Dim tipoTecnicos As New classTipoTecnicos

    Private Sub FrmRelTipoTecnicos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        busqueda(0, String.Empty, 1)
    End Sub

    Private Sub btnBuscarClave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscarClave.Click
        If Me.txtClave.Text.Length > 0 And Me.txtNombre.Text.Length > 0 Then
            busqueda(CInt(Me.txtClave.Text), Me.txtNombre.Text, 4)
        ElseIf Me.txtClave.Text.Length > 0 Then
            If IsNumeric(Me.txtClave.Text) = False Then
                MsgBox("Este campo sólo acepta valores numéricos")
                Exit Sub
            End If
            busqueda(CInt(Me.txtClave.Text), String.Empty, 2)
        ElseIf Me.txtNombre.Text.Length > 0 Then
            busqueda(0, Me.txtNombre.Text, 3)
        Else
            busqueda(0, String.Empty, 1)
        End If
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        actualiza()
        busqueda(0, String.Empty, 1)
    End Sub

    Private Sub busqueda(ByVal prmId As Integer, ByVal prmNombre As String, ByVal prmOp As Integer)
        Me.dgvTipoTecnicos.DataSource = tipoTecnicos.consulta(prmId, prmNombre, prmOp)
    End Sub

    Private Sub actualiza()
        Dim i As Integer
        For i = 0 To Me.dgvTipoTecnicos.RowCount - 1
            tipoTecnicos.Actualiza(dgvTipoTecnicos.Rows(i).Cells("colEsCable").Value(), dgvTipoTecnicos.Rows(i).Cells("colEsAntena").Value(), CInt(dgvTipoTecnicos.Rows(i).Cells(1).Value()))
        Next
        MsgBox("Registros Almacenados Satisfactoriamente", MsgBoxStyle.Information)
    End Sub

    Private Sub cbxCable_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCable.CheckedChanged
        If cbxCable.Checked = True Then
            SeleccionaTodos(1)
        ElseIf cbxCable.Checked = False Then
            SeleccionaTodos(2)
        End If
    End Sub
    Private Sub SeleccionaTodos(ByVal tipo As Integer)
        Try
            If tipo = 1 Then
                For Each dgrValida As DataGridViewRow In dgvTipoTecnicos.Rows
                    If dgrValida.Cells("colEsCable").Value() = False Then
                        dgrValida.Cells("colEsCable").Value() = True
                    End If
                Next
            ElseIf tipo = 2 Then
                For Each dgrValida As DataGridViewRow In dgvTipoTecnicos.Rows
                    If dgrValida.Cells("colEsCable").Value() = True Then
                        dgrValida.Cells("colEsCable").Value() = False
                    End If
                Next
            ElseIf tipo = 3 Then
                For Each dgrValida As DataGridViewRow In dgvTipoTecnicos.Rows
                    If dgrValida.Cells("colEsAntena").Value() = False Then
                        dgrValida.Cells("colEsAntena").Value() = True
                    End If
                Next
            ElseIf tipo = 4 Then
                For Each dgrValida As DataGridViewRow In dgvTipoTecnicos.Rows
                    If dgrValida.Cells("colEsAntena").Value() = True Then
                        dgrValida.Cells("colEsAntena").Value() = False
                    End If
                Next
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub cbxAntena_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxAntena.CheckedChanged
        If cbxAntena.Checked = True Then
            SeleccionaTodos(3)
        ElseIf cbxAntena.Checked = False Then
            SeleccionaTodos(4)
        End If
    End Sub
End Class