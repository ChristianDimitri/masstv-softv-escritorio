<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmAgendaRapida
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Label1 As System.Windows.Forms.Label
        Dim Clv_CitaLabel As System.Windows.Forms.Label
        Dim Clv_TecnicoLabel As System.Windows.Forms.Label
        Dim FechaLabel As System.Windows.Forms.Label
        Dim Descripcion_cortaLabel As System.Windows.Forms.Label
        Dim DescripcionLabel As System.Windows.Forms.Label
        Dim Label6 As System.Windows.Forms.Label
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.MUESTRAHORAS_CITASTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRAHORAS_CITASTableAdapter()
        Me.BUSCADetCitasTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.BUSCADetCitasTableAdapter()
        Me.CONDetCitasTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONDetCitasTableAdapter()
        Me.CONDetCitasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRAHORASCITASBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BUSCADetCitasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONCITASTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONCITASTableAdapter()
        Me.CONCITASBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.TextComentario = New System.Windows.Forms.TextBox()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.Muestra_Tecnicos_AlmacenBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLidia2 = New sofTV.DataSetLidia2()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.Clv_TecnicoTextBox = New System.Windows.Forms.TextBox()
        Me.Descripcion_cortaTextBox = New System.Windows.Forms.TextBox()
        Me.DescripcionTextBox = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Muestra_Tecnicos_AlmacenTableAdapter = New sofTV.DataSetLidia2TableAdapters.Muestra_Tecnicos_AlmacenTableAdapter()
        Label1 = New System.Windows.Forms.Label()
        Clv_CitaLabel = New System.Windows.Forms.Label()
        Clv_TecnicoLabel = New System.Windows.Forms.Label()
        FechaLabel = New System.Windows.Forms.Label()
        Descripcion_cortaLabel = New System.Windows.Forms.Label()
        DescripcionLabel = New System.Windows.Forms.Label()
        Label6 = New System.Windows.Forms.Label()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONDetCitasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAHORASCITASBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BUSCADetCitasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONCITASBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.Muestra_Tecnicos_AlmacenBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(67, 66)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(52, 15)
        Label1.TabIndex = 177
        Label1.Text = "Turno :"
        '
        'Clv_CitaLabel
        '
        Clv_CitaLabel.AutoSize = True
        Clv_CitaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_CitaLabel.ForeColor = System.Drawing.Color.WhiteSmoke
        Clv_CitaLabel.Location = New System.Drawing.Point(12, 110)
        Clv_CitaLabel.Name = "Clv_CitaLabel"
        Clv_CitaLabel.Size = New System.Drawing.Size(52, 15)
        Clv_CitaLabel.TabIndex = 0
        Clv_CitaLabel.Text = "# Cita :"
        '
        'Clv_TecnicoLabel
        '
        Clv_TecnicoLabel.AutoSize = True
        Clv_TecnicoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_TecnicoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_TecnicoLabel.Location = New System.Drawing.Point(56, 11)
        Clv_TecnicoLabel.Name = "Clv_TecnicoLabel"
        Clv_TecnicoLabel.Size = New System.Drawing.Size(65, 15)
        Clv_TecnicoLabel.TabIndex = 2
        Clv_TecnicoLabel.Text = "Tecnico :"
        '
        'FechaLabel
        '
        FechaLabel.AutoSize = True
        FechaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FechaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        FechaLabel.Location = New System.Drawing.Point(67, 39)
        FechaLabel.Name = "FechaLabel"
        FechaLabel.Size = New System.Drawing.Size(54, 15)
        FechaLabel.TabIndex = 4
        FechaLabel.Text = "Fecha :"
        '
        'Descripcion_cortaLabel
        '
        Descripcion_cortaLabel.AutoSize = True
        Descripcion_cortaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Descripcion_cortaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Descripcion_cortaLabel.Location = New System.Drawing.Point(86, 376)
        Descripcion_cortaLabel.Name = "Descripcion_cortaLabel"
        Descripcion_cortaLabel.Size = New System.Drawing.Size(58, 15)
        Descripcion_cortaLabel.TabIndex = 8
        Descripcion_cortaLabel.Text = "Asunto :"
        Descripcion_cortaLabel.Visible = False
        '
        'DescripcionLabel
        '
        DescripcionLabel.AutoSize = True
        DescripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescripcionLabel.ForeColor = System.Drawing.Color.LightSlateGray
        DescripcionLabel.Location = New System.Drawing.Point(18, 401)
        DescripcionLabel.Name = "DescripcionLabel"
        DescripcionLabel.Size = New System.Drawing.Size(126, 15)
        DescripcionLabel.TabIndex = 10
        DescripcionLabel.Text = "Detalle de la Cita :"
        DescripcionLabel.Visible = False
        '
        'Label6
        '
        Label6.AutoSize = True
        Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label6.ForeColor = System.Drawing.Color.LightSlateGray
        Label6.Location = New System.Drawing.Point(32, 93)
        Label6.Name = "Label6"
        Label6.Size = New System.Drawing.Size(89, 15)
        Label6.TabIndex = 334
        Label6.Text = "Comentario :"
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MUESTRAHORAS_CITASTableAdapter
        '
        Me.MUESTRAHORAS_CITASTableAdapter.ClearBeforeFill = True
        '
        'BUSCADetCitasTableAdapter
        '
        Me.BUSCADetCitasTableAdapter.ClearBeforeFill = True
        '
        'CONDetCitasTableAdapter
        '
        Me.CONDetCitasTableAdapter.ClearBeforeFill = True
        '
        'CONDetCitasBindingSource
        '
        Me.CONDetCitasBindingSource.DataMember = "CONDetCitas"
        Me.CONDetCitasBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRAHORASCITASBindingSource
        '
        Me.MUESTRAHORASCITASBindingSource.DataMember = "MUESTRAHORAS_CITAS"
        Me.MUESTRAHORASCITASBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'BUSCADetCitasBindingSource
        '
        Me.BUSCADetCitasBindingSource.DataMember = "BUSCADetCitas"
        Me.BUSCADetCitasBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CONCITASTableAdapter
        '
        Me.CONCITASTableAdapter.ClearBeforeFill = True
        '
        'CONCITASBindingSource
        '
        Me.CONCITASBindingSource.DataMember = "CONCITAS"
        Me.CONCITASBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(Me.Button5)
        Me.Panel1.Controls.Add(Me.TextComentario)
        Me.Panel1.Controls.Add(Label6)
        Me.Panel1.Controls.Add(Me.ComboBox2)
        Me.Panel1.Controls.Add(Me.ComboBox1)
        Me.Panel1.Controls.Add(Label1)
        Me.Panel1.Controls.Add(Me.DateTimePicker1)
        Me.Panel1.Controls.Add(Clv_CitaLabel)
        Me.Panel1.Controls.Add(Clv_TecnicoLabel)
        Me.Panel1.Controls.Add(Me.Clv_TecnicoTextBox)
        Me.Panel1.Controls.Add(FechaLabel)
        Me.Panel1.Controls.Add(Descripcion_cortaLabel)
        Me.Panel1.Controls.Add(Me.Descripcion_cortaTextBox)
        Me.Panel1.Controls.Add(DescripcionLabel)
        Me.Panel1.Controls.Add(Me.DescripcionTextBox)
        Me.Panel1.Location = New System.Drawing.Point(10, 10)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(448, 248)
        Me.Panel1.TabIndex = 20
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(292, 203)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 33)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "&ACEPTAR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(15, 201)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(93, 33)
        Me.Button5.TabIndex = 3
        Me.Button5.TabStop = False
        Me.Button5.Text = "&ACEPTAR"
        Me.Button5.UseVisualStyleBackColor = False
        Me.Button5.Visible = False
        '
        'TextComentario
        '
        Me.TextComentario.Location = New System.Drawing.Point(127, 93)
        Me.TextComentario.MaxLength = 250
        Me.TextComentario.Multiline = True
        Me.TextComentario.Name = "TextComentario"
        Me.TextComentario.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextComentario.Size = New System.Drawing.Size(294, 102)
        Me.TextComentario.TabIndex = 3
        '
        'ComboBox2
        '
        Me.ComboBox2.DataSource = Me.Muestra_Tecnicos_AlmacenBindingSource
        Me.ComboBox2.DisplayMember = "Nombre"
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(127, 10)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(180, 21)
        Me.ComboBox2.TabIndex = 0
        Me.ComboBox2.ValueMember = "clv_Tecnico"
        '
        'Muestra_Tecnicos_AlmacenBindingSource
        '
        Me.Muestra_Tecnicos_AlmacenBindingSource.DataMember = "Muestra_Tecnicos_Almacen"
        Me.Muestra_Tecnicos_AlmacenBindingSource.DataSource = Me.DataSetLidia2
        '
        'DataSetLidia2
        '
        Me.DataSetLidia2.DataSetName = "DataSetLidia2"
        Me.DataSetLidia2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ComboBox1
        '
        Me.ComboBox1.DisplayMember = "turno"
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(127, 63)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(123, 24)
        Me.ComboBox1.TabIndex = 2
        Me.ComboBox1.ValueMember = "id"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCITASBindingSource, "Fecha", True))
        Me.DateTimePicker1.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.CONCITASBindingSource, "Fecha", True))
        Me.DateTimePicker1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(127, 36)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(113, 21)
        Me.DateTimePicker1.TabIndex = 1
        Me.DateTimePicker1.Value = New Date(2007, 5, 3, 0, 0, 0, 0)
        '
        'Clv_TecnicoTextBox
        '
        Me.Clv_TecnicoTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TecnicoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_TecnicoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCITASBindingSource, "Clv_Tecnico", True))
        Me.Clv_TecnicoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_TecnicoTextBox.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TecnicoTextBox.Location = New System.Drawing.Point(25, 11)
        Me.Clv_TecnicoTextBox.Name = "Clv_TecnicoTextBox"
        Me.Clv_TecnicoTextBox.Size = New System.Drawing.Size(25, 14)
        Me.Clv_TecnicoTextBox.TabIndex = 333
        Me.Clv_TecnicoTextBox.TabStop = False
        '
        'Descripcion_cortaTextBox
        '
        Me.Descripcion_cortaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Descripcion_cortaTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Descripcion_cortaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCITASBindingSource, "Descripcion_corta", True))
        Me.Descripcion_cortaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Descripcion_cortaTextBox.Location = New System.Drawing.Point(150, 374)
        Me.Descripcion_cortaTextBox.MaxLength = 100
        Me.Descripcion_cortaTextBox.Name = "Descripcion_cortaTextBox"
        Me.Descripcion_cortaTextBox.Size = New System.Drawing.Size(518, 21)
        Me.Descripcion_cortaTextBox.TabIndex = 9
        Me.Descripcion_cortaTextBox.TabStop = False
        Me.Descripcion_cortaTextBox.Visible = False
        '
        'DescripcionTextBox
        '
        Me.DescripcionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DescripcionTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.DescripcionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCITASBindingSource, "Descripcion", True))
        Me.DescripcionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionTextBox.Location = New System.Drawing.Point(150, 401)
        Me.DescripcionTextBox.MaxLength = 300
        Me.DescripcionTextBox.Multiline = True
        Me.DescripcionTextBox.Name = "DescripcionTextBox"
        Me.DescripcionTextBox.Size = New System.Drawing.Size(518, 76)
        Me.DescripcionTextBox.TabIndex = 11
        Me.DescripcionTextBox.TabStop = False
        Me.DescripcionTextBox.Visible = False
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(164, 305)
        Me.TextBox1.MaxLength = 150
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(301, 21)
        Me.TextBox1.TabIndex = 0
        Me.TextBox1.TabStop = False
        '
        'Muestra_Tecnicos_AlmacenTableAdapter
        '
        Me.Muestra_Tecnicos_AlmacenTableAdapter.ClearBeforeFill = True
        '
        'FrmAgendaRapida
        '
        Me.AcceptButton = Me.Button5
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(450, 258)
        Me.ControlBox = False
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmAgendaRapida"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Datos para la Agenda"
        Me.TopMost = True
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONDetCitasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAHORASCITASBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BUSCADetCitasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONCITASBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.Muestra_Tecnicos_AlmacenBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents MUESTRAHORAS_CITASTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRAHORAS_CITASTableAdapter
    Friend WithEvents BUSCADetCitasTableAdapter As sofTV.NewSofTvDataSetTableAdapters.BUSCADetCitasTableAdapter
    Friend WithEvents CONDetCitasTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONDetCitasTableAdapter
    Friend WithEvents CONDetCitasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAHORASCITASBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BUSCADetCitasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONCITASTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONCITASTableAdapter
    Friend WithEvents CONCITASBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Clv_TecnicoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Descripcion_cortaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DescripcionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents DataSetLidia2 As sofTV.DataSetLidia2
    Friend WithEvents Muestra_Tecnicos_AlmacenBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Tecnicos_AlmacenTableAdapter As sofTV.DataSetLidia2TableAdapters.Muestra_Tecnicos_AlmacenTableAdapter
    Public WithEvents TextComentario As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    'Friend WithEvents Muestra_Tecnicos_AlmacenTableAdapter As sofTV.DataSetLidiaTableAdapters.Muestra_Tecnicos_AlmacenTableAdapter
End Class
