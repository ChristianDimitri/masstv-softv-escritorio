﻿Imports System.Data.SqlClient
Imports sofTV.BAL
Imports System.Collections.Generic

Public Class FrmCompanias

#Region "Variables Globales"
    Public CompaniasClvCompania As Long
    Public CompaniasNombreCompania As String
    Public CompaniasClvRfc As Integer
    Public CompaniasActivo As Boolean
    Public CompaniasOpcion As String
#End Region

#Region "Controles"

    Private Sub FrmCompanias_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If Me.cbxActivo.Checked = False Then
            EliminarRelCompaniasServicio(CompaniasClvCompania, 0, 2)
        End If
    End Sub
    Private Sub FrmCiclo_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        uspConsultaCompaniasMizar() 'LLENAR EL COMBO DE LAS COMPAÑÍAS (cmbCompanias)
        llenarComboServicios() 'LLENAR EL COMBO DE LOS SERVICIOS (cmbServicios)

        If CompaniasOpcion = "C" Or CompaniasOpcion = "M" Then
            Me.txtClvCompania.Text = CompaniasClvCompania
            Me.txtNombreCompania.Text = CompaniasNombreCompania
            Me.cmbRfc.SelectedValue = CompaniasClvRfc
            Me.cbxActivo.Checked = CompaniasActivo
            llenaDgvServicios(CompaniasClvCompania) 'LLENAR EL DATAGRIDVIEW DE SERVICIOS (dgvServicios)
        End If

        If (CompaniasOpcion = "N") Then
            limpiarPantalla()
            Me.gbxServicios.Enabled = False
        ElseIf (CompaniasOpcion = "C") Then
            'Me.txtNombreCompania.Enabled = False
            Me.btnGuardar.Enabled = False
            'Me.cmbRfc.Enabled = False
            'Me.cbxActivo.Enabled = False
            Me.GroupBox3.Enabled = False
            Me.gbxServicios.Enabled = False
        Else
            Me.txtNombreCompania.Enabled = True
            Me.btnGuardar.Enabled = True
            Me.cmbRfc.Enabled = True
            Me.cbxActivo.Enabled = True
        End If
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        If Me.cbxActivo.Checked = False Then
            Dim RES = MsgBox("¡Si no está activa la compañía se eliminarán los servicios relacionados!" _
                             & vbCrLf & "¿Desea continuar?", MsgBoxStyle.YesNo)

            If RES = MsgBoxResult.Yes Then
                EliminarRelCompaniasServicio(CompaniasClvCompania, 0, 2)
            Else
                Exit Sub
            End If
        End If
        Me.Close()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If Me.txtNombreCompania.Text.Length = 0 Then
            MsgBox("¡Capture un Nombre válido!", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.cmbRfc.SelectedValue = 0 Then
            MsgBox("¡Seleccione un RFC!", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.cbxActivo.Checked = False And CompaniasOpcion <> "N" Then
            Dim RES = MsgBox("¡Si no está activa la compañía se eliminarán los servicios relacionados!" _
                             & vbCrLf & "¿Desea continuar?", MsgBoxStyle.YesNo)

            If RES = MsgBoxResult.Yes Then
                EliminarRelCompaniasServicio(CompaniasClvCompania, 0, 2)
            Else
                Exit Sub
            End If
        End If

        If CompaniasOpcion = "N" Then
            Guardar(CompaniasClvCompania, Me.txtNombreCompania.Text, CInt(Me.cmbRfc.SelectedValue), Me.cbxActivo.Checked, 1)
        Else
            Guardar(CompaniasClvCompania, Me.txtNombreCompania.Text, CInt(Me.cmbRfc.SelectedValue), Me.cbxActivo.Checked, 2)
            Me.Close()
        End If
    End Sub

    Private Sub cbxActivo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxActivo.CheckedChanged
        If Me.cbxActivo.Checked = False Then
            Me.gbxServicios.Enabled = False
        Else
            Me.gbxServicios.Enabled = True
        End If
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        If CompaniasOpcion = "N" Then
            MsgBox("¡Guarde primero la Compañía para eliminar Servicios!", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.dgvServicios.RowCount = 0 Then
            MsgBox("¡No hay registros para Eliminar!", MsgBoxStyle.Information)
            Exit Sub
        End If

        If IsNumeric(Me.dgvServicios.SelectedCells(3).Value) = True Then
            EliminarRelCompaniasServicio(CLng(Me.dgvServicios.SelectedCells(3).Value), CInt(Me.dgvServicios.SelectedCells(2).Value), 1)
            MsgBox("¡Servicio eliminado satisfactoriamente!", MsgBoxStyle.Information)

            llenaDgvServicios(CompaniasClvCompania) 'LLENAR EL DATAGRIDVIEW DE SERVICIOS (dgvServicios)
        End If
    End Sub

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        If CompaniasOpcion = "N" Then
            MsgBox("¡Guarde primero la Compañía para agregar Servicios!", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.cmbServicios.SelectedValue = 0 Then
            MsgBox("¡Seleccione un servicio!", MsgBoxStyle.Information)
            Exit Sub
        End If

        Dim resultado As Integer
        resultado = GuardarRelacion(CompaniasClvCompania, CInt(Me.cmbServicios.SelectedValue))
        If resultado = 1 Then
            MsgBox("¡El servicio ya se encuentra en la lista!", MsgBoxStyle.Information)
            Exit Sub
        ElseIf resultado = 2 Then
            MsgBox("¡El servicio ya ha sido dado de alta en otra compañía!", MsgBoxStyle.Information)
            Exit Sub
        Else
            MsgBox("¡Servicio agregado satisfactoriamente!", MsgBoxStyle.Information)
        End If

        llenaDgvServicios(CompaniasClvCompania) 'LLENAR EL DATAGRIDVIEW DE SERVICIOS (dgvServicios)
    End Sub
#End Region

#Region "Guardar Compañía"
    Private Sub limpiarPantalla()
        Me.txtNombreCompania.Text = ""
        Me.txtClvCompania.Text = ""
        Me.cbxActivo.Checked = True
    End Sub

    Private Sub Guardar(ByVal prmClvCompania As Long, ByVal prmNombreCompania As String, ByVal prmClvMizar As Integer, _
                        ByVal prmActivo As Boolean, ByVal prmOp As Integer)
        Dim Companias As New ClassCompanias
        Companias.clvCompania = prmClvCompania
        Companias.nombreCompania = prmNombreCompania
        Companias.clvEmpresaMizar = prmClvMizar
        Companias.activo = prmActivo
        Companias.op = prmOp

        Companias.uspInsertaTblCompanias()
        If Companias.bndInsercion > 0 Then
            MsgBox("La Compañía a registrar ya fue dada de alta anteriormente", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Companias.bndRfc > 0 Then
            MsgBox("El RFC seleccionado está siendo utilizado en otra Compañía", MsgBoxStyle.Information)
            Exit Sub
        Else
            MsgBox("Compañía almacenada exitosamente", MsgBoxStyle.Information)
            CompaniasClvCompania = Companias.newClvCompania
            Me.txtClvCompania.Text = CompaniasClvCompania
            CompaniasOpcion = "M"
            Me.gbxServicios.Enabled = True
        End If
    End Sub
#End Region

#Region "Guardar Relación"
    Private Function GuardarRelacion(ByVal prmClvCompania As Long, ByVal prmClvTipServ As Integer) As Integer
        Dim Companias As New ClassCompanias

        Companias.clvCompania = prmClvCompania
        Companias.clvTipServ = prmClvTipServ
        GuardarRelacion = Companias.uspInsertaTblRelCompaniaServicio()
    End Function
#End Region

#Region "Eliminar Relación"
    Private Sub EliminarRelCompaniasServicio(ByVal prmClvCompania As Long, ByVal prmClvTipServ As Integer, ByVal prmOp As Integer)
        Dim Companias As New ClassCompanias

        Companias.clvCompania = prmClvCompania
        Companias.clvTipServ = prmClvTipServ
        Companias.op = prmOp
        Companias.uspEliminaTblRelCompaniaServicio()
    End Sub
#End Region

#Region "Llenar Combo Compañías Mizar"
    Private Sub uspConsultaCompaniasMizar()
        Dim Companias As New ClassCompanias

        Me.cmbRfc.DataSource = Companias.uspConsultaCompaniasMizar()
    End Sub
#End Region

#Region "Llenar Combo Servicios"
    Private Sub llenarComboServicios()
        Dim Companias As New ClassCompanias

        Me.cmbServicios.DataSource = Companias.uspConsultaTipServ()
    End Sub
#End Region

#Region "Llenar DataGridView Servicios (dgvServicios)"
    Private Sub llenaDgvServicios(ByVal prmClvCompania As Long)
        Dim Companias As New ClassCompanias
        Companias.clvCompania = prmClvCompania

        Me.dgvServicios.DataSource = Companias.uspConsultaTblRelCompaniaServicio()
    End Sub
#End Region

    
End Class