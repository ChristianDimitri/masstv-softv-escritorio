﻿Imports sofTV.BAL
Public Class BrwGrupoConceptosPoliza

    Private Sub NuevoButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NuevoButton.Click
        Dim Frm As New FrmCapturaGrupoConceptosPoliza
        Frm.IdGrupoText.ReadOnly = True
        If (Frm.ShowDialog() = Frm.DialogResult.OK) Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@IDGRUPO", SqlDbType.Int, ParameterDirection.Output)
            BaseII.CreateMyParameter("@Concepto", SqlDbType.VarChar, Frm.ConceptoText.Text, 250)
            BaseII.CreateMyParameter("@Posicion", SqlDbType.Int, CInt(Frm.PosicionText.Text))
            BaseII.CreateMyParameter("@Op", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@CLVCOMPANIA", SqlDbType.Int, GloClvCompania)
            BaseII.CreateMyParameter("@IDPROGRAMACION", SqlDbType.Int, CInt(Frm.tbIdProgramacion.Text))
            BaseII.ProcedimientoOutPut("Softv_AddGrupoConceptoIngreso")
            Frm.IdGrupoText.Text = BaseII.dicoPar("@IDGRUPO").ToString

            If (cbCompania.Text.Length = 0) Then
                Exit Sub
            End If

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, cbCompania.SelectedValue)
            GrupoConceptosDataGrid.DataSource = BaseII.ConsultaDT("Softv_GetGrupoConceptoIngreso")

            'Dim GrupoConcepto As GrupoConceptoIngreso
            'GrupoConcepto.Add(Nothing, Frm.ConceptoText.Text, CInt(Frm.PosicionText.Text), 0)
        End If

    End Sub



    Private Sub BrwGrupoConceptosPoliza_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, 0)
        cbCompania.DataSource = BaseII.ConsultaDT("MUESTRAtblCompanias")

        If (cbCompania.Text.Length = 0) Then
            Exit Sub
        End If

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, cbCompania.SelectedValue)
        GrupoConceptosDataGrid.DataSource = BaseII.ConsultaDT("Softv_GetGrupoConceptoIngreso")
    End Sub


    Private Sub ConsultarButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarButton.Click

        If GrupoConceptosDataGrid.RowCount = 0 Then
            MessageBox.Show("Selecciona un registro.")
            Exit Sub
        End If

        GloClvCompania = cbCompania.SelectedValue

        Dim Frm As New FrmCapturaGrupoConceptosPoliza
        Dim dT As New DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IDGRUPO", SqlDbType.Int, GrupoConceptosDataGrid.SelectedCells(0).Value)
        dT = BaseII.ConsultaDT("Softv_GetGrupoConceptoIngresoById")
        Frm.IdGrupoText.Text = dT.Rows(0).Item(0).ToString
        Frm.ConceptoText.Text = dT.Rows(0).Item(1).ToString
        Frm.PosicionText.Text = dT.Rows(0).Item(2).ToString
        Frm.tbIdProgramacion.Text = dT.Rows(0).Item(4).ToString
        Frm.IdGrupoText.ReadOnly = True
        Frm.ConceptoText.ReadOnly = True
        Frm.PosicionText.ReadOnly = True
        Frm.tbIdProgramacion.ReadOnly = True
        Frm.SaveButton.Enabled = False
        Frm.ShowDialog()
    End Sub

    Private Sub ModificarButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ModificarButton.Click

        If GrupoConceptosDataGrid.RowCount = 0 Then
            MessageBox.Show("Selecciona un registro.")
            Exit Sub
        End If

        GloClvCompania = cbCompania.SelectedValue

        Dim dT As New DataTable
        Dim Frm As New FrmCapturaGrupoConceptosPoliza

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IDGRUPO", SqlDbType.Int, GrupoConceptosDataGrid.SelectedCells(0).Value)
        dT = BaseII.ConsultaDT("Softv_GetGrupoConceptoIngresoById")
        
        Frm.IdGrupoText.Text = dT.Rows(0).Item(0).ToString
        Frm.ConceptoText.Text = dT.Rows(0).Item(1).ToString
        Frm.PosicionText.Text = dT.Rows(0).Item(2).ToString
        Frm.tbIdProgramacion.Text = dT.Rows(0).Item(4).ToString

        Frm.SaveButton.Text = "Modificar"
        Frm.IdGrupoText.ReadOnly = True
        If Frm.ShowDialog() = DialogResult.OK Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@IdGrupo", SqlDbType.Int, Frm.IdGrupoText.Text)
            BaseII.CreateMyParameter("@Concepto", SqlDbType.VarChar, Frm.ConceptoText.Text, 250)
            BaseII.CreateMyParameter("@Posicion", SqlDbType.Int, Frm.PosicionText.Text)
            BaseII.CreateMyParameter("@Op", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@IDPROGRAMACION", SqlDbType.Int, Frm.tbIdProgramacion.Text)
            BaseII.Inserta("Softv_EditGrupoConceptoIngreso")
        End If

        If (cbCompania.Text.Length = 0) Then
            Exit Sub
        End If

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, cbCompania.SelectedValue)
        GrupoConceptosDataGrid.DataSource = BaseII.ConsultaDT("Softv_GetGrupoConceptoIngreso")

        'Dim Frm As New FrmCapturaGrupoConceptosPoliza
        'Dim GrupoConcepto As New GrupoConceptoIngreso
        'GrupoConcepto = GrupoConcepto.GetOne(CInt(GrupoConceptosDataGrid.SelectedRows(0).Cells(0).Value))
        'Frm.IdGrupoText.Text = GrupoConcepto.IdGrupo.ToString
        'Frm.ConceptoText.Text = GrupoConcepto.Concepto.ToString
        'Frm.PosicionText.Text = GrupoConcepto.Posicion.ToString
        'Frm.SaveButton.Text = "Modificar"
        'Frm.IdGrupoText.ReadOnly = True
        'If Frm.ShowDialog() = DialogResult.OK Then
        '    GrupoConcepto.Edit(CInt(Frm.IdGrupoText.Text), Frm.ConceptoText.Text, CInt(Frm.PosicionText.Text), 0)
        'End If
    End Sub

    Private Sub SalirButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalirButton.Click
        Me.close()
    End Sub

    Private Sub GrupoConceptosDataGrid_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GrupoConceptosDataGrid.CellClick
        Try
            IdGrupoLabel1.Text = GrupoConceptosDataGrid.SelectedRows(0).Cells(0).Value.ToString
            ConceptoLabel1.Text = GrupoConceptosDataGrid.SelectedRows(0).Cells(1).Value.ToString
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GrupoConceptosDataGrid_CurrentCellChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GrupoConceptosDataGrid.CurrentCellChanged
        Try
            IdGrupoLabel1.Text = GrupoConceptosDataGrid.SelectedRows(0).Cells(0).Value.ToString
            ConceptoLabel1.Text = GrupoConceptosDataGrid.SelectedRows(0).Cells(1).Value.ToString
        Catch ex As Exception

        End Try

    End Sub

    Private Sub IdGrupoButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IdGrupoButton.Click

        If cbCompania.Text.Length = 0 Or IdGrupoTextBox.Text.Length = 0 Then
            Exit Sub
        End If

        If IsNumeric(IdGrupoTextBox.Text) = False Then
            MessageBox.Show("Teclea una clave numérica válida.")
            IdGrupoTextBox.Clear()
            Exit Sub
        End If

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, cbCompania.SelectedValue)
        BaseII.CreateMyParameter("@IdGrupo", SqlDbType.Int, CInt(IdGrupoTextBox.Text))
        GrupoConceptosDataGrid.DataSource = BaseII.ConsultaDT("Softv_GetbyIDGrupoConceptoIngreso")

    End Sub

    Private Sub ConceptoButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConceptoButton.Click
        If cbCompania.Text.Length = 0 Or ConceptoTextBox.Text.Length = 0 Then
            Exit Sub
        End If

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, cbCompania.SelectedValue)
        BaseII.CreateMyParameter("@Concepto", SqlDbType.VarChar, ConceptoTextBox.Text)
        GrupoConceptosDataGrid.DataSource = BaseII.ConsultaDT("Softv_GetbyConceptoGrupoConceptoIngreso")

    End Sub

    Private Sub cbCompania_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbCompania.SelectedIndexChanged
        If (cbCompania.Text.Length = 0) Then
            Exit Sub
        End If

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, cbCompania.SelectedValue)
        GrupoConceptosDataGrid.DataSource = BaseII.ConsultaDT("Softv_GetGrupoConceptoIngreso")
    End Sub
End Class