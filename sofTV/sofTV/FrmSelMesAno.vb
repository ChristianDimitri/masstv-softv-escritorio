﻿Public Class FrmSelMesAno

   
    Private Sub Label3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBLabel3.Click

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If LocOp = 350 Then
            GloMezclaMes1 = Mes1NumericUpDown.Value
            GloMezclaAno1 = Ano1NumericUpDown.Value
            GloMezclaMes2 = Mes2NumericUpDown.Value
            GloMezclaAno2 = Ano2NumericUpDown.Value
            FrmImprimirContrato.Show()
            Me.Close()
            Exit Sub
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Mes1NumericUpDown_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Mes1NumericUpDown.ValueChanged

        If Ano2NumericUpDown.Value > Ano1NumericUpDown.Value Then
            Mes2NumericUpDown.Minimum = 1
            Mes2NumericUpDown.Maximum = 12
        Else
            Mes2NumericUpDown.Minimum = Mes1NumericUpDown.Value
        End If
    End Sub

    Private Sub Ano1NumericUpDown_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ano1NumericUpDown.ValueChanged

        Ano2NumericUpDown.Minimum = Ano1NumericUpDown.Value
        If Mes2NumericUpDown.Value < Mes1NumericUpDown.Value And Ano1NumericUpDown.Value >= Ano2NumericUpDown.Value Then
            Mes2NumericUpDown.Minimum = Mes1NumericUpDown.Value
            Mes2NumericUpDown.Value = Mes1NumericUpDown.Value
        ElseIf Ano1NumericUpDown.Value < Ano2NumericUpDown.Value Then
            Mes2NumericUpDown.Minimum = 1
            Mes2NumericUpDown.Maximum = 12
        End If
    End Sub

    Private Sub Ano2NumericUpDown_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ano2NumericUpDown.ValueChanged
        If Ano2NumericUpDown.Value > Ano1NumericUpDown.Value Then
            Mes2NumericUpDown.Minimum = 1
            Mes2NumericUpDown.Maximum = 12
        ElseIf Mes2NumericUpDown.Value < Mes1NumericUpDown.Value Then
            Mes2NumericUpDown.Minimum = Mes1NumericUpDown.Value
            Mes2NumericUpDown.Value = Mes1NumericUpDown.Value
        End If
    End Sub

    Private Sub FrmSelMesAno_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
    End Sub
End Class