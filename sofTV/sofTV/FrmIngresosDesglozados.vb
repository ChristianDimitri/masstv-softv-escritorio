﻿Imports sofTV.BAL
Imports System.Collections.Generic


Public Class FrmIngresosDesglozados
    Structure ColumnasGrid
        Dim idxID As Integer
        Dim idxClave As Integer
        Dim idxConcepto As Integer
    End Structure


    Dim CargaGruposI As Boolean = False
    Private _Clave As Integer
    Public Property Clave As Integer
        Get
            Return _Clave
        End Get
        Set(ByVal value As Integer)
            _Clave = value
        End Set
    End Property

    Private Sub FrmIngresosDesglozados_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.IdConceptoText.Text = Me.Clave.ToString

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLVCOMPANIA", SqlDbType.Int, CObj(GloClvCompania))
        IdGrupoCombo.DataSource = BaseII.ConsultaDT("Softv_GetGrupoIngreso")

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLVCOMPANIA", SqlDbType.Int, CObj(GloClvCompania))
        cbxGpoIngresos.DataSource = BaseII.ConsultaDT("Softv_GetGrupoConceptoIngreso")

        'BaseII.limpiaParametros()
        'BaseII.CreateMyParameter("@OPCION", SqlDbType.Int, CObj(GloClvCompania))
        'BaseII.CreateMyParameter("@IDCONCEPTO", SqlDbType.Int, 0)
        'BaseII.CreateMyParameter("@IDGRUPO", SqlDbType.Int, cbxGpoIngresos.SelectedValue)
        'BaseII.CreateMyParameter("@CONCEPTO", SqlDbType.VarChar, "", 250)
        'cbxConceptoIngresos.DataSource = BaseII.ConsultaDT("uspFiltraConceptosIngresos")

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IDINGRESO", SqlDbType.Int, CObj(IdConceptoText.Text))
        dgvRelacion.DataSource = BaseII.ConsultaDT("uspSeleccionaRelacionIngresosConceptos")

        'Dim GrupoIngreso As New GrupoIngreso
        'With Me.IdGrupoCombo
        '    .DataSource = GrupoIngreso.GetAll
        '    .ValueMember = "IdGrupoIngresos"
        '    .DisplayMember = "Concepto"
        'End With

        'Dim GrupoConcepto As New GrupoConceptoIngreso

        'CargaGruposI = False
        'With Me.cbxGpoIngresos
        '    .DataSource = GrupoConcepto.GetAll
        '    .ValueMember = "IdGrupo"
        '    .DisplayMember = "Concepto"
        'End With
        'CargaGruposI = True

        'With Me.cbxConceptoIngresos
        '    .DataSource = FiltraConceptos()
        '    .ValueMember = "Clave"
        '    .DisplayMember = "Concepto"
        'End With

        'Me.dgvRelacion.DataSource = TraeDetalle()
        'ocultaColumnas(Me.dgvRelacion)

        Select Case opcion
            Case "N"
                IdGrupoCombo.SelectedValue = GloClvGrupo
                ActivoCheckBox.Checked = True
                Me.gbxConcepto.Enabled = True
                Me.SaveButton.Enabled = True
                Me.gbxDetalle.Enabled = False
            Case "C"
                TraeConceptos()
                Me.gbxConcepto.Enabled = False
                Me.SaveButton.Enabled = False
                Me.gbxDetalle.Enabled = False
            Case "M"
                Me.gbxConcepto.Enabled = True
                TraeConceptos()
                Me.SaveButton.Enabled = True
                Me.gbxDetalle.Enabled = True
        End Select


    End Sub

    'Private Sub ocultaColumnas(ByRef dgv As DataGridView)

    '    For Each dc As DataGridViewColumn In dgv.Columns

    '        If (dc.Name = "ID") Then
    '            dc.ReadOnly = True
    '            dc.Visible = False
    '        ElseIf (dc.Name = "Clave") Then
    '            dc.ReadOnly = True
    '            dc.Visible = True
    '        ElseIf (dc.Name = "Concepto") Then
    '            dc.ReadOnly = True
    '            dc.Visible = True
    '            dc.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
    '        End If
    '    Next

    'End Sub

    Public Sub Guardar()
        Try
            If IsNumeric(Me.PosicionText.Text) = False Then
                Me.PosicionText.Text = "0"
            End If
            If ConceptoText.Text = "" Then
                MsgBox("Debe capturar el Concepto", vbInformation, "Guardar")
                Return
            End If
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Concepto", SqlDbType.NVarChar, Me.ConceptoText.Text, 250)
            BaseII.CreateMyParameter("@Pos", SqlDbType.BigInt, CInt(Me.PosicionText.Text))
            BaseII.CreateMyParameter("@IdGrupoIngresos", SqlDbType.Int, IdGrupoCombo.SelectedValue)
            BaseII.CreateMyParameter("@Activo", SqlDbType.Bit, ActivoCheckBox.Checked)
            Select Case opcion
                Case "N"
                    BaseII.CreateMyParameter("@ID", ParameterDirection.Output, SqlDbType.BigInt)
                    'BaseII.Inserta("uspInsertaIngresoDesglosado")
                    Dim Regreso As Dictionary(Of String, Object) = BaseII.ProcedimientoOutPut("uspInsertaIngresoDesglosado")
                    Me.IdConceptoText.Text = CInt(Regreso("@ID")).ToString
                Case "M"
                    BaseII.CreateMyParameter("@IdIngreso", SqlDbType.BigInt, CInt(Me.IdConceptoText.Text))
                    BaseII.Inserta("uspModificaIngresoDesglosado")
            End Select
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "Filtra Conceptos")
        End Try
    End Sub

    Public Sub AgregaDetalle()
        Try
            eRes = 0

            If cbxConceptoIngresos.Text.Length = 0 Then
                MessageBox.Show("Selecciona un Concepto de Póliza.")
                Exit Sub
            End If

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@IdIngreso", SqlDbType.BigInt, CInt(Me.IdConceptoText.Text))
            BaseII.CreateMyParameter("@IdConcepto", SqlDbType.Int, Me.cbxConceptoIngresos.SelectedValue)
            BaseII.CreateMyParameter("@Res", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("uspInsertaRelacionIngresosConceptos")
            eRes = CInt(BaseII.dicoPar("@Res").ToString())
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "Filtra Conceptos")
        End Try
    End Sub

    Public Sub EliminaDetalle()
        Try
            If dgvRelacion.Rows.Count = 0 Then
                MessageBox.Show("Selecciona un registro.")
                Exit Sub
            End If

            If MsgBox("¿Desea eliminar de la relación?", vbYesNo + vbDefaultButton2, "Eliminar") = vbYes Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@IdIngreso", SqlDbType.BigInt, CInt(Me.txtID.Text))
                BaseII.CreateMyParameter("@IdConcepto", SqlDbType.Int, CInt(Me.txtClave.Text))
                BaseII.Inserta("uspEliminaRelacionIngresosConceptos")
            End If

        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "Filtra Conceptos")
        End Try
    End Sub

    Public Sub TraeConceptos()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Opcion", SqlDbType.BigInt, 4)
            BaseII.CreateMyParameter("@IdIngreso", SqlDbType.BigInt, CInt(Me.IdConceptoText.Text))
            Dim tblConceptos As DataTable = BaseII.ConsultaDT("uspSeleccionaIngresosDesglozados")
            For Each Registro As DataRow In tblConceptos.Rows
                ConceptoText.Text = Registro("Concepto")
                PosicionText.Text = CInt(Registro("Pos"))
                IdGrupoCombo.SelectedValue = CInt(Registro("IdGrupoIngresos"))
                ActivoCheckBox.Checked = CBool(Registro("Activo"))
            Next
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "Filtra Conceptos")
        End Try
    End Sub

    Public Function TraeDetalle() As DataTable
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@IdIngreso", SqlDbType.BigInt, CInt(Me.IdConceptoText.Text))
            Dim tblConceptos As DataTable = BaseII.ConsultaDT("uspSeleccionaRelacionIngresosConceptos")
            Return tblConceptos
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "Traer Detalles")
        End Try
    End Function


    'Public Function FiltraConceptos() As DataTable
    '    Try
    '        BaseII.limpiaParametros()
    '        BaseII.CreateMyParameter("@Opcion", SqlDbType.BigInt, 4)

    '        Dim GpoIngreso As Integer = 0
    '        If IsNumeric(Me.cbxGpoIngresos.SelectedValue) Then
    '            GpoIngreso = Me.cbxGpoIngresos.SelectedValue
    '        End If
    '        BaseII.CreateMyParameter("@IDGrupo", SqlDbType.BigInt, GpoIngreso)


    '        Dim tblConceptos As DataTable = BaseII.ConsultaDT("uspFiltraConceptosIngresos")
    '        Return tblConceptos
    '    Catch ex As Exception
    '        MsgBox(ex.Message, vbInformation, "Filtra Conceptos")
    '    End Try
    'End Function


    Private Sub PosicionText_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles PosicionText.KeyPress
        Dim caracter As Char = e.KeyChar
        If (Char.IsNumber(caracter)) Or _
                    (caracter = ChrW(Keys.Back)) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    'Private Sub PosicionText_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PosicionText.TextChanged

    'End Sub

    Private Sub ExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub SaveButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveButton.Click
        Guardar()
        TraeConceptos()
        Me.gbxDetalle.Enabled = True
    End Sub

    Private Sub cbxGpoIngresos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxGpoIngresos.SelectedIndexChanged
        If cbxGpoIngresos.Text.Length = 0 Then
            Exit Sub
        End If

        cbxConceptoIngresos.Text = ""

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@OPCION", SqlDbType.Int, 4)
        BaseII.CreateMyParameter("@IDCONCEPTO", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@IDGRUPO", SqlDbType.Int, cbxGpoIngresos.SelectedValue)
        BaseII.CreateMyParameter("@CONCEPTO", SqlDbType.VarChar, "", 250)
        cbxConceptoIngresos.DataSource = BaseII.ConsultaDT("uspFiltraConceptosIngresos")

        'If CargaGruposI Then

        '    With Me.cbxConceptoIngresos
        '        .DataSource = FiltraConceptos()
        '        .ValueMember = "Clave"
        '        .DisplayMember = "Concepto"
        '    End With
        'End If
    End Sub

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click



        AgregaDetalle()

        If eRes = 1 Then
            MessageBox.Show("El Concepto de Póliza ya se encuentra relacionado con otro Ingreso de Desglose.")
            eRes = 0
            Exit Sub
        End If

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IDINGRESO", SqlDbType.Int, CObj(IdConceptoText.Text))
        dgvRelacion.DataSource = BaseII.ConsultaDT("uspSeleccionaRelacionIngresosConceptos")

        'AgregaDetalle()
        'Me.dgvRelacion.DataSource = TraeDetalle()
        'ocultaColumnas(Me.dgvRelacion)
    End Sub

    Private Sub dgvRelacion_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvRelacion.CellClick

        'SeleccionaFila(Me.dgvRelacion, 0)
    End Sub

    Private Sub dgvRelacion_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvRelacion.CellContentClick

    End Sub
    'Private Sub SeleccionaFila(ByVal dgv As DataGridView, ByVal Fila As Integer)
    '    Try
    '        Dim i As Integer = 0

    '        If Fila = 0 Then
    '            i = dgv.CurrentRow.Index
    '        End If

    '        Dim indexGrid As ColumnasGrid = obtenerIdexGridTrabajos(dgv)

    '        Me.txtID.Text = dgv.Item(indexGrid.idxID, i).Value
    '        Me.txtClave.Text = dgv.Item(indexGrid.idxClave, i).Value
    '    Catch ex As Exception
    '        'MsgBox(ex.Message, vbInformation, "Atencion!!!")
    '    End Try
    'End Sub

    'Private Function obtenerIdexGridTrabajos(ByVal dgv As DataGridView) As ColumnasGrid
    '    Dim resultado As New ColumnasGrid
    '    Dim indx As Integer = 0


    '    For Each dc As DataGridViewColumn In dgv.Columns
    '        If (dc.Name = "ID") Then
    '            resultado.idxClave = indx
    '            indx = indx + 1
    '        ElseIf (dc.Name = "Clave") Then
    '            resultado.idxClave = indx
    '            indx = indx + 1
    '        ElseIf (dc.Name = "Concepto") Then
    '            resultado.idxConcepto = indx
    '            indx = indx + 1
    '        Else
    '            indx = indx + 1
    '        End If
    '    Next
    '    Return resultado
    'End Function

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        EliminaDetalle()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IDINGRESO", SqlDbType.Int, CObj(IdConceptoText.Text))
        dgvRelacion.DataSource = BaseII.ConsultaDT("uspSeleccionaRelacionIngresosConceptos")

        'EliminaDetalle()
        'Me.dgvRelacion.DataSource = TraeDetalle()
        'ocultaColumnas(Me.dgvRelacion)
    End Sub

    'Private Sub dgvRelacion_CurrentCellChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvRelacion.CurrentCellChanged
    '    Try
    '        SeleccionaFila(Me.dgvRelacion, 0)
    '    Catch ex As Exception

    '    End Try
    'End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.close()
    End Sub


End Class