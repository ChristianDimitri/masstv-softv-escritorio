Imports System.Data.SqlClient
Imports System.Net.Mail
Imports System.Net.NetworkInformation
Imports System.Net
Imports System.Net.Sockets
Public Class FrmCorreo
    Dim Con, Ins, Sus, Des, Baj, Tem, Fue, Per1, Per2 As Integer
    Dim eCuenta As String = Nothing
    Dim ePass As String = Nothing
    Dim eHost As String = Nothing
    Dim ePort As Integer = 0


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        Try
            Dim CONE As New SqlConnection(MiConexion)
            Dim CON1 As New SqlConnection(MiConexion)
            CON1.Open()
            Me.ConGeneralCorreoTableAdapter.Connection = CON1
            Me.ConGeneralCorreoTableAdapter.Fill(Me.DataSetEric.ConGeneralCorreo)
            CON1.Close()


            eCuenta = Me.CuentaTextBox.Text
            ePass = Me.PasswordTextBox.Text
            eHost = Me.HostTextBox.Text
            If IsNumeric(Me.PortTextBox.Text) = True Then
                ePort = CType(Me.PortTextBox.Text, Integer)
            Else
                ePort = 0
            End If

            If eCuenta.Length = 0 Or ePass.Length = 0 Or eHost.Length = 0 Or ePort.ToString.Length = 0 Then
                MsgBox("No se Puede realizar esta Operaci�n debido a que carece de los datos Necearios (Correo,Password,Host,Port)para enviar el Correo. Consulta Generales del Sistema.", , "Error")
                Exit Sub
            End If

            If Me.CheckBox1.Checked = False And Me.CheckBox3.Checked = False And Me.CheckBox6.Checked = False And Me.CheckBox4.Checked = False And Me.CheckBox5.Checked = False And Me.CheckBox2.Checked = False And Me.CheckBox7.Checked = False Then
                MsgBox("Selecciona al menos un Status.", , "Atenci�n")
                Exit Sub
            End If

            If Me.TextBox2.Text.Length <= 0 Then
                MsgBox("Captura el Asunto.", , "Atenci�n")
                Exit Sub
            End If
            If Me.TextBox1.Text.Length <= 0 Then
                MsgBox("Captura el mensaje.,", , "Atenci�n")
                Exit Sub
            End If

            Me.lblStatus.Text = "Enviando... "
            Me.lblStatus.Refresh()

            Me.GroupBox1.Enabled = False
            Me.GroupBox2.Enabled = False
            Me.GroupBox3.Enabled = False
            Me.Button2.Enabled = False

            If Me.CheckBox1.Checked = True Then
                Con = 1
            Else
                Con = 0
            End If

            If Me.CheckBox3.Checked = True Then
                Ins = 1
            Else
                Ins = 0
            End If

            If Me.CheckBox4.Checked = True Then
                Des = 1
            Else
                Des = 0
            End If

            If Me.CheckBox5.Checked = True Then
                Sus = 1
            Else
                Sus = 0
            End If

            If Me.CheckBox2.Checked = True Then
                Baj = 1
            Else
                Baj = 0
            End If

            If Me.CheckBox7.Checked = True Then
                Tem = 1
            Else
                Tem = 0
            End If

            If Me.CheckBox6.Checked = True Then
                Fue = 1
            Else
                Fue = 0
            End If

            If Me.CheckBox8.Checked = True Then
                Per1 = 1
            Else
                Per1 = 0
            End If

            If Me.CheckBox9.Checked = True Then
                Per2 = 1
            Else
                Per2 = 0
            End If


            Dim eEmail As String = Nothing
            Dim eContrato As Long = 0
            Dim eTotal As Integer = 0
            Dim ContT As Integer = 0
            Dim ContS As Integer = 0
            Dim ContN As Integer = 0


            CONE.Open()
            Dim comando As SqlClient.SqlCommand
            Dim reader As SqlDataReader
            comando = New SqlClient.SqlCommand
            With comando
                .Connection = CONE
                .CommandText = "EXEC MandarCorreoClientes " & CType(LocClv_session, String) & "," & CType(Con, String) & "," & CType(Ins, String) & "," & CType(Sus, String) & "," & CType(Des, String) & "," & CType(Baj, String) & "," & CType(Fue, String) & "," & CType(Tem, String) & "," & CType(Per1, String) & "," & CType(Per2, String)
                .CommandType = CommandType.Text
                .CommandTimeout = 0
            End With 'FIN WITH
            reader = comando.ExecuteReader(CommandBehavior.CloseConnection)
            'Using reader

            While reader.Read
                ContT = ContT + 1
                eContrato = reader.Item(1)
                eEmail = reader.GetValue(2)
                eTotal = reader.Item(3)

                Me.lblStatus.Text = "Enviando... " & CStr(ContT) & " de " & CStr(eTotal) & " correos."
                Me.lblStatus.Refresh()



                Try
                    Dim miEmail As New System.Net.Mail.MailMessage
                    With miEmail
                        .From = New System.Net.Mail.MailAddress(eCuenta)
                        .To.Add(eEmail)
                        .Subject = Me.TextBox2.Text
                        .Body = Me.TextBox1.Text
                        .IsBodyHtml = True
                    End With

                    Dim miSMTP As New System.Net.Mail.SmtpClient
                    miSMTP.UseDefaultCredentials = False
                    miSMTP.Credentials = New System.Net.NetworkCredential(eCuenta, ePass)
                    miSMTP.Port = ePort
                    miSMTP.Host = eHost
                    miSMTP.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network
                    miSMTP.Send(miEmail)


                    CON1.Open()
                    Me.InsPreDetBitacoraCorreoTableAdapter.Connection = CON1
                    Me.InsPreDetBitacoraCorreoTableAdapter.Fill(Me.DataSetEric2.InsPreDetBitacoraCorreo, LocClv_session, eContrato, eEmail, 0, "Se envi� correctamente.")
                    CON1.Close()
                    ContS = ContS + 1

                Catch eX As Exception

                    CON1.Open()
                    Me.InsPreDetBitacoraCorreoTableAdapter.Connection = CON1
                    Me.InsPreDetBitacoraCorreoTableAdapter.Fill(Me.DataSetEric2.InsPreDetBitacoraCorreo, LocClv_session, eContrato, eEmail, 1, eX.Message)
                    CON1.Close()
                    ContN = ContN + 1

                End Try 'FIN DEL TRY QUE MANDA CORREOS



            End While 'FIN DEL WHILE QUE TRAE LA CONSULTA
            ' End Using 'FIN DE USING


            Me.lblStatus.Text = "Correos enviados con �xito: " & CStr(ContS) & ". Correos enviados fallidos: " & CStr(ContN)
            CON1.Open()
            Me.GrabaBitacoraCorreoTableAdapter.Connection = CON1
            Me.GrabaBitacoraCorreoTableAdapter.Fill(Me.DataSetEric2.GrabaBitacoraCorreo, LocClv_session, eClv_Usuario, "Asunto: " & Me.TextBox2.Text & ", Mensaje: " & Me.TextBox1.Text, eClaveCorreo)
            CON1.Close()
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Mandaron Correos", "Asunto:" + Me.TextBox2.Text, "Mensaje: " + Me.TextBox1.Text, LocClv_Ciudad)
            CONE.Close()
            Me.Button1.Enabled = True
        Catch ex As System.Exception
            Me.Button1.Enabled = False
            System.Windows.Forms.MessageBox.Show(ex.Message + " " + ex.ToString)
        End Try 'FIN DEL PRIMER TRY


    End Sub

    Private Sub FrmCorreo_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Dim CONE As New SqlConnection(MiConexion)
        CONE.Open()
        Me.ConGeneralCorreoTableAdapter.Connection = CONE
        Me.ConGeneralCorreoTableAdapter.Fill(Me.DataSetEric.ConGeneralCorreo)
        CONE.Close()
    End Sub

    
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eOpVentas = 57
        eOpCorreo = 1
        FrmImprimirComision.Show()
    End Sub
End Class