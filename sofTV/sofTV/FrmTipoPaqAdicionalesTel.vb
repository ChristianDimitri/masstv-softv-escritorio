Imports System.Data.SqlClient
Public Class FrmTipoPaqAdicionalesTel
    Dim Descripcion As String = Nothing
    Dim Tipo As String = Nothing
    Dim clave_Pais As Long
    Private Sub DameDatosBitacora()
        Descripcion = Me.DescripcionTextBox.Text
        Tipo = Me.DescripcionComboBox.Text
    End Sub

    Private Sub guardabitacora()
        'LocOpTipoPaqAdic
        If LocOpTipoPaqAdic = "M" Then
            'Descripcion = Me.DescripcionTextBox.Text
            bitsist(GloUsuario, 0, LocGloSistema, Me.Text, Me.DescripcionTextBox.Name + "," + Me.Clv_Tipo_Paquete_AdiocionalTextBox.Text, Descripcion, Me.DescripcionTextBox.Text, LocClv_Ciudad)
            'Tipo = Me.DescripcionComboBox.Text
            bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Tipo De Cobro , " + Me.Clv_Tipo_Paquete_AdiocionalTextBox.Text, Tipo, Me.DescripcionComboBox.Text, LocClv_Ciudad)
        ElseIf LocOpTipoPaqAdic = "N" Then
            bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Se Agrego Un Nuevo Tipo Paquete Adicional", Me.Clv_Tipo_Paquete_AdiocionalTextBox.Text, "", LocClv_Ciudad)
        End If
        DameDatosBitacora()

    End Sub
    Private Sub Consulta_tipo_paquete_AdicionalBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Consulta_tipo_paquete_AdicionalBindingNavigatorSaveItem.Click
        If Me.DescripcionTextBox.Text = "" Then
            MsgBox("No Se Ha Capturado La Descripci�n Del Tipo De Pago", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Me.DescripcionComboBox.Text = "" Then
            MsgBox("No Se ha Seleccionado El Tipo De Cobro", MsgBoxStyle.Information)
            Exit Sub
        End If


        If LocOpTipoPaqAdic = "M" Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.Consulta_tipo_paquete_AdicionalTableAdapter.Connection = CON
            Me.Consulta_tipo_paquete_AdicionalTableAdapter.Update(CInt(Me.Clv_Tipo_Paquete_AdiocionalTextBox.Text), Me.DescripcionTextBox.Text, CInt(Me.DescripcionComboBox.SelectedValue), 0, CInt(Me.DescripcionComboBox1.SelectedValue))
            CON.Close()
            Guarda_detalle()
            LocTipoPaquerebnd = True
            guardabitacora()
            MsgBox("Se Guardo Con Exito", MsgBoxStyle.Information)
            Me.Close()
        End If

        If LocOpTipoPaqAdic = "N" Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.Nuevo_Tipo_Paquete_AdicionalTableAdapter.Connection = CON
            Me.Nuevo_Tipo_Paquete_AdicionalTableAdapter.Fill(Me.Procedimientosarnoldo4.Nuevo_Tipo_Paquete_Adicional, eClv_Tipo_Paquete_Adicional, Me.DescripcionTextBox.Text, CInt(Me.DescripcionComboBox.SelectedValue), 0, CInt(Me.DescripcionComboBox1.SelectedValue))
            CON.Close()

            Me.Clv_Tipo_Paquete_AdiocionalTextBox.Text = eClv_Tipo_Paquete_Adicional
            LocTipoPaquerebnd = True
            guardabitacora()
            LocOpTipoPaqAdic = "M"
            Me.Panel1.Enabled = True
            Me.Panel2.Enabled = True
            Me.BindingNavigatorDeleteItem.Enabled = True
            MsgBox("Se Guardo Con Exito", MsgBoxStyle.Information)
        End If
        
        

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Consulta_tipo_paquete_AdicionalBindingSource.CancelEdit()
        Me.Close()
    End Sub

    Private Sub DescripcionTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles DescripcionTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.DescripcionTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub DescripcionTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DescripcionTextBox.TextChanged

    End Sub
    Private Sub Busca(ByVal clave As Long)
        Dim CON4 As New SqlConnection(MiConexion)
        CON4.Open()
        Try
            Me.Muestra_CatalogoTipoCobrosTelTableAdapter.Connection = CON4
            Me.Muestra_CatalogoTipoCobrosTelTableAdapter.Fill(Me.Procedimientosarnoldo4.Muestra_CatalogoTipoCobrosTel, 0)
            Me.Consulta_tipo_paquete_AdicionalTableAdapter.Connection = CON4
            Me.Consulta_tipo_paquete_AdicionalTableAdapter.Fill(Me.Procedimientosarnoldo4.Consulta_tipo_paquete_Adicional, clave)
            If IsNumeric(Me.Clasificacion_LDTextBox.Text) = False Then
                Me.Clasificacion_LDTextBox.Text = 0
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON4.Close()
    End Sub

    Private Sub FrmTipoPaqAdicionalesTel_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Borra_Tmp()
    End Sub

    Private Sub FrmTipoPaqAdicionalesTel_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
        DameClv_Session()


        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Catalogo_Clasificacion_LlamnadasTableAdapter.Connection = CON
        Me.Catalogo_Clasificacion_LlamnadasTableAdapter.Fill(Me.DataSetTelefonia.Catalogo_Clasificacion_Llamnadas)
        CON.Close()
        CON.Open()
        Me.ServiciosTableAdapter.Connection = CON
        Me.ServiciosTableAdapter.Fill(Me.DataSetTelefonia.Servicios)
        CON.Close()

        Select Case LocOpTipoPaqAdic
            Case "N"
                CON.Open()
                Me.Consulta_tipo_paquete_AdicionalBindingSource.AddNew()
                Me.Muestra_CatalogoTipoCobrosTelTableAdapter.Connection = CON
                Me.Muestra_CatalogoTipoCobrosTelTableAdapter.Fill(Me.Procedimientosarnoldo4.Muestra_CatalogoTipoCobrosTel, 0)
                Me.DescripcionComboBox.Text = ""
                CON.Close()
                Me.BindingNavigatorDeleteItem.Enabled = False
                DameClasificacionLlamadas(0, 0)
                Me.Panel1.Enabled = False
                Me.Panel2.Enabled = False
                Me.BindingNavigatorDeleteItem.Enabled = False
            Case "C"
                Me.CMBPanel1.Enabled = False
                Me.Button2.Enabled = False
                Me.Button4.Enabled = False
                Busca(LocGloTipoPaquete)
                DameDatosBitacora()

                DameClasificacionLlamadas(CInt(Me.Clasificacion_LDTextBox.Text), 1)
                Me.Panel1.Enabled = False
                Me.Panel2.Enabled = False
                Me.Consulta_tipo_paquete_AdicionalBindingNavigator.Enabled = False

            Case "M"
                Me.CMBPanel1.Enabled = True
                Busca(LocGloTipoPaquete)
                DameDatosBitacora()

                DameClasificacionLlamadas(CInt(Me.Clasificacion_LDTextBox.Text), 1)
        End Select
        Llena_Paises_Con_Rel()
        Llena_Paises_Sin_Rel()
        Consultar_Detalle()
    End Sub
    Private Sub Llena_Paises_Con_Rel()
        Dim I As Integer
        Dim Fila1 As DataRow
        Dim cone As New SqlClient.SqlConnection(MiConexion)
        Try
            TreeView1.Nodes.Clear()
            cone.Open()
            Me.Muestra_PaisesRelTipoPaqTableAdapter.Connection = cone
            Me.Muestra_PaisesRelTipoPaqTableAdapter.Fill(Me.DataSetLidia2.Muestra_PaisesRelTipoPaq, Me.Clv_Tipo_Paquete_AdiocionalTextBox.Text, 1)
            cone.Close()
            For Each Fila1 In Me.DataSetLidia2.Muestra_PaisesRelTipoPaq
                Me.TreeView1.Nodes.Add(Fila1("Clv_Pais").ToString(), Fila1("Descripcion").ToString + " - " + Fila1("Lada_Pais").ToString)
                Me.TreeView1.Nodes(I).Tag = Fila1("Clv_Pais").ToString
                I += 1
            Next
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Llena_Paises_Sin_Rel()
        Dim I As Integer
        Dim Fila2 As DataRow
        Dim cone As New SqlClient.SqlConnection(MiConexion)
        Try
            TreeView2.Nodes.Clear()
            cone.Open()
            Me.Muestra_PaisesRelTipoPaqTableAdapter.Connection = cone
            Me.Muestra_PaisesRelTipoPaqTableAdapter.Fill(Me.DataSetLidia2.Muestra_PaisesRelTipoPaq, Me.Clv_Tipo_Paquete_AdiocionalTextBox.Text, 0)
            cone.Close()
            For Each Fila2 In Me.DataSetLidia2.Muestra_PaisesRelTipoPaq
                Me.TreeView2.Nodes.Add(Fila2("Clv_Pais").ToString(), Fila2("Descripcion").ToString + " - " + Fila2("Lada_Pais").ToString)
                Me.TreeView2.Nodes(I).Tag = Fila2("Clv_Pais").ToString
                I += 1
            Next
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON1 As New SqlConnection(MiConexion)
        CON1.Open()
        Me.Consulta_tipo_paquete_AdicionalTableAdapter.Connection = CON1
        Me.Consulta_tipo_paquete_AdicionalTableAdapter.Delete(LocGloTipoPaquete)
        CON1.Close()
        MsgBox("Se Elimino Con Exito", MsgBoxStyle.Information)
        LocTipoPaquerebnd = True
        Me.Close()
    End Sub

    Private Sub Tipo_CobroTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Tipo_CobroTextBox.TextChanged
        If LocOpTipoPaqAdic = "C" Or LocOpTipoPaqAdic = "M" Then
            If IsNumeric(Me.Tipo_CobroTextBox.Text) = True Then
                If CLng(Me.Tipo_CobroTextBox.Text) > 0 Then
                    Me.DescripcionComboBox.SelectedIndex = CLng(Me.Tipo_CobroTextBox.Text) - 1
                End If
            End If
        End If
    End Sub

    Private Sub DescripcionComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DescripcionComboBox.SelectedIndexChanged
        If Me.DescripcionComboBox.Text <> "" Then
            Me.Tipo_CobroTextBox.Text = Me.DescripcionComboBox.SelectedValue
        End If
    End Sub

    Private Sub TreeView2_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView2.AfterSelect
        If IsNumeric(e.Node.Tag) = True Then
            clave_Pais = e.Node.Tag
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Modifica_Arbol(0)
        Llena_Paises_Con_Rel()
        Llena_Paises_Sin_Rel()
    End Sub

    Private Sub DameClv_Session()
        Dim cmd As New SqlCommand
        Dim cone As New SqlConnection(MiConexion)
        cone.Open()
        With cmd
            .CommandText = "DameClv_Session"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = cone
            Dim prm As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
            prm.Direction = ParameterDirection.Output
            prm.Value = 0
            .Parameters.Add(prm)
            Dim j As Integer = .ExecuteNonQuery
            Me.TextBoxClv_Session.Text = prm.Value
        End With
        
        cone.Close()
    End Sub

    Private Sub Guarda_detalle()
        Dim cone As New SqlConnection(MiConexion)
        Try


            Dim cmd As New SqlCommand

            cone.Open()
            With cmd
                .CommandText = "Guarda_Rel_Servicios_PaqAdic"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = cone
                Dim prm As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                If IsNumeric(Me.TextBoxClv_Session.Text) = False Then Me.TextBoxClv_Session.Text = 0
                prm.Value = Me.TextBoxClv_Session.Text
                .Parameters.Add(prm)
                prm = New SqlParameter("@Clv_Tipo_Paquete_Adic", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                If IsNumeric(Me.Clv_Tipo_Paquete_AdiocionalTextBox.Text) = False Then Me.Clv_Tipo_Paquete_AdiocionalTextBox.Text = 0
                prm.Value = Me.Clv_Tipo_Paquete_AdiocionalTextBox.Text
                .Parameters.Add(prm)
                Dim j As Integer = .ExecuteNonQuery
            End With
            cone.Close()
        Catch ex As Exception
            cone.Close()
        End Try
    End Sub


    Private Sub Modifica_Arbol(ByVal opt As Integer)
        Dim cmd As New SqlCommand
        Dim cone As New SqlConnection(MiConexion)
        cone.Open()
        With cmd
            .CommandText = "Modifica_PaisesRel"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = cone
            Dim prm As New SqlParameter("@Clv_tipPaqAdic", SqlDbType.BigInt)
            Dim prm1 As New SqlParameter("@Clv_Pais", SqlDbType.Int)
            Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
            prm.Direction = ParameterDirection.Input
            prm2.Direction = ParameterDirection.Input
            prm1.Direction = ParameterDirection.Input
            prm.Value = Me.Clv_Tipo_Paquete_AdiocionalTextBox.Text
            prm1.Value = clave_Pais
            prm2.Value = opt
            .Parameters.Add(prm)
            .Parameters.Add(prm1)
            .Parameters.Add(prm2)
            Dim j As Integer = .ExecuteNonQuery

        End With
        If opt = 1 Then
            bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Bot�n Agregar", "Se Agrego al tipo de Paquete: " + CStr(Me.Clv_Tipo_Paquete_AdiocionalTextBox.Text), "Se agrego el Pais con Clave :", GloCiudad)
        End If
        cone.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Modifica_Arbol(1)
        Llena_Paises_Con_Rel()
        Llena_Paises_Sin_Rel()
    End Sub

    Private Sub TreeView1_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView1.AfterSelect
        If IsNumeric(e.Node.Tag) = True Then
            clave_Pais = e.Node.Tag
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Modifica_Arbol(2)
        Llena_Paises_Con_Rel()
        Llena_Paises_Sin_Rel()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Modifica_Arbol(3)
        Llena_Paises_Con_Rel()
        Llena_Paises_Sin_Rel()
    End Sub

    Private Sub Label3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Consultar_Detalle()
        Dim Con_temp As New SqlConnection(MiConexion)
        Try
            If IsNumeric(Me.Clv_Tipo_Paquete_AdiocionalTextBox.Text) = False Then Me.Clv_Tipo_Paquete_AdiocionalTextBox.Text = 0
            If IsNumeric(Me.TextBoxClv_Session.Text) = False Then Me.TextBoxClv_Session.Text = 0
            Con_temp.Open()
            Me.Consultar_Rel_Servicios_PaqAdic_TMPTableAdapter.Connection = Con_temp
            Me.Consultar_Rel_Servicios_PaqAdic_TMPTableAdapter.Fill(Me.DataSetTelefonia.consultar_Rel_Servicios_PaqAdic_TMP, New System.Nullable(Of Long)(CType(Me.TextBoxClv_Session.Text, Long)), New System.Nullable(Of Integer)(CType(Me.Clv_Tipo_Paquete_AdiocionalTextBox.Text, Integer)))
            Con_temp.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    
    Private Sub Nuevo_Detalle()
        Try
            Dim xcon As New SqlConnection(MiConexion)
            xcon.Open()
            Me.Nuevo_Rel_Servicios_PaqAdic_TMPTableAdapter.Connection = xcon
            Me.Nuevo_Rel_Servicios_PaqAdic_TMPTableAdapter.Fill(Me.DataSetTelefonia.Nuevo_Rel_Servicios_PaqAdic_TMP, New System.Nullable(Of Long)(CType(Me.TextBoxClv_Session.Text, Long)), New System.Nullable(Of Integer)(CType(0, Integer)), New System.Nullable(Of Integer)(CType(Me.ComboBox2.SelectedValue, Integer)), New System.Nullable(Of Decimal)(CType(Me.TextBoxCosto_Det.Text, Decimal)))
            xcon.Close()
            Me.TextBoxCosto_Det.Text = 0
            Consultar_Detalle()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub borrar_Detalle()
        Try
            Dim xcon As New SqlConnection(MiConexion)
            xcon.Open()
            Me.Borrar_Rel_Servicios_PaqAdic_TMPTableAdapter.Connection = xcon
            If IsNumeric(Me.Clv_ServicioTextBox.Text) = False Then Me.Clv_ServicioTextBox.Text = 0
            Me.Borrar_Rel_Servicios_PaqAdic_TMPTableAdapter.Fill(Me.DataSetTelefonia.Borrar_Rel_Servicios_PaqAdic_TMP, New System.Nullable(Of Long)(CType(Me.TextBoxClv_Session.Text, Long)), New System.Nullable(Of Integer)(CType(Me.Clv_Tipo_Paquete_AdiocionalTextBox.Text, Integer)), New System.Nullable(Of Integer)(CType(Me.Clv_ServicioTextBox.Text, Integer)))
            xcon.Close()
            Consultar_Detalle()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        AgregarAlDetalle()
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        borrar_Detalle()
    End Sub

    Private Sub Borra_Tmp()
        Dim con As New SqlConnection(MiConexion)
        Try
            If IsNumeric(Me.TextBoxClv_Session.Text) = False Then Me.TextBoxClv_Session.Text = 0
            con.Open()
            Me.DataTable1TableAdapter.Connection = con
            Me.DataTable1TableAdapter.Fill(Me.DataSetTelefonia.DataTable1, New System.Nullable(Of Long)(CType(Me.TextBoxClv_Session.Text, Long)))
            con.Close()
        Catch ex As System.Exception
            con.Close()
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    
    Private Sub Clv_ServicioTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_ServicioTextBox.TextChanged
        If IsNumeric(Me.Clv_ServicioTextBox.Text) = True Then
            Me.ComboBox2.Text = Me.DESCRIPCIONTextBox1.Text
            Me.ComboBox2.SelectedValue = Me.Clv_ServicioTextBox.Text
            Me.TextBoxCosto_Det.Text = Me.CostoTextBox.Text
        End If
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        Me.TextBoxCosto_Det.Text = 0
    End Sub

    Private Sub DameClasificacionLlamadas(ByVal Clave As Integer, ByVal Op As Integer)
        Dim con As New SqlConnection(MiConexion)
        con.Open()
        Me.DameClasificacionLlamadasTableAdapter.Connection = con
        Me.DameClasificacionLlamadasTableAdapter.Fill(Me.Procedimientosarnoldo4.DameClasificacionLlamadas, Clave, Op)
        con.Close()

    End Sub
    
    Private Sub AgregarAlDetalle()
        If IsNumeric(Me.ComboBox2.SelectedValue) = True Then
            If IsNumeric(Me.TextBoxCosto_Det.Text) = False Then Me.TextBoxCosto_Det.Text = 0
            If IsNumeric(Me.TextBoxCosto_Det.Text) = True Then
                Nuevo_Detalle()
            Else
                MsgBox("Captura el Costo", MsgBoxStyle.Information)
            End If
        Else
            MsgBox("Seleccione el Plan Tarifario", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub TextBoxCosto_Det_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBoxCosto_Det.KeyDown
        If e.KeyCode = Keys.Enter Then
            AgregarAlDetalle()
        End If
    End Sub

End Class