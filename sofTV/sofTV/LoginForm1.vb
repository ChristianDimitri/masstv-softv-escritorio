Imports System
Imports System.IO
Imports System.Net
Imports System.Net.Sockets
Imports Microsoft.VisualBasic
Imports System.Drawing
Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Threading
Imports System.Text

Public Class LoginForm1
    'Hola
    Dim objDraw As System.Drawing.Drawing2D.GraphicsPath = _
  New System.Drawing.Drawing2D.GraphicsPath
    Dim objDraw2 As System.Drawing.Drawing2D.GraphicsPath = _
    New System.Drawing.Drawing2D.GraphicsPath

    ' TODO: inserte el código para realizar autenticación personalizada usando el nombre de usuario y la contraseña proporcionada 
    ' (Consulte http://go.microsoft.com/fwlink/?LinkId=35339).  
    ' El objeto principal personalizado se puede adjuntar al objeto principal del subproceso actual como se indica a continuación: 
    '     My.User.CurrentPrincipal = CustomPrincipal
    ' donde CustomPrincipal es la implementación de IPrincipal utilizada para realizar la autenticación. 
    ' Posteriormente, My.User devolverá la información de identidad encapsulada en el objeto CustomPrincipal
    ' como el nombre de usuario, nombre para mostrar, etc.

    Dim eBnd_Acceso As Integer = 0

    Private Sub OBTEN_IP()
        Dim host As String
        host = Dns.GetHostName
        GloHostName = host
        'Dim hostNameOrAddress As String
        Dim returnValue As IPAddress()
        returnValue = Dns.GetHostAddresses(host)
        GloIpMaquina = returnValue.GetValue(0).ToString
    End Sub

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Private Sub Verifica_Ruta_reportes()
        Dim cad(4) As String
        cad = RutaReportes.Substring(2).Split("\")
        Dim ruta_new As New StringBuilder
        Dim CON1 As New SqlClient.SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()

        If String.Equals(GloIp, cad(0)) = False Then
            ruta_new.Append("\\")
            ruta_new.Append(GloIp)
            ruta_new.Append("\exes\Softv\Reportes")
            Try
                CON1.Open()
                cmd = New SqlCommand()
                With cmd
                    .CommandText = "Modifica_Ruta_reportes"
                    .Connection = CON1
                    .CommandTimeout = 0
                    .CommandType = CommandType.StoredProcedure
                    '@ruta varchar(max),@clave int

                    Dim prm As New SqlParameter("@ruta", SqlDbType.VarChar)
                    prm.Value = ruta_new.ToString()
                    prm.Direction = ParameterDirection.Input
                    .Parameters.Add(prm)

                    Dim prm1 As New SqlParameter("@clave", SqlDbType.Int)
                    prm1.Value = 1
                    prm1.Direction = ParameterDirection.Input
                    .Parameters.Add(prm1)

                    Dim i As Integer = .ExecuteNonQuery()
                End With
                Me.DameRutaTableAdapter.Connection = CON1
                Me.DameRutaTableAdapter.Fill(Me.DataSetLidia.DameRuta, 1, RutaReportes)
                CON1.Close()
            Catch ex As Exception
                System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub acceso()
        Try

            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            Dim eBnd As Integer = 0
            Dim bnd As Integer = 0
            Dim Bnd_1 As Integer = 0
            Dim Comando As SqlClient.SqlCommand
            OBTEN_IP()


            Try
                Comando = New SqlClient.SqlCommand
                With Comando
                    .Connection = CON
                    .CommandText = "ConGeneralRelBD"
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0

                    '' Create a SqlParameter for each parameter in the stored procedure.
                    Dim prm As New SqlParameter("@Clv_Id", SqlDbType.Int)
                    Dim prm1 As New SqlParameter("@NombreBD", SqlDbType.VarChar, 250)
                    Dim prm2 As New SqlParameter("@Clave_Txt", SqlDbType.VarChar, 50)
                    Dim prm3 As New SqlParameter("@Ciudad", SqlDbType.VarChar, 250)

                    prm.Direction = ParameterDirection.Output
                    prm1.Direction = ParameterDirection.Output
                    prm2.Direction = ParameterDirection.Output
                    prm3.Direction = ParameterDirection.Output

                    prm.Value = 0
                    prm1.Value = " "
                    prm2.Value = " "
                    prm3.Value = " "
                    .Parameters.Add(prm)
                    .Parameters.Add(prm1)
                    .Parameters.Add(prm2)
                    .Parameters.Add(prm3)
                    Dim i As Integer = Comando.ExecuteNonQuery()
                    locclv_id = prm.Value
                    LocBdd = prm1.Value
                    LocClv_Ciudad = prm2.Value
                    LocNomciudad = prm3.Value
                End With
            Catch
            End Try

            Me.DAMECAJA_SUCURSALTableAdapter.Connection = CON
            Me.DAMECAJA_SUCURSALTableAdapter.Fill(Me.NewSofTvDataSet.DAMECAJA_SUCURSAL, GloHostName, GloIpMaquina, Glocaja, GloClv_Sucursal, GloNomCaja, Bnd_1, GloTickets)
            'MsgBox(GloClv_Sucursal)
            'If Bnd_1 = 1 Then           
            GloUsuario = Me.UsernameTextBox.Text
            Me.DAMECLAVEUSUARIOTableAdapter.Connection = CON
            Me.DAMECLAVEUSUARIOTableAdapter.Fill(Me.DataSetEdgarRev2.DAMECLAVEUSUARIO, GloUsuario, GloClvUsuario)
            eClv_Usuario = GloClvUsuario
            Me.VerAccesoTableAdapter.Connection = CON
            Me.VerAccesoTableAdapter.Fill(Me.NewSofTvDataSet.VerAcceso, Me.UsernameTextBox.Text, Me.PasswordTextBox.Text, bnd)

            ''Aqui Preguntamos que Servicios Tiene Habilitada la Plaza
            HAB_TV = DAMESTATUSHAB(1)
            HAB_INTERNET = DAMESTATUSHAB(2)
            HAB_TVDIG = DAMESTATUSHAB(3)
            HAB_Telefonia = DAMESTATUSHAB(5)
            HAB_TVDIGCable = DAMESTATUSHAB(6)
            Servicio_Principal = DimeTipSer_CualEsPrincipal()

            If bnd = 1 Then
                Me.VerAccesoAdminTableAdapter.Connection = CON
                Me.VerAccesoAdminTableAdapter.Fill(Me.DataSetEric.VerAccesoAdmin, Me.UsernameTextBox.Text, Me.PasswordTextBox.Text, eBnd)
                CON.Close()
                If eBnd = 1 Then
                    eAccesoAdmin = True
                    gloescr = 0
                Else
                    eAccesoAdmin = False
                    gloescr = 1
                End If

                'Valida pago a SATV
                Dim validaPagoASatv As Integer
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@msj", ParameterDirection.Output, SqlDbType.VarChar, 800)
                BaseII.CreateMyParameter("@status", ParameterDirection.Output, SqlDbType.SmallInt)
                BaseII.ProcedimientoOutPut("Sp_ValidaCorteSistemaSatv")
                validaPagoASatv = BaseII.dicoPar("@status").ToString()
                If validaPagoASatv > 0 And GloUsuario <> "SISTE" Then
                    If validaPagoASatv = 1 Then
                        MsgBox(BaseII.dicoPar("@msj").ToString(), MsgBoxStyle.Exclamation)
                        System.Threading.Thread.Sleep(10000)
                    Else
                        MsgBox(BaseII.dicoPar("@msj").ToString(), MsgBoxStyle.Critical)
                        End
                    End If
                End If

                VerAccesoAdmin(1)
                If eBnd_Acceso = 1 Then
                    My.Forms.FrmCargarReporte.Show()
                    My.Forms.FrmCargarReporte.Close()
                    My.Forms.FrmMiMenu.Show()
                    GloClv_Factura = 0
                    Me.Close()
                Else
                    MsgBox("Acceso Denegado ", MsgBoxStyle.Information)
                    Me.UsernameTextBox.Clear()
                    Me.PasswordTextBox.Clear()
                    Exit Sub
                End If
            Else
                CON.Close()
                MsgBox("Acceso Denegado ", MsgBoxStyle.Information)
                Me.UsernameTextBox.Clear()
                Me.PasswordTextBox.Clear()
                'End
            End If
            'Else
            'MsgBox("La Maquina no tiene Acceso al Sistema de Faturación")
            'End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub LoginForm1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'DataSetEric.ConGeneralRelBD' Puede moverla o quitarla según sea necesario.
        'Me.ConGeneralRelBDTableAdapter.Fill(Me.DataSetEric.ConGeneralRelBD)

        'Dim a As New CultureInfo("es-MX")
        My.Application.ChangeCulture("es-MX")

        'Thread.CurrentThread.CurrentCulture = New CultureInfo("es-MX")
        'Thread.CurrentThread.CurrentUICulture = New CultureInfo("es-MX")
        'MsgBox(Today.ToString)


        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.DameRutaTableAdapter.Connection = CON
        Me.DameRutaTableAdapter.Fill(Me.DataSetLidia.DameRuta, 1, RutaReportes)

        Me.DameIdTableAdapter.Connection = CON
        Me.DameIdTableAdapter.Fill(Me.DataSetEric.DameId, IdSistema)
        'Verifica_ruta_reportes

        Verifica_Ruta_reportes()



        ' MsgBox(RutaReportes.ToString)

        'TODO: esta línea de código carga datos en la tabla 'DataSetEdgarRev2.DimeTipSer_CualEsPrincipal' Puede moverla o quitarla según sea necesario.
        Me.DimeTipSer_CualEsPrincipalTableAdapter.Connection = CON
        Me.DimeTipSer_CualEsPrincipalTableAdapter.Fill(Me.DataSetEdgarRev2.DimeTipSer_CualEsPrincipal)
        'TODO: esta línea de código carga datos en la tabla 'DataSetarnoldo.Muestra_Empresa' Puede moverla o quitarla según sea necesario.
        Me.Muestra_EmpresaTableAdapter.Connection = CON
        Me.Muestra_EmpresaTableAdapter.Fill(Me.DataSetarnoldo.Muestra_Empresa)
        'TODO: esta línea de código carga datos en la tabla 'DataSetLidia.MUESTRAIMAGEN' Puede moverla o quitarla según sea necesario.
        Me.MUESTRAIMAGENTableAdapter.Connection = CON
        Me.MUESTRAIMAGENTableAdapter.Fill(Me.DataSetLidia.MUESTRAIMAGEN)
        'TODO: esta línea de código carga datos en la tabla 'DataSetEDGAR.DameDatosGenerales' Puede moverla o quitarla según sea necesario.
        Me.DameDatosGeneralesTableAdapter.Connection = CON
        Me.DameDatosGeneralesTableAdapter.Fill(Me.DataSetEDGAR.DameDatosGenerales)
        Dim COMANDO As New SqlClient.SqlCommand
        Dim Comando2 As New SqlClient.SqlCommand
        'Dim Comando3 As New SqlClient.SqlCommand
        Dim reader As SqlDataReader
        With COMANDO
            .CommandText = "Dame_SubCiudad"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = CON
            Dim prm As New SqlParameter("@subciudad", SqlDbType.VarChar, 50)
            prm.Direction = ParameterDirection.Output
            prm.Value = ""
            .Parameters.Add(prm)
            Dim i As Integer = COMANDO.ExecuteNonQuery()
            SubCiudad = prm.Value
        End With
        With Comando2
            .Connection = CON
            .CommandText = "Exec DameDatosGenerales_2"
            .CommandType = CommandType.Text
            .CommandTimeout = 0
            reader = Comando2.ExecuteReader()
            Using reader
                While reader.Read
                    ' Process SprocResults datareader here.
                    GloEmpresa = reader.GetValue(1)
                    GloDireccionEmpresa = reader.GetValue(2)
                    GloColonia_CpEmpresa = reader.GetValue(3)
                    GloCiudadEmpresa = reader.GetValue(4)
                    GloRfcEmpresa = reader.GetValue(5)
                    GloTelefonoEmpresa = reader.GetValue(7)
                    ImpresoraOrd = reader.GetValue(23)
                End While
            End Using
        End With
        'With Comando3
        '    .Connection = CON
        '    .CommandText = "Exec ImpresoraOrd"
        '    .CommandType = CommandType.Text
        '    .CommandTimeout = 0
        '    reader = Comando3.ExecuteReader()
        '    Using reader
        '        While reader.Read
        '            ' Process SprocResults datareader here.
        '            GloEmpresa = reader.GetValue(1)
        '        End While
        '    End Using
        'End With


        glotiposervicioppal = Me.TextBox1.Text
        LocGloNomEmpresa = Me.Label3.Text
        Dim polyPoints() As Point = {New Point(320, 125), New Point(700, 125), New Point(700, 50), New Point(320, 50)}
        Dim polyPoints2() As Point = {New Point(320, 200), New Point(700, 200), New Point(700, 150), New Point(320, 150)}
        Dim polyPoints3() As Point = {New Point(559, 209), New Point(500 + 194, 209), New Point(500 + 194, 235), New Point(559, 235)} 'bOTON aCEPTAR
        Dim polyPoints4() As Point = {New Point(355, 209), New Point(489, 209), New Point(489, 235), New Point(355, 235)} 'bOTONES cANCELAR
        Dim polyPoints5() As Point = {New Point(70, 235), New Point(300, 235), New Point(300, 50), New Point(70, 50)}
        'objDraw.AddEllipse(100, 50, 200, 200)
        objDraw.AddPolygon(polyPoints5)
        objDraw.AddPolygon(polyPoints)
        objDraw.AddPolygon(polyPoints2)
        objDraw.AddPolygon(polyPoints3)
        objDraw.AddPolygon(polyPoints4)
        Me.Region = New Region(objDraw)

        CON.Close()
    End Sub

    Private Sub PasswordTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)

    End Sub

    Private Sub Cancel_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        End
        Me.Close()
    End Sub

    Private Sub OK_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        acceso()
    End Sub

    Private Sub PasswordTextBox_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles PasswordTextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            acceso()
        End If
    End Sub

    Private Sub CiudadTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CiudadTextBox.TextChanged
        GloSucursal = " Sucursal : " & CiudadTextBox.Text
        GloCiudad = CiudadTextBox.Text
    End Sub


    'Public Class Inicial
    '    Public Shared Sub Main(ByVal Parametro As String)
    '        Select Case Parametro
    '            Case "Formulario_1"
    '                BrwCiudades.Show()
    '                'mostrar el formulario'
    '            Case "Formulario_2"
    '                'mostrar otro formulario

    '        End Select
    '    End Sub

    Private Sub VerAccesoAdmin(ByVal Op As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("VerAccesoAdmin_2", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar, 10)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = Me.UsernameTextBox.Text
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@Pas", SqlDbType.VarChar, 20)
        PRM2.Direction = ParameterDirection.Input
        PRM2.Value = Me.PasswordTextBox.Text
        CMD.Parameters.Add(PRM2)


        Dim PRM4 As New SqlParameter("@op", SqlDbType.Int)
        PRM4.Direction = ParameterDirection.Input
        PRM4.Value = Op
        CMD.Parameters.Add(PRM4)

        Dim PRM3 As New SqlParameter("@bnd", SqlDbType.Int)
        PRM3.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM3)


        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            eBnd_Acceso = PRM3.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub
End Class
