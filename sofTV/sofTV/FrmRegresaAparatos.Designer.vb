﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRegresaAparatos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Cablemodem = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Mac = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Orden = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colEstadoAparato = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Aparato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colRecibio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colRegresarAlmacen = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.colTipoAparato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.btnRegresarAlmacen = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter5 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.tbBuscaOrden = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.tbBuscaMac = New System.Windows.Forms.TextBox()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter6 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.tbClvAparato = New System.Windows.Forms.TextBox()
        Me.Muestra_ServiciosDigitalesTableAdapter7 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.Clv_Cablemodem, Me.Mac, Me.Clv_Orden, Me.colEstadoAparato, Me.Aparato, Me.colRecibio, Me.colRegresarAlmacen, Me.colTipoAparato})
        Me.DataGridView1.Location = New System.Drawing.Point(13, 141)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(992, 552)
        Me.DataGridView1.TabIndex = 0
        '
        'ID
        '
        Me.ID.DataPropertyName = "ID"
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ID.DefaultCellStyle = DataGridViewCellStyle2
        Me.ID.HeaderText = "ID"
        Me.ID.Name = "ID"
        Me.ID.Visible = False
        '
        'Clv_Cablemodem
        '
        Me.Clv_Cablemodem.DataPropertyName = "CLV_CABLEMODEM"
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_Cablemodem.DefaultCellStyle = DataGridViewCellStyle3
        Me.Clv_Cablemodem.HeaderText = "Clave Aparato"
        Me.Clv_Cablemodem.Name = "Clv_Cablemodem"
        Me.Clv_Cablemodem.ReadOnly = True
        Me.Clv_Cablemodem.Width = 130
        '
        'Mac
        '
        Me.Mac.DataPropertyName = "MAC"
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Mac.DefaultCellStyle = DataGridViewCellStyle4
        Me.Mac.HeaderText = "Mac ó No. Serie"
        Me.Mac.Name = "Mac"
        Me.Mac.ReadOnly = True
        Me.Mac.Width = 170
        '
        'Clv_Orden
        '
        Me.Clv_Orden.DataPropertyName = "CLV_ORDEN"
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_Orden.DefaultCellStyle = DataGridViewCellStyle5
        Me.Clv_Orden.HeaderText = "No. Orden"
        Me.Clv_Orden.Name = "Clv_Orden"
        Me.Clv_Orden.ReadOnly = True
        Me.Clv_Orden.Width = 140
        '
        'colEstadoAparato
        '
        Me.colEstadoAparato.DataPropertyName = "EstadoAparato"
        Me.colEstadoAparato.HeaderText = "Buen Estado"
        Me.colEstadoAparato.Name = "colEstadoAparato"
        '
        'Aparato
        '
        Me.Aparato.DataPropertyName = "APARATO"
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Aparato.DefaultCellStyle = DataGridViewCellStyle6
        Me.Aparato.HeaderText = "Tipo"
        Me.Aparato.Name = "Aparato"
        Me.Aparato.ReadOnly = True
        Me.Aparato.Width = 140
        '
        'colRecibio
        '
        Me.colRecibio.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colRecibio.DataPropertyName = "RECIBIO"
        Me.colRecibio.HeaderText = "Recibió"
        Me.colRecibio.Name = "colRecibio"
        Me.colRecibio.ReadOnly = True
        '
        'colRegresarAlmacen
        '
        Me.colRegresarAlmacen.DataPropertyName = "RegresarAlmacen"
        Me.colRegresarAlmacen.HeaderText = "Regresar Almacen"
        Me.colRegresarAlmacen.Name = "colRegresarAlmacen"
        '
        'colTipoAparato
        '
        Me.colTipoAparato.DataPropertyName = "TipoAparato"
        Me.colTipoAparato.HeaderText = "Tipo Aparato"
        Me.colTipoAparato.Name = "colTipoAparato"
        Me.colTipoAparato.Visible = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(868, 710)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 36)
        Me.Button5.TabIndex = 30
        Me.Button5.Text = "&CERRAR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(721, 710)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 31
        Me.Button1.Text = "&IMPRIMIR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'btnRegresarAlmacen
        '
        Me.btnRegresarAlmacen.BackColor = System.Drawing.Color.DarkOrange
        Me.btnRegresarAlmacen.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRegresarAlmacen.Location = New System.Drawing.Point(470, 710)
        Me.btnRegresarAlmacen.Name = "btnRegresarAlmacen"
        Me.btnRegresarAlmacen.Size = New System.Drawing.Size(236, 36)
        Me.btnRegresarAlmacen.TabIndex = 32
        Me.btnRegresarAlmacen.Text = "&Regresar al Almacen"
        Me.btnRegresarAlmacen.UseVisualStyleBackColor = False
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter5
        '
        Me.Muestra_ServiciosDigitalesTableAdapter5.ClearBeforeFill = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(126, 111)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(231, 16)
        Me.Label2.TabIndex = 44
        Me.Label2.Text = "Introduce el No. Orden a buscar:"
        '
        'tbBuscaOrden
        '
        Me.tbBuscaOrden.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbBuscaOrden.Location = New System.Drawing.Point(391, 105)
        Me.tbBuscaOrden.Name = "tbBuscaOrden"
        Me.tbBuscaOrden.Size = New System.Drawing.Size(189, 26)
        Me.tbBuscaOrden.TabIndex = 43
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(150, 69)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(190, 16)
        Me.Label1.TabIndex = 42
        Me.Label1.Text = "Introduce la Mac a buscar:"
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(632, 99)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(236, 36)
        Me.Button3.TabIndex = 41
        Me.Button3.Text = "&Buscar por No. Orden"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(663, 57)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(171, 36)
        Me.Button2.TabIndex = 40
        Me.Button2.Text = "&Buscar por Mac"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'tbBuscaMac
        '
        Me.tbBuscaMac.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbBuscaMac.Location = New System.Drawing.Point(391, 63)
        Me.tbBuscaMac.Name = "tbBuscaMac"
        Me.tbBuscaMac.Size = New System.Drawing.Size(189, 26)
        Me.tbBuscaMac.TabIndex = 39
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkOrange
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(14, 710)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(185, 36)
        Me.Button4.TabIndex = 45
        Me.Button4.Text = "&Mostrar Todos"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Muestra_ServiciosDigitalesTableAdapter6
        '
        Me.Muestra_ServiciosDigitalesTableAdapter6.ClearBeforeFill = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(116, 27)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(260, 16)
        Me.Label3.TabIndex = 48
        Me.Label3.Text = "Introduce la Clave Aparato a buscar:"
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkOrange
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(633, 12)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(235, 36)
        Me.Button6.TabIndex = 47
        Me.Button6.Text = "&Buscar por Clv Aparato"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'tbClvAparato
        '
        Me.tbClvAparato.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbClvAparato.Location = New System.Drawing.Point(391, 21)
        Me.tbClvAparato.Name = "tbClvAparato"
        Me.tbClvAparato.Size = New System.Drawing.Size(189, 26)
        Me.tbClvAparato.TabIndex = 46
        '
        'Muestra_ServiciosDigitalesTableAdapter7
        '
        Me.Muestra_ServiciosDigitalesTableAdapter7.ClearBeforeFill = True
        '
        'FrmRegresaAparatos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1016, 755)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.tbClvAparato)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.tbBuscaOrden)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.tbBuscaMac)
        Me.Controls.Add(Me.btnRegresarAlmacen)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.DataGridView1)
        Me.MaximizeBox = False
        Me.Name = "FrmRegresaAparatos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Regresas Aparatos al Almacen"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents btnRegresarAlmacen As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Cablemodem As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Mac As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Orden As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colEstadoAparato As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Aparato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colRecibio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colRegresarAlmacen As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colTipoAparato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter5 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tbBuscaOrden As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents tbBuscaMac As System.Windows.Forms.TextBox
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter6 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents tbClvAparato As System.Windows.Forms.TextBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter7 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
