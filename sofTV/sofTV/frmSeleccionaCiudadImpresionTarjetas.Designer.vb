﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSeleccionaCiudadImpresionTarjetas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnDeseleccionarTodasCiudades = New System.Windows.Forms.Button()
        Me.btnDeseleccionarUnaCiudad = New System.Windows.Forms.Button()
        Me.btnSeleccionarTodasCiudades = New System.Windows.Forms.Button()
        Me.btSeleccionarUnaCiudad = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.lbCiudadSeleccionadas = New System.Windows.Forms.ListBox()
        Me.lbCiudadASeleccionar = New System.Windows.Forms.ListBox()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(283, 7)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(165, 15)
        Me.Label2.TabIndex = 19
        Me.Label2.Text = "Ciudades Seleccionadas"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(159, 15)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Ciudades a Seleccionar"
        '
        'btnDeseleccionarTodasCiudades
        '
        Me.btnDeseleccionarTodasCiudades.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeseleccionarTodasCiudades.Location = New System.Drawing.Point(240, 247)
        Me.btnDeseleccionarTodasCiudades.Name = "btnDeseleccionarTodasCiudades"
        Me.btnDeseleccionarTodasCiudades.Size = New System.Drawing.Size(40, 22)
        Me.btnDeseleccionarTodasCiudades.TabIndex = 17
        Me.btnDeseleccionarTodasCiudades.Text = "<<"
        Me.btnDeseleccionarTodasCiudades.UseVisualStyleBackColor = True
        '
        'btnDeseleccionarUnaCiudad
        '
        Me.btnDeseleccionarUnaCiudad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeseleccionarUnaCiudad.Location = New System.Drawing.Point(240, 275)
        Me.btnDeseleccionarUnaCiudad.Name = "btnDeseleccionarUnaCiudad"
        Me.btnDeseleccionarUnaCiudad.Size = New System.Drawing.Size(40, 22)
        Me.btnDeseleccionarUnaCiudad.TabIndex = 16
        Me.btnDeseleccionarUnaCiudad.Text = "<"
        Me.btnDeseleccionarUnaCiudad.UseVisualStyleBackColor = True
        '
        'btnSeleccionarTodasCiudades
        '
        Me.btnSeleccionarTodasCiudades.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSeleccionarTodasCiudades.Location = New System.Drawing.Point(240, 137)
        Me.btnSeleccionarTodasCiudades.Name = "btnSeleccionarTodasCiudades"
        Me.btnSeleccionarTodasCiudades.Size = New System.Drawing.Size(40, 22)
        Me.btnSeleccionarTodasCiudades.TabIndex = 15
        Me.btnSeleccionarTodasCiudades.Text = ">>"
        Me.btnSeleccionarTodasCiudades.UseVisualStyleBackColor = True
        '
        'btSeleccionarUnaCiudad
        '
        Me.btSeleccionarUnaCiudad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btSeleccionarUnaCiudad.Location = New System.Drawing.Point(240, 109)
        Me.btSeleccionarUnaCiudad.Name = "btSeleccionarUnaCiudad"
        Me.btSeleccionarUnaCiudad.Size = New System.Drawing.Size(40, 22)
        Me.btSeleccionarUnaCiudad.TabIndex = 14
        Me.btSeleccionarUnaCiudad.Text = ">"
        Me.btSeleccionarUnaCiudad.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(413, 363)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(117, 33)
        Me.btnCancelar.TabIndex = 13
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.Location = New System.Drawing.Point(290, 362)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(117, 33)
        Me.btnAceptar.TabIndex = 12
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'lbCiudadSeleccionadas
        '
        Me.lbCiudadSeleccionadas.DisplayMember = "NOMBRE"
        Me.lbCiudadSeleccionadas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbCiudadSeleccionadas.FormattingEnabled = True
        Me.lbCiudadSeleccionadas.ItemHeight = 15
        Me.lbCiudadSeleccionadas.Location = New System.Drawing.Point(286, 36)
        Me.lbCiudadSeleccionadas.Name = "lbCiudadSeleccionadas"
        Me.lbCiudadSeleccionadas.Size = New System.Drawing.Size(244, 319)
        Me.lbCiudadSeleccionadas.TabIndex = 11
        Me.lbCiudadSeleccionadas.ValueMember = "CLVCiudad"
        '
        'lbCiudadASeleccionar
        '
        Me.lbCiudadASeleccionar.DisplayMember = "NOMBRE"
        Me.lbCiudadASeleccionar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbCiudadASeleccionar.FormattingEnabled = True
        Me.lbCiudadASeleccionar.ItemHeight = 15
        Me.lbCiudadASeleccionar.Location = New System.Drawing.Point(9, 36)
        Me.lbCiudadASeleccionar.Name = "lbCiudadASeleccionar"
        Me.lbCiudadASeleccionar.Size = New System.Drawing.Size(225, 319)
        Me.lbCiudadASeleccionar.TabIndex = 10
        Me.lbCiudadASeleccionar.ValueMember = "CLVCiudad"
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'frmSeleccionaCiudadImpresionTarjetas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(540, 405)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnDeseleccionarTodasCiudades)
        Me.Controls.Add(Me.btnDeseleccionarUnaCiudad)
        Me.Controls.Add(Me.btnSeleccionarTodasCiudades)
        Me.Controls.Add(Me.btSeleccionarUnaCiudad)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.lbCiudadSeleccionadas)
        Me.Controls.Add(Me.lbCiudadASeleccionar)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSeleccionaCiudadImpresionTarjetas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selección de Ciudades Para Impresión de Tarjetas"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnDeseleccionarTodasCiudades As System.Windows.Forms.Button
    Friend WithEvents btnDeseleccionarUnaCiudad As System.Windows.Forms.Button
    Friend WithEvents btnSeleccionarTodasCiudades As System.Windows.Forms.Button
    Friend WithEvents btSeleccionarUnaCiudad As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents lbCiudadSeleccionadas As System.Windows.Forms.ListBox
    Friend WithEvents lbCiudadASeleccionar As System.Windows.Forms.ListBox
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
