﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Generic
Imports System.Configuration

Namespace Base
    Public Class CBase
        Protected ReadOnly cadenaConexion As String

        Public Sub New()
            cadenaConexion = MiConexion
        End Sub

        Public Function consultarHT(ByVal procedimientoAlmacenado As String, ByVal parametros As SqlParameter()) As ArrayList
            Using conexion As New SqlConnection(cadenaConexion)
                conexion.Open()

                Dim comando As New SqlCommand()
                comando.Connection = conexion
                comando.CommandType = CommandType.StoredProcedure
                comando.CommandTimeout = 0
                comando.CommandText = procedimientoAlmacenado

                If Not parametros Is Nothing Then
                    For Each param As SqlParameter In parametros
                        If param IsNot Nothing Then
                            comando.Parameters.Add(param)
                        End If
                    Next
                End If

                Dim resultados As SqlDataReader = comando.ExecuteReader()
                Dim registros As New ArrayList()

                While resultados.Read()
                    Dim hash As New Hashtable()
                    For columna As Integer = 0 To resultados.FieldCount - 1
                        hash(resultados.GetName(columna)) = resultados.GetValue(columna)
                    Next
                    registros.Add(hash)
                End While

                Return registros
            End Using
        End Function

        Public Function consultarDT(ByVal procedimientoAlmacenado As String, ByVal parametros As SqlParameter()) As DataTable
            Dim tabla As DataTable = Nothing
            Using conexion As New SqlConnection(MiConexion)
                conexion.Open()

                Dim comando As New SqlCommand()
                comando.Connection = conexion
                comando.CommandType = CommandType.StoredProcedure
                comando.CommandTimeout = 0
                comando.CommandText = procedimientoAlmacenado

                If Not parametros Is Nothing Then
                    For Each param As SqlParameter In parametros
                        If param IsNot Nothing Then
                            comando.Parameters.Add(param)
                        End If
                    Next
                End If

                Dim adaptador As New SqlDataAdapter()
                adaptador.SelectCommand = comando

                Dim ds As New DataSet()
                adaptador.Fill(ds, "Tabla")

                tabla = ds.Tables("Tabla")
            End Using

            Return tabla
        End Function

        Public Function consultarDS(ByVal procedimientoAlmacenado As String, ByVal parametros() As SqlParameter) As DataSet
            Using conexion As SqlConnection = New SqlConnection(MiConexion)
                conexion.Open()

                Dim comando As SqlCommand = New SqlCommand()
                comando.Connection = conexion
                comando.CommandType = CommandType.StoredProcedure
                comando.CommandTimeout = 0
                comando.CommandText = procedimientoAlmacenado

                If Not parametros Is Nothing Then
                    For Each param As SqlParameter In parametros
                        If param IsNot Nothing Then
                            comando.Parameters.Add(param)
                        End If
                    Next
                End If

                Dim adaptador As SqlDataAdapter = New SqlDataAdapter()
                adaptador.SelectCommand = comando

                Dim ds As DataSet = New DataSet()
                adaptador.Fill(ds, "Tabla")
                Return ds

            End Using
        End Function

        Public Function consultarXML(ByVal procedimientoAlmacenado As String, ByVal parametros() As SqlParameter, ByVal ruta As String) As String
            Using conexion As SqlConnection = New SqlConnection(MiConexion)
                conexion.Open()

                Dim comando As SqlCommand = New SqlCommand()
                comando.Connection = conexion
                comando.CommandType = CommandType.StoredProcedure
                comando.CommandTimeout = 0
                comando.CommandText = procedimientoAlmacenado

                If Not parametros Is Nothing Then
                    For Each param As SqlParameter In parametros
                        If param IsNot Nothing Then
                            comando.Parameters.Add(param)
                        End If
                    Next
                End If

                Dim adaptador As SqlDataAdapter = New SqlDataAdapter()
                adaptador.SelectCommand = comando

                Dim ds As DataSet = New DataSet()
                adaptador.Fill(ds, "Tabla")

                If Not String.IsNullOrEmpty(ruta) Then
                    ds.WriteXmlSchema(ruta)
                End If

                Return ds.GetXml()

            End Using
        End Function
    End Class

    
    Public Class CVisitaClientes

        Private _clv_visita As Integer
        Private _descripcion As String
        Private _fecha As DateTime
        Private _vendedor As String

        Public Property Clv_Visita() As Integer
            Get
                Return _clv_visita
            End Get
            Set(ByVal value As Integer)
                _clv_visita = value
            End Set
        End Property

        Public Property Descripion() As String
            Get
                Return _descripcion
            End Get
            Set(ByVal value As String)
                _descripcion = value
            End Set
        End Property

        Public Property Fecha() As DateTime
            Get
                Return _fecha
            End Get
            Set(ByVal value As DateTime)
                _fecha = value
            End Set
        End Property

        Public Property Vendedor() As String
            Get
                Return _vendedor
            End Get
            Set(ByVal value As String)
                _vendedor = value
            End Set
        End Property

    End Class
End Namespace

