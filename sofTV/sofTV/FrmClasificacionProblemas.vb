﻿Public Class FrmClasificacionProblemas

#Region "Variables Globales"
    Public ProblemaClvProblema As Long
    Public ProblemaDescripcion As String = Nothing
    Public ProblemaActivo As Boolean = False
    Public ProblemaOpcion As Char = Nothing
#End Region

#Region "Insertar"
    Private Sub Insertar(ByVal prmDescripcion As String, ByVal prmActivo As Boolean) 'PARA INSERTAR UN NUEVO REGISTRO
        Dim Problema As New ClassClasificacionProblemas
        Problema.clvProblema = 0
        Problema.Descripcion = prmDescripcion
        Problema.Activo = prmActivo
        Problema.OpAccion = 1

        If Problema.uspInsertaTblClasificacionProblemas() = True Then
            MsgBox("El Registro ya ha sido dado de alta anteriormente", MsgBoxStyle.Information)
            Exit Sub
        Else
            MsgBox("Registro Almacenado Satisfactoriamente", MsgBoxStyle.Information)
            Me.Close()
        End If
    End Sub

    Private Sub Insertar(ByVal prmClvProblema As Long, ByVal prmDescripcion As String, ByVal prmActivo As Boolean) 'PARA MODIFICAR UN REGISTRO EXISTENTE
        Dim Problema As New ClassClasificacionProblemas
        Problema.clvProblema = prmClvProblema
        Problema.Descripcion = prmDescripcion
        Problema.Activo = prmActivo
        Problema.OpAccion = 2

        If Problema.uspInsertaTblClasificacionProblemas() = True Then
            Exit Sub
        Else
            MsgBox("Registro Modificado Satisfactoriamente", MsgBoxStyle.Information)
            Me.Close()
        End If
    End Sub
#End Region

#Region "Eventos Controles"
    Private Sub FrmClasificacionProblemas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.txtDescripcion.Text = ProblemaDescripcion
        Me.cbxActivo.Checked = ProblemaActivo

        If ProblemaOpcion = "C" Then
            Me.txtDescripcion.Enabled = False
            Me.cbxActivo.Enabled = False
            Me.btnGuardar.Enabled = False
        Else
            Me.txtDescripcion.Enabled = True
            Me.cbxActivo.Enabled = True
            Me.btnGuardar.Enabled = True
        End If
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If Me.txtDescripcion.Text.Length = 0 Then
            MsgBox("Capture una Descripción", MsgBoxStyle.Information)
            Exit Sub
        End If

        If ProblemaOpcion = "N" Then
            Insertar(Me.txtDescripcion.Text.Trim, Me.cbxActivo.CheckState)
        ElseIf ProblemaOpcion = "M" Then
            Insertar(ProblemaClvProblema, Me.txtDescripcion.Text.Trim, Me.cbxActivo.CheckState)
        End If
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub
#End Region
End Class