<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMiMenu
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CMBNombreLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMiMenu))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.CatálogosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CatálogosDeClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientesToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CatalogoÁreaTécnicaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SectoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ÁreasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClasificaciónTécnicaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ServiciosAlClienteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DepartamentosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TrabajosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RelaciónDeTrabajosYRetiroDeAparatosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TecnicosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CatálogoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RelaciónDePuntosImporteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClasificaciónProblemasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TiposTécnicosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TipoInstalacionesAClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CatalogoMotivoPagoNoRealizadoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CatálogoDeGeneralesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TiposDeServicioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ServiciosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CableModemsYAparatosDigitalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ColoniasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TiposDeColoniasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CallesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CiudadesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PromocionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SucursalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BancosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UsuariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CajasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MotivosDeCancelaciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MotivosDeCancelaciónFacturasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MotivosDeReImpresiónFacturasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MotivosDeLlamadaAtenciónAClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AvisosToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DescuentosComboToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReferenciasBancariasToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.TipoServicioEstadoDeCuentaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TiposDeClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SubTiposDeClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CortesiasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CostoAparatosRentaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TablaDeProductividadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrioridadesDeQuejasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CompañíasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TarifasEconopackToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CatálogoDeVentasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TiposDePromotoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PromotoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TablasDeComisionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TablaDePuntosPorServicioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TablaDeEquivalenciaDePuntosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.JefeDeGrupoDeTvToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.JefeDeVentasDeTvToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VendedorDeTvToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.JefeDeVentasDeInternetToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RecuperadoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SeriesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RangosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NivelesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrecioDeComisionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GrupoDeVentasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EstablecerComisionesPorContratosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MetaPorVendedorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ComisiónDeNivelesPorServicioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PolizaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PolizaToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.GruposConceptoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConceptosIngresoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MedidoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ServiciosDeVentasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IndividualesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CarteraToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TelefoníaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NumérosDeTeléfonoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MarcacionesEspecialesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PaisesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CódigosMéxicoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TipoPaquetesAdicionalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PaquetesAdicionalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ServiciosDigitalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EquiposALaVentaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TarifasEspecialesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DesgloseDeIngresosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GrupoDeIngresosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresosDesglozadosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Catalogos33ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductosSatToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProcesosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProcesosDeServicioPremiumToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrimerPeriodoToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.SegundoPeriodoToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.TercerPeriodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DesconexionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ServicioBasicoYCanalesPremiumToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InternetToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProcesoDeCierreDeMesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrimerPeriodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OrdenesDeServicioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItemControlHoras = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuejasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuejasToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AgendaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AtenciónTelefónicaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AgendaToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProcesoDeDesconexiónTemporalPorContratoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProcesoDeReactivaciónDeContratoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContratoMaestroToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ActivaciónPaqueteDePruebaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DepuraciónDeÓrdenesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CambioDeServicioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MesajesInstantaneosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientesVariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClienteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PruebaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CorreoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ResetearAparatosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReseteoMasivoDeAparatosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AtenciónTelefónicaToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CambioDeClienteASoloInternetToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CambioDeClienteAClienteNormalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargosEspecialesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RecuperaciónDeCarteraToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CortesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReprocesamientoPorClienteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EnviarSMSMasivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RecontrataciónToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RegresaAparatosAlAlmacenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VisitasAlClienteDelVendedorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GeneraciónMasivaDeÓrdenesDeCambioDeAparatoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TelefoníaToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImpresiónDeEstadosDeCuentaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImpresiónDeEstadosDeCuentaPágina2NToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AuxToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EstadosDeCarteraToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientesToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportesVariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteDePaquetesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteClientesConComboToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HotelesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OrdenesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GerencialToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteDePermanenciaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteDeClientesSuspendidosQueYaPagaronToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CantidadDeClientesPorMezclaDeServiciosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteDeControlDeCambioDeCajaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteInstalacionesVsPromocionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AnálisisDePenetraciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CalcularComisionesPorVendedorToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ResumenDeVentasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ResumenVendedoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GráficasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PelículasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PPVToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ServiciosContratadosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CancelacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentasTotalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CancelacionDeVentasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ResumenVentasPorStatusToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ComicionesDeVendedoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ServiciosVendidosPorSemanaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReimpresionDeFoliosParaVendedoresToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusDeLosFoliosDeLosVendedoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Medidores2ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MedidoresToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AreaTécnicaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OrdenesDeServicioToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuejasToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.LlamadasTelefónicasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeActividadesDelTécnicoToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AgendaDeActividadesDelTécnicoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MaterialesUtilizadosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TiemposPorServicioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TiempoDeQuejasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteDeProductividadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeInstalacionesDeExtecionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RelaciónDeOrdenesEjecutadasYCapturadasDelTécnicoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteDeClientesPorTipoDeInstalaciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BitácoraDePruebasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BitácoraDeCorreosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientesConAdeudoDeMaterialToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientesVariosMezcladosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DesgloceDeMensualidadesAdelantadasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImporteDeMensualidadesAdelantadasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContratoForzosoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EstadoDeCuentaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CarteraEjecutivaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeClientesPorStausConAdeudoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PuntosDeAntigüedadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InterfazCablemodemsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InterfazDigitalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ValorDeCarteraToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RecontrataciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AtencionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeEquiposRecibidosPorCajerasosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteEjecutivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteDeRecuperaciónDeCajasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ResumenMensualDeRecuperaciónDeCajasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteInterfazDigitalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteDeRenovaciónDeAnualidadesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteClientesInternetToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GeneralesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GeneralesDelSistemaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InterfasCablemodemsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InterfasDecodificadoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GeneralesDeBancosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BancosToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.GeneralesProsaBancomerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GeneralesDeInterfacesDigitalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GeneralesDeInterfacesInternetToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GeneralesDeOXXOToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConfiguracionDelSistemaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GeneralesReportesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SoftvToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FacsoftvToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EncargadosDelSistemaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PreciosDeArticulosDeInstalaciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BitacoraDelSistemaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProcesamientoDeLlamadasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProcesamientoDeCDRToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InterfazDigitalesXcriptToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.MUESTRAIMAGENBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLidia = New sofTV.DataSetLidia()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.BackgroundWorker2 = New System.ComponentModel.BackgroundWorker()
        Me.CMBLabelSistema = New System.Windows.Forms.Label()
        Me.DataSetEdgarRev2 = New sofTV.DataSetEdgarRev2()
        Me.DataSetarnoldo = New sofTV.DataSetarnoldo()
        Me.DameClv_Session_ServiciosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameClv_Session_ServiciosTableAdapter = New sofTV.DataSetarnoldoTableAdapters.DameClv_Session_ServiciosTableAdapter()
        Me.Valida_periodo_reportesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Valida_periodo_reportesTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Valida_periodo_reportesTableAdapter()
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2()
        Me.Borrar_Tablas_Reporte_nuevoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Borrar_Tablas_Reporte_nuevoTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Borrar_Tablas_Reporte_nuevoTableAdapter()
        Me.Borra_Separacion_ClientesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Borra_Separacion_ClientesTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Borra_Separacion_ClientesTableAdapter()
        Me.Borra_temporales_trabajosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Borra_temporales_trabajosTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Borra_temporales_trabajosTableAdapter()
        Me.Procedimientosarnoldo4 = New sofTV.Procedimientosarnoldo4()
        Me.BackgroundWorker3 = New System.ComponentModel.BackgroundWorker()
        Me.MUESTRAIMAGENBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameTipoUsusarioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter5 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter6 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter7 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter8 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter9 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter10 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter11 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter12 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter13 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter14 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter15 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter16 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter17 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter18 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter19 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter20 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter21 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter22 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter23 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter24 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter25 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter26 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter27 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DameEspecifBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameEspecifTableAdapter = New sofTV.DataSetLidiaTableAdapters.DameEspecifTableAdapter()
        Me.MUESTRAIMAGENTableAdapter = New sofTV.DataSetLidiaTableAdapters.MUESTRAIMAGENTableAdapter()
        Me.DameTipoUsusarioBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameTipoUsusarioTableAdapter = New sofTV.DataSetLidiaTableAdapters.DameTipoUsusarioTableAdapter()
        Me.DamePermisosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DamePermisosTableAdapter = New sofTV.DataSetLidiaTableAdapters.DamePermisosTableAdapter()
        Me.ALTASMENUSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ALTASMENUSTableAdapter = New sofTV.DataSetLidiaTableAdapters.ALTASMENUSTableAdapter()
        Me.ALTASformsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ALTASformsTableAdapter = New sofTV.DataSetLidiaTableAdapters.ALTASformsTableAdapter()
        CMBNombreLabel = New System.Windows.Forms.Label()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAIMAGENBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameClv_Session_ServiciosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Valida_periodo_reportesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Borrar_Tablas_Reporte_nuevoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Borra_Separacion_ClientesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Borra_temporales_trabajosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAIMAGENBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameTipoUsusarioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameEspecifBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameTipoUsusarioBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DamePermisosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ALTASMENUSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ALTASformsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBNombreLabel
        '
        CMBNombreLabel.AutoSize = True
        CMBNombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBNombreLabel.ForeColor = System.Drawing.Color.Gray
        CMBNombreLabel.Location = New System.Drawing.Point(737, 685)
        CMBNombreLabel.Name = "CMBNombreLabel"
        CMBNombreLabel.Size = New System.Drawing.Size(75, 20)
        CMBNombreLabel.TabIndex = 7
        CMBNombreLabel.Text = "Ciudad :"
        CMBNombreLabel.Visible = False
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.Orange
        Me.MenuStrip1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CatálogosToolStripMenuItem, Me.ProcesosToolStripMenuItem, Me.ReportesToolStripMenuItem, Me.GeneralesToolStripMenuItem, Me.SalirToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1028, 26)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.TabStop = True
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'CatálogosToolStripMenuItem
        '
        Me.CatálogosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CatálogosDeClientesToolStripMenuItem, Me.CatalogoÁreaTécnicaToolStripMenuItem, Me.CatálogoDeGeneralesToolStripMenuItem, Me.CatálogoDeVentasToolStripMenuItem, Me.PolizaToolStripMenuItem, Me.MedidoresToolStripMenuItem, Me.TelefoníaToolStripMenuItem, Me.DesgloseDeIngresosToolStripMenuItem, Me.Catalogos33ToolStripMenuItem})
        Me.CatálogosToolStripMenuItem.Name = "CatálogosToolStripMenuItem"
        Me.CatálogosToolStripMenuItem.Size = New System.Drawing.Size(94, 22)
        Me.CatálogosToolStripMenuItem.Text = "&Catálogos"
        '
        'CatálogosDeClientesToolStripMenuItem
        '
        Me.CatálogosDeClientesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClientesToolStripMenuItem1})
        Me.CatálogosDeClientesToolStripMenuItem.Name = "CatálogosDeClientesToolStripMenuItem"
        Me.CatálogosDeClientesToolStripMenuItem.Size = New System.Drawing.Size(244, 22)
        Me.CatálogosDeClientesToolStripMenuItem.Text = "Clientes"
        '
        'ClientesToolStripMenuItem1
        '
        Me.ClientesToolStripMenuItem1.Name = "ClientesToolStripMenuItem1"
        Me.ClientesToolStripMenuItem1.Size = New System.Drawing.Size(137, 22)
        Me.ClientesToolStripMenuItem1.Text = "Clientes"
        '
        'CatalogoÁreaTécnicaToolStripMenuItem
        '
        Me.CatalogoÁreaTécnicaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SectoresToolStripMenuItem, Me.ÁreasToolStripMenuItem, Me.ClasificaciónTécnicaToolStripMenuItem, Me.ServiciosAlClienteToolStripMenuItem, Me.TecnicosToolStripMenuItem, Me.CatálogoToolStripMenuItem, Me.RelaciónDePuntosImporteToolStripMenuItem, Me.ClasificaciónProblemasToolStripMenuItem, Me.TiposTécnicosToolStripMenuItem, Me.TipoInstalacionesAClientesToolStripMenuItem, Me.CatalogoMotivoPagoNoRealizadoToolStripMenuItem})
        Me.CatalogoÁreaTécnicaToolStripMenuItem.Name = "CatalogoÁreaTécnicaToolStripMenuItem"
        Me.CatalogoÁreaTécnicaToolStripMenuItem.Size = New System.Drawing.Size(244, 22)
        Me.CatalogoÁreaTécnicaToolStripMenuItem.Text = "Área Técnica"
        '
        'SectoresToolStripMenuItem
        '
        Me.SectoresToolStripMenuItem.Name = "SectoresToolStripMenuItem"
        Me.SectoresToolStripMenuItem.Size = New System.Drawing.Size(346, 22)
        Me.SectoresToolStripMenuItem.Text = "Nodos"
        '
        'ÁreasToolStripMenuItem
        '
        Me.ÁreasToolStripMenuItem.Name = "ÁreasToolStripMenuItem"
        Me.ÁreasToolStripMenuItem.Size = New System.Drawing.Size(346, 22)
        Me.ÁreasToolStripMenuItem.Text = "Taps"
        '
        'ClasificaciónTécnicaToolStripMenuItem
        '
        Me.ClasificaciónTécnicaToolStripMenuItem.Name = "ClasificaciónTécnicaToolStripMenuItem"
        Me.ClasificaciónTécnicaToolStripMenuItem.Size = New System.Drawing.Size(346, 22)
        Me.ClasificaciónTécnicaToolStripMenuItem.Text = "Clasificación Técnica"
        '
        'ServiciosAlClienteToolStripMenuItem
        '
        Me.ServiciosAlClienteToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DepartamentosToolStripMenuItem, Me.TrabajosToolStripMenuItem, Me.RelaciónDeTrabajosYRetiroDeAparatosToolStripMenuItem})
        Me.ServiciosAlClienteToolStripMenuItem.Name = "ServiciosAlClienteToolStripMenuItem"
        Me.ServiciosAlClienteToolStripMenuItem.Size = New System.Drawing.Size(346, 22)
        Me.ServiciosAlClienteToolStripMenuItem.Text = "Servicios al Cliente"
        '
        'DepartamentosToolStripMenuItem
        '
        Me.DepartamentosToolStripMenuItem.Name = "DepartamentosToolStripMenuItem"
        Me.DepartamentosToolStripMenuItem.Size = New System.Drawing.Size(389, 22)
        Me.DepartamentosToolStripMenuItem.Text = "Departamentos"
        '
        'TrabajosToolStripMenuItem
        '
        Me.TrabajosToolStripMenuItem.Name = "TrabajosToolStripMenuItem"
        Me.TrabajosToolStripMenuItem.Size = New System.Drawing.Size(389, 22)
        Me.TrabajosToolStripMenuItem.Text = "Trabajos"
        '
        'RelaciónDeTrabajosYRetiroDeAparatosToolStripMenuItem
        '
        Me.RelaciónDeTrabajosYRetiroDeAparatosToolStripMenuItem.Name = "RelaciónDeTrabajosYRetiroDeAparatosToolStripMenuItem"
        Me.RelaciónDeTrabajosYRetiroDeAparatosToolStripMenuItem.Size = New System.Drawing.Size(389, 22)
        Me.RelaciónDeTrabajosYRetiroDeAparatosToolStripMenuItem.Text = "Relación de Trabajos y Retiro de Aparatos"
        '
        'TecnicosToolStripMenuItem
        '
        Me.TecnicosToolStripMenuItem.Name = "TecnicosToolStripMenuItem"
        Me.TecnicosToolStripMenuItem.Size = New System.Drawing.Size(346, 22)
        Me.TecnicosToolStripMenuItem.Text = "Tecnicos"
        Me.TecnicosToolStripMenuItem.Visible = False
        '
        'CatálogoToolStripMenuItem
        '
        Me.CatálogoToolStripMenuItem.Name = "CatálogoToolStripMenuItem"
        Me.CatálogoToolStripMenuItem.Size = New System.Drawing.Size(346, 22)
        Me.CatálogoToolStripMenuItem.Text = "Catálogo Postes"
        '
        'RelaciónDePuntosImporteToolStripMenuItem
        '
        Me.RelaciónDePuntosImporteToolStripMenuItem.Name = "RelaciónDePuntosImporteToolStripMenuItem"
        Me.RelaciónDePuntosImporteToolStripMenuItem.Size = New System.Drawing.Size(346, 22)
        Me.RelaciónDePuntosImporteToolStripMenuItem.Text = "Incentivo a Técnicos"
        '
        'ClasificaciónProblemasToolStripMenuItem
        '
        Me.ClasificaciónProblemasToolStripMenuItem.Name = "ClasificaciónProblemasToolStripMenuItem"
        Me.ClasificaciónProblemasToolStripMenuItem.Size = New System.Drawing.Size(346, 22)
        Me.ClasificaciónProblemasToolStripMenuItem.Text = "Clasificación Problemas"
        '
        'TiposTécnicosToolStripMenuItem
        '
        Me.TiposTécnicosToolStripMenuItem.Name = "TiposTécnicosToolStripMenuItem"
        Me.TiposTécnicosToolStripMenuItem.Size = New System.Drawing.Size(346, 22)
        Me.TiposTécnicosToolStripMenuItem.Text = "Tipos Técnicos"
        '
        'TipoInstalacionesAClientesToolStripMenuItem
        '
        Me.TipoInstalacionesAClientesToolStripMenuItem.Name = "TipoInstalacionesAClientesToolStripMenuItem"
        Me.TipoInstalacionesAClientesToolStripMenuItem.Size = New System.Drawing.Size(346, 22)
        Me.TipoInstalacionesAClientesToolStripMenuItem.Text = "Tipo Instalaciones a Clientes"
        '
        'CatalogoMotivoPagoNoRealizadoToolStripMenuItem
        '
        Me.CatalogoMotivoPagoNoRealizadoToolStripMenuItem.Name = "CatalogoMotivoPagoNoRealizadoToolStripMenuItem"
        Me.CatalogoMotivoPagoNoRealizadoToolStripMenuItem.Size = New System.Drawing.Size(346, 22)
        Me.CatalogoMotivoPagoNoRealizadoToolStripMenuItem.Text = "Catalogo Motivo, Pago No Realizado"
        '
        'CatálogoDeGeneralesToolStripMenuItem
        '
        Me.CatálogoDeGeneralesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TiposDeServicioToolStripMenuItem, Me.ServiciosToolStripMenuItem, Me.CableModemsYAparatosDigitalesToolStripMenuItem, Me.ColoniasToolStripMenuItem, Me.TiposDeColoniasToolStripMenuItem, Me.CallesToolStripMenuItem, Me.CiudadesToolStripMenuItem, Me.PromocionesToolStripMenuItem, Me.SucursalesToolStripMenuItem, Me.BancosToolStripMenuItem, Me.UsuariosToolStripMenuItem, Me.CajasToolStripMenuItem, Me.MotivosDeCancelaciónToolStripMenuItem, Me.MotivosDeCancelaciónFacturasToolStripMenuItem, Me.MotivosDeReImpresiónFacturasToolStripMenuItem, Me.MotivosDeLlamadaAtenciónAClientesToolStripMenuItem, Me.AvisosToolStripMenuItem1, Me.DescuentosComboToolStripMenuItem, Me.ReferenciasBancariasToolStripMenuItem1, Me.TipoServicioEstadoDeCuentaToolStripMenuItem, Me.TiposDeClientesToolStripMenuItem, Me.SubTiposDeClientesToolStripMenuItem, Me.CortesiasToolStripMenuItem, Me.CostoAparatosRentaToolStripMenuItem, Me.TablaDeProductividadToolStripMenuItem, Me.PrioridadesDeQuejasToolStripMenuItem, Me.CompañíasToolStripMenuItem, Me.TarifasEconopackToolStripMenuItem})
        Me.CatálogoDeGeneralesToolStripMenuItem.Name = "CatálogoDeGeneralesToolStripMenuItem"
        Me.CatálogoDeGeneralesToolStripMenuItem.Size = New System.Drawing.Size(244, 22)
        Me.CatálogoDeGeneralesToolStripMenuItem.Text = "Catálogo de Generales"
        '
        'TiposDeServicioToolStripMenuItem
        '
        Me.TiposDeServicioToolStripMenuItem.Name = "TiposDeServicioToolStripMenuItem"
        Me.TiposDeServicioToolStripMenuItem.Size = New System.Drawing.Size(371, 22)
        Me.TiposDeServicioToolStripMenuItem.Text = "Tipos de Servicio"
        '
        'ServiciosToolStripMenuItem
        '
        Me.ServiciosToolStripMenuItem.Name = "ServiciosToolStripMenuItem"
        Me.ServiciosToolStripMenuItem.Size = New System.Drawing.Size(371, 22)
        Me.ServiciosToolStripMenuItem.Text = "Servicios"
        '
        'CableModemsYAparatosDigitalesToolStripMenuItem
        '
        Me.CableModemsYAparatosDigitalesToolStripMenuItem.Name = "CableModemsYAparatosDigitalesToolStripMenuItem"
        Me.CableModemsYAparatosDigitalesToolStripMenuItem.Size = New System.Drawing.Size(371, 22)
        Me.CableModemsYAparatosDigitalesToolStripMenuItem.Text = "CableModems y Aparatos Digitales"
        '
        'ColoniasToolStripMenuItem
        '
        Me.ColoniasToolStripMenuItem.Name = "ColoniasToolStripMenuItem"
        Me.ColoniasToolStripMenuItem.Size = New System.Drawing.Size(371, 22)
        Me.ColoniasToolStripMenuItem.Text = "Colonias"
        '
        'TiposDeColoniasToolStripMenuItem
        '
        Me.TiposDeColoniasToolStripMenuItem.Name = "TiposDeColoniasToolStripMenuItem"
        Me.TiposDeColoniasToolStripMenuItem.Size = New System.Drawing.Size(371, 22)
        Me.TiposDeColoniasToolStripMenuItem.Text = "Tipos de Colonias"
        '
        'CallesToolStripMenuItem
        '
        Me.CallesToolStripMenuItem.Name = "CallesToolStripMenuItem"
        Me.CallesToolStripMenuItem.Size = New System.Drawing.Size(371, 22)
        Me.CallesToolStripMenuItem.Text = "Calles"
        '
        'CiudadesToolStripMenuItem
        '
        Me.CiudadesToolStripMenuItem.Name = "CiudadesToolStripMenuItem"
        Me.CiudadesToolStripMenuItem.Size = New System.Drawing.Size(371, 22)
        Me.CiudadesToolStripMenuItem.Text = "Ciudades"
        '
        'PromocionesToolStripMenuItem
        '
        Me.PromocionesToolStripMenuItem.Name = "PromocionesToolStripMenuItem"
        Me.PromocionesToolStripMenuItem.Size = New System.Drawing.Size(371, 22)
        Me.PromocionesToolStripMenuItem.Text = "Promociones"
        Me.PromocionesToolStripMenuItem.Visible = False
        '
        'SucursalesToolStripMenuItem
        '
        Me.SucursalesToolStripMenuItem.Name = "SucursalesToolStripMenuItem"
        Me.SucursalesToolStripMenuItem.Size = New System.Drawing.Size(371, 22)
        Me.SucursalesToolStripMenuItem.Text = "Sucursales"
        '
        'BancosToolStripMenuItem
        '
        Me.BancosToolStripMenuItem.Name = "BancosToolStripMenuItem"
        Me.BancosToolStripMenuItem.Size = New System.Drawing.Size(371, 22)
        Me.BancosToolStripMenuItem.Text = "Bancos"
        '
        'UsuariosToolStripMenuItem
        '
        Me.UsuariosToolStripMenuItem.Name = "UsuariosToolStripMenuItem"
        Me.UsuariosToolStripMenuItem.Size = New System.Drawing.Size(371, 22)
        Me.UsuariosToolStripMenuItem.Text = "Usuarios"
        '
        'CajasToolStripMenuItem
        '
        Me.CajasToolStripMenuItem.Name = "CajasToolStripMenuItem"
        Me.CajasToolStripMenuItem.Size = New System.Drawing.Size(371, 22)
        Me.CajasToolStripMenuItem.Text = "Cajas"
        '
        'MotivosDeCancelaciónToolStripMenuItem
        '
        Me.MotivosDeCancelaciónToolStripMenuItem.Name = "MotivosDeCancelaciónToolStripMenuItem"
        Me.MotivosDeCancelaciónToolStripMenuItem.Size = New System.Drawing.Size(371, 22)
        Me.MotivosDeCancelaciónToolStripMenuItem.Text = "Motivos de Cancelación"
        '
        'MotivosDeCancelaciónFacturasToolStripMenuItem
        '
        Me.MotivosDeCancelaciónFacturasToolStripMenuItem.Name = "MotivosDeCancelaciónFacturasToolStripMenuItem"
        Me.MotivosDeCancelaciónFacturasToolStripMenuItem.Size = New System.Drawing.Size(371, 22)
        Me.MotivosDeCancelaciónFacturasToolStripMenuItem.Text = "Motivos de Cancelación Facturas"
        '
        'MotivosDeReImpresiónFacturasToolStripMenuItem
        '
        Me.MotivosDeReImpresiónFacturasToolStripMenuItem.Name = "MotivosDeReImpresiónFacturasToolStripMenuItem"
        Me.MotivosDeReImpresiónFacturasToolStripMenuItem.Size = New System.Drawing.Size(371, 22)
        Me.MotivosDeReImpresiónFacturasToolStripMenuItem.Text = "Motivos de ReImpresión Facturas"
        '
        'MotivosDeLlamadaAtenciónAClientesToolStripMenuItem
        '
        Me.MotivosDeLlamadaAtenciónAClientesToolStripMenuItem.Name = "MotivosDeLlamadaAtenciónAClientesToolStripMenuItem"
        Me.MotivosDeLlamadaAtenciónAClientesToolStripMenuItem.Size = New System.Drawing.Size(371, 22)
        Me.MotivosDeLlamadaAtenciónAClientesToolStripMenuItem.Text = "Motivos de Llamada Atención a Clientes"
        '
        'AvisosToolStripMenuItem1
        '
        Me.AvisosToolStripMenuItem1.Name = "AvisosToolStripMenuItem1"
        Me.AvisosToolStripMenuItem1.Size = New System.Drawing.Size(371, 22)
        Me.AvisosToolStripMenuItem1.Text = "Avisos"
        '
        'DescuentosComboToolStripMenuItem
        '
        Me.DescuentosComboToolStripMenuItem.Name = "DescuentosComboToolStripMenuItem"
        Me.DescuentosComboToolStripMenuItem.Size = New System.Drawing.Size(371, 22)
        Me.DescuentosComboToolStripMenuItem.Text = "Descuentos Combo"
        '
        'ReferenciasBancariasToolStripMenuItem1
        '
        Me.ReferenciasBancariasToolStripMenuItem1.Name = "ReferenciasBancariasToolStripMenuItem1"
        Me.ReferenciasBancariasToolStripMenuItem1.Size = New System.Drawing.Size(371, 22)
        Me.ReferenciasBancariasToolStripMenuItem1.Text = "Referencias Bancarias"
        Me.ReferenciasBancariasToolStripMenuItem1.Visible = False
        '
        'TipoServicioEstadoDeCuentaToolStripMenuItem
        '
        Me.TipoServicioEstadoDeCuentaToolStripMenuItem.Name = "TipoServicioEstadoDeCuentaToolStripMenuItem"
        Me.TipoServicioEstadoDeCuentaToolStripMenuItem.Size = New System.Drawing.Size(371, 22)
        Me.TipoServicioEstadoDeCuentaToolStripMenuItem.Text = "Paquete Servicio (Estado de Cuenta)"
        '
        'TiposDeClientesToolStripMenuItem
        '
        Me.TiposDeClientesToolStripMenuItem.Name = "TiposDeClientesToolStripMenuItem"
        Me.TiposDeClientesToolStripMenuItem.Size = New System.Drawing.Size(371, 22)
        Me.TiposDeClientesToolStripMenuItem.Text = "Tipos de Clientes"
        '
        'SubTiposDeClientesToolStripMenuItem
        '
        Me.SubTiposDeClientesToolStripMenuItem.Name = "SubTiposDeClientesToolStripMenuItem"
        Me.SubTiposDeClientesToolStripMenuItem.Size = New System.Drawing.Size(371, 22)
        Me.SubTiposDeClientesToolStripMenuItem.Text = "SubTipos de Clientes"
        '
        'CortesiasToolStripMenuItem
        '
        Me.CortesiasToolStripMenuItem.Name = "CortesiasToolStripMenuItem"
        Me.CortesiasToolStripMenuItem.Size = New System.Drawing.Size(371, 22)
        Me.CortesiasToolStripMenuItem.Text = "Cortesias"
        '
        'CostoAparatosRentaToolStripMenuItem
        '
        Me.CostoAparatosRentaToolStripMenuItem.Name = "CostoAparatosRentaToolStripMenuItem"
        Me.CostoAparatosRentaToolStripMenuItem.Size = New System.Drawing.Size(371, 22)
        Me.CostoAparatosRentaToolStripMenuItem.Text = "Costo Aparatos (Renta)"
        '
        'TablaDeProductividadToolStripMenuItem
        '
        Me.TablaDeProductividadToolStripMenuItem.Name = "TablaDeProductividadToolStripMenuItem"
        Me.TablaDeProductividadToolStripMenuItem.Size = New System.Drawing.Size(371, 22)
        Me.TablaDeProductividadToolStripMenuItem.Text = "Tabla De Productividad"
        Me.TablaDeProductividadToolStripMenuItem.Visible = False
        '
        'PrioridadesDeQuejasToolStripMenuItem
        '
        Me.PrioridadesDeQuejasToolStripMenuItem.Name = "PrioridadesDeQuejasToolStripMenuItem"
        Me.PrioridadesDeQuejasToolStripMenuItem.Size = New System.Drawing.Size(371, 22)
        Me.PrioridadesDeQuejasToolStripMenuItem.Text = "Prioridades de Quejas"
        '
        'CompañíasToolStripMenuItem
        '
        Me.CompañíasToolStripMenuItem.Name = "CompañíasToolStripMenuItem"
        Me.CompañíasToolStripMenuItem.Size = New System.Drawing.Size(371, 22)
        Me.CompañíasToolStripMenuItem.Text = "Compañías"
        '
        'TarifasEconopackToolStripMenuItem
        '
        Me.TarifasEconopackToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TarifasEconopackToolStripMenuItem.Name = "TarifasEconopackToolStripMenuItem"
        Me.TarifasEconopackToolStripMenuItem.Size = New System.Drawing.Size(371, 22)
        Me.TarifasEconopackToolStripMenuItem.Text = "Indicadores Econopack"
        '
        'CatálogoDeVentasToolStripMenuItem
        '
        Me.CatálogoDeVentasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TiposDePromotoresToolStripMenuItem, Me.PromotoresToolStripMenuItem, Me.TablasDeComisionesToolStripMenuItem, Me.SeriesToolStripMenuItem, Me.RangosToolStripMenuItem, Me.NivelesToolStripMenuItem, Me.PrecioDeComisionesToolStripMenuItem, Me.GrupoDeVentasToolStripMenuItem, Me.EstablecerComisionesPorContratosToolStripMenuItem, Me.MetaPorVendedorToolStripMenuItem, Me.ComisiónDeNivelesPorServicioToolStripMenuItem})
        Me.CatálogoDeVentasToolStripMenuItem.Name = "CatálogoDeVentasToolStripMenuItem"
        Me.CatálogoDeVentasToolStripMenuItem.Size = New System.Drawing.Size(244, 22)
        Me.CatálogoDeVentasToolStripMenuItem.Text = "Ventas"
        '
        'TiposDePromotoresToolStripMenuItem
        '
        Me.TiposDePromotoresToolStripMenuItem.Enabled = False
        Me.TiposDePromotoresToolStripMenuItem.Name = "TiposDePromotoresToolStripMenuItem"
        Me.TiposDePromotoresToolStripMenuItem.Size = New System.Drawing.Size(351, 22)
        Me.TiposDePromotoresToolStripMenuItem.Text = "Tipos de Vendedores"
        Me.TiposDePromotoresToolStripMenuItem.Visible = False
        '
        'PromotoresToolStripMenuItem
        '
        Me.PromotoresToolStripMenuItem.Name = "PromotoresToolStripMenuItem"
        Me.PromotoresToolStripMenuItem.Size = New System.Drawing.Size(351, 22)
        Me.PromotoresToolStripMenuItem.Text = "Vendedores"
        '
        'TablasDeComisionesToolStripMenuItem
        '
        Me.TablasDeComisionesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TablaDePuntosPorServicioToolStripMenuItem, Me.TablaDeEquivalenciaDePuntosToolStripMenuItem, Me.JefeDeGrupoDeTvToolStripMenuItem, Me.JefeDeVentasDeTvToolStripMenuItem, Me.VendedorDeTvToolStripMenuItem, Me.JefeDeVentasDeInternetToolStripMenuItem1, Me.RecuperadoresToolStripMenuItem})
        Me.TablasDeComisionesToolStripMenuItem.Enabled = False
        Me.TablasDeComisionesToolStripMenuItem.Name = "TablasDeComisionesToolStripMenuItem"
        Me.TablasDeComisionesToolStripMenuItem.Size = New System.Drawing.Size(351, 22)
        Me.TablasDeComisionesToolStripMenuItem.Text = "Tablas de Comisiones"
        Me.TablasDeComisionesToolStripMenuItem.Visible = False
        '
        'TablaDePuntosPorServicioToolStripMenuItem
        '
        Me.TablaDePuntosPorServicioToolStripMenuItem.Name = "TablaDePuntosPorServicioToolStripMenuItem"
        Me.TablaDePuntosPorServicioToolStripMenuItem.Size = New System.Drawing.Size(315, 22)
        Me.TablaDePuntosPorServicioToolStripMenuItem.Text = "Tabla de Puntos por Servicio"
        '
        'TablaDeEquivalenciaDePuntosToolStripMenuItem
        '
        Me.TablaDeEquivalenciaDePuntosToolStripMenuItem.Name = "TablaDeEquivalenciaDePuntosToolStripMenuItem"
        Me.TablaDeEquivalenciaDePuntosToolStripMenuItem.Size = New System.Drawing.Size(315, 22)
        Me.TablaDeEquivalenciaDePuntosToolStripMenuItem.Text = "Tabla de Equivalencia de Puntos"
        '
        'JefeDeGrupoDeTvToolStripMenuItem
        '
        Me.JefeDeGrupoDeTvToolStripMenuItem.Name = "JefeDeGrupoDeTvToolStripMenuItem"
        Me.JefeDeGrupoDeTvToolStripMenuItem.Size = New System.Drawing.Size(315, 22)
        Me.JefeDeGrupoDeTvToolStripMenuItem.Text = "Jefe de Grupo de Tv."
        '
        'JefeDeVentasDeTvToolStripMenuItem
        '
        Me.JefeDeVentasDeTvToolStripMenuItem.Name = "JefeDeVentasDeTvToolStripMenuItem"
        Me.JefeDeVentasDeTvToolStripMenuItem.Size = New System.Drawing.Size(315, 22)
        Me.JefeDeVentasDeTvToolStripMenuItem.Text = "Jefe de Ventas de Tv."
        '
        'VendedorDeTvToolStripMenuItem
        '
        Me.VendedorDeTvToolStripMenuItem.Name = "VendedorDeTvToolStripMenuItem"
        Me.VendedorDeTvToolStripMenuItem.Size = New System.Drawing.Size(315, 22)
        Me.VendedorDeTvToolStripMenuItem.Text = "Vendedor de Tv."
        '
        'JefeDeVentasDeInternetToolStripMenuItem1
        '
        Me.JefeDeVentasDeInternetToolStripMenuItem1.Name = "JefeDeVentasDeInternetToolStripMenuItem1"
        Me.JefeDeVentasDeInternetToolStripMenuItem1.Size = New System.Drawing.Size(315, 22)
        Me.JefeDeVentasDeInternetToolStripMenuItem1.Text = "Jefe de Ventas de Internet"
        '
        'RecuperadoresToolStripMenuItem
        '
        Me.RecuperadoresToolStripMenuItem.Name = "RecuperadoresToolStripMenuItem"
        Me.RecuperadoresToolStripMenuItem.Size = New System.Drawing.Size(315, 22)
        Me.RecuperadoresToolStripMenuItem.Text = "Recuperadores"
        '
        'SeriesToolStripMenuItem
        '
        Me.SeriesToolStripMenuItem.Name = "SeriesToolStripMenuItem"
        Me.SeriesToolStripMenuItem.Size = New System.Drawing.Size(351, 22)
        Me.SeriesToolStripMenuItem.Text = "Series"
        '
        'RangosToolStripMenuItem
        '
        Me.RangosToolStripMenuItem.Name = "RangosToolStripMenuItem"
        Me.RangosToolStripMenuItem.Size = New System.Drawing.Size(351, 22)
        Me.RangosToolStripMenuItem.Text = "Rangos"
        '
        'NivelesToolStripMenuItem
        '
        Me.NivelesToolStripMenuItem.Name = "NivelesToolStripMenuItem"
        Me.NivelesToolStripMenuItem.Size = New System.Drawing.Size(351, 22)
        Me.NivelesToolStripMenuItem.Text = "Niveles"
        '
        'PrecioDeComisionesToolStripMenuItem
        '
        Me.PrecioDeComisionesToolStripMenuItem.Name = "PrecioDeComisionesToolStripMenuItem"
        Me.PrecioDeComisionesToolStripMenuItem.Size = New System.Drawing.Size(351, 22)
        Me.PrecioDeComisionesToolStripMenuItem.Text = "Establecer Comisiones por Servicio"
        '
        'GrupoDeVentasToolStripMenuItem
        '
        Me.GrupoDeVentasToolStripMenuItem.Name = "GrupoDeVentasToolStripMenuItem"
        Me.GrupoDeVentasToolStripMenuItem.Size = New System.Drawing.Size(351, 22)
        Me.GrupoDeVentasToolStripMenuItem.Text = "Grupo de Ventas"
        '
        'EstablecerComisionesPorContratosToolStripMenuItem
        '
        Me.EstablecerComisionesPorContratosToolStripMenuItem.Name = "EstablecerComisionesPorContratosToolStripMenuItem"
        Me.EstablecerComisionesPorContratosToolStripMenuItem.Size = New System.Drawing.Size(351, 22)
        Me.EstablecerComisionesPorContratosToolStripMenuItem.Text = "Establecer Comisiones Por Contratos"
        '
        'MetaPorVendedorToolStripMenuItem
        '
        Me.MetaPorVendedorToolStripMenuItem.Name = "MetaPorVendedorToolStripMenuItem"
        Me.MetaPorVendedorToolStripMenuItem.Size = New System.Drawing.Size(351, 22)
        Me.MetaPorVendedorToolStripMenuItem.Text = "Meta Por Vendedor"
        '
        'ComisiónDeNivelesPorServicioToolStripMenuItem
        '
        Me.ComisiónDeNivelesPorServicioToolStripMenuItem.Name = "ComisiónDeNivelesPorServicioToolStripMenuItem"
        Me.ComisiónDeNivelesPorServicioToolStripMenuItem.Size = New System.Drawing.Size(351, 22)
        Me.ComisiónDeNivelesPorServicioToolStripMenuItem.Text = "Comisión de Niveles por Servicio"
        '
        'PolizaToolStripMenuItem
        '
        Me.PolizaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PolizaToolStripMenuItem1, Me.GruposConceptoToolStripMenuItem, Me.ConceptosIngresoToolStripMenuItem})
        Me.PolizaToolStripMenuItem.Name = "PolizaToolStripMenuItem"
        Me.PolizaToolStripMenuItem.Size = New System.Drawing.Size(244, 22)
        Me.PolizaToolStripMenuItem.Text = "Poliza"
        '
        'PolizaToolStripMenuItem1
        '
        Me.PolizaToolStripMenuItem1.Name = "PolizaToolStripMenuItem1"
        Me.PolizaToolStripMenuItem1.Size = New System.Drawing.Size(215, 22)
        Me.PolizaToolStripMenuItem1.Text = "Poliza"
        Me.PolizaToolStripMenuItem1.Visible = False
        '
        'GruposConceptoToolStripMenuItem
        '
        Me.GruposConceptoToolStripMenuItem.Name = "GruposConceptoToolStripMenuItem"
        Me.GruposConceptoToolStripMenuItem.Size = New System.Drawing.Size(215, 22)
        Me.GruposConceptoToolStripMenuItem.Text = "Grupos Concepto"
        '
        'ConceptosIngresoToolStripMenuItem
        '
        Me.ConceptosIngresoToolStripMenuItem.Name = "ConceptosIngresoToolStripMenuItem"
        Me.ConceptosIngresoToolStripMenuItem.Size = New System.Drawing.Size(215, 22)
        Me.ConceptosIngresoToolStripMenuItem.Text = "Conceptos Ingreso"
        '
        'MedidoresToolStripMenuItem
        '
        Me.MedidoresToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ServiciosDeVentasToolStripMenuItem, Me.IndividualesToolStripMenuItem, Me.IngresosToolStripMenuItem, Me.CarteraToolStripMenuItem})
        Me.MedidoresToolStripMenuItem.Name = "MedidoresToolStripMenuItem"
        Me.MedidoresToolStripMenuItem.Size = New System.Drawing.Size(244, 22)
        Me.MedidoresToolStripMenuItem.Text = "Medidores"
        '
        'ServiciosDeVentasToolStripMenuItem
        '
        Me.ServiciosDeVentasToolStripMenuItem.Name = "ServiciosDeVentasToolStripMenuItem"
        Me.ServiciosDeVentasToolStripMenuItem.Size = New System.Drawing.Size(169, 22)
        Me.ServiciosDeVentasToolStripMenuItem.Text = "Globales"
        '
        'IndividualesToolStripMenuItem
        '
        Me.IndividualesToolStripMenuItem.Name = "IndividualesToolStripMenuItem"
        Me.IndividualesToolStripMenuItem.Size = New System.Drawing.Size(169, 22)
        Me.IndividualesToolStripMenuItem.Text = "Individuales"
        '
        'IngresosToolStripMenuItem
        '
        Me.IngresosToolStripMenuItem.Name = "IngresosToolStripMenuItem"
        Me.IngresosToolStripMenuItem.Size = New System.Drawing.Size(169, 22)
        Me.IngresosToolStripMenuItem.Text = "Ingresos"
        '
        'CarteraToolStripMenuItem
        '
        Me.CarteraToolStripMenuItem.Name = "CarteraToolStripMenuItem"
        Me.CarteraToolStripMenuItem.Size = New System.Drawing.Size(169, 22)
        Me.CarteraToolStripMenuItem.Text = "Cartera"
        '
        'TelefoníaToolStripMenuItem
        '
        Me.TelefoníaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NumérosDeTeléfonoToolStripMenuItem, Me.MarcacionesEspecialesToolStripMenuItem, Me.PaisesToolStripMenuItem, Me.CódigosMéxicoToolStripMenuItem, Me.TipoPaquetesAdicionalesToolStripMenuItem, Me.PaquetesAdicionalesToolStripMenuItem, Me.ServiciosDigitalesToolStripMenuItem, Me.EquiposALaVentaToolStripMenuItem, Me.TarifasEspecialesToolStripMenuItem})
        Me.TelefoníaToolStripMenuItem.Name = "TelefoníaToolStripMenuItem"
        Me.TelefoníaToolStripMenuItem.Size = New System.Drawing.Size(244, 22)
        Me.TelefoníaToolStripMenuItem.Text = "Telefonía"
        '
        'NumérosDeTeléfonoToolStripMenuItem
        '
        Me.NumérosDeTeléfonoToolStripMenuItem.Name = "NumérosDeTeléfonoToolStripMenuItem"
        Me.NumérosDeTeléfonoToolStripMenuItem.Size = New System.Drawing.Size(272, 22)
        Me.NumérosDeTeléfonoToolStripMenuItem.Text = "Numeración Telefónica"
        '
        'MarcacionesEspecialesToolStripMenuItem
        '
        Me.MarcacionesEspecialesToolStripMenuItem.Name = "MarcacionesEspecialesToolStripMenuItem"
        Me.MarcacionesEspecialesToolStripMenuItem.Size = New System.Drawing.Size(272, 22)
        Me.MarcacionesEspecialesToolStripMenuItem.Text = "Marcaciones Especiales"
        '
        'PaisesToolStripMenuItem
        '
        Me.PaisesToolStripMenuItem.Name = "PaisesToolStripMenuItem"
        Me.PaisesToolStripMenuItem.Size = New System.Drawing.Size(272, 22)
        Me.PaisesToolStripMenuItem.Text = "Código de Paises"
        '
        'CódigosMéxicoToolStripMenuItem
        '
        Me.CódigosMéxicoToolStripMenuItem.Name = "CódigosMéxicoToolStripMenuItem"
        Me.CódigosMéxicoToolStripMenuItem.Size = New System.Drawing.Size(272, 22)
        Me.CódigosMéxicoToolStripMenuItem.Text = "Códigos México"
        '
        'TipoPaquetesAdicionalesToolStripMenuItem
        '
        Me.TipoPaquetesAdicionalesToolStripMenuItem.Name = "TipoPaquetesAdicionalesToolStripMenuItem"
        Me.TipoPaquetesAdicionalesToolStripMenuItem.Size = New System.Drawing.Size(272, 22)
        Me.TipoPaquetesAdicionalesToolStripMenuItem.Text = "Tarifas de Larga Distancia"
        '
        'PaquetesAdicionalesToolStripMenuItem
        '
        Me.PaquetesAdicionalesToolStripMenuItem.Name = "PaquetesAdicionalesToolStripMenuItem"
        Me.PaquetesAdicionalesToolStripMenuItem.Size = New System.Drawing.Size(272, 22)
        Me.PaquetesAdicionalesToolStripMenuItem.Text = "Paquetes Adicionales"
        '
        'ServiciosDigitalesToolStripMenuItem
        '
        Me.ServiciosDigitalesToolStripMenuItem.Name = "ServiciosDigitalesToolStripMenuItem"
        Me.ServiciosDigitalesToolStripMenuItem.Size = New System.Drawing.Size(272, 22)
        Me.ServiciosDigitalesToolStripMenuItem.Text = "Servicios Digitales"
        '
        'EquiposALaVentaToolStripMenuItem
        '
        Me.EquiposALaVentaToolStripMenuItem.Name = "EquiposALaVentaToolStripMenuItem"
        Me.EquiposALaVentaToolStripMenuItem.Size = New System.Drawing.Size(272, 22)
        Me.EquiposALaVentaToolStripMenuItem.Text = "Equipos a la Venta"
        '
        'TarifasEspecialesToolStripMenuItem
        '
        Me.TarifasEspecialesToolStripMenuItem.Name = "TarifasEspecialesToolStripMenuItem"
        Me.TarifasEspecialesToolStripMenuItem.Size = New System.Drawing.Size(272, 22)
        Me.TarifasEspecialesToolStripMenuItem.Text = "Tarifas Especiales"
        '
        'DesgloseDeIngresosToolStripMenuItem
        '
        Me.DesgloseDeIngresosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GrupoDeIngresosToolStripMenuItem, Me.IngresosDesglozadosToolStripMenuItem})
        Me.DesgloseDeIngresosToolStripMenuItem.Name = "DesgloseDeIngresosToolStripMenuItem"
        Me.DesgloseDeIngresosToolStripMenuItem.Size = New System.Drawing.Size(244, 22)
        Me.DesgloseDeIngresosToolStripMenuItem.Text = "Desglose de ingresos"
        '
        'GrupoDeIngresosToolStripMenuItem
        '
        Me.GrupoDeIngresosToolStripMenuItem.Name = "GrupoDeIngresosToolStripMenuItem"
        Me.GrupoDeIngresosToolStripMenuItem.Size = New System.Drawing.Size(240, 22)
        Me.GrupoDeIngresosToolStripMenuItem.Text = "Grupo de Ingresos"
        '
        'IngresosDesglozadosToolStripMenuItem
        '
        Me.IngresosDesglozadosToolStripMenuItem.Name = "IngresosDesglozadosToolStripMenuItem"
        Me.IngresosDesglozadosToolStripMenuItem.Size = New System.Drawing.Size(240, 22)
        Me.IngresosDesglozadosToolStripMenuItem.Text = "Ingresos Desglozados"
        '
        'Catalogos33ToolStripMenuItem
        '
        Me.Catalogos33ToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ProductosSatToolStripMenuItem})
        Me.Catalogos33ToolStripMenuItem.Name = "Catalogos33ToolStripMenuItem"
        Me.Catalogos33ToolStripMenuItem.Size = New System.Drawing.Size(244, 22)
        Me.Catalogos33ToolStripMenuItem.Text = "Catalogos 3.3"
        '
        'ProductosSatToolStripMenuItem
        '
        Me.ProductosSatToolStripMenuItem.Name = "ProductosSatToolStripMenuItem"
        Me.ProductosSatToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.ProductosSatToolStripMenuItem.Text = "Productos Sat"
        '
        'ProcesosToolStripMenuItem
        '
        Me.ProcesosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ProcesosDeServicioPremiumToolStripMenuItem, Me.DesconexionToolStripMenuItem, Me.ProcesoDeCierreDeMesToolStripMenuItem, Me.OrdenesDeServicioToolStripMenuItem, Me.ToolStripMenuItemControlHoras, Me.QuejasToolStripMenuItem, Me.AgendaToolStripMenuItem1, Me.ProcesoDeDesconexiónTemporalPorContratoToolStripMenuItem, Me.ProcesoDeReactivaciónDeContratoToolStripMenuItem, Me.ContratoMaestroToolStripMenuItem, Me.ActivaciónPaqueteDePruebaToolStripMenuItem, Me.DepuraciónDeÓrdenesToolStripMenuItem, Me.CambioDeServicioToolStripMenuItem, Me.MesajesInstantaneosToolStripMenuItem, Me.CorreoToolStripMenuItem, Me.ResetearAparatosToolStripMenuItem, Me.ReseteoMasivoDeAparatosToolStripMenuItem, Me.AtenciónTelefónicaToolStripMenuItem1, Me.CambioDeClienteASoloInternetToolStripMenuItem, Me.CambioDeClienteAClienteNormalToolStripMenuItem, Me.CargosEspecialesToolStripMenuItem, Me.RecuperaciónDeCarteraToolStripMenuItem, Me.CortesToolStripMenuItem, Me.ReprocesamientoPorClienteToolStripMenuItem, Me.EnviarSMSMasivoToolStripMenuItem, Me.RecontrataciónToolStripMenuItem1, Me.RegresaAparatosAlAlmacenToolStripMenuItem, Me.VisitasAlClienteDelVendedorToolStripMenuItem, Me.GeneraciónMasivaDeÓrdenesDeCambioDeAparatoToolStripMenuItem})
        Me.ProcesosToolStripMenuItem.Name = "ProcesosToolStripMenuItem"
        Me.ProcesosToolStripMenuItem.Size = New System.Drawing.Size(88, 22)
        Me.ProcesosToolStripMenuItem.Text = "Procesos"
        '
        'ProcesosDeServicioPremiumToolStripMenuItem
        '
        Me.ProcesosDeServicioPremiumToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PrimerPeriodoToolStripMenuItem1, Me.SegundoPeriodoToolStripMenuItem1, Me.TercerPeriodoToolStripMenuItem})
        Me.ProcesosDeServicioPremiumToolStripMenuItem.Name = "ProcesosDeServicioPremiumToolStripMenuItem"
        Me.ProcesosDeServicioPremiumToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.ProcesosDeServicioPremiumToolStripMenuItem.Text = "Desconexión"
        '
        'PrimerPeriodoToolStripMenuItem1
        '
        Me.PrimerPeriodoToolStripMenuItem1.Name = "PrimerPeriodoToolStripMenuItem1"
        Me.PrimerPeriodoToolStripMenuItem1.Size = New System.Drawing.Size(202, 22)
        Me.PrimerPeriodoToolStripMenuItem1.Text = "Primer Periodo"
        '
        'SegundoPeriodoToolStripMenuItem1
        '
        Me.SegundoPeriodoToolStripMenuItem1.Name = "SegundoPeriodoToolStripMenuItem1"
        Me.SegundoPeriodoToolStripMenuItem1.Size = New System.Drawing.Size(202, 22)
        Me.SegundoPeriodoToolStripMenuItem1.Text = "Segundo Periodo"
        '
        'TercerPeriodoToolStripMenuItem
        '
        Me.TercerPeriodoToolStripMenuItem.Name = "TercerPeriodoToolStripMenuItem"
        Me.TercerPeriodoToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
        Me.TercerPeriodoToolStripMenuItem.Text = "Tercer Periodo"
        '
        'DesconexionToolStripMenuItem
        '
        Me.DesconexionToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ServicioBasicoYCanalesPremiumToolStripMenuItem, Me.InternetToolStripMenuItem})
        Me.DesconexionToolStripMenuItem.Name = "DesconexionToolStripMenuItem"
        Me.DesconexionToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.DesconexionToolStripMenuItem.Text = "Desconexión"
        Me.DesconexionToolStripMenuItem.Visible = False
        '
        'ServicioBasicoYCanalesPremiumToolStripMenuItem
        '
        Me.ServicioBasicoYCanalesPremiumToolStripMenuItem.Name = "ServicioBasicoYCanalesPremiumToolStripMenuItem"
        Me.ServicioBasicoYCanalesPremiumToolStripMenuItem.Size = New System.Drawing.Size(339, 22)
        Me.ServicioBasicoYCanalesPremiumToolStripMenuItem.Text = "Servicio Basico Y Canales Premium"
        '
        'InternetToolStripMenuItem
        '
        Me.InternetToolStripMenuItem.Name = "InternetToolStripMenuItem"
        Me.InternetToolStripMenuItem.Size = New System.Drawing.Size(339, 22)
        Me.InternetToolStripMenuItem.Text = "Internet"
        '
        'ProcesoDeCierreDeMesToolStripMenuItem
        '
        Me.ProcesoDeCierreDeMesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PrimerPeriodoToolStripMenuItem})
        Me.ProcesoDeCierreDeMesToolStripMenuItem.Enabled = False
        Me.ProcesoDeCierreDeMesToolStripMenuItem.Name = "ProcesoDeCierreDeMesToolStripMenuItem"
        Me.ProcesoDeCierreDeMesToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.ProcesoDeCierreDeMesToolStripMenuItem.Text = "Cierre de Mes"
        Me.ProcesoDeCierreDeMesToolStripMenuItem.Visible = False
        '
        'PrimerPeriodoToolStripMenuItem
        '
        Me.PrimerPeriodoToolStripMenuItem.Name = "PrimerPeriodoToolStripMenuItem"
        Me.PrimerPeriodoToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.PrimerPeriodoToolStripMenuItem.Text = "Primer Periodo"
        '
        'OrdenesDeServicioToolStripMenuItem
        '
        Me.OrdenesDeServicioToolStripMenuItem.Name = "OrdenesDeServicioToolStripMenuItem"
        Me.OrdenesDeServicioToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.OrdenesDeServicioToolStripMenuItem.Text = "Ordenes de Servicio"
        '
        'ToolStripMenuItemControlHoras
        '
        Me.ToolStripMenuItemControlHoras.Name = "ToolStripMenuItemControlHoras"
        Me.ToolStripMenuItemControlHoras.Size = New System.Drawing.Size(473, 22)
        Me.ToolStripMenuItemControlHoras.Text = "Control de Horas Ordenes de Servicio"
        '
        'QuejasToolStripMenuItem
        '
        Me.QuejasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.QuejasToolStripMenuItem1, Me.AgendaToolStripMenuItem, Me.AtenciónTelefónicaToolStripMenuItem})
        Me.QuejasToolStripMenuItem.Name = "QuejasToolStripMenuItem"
        Me.QuejasToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.QuejasToolStripMenuItem.Text = "Quejas"
        '
        'QuejasToolStripMenuItem1
        '
        Me.QuejasToolStripMenuItem1.Name = "QuejasToolStripMenuItem1"
        Me.QuejasToolStripMenuItem1.Size = New System.Drawing.Size(185, 22)
        Me.QuejasToolStripMenuItem1.Text = "Quejas"
        '
        'AgendaToolStripMenuItem
        '
        Me.AgendaToolStripMenuItem.Name = "AgendaToolStripMenuItem"
        Me.AgendaToolStripMenuItem.Size = New System.Drawing.Size(185, 22)
        Me.AgendaToolStripMenuItem.Text = "Agenda"
        Me.AgendaToolStripMenuItem.Visible = False
        '
        'AtenciónTelefónicaToolStripMenuItem
        '
        Me.AtenciónTelefónicaToolStripMenuItem.Name = "AtenciónTelefónicaToolStripMenuItem"
        Me.AtenciónTelefónicaToolStripMenuItem.Size = New System.Drawing.Size(185, 22)
        Me.AtenciónTelefónicaToolStripMenuItem.Text = "Generar Queja"
        '
        'AgendaToolStripMenuItem1
        '
        Me.AgendaToolStripMenuItem1.Name = "AgendaToolStripMenuItem1"
        Me.AgendaToolStripMenuItem1.Size = New System.Drawing.Size(473, 22)
        Me.AgendaToolStripMenuItem1.Text = "Agenda"
        '
        'ProcesoDeDesconexiónTemporalPorContratoToolStripMenuItem
        '
        Me.ProcesoDeDesconexiónTemporalPorContratoToolStripMenuItem.Name = "ProcesoDeDesconexiónTemporalPorContratoToolStripMenuItem"
        Me.ProcesoDeDesconexiónTemporalPorContratoToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.ProcesoDeDesconexiónTemporalPorContratoToolStripMenuItem.Text = "Desconexión Temporal por Contrato"
        '
        'ProcesoDeReactivaciónDeContratoToolStripMenuItem
        '
        Me.ProcesoDeReactivaciónDeContratoToolStripMenuItem.Name = "ProcesoDeReactivaciónDeContratoToolStripMenuItem"
        Me.ProcesoDeReactivaciónDeContratoToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.ProcesoDeReactivaciónDeContratoToolStripMenuItem.Text = "Reactivación de Contrato"
        '
        'ContratoMaestroToolStripMenuItem
        '
        Me.ContratoMaestroToolStripMenuItem.Name = "ContratoMaestroToolStripMenuItem"
        Me.ContratoMaestroToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.ContratoMaestroToolStripMenuItem.Text = "Contrato Maestro"
        '
        'ActivaciónPaqueteDePruebaToolStripMenuItem
        '
        Me.ActivaciónPaqueteDePruebaToolStripMenuItem.Name = "ActivaciónPaqueteDePruebaToolStripMenuItem"
        Me.ActivaciónPaqueteDePruebaToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.ActivaciónPaqueteDePruebaToolStripMenuItem.Text = "Activación Paquete de Prueba"
        '
        'DepuraciónDeÓrdenesToolStripMenuItem
        '
        Me.DepuraciónDeÓrdenesToolStripMenuItem.Name = "DepuraciónDeÓrdenesToolStripMenuItem"
        Me.DepuraciónDeÓrdenesToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.DepuraciónDeÓrdenesToolStripMenuItem.Text = "Depuración de Órdenes"
        Me.DepuraciónDeÓrdenesToolStripMenuItem.Visible = False
        '
        'CambioDeServicioToolStripMenuItem
        '
        Me.CambioDeServicioToolStripMenuItem.Name = "CambioDeServicioToolStripMenuItem"
        Me.CambioDeServicioToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.CambioDeServicioToolStripMenuItem.Text = "Cambio de Servicio"
        '
        'MesajesInstantaneosToolStripMenuItem
        '
        Me.MesajesInstantaneosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClientesVariosToolStripMenuItem, Me.ClienteToolStripMenuItem, Me.PruebaToolStripMenuItem})
        Me.MesajesInstantaneosToolStripMenuItem.Name = "MesajesInstantaneosToolStripMenuItem"
        Me.MesajesInstantaneosToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.MesajesInstantaneosToolStripMenuItem.Text = "Mensajes Instantáneos"
        '
        'ClientesVariosToolStripMenuItem
        '
        Me.ClientesVariosToolStripMenuItem.Name = "ClientesVariosToolStripMenuItem"
        Me.ClientesVariosToolStripMenuItem.Size = New System.Drawing.Size(276, 22)
        Me.ClientesVariosToolStripMenuItem.Text = "Clientes Varios"
        '
        'ClienteToolStripMenuItem
        '
        Me.ClienteToolStripMenuItem.Name = "ClienteToolStripMenuItem"
        Me.ClienteToolStripMenuItem.Size = New System.Drawing.Size(276, 22)
        Me.ClienteToolStripMenuItem.Text = "Por Cliente"
        '
        'PruebaToolStripMenuItem
        '
        Me.PruebaToolStripMenuItem.Name = "PruebaToolStripMenuItem"
        Me.PruebaToolStripMenuItem.Size = New System.Drawing.Size(276, 22)
        Me.PruebaToolStripMenuItem.Text = "Programación de Mensajes"
        '
        'CorreoToolStripMenuItem
        '
        Me.CorreoToolStripMenuItem.Name = "CorreoToolStripMenuItem"
        Me.CorreoToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.CorreoToolStripMenuItem.Text = "Correo"
        Me.CorreoToolStripMenuItem.Visible = False
        '
        'ResetearAparatosToolStripMenuItem
        '
        Me.ResetearAparatosToolStripMenuItem.Name = "ResetearAparatosToolStripMenuItem"
        Me.ResetearAparatosToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.ResetearAparatosToolStripMenuItem.Text = "Resetear Aparatos"
        '
        'ReseteoMasivoDeAparatosToolStripMenuItem
        '
        Me.ReseteoMasivoDeAparatosToolStripMenuItem.Name = "ReseteoMasivoDeAparatosToolStripMenuItem"
        Me.ReseteoMasivoDeAparatosToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.ReseteoMasivoDeAparatosToolStripMenuItem.Text = "Reseteo Masivo de Aparatos"
        '
        'AtenciónTelefónicaToolStripMenuItem1
        '
        Me.AtenciónTelefónicaToolStripMenuItem1.Name = "AtenciónTelefónicaToolStripMenuItem1"
        Me.AtenciónTelefónicaToolStripMenuItem1.Size = New System.Drawing.Size(473, 22)
        Me.AtenciónTelefónicaToolStripMenuItem1.Text = "Atención de Llamadas"
        '
        'CambioDeClienteASoloInternetToolStripMenuItem
        '
        Me.CambioDeClienteASoloInternetToolStripMenuItem.Name = "CambioDeClienteASoloInternetToolStripMenuItem"
        Me.CambioDeClienteASoloInternetToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.CambioDeClienteASoloInternetToolStripMenuItem.Text = "Cambio de Cliente Normal a Solo Internet"
        '
        'CambioDeClienteAClienteNormalToolStripMenuItem
        '
        Me.CambioDeClienteAClienteNormalToolStripMenuItem.Name = "CambioDeClienteAClienteNormalToolStripMenuItem"
        Me.CambioDeClienteAClienteNormalToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.CambioDeClienteAClienteNormalToolStripMenuItem.Text = "Cambio de Solo Internet a Cliente Normal"
        '
        'CargosEspecialesToolStripMenuItem
        '
        Me.CargosEspecialesToolStripMenuItem.Name = "CargosEspecialesToolStripMenuItem"
        Me.CargosEspecialesToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.CargosEspecialesToolStripMenuItem.Text = "Cargos y Bonificaciones Especiales"
        Me.CargosEspecialesToolStripMenuItem.Visible = False
        '
        'RecuperaciónDeCarteraToolStripMenuItem
        '
        Me.RecuperaciónDeCarteraToolStripMenuItem.Name = "RecuperaciónDeCarteraToolStripMenuItem"
        Me.RecuperaciónDeCarteraToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.RecuperaciónDeCarteraToolStripMenuItem.Text = "Recuperación de Cartera"
        Me.RecuperaciónDeCarteraToolStripMenuItem.Visible = False
        '
        'CortesToolStripMenuItem
        '
        Me.CortesToolStripMenuItem.Name = "CortesToolStripMenuItem"
        Me.CortesToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.CortesToolStripMenuItem.Text = "Cortes"
        Me.CortesToolStripMenuItem.Visible = False
        '
        'ReprocesamientoPorClienteToolStripMenuItem
        '
        Me.ReprocesamientoPorClienteToolStripMenuItem.Name = "ReprocesamientoPorClienteToolStripMenuItem"
        Me.ReprocesamientoPorClienteToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.ReprocesamientoPorClienteToolStripMenuItem.Text = "Reprocesamiento por Cliente"
        Me.ReprocesamientoPorClienteToolStripMenuItem.Visible = False
        '
        'EnviarSMSMasivoToolStripMenuItem
        '
        Me.EnviarSMSMasivoToolStripMenuItem.Name = "EnviarSMSMasivoToolStripMenuItem"
        Me.EnviarSMSMasivoToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.EnviarSMSMasivoToolStripMenuItem.Text = "Enviar SMS masivo"
        Me.EnviarSMSMasivoToolStripMenuItem.Visible = False
        '
        'RecontrataciónToolStripMenuItem1
        '
        Me.RecontrataciónToolStripMenuItem1.Name = "RecontrataciónToolStripMenuItem1"
        Me.RecontrataciónToolStripMenuItem1.Size = New System.Drawing.Size(473, 22)
        Me.RecontrataciónToolStripMenuItem1.Text = "Recontratación"
        Me.RecontrataciónToolStripMenuItem1.Visible = False
        '
        'RegresaAparatosAlAlmacenToolStripMenuItem
        '
        Me.RegresaAparatosAlAlmacenToolStripMenuItem.Name = "RegresaAparatosAlAlmacenToolStripMenuItem"
        Me.RegresaAparatosAlAlmacenToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.RegresaAparatosAlAlmacenToolStripMenuItem.Text = "Regresa Aparatos al Almacen"
        '
        'VisitasAlClienteDelVendedorToolStripMenuItem
        '
        Me.VisitasAlClienteDelVendedorToolStripMenuItem.Name = "VisitasAlClienteDelVendedorToolStripMenuItem"
        Me.VisitasAlClienteDelVendedorToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.VisitasAlClienteDelVendedorToolStripMenuItem.Text = "Visitas al Cliente del Vendedor"
        '
        'GeneraciónMasivaDeÓrdenesDeCambioDeAparatoToolStripMenuItem
        '
        Me.GeneraciónMasivaDeÓrdenesDeCambioDeAparatoToolStripMenuItem.Name = "GeneraciónMasivaDeÓrdenesDeCambioDeAparatoToolStripMenuItem"
        Me.GeneraciónMasivaDeÓrdenesDeCambioDeAparatoToolStripMenuItem.Size = New System.Drawing.Size(473, 22)
        Me.GeneraciónMasivaDeÓrdenesDeCambioDeAparatoToolStripMenuItem.Text = "Generación Masiva de Órdenes de Cambio de Aparato"
        '
        'ReportesToolStripMenuItem
        '
        Me.ReportesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TelefoníaToolStripMenuItem1, Me.EstadosDeCarteraToolStripMenuItem, Me.AnálisisDePenetraciónToolStripMenuItem, Me.VentasToolStripMenuItem, Me.Medidores2ToolStripMenuItem, Me.MedidoresToolStripMenuItem1, Me.AreaTécnicaToolStripMenuItem, Me.ClientesToolStripMenuItem2, Me.BitácoraDePruebasToolStripMenuItem, Me.BitácoraDeCorreosToolStripMenuItem, Me.ClientesConAdeudoDeMaterialToolStripMenuItem, Me.ClientesVariosMezcladosToolStripMenuItem, Me.DesgloceDeMensualidadesAdelantadasToolStripMenuItem, Me.ImporteDeMensualidadesAdelantadasToolStripMenuItem, Me.ContratoForzosoToolStripMenuItem, Me.EstadoDeCuentaToolStripMenuItem, Me.CarteraEjecutivaToolStripMenuItem, Me.ListadoDeClientesPorStausConAdeudoToolStripMenuItem, Me.PuntosDeAntigüedadToolStripMenuItem, Me.InterfazCablemodemsToolStripMenuItem, Me.InterfazDigitalesToolStripMenuItem, Me.ValorDeCarteraToolStripMenuItem, Me.RecontrataciónToolStripMenuItem, Me.AtencionToolStripMenuItem, Me.ListadoDeEquiposRecibidosPorCajerasosToolStripMenuItem, Me.ReporteEjecutivoToolStripMenuItem, Me.ReporteDeRecuperaciónDeCajasToolStripMenuItem, Me.ResumenMensualDeRecuperaciónDeCajasToolStripMenuItem, Me.ReporteInterfazDigitalToolStripMenuItem, Me.ReporteDeRenovaciónDeAnualidadesToolStripMenuItem, Me.ReporteClientesInternetToolStripMenuItem})
        Me.ReportesToolStripMenuItem.Name = "ReportesToolStripMenuItem"
        Me.ReportesToolStripMenuItem.Size = New System.Drawing.Size(88, 22)
        Me.ReportesToolStripMenuItem.Text = "&Reportes"
        '
        'TelefoníaToolStripMenuItem1
        '
        Me.TelefoníaToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ImpresiónDeEstadosDeCuentaToolStripMenuItem, Me.ImpresiónDeEstadosDeCuentaPágina2NToolStripMenuItem, Me.AuxToolStripMenuItem})
        Me.TelefoníaToolStripMenuItem1.Name = "TelefoníaToolStripMenuItem1"
        Me.TelefoníaToolStripMenuItem1.Size = New System.Drawing.Size(413, 22)
        Me.TelefoníaToolStripMenuItem1.Text = "Telefonía"
        Me.TelefoníaToolStripMenuItem1.Visible = False
        '
        'ImpresiónDeEstadosDeCuentaToolStripMenuItem
        '
        Me.ImpresiónDeEstadosDeCuentaToolStripMenuItem.Name = "ImpresiónDeEstadosDeCuentaToolStripMenuItem"
        Me.ImpresiónDeEstadosDeCuentaToolStripMenuItem.Size = New System.Drawing.Size(397, 22)
        Me.ImpresiónDeEstadosDeCuentaToolStripMenuItem.Text = "Impresión de Estados de Cuenta"
        '
        'ImpresiónDeEstadosDeCuentaPágina2NToolStripMenuItem
        '
        Me.ImpresiónDeEstadosDeCuentaPágina2NToolStripMenuItem.Name = "ImpresiónDeEstadosDeCuentaPágina2NToolStripMenuItem"
        Me.ImpresiónDeEstadosDeCuentaPágina2NToolStripMenuItem.Size = New System.Drawing.Size(397, 22)
        Me.ImpresiónDeEstadosDeCuentaPágina2NToolStripMenuItem.Text = "Impresión de Estados de Cuenta (Página 2)"
        '
        'AuxToolStripMenuItem
        '
        Me.AuxToolStripMenuItem.Name = "AuxToolStripMenuItem"
        Me.AuxToolStripMenuItem.Size = New System.Drawing.Size(397, 22)
        Me.AuxToolStripMenuItem.Text = "Auxiliar"
        '
        'EstadosDeCarteraToolStripMenuItem
        '
        Me.EstadosDeCarteraToolStripMenuItem.Name = "EstadosDeCarteraToolStripMenuItem"
        Me.EstadosDeCarteraToolStripMenuItem.Size = New System.Drawing.Size(413, 22)
        Me.EstadosDeCarteraToolStripMenuItem.Text = "Estados de Cartera"
        '
        'ClientesToolStripMenuItem2
        '
        Me.ClientesToolStripMenuItem2.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ReportesVariosToolStripMenuItem, Me.ReporteDePaquetesToolStripMenuItem, Me.RToolStripMenuItem, Me.ReporteClientesConComboToolStripMenuItem, Me.HotelesToolStripMenuItem, Me.OrdenesToolStripMenuItem, Me.GerencialToolStripMenuItem, Me.ReporteDePermanenciaToolStripMenuItem, Me.ReporteDeClientesSuspendidosQueYaPagaronToolStripMenuItem, Me.CantidadDeClientesPorMezclaDeServiciosToolStripMenuItem, Me.ReporteDeControlDeCambioDeCajaToolStripMenuItem, Me.ReporteInstalacionesVsPromocionesToolStripMenuItem})
        Me.ClientesToolStripMenuItem2.Name = "ClientesToolStripMenuItem2"
        Me.ClientesToolStripMenuItem2.Size = New System.Drawing.Size(413, 22)
        Me.ClientesToolStripMenuItem2.Text = "Clientes"
        '
        'ReportesVariosToolStripMenuItem
        '
        Me.ReportesVariosToolStripMenuItem.Name = "ReportesVariosToolStripMenuItem"
        Me.ReportesVariosToolStripMenuItem.Size = New System.Drawing.Size(465, 22)
        Me.ReportesVariosToolStripMenuItem.Text = "Reportes Varios"
        '
        'ReporteDePaquetesToolStripMenuItem
        '
        Me.ReporteDePaquetesToolStripMenuItem.Name = "ReporteDePaquetesToolStripMenuItem"
        Me.ReporteDePaquetesToolStripMenuItem.Size = New System.Drawing.Size(465, 22)
        Me.ReporteDePaquetesToolStripMenuItem.Text = "Reporte De Paquetes"
        '
        'RToolStripMenuItem
        '
        Me.RToolStripMenuItem.Name = "RToolStripMenuItem"
        Me.RToolStripMenuItem.Size = New System.Drawing.Size(465, 22)
        Me.RToolStripMenuItem.Text = "Reporte General De Datos Del Cliente"
        '
        'ReporteClientesConComboToolStripMenuItem
        '
        Me.ReporteClientesConComboToolStripMenuItem.Name = "ReporteClientesConComboToolStripMenuItem"
        Me.ReporteClientesConComboToolStripMenuItem.Size = New System.Drawing.Size(465, 22)
        Me.ReporteClientesConComboToolStripMenuItem.Text = "Reporte Clientes Con Combo"
        '
        'HotelesToolStripMenuItem
        '
        Me.HotelesToolStripMenuItem.Name = "HotelesToolStripMenuItem"
        Me.HotelesToolStripMenuItem.Size = New System.Drawing.Size(465, 22)
        Me.HotelesToolStripMenuItem.Text = "Hoteles"
        '
        'OrdenesToolStripMenuItem
        '
        Me.OrdenesToolStripMenuItem.Name = "OrdenesToolStripMenuItem"
        Me.OrdenesToolStripMenuItem.Size = New System.Drawing.Size(465, 22)
        Me.OrdenesToolStripMenuItem.Text = "Ordenes"
        '
        'GerencialToolStripMenuItem
        '
        Me.GerencialToolStripMenuItem.Name = "GerencialToolStripMenuItem"
        Me.GerencialToolStripMenuItem.Size = New System.Drawing.Size(465, 22)
        Me.GerencialToolStripMenuItem.Text = "Gerencial"
        '
        'ReporteDePermanenciaToolStripMenuItem
        '
        Me.ReporteDePermanenciaToolStripMenuItem.Name = "ReporteDePermanenciaToolStripMenuItem"
        Me.ReporteDePermanenciaToolStripMenuItem.Size = New System.Drawing.Size(465, 22)
        Me.ReporteDePermanenciaToolStripMenuItem.Text = "Reporte De Permanencia"
        '
        'ReporteDeClientesSuspendidosQueYaPagaronToolStripMenuItem
        '
        Me.ReporteDeClientesSuspendidosQueYaPagaronToolStripMenuItem.Name = "ReporteDeClientesSuspendidosQueYaPagaronToolStripMenuItem"
        Me.ReporteDeClientesSuspendidosQueYaPagaronToolStripMenuItem.Size = New System.Drawing.Size(465, 22)
        Me.ReporteDeClientesSuspendidosQueYaPagaronToolStripMenuItem.Text = "Reporte De Clientes Desconectados Que Ya Pagaron"
        '
        'CantidadDeClientesPorMezclaDeServiciosToolStripMenuItem
        '
        Me.CantidadDeClientesPorMezclaDeServiciosToolStripMenuItem.Name = "CantidadDeClientesPorMezclaDeServiciosToolStripMenuItem"
        Me.CantidadDeClientesPorMezclaDeServiciosToolStripMenuItem.Size = New System.Drawing.Size(465, 22)
        Me.CantidadDeClientesPorMezclaDeServiciosToolStripMenuItem.Text = "Cantidad de Clientes por Mezcla de Servicios"
        '
        'ReporteDeControlDeCambioDeCajaToolStripMenuItem
        '
        Me.ReporteDeControlDeCambioDeCajaToolStripMenuItem.Name = "ReporteDeControlDeCambioDeCajaToolStripMenuItem"
        Me.ReporteDeControlDeCambioDeCajaToolStripMenuItem.Size = New System.Drawing.Size(465, 22)
        Me.ReporteDeControlDeCambioDeCajaToolStripMenuItem.Text = "Reporte de Control de Cambio de Caja"
        '
        'ReporteInstalacionesVsPromocionesToolStripMenuItem
        '
        Me.ReporteInstalacionesVsPromocionesToolStripMenuItem.Name = "ReporteInstalacionesVsPromocionesToolStripMenuItem"
        Me.ReporteInstalacionesVsPromocionesToolStripMenuItem.Size = New System.Drawing.Size(465, 22)
        Me.ReporteInstalacionesVsPromocionesToolStripMenuItem.Text = "Reporte Instalaciones vs Promociones"
        '
        'AnálisisDePenetraciónToolStripMenuItem
        '
        Me.AnálisisDePenetraciónToolStripMenuItem.Name = "AnálisisDePenetraciónToolStripMenuItem"
        Me.AnálisisDePenetraciónToolStripMenuItem.Size = New System.Drawing.Size(413, 22)
        Me.AnálisisDePenetraciónToolStripMenuItem.Text = "Análisis de Penetración"
        '
        'VentasToolStripMenuItem
        '
        Me.VentasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CalcularComisionesPorVendedorToolStripMenuItem1, Me.ResumenDeVentasToolStripMenuItem, Me.ResumenVendedoresToolStripMenuItem, Me.GráficasToolStripMenuItem, Me.PelículasToolStripMenuItem, Me.PPVToolStripMenuItem, Me.ServiciosContratadosToolStripMenuItem, Me.CancelacionesToolStripMenuItem, Me.VentasTotalesToolStripMenuItem, Me.CancelacionDeVentasToolStripMenuItem, Me.ResumenVentasPorStatusToolStripMenuItem, Me.ComicionesDeVendedoresToolStripMenuItem, Me.ServiciosVendidosPorSemanaToolStripMenuItem, Me.ReimpresionDeFoliosParaVendedoresToolStripMenuItem1, Me.StatusDeLosFoliosDeLosVendedoresToolStripMenuItem})
        Me.VentasToolStripMenuItem.Name = "VentasToolStripMenuItem"
        Me.VentasToolStripMenuItem.Size = New System.Drawing.Size(413, 22)
        Me.VentasToolStripMenuItem.Text = "Ventas"
        '
        'CalcularComisionesPorVendedorToolStripMenuItem1
        '
        Me.CalcularComisionesPorVendedorToolStripMenuItem1.Name = "CalcularComisionesPorVendedorToolStripMenuItem1"
        Me.CalcularComisionesPorVendedorToolStripMenuItem1.Size = New System.Drawing.Size(437, 22)
        Me.CalcularComisionesPorVendedorToolStripMenuItem1.Text = "Reportes Generales"
        '
        'ResumenDeVentasToolStripMenuItem
        '
        Me.ResumenDeVentasToolStripMenuItem.Name = "ResumenDeVentasToolStripMenuItem"
        Me.ResumenDeVentasToolStripMenuItem.Size = New System.Drawing.Size(437, 22)
        Me.ResumenDeVentasToolStripMenuItem.Text = "Resumen Sucursal"
        '
        'ResumenVendedoresToolStripMenuItem
        '
        Me.ResumenVendedoresToolStripMenuItem.Name = "ResumenVendedoresToolStripMenuItem"
        Me.ResumenVendedoresToolStripMenuItem.Size = New System.Drawing.Size(437, 22)
        Me.ResumenVendedoresToolStripMenuItem.Text = "Resumen Vendedores"
        '
        'GráficasToolStripMenuItem
        '
        Me.GráficasToolStripMenuItem.Name = "GráficasToolStripMenuItem"
        Me.GráficasToolStripMenuItem.Size = New System.Drawing.Size(437, 22)
        Me.GráficasToolStripMenuItem.Text = "Gráficas"
        '
        'PelículasToolStripMenuItem
        '
        Me.PelículasToolStripMenuItem.Name = "PelículasToolStripMenuItem"
        Me.PelículasToolStripMenuItem.Size = New System.Drawing.Size(437, 22)
        Me.PelículasToolStripMenuItem.Text = "Películas"
        '
        'PPVToolStripMenuItem
        '
        Me.PPVToolStripMenuItem.Name = "PPVToolStripMenuItem"
        Me.PPVToolStripMenuItem.Size = New System.Drawing.Size(437, 22)
        Me.PPVToolStripMenuItem.Text = "Películas por Cliente"
        '
        'ServiciosContratadosToolStripMenuItem
        '
        Me.ServiciosContratadosToolStripMenuItem.Name = "ServiciosContratadosToolStripMenuItem"
        Me.ServiciosContratadosToolStripMenuItem.Size = New System.Drawing.Size(437, 22)
        Me.ServiciosContratadosToolStripMenuItem.Text = "Servicios Contratados"
        '
        'CancelacionesToolStripMenuItem
        '
        Me.CancelacionesToolStripMenuItem.Name = "CancelacionesToolStripMenuItem"
        Me.CancelacionesToolStripMenuItem.Size = New System.Drawing.Size(437, 22)
        Me.CancelacionesToolStripMenuItem.Text = "Baja de Servicios"
        '
        'VentasTotalesToolStripMenuItem
        '
        Me.VentasTotalesToolStripMenuItem.Name = "VentasTotalesToolStripMenuItem"
        Me.VentasTotalesToolStripMenuItem.Size = New System.Drawing.Size(437, 22)
        Me.VentasTotalesToolStripMenuItem.Text = "Ventas Totales"
        '
        'CancelacionDeVentasToolStripMenuItem
        '
        Me.CancelacionDeVentasToolStripMenuItem.Name = "CancelacionDeVentasToolStripMenuItem"
        Me.CancelacionDeVentasToolStripMenuItem.Size = New System.Drawing.Size(437, 22)
        Me.CancelacionDeVentasToolStripMenuItem.Text = "Cancelaciones De Ventas"
        '
        'ResumenVentasPorStatusToolStripMenuItem
        '
        Me.ResumenVentasPorStatusToolStripMenuItem.Name = "ResumenVentasPorStatusToolStripMenuItem"
        Me.ResumenVentasPorStatusToolStripMenuItem.Size = New System.Drawing.Size(437, 22)
        Me.ResumenVentasPorStatusToolStripMenuItem.Text = "Status de Ventas Globales"
        '
        'ComicionesDeVendedoresToolStripMenuItem
        '
        Me.ComicionesDeVendedoresToolStripMenuItem.Name = "ComicionesDeVendedoresToolStripMenuItem"
        Me.ComicionesDeVendedoresToolStripMenuItem.Size = New System.Drawing.Size(437, 22)
        Me.ComicionesDeVendedoresToolStripMenuItem.Text = "Comiciones De Vendedores"
        '
        'ServiciosVendidosPorSemanaToolStripMenuItem
        '
        Me.ServiciosVendidosPorSemanaToolStripMenuItem.Name = "ServiciosVendidosPorSemanaToolStripMenuItem"
        Me.ServiciosVendidosPorSemanaToolStripMenuItem.Size = New System.Drawing.Size(437, 22)
        Me.ServiciosVendidosPorSemanaToolStripMenuItem.Text = "Reporte de Comisiones por Ventas"
        '
        'ReimpresionDeFoliosParaVendedoresToolStripMenuItem1
        '
        Me.ReimpresionDeFoliosParaVendedoresToolStripMenuItem1.Name = "ReimpresionDeFoliosParaVendedoresToolStripMenuItem1"
        Me.ReimpresionDeFoliosParaVendedoresToolStripMenuItem1.Size = New System.Drawing.Size(437, 22)
        Me.ReimpresionDeFoliosParaVendedoresToolStripMenuItem1.Text = "Reimpresion de Folios para Vendedores"
        '
        'StatusDeLosFoliosDeLosVendedoresToolStripMenuItem
        '
        Me.StatusDeLosFoliosDeLosVendedoresToolStripMenuItem.Name = "StatusDeLosFoliosDeLosVendedoresToolStripMenuItem"
        Me.StatusDeLosFoliosDeLosVendedoresToolStripMenuItem.Size = New System.Drawing.Size(437, 22)
        Me.StatusDeLosFoliosDeLosVendedoresToolStripMenuItem.Text = "Folios Faltantes y Cancelados de los Vendedores"
        '
        'Medidores2ToolStripMenuItem
        '
        Me.Medidores2ToolStripMenuItem.Name = "Medidores2ToolStripMenuItem"
        Me.Medidores2ToolStripMenuItem.Size = New System.Drawing.Size(413, 22)
        Me.Medidores2ToolStripMenuItem.Text = "Medidores"
        '
        'MedidoresToolStripMenuItem1
        '
        Me.MedidoresToolStripMenuItem1.Name = "MedidoresToolStripMenuItem1"
        Me.MedidoresToolStripMenuItem1.Size = New System.Drawing.Size(413, 22)
        Me.MedidoresToolStripMenuItem1.Text = "Medidores"
        Me.MedidoresToolStripMenuItem1.Visible = False
        '
        'AreaTécnicaToolStripMenuItem
        '
        Me.AreaTécnicaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OrdenesDeServicioToolStripMenuItem1, Me.QuejasToolStripMenuItem2, Me.LlamadasTelefónicasToolStripMenuItem, Me.ListadoDeActividadesDelTécnicoToolStripMenuItem1, Me.AgendaDeActividadesDelTécnicoToolStripMenuItem, Me.MaterialesUtilizadosToolStripMenuItem, Me.TiemposPorServicioToolStripMenuItem, Me.TiempoDeQuejasToolStripMenuItem, Me.ReporteDeProductividadToolStripMenuItem, Me.ListadoDeInstalacionesDeExtecionesToolStripMenuItem, Me.RelaciónDeOrdenesEjecutadasYCapturadasDelTécnicoToolStripMenuItem, Me.ReporteDeClientesPorTipoDeInstalaciónToolStripMenuItem})
        Me.AreaTécnicaToolStripMenuItem.Name = "AreaTécnicaToolStripMenuItem"
        Me.AreaTécnicaToolStripMenuItem.Size = New System.Drawing.Size(413, 22)
        Me.AreaTécnicaToolStripMenuItem.Text = "Area Técnica"
        '
        'OrdenesDeServicioToolStripMenuItem1
        '
        Me.OrdenesDeServicioToolStripMenuItem1.Name = "OrdenesDeServicioToolStripMenuItem1"
        Me.OrdenesDeServicioToolStripMenuItem1.Size = New System.Drawing.Size(505, 22)
        Me.OrdenesDeServicioToolStripMenuItem1.Text = "Ordenes de Servicio"
        '
        'QuejasToolStripMenuItem2
        '
        Me.QuejasToolStripMenuItem2.Name = "QuejasToolStripMenuItem2"
        Me.QuejasToolStripMenuItem2.Size = New System.Drawing.Size(505, 22)
        Me.QuejasToolStripMenuItem2.Text = "Quejas"
        '
        'LlamadasTelefónicasToolStripMenuItem
        '
        Me.LlamadasTelefónicasToolStripMenuItem.Name = "LlamadasTelefónicasToolStripMenuItem"
        Me.LlamadasTelefónicasToolStripMenuItem.Size = New System.Drawing.Size(505, 22)
        Me.LlamadasTelefónicasToolStripMenuItem.Text = "Atención Telefónica"
        '
        'ListadoDeActividadesDelTécnicoToolStripMenuItem1
        '
        Me.ListadoDeActividadesDelTécnicoToolStripMenuItem1.Name = "ListadoDeActividadesDelTécnicoToolStripMenuItem1"
        Me.ListadoDeActividadesDelTécnicoToolStripMenuItem1.Size = New System.Drawing.Size(505, 22)
        Me.ListadoDeActividadesDelTécnicoToolStripMenuItem1.Text = "Listado de Actividades del Técnico"
        '
        'AgendaDeActividadesDelTécnicoToolStripMenuItem
        '
        Me.AgendaDeActividadesDelTécnicoToolStripMenuItem.Name = "AgendaDeActividadesDelTécnicoToolStripMenuItem"
        Me.AgendaDeActividadesDelTécnicoToolStripMenuItem.Size = New System.Drawing.Size(505, 22)
        Me.AgendaDeActividadesDelTécnicoToolStripMenuItem.Text = "Agenda de Actividades del Técnico"
        '
        'MaterialesUtilizadosToolStripMenuItem
        '
        Me.MaterialesUtilizadosToolStripMenuItem.Name = "MaterialesUtilizadosToolStripMenuItem"
        Me.MaterialesUtilizadosToolStripMenuItem.Size = New System.Drawing.Size(505, 22)
        Me.MaterialesUtilizadosToolStripMenuItem.Text = "Materiales Utilizados"
        '
        'TiemposPorServicioToolStripMenuItem
        '
        Me.TiemposPorServicioToolStripMenuItem.Name = "TiemposPorServicioToolStripMenuItem"
        Me.TiemposPorServicioToolStripMenuItem.Size = New System.Drawing.Size(505, 22)
        Me.TiemposPorServicioToolStripMenuItem.Text = "Tiempo de Orden  Servicio Ejecutadas"
        '
        'TiempoDeQuejasToolStripMenuItem
        '
        Me.TiempoDeQuejasToolStripMenuItem.Name = "TiempoDeQuejasToolStripMenuItem"
        Me.TiempoDeQuejasToolStripMenuItem.Size = New System.Drawing.Size(505, 22)
        Me.TiempoDeQuejasToolStripMenuItem.Text = "Tiempo de Quejas Ejecutadas"
        '
        'ReporteDeProductividadToolStripMenuItem
        '
        Me.ReporteDeProductividadToolStripMenuItem.Name = "ReporteDeProductividadToolStripMenuItem"
        Me.ReporteDeProductividadToolStripMenuItem.Size = New System.Drawing.Size(505, 22)
        Me.ReporteDeProductividadToolStripMenuItem.Text = "Reporte De Productividad"
        '
        'ListadoDeInstalacionesDeExtecionesToolStripMenuItem
        '
        Me.ListadoDeInstalacionesDeExtecionesToolStripMenuItem.Name = "ListadoDeInstalacionesDeExtecionesToolStripMenuItem"
        Me.ListadoDeInstalacionesDeExtecionesToolStripMenuItem.Size = New System.Drawing.Size(505, 22)
        Me.ListadoDeInstalacionesDeExtecionesToolStripMenuItem.Text = "Reporte de Instalaciones de Extensiones"
        '
        'RelaciónDeOrdenesEjecutadasYCapturadasDelTécnicoToolStripMenuItem
        '
        Me.RelaciónDeOrdenesEjecutadasYCapturadasDelTécnicoToolStripMenuItem.Name = "RelaciónDeOrdenesEjecutadasYCapturadasDelTécnicoToolStripMenuItem"
        Me.RelaciónDeOrdenesEjecutadasYCapturadasDelTécnicoToolStripMenuItem.Size = New System.Drawing.Size(505, 22)
        Me.RelaciónDeOrdenesEjecutadasYCapturadasDelTécnicoToolStripMenuItem.Text = "Relación de Ordenes Ejecutadas y Capturadas del Técnico"
        '
        'ReporteDeClientesPorTipoDeInstalaciónToolStripMenuItem
        '
        Me.ReporteDeClientesPorTipoDeInstalaciónToolStripMenuItem.Name = "ReporteDeClientesPorTipoDeInstalaciónToolStripMenuItem"
        Me.ReporteDeClientesPorTipoDeInstalaciónToolStripMenuItem.Size = New System.Drawing.Size(505, 22)
        Me.ReporteDeClientesPorTipoDeInstalaciónToolStripMenuItem.Text = "Reporte de Clientes por Tipo de Instalación"
        '
        'BitácoraDePruebasToolStripMenuItem
        '
        Me.BitácoraDePruebasToolStripMenuItem.Name = "BitácoraDePruebasToolStripMenuItem"
        Me.BitácoraDePruebasToolStripMenuItem.Size = New System.Drawing.Size(413, 22)
        Me.BitácoraDePruebasToolStripMenuItem.Text = "Bitácora de Pruebas"
        '
        'BitácoraDeCorreosToolStripMenuItem
        '
        Me.BitácoraDeCorreosToolStripMenuItem.Name = "BitácoraDeCorreosToolStripMenuItem"
        Me.BitácoraDeCorreosToolStripMenuItem.Size = New System.Drawing.Size(413, 22)
        Me.BitácoraDeCorreosToolStripMenuItem.Text = "Bitácora de Correos"
        Me.BitácoraDeCorreosToolStripMenuItem.Visible = False
        '
        'ClientesConAdeudoDeMaterialToolStripMenuItem
        '
        Me.ClientesConAdeudoDeMaterialToolStripMenuItem.Name = "ClientesConAdeudoDeMaterialToolStripMenuItem"
        Me.ClientesConAdeudoDeMaterialToolStripMenuItem.Size = New System.Drawing.Size(413, 22)
        Me.ClientesConAdeudoDeMaterialToolStripMenuItem.Text = "Clientes con Adeudo de Material"
        '
        'ClientesVariosMezcladosToolStripMenuItem
        '
        Me.ClientesVariosMezcladosToolStripMenuItem.Name = "ClientesVariosMezcladosToolStripMenuItem"
        Me.ClientesVariosMezcladosToolStripMenuItem.Size = New System.Drawing.Size(413, 22)
        Me.ClientesVariosMezcladosToolStripMenuItem.Text = "Clientes Varios Mezclados"
        Me.ClientesVariosMezcladosToolStripMenuItem.Visible = False
        '
        'DesgloceDeMensualidadesAdelantadasToolStripMenuItem
        '
        Me.DesgloceDeMensualidadesAdelantadasToolStripMenuItem.Name = "DesgloceDeMensualidadesAdelantadasToolStripMenuItem"
        Me.DesgloceDeMensualidadesAdelantadasToolStripMenuItem.Size = New System.Drawing.Size(413, 22)
        Me.DesgloceDeMensualidadesAdelantadasToolStripMenuItem.Text = "Desgloce de Mensualidades Adelantadas"
        '
        'ImporteDeMensualidadesAdelantadasToolStripMenuItem
        '
        Me.ImporteDeMensualidadesAdelantadasToolStripMenuItem.Name = "ImporteDeMensualidadesAdelantadasToolStripMenuItem"
        Me.ImporteDeMensualidadesAdelantadasToolStripMenuItem.Size = New System.Drawing.Size(413, 22)
        Me.ImporteDeMensualidadesAdelantadasToolStripMenuItem.Text = "Importe De Mensualidades Adelantadas"
        '
        'ContratoForzosoToolStripMenuItem
        '
        Me.ContratoForzosoToolStripMenuItem.Name = "ContratoForzosoToolStripMenuItem"
        Me.ContratoForzosoToolStripMenuItem.Size = New System.Drawing.Size(413, 22)
        Me.ContratoForzosoToolStripMenuItem.Text = "Contrato Forzoso"
        '
        'EstadoDeCuentaToolStripMenuItem
        '
        Me.EstadoDeCuentaToolStripMenuItem.Enabled = False
        Me.EstadoDeCuentaToolStripMenuItem.Name = "EstadoDeCuentaToolStripMenuItem"
        Me.EstadoDeCuentaToolStripMenuItem.Size = New System.Drawing.Size(413, 22)
        Me.EstadoDeCuentaToolStripMenuItem.Text = "Estado de Cuenta"
        Me.EstadoDeCuentaToolStripMenuItem.Visible = False
        '
        'CarteraEjecutivaToolStripMenuItem
        '
        Me.CarteraEjecutivaToolStripMenuItem.Name = "CarteraEjecutivaToolStripMenuItem"
        Me.CarteraEjecutivaToolStripMenuItem.Size = New System.Drawing.Size(413, 22)
        Me.CarteraEjecutivaToolStripMenuItem.Text = "Cartera Ejecutiva"
        '
        'ListadoDeClientesPorStausConAdeudoToolStripMenuItem
        '
        Me.ListadoDeClientesPorStausConAdeudoToolStripMenuItem.Name = "ListadoDeClientesPorStausConAdeudoToolStripMenuItem"
        Me.ListadoDeClientesPorStausConAdeudoToolStripMenuItem.Size = New System.Drawing.Size(413, 22)
        Me.ListadoDeClientesPorStausConAdeudoToolStripMenuItem.Text = "Listado de Recuperación para Cartera"
        '
        'PuntosDeAntigüedadToolStripMenuItem
        '
        Me.PuntosDeAntigüedadToolStripMenuItem.Name = "PuntosDeAntigüedadToolStripMenuItem"
        Me.PuntosDeAntigüedadToolStripMenuItem.Size = New System.Drawing.Size(413, 22)
        Me.PuntosDeAntigüedadToolStripMenuItem.Text = "Resumen de Puntos de Antigüedad"
        '
        'InterfazCablemodemsToolStripMenuItem
        '
        Me.InterfazCablemodemsToolStripMenuItem.Name = "InterfazCablemodemsToolStripMenuItem"
        Me.InterfazCablemodemsToolStripMenuItem.Size = New System.Drawing.Size(413, 22)
        Me.InterfazCablemodemsToolStripMenuItem.Text = "Interfaz Cablemodems"
        '
        'InterfazDigitalesToolStripMenuItem
        '
        Me.InterfazDigitalesToolStripMenuItem.Name = "InterfazDigitalesToolStripMenuItem"
        Me.InterfazDigitalesToolStripMenuItem.Size = New System.Drawing.Size(413, 22)
        Me.InterfazDigitalesToolStripMenuItem.Text = "Interfaz Digitales"
        '
        'ValorDeCarteraToolStripMenuItem
        '
        Me.ValorDeCarteraToolStripMenuItem.Name = "ValorDeCarteraToolStripMenuItem"
        Me.ValorDeCarteraToolStripMenuItem.Size = New System.Drawing.Size(413, 22)
        Me.ValorDeCarteraToolStripMenuItem.Text = "Valor de Cartera"
        '
        'RecontrataciónToolStripMenuItem
        '
        Me.RecontrataciónToolStripMenuItem.Name = "RecontrataciónToolStripMenuItem"
        Me.RecontrataciónToolStripMenuItem.Size = New System.Drawing.Size(413, 22)
        Me.RecontrataciónToolStripMenuItem.Text = "Recontratación"
        Me.RecontrataciónToolStripMenuItem.Visible = False
        '
        'AtencionToolStripMenuItem
        '
        Me.AtencionToolStripMenuItem.Name = "AtencionToolStripMenuItem"
        Me.AtencionToolStripMenuItem.Size = New System.Drawing.Size(413, 22)
        Me.AtencionToolStripMenuItem.Text = "Atencion De Llamadas Télefonicas"
        '
        'ListadoDeEquiposRecibidosPorCajerasosToolStripMenuItem
        '
        Me.ListadoDeEquiposRecibidosPorCajerasosToolStripMenuItem.Name = "ListadoDeEquiposRecibidosPorCajerasosToolStripMenuItem"
        Me.ListadoDeEquiposRecibidosPorCajerasosToolStripMenuItem.Size = New System.Drawing.Size(413, 22)
        Me.ListadoDeEquiposRecibidosPorCajerasosToolStripMenuItem.Text = "Listado de Equipos Recibidos por Cajeras(os)"
        '
        'ReporteEjecutivoToolStripMenuItem
        '
        Me.ReporteEjecutivoToolStripMenuItem.Name = "ReporteEjecutivoToolStripMenuItem"
        Me.ReporteEjecutivoToolStripMenuItem.Size = New System.Drawing.Size(413, 22)
        Me.ReporteEjecutivoToolStripMenuItem.Text = "Reporte Ejecutivo de Ingresos y Operaciones"
        '
        'ReporteDeRecuperaciónDeCajasToolStripMenuItem
        '
        Me.ReporteDeRecuperaciónDeCajasToolStripMenuItem.Name = "ReporteDeRecuperaciónDeCajasToolStripMenuItem"
        Me.ReporteDeRecuperaciónDeCajasToolStripMenuItem.Size = New System.Drawing.Size(413, 22)
        Me.ReporteDeRecuperaciónDeCajasToolStripMenuItem.Text = "Reporte de Recuperación de Cajas"
        '
        'ResumenMensualDeRecuperaciónDeCajasToolStripMenuItem
        '
        Me.ResumenMensualDeRecuperaciónDeCajasToolStripMenuItem.Name = "ResumenMensualDeRecuperaciónDeCajasToolStripMenuItem"
        Me.ResumenMensualDeRecuperaciónDeCajasToolStripMenuItem.Size = New System.Drawing.Size(413, 22)
        Me.ResumenMensualDeRecuperaciónDeCajasToolStripMenuItem.Text = "Resumen Mensual de Recuperación de Cajas"
        '
        'ReporteInterfazDigitalToolStripMenuItem
        '
        Me.ReporteInterfazDigitalToolStripMenuItem.Name = "ReporteInterfazDigitalToolStripMenuItem"
        Me.ReporteInterfazDigitalToolStripMenuItem.Size = New System.Drawing.Size(413, 22)
        Me.ReporteInterfazDigitalToolStripMenuItem.Text = "Reporte InterfazDigital"
        '
        'ReporteDeRenovaciónDeAnualidadesToolStripMenuItem
        '
        Me.ReporteDeRenovaciónDeAnualidadesToolStripMenuItem.Name = "ReporteDeRenovaciónDeAnualidadesToolStripMenuItem"
        Me.ReporteDeRenovaciónDeAnualidadesToolStripMenuItem.Size = New System.Drawing.Size(413, 22)
        Me.ReporteDeRenovaciónDeAnualidadesToolStripMenuItem.Text = "Reporte de Renovación de Anualidades"
        '
        'ReporteClientesInternetToolStripMenuItem
        '
        Me.ReporteClientesInternetToolStripMenuItem.Name = "ReporteClientesInternetToolStripMenuItem"
        Me.ReporteClientesInternetToolStripMenuItem.Size = New System.Drawing.Size(413, 22)
        Me.ReporteClientesInternetToolStripMenuItem.Text = "Reporte Clientes Internet"
        '
        'GeneralesToolStripMenuItem
        '
        Me.GeneralesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GeneralesDelSistemaToolStripMenuItem, Me.InterfasCablemodemsToolStripMenuItem, Me.InterfasDecodificadoresToolStripMenuItem, Me.GeneralesDeBancosToolStripMenuItem, Me.GeneralesDeInterfacesDigitalesToolStripMenuItem, Me.GeneralesDeInterfacesInternetToolStripMenuItem, Me.GeneralesDeOXXOToolStripMenuItem, Me.ConfiguracionDelSistemaToolStripMenuItem, Me.GeneralesReportesToolStripMenuItem, Me.EncargadosDelSistemaToolStripMenuItem, Me.PreciosDeArticulosDeInstalaciónToolStripMenuItem, Me.BitacoraDelSistemaToolStripMenuItem, Me.ProcesamientoDeLlamadasToolStripMenuItem, Me.ProcesamientoDeCDRToolStripMenuItem, Me.InterfazDigitalesXcriptToolStripMenuItem})
        Me.GeneralesToolStripMenuItem.Name = "GeneralesToolStripMenuItem"
        Me.GeneralesToolStripMenuItem.Size = New System.Drawing.Size(96, 22)
        Me.GeneralesToolStripMenuItem.Text = "&Generales"
        '
        'GeneralesDelSistemaToolStripMenuItem
        '
        Me.GeneralesDelSistemaToolStripMenuItem.Name = "GeneralesDelSistemaToolStripMenuItem"
        Me.GeneralesDelSistemaToolStripMenuItem.Size = New System.Drawing.Size(335, 22)
        Me.GeneralesDelSistemaToolStripMenuItem.Text = "Sistema"
        '
        'InterfasCablemodemsToolStripMenuItem
        '
        Me.InterfasCablemodemsToolStripMenuItem.Name = "InterfasCablemodemsToolStripMenuItem"
        Me.InterfasCablemodemsToolStripMenuItem.Size = New System.Drawing.Size(335, 22)
        Me.InterfasCablemodemsToolStripMenuItem.Text = "Interfaz Cablemodems"
        '
        'InterfasDecodificadoresToolStripMenuItem
        '
        Me.InterfasDecodificadoresToolStripMenuItem.Name = "InterfasDecodificadoresToolStripMenuItem"
        Me.InterfasDecodificadoresToolStripMenuItem.Size = New System.Drawing.Size(335, 22)
        Me.InterfasDecodificadoresToolStripMenuItem.Text = "Interfaz Digitales"
        '
        'GeneralesDeBancosToolStripMenuItem
        '
        Me.GeneralesDeBancosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BancosToolStripMenuItem1, Me.GeneralesProsaBancomerToolStripMenuItem})
        Me.GeneralesDeBancosToolStripMenuItem.Name = "GeneralesDeBancosToolStripMenuItem"
        Me.GeneralesDeBancosToolStripMenuItem.Size = New System.Drawing.Size(335, 22)
        Me.GeneralesDeBancosToolStripMenuItem.Text = "Bancos"
        '
        'BancosToolStripMenuItem1
        '
        Me.BancosToolStripMenuItem1.Name = "BancosToolStripMenuItem1"
        Me.BancosToolStripMenuItem1.Size = New System.Drawing.Size(277, 22)
        Me.BancosToolStripMenuItem1.Text = "Bancos"
        '
        'GeneralesProsaBancomerToolStripMenuItem
        '
        Me.GeneralesProsaBancomerToolStripMenuItem.Name = "GeneralesProsaBancomerToolStripMenuItem"
        Me.GeneralesProsaBancomerToolStripMenuItem.Size = New System.Drawing.Size(277, 22)
        Me.GeneralesProsaBancomerToolStripMenuItem.Text = "Generales Prosa Bancomer"
        '
        'GeneralesDeInterfacesDigitalesToolStripMenuItem
        '
        Me.GeneralesDeInterfacesDigitalesToolStripMenuItem.Name = "GeneralesDeInterfacesDigitalesToolStripMenuItem"
        Me.GeneralesDeInterfacesDigitalesToolStripMenuItem.Size = New System.Drawing.Size(335, 22)
        Me.GeneralesDeInterfacesDigitalesToolStripMenuItem.Text = "Interfaces Digitales"
        '
        'GeneralesDeInterfacesInternetToolStripMenuItem
        '
        Me.GeneralesDeInterfacesInternetToolStripMenuItem.Name = "GeneralesDeInterfacesInternetToolStripMenuItem"
        Me.GeneralesDeInterfacesInternetToolStripMenuItem.Size = New System.Drawing.Size(335, 22)
        Me.GeneralesDeInterfacesInternetToolStripMenuItem.Text = "Interfaces Internet"
        '
        'GeneralesDeOXXOToolStripMenuItem
        '
        Me.GeneralesDeOXXOToolStripMenuItem.Name = "GeneralesDeOXXOToolStripMenuItem"
        Me.GeneralesDeOXXOToolStripMenuItem.Size = New System.Drawing.Size(335, 22)
        Me.GeneralesDeOXXOToolStripMenuItem.Text = "Cobros en el OXXO"
        '
        'ConfiguracionDelSistemaToolStripMenuItem
        '
        Me.ConfiguracionDelSistemaToolStripMenuItem.Name = "ConfiguracionDelSistemaToolStripMenuItem"
        Me.ConfiguracionDelSistemaToolStripMenuItem.Size = New System.Drawing.Size(335, 22)
        Me.ConfiguracionDelSistemaToolStripMenuItem.Text = "Configuracion del Sistema"
        Me.ConfiguracionDelSistemaToolStripMenuItem.Visible = False
        '
        'GeneralesReportesToolStripMenuItem
        '
        Me.GeneralesReportesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SoftvToolStripMenuItem, Me.FacsoftvToolStripMenuItem})
        Me.GeneralesReportesToolStripMenuItem.Name = "GeneralesReportesToolStripMenuItem"
        Me.GeneralesReportesToolStripMenuItem.Size = New System.Drawing.Size(335, 22)
        Me.GeneralesReportesToolStripMenuItem.Text = "Reportes"
        Me.GeneralesReportesToolStripMenuItem.Visible = False
        '
        'SoftvToolStripMenuItem
        '
        Me.SoftvToolStripMenuItem.Name = "SoftvToolStripMenuItem"
        Me.SoftvToolStripMenuItem.Size = New System.Drawing.Size(140, 22)
        Me.SoftvToolStripMenuItem.Text = "Softv"
        '
        'FacsoftvToolStripMenuItem
        '
        Me.FacsoftvToolStripMenuItem.Name = "FacsoftvToolStripMenuItem"
        Me.FacsoftvToolStripMenuItem.Size = New System.Drawing.Size(140, 22)
        Me.FacsoftvToolStripMenuItem.Text = "Facsoftv"
        '
        'EncargadosDelSistemaToolStripMenuItem
        '
        Me.EncargadosDelSistemaToolStripMenuItem.Name = "EncargadosDelSistemaToolStripMenuItem"
        Me.EncargadosDelSistemaToolStripMenuItem.Size = New System.Drawing.Size(335, 22)
        Me.EncargadosDelSistemaToolStripMenuItem.Text = "Encargados del Sistema"
        Me.EncargadosDelSistemaToolStripMenuItem.Visible = False
        '
        'PreciosDeArticulosDeInstalaciónToolStripMenuItem
        '
        Me.PreciosDeArticulosDeInstalaciónToolStripMenuItem.Name = "PreciosDeArticulosDeInstalaciónToolStripMenuItem"
        Me.PreciosDeArticulosDeInstalaciónToolStripMenuItem.Size = New System.Drawing.Size(335, 22)
        Me.PreciosDeArticulosDeInstalaciónToolStripMenuItem.Text = "Precios de Articulos de Instalación"
        '
        'BitacoraDelSistemaToolStripMenuItem
        '
        Me.BitacoraDelSistemaToolStripMenuItem.Name = "BitacoraDelSistemaToolStripMenuItem"
        Me.BitacoraDelSistemaToolStripMenuItem.Size = New System.Drawing.Size(335, 22)
        Me.BitacoraDelSistemaToolStripMenuItem.Text = "Bitacora del Sistema"
        '
        'ProcesamientoDeLlamadasToolStripMenuItem
        '
        Me.ProcesamientoDeLlamadasToolStripMenuItem.Enabled = False
        Me.ProcesamientoDeLlamadasToolStripMenuItem.Name = "ProcesamientoDeLlamadasToolStripMenuItem"
        Me.ProcesamientoDeLlamadasToolStripMenuItem.Size = New System.Drawing.Size(335, 22)
        Me.ProcesamientoDeLlamadasToolStripMenuItem.Text = "Llamadas procesadas"
        Me.ProcesamientoDeLlamadasToolStripMenuItem.Visible = False
        '
        'ProcesamientoDeCDRToolStripMenuItem
        '
        Me.ProcesamientoDeCDRToolStripMenuItem.Enabled = False
        Me.ProcesamientoDeCDRToolStripMenuItem.Name = "ProcesamientoDeCDRToolStripMenuItem"
        Me.ProcesamientoDeCDRToolStripMenuItem.Size = New System.Drawing.Size(335, 22)
        Me.ProcesamientoDeCDRToolStripMenuItem.Text = "CDR procesados"
        Me.ProcesamientoDeCDRToolStripMenuItem.Visible = False
        '
        'InterfazDigitalesXcriptToolStripMenuItem
        '
        Me.InterfazDigitalesXcriptToolStripMenuItem.Name = "InterfazDigitalesXcriptToolStripMenuItem"
        Me.InterfazDigitalesXcriptToolStripMenuItem.Size = New System.Drawing.Size(335, 22)
        Me.InterfazDigitalesXcriptToolStripMenuItem.Text = "Interfaz Digitales Xcript"
        '
        'SalirToolStripMenuItem
        '
        Me.SalirToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.SalirToolStripMenuItem.Name = "SalirToolStripMenuItem"
        Me.SalirToolStripMenuItem.Size = New System.Drawing.Size(56, 22)
        Me.SalirToolStripMenuItem.Text = "&Salir"
        '
        'PictureBox2
        '
        Me.PictureBox2.DataBindings.Add(New System.Windows.Forms.Binding("Image", Me.MUESTRAIMAGENBindingSource, "IMAGEN", True))
        Me.PictureBox2.Location = New System.Drawing.Point(48, 137)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(881, 466)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 3
        Me.PictureBox2.TabStop = False
        '
        'MUESTRAIMAGENBindingSource
        '
        Me.MUESTRAIMAGENBindingSource.DataMember = "MUESTRAIMAGEN"
        Me.MUESTRAIMAGENBindingSource.DataSource = Me.DataSetLidia
        '
        'DataSetLidia
        '
        Me.DataSetLidia.DataSetName = "DataSetLidia"
        Me.DataSetLidia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(924, 151)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(92, 53)
        Me.Button1.TabIndex = 4
        Me.Button1.TabStop = False
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(420, 651)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "Button2"
        Me.Button2.UseVisualStyleBackColor = True
        Me.Button2.Visible = False
        '
        'CMBLabel1
        '
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.Gray
        Me.CMBLabel1.Location = New System.Drawing.Point(48, 44)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(881, 37)
        Me.CMBLabel1.TabIndex = 6
        Me.CMBLabel1.Text = "Label1"
        Me.CMBLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BackgroundWorker1
        '
        Me.BackgroundWorker1.WorkerReportsProgress = True
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(160, 163)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(600, 122)
        Me.Panel1.TabIndex = 8
        Me.Panel1.Visible = False
        '
        'BackgroundWorker2
        '
        Me.BackgroundWorker2.WorkerReportsProgress = True
        '
        'CMBLabelSistema
        '
        Me.CMBLabelSistema.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabelSistema.ForeColor = System.Drawing.Color.Gray
        Me.CMBLabelSistema.Location = New System.Drawing.Point(43, 81)
        Me.CMBLabelSistema.Name = "CMBLabelSistema"
        Me.CMBLabelSistema.Size = New System.Drawing.Size(881, 37)
        Me.CMBLabelSistema.TabIndex = 8
        Me.CMBLabelSistema.Text = "Softv"
        Me.CMBLabelSistema.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'DataSetEdgarRev2
        '
        Me.DataSetEdgarRev2.DataSetName = "DataSetEdgarRev2"
        Me.DataSetEdgarRev2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DataSetarnoldo
        '
        Me.DataSetarnoldo.DataSetName = "DataSetarnoldo"
        Me.DataSetarnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DameClv_Session_ServiciosBindingSource
        '
        Me.DameClv_Session_ServiciosBindingSource.DataMember = "DameClv_Session_Servicios"
        Me.DameClv_Session_ServiciosBindingSource.DataSource = Me.DataSetarnoldo
        '
        'DameClv_Session_ServiciosTableAdapter
        '
        Me.DameClv_Session_ServiciosTableAdapter.ClearBeforeFill = True
        '
        'Valida_periodo_reportesBindingSource
        '
        Me.Valida_periodo_reportesBindingSource.DataMember = "Valida_periodo_reportes"
        Me.Valida_periodo_reportesBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Valida_periodo_reportesTableAdapter
        '
        Me.Valida_periodo_reportesTableAdapter.ClearBeforeFill = True
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Borrar_Tablas_Reporte_nuevoBindingSource
        '
        Me.Borrar_Tablas_Reporte_nuevoBindingSource.DataMember = "Borrar_Tablas_Reporte_nuevo"
        Me.Borrar_Tablas_Reporte_nuevoBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Borrar_Tablas_Reporte_nuevoTableAdapter
        '
        Me.Borrar_Tablas_Reporte_nuevoTableAdapter.ClearBeforeFill = True
        '
        'Borra_Separacion_ClientesBindingSource
        '
        Me.Borra_Separacion_ClientesBindingSource.DataMember = "Borra_Separacion_Clientes"
        Me.Borra_Separacion_ClientesBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Borra_Separacion_ClientesTableAdapter
        '
        Me.Borra_Separacion_ClientesTableAdapter.ClearBeforeFill = True
        '
        'Borra_temporales_trabajosBindingSource
        '
        Me.Borra_temporales_trabajosBindingSource.DataMember = "Borra_temporales_trabajos"
        Me.Borra_temporales_trabajosBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Borra_temporales_trabajosTableAdapter
        '
        Me.Borra_temporales_trabajosTableAdapter.ClearBeforeFill = True
        '
        'Procedimientosarnoldo4
        '
        Me.Procedimientosarnoldo4.DataSetName = "Procedimientosarnoldo4"
        Me.Procedimientosarnoldo4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BackgroundWorker3
        '
        '
        'MUESTRAIMAGENBindingSource1
        '
        Me.MUESTRAIMAGENBindingSource1.DataMember = "MUESTRAIMAGEN"
        '
        'DameTipoUsusarioBindingSource
        '
        Me.DameTipoUsusarioBindingSource.DataMember = "DameTipoUsusario"
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter5
        '
        Me.Muestra_ServiciosDigitalesTableAdapter5.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter6
        '
        Me.Muestra_ServiciosDigitalesTableAdapter6.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter7
        '
        Me.Muestra_ServiciosDigitalesTableAdapter7.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter8
        '
        Me.Muestra_ServiciosDigitalesTableAdapter8.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter9
        '
        Me.Muestra_ServiciosDigitalesTableAdapter9.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter10
        '
        Me.Muestra_ServiciosDigitalesTableAdapter10.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter11
        '
        Me.Muestra_ServiciosDigitalesTableAdapter11.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter12
        '
        Me.Muestra_ServiciosDigitalesTableAdapter12.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter13
        '
        Me.Muestra_ServiciosDigitalesTableAdapter13.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter14
        '
        Me.Muestra_ServiciosDigitalesTableAdapter14.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter15
        '
        Me.Muestra_ServiciosDigitalesTableAdapter15.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter16
        '
        Me.Muestra_ServiciosDigitalesTableAdapter16.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter17
        '
        Me.Muestra_ServiciosDigitalesTableAdapter17.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter18
        '
        Me.Muestra_ServiciosDigitalesTableAdapter18.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter19
        '
        Me.Muestra_ServiciosDigitalesTableAdapter19.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter20
        '
        Me.Muestra_ServiciosDigitalesTableAdapter20.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter21
        '
        Me.Muestra_ServiciosDigitalesTableAdapter21.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter22
        '
        Me.Muestra_ServiciosDigitalesTableAdapter22.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter23
        '
        Me.Muestra_ServiciosDigitalesTableAdapter23.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter24
        '
        Me.Muestra_ServiciosDigitalesTableAdapter24.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter25
        '
        Me.Muestra_ServiciosDigitalesTableAdapter25.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter26
        '
        Me.Muestra_ServiciosDigitalesTableAdapter26.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter27
        '
        Me.Muestra_ServiciosDigitalesTableAdapter27.ClearBeforeFill = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(50, 640)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(70, 16)
        Me.Label2.TabIndex = 20
        Me.Label2.Text = "Usuario: "
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(50, 610)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(254, 16)
        Me.Label1.TabIndex = 19
        Me.Label1.Text = "Versión: 3.0.0.41, Fecha: 28/06/2018"
        '
        'DameEspecifBindingSource
        '
        Me.DameEspecifBindingSource.DataMember = "DameEspecif"
        Me.DameEspecifBindingSource.DataSource = Me.DataSetLidia
        '
        'DameEspecifTableAdapter
        '
        Me.DameEspecifTableAdapter.ClearBeforeFill = True
        '
        'MUESTRAIMAGENTableAdapter
        '
        Me.MUESTRAIMAGENTableAdapter.ClearBeforeFill = True
        '
        'DameTipoUsusarioBindingSource1
        '
        Me.DameTipoUsusarioBindingSource1.DataMember = "DameTipoUsusario"
        Me.DameTipoUsusarioBindingSource1.DataSource = Me.DataSetLidia
        '
        'DameTipoUsusarioTableAdapter
        '
        Me.DameTipoUsusarioTableAdapter.ClearBeforeFill = True
        '
        'DamePermisosBindingSource
        '
        Me.DamePermisosBindingSource.DataMember = "DamePermisos"
        Me.DamePermisosBindingSource.DataSource = Me.DataSetLidia
        '
        'DamePermisosTableAdapter
        '
        Me.DamePermisosTableAdapter.ClearBeforeFill = True
        '
        'ALTASMENUSBindingSource
        '
        Me.ALTASMENUSBindingSource.DataMember = "ALTASMENUS"
        Me.ALTASMENUSBindingSource.DataSource = Me.DataSetLidia
        '
        'ALTASMENUSTableAdapter
        '
        Me.ALTASMENUSTableAdapter.ClearBeforeFill = True
        '
        'ALTASformsBindingSource
        '
        Me.ALTASformsBindingSource.DataMember = "ALTASforms"
        Me.ALTASformsBindingSource.DataSource = Me.DataSetLidia
        '
        'ALTASformsTableAdapter
        '
        Me.ALTASformsTableAdapter.ClearBeforeFill = True
        '
        'FrmMiMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1028, 713)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.CMBLabelSistema)
        Me.Controls.Add(CMBNombreLabel)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "FrmMiMenu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Menú Principal"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAIMAGENBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameClv_Session_ServiciosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Valida_periodo_reportesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Borrar_Tablas_Reporte_nuevoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Borra_Separacion_ClientesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Borra_temporales_trabajosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAIMAGENBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameTipoUsusarioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameEspecifBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameTipoUsusarioBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DamePermisosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ALTASMENUSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ALTASformsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents CatálogosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CatálogosDeClientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientesToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CatalogoÁreaTécnicaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ÁreasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClasificaciónTécnicaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ServiciosAlClienteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TecnicosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CatálogoDeGeneralesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TiposDeServicioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ServiciosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CableModemsYAparatosDigitalesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ColoniasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TiposDeColoniasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CallesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CiudadesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PromocionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SucursalesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BancosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UsuariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CajasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MotivosDeCancelaciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CatálogoDeVentasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TiposDePromotoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PromotoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TablasDeComisionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TablaDePuntosPorServicioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TablaDeEquivalenciaDePuntosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents JefeDeGrupoDeTvToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents JefeDeVentasDeTvToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VendedorDeTvToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents JefeDeVentasDeInternetToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RecuperadoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SeriesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProcesosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProcesosDeServicioPremiumToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OrdenesDeServicioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents QuejasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents QuejasToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AgendaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AtenciónTelefónicaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReportesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EstadosDeCarteraToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientesToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReportesVariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AreaTécnicaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AnálisisDePenetraciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VentasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GeneralesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GeneralesDelSistemaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InterfasCablemodemsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InterfasDecodificadoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GeneralesDeBancosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SalirToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents BancosToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OrdenesDeServicioToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents QuejasToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SectoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LlamadasTelefónicasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GeneralesDeInterfacesDigitalesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents GeneralesDeOXXOToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConfiguracionDelSistemaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MotivosDeCancelaciónFacturasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MotivosDeReImpresiónFacturasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GeneralesDeInterfacesInternetToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RangosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrecioDeComisionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CalcularComisionesPorVendedorToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GeneralesReportesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SoftvToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FacsoftvToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EncargadosDelSistemaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrimerPeriodoToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SegundoPeriodoToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataSetEdgarRev2 As sofTV.DataSetEdgarRev2
    Friend WithEvents AvisosToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents ProcesoDeDesconexiónTemporalPorContratoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContratoMaestroToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ActivaciónPaqueteDePruebaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PreciosDeArticulosDeInstalaciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientesConAdeudoDeMaterialToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BitácoraDePruebasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProcesoDeReactivaciónDeContratoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DepuraciónDeÓrdenesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CambioDeServicioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientesVariosMezcladosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataSetarnoldo As sofTV.DataSetarnoldo
    Friend WithEvents DameClv_Session_ServiciosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameClv_Session_ServiciosTableAdapter As sofTV.DataSetarnoldoTableAdapters.DameClv_Session_ServiciosTableAdapter
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents Borrar_Tablas_Reporte_nuevoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Borrar_Tablas_Reporte_nuevoTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Borrar_Tablas_Reporte_nuevoTableAdapter
    Friend WithEvents Valida_periodo_reportesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Valida_periodo_reportesTableAdapter As sofTV.DataSetarnoldoTableAdapters.Valida_periodo_reportesTableAdapter
    Friend WithEvents ReporteDePaquetesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Borra_Separacion_ClientesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Borra_Separacion_ClientesTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Borra_Separacion_ClientesTableAdapter
    Friend WithEvents ProcesoDeCierreDeMesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrimerPeriodoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Borra_temporales_trabajosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Borra_temporales_trabajosTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Borra_temporales_trabajosTableAdapter
    Friend WithEvents ListadoDeActividadesDelTécnicoToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ResumenDeVentasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MesajesInstantaneosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CorreoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ResetearAparatosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PelículasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ResumenVendedoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GráficasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientesVariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClienteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MotivosDeLlamadaAtenciónAClientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PruebaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AtenciónTelefónicaToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CambioDeClienteASoloInternetToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CambioDeClienteAClienteNormalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents AgendaDeActividadesDelTécnicoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PolizaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProgramacionesDeMesnajesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DesgloceDeMensualidadesAdelantadasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContratoForzosoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MedidoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ServiciosDeVentasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CarteraToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MedidoresToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GrupoDeVentasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BitacoraDelSistemaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AgendaToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IndividualesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Medidores2ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CargosEspecialesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DescuentosComboToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TelefoníaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NumérosDeTeléfonoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PaisesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TipoPaquetesAdicionalesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PaquetesAdicionalesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ServiciosDigitalesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CMBLabelSistema As System.Windows.Forms.Label
    Friend WithEvents ImporteDeMensualidadesAdelantadasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Procedimientosarnoldo4 As sofTV.Procedimientosarnoldo4
    Friend WithEvents PPVToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EstadoDeCuentaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteClientesConComboToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Private WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents BackgroundWorker2 As System.ComponentModel.BackgroundWorker
    Friend WithEvents CarteraEjecutivaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ServiciosContratadosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeClientesPorStausConAdeudoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CancelacionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MarcacionesEspecialesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EquiposALaVentaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BitácoraDeCorreosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CódigosMéxicoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VentasTotalesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DesconexionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ServicioBasicoYCanalesPremiumToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InternetToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PuntosDeAntigüedadToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InterfazCablemodemsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InterfazDigitalesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TarifasEspecialesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RecuperaciónDeCarteraToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CortesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GeneralesProsaBancomerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReprocesamientoPorClienteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProcesamientoDeLlamadasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProcesamientoDeCDRToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TelefoníaToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImpresiónDeEstadosDeCuentaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AuxToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReferenciasBancariasToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImpresiónDeEstadosDeCuentaPágina2NToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HotelesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OrdenesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GerencialToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CancelacionDeVentasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MUESTRAIMAGENBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents DameTipoUsusarioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DataSetLidia As sofTV.DataSetLidia
    Friend WithEvents DameEspecifBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameEspecifTableAdapter As sofTV.DataSetLidiaTableAdapters.DameEspecifTableAdapter
    Friend WithEvents MUESTRAIMAGENBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAIMAGENTableAdapter As sofTV.DataSetLidiaTableAdapters.MUESTRAIMAGENTableAdapter
    Friend WithEvents DameTipoUsusarioBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents DameTipoUsusarioTableAdapter As sofTV.DataSetLidiaTableAdapters.DameTipoUsusarioTableAdapter
    Friend WithEvents DamePermisosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DamePermisosTableAdapter As sofTV.DataSetLidiaTableAdapters.DamePermisosTableAdapter
    Friend WithEvents ALTASMENUSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ALTASMENUSTableAdapter As sofTV.DataSetLidiaTableAdapters.ALTASMENUSTableAdapter
    Friend WithEvents ALTASformsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ALTASformsTableAdapter As sofTV.DataSetLidiaTableAdapters.ALTASformsTableAdapter
    Friend WithEvents ReporteDePermanenciaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ResumenVentasPorStatusToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReseteoMasivoDeAparatosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EnviarSMSMasivoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ValorDeCarteraToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MaterialesUtilizadosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TercerPeriodoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BackgroundWorker3 As System.ComponentModel.BackgroundWorker
    Friend WithEvents CatálogoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TipoServicioEstadoDeCuentaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TiposDeClientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SubTiposDeClientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CortesiasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TiemposPorServicioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TiempoDeQuejasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CostoAparatosRentaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TablaDeProductividadToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteDeProductividadToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RelaciónDePuntosImporteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RecontrataciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AtencionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RecontrataciónToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EstablecerComisionesPorContratosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MetaPorVendedorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ComicionesDeVendedoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ServiciosVendidosPorSemanaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NivelesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ComisiónDeNivelesPorServicioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReimpresionDeFoliosParaVendedoresToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RegresaAparatosAlAlmacenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VisitasAlClienteDelVendedorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteDeClientesSuspendidosQueYaPagaronToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TrabajosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RelaciónDeTrabajosYRetiroDeAparatosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatusDeLosFoliosDeLosVendedoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents PrioridadesDeQuejasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeEquiposRecibidosPorCajerasosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents PolizaToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GruposConceptoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConceptosIngresoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DesgloseDeIngresosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GrupoDeIngresosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresosDesglozadosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CantidadDeClientesPorMezclaDeServiciosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DepartamentosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GeneraciónMasivaDeÓrdenesDeCambioDeAparatoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteDeControlDeCambioDeCajaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItemControlHoras As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeInstalacionesDeExtecionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClasificaciónProblemasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteEjecutivoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CompañíasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter5 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents TiposTécnicosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter6 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents RelaciónDeOrdenesEjecutadasYCapturadasDelTécnicoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteInterfazDigitalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TipoInstalacionesAClientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteDeClientesPorTipoDeInstalaciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter7 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents ReporteDeRecuperaciónDeCajasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter8 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents ResumenMensualDeRecuperaciónDeCajasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteDeRenovaciónDeAnualidadesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter9 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter10 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter11 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter12 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter13 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents CatalogoMotivoPagoNoRealizadoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter14 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents TarifasEconopackToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter15 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents ReporteInstalacionesVsPromocionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InterfazDigitalesXcriptToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter16 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter17 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter18 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter19 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter20 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter21 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter22 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter23 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents ReporteClientesInternetToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter24 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter25 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter26 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter27 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Catalogos33ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProductosSatToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    'Friend WithEvents CatálogoTelefoníaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    ' Friend WithEvents CatalogoPaisesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
