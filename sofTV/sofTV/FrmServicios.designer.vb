﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmServicios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_ServicioLabel As System.Windows.Forms.Label
        Dim Clv_TipSerLabel As System.Windows.Forms.Label
        Dim DescripcionLabel As System.Windows.Forms.Label
        Dim Clv_TxtLabel As System.Windows.Forms.Label
        Dim PrecioLabel As System.Windows.Forms.Label
        Dim CLV_SERVICIOLabel1 As System.Windows.Forms.Label
        Dim Punto_Pronto_PagoLabel As System.Windows.Forms.Label
        Dim Puntos11Label As System.Windows.Forms.Label
        Dim Puntos6Label As System.Windows.Forms.Label
        Dim Puntos3Label As System.Windows.Forms.Label
        Dim CLAVELabel As System.Windows.Forms.Label
        Dim CLV_LLAVELabel2 As System.Windows.Forms.Label
        Dim Genera_OrdenLabel1 As System.Windows.Forms.Label
        Dim VigenteLabel As System.Windows.Forms.Label
        Dim Aplica_ComisionLabel As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim Periodo_FinalLabel As System.Windows.Forms.Label
        Dim Periodo_InicialLabel As System.Windows.Forms.Label
        Dim DIA_INICIALLabel As System.Windows.Forms.Label
        Dim DIA_FINALLabel As System.Windows.Forms.Label
        Dim Precio_AdicionalLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Clv_TipoPromocionLabel As System.Windows.Forms.Label
        Dim Numero_CortesiasLabel As System.Windows.Forms.Label
        Dim MensDescuentoNetLabel As System.Windows.Forms.Label
        Dim Label8 As System.Windows.Forms.Label
        Dim ContDescuentoNetLabel As System.Windows.Forms.Label
        Dim Label10 As System.Windows.Forms.Label
        Dim Label12 As System.Windows.Forms.Label
        Dim Label13 As System.Windows.Forms.Label
        Dim lblCtaContable As System.Windows.Forms.Label
        Dim lblPosicion As System.Windows.Forms.Label
        Dim Label15 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmServicios))
        Me.PRECIOLabel1 = New System.Windows.Forms.Label()
        Me.CMBAplicanComLabel = New System.Windows.Forms.Label()
        Me.CMBSale_en_CarteraLabel = New System.Windows.Forms.Label()
        Me.CMBGenera_OrdenLabel = New System.Windows.Forms.Label()
        Me.CMBEs_PrincipalLabel = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cbClaveProducto = New System.Windows.Forms.ComboBox()
        Me.CONRelTrabajosNoCobroMensualBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEDGAR = New sofTV.DataSetEDGAR()
        Me.pnlContable = New System.Windows.Forms.Panel()
        Me.txtCuentaContable = New System.Windows.Forms.TextBox()
        Me.txtPosicion = New System.Windows.Forms.TextBox()
        Me.PnlClaveEquivalente = New System.Windows.Forms.Panel()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.CONSULTAClv_EquiBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLidia = New sofTV.DataSetLidia()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.HDCheckBox = New System.Windows.Forms.CheckBox()
        Me.AplicaIEPSCheckBox = New System.Windows.Forms.CheckBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.DescripcionTextBox = New System.Windows.Forms.TextBox()
        Me.CONSERVICIOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.CMBLabel10 = New System.Windows.Forms.Label()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.Panel21 = New System.Windows.Forms.Panel()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.Panel15 = New System.Windows.Forms.Panel()
        Me.ComboBox4 = New System.Windows.Forms.ComboBox()
        Me.MUESTRATIPOCLIENTESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Clv_TipoClienteTextBox = New System.Windows.Forms.TextBox()
        Me.CONREL_TARIFADOS_SERVICIOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Panel19 = New System.Windows.Forms.Panel()
        Me.Panel20 = New System.Windows.Forms.Panel()
        Me.Dame_ContratacionParcialDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Dame_ContratacionParcialBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ComboBox9 = New System.Windows.Forms.ComboBox()
        Me.Dame_PagosParcialesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Panel14 = New System.Windows.Forms.Panel()
        Me.Precio_InalambricoTextBox = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.MuestraTipoPromocionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CMBTextBox6 = New System.Windows.Forms.TextBox()
        Me.Clv_TipoPromocionTextBox = New System.Windows.Forms.TextBox()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.IEPS2CheckBox = New System.Windows.Forms.CheckBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.MUESTRASOLOTARIFADOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel11adicional2 = New System.Windows.Forms.Panel()
        Me.tbPrecioAdic = New System.Windows.Forms.TextBox()
        Me.LabelPrecioAdic = New System.Windows.Forms.Label()
        Me.Precio_AdicionalTextBox = New System.Windows.Forms.TextBox()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.tbPrecio = New System.Windows.Forms.TextBox()
        Me.LabelPrecio = New System.Windows.Forms.Label()
        Me.Panel22 = New System.Windows.Forms.Panel()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.Panel13 = New System.Windows.Forms.Panel()
        Me.Numero_CortesiasTextBox = New System.Windows.Forms.TextBox()
        Me.CMBTextBox4 = New System.Windows.Forms.TextBox()
        Me.PRECIOTextBox1 = New System.Windows.Forms.TextBox()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Periodo_InicialDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.Periodo_FinalDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.CMBTextBox1 = New System.Windows.Forms.TextBox()
        Me.DIA_INICIALNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.DIA_FINALNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Porcetaje_DescuentoTextBox = New System.Windows.Forms.TextBox()
        Me.CMBTextBox2 = New System.Windows.Forms.TextBox()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.BrincaMes_Label = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Se_Cobra_ProporcionalCheckBox = New System.Windows.Forms.CheckBox()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.Clv_TrabajoTextBox = New System.Windows.Forms.TextBox()
        Me.ComboBox3 = New System.Windows.Forms.ComboBox()
        Me.MUESTRATRABAJOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CMBTextBox5 = New System.Windows.Forms.TextBox()
        Me.BRINCA_MESCheckBox = New System.Windows.Forms.CheckBox()
        Me.Aplica_ComisionCheckBox = New System.Windows.Forms.CheckBox()
        Me.Genera_OrdenCheckBox1 = New System.Windows.Forms.CheckBox()
        Me.VigenteCheckBox = New System.Windows.Forms.CheckBox()
        Me.CLV_LLAVETextBox2 = New System.Windows.Forms.TextBox()
        Me.CLAVETextBox = New System.Windows.Forms.TextBox()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.CLVLLAVEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CONCEPTODataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PRECIODataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PrecioAdicionalDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PeriodoInicialDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PeriodoFinalDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DIAINICIALDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DIAFINALDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PorcetajeDescuentoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BRINCAMESDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.AplicaComisionDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.GeneraOrdenDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Promocion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TRABAJO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Numero_Cortesias = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Precio_Inalambrico = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BUSCAREL_TARIFADOS_SERVICIOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton4 = New System.Windows.Forms.ToolStripButton()
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton3 = New System.Windows.Forms.ToolStripButton()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.PanelPagoAdelantado = New System.Windows.Forms.Panel()
        Me.Panel16 = New System.Windows.Forms.Panel()
        Me.CheckBox3 = New System.Windows.Forms.CheckBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.ComboBox8 = New System.Windows.Forms.ComboBox()
        Me.MUESTRATIPOCLIENTES2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEdgarRev2 = New sofTV.DataSetEdgarRev2()
        Me.Cancelar1 = New System.Windows.Forms.Button()
        Me.CMBTextBox7 = New System.Windows.Forms.TextBox()
        Me.Eliminar1 = New System.Windows.Forms.Button()
        Me.Panel17 = New System.Windows.Forms.Panel()
        Me.CONGeneralDescuentoNetDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CONGeneralDescuentoNetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric2 = New sofTV.DataSetEric2()
        Me.Clv_Servicio_Con_AplicaTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_TipSerTextBox1 = New System.Windows.Forms.TextBox()
        Me.Clv_ServicioTextBox2 = New System.Windows.Forms.TextBox()
        Me.PanelCapDesNet = New System.Windows.Forms.Panel()
        Me.PuntosTextBox = New System.Windows.Forms.TextBox()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.MensDescuentoNetTextBox = New System.Windows.Forms.TextBox()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.ContDescuentoNetTextBox = New System.Windows.Forms.TextBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.ConceptoComboBox = New System.Windows.Forms.ComboBox()
        Me.DescripcionComboBox = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.ComboBox7 = New System.Windows.Forms.ComboBox()
        Me.MuesteServiciosDescuentoNetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ComboBox6 = New System.Windows.Forms.ComboBox()
        Me.MuestraTipSerPrincipalDescuentNetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Guardar1 = New System.Windows.Forms.Button()
        Me.Agregar1 = New System.Windows.Forms.Button()
        Me.Modificar1 = New System.Windows.Forms.Button()
        Me.CMBTextBox3 = New System.Windows.Forms.TextBox()
        Me.Punto_Pronto_PagoNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.Puntos3NumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.Puntos6NumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.Puntos11NumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.Clv_TxtTextBox = New System.Windows.Forms.TextBox()
        Me.Es_PrincipalCheckBox = New System.Windows.Forms.CheckBox()
        Me.Sale_en_CarteraCheckBox1 = New System.Windows.Forms.CheckBox()
        Me.CMBTextBox28 = New System.Windows.Forms.TextBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel18 = New System.Windows.Forms.Panel()
        Me.ComboBox5 = New System.Windows.Forms.ComboBox()
        Me.MUESTRATRABAJOSNOCOBROMENSUALBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sale_en_CarteraCheckBox = New System.Windows.Forms.CheckBox()
        Me.Genera_OrdenCheckBox = New System.Windows.Forms.CheckBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.PrecioTextBox = New System.Windows.Forms.TextBox()
        Me.CLV_SERVICIOTextBox1 = New System.Windows.Forms.TextBox()
        Me.CLV_LLAVETextBox = New System.Windows.Forms.TextBox()
        Me.CONSERVICIOSBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.CONSERVICIOSBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Clv_ServicioTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_TipSerTextBox = New System.Windows.Forms.TextBox()
        Me.BUSCAPuntosPagoAdelantadoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ValidaPeriodosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button5 = New System.Windows.Forms.Button()
        Me.CONSERVICIOSTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONSERVICIOSTableAdapter()
        Me.MUESTRASOLOTARIFADOSTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRASOLOTARIFADOSTableAdapter()
        Me.CONREL_TARIFADOS_SERVICIOSTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONREL_TARIFADOS_SERVICIOSTableAdapter()
        Me.BUSCAREL_TARIFADOS_SERVICIOSTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.BUSCAREL_TARIFADOS_SERVICIOSTableAdapter()
        Me.ValidaPeriodosTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.ValidaPeriodosTableAdapter()
        Me.MODREL_TARIFADOS_SERVICIOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MODREL_TARIFADOS_SERVICIOSTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MODREL_TARIFADOS_SERVICIOSTableAdapter()
        Me.BUSCAPuntos_Pago_AdelantadoTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.BUSCAPuntos_Pago_AdelantadoTableAdapter()
        Me.NUEPuntos_Pago_AdelantadoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NUEPuntos_Pago_AdelantadoTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.NUEPuntos_Pago_AdelantadoTableAdapter()
        Me.MuestraTipoPromocionTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MuestraTipoPromocionTableAdapter()
        Me.MUESTRATRABAJOSTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRATRABAJOSTableAdapter()
        Me.MUESTRA_TIPOCLIENTESTableAdapter = New sofTV.DataSetEDGARTableAdapters.MUESTRA_TIPOCLIENTESTableAdapter()
        Me.MUESTRATRABAJOS_NOCOBROMENSUALTableAdapter = New sofTV.DataSetEDGARTableAdapters.MUESTRATRABAJOS_NOCOBROMENSUALTableAdapter()
        Me.CONRel_Trabajos_NoCobroMensualTableAdapter = New sofTV.DataSetEDGARTableAdapters.CONRel_Trabajos_NoCobroMensualTableAdapter()
        Me.GUARDARel_Trabajos_NoCobroMensualBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GUARDARel_Trabajos_NoCobroMensualTableAdapter = New sofTV.DataSetEDGARTableAdapters.GUARDARel_Trabajos_NoCobroMensualTableAdapter()
        Me.BORRel_Trabajos_NoCobroMensualBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BORRel_Trabajos_NoCobroMensualTableAdapter = New sofTV.DataSetEDGARTableAdapters.BORRel_Trabajos_NoCobroMensualTableAdapter()
        Me.MuestraTipSerPrincipalDescuentNetTableAdapter = New sofTV.DataSetEDGARTableAdapters.MuestraTipSerPrincipalDescuentNetTableAdapter()
        Me.MuesteServiciosDescuentoNetTableAdapter = New sofTV.DataSetEDGARTableAdapters.MuesteServiciosDescuentoNetTableAdapter()
        Me.BORGeneralDescuentoNetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BORGeneralDescuentoNetTableAdapter = New sofTV.DataSetEDGARTableAdapters.BORGeneralDescuentoNetTableAdapter()
        Me.Valida_Guarda_GeneralDescuentoNetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Valida_Guarda_GeneralDescuentoNetTableAdapter = New sofTV.DataSetEDGARTableAdapters.Valida_Guarda_GeneralDescuentoNetTableAdapter()
        Me.MUESTRA_TIPOCLIENTES_2TableAdapter = New sofTV.DataSetEdgarRev2TableAdapters.MUESTRA_TIPOCLIENTES_2TableAdapter()
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.BorAplicaSoloInternetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorAplicaSoloInternetTableAdapter = New sofTV.DataSetEricTableAdapters.BorAplicaSoloInternetTableAdapter()
        Me.NueAplicaSoloInternetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NueAplicaSoloInternetTableAdapter = New sofTV.DataSetEricTableAdapters.NueAplicaSoloInternetTableAdapter()
        Me.ValidaAplicaSoloInternetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ValidaAplicaSoloInternetTableAdapter = New sofTV.DataSetEricTableAdapters.ValidaAplicaSoloInternetTableAdapter()
        Me.Dame_PagosParcialesTableAdapter = New sofTV.DataSetLidiaTableAdapters.Dame_PagosParcialesTableAdapter()
        Me.Dame_ContratacionParcialTableAdapter = New sofTV.DataSetLidiaTableAdapters.Dame_ContratacionParcialTableAdapter()
        Me.NUEVOClv_EquiBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NUEVOClv_EquiTableAdapter = New sofTV.DataSetLidiaTableAdapters.NUEVOClv_EquiTableAdapter()
        Me.CONSULTAClv_EquiTableAdapter = New sofTV.DataSetLidiaTableAdapters.CONSULTAClv_EquiTableAdapter()
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2()
        Me.Valida_borra_servicioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Valida_borra_servicioTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Valida_borra_servicioTableAdapter()
        Me.CONGeneralDescuentoNetTableAdapter = New sofTV.DataSetEric2TableAdapters.CONGeneralDescuentoNetTableAdapter()
        Me.Guarda_GeneralDescuentoNetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Guarda_GeneralDescuentoNetTableAdapter = New sofTV.DataSetEric2TableAdapters.Guarda_GeneralDescuentoNetTableAdapter()
        Me.NUEREL_TARIFADOS_SERVICIOSALLBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NUEREL_TARIFADOS_SERVICIOSALLTableAdapter = New sofTV.DataSetEric2TableAdapters.NUEREL_TARIFADOS_SERVICIOSALLTableAdapter()
        Me.MODREL_TARIFADOS_SERVICIOSALLBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MODREL_TARIFADOS_SERVICIOSALLTableAdapter = New sofTV.DataSetEric2TableAdapters.MODREL_TARIFADOS_SERVICIOSALLTableAdapter()
        Me.Guarda_GeneralDescuentoNetAllBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Guarda_GeneralDescuentoNetAllTableAdapter = New sofTV.DataSetEric2TableAdapters.Guarda_GeneralDescuentoNetAllTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter5 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Clv_ServicioLabel = New System.Windows.Forms.Label()
        Clv_TipSerLabel = New System.Windows.Forms.Label()
        DescripcionLabel = New System.Windows.Forms.Label()
        Clv_TxtLabel = New System.Windows.Forms.Label()
        PrecioLabel = New System.Windows.Forms.Label()
        CLV_SERVICIOLabel1 = New System.Windows.Forms.Label()
        Punto_Pronto_PagoLabel = New System.Windows.Forms.Label()
        Puntos11Label = New System.Windows.Forms.Label()
        Puntos6Label = New System.Windows.Forms.Label()
        Puntos3Label = New System.Windows.Forms.Label()
        CLAVELabel = New System.Windows.Forms.Label()
        CLV_LLAVELabel2 = New System.Windows.Forms.Label()
        Genera_OrdenLabel1 = New System.Windows.Forms.Label()
        VigenteLabel = New System.Windows.Forms.Label()
        Aplica_ComisionLabel = New System.Windows.Forms.Label()
        Label4 = New System.Windows.Forms.Label()
        Periodo_FinalLabel = New System.Windows.Forms.Label()
        Periodo_InicialLabel = New System.Windows.Forms.Label()
        DIA_INICIALLabel = New System.Windows.Forms.Label()
        DIA_FINALLabel = New System.Windows.Forms.Label()
        Precio_AdicionalLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Clv_TipoPromocionLabel = New System.Windows.Forms.Label()
        Numero_CortesiasLabel = New System.Windows.Forms.Label()
        MensDescuentoNetLabel = New System.Windows.Forms.Label()
        Label8 = New System.Windows.Forms.Label()
        ContDescuentoNetLabel = New System.Windows.Forms.Label()
        Label10 = New System.Windows.Forms.Label()
        Label12 = New System.Windows.Forms.Label()
        Label13 = New System.Windows.Forms.Label()
        lblCtaContable = New System.Windows.Forms.Label()
        lblPosicion = New System.Windows.Forms.Label()
        Label15 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.CONRelTrabajosNoCobroMensualBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlContable.SuspendLayout()
        Me.PnlClaveEquivalente.SuspendLayout()
        CType(Me.CONSULTAClv_EquiBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONSERVICIOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.Panel21.SuspendLayout()
        Me.Panel15.SuspendLayout()
        CType(Me.MUESTRATIPOCLIENTESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONREL_TARIFADOS_SERVICIOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        Me.Panel19.SuspendLayout()
        Me.Panel20.SuspendLayout()
        CType(Me.Dame_ContratacionParcialDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_ContratacionParcialBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_PagosParcialesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel14.SuspendLayout()
        Me.Panel11.SuspendLayout()
        CType(Me.MuestraTipoPromocionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel9.SuspendLayout()
        CType(Me.MUESTRASOLOTARIFADOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel11adicional2.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel22.SuspendLayout()
        Me.Panel13.SuspendLayout()
        Me.Panel7.SuspendLayout()
        CType(Me.DIA_INICIALNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DIA_FINALNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel6.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel12.SuspendLayout()
        CType(Me.MUESTRATRABAJOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel10.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BUSCAREL_TARIFADOS_SERVICIOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.PanelPagoAdelantado.SuspendLayout()
        Me.Panel16.SuspendLayout()
        CType(Me.MUESTRATIPOCLIENTES2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel17.SuspendLayout()
        CType(Me.CONGeneralDescuentoNetDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONGeneralDescuentoNetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelCapDesNet.SuspendLayout()
        CType(Me.MuesteServiciosDescuentoNetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTipSerPrincipalDescuentNetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Punto_Pronto_PagoNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Puntos3NumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Puntos6NumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Puntos11NumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.Panel18.SuspendLayout()
        CType(Me.MUESTRATRABAJOSNOCOBROMENSUALBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.CONSERVICIOSBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONSERVICIOSBindingNavigator.SuspendLayout()
        CType(Me.BUSCAPuntosPagoAdelantadoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ValidaPeriodosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MODREL_TARIFADOS_SERVICIOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NUEPuntos_Pago_AdelantadoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GUARDARel_Trabajos_NoCobroMensualBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BORRel_Trabajos_NoCobroMensualBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BORGeneralDescuentoNetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Valida_Guarda_GeneralDescuentoNetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorAplicaSoloInternetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NueAplicaSoloInternetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ValidaAplicaSoloInternetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NUEVOClv_EquiBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Valida_borra_servicioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Guarda_GeneralDescuentoNetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NUEREL_TARIFADOS_SERVICIOSALLBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MODREL_TARIFADOS_SERVICIOSALLBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Guarda_GeneralDescuentoNetAllBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Clv_ServicioLabel
        '
        Clv_ServicioLabel.AutoSize = True
        Clv_ServicioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_ServicioLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_ServicioLabel.Location = New System.Drawing.Point(339, 35)
        Clv_ServicioLabel.Name = "Clv_ServicioLabel"
        Clv_ServicioLabel.Size = New System.Drawing.Size(85, 15)
        Clv_ServicioLabel.TabIndex = 0
        Clv_ServicioLabel.Text = "Clv Servicio:"
        Clv_ServicioLabel.Visible = False
        '
        'Clv_TipSerLabel
        '
        Clv_TipSerLabel.AutoSize = True
        Clv_TipSerLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_TipSerLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_TipSerLabel.Location = New System.Drawing.Point(240, 34)
        Clv_TipSerLabel.Name = "Clv_TipSerLabel"
        Clv_TipSerLabel.Size = New System.Drawing.Size(80, 15)
        Clv_TipSerLabel.TabIndex = 2
        Clv_TipSerLabel.Text = "Clv Tip Ser:"
        Clv_TipSerLabel.Visible = False
        '
        'DescripcionLabel
        '
        DescripcionLabel.AutoSize = True
        DescripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescripcionLabel.ForeColor = System.Drawing.Color.LightSlateGray
        DescripcionLabel.Location = New System.Drawing.Point(33, 63)
        DescripcionLabel.Name = "DescripcionLabel"
        DescripcionLabel.Size = New System.Drawing.Size(87, 15)
        DescripcionLabel.TabIndex = 4
        DescripcionLabel.Text = "Descripción:"
        '
        'Clv_TxtLabel
        '
        Clv_TxtLabel.AutoSize = True
        Clv_TxtLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_TxtLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_TxtLabel.Location = New System.Drawing.Point(70, 34)
        Clv_TxtLabel.Name = "Clv_TxtLabel"
        Clv_TxtLabel.Size = New System.Drawing.Size(50, 15)
        Clv_TxtLabel.TabIndex = 6
        Clv_TxtLabel.Text = "Clave :"
        '
        'PrecioLabel
        '
        PrecioLabel.AutoSize = True
        PrecioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PrecioLabel.ForeColor = System.Drawing.Color.LightSlateGray
        PrecioLabel.Location = New System.Drawing.Point(7, 9)
        PrecioLabel.Name = "PrecioLabel"
        PrecioLabel.Size = New System.Drawing.Size(52, 15)
        PrecioLabel.TabIndex = 300
        PrecioLabel.Text = "Precio:"
        '
        'CLV_SERVICIOLabel1
        '
        CLV_SERVICIOLabel1.AutoSize = True
        CLV_SERVICIOLabel1.Location = New System.Drawing.Point(341, 125)
        CLV_SERVICIOLabel1.Name = "CLV_SERVICIOLabel1"
        CLV_SERVICIOLabel1.Size = New System.Drawing.Size(83, 13)
        CLV_SERVICIOLabel1.TabIndex = 3
        CLV_SERVICIOLabel1.Text = "CLV SERVICIO:"
        CLV_SERVICIOLabel1.Visible = False
        '
        'Punto_Pronto_PagoLabel
        '
        Punto_Pronto_PagoLabel.AutoSize = True
        Punto_Pronto_PagoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Punto_Pronto_PagoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Punto_Pronto_PagoLabel.Location = New System.Drawing.Point(42, 35)
        Punto_Pronto_PagoLabel.Name = "Punto_Pronto_PagoLabel"
        Punto_Pronto_PagoLabel.Size = New System.Drawing.Size(131, 15)
        Punto_Pronto_PagoLabel.TabIndex = 611
        Punto_Pronto_PagoLabel.Text = "Punto Pronto Pago:"
        '
        'Puntos11Label
        '
        Puntos11Label.AutoSize = True
        Puntos11Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Puntos11Label.ForeColor = System.Drawing.Color.LightSlateGray
        Puntos11Label.Location = New System.Drawing.Point(10, 116)
        Puntos11Label.Name = "Puntos11Label"
        Puntos11Label.Size = New System.Drawing.Size(164, 15)
        Puntos11Label.TabIndex = 604
        Puntos11Label.Text = "Puntos 12 o Mas Meses:"
        '
        'Puntos6Label
        '
        Puntos6Label.AutoSize = True
        Puntos6Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Puntos6Label.ForeColor = System.Drawing.Color.LightSlateGray
        Puntos6Label.Location = New System.Drawing.Point(25, 91)
        Puntos6Label.Name = "Puntos6Label"
        Puntos6Label.Size = New System.Drawing.Size(149, 15)
        Puntos6Label.TabIndex = 603
        Puntos6Label.Text = "Puntos 6 a 11 Meses :"
        '
        'Puntos3Label
        '
        Puntos3Label.AutoSize = True
        Puntos3Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Puntos3Label.ForeColor = System.Drawing.Color.LightSlateGray
        Puntos3Label.Location = New System.Drawing.Point(37, 64)
        Puntos3Label.Name = "Puntos3Label"
        Puntos3Label.Size = New System.Drawing.Size(137, 15)
        Puntos3Label.TabIndex = 602
        Puntos3Label.Text = "Puntos 3 a 5 Meses:"
        '
        'CLAVELabel
        '
        CLAVELabel.AutoSize = True
        CLAVELabel.Location = New System.Drawing.Point(12, 435)
        CLAVELabel.Name = "CLAVELabel"
        CLAVELabel.Size = New System.Drawing.Size(44, 13)
        CLAVELabel.TabIndex = 5
        CLAVELabel.Text = "CLAVE:"
        CLAVELabel.Visible = False
        '
        'CLV_LLAVELabel2
        '
        CLV_LLAVELabel2.AutoSize = True
        CLV_LLAVELabel2.BackColor = System.Drawing.Color.WhiteSmoke
        CLV_LLAVELabel2.ForeColor = System.Drawing.Color.WhiteSmoke
        CLV_LLAVELabel2.Location = New System.Drawing.Point(142, 30)
        CLV_LLAVELabel2.Name = "CLV_LLAVELabel2"
        CLV_LLAVELabel2.Size = New System.Drawing.Size(66, 13)
        CLV_LLAVELabel2.TabIndex = 619
        CLV_LLAVELabel2.Text = "CLV LLAVE:"
        '
        'Genera_OrdenLabel1
        '
        Genera_OrdenLabel1.AutoSize = True
        Genera_OrdenLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Genera_OrdenLabel1.ForeColor = System.Drawing.Color.DarkRed
        Genera_OrdenLabel1.Location = New System.Drawing.Point(25, 78)
        Genera_OrdenLabel1.Name = "Genera_OrdenLabel1"
        Genera_OrdenLabel1.Size = New System.Drawing.Size(101, 15)
        Genera_OrdenLabel1.TabIndex = 607
        Genera_OrdenLabel1.Text = "Genera Orden:"
        '
        'VigenteLabel
        '
        VigenteLabel.AutoSize = True
        VigenteLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        VigenteLabel.ForeColor = System.Drawing.Color.Red
        VigenteLabel.Location = New System.Drawing.Point(63, 20)
        VigenteLabel.Name = "VigenteLabel"
        VigenteLabel.Size = New System.Drawing.Size(63, 15)
        VigenteLabel.TabIndex = 625
        VigenteLabel.Text = "Vigente :"
        '
        'Aplica_ComisionLabel
        '
        Aplica_ComisionLabel.AutoSize = True
        Aplica_ComisionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Aplica_ComisionLabel.ForeColor = System.Drawing.Color.DarkRed
        Aplica_ComisionLabel.Location = New System.Drawing.Point(8, 40)
        Aplica_ComisionLabel.Name = "Aplica_ComisionLabel"
        Aplica_ComisionLabel.Size = New System.Drawing.Size(118, 15)
        Aplica_ComisionLabel.TabIndex = 606
        Aplica_ComisionLabel.Text = "Aplica Comisión :"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Label4.Location = New System.Drawing.Point(3, 6)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(64, 15)
        Label4.TabIndex = 22
        Label4.Text = "Trabajo :"
        '
        'Periodo_FinalLabel
        '
        Periodo_FinalLabel.AutoSize = True
        Periodo_FinalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Periodo_FinalLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Periodo_FinalLabel.Location = New System.Drawing.Point(145, 19)
        Periodo_FinalLabel.Name = "Periodo_FinalLabel"
        Periodo_FinalLabel.Size = New System.Drawing.Size(52, 15)
        Periodo_FinalLabel.TabIndex = 601
        Periodo_FinalLabel.Text = "Hasta :"
        '
        'Periodo_InicialLabel
        '
        Periodo_InicialLabel.AutoSize = True
        Periodo_InicialLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Periodo_InicialLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Periodo_InicialLabel.Location = New System.Drawing.Point(15, 19)
        Periodo_InicialLabel.Name = "Periodo_InicialLabel"
        Periodo_InicialLabel.Size = New System.Drawing.Size(77, 15)
        Periodo_InicialLabel.TabIndex = 600
        Periodo_InicialLabel.Text = "Apartir de :"
        '
        'DIA_INICIALLabel
        '
        DIA_INICIALLabel.AutoSize = True
        DIA_INICIALLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DIA_INICIALLabel.ForeColor = System.Drawing.Color.LightSlateGray
        DIA_INICIALLabel.Location = New System.Drawing.Point(6, 70)
        DIA_INICIALLabel.Name = "DIA_INICIALLabel"
        DIA_INICIALLabel.Size = New System.Drawing.Size(80, 15)
        DIA_INICIALLabel.TabIndex = 9
        DIA_INICIALLabel.Text = "Día Inicial :"
        '
        'DIA_FINALLabel
        '
        DIA_FINALLabel.AutoSize = True
        DIA_FINALLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DIA_FINALLabel.ForeColor = System.Drawing.Color.LightSlateGray
        DIA_FINALLabel.Location = New System.Drawing.Point(163, 70)
        DIA_FINALLabel.Name = "DIA_FINALLabel"
        DIA_FINALLabel.Size = New System.Drawing.Size(73, 15)
        DIA_FINALLabel.TabIndex = 11
        DIA_FINALLabel.Text = "Día Final :"
        '
        'Precio_AdicionalLabel
        '
        Precio_AdicionalLabel.AutoSize = True
        Precio_AdicionalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Precio_AdicionalLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Precio_AdicionalLabel.Location = New System.Drawing.Point(1, 6)
        Precio_AdicionalLabel.Name = "Precio_AdicionalLabel"
        Precio_AdicionalLabel.Size = New System.Drawing.Size(67, 13)
        Precio_AdicionalLabel.TabIndex = 608
        Precio_AdicionalLabel.Text = " Adicional:"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(4, 4)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(103, 15)
        Label1.TabIndex = 15
        Label1.Text = "Tipo Concepto:"
        '
        'Clv_TipoPromocionLabel
        '
        Clv_TipoPromocionLabel.AutoSize = True
        Clv_TipoPromocionLabel.Location = New System.Drawing.Point(37, 26)
        Clv_TipoPromocionLabel.Name = "Clv_TipoPromocionLabel"
        Clv_TipoPromocionLabel.Size = New System.Drawing.Size(102, 13)
        Clv_TipoPromocionLabel.TabIndex = 629
        Clv_TipoPromocionLabel.Text = "Clv Tipo Promocion:"
        '
        'Numero_CortesiasLabel
        '
        Numero_CortesiasLabel.AutoSize = True
        Numero_CortesiasLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Numero_CortesiasLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Numero_CortesiasLabel.Location = New System.Drawing.Point(16, 5)
        Numero_CortesiasLabel.Name = "Numero_CortesiasLabel"
        Numero_CortesiasLabel.Size = New System.Drawing.Size(150, 15)
        Numero_CortesiasLabel.TabIndex = 0
        Numero_CortesiasLabel.Text = "Numero de Cortesías :"
        '
        'MensDescuentoNetLabel
        '
        MensDescuentoNetLabel.AutoSize = True
        MensDescuentoNetLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        MensDescuentoNetLabel.ForeColor = System.Drawing.Color.LightSlateGray
        MensDescuentoNetLabel.Location = New System.Drawing.Point(208, 83)
        MensDescuentoNetLabel.Name = "MensDescuentoNetLabel"
        MensDescuentoNetLabel.Size = New System.Drawing.Size(98, 15)
        MensDescuentoNetLabel.TabIndex = 625
        MensDescuentoNetLabel.Text = "Mensualidad :"
        '
        'Label8
        '
        Label8.AutoSize = True
        Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label8.ForeColor = System.Drawing.Color.LightSlateGray
        Label8.Location = New System.Drawing.Point(3, 2)
        Label8.Name = "Label8"
        Label8.Size = New System.Drawing.Size(64, 15)
        Label8.TabIndex = 23
        Label8.Text = "Trabajo :"
        '
        'ContDescuentoNetLabel
        '
        ContDescuentoNetLabel.AutoSize = True
        ContDescuentoNetLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ContDescuentoNetLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ContDescuentoNetLabel.Location = New System.Drawing.Point(9, 83)
        ContDescuentoNetLabel.Name = "ContDescuentoNetLabel"
        ContDescuentoNetLabel.Size = New System.Drawing.Size(92, 15)
        ContDescuentoNetLabel.TabIndex = 1
        ContDescuentoNetLabel.Text = "Contratación:"
        '
        'Label10
        '
        Label10.AutoSize = True
        Label10.Location = New System.Drawing.Point(37, 26)
        Label10.Name = "Label10"
        Label10.Size = New System.Drawing.Size(102, 13)
        Label10.TabIndex = 629
        Label10.Text = "Clv Tipo Promocion:"
        '
        'Label12
        '
        Label12.AutoSize = True
        Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label12.ForeColor = System.Drawing.Color.LightSlateGray
        Label12.Location = New System.Drawing.Point(419, 82)
        Label12.Name = "Label12"
        Label12.Size = New System.Drawing.Size(59, 15)
        Label12.TabIndex = 633
        Label12.Text = "Puntos :"
        '
        'Label13
        '
        Label13.AutoSize = True
        Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label13.ForeColor = System.Drawing.Color.LightSlateGray
        Label13.Location = New System.Drawing.Point(70, 5)
        Label13.Name = "Label13"
        Label13.Size = New System.Drawing.Size(96, 15)
        Label13.TabIndex = 0
        Label13.Text = "Contratación :"
        '
        'lblCtaContable
        '
        lblCtaContable.AutoSize = True
        lblCtaContable.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        lblCtaContable.ForeColor = System.Drawing.Color.LightSlateGray
        lblCtaContable.Location = New System.Drawing.Point(3, 4)
        lblCtaContable.Name = "lblCtaContable"
        lblCtaContable.Size = New System.Drawing.Size(97, 15)
        lblCtaContable.TabIndex = 8
        lblCtaContable.Text = "Cta Contable :"
        '
        'lblPosicion
        '
        lblPosicion.AutoSize = True
        lblPosicion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        lblPosicion.ForeColor = System.Drawing.Color.LightSlateGray
        lblPosicion.Location = New System.Drawing.Point(250, 6)
        lblPosicion.Name = "lblPosicion"
        lblPosicion.Size = New System.Drawing.Size(70, 15)
        lblPosicion.TabIndex = 10
        lblPosicion.Text = "Posición :"
        '
        'Label15
        '
        Label15.AutoSize = True
        Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label15.ForeColor = System.Drawing.Color.LightSlateGray
        Label15.Location = New System.Drawing.Point(2, 125)
        Label15.Name = "Label15"
        Label15.Size = New System.Drawing.Size(107, 15)
        Label15.TabIndex = 650
        Label15.Text = "Clave Producto:"
        '
        'PRECIOLabel1
        '
        Me.PRECIOLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PRECIOLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.PRECIOLabel1.Location = New System.Drawing.Point(7, 22)
        Me.PRECIOLabel1.Name = "PRECIOLabel1"
        Me.PRECIOLabel1.Size = New System.Drawing.Size(64, 19)
        Me.PRECIOLabel1.TabIndex = 7
        Me.PRECIOLabel1.Text = " Principal"
        Me.PRECIOLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'CMBAplicanComLabel
        '
        Me.CMBAplicanComLabel.AutoSize = True
        Me.CMBAplicanComLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBAplicanComLabel.ForeColor = System.Drawing.Color.DarkOrange
        Me.CMBAplicanComLabel.Location = New System.Drawing.Point(374, 5)
        Me.CMBAplicanComLabel.Name = "CMBAplicanComLabel"
        Me.CMBAplicanComLabel.Size = New System.Drawing.Size(156, 20)
        Me.CMBAplicanComLabel.TabIndex = 8
        Me.CMBAplicanComLabel.Text = "Aplican Comisión :"
        '
        'CMBSale_en_CarteraLabel
        '
        Me.CMBSale_en_CarteraLabel.AutoSize = True
        Me.CMBSale_en_CarteraLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBSale_en_CarteraLabel.ForeColor = System.Drawing.Color.DarkOrange
        Me.CMBSale_en_CarteraLabel.Location = New System.Drawing.Point(797, 54)
        Me.CMBSale_en_CarteraLabel.Name = "CMBSale_en_CarteraLabel"
        Me.CMBSale_en_CarteraLabel.Size = New System.Drawing.Size(144, 20)
        Me.CMBSale_en_CarteraLabel.TabIndex = 10
        Me.CMBSale_en_CarteraLabel.Text = "Cobro Mensual  :"
        '
        'CMBGenera_OrdenLabel
        '
        Me.CMBGenera_OrdenLabel.AutoSize = True
        Me.CMBGenera_OrdenLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBGenera_OrdenLabel.ForeColor = System.Drawing.Color.DarkOrange
        Me.CMBGenera_OrdenLabel.Location = New System.Drawing.Point(397, 26)
        Me.CMBGenera_OrdenLabel.Name = "CMBGenera_OrdenLabel"
        Me.CMBGenera_OrdenLabel.Size = New System.Drawing.Size(133, 20)
        Me.CMBGenera_OrdenLabel.TabIndex = 301
        Me.CMBGenera_OrdenLabel.Text = "Genera Orden :"
        '
        'CMBEs_PrincipalLabel
        '
        Me.CMBEs_PrincipalLabel.AutoSize = True
        Me.CMBEs_PrincipalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBEs_PrincipalLabel.ForeColor = System.Drawing.Color.DarkOrange
        Me.CMBEs_PrincipalLabel.Location = New System.Drawing.Point(846, 74)
        Me.CMBEs_PrincipalLabel.Name = "CMBEs_PrincipalLabel"
        Me.CMBEs_PrincipalLabel.Size = New System.Drawing.Size(92, 20)
        Me.CMBEs_PrincipalLabel.TabIndex = 401
        Me.CMBEs_PrincipalLabel.Text = " Principal :"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Label15)
        Me.Panel1.Controls.Add(Me.cbClaveProducto)
        Me.Panel1.Controls.Add(Me.pnlContable)
        Me.Panel1.Controls.Add(Me.PnlClaveEquivalente)
        Me.Panel1.Controls.Add(Me.HDCheckBox)
        Me.Panel1.Controls.Add(Me.AplicaIEPSCheckBox)
        Me.Panel1.Controls.Add(Me.CheckBox1)
        Me.Panel1.Controls.Add(Me.DescripcionTextBox)
        Me.Panel1.Controls.Add(Me.CMBLabel10)
        Me.Panel1.Controls.Add(Me.TabControl1)
        Me.Panel1.Controls.Add(Me.Clv_TxtTextBox)
        Me.Panel1.Controls.Add(Me.Es_PrincipalCheckBox)
        Me.Panel1.Controls.Add(Me.Sale_en_CarteraCheckBox1)
        Me.Panel1.Controls.Add(Me.CMBTextBox28)
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.CMBEs_PrincipalLabel)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(CLV_SERVICIOLabel1)
        Me.Panel1.Controls.Add(Me.CLV_SERVICIOTextBox1)
        Me.Panel1.Controls.Add(Me.CLV_LLAVETextBox)
        Me.Panel1.Controls.Add(Me.CONSERVICIOSBindingNavigator)
        Me.Panel1.Controls.Add(Clv_ServicioLabel)
        Me.Panel1.Controls.Add(Me.Clv_ServicioTextBox)
        Me.Panel1.Controls.Add(Clv_TipSerLabel)
        Me.Panel1.Controls.Add(Me.Clv_TipSerTextBox)
        Me.Panel1.Controls.Add(DescripcionLabel)
        Me.Panel1.Controls.Add(Clv_TxtLabel)
        Me.Panel1.Controls.Add(Me.CMBSale_en_CarteraLabel)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(997, 683)
        Me.Panel1.TabIndex = 0
        '
        'cbClaveProducto
        '
        Me.cbClaveProducto.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONRelTrabajosNoCobroMensualBindingSource, "Clv_Trabajo", True))
        Me.cbClaveProducto.DisplayMember = "Descripcion"
        Me.cbClaveProducto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbClaveProducto.FormattingEnabled = True
        Me.cbClaveProducto.Location = New System.Drawing.Point(111, 123)
        Me.cbClaveProducto.Name = "cbClaveProducto"
        Me.cbClaveProducto.Size = New System.Drawing.Size(301, 23)
        Me.cbClaveProducto.TabIndex = 649
        Me.cbClaveProducto.ValueMember = "id_ClaveProdServ"
        '
        'CONRelTrabajosNoCobroMensualBindingSource
        '
        Me.CONRelTrabajosNoCobroMensualBindingSource.DataMember = "CONRel_Trabajos_NoCobroMensual"
        Me.CONRelTrabajosNoCobroMensualBindingSource.DataSource = Me.DataSetEDGAR
        '
        'DataSetEDGAR
        '
        Me.DataSetEDGAR.DataSetName = "DataSetEDGAR"
        Me.DataSetEDGAR.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'pnlContable
        '
        Me.pnlContable.Controls.Add(Me.txtCuentaContable)
        Me.pnlContable.Controls.Add(Me.txtPosicion)
        Me.pnlContable.Controls.Add(lblPosicion)
        Me.pnlContable.Controls.Add(lblCtaContable)
        Me.pnlContable.Location = New System.Drawing.Point(381, 152)
        Me.pnlContable.Name = "pnlContable"
        Me.pnlContable.Size = New System.Drawing.Size(417, 26)
        Me.pnlContable.TabIndex = 638
        Me.pnlContable.Visible = False
        '
        'txtCuentaContable
        '
        Me.txtCuentaContable.BackColor = System.Drawing.Color.White
        Me.txtCuentaContable.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCuentaContable.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtCuentaContable.Location = New System.Drawing.Point(106, 3)
        Me.txtCuentaContable.Name = "txtCuentaContable"
        Me.txtCuentaContable.Size = New System.Drawing.Size(145, 21)
        Me.txtCuentaContable.TabIndex = 12
        '
        'txtPosicion
        '
        Me.txtPosicion.BackColor = System.Drawing.Color.White
        Me.txtPosicion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPosicion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtPosicion.Location = New System.Drawing.Point(317, 4)
        Me.txtPosicion.Name = "txtPosicion"
        Me.txtPosicion.Size = New System.Drawing.Size(97, 21)
        Me.txtPosicion.TabIndex = 11
        '
        'PnlClaveEquivalente
        '
        Me.PnlClaveEquivalente.Controls.Add(Me.TextBox3)
        Me.PnlClaveEquivalente.Controls.Add(Me.Label11)
        Me.PnlClaveEquivalente.Location = New System.Drawing.Point(3, 150)
        Me.PnlClaveEquivalente.Name = "PnlClaveEquivalente"
        Me.PnlClaveEquivalente.Size = New System.Drawing.Size(272, 28)
        Me.PnlClaveEquivalente.TabIndex = 637
        '
        'TextBox3
        '
        Me.TextBox3.BackColor = System.Drawing.Color.White
        Me.TextBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTAClv_EquiBindingSource, "Clv_equivalente", True))
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(130, 4)
        Me.TextBox3.MaxLength = 10
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(141, 21)
        Me.TextBox3.TabIndex = 407
        Me.TextBox3.Visible = False
        '
        'CONSULTAClv_EquiBindingSource
        '
        Me.CONSULTAClv_EquiBindingSource.DataMember = "CONSULTAClv_Equi"
        Me.CONSULTAClv_EquiBindingSource.DataSource = Me.DataSetLidia
        '
        'DataSetLidia
        '
        Me.DataSetLidia.DataSetName = "DataSetLidia"
        Me.DataSetLidia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label11.Location = New System.Drawing.Point(3, 6)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(125, 15)
        Me.Label11.TabIndex = 408
        Me.Label11.Text = "Clave Equivalente:"
        Me.Label11.Visible = False
        '
        'HDCheckBox
        '
        Me.HDCheckBox.AutoSize = True
        Me.HDCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.HDCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.HDCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.HDCheckBox.ForeColor = System.Drawing.Color.DarkRed
        Me.HDCheckBox.Location = New System.Drawing.Point(527, 36)
        Me.HDCheckBox.Name = "HDCheckBox"
        Me.HDCheckBox.Size = New System.Drawing.Size(54, 20)
        Me.HDCheckBox.TabIndex = 636
        Me.HDCheckBox.Text = "HD :"
        Me.HDCheckBox.UseVisualStyleBackColor = True
        '
        'AplicaIEPSCheckBox
        '
        Me.AplicaIEPSCheckBox.AutoSize = True
        Me.AplicaIEPSCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.AplicaIEPSCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AplicaIEPSCheckBox.ForeColor = System.Drawing.Color.DarkRed
        Me.AplicaIEPSCheckBox.Location = New System.Drawing.Point(278, 154)
        Me.AplicaIEPSCheckBox.Name = "AplicaIEPSCheckBox"
        Me.AplicaIEPSCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.AplicaIEPSCheckBox.Size = New System.Drawing.Size(97, 19)
        Me.AplicaIEPSCheckBox.TabIndex = 417
        Me.AplicaIEPSCheckBox.Text = "Aplica IEPS"
        Me.AplicaIEPSCheckBox.UseVisualStyleBackColor = True
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.ForeColor = System.Drawing.Color.Black
        Me.CheckBox1.Location = New System.Drawing.Point(947, 150)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CheckBox1.Size = New System.Drawing.Size(12, 11)
        Me.CheckBox1.TabIndex = 7
        Me.CheckBox1.TabStop = False
        Me.CheckBox1.UseVisualStyleBackColor = True
        Me.CheckBox1.Visible = False
        '
        'DescripcionTextBox
        '
        Me.DescripcionTextBox.BackColor = System.Drawing.Color.White
        Me.DescripcionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DescripcionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSERVICIOSBindingSource, "Descripcion", True))
        Me.DescripcionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionTextBox.Location = New System.Drawing.Point(132, 62)
        Me.DescripcionTextBox.MaxLength = 150
        Me.DescripcionTextBox.Name = "DescripcionTextBox"
        Me.DescripcionTextBox.Size = New System.Drawing.Size(449, 21)
        Me.DescripcionTextBox.TabIndex = 1
        '
        'CONSERVICIOSBindingSource
        '
        Me.CONSERVICIOSBindingSource.DataMember = "CONSERVICIOS"
        Me.CONSERVICIOSBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CMBLabel10
        '
        Me.CMBLabel10.AutoSize = True
        Me.CMBLabel10.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel10.ForeColor = System.Drawing.Color.DarkOrange
        Me.CMBLabel10.Location = New System.Drawing.Point(804, 145)
        Me.CMBLabel10.Name = "CMBLabel10"
        Me.CMBLabel10.Size = New System.Drawing.Size(139, 20)
        Me.CMBLabel10.TabIndex = 406
        Me.CMBLabel10.Text = "Se Vende Solo :"
        Me.CMBLabel10.Visible = False
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(3, 177)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(982, 501)
        Me.TabControl1.TabIndex = 7
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage1.Controls.Add(Me.SplitContainer1)
        Me.TabPage1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage1.ForeColor = System.Drawing.Color.SteelBlue
        Me.TabPage1.Location = New System.Drawing.Point(4, 29)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(974, 468)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Detalle del Servicio"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Enabled = False
        Me.SplitContainer1.Location = New System.Drawing.Point(3, 6)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel21)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel15)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel4)
        Me.SplitContainer1.Panel1.Controls.Add(CLV_LLAVELabel2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CLV_LLAVETextBox2)
        Me.SplitContainer1.Panel1.Controls.Add(CLAVELabel)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CLAVETextBox)
        Me.SplitContainer1.Panel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.SplitContainer1.Panel1.ForeColor = System.Drawing.Color.Black
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.Panel10)
        Me.SplitContainer1.Panel2.Controls.Add(Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator)
        Me.SplitContainer1.Size = New System.Drawing.Size(971, 458)
        Me.SplitContainer1.SplitterDistance = 341
        Me.SplitContainer1.TabIndex = 304
        Me.SplitContainer1.TabStop = False
        '
        'Panel21
        '
        Me.Panel21.Controls.Add(Me.CheckBox2)
        Me.Panel21.Enabled = False
        Me.Panel21.Location = New System.Drawing.Point(6, 51)
        Me.Panel21.Name = "Panel21"
        Me.Panel21.Size = New System.Drawing.Size(268, 25)
        Me.Panel21.TabIndex = 621
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox2.ForeColor = System.Drawing.Color.Red
        Me.CheckBox2.Location = New System.Drawing.Point(6, 5)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(225, 17)
        Me.CheckBox2.TabIndex = 622
        Me.CheckBox2.Text = "Aplicar a Todos los Tipos de Cobro"
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'Panel15
        '
        Me.Panel15.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel15.Controls.Add(Me.ComboBox4)
        Me.Panel15.Controls.Add(Me.Clv_TipoClienteTextBox)
        Me.Panel15.Controls.Add(Me.Label5)
        Me.Panel15.Location = New System.Drawing.Point(4, 3)
        Me.Panel15.Name = "Panel15"
        Me.Panel15.Size = New System.Drawing.Size(318, 40)
        Me.Panel15.TabIndex = 8
        Me.Panel15.TabStop = True
        '
        'ComboBox4
        '
        Me.ComboBox4.DataSource = Me.MUESTRATIPOCLIENTESBindingSource
        Me.ComboBox4.DisplayMember = "DESCRIPCION"
        Me.ComboBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox4.ForeColor = System.Drawing.Color.Blue
        Me.ComboBox4.FormattingEnabled = True
        Me.ComboBox4.Location = New System.Drawing.Point(103, 5)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(206, 28)
        Me.ComboBox4.TabIndex = 8
        Me.ComboBox4.ValueMember = "CLV_TIPOCLIENTE"
        '
        'MUESTRATIPOCLIENTESBindingSource
        '
        Me.MUESTRATIPOCLIENTESBindingSource.DataMember = "MUESTRA_TIPOCLIENTES"
        Me.MUESTRATIPOCLIENTESBindingSource.DataSource = Me.DataSetEDGAR
        '
        'Clv_TipoClienteTextBox
        '
        Me.Clv_TipoClienteTextBox.BackColor = System.Drawing.Color.Gainsboro
        Me.Clv_TipoClienteTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_TipoClienteTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONREL_TARIFADOS_SERVICIOSBindingSource, "Clv_TipoCliente", True))
        Me.Clv_TipoClienteTextBox.ForeColor = System.Drawing.Color.Gainsboro
        Me.Clv_TipoClienteTextBox.Location = New System.Drawing.Point(141, 20)
        Me.Clv_TipoClienteTextBox.Name = "Clv_TipoClienteTextBox"
        Me.Clv_TipoClienteTextBox.Size = New System.Drawing.Size(100, 13)
        Me.Clv_TipoClienteTextBox.TabIndex = 621
        '
        'CONREL_TARIFADOS_SERVICIOSBindingSource
        '
        Me.CONREL_TARIFADOS_SERVICIOSBindingSource.DataMember = "CONREL_TARIFADOS_SERVICIOS"
        Me.CONREL_TARIFADOS_SERVICIOSBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Navy
        Me.Label5.Location = New System.Drawing.Point(5, 11)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(85, 15)
        Me.Label5.TabIndex = 305
        Me.Label5.Text = "Tipo Cobro :"
        '
        'Panel4
        '
        Me.Panel4.AutoScroll = True
        Me.Panel4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel4.Controls.Add(Me.Panel19)
        Me.Panel4.Controls.Add(Me.Panel14)
        Me.Panel4.Controls.Add(Me.Panel11)
        Me.Panel4.Controls.Add(Me.Panel9)
        Me.Panel4.Controls.Add(Me.Panel11adicional2)
        Me.Panel4.Controls.Add(Me.Panel8)
        Me.Panel4.Controls.Add(Me.Panel7)
        Me.Panel4.Controls.Add(Me.Panel6)
        Me.Panel4.Controls.Add(Me.Panel5)
        Me.Panel4.Enabled = False
        Me.Panel4.Location = New System.Drawing.Point(0, 82)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(322, 594)
        Me.Panel4.TabIndex = 405
        '
        'Panel19
        '
        Me.Panel19.Controls.Add(Me.Panel20)
        Me.Panel19.Controls.Add(Me.ComboBox9)
        Me.Panel19.Controls.Add(Me.TextBox1)
        Me.Panel19.Controls.Add(Me.TextBox2)
        Me.Panel19.Controls.Add(Label10)
        Me.Panel19.Location = New System.Drawing.Point(4, 592)
        Me.Panel19.Name = "Panel19"
        Me.Panel19.Size = New System.Drawing.Size(310, 193)
        Me.Panel19.TabIndex = 24
        Me.Panel19.TabStop = True
        Me.Panel19.Visible = False
        '
        'Panel20
        '
        Me.Panel20.Controls.Add(Me.Dame_ContratacionParcialDataGridView)
        Me.Panel20.Location = New System.Drawing.Point(10, 62)
        Me.Panel20.Name = "Panel20"
        Me.Panel20.Size = New System.Drawing.Size(288, 113)
        Me.Panel20.TabIndex = 631
        '
        'Dame_ContratacionParcialDataGridView
        '
        Me.Dame_ContratacionParcialDataGridView.AutoGenerateColumns = False
        Me.Dame_ContratacionParcialDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.Dame_ContratacionParcialDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9, Me.DataGridViewTextBoxColumn10})
        Me.Dame_ContratacionParcialDataGridView.DataSource = Me.Dame_ContratacionParcialBindingSource
        Me.Dame_ContratacionParcialDataGridView.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Dame_ContratacionParcialDataGridView.Location = New System.Drawing.Point(0, 0)
        Me.Dame_ContratacionParcialDataGridView.Name = "Dame_ContratacionParcialDataGridView"
        Me.Dame_ContratacionParcialDataGridView.Size = New System.Drawing.Size(288, 113)
        Me.Dame_ContratacionParcialDataGridView.TabIndex = 0
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "clave_tv"
        Me.DataGridViewTextBoxColumn8.HeaderText = "clave_tv"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "num_pago"
        Me.DataGridViewTextBoxColumn9.HeaderText = "Número de Pagos"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.Width = 125
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "monto"
        Me.DataGridViewTextBoxColumn10.HeaderText = "Monto"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.Width = 125
        '
        'Dame_ContratacionParcialBindingSource
        '
        Me.Dame_ContratacionParcialBindingSource.DataMember = "Dame_ContratacionParcial"
        Me.Dame_ContratacionParcialBindingSource.DataSource = Me.DataSetLidia
        '
        'ComboBox9
        '
        Me.ComboBox9.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONREL_TARIFADOS_SERVICIOSBindingSource, "Clv_TipoPromocion", True))
        Me.ComboBox9.DataSource = Me.Dame_PagosParcialesBindingSource
        Me.ComboBox9.DisplayMember = "Concepto"
        Me.ComboBox9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox9.FormattingEnabled = True
        Me.ComboBox9.Location = New System.Drawing.Point(17, 21)
        Me.ComboBox9.Name = "ComboBox9"
        Me.ComboBox9.Size = New System.Drawing.Size(292, 23)
        Me.ComboBox9.TabIndex = 23
        Me.ComboBox9.ValueMember = "Clave_Tv"
        '
        'Dame_PagosParcialesBindingSource
        '
        Me.Dame_PagosParcialesBindingSource.DataMember = "Dame_PagosParciales"
        Me.Dame_PagosParcialesBindingSource.DataSource = Me.DataSetLidia
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.DarkOrange
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.ForeColor = System.Drawing.Color.White
        Me.TextBox1.Location = New System.Drawing.Point(6, 6)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(304, 13)
        Me.TextBox1.TabIndex = 628
        Me.TextBox1.TabStop = False
        Me.TextBox1.Text = "Contratación por Pagos"
        Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox2
        '
        Me.TextBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONREL_TARIFADOS_SERVICIOSBindingSource, "Clv_TipoPromocion", True))
        Me.TextBox2.Location = New System.Drawing.Point(145, 23)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(100, 20)
        Me.TextBox2.TabIndex = 630
        '
        'Panel14
        '
        Me.Panel14.Controls.Add(Me.Precio_InalambricoTextBox)
        Me.Panel14.Controls.Add(Me.Label14)
        Me.Panel14.Location = New System.Drawing.Point(1, 108)
        Me.Panel14.Name = "Panel14"
        Me.Panel14.Size = New System.Drawing.Size(146, 24)
        Me.Panel14.TabIndex = 11
        Me.Panel14.TabStop = True
        Me.Panel14.Visible = False
        '
        'Precio_InalambricoTextBox
        '
        Me.Precio_InalambricoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Precio_InalambricoTextBox.CausesValidation = False
        Me.Precio_InalambricoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONREL_TARIFADOS_SERVICIOSBindingSource, "Precio_Inalambrico", True))
        Me.Precio_InalambricoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Precio_InalambricoTextBox.Location = New System.Drawing.Point(73, 1)
        Me.Precio_InalambricoTextBox.Name = "Precio_InalambricoTextBox"
        Me.Precio_InalambricoTextBox.Size = New System.Drawing.Size(73, 21)
        Me.Precio_InalambricoTextBox.TabIndex = 11
        '
        'Label14
        '
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label14.Location = New System.Drawing.Point(2, 4)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(81, 19)
        Me.Label14.TabIndex = 12
        Me.Label14.Text = "Inalámbrico"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel11
        '
        Me.Panel11.Controls.Add(Me.ComboBox2)
        Me.Panel11.Controls.Add(Me.CMBTextBox6)
        Me.Panel11.Controls.Add(Me.Clv_TipoPromocionTextBox)
        Me.Panel11.Controls.Add(Clv_TipoPromocionLabel)
        Me.Panel11.Location = New System.Drawing.Point(4, 523)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(310, 65)
        Me.Panel11.TabIndex = 23
        Me.Panel11.TabStop = True
        Me.Panel11.Visible = False
        '
        'ComboBox2
        '
        Me.ComboBox2.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONREL_TARIFADOS_SERVICIOSBindingSource, "Clv_TipoPromocion", True))
        Me.ComboBox2.DataSource = Me.MuestraTipoPromocionBindingSource
        Me.ComboBox2.DisplayMember = "Descripcion"
        Me.ComboBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(17, 21)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(292, 23)
        Me.ComboBox2.TabIndex = 23
        Me.ComboBox2.ValueMember = "clave"
        '
        'MuestraTipoPromocionBindingSource
        '
        Me.MuestraTipoPromocionBindingSource.DataMember = "MuestraTipoPromocion"
        Me.MuestraTipoPromocionBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CMBTextBox6
        '
        Me.CMBTextBox6.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox6.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox6.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox6.Location = New System.Drawing.Point(6, 6)
        Me.CMBTextBox6.Name = "CMBTextBox6"
        Me.CMBTextBox6.Size = New System.Drawing.Size(323, 13)
        Me.CMBTextBox6.TabIndex = 628
        Me.CMBTextBox6.TabStop = False
        Me.CMBTextBox6.Text = "Promoción "
        Me.CMBTextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Clv_TipoPromocionTextBox
        '
        Me.Clv_TipoPromocionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONREL_TARIFADOS_SERVICIOSBindingSource, "Clv_TipoPromocion", True))
        Me.Clv_TipoPromocionTextBox.Location = New System.Drawing.Point(145, 23)
        Me.Clv_TipoPromocionTextBox.Name = "Clv_TipoPromocionTextBox"
        Me.Clv_TipoPromocionTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Clv_TipoPromocionTextBox.TabIndex = 630
        '
        'Panel9
        '
        Me.Panel9.Controls.Add(Me.IEPS2CheckBox)
        Me.Panel9.Controls.Add(Me.ComboBox1)
        Me.Panel9.Controls.Add(Label1)
        Me.Panel9.Location = New System.Drawing.Point(3, 6)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(310, 50)
        Me.Panel9.TabIndex = 9
        Me.Panel9.TabStop = True
        '
        'IEPS2CheckBox
        '
        Me.IEPS2CheckBox.AutoSize = True
        Me.IEPS2CheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.IEPS2CheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IEPS2CheckBox.ForeColor = System.Drawing.Color.Maroon
        Me.IEPS2CheckBox.Location = New System.Drawing.Point(200, 4)
        Me.IEPS2CheckBox.Name = "IEPS2CheckBox"
        Me.IEPS2CheckBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.IEPS2CheckBox.Size = New System.Drawing.Size(90, 17)
        Me.IEPS2CheckBox.TabIndex = 17
        Me.IEPS2CheckBox.Text = "Aplica IEPS"
        Me.IEPS2CheckBox.UseVisualStyleBackColor = True
        '
        'ComboBox1
        '
        Me.ComboBox1.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONREL_TARIFADOS_SERVICIOSBindingSource, "CLAVE", True))
        Me.ComboBox1.DataSource = Me.MUESTRASOLOTARIFADOSBindingSource
        Me.ComboBox1.DisplayMember = "Concepto"
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.ForeColor = System.Drawing.Color.Red
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(6, 22)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(284, 24)
        Me.ComboBox1.TabIndex = 9
        Me.ComboBox1.ValueMember = "Clave"
        '
        'MUESTRASOLOTARIFADOSBindingSource
        '
        Me.MUESTRASOLOTARIFADOSBindingSource.DataMember = "MUESTRASOLOTARIFADOS"
        Me.MUESTRASOLOTARIFADOSBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'Panel11adicional2
        '
        Me.Panel11adicional2.Controls.Add(Me.tbPrecioAdic)
        Me.Panel11adicional2.Controls.Add(Me.LabelPrecioAdic)
        Me.Panel11adicional2.Controls.Add(Me.Precio_AdicionalTextBox)
        Me.Panel11adicional2.Controls.Add(Precio_AdicionalLabel)
        Me.Panel11adicional2.Location = New System.Drawing.Point(1, 108)
        Me.Panel11adicional2.Name = "Panel11adicional2"
        Me.Panel11adicional2.Size = New System.Drawing.Size(304, 26)
        Me.Panel11adicional2.TabIndex = 11
        Me.Panel11adicional2.TabStop = True
        '
        'tbPrecioAdic
        '
        Me.tbPrecioAdic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbPrecioAdic.CausesValidation = False
        Me.tbPrecioAdic.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbPrecioAdic.Location = New System.Drawing.Point(224, 2)
        Me.tbPrecioAdic.Name = "tbPrecioAdic"
        Me.tbPrecioAdic.Size = New System.Drawing.Size(73, 21)
        Me.tbPrecioAdic.TabIndex = 628
        Me.tbPrecioAdic.Visible = False
        '
        'LabelPrecioAdic
        '
        Me.LabelPrecioAdic.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelPrecioAdic.ForeColor = System.Drawing.Color.LightSlateGray
        Me.LabelPrecioAdic.Location = New System.Drawing.Point(142, 3)
        Me.LabelPrecioAdic.Name = "LabelPrecioAdic"
        Me.LabelPrecioAdic.Size = New System.Drawing.Size(80, 19)
        Me.LabelPrecioAdic.TabIndex = 627
        Me.LabelPrecioAdic.Text = "Renta Adic."
        Me.LabelPrecioAdic.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.LabelPrecioAdic.Visible = False
        '
        'Precio_AdicionalTextBox
        '
        Me.Precio_AdicionalTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Precio_AdicionalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONREL_TARIFADOS_SERVICIOSBindingSource, "Precio_Adicional", True))
        Me.Precio_AdicionalTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Precio_AdicionalTextBox.Location = New System.Drawing.Point(69, 2)
        Me.Precio_AdicionalTextBox.Name = "Precio_AdicionalTextBox"
        Me.Precio_AdicionalTextBox.Size = New System.Drawing.Size(73, 21)
        Me.Precio_AdicionalTextBox.TabIndex = 11
        '
        'Panel8
        '
        Me.Panel8.Controls.Add(Me.tbPrecio)
        Me.Panel8.Controls.Add(Me.LabelPrecio)
        Me.Panel8.Controls.Add(Me.Panel22)
        Me.Panel8.Controls.Add(Me.Panel13)
        Me.Panel8.Controls.Add(Me.CMBTextBox4)
        Me.Panel8.Controls.Add(Me.PRECIOTextBox1)
        Me.Panel8.Controls.Add(Me.PRECIOLabel1)
        Me.Panel8.Location = New System.Drawing.Point(3, 59)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(310, 134)
        Me.Panel8.TabIndex = 10
        Me.Panel8.TabStop = True
        '
        'tbPrecio
        '
        Me.tbPrecio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbPrecio.CausesValidation = False
        Me.tbPrecio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbPrecio.Location = New System.Drawing.Point(226, 20)
        Me.tbPrecio.Name = "tbPrecio"
        Me.tbPrecio.Size = New System.Drawing.Size(73, 21)
        Me.tbPrecio.TabIndex = 627
        Me.tbPrecio.Visible = False
        '
        'LabelPrecio
        '
        Me.LabelPrecio.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelPrecio.ForeColor = System.Drawing.Color.LightSlateGray
        Me.LabelPrecio.Location = New System.Drawing.Point(152, 23)
        Me.LabelPrecio.Name = "LabelPrecio"
        Me.LabelPrecio.Size = New System.Drawing.Size(72, 19)
        Me.LabelPrecio.TabIndex = 626
        Me.LabelPrecio.Text = "Renta Pri."
        Me.LabelPrecio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.LabelPrecio.Visible = False
        '
        'Panel22
        '
        Me.Panel22.Controls.Add(Label13)
        Me.Panel22.Controls.Add(Me.TextBox7)
        Me.Panel22.Location = New System.Drawing.Point(6, 75)
        Me.Panel22.Name = "Panel22"
        Me.Panel22.Size = New System.Drawing.Size(290, 25)
        Me.Panel22.TabIndex = 13
        Me.Panel22.TabStop = True
        Me.Panel22.Visible = False
        '
        'TextBox7
        '
        Me.TextBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox7.CausesValidation = False
        Me.TextBox7.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONREL_TARIFADOS_SERVICIOSBindingSource, "Numero_Cortesias", True))
        Me.TextBox7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox7.Location = New System.Drawing.Point(173, 3)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(100, 21)
        Me.TextBox7.TabIndex = 12
        '
        'Panel13
        '
        Me.Panel13.Controls.Add(Numero_CortesiasLabel)
        Me.Panel13.Controls.Add(Me.Numero_CortesiasTextBox)
        Me.Panel13.Location = New System.Drawing.Point(6, 105)
        Me.Panel13.Name = "Panel13"
        Me.Panel13.Size = New System.Drawing.Size(290, 25)
        Me.Panel13.TabIndex = 12
        Me.Panel13.TabStop = True
        Me.Panel13.Visible = False
        '
        'Numero_CortesiasTextBox
        '
        Me.Numero_CortesiasTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Numero_CortesiasTextBox.CausesValidation = False
        Me.Numero_CortesiasTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONREL_TARIFADOS_SERVICIOSBindingSource, "Numero_Cortesias", True))
        Me.Numero_CortesiasTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Numero_CortesiasTextBox.Location = New System.Drawing.Point(172, 3)
        Me.Numero_CortesiasTextBox.Name = "Numero_CortesiasTextBox"
        Me.Numero_CortesiasTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Numero_CortesiasTextBox.TabIndex = 12
        '
        'CMBTextBox4
        '
        Me.CMBTextBox4.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox4.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox4.Location = New System.Drawing.Point(3, 3)
        Me.CMBTextBox4.Name = "CMBTextBox4"
        Me.CMBTextBox4.Size = New System.Drawing.Size(323, 13)
        Me.CMBTextBox4.TabIndex = 625
        Me.CMBTextBox4.TabStop = False
        Me.CMBTextBox4.Text = "Precios del Servicio  :"
        Me.CMBTextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'PRECIOTextBox1
        '
        Me.PRECIOTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PRECIOTextBox1.CausesValidation = False
        Me.PRECIOTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONREL_TARIFADOS_SERVICIOSBindingSource, "PRECIO", True))
        Me.PRECIOTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PRECIOTextBox1.Location = New System.Drawing.Point(71, 22)
        Me.PRECIOTextBox1.Name = "PRECIOTextBox1"
        Me.PRECIOTextBox1.Size = New System.Drawing.Size(73, 21)
        Me.PRECIOTextBox1.TabIndex = 10
        '
        'Panel7
        '
        Me.Panel7.Controls.Add(Me.Periodo_InicialDateTimePicker)
        Me.Panel7.Controls.Add(Me.Periodo_FinalDateTimePicker)
        Me.Panel7.Controls.Add(Me.CMBTextBox1)
        Me.Panel7.Controls.Add(DIA_FINALLabel)
        Me.Panel7.Controls.Add(DIA_INICIALLabel)
        Me.Panel7.Controls.Add(Periodo_InicialLabel)
        Me.Panel7.Controls.Add(Periodo_FinalLabel)
        Me.Panel7.Controls.Add(Me.DIA_INICIALNumericUpDown)
        Me.Panel7.Controls.Add(Me.DIA_FINALNumericUpDown)
        Me.Panel7.Location = New System.Drawing.Point(3, 196)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(311, 104)
        Me.Panel7.TabIndex = 13
        Me.Panel7.TabStop = True
        '
        'Periodo_InicialDateTimePicker
        '
        Me.Periodo_InicialDateTimePicker.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Periodo_InicialDateTimePicker.CustomFormat = "MMM/yyyy"
        Me.Periodo_InicialDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.CONREL_TARIFADOS_SERVICIOSBindingSource, "Periodo_Inicial", True))
        Me.Periodo_InicialDateTimePicker.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Periodo_InicialDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.Periodo_InicialDateTimePicker.Location = New System.Drawing.Point(27, 37)
        Me.Periodo_InicialDateTimePicker.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.Periodo_InicialDateTimePicker.Name = "Periodo_InicialDateTimePicker"
        Me.Periodo_InicialDateTimePicker.Size = New System.Drawing.Size(100, 21)
        Me.Periodo_InicialDateTimePicker.TabIndex = 13
        Me.Periodo_InicialDateTimePicker.Value = New Date(2007, 4, 18, 0, 0, 0, 0)
        '
        'Periodo_FinalDateTimePicker
        '
        Me.Periodo_FinalDateTimePicker.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Periodo_FinalDateTimePicker.CustomFormat = "MMM/yyyy"
        Me.Periodo_FinalDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.CONREL_TARIFADOS_SERVICIOSBindingSource, "Periodo_Final", True))
        Me.Periodo_FinalDateTimePicker.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Periodo_FinalDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.Periodo_FinalDateTimePicker.Location = New System.Drawing.Point(158, 37)
        Me.Periodo_FinalDateTimePicker.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.Periodo_FinalDateTimePicker.Name = "Periodo_FinalDateTimePicker"
        Me.Periodo_FinalDateTimePicker.Size = New System.Drawing.Size(100, 21)
        Me.Periodo_FinalDateTimePicker.TabIndex = 14
        '
        'CMBTextBox1
        '
        Me.CMBTextBox1.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox1.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox1.Location = New System.Drawing.Point(3, 3)
        Me.CMBTextBox1.Name = "CMBTextBox1"
        Me.CMBTextBox1.Size = New System.Drawing.Size(323, 13)
        Me.CMBTextBox1.TabIndex = 622
        Me.CMBTextBox1.TabStop = False
        Me.CMBTextBox1.Text = "Periodo de Vigencia :"
        Me.CMBTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DIA_INICIALNumericUpDown
        '
        Me.DIA_INICIALNumericUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DIA_INICIALNumericUpDown.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.CONREL_TARIFADOS_SERVICIOSBindingSource, "DIA_INICIAL", True))
        Me.DIA_INICIALNumericUpDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DIA_INICIALNumericUpDown.Location = New System.Drawing.Point(87, 68)
        Me.DIA_INICIALNumericUpDown.Maximum = New Decimal(New Integer() {31, 0, 0, 0})
        Me.DIA_INICIALNumericUpDown.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.DIA_INICIALNumericUpDown.Name = "DIA_INICIALNumericUpDown"
        Me.DIA_INICIALNumericUpDown.Size = New System.Drawing.Size(57, 21)
        Me.DIA_INICIALNumericUpDown.TabIndex = 15
        Me.DIA_INICIALNumericUpDown.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'DIA_FINALNumericUpDown
        '
        Me.DIA_FINALNumericUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DIA_FINALNumericUpDown.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.CONREL_TARIFADOS_SERVICIOSBindingSource, "DIA_FINAL", True))
        Me.DIA_FINALNumericUpDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DIA_FINALNumericUpDown.Location = New System.Drawing.Point(242, 68)
        Me.DIA_FINALNumericUpDown.Maximum = New Decimal(New Integer() {31, 0, 0, 0})
        Me.DIA_FINALNumericUpDown.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.DIA_FINALNumericUpDown.Name = "DIA_FINALNumericUpDown"
        Me.DIA_FINALNumericUpDown.Size = New System.Drawing.Size(57, 21)
        Me.DIA_FINALNumericUpDown.TabIndex = 16
        Me.DIA_FINALNumericUpDown.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.Label2)
        Me.Panel6.Controls.Add(Me.Porcetaje_DescuentoTextBox)
        Me.Panel6.Controls.Add(Me.CMBTextBox2)
        Me.Panel6.Location = New System.Drawing.Point(3, 305)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(311, 49)
        Me.Panel6.TabIndex = 17
        Me.Panel6.TabStop = True
        Me.Panel6.Visible = False
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(3, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(197, 20)
        Me.Label2.TabIndex = 625
        Me.Label2.Text = "Label2"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Porcetaje_DescuentoTextBox
        '
        Me.Porcetaje_DescuentoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Porcetaje_DescuentoTextBox.CausesValidation = False
        Me.Porcetaje_DescuentoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONREL_TARIFADOS_SERVICIOSBindingSource, "Porcetaje_Descuento", True))
        Me.Porcetaje_DescuentoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Porcetaje_DescuentoTextBox.Location = New System.Drawing.Point(206, 22)
        Me.Porcetaje_DescuentoTextBox.MaxLength = 18
        Me.Porcetaje_DescuentoTextBox.Name = "Porcetaje_DescuentoTextBox"
        Me.Porcetaje_DescuentoTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Porcetaje_DescuentoTextBox.TabIndex = 17
        '
        'CMBTextBox2
        '
        Me.CMBTextBox2.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox2.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox2.Location = New System.Drawing.Point(3, 3)
        Me.CMBTextBox2.Name = "CMBTextBox2"
        Me.CMBTextBox2.Size = New System.Drawing.Size(323, 13)
        Me.CMBTextBox2.TabIndex = 623
        Me.CMBTextBox2.TabStop = False
        Me.CMBTextBox2.Text = "Descuentos con Otros   :"
        Me.CMBTextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.BrincaMes_Label)
        Me.Panel5.Controls.Add(Me.Label7)
        Me.Panel5.Controls.Add(Me.Se_Cobra_ProporcionalCheckBox)
        Me.Panel5.Controls.Add(Me.Panel12)
        Me.Panel5.Controls.Add(Me.CMBTextBox5)
        Me.Panel5.Controls.Add(Me.BRINCA_MESCheckBox)
        Me.Panel5.Controls.Add(Me.Aplica_ComisionCheckBox)
        Me.Panel5.Controls.Add(Aplica_ComisionLabel)
        Me.Panel5.Controls.Add(VigenteLabel)
        Me.Panel5.Controls.Add(Me.Genera_OrdenCheckBox1)
        Me.Panel5.Controls.Add(Me.VigenteCheckBox)
        Me.Panel5.Controls.Add(Genera_OrdenLabel1)
        Me.Panel5.Location = New System.Drawing.Point(3, 359)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(311, 158)
        Me.Panel5.TabIndex = 18
        Me.Panel5.TabStop = True
        '
        'BrincaMes_Label
        '
        Me.BrincaMes_Label.AutoSize = True
        Me.BrincaMes_Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BrincaMes_Label.ForeColor = System.Drawing.Color.DarkRed
        Me.BrincaMes_Label.Location = New System.Drawing.Point(35, 58)
        Me.BrincaMes_Label.Name = "BrincaMes_Label"
        Me.BrincaMes_Label.Size = New System.Drawing.Size(91, 15)
        Me.BrincaMes_Label.TabIndex = 631
        Me.BrincaMes_Label.Text = "Avanza Mes :"
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.DarkRed
        Me.Label7.Location = New System.Drawing.Point(166, 27)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(85, 28)
        Me.Label7.TabIndex = 630
        Me.Label7.Text = "Se Cobra Proporcional:"
        Me.Label7.Visible = False
        '
        'Se_Cobra_ProporcionalCheckBox
        '
        Me.Se_Cobra_ProporcionalCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONREL_TARIFADOS_SERVICIOSBindingSource, "Se_Cobra_Proporcional", True))
        Me.Se_Cobra_ProporcionalCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Se_Cobra_ProporcionalCheckBox.Location = New System.Drawing.Point(251, 36)
        Me.Se_Cobra_ProporcionalCheckBox.Name = "Se_Cobra_ProporcionalCheckBox"
        Me.Se_Cobra_ProporcionalCheckBox.Size = New System.Drawing.Size(19, 24)
        Me.Se_Cobra_ProporcionalCheckBox.TabIndex = 22
        Me.Se_Cobra_ProporcionalCheckBox.Visible = False
        '
        'Panel12
        '
        Me.Panel12.Controls.Add(Me.Clv_TrabajoTextBox)
        Me.Panel12.Controls.Add(Me.ComboBox3)
        Me.Panel12.Controls.Add(Label4)
        Me.Panel12.Location = New System.Drawing.Point(9, 95)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(302, 57)
        Me.Panel12.TabIndex = 628
        Me.Panel12.Visible = False
        '
        'Clv_TrabajoTextBox
        '
        Me.Clv_TrabajoTextBox.CausesValidation = False
        Me.Clv_TrabajoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONREL_TARIFADOS_SERVICIOSBindingSource, "Clv_Trabajo", True))
        Me.Clv_TrabajoTextBox.Location = New System.Drawing.Point(142, 3)
        Me.Clv_TrabajoTextBox.Name = "Clv_TrabajoTextBox"
        Me.Clv_TrabajoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Clv_TrabajoTextBox.TabIndex = 23
        Me.Clv_TrabajoTextBox.TabStop = False
        Me.Clv_TrabajoTextBox.Visible = False
        '
        'ComboBox3
        '
        Me.ComboBox3.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONREL_TARIFADOS_SERVICIOSBindingSource, "Clv_Trabajo", True))
        Me.ComboBox3.DataSource = Me.MUESTRATRABAJOSBindingSource
        Me.ComboBox3.DisplayMember = "Descripcion"
        Me.ComboBox3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox3.FormattingEnabled = True
        Me.ComboBox3.Location = New System.Drawing.Point(6, 24)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(289, 21)
        Me.ComboBox3.TabIndex = 22
        Me.ComboBox3.ValueMember = "Clv_Trabajo"
        '
        'MUESTRATRABAJOSBindingSource
        '
        Me.MUESTRATRABAJOSBindingSource.DataMember = "MUESTRATRABAJOS"
        Me.MUESTRATRABAJOSBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CMBTextBox5
        '
        Me.CMBTextBox5.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox5.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox5.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox5.Location = New System.Drawing.Point(3, 3)
        Me.CMBTextBox5.Name = "CMBTextBox5"
        Me.CMBTextBox5.Size = New System.Drawing.Size(323, 13)
        Me.CMBTextBox5.TabIndex = 627
        Me.CMBTextBox5.TabStop = False
        Me.CMBTextBox5.Text = "Opciones "
        Me.CMBTextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BRINCA_MESCheckBox
        '
        Me.BRINCA_MESCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONREL_TARIFADOS_SERVICIOSBindingSource, "BRINCA_MES", True))
        Me.BRINCA_MESCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BRINCA_MESCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BRINCA_MESCheckBox.Location = New System.Drawing.Point(142, 58)
        Me.BRINCA_MESCheckBox.Name = "BRINCA_MESCheckBox"
        Me.BRINCA_MESCheckBox.Size = New System.Drawing.Size(18, 20)
        Me.BRINCA_MESCheckBox.TabIndex = 20
        '
        'Aplica_ComisionCheckBox
        '
        Me.Aplica_ComisionCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONREL_TARIFADOS_SERVICIOSBindingSource, "Aplica_Comision", True))
        Me.Aplica_ComisionCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Aplica_ComisionCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Aplica_ComisionCheckBox.Location = New System.Drawing.Point(142, 40)
        Me.Aplica_ComisionCheckBox.Name = "Aplica_ComisionCheckBox"
        Me.Aplica_ComisionCheckBox.Size = New System.Drawing.Size(18, 20)
        Me.Aplica_ComisionCheckBox.TabIndex = 19
        '
        'Genera_OrdenCheckBox1
        '
        Me.Genera_OrdenCheckBox1.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONREL_TARIFADOS_SERVICIOSBindingSource, "Genera_Orden", True))
        Me.Genera_OrdenCheckBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Genera_OrdenCheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Genera_OrdenCheckBox1.Location = New System.Drawing.Point(142, 75)
        Me.Genera_OrdenCheckBox1.Name = "Genera_OrdenCheckBox1"
        Me.Genera_OrdenCheckBox1.Size = New System.Drawing.Size(18, 20)
        Me.Genera_OrdenCheckBox1.TabIndex = 21
        '
        'VigenteCheckBox
        '
        Me.VigenteCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONREL_TARIFADOS_SERVICIOSBindingSource, "Vigente", True))
        Me.VigenteCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.VigenteCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VigenteCheckBox.Location = New System.Drawing.Point(142, 23)
        Me.VigenteCheckBox.Name = "VigenteCheckBox"
        Me.VigenteCheckBox.Size = New System.Drawing.Size(18, 20)
        Me.VigenteCheckBox.TabIndex = 18
        '
        'CLV_LLAVETextBox2
        '
        Me.CLV_LLAVETextBox2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CLV_LLAVETextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CLV_LLAVETextBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONREL_TARIFADOS_SERVICIOSBindingSource, "CLV_LLAVE", True))
        Me.CLV_LLAVETextBox2.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.CLV_LLAVETextBox2.Location = New System.Drawing.Point(214, 32)
        Me.CLV_LLAVETextBox2.Name = "CLV_LLAVETextBox2"
        Me.CLV_LLAVETextBox2.Size = New System.Drawing.Size(100, 13)
        Me.CLV_LLAVETextBox2.TabIndex = 620
        '
        'CLAVETextBox
        '
        Me.CLAVETextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CLAVETextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CLAVETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONREL_TARIFADOS_SERVICIOSBindingSource, "CLAVE", True))
        Me.CLAVETextBox.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.CLAVETextBox.Location = New System.Drawing.Point(195, 399)
        Me.CLAVETextBox.Name = "CLAVETextBox"
        Me.CLAVETextBox.Size = New System.Drawing.Size(104, 13)
        Me.CLAVETextBox.TabIndex = 600
        Me.CLAVETextBox.TabStop = False
        '
        'Panel10
        '
        Me.Panel10.Controls.Add(Me.DataGridView1)
        Me.Panel10.Location = New System.Drawing.Point(3, 26)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(620, 434)
        Me.Panel10.TabIndex = 623
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Chocolate
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CLVLLAVEDataGridViewTextBoxColumn, Me.CONCEPTODataGridViewTextBoxColumn, Me.PRECIODataGridViewTextBoxColumn, Me.PrecioAdicionalDataGridViewTextBoxColumn, Me.PeriodoInicialDataGridViewTextBoxColumn, Me.PeriodoFinalDataGridViewTextBoxColumn, Me.DIAINICIALDataGridViewTextBoxColumn, Me.DIAFINALDataGridViewTextBoxColumn, Me.PorcetajeDescuentoDataGridViewTextBoxColumn, Me.BRINCAMESDataGridViewCheckBoxColumn, Me.AplicaComisionDataGridViewCheckBoxColumn, Me.GeneraOrdenDataGridViewTextBoxColumn, Me.Promocion, Me.TRABAJO, Me.Numero_Cortesias, Me.Precio_Inalambrico})
        Me.DataGridView1.DataSource = Me.BUSCAREL_TARIFADOS_SERVICIOSBindingSource
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(620, 434)
        Me.DataGridView1.TabIndex = 5
        Me.DataGridView1.TabStop = False
        '
        'CLVLLAVEDataGridViewTextBoxColumn
        '
        Me.CLVLLAVEDataGridViewTextBoxColumn.DataPropertyName = "CLV_LLAVE"
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black
        Me.CLVLLAVEDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle2
        Me.CLVLLAVEDataGridViewTextBoxColumn.HeaderText = "CLV_LLAVE"
        Me.CLVLLAVEDataGridViewTextBoxColumn.Name = "CLVLLAVEDataGridViewTextBoxColumn"
        Me.CLVLLAVEDataGridViewTextBoxColumn.ReadOnly = True
        Me.CLVLLAVEDataGridViewTextBoxColumn.Visible = False
        Me.CLVLLAVEDataGridViewTextBoxColumn.Width = 5
        '
        'CONCEPTODataGridViewTextBoxColumn
        '
        Me.CONCEPTODataGridViewTextBoxColumn.DataPropertyName = "CONCEPTO"
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black
        Me.CONCEPTODataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle3
        Me.CONCEPTODataGridViewTextBoxColumn.HeaderText = "Concepto"
        Me.CONCEPTODataGridViewTextBoxColumn.Name = "CONCEPTODataGridViewTextBoxColumn"
        Me.CONCEPTODataGridViewTextBoxColumn.ReadOnly = True
        Me.CONCEPTODataGridViewTextBoxColumn.Width = 150
        '
        'PRECIODataGridViewTextBoxColumn
        '
        Me.PRECIODataGridViewTextBoxColumn.DataPropertyName = "PRECIO"
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black
        Me.PRECIODataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle4
        Me.PRECIODataGridViewTextBoxColumn.HeaderText = "Precio"
        Me.PRECIODataGridViewTextBoxColumn.Name = "PRECIODataGridViewTextBoxColumn"
        Me.PRECIODataGridViewTextBoxColumn.ReadOnly = True
        '
        'PrecioAdicionalDataGridViewTextBoxColumn
        '
        Me.PrecioAdicionalDataGridViewTextBoxColumn.DataPropertyName = "Precio_Inalambrico"
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black
        Me.PrecioAdicionalDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle5
        Me.PrecioAdicionalDataGridViewTextBoxColumn.HeaderText = "Precio Adicionales"
        Me.PrecioAdicionalDataGridViewTextBoxColumn.Name = "PrecioAdicionalDataGridViewTextBoxColumn"
        Me.PrecioAdicionalDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PeriodoInicialDataGridViewTextBoxColumn
        '
        Me.PeriodoInicialDataGridViewTextBoxColumn.DataPropertyName = "Periodo_Inicial"
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black
        Me.PeriodoInicialDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle6
        Me.PeriodoInicialDataGridViewTextBoxColumn.HeaderText = "Apartir de"
        Me.PeriodoInicialDataGridViewTextBoxColumn.Name = "PeriodoInicialDataGridViewTextBoxColumn"
        Me.PeriodoInicialDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PeriodoFinalDataGridViewTextBoxColumn
        '
        Me.PeriodoFinalDataGridViewTextBoxColumn.DataPropertyName = "Periodo_Final"
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black
        Me.PeriodoFinalDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle7
        Me.PeriodoFinalDataGridViewTextBoxColumn.HeaderText = "Hasta "
        Me.PeriodoFinalDataGridViewTextBoxColumn.Name = "PeriodoFinalDataGridViewTextBoxColumn"
        Me.PeriodoFinalDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DIAINICIALDataGridViewTextBoxColumn
        '
        Me.DIAINICIALDataGridViewTextBoxColumn.DataPropertyName = "DIA_INICIAL"
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black
        Me.DIAINICIALDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle8
        Me.DIAINICIALDataGridViewTextBoxColumn.HeaderText = "Día Inicial"
        Me.DIAINICIALDataGridViewTextBoxColumn.Name = "DIAINICIALDataGridViewTextBoxColumn"
        Me.DIAINICIALDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DIAFINALDataGridViewTextBoxColumn
        '
        Me.DIAFINALDataGridViewTextBoxColumn.DataPropertyName = "DIA_FINAL"
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black
        Me.DIAFINALDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle9
        Me.DIAFINALDataGridViewTextBoxColumn.HeaderText = "Día Final"
        Me.DIAFINALDataGridViewTextBoxColumn.Name = "DIAFINALDataGridViewTextBoxColumn"
        Me.DIAFINALDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PorcetajeDescuentoDataGridViewTextBoxColumn
        '
        Me.PorcetajeDescuentoDataGridViewTextBoxColumn.DataPropertyName = "Porcetaje_Descuento"
        DataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black
        Me.PorcetajeDescuentoDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle10
        Me.PorcetajeDescuentoDataGridViewTextBoxColumn.HeaderText = "Porcetaje Descuento"
        Me.PorcetajeDescuentoDataGridViewTextBoxColumn.Name = "PorcetajeDescuentoDataGridViewTextBoxColumn"
        Me.PorcetajeDescuentoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'BRINCAMESDataGridViewCheckBoxColumn
        '
        Me.BRINCAMESDataGridViewCheckBoxColumn.DataPropertyName = "BRINCA_MES"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle11.NullValue = False
        Me.BRINCAMESDataGridViewCheckBoxColumn.DefaultCellStyle = DataGridViewCellStyle11
        Me.BRINCAMESDataGridViewCheckBoxColumn.HeaderText = "Avanza Mes"
        Me.BRINCAMESDataGridViewCheckBoxColumn.Name = "BRINCAMESDataGridViewCheckBoxColumn"
        Me.BRINCAMESDataGridViewCheckBoxColumn.ReadOnly = True
        '
        'AplicaComisionDataGridViewCheckBoxColumn
        '
        Me.AplicaComisionDataGridViewCheckBoxColumn.DataPropertyName = "Aplica_Comision"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle12.NullValue = False
        Me.AplicaComisionDataGridViewCheckBoxColumn.DefaultCellStyle = DataGridViewCellStyle12
        Me.AplicaComisionDataGridViewCheckBoxColumn.HeaderText = "Aplica Comisión"
        Me.AplicaComisionDataGridViewCheckBoxColumn.Name = "AplicaComisionDataGridViewCheckBoxColumn"
        Me.AplicaComisionDataGridViewCheckBoxColumn.ReadOnly = True
        '
        'GeneraOrdenDataGridViewTextBoxColumn
        '
        Me.GeneraOrdenDataGridViewTextBoxColumn.DataPropertyName = "Genera_Orden"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle13.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle13.NullValue = False
        Me.GeneraOrdenDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle13
        Me.GeneraOrdenDataGridViewTextBoxColumn.HeaderText = "Genera Orden"
        Me.GeneraOrdenDataGridViewTextBoxColumn.Name = "GeneraOrdenDataGridViewTextBoxColumn"
        Me.GeneraOrdenDataGridViewTextBoxColumn.ReadOnly = True
        Me.GeneraOrdenDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GeneraOrdenDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'Promocion
        '
        Me.Promocion.DataPropertyName = "Promocion"
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Promocion.DefaultCellStyle = DataGridViewCellStyle14
        Me.Promocion.HeaderText = "Promoción"
        Me.Promocion.Name = "Promocion"
        Me.Promocion.ReadOnly = True
        Me.Promocion.Width = 300
        '
        'TRABAJO
        '
        Me.TRABAJO.DataPropertyName = "TRABAJO"
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TRABAJO.DefaultCellStyle = DataGridViewCellStyle15
        Me.TRABAJO.HeaderText = "Servicio al Cliente"
        Me.TRABAJO.Name = "TRABAJO"
        Me.TRABAJO.ReadOnly = True
        Me.TRABAJO.Width = 250
        '
        'Numero_Cortesias
        '
        Me.Numero_Cortesias.DataPropertyName = "Numero_Cortesias"
        Me.Numero_Cortesias.HeaderText = "Numero_Cortesias"
        Me.Numero_Cortesias.Name = "Numero_Cortesias"
        Me.Numero_Cortesias.ReadOnly = True
        Me.Numero_Cortesias.Width = 5
        '
        'Precio_Inalambrico
        '
        Me.Precio_Inalambrico.DataPropertyName = "Precio_Inalambrico"
        Me.Precio_Inalambrico.HeaderText = "Precio_Inalambrico"
        Me.Precio_Inalambrico.Name = "Precio_Inalambrico"
        Me.Precio_Inalambrico.ReadOnly = True
        Me.Precio_Inalambrico.Width = 5
        '
        'BUSCAREL_TARIFADOS_SERVICIOSBindingSource
        '
        Me.BUSCAREL_TARIFADOS_SERVICIOSBindingSource.DataMember = "BUSCAREL_TARIFADOS_SERVICIOS"
        Me.BUSCAREL_TARIFADOS_SERVICIOSBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CONREL_TARIFADOS_SERVICIOSBindingNavigator
        '
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.AddNewItem = Nothing
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.BindingSource = Me.CONREL_TARIFADOS_SERVICIOSBindingSource
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.CountItem = Nothing
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.DeleteItem = Nothing
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.Enabled = False
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorAddNewItem, Me.ToolStripButton4, Me.CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem, Me.ToolStripButton2, Me.ToolStripButton3})
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.MoveFirstItem = Nothing
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.MoveLastItem = Nothing
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.MoveNextItem = Nothing
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.MovePreviousItem = Nothing
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.Name = "CONREL_TARIFADOS_SERVICIOSBindingNavigator"
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.PositionItem = Nothing
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.Size = New System.Drawing.Size(626, 25)
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.TabIndex = 622
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorAddNewItem.ForeColor = System.Drawing.Color.Black
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(126, 22)
        Me.BindingNavigatorAddNewItem.Text = "&Agregar Nuevo"
        '
        'ToolStripButton4
        '
        Me.ToolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton4.Enabled = False
        Me.ToolStripButton4.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton4.ForeColor = System.Drawing.Color.Black
        Me.ToolStripButton4.Image = CType(resources.GetObject("ToolStripButton4.Image"), System.Drawing.Image)
        Me.ToolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton4.Name = "ToolStripButton4"
        Me.ToolStripButton4.Size = New System.Drawing.Size(71, 22)
        Me.ToolStripButton4.Text = "&Modificar"
        '
        'CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem
        '
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem.Enabled = False
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem.ForeColor = System.Drawing.Color.Black
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem.Name = "CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem"
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem.Size = New System.Drawing.Size(80, 22)
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem.Text = "G&uardar"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.Enabled = False
        Me.ToolStripButton2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton2.ForeColor = System.Drawing.Color.Black
        Me.ToolStripButton2.Image = CType(resources.GetObject("ToolStripButton2.Image"), System.Drawing.Image)
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton2.Size = New System.Drawing.Size(77, 22)
        Me.ToolStripButton2.Text = "E&liminar"
        '
        'ToolStripButton3
        '
        Me.ToolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton3.Enabled = False
        Me.ToolStripButton3.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton3.ForeColor = System.Drawing.Color.Black
        Me.ToolStripButton3.Image = CType(resources.GetObject("ToolStripButton3.Image"), System.Drawing.Image)
        Me.ToolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton3.Name = "ToolStripButton3"
        Me.ToolStripButton3.Size = New System.Drawing.Size(68, 22)
        Me.ToolStripButton3.Text = "Ca&ncelar"
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage2.Controls.Add(Me.PanelPagoAdelantado)
        Me.TabPage2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage2.Location = New System.Drawing.Point(4, 29)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(974, 468)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Puntos de Descuento"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'PanelPagoAdelantado
        '
        Me.PanelPagoAdelantado.Controls.Add(Me.Panel16)
        Me.PanelPagoAdelantado.Controls.Add(Me.CMBTextBox3)
        Me.PanelPagoAdelantado.Controls.Add(Puntos3Label)
        Me.PanelPagoAdelantado.Controls.Add(Puntos6Label)
        Me.PanelPagoAdelantado.Controls.Add(Puntos11Label)
        Me.PanelPagoAdelantado.Controls.Add(Punto_Pronto_PagoLabel)
        Me.PanelPagoAdelantado.Controls.Add(Me.Punto_Pronto_PagoNumericUpDown)
        Me.PanelPagoAdelantado.Controls.Add(Me.Puntos3NumericUpDown)
        Me.PanelPagoAdelantado.Controls.Add(Me.Puntos6NumericUpDown)
        Me.PanelPagoAdelantado.Controls.Add(Me.Puntos11NumericUpDown)
        Me.PanelPagoAdelantado.Location = New System.Drawing.Point(6, 6)
        Me.PanelPagoAdelantado.Name = "PanelPagoAdelantado"
        Me.PanelPagoAdelantado.Size = New System.Drawing.Size(962, 458)
        Me.PanelPagoAdelantado.TabIndex = 10
        Me.PanelPagoAdelantado.TabStop = True
        '
        'Panel16
        '
        Me.Panel16.Controls.Add(Me.CheckBox3)
        Me.Panel16.Controls.Add(Me.Label9)
        Me.Panel16.Controls.Add(Me.ComboBox8)
        Me.Panel16.Controls.Add(Me.Cancelar1)
        Me.Panel16.Controls.Add(Me.CMBTextBox7)
        Me.Panel16.Controls.Add(Me.Eliminar1)
        Me.Panel16.Controls.Add(Me.Panel17)
        Me.Panel16.Controls.Add(Me.PanelCapDesNet)
        Me.Panel16.Controls.Add(Me.Guardar1)
        Me.Panel16.Controls.Add(Me.Agregar1)
        Me.Panel16.Controls.Add(Me.Modificar1)
        Me.Panel16.Location = New System.Drawing.Point(321, 3)
        Me.Panel16.Name = "Panel16"
        Me.Panel16.Size = New System.Drawing.Size(638, 452)
        Me.Panel16.TabIndex = 14
        Me.Panel16.TabStop = True
        Me.Panel16.Visible = False
        '
        'CheckBox3
        '
        Me.CheckBox3.AutoSize = True
        Me.CheckBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox3.ForeColor = System.Drawing.Color.Red
        Me.CheckBox3.Location = New System.Drawing.Point(126, 71)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(290, 17)
        Me.CheckBox3.TabIndex = 641
        Me.CheckBox3.Text = "Aplicar Descuento a Todos los Tipos de Cobro"
        Me.CheckBox3.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Navy
        Me.Label9.Location = New System.Drawing.Point(35, 44)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(85, 15)
        Me.Label9.TabIndex = 640
        Me.Label9.Text = "Tipo Cobro :"
        '
        'ComboBox8
        '
        Me.ComboBox8.DataSource = Me.MUESTRATIPOCLIENTES2BindingSource
        Me.ComboBox8.DisplayMember = "DESCRIPCION"
        Me.ComboBox8.FormattingEnabled = True
        Me.ComboBox8.Location = New System.Drawing.Point(126, 41)
        Me.ComboBox8.Name = "ComboBox8"
        Me.ComboBox8.Size = New System.Drawing.Size(372, 24)
        Me.ComboBox8.TabIndex = 14
        Me.ComboBox8.ValueMember = "CLV_TIPOCLIENTE"
        '
        'MUESTRATIPOCLIENTES2BindingSource
        '
        Me.MUESTRATIPOCLIENTES2BindingSource.DataMember = "MUESTRA_TIPOCLIENTES_2"
        Me.MUESTRATIPOCLIENTES2BindingSource.DataSource = Me.DataSetEdgarRev2
        '
        'DataSetEdgarRev2
        '
        Me.DataSetEdgarRev2.DataSetName = "DataSetEdgarRev2"
        Me.DataSetEdgarRev2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Cancelar1
        '
        Me.Cancelar1.BackColor = System.Drawing.SystemColors.Control
        Me.Cancelar1.Location = New System.Drawing.Point(457, 226)
        Me.Cancelar1.Name = "Cancelar1"
        Me.Cancelar1.Size = New System.Drawing.Size(80, 25)
        Me.Cancelar1.TabIndex = 638
        Me.Cancelar1.Text = "&Cancelar"
        Me.Cancelar1.UseVisualStyleBackColor = False
        '
        'CMBTextBox7
        '
        Me.CMBTextBox7.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox7.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox7.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox7.Location = New System.Drawing.Point(0, 3)
        Me.CMBTextBox7.Multiline = True
        Me.CMBTextBox7.Name = "CMBTextBox7"
        Me.CMBTextBox7.ReadOnly = True
        Me.CMBTextBox7.Size = New System.Drawing.Size(635, 23)
        Me.CMBTextBox7.TabIndex = 627
        Me.CMBTextBox7.TabStop = False
        Me.CMBTextBox7.Text = "      Descuento por tener un Servicio :"
        Me.CMBTextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Eliminar1
        '
        Me.Eliminar1.BackColor = System.Drawing.SystemColors.Control
        Me.Eliminar1.Location = New System.Drawing.Point(371, 226)
        Me.Eliminar1.Name = "Eliminar1"
        Me.Eliminar1.Size = New System.Drawing.Size(80, 25)
        Me.Eliminar1.TabIndex = 637
        Me.Eliminar1.Text = "&Eliminar"
        Me.Eliminar1.UseVisualStyleBackColor = False
        '
        'Panel17
        '
        Me.Panel17.AutoScroll = True
        Me.Panel17.Controls.Add(Me.CONGeneralDescuentoNetDataGridView)
        Me.Panel17.Controls.Add(Me.Clv_Servicio_Con_AplicaTextBox)
        Me.Panel17.Controls.Add(Me.Clv_TipSerTextBox1)
        Me.Panel17.Controls.Add(Me.Clv_ServicioTextBox2)
        Me.Panel17.Location = New System.Drawing.Point(6, 270)
        Me.Panel17.Name = "Panel17"
        Me.Panel17.Size = New System.Drawing.Size(632, 161)
        Me.Panel17.TabIndex = 634
        '
        'CONGeneralDescuentoNetDataGridView
        '
        Me.CONGeneralDescuentoNetDataGridView.AllowUserToAddRows = False
        Me.CONGeneralDescuentoNetDataGridView.AllowUserToDeleteRows = False
        Me.CONGeneralDescuentoNetDataGridView.AutoGenerateColumns = False
        Me.CONGeneralDescuentoNetDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.CONGeneralDescuentoNetDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn11, Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn4})
        Me.CONGeneralDescuentoNetDataGridView.DataSource = Me.CONGeneralDescuentoNetBindingSource
        Me.CONGeneralDescuentoNetDataGridView.Location = New System.Drawing.Point(3, 3)
        Me.CONGeneralDescuentoNetDataGridView.Name = "CONGeneralDescuentoNetDataGridView"
        Me.CONGeneralDescuentoNetDataGridView.ReadOnly = True
        Me.CONGeneralDescuentoNetDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.CONGeneralDescuentoNetDataGridView.Size = New System.Drawing.Size(619, 151)
        Me.CONGeneralDescuentoNetDataGridView.TabIndex = 0
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Concepto"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Concepto"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 150
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Descripcion"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Descripción"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 150
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "ContDescuentoNet"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Contratación"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "MensDescuentoNet"
        Me.DataGridViewTextBoxColumn7.HeaderText = "Mensualidad"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "Puntos"
        Me.DataGridViewTextBoxColumn11.HeaderText = "Puntos"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Clv_Servicio"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Clv_Servicio"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Clv_TipSer"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Clv_TipSer"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Clv_Servicio_Con_Aplica"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Clv_Servicio_Con_Aplica"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'CONGeneralDescuentoNetBindingSource
        '
        Me.CONGeneralDescuentoNetBindingSource.DataMember = "CONGeneralDescuentoNet"
        Me.CONGeneralDescuentoNetBindingSource.DataSource = Me.DataSetEric2
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric2"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Clv_Servicio_Con_AplicaTextBox
        '
        Me.Clv_Servicio_Con_AplicaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONGeneralDescuentoNetBindingSource, "Clv_Servicio_Con_Aplica", True))
        Me.Clv_Servicio_Con_AplicaTextBox.Location = New System.Drawing.Point(18, 47)
        Me.Clv_Servicio_Con_AplicaTextBox.Name = "Clv_Servicio_Con_AplicaTextBox"
        Me.Clv_Servicio_Con_AplicaTextBox.ReadOnly = True
        Me.Clv_Servicio_Con_AplicaTextBox.Size = New System.Drawing.Size(10, 22)
        Me.Clv_Servicio_Con_AplicaTextBox.TabIndex = 6
        Me.Clv_Servicio_Con_AplicaTextBox.TabStop = False
        '
        'Clv_TipSerTextBox1
        '
        Me.Clv_TipSerTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONGeneralDescuentoNetBindingSource, "Clv_TipSer", True))
        Me.Clv_TipSerTextBox1.Location = New System.Drawing.Point(34, 19)
        Me.Clv_TipSerTextBox1.Name = "Clv_TipSerTextBox1"
        Me.Clv_TipSerTextBox1.ReadOnly = True
        Me.Clv_TipSerTextBox1.Size = New System.Drawing.Size(10, 22)
        Me.Clv_TipSerTextBox1.TabIndex = 4
        Me.Clv_TipSerTextBox1.TabStop = False
        '
        'Clv_ServicioTextBox2
        '
        Me.Clv_ServicioTextBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONGeneralDescuentoNetBindingSource, "Clv_Servicio", True))
        Me.Clv_ServicioTextBox2.Location = New System.Drawing.Point(18, 19)
        Me.Clv_ServicioTextBox2.Name = "Clv_ServicioTextBox2"
        Me.Clv_ServicioTextBox2.ReadOnly = True
        Me.Clv_ServicioTextBox2.Size = New System.Drawing.Size(10, 22)
        Me.Clv_ServicioTextBox2.TabIndex = 2
        Me.Clv_ServicioTextBox2.TabStop = False
        '
        'PanelCapDesNet
        '
        Me.PanelCapDesNet.Controls.Add(Me.PuntosTextBox)
        Me.PanelCapDesNet.Controls.Add(Me.TextBox6)
        Me.PanelCapDesNet.Controls.Add(Me.MensDescuentoNetTextBox)
        Me.PanelCapDesNet.Controls.Add(Me.TextBox5)
        Me.PanelCapDesNet.Controls.Add(Me.ContDescuentoNetTextBox)
        Me.PanelCapDesNet.Controls.Add(Me.TextBox4)
        Me.PanelCapDesNet.Controls.Add(Me.ConceptoComboBox)
        Me.PanelCapDesNet.Controls.Add(Me.DescripcionComboBox)
        Me.PanelCapDesNet.Controls.Add(Label12)
        Me.PanelCapDesNet.Controls.Add(ContDescuentoNetLabel)
        Me.PanelCapDesNet.Controls.Add(Me.Label3)
        Me.PanelCapDesNet.Controls.Add(Me.Label6)
        Me.PanelCapDesNet.Controls.Add(Me.ComboBox7)
        Me.PanelCapDesNet.Controls.Add(MensDescuentoNetLabel)
        Me.PanelCapDesNet.Controls.Add(Me.ComboBox6)
        Me.PanelCapDesNet.Enabled = False
        Me.PanelCapDesNet.Location = New System.Drawing.Point(29, 102)
        Me.PanelCapDesNet.Name = "PanelCapDesNet"
        Me.PanelCapDesNet.Size = New System.Drawing.Size(596, 118)
        Me.PanelCapDesNet.TabIndex = 15
        Me.PanelCapDesNet.TabStop = True
        '
        'PuntosTextBox
        '
        Me.PuntosTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONGeneralDescuentoNetBindingSource, "Puntos", True))
        Me.PuntosTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PuntosTextBox.Location = New System.Drawing.Point(480, 75)
        Me.PuntosTextBox.Name = "PuntosTextBox"
        Me.PuntosTextBox.Size = New System.Drawing.Size(100, 21)
        Me.PuntosTextBox.TabIndex = 636
        Me.PuntosTextBox.Visible = False
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(480, 75)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(100, 22)
        Me.TextBox6.TabIndex = 639
        '
        'MensDescuentoNetTextBox
        '
        Me.MensDescuentoNetTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONGeneralDescuentoNetBindingSource, "MensDescuentoNet", True))
        Me.MensDescuentoNetTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MensDescuentoNetTextBox.Location = New System.Drawing.Point(312, 76)
        Me.MensDescuentoNetTextBox.Name = "MensDescuentoNetTextBox"
        Me.MensDescuentoNetTextBox.Size = New System.Drawing.Size(100, 21)
        Me.MensDescuentoNetTextBox.TabIndex = 635
        Me.MensDescuentoNetTextBox.Visible = False
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(312, 76)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(100, 22)
        Me.TextBox5.TabIndex = 638
        '
        'ContDescuentoNetTextBox
        '
        Me.ContDescuentoNetTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONGeneralDescuentoNetBindingSource, "ContDescuentoNet", True))
        Me.ContDescuentoNetTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContDescuentoNetTextBox.Location = New System.Drawing.Point(107, 75)
        Me.ContDescuentoNetTextBox.Name = "ContDescuentoNetTextBox"
        Me.ContDescuentoNetTextBox.Size = New System.Drawing.Size(100, 21)
        Me.ContDescuentoNetTextBox.TabIndex = 634
        Me.ContDescuentoNetTextBox.Visible = False
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(107, 75)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(100, 22)
        Me.TextBox4.TabIndex = 637
        '
        'ConceptoComboBox
        '
        Me.ConceptoComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONGeneralDescuentoNetBindingSource, "Concepto", True))
        Me.ConceptoComboBox.Enabled = False
        Me.ConceptoComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConceptoComboBox.FormattingEnabled = True
        Me.ConceptoComboBox.Location = New System.Drawing.Point(201, 9)
        Me.ConceptoComboBox.Name = "ConceptoComboBox"
        Me.ConceptoComboBox.Size = New System.Drawing.Size(264, 23)
        Me.ConceptoComboBox.TabIndex = 7
        Me.ConceptoComboBox.Visible = False
        '
        'DescripcionComboBox
        '
        Me.DescripcionComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONGeneralDescuentoNetBindingSource, "Descripcion", True))
        Me.DescripcionComboBox.Enabled = False
        Me.DescripcionComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionComboBox.FormattingEnabled = True
        Me.DescripcionComboBox.Location = New System.Drawing.Point(178, 38)
        Me.DescripcionComboBox.Name = "DescripcionComboBox"
        Me.DescripcionComboBox.Size = New System.Drawing.Size(291, 23)
        Me.DescripcionComboBox.TabIndex = 8
        Me.DescripcionComboBox.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label3.Location = New System.Drawing.Point(6, 12)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(193, 15)
        Me.Label3.TabIndex = 629
        Me.Label3.Text = "Seleccione el Tipo Servicio  :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label6.Location = New System.Drawing.Point(15, 41)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(157, 15)
        Me.Label6.TabIndex = 631
        Me.Label6.Text = "Seleccione el Servicio :"
        '
        'ComboBox7
        '
        Me.ComboBox7.DataSource = Me.MuesteServiciosDescuentoNetBindingSource
        Me.ComboBox7.DisplayMember = "Descripcion"
        Me.ComboBox7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox7.FormattingEnabled = True
        Me.ComboBox7.Location = New System.Drawing.Point(178, 38)
        Me.ComboBox7.Name = "ComboBox7"
        Me.ComboBox7.Size = New System.Drawing.Size(291, 23)
        Me.ComboBox7.TabIndex = 16
        Me.ComboBox7.ValueMember = "Clv_Servicio"
        '
        'MuesteServiciosDescuentoNetBindingSource
        '
        Me.MuesteServiciosDescuentoNetBindingSource.DataMember = "MuesteServiciosDescuentoNet"
        Me.MuesteServiciosDescuentoNetBindingSource.DataSource = Me.DataSetEDGAR
        '
        'ComboBox6
        '
        Me.ComboBox6.DataSource = Me.MuestraTipSerPrincipalDescuentNetBindingSource
        Me.ComboBox6.DisplayMember = "Concepto"
        Me.ComboBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox6.ForeColor = System.Drawing.Color.Red
        Me.ComboBox6.FormattingEnabled = True
        Me.ComboBox6.Location = New System.Drawing.Point(201, 9)
        Me.ComboBox6.Name = "ComboBox6"
        Me.ComboBox6.Size = New System.Drawing.Size(264, 23)
        Me.ComboBox6.TabIndex = 15
        Me.ComboBox6.ValueMember = "Clv_Tipser"
        '
        'MuestraTipSerPrincipalDescuentNetBindingSource
        '
        Me.MuestraTipSerPrincipalDescuentNetBindingSource.DataMember = "MuestraTipSerPrincipalDescuentNet"
        Me.MuestraTipSerPrincipalDescuentNetBindingSource.DataSource = Me.DataSetEDGAR
        '
        'Guardar1
        '
        Me.Guardar1.BackColor = System.Drawing.SystemColors.Control
        Me.Guardar1.Enabled = False
        Me.Guardar1.Location = New System.Drawing.Point(285, 226)
        Me.Guardar1.Name = "Guardar1"
        Me.Guardar1.Size = New System.Drawing.Size(80, 25)
        Me.Guardar1.TabIndex = 636
        Me.Guardar1.Text = "&Guardar"
        Me.Guardar1.UseVisualStyleBackColor = False
        '
        'Agregar1
        '
        Me.Agregar1.BackColor = System.Drawing.SystemColors.Control
        Me.Agregar1.Location = New System.Drawing.Point(113, 226)
        Me.Agregar1.Name = "Agregar1"
        Me.Agregar1.Size = New System.Drawing.Size(80, 25)
        Me.Agregar1.TabIndex = 634
        Me.Agregar1.Text = "&Nuevo"
        Me.Agregar1.UseVisualStyleBackColor = False
        '
        'Modificar1
        '
        Me.Modificar1.BackColor = System.Drawing.SystemColors.Control
        Me.Modificar1.Location = New System.Drawing.Point(199, 226)
        Me.Modificar1.Name = "Modificar1"
        Me.Modificar1.Size = New System.Drawing.Size(80, 25)
        Me.Modificar1.TabIndex = 635
        Me.Modificar1.Text = "&Modificar"
        Me.Modificar1.UseVisualStyleBackColor = False
        '
        'CMBTextBox3
        '
        Me.CMBTextBox3.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox3.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox3.Location = New System.Drawing.Point(3, 3)
        Me.CMBTextBox3.Multiline = True
        Me.CMBTextBox3.Name = "CMBTextBox3"
        Me.CMBTextBox3.ReadOnly = True
        Me.CMBTextBox3.Size = New System.Drawing.Size(280, 26)
        Me.CMBTextBox3.TabIndex = 624
        Me.CMBTextBox3.TabStop = False
        Me.CMBTextBox3.Text = "Descuento por Pronto Pago y Pago Adelantado :"
        Me.CMBTextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Punto_Pronto_PagoNumericUpDown
        '
        Me.Punto_Pronto_PagoNumericUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Punto_Pronto_PagoNumericUpDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Punto_Pronto_PagoNumericUpDown.Location = New System.Drawing.Point(180, 35)
        Me.Punto_Pronto_PagoNumericUpDown.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.Punto_Pronto_PagoNumericUpDown.Name = "Punto_Pronto_PagoNumericUpDown"
        Me.Punto_Pronto_PagoNumericUpDown.Size = New System.Drawing.Size(57, 21)
        Me.Punto_Pronto_PagoNumericUpDown.TabIndex = 10
        '
        'Puntos3NumericUpDown
        '
        Me.Puntos3NumericUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Puntos3NumericUpDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Puntos3NumericUpDown.Location = New System.Drawing.Point(180, 62)
        Me.Puntos3NumericUpDown.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.Puntos3NumericUpDown.Name = "Puntos3NumericUpDown"
        Me.Puntos3NumericUpDown.Size = New System.Drawing.Size(57, 21)
        Me.Puntos3NumericUpDown.TabIndex = 11
        '
        'Puntos6NumericUpDown
        '
        Me.Puntos6NumericUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Puntos6NumericUpDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Puntos6NumericUpDown.Location = New System.Drawing.Point(180, 89)
        Me.Puntos6NumericUpDown.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.Puntos6NumericUpDown.Name = "Puntos6NumericUpDown"
        Me.Puntos6NumericUpDown.Size = New System.Drawing.Size(57, 21)
        Me.Puntos6NumericUpDown.TabIndex = 12
        '
        'Puntos11NumericUpDown
        '
        Me.Puntos11NumericUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Puntos11NumericUpDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Puntos11NumericUpDown.Location = New System.Drawing.Point(180, 116)
        Me.Puntos11NumericUpDown.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.Puntos11NumericUpDown.Name = "Puntos11NumericUpDown"
        Me.Puntos11NumericUpDown.Size = New System.Drawing.Size(57, 21)
        Me.Puntos11NumericUpDown.TabIndex = 13
        '
        'Clv_TxtTextBox
        '
        Me.Clv_TxtTextBox.BackColor = System.Drawing.Color.White
        Me.Clv_TxtTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_TxtTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Clv_TxtTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSERVICIOSBindingSource, "Clv_Txt", True))
        Me.Clv_TxtTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_TxtTextBox.Location = New System.Drawing.Point(131, 33)
        Me.Clv_TxtTextBox.MaxLength = 5
        Me.Clv_TxtTextBox.Name = "Clv_TxtTextBox"
        Me.Clv_TxtTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Clv_TxtTextBox.TabIndex = 0
        '
        'Es_PrincipalCheckBox
        '
        Me.Es_PrincipalCheckBox.Checked = True
        Me.Es_PrincipalCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me.Es_PrincipalCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONSERVICIOSBindingSource, "Es_Principal", True))
        Me.Es_PrincipalCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Es_PrincipalCheckBox.Location = New System.Drawing.Point(947, 75)
        Me.Es_PrincipalCheckBox.Name = "Es_PrincipalCheckBox"
        Me.Es_PrincipalCheckBox.Size = New System.Drawing.Size(25, 17)
        Me.Es_PrincipalCheckBox.TabIndex = 4
        '
        'Sale_en_CarteraCheckBox1
        '
        Me.Sale_en_CarteraCheckBox1.Checked = True
        Me.Sale_en_CarteraCheckBox1.CheckState = System.Windows.Forms.CheckState.Checked
        Me.Sale_en_CarteraCheckBox1.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONSERVICIOSBindingSource, "Sale_en_Cartera", True))
        Me.Sale_en_CarteraCheckBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Sale_en_CarteraCheckBox1.Location = New System.Drawing.Point(947, 50)
        Me.Sale_en_CarteraCheckBox1.Name = "Sale_en_CarteraCheckBox1"
        Me.Sale_en_CarteraCheckBox1.Size = New System.Drawing.Size(23, 24)
        Me.Sale_en_CarteraCheckBox1.TabIndex = 3
        '
        'CMBTextBox28
        '
        Me.CMBTextBox28.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox28.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox28.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox28.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox28.Location = New System.Drawing.Point(711, 30)
        Me.CMBTextBox28.Name = "CMBTextBox28"
        Me.CMBTextBox28.ReadOnly = True
        Me.CMBTextBox28.Size = New System.Drawing.Size(248, 19)
        Me.CMBTextBox28.TabIndex = 404
        Me.CMBTextBox28.TabStop = False
        Me.CMBTextBox28.Text = "Es Servicio :"
        Me.CMBTextBox28.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Panel18)
        Me.Panel3.Controls.Add(Me.CMBGenera_OrdenLabel)
        Me.Panel3.Controls.Add(Me.CMBAplicanComLabel)
        Me.Panel3.Controls.Add(Me.Sale_en_CarteraCheckBox)
        Me.Panel3.Controls.Add(Me.Genera_OrdenCheckBox)
        Me.Panel3.Location = New System.Drawing.Point(413, 98)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(563, 48)
        Me.Panel3.TabIndex = 5
        Me.Panel3.TabStop = True
        '
        'Panel18
        '
        Me.Panel18.Controls.Add(Label8)
        Me.Panel18.Controls.Add(Me.ComboBox5)
        Me.Panel18.Location = New System.Drawing.Point(5, 23)
        Me.Panel18.Name = "Panel18"
        Me.Panel18.Size = New System.Drawing.Size(381, 22)
        Me.Panel18.TabIndex = 303
        Me.Panel18.Visible = False
        '
        'ComboBox5
        '
        Me.ComboBox5.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONRelTrabajosNoCobroMensualBindingSource, "Clv_Trabajo", True))
        Me.ComboBox5.DataSource = Me.MUESTRATRABAJOSNOCOBROMENSUALBindingSource
        Me.ComboBox5.DisplayMember = "Descripcion"
        Me.ComboBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox5.FormattingEnabled = True
        Me.ComboBox5.Location = New System.Drawing.Point(73, 1)
        Me.ComboBox5.Name = "ComboBox5"
        Me.ComboBox5.Size = New System.Drawing.Size(308, 23)
        Me.ComboBox5.TabIndex = 7
        Me.ComboBox5.ValueMember = "Clv_Trabajo"
        '
        'MUESTRATRABAJOSNOCOBROMENSUALBindingSource
        '
        Me.MUESTRATRABAJOSNOCOBROMENSUALBindingSource.DataMember = "MUESTRATRABAJOS_NOCOBROMENSUAL"
        Me.MUESTRATRABAJOSNOCOBROMENSUALBindingSource.DataSource = Me.DataSetEDGAR
        '
        'Sale_en_CarteraCheckBox
        '
        Me.Sale_en_CarteraCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONSERVICIOSBindingSource, "AplicanCom", True))
        Me.Sale_en_CarteraCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Sale_en_CarteraCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Sale_en_CarteraCheckBox.Location = New System.Drawing.Point(536, 5)
        Me.Sale_en_CarteraCheckBox.Name = "Sale_en_CarteraCheckBox"
        Me.Sale_en_CarteraCheckBox.Size = New System.Drawing.Size(21, 24)
        Me.Sale_en_CarteraCheckBox.TabIndex = 5
        '
        'Genera_OrdenCheckBox
        '
        Me.Genera_OrdenCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONSERVICIOSBindingSource, "Genera_Orden", True))
        Me.Genera_OrdenCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Genera_OrdenCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Genera_OrdenCheckBox.Location = New System.Drawing.Point(536, 25)
        Me.Genera_OrdenCheckBox.Name = "Genera_OrdenCheckBox"
        Me.Genera_OrdenCheckBox.Size = New System.Drawing.Size(21, 24)
        Me.Genera_OrdenCheckBox.TabIndex = 6
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.PrecioTextBox)
        Me.Panel2.Controls.Add(PrecioLabel)
        Me.Panel2.Location = New System.Drawing.Point(62, 88)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(273, 31)
        Me.Panel2.TabIndex = 2
        Me.Panel2.TabStop = True
        '
        'PrecioTextBox
        '
        Me.PrecioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PrecioTextBox.CausesValidation = False
        Me.PrecioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSERVICIOSBindingSource, "Precio", True))
        Me.PrecioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PrecioTextBox.Location = New System.Drawing.Point(70, 6)
        Me.PrecioTextBox.Name = "PrecioTextBox"
        Me.PrecioTextBox.Size = New System.Drawing.Size(140, 21)
        Me.PrecioTextBox.TabIndex = 2
        '
        'CLV_SERVICIOTextBox1
        '
        Me.CLV_SERVICIOTextBox1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CLV_SERVICIOTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CLV_SERVICIOTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONREL_TARIFADOS_SERVICIOSBindingSource, "CLV_SERVICIO", True))
        Me.CLV_SERVICIOTextBox1.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.CLV_SERVICIOTextBox1.Location = New System.Drawing.Point(364, 68)
        Me.CLV_SERVICIOTextBox1.Name = "CLV_SERVICIOTextBox1"
        Me.CLV_SERVICIOTextBox1.ReadOnly = True
        Me.CLV_SERVICIOTextBox1.Size = New System.Drawing.Size(104, 13)
        Me.CLV_SERVICIOTextBox1.TabIndex = 400
        Me.CLV_SERVICIOTextBox1.TabStop = False
        '
        'CLV_LLAVETextBox
        '
        Me.CLV_LLAVETextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CLV_LLAVETextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CLV_LLAVETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAREL_TARIFADOS_SERVICIOSBindingSource, "CLV_LLAVE", True))
        Me.CLV_LLAVETextBox.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.CLV_LLAVETextBox.Location = New System.Drawing.Point(89, 162)
        Me.CLV_LLAVETextBox.Name = "CLV_LLAVETextBox"
        Me.CLV_LLAVETextBox.Size = New System.Drawing.Size(104, 13)
        Me.CLV_LLAVETextBox.TabIndex = 200
        Me.CLV_LLAVETextBox.TabStop = False
        '
        'CONSERVICIOSBindingNavigator
        '
        Me.CONSERVICIOSBindingNavigator.AddNewItem = Nothing
        Me.CONSERVICIOSBindingNavigator.BindingSource = Me.CONSERVICIOSBindingSource
        Me.CONSERVICIOSBindingNavigator.CountItem = Nothing
        Me.CONSERVICIOSBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONSERVICIOSBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.CONSERVICIOSBindingNavigatorSaveItem, Me.BindingNavigatorDeleteItem})
        Me.CONSERVICIOSBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONSERVICIOSBindingNavigator.MoveFirstItem = Nothing
        Me.CONSERVICIOSBindingNavigator.MoveLastItem = Nothing
        Me.CONSERVICIOSBindingNavigator.MoveNextItem = Nothing
        Me.CONSERVICIOSBindingNavigator.MovePreviousItem = Nothing
        Me.CONSERVICIOSBindingNavigator.Name = "CONSERVICIOSBindingNavigator"
        Me.CONSERVICIOSBindingNavigator.PositionItem = Nothing
        Me.CONSERVICIOSBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONSERVICIOSBindingNavigator.Size = New System.Drawing.Size(997, 25)
        Me.CONSERVICIOSBindingNavigator.TabIndex = 12
        Me.CONSERVICIOSBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(90, 22)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(79, 22)
        Me.ToolStripButton1.Text = "&CANCELAR"
        '
        'CONSERVICIOSBindingNavigatorSaveItem
        '
        Me.CONSERVICIOSBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONSERVICIOSBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONSERVICIOSBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONSERVICIOSBindingNavigatorSaveItem.Name = "CONSERVICIOSBindingNavigatorSaveItem"
        Me.CONSERVICIOSBindingNavigatorSaveItem.Size = New System.Drawing.Size(91, 22)
        Me.CONSERVICIOSBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'Clv_ServicioTextBox
        '
        Me.Clv_ServicioTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_ServicioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_ServicioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSERVICIOSBindingSource, "Clv_Servicio", True))
        Me.Clv_ServicioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_ServicioTextBox.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_ServicioTextBox.Location = New System.Drawing.Point(355, 41)
        Me.Clv_ServicioTextBox.Name = "Clv_ServicioTextBox"
        Me.Clv_ServicioTextBox.ReadOnly = True
        Me.Clv_ServicioTextBox.Size = New System.Drawing.Size(104, 14)
        Me.Clv_ServicioTextBox.TabIndex = 100
        Me.Clv_ServicioTextBox.TabStop = False
        '
        'Clv_TipSerTextBox
        '
        Me.Clv_TipSerTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TipSerTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_TipSerTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSERVICIOSBindingSource, "Clv_TipSer", True))
        Me.Clv_TipSerTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_TipSerTextBox.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TipSerTextBox.Location = New System.Drawing.Point(326, 94)
        Me.Clv_TipSerTextBox.Name = "Clv_TipSerTextBox"
        Me.Clv_TipSerTextBox.Size = New System.Drawing.Size(104, 14)
        Me.Clv_TipSerTextBox.TabIndex = 300
        Me.Clv_TipSerTextBox.TabStop = False
        '
        'BUSCAPuntosPagoAdelantadoBindingSource
        '
        Me.BUSCAPuntosPagoAdelantadoBindingSource.DataMember = "BUSCAPuntos_Pago_Adelantado"
        Me.BUSCAPuntosPagoAdelantadoBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'ValidaPeriodosBindingSource
        '
        Me.ValidaPeriodosBindingSource.DataMember = "ValidaPeriodos"
        Me.ValidaPeriodosBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(873, 701)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 4
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'CONSERVICIOSTableAdapter
        '
        Me.CONSERVICIOSTableAdapter.ClearBeforeFill = True
        '
        'MUESTRASOLOTARIFADOSTableAdapter
        '
        Me.MUESTRASOLOTARIFADOSTableAdapter.ClearBeforeFill = True
        '
        'CONREL_TARIFADOS_SERVICIOSTableAdapter
        '
        Me.CONREL_TARIFADOS_SERVICIOSTableAdapter.ClearBeforeFill = True
        '
        'BUSCAREL_TARIFADOS_SERVICIOSTableAdapter
        '
        Me.BUSCAREL_TARIFADOS_SERVICIOSTableAdapter.ClearBeforeFill = True
        '
        'ValidaPeriodosTableAdapter
        '
        Me.ValidaPeriodosTableAdapter.ClearBeforeFill = True
        '
        'MODREL_TARIFADOS_SERVICIOSBindingSource
        '
        Me.MODREL_TARIFADOS_SERVICIOSBindingSource.DataMember = "MODREL_TARIFADOS_SERVICIOS"
        Me.MODREL_TARIFADOS_SERVICIOSBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MODREL_TARIFADOS_SERVICIOSTableAdapter
        '
        Me.MODREL_TARIFADOS_SERVICIOSTableAdapter.ClearBeforeFill = True
        '
        'BUSCAPuntos_Pago_AdelantadoTableAdapter
        '
        Me.BUSCAPuntos_Pago_AdelantadoTableAdapter.ClearBeforeFill = True
        '
        'NUEPuntos_Pago_AdelantadoBindingSource
        '
        Me.NUEPuntos_Pago_AdelantadoBindingSource.DataMember = "NUEPuntos_Pago_Adelantado"
        Me.NUEPuntos_Pago_AdelantadoBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NUEPuntos_Pago_AdelantadoTableAdapter
        '
        Me.NUEPuntos_Pago_AdelantadoTableAdapter.ClearBeforeFill = True
        '
        'MuestraTipoPromocionTableAdapter
        '
        Me.MuestraTipoPromocionTableAdapter.ClearBeforeFill = True
        '
        'MUESTRATRABAJOSTableAdapter
        '
        Me.MUESTRATRABAJOSTableAdapter.ClearBeforeFill = True
        '
        'MUESTRA_TIPOCLIENTESTableAdapter
        '
        Me.MUESTRA_TIPOCLIENTESTableAdapter.ClearBeforeFill = True
        '
        'MUESTRATRABAJOS_NOCOBROMENSUALTableAdapter
        '
        Me.MUESTRATRABAJOS_NOCOBROMENSUALTableAdapter.ClearBeforeFill = True
        '
        'CONRel_Trabajos_NoCobroMensualTableAdapter
        '
        Me.CONRel_Trabajos_NoCobroMensualTableAdapter.ClearBeforeFill = True
        '
        'GUARDARel_Trabajos_NoCobroMensualBindingSource
        '
        Me.GUARDARel_Trabajos_NoCobroMensualBindingSource.DataMember = "GUARDARel_Trabajos_NoCobroMensual"
        Me.GUARDARel_Trabajos_NoCobroMensualBindingSource.DataSource = Me.DataSetEDGAR
        '
        'GUARDARel_Trabajos_NoCobroMensualTableAdapter
        '
        Me.GUARDARel_Trabajos_NoCobroMensualTableAdapter.ClearBeforeFill = True
        '
        'BORRel_Trabajos_NoCobroMensualBindingSource
        '
        Me.BORRel_Trabajos_NoCobroMensualBindingSource.DataMember = "BORRel_Trabajos_NoCobroMensual"
        Me.BORRel_Trabajos_NoCobroMensualBindingSource.DataSource = Me.DataSetEDGAR
        '
        'BORRel_Trabajos_NoCobroMensualTableAdapter
        '
        Me.BORRel_Trabajos_NoCobroMensualTableAdapter.ClearBeforeFill = True
        '
        'MuestraTipSerPrincipalDescuentNetTableAdapter
        '
        Me.MuestraTipSerPrincipalDescuentNetTableAdapter.ClearBeforeFill = True
        '
        'MuesteServiciosDescuentoNetTableAdapter
        '
        Me.MuesteServiciosDescuentoNetTableAdapter.ClearBeforeFill = True
        '
        'BORGeneralDescuentoNetBindingSource
        '
        Me.BORGeneralDescuentoNetBindingSource.DataMember = "BORGeneralDescuentoNet"
        Me.BORGeneralDescuentoNetBindingSource.DataSource = Me.DataSetEDGAR
        '
        'BORGeneralDescuentoNetTableAdapter
        '
        Me.BORGeneralDescuentoNetTableAdapter.ClearBeforeFill = True
        '
        'Valida_Guarda_GeneralDescuentoNetBindingSource
        '
        Me.Valida_Guarda_GeneralDescuentoNetBindingSource.DataMember = "Valida_Guarda_GeneralDescuentoNet"
        Me.Valida_Guarda_GeneralDescuentoNetBindingSource.DataSource = Me.DataSetEDGAR
        '
        'Valida_Guarda_GeneralDescuentoNetTableAdapter
        '
        Me.Valida_Guarda_GeneralDescuentoNetTableAdapter.ClearBeforeFill = True
        '
        'MUESTRA_TIPOCLIENTES_2TableAdapter
        '
        Me.MUESTRA_TIPOCLIENTES_2TableAdapter.ClearBeforeFill = True
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BorAplicaSoloInternetBindingSource
        '
        Me.BorAplicaSoloInternetBindingSource.DataMember = "BorAplicaSoloInternet"
        Me.BorAplicaSoloInternetBindingSource.DataSource = Me.DataSetEric
        '
        'BorAplicaSoloInternetTableAdapter
        '
        Me.BorAplicaSoloInternetTableAdapter.ClearBeforeFill = True
        '
        'NueAplicaSoloInternetBindingSource
        '
        Me.NueAplicaSoloInternetBindingSource.DataMember = "NueAplicaSoloInternet"
        Me.NueAplicaSoloInternetBindingSource.DataSource = Me.DataSetEric
        '
        'NueAplicaSoloInternetTableAdapter
        '
        Me.NueAplicaSoloInternetTableAdapter.ClearBeforeFill = True
        '
        'ValidaAplicaSoloInternetBindingSource
        '
        Me.ValidaAplicaSoloInternetBindingSource.DataMember = "ValidaAplicaSoloInternet"
        Me.ValidaAplicaSoloInternetBindingSource.DataSource = Me.DataSetEric
        '
        'ValidaAplicaSoloInternetTableAdapter
        '
        Me.ValidaAplicaSoloInternetTableAdapter.ClearBeforeFill = True
        '
        'Dame_PagosParcialesTableAdapter
        '
        Me.Dame_PagosParcialesTableAdapter.ClearBeforeFill = True
        '
        'Dame_ContratacionParcialTableAdapter
        '
        Me.Dame_ContratacionParcialTableAdapter.ClearBeforeFill = True
        '
        'NUEVOClv_EquiBindingSource
        '
        Me.NUEVOClv_EquiBindingSource.DataMember = "NUEVOClv_Equi"
        Me.NUEVOClv_EquiBindingSource.DataSource = Me.DataSetLidia
        '
        'NUEVOClv_EquiTableAdapter
        '
        Me.NUEVOClv_EquiTableAdapter.ClearBeforeFill = True
        '
        'CONSULTAClv_EquiTableAdapter
        '
        Me.CONSULTAClv_EquiTableAdapter.ClearBeforeFill = True
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Valida_borra_servicioBindingSource
        '
        Me.Valida_borra_servicioBindingSource.DataMember = "Valida_borra_servicio"
        Me.Valida_borra_servicioBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Valida_borra_servicioTableAdapter
        '
        Me.Valida_borra_servicioTableAdapter.ClearBeforeFill = True
        '
        'CONGeneralDescuentoNetTableAdapter
        '
        Me.CONGeneralDescuentoNetTableAdapter.ClearBeforeFill = True
        '
        'Guarda_GeneralDescuentoNetBindingSource
        '
        Me.Guarda_GeneralDescuentoNetBindingSource.DataMember = "Guarda_GeneralDescuentoNet"
        Me.Guarda_GeneralDescuentoNetBindingSource.DataSource = Me.DataSetEric2
        '
        'Guarda_GeneralDescuentoNetTableAdapter
        '
        Me.Guarda_GeneralDescuentoNetTableAdapter.ClearBeforeFill = True
        '
        'NUEREL_TARIFADOS_SERVICIOSALLBindingSource
        '
        Me.NUEREL_TARIFADOS_SERVICIOSALLBindingSource.DataMember = "NUEREL_TARIFADOS_SERVICIOSALL"
        Me.NUEREL_TARIFADOS_SERVICIOSALLBindingSource.DataSource = Me.DataSetEric2
        '
        'NUEREL_TARIFADOS_SERVICIOSALLTableAdapter
        '
        Me.NUEREL_TARIFADOS_SERVICIOSALLTableAdapter.ClearBeforeFill = True
        '
        'MODREL_TARIFADOS_SERVICIOSALLBindingSource
        '
        Me.MODREL_TARIFADOS_SERVICIOSALLBindingSource.DataMember = "MODREL_TARIFADOS_SERVICIOSALL"
        Me.MODREL_TARIFADOS_SERVICIOSALLBindingSource.DataSource = Me.DataSetEric2
        '
        'MODREL_TARIFADOS_SERVICIOSALLTableAdapter
        '
        Me.MODREL_TARIFADOS_SERVICIOSALLTableAdapter.ClearBeforeFill = True
        '
        'Guarda_GeneralDescuentoNetAllBindingSource
        '
        Me.Guarda_GeneralDescuentoNetAllBindingSource.DataMember = "Guarda_GeneralDescuentoNetAll"
        Me.Guarda_GeneralDescuentoNetAllBindingSource.DataSource = Me.DataSetEric2
        '
        'Guarda_GeneralDescuentoNetAllTableAdapter
        '
        Me.Guarda_GeneralDescuentoNetAllTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter5
        '
        Me.Muestra_ServiciosDigitalesTableAdapter5.ClearBeforeFill = True
        '
        'FrmServicios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1016, 741)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Button5)
        Me.MaximizeBox = False
        Me.Name = "FrmServicios"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Servicios"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.CONRelTrabajosNoCobroMensualBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlContable.ResumeLayout(False)
        Me.pnlContable.PerformLayout()
        Me.PnlClaveEquivalente.ResumeLayout(False)
        Me.PnlClaveEquivalente.PerformLayout()
        CType(Me.CONSULTAClv_EquiBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONSERVICIOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        Me.SplitContainer1.ResumeLayout(False)
        Me.Panel21.ResumeLayout(False)
        Me.Panel21.PerformLayout()
        Me.Panel15.ResumeLayout(False)
        Me.Panel15.PerformLayout()
        CType(Me.MUESTRATIPOCLIENTESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONREL_TARIFADOS_SERVICIOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel19.ResumeLayout(False)
        Me.Panel19.PerformLayout()
        Me.Panel20.ResumeLayout(False)
        CType(Me.Dame_ContratacionParcialDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_ContratacionParcialBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_PagosParcialesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel14.ResumeLayout(False)
        Me.Panel14.PerformLayout()
        Me.Panel11.ResumeLayout(False)
        Me.Panel11.PerformLayout()
        CType(Me.MuestraTipoPromocionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        CType(Me.MUESTRASOLOTARIFADOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel11adicional2.ResumeLayout(False)
        Me.Panel11adicional2.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.Panel22.ResumeLayout(False)
        Me.Panel22.PerformLayout()
        Me.Panel13.ResumeLayout(False)
        Me.Panel13.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        CType(Me.DIA_INICIALNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DIA_FINALNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel12.ResumeLayout(False)
        Me.Panel12.PerformLayout()
        CType(Me.MUESTRATRABAJOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel10.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BUSCAREL_TARIFADOS_SERVICIOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.ResumeLayout(False)
        Me.CONREL_TARIFADOS_SERVICIOSBindingNavigator.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.PanelPagoAdelantado.ResumeLayout(False)
        Me.PanelPagoAdelantado.PerformLayout()
        Me.Panel16.ResumeLayout(False)
        Me.Panel16.PerformLayout()
        CType(Me.MUESTRATIPOCLIENTES2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel17.ResumeLayout(False)
        Me.Panel17.PerformLayout()
        CType(Me.CONGeneralDescuentoNetDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONGeneralDescuentoNetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelCapDesNet.ResumeLayout(False)
        Me.PanelCapDesNet.PerformLayout()
        CType(Me.MuesteServiciosDescuentoNetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTipSerPrincipalDescuentNetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Punto_Pronto_PagoNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Puntos3NumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Puntos6NumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Puntos11NumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel18.ResumeLayout(False)
        Me.Panel18.PerformLayout()
        CType(Me.MUESTRATRABAJOSNOCOBROMENSUALBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.CONSERVICIOSBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONSERVICIOSBindingNavigator.ResumeLayout(False)
        Me.CONSERVICIOSBindingNavigator.PerformLayout()
        CType(Me.BUSCAPuntosPagoAdelantadoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ValidaPeriodosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MODREL_TARIFADOS_SERVICIOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NUEPuntos_Pago_AdelantadoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GUARDARel_Trabajos_NoCobroMensualBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BORRel_Trabajos_NoCobroMensualBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BORGeneralDescuentoNetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Valida_Guarda_GeneralDescuentoNetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorAplicaSoloInternetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NueAplicaSoloInternetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ValidaAplicaSoloInternetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NUEVOClv_EquiBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Valida_borra_servicioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Guarda_GeneralDescuentoNetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NUEREL_TARIFADOS_SERVICIOSALLBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MODREL_TARIFADOS_SERVICIOSALLBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Guarda_GeneralDescuentoNetAllBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONSERVICIOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONSERVICIOSTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONSERVICIOSTableAdapter
    Friend WithEvents Clv_ServicioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_TipSerTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Sale_en_CarteraCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents CONSERVICIOSBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONSERVICIOSBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Genera_OrdenCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents PrecioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MUESTRASOLOTARIFADOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRASOLOTARIFADOSTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRASOLOTARIFADOSTableAdapter
    Friend WithEvents CONREL_TARIFADOS_SERVICIOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONREL_TARIFADOS_SERVICIOSTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONREL_TARIFADOS_SERVICIOSTableAdapter
    Friend WithEvents CLV_LLAVETextBox As System.Windows.Forms.TextBox
    Friend WithEvents CLV_SERVICIOTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents BUSCAREL_TARIFADOS_SERVICIOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents CMBTextBox28 As System.Windows.Forms.TextBox
    Friend WithEvents ValidaPeriodosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ValidaPeriodosTableAdapter As sofTV.NewSofTvDataSetTableAdapters.ValidaPeriodosTableAdapter
    Friend WithEvents MODREL_TARIFADOS_SERVICIOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MODREL_TARIFADOS_SERVICIOSTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MODREL_TARIFADOS_SERVICIOSTableAdapter
    Friend WithEvents PuntoProntoPagoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Puntos3DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Puntos6DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Puntos11DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BUSCAPuntosPagoAdelantadoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BUSCAPuntos_Pago_AdelantadoTableAdapter As sofTV.NewSofTvDataSetTableAdapters.BUSCAPuntos_Pago_AdelantadoTableAdapter
    Friend WithEvents NUEPuntos_Pago_AdelantadoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NUEPuntos_Pago_AdelantadoTableAdapter As sofTV.NewSofTvDataSetTableAdapters.NUEPuntos_Pago_AdelantadoTableAdapter
    Friend WithEvents MuestraTipoPromocionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipoPromocionTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MuestraTipoPromocionTableAdapter
    Friend WithEvents BUSCAREL_TARIFADOS_SERVICIOSTableAdapter As sofTV.NewSofTvDataSetTableAdapters.BUSCAREL_TARIFADOS_SERVICIOSTableAdapter
    Friend WithEvents MUESTRATRABAJOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRATRABAJOSTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRATRABAJOSTableAdapter
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Panel11 As System.Windows.Forms.Panel
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents CMBTextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents Clv_TipoPromocionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents Panel11adicional2 As System.Windows.Forms.Panel
    Friend WithEvents Precio_AdicionalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CMBTextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents PRECIOTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Periodo_InicialDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Periodo_FinalDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents CMBTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents DIA_INICIALNumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents DIA_FINALNumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents CMBTextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Panel12 As System.Windows.Forms.Panel
    Friend WithEvents Clv_TrabajoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
    Friend WithEvents CMBTextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents BRINCA_MESCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents Aplica_ComisionCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents Genera_OrdenCheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents VigenteCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents CLV_LLAVETextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents CLAVETextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents CONREL_TARIFADOS_SERVICIOSBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton4 As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONREL_TARIFADOS_SERVICIOSBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton3 As System.Windows.Forms.ToolStripButton
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents PanelPagoAdelantado As System.Windows.Forms.Panel
    Friend WithEvents CMBTextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Punto_Pronto_PagoNumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents Puntos3NumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents Puntos6NumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents Puntos11NumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Es_PrincipalCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents Sale_en_CarteraCheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents Clv_TxtTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DescripcionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel13 As System.Windows.Forms.Panel
    Friend WithEvents Numero_CortesiasTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel14 As System.Windows.Forms.Panel
    Friend WithEvents Precio_InalambricoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents ComboBox4 As System.Windows.Forms.ComboBox
    Friend WithEvents MUESTRATIPOCLIENTESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DataSetEDGAR As sofTV.DataSetEDGAR
    Friend WithEvents MUESTRA_TIPOCLIENTESTableAdapter As sofTV.DataSetEDGARTableAdapters.MUESTRA_TIPOCLIENTESTableAdapter
    Friend WithEvents Clv_TipoClienteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel15 As System.Windows.Forms.Panel
    Friend WithEvents Panel16 As System.Windows.Forms.Panel
    Friend WithEvents CMBTextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents Panel18 As System.Windows.Forms.Panel
    Friend WithEvents ComboBox5 As System.Windows.Forms.ComboBox
    Friend WithEvents CONRelTrabajosNoCobroMensualBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRATRABAJOSNOCOBROMENSUALBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRATRABAJOS_NOCOBROMENSUALTableAdapter As sofTV.DataSetEDGARTableAdapters.MUESTRATRABAJOS_NOCOBROMENSUALTableAdapter
    Friend WithEvents CONRel_Trabajos_NoCobroMensualTableAdapter As sofTV.DataSetEDGARTableAdapters.CONRel_Trabajos_NoCobroMensualTableAdapter
    Friend WithEvents GUARDARel_Trabajos_NoCobroMensualBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GUARDARel_Trabajos_NoCobroMensualTableAdapter As sofTV.DataSetEDGARTableAdapters.GUARDARel_Trabajos_NoCobroMensualTableAdapter
    Friend WithEvents BORRel_Trabajos_NoCobroMensualBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BORRel_Trabajos_NoCobroMensualTableAdapter As sofTV.DataSetEDGARTableAdapters.BORRel_Trabajos_NoCobroMensualTableAdapter
    Friend WithEvents Porcetaje_DescuentoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ComboBox6 As System.Windows.Forms.ComboBox
    Friend WithEvents MuestraTipSerPrincipalDescuentNetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipSerPrincipalDescuentNetTableAdapter As sofTV.DataSetEDGARTableAdapters.MuestraTipSerPrincipalDescuentNetTableAdapter
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents ComboBox7 As System.Windows.Forms.ComboBox
    Friend WithEvents MuesteServiciosDescuentoNetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuesteServiciosDescuentoNetTableAdapter As sofTV.DataSetEDGARTableAdapters.MuesteServiciosDescuentoNetTableAdapter
    Friend WithEvents Panel17 As System.Windows.Forms.Panel
    Friend WithEvents PanelCapDesNet As System.Windows.Forms.Panel
    Friend WithEvents Agregar1 As System.Windows.Forms.Button
    Friend WithEvents Cancelar1 As System.Windows.Forms.Button
    Friend WithEvents Eliminar1 As System.Windows.Forms.Button
    Friend WithEvents Guardar1 As System.Windows.Forms.Button
    Friend WithEvents Modificar1 As System.Windows.Forms.Button
    Friend WithEvents Se_Cobra_ProporcionalCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents BORGeneralDescuentoNetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BORGeneralDescuentoNetTableAdapter As sofTV.DataSetEDGARTableAdapters.BORGeneralDescuentoNetTableAdapter
    Friend WithEvents Valida_Guarda_GeneralDescuentoNetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Valida_Guarda_GeneralDescuentoNetTableAdapter As sofTV.DataSetEDGARTableAdapters.Valida_Guarda_GeneralDescuentoNetTableAdapter
    Friend WithEvents BrincaMes_Label As System.Windows.Forms.Label
    Friend WithEvents ComboBox8 As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents MUESTRATIPOCLIENTES2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DataSetEdgarRev2 As sofTV.DataSetEdgarRev2
    Friend WithEvents MUESTRA_TIPOCLIENTES_2TableAdapter As sofTV.DataSetEdgarRev2TableAdapters.MUESTRA_TIPOCLIENTES_2TableAdapter
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents BorAplicaSoloInternetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorAplicaSoloInternetTableAdapter As sofTV.DataSetEricTableAdapters.BorAplicaSoloInternetTableAdapter
    Friend WithEvents NueAplicaSoloInternetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NueAplicaSoloInternetTableAdapter As sofTV.DataSetEricTableAdapters.NueAplicaSoloInternetTableAdapter
    Friend WithEvents ValidaAplicaSoloInternetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ValidaAplicaSoloInternetTableAdapter As sofTV.DataSetEricTableAdapters.ValidaAplicaSoloInternetTableAdapter
    Friend WithEvents Panel19 As System.Windows.Forms.Panel
    Friend WithEvents ComboBox9 As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Panel20 As System.Windows.Forms.Panel
    Friend WithEvents DataSetLidia As sofTV.DataSetLidia
    Friend WithEvents Dame_PagosParcialesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_PagosParcialesTableAdapter As sofTV.DataSetLidiaTableAdapters.Dame_PagosParcialesTableAdapter
    Friend WithEvents Dame_ContratacionParcialBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_ContratacionParcialTableAdapter As sofTV.DataSetLidiaTableAdapters.Dame_ContratacionParcialTableAdapter
    Friend WithEvents Dame_ContratacionParcialDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents NUEVOClv_EquiBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NUEVOClv_EquiTableAdapter As sofTV.DataSetLidiaTableAdapters.NUEVOClv_EquiTableAdapter
    Friend WithEvents CONSULTAClv_EquiBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONSULTAClv_EquiTableAdapter As sofTV.DataSetLidiaTableAdapters.CONSULTAClv_EquiTableAdapter
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents Valida_borra_servicioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Valida_borra_servicioTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Valida_borra_servicioTableAdapter
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric2
    Friend WithEvents CONGeneralDescuentoNetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONGeneralDescuentoNetTableAdapter As sofTV.DataSetEric2TableAdapters.CONGeneralDescuentoNetTableAdapter
    Friend WithEvents CONGeneralDescuentoNetDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents Guarda_GeneralDescuentoNetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Guarda_GeneralDescuentoNetTableAdapter As sofTV.DataSetEric2TableAdapters.Guarda_GeneralDescuentoNetTableAdapter
    Friend WithEvents Clv_Servicio_Con_AplicaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_TipSerTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Clv_ServicioTextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents PuntosTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MensDescuentoNetTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ContDescuentoNetTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DescripcionComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents ConceptoComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CMBAplicanComLabel As System.Windows.Forms.Label
    Friend WithEvents CMBSale_en_CarteraLabel As System.Windows.Forms.Label
    Friend WithEvents CMBGenera_OrdenLabel As System.Windows.Forms.Label
    Friend WithEvents CMBEs_PrincipalLabel As System.Windows.Forms.Label
    Friend WithEvents CMBLabel10 As System.Windows.Forms.Label
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents NUEREL_TARIFADOS_SERVICIOSALLBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NUEREL_TARIFADOS_SERVICIOSALLTableAdapter As sofTV.DataSetEric2TableAdapters.NUEREL_TARIFADOS_SERVICIOSALLTableAdapter
    Friend WithEvents MODREL_TARIFADOS_SERVICIOSALLBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MODREL_TARIFADOS_SERVICIOSALLTableAdapter As sofTV.DataSetEric2TableAdapters.MODREL_TARIFADOS_SERVICIOSALLTableAdapter
    Friend WithEvents Panel21 As System.Windows.Forms.Panel
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    Friend WithEvents Guarda_GeneralDescuentoNetAllBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Guarda_GeneralDescuentoNetAllTableAdapter As sofTV.DataSetEric2TableAdapters.Guarda_GeneralDescuentoNetAllTableAdapter
    Friend WithEvents CLVLLAVEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CONCEPTODataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PRECIODataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PrecioAdicionalDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PeriodoInicialDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PeriodoFinalDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DIAINICIALDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DIAFINALDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PorcetajeDescuentoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BRINCAMESDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents AplicaComisionDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents GeneraOrdenDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Promocion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TRABAJO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Numero_Cortesias As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Precio_Inalambrico As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PRECIOLabel1 As System.Windows.Forms.Label
    Friend WithEvents Panel22 As System.Windows.Forms.Panel
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents AplicaIEPSCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents IEPS2CheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents LabelPrecio As System.Windows.Forms.Label
    Friend WithEvents LabelPrecioAdic As System.Windows.Forms.Label
    Friend WithEvents tbPrecio As System.Windows.Forms.TextBox
    Friend WithEvents tbPrecioAdic As System.Windows.Forms.TextBox
    Friend WithEvents HDCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents PnlClaveEquivalente As System.Windows.Forms.Panel
    Friend WithEvents pnlContable As System.Windows.Forms.Panel
    Friend WithEvents txtPosicion As System.Windows.Forms.TextBox
    Friend WithEvents txtCuentaContable As System.Windows.Forms.TextBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter5 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents cbClaveProducto As System.Windows.Forms.ComboBox
End Class
