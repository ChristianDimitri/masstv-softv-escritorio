Imports System.Data.SqlClient
Public Class FrmSubContrato


    Dim BndContrato As Boolean = False
    Dim BndSubContrato As Boolean = False
    Dim eMsg As String = Nothing
    Dim eRes As Integer = 0
    Private Sub guardabitacora(ByVal op As Integer)
        Try
            Select Case op
                Case 0
                    bitsist(GloUsuario, Me.CONTRATOTextBox.Text, LocGloSistema, Me.Name, "Se Agrego Un contrato Maestro", "", "Se Agrego Un Contrato Maestro", LocClv_Ciudad)
                Case 1
                    bitsist(GloUsuario, Me.CONTRATOTextBox.Text, LocGloSistema, Me.Name, "Se Elimino un Contrato Maestro", "", "Se Elimino un contrato Maestro", LocClv_Ciudad)
                Case 2
                    bitsist(GloUsuario, Me.CONTRATOTextBox.Text, LocGloSistema, Me.Name, "Se Agrego Un Subcontrato", "", "Se Agrego un subcontrato:" + Me.CONTRATOLabel3.Text, LocClv_Ciudad)
                Case 3
                    bitsist(GloUsuario, Me.CONTRATOTextBox.Text, LocGloSistema, Me.Name, "Se Elimino Un Subcontrato", "", "Se Elimino un subcontrato:" + Me.CONTRATOLabel3.Text, LocClv_Ciudad)
            End Select
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
    End Sub

    Private Sub FrmSubContrato_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If BndContrato = True Then
            BndContrato = False
            If eGloContrato > 0 Then
                Me.NueContratoMaestroTableAdapter.Connection = CON
                Me.NueContratoMaestroTableAdapter.Fill(Me.DataSetEric.NueContratoMaestro, eGloContrato, 0, eRes, eMsg)

                If eRes = 1 Then
                    MsgBox(eMsg)
                Else
                    'Me.BindingNavigator1.Enabled = False
                    Me.ToolStripButton2.Enabled = False
                    Me.ToolStripButton3.Enabled = True
                    Me.BindingNavigator2.Enabled = True
                    Me.ConContratoMaestroTableAdapter.Connection = CON
                    Me.ConContratoMaestroTableAdapter.Fill(Me.DataSetEric.ConContratoMaestro, eGloContrato, 0, "", "", "", "", 0)
                    eGloContratoAux = eGloContrato
                    guardabitacora(0)
                End If
            End If

        End If
        If BndSubContrato = True Then
            BndSubContrato = False
            If eGloSubContrato > 0 Then
                Me.NueSubContratoTableAdapter.Connection = CON
                Me.NueSubContratoTableAdapter.Fill(Me.DataSetEric.NueSubContrato, Me.CONTRATOTextBox.Text, eGloSubContrato, eRes, eMsg)
                If eRes = 1 Then
                    MsgBox(eMsg)
                Else
                    Me.ConContratoMaestroTableAdapter.Connection = CON
                    Me.ConContratoMaestroTableAdapter.Fill(Me.DataSetEric.ConContratoMaestro, Me.CONTRATOTextBox.Text, 0, "", "", "", "", 0)
                    guardabitacora(2)
                End If
            End If
        End If
        CON.Close()
    End Sub

    Private Sub FrmSubContrato_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Me.GroupBox1.BackColor = Color.Gray
        Me.GroupBox2.BackColor = Color.Gray
        Me.GroupBox3.BackColor = Color.Gray
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.CONTRATOTextBox.BackColor = Me.NOMBRELabel1.BackColor
        If eOpcion = "N" Then
            Me.GroupBox1.Enabled = True
            Me.ToolStripButton3.Enabled = False
            Me.BindingNavigator2.Enabled = False
        End If

        If eOpcion = "C" Then
            eGloContrato = eGloContratoAux
            Me.ConContratoMaestroTableAdapter.Connection = CON
            Me.ConContratoMaestroTableAdapter.Fill(Me.DataSetEric.ConContratoMaestro, eGloContratoAux, 0, "", "", "", "", 0)
            Me.BindingNavigator1.Enabled = False
            Me.BindingNavigator2.Enabled = False
        End If

        If eOpcion = "M" Then
            eGloContrato = eGloContratoAux
            Me.ConContratoMaestroTableAdapter.Connection = CON
            Me.ConContratoMaestroTableAdapter.Fill(Me.DataSetEric.ConContratoMaestro, eGloContratoAux, 0, "", "", "", "", 0)
            'Me.BindingNavigator1.Enabled = False
            Me.ToolStripButton2.Enabled = False
            Me.ToolStripButton3.Enabled = True
        End If
        CON.Close()
    End Sub


    Private Sub ToolStripButton4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton4.Click
        BndSubContrato = True
        eGloContrato = 0
        eGloSubContrato = 0
        BrwSelContrato.Show()
    End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        BndContrato = True
        eGloContrato = 0
        eGloSubContrato = 0
        BrwSelContrato.Show()
    End Sub

    Private Sub CONTRATOTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONTRATOTextBox.TextChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Me.CONTRATOTextBox.Text.Length > 0 Then
            Me.ConSubContratoTableAdapter.Connection = CON
            Me.ConSubContratoTableAdapter.Fill(Me.DataSetEric.ConSubContrato, Me.CONTRATOTextBox.Text, 0, "", "", "", "", 3)
        End If
        CON.Close()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If Me.ConSubContratoDataGridView.RowCount > 0 Then
                Me.BorContratoMaestroTableAdapter.Connection = CON
                Me.BorContratoMaestroTableAdapter.Fill(Me.DataSetEric.BorContratoMaestro, Me.CONTRATOTextBox.Text, Me.CONTRATOLabel3.Text)
                guardabitacora(3)
                MsgBox(mensaje6)
                Me.ConContratoMaestroTableAdapter.Connection = CON
                Me.ConContratoMaestroTableAdapter.Fill(Me.DataSetEric.ConContratoMaestro, eGloContratoAux, 0, "", "", "", "", 0)
            Else
                MsgBox("No Existen SubContratos a Eliminar.", , "Atenci�n")
            End If
            CON.Close()
        Catch
            MsgBox("Se ha Producido un Error.")
        End Try
    End Sub

    Private Sub ToolStripButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton3.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ValidaBorMaestroTableAdapter.Connection = CON
        Me.ValidaBorMaestroTableAdapter.Fill(Me.DataSetEric.ValidaBorMaestro, Me.CONTRATOTextBox.Text, eRes, eMsg)
        If eRes = 1 Then
            MsgBox(eMsg)
        Else
            guardabitacora(1)
            MsgBox(mensaje6)
            Me.Close()
        End If
        CON.Close()
    End Sub
End Class