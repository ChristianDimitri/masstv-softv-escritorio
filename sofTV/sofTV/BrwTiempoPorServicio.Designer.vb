﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwTiempoPorServicio
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.lblTipSer = New System.Windows.Forms.Label()
        Me.CmbTipSer = New System.Windows.Forms.ComboBox()
        Me.CmbTrabajo = New System.Windows.Forms.ComboBox()
        Me.CmbTecnico = New System.Windows.Forms.ComboBox()
        Me.fechaeje2 = New System.Windows.Forms.DateTimePicker()
        Me.fechaeje1 = New System.Windows.Forms.DateTimePicker()
        Me.fechaSol2 = New System.Windows.Forms.DateTimePicker()
        Me.fechaSol1 = New System.Windows.Forms.DateTimePicker()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.FECEJEFIN = New System.Windows.Forms.Label()
        Me.FECEJEINI = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.FECSOLFIN = New System.Windows.Forms.Label()
        Me.FECSOLINI = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.NUMFINLBL = New System.Windows.Forms.Label()
        Me.NUMINILbl = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.CheckBox5 = New System.Windows.Forms.CheckBox()
        Me.CheckBox4 = New System.Windows.Forms.CheckBox()
        Me.CheckBox3 = New System.Windows.Forms.CheckBox()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.NOMTRABAJO = New System.Windows.Forms.Label()
        Me.CLV_COLONIA = New System.Windows.Forms.Label()
        Me.CLV_TRABAJO = New System.Windows.Forms.Label()
        Me.CatalogodeReportesAreaTecnicaBindingSource2 = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewsoftvDataSet1 = New sofTV.NewsoftvDataSet1()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.CatalogodeReportesAreaTecnicaTableAdapter = New sofTV.NewsoftvDataSet1TableAdapters.CatalogodeReportesAreaTecnicaTableAdapter()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.cmbTipoServicio = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.MuestraTipSerPrincipalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Panel4.SuspendLayout()
        CType(Me.CatalogodeReportesAreaTecnicaBindingSource2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.MuestraTipSerPrincipalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.DarkKhaki
        Me.Panel4.Controls.Add(Me.lblTipSer)
        Me.Panel4.Controls.Add(Me.CmbTipSer)
        Me.Panel4.Controls.Add(Me.CmbTrabajo)
        Me.Panel4.Controls.Add(Me.CmbTecnico)
        Me.Panel4.Controls.Add(Me.fechaeje2)
        Me.Panel4.Controls.Add(Me.fechaeje1)
        Me.Panel4.Controls.Add(Me.fechaSol2)
        Me.Panel4.Controls.Add(Me.fechaSol1)
        Me.Panel4.Controls.Add(Me.Label20)
        Me.Panel4.Controls.Add(Me.Label18)
        Me.Panel4.Controls.Add(Me.FECEJEFIN)
        Me.Panel4.Controls.Add(Me.FECEJEINI)
        Me.Panel4.Controls.Add(Me.Label15)
        Me.Panel4.Controls.Add(Me.Label16)
        Me.Panel4.Controls.Add(Me.FECSOLFIN)
        Me.Panel4.Controls.Add(Me.FECSOLINI)
        Me.Panel4.Controls.Add(Me.Label11)
        Me.Panel4.Controls.Add(Me.Label12)
        Me.Panel4.Controls.Add(Me.NUMFINLBL)
        Me.Panel4.Controls.Add(Me.NUMINILbl)
        Me.Panel4.Controls.Add(Me.Label5)
        Me.Panel4.Controls.Add(Me.Label4)
        Me.Panel4.Controls.Add(Me.CheckBox5)
        Me.Panel4.Controls.Add(Me.CheckBox4)
        Me.Panel4.Controls.Add(Me.CheckBox3)
        Me.Panel4.Controls.Add(Me.CheckBox2)
        Me.Panel4.Controls.Add(Me.Label3)
        Me.Panel4.Controls.Add(Me.CheckBox1)
        Me.Panel4.Controls.Add(Me.NOMTRABAJO)
        Me.Panel4.Controls.Add(Me.CLV_COLONIA)
        Me.Panel4.Controls.Add(Me.CLV_TRABAJO)
        Me.Panel4.Location = New System.Drawing.Point(5, 12)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(274, 650)
        Me.Panel4.TabIndex = 16
        Me.Panel4.TabStop = True
        '
        'lblTipSer
        '
        Me.lblTipSer.AutoSize = True
        Me.lblTipSer.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipSer.Location = New System.Drawing.Point(18, 39)
        Me.lblTipSer.Name = "lblTipSer"
        Me.lblTipSer.Size = New System.Drawing.Size(100, 13)
        Me.lblTipSer.TabIndex = 1
        Me.lblTipSer.Text = "Tipo de Servicio"
        '
        'CmbTipSer
        '
        Me.CmbTipSer.DisplayMember = "Concepto"
        Me.CmbTipSer.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.CmbTipSer.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CmbTipSer.ForeColor = System.Drawing.Color.Red
        Me.CmbTipSer.FormattingEnabled = True
        Me.CmbTipSer.Location = New System.Drawing.Point(5, 55)
        Me.CmbTipSer.Name = "CmbTipSer"
        Me.CmbTipSer.Size = New System.Drawing.Size(255, 24)
        Me.CmbTipSer.TabIndex = 201
        Me.CmbTipSer.ValueMember = "Clv_TipSer"
        '
        'CmbTrabajo
        '
        Me.CmbTrabajo.DisplayMember = "Descripcion"
        Me.CmbTrabajo.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.CmbTrabajo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CmbTrabajo.ForeColor = System.Drawing.Color.Red
        Me.CmbTrabajo.FormattingEnabled = True
        Me.CmbTrabajo.Location = New System.Drawing.Point(5, 498)
        Me.CmbTrabajo.Name = "CmbTrabajo"
        Me.CmbTrabajo.Size = New System.Drawing.Size(255, 21)
        Me.CmbTrabajo.TabIndex = 200
        Me.CmbTrabajo.ValueMember = "Clv_trabajo"
        '
        'CmbTecnico
        '
        Me.CmbTecnico.DisplayMember = "Nombre"
        Me.CmbTecnico.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.CmbTecnico.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CmbTecnico.ForeColor = System.Drawing.Color.Red
        Me.CmbTecnico.FormattingEnabled = True
        Me.CmbTecnico.Location = New System.Drawing.Point(5, 415)
        Me.CmbTecnico.Name = "CmbTecnico"
        Me.CmbTecnico.Size = New System.Drawing.Size(255, 21)
        Me.CmbTecnico.TabIndex = 199
        Me.CmbTecnico.ValueMember = "clvTecnico"
        '
        'fechaeje2
        '
        Me.fechaeje2.CustomFormat = "dd/MM/yyyy"
        Me.fechaeje2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fechaeje2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.fechaeje2.Location = New System.Drawing.Point(147, 324)
        Me.fechaeje2.Name = "fechaeje2"
        Me.fechaeje2.Size = New System.Drawing.Size(113, 21)
        Me.fechaeje2.TabIndex = 198
        Me.fechaeje2.Value = New Date(2011, 1, 10, 0, 0, 0, 0)
        '
        'fechaeje1
        '
        Me.fechaeje1.CustomFormat = "dd/MM/yyyy"
        Me.fechaeje1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fechaeje1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.fechaeje1.Location = New System.Drawing.Point(5, 323)
        Me.fechaeje1.Name = "fechaeje1"
        Me.fechaeje1.Size = New System.Drawing.Size(113, 21)
        Me.fechaeje1.TabIndex = 197
        Me.fechaeje1.Value = New Date(2011, 1, 10, 0, 0, 0, 0)
        '
        'fechaSol2
        '
        Me.fechaSol2.CustomFormat = "dd/MM/yyyy"
        Me.fechaSol2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fechaSol2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.fechaSol2.Location = New System.Drawing.Point(147, 227)
        Me.fechaSol2.Name = "fechaSol2"
        Me.fechaSol2.Size = New System.Drawing.Size(113, 21)
        Me.fechaSol2.TabIndex = 196
        Me.fechaSol2.Value = New Date(2011, 1, 10, 0, 0, 0, 0)
        '
        'fechaSol1
        '
        Me.fechaSol1.CustomFormat = "dd/MM/yyyy"
        Me.fechaSol1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fechaSol1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.fechaSol1.Location = New System.Drawing.Point(5, 226)
        Me.fechaSol1.Name = "fechaSol1"
        Me.fechaSol1.Size = New System.Drawing.Size(113, 21)
        Me.fechaSol1.TabIndex = 195
        Me.fechaSol1.Value = New Date(2011, 1, 10, 0, 0, 0, 0)
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.Gainsboro
        Me.Label20.Location = New System.Drawing.Point(12, 482)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(58, 13)
        Me.Label20.TabIndex = 189
        Me.Label20.Text = "Trabajo :"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.Gainsboro
        Me.Label18.Location = New System.Drawing.Point(12, 386)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(112, 13)
        Me.Label18.TabIndex = 187
        Me.Label18.Text = "Nombre Técnico  :"
        '
        'FECEJEFIN
        '
        Me.FECEJEFIN.Location = New System.Drawing.Point(199, 308)
        Me.FECEJEFIN.Name = "FECEJEFIN"
        Me.FECEJEFIN.Size = New System.Drawing.Size(65, 13)
        Me.FECEJEFIN.TabIndex = 186
        '
        'FECEJEINI
        '
        Me.FECEJEINI.Location = New System.Drawing.Point(70, 307)
        Me.FECEJEINI.Name = "FECEJEINI"
        Me.FECEJEINI.Size = New System.Drawing.Size(65, 13)
        Me.FECEJEINI.TabIndex = 185
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Gainsboro
        Me.Label15.Location = New System.Drawing.Point(141, 308)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(52, 13)
        Me.Label15.TabIndex = 184
        Me.Label15.Text = "Hasta  :"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.Gainsboro
        Me.Label16.Location = New System.Drawing.Point(9, 307)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(55, 13)
        Me.Label16.TabIndex = 183
        Me.Label16.Text = "Desde  :"
        '
        'FECSOLFIN
        '
        Me.FECSOLFIN.Location = New System.Drawing.Point(196, 249)
        Me.FECSOLFIN.Name = "FECSOLFIN"
        Me.FECSOLFIN.Size = New System.Drawing.Size(65, 13)
        Me.FECSOLFIN.TabIndex = 182
        '
        'FECSOLINI
        '
        Me.FECSOLINI.Location = New System.Drawing.Point(67, 248)
        Me.FECSOLINI.Name = "FECSOLINI"
        Me.FECSOLINI.Size = New System.Drawing.Size(65, 13)
        Me.FECSOLINI.TabIndex = 181
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Gainsboro
        Me.Label11.Location = New System.Drawing.Point(144, 211)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(52, 13)
        Me.Label11.TabIndex = 180
        Me.Label11.Text = "Hasta  :"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Gainsboro
        Me.Label12.Location = New System.Drawing.Point(12, 210)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(55, 13)
        Me.Label12.TabIndex = 179
        Me.Label12.Text = "Desde  :"
        '
        'NUMFINLBL
        '
        Me.NUMFINLBL.Location = New System.Drawing.Point(196, 124)
        Me.NUMFINLBL.Name = "NUMFINLBL"
        Me.NUMFINLBL.Size = New System.Drawing.Size(65, 14)
        Me.NUMFINLBL.TabIndex = 178
        '
        'NUMINILbl
        '
        Me.NUMINILbl.Location = New System.Drawing.Point(64, 124)
        Me.NUMINILbl.Name = "NUMINILbl"
        Me.NUMINILbl.Size = New System.Drawing.Size(65, 14)
        Me.NUMINILbl.TabIndex = 177
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Gainsboro
        Me.Label5.Location = New System.Drawing.Point(138, 124)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(52, 13)
        Me.Label5.TabIndex = 176
        Me.Label5.Text = "Hasta  :"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Gainsboro
        Me.Label4.Location = New System.Drawing.Point(6, 124)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(55, 13)
        Me.Label4.TabIndex = 174
        Me.Label4.Text = "Desde  :"
        '
        'CheckBox5
        '
        Me.CheckBox5.AutoSize = True
        Me.CheckBox5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox5.Location = New System.Drawing.Point(170, 467)
        Me.CheckBox5.Name = "CheckBox5"
        Me.CheckBox5.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CheckBox5.Size = New System.Drawing.Size(72, 19)
        Me.CheckBox5.TabIndex = 8
        Me.CheckBox5.Text = "Trabajo"
        Me.CheckBox5.UseVisualStyleBackColor = True
        '
        'CheckBox4
        '
        Me.CheckBox4.AutoSize = True
        Me.CheckBox4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox4.Location = New System.Drawing.Point(170, 380)
        Me.CheckBox4.Name = "CheckBox4"
        Me.CheckBox4.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CheckBox4.Size = New System.Drawing.Size(73, 19)
        Me.CheckBox4.TabIndex = 7
        Me.CheckBox4.Text = "Técnico"
        Me.CheckBox4.UseVisualStyleBackColor = True
        '
        'CheckBox3
        '
        Me.CheckBox3.AutoSize = True
        Me.CheckBox3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox3.Location = New System.Drawing.Point(114, 285)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CheckBox3.Size = New System.Drawing.Size(129, 19)
        Me.CheckBox3.TabIndex = 6
        Me.CheckBox3.Text = "Fecha Ejecución"
        Me.CheckBox3.UseVisualStyleBackColor = True
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox2.Location = New System.Drawing.Point(121, 185)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CheckBox2.Size = New System.Drawing.Size(122, 19)
        Me.CheckBox2.TabIndex = 5
        Me.CheckBox2.Text = "Fecha Solicitud"
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.DarkGoldenrod
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(0, 2)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(274, 26)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Búsqueda Por  :"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.Location = New System.Drawing.Point(31, 92)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CheckBox1.Size = New System.Drawing.Size(212, 19)
        Me.CheckBox1.TabIndex = 4
        Me.CheckBox1.Text = "Número de Orden de Servicio"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'NOMTRABAJO
        '
        Me.NOMTRABAJO.Location = New System.Drawing.Point(80, 364)
        Me.NOMTRABAJO.Name = "NOMTRABAJO"
        Me.NOMTRABAJO.Size = New System.Drawing.Size(189, 35)
        Me.NOMTRABAJO.TabIndex = 188
        '
        'CLV_COLONIA
        '
        Me.CLV_COLONIA.ForeColor = System.Drawing.Color.DarkKhaki
        Me.CLV_COLONIA.Location = New System.Drawing.Point(6, 451)
        Me.CLV_COLONIA.Name = "CLV_COLONIA"
        Me.CLV_COLONIA.Size = New System.Drawing.Size(65, 13)
        Me.CLV_COLONIA.TabIndex = 192
        '
        'CLV_TRABAJO
        '
        Me.CLV_TRABAJO.ForeColor = System.Drawing.Color.DarkKhaki
        Me.CLV_TRABAJO.Location = New System.Drawing.Point(17, 386)
        Me.CLV_TRABAJO.Name = "CLV_TRABAJO"
        Me.CLV_TRABAJO.Size = New System.Drawing.Size(65, 13)
        Me.CLV_TRABAJO.TabIndex = 191
        '
        'CatalogodeReportesAreaTecnicaBindingSource2
        '
        Me.CatalogodeReportesAreaTecnicaBindingSource2.DataMember = "CatalogodeReportesAreaTecnica"
        Me.CatalogodeReportesAreaTecnicaBindingSource2.DataSource = Me.NewsoftvDataSet1
        '
        'NewsoftvDataSet1
        '
        Me.NewsoftvDataSet1.DataSetName = "NewsoftvDataSet1"
        Me.NewsoftvDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Olive
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(514, 681)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(226, 33)
        Me.Button1.TabIndex = 19
        Me.Button1.Text = "Imprimir &Listado"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'CatalogodeReportesAreaTecnicaTableAdapter
        '
        Me.CatalogodeReportesAreaTecnicaTableAdapter.ClearBeforeFill = True
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.Olive
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.White
        Me.Button5.Location = New System.Drawing.Point(53, 681)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(165, 33)
        Me.Button5.TabIndex = 21
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'BackgroundWorker1
        '
        Me.BackgroundWorker1.WorkerReportsProgress = True
        Me.BackgroundWorker1.WorkerSupportsCancellation = True
        '
        'cmbTipoServicio
        '
        Me.cmbTipoServicio.ActiveViewIndex = -1
        Me.cmbTipoServicio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.cmbTipoServicio.DisplayGroupTree = False
        Me.cmbTipoServicio.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cmbTipoServicio.Location = New System.Drawing.Point(0, 0)
        Me.cmbTipoServicio.Name = "cmbTipoServicio"
        Me.cmbTipoServicio.SelectionFormula = ""
        Me.cmbTipoServicio.Size = New System.Drawing.Size(712, 650)
        Me.cmbTipoServicio.TabIndex = 0
        Me.cmbTipoServicio.TabStop = False
        Me.cmbTipoServicio.ViewTimeSelectionFormula = ""
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.cmbTipoServicio)
        Me.Panel1.Location = New System.Drawing.Point(285, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(712, 650)
        Me.Panel1.TabIndex = 18
        '
        'MuestraTipSerPrincipalBindingSource
        '
        Me.MuestraTipSerPrincipalBindingSource.DataMember = "MuestraTipSerPrincipal"
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'BrwTiempoPorServicio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1001, 722)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "BrwTiempoPorServicio"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Busqueda de Órdenes por Tiempo de Servicio"
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.CatalogodeReportesAreaTecnicaBindingSource2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.MuestraTipSerPrincipalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents FECEJEFIN As System.Windows.Forms.Label
    Friend WithEvents FECEJEINI As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents FECSOLFIN As System.Windows.Forms.Label
    Friend WithEvents FECSOLINI As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents NUMFINLBL As System.Windows.Forms.Label
    Friend WithEvents NUMINILbl As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents CheckBox5 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox4 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents NOMTRABAJO As System.Windows.Forms.Label
    Friend WithEvents CLV_COLONIA As System.Windows.Forms.Label
    Friend WithEvents CLV_TRABAJO As System.Windows.Forms.Label
    Friend WithEvents CatalogodeReportesAreaTecnicaBindingSource2 As System.Windows.Forms.BindingSource
    Friend WithEvents NewsoftvDataSet1 As sofTV.NewsoftvDataSet1
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents CatalogodeReportesAreaTecnicaTableAdapter As sofTV.NewsoftvDataSet1TableAdapters.CatalogodeReportesAreaTecnicaTableAdapter
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Private WithEvents cmbTipoServicio As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents MuestraTipSerPrincipalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CmbTrabajo As System.Windows.Forms.ComboBox
    Friend WithEvents CmbTecnico As System.Windows.Forms.ComboBox
    Friend WithEvents fechaeje2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents fechaeje1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents fechaSol2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents fechaSol1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents CmbTipSer As System.Windows.Forms.ComboBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents lblTipSer As System.Windows.Forms.Label
End Class
