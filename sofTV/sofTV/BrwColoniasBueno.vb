Imports System.Data.SqlClient
Public Class BrwColoniasBueno

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub llenarGrid(ByRef dt As DataTable)
        DataGridView1.DataSource = Nothing
        DataGridView1.Rows.Clear()
        DataGridView1.DataSource = dt
        DataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect
        DataGridView1.Columns(0).HeaderText = "Clave"
        DataGridView1.Columns(0).AutoSizeMode = DataGridViewAutoSizeColumnMode.NotSet
        DataGridView1.Columns(0).Width = 100
        DataGridView1.Columns(1).HeaderText = "Nombre De La Colonia"
        DataGridView1.Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
    End Sub

    Private Sub BuscaColonia(ByVal op As Integer)
        Dim dt As DataTable
        Select Case op
            Case 0
                dt = sofTV.BAL.Colonia.BusColonia(New System.Nullable(Of Integer)(CType(IIf(Me.TextBox1.Text <> "", CType(Me.TextBox1.Text, Integer), 0), Integer)), "", New System.Nullable(Of Integer)(CType(0, Integer)))
            Case 1
                dt = sofTV.BAL.Colonia.BusColonia(New System.Nullable(Of Integer)(CType(0, Integer)), Me.TextBox2.Text, New System.Nullable(Of Integer)(CType(1, Integer)))
            Case Else
                dt = sofTV.BAL.Colonia.BusColonia(New System.Nullable(Of Integer)(CType(0, Integer)), "", New System.Nullable(Of Integer)(CType(3, Integer)))
        End Select
        llenarGrid(dt)
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Me.BuscaColonia(0)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.BuscaColonia(1)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BuscaColonia(0)
        End If
    End Sub



    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BuscaColonia(1)
        End If
    End Sub



    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opcion = "N"
        FrmColoniasBueno.Show()
    End Sub

    Private Sub consultar()
        If GloClv_COLONIA > 0 Then
            opcion = "C"
            FrmColoniasBueno.Show()
        Else
            MsgBox("Seleccione una Colonia")
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.DataGridView1.RowCount > 0 Then
            consultar()
        Else
            MsgBox(mensaje2)
        End If
    End Sub

    Private Sub modificar()
        If GloClv_COLONIA > 0 Then
            opcion = "M"
            FrmColoniasBueno.Show()
        Else
            MsgBox("Seleccione una Colonia")
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If Me.DataGridView1.RowCount > 0 Then
            modificar()
        Else
            MsgBox(mensaje1)
        End If
    End Sub



    Private Sub Clv_calleLabel2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Clv_calleLabel2.TextChanged
        GloClv_COLONIA = Me.Clv_calleLabel2.Text
    End Sub

    Private Sub BrwColonias_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            Me.BuscaColonia(3)
        End If
    End Sub

    Private Sub BrwColonias_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        'Me.DamePermisosFormTableAdapter.Fill(Me.NewSofTvDataSet.DamePermisosForm, GloTipoUsuario, Me.Name, 1, glolec, gloescr, gloctr)
        If gloescr = 1 And GloTipoUsuario <> 3 And GloTipoUsuario <> 44 Then
            Me.Button2.Enabled = False
            Me.Button4.Enabled = False
        End If
        Me.BuscaColonia(3)
    End Sub


    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If Button3.Enabled = True Then
            consultar()
        ElseIf Button4.Enabled = True Then
            modificar()
        End If
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click

    End Sub

    Private Sub DataGridView1_CurrentCellChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridView1.CurrentCellChanged
        Try

            Clv_calleLabel2.Text = Me.DataGridView1.CurrentRow.Cells(0).Value
            GloClv_COLONIA = Integer.Parse(Me.DataGridView1.CurrentRow.Cells(0).Value)
            CMBNombreTextBox.Text = Me.DataGridView1.CurrentRow.Cells(1).Value
        Catch ex As Exception
        End Try

    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub
End Class