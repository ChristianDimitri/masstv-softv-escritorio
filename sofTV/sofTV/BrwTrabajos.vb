﻿Imports System.Data.SqlClient
Public Class BrwTrabajos
    Private Cerrando As Boolean = False


    Private Sub Clv_TrabajoLabel1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Clv_TrabajoLabel1.TextChanged
        gloClave = Me.Clv_TrabajoLabel1.Text
    End Sub


    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opcion = "N"
        GloClv_TipSer = Me.ComboBox4.SelectedValue
        FrmTrabajos.Show()
    End Sub

    Private Sub consultar()
        If gloClave > 0 Then
            opcion = "C"
            GloClv_TipSer = Me.ComboBox4.SelectedValue
            gloClave = Me.Clv_TrabajoLabel1.Text
            FrmTrabajos.Show()
        Else
            MsgBox(mensaje2)
        End If

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.DataGridView1.RowCount > 0 Then
            consultar()
        Else
            MsgBox(mensaje2)
        End If
    End Sub

    Private Sub modificarFijos()
        If gloClave > 0 Then
            GloClv_TipSer = Me.ComboBox4.SelectedValue
            opcion = "M"
            gloClave = Me.Clv_TrabajoLabel1.Text
            Dim frm As New FrmTrabajos()
            frm.BindingNavigatorDeleteItem.Visible = False
            frm.TRABAJOTextBox.Enabled = False
            frm.DESCRIPCIONTextBox.Enabled = False
            frm.Panel2.Enabled = False
            frm.SICACheckBox.Enabled = False
            frm.Show()
        Else
            MsgBox("Seleccione algun Tipo de Servicio")
        End If
    End Sub

    Private Sub modificar()
        If gloClave > 0 Then
            GloClv_TipSer = Me.ComboBox4.SelectedValue
            opcion = "M"
            gloClave = Me.Clv_TrabajoLabel1.Text
            FrmTrabajos.Show()
        Else
            MsgBox("Seleccione algun Tipo de Servicio")
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click

        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Me.DataGridView1.RowCount > 0 Then
            Me.BUSCAclavesTableAdapter.Connection = CON
            Me.BUSCAclavesTableAdapter.Fill(Me.DataSetLidia.BUSCAclaves, Me.Clv_calleLabel2.Text, Me.CMBNombreTextBox.Text)
            If Me.TextBox3.Text <> "" And Me.TextBox4.Text <> "" Then
                'MsgBox("No Se Puede Modificar el Registro, Ya Que es Fijo", MsgBoxStyle.Information, "Atención")
                modificarFijos()
            Else
                modificar()
            End If
        Else
            MsgBox(mensaje1)
        End If
        Me.TextBox3.Text = ""
        Me.TextBox4.Text = ""
        CON.Close()
    End Sub

    Private Sub Busca(ByVal op As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            If IsNumeric(Me.ComboBox4.SelectedValue) = True Then
                If op = 0 Then
                    If Len(Trim(Me.TextBox1.Text)) > 0 Then
                        Me.BUSCATRABAJOSTableAdapter.Connection = CON
                        Me.BUSCATRABAJOSTableAdapter.Fill(Me.NewSofTvDataSet.BUSCATRABAJOS, Me.TextBox1.Text, Me.ComboBox4.SelectedValue, "", New System.Nullable(Of Integer)(CType(0, Integer)))
                    Else
                        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                    End If

                ElseIf op = 1 Then
                    If Len(Trim(Me.TextBox2.Text)) > 0 Then
                        Me.BUSCATRABAJOSTableAdapter.Connection = CON
                        Me.BUSCATRABAJOSTableAdapter.Fill(Me.NewSofTvDataSet.BUSCATRABAJOS, "", Me.ComboBox4.SelectedValue, Me.TextBox2.Text, New System.Nullable(Of Integer)(CType(1, Integer)))
                    Else
                        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                    End If
                Else
                    Me.BUSCATRABAJOSTableAdapter.Connection = CON
                    Me.BUSCATRABAJOSTableAdapter.Fill(Me.NewSofTvDataSet.BUSCATRABAJOS, "", Me.ComboBox4.SelectedValue, "", New System.Nullable(Of Integer)(CType(2, Integer)))
                End If
                Me.TextBox1.Clear()
                Me.TextBox2.Clear()
            Else
                MsgBox("Seleccione el Tipo de Servicio")
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()

    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Busca(0)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Busca(1)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(0)
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
        End If
    End Sub

    Private Sub BrwTrabajos_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            Busca(2)
        End If
    End Sub

    Private Sub BrwTrabajos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        'Me.DamePermisosFormTableAdapter.Fill(Me.NewSofTvDataSet.DamePermisosForm, GloTipoUsuario, Me.Name, 1, glolec, gloescr, gloctr)
        If gloescr = 1 Then
            Me.Button2.Enabled = False
            Me.Button4.Enabled = False
        End If
        'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerPrincipal' Puede moverla o quitarla según sea necesario.
        Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
        Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)
        Busca(2)
        CON.Close()
    End Sub

    Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.SelectedIndexChanged
        'MsgBox(Me.ComboBox4.SelectedIndex)
        If Me.ComboBox4.SelectedIndex >= 0 Then
            GloClv_TipSer = Me.ComboBox4.SelectedValue
            Busca(2)
        End If
    End Sub



    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If Button3.Enabled = True Then
            consultar()
        ElseIf Button4.Enabled = True Then
            modificar()
        End If
    End Sub



    Private Sub Clv_TrabajoLabel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_TrabajoLabel1.Click
        If IsNumeric(Me.Clv_TrabajoLabel1.Text) = True Then
            gloClave = Me.Clv_TrabajoLabel1.Text
        End If
    End Sub

End Class