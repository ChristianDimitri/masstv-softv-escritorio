﻿Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.Threading

Module ModuleEdoTv

    'Función que genera el Código de Barras del el Estado de Cuenta
    Public Sub AgregaImgCodeBarEstadoDeCuentaTV(ByVal oClv_Recibo As Integer, ByVal strBarCode As String, ByVal prmop As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        conexion.Open()

        Dim comando As New SqlClient.SqlCommand("AgregaImgCodeBarEstadoDeCuentaTV", conexion)

        Try

            'GENERAMOS EL CÓDIGO DE BARRAS DE ESTE ESTADO DE CUENTA
            Dim imgBarCode As Byte()
            imgBarCode = GeneraCodeBar128Nuevo(strBarCode)

            'GUADAMOS EL CÓDIGO DE BARRAS GENERADO
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 0

            comando.Parameters.Add("@Contrato", SqlDbType.BigInt)
            comando.Parameters(0).Direction = ParameterDirection.Input
            comando.Parameters(0).Value = oClv_Recibo

            comando.Parameters.Add("@imgBarCode", SqlDbType.Image)
            comando.Parameters(1).Direction = ParameterDirection.Input
            comando.Parameters(1).Value = imgBarCode

            comando.Parameters.Add("@op", SqlDbType.Int)
            comando.Parameters(2).Direction = ParameterDirection.Input
            comando.Parameters(2).Value = prmop

            comando.ExecuteNonQuery()
            imgBarCode = Nothing

            GC.Collect()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
        End Try

    End Sub
    Public Sub AgregaImgCodeBarEstadoDeCuentaTVPrimerMensualidad(ByVal oClv_Recibo As Integer, ByVal strBarCode As String, ByVal prmop As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        conexion.Open()

        Dim comando As New SqlClient.SqlCommand("AgregaImgCodeBarEstadoDeCuentaTV_PrimerMensualidad", conexion)

        Try

            'GENERAMOS EL CÓDIGO DE BARRAS DE ESTE ESTADO DE CUENTA
            Dim imgBarCode As Byte()
            imgBarCode = GeneraCodeBar128Nuevo(strBarCode)

            'GUADAMOS EL CÓDIGO DE BARRAS GENERADO
            comando.CommandType = CommandType.StoredProcedure

            comando.Parameters.Add("@Contrato", SqlDbType.BigInt)
            comando.Parameters(0).Direction = ParameterDirection.Input
            comando.Parameters(0).Value = oClv_Recibo

            comando.Parameters.Add("@imgBarCode", SqlDbType.Image)
            comando.Parameters(1).Direction = ParameterDirection.Input
            comando.Parameters(1).Value = imgBarCode

            comando.Parameters.Add("@op", SqlDbType.Int)
            comando.Parameters(2).Direction = ParameterDirection.Input
            comando.Parameters(2).Value = prmop

            comando.ExecuteNonQuery()

            conexion.Close()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally

        End Try

    End Sub
End Module

