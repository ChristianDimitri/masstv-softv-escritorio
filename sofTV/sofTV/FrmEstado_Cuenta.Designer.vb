<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmEstado_Cuenta
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.CMBPanel2 = New System.Windows.Forms.Panel
        Me.CaracterComboBox = New System.Windows.Forms.ComboBox
        Me.Muestra_CaracteresBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2
        Me.CMBLabel4 = New System.Windows.Forms.Label
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.Muestra_MesesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CMBLabel2 = New System.Windows.Forms.Label
        Me.CMBLabel1 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.CMBCheckBox8 = New System.Windows.Forms.CheckBox
        Me.CMBPanel1 = New System.Windows.Forms.Panel
        Me.CMBCheckBox3 = New System.Windows.Forms.CheckBox
        Me.CMBCheckBox1 = New System.Windows.Forms.CheckBox
        Me.CMBLabel3 = New System.Windows.Forms.Label
        Me.DataSetarnoldo = New sofTV.DataSetarnoldo
        Me.DameClv_Session_ServiciosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameClv_Session_ServiciosTableAdapter = New sofTV.DataSetarnoldoTableAdapters.DameClv_Session_ServiciosTableAdapter
        Me.Borrar_Session_ServiciosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Borrar_Session_ServiciosTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Borrar_Session_ServiciosTableAdapter
        Me.Borra_temporalesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Borra_temporalesTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Borra_temporalesTableAdapter
        Me.Borra_Rel_Reportes_SoloInternetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Borra_Rel_Reportes_SoloInternetTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Borra_Rel_Reportes_SoloInternetTableAdapter
        Me.InsertaRel_Reporte_Ciudad_Ultimo_mesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertaRel_Reporte_Ciudad_Ultimo_mesTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.InsertaRel_Reporte_Ciudad_Ultimo_mesTableAdapter
        Me.Muestra_CaracteresTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_CaracteresTableAdapter
        Me.Inserta_Rel_Reportes_SoloInternetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Rel_Reportes_SoloInternetTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_Rel_Reportes_SoloInternetTableAdapter
        Me.Muestra_MesesTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_MesesTableAdapter
        Me.CMBPanel3 = New System.Windows.Forms.Panel
        Me.CheckBox3 = New System.Windows.Forms.CheckBox
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.CheckBox2 = New System.Windows.Forms.CheckBox
        Me.CMBPanel2.SuspendLayout()
        CType(Me.Muestra_CaracteresBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Muestra_MesesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CMBPanel1.SuspendLayout()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameClv_Session_ServiciosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Borrar_Session_ServiciosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Borra_temporalesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Borra_Rel_Reportes_SoloInternetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertaRel_Reporte_Ciudad_Ultimo_mesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Rel_Reportes_SoloInternetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CMBPanel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.Olive
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.White
        Me.Button5.Location = New System.Drawing.Point(28, 266)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(341, 33)
        Me.Button5.TabIndex = 5
        Me.Button5.Text = "&IMPRIMIR ESTADOS DE CUENTA"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Olive
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(396, 266)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(173, 33)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'CMBPanel2
        '
        Me.CMBPanel2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CMBPanel2.Controls.Add(Me.CaracterComboBox)
        Me.CMBPanel2.Controls.Add(Me.CMBLabel4)
        Me.CMBPanel2.Controls.Add(Me.ComboBox1)
        Me.CMBPanel2.Controls.Add(Me.CMBLabel2)
        Me.CMBPanel2.Controls.Add(Me.CMBLabel1)
        Me.CMBPanel2.Controls.Add(Me.TextBox1)
        Me.CMBPanel2.Enabled = False
        Me.CMBPanel2.Location = New System.Drawing.Point(46, 141)
        Me.CMBPanel2.Name = "CMBPanel2"
        Me.CMBPanel2.Size = New System.Drawing.Size(482, 104)
        Me.CMBPanel2.TabIndex = 20
        '
        'CaracterComboBox
        '
        Me.CaracterComboBox.DataSource = Me.Muestra_CaracteresBindingSource
        Me.CaracterComboBox.DisplayMember = "caracter"
        Me.CaracterComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CaracterComboBox.FormattingEnabled = True
        Me.CaracterComboBox.Location = New System.Drawing.Point(229, 10)
        Me.CaracterComboBox.Name = "CaracterComboBox"
        Me.CaracterComboBox.Size = New System.Drawing.Size(201, 24)
        Me.CaracterComboBox.TabIndex = 17
        Me.CaracterComboBox.ValueMember = "clv_caracter"
        '
        'Muestra_CaracteresBindingSource
        '
        Me.Muestra_CaracteresBindingSource.DataMember = "Muestra_Caracteres"
        Me.Muestra_CaracteresBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CMBLabel4
        '
        Me.CMBLabel4.AutoSize = True
        Me.CMBLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel4.Location = New System.Drawing.Point(13, 13)
        Me.CMBLabel4.Name = "CMBLabel4"
        Me.CMBLabel4.Size = New System.Drawing.Size(201, 16)
        Me.CMBLabel4.TabIndex = 16
        Me.CMBLabel4.Text = "Buscar Por El Mes que Sea:"
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.Muestra_MesesBindingSource
        Me.ComboBox1.DisplayMember = "mes"
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(229, 40)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(201, 24)
        Me.ComboBox1.TabIndex = 12
        Me.ComboBox1.ValueMember = "clv_mes"
        '
        'Muestra_MesesBindingSource
        '
        Me.Muestra_MesesBindingSource.DataMember = "Muestra_Meses"
        Me.Muestra_MesesBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(179, 73)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(35, 16)
        Me.CMBLabel2.TabIndex = 15
        Me.CMBLabel2.Text = "Año"
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(87, 43)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(127, 16)
        Me.CMBLabel1.TabIndex = 14
        Me.CMBLabel1.Text = "Mes Que Adeuda"
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(229, 70)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(114, 22)
        Me.TextBox1.TabIndex = 13
        '
        'CMBCheckBox8
        '
        Me.CMBCheckBox8.AutoSize = True
        Me.CMBCheckBox8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBCheckBox8.Location = New System.Drawing.Point(50, 117)
        Me.CMBCheckBox8.Name = "CMBCheckBox8"
        Me.CMBCheckBox8.Size = New System.Drawing.Size(277, 20)
        Me.CMBCheckBox8.TabIndex = 19
        Me.CMBCheckBox8.Text = "Buscar Por Mes Que Adeudan y Año"
        Me.CMBCheckBox8.UseVisualStyleBackColor = True
        '
        'CMBPanel1
        '
        Me.CMBPanel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CMBPanel1.Controls.Add(Me.CMBCheckBox3)
        Me.CMBPanel1.Controls.Add(Me.CMBCheckBox1)
        Me.CMBPanel1.Location = New System.Drawing.Point(50, 36)
        Me.CMBPanel1.Name = "CMBPanel1"
        Me.CMBPanel1.Size = New System.Drawing.Size(478, 47)
        Me.CMBPanel1.TabIndex = 18
        '
        'CMBCheckBox3
        '
        Me.CMBCheckBox3.AutoSize = True
        Me.CMBCheckBox3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CMBCheckBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBCheckBox3.Location = New System.Drawing.Point(310, 13)
        Me.CMBCheckBox3.Name = "CMBCheckBox3"
        Me.CMBCheckBox3.Size = New System.Drawing.Size(85, 19)
        Me.CMBCheckBox3.TabIndex = 1
        Me.CMBCheckBox3.Text = "Instalado"
        Me.CMBCheckBox3.UseVisualStyleBackColor = False
        '
        'CMBCheckBox1
        '
        Me.CMBCheckBox1.AutoSize = True
        Me.CMBCheckBox1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CMBCheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBCheckBox1.Location = New System.Drawing.Point(69, 13)
        Me.CMBCheckBox1.Name = "CMBCheckBox1"
        Me.CMBCheckBox1.Size = New System.Drawing.Size(96, 19)
        Me.CMBCheckBox1.TabIndex = 0
        Me.CMBCheckBox1.Text = "Contratado"
        Me.CMBCheckBox1.UseVisualStyleBackColor = False
        '
        'CMBLabel3
        '
        Me.CMBLabel3.AutoSize = True
        Me.CMBLabel3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel3.Location = New System.Drawing.Point(46, 9)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(306, 24)
        Me.CMBLabel3.TabIndex = 17
        Me.CMBLabel3.Text = "Selecciona el Status de Cliente:"
        '
        'DataSetarnoldo
        '
        Me.DataSetarnoldo.DataSetName = "DataSetarnoldo"
        Me.DataSetarnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DameClv_Session_ServiciosBindingSource
        '
        Me.DameClv_Session_ServiciosBindingSource.DataMember = "DameClv_Session_Servicios"
        Me.DameClv_Session_ServiciosBindingSource.DataSource = Me.DataSetarnoldo
        '
        'DameClv_Session_ServiciosTableAdapter
        '
        Me.DameClv_Session_ServiciosTableAdapter.ClearBeforeFill = True
        '
        'Borrar_Session_ServiciosBindingSource
        '
        Me.Borrar_Session_ServiciosBindingSource.DataMember = "Borrar_Session_Servicios"
        Me.Borrar_Session_ServiciosBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Borrar_Session_ServiciosTableAdapter
        '
        Me.Borrar_Session_ServiciosTableAdapter.ClearBeforeFill = True
        '
        'Borra_temporalesBindingSource
        '
        Me.Borra_temporalesBindingSource.DataMember = "Borra_temporales"
        Me.Borra_temporalesBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Borra_temporalesTableAdapter
        '
        Me.Borra_temporalesTableAdapter.ClearBeforeFill = True
        '
        'Borra_Rel_Reportes_SoloInternetBindingSource
        '
        Me.Borra_Rel_Reportes_SoloInternetBindingSource.DataMember = "Borra_Rel_Reportes_SoloInternet"
        Me.Borra_Rel_Reportes_SoloInternetBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Borra_Rel_Reportes_SoloInternetTableAdapter
        '
        Me.Borra_Rel_Reportes_SoloInternetTableAdapter.ClearBeforeFill = True
        '
        'InsertaRel_Reporte_Ciudad_Ultimo_mesBindingSource
        '
        Me.InsertaRel_Reporte_Ciudad_Ultimo_mesBindingSource.DataMember = "InsertaRel_Reporte_Ciudad_Ultimo_mes"
        Me.InsertaRel_Reporte_Ciudad_Ultimo_mesBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'InsertaRel_Reporte_Ciudad_Ultimo_mesTableAdapter
        '
        Me.InsertaRel_Reporte_Ciudad_Ultimo_mesTableAdapter.ClearBeforeFill = True
        '
        'Muestra_CaracteresTableAdapter
        '
        Me.Muestra_CaracteresTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Rel_Reportes_SoloInternetBindingSource
        '
        Me.Inserta_Rel_Reportes_SoloInternetBindingSource.DataMember = "Inserta_Rel_Reportes_SoloInternet"
        Me.Inserta_Rel_Reportes_SoloInternetBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Inserta_Rel_Reportes_SoloInternetTableAdapter
        '
        Me.Inserta_Rel_Reportes_SoloInternetTableAdapter.ClearBeforeFill = True
        '
        'Muestra_MesesTableAdapter
        '
        Me.Muestra_MesesTableAdapter.ClearBeforeFill = True
        '
        'CMBPanel3
        '
        Me.CMBPanel3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CMBPanel3.Controls.Add(Me.CheckBox3)
        Me.CMBPanel3.Controls.Add(Me.CheckBox1)
        Me.CMBPanel3.Controls.Add(Me.CheckBox2)
        Me.CMBPanel3.Location = New System.Drawing.Point(46, 36)
        Me.CMBPanel3.Name = "CMBPanel3"
        Me.CMBPanel3.Size = New System.Drawing.Size(482, 47)
        Me.CMBPanel3.TabIndex = 19
        '
        'CheckBox3
        '
        Me.CheckBox3.AutoSize = True
        Me.CheckBox3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CheckBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox3.Location = New System.Drawing.Point(189, 13)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(102, 19)
        Me.CheckBox3.TabIndex = 2
        Me.CheckBox3.Text = "Suspendido"
        Me.CheckBox3.UseVisualStyleBackColor = False
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.Location = New System.Drawing.Point(297, 13)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(182, 19)
        Me.CheckBox1.TabIndex = 1
        Me.CheckBox1.Text = "Desconectado Temporal"
        Me.CheckBox1.UseVisualStyleBackColor = False
        Me.CheckBox1.Visible = False
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CheckBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox2.Location = New System.Drawing.Point(16, 13)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(117, 19)
        Me.CheckBox2.TabIndex = 0
        Me.CheckBox2.Text = "Desconectado"
        Me.CheckBox2.UseVisualStyleBackColor = False
        '
        'FrmEstado_Cuenta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(589, 324)
        Me.Controls.Add(Me.CMBPanel3)
        Me.Controls.Add(Me.CMBPanel2)
        Me.Controls.Add(Me.CMBCheckBox8)
        Me.Controls.Add(Me.CMBPanel1)
        Me.Controls.Add(Me.CMBLabel3)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button5)
        Me.MaximizeBox = False
        Me.Name = "FrmEstado_Cuenta"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Impresión de Estados de Cuenta"
        Me.CMBPanel2.ResumeLayout(False)
        Me.CMBPanel2.PerformLayout()
        CType(Me.Muestra_CaracteresBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Muestra_MesesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CMBPanel1.ResumeLayout(False)
        Me.CMBPanel1.PerformLayout()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameClv_Session_ServiciosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Borrar_Session_ServiciosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Borra_temporalesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Borra_Rel_Reportes_SoloInternetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertaRel_Reporte_Ciudad_Ultimo_mesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Rel_Reportes_SoloInternetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CMBPanel3.ResumeLayout(False)
        Me.CMBPanel3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents CMBPanel2 As System.Windows.Forms.Panel
    Friend WithEvents CaracterComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents CMBLabel4 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents CMBCheckBox8 As System.Windows.Forms.CheckBox
    Friend WithEvents CMBPanel1 As System.Windows.Forms.Panel
    Friend WithEvents CMBCheckBox3 As System.Windows.Forms.CheckBox
    Friend WithEvents CMBCheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents DataSetarnoldo As sofTV.DataSetarnoldo
    Friend WithEvents DameClv_Session_ServiciosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameClv_Session_ServiciosTableAdapter As sofTV.DataSetarnoldoTableAdapters.DameClv_Session_ServiciosTableAdapter
    Friend WithEvents Borrar_Session_ServiciosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Borrar_Session_ServiciosTableAdapter As sofTV.DataSetarnoldoTableAdapters.Borrar_Session_ServiciosTableAdapter
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents Borra_temporalesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Borra_temporalesTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Borra_temporalesTableAdapter
    Friend WithEvents Borra_Rel_Reportes_SoloInternetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Borra_Rel_Reportes_SoloInternetTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Borra_Rel_Reportes_SoloInternetTableAdapter
    Friend WithEvents InsertaRel_Reporte_Ciudad_Ultimo_mesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertaRel_Reporte_Ciudad_Ultimo_mesTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.InsertaRel_Reporte_Ciudad_Ultimo_mesTableAdapter
    Friend WithEvents Muestra_CaracteresBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_CaracteresTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_CaracteresTableAdapter
    Friend WithEvents Inserta_Rel_Reportes_SoloInternetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Rel_Reportes_SoloInternetTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_Rel_Reportes_SoloInternetTableAdapter
    Friend WithEvents Muestra_MesesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_MesesTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Muestra_MesesTableAdapter
    Friend WithEvents CMBPanel3 As System.Windows.Forms.Panel
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
End Class
