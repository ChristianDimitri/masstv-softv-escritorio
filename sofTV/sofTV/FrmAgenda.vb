Imports System.Data.SqlClient
Imports System.Text

Public Class FrmAgenda
    Dim tipo As String
    Dim ClaveHora As Integer
    Dim ResValida As Integer = 0

    Private Sub FrmAgenda_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

        If GLOCONTRATOSEL > 0 Then
            Me.ContratoTextBox.Text = GLOCONTRATOSEL
            GLOCONTRATOSEL = 0
            Me.BUSCACLIENTES(0)
        End If
        Me.BUSCACLIENTES(0)
    End Sub


    Private Sub CONSULTARREL_CITAS()
        Dim Con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        'Contrato
        If Contrato = Nothing Then Contrato = 0
        Try
            'Muestra_Plazo_Forzozo (@contrato bigint,@Clv_unicanet bigint,@Plazo varchar(10) output)
            cmd = New SqlCommand()
            Con.Open()
            With cmd
                .CommandText = "CONSULTARREL_CITAS"
                .Connection = Con
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@Clv_Cita", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = Me.Clv_CitaTextBox.Text
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@Comentario", SqlDbType.VarChar, 250)
                prm1.Direction = ParameterDirection.Output
                'If IsNumeric(Me.TextCosto.Text) = True Then
                'prm1.Value = Me.TextCosto.Text
                'Else
                'prm1.Value = 0
                'End If

                .Parameters.Add(prm1)

                Dim ia As Integer = .ExecuteNonQuery()

                Me.TextComentario.Text = prm1.Value

            End With
            Con.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

   

    Private Sub creaarbol()
        Dim CON As New SqlConnection(MiConexion)

        Try
            Dim I As Integer = 0
            Dim X As Integer = 0


            If IsNumeric(Me.ContratoTextBox.Text) = True Then
                CON.Open()
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)))
                CON.Close()
            Else
                CON.Open()
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(0, Long)))
                CON.Close()
            End If
            Dim pasa As Boolean = False
            Dim Net As Boolean = False
            Dim dig As Boolean = False
            Dim jNet As Integer = -1
            Dim PasaJNet As Boolean = False
            Dim jDig As Integer = -1
            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.dameSerDELCli.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                'MsgBox(Mid(FilaRow("Servicio").ToString(), 1, 21))
                If Mid(FilaRow("Servicio").ToString(), 1, 3) = "---" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    Net = False
                    dig = False
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 19) = "Servicio Televisi�n" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 31) = "Servicios de Televisi�n Digital" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 21) = "Servicios de Internet" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 22) = "Servicios de Tel�fonia" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                Else
                    If Mid(FilaRow("Servicio").ToString(), 1, 14) = "Mac Cablemodem" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jNet = jNet + 1
                        pasa = False
                        Net = True
                    ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Aparato Digital" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jDig = jDig + 1
                        pasa = False
                        dig = True
                    Else
                        If Net = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jNet).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        ElseIf dig = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jDig).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        Else
                            Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                            pasa = False
                        End If
                    End If
                End If
                If pasa = True Then I = I + 1
            Next

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub



    Private Sub BUSCACLIENTES(ByVal OP As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Try
            If IsNumeric(Me.ContratoTextBox.Text) = True Then
                CON.Open()
                Me.BUSCLIPORCONTRATO2TableAdapter1.Connection = CON
                Me.BUSCLIPORCONTRATO2TableAdapter1.Fill(Me.Procedimientosarnoldo4.BUSCLIPORCONTRATO2, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), New System.Nullable(Of Integer)(CType(0, Integer)))
                CON.Close()
                CREAARBOL()
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub FrmAgenda_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles Me.DragEnter

    End Sub

    Private Sub FrmAgenda_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)

        colorea(Me, Me.Name)
        If opcion = "N" Then
            Me.CONCITASBindingSource.AddNew()
            Button1.Enabled = True
        ElseIf opcion = "C" Then
            Me.buscacita(gloClave)
            Me.ComboBox2.SelectedValue = GloClv_tecnico
            Dame_Tipo()
            Panel1.Enabled = False
            If tipo = "O" Then
                Me.Panel2.Visible = True
                DetalleOrden()
            End If
            CON.Open()
            Me.BUSCADetCitasTableAdapter.Connection = CON
            Me.BUSCADetCitasTableAdapter.Fill(Me.NewSofTvDataSet.BUSCADetCitas, Me.Clv_CitaTextBox.Text)
            CON.Close()
            CONSULTARREL_CITAS()
        ElseIf opcion = "M" Then
            Me.ComboBox2.SelectedValue = GloClv_tecnico
            Me.buscacita(gloClave)
            Dame_Tipo()
            CON.Open()
            Me.BUSCADetCitasTableAdapter.Connection = CON
            Me.BUSCADetCitasTableAdapter.Fill(Me.NewSofTvDataSet.BUSCADetCitas, Me.Clv_CitaTextBox.Text)
            CON.Close()
            CONSULTARREL_CITAS()
            If tipo = "O" Then
                Me.Panel2.Visible = True
                DetalleOrden()
            End If

        End If
        If IsNumeric(ContratoTextBox.Text) = True Then
            LlenaTecnico(ContratoTextBox.Text)
        Else
            LlenaTecnico(0)
        End If

        Me.TextBox1.Text = GLONOM_TECNICO
        Me.Clv_TecnicoTextBox.Text = GloClv_tecnico
        Me.Clv_TecnicoTextBox.Enabled = False
        Me.ComboBox2.SelectedValue = GloClv_tecnico
        CON.Open()
        'LlenaCombo(Me.DateTimePicker1.Text, GloClv_tecnico)
        If opcion = "N" Then
            Me.DateTimePicker1.Value = Today
        Else
            Me.DateTimePicker1.Value = ObtenerFechaCita(Me.Clv_CitaTextBox.Text)
        End If

        Me.ComboBox1.DataSource = spConsultaTurnos()
        opcionprimera = 0
        Me.BUSCADetCitasTableAdapter.Connection = CON
        Me.BUSCADetCitasTableAdapter.Fill(Me.NewSofTvDataSet.BUSCADetCitas, Me.Clv_CitaTextBox.Text)
        CON.Close()
        Me.ComboBox1.Text = ObtenerTurnoCita(Me.Clv_CitaTextBox.Text)


    End Sub
    Private Sub Dame_Tipo()
        Dim conlidia As New SqlClient.SqlConnection(MiConexion)
        Dim comando As New SqlClient.SqlCommand
        conlidia.Open()
        With comando
            .CommandText = "Dame_TipoCita"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = conlidia
            Dim prm As New SqlParameter("@Clv_Cita", SqlDbType.BigInt)
            prm.Direction = ParameterDirection.Input
            prm.Value = Me.Clv_CitaTextBox.Text
            .Parameters.Add(prm)
            Dim prm1 As New SqlParameter("@Tipo", SqlDbType.VarChar, 1)
            prm1.Direction = ParameterDirection.Output
            prm1.Value = ""
            .Parameters.Add(prm1)
            Dim i As Integer = comando.ExecuteNonQuery
            If IsDBNull(prm1.Value) Then
                MsgBox("Cita no existe o tiene algun problema")
                tipo = ""
            Else
                tipo = prm1.Value
            End If

        End With
        conlidia.Close()
    End Sub

    Public Sub DetalleOrden()
        Dim CON As New SqlConnection(MiConexion)
        Try

            Dim I As Integer = 0
            Dim X As Integer = 0

            If IsNumeric(Me.TextBox2.Text) = True Then
                CON.Open()
                Me.Dame_DetOrdSerTableAdapter.Connection = CON
                Me.Dame_DetOrdSerTableAdapter.Fill(Me.DataSetLidia2.Dame_DetOrdSer, New System.Nullable(Of Long)(CType(Me.TextBox2.Text, Long)))
                CON.Close()
                Dim FilaRow As DataRow
                Me.TreeView2.Nodes.Clear()
                For Each FilaRow In Me.DataSetLidia2.Dame_DetOrdSer.Rows
                    Me.TreeView2.Nodes.Add(Trim(FilaRow("descripcion").ToString()))
                    I += 1
                Next
                Me.TreeView2.ExpandAll()
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub buscacita(ByVal gloclv_cita As Integer)
        Dim CON As New SqlConnection(MiConexion)

        Try
            CON.Open()
            Me.CONCITASTableAdapter.Connection = CON
            Me.CONCITASTableAdapter.Fill(Me.NewSofTvDataSet.CONCITAS, New System.Nullable(Of Integer)(CType(gloclv_cita, Integer)))
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub CONCITASBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONCITASBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)

        Dim clave As Integer = 0
        If Len(Me.ComboBox1.Text) = 0 Then
            MsgBox("Se requiere el turno")
            Exit Sub
        End If
        If IsNumeric(Me.Clv_TecnicoTextBox.Text) = False Then
            MsgBox("Se requiere un T�cnico")
            Exit Sub
        End If
        If IsNumeric(Me.ContratoTextBox.Text) = False Then
            MsgBox("Se requiere un Contrato")
            Exit Sub
        End If

        Me.Validate()
        GLONOM_TECNICO = Me.ComboBox2.Text
        GloClv_tecnico = Me.ComboBox2.SelectedValue
        HoraAgenda = "6:00"
        ClaveHora = Me.ComboBox1.SelectedValue

        'ValidaDisponiblidadHoraTecnico(Me.DateTimePicker1.Value, GloClv_tecnico, ClaveHora)
        'If ResValida = 1 Then
        '    MsgBox("El T�cnico ya tiene asignada La Hora Seleccionada ", MsgBoxStyle.Exclamation)
        '    Exit Sub
        'Else
        'If Len(Me.Clv_CitaTextBox.Text) = 0 Then
        '    Exit Sub
        'End If
        Modifica_Rel_Citas(Me.Clv_CitaTextBox.Text, GloClv_tecnico, Me.TextComentario.Text, Me.DateTimePicker1.Value, ClaveHora, Me.ComboBox1.Text)
        bitsist(GloUsuario, 0, LocGloSistema, "Agenda del T�cnico", "", "Dar de alta nueva cita: " + CStr(Me.Clv_CitaTextBox.Text), GloSucursal, LocClv_Ciudad)
        MsgBox(mensaje5)
        'NUEMOVREL_CITAS(Me.Clv_CitaTextBox.Text, Me.TextComentario.Text)
        'bitsist(GloUsuario, 0, LocGloSistema, "Agenda del T�cnico", "", "Dar de alta nueva cita: " + CStr(Me.Clv_CitaTextBox.Text), GloSucursal, LocClv_Ciudad)
        ' MsgBox(mensaje5)
        FechaAgenda = ""
        HoraAgenda = ""
        ClaveHora = 0
        GloBnd = True
        Me.Close()
        'End If

    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)

        If IsNumeric(Me.Clv_CitaTextBox.Text) = True Then
            bitsist(GloUsuario, Me.ContratoTextBox.Text, LocGloSistema, "Agenda del T�cnico", "", "Eliminaci�n de Cita: " + CStr(Me.Clv_CitaTextBox.Text), "T�cnico: " + CStr(Me.TextBox1.Text), LocClv_Ciudad)
            CON.Open()
            Me.CONCITASTableAdapter.Connection = CON
            Me.CONCITASTableAdapter.Delete(Me.Clv_CitaTextBox.Text)
            CON.Close()
            MsgBox(mensaje6)
            GloBnd = True
            Me.Close()
        End If

    End Sub
    Private Sub Modifica_Rel_Citas(ByVal Clv_Cita As Long, ByVal ClaveTecnico As Long, ByVal Comentario As String, ByVal Fecha As Date, ByVal ClaveHora As Integer, _
                                   ByVal prmTurnoAgenda As String)
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder
        Dim cmd As New SqlCommand("MODIFICA_REL_CITAS", CON)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Clv_Cita", CStr(Clv_Cita))
        cmd.Parameters.AddWithValue("@ClaveTecnico", CStr(ClaveTecnico))
        cmd.Parameters.AddWithValue("@Comentario", Comentario)
        cmd.Parameters.AddWithValue("@Fecha", Fecha)
        cmd.Parameters.AddWithValue("@ClaveHora", ClaveHora)
        cmd.Parameters.AddWithValue("@TURNO", prmTurnoAgenda)
        Dim DA As New SqlDataAdapter(cmd)
        Dim DT As New DataTable
        Dim BS As New BindingSource

        Try
            CON.Open()
            cmd.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        GLOCONTRATOSEL = 0
        FrmSelCliente.Show()
    End Sub

    Private Sub ContratoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContratoTextBox.TextChanged
        Me.BUSCACLIENTES(0)
    End Sub


    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Me.CONCITASBindingSource.CancelEdit()
        GloBnd = True
        Me.Close()
    End Sub


    Private Sub DateTimePicker1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DateTimePicker1.TextChanged
        Dim CON As New SqlConnection(MiConexion)

        If IsDate(Me.DateTimePicker1.Text) = True Then 'And IsNumeric(Me.Clv_TecnicoTextBox.Text) = True Then
            CON.Open()
            Me.MUESTRAHORAS_CITASTableAdapter.Connection = CON
            Me.MUESTRAHORAS_CITASTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRAHORAS_CITAS, Me.DateTimePicker1.Text, Me.Clv_TecnicoTextBox.Text)
            CON.Close()
        End If

    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Dim CON As New SqlConnection(MiConexion)

        If IsDate(Me.DateTimePicker1.Text) = True Then ' And IsNumeric(Me.Clv_TecnicoTextBox.Text) = True Then
            CON.Open()
            Me.MUESTRAHORAS_CITASTableAdapter.Connection = CON
            Me.MUESTRAHORAS_CITASTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRAHORAS_CITAS, Me.DateTimePicker1.Text, Me.Clv_TecnicoTextBox.Text)
            CON.Close()
        End If

    End Sub
    Private Sub bUSCAqUEJA()
        Dim CON As New SqlConnection(MiConexion)

        Try
            Dame_Tipo()
            If tipo = "Q" Then
                CON.Open()
                Me.VERQUEJAS_CITASTableAdapter.Connection = CON
                Me.VERQUEJAS_CITASTableAdapter.Fill(Me.NewSofTvDataSet.VERQUEJAS_CITAS, New System.Nullable(Of Long)(CType(Me.Clv_CitaTextBox.Text, Long)))
                CON.Close()
            ElseIf tipo = "O" Then
                CON.Open()
                Me.VERORDENES_CITASTableAdapter.Connection = CON
                Me.VERORDENES_CITASTableAdapter.Fill(Me.DataSetLidia2.VERORDENES_CITAS, New System.Nullable(Of Long)(CType(Me.Clv_CitaTextBox.Text, Long)))
                CON.Close()
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Clv_CitaTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_CitaTextBox.TextChanged
        If IsNumeric(Me.Clv_CitaTextBox.Text) = True Then
            bUSCAqUEJA()
        End If
    End Sub
    Private Sub LlenaCombo(ByVal FEcha As String, ByVal ClvTecnico As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder

        StrSQL.Append("EXEC MUESTRAHORAS_CITAS ")
        StrSQL.Append("'" & FEcha & "',")
        StrSQL.Append(CStr(ClvTecnico))

        Dim DA As New SqlDataAdapter(StrSQL.ToString, CON)
        Dim DT As New DataTable
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            ComboBox1.DataSource = BS.DataSource
            HoraAgenda = Me.ComboBox1.Text
            Me.ComboBox1.FindStringExact(HoraAgenda, 0)
            ClaveHora = Me.ComboBox1.SelectedValue
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try

    End Sub
    Private Sub LlenaTecnico(ByVal OP As Integer)

        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder

        StrSQL.Append("EXEC Muestra_Tecnicos_Almacen2 ")

        StrSQL.Append(CStr(OP))

        Dim DA As New SqlDataAdapter(StrSQL.ToString, CON)
        Dim DT As New DataTable
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.ComboBox2.DataSource = BS.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try


    End Sub
    Private Sub Clv_QuejaLabel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_QuejaLabel.Click

    End Sub


    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'GLONOM_TECNICO = Me.ComboBox2.Text
        'GloClv_tecnico = Me.ComboBox2.SelectedValue
        'FechaAgenda = Me.DateTimePicker1.Text
        'FrmTrabajosTecnico.Show()


    End Sub
    Private Sub ValidaDisponiblidadHoraTecnico(ByVal Fecha As DateTime, ByVal Clv_tecnico As Integer, ByVal Clv_hora As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("Valida_HORAS_CITAS", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@FECHA", SqlDbType.DateTime)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Fecha
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@CLV_TECNICO", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_tecnico
        comando.Parameters.Add(parametro2)

        Dim parametro4 As New SqlParameter("@Clv_Hora", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = Clv_hora
        comando.Parameters.Add(parametro4)

        Dim parametro3 As New SqlParameter("@Res", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Output
        parametro3.Value = 0
        comando.Parameters.Add(parametro3)


        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            ResValida = CInt(parametro3.Value)

            conexion.Close()
        Catch ex As Exception

            conexion.Close()
            MsgBox(ex.Message)


        End Try

    End Sub


    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged

    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        If opcion <> "C" Then
            If opcionprimera = 0 Or opcion <> "M" Then
                GLONOM_TECNICO = Me.ComboBox2.Text
                GloClv_tecnico = Me.ComboBox2.SelectedValue
                FechaAgenda = Me.DateTimePicker1.Text
                FrmTrabajosTecnico.Show()


            End If
        End If

    End Sub

    Private Function ObtenerFechaCita(ByVal prmClaveCita As Integer) As Date
        Dim Con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()

        If Contrato = Nothing Then Contrato = 0
        Try
            cmd = New SqlCommand()
            Con.Open()
            With cmd
                .CommandText = "CONSULTARREL_CITAS_fecha"
                .Connection = Con
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@Clv_Cita", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = prmClaveCita
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@Fecha", SqlDbType.DateTime)
                prm1.Direction = ParameterDirection.Output
                .Parameters.Add(prm1)

                Dim ia As Integer = .ExecuteNonQuery()

                Return prm1.Value

            End With
            Con.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Function


End Class