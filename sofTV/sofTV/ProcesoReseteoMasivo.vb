Imports System.Data.Sql
Imports System.Data.SqlClient


Public Class ProcesoReseteoMasivo

    Dim res As Integer = 0
    Private Sub ProcesoReseteoMasivo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)

        Me.Button1.BackColor = Color.Gray

        Me.Button2.BackColor = Color.Gray
    End Sub

    Private Function Ejecutar(ByVal inicio As Int64, ByVal fin As Int64) As Integer
        Dim CON As New SqlConnection(MiConexion)
        Dim comando As SqlCommand = New SqlCommand("ResetServCteTodos", CON)
        comando.CommandType = CommandType.StoredProcedure
        comando.Parameters.Add(New SqlParameter("@inicio", inicio))
        comando.Parameters.Add(New SqlParameter("@final", fin))
        comando.Parameters.Add(New SqlParameter("@Res", SqlDbType.Int))
        comando.Parameters("@Res").Value = 0
        comando.Parameters("@Res").Direction = ParameterDirection.Output
        comando.Parameters.Add(New SqlParameter("@Msg", SqlDbType.VarChar))
        comando.Parameters("@Msg").Value = ""
        comando.Parameters("@Msg").Direction = ParameterDirection.Output
        Dim resultado As Integer = 1
        Try
            CON.Open()
            comando.ExecuteReader()
            resultado = comando.Parameters("@Res").Value.ToString()
        Catch ex As Exception
            Windows.Forms.MessageBox.Show("Hubo un error durante el proceso de conexion a la base de datos.", "Error")

        End Try
        CON.Close()

        Return resultado
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim inicio As Integer
        Dim fin As Integer
        Try
            inicio = Convert.ToInt64(TextBox1.Text.ToString.Trim)
            fin = Convert.ToInt64(TextBox2.Text.ToString.Trim)
        Catch ex As Exception
            MessageBox.Show("Los datos deben de ser numericos", "Error")
        End Try
        If inicio < fin Then
            CMBLabel1.Visible = True
            ProgressBar1.Visible = True
            Me.Refresh()
            ProgressBar1.Maximum = 100
            ProgressBar1.Minimum = 0
            ProgressBar1.Value = 50
            res = Me.Ejecutar(inicio, fin)
            ProgressBar1.Value = 100
            If res = 0 Then
                MessageBox.Show("El Proceso se Realiz� con �xito.", "Aviso")
            Else
                MessageBox.Show("No se realiz� el Proceso Satisfactoriamente.", "Aviso")
            End If
        Else
            MessageBox.Show("Los rangos son incorrectos favor de verificarlos", "Error")
        End If
        CMBLabel1.Visible = False
        ProgressBar1.Visible = False
        ProgressBar1.Value = 0
        Me.Refresh()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
End Class