Imports System.Data.SqlClient
Imports System.Text

Public Class BrwAtenTel



    Private Sub BrwAtenTel_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        ConAtenTelPORSEPARADO(0, "", "", "", "", 0, 0, Today, "", "", 0)
    End Sub

    Private Sub BrwAtenTel_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Me.DateTimePicker1.Value = Today
        ConAtenTelPORSEPARADO(0, "", "", "", "", 0, 0, Today, "", "", 0)

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        'Dim CON As New SqlConnection(MiConexion)
        If Len(Me.TextBox1.Text) = 0 And Len(Me.APaternoTextBox.Text) = 0 And Len(Me.AMaternoTextBox.Text) = 0 Then
            MsgBox("Ingrese datos v�lidos para una b�squeda m�s precisa", MsgBoxStyle.Information)
            ConAtenTelPORSEPARADO(0, "", "", "", "", 0, 0, Today, "", "", 0)
        Else
            ConAtenTelPORSEPARADO(0, Me.TextBox1.Text, APaternoTextBox.Text, AMaternoTextBox.Text, "", 0, 0, Today, "", "", 1)

            Me.TextBox1.Clear()
            Me.APaternoTextBox.Clear()
            Me.AMaternoTextBox.Clear()
            Me.TextBox2.Clear()
        End If
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        'Dim CON As New SqlConnection(MiConexion)
        'If InStr(1, "0123456789,-" & Chr(8), e.KeyChar) = 0 Then 'VALIDACI�N PARA QUE S�LO ADMITA VALORES NUM�RICOS EN EL TEXTBOX (txtCP)
        '    e.KeyChar = ""
        'End If
        If Asc(e.KeyChar) = 13 Then
            If Len(Me.TextBox1.Text) = 0 And Len(Me.APaternoTextBox.Text) = 0 And Len(Me.AMaternoTextBox.Text) = 0 Then
                MsgBox("Ingrese datos v�lidos para una b�squeda m�s precisa", MsgBoxStyle.Information)
                ConAtenTelPORSEPARADO(0, "", "", "", "", 0, 0, Today, "", "", 0)
            Else
                ConAtenTelPORSEPARADO(0, Me.TextBox1.Text, APaternoTextBox.Text, AMaternoTextBox.Text, "", 0, 0, Today, "", "", 1)

                Me.TextBox1.Clear()
                Me.APaternoTextBox.Clear()
                Me.AMaternoTextBox.Clear()
                Me.TextBox2.Clear()
            End If
        End If
    End Sub


    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        'Dim CON As New SqlConnection(MiConexion)
        If Len(Me.TextBox2.Text) = 0 Then
            MsgBox("Ingrese datos v�lidos para una b�squeda m�s precisa", MsgBoxStyle.Information)
            ConAtenTelPORSEPARADO(0, "", "", "", "", 0, 0, Today, "", "", 0)
        Else
            ConAtenTelPORSEPARADO(0, "", "", "", Me.TextBox2.Text, 0, 0, Today, "", "", 2)

            Me.TextBox1.Clear()
            Me.APaternoTextBox.Clear()
            Me.AMaternoTextBox.Clear()
            Me.TextBox2.Clear()
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        'Dim CON As New SqlConnection(MiConexion)
        If Asc(e.KeyChar) = 13 Then

            If Len(Me.TextBox2.Text) = 0 Then
                MsgBox("Ingrese datos v�lidos para una b�squeda m�s precisa", MsgBoxStyle.Information)
                ConAtenTelPORSEPARADO(0, "", "", "", "", 0, 0, Today, "", "", 0)
            Else
                ConAtenTelPORSEPARADO(0, "", "", "", Me.TextBox2.Text, 0, 0, Today, "", "", 2)

                Me.TextBox1.Clear()
                Me.APaternoTextBox.Clear()
                Me.AMaternoTextBox.Clear()
                Me.TextBox2.Clear()
            End If
        End If
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Dim CON As New SqlConnection(MiConexion)

        ConAtenTelPORSEPARADO(0, "", "", "", "", 0, 0, Me.DateTimePicker1.Value, "", "", 5)

        Me.TextBox1.Clear()
        Me.TextBox2.Clear()
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eOpcion = "N"
        FrmAtenTel.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Me.ConAtenTelDataGridView.RowCount > 0 Then
            eOpcion = "C"
            eClave = Me.ConAtenTelDataGridView.SelectedCells.Item(0).Value
            FrmAtenTel.Show()
        Else
            MsgBox("Selecciona una Llamada a Consultar.", , "Atenci�n")
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.ConAtenTelDataGridView.RowCount > 0 Then
            eOpcion = "M"
            eClave = Me.ConAtenTelDataGridView.SelectedCells.Item(0).Value
            FrmAtenTel.Show()
        Else
            MsgBox("Selecciona una Llamda a Modificar.", , "Atenci�n")
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
    End Sub

    Private Sub ConAtenTelPORSEPARADO(ByVal ClaveCli As Integer, ByVal NombreCli As String, ByVal APaternoCli As String, ByVal AMaternoCli As String, _
                                     ByVal UsuarioCli As String, ByVal Clv_TipSerCli As Integer, ByVal ColoniaCli As Integer, ByVal Fecha As DateTime, ByVal SetUpBox As String, ByVal Tarjeta As String, ByVal OpCli As Integer)

        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder

        StrSQL.Append("EXEC ConAtenTelPORSEPARADO ")
        StrSQL.Append(CStr(ClaveCli) & ",")
        StrSQL.Append("'" & NombreCli & "',")
        StrSQL.Append("'" & APaternoCli & "',")
        StrSQL.Append("'" & AMaternoCli & "',")
        StrSQL.Append("'" & UsuarioCli & "',")
        StrSQL.Append(CStr(Clv_TipSerCli) & ",")
        StrSQL.Append(CStr(ColoniaCli) & ",")
        StrSQL.Append("'" & Fecha & "',")
        StrSQL.Append("'" & SetUpBox & "', ")
        StrSQL.Append("'" & Tarjeta & "', ")
        StrSQL.Append(CStr(OpCli))


        Dim DA As New SqlDataAdapter(StrSQL.ToString, CON)
        Dim DT As New DataTable
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.ConAtenTelDataGridView.DataSource = BS.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub
    
    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged

    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        If Len(Me.TxtSetUpBox.Text) = 0 And Len(Me.TxtTarjeta.Text) = 0 Then
            MsgBox("Ingrese datos v�lidos para una b�squeda m�s precisa", MsgBoxStyle.Information)
            ConAtenTelPORSEPARADO(0, "", "", "", "", 0, 0, Today, "", "", 0)
        Else
            ConAtenTelPORSEPARADO(0, "", "", "", "", 0, 0, Today, Me.TxtSetUpBox.Text, Me.TxtTarjeta.Text, 7)
            Me.TxtSetUpBox.Clear()
            Me.TxtTarjeta.Clear()
        End If
    End Sub

    Private Sub APaternoTextBox_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles APaternoTextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If Len(Me.TextBox1.Text) = 0 And Len(Me.APaternoTextBox.Text) = 0 And Len(Me.AMaternoTextBox.Text) = 0 Then
                MsgBox("Ingrese datos v�lidos para una b�squeda m�s precisa", MsgBoxStyle.Information)
                ConAtenTelPORSEPARADO(0, "", "", "", "", 0, 0, Today, "", "", 0)
            Else
                ConAtenTelPORSEPARADO(0, Me.TextBox1.Text, APaternoTextBox.Text, AMaternoTextBox.Text, "", 0, 0, Today, "", "", 1)

                Me.TextBox1.Clear()
                Me.APaternoTextBox.Clear()
                Me.AMaternoTextBox.Clear()
                Me.TextBox2.Clear()
            End If
        End If
    End Sub

    Private Sub AMaternoTextBox_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles AMaternoTextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If Len(Me.TextBox1.Text) = 0 And Len(Me.APaternoTextBox.Text) = 0 And Len(Me.AMaternoTextBox.Text) = 0 Then
                MsgBox("Ingrese datos v�lidos para una b�squeda m�s precisa", MsgBoxStyle.Information)
                ConAtenTelPORSEPARADO(0, "", "", "", "", 0, 0, Today, "", "", 0)
            Else
                ConAtenTelPORSEPARADO(0, Me.TextBox1.Text, APaternoTextBox.Text, AMaternoTextBox.Text, "", 0, 0, Today, "", "", 1)

                Me.TextBox1.Clear()
                Me.APaternoTextBox.Clear()
                Me.AMaternoTextBox.Clear()
                Me.TextBox2.Clear()
            End If
        End If
    End Sub

    Private Sub TxtSetUpBox_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TxtSetUpBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If Len(Me.TxtSetUpBox.Text) = 0 And Len(Me.TxtTarjeta.Text) = 0 Then
                MsgBox("Ingrese datos v�lidos para una b�squeda m�s precisa", MsgBoxStyle.Information)
                ConAtenTelPORSEPARADO(0, "", "", "", "", 0, 0, Today, "", "", 0)
            Else
                ConAtenTelPORSEPARADO(0, "", "", "", "", 0, 0, Today, Me.TxtSetUpBox.Text, Me.TxtTarjeta.Text, 7)
                Me.TxtSetUpBox.Clear()
                Me.TxtTarjeta.Clear()
            End If
        End If
    End Sub

    Private Sub TxtTarjeta_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TxtTarjeta.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If Len(Me.TxtSetUpBox.Text) = 0 And Len(Me.TxtTarjeta.Text) = 0 Then
                MsgBox("Ingrese datos v�lidos para una b�squeda m�s precisa", MsgBoxStyle.Information)
                ConAtenTelPORSEPARADO(0, "", "", "", "", 0, 0, Today, "", "", 0)
            Else
                ConAtenTelPORSEPARADO(0, "", "", "", "", 0, 0, Today, Me.TxtSetUpBox.Text, Me.TxtTarjeta.Text, 7)
                Me.TxtSetUpBox.Clear()
                Me.TxtTarjeta.Clear()
            End If
        End If
    End Sub
End Class