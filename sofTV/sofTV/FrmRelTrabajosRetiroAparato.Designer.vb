﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRelTrabajosRetiroAparato
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblTitulo = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.dgvRelTrabajosRetiroAparato = New System.Windows.Forms.DataGridView()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.ident = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Trabajo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AplicaRetiroAparato = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvRelTrabajosRetiroAparato, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblTitulo
        '
        Me.lblTitulo.AutoSize = True
        Me.lblTitulo.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.Location = New System.Drawing.Point(142, 9)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(500, 24)
        Me.lblTitulo.TabIndex = 67
        Me.lblTitulo.Text = "Relación de Servicios al Cliente y el Retiro de Aparato"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dgvRelTrabajosRetiroAparato)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 52)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(771, 494)
        Me.GroupBox1.TabIndex = 68
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Listado de la Relación entre los Servicios al Cliente y el retiro de Aparato"
        '
        'dgvRelTrabajosRetiroAparato
        '
        Me.dgvRelTrabajosRetiroAparato.AllowUserToAddRows = False
        Me.dgvRelTrabajosRetiroAparato.AllowUserToDeleteRows = False
        Me.dgvRelTrabajosRetiroAparato.AllowUserToOrderColumns = True
        Me.dgvRelTrabajosRetiroAparato.AllowUserToResizeColumns = False
        Me.dgvRelTrabajosRetiroAparato.AllowUserToResizeRows = False
        Me.dgvRelTrabajosRetiroAparato.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRelTrabajosRetiroAparato.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ident, Me.Trabajo, Me.Descripcion, Me.AplicaRetiroAparato})
        Me.dgvRelTrabajosRetiroAparato.Location = New System.Drawing.Point(17, 22)
        Me.dgvRelTrabajosRetiroAparato.Name = "dgvRelTrabajosRetiroAparato"
        Me.dgvRelTrabajosRetiroAparato.Size = New System.Drawing.Size(744, 463)
        Me.dgvRelTrabajosRetiroAparato.TabIndex = 0
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.DarkOrange
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.ForeColor = System.Drawing.Color.Black
        Me.btnSalir.Location = New System.Drawing.Point(657, 547)
        Me.btnSalir.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(116, 43)
        Me.btnSalir.TabIndex = 78
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'btnGuardar
        '
        Me.btnGuardar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardar.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.ForeColor = System.Drawing.Color.Black
        Me.btnGuardar.Location = New System.Drawing.Point(526, 547)
        Me.btnGuardar.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(116, 43)
        Me.btnGuardar.TabIndex = 79
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = False
        '
        'ident
        '
        Me.ident.DataPropertyName = "ident"
        Me.ident.HeaderText = "ident"
        Me.ident.Name = "ident"
        Me.ident.Visible = False
        '
        'Trabajo
        '
        Me.Trabajo.DataPropertyName = "Trabajo"
        Me.Trabajo.HeaderText = "Clave del Trabajo"
        Me.Trabajo.Name = "Trabajo"
        Me.Trabajo.ReadOnly = True
        '
        'Descripcion
        '
        Me.Descripcion.DataPropertyName = "Descripcion"
        Me.Descripcion.HeaderText = "Descripción"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        Me.Descripcion.Width = 450
        '
        'AplicaRetiroAparato
        '
        Me.AplicaRetiroAparato.DataPropertyName = "AplicaRetiroAparato"
        Me.AplicaRetiroAparato.HeaderText = "Aplica retiro de aparato"
        Me.AplicaRetiroAparato.Name = "AplicaRetiroAparato"
        Me.AplicaRetiroAparato.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.AplicaRetiroAparato.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.AplicaRetiroAparato.Width = 130
        '
        'FrmRelTrabajosRetiroAparato
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 18.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(785, 603)
        Me.Controls.Add(Me.lblTitulo)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.Name = "FrmRelTrabajosRetiroAparato"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Relación entre Trabajos y Retiro Aparato"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dgvRelTrabajosRetiroAparato, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblTitulo As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvRelTrabajosRetiroAparato As System.Windows.Forms.DataGridView
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents ident As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Trabajo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AplicaRetiroAparato As System.Windows.Forms.DataGridViewCheckBoxColumn
End Class
