﻿Imports System
Imports System.IO
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Niveles
Imports sofTV.Base

Public Class FrmServicioNivel

    Public consultas As New CBase


    Dim dico As New Dictionary(Of Integer, DataRow)

    '****** Reporte ******
    Dim selDiaMetas As New FrmSelDiaMetas()
    Dim selVendedor As New FrmSelVendedor()
    Dim imprimirCentralizada As New FrmImprimirCentralizada()
    Dim reporte As New ReportDocument()

    '****** Tbl_ServicoNivel ******
    Public filas As Integer, IdServicoNivel As Integer

    Public tblServicos As DataTable, tblNivel As DataTable, tblServicoNivel As DataTable
    Public ds As DataSet
    Public servicio As Integer, nivel As Integer, puntos As Integer, inserta As Integer, Anualidad As Boolean, GrupoVentas As Integer
    Dim ruta As String = Nothing

    Public Sub New()
        InitializeComponent()
    End Sub

    Private Sub FrmServicioNivel_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.Btn_Agregar.ForeColor = Color.White
        Me.Btn_Agregar.BackColor = Color.Gray
        Me.Btn_Salir.ForeColor = Color.White
        Me.Btn_Salir.BackColor = Color.Gray
        LlenaComboGrupoVentas(0, 0)
        LlenaComboServicios()
        LlenaComboNivel()
        Try
            LlenaGridServicoNivel(CInt(Me.cmbGrupoVentas.SelectedValue))
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Btn_Salir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Salir.Click
        Me.Close()
    End Sub

    Private Sub Btn_Agregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Agregar.Click
        Try
            servicio = dico(CMB_Servicio.SelectedValue)("Clv_Servicio").ToString()
            Anualidad = dico(CMB_Servicio.SelectedValue)("Anualidad").ToString()
            nivel = Convert.ToInt32(CMB_Nivel.SelectedValue.ToString())
            GrupoVentas = CInt(Me.cmbGrupoVentas.SelectedValue)

            If Me.Txt_Puntos.Text <= 0 Then
                MsgBox("Capture un Número de Puntos válido", MsgBoxStyle.Information)
                Exit Sub
            End If

            If servicio <> 0 And nivel <> 0 And GrupoVentas <> 0 And IsNumeric(Txt_Puntos.Text.ToString()) Then

                puntos = Convert.ToInt32(Txt_Puntos.Text.ToString())
                '-------------------------------------------------------------
                inserta = InsertaServicioNivel(servicio, nivel, puntos, Anualidad, GrupoVentas)

                If inserta = 0 Then
                    MsgBox("Actualmente se encuentra el servicio registrado", MsgBoxStyle.Information)
                End If

                LlenaGridServicoNivel(CInt(Me.cmbGrupoVentas.SelectedValue))
                '-------------------------------------------------------------
            Else
                MsgBox("Seleccione un Grupo de Ventas, Servicio, Nivel y capture los Puntos como datos enteros", MsgBoxStyle.Exclamation)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub DGV_ServicioNivel_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGV_ServicioNivel.CellClick
        If e.RowIndex > -1 And e.ColumnIndex > -1 Then

            If DGV_ServicioNivel.Columns(e.ColumnIndex).Name = "Eliminar" Then

                IdServicoNivel = Convert.ToInt32(DGV_ServicioNivel.Rows(e.RowIndex).Cells("Id").Value.ToString())
                '-------------------------------------------------------------
                EliminarServicioNivel(IdServicoNivel)
                LlenaGridServicoNivel(CInt(Me.cmbGrupoVentas.SelectedValue))
                '-------------------------------------------------------------
            End If
        End If
    End Sub

    '---------------------------------------------------------------------------------------------------------------------------
    Private Sub LlenaComboServicios()
        Try
            tblServicos = consultas.consultarDT("SoftvSaltillo_ServicioS", Nothing)

            If tblServicos.Rows.Count > 0 Then
                CMB_Servicio.DataSource = tblServicos
                CMB_Servicio.DisplayMember = "Descripcion"
                CMB_Servicio.ValueMember = "id"

                For Each dr As DataRow In tblServicos.Rows
                    dico.Add(dr("id"), dr)
                Next
            Else
                MsgBox("No hay servicios registrados", MsgBoxStyle.Information)
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub LlenaComboNivel()
        Try
            tblNivel = consultas.consultarDT("SoftvSaltillo_Nivel", Nothing)

            If tblNivel.Rows.Count > 0 Then

                CMB_Nivel.DataSource = tblNivel
                CMB_Nivel.DisplayMember = "Nivel"
                CMB_Nivel.ValueMember = "Id"
            Else
                MsgBox("No hay niveles registrados", MsgBoxStyle.Information)
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub LlenaGridServicoNivel(ByVal prmIdGrupoVentas As Integer)
        Try
            Dim parametros(1) As SqlParameter

            parametros(0) = New SqlParameter("@idGrupoVentas", SqlDbType.Int)
            parametros(0).Value = prmIdGrupoVentas

            tblServicoNivel = consultas.consultarDT("SoftvSaltillo_ConsultaServicioNivel", parametros)

            DGV_ServicioNivel.DataSource = tblServicoNivel
            DGV_ServicioNivel.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub

    Private Function InsertaServicioNivel(ByVal servicio As Integer, ByVal nivel As Integer, ByVal puntos As Integer, ByVal Anualidad As Boolean, ByVal idGrupoVnetas As Integer) As Integer
        Try
            Dim parametro(5) As SqlParameter

            parametro(0) = New SqlParameter("@servicio", SqlDbType.Int)
            parametro(0).Value = servicio
            parametro(1) = New SqlParameter("@nivel", SqlDbType.Int)
            parametro(1).Value = nivel
            parametro(2) = New SqlParameter("@puntos", SqlDbType.Int)
            parametro(2).Value = puntos
            parametro(3) = New SqlParameter("@Anualidad", SqlDbType.Bit)
            parametro(3).Value = Anualidad
            parametro(4) = New SqlParameter("@idGrupoVentas", SqlDbType.Int)
            parametro(4).Value = idGrupoVnetas

            Dim registros As ArrayList = consultas.consultarHT("SoftvSaltillo_InsertaServicioNivel", parametro)

            If registros.Count > 0 Then
                Return Convert.ToInt32(DirectCast(registros(0), Hashtable)("Servicio").ToString())
            Else
                Return 0
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Return 0
        End Try
    End Function

    Private Sub EliminarServicioNivel(ByVal id As Integer)
        Try
            Using con As New SqlConnection(MiConexion)
                Using comando As New SqlCommand(MiConexion, con)

                    con.Open()
                    comando.Connection = con
                    comando.CommandType = CommandType.StoredProcedure
                    comando.CommandText = "SoftvSaltillo_EliminarServicioNivel"

                    comando.Parameters.Add("@Id", SqlDbType.Int)
                    comando.Parameters("@Id").Value = id

                    comando.ExecuteNonQuery()
                End Using
            End Using
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub LlenaComboGrupoVentas(ByVal prmClvGrupo As Integer, ByVal prmOp As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Grupo", SqlDbType.Int, prmClvGrupo)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, prmOp)
        Me.cmbGrupoVentas.DataSource = BaseII.ConsultaDT("ConGrupoVentas")
    End Sub

    Private Sub cmbGrupoVentas_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbGrupoVentas.SelectedIndexChanged
        Try
            LlenaGridServicoNivel(CInt(Me.cmbGrupoVentas.SelectedValue))
        Catch ex As Exception

        End Try
    End Sub
End Class

