Public Class FrmSelFechasPPE

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eFechaIniPPE = Me.DateTimePicker1.Value
        eFechaIni = eFechaIniPPE
        eFechaFinPPE = Me.DateTimePicker2.Value
        eFechaFin = eFechaFinPPE

        If eBndGraf = True Then
            eBndGraf = False
            If eOpVentas = 11 Or eOpVentas = 12 Or eOpVentas = 13 Then
                FrmImprimirComision.Show()
            End If
            If eOpVentas = 14 Then
                FrmSelSucVen.Show()
            End If
            'If eOpVentas = 15 Then

            'End If
            If eOpVentas = 16 Then
                FrmSelVendedor.Show()
            End If
            If eOpVentas = 17 Then
                FrmSelTipServE.Show()
            End If
        ElseIf eOpVentas = 55 Or eOpVentas = 56 Or eOpVentas = 68 Then
            FrmImprimirComision.Show()
        ElseIf eOpVentas = 57 Then
            eClaveCorreo = 0
            eopcorreo=2
            FrmImprimirComision.Show()
        ElseIf eOpVentas = 61 Then
            FrmSelTipServE.Show()
        ElseIf eOpVentas = 62 Then
            FrmImprimirComision.Show()
        ElseIf eOpVentas = 62 Then
            FrmImprimirComision.Show()
        ElseIf eOpVentas = 64 Then
            FrmImprimirComision.Show()
        ElseIf eOpVentas = 77 Then
            FrmImprimirComision.Show()
            'Yahve --------------------------------------------
        ElseIf eOpVentas = 100 Then
            FrmImprimirComision.Show()

        Else
            FrmImprimirPPE.Show()
        End If
        Me.Close()
    End Sub

    Private Sub FrmSelFechasPPE_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Me.DateTimePicker1.MaxDate = Today
        Me.DateTimePicker1.Value = Today
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
        If eOpPPE = 1 Then
            Me.Text = "Venta de Pel�culas"
        ElseIf eOpPPE = 2 Then
            Me.Text = "Bit�cora de Pruebas"
        ElseIf eOpPPE = 3 Then
            Me.Text = "Resumen de Ventas Sucursal"
        ElseIf eOpPPE = 4 Then
            Me.Text = "Resumen de Ventas Vendedores"
        ElseIf eOpPPE = 0 Then
            If eOpVentas = 55 Then
                Me.Text = "Selecciona un Rango de Fechas"
            ElseIf eOpVentas = 56 Then
                Me.Text = "Ventas en Baja"
                Me.GroupBox2.Text = "Fechas de las Bajas"
            ElseIf eOpVentas = 57 Then
                Me.Text = "Bit�cora de Correos"
                Me.GroupBox2.Text = "Fechas de los env�os"
            ElseIf eOpVentas = 61 Then
                Me.Text = "Ventas Totales"
                Me.GroupBox2.Text = "Fechas de las ventas"
            ElseIf eOpVentas = 62 Then
                Me.Text = "Interfaz " & eServicio
                Me.GroupBox2.Text = "Fechas"
            ElseIf eOpVentas = 64 Then
                Me.Text = "Reporte Auxuliar " & eServicio
                Me.GroupBox2.Text = "Rango de fechas de los periodos"
            ElseIf eOpVentas = 68 Then
                Me.Text = "Reporte Resumen de Cancelaciones"
                Me.GroupBox2.Text = "Rango de Fechas de las Cancelaciones"
                'Yahve -----
            ElseIf eOpVentas = 77 Then
                Me.Text = "Reporte Recontrataciones"
                Me.GroupBox2.Text = "Rango de Fechas de Solicitud"
            ElseIf eOpVentas = 100 Then
                Me.Text = "Resumen de Puntos de Antig�edad"
                Me.GroupBox2.Text = "Seleccione un Rango de Fechas"
            End If
        End If
        If eBndGraf = True Then
            Me.Text = "Selecciona un Rango de Fechas"
        End If

    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
    End Sub

    Private Sub DateTimePicker2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker2.ValueChanged
        Me.DateTimePicker1.MaxDate = Me.DateTimePicker2.Value
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
End Class