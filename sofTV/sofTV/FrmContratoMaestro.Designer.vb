<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmContratoMaestro
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CONTRATOLabel As System.Windows.Forms.Label
        Dim NOMBRELabel As System.Windows.Forms.Label
        Dim CALLELabel As System.Windows.Forms.Label
        Dim COLONIALabel As System.Windows.Forms.Label
        Dim NUMEROLabel As System.Windows.Forms.Label
        Dim CIUDADLabel As System.Windows.Forms.Label
        Dim SOLOINTERNETLabel As System.Windows.Forms.Label
        Dim ESHOTELLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.AMaternoTextBox = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.APaternoTextBox = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.NOMBRELabel1 = New System.Windows.Forms.Label()
        Me.ConContratoMaestroBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.CONTRATOLabel1 = New System.Windows.Forms.Label()
        Me.ESHOTELCheckBox = New System.Windows.Forms.CheckBox()
        Me.SOLOINTERNETCheckBox = New System.Windows.Forms.CheckBox()
        Me.CALLELabel1 = New System.Windows.Forms.Label()
        Me.CIUDADLabel1 = New System.Windows.Forms.Label()
        Me.COLONIALabel1 = New System.Windows.Forms.Label()
        Me.NUMEROLabel1 = New System.Windows.Forms.Label()
        Me.ConContratoMaestroTableAdapter = New sofTV.DataSetEricTableAdapters.ConContratoMaestroTableAdapter()
        Me.ConContratoMaestroDataGridView = New System.Windows.Forms.DataGridView()
        Me.CONTRATO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NOMBRE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CALLE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COLONIA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NUMERO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CIUDAD = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SOLOINTERNET = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ESHOTEL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.TxtSetUpBox = New System.Windows.Forms.TextBox()
        Me.TxtTarjeta = New System.Windows.Forms.TextBox()
        Me.Button8 = New System.Windows.Forms.Button()
        CONTRATOLabel = New System.Windows.Forms.Label()
        NOMBRELabel = New System.Windows.Forms.Label()
        CALLELabel = New System.Windows.Forms.Label()
        COLONIALabel = New System.Windows.Forms.Label()
        NUMEROLabel = New System.Windows.Forms.Label()
        CIUDADLabel = New System.Windows.Forms.Label()
        SOLOINTERNETLabel = New System.Windows.Forms.Label()
        ESHOTELLabel = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.ConContratoMaestroBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConContratoMaestroDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CONTRATOLabel
        '
        CONTRATOLabel.AutoSize = True
        CONTRATOLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CONTRATOLabel.ForeColor = System.Drawing.Color.White
        CONTRATOLabel.Location = New System.Drawing.Point(7, 33)
        CONTRATOLabel.Name = "CONTRATOLabel"
        CONTRATOLabel.Size = New System.Drawing.Size(63, 13)
        CONTRATOLabel.TabIndex = 0
        CONTRATOLabel.Text = "Contrato :"
        '
        'NOMBRELabel
        '
        NOMBRELabel.AutoSize = True
        NOMBRELabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NOMBRELabel.ForeColor = System.Drawing.Color.White
        NOMBRELabel.Location = New System.Drawing.Point(7, 58)
        NOMBRELabel.Name = "NOMBRELabel"
        NOMBRELabel.Size = New System.Drawing.Size(58, 13)
        NOMBRELabel.TabIndex = 2
        NOMBRELabel.Text = "Nombre :"
        '
        'CALLELabel
        '
        CALLELabel.AutoSize = True
        CALLELabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CALLELabel.ForeColor = System.Drawing.Color.White
        CALLELabel.Location = New System.Drawing.Point(7, 94)
        CALLELabel.Name = "CALLELabel"
        CALLELabel.Size = New System.Drawing.Size(43, 13)
        CALLELabel.TabIndex = 4
        CALLELabel.Text = "Calle :"
        '
        'COLONIALabel
        '
        COLONIALabel.AutoSize = True
        COLONIALabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        COLONIALabel.ForeColor = System.Drawing.Color.White
        COLONIALabel.Location = New System.Drawing.Point(7, 123)
        COLONIALabel.Name = "COLONIALabel"
        COLONIALabel.Size = New System.Drawing.Size(57, 13)
        COLONIALabel.TabIndex = 6
        COLONIALabel.Text = "Colonia :"
        '
        'NUMEROLabel
        '
        NUMEROLabel.AutoSize = True
        NUMEROLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NUMEROLabel.ForeColor = System.Drawing.Color.White
        NUMEROLabel.Location = New System.Drawing.Point(7, 153)
        NUMEROLabel.Name = "NUMEROLabel"
        NUMEROLabel.Size = New System.Drawing.Size(58, 13)
        NUMEROLabel.TabIndex = 8
        NUMEROLabel.Text = "Número :"
        '
        'CIUDADLabel
        '
        CIUDADLabel.AutoSize = True
        CIUDADLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CIUDADLabel.ForeColor = System.Drawing.Color.White
        CIUDADLabel.Location = New System.Drawing.Point(7, 191)
        CIUDADLabel.Name = "CIUDADLabel"
        CIUDADLabel.Size = New System.Drawing.Size(54, 13)
        CIUDADLabel.TabIndex = 10
        CIUDADLabel.Text = "Ciudad :"
        '
        'SOLOINTERNETLabel
        '
        SOLOINTERNETLabel.AutoSize = True
        SOLOINTERNETLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SOLOINTERNETLabel.ForeColor = System.Drawing.Color.White
        SOLOINTERNETLabel.Location = New System.Drawing.Point(7, 220)
        SOLOINTERNETLabel.Name = "SOLOINTERNETLabel"
        SOLOINTERNETLabel.Size = New System.Drawing.Size(88, 13)
        SOLOINTERNETLabel.TabIndex = 12
        SOLOINTERNETLabel.Text = "Solo Internet :"
        '
        'ESHOTELLabel
        '
        ESHOTELLabel.AutoSize = True
        ESHOTELLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ESHOTELLabel.ForeColor = System.Drawing.Color.White
        ESHOTELLabel.Location = New System.Drawing.Point(121, 220)
        ESHOTELLabel.Name = "ESHOTELLabel"
        ESHOTELLabel.Size = New System.Drawing.Size(63, 13)
        ESHOTELLabel.TabIndex = 14
        ESHOTELLabel.Text = "Es Hotel :"
        ESHOTELLabel.Visible = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.Button8)
        Me.Panel1.Controls.Add(Me.TxtTarjeta)
        Me.Panel1.Controls.Add(Me.TxtSetUpBox)
        Me.Panel1.Controls.Add(Me.Label15)
        Me.Panel1.Controls.Add(Me.Label14)
        Me.Panel1.Controls.Add(Me.AMaternoTextBox)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.APaternoTextBox)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.TextBox5)
        Me.Panel1.Controls.Add(Me.Button6)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Button5)
        Me.Panel1.Controls.Add(Me.TextBox4)
        Me.Panel1.Controls.Add(Me.TextBox3)
        Me.Panel1.Controls.Add(Me.TextBox2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.TextBox1)
        Me.Panel1.Controls.Add(Me.Button7)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Location = New System.Drawing.Point(2, 14)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(278, 716)
        Me.Panel1.TabIndex = 4
        Me.Panel1.TabStop = True
        '
        'AMaternoTextBox
        '
        Me.AMaternoTextBox.BackColor = System.Drawing.Color.LightGray
        Me.AMaternoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AMaternoTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.AMaternoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AMaternoTextBox.Location = New System.Drawing.Point(12, 173)
        Me.AMaternoTextBox.Name = "AMaternoTextBox"
        Me.AMaternoTextBox.Size = New System.Drawing.Size(241, 24)
        Me.AMaternoTextBox.TabIndex = 60
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(10, 155)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(124, 15)
        Me.Label9.TabIndex = 62
        Me.Label9.Text = "Apellido Materno :"
        '
        'APaternoTextBox
        '
        Me.APaternoTextBox.BackColor = System.Drawing.Color.LightGray
        Me.APaternoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.APaternoTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.APaternoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.APaternoTextBox.Location = New System.Drawing.Point(11, 131)
        Me.APaternoTextBox.Name = "APaternoTextBox"
        Me.APaternoTextBox.Size = New System.Drawing.Size(242, 24)
        Me.APaternoTextBox.TabIndex = 59
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(8, 113)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(121, 15)
        Me.Label12.TabIndex = 61
        Me.Label12.Text = "Apellido Paterno :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(6, 252)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(66, 15)
        Me.Label6.TabIndex = 50
        Me.Label6.Text = "Número :"
        '
        'TextBox5
        '
        Me.TextBox5.BackColor = System.Drawing.Color.LightGray
        Me.TextBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox5.Location = New System.Drawing.Point(13, 315)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(238, 24)
        Me.TextBox5.TabIndex = 9
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkOrange
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(171, 343)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(88, 23)
        Me.Button6.TabIndex = 10
        Me.Button6.Text = "Buscar"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(5, 297)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(60, 15)
        Me.Label5.TabIndex = 49
        Me.Label5.Text = "Ciudad :"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(10, 210)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 15)
        Me.Label4.TabIndex = 48
        Me.Label4.Text = "Calle :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(7, 69)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(70, 15)
        Me.Label3.TabIndex = 47
        Me.Label3.Text = "Nombre  :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(7, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 15)
        Me.Label2.TabIndex = 46
        Me.Label2.Text = "Contrato :"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(170, 201)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(88, 23)
        Me.Button5.TabIndex = 6
        Me.Button5.Text = "Buscar"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'TextBox4
        '
        Me.TextBox4.BackColor = System.Drawing.Color.LightGray
        Me.TextBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox4.Location = New System.Drawing.Point(13, 273)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(240, 24)
        Me.TextBox4.TabIndex = 8
        '
        'TextBox3
        '
        Me.TextBox3.BackColor = System.Drawing.Color.LightGray
        Me.TextBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(13, 228)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(240, 24)
        Me.TextBox3.TabIndex = 7
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.Color.LightGray
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(10, 87)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(243, 24)
        Me.TextBox2.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(5, 5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(246, 20)
        Me.Label1.TabIndex = 45
        Me.Label1.Text = "Buscar Contrato Maestro por:"
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.LightGray
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(10, 42)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(152, 24)
        Me.TextBox1.TabIndex = 3
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.DarkOrange
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.Location = New System.Drawing.Point(171, 42)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(88, 23)
        Me.Button7.TabIndex = 4
        Me.Button7.Text = "Buscar"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel2.Controls.Add(Me.Label7)
        Me.Panel2.Controls.Add(Me.NOMBRELabel1)
        Me.Panel2.Controls.Add(ESHOTELLabel)
        Me.Panel2.Controls.Add(Me.CONTRATOLabel1)
        Me.Panel2.Controls.Add(Me.ESHOTELCheckBox)
        Me.Panel2.Controls.Add(CONTRATOLabel)
        Me.Panel2.Controls.Add(SOLOINTERNETLabel)
        Me.Panel2.Controls.Add(NOMBRELabel)
        Me.Panel2.Controls.Add(Me.SOLOINTERNETCheckBox)
        Me.Panel2.Controls.Add(Me.CALLELabel1)
        Me.Panel2.Controls.Add(CIUDADLabel)
        Me.Panel2.Controls.Add(CALLELabel)
        Me.Panel2.Controls.Add(Me.CIUDADLabel1)
        Me.Panel2.Controls.Add(Me.COLONIALabel1)
        Me.Panel2.Controls.Add(NUMEROLabel)
        Me.Panel2.Controls.Add(COLONIALabel)
        Me.Panel2.Controls.Add(Me.NUMEROLabel1)
        Me.Panel2.Location = New System.Drawing.Point(3, 476)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(272, 237)
        Me.Panel2.TabIndex = 16
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(5, 4)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(147, 18)
        Me.Label7.TabIndex = 51
        Me.Label7.Text = "Datos del Cliente :"
        '
        'NOMBRELabel1
        '
        Me.NOMBRELabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConContratoMaestroBindingSource, "NOMBRE", True))
        Me.NOMBRELabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NOMBRELabel1.Location = New System.Drawing.Point(68, 56)
        Me.NOMBRELabel1.Name = "NOMBRELabel1"
        Me.NOMBRELabel1.Size = New System.Drawing.Size(187, 23)
        Me.NOMBRELabel1.TabIndex = 3
        Me.NOMBRELabel1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'ConContratoMaestroBindingSource
        '
        Me.ConContratoMaestroBindingSource.DataMember = "ConContratoMaestro"
        Me.ConContratoMaestroBindingSource.DataSource = Me.DataSetEric
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CONTRATOLabel1
        '
        Me.CONTRATOLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConContratoMaestroBindingSource, "CONTRATO", True))
        Me.CONTRATOLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONTRATOLabel1.Location = New System.Drawing.Point(69, 24)
        Me.CONTRATOLabel1.Name = "CONTRATOLabel1"
        Me.CONTRATOLabel1.Size = New System.Drawing.Size(100, 23)
        Me.CONTRATOLabel1.TabIndex = 1
        Me.CONTRATOLabel1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'ESHOTELCheckBox
        '
        Me.ESHOTELCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.ConContratoMaestroBindingSource, "ESHOTEL", True))
        Me.ESHOTELCheckBox.Enabled = False
        Me.ESHOTELCheckBox.Location = New System.Drawing.Point(208, 215)
        Me.ESHOTELCheckBox.Name = "ESHOTELCheckBox"
        Me.ESHOTELCheckBox.Size = New System.Drawing.Size(19, 24)
        Me.ESHOTELCheckBox.TabIndex = 15
        Me.ESHOTELCheckBox.TabStop = False
        Me.ESHOTELCheckBox.Visible = False
        '
        'SOLOINTERNETCheckBox
        '
        Me.SOLOINTERNETCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.ConContratoMaestroBindingSource, "SOLOINTERNET", True))
        Me.SOLOINTERNETCheckBox.Enabled = False
        Me.SOLOINTERNETCheckBox.Location = New System.Drawing.Point(92, 215)
        Me.SOLOINTERNETCheckBox.Name = "SOLOINTERNETCheckBox"
        Me.SOLOINTERNETCheckBox.Size = New System.Drawing.Size(23, 24)
        Me.SOLOINTERNETCheckBox.TabIndex = 13
        Me.SOLOINTERNETCheckBox.TabStop = False
        '
        'CALLELabel1
        '
        Me.CALLELabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConContratoMaestroBindingSource, "CALLE", True))
        Me.CALLELabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CALLELabel1.Location = New System.Drawing.Point(68, 85)
        Me.CALLELabel1.Name = "CALLELabel1"
        Me.CALLELabel1.Size = New System.Drawing.Size(187, 23)
        Me.CALLELabel1.TabIndex = 5
        Me.CALLELabel1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'CIUDADLabel1
        '
        Me.CIUDADLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConContratoMaestroBindingSource, "CIUDAD", True))
        Me.CIUDADLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CIUDADLabel1.Location = New System.Drawing.Point(64, 181)
        Me.CIUDADLabel1.Name = "CIUDADLabel1"
        Me.CIUDADLabel1.Size = New System.Drawing.Size(187, 23)
        Me.CIUDADLabel1.TabIndex = 11
        Me.CIUDADLabel1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'COLONIALabel1
        '
        Me.COLONIALabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConContratoMaestroBindingSource, "COLONIA", True))
        Me.COLONIALabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.COLONIALabel1.Location = New System.Drawing.Point(69, 113)
        Me.COLONIALabel1.Name = "COLONIALabel1"
        Me.COLONIALabel1.Size = New System.Drawing.Size(187, 23)
        Me.COLONIALabel1.TabIndex = 7
        Me.COLONIALabel1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'NUMEROLabel1
        '
        Me.NUMEROLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConContratoMaestroBindingSource, "NUMERO", True))
        Me.NUMEROLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NUMEROLabel1.Location = New System.Drawing.Point(63, 144)
        Me.NUMEROLabel1.Name = "NUMEROLabel1"
        Me.NUMEROLabel1.Size = New System.Drawing.Size(187, 23)
        Me.NUMEROLabel1.TabIndex = 9
        Me.NUMEROLabel1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'ConContratoMaestroTableAdapter
        '
        Me.ConContratoMaestroTableAdapter.ClearBeforeFill = True
        '
        'ConContratoMaestroDataGridView
        '
        Me.ConContratoMaestroDataGridView.AllowUserToAddRows = False
        Me.ConContratoMaestroDataGridView.AllowUserToDeleteRows = False
        Me.ConContratoMaestroDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ConContratoMaestroDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.ConContratoMaestroDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CONTRATO, Me.NOMBRE, Me.CALLE, Me.COLONIA, Me.NUMERO, Me.CIUDAD, Me.SOLOINTERNET, Me.ESHOTEL})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ConContratoMaestroDataGridView.DefaultCellStyle = DataGridViewCellStyle4
        Me.ConContratoMaestroDataGridView.Location = New System.Drawing.Point(286, 12)
        Me.ConContratoMaestroDataGridView.Name = "ConContratoMaestroDataGridView"
        Me.ConContratoMaestroDataGridView.ReadOnly = True
        Me.ConContratoMaestroDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.ConContratoMaestroDataGridView.Size = New System.Drawing.Size(549, 716)
        Me.ConContratoMaestroDataGridView.TabIndex = 3
        '
        'CONTRATO
        '
        Me.CONTRATO.DataPropertyName = "CONTRATO"
        Me.CONTRATO.HeaderText = "Contrato"
        Me.CONTRATO.Name = "CONTRATO"
        Me.CONTRATO.ReadOnly = True
        '
        'NOMBRE
        '
        Me.NOMBRE.DataPropertyName = "NOMBRE"
        Me.NOMBRE.HeaderText = "Nombre"
        Me.NOMBRE.Name = "NOMBRE"
        Me.NOMBRE.ReadOnly = True
        '
        'CALLE
        '
        Me.CALLE.DataPropertyName = "CALLE"
        Me.CALLE.HeaderText = "Calle"
        Me.CALLE.Name = "CALLE"
        Me.CALLE.ReadOnly = True
        '
        'COLONIA
        '
        Me.COLONIA.DataPropertyName = "COLONIA"
        Me.COLONIA.HeaderText = "Colonia"
        Me.COLONIA.Name = "COLONIA"
        Me.COLONIA.ReadOnly = True
        '
        'NUMERO
        '
        Me.NUMERO.DataPropertyName = "NUMERO"
        Me.NUMERO.HeaderText = "Numero"
        Me.NUMERO.Name = "NUMERO"
        Me.NUMERO.ReadOnly = True
        '
        'CIUDAD
        '
        Me.CIUDAD.DataPropertyName = "CIUDAD"
        Me.CIUDAD.HeaderText = "Ciudad"
        Me.CIUDAD.Name = "CIUDAD"
        Me.CIUDAD.ReadOnly = True
        '
        'SOLOINTERNET
        '
        Me.SOLOINTERNET.DataPropertyName = "SOLOINTERNET"
        Me.SOLOINTERNET.HeaderText = "Solo Internet"
        Me.SOLOINTERNET.Name = "SOLOINTERNET"
        Me.SOLOINTERNET.ReadOnly = True
        '
        'ESHOTEL
        '
        Me.ESHOTEL.DataPropertyName = "ESHOTEL"
        Me.ESHOTEL.HeaderText = "Es Hotel"
        Me.ESHOTEL.Name = "ESHOTEL"
        Me.ESHOTEL.ReadOnly = True
        '
        'Button2
        '
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(858, 56)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(136, 36)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "&CONSULTAR"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(858, 14)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "&NUEVO"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(858, 98)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(136, 36)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "&MODIFICAR"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(858, 676)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(136, 36)
        Me.Button4.TabIndex = 3
        Me.Button4.Text = "&SALIR"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(15, 399)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(124, 15)
        Me.Label15.TabIndex = 68
        Me.Label15.Text = "No. Serie Tarjeta :"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(12, 355)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(86, 15)
        Me.Label14.TabIndex = 67
        Me.Label14.Text = "Set Up Box :"
        '
        'TxtSetUpBox
        '
        Me.TxtSetUpBox.BackColor = System.Drawing.Color.LightGray
        Me.TxtSetUpBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TxtSetUpBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtSetUpBox.Location = New System.Drawing.Point(11, 372)
        Me.TxtSetUpBox.Name = "TxtSetUpBox"
        Me.TxtSetUpBox.Size = New System.Drawing.Size(238, 24)
        Me.TxtSetUpBox.TabIndex = 69
        '
        'TxtTarjeta
        '
        Me.TxtTarjeta.BackColor = System.Drawing.Color.LightGray
        Me.TxtTarjeta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TxtTarjeta.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtTarjeta.Location = New System.Drawing.Point(11, 417)
        Me.TxtTarjeta.Name = "TxtTarjeta"
        Me.TxtTarjeta.Size = New System.Drawing.Size(238, 24)
        Me.TxtTarjeta.TabIndex = 70
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.DarkOrange
        Me.Button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.Location = New System.Drawing.Point(171, 447)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(88, 23)
        Me.Button8.TabIndex = 71
        Me.Button8.Text = "Buscar"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'FrmContratoMaestro
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1016, 734)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ConContratoMaestroDataGridView)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "FrmContratoMaestro"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Contrato Maestro"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.ConContratoMaestroBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConContratoMaestroDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel

    Private Sub FillToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub FillToolStripButton_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub FillToolStripButton_Click_2(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub FillToolStripButton_Click_3(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents ConContratoMaestroBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConContratoMaestroTableAdapter As sofTV.DataSetEricTableAdapters.ConContratoMaestroTableAdapter
    Friend WithEvents ConContratoMaestroDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents NOMBRELabel1 As System.Windows.Forms.Label
    Friend WithEvents CONTRATOLabel1 As System.Windows.Forms.Label
    Friend WithEvents ESHOTELCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents SOLOINTERNETCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents CALLELabel1 As System.Windows.Forms.Label
    Friend WithEvents CIUDADLabel1 As System.Windows.Forms.Label
    Friend WithEvents COLONIALabel1 As System.Windows.Forms.Label
    Friend WithEvents NUMEROLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents AMaternoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents APaternoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents CONTRATO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NOMBRE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CALLE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents COLONIA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NUMERO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CIUDAD As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SOLOINTERNET As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ESHOTEL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents TxtTarjeta As System.Windows.Forms.TextBox
    Friend WithEvents TxtSetUpBox As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
End Class
