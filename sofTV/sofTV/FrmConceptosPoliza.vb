﻿Imports sofTV.BAL
Imports System.Collections.Generic
Public Class FrmConceptosPoliza
    Private _Clave As Integer
    Public Property Clave As Integer
        Get
            Return _Clave
        End Get
        Set(ByVal value As Integer)
            _Clave = value
        End Set
    End Property

    Private Sub FrmConceptosPoliza_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.IdConceptoText.Text = Me.Clave.ToString

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, GloClvCompania)
        IdGrupoCombo.DataSource = BaseII.ConsultaDT("Softv_GetGrupoConceptoIngreso")

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, GloClvCompania)
        Clv_ServicioComboBox.DataSource = BaseII.ConsultaDT("MUESTRAServiciosPoliza")

        BaseII.limpiaParametros()
        cbTipserv.DataSource = BaseII.ConsultaDT("MuestraTipSerPrincipal")


        'Dim GrupoConcepto As New GrupoConceptoIngreso
        'With Me.IdGrupoCombo
        '    .DataSource = GrupoConcepto.GetAll
        '    .ValueMember = "IdGrupo"
        '    .DisplayMember = "Concepto"
        'End With

        'Dim LocServicios As New Servicio

        'Dim ListaServicios As New List(Of Servicio)
        'LocServicios = New Servicio(0, 1, "Ninguno", "", False, False, 0, False, False, 0, 0)
        'ListaServicios.Add(LocServicios)

        'For Each Servi As Servicio In LocServicios.GetAll()
        '    'If Servi.Sale_en_Cartera = False Then
        '    ListaServicios.Add(Servi)
        '    'End If
        'Next


        'With Me.Clv_ServicioComboBox
        '    .DataSource = ListaServicios 'LocServicios.GetAll()
        '    .ValueMember = "Clv_Servicio"
        '    .DisplayMember = "Descripcion"
        'End With
       

        Select Case opcion
            Case "N"
                IdGrupoCombo.SelectedValue = GloClvGrupo
                ActivoCheckBox.Checked = True
                Me.gbxConcepto.Enabled = True
                Me.SaveButton.Enabled = True
            Case "C"
                TraeConceptos()
                Me.gbxConcepto.Enabled = False
                Me.SaveButton.Enabled = False
            Case "M"
                Me.gbxConcepto.Enabled = True
                TraeConceptos()
                Me.SaveButton.Enabled = True
        End Select

        

    End Sub


    Private Sub SaveButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveButton.Click
        If IsNumeric(Me.PosicionText.Text) = False Then
            Me.PosicionText.Text = "0"
        End If

        If IsNumeric(Me.IdProgramadaTextBox.Text) = False Then
            Me.IdProgramadaTextBox.Text = "0"
        End If

        Guardar()
        Me.Close()
    End Sub

    Public Sub Guardar()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Concepto", SqlDbType.NVarChar, Me.ConceptoText.Text, 250)
            BaseII.CreateMyParameter("@Posicion", SqlDbType.BigInt, CInt(Me.PosicionText.Text))
            BaseII.CreateMyParameter("@IdGrupo", SqlDbType.Int, IdGrupoCombo.SelectedValue)
            BaseII.CreateMyParameter("@Clv_Servicio", SqlDbType.Int, Clv_ServicioComboBox.SelectedValue)
            BaseII.CreateMyParameter("@Activo", SqlDbType.Bit, ActivoCheckBox.Checked)
            BaseII.CreateMyParameter("@Cuenta", SqlDbType.NVarChar, Me.CuentaTextBox.Text, 250)
            BaseII.CreateMyParameter("@IdProgramada", SqlDbType.Int, CInt(Me.IdProgramadaTextBox.Text))
            BaseII.CreateMyParameter("@Clv_TipServ", SqlDbType.Int, cbTipserv.SelectedValue)
            Select Case opcion
                Case "N"
                    BaseII.Inserta("uspInsertaConceptoIngreso")
                Case "M"
                    BaseII.CreateMyParameter("@IdConcepto", SqlDbType.BigInt, CInt(Me.IdConceptoText.Text))
                    BaseII.Inserta("uspModificaConceptoIngreso")
            End Select
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "Filtra Conceptos")
        End Try
    End Sub

    Public Sub TraeConceptos()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Opcion", SqlDbType.BigInt, 3)
            BaseII.CreateMyParameter("@IDConcepto", SqlDbType.BigInt, CInt(Me.IdConceptoText.Text))
            Dim tblConceptos As DataTable = BaseII.ConsultaDT("uspFiltraConceptosIngresos")
            For Each Registro As DataRow In tblConceptos.Rows
                ConceptoText.Text = Registro("Concepto")
                PosicionText.Text = CInt(Registro("Posicion"))
                IdGrupoCombo.SelectedValue = CInt(Registro("IdGrupo"))
                Clv_ServicioComboBox.SelectedValue = CInt(Registro("Clv_Servicio"))
                CuentaTextBox.Text = Registro("Cuenta")
                IdProgramadaTextBox.Text = CInt(Registro("IdProgramada"))
                ActivoCheckBox.Checked = CBool(Registro("Activo"))
                cbTipserv.SelectedValue = CInt(Registro("clv_tipserv"))
            Next
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "Filtra Conceptos")
        End Try
    End Sub

    Private Sub PosicionText_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles PosicionText.KeyPress
        Dim caracter As Char = e.KeyChar
        If (Char.IsNumber(caracter)) Or _
                    (caracter = ChrW(Keys.Back)) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub IdProgramadaTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles IdProgramadaTextBox.KeyPress
        Dim caracter As Char = e.KeyChar
        If (Char.IsNumber(caracter)) Or _
                    (caracter = ChrW(Keys.Back)) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub


    Private Sub ExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitButton.Click
        Me.close()

    End Sub

    Public Function AgregaRegistro(ByVal prmTabla As DataTable, ByVal prmTexto As String, ByVal prmCampoOrden As String, ByVal prmTipoTabla As Integer) As DataTable
        Dim dT As DataTable = Nothing
        Dim UltimoReg As Integer = prmTabla.Rows.Count
        prmTabla.Rows.Add()

        Select Case prmTipoTabla
            Case 1 ' Grupo

                prmTabla.Rows(UltimoReg).Item(0) = "0"
                prmTabla.Rows(UltimoReg).Item(1) = prmTexto
                Dim DvwTabla As DataView = prmTabla.DefaultView
                DvwTabla.Sort = "NOMBRE"
                dT = DvwTabla.ToTable("tblCata", True, prmCampoOrden, "NOMBRE")
            Case 2 ' Colonias

        End Select
        Return dT
    End Function

End Class