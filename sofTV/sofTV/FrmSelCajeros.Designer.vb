﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelCajeros
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.lbxCajerosSinAsignar = New System.Windows.Forms.ListBox()
        Me.lbxCajerosAsignados = New System.Windows.Forms.ListBox()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnQuitarTodos = New System.Windows.Forms.Button()
        Me.btnAgregarUno = New System.Windows.Forms.Button()
        Me.btnAgregarTodos = New System.Windows.Forms.Button()
        Me.btnQuitarUno = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.cmbGruposVentas = New System.Windows.Forms.ComboBox()
        Me.lblGrupoVentas = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnSalir
        '
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(541, 355)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(127, 36)
        Me.btnSalir.TabIndex = 17
        Me.btnSalir.Text = "&SALIR"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'lbxCajerosSinAsignar
        '
        Me.lbxCajerosSinAsignar.DisplayMember = "Nombre"
        Me.lbxCajerosSinAsignar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbxCajerosSinAsignar.ItemHeight = 15
        Me.lbxCajerosSinAsignar.Location = New System.Drawing.Point(12, 45)
        Me.lbxCajerosSinAsignar.Name = "lbxCajerosSinAsignar"
        Me.lbxCajerosSinAsignar.Size = New System.Drawing.Size(276, 304)
        Me.lbxCajerosSinAsignar.TabIndex = 16
        Me.lbxCajerosSinAsignar.ValueMember = "Clv_Usuario"
        '
        'lbxCajerosAsignados
        '
        Me.lbxCajerosAsignados.DisplayMember = "Nombre"
        Me.lbxCajerosAsignados.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbxCajerosAsignados.ItemHeight = 15
        Me.lbxCajerosAsignados.Location = New System.Drawing.Point(392, 45)
        Me.lbxCajerosAsignados.Name = "lbxCajerosAsignados"
        Me.lbxCajerosAsignados.Size = New System.Drawing.Size(276, 304)
        Me.lbxCajerosAsignados.TabIndex = 15
        Me.lbxCajerosAsignados.ValueMember = "Clv_Usuario"
        '
        'btnAceptar
        '
        Me.btnAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.Location = New System.Drawing.Point(392, 355)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(127, 36)
        Me.btnAceptar.TabIndex = 14
        Me.btnAceptar.Text = "&ACEPTAR"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnQuitarTodos
        '
        Me.btnQuitarTodos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnQuitarTodos.Location = New System.Drawing.Point(307, 246)
        Me.btnQuitarTodos.Name = "btnQuitarTodos"
        Me.btnQuitarTodos.Size = New System.Drawing.Size(64, 25)
        Me.btnQuitarTodos.TabIndex = 13
        Me.btnQuitarTodos.Text = "<<"
        Me.btnQuitarTodos.UseVisualStyleBackColor = True
        '
        'btnAgregarUno
        '
        Me.btnAgregarUno.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregarUno.Location = New System.Drawing.Point(307, 61)
        Me.btnAgregarUno.Name = "btnAgregarUno"
        Me.btnAgregarUno.Size = New System.Drawing.Size(64, 25)
        Me.btnAgregarUno.TabIndex = 10
        Me.btnAgregarUno.Text = ">"
        Me.btnAgregarUno.UseVisualStyleBackColor = True
        '
        'btnAgregarTodos
        '
        Me.btnAgregarTodos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregarTodos.Location = New System.Drawing.Point(307, 92)
        Me.btnAgregarTodos.Name = "btnAgregarTodos"
        Me.btnAgregarTodos.Size = New System.Drawing.Size(64, 25)
        Me.btnAgregarTodos.TabIndex = 11
        Me.btnAgregarTodos.Text = ">>"
        Me.btnAgregarTodos.UseVisualStyleBackColor = True
        '
        'btnQuitarUno
        '
        Me.btnQuitarUno.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnQuitarUno.Location = New System.Drawing.Point(307, 215)
        Me.btnQuitarUno.Name = "btnQuitarUno"
        Me.btnQuitarUno.Size = New System.Drawing.Size(64, 25)
        Me.btnQuitarUno.TabIndex = 12
        Me.btnQuitarUno.Text = "<"
        Me.btnQuitarUno.UseVisualStyleBackColor = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'cmbGruposVentas
        '
        Me.cmbGruposVentas.DisplayMember = "grupo"
        Me.cmbGruposVentas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbGruposVentas.FormattingEnabled = True
        Me.cmbGruposVentas.Location = New System.Drawing.Point(147, 12)
        Me.cmbGruposVentas.Name = "cmbGruposVentas"
        Me.cmbGruposVentas.Size = New System.Drawing.Size(320, 24)
        Me.cmbGruposVentas.TabIndex = 18
        Me.cmbGruposVentas.ValueMember = "clvGrupo"
        '
        'lblGrupoVentas
        '
        Me.lblGrupoVentas.AutoSize = True
        Me.lblGrupoVentas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrupoVentas.Location = New System.Drawing.Point(9, 15)
        Me.lblGrupoVentas.Name = "lblGrupoVentas"
        Me.lblGrupoVentas.Size = New System.Drawing.Size(132, 16)
        Me.lblGrupoVentas.TabIndex = 19
        Me.lblGrupoVentas.Text = "Grupo de Ventas :"
        '
        'FrmSelCajeros
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(680, 397)
        Me.Controls.Add(Me.lblGrupoVentas)
        Me.Controls.Add(Me.cmbGruposVentas)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.lbxCajerosSinAsignar)
        Me.Controls.Add(Me.lbxCajerosAsignados)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.btnQuitarTodos)
        Me.Controls.Add(Me.btnAgregarUno)
        Me.Controls.Add(Me.btnAgregarTodos)
        Me.Controls.Add(Me.btnQuitarUno)
        Me.Name = "FrmSelCajeros"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Seleccion Cajeros"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents lbxCajerosSinAsignar As System.Windows.Forms.ListBox
    Friend WithEvents lbxCajerosAsignados As System.Windows.Forms.ListBox
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnQuitarTodos As System.Windows.Forms.Button
    Friend WithEvents btnAgregarUno As System.Windows.Forms.Button
    Friend WithEvents btnAgregarTodos As System.Windows.Forms.Button
    Friend WithEvents btnQuitarUno As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents cmbGruposVentas As System.Windows.Forms.ComboBox
    Friend WithEvents lblGrupoVentas As System.Windows.Forms.Label
End Class
