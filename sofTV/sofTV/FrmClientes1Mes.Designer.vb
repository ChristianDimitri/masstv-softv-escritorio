﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmClientes1Mes
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.CMBlblTitulo = New System.Windows.Forms.Label()
        Me.cbxInstalados = New System.Windows.Forms.CheckBox()
        Me.cbxDesconectados = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'btnAceptar
        '
        Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.Location = New System.Drawing.Point(26, 170)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(90, 39)
        Me.btnAceptar.TabIndex = 2
        Me.btnAceptar.Text = "ACEPTAR"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(170, 170)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(90, 39)
        Me.btnCancelar.TabIndex = 3
        Me.btnCancelar.Text = "CANCELAR"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'CMBlblTitulo
        '
        Me.CMBlblTitulo.AutoSize = True
        Me.CMBlblTitulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblTitulo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBlblTitulo.Location = New System.Drawing.Point(23, 33)
        Me.CMBlblTitulo.Name = "CMBlblTitulo"
        Me.CMBlblTitulo.Size = New System.Drawing.Size(237, 18)
        Me.CMBlblTitulo.TabIndex = 4
        Me.CMBlblTitulo.Text = "Seleccionar Status Del Cliente"
        '
        'cbxInstalados
        '
        Me.cbxInstalados.AutoSize = True
        Me.cbxInstalados.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxInstalados.Location = New System.Drawing.Point(26, 95)
        Me.cbxInstalados.Name = "cbxInstalados"
        Me.cbxInstalados.Size = New System.Drawing.Size(92, 19)
        Me.cbxInstalados.TabIndex = 5
        Me.cbxInstalados.Text = "Instalados"
        Me.cbxInstalados.UseVisualStyleBackColor = True
        '
        'cbxDesconectados
        '
        Me.cbxDesconectados.AutoSize = True
        Me.cbxDesconectados.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxDesconectados.Location = New System.Drawing.Point(136, 95)
        Me.cbxDesconectados.Name = "cbxDesconectados"
        Me.cbxDesconectados.Size = New System.Drawing.Size(124, 19)
        Me.cbxDesconectados.TabIndex = 6
        Me.cbxDesconectados.Text = "Desconectados"
        Me.cbxDesconectados.UseVisualStyleBackColor = True
        '
        'FrmClientes1Mes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 221)
        Me.Controls.Add(Me.cbxDesconectados)
        Me.Controls.Add(Me.cbxInstalados)
        Me.Controls.Add(Me.CMBlblTitulo)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Name = "FrmClientes1Mes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Seleccionar Status Del Cliente"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents CMBlblTitulo As System.Windows.Forms.Label
    Friend WithEvents cbxInstalados As System.Windows.Forms.CheckBox
    Friend WithEvents cbxDesconectados As System.Windows.Forms.CheckBox
End Class
