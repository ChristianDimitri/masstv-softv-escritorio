<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDescuentoCombo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim ContLabel As System.Windows.Forms.Label
        Dim MensLabel As System.Windows.Forms.Label
        Dim RecoLabel As System.Windows.Forms.Label
        Dim PuntosLabel As System.Windows.Forms.Label
        Dim ConceptoLabel As System.Windows.Forms.Label
        Dim DescripcionLabel1 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmDescuentoCombo))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.DataSetEric2 = New sofTV.DataSetEric2
        Me.MuestraTipoClientesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraTipoClientesTableAdapter = New sofTV.DataSetEric2TableAdapters.MuestraTipoClientesTableAdapter
        Me.DescripcionComboBox = New System.Windows.Forms.ComboBox
        Me.MuestraTipServEricBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraTipServEricTableAdapter = New sofTV.DataSetEric2TableAdapters.MuestraTipServEricTableAdapter
        Me.ConceptoComboBox = New System.Windows.Forms.ComboBox
        Me.MuestraServiciosEricBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraServiciosEricTableAdapter = New sofTV.DataSetEric2TableAdapters.MuestraServiciosEricTableAdapter
        Me.DescripcionComboBox1 = New System.Windows.Forms.ComboBox
        Me.Clv_ServicioTextBox1 = New System.Windows.Forms.TextBox
        Me.Clv_TipoClienteTextBox = New System.Windows.Forms.TextBox
        Me.Clv_TipSerTextBox1 = New System.Windows.Forms.TextBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button6 = New System.Windows.Forms.Button
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.TextBox4 = New System.Windows.Forms.TextBox
        Me.BorDetDescuentoComboBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorDetDescuentoComboTableAdapter = New sofTV.DataSetEric2TableAdapters.BorDetDescuentoComboTableAdapter
        Me.EliPreDetDescuentoComboBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EliPreDetDescuentoComboTableAdapter = New sofTV.DataSetEric2TableAdapters.EliPreDetDescuentoComboTableAdapter
        Me.Dame_clv_session_ReportesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_clv_session_ReportesTableAdapter = New sofTV.DataSetEric2TableAdapters.Dame_clv_session_ReportesTableAdapter
        Me.Label1 = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Label3 = New System.Windows.Forms.Label
        Me.TextBox6 = New System.Windows.Forms.TextBox
        Me.PuntosTextBox = New System.Windows.Forms.TextBox
        Me.ConDetDescuentoCombo1BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LlamadaTextBox = New System.Windows.Forms.TextBox
        Me.ContTextBox = New System.Windows.Forms.TextBox
        Me.MensTextBox = New System.Windows.Forms.TextBox
        Me.RecoTextBox = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.TextBox5 = New System.Windows.Forms.TextBox
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.ModDescuentoComboBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ModDescuentoComboTableAdapter = New sofTV.DataSetEric2TableAdapters.ModDescuentoComboTableAdapter
        Me.NueDescuentoComboBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NueDescuentoComboTableAdapter = New sofTV.DataSetEric2TableAdapters.NueDescuentoComboTableAdapter
        Me.txtClvDescuentoOriginal = New System.Windows.Forms.TextBox
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton
        Me.DameClv_DescuentoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameClv_DescuentoTableAdapter = New sofTV.DataSetEric2TableAdapters.DameClv_DescuentoTableAdapter
        Me.AgrPreDetDescuentoComboBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AgrPreDetDescuentoComboTableAdapter = New sofTV.DataSetEric2TableAdapters.AgrPreDetDescuentoComboTableAdapter
        Me.NueDetDescuentoComboBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NueDetDescuentoComboTableAdapter = New sofTV.DataSetEric2TableAdapters.NueDetDescuentoComboTableAdapter
        Me.ConDetDescuentoCombo1TableAdapter = New sofTV.DataSetEric2TableAdapters.ConDetDescuentoCombo1TableAdapter
        Me.ConDetDescuentoCombo1DataGridView = New System.Windows.Forms.DataGridView
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ClaveTextBox = New System.Windows.Forms.TextBox
        Me.Clv_DescuentoTextBox = New System.Windows.Forms.TextBox
        Me.Clv_TipSerTextBox = New System.Windows.Forms.TextBox
        Me.Clv_ServicioTextBox = New System.Windows.Forms.TextBox
        Me.CMBLabel4 = New System.Windows.Forms.Label
        ContLabel = New System.Windows.Forms.Label
        MensLabel = New System.Windows.Forms.Label
        RecoLabel = New System.Windows.Forms.Label
        PuntosLabel = New System.Windows.Forms.Label
        ConceptoLabel = New System.Windows.Forms.Label
        DescripcionLabel1 = New System.Windows.Forms.Label
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTipoClientesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTipServEricBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraServiciosEricBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorDetDescuentoComboBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EliPreDetDescuentoComboBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_clv_session_ReportesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.ConDetDescuentoCombo1BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.ModDescuentoComboBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NueDescuentoComboBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        CType(Me.DameClv_DescuentoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AgrPreDetDescuentoComboBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NueDetDescuentoComboBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConDetDescuentoCombo1DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ContLabel
        '
        ContLabel.AutoSize = True
        ContLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ContLabel.Location = New System.Drawing.Point(47, 42)
        ContLabel.Name = "ContLabel"
        ContLabel.Size = New System.Drawing.Size(79, 13)
        ContLabel.TabIndex = 12
        ContLabel.Text = "Contratación"
        '
        'MensLabel
        '
        MensLabel.AutoSize = True
        MensLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        MensLabel.Location = New System.Drawing.Point(177, 40)
        MensLabel.Name = "MensLabel"
        MensLabel.Size = New System.Drawing.Size(78, 13)
        MensLabel.TabIndex = 14
        MensLabel.Text = "Mensualidad"
        '
        'RecoLabel
        '
        RecoLabel.AutoSize = True
        RecoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        RecoLabel.Location = New System.Drawing.Point(312, 40)
        RecoLabel.Name = "RecoLabel"
        RecoLabel.Size = New System.Drawing.Size(74, 13)
        RecoLabel.TabIndex = 16
        RecoLabel.Text = "Reconexión"
        '
        'PuntosLabel
        '
        PuntosLabel.AutoSize = True
        PuntosLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PuntosLabel.Location = New System.Drawing.Point(461, 42)
        PuntosLabel.Name = "PuntosLabel"
        PuntosLabel.Size = New System.Drawing.Size(46, 13)
        PuntosLabel.TabIndex = 18
        PuntosLabel.Text = "Puntos"
        '
        'ConceptoLabel
        '
        ConceptoLabel.AutoSize = True
        ConceptoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ConceptoLabel.Location = New System.Drawing.Point(74, 11)
        ConceptoLabel.Name = "ConceptoLabel"
        ConceptoLabel.Size = New System.Drawing.Size(114, 15)
        ConceptoLabel.TabIndex = 22
        ConceptoLabel.Text = "Tipo de Servicio:"
        '
        'DescripcionLabel1
        '
        DescripcionLabel1.AutoSize = True
        DescripcionLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescripcionLabel1.Location = New System.Drawing.Point(126, 52)
        DescripcionLabel1.Name = "DescripcionLabel1"
        DescripcionLabel1.Size = New System.Drawing.Size(62, 15)
        DescripcionLabel1.TabIndex = 24
        DescripcionLabel1.Text = "Servicio:"
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric2"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MuestraTipoClientesBindingSource
        '
        Me.MuestraTipoClientesBindingSource.DataMember = "MuestraTipoClientes"
        Me.MuestraTipoClientesBindingSource.DataSource = Me.DataSetEric2
        '
        'MuestraTipoClientesTableAdapter
        '
        Me.MuestraTipoClientesTableAdapter.ClearBeforeFill = True
        '
        'DescripcionComboBox
        '
        Me.DescripcionComboBox.DataSource = Me.MuestraTipoClientesBindingSource
        Me.DescripcionComboBox.DisplayMember = "Descripcion"
        Me.DescripcionComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionComboBox.FormattingEnabled = True
        Me.DescripcionComboBox.Location = New System.Drawing.Point(282, 84)
        Me.DescripcionComboBox.Name = "DescripcionComboBox"
        Me.DescripcionComboBox.Size = New System.Drawing.Size(267, 23)
        Me.DescripcionComboBox.TabIndex = 1
        Me.DescripcionComboBox.ValueMember = "Clv_TipoCliente"
        '
        'MuestraTipServEricBindingSource
        '
        Me.MuestraTipServEricBindingSource.DataMember = "MuestraTipServEric"
        Me.MuestraTipServEricBindingSource.DataSource = Me.DataSetEric2
        '
        'MuestraTipServEricTableAdapter
        '
        Me.MuestraTipServEricTableAdapter.ClearBeforeFill = True
        '
        'ConceptoComboBox
        '
        Me.ConceptoComboBox.DataSource = Me.MuestraTipServEricBindingSource
        Me.ConceptoComboBox.DisplayMember = "Concepto"
        Me.ConceptoComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConceptoComboBox.FormattingEnabled = True
        Me.ConceptoComboBox.Location = New System.Drawing.Point(197, 3)
        Me.ConceptoComboBox.Name = "ConceptoComboBox"
        Me.ConceptoComboBox.Size = New System.Drawing.Size(267, 23)
        Me.ConceptoComboBox.TabIndex = 3
        Me.ConceptoComboBox.ValueMember = "Clv_TipSer"
        '
        'MuestraServiciosEricBindingSource
        '
        Me.MuestraServiciosEricBindingSource.DataMember = "MuestraServiciosEric"
        Me.MuestraServiciosEricBindingSource.DataSource = Me.DataSetEric2
        '
        'MuestraServiciosEricTableAdapter
        '
        Me.MuestraServiciosEricTableAdapter.ClearBeforeFill = True
        '
        'DescripcionComboBox1
        '
        Me.DescripcionComboBox1.DataSource = Me.MuestraServiciosEricBindingSource
        Me.DescripcionComboBox1.DisplayMember = "Descripcion"
        Me.DescripcionComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionComboBox1.FormattingEnabled = True
        Me.DescripcionComboBox1.Location = New System.Drawing.Point(197, 44)
        Me.DescripcionComboBox1.Name = "DescripcionComboBox1"
        Me.DescripcionComboBox1.Size = New System.Drawing.Size(267, 23)
        Me.DescripcionComboBox1.TabIndex = 4
        Me.DescripcionComboBox1.ValueMember = "Clv_Servicio"
        '
        'Clv_ServicioTextBox1
        '
        Me.Clv_ServicioTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraServiciosEricBindingSource, "Clv_Servicio", True))
        Me.Clv_ServicioTextBox1.Location = New System.Drawing.Point(413, 5)
        Me.Clv_ServicioTextBox1.Name = "Clv_ServicioTextBox1"
        Me.Clv_ServicioTextBox1.ReadOnly = True
        Me.Clv_ServicioTextBox1.Size = New System.Drawing.Size(10, 20)
        Me.Clv_ServicioTextBox1.TabIndex = 26
        Me.Clv_ServicioTextBox1.TabStop = False
        '
        'Clv_TipoClienteTextBox
        '
        Me.Clv_TipoClienteTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraTipoClientesBindingSource, "Clv_TipoCliente", True))
        Me.Clv_TipoClienteTextBox.Location = New System.Drawing.Point(397, 5)
        Me.Clv_TipoClienteTextBox.Name = "Clv_TipoClienteTextBox"
        Me.Clv_TipoClienteTextBox.ReadOnly = True
        Me.Clv_TipoClienteTextBox.Size = New System.Drawing.Size(10, 20)
        Me.Clv_TipoClienteTextBox.TabIndex = 27
        Me.Clv_TipoClienteTextBox.TabStop = False
        '
        'Clv_TipSerTextBox1
        '
        Me.Clv_TipSerTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraTipServEricBindingSource, "Clv_TipSer", True))
        Me.Clv_TipSerTextBox1.Location = New System.Drawing.Point(383, 5)
        Me.Clv_TipSerTextBox1.Name = "Clv_TipSerTextBox1"
        Me.Clv_TipSerTextBox1.ReadOnly = True
        Me.Clv_TipSerTextBox1.Size = New System.Drawing.Size(10, 20)
        Me.Clv_TipSerTextBox1.TabIndex = 28
        Me.Clv_TipSerTextBox1.TabStop = False
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(75, 96)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(109, 32)
        Me.Button1.TabIndex = 11
        Me.Button1.Text = "&Agregar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'CheckBox1
        '
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.ForeColor = System.Drawing.Color.SteelBlue
        Me.CheckBox1.Location = New System.Drawing.Point(61, 5)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(10, 10)
        Me.CheckBox1.TabIndex = 3
        Me.CheckBox1.Text = "Aplicar Combo a Todos los Tipos de Cliente"
        Me.CheckBox1.UseVisualStyleBackColor = True
        Me.CheckBox1.Visible = False
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(26, 56)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(116, 21)
        Me.TextBox1.TabIndex = 6
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(190, 96)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(109, 32)
        Me.Button2.TabIndex = 12
        Me.Button2.Text = "&Modificar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(305, 96)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(109, 32)
        Me.Button3.TabIndex = 13
        Me.Button3.Text = "&Guardar"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(416, 96)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(109, 32)
        Me.Button4.TabIndex = 14
        Me.Button4.Text = "&Eliminar"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(528, 96)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(109, 32)
        Me.Button5.TabIndex = 15
        Me.Button5.Text = "&Cancelar"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(651, 578)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(136, 36)
        Me.Button6.TabIndex = 19
        Me.Button6.Text = "&SALIR"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'TextBox2
        '
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(161, 56)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(113, 21)
        Me.TextBox2.TabIndex = 7
        '
        'TextBox3
        '
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(294, 56)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(116, 21)
        Me.TextBox3.TabIndex = 8
        '
        'TextBox4
        '
        Me.TextBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox4.Location = New System.Drawing.Point(426, 56)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(116, 21)
        Me.TextBox4.TabIndex = 9
        '
        'BorDetDescuentoComboBindingSource
        '
        Me.BorDetDescuentoComboBindingSource.DataMember = "BorDetDescuentoCombo"
        Me.BorDetDescuentoComboBindingSource.DataSource = Me.DataSetEric2
        '
        'BorDetDescuentoComboTableAdapter
        '
        Me.BorDetDescuentoComboTableAdapter.ClearBeforeFill = True
        '
        'EliPreDetDescuentoComboBindingSource
        '
        Me.EliPreDetDescuentoComboBindingSource.DataMember = "EliPreDetDescuentoCombo"
        Me.EliPreDetDescuentoComboBindingSource.DataSource = Me.DataSetEric2
        '
        'EliPreDetDescuentoComboTableAdapter
        '
        Me.EliPreDetDescuentoComboTableAdapter.ClearBeforeFill = True
        '
        'Dame_clv_session_ReportesBindingSource
        '
        Me.Dame_clv_session_ReportesBindingSource.DataMember = "Dame_clv_session_Reportes"
        Me.Dame_clv_session_ReportesBindingSource.DataSource = Me.DataSetEric2
        '
        'Dame_clv_session_ReportesTableAdapter
        '
        Me.Dame_clv_session_ReportesTableAdapter.ClearBeforeFill = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(165, 92)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(108, 15)
        Me.Label1.TabIndex = 21
        Me.Label1.Text = "Tipo de Cliente:"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.CMBLabel4)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(ContLabel)
        Me.Panel2.Controls.Add(Me.Button5)
        Me.Panel2.Controls.Add(Me.TextBox6)
        Me.Panel2.Controls.Add(Me.Button4)
        Me.Panel2.Controls.Add(Me.TextBox4)
        Me.Panel2.Controls.Add(Me.Button3)
        Me.Panel2.Controls.Add(Me.TextBox3)
        Me.Panel2.Controls.Add(Me.Button2)
        Me.Panel2.Controls.Add(Me.TextBox2)
        Me.Panel2.Controls.Add(Me.Button1)
        Me.Panel2.Controls.Add(Me.PuntosTextBox)
        Me.Panel2.Controls.Add(PuntosLabel)
        Me.Panel2.Controls.Add(Me.LlamadaTextBox)
        Me.Panel2.Controls.Add(Me.RecoTextBox)
        Me.Panel2.Controls.Add(MensLabel)
        Me.Panel2.Controls.Add(Me.MensTextBox)
        Me.Panel2.Controls.Add(RecoLabel)
        Me.Panel2.Controls.Add(Me.TextBox1)
        Me.Panel2.Controls.Add(Me.ContTextBox)
        Me.Panel2.Location = New System.Drawing.Point(35, 201)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(701, 140)
        Me.Panel2.TabIndex = 5
        Me.Panel2.TabStop = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(569, 40)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(106, 13)
        Me.Label3.TabIndex = 42
        Me.Label3.Text = "llamada Adicional"
        '
        'TextBox6
        '
        Me.TextBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox6.Location = New System.Drawing.Point(564, 56)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(116, 21)
        Me.TextBox6.TabIndex = 10
        '
        'PuntosTextBox
        '
        Me.PuntosTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConDetDescuentoCombo1BindingSource, "Puntos", True))
        Me.PuntosTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PuntosTextBox.Location = New System.Drawing.Point(426, 56)
        Me.PuntosTextBox.Name = "PuntosTextBox"
        Me.PuntosTextBox.ReadOnly = True
        Me.PuntosTextBox.Size = New System.Drawing.Size(116, 21)
        Me.PuntosTextBox.TabIndex = 39
        Me.PuntosTextBox.TabStop = False
        '
        'ConDetDescuentoCombo1BindingSource
        '
        Me.ConDetDescuentoCombo1BindingSource.DataMember = "ConDetDescuentoCombo1"
        Me.ConDetDescuentoCombo1BindingSource.DataSource = Me.DataSetEric2
        '
        'LlamadaTextBox
        '
        Me.LlamadaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConDetDescuentoCombo1BindingSource, "Llamada", True))
        Me.LlamadaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LlamadaTextBox.Location = New System.Drawing.Point(564, 56)
        Me.LlamadaTextBox.Name = "LlamadaTextBox"
        Me.LlamadaTextBox.ReadOnly = True
        Me.LlamadaTextBox.Size = New System.Drawing.Size(116, 21)
        Me.LlamadaTextBox.TabIndex = 40
        Me.LlamadaTextBox.TabStop = False
        '
        'ContTextBox
        '
        Me.ContTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConDetDescuentoCombo1BindingSource, "Cont", True))
        Me.ContTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContTextBox.Location = New System.Drawing.Point(26, 56)
        Me.ContTextBox.Name = "ContTextBox"
        Me.ContTextBox.ReadOnly = True
        Me.ContTextBox.Size = New System.Drawing.Size(116, 21)
        Me.ContTextBox.TabIndex = 36
        Me.ContTextBox.TabStop = False
        '
        'MensTextBox
        '
        Me.MensTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConDetDescuentoCombo1BindingSource, "Mens", True))
        Me.MensTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MensTextBox.Location = New System.Drawing.Point(161, 56)
        Me.MensTextBox.Name = "MensTextBox"
        Me.MensTextBox.ReadOnly = True
        Me.MensTextBox.Size = New System.Drawing.Size(113, 21)
        Me.MensTextBox.TabIndex = 37
        Me.MensTextBox.TabStop = False
        '
        'RecoTextBox
        '
        Me.RecoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConDetDescuentoCombo1BindingSource, "Reco", True))
        Me.RecoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RecoTextBox.Location = New System.Drawing.Point(294, 56)
        Me.RecoTextBox.Name = "RecoTextBox"
        Me.RecoTextBox.ReadOnly = True
        Me.RecoTextBox.Size = New System.Drawing.Size(116, 21)
        Me.RecoTextBox.TabIndex = 38
        Me.RecoTextBox.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(138, 50)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(135, 15)
        Me.Label2.TabIndex = 23
        Me.Label2.Text = "Nombre del Combo:"
        '
        'TextBox5
        '
        Me.TextBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox5.Location = New System.Drawing.Point(282, 44)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(267, 21)
        Me.TextBox5.TabIndex = 0
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(ConceptoLabel)
        Me.Panel3.Controls.Add(Me.DescripcionComboBox1)
        Me.Panel3.Controls.Add(DescripcionLabel1)
        Me.Panel3.Controls.Add(Me.ConceptoComboBox)
        Me.Panel3.Location = New System.Drawing.Point(85, 119)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(587, 76)
        Me.Panel3.TabIndex = 2
        Me.Panel3.TabStop = True
        '
        'ModDescuentoComboBindingSource
        '
        Me.ModDescuentoComboBindingSource.DataMember = "ModDescuentoCombo"
        Me.ModDescuentoComboBindingSource.DataSource = Me.DataSetEric2
        '
        'ModDescuentoComboTableAdapter
        '
        Me.ModDescuentoComboTableAdapter.ClearBeforeFill = True
        '
        'NueDescuentoComboBindingSource
        '
        Me.NueDescuentoComboBindingSource.DataMember = "NueDescuentoCombo"
        Me.NueDescuentoComboBindingSource.DataSource = Me.DataSetEric2
        '
        'NueDescuentoComboTableAdapter
        '
        Me.NueDescuentoComboTableAdapter.ClearBeforeFill = True
        '
        'txtClvDescuentoOriginal
        '
        Me.txtClvDescuentoOriginal.Location = New System.Drawing.Point(257, 5)
        Me.txtClvDescuentoOriginal.Name = "txtClvDescuentoOriginal"
        Me.txtClvDescuentoOriginal.ReadOnly = True
        Me.txtClvDescuentoOriginal.Size = New System.Drawing.Size(10, 20)
        Me.txtClvDescuentoOriginal.TabIndex = 29
        Me.txtClvDescuentoOriginal.TabStop = False
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Nothing
        Me.BindingNavigator1.CountItem = Nothing
        Me.BindingNavigator1.DeleteItem = Nothing
        Me.BindingNavigator1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.ToolStripButton1})
        Me.BindingNavigator1.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigator1.MoveFirstItem = Nothing
        Me.BindingNavigator1.MoveLastItem = Nothing
        Me.BindingNavigator1.MoveNextItem = Nothing
        Me.BindingNavigator1.MovePreviousItem = Nothing
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Nothing
        Me.BindingNavigator1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.BindingNavigator1.Size = New System.Drawing.Size(799, 25)
        Me.BindingNavigator1.TabIndex = 30
        Me.BindingNavigator1.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(121, 22)
        Me.BindingNavigatorDeleteItem.Text = "&Eliminar Combo"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(106, 22)
        Me.ToolStripButton1.Text = "&Guardar Combo"
        '
        'DameClv_DescuentoBindingSource
        '
        Me.DameClv_DescuentoBindingSource.DataMember = "DameClv_Descuento"
        Me.DameClv_DescuentoBindingSource.DataSource = Me.DataSetEric2
        '
        'DameClv_DescuentoTableAdapter
        '
        Me.DameClv_DescuentoTableAdapter.ClearBeforeFill = True
        '
        'AgrPreDetDescuentoComboBindingSource
        '
        Me.AgrPreDetDescuentoComboBindingSource.DataMember = "AgrPreDetDescuentoCombo"
        Me.AgrPreDetDescuentoComboBindingSource.DataSource = Me.DataSetEric2
        '
        'AgrPreDetDescuentoComboTableAdapter
        '
        Me.AgrPreDetDescuentoComboTableAdapter.ClearBeforeFill = True
        '
        'NueDetDescuentoComboBindingSource
        '
        Me.NueDetDescuentoComboBindingSource.DataMember = "NueDetDescuentoCombo"
        Me.NueDetDescuentoComboBindingSource.DataSource = Me.DataSetEric2
        '
        'NueDetDescuentoComboTableAdapter
        '
        Me.NueDetDescuentoComboTableAdapter.ClearBeforeFill = True
        '
        'ConDetDescuentoCombo1TableAdapter
        '
        Me.ConDetDescuentoCombo1TableAdapter.ClearBeforeFill = True
        '
        'ConDetDescuentoCombo1DataGridView
        '
        Me.ConDetDescuentoCombo1DataGridView.AllowUserToAddRows = False
        Me.ConDetDescuentoCombo1DataGridView.AllowUserToDeleteRows = False
        Me.ConDetDescuentoCombo1DataGridView.AutoGenerateColumns = False
        Me.ConDetDescuentoCombo1DataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ConDetDescuentoCombo1DataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.ConDetDescuentoCombo1DataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9, Me.DataGridViewTextBoxColumn10, Me.DataGridViewTextBoxColumn11})
        Me.ConDetDescuentoCombo1DataGridView.DataSource = Me.ConDetDescuentoCombo1BindingSource
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ConDetDescuentoCombo1DataGridView.DefaultCellStyle = DataGridViewCellStyle2
        Me.ConDetDescuentoCombo1DataGridView.Location = New System.Drawing.Point(35, 347)
        Me.ConDetDescuentoCombo1DataGridView.Name = "ConDetDescuentoCombo1DataGridView"
        Me.ConDetDescuentoCombo1DataGridView.ReadOnly = True
        Me.ConDetDescuentoCombo1DataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.ConDetDescuentoCombo1DataGridView.Size = New System.Drawing.Size(701, 220)
        Me.ConDetDescuentoCombo1DataGridView.TabIndex = 31
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Clave"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Clave"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Clv_Descuento"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Clv_Descuento"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Clv_TipSer"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Clv_TipSer"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Clv_Servicio"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Clv_Servicio"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Descripcion"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Servicio"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 150
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "Cont"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Contratación"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "Mens"
        Me.DataGridViewTextBoxColumn7.HeaderText = "Mensualidad"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "Reco"
        Me.DataGridViewTextBoxColumn8.HeaderText = "Reconexión"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "Puntos"
        Me.DataGridViewTextBoxColumn9.HeaderText = "Puntos"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "Llamada"
        Me.DataGridViewTextBoxColumn10.HeaderText = "Llamada"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "Combo"
        Me.DataGridViewTextBoxColumn11.HeaderText = "Combo"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.Visible = False
        '
        'ClaveTextBox
        '
        Me.ClaveTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConDetDescuentoCombo1BindingSource, "Clave", True))
        Me.ClaveTextBox.Location = New System.Drawing.Point(302, 5)
        Me.ClaveTextBox.Name = "ClaveTextBox"
        Me.ClaveTextBox.ReadOnly = True
        Me.ClaveTextBox.Size = New System.Drawing.Size(10, 20)
        Me.ClaveTextBox.TabIndex = 32
        Me.ClaveTextBox.TabStop = False
        '
        'Clv_DescuentoTextBox
        '
        Me.Clv_DescuentoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConDetDescuentoCombo1BindingSource, "Clv_Descuento", True))
        Me.Clv_DescuentoTextBox.Location = New System.Drawing.Point(318, 5)
        Me.Clv_DescuentoTextBox.Name = "Clv_DescuentoTextBox"
        Me.Clv_DescuentoTextBox.ReadOnly = True
        Me.Clv_DescuentoTextBox.Size = New System.Drawing.Size(10, 20)
        Me.Clv_DescuentoTextBox.TabIndex = 33
        Me.Clv_DescuentoTextBox.TabStop = False
        '
        'Clv_TipSerTextBox
        '
        Me.Clv_TipSerTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConDetDescuentoCombo1BindingSource, "Clv_TipSer", True))
        Me.Clv_TipSerTextBox.Location = New System.Drawing.Point(334, 5)
        Me.Clv_TipSerTextBox.Name = "Clv_TipSerTextBox"
        Me.Clv_TipSerTextBox.ReadOnly = True
        Me.Clv_TipSerTextBox.Size = New System.Drawing.Size(10, 20)
        Me.Clv_TipSerTextBox.TabIndex = 34
        Me.Clv_TipSerTextBox.TabStop = False
        '
        'Clv_ServicioTextBox
        '
        Me.Clv_ServicioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConDetDescuentoCombo1BindingSource, "Clv_Servicio", True))
        Me.Clv_ServicioTextBox.Location = New System.Drawing.Point(350, 5)
        Me.Clv_ServicioTextBox.Name = "Clv_ServicioTextBox"
        Me.Clv_ServicioTextBox.ReadOnly = True
        Me.Clv_ServicioTextBox.Size = New System.Drawing.Size(10, 20)
        Me.Clv_ServicioTextBox.TabIndex = 35
        Me.Clv_ServicioTextBox.TabStop = False
        '
        'CMBLabel4
        '
        Me.CMBLabel4.AutoSize = True
        Me.CMBLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel4.Location = New System.Drawing.Point(23, 12)
        Me.CMBLabel4.Name = "CMBLabel4"
        Me.CMBLabel4.Size = New System.Drawing.Size(98, 16)
        Me.CMBLabel4.TabIndex = 43
        Me.CMBLabel4.Text = "Descuentos :"
        '
        'FrmDescuentoCombo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(799, 626)
        Me.Controls.Add(Me.BindingNavigator1)
        Me.Controls.Add(Me.Clv_ServicioTextBox)
        Me.Controls.Add(Me.Clv_TipSerTextBox)
        Me.Controls.Add(Me.Clv_DescuentoTextBox)
        Me.Controls.Add(Me.ClaveTextBox)
        Me.Controls.Add(Me.ConDetDescuentoCombo1DataGridView)
        Me.Controls.Add(Me.txtClvDescuentoOriginal)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.DescripcionComboBox)
        Me.Controls.Add(Me.TextBox5)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Clv_TipSerTextBox1)
        Me.Controls.Add(Me.Clv_TipoClienteTextBox)
        Me.Controls.Add(Me.Clv_ServicioTextBox1)
        Me.Name = "FrmDescuentoCombo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Descuento Combo"
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTipoClientesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTipServEricBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraServiciosEricBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorDetDescuentoComboBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EliPreDetDescuentoComboBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_clv_session_ReportesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.ConDetDescuentoCombo1BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.ModDescuentoComboBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NueDescuentoComboBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        CType(Me.DameClv_DescuentoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AgrPreDetDescuentoComboBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NueDetDescuentoComboBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConDetDescuentoCombo1DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric2
    Friend WithEvents MuestraTipoClientesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipoClientesTableAdapter As sofTV.DataSetEric2TableAdapters.MuestraTipoClientesTableAdapter
    Friend WithEvents DescripcionComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents MuestraTipServEricBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipServEricTableAdapter As sofTV.DataSetEric2TableAdapters.MuestraTipServEricTableAdapter
    Friend WithEvents ConceptoComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents MuestraServiciosEricBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraServiciosEricTableAdapter As sofTV.DataSetEric2TableAdapters.MuestraServiciosEricTableAdapter
    Friend WithEvents DescripcionComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Clv_ServicioTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Clv_TipoClienteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_TipSerTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents BorDetDescuentoComboBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorDetDescuentoComboTableAdapter As sofTV.DataSetEric2TableAdapters.BorDetDescuentoComboTableAdapter
    Friend WithEvents EliPreDetDescuentoComboBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EliPreDetDescuentoComboTableAdapter As sofTV.DataSetEric2TableAdapters.EliPreDetDescuentoComboTableAdapter
    Friend WithEvents Dame_clv_session_ReportesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_clv_session_ReportesTableAdapter As sofTV.DataSetEric2TableAdapters.Dame_clv_session_ReportesTableAdapter
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents ModDescuentoComboBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ModDescuentoComboTableAdapter As sofTV.DataSetEric2TableAdapters.ModDescuentoComboTableAdapter
    Friend WithEvents NueDescuentoComboBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NueDescuentoComboTableAdapter As sofTV.DataSetEric2TableAdapters.NueDescuentoComboTableAdapter
    Friend WithEvents txtClvDescuentoOriginal As System.Windows.Forms.TextBox
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents DameClv_DescuentoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameClv_DescuentoTableAdapter As sofTV.DataSetEric2TableAdapters.DameClv_DescuentoTableAdapter
    Friend WithEvents AgrPreDetDescuentoComboBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AgrPreDetDescuentoComboTableAdapter As sofTV.DataSetEric2TableAdapters.AgrPreDetDescuentoComboTableAdapter
    Friend WithEvents NueDetDescuentoComboBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NueDetDescuentoComboTableAdapter As sofTV.DataSetEric2TableAdapters.NueDetDescuentoComboTableAdapter
    Friend WithEvents ConDetDescuentoCombo1BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConDetDescuentoCombo1TableAdapter As sofTV.DataSetEric2TableAdapters.ConDetDescuentoCombo1TableAdapter
    Friend WithEvents LlamadaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PuntosTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ContTextBox As System.Windows.Forms.TextBox
    Friend WithEvents RecoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MensTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ConDetDescuentoCombo1DataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents ClaveTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_DescuentoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_TipSerTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_ServicioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel4 As System.Windows.Forms.Label
End Class
