﻿Public Class TarifasEco
    Dim Meses As Integer
    Dim ImporteMin As Double

    Private Sub TarifasEco_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Me.Label1.ForeColor = Color.Black
        Me.Label2.ForeColor = Color.Black
        Me.Label3.ForeColor = Color.Black

        DameIndicadoresEco()

        NUDMeses.Value = Meses
        TBoxImpMin.Text = ImporteMin
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        If NUDMeses.Value > 0 And IsNumeric(TBoxImpMin.Text) And TBoxImpMin.Text <> "" Then
            ActualizaIndicadores(NUDMeses.Value, TBoxImpMin.Text)
            MsgBox("Datos Actualizados Correctamente", MsgBoxStyle.Information)
            'NUDMeses.Value = 0
            'TBoxImpMin.Text = ""
        Else
            MsgBox("Datos Incorrectos", MsgBoxStyle.Exclamation)
        End If

    End Sub

    Private Function ActualizaIndicadores(ByVal Meses As Integer, ByVal ImporteMin As Integer) As Boolean
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Meses", SqlDbType.Int, Meses)
        BaseII.CreateMyParameter("@ImporteMin", SqlDbType.Int, ImporteMin)
        BaseII.ProcedimientoOutPut("ActualizaIndicadoresEco")
    End Function

    Private Sub DameIndicadoresEco()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Meses", ParameterDirection.Output, SqlDbType.Money)
        BaseII.CreateMyParameter("@ImporteMin", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("DameIndicadoresEco")
        Meses = CInt(BaseII.dicoPar("@Meses").ToString)
        ImporteMin = CDbl(BaseII.dicoPar("@ImporteMin").ToString)
    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

End Class