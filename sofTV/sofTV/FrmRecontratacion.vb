﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Reflection
Imports System.Data.SqlClient
Imports System.Net
Imports System.Net.Sockets
Imports System.IO
Imports System
Imports System.IO.StreamReader
Imports System.IO.File
Imports System.Text
Imports System.Collections
Imports System.Collections.Generic

Public Class FrmRecontratacion

    Dim Res As Integer = 0
    Dim Msj As String = String.Empty
    Dim eContratoNet As Long = 0
    Dim eClv_Cablemodem As Integer = 0
    Dim eClv_Servicio As Integer = 0
    Dim Op As Boolean = False
    Dim aparatoNet As Boolean = False
    Dim aparatoDig As Boolean = False
    Dim TieneAdeudo As Integer = 0
    Dim BndEco As Integer = 0
    Private LocNomImpresora_Contratos As String = Nothing
    Private LocNomImpresora_Tarjetas As String = Nothing
    Private clv_empresa As String = Nothing


    Private Sub MuestraInfoCliente(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("MuestraInfoCliente", conexion)
        comando.CommandType = CommandType.StoredProcedure
        Dim reader As SqlDataReader

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            reader = comando.ExecuteReader
            While (reader.Read())
                Me.LabelNombre.Text = reader(0).ToString()
                Me.LabelCalle.Text = reader(1).ToString()
                Me.LabelNumero.Text = reader(2).ToString()
                Me.LabelColonia.Text = reader(3).ToString()
                Me.LabelCiudad.Text = reader(4).ToString()
                Me.LabelTelefono.Text = reader(5).ToString()
                Me.LabelCelular.Text = reader(6).ToString()
                Me.CheckBoxSoloInternet.Checked = reader(7).ToString
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub Limpiar()

        Me.LabelNombre.Text = ""
        Me.LabelCalle.Text = ""
        Me.LabelNumero.Text = ""
        Me.LabelColonia.Text = ""
        Me.LabelCiudad.Text = ""
        Me.LabelTelefono.Text = ""
        Me.LabelCelular.Text = ""
        Me.CheckBoxSoloInternet.Checked = False
        tvTV.Nodes.Clear()
        tvNET.Nodes.Clear()
        tvDIG.Nodes.Clear()
        lblTV.Text = ""
        lblNET.Text = ""
        lblDIG.Text = ""
        lblDIGCable.Text = ""
        eContratoNet = 0
        eClv_Servicio = 0
        tcServicios.Enabled = False
        BorReconSession(eClv_Session)

    End Sub

    Private Sub ButtonBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBuscar.Click
        GLOCONTRATOSEL = 0
        GloClv_TipSer = 2309
        FrmSelCliente.Show()
    End Sub

    Private Sub TextBoxContrato_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBoxContrato.KeyDown
        'If (e.KeyCode <> Keys.Enter) Then
        '    Exit Sub
        'End If
        'If IsNumeric(Me.TextBoxContrato.Text) = False Then
        '    Exit Sub
        'End If
        'If CInt(Me.TextBoxContrato.Text) <= 0 Then
        '    Exit Sub
        'End If

        'MuestraInfoCliente(TextBoxContrato.Text)
        'eContrato = TextBoxContrato.Text
        'tcServicios.Enabled = True


        ''If CheckBoxSoloInternet.Checked = True Then
        ''    If tcServicios.TabPages.Count > 1 Then
        ''        tcServicios.TabPages.Remove(tpDIG)
        ''        eClv_TipSer = 2
        ''    End If
        ''Else
        ''    If tcServicios.TabPages.Count = 1 Then
        ''        tcServicios.TabPages.Remove(tpNET)
        ''        tcServicios.TabPages.Add(tpDIG)
        ''        tcServicios.TabPages.Add(tpNET)
        ''        eClv_TipSer = 3
        ''    End If
        ''End If

        'ValidaServ(eContrato, eClv_TipSer)

        'If Res = 1 Then
        '    Exit Sub
        'End If



        'DameClv_Session()
        'NueDetReconNetTmp(0, eClv_Session, eContrato, String.Empty, 0, String.Empty, String.Empty, 0, 0)
        'NueDetReconDigTmp(0, eClv_Session, eContrato, String.Empty, 0, 0)
        'NueDetReconAntTmp(eClv_Session, eContrato)
        'LlenatvNet()
        'LlenatvDig()


        'aparatoDig = False
        'If tvDIG.Nodes.Count > 0 Then aparatoDig = True

        'aparatoNet = False
        'If tvNET.Nodes.Count > 0 Then aparatoNet = True

    End Sub

    Private Sub TextBoxContrato_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBoxContrato.KeyPress
        e.KeyChar = Chr(ValidaKey(TextBoxContrato, Asc(LCase(e.KeyChar)), "N"))
        If Asc(e.KeyChar) = 13 Then

            MuestraInfoCliente(TextBoxContrato.Text)
            eContrato = TextBoxContrato.Text
            tcServicios.Enabled = True


            'If CheckBoxSoloInternet.Checked = True Then
            '    If tcServicios.TabPages.Count > 1 Then
            '        tcServicios.TabPages.Remove(tpDIG)
            '        eClv_TipSer = 2
            '    End If
            'Else
            '    If tcServicios.TabPages.Count = 1 Then
            '        tcServicios.TabPages.Remove(tpNET)
            '        tcServicios.TabPages.Add(tpDIG)
            '        tcServicios.TabPages.Add(tpNET)
            '        eClv_TipSer = 3
            '    End If
            'End If

            ValidaServ(eContrato, eClv_TipSer)


            If Res = 1 Then
                Exit Sub
            End If

            uspDimeAdeudoBaja()

            If TieneAdeudo >= 1 Then
                Dim Resultado = MessageBox.Show("Desea Pasar a EconoPack", "EconoPack", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                If Resultado = DialogResult.Yes Then
                    BndEco = True
                    MsgBox("El cliente Pasara a Econopack", MsgBoxStyle.Information, "EconoPack")
                ElseIf Resultado = DialogResult.No Then
                    BndEco = False
                    MsgBox("El cliente Rechazo el servicio Econopack", MsgBoxStyle.Information, "EconoPack")
                End If

            ElseIf TieneAdeudo = 0 Then
                BndEco = False

            End If

            
            DameClv_Session()
            NueDetReconNetTmp(0, eClv_Session, eContrato, String.Empty, 0, String.Empty, String.Empty, 0, 0)
            NueDetReconDigTmp(0, eClv_Session, eContrato, String.Empty, 0, eClv_TipSer, 0)
            NueDetReconAntTmp(eClv_Session, eContrato)
            LlenatvNet()
            LlenatvDig()
            LlenatvDigCable()


            aparatoDig = False
            If tvDIG.Nodes.Count > 0 Then aparatoDig = True

            aparatoNet = False
            If tvNET.Nodes.Count > 0 Then aparatoNet = True


        End If
    End Sub

    Private Sub TextBoxContrato_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBoxContrato.TextChanged
        Limpiar()
    End Sub

    Private Sub ValidaServ(ByVal Contrato As Long, ByVal Clv_TipSer As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaServReCon", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_TipSer
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Res", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        parametro4.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            Res = 0
            Msj = String.Empty
            Res = parametro3.Value
            Msj = parametro4.Value

            If Res > 0 Then
                Op = False
                If eClv_TipSer = 2 Then lblNET.Text = parametro4.Value.ToString
                If eClv_TipSer = 3 Then lblDIG.Text = parametro4.Value.ToString
                If eClv_TipSer = 6 Then lblDIGCable.Text = parametro4.Value.ToString
            Else
                Op = True

                If eClv_TipSer = 2 Then lblNET.Text = ""
                If eClv_TipSer = 3 Then lblDIG.Text = ""
                If eClv_TipSer = 6 Then lblDIG.Text = ""
            End If



            BloquearBN(Clv_TipSer, Op)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try


    End Sub

    Private Sub BloquearBN(ByVal Clv_TipSer As Integer, ByVal Op As Boolean)
        If Clv_TipSer = 2 Then
            bnNETCablemodem.Enabled = Op
            bnNETServicio.Enabled = Op
        ElseIf Clv_TipSer = 3 Then
            bnDIGTarjeta.Enabled = Op
            bnDIGServicio.Enabled = Op
        ElseIf Clv_TipSer = 6 Then
            bnDIGCableTarjeta.Enabled = Op
            bnDIGCableServicio.Enabled = Op
        End If
    End Sub

    Private Function ConDetReconTVTmp(ByVal ClvSession As Long) As DataTable
        Try
            Dim conexion As New SqlConnection(MiConexion)
            Dim strSQL As New StringBuilder

            strSQL.Append("EXEC ConDetReconTVTmp " & CStr(ClvSession))

            Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
            Dim dataTable As New DataTable

            dataAdapter.Fill(dataTable)

            Return dataTable

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Function

    Private Function ConDetReconNetTmp(ByVal Op As Integer, ByVal ClvSession As Long, ByVal ContratoNet As Long) As DataTable
        Try
            Dim conexion As New SqlConnection(MiConexion)
            Dim strSQL As New StringBuilder
            strSQL.Append("EXEC ConDetReconNetTmp " & CStr(Op) & ", " & CStr(ClvSession) & ", " & CStr(ContratoNet))

            Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
            Dim dataTable As New DataTable

            dataAdapter.Fill(dataTable)

            Return dataTable

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Function

    Private Function ConDetReconDigTmp(ByVal Op As Integer, ByVal ClvSession As Long, ByVal ContratoNet As Long) As DataTable
        Try
            Dim conexion As New SqlConnection(MiConexion)
            Dim strSQL As New StringBuilder
            strSQL.Append("EXEC ConDetReconDigTmp " & CStr(Op) & ", " & CStr(ClvSession) & ", " & CStr(ContratoNet))

            Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
            Dim dataTable As New DataTable

            dataAdapter.Fill(dataTable)

            Return dataTable

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Function

    Private Function ConDetReconDigCableTmp(ByVal Op As Integer, ByVal ClvSession As Long, ByVal ContratoNet As Long) As DataTable
        Try
            Dim conexion As New SqlConnection(MiConexion)
            Dim strSQL As New StringBuilder
            strSQL.Append("EXEC ConDetReconDigCableTmp " & CStr(Op) & ", " & CStr(ClvSession) & ", " & CStr(ContratoNet))

            Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
            Dim dataTable As New DataTable

            dataAdapter.Fill(dataTable)

            Return dataTable

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Function

    Private Sub NueDetReconTVTmp(ByVal ClvSession As Long, ByVal Contrato As Long, ByVal TVSinPago As Integer, ByVal TVConPago As Integer, ByVal Clv_Servicio As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueDetReconTVTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@ClvSession", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = ClvSession
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Contrato
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@TVSinPago", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = TVSinPago
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@TVConPago", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = TVConPago
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = Clv_Servicio
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@Res", SqlDbType.Int)
        parametro6.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro6)

        Dim parametro7 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        parametro7.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro7)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            Res = 0
            Msj = String.Empty
            Res = parametro6.Value
            Msj = parametro7.Value
            If Res = 1 Then
                MsgBox(Msj, MsgBoxStyle.Exclamation)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub NueDetReconNetTmp(ByVal Op As Integer, ByVal ClvSession As Long, ByVal Contrato As Long, ByVal Agregar As Char, ByVal ContratoNet As Long, ByVal TipoCablemodem As Char, ByVal TipoAdquisicion As Char, ByVal TipoServicio As Integer, ByVal Clv_Servicio As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueDetReconNetTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@Op", SqlDbType.Int)
        par.Direction = ParameterDirection.Input
        par.Value = Op
        comando.Parameters.Add(par)

        Dim parametro As New SqlParameter("@ClvSession", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = ClvSession
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Contrato
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Agregar", SqlDbType.VarChar, 1)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Agregar
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@ContratoNet", SqlDbType.BigInt)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = ContratoNet
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@TipoCablemodem", SqlDbType.VarChar, 1)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = TipoCablemodem
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@TipoAdquisicion", SqlDbType.VarChar, 1)
        parametro6.Direction = ParameterDirection.Input
        parametro6.Value = TipoAdquisicion
        comando.Parameters.Add(parametro6)

        Dim parametro7 As New SqlParameter("@TipoServicio", SqlDbType.VarChar, 1)
        parametro7.Direction = ParameterDirection.Input
        parametro7.Value = TipoServicio
        comando.Parameters.Add(parametro7)

        Dim parametro8 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        parametro8.Direction = ParameterDirection.Input
        parametro8.Value = Clv_Servicio
        comando.Parameters.Add(parametro8)

        Dim parametro9 As New SqlParameter("@Res", SqlDbType.Int)
        parametro9.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro9)

        Dim parametro10 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        parametro10.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro10)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            Res = 0
            Msj = String.Empty
            Res = parametro9.Value
            Msj = parametro10.Value
            If Res = 1 Then
                MsgBox(Msj, MsgBoxStyle.Exclamation)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub NUEDetReconDigTipSerTmp(ByVal ClvSession As Integer, ByVal ContratoNet As Integer, ByVal Clv_TipSer As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NUEDetReconDigTipSerTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@ClvSession", SqlDbType.Int)
        par.Direction = ParameterDirection.Input
        par.Value = ClvSession
        comando.Parameters.Add(par)

        Dim parametro As New SqlParameter("@ContratoNet", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = ContratoNet
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_TipSer", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_TipSer
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub NueDetReconDigTmp(ByVal op As Integer, ByVal ClvSession As Long, ByVal Contrato As Long, ByVal Agregar As Char, ByVal ContratoNet As Long, ByVal Clv_TipSer As Integer, ByVal Clv_Servicio As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueDetReconDigTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@Op", SqlDbType.Int)
        par.Direction = ParameterDirection.Input
        par.Value = op
        comando.Parameters.Add(par)

        Dim parametro As New SqlParameter("@ClvSession", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = ClvSession
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Contrato
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Agregar", SqlDbType.VarChar, 1)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Agregar
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@ContratoNet", SqlDbType.BigInt)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = ContratoNet
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = Clv_TipSer
        comando.Parameters.Add(parametro5)

        Dim parametro8 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        parametro8.Direction = ParameterDirection.Input
        parametro8.Value = Clv_Servicio
        comando.Parameters.Add(parametro8)

        Dim parametro9 As New SqlParameter("@Res", SqlDbType.Int)
        parametro9.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro9)

        Dim parametro10 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        parametro10.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro10)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            Res = 0
            Msj = String.Empty
            Res = parametro9.Value
            Msj = parametro10.Value
            If Res = 1 Then
                MsgBox(Msj, MsgBoxStyle.Exclamation)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub NueDetReconAntTmp(ByVal ClvSession As Long, ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueDetReconAntTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0


        Dim parametro As New SqlParameter("@ClvSession", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = ClvSession
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Contrato
        comando.Parameters.Add(parametro2)



        Try
            conexion.Open()
            comando.ExecuteNonQuery()


        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub BorDetReconTVTmp(ByVal ClvSession As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorDetReconTVTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@ClvSession", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = ClvSession
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub BorDetReconNetTmp(ByVal ClvSession As Long, ByVal Eliminar As Char, ByVal ContratoNet As Long, ByVal Clv_Servicio As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorDetReconNetTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@ClvSession", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = ClvSession
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Eliminar", SqlDbType.VarChar, 1)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Eliminar
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@ContratoNet", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = ContratoNet
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = Clv_Servicio
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Res", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        parametro6.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro6)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            Res = 0
            Msj = String.Empty
            Res = parametro5.Value
            Msj = parametro6.Value
            If Res = 1 Then
                MsgBox(Msj, MsgBoxStyle.Exclamation)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try


    End Sub

    Private Sub BorDetReconDigTmp(ByVal ClvSession As Long, ByVal Eliminar As Char, ByVal ContratoNet As Long, ByVal Clv_Servicio As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorDetReconDigTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@ClvSession", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = ClvSession
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Eliminar", SqlDbType.VarChar, 1)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Eliminar
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@ContratoNet", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = ContratoNet
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = Clv_Servicio
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Res", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        parametro6.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro6)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            Res = 0
            Msj = String.Empty
            Res = parametro5.Value
            Msj = parametro6.Value
            If Res = 1 Then
                MsgBox(Msj, MsgBoxStyle.Exclamation)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try


    End Sub

    Private Sub ValidaServReCon(ByVal Contrato As Long, ByVal Clv_TipSer As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaServReCon", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_TipSer
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Res", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        parametro4.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            Res = 0
            Msj = String.Empty
            Res = parametro3.Value
            Msj = parametro4.Value
            If Res = 1 Then
                MsgBox(Msj, MsgBoxStyle.Exclamation)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Function MuestraTipoCablemodem() As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraTipoCablemodem")

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable

        dataAdapter.Fill(dataTable)

        Return dataTable

    End Function

    Private Function MuestraTipoAsignacion() As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraTipoAsignacion")

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable

        dataAdapter.Fill(dataTable)

        Return dataTable

    End Function

    Private Function MuestraTipSerInternet() As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraTipSerInternet")

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable

        dataAdapter.Fill(dataTable)

        Return dataTable
    End Function

    Private Function MuestraServicioRecon(ByVal ClvSession As Long, ByVal Clv_TipSer As Integer, ByVal ContratoNet As Long) As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraServicioRecon " & CStr(ClvSession) & ", " & CStr(Clv_TipSer) & ", " & CStr(ContratoNet))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable

        dataAdapter.Fill(dataTable)

        Return dataTable

    End Function

    Private Sub LlenatvNet()
        Try
            Dim dtA As New DataTable
            Dim dtS As New DataTable
            Dim a As Integer = 0
            Dim s As Integer = 0

            dtA = ConDetReconNetTmp(0, eClv_Session, 0)

            tvNET.Nodes.Clear()

            For Each aparato As DataRow In dtA.Rows

                tvNET.Nodes.Add(aparato("Aparato").ToString())
                tvNET.Nodes(a).Tag = aparato("ContratoNet").ToString()
                dtS = ConDetReconNetTmp(1, eClv_Session, aparato("ContratoNet").ToString())

                For Each servicio As DataRow In dtS.Rows
                    tvNET.Nodes(a).Nodes.Add(servicio("Servicio").ToString()).ForeColor = Color.Blue
                    tvNET.Nodes(a).Nodes(s).Tag = servicio("Clv_Servicio").ToString()
                    s += 1

                Next

                tvNET.Nodes(a).ExpandAll()
                a += 1
                s = 0
            Next



        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub LlenatvDig()
        Try
            Dim dtA As New DataTable
            Dim dtS As New DataTable
            Dim a As Integer = 0
            Dim s As Integer = 0

            dtA = ConDetReconDigTmp(0, eClv_Session, 0)

            tvDIG.Nodes.Clear()

            For Each aparato As DataRow In dtA.Rows
                tvDIG.Nodes.Add(aparato("Aparato").ToString())
                tvDIG.Nodes(a).Tag = aparato("ContratoNet").ToString()
                dtS = ConDetReconDigTmp(1, eClv_Session, aparato("ContratoNet"))
                For Each servicio As DataRow In dtS.Rows
                    tvDIG.Nodes(a).Nodes.Add(servicio("Servicio").ToString()).ForeColor = Color.Blue
                    tvDIG.Nodes(a).Nodes(s).Tag = servicio("Clv_Servicio").ToString()
                    s += 1
                Next

                tvDIG.Nodes(a).ExpandAll()

                a += 1
                s = 0

            Next


        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub LlenatvDigCable()
        Try
            Dim dtA As New DataTable
            Dim dtS As New DataTable
            Dim a As Integer = 0
            Dim s As Integer = 0

            dtA = ConDetReconDigCableTmp(0, eClv_Session, 0)

            tvDIGCable.Nodes.Clear()

            For Each aparato As DataRow In dtA.Rows
                tvDIGCable.Nodes.Add(aparato("Aparato").ToString())
                tvDIGCable.Nodes(a).Tag = aparato("ContratoNet").ToString()
                dtS = ConDetReconDigCableTmp(1, eClv_Session, aparato("ContratoNet"))
                For Each servicio As DataRow In dtS.Rows
                    tvDIGCable.Nodes(a).Nodes.Add(servicio("Servicio").ToString()).ForeColor = Color.Blue
                    tvDIGCable.Nodes(a).Nodes(s).Tag = servicio("Clv_Servicio").ToString()
                    s += 1
                Next

                tvDIGCable.Nodes(a).ExpandAll()

                a += 1
                s = 0

            Next


        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub GrabaReContratacion(ByVal Contrato As Long, ByVal Clv_Usuario As String, ByVal ClvSession As Long, ByVal SoloInternet As Boolean)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("GrabaReContratacion", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar, 5)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_Usuario
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@ClvSession", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = ClvSession
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@SoloInternet", SqlDbType.Bit)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = SoloInternet
        comando.Parameters.Add(parametro4)


        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            MsgBox(mensaje5, MsgBoxStyle.Information)
            Impresion_Contrato()
            Limpiar()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub BorReconSession(ByVal ClvSession As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorReconSession", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@ClvSession", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = ClvSession
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub DameClv_Session()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("DameClv_Session", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eClv_Session = parametro.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub FrmRecontratacion_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GLOCONTRATOSEL > 0 Then
            Me.TextBoxContrato.Text = GLOCONTRATOSEL
            GLOCONTRATOSEL = 0
            eContrato = Me.TextBoxContrato.Text

            MuestraInfoCliente(TextBoxContrato.Text)
            eContrato = TextBoxContrato.Text
            DameClv_Session()
            NueDetReconNetTmp(0, eClv_Session, eContrato, String.Empty, 0, String.Empty, String.Empty, 0, 0)
            NueDetReconDigTmp(0, eClv_Session, eContrato, String.Empty, 0, eClv_TipSer, 0)
            NueDetReconAntTmp(eClv_Session, eContrato)
            LlenatvNet()
            LlenatvDig()
            LlenatvDigCable()
            tcServicios.Enabled = True

        End If
    End Sub

    Private Sub FrmRecontratacion_DragEnter(sender As Object, e As DragEventArgs) Handles Me.DragEnter

    End Sub

    Private Sub FrmRecontratacion_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' colorea(Me, Me.Name)
        'Me.CMBGroupBox1.BackColor = Color.Gray
 
        eClv_Session = 0
        eClv_TipSer = 2
        eContrato = 0
        GLOCONTRATOSEL = 0
        comboTipoCa.DataSource = MuestraTipoCablemodem()
        comboTipoAs.DataSource = MuestraTipoAsignacion()
        comboTipoSer.DataSource = MuestraTipSerInternet()
        comboSerNET.DataSource = MuestraServicioRecon(eClv_Session, 2, 0)
        'comboSerDIG.DataSource = MuestraServicioRecon(eClv_Session, 3, eContratoNet)
        'comboSerDIGCable.DataSource = MuestraServicioRecon(eClv_Session, 6, 0)
        tcServicios.TabPages.Remove(tpTV)
        tcServicios.TabPages.Remove(tpDIG)
        tcServicios.TabPages.Remove(tpDIGCable)
        SeleccioneImpresora()
        ImpresoraContatos = LocNomImpresora_Contratos
    End Sub

    Public Sub SeleccioneImpresora()
        Dim CON80 As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand()
        Try
            CMD = New SqlCommand()
            CON80.Open()
            With CMD
                .CommandText = "Selecciona_Impresora_Sucursal"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 0

                Dim prm As New SqlParameter("@clv_sucursal", SqlDbType.Int)
                prm.Direction = ParameterDirection.Input
                prm.Value = GloClv_Sucursal
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@Impresora_Tarjetas", SqlDbType.VarChar, 50)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = ""
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@Impresora_Contratos", SqlDbType.VarChar, 50)
                prm2.Direction = ParameterDirection.Output
                prm2.Value = ""
                .Parameters.Add(prm2)

                Dim i As Integer = .ExecuteNonQuery()
                LocNomImpresora_Tarjetas = prm1.Value
                LocNomImpresora_Contratos = prm2.Value
            End With

        Catch ex As Exception

        Finally
            CON80.Close()
        End Try
    End Sub

    Private Sub tcServicios_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tcServicios.SelectedIndexChanged
        If tcServicios.TabPages.Count = 0 Then
            Exit Sub
        End If
        If tcServicios.SelectedTab.Name = "tpTV" Then
            eClv_TipSer = 1
        ElseIf tcServicios.SelectedTab.Name = "tpNET" Then
            eClv_TipSer = 2
        ElseIf tcServicios.SelectedTab.Name = "tpDIG" Then
            eClv_TipSer = 3
        ElseIf tcServicios.SelectedTab.Name = "tpDIGCable" Then
            eClv_TipSer = 6
            comboSerDIGCable.DataSource = MuestraServicioRecon(eClv_Session, eClv_TipSer, 0) 'LLENAMOS EL COMBO DE LOS SERVICIOS DE TIPO CABLE (CLVTIPSER=6)
            If IsNumeric(Me.comboSerDIGCable.SelectedValue) = True Then
                ConsultaAparatosContratacion(CInt(Me.comboSerDIGCable.SelectedValue), eClv_TipSer) 'LLENAMOS EL COMBO DE LOS TIPOS DE APARATOS PARA CABLE (CLVTIPSER=6)
            End If
        End If

        eContratoNet = 0
        eClv_Servicio = 0

        If eContrato > 0 Then ValidaServ(eContrato, eClv_TipSer)
    End Sub

    Private Sub tsbAgregarNET_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbAgregarNET.Click
        panelCNET.Visible = True
        tsbEliminarNET.Enabled = False
    End Sub

    Private Sub tsbEliminarNET_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbEliminarNET.Click
        If eContratoNet = 0 Then
            MsgBox("Selecciona un Cablemodem.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        BorDetReconNetTmp(eClv_Session, "A", eContratoNet, 0)
        LlenatvNet()
        eContratoNet = 0
    End Sub

    Private Sub btnCNETAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCNETAceptar.Click
        If comboTipoCa.SelectedIndex <= -1 Then
            MsgBox("Selecciona un Tipo de Cablemodem.", MsgBoxStyle.Information)
            Exit Sub
        End If
        If comboTipoAs.SelectedIndex <= -1 Then
            MsgBox("Selecciona un Tipo de Asignación.", MsgBoxStyle.Information)
            Exit Sub
        End If
        If comboTipoSer.SelectedValue <= -1 Then
            MsgBox("Selecciona un Tipo de Servicio.", MsgBoxStyle.Information)
            Exit Sub
        End If

        NueDetReconNetTmp(1, eClv_Session, eContrato, "A", 0, comboTipoCa.SelectedValue, comboTipoAs.SelectedValue, comboTipoSer.SelectedValue, 0)
        LlenatvNet()
        panelCNET.Visible = False
        tsbEliminarNET.Enabled = True

    End Sub

    Private Sub btnCNETCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCNETCancelar.Click
        panelCNET.Visible = False
        tsbEliminarNET.Enabled = True
    End Sub

    Private Sub tsbAgregarSNET_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbAgregarSNET.Click
        If eContratoNet = 0 Then
            MsgBox("Selecciona en que Cablemodem se agregará el Servicio.", MsgBoxStyle.Information)
            Exit Sub
        End If
        panelSNET.Visible = True
        tsbEliminarSNET.Enabled = False
        comboSerNET.DataSource = MuestraServicioRecon(eClv_Session, 2, 0)
    End Sub

    Private Sub tsbEliminarSNET_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbEliminarSNET.Click
        If eClv_Servicio = 0 Then
            MsgBox("Selecciona un Servicio.", MsgBoxStyle.Information)
            Exit Sub
        End If
        BorDetReconNetTmp(eClv_Session, "S", eContratoNet, eClv_Servicio)
        eClv_Servicio = 0
        LlenatvNet()
    End Sub

    Private Sub btnSNETAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSNETAceptar.Click
        If comboSerNET.SelectedIndex <= -1 Then
            MsgBox("Selecciona un Servicio.", MsgBoxStyle.Information)
            Exit Sub
        End If

        NueDetReconNetTmp(1, eClv_Session, 0, "S", eContratoNet, String.Empty, String.Empty, 0, comboSerNET.SelectedValue)
        LlenatvNet()

        eContratoNet = 0
        panelSNET.Visible = False
        tsbEliminarSNET.Enabled = True
    End Sub

    Private Sub btnSNETCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSNETCancelar.Click
        eContratoNet = 0
        panelSNET.Visible = False
        tsbEliminarSNET.Enabled = True
    End Sub

    Private Sub tsbAgregarTDIG_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbAgregarTDIG.Click
        VALIDADetReconDigTipSerTmp(eClv_Session, eClv_TipSer)
        If (eMsj.Length > 0) Then
            MessageBox.Show(eMsj)
            Exit Sub
        End If
        NueDetReconDigTmp(1, eClv_Session, eContrato, "A", 0, eClv_TipSer, 0)
        NueDetReconAntTmp(eClv_Session, eContrato)
        LlenatvDig()
        comboSerDIG.DataSource = MuestraServicioRecon(eClv_Session, eClv_TipSer, eContratoNet) 'LLENAMOS EL COMBO DE LOS SERVICIOS DE TIPO DTH (CLVTIPSER=3)
        If IsNumeric(Me.comboSerDIG.SelectedValue) = True Then
            ConsultaAparatosContratacion(CInt(Me.comboSerDIG.SelectedValue), eClv_TipSer) 'LLENAMOS EL COMBO DE LOS TIPOS DE APARATOS PARA DTH (CLVTIPSER=3)
        End If
    End Sub

    Private Sub tsbEliminarTDIG_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbEliminarTDIG.Click
        If eContratoNet = 0 Then
            MsgBox("Selecciona una Tarjeta.", MsgBoxStyle.Information)
            Exit Sub
        End If
        BorDetReconDigTmp(eClv_Session, "A", eContratoNet, 0)
        LlenatvDig()
        BorraRelClientesApartos(eContrato, eContratoNet) 'ELIMINAMOS LA RELACIÓN DEL TIPO DEL APARATO CON EL SERVICIO ()

        eContratoNet = 0

    End Sub

    Private Sub tsbAgregarSDIG_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbAgregarSDIG.Click
        If eContratoNet = 0 Then
            MsgBox("Selecciona una Tarjeta a la que se le agregará el Servicio.", MsgBoxStyle.Information)
            Exit Sub
        End If
        panelDIG.Visible = True
        tsbEliminarTDIG.Enabled = False
        comboSerDIG.DataSource = MuestraServicioRecon(eClv_Session, 3, eContratoNet)
        If Me.cmbTipoAparatoDth.Items.Count = 0 Then
            If IsNumeric(Me.comboSerDIG.SelectedValue) = True Then
                ConsultaAparatosContratacion(CInt(Me.comboSerDIG.SelectedValue), eClv_TipSer) 'LLENAMOS EL COMBO DE LOS TIPOS DE APARATOS PARA DTH (CLVTIPSER=3)
            End If
        End If

        If BndEco = True Then
            Me.comboSerDIG.SelectedIndex = Me.comboSerDIG.FindStringExact("Econo-pack")
            Me.comboSerDIG.Enabled = False
        End If

    End Sub

    Private Sub tsbEliminarSDIG_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbEliminarSDIG.Click
        If eClv_Servicio = 0 Then
            MsgBox("Seleccion un Servicio.", MsgBoxStyle.Information)
            Exit Sub
        End If

        BorDetReconDigTmp(eClv_Session, "S", eContratoNet, eClv_Servicio)
        LlenatvDig()
        eClv_Servicio = 0


    End Sub

    Private Sub btnDIGAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDIGAceptar.Click
        If comboSerDIG.SelectedIndex <= -1 Then
            MsgBox("Selecciona un Servicio.", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.cmbTipoAparatoDth.SelectedIndex <= -1 Then
            MsgBox("¡Selecciona una Marca de Aparato para continuar!", MsgBoxStyle.Information)
            Exit Sub
        End If

        NueDetReconDigTmp(1, eClv_Session, eContrato, "S", eContratoNet, eClv_TipSer, comboSerDIG.SelectedValue)
        LlenatvDig()
        GuardaRelClientesAparatos(eContrato, eContratoNet, CInt(Me.comboSerDIG.SelectedValue), CInt(Me.cmbTipoAparatoDth.SelectedValue)) 'GUARDA LA RELACIÓN DE LA MARCA DEL APARATO A INSTALAR CON EL SERVICIO

        panelDIG.Visible = False
        tsbEliminarTDIG.Enabled = True
    End Sub

    Private Sub btnDIGCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDIGCancelar.Click
        panelDIG.Visible = False
        tsbEliminarTDIG.Enabled = True
    End Sub

    Private Sub tvNET_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvNET.AfterSelect
        If IsNumeric(e.Node.Tag) = False Then
            Exit Sub
        End If
        If e.Node.Level = 0 Then
            eContratoNet = CLng(e.Node.Tag)
        Else
            eContratoNet = CLng(e.Node.Parent.Tag)
            eClv_Servicio = CInt(e.Node.Tag)
        End If
    End Sub

    Private Sub tvDIG_AfterSelect(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvDIG.AfterSelect
        If IsNumeric(e.Node.Tag) = False Then
            Exit Sub
        End If
        If e.Node.Level = 0 Then
            eContratoNet = CLng(e.Node.Tag)
        Else
            eContratoNet = CLng(e.Node.Parent.Tag)
            eClv_Servicio = CInt(e.Node.Tag)
        End If
    End Sub


    Private Sub tsbRecon_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbRecon.Click

        ChecaAntesGrabaRecontratacion(eClv_Session, CheckBoxSoloInternet.Checked, aparatoNet, aparatoDig)

        If eRes = 1 Then
            MsgBox(eMsj, MsgBoxStyle.Information)
            Exit Sub
        End If

        If eRes = 2 Then
            Dim r As String
            r = MsgBox(eMsj, MsgBoxStyle.YesNo).ToString
            If r = "Yes" Then
                GrabaReContratacion(eContrato, GloUsuario, eClv_Session, True)
                Exit Sub
            Else
                MsgBox("Agrega Servicios Digitales", MsgBoxStyle.Information)
                Exit Sub
            End If
        End If

        GrabaReContratacion(eContrato, GloUsuario, eClv_Session, CheckBoxSoloInternet.Checked)
        Me.Close()
        Me.Dispose()


    End Sub
    Public Sub DameClvEmpresa()
        Dim CON80 As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand()
        Try
            CMD = New SqlCommand()
            CON80.Open()
            With CMD
                .CommandText = "Dame_clv_Empresa"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 0


                Dim prm1 As New SqlParameter("@clv_empresa", SqlDbType.VarChar, 2)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = ""
                .Parameters.Add(prm1)

                Dim i As Integer = .ExecuteNonQuery()
                clv_empresa = prm1.Value

            End With

        Catch ex As Exception

        Finally
            CON80.Close()
        End Try
    End Sub

    Public Sub ChecaServicio()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ChecaServicioContratado", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Me.TextBoxContrato.Text
        comando.Parameters.Add(parametro)

        
        Dim parametro3 As New SqlParameter("@Error", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Output
        parametro3.Value = 0
        comando.Parameters.Add(parametro3)


        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            
            eErrorContrato = parametro3.Value

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub Impresion_Contrato()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim valida As Integer = 0
        Dim valida2 As Integer = 0
        Dim yes As Integer = 0
        Dim tv As Integer = 0, Int As Integer = 0, Dig As Integer = 0
        If IsNumeric(Me.TextBoxContrato.Text) = True Then
            eContrato = Me.TextBoxContrato.Text
            DameClvEmpresa()

            If LocNomImpresora_Contratos = "" Then
                MsgBox("No se ha asignado una Impresora de Contratos a esta Sucursal", MsgBoxStyle.Information)
            Else
                If clv_empresa = "AG" Or clv_empresa = "SA" Or clv_empresa = "VA" Then
                    ChecaServicio()
                    If eErrorContrato = 0 Then
                        If clv_empresa = "AG" Then
                            'yes = MsgBox("Presioné en aceptar cuando este lista  Impresora para Imprimir el Contrato ", MsgBoxStyle.YesNo)
                            'If yes = 6 Then
                            ConfigureCrystalReportsContrato(eContrato)
                            '    yes = MsgBox("Presioné en aceptar cuando este lista  Impresora para Imprimir la Copia del Contrato  ", MsgBoxStyle.YesNo)
                            '    If yes = 6 Then
                            '        ConfigureCrystalReportsContrato(eContrato)
                            '    End If
                            'End If

                        End If
                        'If valida2 = 0 Then
                        '    MsgBox("Se Imprimio con Éxito", MsgBoxStyle.Information)
                        'End If
                        'FrmImprimirContrato.Show()
                    Else
                        MsgBox("El Cliente no cuenta con Servicios Asignados", , "Advertencia")
                        Exit Sub
                    End If

                End If
            End If
        Else
            MsgBox("No Se Ha Seleccionado Un Contrato ", MsgBoxStyle.Information)
        End If
        CON.Close()
    End Sub
    Private Sub ConfigureCrystalReportsContrato(ByVal Clv_Factura As Long)
        'Mandamos Capturar los datos complementarios del Cliente (pagaré)
        FrmDatosComplementariosContrato.ContratoAImprimir = Me.TextBoxContrato.Text
        FrmDatosComplementariosContrato.ShowDialog()

        If eErrorContrato = 1 Then
            eErrorContrato = 0
            Exit Sub
        End If

        'Mandamos llamar todos los datos del Cliente
        Dim ds As New DataSet
        Try
            ds = DatosGeneralesImpresionDeContrato(Me.TextBoxContrato.Text)


            'Asignamos el Reporte al Objeto Global
            rDocument = New ReportDocument()

            'Ruta del Reporte
            rDocument.Load(RutaReportes.ToString & "\rptReportContrato.rpt")

            ds.Tables(0).TableName = "ReportContrato"
            ds.Tables(1).TableName = "tbl_Pagare"

            'Origen de Datos
            rDocument.SetDataSource(ds)

            'Mandamos el reporte a Impresión
            'FrmVisorDeReportes.Show()

            'A PDF
            'rDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, rutaArchivos.ToString & "\" & Contrato & "_" & PeriodoDeCobro.ToString & ".pdf")
            'rDocument.Dispose()

            'A impresora Directo
            rDocument.PrintOptions.PrinterName = LocNomImpresora_Contratos
            Dim yes As Integer = 0
            yes = MsgBox("Presione en Sí cuando este lista la  Impresora para Imprimir el Contrato ", MsgBoxStyle.YesNo)
            If yes = 6 Then
                rDocument.PrintToPrinter(1, True, 1, 1)
                yes = MsgBox("Presione en Sí cuando este lista la Impresora para Imprimir la Copia del Contrato  ", MsgBoxStyle.YesNo)
                If yes = 6 Then
                    rDocument.PrintToPrinter(1, True, 1, 1)
                End If
            End If



        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Limpiar()
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub ChecaAntesGrabaRecontratacion(ByVal ClvSession As Long, ByVal SoloInternet As Boolean, ByVal aparatoNet As Boolean, ByVal aparatoDig As Boolean)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ChecaAntesGrabaRecontratacion", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@ClvSession", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = ClvSession
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@SoloInternet", SqlDbType.Bit)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = SoloInternet
        comando.Parameters.Add(parametro2)

        Dim parametro5 As New SqlParameter("@aparatoNet", SqlDbType.Bit)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = aparatoNet
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@aparatoDig", SqlDbType.Bit)
        parametro6.Direction = ParameterDirection.Input
        parametro6.Value = aparatoDig
        comando.Parameters.Add(parametro6)

        Dim parametro3 As New SqlParameter("@Res", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        parametro4.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = 0
            eMsj = String.Empty
            eRes = parametro3.Value
            eMsj = parametro4.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub



    Private Sub tvDIGCable_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvDIGCable.AfterSelect
        If IsNumeric(e.Node.Tag) = False Then
            Exit Sub
        End If
        If e.Node.Level = 0 Then
            eContratoNet = CLng(e.Node.Tag)
        Else
            eContratoNet = CLng(e.Node.Parent.Tag)
            eClv_Servicio = CInt(e.Node.Tag)
        End If
    End Sub

    Private Sub tsbAgregarTDIGCable_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbAgregarTDIGCable.Click
        VALIDADetReconDigTipSerTmp(eClv_Session, eClv_TipSer)
        If (eMsj.Length > 0) Then
            MessageBox.Show(eMsj)
            Exit Sub
        End If

        If eClv_TipSer = 3 Then
            NueDetReconAntTmp(eClv_Session, eContrato)
        ElseIf eClv_TipSer = 6 Then
            NueDetReconDigTmp(1, eClv_Session, eContrato, "A", 0, eClv_TipSer, 0)
        End If

        LlenatvDigCable()
    End Sub

    Private Sub tsbEliminarTDIGCable_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbEliminarTDIGCable.Click
        If eContratoNet = 0 Then
            MsgBox("Selecciona una Tarjeta.", MsgBoxStyle.Information)
            Exit Sub
        End If
        BorDetReconDigTmp(eClv_Session, "A", eContratoNet, 0)
        LlenatvDigCable()
        BorraRelClientesApartos(eContrato, eContratoNet) 'ELIMINAMOS LA RELACIÓN DEL TIPO DEL APARATO CON EL SERVICIO ()

        eContratoNet = 0
    End Sub

    Private Sub tsbAgregarSDIGCable_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbAgregarSDIGCable.Click
        If eContratoNet = 0 Then
            MsgBox("Selecciona una Tarjeta a la que se le agregará el Servicio.", MsgBoxStyle.Information)
            Exit Sub
        End If
        panelDIGCable.Visible = True
        tsbEliminarTDIGCable.Enabled = False
        comboSerDIGCable.DataSource = MuestraServicioRecon(eClv_Session, 6, eContratoNet)
    End Sub

    Private Sub tsbEliminarSDIGCable_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbEliminarSDIGCable.Click
        If eClv_Servicio = 0 Then
            MsgBox("Seleccion un Servicio.", MsgBoxStyle.Information)
            Exit Sub
        End If
        BorDetReconDigTmp(eClv_Session, "S", eContratoNet, eClv_Servicio)
        LlenatvDigCable()
        eClv_Servicio = 0
    End Sub

    Private Sub btnDIGCableAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDIGCableAceptar.Click
        If comboSerDIGCable.SelectedIndex <= -1 Then
            MsgBox("¡Selecciona un Servicio para continuar!", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.cmbTipoAparatoCable.SelectedIndex <= -1 Then
            MsgBox("¡Selecciona una Marca de Aparato para continuar!", MsgBoxStyle.Information)
            Exit Sub
        End If

        NueDetReconDigTmp(1, eClv_Session, eContrato, "S", eContratoNet, eClv_TipSer, comboSerDIGCable.SelectedValue)
        LlenatvDigCable()
        GuardaRelClientesAparatos(eContrato, eContratoNet, CInt(Me.comboSerDIGCable.SelectedValue), CInt(Me.cmbTipoAparatoCable.SelectedValue)) 'GUARDA LA RELACIÓN DE LA MARCA DEL APARATO A INSTALAR CON EL SERVICIO

        panelDIGCable.Visible = False
        tsbEliminarTDIGCable.Enabled = True
    End Sub

    Private Sub btnDIGCableCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDIGCableCancelar.Click
        panelDIGCable.Visible = False
        tsbEliminarTDIGCable.Enabled = True
    End Sub

    Private Sub VALIDADetReconDigTipSerTmp(ByVal ClvSession As Integer, ByVal Clv_TipSer As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("VALIDADetReconDigTipSerTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@ClvSession", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = ClvSession
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_TipSer
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eMsj = String.Empty
            eMsj = parametro3.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

#Region "METODOS TIPO APARATOS"
    Private Sub ConsultaAparatosContratacion(ByVal prmClvServicio As Integer, ByVal prmClvTipSer As Integer)
        Try
            Dim tipoAparato As New BaseIII

            tipoAparato.limpiaParametros()
            tipoAparato.CreateMyParameter("@Clv_Servicio", SqlDbType.Int, prmClvServicio)
            If prmClvTipSer = 3 Then
                Me.cmbTipoAparatoDth.DataSource = tipoAparato.ConsultaDT("ConsultaAparatosContratacion")
            ElseIf prmClvTipSer = 6 Then
                Me.cmbTipoAparatoCable.DataSource = tipoAparato.ConsultaDT("ConsultaAparatosContratacion")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub GuardaRelClientesAparatos(ByVal prmContrato As Integer, ByVal prmContratoNet As Integer, ByVal prmClvServicio As Integer, ByVal prmIdArticulo As Integer)
        Try
            Dim tipoAparato As New BaseIII

            tipoAparato.limpiaParametros()
            tipoAparato.CreateMyParameter("@Contrato", SqlDbType.Int, prmContrato)
            tipoAparato.CreateMyParameter("@ContratoNet", SqlDbType.Int, prmContratoNet)
            tipoAparato.CreateMyParameter("@Clv_Servicio", SqlDbType.Int, prmClvServicio)
            tipoAparato.CreateMyParameter("@IdArticulo", SqlDbType.Int, prmIdArticulo)
            tipoAparato.Inserta("GuardaRelClientesAparatos")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BorraRelClientesApartos(ByVal prmContrato As Integer, ByVal prmContratoNet As Integer)
        Try
            Dim tipoAparato As New BaseIII

            tipoAparato.limpiaParametros()
            tipoAparato.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, prmContrato)
            tipoAparato.CreateMyParameter("@CONTRATONET", SqlDbType.BigInt, prmContratoNet)
            tipoAparato.Inserta("UspBorraRelAparatosClientes")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region


    Private Function DataSetEDGAR() As Object
        Throw New NotImplementedException
    End Function

    Private Sub uspDimeAdeudoBaja()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CInt(Me.TextBoxContrato.Text))
        BaseII.CreateMyParameter("@Adeudo", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("uspDimeAdeudoBaja")
        TieneAdeudo = CInt(BaseII.dicoPar("@Adeudo").ToString)
    End Sub

End Class