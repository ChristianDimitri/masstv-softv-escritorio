﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmFiltroFechasCnr
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.rbPorContrato = New System.Windows.Forms.RadioButton()
        Me.rbPorFechas = New System.Windows.Forms.RadioButton()
        Me.lblContrato = New System.Windows.Forms.Label()
        Me.txtContrato = New System.Windows.Forms.TextBox()
        Me.lblFechaIni = New System.Windows.Forms.Label()
        Me.lblFechaFin = New System.Windows.Forms.Label()
        Me.dtpFechaIni = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaFin = New System.Windows.Forms.DateTimePicker()
        Me.btnGenerar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.gbRangos = New System.Windows.Forms.GroupBox()
        Me.pnlFechas = New System.Windows.Forms.Panel()
        Me.pnlContrato = New System.Windows.Forms.Panel()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.gbRangos.SuspendLayout()
        Me.pnlFechas.SuspendLayout()
        Me.pnlContrato.SuspendLayout()
        Me.SuspendLayout()
        '
        'rbPorContrato
        '
        Me.rbPorContrato.AutoSize = True
        Me.rbPorContrato.Checked = True
        Me.rbPorContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbPorContrato.Location = New System.Drawing.Point(14, 21)
        Me.rbPorContrato.Name = "rbPorContrato"
        Me.rbPorContrato.Size = New System.Drawing.Size(139, 24)
        Me.rbPorContrato.TabIndex = 0
        Me.rbPorContrato.TabStop = True
        Me.rbPorContrato.Text = "Por Contrato :"
        Me.rbPorContrato.UseVisualStyleBackColor = True
        '
        'rbPorFechas
        '
        Me.rbPorFechas.AutoSize = True
        Me.rbPorFechas.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbPorFechas.Location = New System.Drawing.Point(14, 104)
        Me.rbPorFechas.Name = "rbPorFechas"
        Me.rbPorFechas.Size = New System.Drawing.Size(128, 24)
        Me.rbPorFechas.TabIndex = 3
        Me.rbPorFechas.Text = "Por Fechas :"
        Me.rbPorFechas.UseVisualStyleBackColor = True
        '
        'lblContrato
        '
        Me.lblContrato.AutoSize = True
        Me.lblContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContrato.Location = New System.Drawing.Point(21, 6)
        Me.lblContrato.Name = "lblContrato"
        Me.lblContrato.Size = New System.Drawing.Size(74, 16)
        Me.lblContrato.TabIndex = 1
        Me.lblContrato.Text = "Contrato :"
        '
        'txtContrato
        '
        Me.txtContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtContrato.Location = New System.Drawing.Point(101, 3)
        Me.txtContrato.Name = "txtContrato"
        Me.txtContrato.Size = New System.Drawing.Size(138, 22)
        Me.txtContrato.TabIndex = 2
        '
        'lblFechaIni
        '
        Me.lblFechaIni.AutoSize = True
        Me.lblFechaIni.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFechaIni.Location = New System.Drawing.Point(3, 6)
        Me.lblFechaIni.Name = "lblFechaIni"
        Me.lblFechaIni.Size = New System.Drawing.Size(104, 16)
        Me.lblFechaIni.TabIndex = 4
        Me.lblFechaIni.Text = "Fecha Inicial :"
        '
        'lblFechaFin
        '
        Me.lblFechaFin.AutoSize = True
        Me.lblFechaFin.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFechaFin.Location = New System.Drawing.Point(10, 36)
        Me.lblFechaFin.Name = "lblFechaFin"
        Me.lblFechaFin.Size = New System.Drawing.Size(97, 16)
        Me.lblFechaFin.TabIndex = 6
        Me.lblFechaFin.Text = "Fecha Final :"
        '
        'dtpFechaIni
        '
        Me.dtpFechaIni.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaIni.Location = New System.Drawing.Point(111, 3)
        Me.dtpFechaIni.Name = "dtpFechaIni"
        Me.dtpFechaIni.Size = New System.Drawing.Size(147, 22)
        Me.dtpFechaIni.TabIndex = 5
        '
        'dtpFechaFin
        '
        Me.dtpFechaFin.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaFin.Location = New System.Drawing.Point(111, 35)
        Me.dtpFechaFin.Name = "dtpFechaFin"
        Me.dtpFechaFin.Size = New System.Drawing.Size(147, 22)
        Me.dtpFechaFin.TabIndex = 7
        '
        'btnGenerar
        '
        Me.btnGenerar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGenerar.Location = New System.Drawing.Point(26, 229)
        Me.btnGenerar.Name = "btnGenerar"
        Me.btnGenerar.Size = New System.Drawing.Size(112, 33)
        Me.btnGenerar.TabIndex = 8
        Me.btnGenerar.Text = "&Generar"
        Me.btnGenerar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(166, 229)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(110, 33)
        Me.btnCancelar.TabIndex = 9
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'gbRangos
        '
        Me.gbRangos.Controls.Add(Me.pnlFechas)
        Me.gbRangos.Controls.Add(Me.pnlContrato)
        Me.gbRangos.Controls.Add(Me.rbPorContrato)
        Me.gbRangos.Controls.Add(Me.rbPorFechas)
        Me.gbRangos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbRangos.Location = New System.Drawing.Point(12, 12)
        Me.gbRangos.Name = "gbRangos"
        Me.gbRangos.Size = New System.Drawing.Size(285, 211)
        Me.gbRangos.TabIndex = 10
        Me.gbRangos.TabStop = False
        Me.gbRangos.Text = "Rangos"
        '
        'pnlFechas
        '
        Me.pnlFechas.Controls.Add(Me.dtpFechaIni)
        Me.pnlFechas.Controls.Add(Me.lblFechaFin)
        Me.pnlFechas.Controls.Add(Me.lblFechaIni)
        Me.pnlFechas.Controls.Add(Me.dtpFechaFin)
        Me.pnlFechas.Location = New System.Drawing.Point(6, 134)
        Me.pnlFechas.Name = "pnlFechas"
        Me.pnlFechas.Size = New System.Drawing.Size(269, 64)
        Me.pnlFechas.TabIndex = 9
        '
        'pnlContrato
        '
        Me.pnlContrato.Controls.Add(Me.txtContrato)
        Me.pnlContrato.Controls.Add(Me.lblContrato)
        Me.pnlContrato.Location = New System.Drawing.Point(14, 51)
        Me.pnlContrato.Name = "pnlContrato"
        Me.pnlContrato.Size = New System.Drawing.Size(250, 36)
        Me.pnlContrato.TabIndex = 8
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'FrmFiltroFechasCnr
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(304, 274)
        Me.Controls.Add(Me.gbRangos)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnGenerar)
        Me.Name = "FrmFiltroFechasCnr"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Seleccion de Rangos"
        Me.gbRangos.ResumeLayout(False)
        Me.gbRangos.PerformLayout()
        Me.pnlFechas.ResumeLayout(False)
        Me.pnlFechas.PerformLayout()
        Me.pnlContrato.ResumeLayout(False)
        Me.pnlContrato.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblContrato As System.Windows.Forms.Label
    Friend WithEvents lblFechaIni As System.Windows.Forms.Label
    Friend WithEvents lblFechaFin As System.Windows.Forms.Label
    Friend WithEvents btnGenerar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents gbRangos As System.Windows.Forms.GroupBox
    Friend WithEvents pnlContrato As System.Windows.Forms.Panel
    Friend WithEvents pnlFechas As System.Windows.Forms.Panel
    Public WithEvents txtContrato As System.Windows.Forms.TextBox
    Public WithEvents dtpFechaIni As System.Windows.Forms.DateTimePicker
    Public WithEvents dtpFechaFin As System.Windows.Forms.DateTimePicker
    Public WithEvents rbPorContrato As System.Windows.Forms.RadioButton
    Public WithEvents rbPorFechas As System.Windows.Forms.RadioButton
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
