<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmReset
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CONTRATOLabel As System.Windows.Forms.Label
        Dim NOMBRELabel As System.Windows.Forms.Label
        Dim CALLELabel As System.Windows.Forms.Label
        Dim COLONIALabel As System.Windows.Forms.Label
        Dim NUMEROLabel As System.Windows.Forms.Label
        Dim CIUDADLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.MuestraServCteResetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraServCteResetTableAdapter = New sofTV.DataSetEricTableAdapters.MuestraServCteResetTableAdapter()
        Me.MuestraServCteResetDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ResetServCteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ResetServCteTableAdapter = New sofTV.DataSetEricTableAdapters.ResetServCteTableAdapter()
        Me.DameClientesActivosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameClientesActivosTableAdapter = New sofTV.DataSetEricTableAdapters.DameClientesActivosTableAdapter()
        Me.NOMBRETextBox = New System.Windows.Forms.TextBox()
        Me.CALLETextBox = New System.Windows.Forms.TextBox()
        Me.COLONIATextBox = New System.Windows.Forms.TextBox()
        Me.NUMEROTextBox = New System.Windows.Forms.TextBox()
        Me.CIUDADTextBox = New System.Windows.Forms.TextBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.ContratoTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_CableModemTextBox = New System.Windows.Forms.TextBox()
        Me.Button4 = New System.Windows.Forms.Button()
        CONTRATOLabel = New System.Windows.Forms.Label()
        NOMBRELabel = New System.Windows.Forms.Label()
        CALLELabel = New System.Windows.Forms.Label()
        COLONIALabel = New System.Windows.Forms.Label()
        NUMEROLabel = New System.Windows.Forms.Label()
        CIUDADLabel = New System.Windows.Forms.Label()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraServCteResetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraServCteResetDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ResetServCteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameClientesActivosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'CONTRATOLabel
        '
        CONTRATOLabel.AutoSize = True
        CONTRATOLabel.Location = New System.Drawing.Point(14, 44)
        CONTRATOLabel.Name = "CONTRATOLabel"
        CONTRATOLabel.Size = New System.Drawing.Size(74, 16)
        CONTRATOLabel.TabIndex = 3
        CONTRATOLabel.Text = "Contrato :"
        '
        'NOMBRELabel
        '
        NOMBRELabel.AutoSize = True
        NOMBRELabel.Location = New System.Drawing.Point(14, 70)
        NOMBRELabel.Name = "NOMBRELabel"
        NOMBRELabel.Size = New System.Drawing.Size(71, 16)
        NOMBRELabel.TabIndex = 5
        NOMBRELabel.Text = "Nombre :"
        '
        'CALLELabel
        '
        CALLELabel.AutoSize = True
        CALLELabel.Location = New System.Drawing.Point(14, 96)
        CALLELabel.Name = "CALLELabel"
        CALLELabel.Size = New System.Drawing.Size(52, 16)
        CALLELabel.TabIndex = 7
        CALLELabel.Text = "Calle :"
        '
        'COLONIALabel
        '
        COLONIALabel.AutoSize = True
        COLONIALabel.Location = New System.Drawing.Point(14, 122)
        COLONIALabel.Name = "COLONIALabel"
        COLONIALabel.Size = New System.Drawing.Size(69, 16)
        COLONIALabel.TabIndex = 9
        COLONIALabel.Text = "Colonia :"
        '
        'NUMEROLabel
        '
        NUMEROLabel.AutoSize = True
        NUMEROLabel.Location = New System.Drawing.Point(384, 125)
        NUMEROLabel.Name = "NUMEROLabel"
        NUMEROLabel.Size = New System.Drawing.Size(24, 16)
        NUMEROLabel.TabIndex = 11
        NUMEROLabel.Text = "# :"
        '
        'CIUDADLabel
        '
        CIUDADLabel.AutoSize = True
        CIUDADLabel.Location = New System.Drawing.Point(14, 150)
        CIUDADLabel.Name = "CIUDADLabel"
        CIUDADLabel.Size = New System.Drawing.Size(65, 16)
        CIUDADLabel.TabIndex = 13
        CIUDADLabel.Text = "Ciudad :"
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MuestraServCteResetBindingSource
        '
        Me.MuestraServCteResetBindingSource.DataMember = "MuestraServCteReset"
        Me.MuestraServCteResetBindingSource.DataSource = Me.DataSetEric
        '
        'MuestraServCteResetTableAdapter
        '
        Me.MuestraServCteResetTableAdapter.ClearBeforeFill = True
        '
        'MuestraServCteResetDataGridView
        '
        Me.MuestraServCteResetDataGridView.AllowUserToAddRows = False
        Me.MuestraServCteResetDataGridView.AllowUserToDeleteRows = False
        Me.MuestraServCteResetDataGridView.AutoGenerateColumns = False
        Me.MuestraServCteResetDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MuestraServCteResetDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.MuestraServCteResetDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3})
        Me.MuestraServCteResetDataGridView.DataSource = Me.MuestraServCteResetBindingSource
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MuestraServCteResetDataGridView.DefaultCellStyle = DataGridViewCellStyle2
        Me.MuestraServCteResetDataGridView.Location = New System.Drawing.Point(17, 21)
        Me.MuestraServCteResetDataGridView.Name = "MuestraServCteResetDataGridView"
        Me.MuestraServCteResetDataGridView.ReadOnly = True
        Me.MuestraServCteResetDataGridView.Size = New System.Drawing.Size(376, 236)
        Me.MuestraServCteResetDataGridView.TabIndex = 2
        Me.MuestraServCteResetDataGridView.TabStop = False
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Contrato"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Contrato"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Clv_CableModem"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Clv_CableModem"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "MacCableModem"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Aparato"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 250
        '
        'ResetServCteBindingSource
        '
        Me.ResetServCteBindingSource.DataMember = "ResetServCte"
        Me.ResetServCteBindingSource.DataSource = Me.DataSetEric
        '
        'ResetServCteTableAdapter
        '
        Me.ResetServCteTableAdapter.ClearBeforeFill = True
        '
        'DameClientesActivosBindingSource
        '
        Me.DameClientesActivosBindingSource.DataMember = "DameClientesActivos"
        Me.DameClientesActivosBindingSource.DataSource = Me.DataSetEric
        '
        'DameClientesActivosTableAdapter
        '
        Me.DameClientesActivosTableAdapter.ClearBeforeFill = True
        '
        'NOMBRETextBox
        '
        Me.NOMBRETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "NOMBRE", True))
        Me.NOMBRETextBox.Location = New System.Drawing.Point(114, 67)
        Me.NOMBRETextBox.Name = "NOMBRETextBox"
        Me.NOMBRETextBox.ReadOnly = True
        Me.NOMBRETextBox.Size = New System.Drawing.Size(427, 22)
        Me.NOMBRETextBox.TabIndex = 6
        Me.NOMBRETextBox.TabStop = False
        '
        'CALLETextBox
        '
        Me.CALLETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "CALLE", True))
        Me.CALLETextBox.Location = New System.Drawing.Point(114, 93)
        Me.CALLETextBox.Name = "CALLETextBox"
        Me.CALLETextBox.ReadOnly = True
        Me.CALLETextBox.Size = New System.Drawing.Size(257, 22)
        Me.CALLETextBox.TabIndex = 8
        Me.CALLETextBox.TabStop = False
        '
        'COLONIATextBox
        '
        Me.COLONIATextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "COLONIA", True))
        Me.COLONIATextBox.Location = New System.Drawing.Point(114, 119)
        Me.COLONIATextBox.Name = "COLONIATextBox"
        Me.COLONIATextBox.ReadOnly = True
        Me.COLONIATextBox.Size = New System.Drawing.Size(257, 22)
        Me.COLONIATextBox.TabIndex = 10
        Me.COLONIATextBox.TabStop = False
        '
        'NUMEROTextBox
        '
        Me.NUMEROTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "NUMERO", True))
        Me.NUMEROTextBox.Location = New System.Drawing.Point(414, 122)
        Me.NUMEROTextBox.Name = "NUMEROTextBox"
        Me.NUMEROTextBox.ReadOnly = True
        Me.NUMEROTextBox.Size = New System.Drawing.Size(104, 22)
        Me.NUMEROTextBox.TabIndex = 12
        Me.NUMEROTextBox.TabStop = False
        '
        'CIUDADTextBox
        '
        Me.CIUDADTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "CIUDAD", True))
        Me.CIUDADTextBox.Location = New System.Drawing.Point(114, 147)
        Me.CIUDADTextBox.Name = "CIUDADTextBox"
        Me.CIUDADTextBox.ReadOnly = True
        Me.CIUDADTextBox.Size = New System.Drawing.Size(104, 22)
        Me.CIUDADTextBox.TabIndex = 14
        Me.CIUDADTextBox.TabStop = False
        '
        'Button3
        '
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Location = New System.Drawing.Point(231, 34)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(44, 23)
        Me.Button3.TabIndex = 1
        Me.Button3.Text = "..."
        Me.Button3.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Location = New System.Drawing.Point(114, 38)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(100, 22)
        Me.TextBox1.TabIndex = 0
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Checked = True
        Me.RadioButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton1.Location = New System.Drawing.Point(34, 29)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(73, 22)
        Me.RadioButton1.TabIndex = 1
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Digital"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.NOMBRETextBox)
        Me.GroupBox1.Controls.Add(Me.CIUDADTextBox)
        Me.GroupBox1.Controls.Add(CIUDADLabel)
        Me.GroupBox1.Controls.Add(Me.NUMEROTextBox)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(NUMEROLabel)
        Me.GroupBox1.Controls.Add(Me.Button3)
        Me.GroupBox1.Controls.Add(Me.COLONIATextBox)
        Me.GroupBox1.Controls.Add(CONTRATOLabel)
        Me.GroupBox1.Controls.Add(COLONIALabel)
        Me.GroupBox1.Controls.Add(NOMBRELabel)
        Me.GroupBox1.Controls.Add(Me.CALLETextBox)
        Me.GroupBox1.Controls.Add(CALLELabel)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(69, 43)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(582, 182)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Cliente"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.RadioButton2)
        Me.GroupBox2.Controls.Add(Me.RadioButton1)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(714, 77)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(156, 100)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Tipo de Servicio"
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton2.Location = New System.Drawing.Point(34, 55)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(82, 22)
        Me.RadioButton2.TabIndex = 45
        Me.RadioButton2.Text = "Internet"
        Me.RadioButton2.UseVisualStyleBackColor = True
        Me.RadioButton2.Visible = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.MuestraServCteResetDataGridView)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(69, 274)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(440, 263)
        Me.GroupBox3.TabIndex = 44
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Tarjeta(s) ó CableModem(s) del Cliente"
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(535, 274)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "&RESET"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(748, 495)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(136, 36)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "&SALIR"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'ContratoTextBox
        '
        Me.ContratoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraServCteResetBindingSource, "Contrato", True))
        Me.ContratoTextBox.Location = New System.Drawing.Point(575, 285)
        Me.ContratoTextBox.Name = "ContratoTextBox"
        Me.ContratoTextBox.ReadOnly = True
        Me.ContratoTextBox.Size = New System.Drawing.Size(10, 20)
        Me.ContratoTextBox.TabIndex = 46
        Me.ContratoTextBox.TabStop = False
        '
        'Clv_CableModemTextBox
        '
        Me.Clv_CableModemTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraServCteResetBindingSource, "Clv_CableModem", True))
        Me.Clv_CableModemTextBox.Location = New System.Drawing.Point(591, 285)
        Me.Clv_CableModemTextBox.Name = "Clv_CableModemTextBox"
        Me.Clv_CableModemTextBox.ReadOnly = True
        Me.Clv_CableModemTextBox.Size = New System.Drawing.Size(10, 20)
        Me.Clv_CableModemTextBox.TabIndex = 47
        Me.Clv_CableModemTextBox.TabStop = False
        '
        'Button4
        '
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(535, 316)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(136, 39)
        Me.Button4.TabIndex = 48
        Me.Button4.Text = "&RESET PRIMER MODELO"
        Me.Button4.UseVisualStyleBackColor = True
        Me.Button4.Visible = False
        '
        'FrmReset
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(914, 567)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Clv_CableModemTextBox)
        Me.Controls.Add(Me.ContratoTextBox)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "FrmReset"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Resetear Aparato"
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraServCteResetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraServCteResetDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ResetServCteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameClientesActivosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents MuestraServCteResetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraServCteResetTableAdapter As sofTV.DataSetEricTableAdapters.MuestraServCteResetTableAdapter
    Friend WithEvents MuestraServCteResetDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents ResetServCteBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ResetServCteTableAdapter As sofTV.DataSetEricTableAdapters.ResetServCteTableAdapter
    Friend WithEvents DameClientesActivosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameClientesActivosTableAdapter As sofTV.DataSetEricTableAdapters.DameClientesActivosTableAdapter
    Friend WithEvents NOMBRETextBox As System.Windows.Forms.TextBox
    Friend WithEvents CALLETextBox As System.Windows.Forms.TextBox
    Friend WithEvents COLONIATextBox As System.Windows.Forms.TextBox
    Friend WithEvents NUMEROTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CIUDADTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents ContratoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_CableModemTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Button4 As System.Windows.Forms.Button
End Class
