﻿Public Class frmFiltroMesesAnio

    Dim reporte As New classReporteRecuperacionCajas

#Region "Controles"
    Private Sub frmFiltroMesesAnio_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        llenaComboMeses()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub bntAceptar_Click(sender As System.Object, e As System.EventArgs) Handles bntAceptar.Click
        If IsNumeric(Me.cmbMeses.SelectedValue) = False Then
            MsgBox("¡Seleccione al menos un Mes de la lista!", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.txtInsertaAnio.Text.Length = 0 Then
            MsgBox("¡Ingrese un año para continuar!", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.txtInsertaAnio.Text.Length < 4 Then
            MsgBox("¡El año debe contener 4 caracteres como mínimo!", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.txtInsertaAnio.Text < 2012 Then
            MsgBox("¡El año debe ser mayor o igual a 2012!", MsgBoxStyle.Information)
            Exit Sub
        End If

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub txtInsertaAnio_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtInsertaAnio.KeyPress
        If InStr(1, "0123456789,-" & Chr(8), e.KeyChar) = 0 Then 'VALIDACIÓN PARA QUE SÓLO ADMITA VALORES NUMÉRICOS EN EL TEXTBOX (txtInsertaAnio)
            e.KeyChar = ""
        End If
    End Sub
#End Region

#Region "Métodos"
    Private Sub llenaComboMeses()
        Me.cmbMeses.DataSource = reporte.uspMuestraMesesDelAnio
    End Sub
#End Region
End Class