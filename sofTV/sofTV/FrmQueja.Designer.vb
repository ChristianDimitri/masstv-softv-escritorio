﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmQueja
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_QuejaLabel As System.Windows.Forms.Label
        Dim ContratoLabel As System.Windows.Forms.Label
        Dim Fecha_SoliciutudLabel As System.Windows.Forms.Label
        Dim ObservacionesLabel As System.Windows.Forms.Label
        Dim StatusLabel As System.Windows.Forms.Label
        Dim Clave_TecnicoLabel As System.Windows.Forms.Label
        Dim ProblemaLabel As System.Windows.Forms.Label
        Dim SolucionLabel As System.Windows.Forms.Label
        Dim ClasificacionLabel As System.Windows.Forms.Label
        Dim Tipo_TrabajoLabel As System.Windows.Forms.Label
        Dim ServicioLabel As System.Windows.Forms.Label
        Dim SOLOINTERNETLabel1 As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Visita1Label1 As System.Windows.Forms.Label
        Dim Visita2Label As System.Windows.Forms.Label
        Dim Label7 As System.Windows.Forms.Label
        Dim Label12 As System.Windows.Forms.Label
        Dim Label13 As System.Windows.Forms.Label
        Dim lblClasifProblema As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmQueja))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.TextComentario = New System.Windows.Forms.TextBox()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.dtpHoraEnProceso = New System.Windows.Forms.DateTimePicker()
        Me.EnProcesoMaskedTextBox = New System.Windows.Forms.MaskedTextBox()
        Me.lblEnProceso = New System.Windows.Forms.Label()
        Me.Visita3MaskedTextBox = New System.Windows.Forms.MaskedTextBox()
        Me.Visita3MaskedTextBox1 = New System.Windows.Forms.TextBox()
        Me.Hora_sol = New System.Windows.Forms.DateTimePicker()
        Me.dtpHoraVisita2 = New System.Windows.Forms.DateTimePicker()
        Me.dtpHoraVisita3 = New System.Windows.Forms.DateTimePicker()
        Me.Visita2MaskedTextBox = New System.Windows.Forms.MaskedTextBox()
        Me.dtpHoraVisita1 = New System.Windows.Forms.DateTimePicker()
        Me.Visita3Label = New System.Windows.Forms.Label()
        Me.EjecutoLabel1 = New System.Windows.Forms.Label()
        Me.Consulta_RelQuejaUsuarioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric2 = New sofTV.DataSetEric2()
        Me.ComboBox5 = New System.Windows.Forms.ComboBox()
        Me.CONQUEJASBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.MuestraTipSerPrincipalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GeneroLabel1 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Fecha_EjecucionMaskedTextBox = New System.Windows.Forms.MaskedTextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Fecha_EjecucionMaskedTextBox1 = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.FechaTextBox = New System.Windows.Forms.TextBox()
        Me.Visita1MaskedTextBox = New System.Windows.Forms.MaskedTextBox()
        Me.CMBLabel7 = New System.Windows.Forms.Label()
        Me.Clv_QuejaTextBox = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.CMBTextBox4 = New System.Windows.Forms.TextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.rbEnProceso = New System.Windows.Forms.RadioButton()
        Me.RadioButton3 = New System.Windows.Forms.RadioButton()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.ESHOTELCheckBox = New System.Windows.Forms.CheckBox()
        Me.SOLOINTERNETCheckBox = New System.Windows.Forms.CheckBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.NOMBRELabel1 = New System.Windows.Forms.Label()
        Me.CALLELabel1 = New System.Windows.Forms.Label()
        Me.COLONIALabel1 = New System.Windows.Forms.Label()
        Me.NUMEROLabel1 = New System.Windows.Forms.Label()
        Me.CIUDADLabel1 = New System.Windows.Forms.Label()
        Me.CONQUEJASBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.CONQUEJASBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.ContratoTextBox = New System.Windows.Forms.TextBox()
        Me.ClasificacionTextBox = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.StatusTextBox = New System.Windows.Forms.TextBox()
        Me.Visita2MaskedTextBox1 = New System.Windows.Forms.TextBox()
        Me.Visita1MaskedTextBox1 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.cmbPrioridadQueja = New System.Windows.Forms.ComboBox()
        Me.BUSCLIPORCONTRATOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DamefechahoraservBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetarnoldo = New sofTV.DataSetarnoldo()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.SolucionTextBox = New System.Windows.Forms.TextBox()
        Me.ProblemaTextBox = New System.Windows.Forms.TextBox()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.MuestraTecnicosQuejasbuenoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Procedimientosarnoldo4 = New sofTV.Procedimientosarnoldo4()
        Me.MuestraTecnicosAlmacenBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ObservacionesTextBoxX = New System.Windows.Forms.TextBox()
        Me.ComboBox4 = New System.Windows.Forms.ComboBox()
        Me.MUESTRATRABAJOSQUEJASBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.MUESTRACLASIFICACIONQUEJASBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Clv_TrabajoTextBox = New System.Windows.Forms.TextBox()
        Me.Clave_TecnicoTextBox = New System.Windows.Forms.TextBox()
        Me.FechaDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.DameFechadelServidorHoraBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRATECNICOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameUltimo_dia_del_MesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRATRABAJOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button5 = New System.Windows.Forms.Button()
        Me.CONQUEJASTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONQUEJASTableAdapter()
        Me.MUESTRACLASIFICACIONQUEJASTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACLASIFICACIONQUEJASTableAdapter()
        Me.MUESTRATECNICOSTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRATECNICOSTableAdapter()
        Me.MUESTRATRABAJOSTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRATRABAJOSTableAdapter()
        Me.DameSerDELCliBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameSerDELCliTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.dameSerDELCliTableAdapter()
        Me.BUSCLIPORCONTRATOTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.BUSCLIPORCONTRATOTableAdapter()
        Me.NUE_CITASTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.NUE_CITASTableAdapter()
        Me.NUE_CITASBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONDetCitasTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONDetCitasTableAdapter()
        Me.CONDetCitasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NUEREL_CITAS_QUEJASBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NUEREL_CITAS_QUEJASTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.NUEREL_CITAS_QUEJASTableAdapter()
        Me.DameUltimo_dia_del_MesTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.DameUltimo_dia_del_MesTableAdapter()
        Me.MUESTRATRABAJOSQUEJASTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRATRABAJOSQUEJASTableAdapter()
        Me.MuestraTipSerPrincipalTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MuestraTipSerPrincipalTableAdapter()
        Me.DAMEFECHADELSERVIDORBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DAMEFECHADELSERVIDORTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.DAMEFECHADELSERVIDORTableAdapter()
        Me.DameFechadelServidorHoraTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.DameFechadelServidorHoraTableAdapter()
        Me.Muestra_Tecnicos_AlmacenTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Muestra_Tecnicos_AlmacenTableAdapter()
        Me.Inserta_DetBitTecBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_DetBitTecTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Inserta_DetBitTecTableAdapter()
        Me.DameClv_Session_TecnicosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameClv_Session_TecnicosTableAdapter = New sofTV.DataSetarnoldoTableAdapters.DameClv_Session_TecnicosTableAdapter()
        Me.Muestra_no_quejaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Muestra_no_quejaTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Muestra_no_quejaTableAdapter()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.lblObsAgendaMostrar = New System.Windows.Forms.Label()
        Me.lblObsAgenda = New System.Windows.Forms.Label()
        Me.cmbClasifProblemas = New System.Windows.Forms.ComboBox()
        Me.ObservacionesTextBox = New System.Windows.Forms.RichTextBox()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Clv_TipoQuejaTextBox = New System.Windows.Forms.TextBox()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.cbCuadrilla = New System.Windows.Forms.ComboBox()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.Dame_Impresora_OrdenesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_Impresora_OrdenesTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Dame_Impresora_OrdenesTableAdapter()
        Me.DataSetLidia = New sofTV.DataSetLidia()
        Me.Dame_Folio_QBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_Folio_QTableAdapter = New sofTV.DataSetLidiaTableAdapters.Dame_Folio_QTableAdapter()
        Me.Inserta_Bitacora_tec_QBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Bitacora_tec_QTableAdapter = New sofTV.DataSetLidiaTableAdapters.Inserta_Bitacora_tec_QTableAdapter()
        Me.Inserta_Rel_Bitacora_QuejaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Rel_Bitacora_QuejaTableAdapter = New sofTV.DataSetLidiaTableAdapters.Inserta_Rel_Bitacora_QuejaTableAdapter()
        Me.Dame_fecha_hora_servTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Dame_fecha_hora_servTableAdapter()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.BuscaBloqueadoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BuscaBloqueadoTableAdapter = New sofTV.DataSetLidiaTableAdapters.BuscaBloqueadoTableAdapter()
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2()
        Me.Inserta_RelCobraDescBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_RelCobraDescTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_RelCobraDescTableAdapter()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.NueRelQuejaUsuarioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NueRelQuejaUsuarioTableAdapter = New sofTV.DataSetEricTableAdapters.NueRelQuejaUsuarioTableAdapter()
        Me.Consulta_RelQuejaUsuarioTableAdapter = New sofTV.DataSetEric2TableAdapters.Consulta_RelQuejaUsuarioTableAdapter()
        Me.Muestra_Tecnicos_QuejasbuenoTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Muestra_Tecnicos_QuejasbuenoTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter5 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter6 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter7 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter8 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter9 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter10 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter11 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter12 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter13 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter14 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.TxtRefOxxo = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Clv_QuejaLabel = New System.Windows.Forms.Label()
        ContratoLabel = New System.Windows.Forms.Label()
        Fecha_SoliciutudLabel = New System.Windows.Forms.Label()
        ObservacionesLabel = New System.Windows.Forms.Label()
        StatusLabel = New System.Windows.Forms.Label()
        Clave_TecnicoLabel = New System.Windows.Forms.Label()
        ProblemaLabel = New System.Windows.Forms.Label()
        SolucionLabel = New System.Windows.Forms.Label()
        ClasificacionLabel = New System.Windows.Forms.Label()
        Tipo_TrabajoLabel = New System.Windows.Forms.Label()
        ServicioLabel = New System.Windows.Forms.Label()
        SOLOINTERNETLabel1 = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Visita1Label1 = New System.Windows.Forms.Label()
        Visita2Label = New System.Windows.Forms.Label()
        Label7 = New System.Windows.Forms.Label()
        Label12 = New System.Windows.Forms.Label()
        Label13 = New System.Windows.Forms.Label()
        lblClasifProblema = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.Consulta_RelQuejaUsuarioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONQUEJASBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTipSerPrincipalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel6.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.CONQUEJASBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONQUEJASBindingNavigator.SuspendLayout()
        CType(Me.BUSCLIPORCONTRATOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DamefechahoraservBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTecnicosQuejasbuenoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTecnicosAlmacenBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRATRABAJOSQUEJASBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACLASIFICACIONQUEJASBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameFechadelServidorHoraBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRATECNICOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameUltimo_dia_del_MesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRATRABAJOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameSerDELCliBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NUE_CITASBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONDetCitasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NUEREL_CITAS_QUEJASBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAMEFECHADELSERVIDORBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_DetBitTecBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameClv_Session_TecnicosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Muestra_no_quejaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel10.SuspendLayout()
        CType(Me.Dame_Impresora_OrdenesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_Folio_QBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Bitacora_tec_QBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Rel_Bitacora_QuejaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BuscaBloqueadoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_RelCobraDescBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NueRelQuejaUsuarioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel12.SuspendLayout()
        Me.SuspendLayout()
        '
        'Clv_QuejaLabel
        '
        Clv_QuejaLabel.AutoSize = True
        Clv_QuejaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_QuejaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_QuejaLabel.Location = New System.Drawing.Point(34, 81)
        Clv_QuejaLabel.Name = "Clv_QuejaLabel"
        Clv_QuejaLabel.Size = New System.Drawing.Size(50, 15)
        Clv_QuejaLabel.TabIndex = 0
        Clv_QuejaLabel.Text = "Clave :"
        '
        'ContratoLabel
        '
        ContratoLabel.AutoSize = True
        ContratoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ContratoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ContratoLabel.Location = New System.Drawing.Point(21, 109)
        ContratoLabel.Name = "ContratoLabel"
        ContratoLabel.Size = New System.Drawing.Size(65, 15)
        ContratoLabel.TabIndex = 2
        ContratoLabel.Text = "Contrato:"
        '
        'Fecha_SoliciutudLabel
        '
        Fecha_SoliciutudLabel.AutoSize = True
        Fecha_SoliciutudLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_SoliciutudLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Fecha_SoliciutudLabel.Location = New System.Drawing.Point(667, 174)
        Fecha_SoliciutudLabel.Name = "Fecha_SoliciutudLabel"
        Fecha_SoliciutudLabel.Size = New System.Drawing.Size(71, 15)
        Fecha_SoliciutudLabel.TabIndex = 6
        Fecha_SoliciutudLabel.Text = "Solicitud :"
        '
        'ObservacionesLabel
        '
        ObservacionesLabel.AutoSize = True
        ObservacionesLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ObservacionesLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ObservacionesLabel.Location = New System.Drawing.Point(13, 231)
        ObservacionesLabel.Name = "ObservacionesLabel"
        ObservacionesLabel.Size = New System.Drawing.Size(105, 15)
        ObservacionesLabel.TabIndex = 10
        ObservacionesLabel.Text = "Observaciones:"
        '
        'StatusLabel
        '
        StatusLabel.AutoSize = True
        StatusLabel.BackColor = System.Drawing.Color.WhiteSmoke
        StatusLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        StatusLabel.ForeColor = System.Drawing.Color.WhiteSmoke
        StatusLabel.Location = New System.Drawing.Point(123, 122)
        StatusLabel.Name = "StatusLabel"
        StatusLabel.Size = New System.Drawing.Size(51, 15)
        StatusLabel.TabIndex = 12
        StatusLabel.Text = "Status:"
        '
        'Clave_TecnicoLabel
        '
        Clave_TecnicoLabel.AutoSize = True
        Clave_TecnicoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clave_TecnicoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clave_TecnicoLabel.Location = New System.Drawing.Point(63, 10)
        Clave_TecnicoLabel.Name = "Clave_TecnicoLabel"
        Clave_TecnicoLabel.Size = New System.Drawing.Size(61, 15)
        Clave_TecnicoLabel.TabIndex = 14
        Clave_TecnicoLabel.Text = "Técnico:"
        '
        'ProblemaLabel
        '
        ProblemaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ProblemaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ProblemaLabel.Location = New System.Drawing.Point(34, 68)
        ProblemaLabel.Name = "ProblemaLabel"
        ProblemaLabel.Size = New System.Drawing.Size(86, 36)
        ProblemaLabel.TabIndex = 16
        ProblemaLabel.Text = "Reporte del Cliente :"
        '
        'SolucionLabel
        '
        SolucionLabel.AutoSize = True
        SolucionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SolucionLabel.ForeColor = System.Drawing.Color.LightSlateGray
        SolucionLabel.Location = New System.Drawing.Point(6, 9)
        SolucionLabel.Name = "SolucionLabel"
        SolucionLabel.Size = New System.Drawing.Size(111, 15)
        SolucionLabel.TabIndex = 18
        SolucionLabel.Text = "Problema Real :"
        '
        'ClasificacionLabel
        '
        ClasificacionLabel.AutoSize = True
        ClasificacionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ClasificacionLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ClasificacionLabel.Location = New System.Drawing.Point(6, 9)
        ClasificacionLabel.Name = "ClasificacionLabel"
        ClasificacionLabel.Size = New System.Drawing.Size(194, 15)
        ClasificacionLabel.TabIndex = 28
        ClasificacionLabel.Text = "Departamento Responsable :"
        '
        'Tipo_TrabajoLabel
        '
        Tipo_TrabajoLabel.AutoSize = True
        Tipo_TrabajoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Tipo_TrabajoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Tipo_TrabajoLabel.Location = New System.Drawing.Point(12, 4)
        Tipo_TrabajoLabel.Name = "Tipo_TrabajoLabel"
        Tipo_TrabajoLabel.Size = New System.Drawing.Size(123, 15)
        Tipo_TrabajoLabel.TabIndex = 32
        Tipo_TrabajoLabel.Text = "Tipo de Solución :"
        '
        'ServicioLabel
        '
        ServicioLabel.AutoSize = True
        ServicioLabel.Location = New System.Drawing.Point(9, 96)
        ServicioLabel.Name = "ServicioLabel"
        ServicioLabel.Size = New System.Drawing.Size(151, 16)
        ServicioLabel.TabIndex = 18
        ServicioLabel.Text = "Servicio Asignados :"
        '
        'SOLOINTERNETLabel1
        '
        SOLOINTERNETLabel1.AutoSize = True
        SOLOINTERNETLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SOLOINTERNETLabel1.ForeColor = System.Drawing.Color.White
        SOLOINTERNETLabel1.Location = New System.Drawing.Point(300, 73)
        SOLOINTERNETLabel1.Name = "SOLOINTERNETLabel1"
        SOLOINTERNETLabel1.Size = New System.Drawing.Size(97, 15)
        SOLOINTERNETLabel1.TabIndex = 16
        SOLOINTERNETLabel1.Text = "Solo Internet :"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.OrangeRed
        Label1.Location = New System.Drawing.Point(3, 1)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(67, 20)
        Label1.TabIndex = 14
        Label1.Text = "Status:"
        '
        'Visita1Label1
        '
        Visita1Label1.AutoSize = True
        Visita1Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Visita1Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Visita1Label1.Location = New System.Drawing.Point(604, 236)
        Visita1Label1.Name = "Visita1Label1"
        Visita1Label1.Size = New System.Drawing.Size(54, 15)
        Visita1Label1.TabIndex = 409
        Visita1Label1.Text = "Visita1:"
        '
        'Visita2Label
        '
        Visita2Label.AutoSize = True
        Visita2Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Visita2Label.ForeColor = System.Drawing.Color.LightSlateGray
        Visita2Label.Location = New System.Drawing.Point(604, 263)
        Visita2Label.Name = "Visita2Label"
        Visita2Label.Size = New System.Drawing.Size(54, 15)
        Visita2Label.TabIndex = 410
        Visita2Label.Text = "Visita2:"
        '
        'Label7
        '
        Label7.AutoSize = True
        Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label7.ForeColor = System.Drawing.Color.LightSlateGray
        Label7.Location = New System.Drawing.Point(58, 42)
        Label7.Name = "Label7"
        Label7.Size = New System.Drawing.Size(69, 15)
        Label7.TabIndex = 15
        Label7.Text = "Cuadrilla:"
        '
        'Label12
        '
        Label12.AutoSize = True
        Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label12.ForeColor = System.Drawing.Color.LightSlateGray
        Label12.Location = New System.Drawing.Point(57, 41)
        Label12.Name = "Label12"
        Label12.Size = New System.Drawing.Size(70, 15)
        Label12.TabIndex = 457
        Label12.Text = "Prioridad:"
        '
        'Label13
        '
        Label13.AutoSize = True
        Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label13.ForeColor = System.Drawing.Color.LightSlateGray
        Label13.Location = New System.Drawing.Point(209, 67)
        Label13.Name = "Label13"
        Label13.Size = New System.Drawing.Size(89, 15)
        Label13.TabIndex = 461
        Label13.Text = "Comentario :"
        '
        'lblClasifProblema
        '
        lblClasifProblema.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        lblClasifProblema.ForeColor = System.Drawing.Color.LightSlateGray
        lblClasifProblema.Location = New System.Drawing.Point(12, 118)
        lblClasifProblema.Name = "lblClasifProblema"
        lblClasifProblema.Size = New System.Drawing.Size(106, 32)
        lblClasifProblema.TabIndex = 460
        lblClasifProblema.Text = "Clasificacion del Problema :"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.Panel12)
        Me.Panel1.Controls.Add(Me.TextComentario)
        Me.Panel1.Controls.Add(Label13)
        Me.Panel1.Controls.Add(Me.Panel11)
        Me.Panel1.Controls.Add(Me.dtpHoraEnProceso)
        Me.Panel1.Controls.Add(Me.EnProcesoMaskedTextBox)
        Me.Panel1.Controls.Add(Me.lblEnProceso)
        Me.Panel1.Controls.Add(Me.Visita3MaskedTextBox)
        Me.Panel1.Controls.Add(Me.Visita3MaskedTextBox1)
        Me.Panel1.Controls.Add(Me.Hora_sol)
        Me.Panel1.Controls.Add(Me.dtpHoraVisita2)
        Me.Panel1.Controls.Add(Me.dtpHoraVisita3)
        Me.Panel1.Controls.Add(Me.Visita2MaskedTextBox)
        Me.Panel1.Controls.Add(Me.dtpHoraVisita1)
        Me.Panel1.Controls.Add(Me.Visita3Label)
        Me.Panel1.Controls.Add(Me.EjecutoLabel1)
        Me.Panel1.Controls.Add(Me.ComboBox5)
        Me.Panel1.Controls.Add(Me.GeneroLabel1)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Panel6)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.FechaTextBox)
        Me.Panel1.Controls.Add(Me.Visita1MaskedTextBox)
        Me.Panel1.Controls.Add(Visita2Label)
        Me.Panel1.Controls.Add(Visita1Label1)
        Me.Panel1.Controls.Add(Me.CMBLabel7)
        Me.Panel1.Controls.Add(Me.Clv_QuejaTextBox)
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(Me.CMBTextBox4)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.CONQUEJASBindingNavigator)
        Me.Panel1.Controls.Add(Clv_QuejaLabel)
        Me.Panel1.Controls.Add(ContratoLabel)
        Me.Panel1.Controls.Add(Me.ContratoTextBox)
        Me.Panel1.Controls.Add(Fecha_SoliciutudLabel)
        Me.Panel1.Controls.Add(StatusLabel)
        Me.Panel1.Controls.Add(Me.ClasificacionTextBox)
        Me.Panel1.Controls.Add(Me.TextBox1)
        Me.Panel1.Controls.Add(Me.StatusTextBox)
        Me.Panel1.Controls.Add(Me.Visita2MaskedTextBox1)
        Me.Panel1.Controls.Add(Me.Visita1MaskedTextBox1)
        Me.Panel1.Controls.Add(Me.TextBox2)
        Me.Panel1.Location = New System.Drawing.Point(2, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(995, 352)
        Me.Panel1.TabIndex = 0
        Me.Panel1.TabStop = True
        '
        'TextComentario
        '
        Me.TextComentario.Enabled = False
        Me.TextComentario.Location = New System.Drawing.Point(304, 58)
        Me.TextComentario.MaxLength = 250
        Me.TextComentario.Multiline = True
        Me.TextComentario.Name = "TextComentario"
        Me.TextComentario.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextComentario.Size = New System.Drawing.Size(288, 40)
        Me.TextComentario.TabIndex = 460
        '
        'Panel11
        '
        Me.Panel11.Location = New System.Drawing.Point(882, 197)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(51, 145)
        Me.Panel11.TabIndex = 452
        '
        'dtpHoraEnProceso
        '
        Me.dtpHoraEnProceso.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpHoraEnProceso.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtpHoraEnProceso.Location = New System.Drawing.Point(788, 313)
        Me.dtpHoraEnProceso.Name = "dtpHoraEnProceso"
        Me.dtpHoraEnProceso.Size = New System.Drawing.Size(145, 21)
        Me.dtpHoraEnProceso.TabIndex = 456
        '
        'EnProcesoMaskedTextBox
        '
        Me.EnProcesoMaskedTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EnProcesoMaskedTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EnProcesoMaskedTextBox.Location = New System.Drawing.Point(664, 313)
        Me.EnProcesoMaskedTextBox.Mask = "00/00/0000"
        Me.EnProcesoMaskedTextBox.Name = "EnProcesoMaskedTextBox"
        Me.EnProcesoMaskedTextBox.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.EnProcesoMaskedTextBox.Size = New System.Drawing.Size(107, 21)
        Me.EnProcesoMaskedTextBox.TabIndex = 455
        Me.EnProcesoMaskedTextBox.TabStop = False
        '
        'lblEnProceso
        '
        Me.lblEnProceso.AutoSize = True
        Me.lblEnProceso.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEnProceso.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblEnProceso.Location = New System.Drawing.Point(573, 315)
        Me.lblEnProceso.Name = "lblEnProceso"
        Me.lblEnProceso.Size = New System.Drawing.Size(84, 15)
        Me.lblEnProceso.TabIndex = 454
        Me.lblEnProceso.Text = "En Proceso:"
        '
        'Visita3MaskedTextBox
        '
        Me.Visita3MaskedTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Visita3MaskedTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Visita3MaskedTextBox.Location = New System.Drawing.Point(664, 286)
        Me.Visita3MaskedTextBox.Mask = "00/00/0000"
        Me.Visita3MaskedTextBox.Name = "Visita3MaskedTextBox"
        Me.Visita3MaskedTextBox.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.Visita3MaskedTextBox.Size = New System.Drawing.Size(107, 21)
        Me.Visita3MaskedTextBox.TabIndex = 447
        Me.Visita3MaskedTextBox.TabStop = False
        '
        'Visita3MaskedTextBox1
        '
        Me.Visita3MaskedTextBox1.Location = New System.Drawing.Point(671, 287)
        Me.Visita3MaskedTextBox1.Name = "Visita3MaskedTextBox1"
        Me.Visita3MaskedTextBox1.Size = New System.Drawing.Size(99, 20)
        Me.Visita3MaskedTextBox1.TabIndex = 453
        '
        'Hora_sol
        '
        Me.Hora_sol.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Hora_sol.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.Hora_sol.Location = New System.Drawing.Point(788, 206)
        Me.Hora_sol.Name = "Hora_sol"
        Me.Hora_sol.Size = New System.Drawing.Size(136, 21)
        Me.Hora_sol.TabIndex = 449
        '
        'dtpHoraVisita2
        '
        Me.dtpHoraVisita2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpHoraVisita2.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtpHoraVisita2.Location = New System.Drawing.Point(788, 260)
        Me.dtpHoraVisita2.Name = "dtpHoraVisita2"
        Me.dtpHoraVisita2.Size = New System.Drawing.Size(145, 21)
        Me.dtpHoraVisita2.TabIndex = 451
        '
        'dtpHoraVisita3
        '
        Me.dtpHoraVisita3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpHoraVisita3.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtpHoraVisita3.Location = New System.Drawing.Point(788, 286)
        Me.dtpHoraVisita3.Name = "dtpHoraVisita3"
        Me.dtpHoraVisita3.Size = New System.Drawing.Size(145, 21)
        Me.dtpHoraVisita3.TabIndex = 450
        '
        'Visita2MaskedTextBox
        '
        Me.Visita2MaskedTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Visita2MaskedTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Visita2MaskedTextBox.Location = New System.Drawing.Point(664, 261)
        Me.Visita2MaskedTextBox.Mask = "00/00/0000"
        Me.Visita2MaskedTextBox.Name = "Visita2MaskedTextBox"
        Me.Visita2MaskedTextBox.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.Visita2MaskedTextBox.Size = New System.Drawing.Size(107, 21)
        Me.Visita2MaskedTextBox.TabIndex = 415
        Me.Visita2MaskedTextBox.TabStop = False
        '
        'dtpHoraVisita1
        '
        Me.dtpHoraVisita1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpHoraVisita1.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtpHoraVisita1.Location = New System.Drawing.Point(788, 233)
        Me.dtpHoraVisita1.Name = "dtpHoraVisita1"
        Me.dtpHoraVisita1.Size = New System.Drawing.Size(145, 21)
        Me.dtpHoraVisita1.TabIndex = 448
        '
        'Visita3Label
        '
        Me.Visita3Label.AutoSize = True
        Me.Visita3Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Visita3Label.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Visita3Label.Location = New System.Drawing.Point(604, 289)
        Me.Visita3Label.Name = "Visita3Label"
        Me.Visita3Label.Size = New System.Drawing.Size(54, 15)
        Me.Visita3Label.TabIndex = 446
        Me.Visita3Label.Text = "Visita3:"
        '
        'EjecutoLabel1
        '
        Me.EjecutoLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_RelQuejaUsuarioBindingSource, "Ejecuto", True))
        Me.EjecutoLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EjecutoLabel1.Location = New System.Drawing.Point(667, 121)
        Me.EjecutoLabel1.Name = "EjecutoLabel1"
        Me.EjecutoLabel1.Size = New System.Drawing.Size(306, 23)
        Me.EjecutoLabel1.TabIndex = 445
        Me.EjecutoLabel1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        Me.EjecutoLabel1.Visible = False
        '
        'Consulta_RelQuejaUsuarioBindingSource
        '
        Me.Consulta_RelQuejaUsuarioBindingSource.DataMember = "Consulta_RelQuejaUsuario"
        Me.Consulta_RelQuejaUsuarioBindingSource.DataSource = Me.DataSetEric2
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric2"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ComboBox5
        '
        Me.ComboBox5.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONQUEJASBindingSource, "Clv_TipSer", True))
        Me.ComboBox5.DataSource = Me.MuestraTipSerPrincipalBindingSource
        Me.ComboBox5.DisplayMember = "Concepto"
        Me.ComboBox5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox5.ForeColor = System.Drawing.Color.Red
        Me.ComboBox5.FormattingEnabled = True
        Me.ComboBox5.Location = New System.Drawing.Point(165, 28)
        Me.ComboBox5.Name = "ComboBox5"
        Me.ComboBox5.Size = New System.Drawing.Size(307, 26)
        Me.ComboBox5.TabIndex = 405
        Me.ComboBox5.TabStop = False
        Me.ComboBox5.ValueMember = "Clv_TipSerPrincipal"
        '
        'CONQUEJASBindingSource
        '
        Me.CONQUEJASBindingSource.DataMember = "CONQUEJAS"
        Me.CONQUEJASBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MuestraTipSerPrincipalBindingSource
        '
        Me.MuestraTipSerPrincipalBindingSource.DataMember = "MuestraTipSerPrincipal"
        Me.MuestraTipSerPrincipalBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'GeneroLabel1
        '
        Me.GeneroLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_RelQuejaUsuarioBindingSource, "Genero", True))
        Me.GeneroLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GeneroLabel1.Location = New System.Drawing.Point(667, 98)
        Me.GeneroLabel1.Name = "GeneroLabel1"
        Me.GeneroLabel1.Size = New System.Drawing.Size(306, 23)
        Me.GeneroLabel1.TabIndex = 444
        Me.GeneroLabel1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        Me.GeneroLabel1.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(599, 130)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(63, 15)
        Me.Label5.TabIndex = 443
        Me.Label5.Text = "Ejecutó :"
        Me.Label5.Visible = False
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.Fecha_EjecucionMaskedTextBox)
        Me.Panel6.Controls.Add(Me.Label2)
        Me.Panel6.Controls.Add(Me.Fecha_EjecucionMaskedTextBox1)
        Me.Panel6.Location = New System.Drawing.Point(577, 196)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(205, 34)
        Me.Panel6.TabIndex = 422
        '
        'Fecha_EjecucionMaskedTextBox
        '
        Me.Fecha_EjecucionMaskedTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Fecha_EjecucionMaskedTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fecha_EjecucionMaskedTextBox.Location = New System.Drawing.Point(87, 8)
        Me.Fecha_EjecucionMaskedTextBox.Mask = "00/00/0000 00:00 a.m."
        Me.Fecha_EjecucionMaskedTextBox.Name = "Fecha_EjecucionMaskedTextBox"
        Me.Fecha_EjecucionMaskedTextBox.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.Fecha_EjecucionMaskedTextBox.Size = New System.Drawing.Size(107, 21)
        Me.Fecha_EjecucionMaskedTextBox.TabIndex = 411
        Me.Fecha_EjecucionMaskedTextBox.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label2.Location = New System.Drawing.Point(3, 10)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 15)
        Me.Label2.TabIndex = 401
        Me.Label2.Text = "Ejecución :"
        '
        'Fecha_EjecucionMaskedTextBox1
        '
        Me.Fecha_EjecucionMaskedTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONQUEJASBindingSource, "Fecha_Ejecucion", True))
        Me.Fecha_EjecucionMaskedTextBox1.Location = New System.Drawing.Point(87, 9)
        Me.Fecha_EjecucionMaskedTextBox1.Name = "Fecha_EjecucionMaskedTextBox1"
        Me.Fecha_EjecucionMaskedTextBox1.Size = New System.Drawing.Size(100, 20)
        Me.Fecha_EjecucionMaskedTextBox1.TabIndex = 420
        Me.Fecha_EjecucionMaskedTextBox1.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(599, 108)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(62, 15)
        Me.Label4.TabIndex = 442
        Me.Label4.Text = "Generó :"
        Me.Label4.Visible = False
        '
        'FechaTextBox
        '
        Me.FechaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FechaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONQUEJASBindingSource, "Fecha_Soliciutud", True))
        Me.FechaTextBox.Enabled = False
        Me.FechaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.FechaTextBox.Location = New System.Drawing.Point(744, 171)
        Me.FechaTextBox.Name = "FechaTextBox"
        Me.FechaTextBox.Size = New System.Drawing.Size(147, 21)
        Me.FechaTextBox.TabIndex = 421
        Me.FechaTextBox.TabStop = False
        '
        'Visita1MaskedTextBox
        '
        Me.Visita1MaskedTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Visita1MaskedTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Visita1MaskedTextBox.Location = New System.Drawing.Point(664, 234)
        Me.Visita1MaskedTextBox.Mask = "00/00/0000"
        Me.Visita1MaskedTextBox.Name = "Visita1MaskedTextBox"
        Me.Visita1MaskedTextBox.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.Visita1MaskedTextBox.Size = New System.Drawing.Size(107, 21)
        Me.Visita1MaskedTextBox.TabIndex = 413
        Me.Visita1MaskedTextBox.TabStop = False
        '
        'CMBLabel7
        '
        Me.CMBLabel7.AutoSize = True
        Me.CMBLabel7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel7.Location = New System.Drawing.Point(8, 30)
        Me.CMBLabel7.Name = "CMBLabel7"
        Me.CMBLabel7.Size = New System.Drawing.Size(154, 18)
        Me.CMBLabel7.TabIndex = 406
        Me.CMBLabel7.Text = "Tipo de Servicios  :"
        '
        'Clv_QuejaTextBox
        '
        Me.Clv_QuejaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_QuejaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONQUEJASBindingSource, "Clv_Queja", True))
        Me.Clv_QuejaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_QuejaTextBox.Location = New System.Drawing.Point(94, 80)
        Me.Clv_QuejaTextBox.Name = "Clv_QuejaTextBox"
        Me.Clv_QuejaTextBox.ReadOnly = True
        Me.Clv_QuejaTextBox.Size = New System.Drawing.Size(104, 21)
        Me.Clv_QuejaTextBox.TabIndex = 100
        Me.Clv_QuejaTextBox.TabStop = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(205, 108)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(30, 22)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "..."
        Me.Button1.UseVisualStyleBackColor = False
        '
        'CMBTextBox4
        '
        Me.CMBTextBox4.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox4.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox4.Location = New System.Drawing.Point(614, 151)
        Me.CMBTextBox4.Name = "CMBTextBox4"
        Me.CMBTextBox4.Size = New System.Drawing.Size(359, 19)
        Me.CMBTextBox4.TabIndex = 176
        Me.CMBTextBox4.TabStop = False
        Me.CMBTextBox4.Text = "Fechas y Hora de"
        Me.CMBTextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.rbEnProceso)
        Me.Panel2.Controls.Add(Me.RadioButton3)
        Me.Panel2.Controls.Add(Me.RadioButton2)
        Me.Panel2.Controls.Add(Me.RadioButton1)
        Me.Panel2.Controls.Add(Label1)
        Me.Panel2.Location = New System.Drawing.Point(598, 28)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(375, 67)
        Me.Panel2.TabIndex = 175
        '
        'rbEnProceso
        '
        Me.rbEnProceso.AutoSize = True
        Me.rbEnProceso.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.rbEnProceso.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbEnProceso.ForeColor = System.Drawing.Color.DarkOrange
        Me.rbEnProceso.Location = New System.Drawing.Point(226, 39)
        Me.rbEnProceso.Name = "rbEnProceso"
        Me.rbEnProceso.Size = New System.Drawing.Size(114, 22)
        Me.rbEnProceso.TabIndex = 18
        Me.rbEnProceso.TabStop = True
        Me.rbEnProceso.Text = "En Proceso"
        Me.rbEnProceso.UseVisualStyleBackColor = True
        '
        'RadioButton3
        '
        Me.RadioButton3.AutoSize = True
        Me.RadioButton3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton3.ForeColor = System.Drawing.Color.DarkOrange
        Me.RadioButton3.Location = New System.Drawing.Point(108, 39)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(66, 22)
        Me.RadioButton3.TabIndex = 17
        Me.RadioButton3.Text = "Visita"
        Me.RadioButton3.UseVisualStyleBackColor = True
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton2.ForeColor = System.Drawing.Color.DarkOrange
        Me.RadioButton2.Location = New System.Drawing.Point(226, 7)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(99, 22)
        Me.RadioButton2.TabIndex = 16
        Me.RadioButton2.Text = "Ejecutada"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Checked = True
        Me.RadioButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton1.ForeColor = System.Drawing.Color.DarkOrange
        Me.RadioButton1.Location = New System.Drawing.Point(108, 7)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(99, 22)
        Me.RadioButton1.TabIndex = 15
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Pendiente"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.DarkOrange
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.TreeView1)
        Me.GroupBox1.Controls.Add(ServicioLabel)
        Me.GroupBox1.Controls.Add(Me.ESHOTELCheckBox)
        Me.GroupBox1.Controls.Add(SOLOINTERNETLabel1)
        Me.GroupBox1.Controls.Add(Me.SOLOINTERNETCheckBox)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.NOMBRELabel1)
        Me.GroupBox1.Controls.Add(Me.CALLELabel1)
        Me.GroupBox1.Controls.Add(Me.COLONIALabel1)
        Me.GroupBox1.Controls.Add(Me.NUMEROLabel1)
        Me.GroupBox1.Controls.Add(Me.CIUDADLabel1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.White
        Me.GroupBox1.Location = New System.Drawing.Point(24, 133)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(536, 183)
        Me.GroupBox1.TabIndex = 174
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Cliente"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(427, 73)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(67, 15)
        Me.Label6.TabIndex = 20
        Me.Label6.Text = "Es hotel :"
        '
        'TreeView1
        '
        Me.TreeView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView1.Location = New System.Drawing.Point(166, 97)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(358, 80)
        Me.TreeView1.TabIndex = 19
        Me.TreeView1.TabStop = False
        '
        'ESHOTELCheckBox
        '
        Me.ESHOTELCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ESHOTELCheckBox.Location = New System.Drawing.Point(503, 67)
        Me.ESHOTELCheckBox.Name = "ESHOTELCheckBox"
        Me.ESHOTELCheckBox.Size = New System.Drawing.Size(21, 24)
        Me.ESHOTELCheckBox.TabIndex = 18
        Me.ESHOTELCheckBox.TabStop = False
        '
        'SOLOINTERNETCheckBox
        '
        Me.SOLOINTERNETCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.SOLOINTERNETCheckBox.Location = New System.Drawing.Point(403, 67)
        Me.SOLOINTERNETCheckBox.Name = "SOLOINTERNETCheckBox"
        Me.SOLOINTERNETCheckBox.Size = New System.Drawing.Size(21, 24)
        Me.SOLOINTERNETCheckBox.TabIndex = 17
        Me.SOLOINTERNETCheckBox.TabStop = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(378, 41)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(31, 13)
        Me.Label11.TabIndex = 16
        Me.Label11.Text = "Col. :"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(299, 41)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(20, 13)
        Me.Label10.TabIndex = 15
        Me.Label10.Text = "# :"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(9, 41)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(58, 13)
        Me.Label9.TabIndex = 14
        Me.Label9.Text = "Dirección :"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(10, 18)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(50, 13)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Nombre :"
        '
        'NOMBRELabel1
        '
        Me.NOMBRELabel1.AutoEllipsis = True
        Me.NOMBRELabel1.BackColor = System.Drawing.Color.DarkOrange
        Me.NOMBRELabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NOMBRELabel1.Location = New System.Drawing.Point(69, 18)
        Me.NOMBRELabel1.Name = "NOMBRELabel1"
        Me.NOMBRELabel1.Size = New System.Drawing.Size(467, 23)
        Me.NOMBRELabel1.TabIndex = 3
        '
        'CALLELabel1
        '
        Me.CALLELabel1.AutoEllipsis = True
        Me.CALLELabel1.BackColor = System.Drawing.Color.DarkOrange
        Me.CALLELabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CALLELabel1.Location = New System.Drawing.Point(69, 41)
        Me.CALLELabel1.Name = "CALLELabel1"
        Me.CALLELabel1.Size = New System.Drawing.Size(224, 23)
        Me.CALLELabel1.TabIndex = 5
        '
        'COLONIALabel1
        '
        Me.COLONIALabel1.AutoEllipsis = True
        Me.COLONIALabel1.BackColor = System.Drawing.Color.DarkOrange
        Me.COLONIALabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.COLONIALabel1.Location = New System.Drawing.Point(415, 41)
        Me.COLONIALabel1.Name = "COLONIALabel1"
        Me.COLONIALabel1.Size = New System.Drawing.Size(121, 23)
        Me.COLONIALabel1.TabIndex = 7
        '
        'NUMEROLabel1
        '
        Me.NUMEROLabel1.AutoEllipsis = True
        Me.NUMEROLabel1.BackColor = System.Drawing.Color.DarkOrange
        Me.NUMEROLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NUMEROLabel1.Location = New System.Drawing.Point(325, 41)
        Me.NUMEROLabel1.Name = "NUMEROLabel1"
        Me.NUMEROLabel1.Size = New System.Drawing.Size(47, 23)
        Me.NUMEROLabel1.TabIndex = 9
        '
        'CIUDADLabel1
        '
        Me.CIUDADLabel1.AutoEllipsis = True
        Me.CIUDADLabel1.BackColor = System.Drawing.Color.DarkOrange
        Me.CIUDADLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CIUDADLabel1.Location = New System.Drawing.Point(69, 65)
        Me.CIUDADLabel1.Name = "CIUDADLabel1"
        Me.CIUDADLabel1.Size = New System.Drawing.Size(224, 23)
        Me.CIUDADLabel1.TabIndex = 11
        '
        'CONQUEJASBindingNavigator
        '
        Me.CONQUEJASBindingNavigator.AddNewItem = Nothing
        Me.CONQUEJASBindingNavigator.BindingSource = Me.CONQUEJASBindingSource
        Me.CONQUEJASBindingNavigator.CountItem = Nothing
        Me.CONQUEJASBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONQUEJASBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.CONQUEJASBindingNavigatorSaveItem, Me.BindingNavigatorDeleteItem})
        Me.CONQUEJASBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONQUEJASBindingNavigator.MoveFirstItem = Nothing
        Me.CONQUEJASBindingNavigator.MoveLastItem = Nothing
        Me.CONQUEJASBindingNavigator.MoveNextItem = Nothing
        Me.CONQUEJASBindingNavigator.MovePreviousItem = Nothing
        Me.CONQUEJASBindingNavigator.Name = "CONQUEJASBindingNavigator"
        Me.CONQUEJASBindingNavigator.PositionItem = Nothing
        Me.CONQUEJASBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONQUEJASBindingNavigator.Size = New System.Drawing.Size(993, 25)
        Me.CONQUEJASBindingNavigator.TabIndex = 9
        Me.CONQUEJASBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(90, 22)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        Me.BindingNavigatorDeleteItem.Visible = False
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(79, 22)
        Me.ToolStripButton1.Text = "&CANCELAR"
        '
        'CONQUEJASBindingNavigatorSaveItem
        '
        Me.CONQUEJASBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONQUEJASBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONQUEJASBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONQUEJASBindingNavigatorSaveItem.Name = "CONQUEJASBindingNavigatorSaveItem"
        Me.CONQUEJASBindingNavigatorSaveItem.Size = New System.Drawing.Size(91, 22)
        Me.CONQUEJASBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'ContratoTextBox
        '
        Me.ContratoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ContratoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONQUEJASBindingSource, "Contrato", True))
        Me.ContratoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContratoTextBox.Location = New System.Drawing.Point(94, 108)
        Me.ContratoTextBox.Name = "ContratoTextBox"
        Me.ContratoTextBox.Size = New System.Drawing.Size(106, 21)
        Me.ContratoTextBox.TabIndex = 0
        '
        'ClasificacionTextBox
        '
        Me.ClasificacionTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClasificacionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.ClasificacionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONQUEJASBindingSource, "Clasificacion", True))
        Me.ClasificacionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClasificacionTextBox.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.ClasificacionTextBox.Location = New System.Drawing.Point(454, 277)
        Me.ClasificacionTextBox.Name = "ClasificacionTextBox"
        Me.ClasificacionTextBox.Size = New System.Drawing.Size(21, 14)
        Me.ClasificacionTextBox.TabIndex = 290
        Me.ClasificacionTextBox.TabStop = False
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox1.Location = New System.Drawing.Point(212, 122)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(24, 14)
        Me.TextBox1.TabIndex = 150
        Me.TextBox1.TabStop = False
        Me.TextBox1.Text = "P"
        '
        'StatusTextBox
        '
        Me.StatusTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONQUEJASBindingSource, "Status", True))
        Me.StatusTextBox.Location = New System.Drawing.Point(165, 81)
        Me.StatusTextBox.Name = "StatusTextBox"
        Me.StatusTextBox.Size = New System.Drawing.Size(21, 20)
        Me.StatusTextBox.TabIndex = 417
        Me.StatusTextBox.TabStop = False
        '
        'Visita2MaskedTextBox1
        '
        Me.Visita2MaskedTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONQUEJASBindingSource, "Visita2", True))
        Me.Visita2MaskedTextBox1.Location = New System.Drawing.Point(670, 261)
        Me.Visita2MaskedTextBox1.Name = "Visita2MaskedTextBox1"
        Me.Visita2MaskedTextBox1.Size = New System.Drawing.Size(100, 20)
        Me.Visita2MaskedTextBox1.TabIndex = 419
        Me.Visita2MaskedTextBox1.TabStop = False
        '
        'Visita1MaskedTextBox1
        '
        Me.Visita1MaskedTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONQUEJASBindingSource, "Visita1", True))
        Me.Visita1MaskedTextBox1.Location = New System.Drawing.Point(670, 234)
        Me.Visita1MaskedTextBox1.Name = "Visita1MaskedTextBox1"
        Me.Visita1MaskedTextBox1.Size = New System.Drawing.Size(100, 20)
        Me.Visita1MaskedTextBox1.TabIndex = 418
        Me.Visita1MaskedTextBox1.TabStop = False
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.ForeColor = System.Drawing.Color.Red
        Me.TextBox2.Location = New System.Drawing.Point(181, 50)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(280, 19)
        Me.TextBox2.TabIndex = 404
        Me.TextBox2.TabStop = False
        Me.TextBox2.Visible = False
        '
        'cmbPrioridadQueja
        '
        Me.cmbPrioridadQueja.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONQUEJASBindingSource, "Clave_Tecnico", True))
        Me.cmbPrioridadQueja.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmbPrioridadQueja.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbPrioridadQueja.FormattingEnabled = True
        Me.cmbPrioridadQueja.Location = New System.Drawing.Point(142, 39)
        Me.cmbPrioridadQueja.Name = "cmbPrioridadQueja"
        Me.cmbPrioridadQueja.Size = New System.Drawing.Size(383, 23)
        Me.cmbPrioridadQueja.TabIndex = 458
        '
        'BUSCLIPORCONTRATOBindingSource
        '
        Me.BUSCLIPORCONTRATOBindingSource.DataMember = "BUSCLIPORCONTRATO"
        Me.BUSCLIPORCONTRATOBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'DamefechahoraservBindingSource
        '
        Me.DamefechahoraservBindingSource.DataMember = "Dame_fecha_hora_serv"
        Me.DamefechahoraservBindingSource.DataSource = Me.DataSetarnoldo
        '
        'DataSetarnoldo
        '
        Me.DataSetarnoldo.DataSetName = "DataSetarnoldo"
        Me.DataSetarnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TextBox3
        '
        Me.TextBox3.AllowDrop = True
        Me.TextBox3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox3.Enabled = False
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(531, 47)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(100, 14)
        Me.TextBox3.TabIndex = 434
        Me.TextBox3.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(555, 12)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(55, 16)
        Me.Label3.TabIndex = 433
        Me.Label3.Text = "Label3"
        Me.Label3.Visible = False
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(5, 2)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(191, 33)
        Me.Button3.TabIndex = 432
        Me.Button3.TabStop = False
        Me.Button3.Text = "&Descarga de Material."
        Me.Button3.UseVisualStyleBackColor = True
        '
        'SolucionTextBox
        '
        Me.SolucionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SolucionTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.SolucionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONQUEJASBindingSource, "Solucion", True))
        Me.SolucionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SolucionTextBox.Location = New System.Drawing.Point(120, 5)
        Me.SolucionTextBox.Multiline = True
        Me.SolucionTextBox.Name = "SolucionTextBox"
        Me.SolucionTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.SolucionTextBox.Size = New System.Drawing.Size(783, 52)
        Me.SolucionTextBox.TabIndex = 7
        '
        'ProblemaTextBox
        '
        Me.ProblemaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ProblemaTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ProblemaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONQUEJASBindingSource, "Problema", True))
        Me.ProblemaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.ProblemaTextBox.Location = New System.Drawing.Point(123, 67)
        Me.ProblemaTextBox.Multiline = True
        Me.ProblemaTextBox.Name = "ProblemaTextBox"
        Me.ProblemaTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.ProblemaTextBox.Size = New System.Drawing.Size(783, 52)
        Me.ProblemaTextBox.TabIndex = 6
        '
        'ComboBox2
        '
        Me.ComboBox2.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONQUEJASBindingSource, "Clave_Tecnico", True))
        Me.ComboBox2.DataSource = Me.MuestraTecnicosQuejasbuenoBindingSource
        Me.ComboBox2.DisplayMember = "Tecnico"
        Me.ComboBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(139, 2)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(383, 23)
        Me.ComboBox2.TabIndex = 3
        Me.ComboBox2.ValueMember = "clv_Tecnico"
        '
        'MuestraTecnicosQuejasbuenoBindingSource
        '
        Me.MuestraTecnicosQuejasbuenoBindingSource.DataMember = "Muestra_Tecnicos_Quejasbueno"
        Me.MuestraTecnicosQuejasbuenoBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Procedimientosarnoldo4
        '
        Me.Procedimientosarnoldo4.DataSetName = "Procedimientosarnoldo4"
        Me.Procedimientosarnoldo4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MuestraTecnicosAlmacenBindingSource
        '
        Me.MuestraTecnicosAlmacenBindingSource.DataMember = "Muestra_Tecnicos_Almacen"
        Me.MuestraTecnicosAlmacenBindingSource.DataSource = Me.DataSetarnoldo
        '
        'ObservacionesTextBoxX
        '
        Me.ObservacionesTextBoxX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ObservacionesTextBoxX.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ObservacionesTextBoxX.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONQUEJASBindingSource, "Observaciones", True))
        Me.ObservacionesTextBoxX.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.ObservacionesTextBoxX.Location = New System.Drawing.Point(125, 228)
        Me.ObservacionesTextBoxX.MaxLength = 250
        Me.ObservacionesTextBoxX.Multiline = True
        Me.ObservacionesTextBoxX.Name = "ObservacionesTextBoxX"
        Me.ObservacionesTextBoxX.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.ObservacionesTextBoxX.Size = New System.Drawing.Size(783, 48)
        Me.ObservacionesTextBoxX.TabIndex = 8
        '
        'ComboBox4
        '
        Me.ComboBox4.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONQUEJASBindingSource, "Clv_Trabajo", True))
        Me.ComboBox4.DataSource = Me.MUESTRATRABAJOSQUEJASBindingSource
        Me.ComboBox4.DisplayMember = "Descripcion"
        Me.ComboBox4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox4.FormattingEnabled = True
        Me.ComboBox4.Location = New System.Drawing.Point(139, 3)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(307, 23)
        Me.ComboBox4.TabIndex = 4
        Me.ComboBox4.ValueMember = "Clv_Trabajo"
        '
        'MUESTRATRABAJOSQUEJASBindingSource
        '
        Me.MUESTRATRABAJOSQUEJASBindingSource.DataMember = "MUESTRATRABAJOSQUEJAS"
        Me.MUESTRATRABAJOSQUEJASBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'ComboBox1
        '
        Me.ComboBox1.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONQUEJASBindingSource, "Clasificacion", True))
        Me.ComboBox1.DataSource = Me.MUESTRACLASIFICACIONQUEJASBindingSource
        Me.ComboBox1.DisplayMember = "Concepto"
        Me.ComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(206, 3)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(307, 23)
        Me.ComboBox1.TabIndex = 2
        Me.ComboBox1.ValueMember = "Clave"
        '
        'MUESTRACLASIFICACIONQUEJASBindingSource
        '
        Me.MUESTRACLASIFICACIONQUEJASBindingSource.DataMember = "MUESTRACLASIFICACIONQUEJAS"
        Me.MUESTRACLASIFICACIONQUEJASBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'Clv_TrabajoTextBox
        '
        Me.Clv_TrabajoTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TrabajoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_TrabajoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONQUEJASBindingSource, "Clv_Trabajo", True))
        Me.Clv_TrabajoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_TrabajoTextBox.ForeColor = System.Drawing.Color.DarkRed
        Me.Clv_TrabajoTextBox.Location = New System.Drawing.Point(403, 6)
        Me.Clv_TrabajoTextBox.Name = "Clv_TrabajoTextBox"
        Me.Clv_TrabajoTextBox.Size = New System.Drawing.Size(23, 14)
        Me.Clv_TrabajoTextBox.TabIndex = 400
        Me.Clv_TrabajoTextBox.TabStop = False
        '
        'Clave_TecnicoTextBox
        '
        Me.Clave_TecnicoTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Clave_TecnicoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clave_TecnicoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONQUEJASBindingSource, "Clave_Tecnico", True))
        Me.Clave_TecnicoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clave_TecnicoTextBox.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Clave_TecnicoTextBox.Location = New System.Drawing.Point(387, -28)
        Me.Clave_TecnicoTextBox.Name = "Clave_TecnicoTextBox"
        Me.Clave_TecnicoTextBox.Size = New System.Drawing.Size(21, 14)
        Me.Clave_TecnicoTextBox.TabIndex = 150
        Me.Clave_TecnicoTextBox.TabStop = False
        '
        'FechaDateTimePicker
        '
        Me.FechaDateTimePicker.CustomFormat = "dd/MMM/yyyy HH:ss"
        Me.FechaDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.DamefechahoraservBindingSource, "Fecha", True))
        Me.FechaDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FechaDateTimePicker.Location = New System.Drawing.Point(233, 203)
        Me.FechaDateTimePicker.Name = "FechaDateTimePicker"
        Me.FechaDateTimePicker.Size = New System.Drawing.Size(189, 20)
        Me.FechaDateTimePicker.TabIndex = 5
        Me.FechaDateTimePicker.TabStop = False
        Me.FechaDateTimePicker.Value = New Date(2007, 12, 10, 0, 0, 0, 0)
        '
        'DameFechadelServidorHoraBindingSource
        '
        Me.DameFechadelServidorHoraBindingSource.DataMember = "DameFechadelServidorHora"
        Me.DameFechadelServidorHoraBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRATECNICOSBindingSource
        '
        Me.MUESTRATECNICOSBindingSource.DataMember = "MUESTRATECNICOS"
        Me.MUESTRATECNICOSBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'DameUltimo_dia_del_MesBindingSource
        '
        Me.DameUltimo_dia_del_MesBindingSource.DataMember = "DameUltimo_dia_del_Mes"
        Me.DameUltimo_dia_del_MesBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRATRABAJOSBindingSource
        '
        Me.MUESTRATRABAJOSBindingSource.DataMember = "MUESTRATRABAJOS"
        Me.MUESTRATRABAJOSBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(872, 722)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 10
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'CONQUEJASTableAdapter
        '
        Me.CONQUEJASTableAdapter.ClearBeforeFill = True
        '
        'MUESTRACLASIFICACIONQUEJASTableAdapter
        '
        Me.MUESTRACLASIFICACIONQUEJASTableAdapter.ClearBeforeFill = True
        '
        'MUESTRATECNICOSTableAdapter
        '
        Me.MUESTRATECNICOSTableAdapter.ClearBeforeFill = True
        '
        'MUESTRATRABAJOSTableAdapter
        '
        Me.MUESTRATRABAJOSTableAdapter.ClearBeforeFill = True
        '
        'DameSerDELCliBindingSource
        '
        Me.DameSerDELCliBindingSource.DataMember = "dameSerDELCli"
        Me.DameSerDELCliBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'DameSerDELCliTableAdapter
        '
        Me.DameSerDELCliTableAdapter.ClearBeforeFill = True
        '
        'BUSCLIPORCONTRATOTableAdapter
        '
        Me.BUSCLIPORCONTRATOTableAdapter.ClearBeforeFill = True
        '
        'NUE_CITASTableAdapter
        '
        Me.NUE_CITASTableAdapter.ClearBeforeFill = True
        '
        'NUE_CITASBindingSource
        '
        Me.NUE_CITASBindingSource.DataMember = "NUE_CITAS"
        Me.NUE_CITASBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CONDetCitasTableAdapter
        '
        Me.CONDetCitasTableAdapter.ClearBeforeFill = True
        '
        'CONDetCitasBindingSource
        '
        Me.CONDetCitasBindingSource.DataMember = "CONDetCitas"
        Me.CONDetCitasBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NUEREL_CITAS_QUEJASBindingSource
        '
        Me.NUEREL_CITAS_QUEJASBindingSource.DataMember = "NUEREL_CITAS_QUEJAS"
        Me.NUEREL_CITAS_QUEJASBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NUEREL_CITAS_QUEJASTableAdapter
        '
        Me.NUEREL_CITAS_QUEJASTableAdapter.ClearBeforeFill = True
        '
        'DameUltimo_dia_del_MesTableAdapter
        '
        Me.DameUltimo_dia_del_MesTableAdapter.ClearBeforeFill = True
        '
        'MUESTRATRABAJOSQUEJASTableAdapter
        '
        Me.MUESTRATRABAJOSQUEJASTableAdapter.ClearBeforeFill = True
        '
        'MuestraTipSerPrincipalTableAdapter
        '
        Me.MuestraTipSerPrincipalTableAdapter.ClearBeforeFill = True
        '
        'DAMEFECHADELSERVIDORBindingSource
        '
        Me.DAMEFECHADELSERVIDORBindingSource.DataMember = "DAMEFECHADELSERVIDOR"
        Me.DAMEFECHADELSERVIDORBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'DAMEFECHADELSERVIDORTableAdapter
        '
        Me.DAMEFECHADELSERVIDORTableAdapter.ClearBeforeFill = True
        '
        'DameFechadelServidorHoraTableAdapter
        '
        Me.DameFechadelServidorHoraTableAdapter.ClearBeforeFill = True
        '
        'Muestra_Tecnicos_AlmacenTableAdapter
        '
        Me.Muestra_Tecnicos_AlmacenTableAdapter.ClearBeforeFill = True
        '
        'Inserta_DetBitTecBindingSource
        '
        Me.Inserta_DetBitTecBindingSource.DataMember = "Inserta_DetBitTec"
        Me.Inserta_DetBitTecBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Inserta_DetBitTecTableAdapter
        '
        Me.Inserta_DetBitTecTableAdapter.ClearBeforeFill = True
        '
        'DameClv_Session_TecnicosBindingSource
        '
        Me.DameClv_Session_TecnicosBindingSource.DataMember = "DameClv_Session_Tecnicos"
        Me.DameClv_Session_TecnicosBindingSource.DataSource = Me.DataSetarnoldo
        '
        'DameClv_Session_TecnicosTableAdapter
        '
        Me.DameClv_Session_TecnicosTableAdapter.ClearBeforeFill = True
        '
        'Muestra_no_quejaBindingSource
        '
        Me.Muestra_no_quejaBindingSource.DataMember = "Muestra_no_queja"
        Me.Muestra_no_quejaBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Muestra_no_quejaTableAdapter
        '
        Me.Muestra_no_quejaTableAdapter.ClearBeforeFill = True
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel3.Controls.Add(Me.ComboBox1)
        Me.Panel3.Controls.Add(ClasificacionLabel)
        Me.Panel3.Location = New System.Drawing.Point(2, 318)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(548, 28)
        Me.Panel3.TabIndex = 2
        Me.Panel3.TabStop = True
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel4.Controls.Add(Me.Button3)
        Me.Panel4.Location = New System.Drawing.Point(695, 363)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(199, 38)
        Me.Panel4.TabIndex = 436
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel5.Controls.Add(Me.lblObsAgendaMostrar)
        Me.Panel5.Controls.Add(Me.lblObsAgenda)
        Me.Panel5.Controls.Add(Me.cmbClasifProblemas)
        Me.Panel5.Controls.Add(lblClasifProblema)
        Me.Panel5.Controls.Add(Me.ObservacionesTextBox)
        Me.Panel5.Controls.Add(Me.cmbPrioridadQueja)
        Me.Panel5.Controls.Add(Me.Panel9)
        Me.Panel5.Controls.Add(Label12)
        Me.Panel5.Controls.Add(Me.Panel7)
        Me.Panel5.Controls.Add(Me.TextBox3)
        Me.Panel5.Controls.Add(Me.ProblemaTextBox)
        Me.Panel5.Controls.Add(Me.Clave_TecnicoTextBox)
        Me.Panel5.Controls.Add(Me.Label3)
        Me.Panel5.Controls.Add(ProblemaLabel)
        Me.Panel5.Controls.Add(ObservacionesLabel)
        Me.Panel5.Controls.Add(Me.ObservacionesTextBoxX)
        Me.Panel5.Controls.Add(Me.FechaDateTimePicker)
        Me.Panel5.Location = New System.Drawing.Point(2, 412)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(995, 304)
        Me.Panel5.TabIndex = 3
        Me.Panel5.TabStop = True
        '
        'lblObsAgendaMostrar
        '
        Me.lblObsAgendaMostrar.BackColor = System.Drawing.Color.Yellow
        Me.lblObsAgendaMostrar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblObsAgendaMostrar.ForeColor = System.Drawing.Color.Red
        Me.lblObsAgendaMostrar.Location = New System.Drawing.Point(124, 281)
        Me.lblObsAgendaMostrar.Name = "lblObsAgendaMostrar"
        Me.lblObsAgendaMostrar.Size = New System.Drawing.Size(868, 13)
        Me.lblObsAgendaMostrar.TabIndex = 462
        '
        'lblObsAgenda
        '
        Me.lblObsAgenda.AutoSize = True
        Me.lblObsAgenda.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblObsAgenda.ForeColor = System.Drawing.Color.SlateGray
        Me.lblObsAgenda.Location = New System.Drawing.Point(38, 281)
        Me.lblObsAgenda.Name = "lblObsAgenda"
        Me.lblObsAgenda.Size = New System.Drawing.Size(80, 13)
        Me.lblObsAgenda.TabIndex = 439
        Me.lblObsAgenda.Text = "Obs Agenda:"
        '
        'cmbClasifProblemas
        '
        Me.cmbClasifProblemas.DisplayMember = "descripcion"
        Me.cmbClasifProblemas.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmbClasifProblemas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbClasifProblemas.FormattingEnabled = True
        Me.cmbClasifProblemas.Location = New System.Drawing.Point(124, 127)
        Me.cmbClasifProblemas.Name = "cmbClasifProblemas"
        Me.cmbClasifProblemas.Size = New System.Drawing.Size(422, 23)
        Me.cmbClasifProblemas.TabIndex = 461
        Me.cmbClasifProblemas.ValueMember = "clvProblema"
        '
        'ObservacionesTextBox
        '
        Me.ObservacionesTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONQUEJASBindingSource, "Observaciones", True))
        Me.ObservacionesTextBox.Location = New System.Drawing.Point(124, 228)
        Me.ObservacionesTextBox.Name = "ObservacionesTextBox"
        Me.ObservacionesTextBox.Size = New System.Drawing.Size(784, 48)
        Me.ObservacionesTextBox.TabIndex = 459
        Me.ObservacionesTextBox.Text = ""
        '
        'Panel9
        '
        Me.Panel9.Controls.Add(Me.SolucionTextBox)
        Me.Panel9.Controls.Add(SolucionLabel)
        Me.Panel9.Location = New System.Drawing.Point(3, 156)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(921, 67)
        Me.Panel9.TabIndex = 437
        '
        'Panel7
        '
        Me.Panel7.Controls.Add(Me.ComboBox4)
        Me.Panel7.Controls.Add(Tipo_TrabajoLabel)
        Me.Panel7.Controls.Add(Me.Clv_TipoQuejaTextBox)
        Me.Panel7.Controls.Add(Me.Clv_TrabajoTextBox)
        Me.Panel7.Location = New System.Drawing.Point(3, 4)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(545, 29)
        Me.Panel7.TabIndex = 435
        '
        'Clv_TipoQuejaTextBox
        '
        Me.Clv_TipoQuejaTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TipoQuejaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_TipoQuejaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONQUEJASBindingSource, "Clv_TipoQueja", True))
        Me.Clv_TipoQuejaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_TipoQuejaTextBox.ForeColor = System.Drawing.Color.Red
        Me.Clv_TipoQuejaTextBox.Location = New System.Drawing.Point(398, 6)
        Me.Clv_TipoQuejaTextBox.Name = "Clv_TipoQuejaTextBox"
        Me.Clv_TipoQuejaTextBox.Size = New System.Drawing.Size(21, 14)
        Me.Clv_TipoQuejaTextBox.TabIndex = 250
        Me.Clv_TipoQuejaTextBox.TabStop = False
        '
        'Panel8
        '
        Me.Panel8.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel8.Controls.Add(Label7)
        Me.Panel8.Controls.Add(Me.cbCuadrilla)
        Me.Panel8.Controls.Add(Me.Panel10)
        Me.Panel8.Location = New System.Drawing.Point(2, 350)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(578, 60)
        Me.Panel8.TabIndex = 436
        '
        'cbCuadrilla
        '
        Me.cbCuadrilla.DisplayMember = "Tecnico"
        Me.cbCuadrilla.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cbCuadrilla.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbCuadrilla.FormattingEnabled = True
        Me.cbCuadrilla.Location = New System.Drawing.Point(142, 34)
        Me.cbCuadrilla.Name = "cbCuadrilla"
        Me.cbCuadrilla.Size = New System.Drawing.Size(383, 23)
        Me.cbCuadrilla.TabIndex = 15
        Me.cbCuadrilla.ValueMember = "clv_Tecnico"
        '
        'Panel10
        '
        Me.Panel10.Controls.Add(Me.ComboBox2)
        Me.Panel10.Controls.Add(Clave_TecnicoLabel)
        Me.Panel10.Location = New System.Drawing.Point(3, 3)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(545, 27)
        Me.Panel10.TabIndex = 15
        '
        'Dame_Impresora_OrdenesBindingSource
        '
        Me.Dame_Impresora_OrdenesBindingSource.DataMember = "Dame_Impresora_Ordenes"
        Me.Dame_Impresora_OrdenesBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Dame_Impresora_OrdenesTableAdapter
        '
        Me.Dame_Impresora_OrdenesTableAdapter.ClearBeforeFill = True
        '
        'DataSetLidia
        '
        Me.DataSetLidia.DataSetName = "DataSetLidia"
        Me.DataSetLidia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Dame_Folio_QBindingSource
        '
        Me.Dame_Folio_QBindingSource.DataMember = "Dame_Folio_Q"
        Me.Dame_Folio_QBindingSource.DataSource = Me.DataSetLidia
        '
        'Dame_Folio_QTableAdapter
        '
        Me.Dame_Folio_QTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Bitacora_tec_QBindingSource
        '
        Me.Inserta_Bitacora_tec_QBindingSource.DataMember = "Inserta_Bitacora_tec_Q"
        Me.Inserta_Bitacora_tec_QBindingSource.DataSource = Me.DataSetLidia
        '
        'Inserta_Bitacora_tec_QTableAdapter
        '
        Me.Inserta_Bitacora_tec_QTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Rel_Bitacora_QuejaBindingSource
        '
        Me.Inserta_Rel_Bitacora_QuejaBindingSource.DataMember = "Inserta_Rel_Bitacora_Queja"
        Me.Inserta_Rel_Bitacora_QuejaBindingSource.DataSource = Me.DataSetLidia
        '
        'Inserta_Rel_Bitacora_QuejaTableAdapter
        '
        Me.Inserta_Rel_Bitacora_QuejaTableAdapter.ClearBeforeFill = True
        '
        'Dame_fecha_hora_servTableAdapter
        '
        Me.Dame_fecha_hora_servTableAdapter.ClearBeforeFill = True
        '
        'TextBox4
        '
        Me.TextBox4.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DamefechahoraservBindingSource, "Fecha", True))
        Me.TextBox4.Location = New System.Drawing.Point(39, 800)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(191, 20)
        Me.TextBox4.TabIndex = 438
        Me.TextBox4.TabStop = False
        '
        'BuscaBloqueadoBindingSource
        '
        Me.BuscaBloqueadoBindingSource.DataMember = "BuscaBloqueado"
        Me.BuscaBloqueadoBindingSource.DataSource = Me.DataSetLidia
        '
        'BuscaBloqueadoTableAdapter
        '
        Me.BuscaBloqueadoTableAdapter.ClearBeforeFill = True
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Inserta_RelCobraDescBindingSource
        '
        Me.Inserta_RelCobraDescBindingSource.DataMember = "Inserta_RelCobraDesc"
        Me.Inserta_RelCobraDescBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Inserta_RelCobraDescTableAdapter
        '
        Me.Inserta_RelCobraDescTableAdapter.ClearBeforeFill = True
        '
        'Timer1
        '
        Me.Timer1.Interval = 500
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'NueRelQuejaUsuarioBindingSource
        '
        Me.NueRelQuejaUsuarioBindingSource.DataMember = "NueRelQuejaUsuario"
        Me.NueRelQuejaUsuarioBindingSource.DataSource = Me.DataSetEric
        '
        'NueRelQuejaUsuarioTableAdapter
        '
        Me.NueRelQuejaUsuarioTableAdapter.ClearBeforeFill = True
        '
        'Consulta_RelQuejaUsuarioTableAdapter
        '
        Me.Consulta_RelQuejaUsuarioTableAdapter.ClearBeforeFill = True
        '
        'Muestra_Tecnicos_QuejasbuenoTableAdapter
        '
        Me.Muestra_Tecnicos_QuejasbuenoTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter5
        '
        Me.Muestra_ServiciosDigitalesTableAdapter5.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter6
        '
        Me.Muestra_ServiciosDigitalesTableAdapter6.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter7
        '
        Me.Muestra_ServiciosDigitalesTableAdapter7.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter8
        '
        Me.Muestra_ServiciosDigitalesTableAdapter8.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter9
        '
        Me.Muestra_ServiciosDigitalesTableAdapter9.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter10
        '
        Me.Muestra_ServiciosDigitalesTableAdapter10.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter11
        '
        Me.Muestra_ServiciosDigitalesTableAdapter11.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter12
        '
        Me.Muestra_ServiciosDigitalesTableAdapter12.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter13
        '
        Me.Muestra_ServiciosDigitalesTableAdapter13.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter14
        '
        Me.Muestra_ServiciosDigitalesTableAdapter14.ClearBeforeFill = True
        '
        'Panel12
        '
        Me.Panel12.Controls.Add(Me.TxtRefOxxo)
        Me.Panel12.Controls.Add(Me.Label19)
        Me.Panel12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel12.Location = New System.Drawing.Point(255, 103)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(305, 27)
        Me.Panel12.TabIndex = 462
        '
        'TxtRefOxxo
        '
        Me.TxtRefOxxo.Location = New System.Drawing.Point(112, 2)
        Me.TxtRefOxxo.Name = "TxtRefOxxo"
        Me.TxtRefOxxo.Size = New System.Drawing.Size(162, 21)
        Me.TxtRefOxxo.TabIndex = 1
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label19.Location = New System.Drawing.Point(29, 5)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(77, 15)
        Me.Label19.TabIndex = 0
        Me.Label19.Text = "Ref. Oxxo :"
        '
        'FrmQueja
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1016, 763)
        Me.Controls.Add(Me.TextBox4)
        Me.Controls.Add(Me.Panel8)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Panel1)
        Me.MaximizeBox = False
        Me.Name = "FrmQueja"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Quejas"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.Consulta_RelQuejaUsuarioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONQUEJASBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTipSerPrincipalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.CONQUEJASBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONQUEJASBindingNavigator.ResumeLayout(False)
        Me.CONQUEJASBindingNavigator.PerformLayout()
        CType(Me.BUSCLIPORCONTRATOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DamefechahoraservBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTecnicosQuejasbuenoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTecnicosAlmacenBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRATRABAJOSQUEJASBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACLASIFICACIONQUEJASBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameFechadelServidorHoraBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRATECNICOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameUltimo_dia_del_MesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRATRABAJOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameSerDELCliBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NUE_CITASBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONDetCitasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NUEREL_CITAS_QUEJASBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAMEFECHADELSERVIDORBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_DetBitTecBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameClv_Session_TecnicosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Muestra_no_quejaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        CType(Me.Dame_Impresora_OrdenesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_Folio_QBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Bitacora_tec_QBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Rel_Bitacora_QuejaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BuscaBloqueadoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_RelCobraDescBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NueRelQuejaUsuarioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel12.ResumeLayout(False)
        Me.Panel12.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONQUEJASBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONQUEJASTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONQUEJASTableAdapter
    Friend WithEvents Clv_QuejaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ContratoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ObservacionesTextBoxX As System.Windows.Forms.TextBox
    Friend WithEvents Clave_TecnicoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ClasificacionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CONQUEJASBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONQUEJASBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents MUESTRACLASIFICACIONQUEJASBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACLASIFICACIONQUEJASTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACLASIFICACIONQUEJASTableAdapter
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents MUESTRATECNICOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRATECNICOSTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRATECNICOSTableAdapter
    Friend WithEvents ComboBox4 As System.Windows.Forms.ComboBox
    Friend WithEvents MUESTRATRABAJOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Clv_TrabajoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MUESTRATRABAJOSTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRATRABAJOSTableAdapter
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents ESHOTELCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents SOLOINTERNETCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents NOMBRELabel1 As System.Windows.Forms.Label
    Friend WithEvents CALLELabel1 As System.Windows.Forms.Label
    Friend WithEvents COLONIALabel1 As System.Windows.Forms.Label
    Friend WithEvents NUMEROLabel1 As System.Windows.Forms.Label
    Friend WithEvents CIUDADLabel1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents RadioButton3 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents CMBTextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents DameSerDELCliBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameSerDELCliTableAdapter As sofTV.NewSofTvDataSetTableAdapters.dameSerDELCliTableAdapter
    Friend WithEvents BUSCLIPORCONTRATOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BUSCLIPORCONTRATOTableAdapter As sofTV.NewSofTvDataSetTableAdapters.BUSCLIPORCONTRATOTableAdapter
    Friend WithEvents NUE_CITASTableAdapter As sofTV.NewSofTvDataSetTableAdapters.NUE_CITASTableAdapter
    Friend WithEvents NUE_CITASBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONDetCitasTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONDetCitasTableAdapter
    Friend WithEvents CONDetCitasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NUEREL_CITAS_QUEJASBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NUEREL_CITAS_QUEJASTableAdapter As sofTV.NewSofTvDataSetTableAdapters.NUEREL_CITAS_QUEJASTableAdapter
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents DameUltimo_dia_del_MesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameUltimo_dia_del_MesTableAdapter As sofTV.NewSofTvDataSetTableAdapters.DameUltimo_dia_del_MesTableAdapter
    Friend WithEvents FechaDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents MUESTRATRABAJOSQUEJASBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRATRABAJOSQUEJASTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRATRABAJOSQUEJASTableAdapter
    Friend WithEvents MuestraTipSerPrincipalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipSerPrincipalTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MuestraTipSerPrincipalTableAdapter
    Friend WithEvents ComboBox5 As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel7 As System.Windows.Forms.Label
    Friend WithEvents ProblemaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SolucionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DAMEFECHADELSERVIDORBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMEFECHADELSERVIDORTableAdapter As sofTV.NewSofTvDataSetTableAdapters.DAMEFECHADELSERVIDORTableAdapter
    Friend WithEvents DameFechadelServidorHoraBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameFechadelServidorHoraTableAdapter As sofTV.NewSofTvDataSetTableAdapters.DameFechadelServidorHoraTableAdapter
    Friend WithEvents Fecha_EjecucionMaskedTextBox As System.Windows.Forms.MaskedTextBox
    Friend WithEvents StatusTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Visita2MaskedTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Visita1MaskedTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Visita2MaskedTextBox As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Visita1MaskedTextBox As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Fecha_EjecucionMaskedTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents MuestraTecnicosAlmacenBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DataSetarnoldo As sofTV.DataSetarnoldo
    Friend WithEvents Muestra_Tecnicos_AlmacenTableAdapter As sofTV.DataSetarnoldoTableAdapters.Muestra_Tecnicos_AlmacenTableAdapter
    Friend WithEvents Inserta_DetBitTecBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_DetBitTecTableAdapter As sofTV.DataSetarnoldoTableAdapters.Inserta_DetBitTecTableAdapter
    Friend WithEvents DameClv_Session_TecnicosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameClv_Session_TecnicosTableAdapter As sofTV.DataSetarnoldoTableAdapters.DameClv_Session_TecnicosTableAdapter
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Muestra_no_quejaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_no_quejaTableAdapter As sofTV.DataSetarnoldoTableAdapters.Muestra_no_quejaTableAdapter
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Dame_Impresora_OrdenesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_Impresora_OrdenesTableAdapter As sofTV.DataSetarnoldoTableAdapters.Dame_Impresora_OrdenesTableAdapter
    Friend WithEvents DataSetLidia As sofTV.DataSetLidia
    Friend WithEvents Dame_Folio_QBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_Folio_QTableAdapter As sofTV.DataSetLidiaTableAdapters.Dame_Folio_QTableAdapter
    Friend WithEvents Inserta_Bitacora_tec_QBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Bitacora_tec_QTableAdapter As sofTV.DataSetLidiaTableAdapters.Inserta_Bitacora_tec_QTableAdapter
    Friend WithEvents Inserta_Rel_Bitacora_QuejaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Rel_Bitacora_QuejaTableAdapter As sofTV.DataSetLidiaTableAdapters.Inserta_Rel_Bitacora_QuejaTableAdapter
    Friend WithEvents DamefechahoraservBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_fecha_hora_servTableAdapter As sofTV.DataSetarnoldoTableAdapters.Dame_fecha_hora_servTableAdapter
    Friend WithEvents FechaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents BuscaBloqueadoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BuscaBloqueadoTableAdapter As sofTV.DataSetLidiaTableAdapters.BuscaBloqueadoTableAdapter
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents Inserta_RelCobraDescBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_RelCobraDescTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_RelCobraDescTableAdapter
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents NueRelQuejaUsuarioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NueRelQuejaUsuarioTableAdapter As sofTV.DataSetEricTableAdapters.NueRelQuejaUsuarioTableAdapter
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric2
    Friend WithEvents Consulta_RelQuejaUsuarioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_RelQuejaUsuarioTableAdapter As sofTV.DataSetEric2TableAdapters.Consulta_RelQuejaUsuarioTableAdapter
    Friend WithEvents EjecutoLabel1 As System.Windows.Forms.Label
    Friend WithEvents GeneroLabel1 As System.Windows.Forms.Label
    Friend WithEvents Clv_TipoQuejaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents MuestraTecnicosQuejasbuenoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Procedimientosarnoldo4 As sofTV.Procedimientosarnoldo4
    Friend WithEvents Muestra_Tecnicos_QuejasbuenoTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Muestra_Tecnicos_QuejasbuenoTableAdapter
    Friend WithEvents cbCuadrilla As System.Windows.Forms.ComboBox
    Friend WithEvents Visita3MaskedTextBox As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Visita3Label As System.Windows.Forms.Label
    Friend WithEvents dtpHoraVisita2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpHoraVisita3 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpHoraVisita1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Hora_sol As System.Windows.Forms.DateTimePicker
    Friend WithEvents Panel11 As System.Windows.Forms.Panel
    Friend WithEvents Visita3MaskedTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents rbEnProceso As System.Windows.Forms.RadioButton
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents EnProcesoMaskedTextBox As System.Windows.Forms.MaskedTextBox
    Friend WithEvents lblEnProceso As System.Windows.Forms.Label
    Friend WithEvents dtpHoraEnProceso As System.Windows.Forms.DateTimePicker
    Friend WithEvents cmbPrioridadQueja As System.Windows.Forms.ComboBox
    Friend WithEvents ObservacionesTextBox As System.Windows.Forms.RichTextBox
    Friend WithEvents TextComentario As System.Windows.Forms.TextBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter5 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter6 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter7 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter8 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter9 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter10 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents cmbClasifProblemas As System.Windows.Forms.ComboBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter11 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents lblObsAgendaMostrar As System.Windows.Forms.Label
    Friend WithEvents lblObsAgenda As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter12 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter13 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter14 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Panel12 As System.Windows.Forms.Panel
    Friend WithEvents TxtRefOxxo As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
End Class
