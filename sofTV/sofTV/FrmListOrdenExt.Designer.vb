﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmListOrdenExt
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cbxListado = New System.Windows.Forms.RadioButton()
        Me.cbxOrden = New System.Windows.Forms.RadioButton()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.CMBlblTitulo = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'cbxListado
        '
        Me.cbxListado.AutoSize = True
        Me.cbxListado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxListado.Location = New System.Drawing.Point(33, 92)
        Me.cbxListado.Name = "cbxListado"
        Me.cbxListado.Size = New System.Drawing.Size(82, 19)
        Me.cbxListado.TabIndex = 0
        Me.cbxListado.TabStop = True
        Me.cbxListado.Text = "LISTADO"
        Me.cbxListado.UseVisualStyleBackColor = True
        '
        'cbxOrden
        '
        Me.cbxOrden.AutoSize = True
        Me.cbxOrden.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxOrden.Location = New System.Drawing.Point(173, 92)
        Me.cbxOrden.Name = "cbxOrden"
        Me.cbxOrden.Size = New System.Drawing.Size(92, 19)
        Me.cbxOrden.TabIndex = 1
        Me.cbxOrden.TabStop = True
        Me.cbxOrden.Text = "ORDENES"
        Me.cbxOrden.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.Location = New System.Drawing.Point(33, 157)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(93, 45)
        Me.btnAceptar.TabIndex = 2
        Me.btnAceptar.Text = "ACEPTAR"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(173, 157)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(93, 45)
        Me.btnCancelar.TabIndex = 3
        Me.btnCancelar.Text = "CANCELAR"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'CMBlblTitulo
        '
        Me.CMBlblTitulo.AutoSize = True
        Me.CMBlblTitulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblTitulo.Location = New System.Drawing.Point(9, 35)
        Me.CMBlblTitulo.Name = "CMBlblTitulo"
        Me.CMBlblTitulo.Size = New System.Drawing.Size(278, 16)
        Me.CMBlblTitulo.TabIndex = 4
        Me.CMBlblTitulo.Text = "SELECCIONA EL REPORTE A INCLUIR"
        '
        'FrmListOrdenExt
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(297, 231)
        Me.Controls.Add(Me.CMBlblTitulo)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.cbxOrden)
        Me.Controls.Add(Me.cbxListado)
        Me.Name = "FrmListOrdenExt"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FrmListOrdenExt"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbxListado As System.Windows.Forms.RadioButton
    Friend WithEvents cbxOrden As System.Windows.Forms.RadioButton
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents CMBlblTitulo As System.Windows.Forms.Label
End Class
