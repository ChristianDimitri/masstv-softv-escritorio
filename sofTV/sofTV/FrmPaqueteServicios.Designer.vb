﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPaqueteServicios
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmPaqueteServicios))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tbNumero = New System.Windows.Forms.TextBox()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.IdPaquete = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Numero = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LetraPaquete = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cbServicioDig = New System.Windows.Forms.ComboBox()
        Me.tbLetraPaquete = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbServicioNet = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.nudTvAdic = New System.Windows.Forms.NumericUpDown()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.panelGuardar = New System.Windows.Forms.Panel()
        Me.dgwAdi = New System.Windows.Forms.DataGridView()
        Me.Clv_ServicioAdi = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescripcionAdi = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SeleccionaAdi = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.bnGuardar = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbGuardar = New System.Windows.Forms.ToolStripButton()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.bnEliminar = New System.Windows.Forms.Button()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudTvAdic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panelGuardar.SuspendLayout()
        CType(Me.dgwAdi, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bnGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnGuardar.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(85, 59)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(58, 15)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Número"
        '
        'tbNumero
        '
        Me.tbNumero.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNumero.Location = New System.Drawing.Point(149, 53)
        Me.tbNumero.Name = "tbNumero"
        Me.tbNumero.Size = New System.Drawing.Size(170, 21)
        Me.tbNumero.TabIndex = 2
        '
        'dgw
        '
        Me.dgw.AllowUserToAddRows = False
        Me.dgw.AllowUserToDeleteRows = False
        Me.dgw.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgw.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdPaquete, Me.Numero, Me.Descripcion, Me.LetraPaquete})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgw.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgw.Location = New System.Drawing.Point(99, 283)
        Me.dgw.Name = "dgw"
        Me.dgw.ReadOnly = True
        Me.dgw.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgw.Size = New System.Drawing.Size(785, 364)
        Me.dgw.TabIndex = 3
        '
        'IdPaquete
        '
        Me.IdPaquete.DataPropertyName = "IdPaquete"
        Me.IdPaquete.HeaderText = "IdPaquete"
        Me.IdPaquete.Name = "IdPaquete"
        Me.IdPaquete.ReadOnly = True
        Me.IdPaquete.Visible = False
        '
        'Numero
        '
        Me.Numero.DataPropertyName = "Numero"
        Me.Numero.HeaderText = "No."
        Me.Numero.Name = "Numero"
        Me.Numero.ReadOnly = True
        Me.Numero.Width = 50
        '
        'Descripcion
        '
        Me.Descripcion.DataPropertyName = "Descripcion"
        Me.Descripcion.HeaderText = "Tipo Servicio"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        Me.Descripcion.Width = 550
        '
        'LetraPaquete
        '
        Me.LetraPaquete.DataPropertyName = "LetraPaquete"
        Me.LetraPaquete.HeaderText = "Letra Paquete"
        Me.LetraPaquete.Name = "LetraPaquete"
        Me.LetraPaquete.ReadOnly = True
        Me.LetraPaquete.Width = 120
        '
        'cbServicioDig
        '
        Me.cbServicioDig.DisplayMember = "Descripcion"
        Me.cbServicioDig.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbServicioDig.FormattingEnabled = True
        Me.cbServicioDig.Location = New System.Drawing.Point(149, 107)
        Me.cbServicioDig.Name = "cbServicioDig"
        Me.cbServicioDig.Size = New System.Drawing.Size(276, 23)
        Me.cbServicioDig.TabIndex = 4
        Me.cbServicioDig.ValueMember = "Clv_Servicio"
        '
        'tbLetraPaquete
        '
        Me.tbLetraPaquete.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbLetraPaquete.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbLetraPaquete.Location = New System.Drawing.Point(149, 80)
        Me.tbLetraPaquete.MaxLength = 1
        Me.tbLetraPaquete.Name = "tbLetraPaquete"
        Me.tbLetraPaquete.Size = New System.Drawing.Size(170, 21)
        Me.tbLetraPaquete.TabIndex = 6
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(46, 86)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(97, 15)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Letra Paquete"
        '
        'cbServicioNet
        '
        Me.cbServicioNet.DisplayMember = "Descripcion"
        Me.cbServicioNet.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbServicioNet.FormattingEnabled = True
        Me.cbServicioNet.Location = New System.Drawing.Point(149, 163)
        Me.cbServicioNet.Name = "cbServicioNet"
        Me.cbServicioNet.Size = New System.Drawing.Size(276, 23)
        Me.cbServicioNet.TabIndex = 8
        Me.cbServicioNet.ValueMember = "Clv_Servicio"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(32, 142)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(111, 15)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Tv´s Adicionales"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(39, 115)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(104, 15)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Servicio Digital"
        '
        'nudTvAdic
        '
        Me.nudTvAdic.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudTvAdic.Location = New System.Drawing.Point(149, 136)
        Me.nudTvAdic.Name = "nudTvAdic"
        Me.nudTvAdic.Size = New System.Drawing.Size(57, 21)
        Me.nudTvAdic.TabIndex = 12
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(523, 34)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(222, 19)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "Servicios Digitales Adicionales"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(32, 171)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(111, 15)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "Servicio Internet"
        '
        'panelGuardar
        '
        Me.panelGuardar.Controls.Add(Me.dgwAdi)
        Me.panelGuardar.Controls.Add(Me.bnGuardar)
        Me.panelGuardar.Controls.Add(Me.Label2)
        Me.panelGuardar.Controls.Add(Me.Label6)
        Me.panelGuardar.Controls.Add(Me.Label1)
        Me.panelGuardar.Controls.Add(Me.Label5)
        Me.panelGuardar.Controls.Add(Me.tbNumero)
        Me.panelGuardar.Controls.Add(Me.nudTvAdic)
        Me.panelGuardar.Controls.Add(Me.cbServicioDig)
        Me.panelGuardar.Controls.Add(Me.Label4)
        Me.panelGuardar.Controls.Add(Me.Label3)
        Me.panelGuardar.Controls.Add(Me.tbLetraPaquete)
        Me.panelGuardar.Controls.Add(Me.cbServicioNet)
        Me.panelGuardar.Location = New System.Drawing.Point(36, 23)
        Me.panelGuardar.Name = "panelGuardar"
        Me.panelGuardar.Size = New System.Drawing.Size(925, 236)
        Me.panelGuardar.TabIndex = 15
        '
        'dgwAdi
        '
        Me.dgwAdi.AllowUserToAddRows = False
        Me.dgwAdi.AllowUserToDeleteRows = False
        Me.dgwAdi.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgwAdi.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgwAdi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgwAdi.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_ServicioAdi, Me.DescripcionAdi, Me.SeleccionaAdi})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgwAdi.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgwAdi.Location = New System.Drawing.Point(526, 53)
        Me.dgwAdi.Name = "dgwAdi"
        Me.dgwAdi.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgwAdi.Size = New System.Drawing.Size(354, 168)
        Me.dgwAdi.TabIndex = 19
        '
        'Clv_ServicioAdi
        '
        Me.Clv_ServicioAdi.DataPropertyName = "Clv_Servicio"
        Me.Clv_ServicioAdi.HeaderText = "Clv_Servicio"
        Me.Clv_ServicioAdi.Name = "Clv_ServicioAdi"
        Me.Clv_ServicioAdi.Visible = False
        '
        'DescripcionAdi
        '
        Me.DescripcionAdi.DataPropertyName = "Descripcion"
        Me.DescripcionAdi.HeaderText = "Servicio"
        Me.DescripcionAdi.Name = "DescripcionAdi"
        Me.DescripcionAdi.Width = 200
        '
        'SeleccionaAdi
        '
        Me.SeleccionaAdi.DataPropertyName = "Selecciona"
        Me.SeleccionaAdi.HeaderText = "Selecciona"
        Me.SeleccionaAdi.Name = "SeleccionaAdi"
        Me.SeleccionaAdi.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.SeleccionaAdi.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'bnGuardar
        '
        Me.bnGuardar.AddNewItem = Nothing
        Me.bnGuardar.CountItem = Nothing
        Me.bnGuardar.DeleteItem = Nothing
        Me.bnGuardar.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnGuardar.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbGuardar})
        Me.bnGuardar.Location = New System.Drawing.Point(0, 0)
        Me.bnGuardar.MoveFirstItem = Nothing
        Me.bnGuardar.MoveLastItem = Nothing
        Me.bnGuardar.MoveNextItem = Nothing
        Me.bnGuardar.MovePreviousItem = Nothing
        Me.bnGuardar.Name = "bnGuardar"
        Me.bnGuardar.PositionItem = Nothing
        Me.bnGuardar.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnGuardar.Size = New System.Drawing.Size(925, 25)
        Me.bnGuardar.TabIndex = 15
        Me.bnGuardar.Text = "BindingNavigator1"
        '
        'tsbGuardar
        '
        Me.tsbGuardar.Image = CType(resources.GetObject("tsbGuardar.Image"), System.Drawing.Image)
        Me.tsbGuardar.Name = "tsbGuardar"
        Me.tsbGuardar.Size = New System.Drawing.Size(76, 22)
        Me.tsbGuardar.Text = "&Agregar"
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(860, 682)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(136, 36)
        Me.bnSalir.TabIndex = 17
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'bnEliminar
        '
        Me.bnEliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnEliminar.Location = New System.Drawing.Point(890, 283)
        Me.bnEliminar.Name = "bnEliminar"
        Me.bnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.bnEliminar.TabIndex = 18
        Me.bnEliminar.Text = "&Eliminar"
        Me.bnEliminar.UseVisualStyleBackColor = True
        '
        'FrmPaqueteServicios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1008, 730)
        Me.Controls.Add(Me.bnEliminar)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.dgw)
        Me.Controls.Add(Me.panelGuardar)
        Me.Name = "FrmPaqueteServicios"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Paquetes - Estados de Cuenta"
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudTvAdic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panelGuardar.ResumeLayout(False)
        Me.panelGuardar.PerformLayout()
        CType(Me.dgwAdi, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bnGuardar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnGuardar.ResumeLayout(False)
        Me.bnGuardar.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tbNumero As System.Windows.Forms.TextBox
    Friend WithEvents dgw As System.Windows.Forms.DataGridView
    Friend WithEvents cbServicioDig As System.Windows.Forms.ComboBox
    Friend WithEvents tbLetraPaquete As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbServicioNet As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents nudTvAdic As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents panelGuardar As System.Windows.Forms.Panel
    Friend WithEvents bnGuardar As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbGuardar As System.Windows.Forms.ToolStripButton
    Friend WithEvents bnSalir As System.Windows.Forms.Button
    Friend WithEvents bnEliminar As System.Windows.Forms.Button
    Friend WithEvents dgwAdi As System.Windows.Forms.DataGridView
    Friend WithEvents Clv_ServicioAdi As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescripcionAdi As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SeleccionaAdi As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents IdPaquete As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Numero As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LetraPaquete As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
