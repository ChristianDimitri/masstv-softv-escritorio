Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient

Public Class FrmRepPenetracion
    Private customersByCityReport As ReportDocument
    Dim princ As String = Nothing
    Dim oprep As String = Nothing
    Dim Titulo As String = Nothing
    Private Sub ConfigureCrystalReportsNew6()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword
            Dim Contrataciones As Boolean = False
            Dim Instalaciones As Boolean = False
            Dim Fuera_Area As Boolean = False
            Dim Cancelaciones As Boolean = False
            Dim reportPath As String = Nothing

            Dim mySelectFormula As String = Titulo
            Dim Subformula As String = Nothing



            mySelectFormula = "Reporte de Penetraci�n "
            reportPath = RutaReportes + "\ReportePenetracion_CalleyColonia.rpt"

            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            ''@op
            'customersByCityReport.SetParameterValue(0, GloClv_tipser2)
            ''@clv_colonia
            'customersByCityReport.SetParameterValue(1, Locclv_colonia)
            ''@clv_calle
            'customersByCityReport.SetParameterValue(2, Locclv_calle)
            ''@clv_txt
            'customersByCityReport.SetParameterValue(3, Locclv_txt)
            customersByCityReport.SetParameterValue(0, GloClv_tipser2)
            '@clv_colonia
            customersByCityReport.SetParameterValue(1, Locclv_colonia)
            '@clv_calle
            customersByCityReport.SetParameterValue(2, Locclv_calle)
            '@clv_txt
            customersByCityReport.SetParameterValue(3, "")
            '@Clv_Sector
            customersByCityReport.SetParameterValue(4, eClv_Sector)
            'Tipo
            customersByCityReport.SetParameterValue(5, 1)
            'Session
            customersByCityReport.SetParameterValue(6, eClv_Session)




            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait

            CrystalReportViewer1.ReportSource = customersByCityReport



            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & GloSucursal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Colonia").Text = "' Colonia: " & LocDesPC & IIf(LocDesPCa.Trim().Length > 0, " Y " & "Calle: " & LocDesPCa & "'", "'")

            customersByCityReport.DataDefinition.FormulaFields("Activos").Text = "'" & "" & "'"
            customersByCityReport.DataDefinition.FormulaFields("TotalCartera").Text = "'" & "" & "'"
            'customersByCityReport.DataDefinition.FormulaFields("Servicio").Text = "'Servicio: " & LocDesP & "'"
            'customersByCityReport.DataDefinition.FormulaFields("Calle").Text = "'" & LocDesPCa & "'"






            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()

    End Sub

    Private Sub FrmRepPenetracion_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'If oprep = "2" Then

        '    'Dim CON As New SqlConnection(MiConexion)
        '    'CON.Open()
        '    'Me.Dame_Datos_Rep_PTableAdapter.Connection = CON
        '    'Me.Dame_Datos_Rep_PTableAdapter.Fill(Me.DataSetarnoldo.Dame_Datos_Rep_P, GloClv_tipser2, Locclv_colonia, Locclv_calle, Locclv_txt)
        '    'CON.Close()

        '    'If CInt(Me.SalidasTapsTextBox.Text) > 0 Then
        '    'Me.TextBox2.Text = Math.Round(((CDbl(Me.ITextBox.Text) + CDbl(Me.DTextBox.Text) + CDbl(Me.STextBox.Text)) / CDbl(Me.SalidasTapsTextBox.Text)) * 100, 2)
        '    'Me.TextBox3.Text = Math.Round((CDbl(Me.ITextBox.Text) / CDbl(Me.SalidasTapsTextBox.Text)) * 100, 2)
        '    ConfigureCrystalReportsNew6()
        '    'Else
        '    '    MsgBox("En esa Colonia(s) y/o Calle(s) no existen salidas en los Taps", MsgBoxStyle.Information)
        '    'End If
        'ElseIf oprep = "1" Then

        '    'Mandamos llamar el reporte #1: Calles y Colonias
        '    ReportePermanenciaCrystal()

        'End If

        'oprep = "0"

    End Sub

    Private Sub FrmRepPenetracion_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerPrincipal' Puede moverla o quitarla seg�n sea necesario.
        Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
        Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)

        princ = Me.ComboBox4.SelectedValue.ToString
        GloClv_tipser2 = Me.ComboBox4.SelectedValue
        gloClvTipSerReportes = GloClv_tipser2
        Select Case princ
            Case "1"
                Me.Catalogo_Reportes_PenetracionTableAdapter.Connection = CON
                Me.Catalogo_Reportes_PenetracionTableAdapter.Fill(Me.DataSetarnoldo.Catalogo_Reportes_Penetracion, Me.ComboBox4.SelectedValue)
            Case "2"
                Me.Catalogo_Reportes_PenetracionTableAdapter.Connection = CON
                Me.Catalogo_Reportes_PenetracionTableAdapter.Fill(Me.DataSetarnoldo.Catalogo_Reportes_Penetracion, Me.ComboBox4.SelectedValue)
            Case "3"
                Me.Catalogo_Reportes_PenetracionTableAdapter.Connection = CON
                Me.Catalogo_Reportes_PenetracionTableAdapter.Fill(Me.DataSetarnoldo.Catalogo_Reportes_Penetracion, Me.ComboBox4.SelectedValue)
            Case "6"
                Me.Catalogo_Reportes_PenetracionTableAdapter.Connection = CON
                Me.Catalogo_Reportes_PenetracionTableAdapter.Fill(Me.DataSetarnoldo.Catalogo_Reportes_Penetracion, Me.ComboBox4.SelectedValue)
        End Select
        CON.Close()
        colorea(Me, Me.Name)
    End Sub

    Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.SelectedIndexChanged
        If IsNumeric(Me.ComboBox4.SelectedValue) = True Then
            GloClv_tipser2 = ComboBox4.SelectedValue
            gloClvTipSerReportes = GloClv_tipser2
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.Catalogo_Reportes_PenetracionTableAdapter.Connection = CON
            Me.Catalogo_Reportes_PenetracionTableAdapter.Fill(Me.DataSetarnoldo.Catalogo_Reportes_Penetracion, Me.ComboBox4.SelectedValue)
            CON.Close()
        End If
    End Sub

    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        If IsNumeric(Me.DataGridView1.SelectedCells(0).Value) = True Then
            oprep = Me.DataGridView1.SelectedCells(0).Value.ToString
            Titulo = Me.DataGridView1.SelectedCells(1).Value.ToString

            If oprep = "1" Then
                FrmSelDatosP.ShowDialog()
                Dame_clv_session_Reportes()
                ReportePermanenciaCrystal()
            End If

            If oprep = "2" Then

                FrmFiltroSector.Clv_Sector = 0
                FrmFiltroSector.ShowDialog()
                eClv_Sector = FrmFiltroSector.Clv_Sector
                LocDesPC = FrmFiltroSector.NombreSector
                LocDesPCa = ""
                Locbndpen1 = True

                'El segundo filtro es el de los Servicios
                Dame_clv_session_Reportes()
                FrmSelServicioE.ShowDialog()
                ConfigureCrystalReportsNew6()
                FrmFiltroSector.Clv_Sector = 0

            End If

        End If
    End Sub

    Private Sub Dame_clv_session_Reportes()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("Dame_clv_session_Reportes", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eClv_Session = CLng(parametro.Value.ToString())
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    '--------- PRIMER REPORTE DE PENETRACI�N  -------------
    Private Sub ReportePenetracion(ByVal Op As Integer, ByVal Clv_Colonia As Integer, ByVal Clv_Calle As Integer, ByVal Clv_Session As Long, ByVal Clv_Sector As Integer, ByVal Tipo As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ReportePenetracion", conexion)
        comando.CommandType = CommandType.StoredProcedure
        Dim reader As SqlDataReader

        Dim parametro As New SqlParameter("@Op", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Op
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_Colonia", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_Colonia
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Clv_Calle", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Clv_Calle
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = Clv_Session
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Clv_Sector", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = Clv_Sector
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@Tipo", SqlDbType.Int)
        parametro6.Direction = ParameterDirection.Input
        parametro6.Value = Tipo
        comando.Parameters.Add(parametro6)

        Try
            conexion.Open()
            reader = comando.ExecuteReader

            While (reader.Read())
                Me.SalidasTapsTextBox.Text = reader(3).ToString()
                Me.TextBox3.Text = reader(4).ToString()
                Me.TextBox2.Text = reader(5).ToString()
                Me.CTextBox.Text = reader(6).ToString()
                Me.ITextBox.Text = reader(7).ToString()
                Me.DTextBox.Text = reader(8).ToString()
                Me.STextBox.Text = reader(9).ToString()
                Me.FTextBox.Text = reader(10).ToString()
                Me.BTextBox.Text = reader(11).ToString()
                'reader(12).ToString()
                Me.TotalTextBox.Text = reader(13).ToString()
                Exit Sub
            End While

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub ReportePermanenciaCrystal()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword
            Dim Contrataciones As Boolean = False
            Dim Instalaciones As Boolean = False
            Dim Fuera_Area As Boolean = False
            Dim Cancelaciones As Boolean = False
            Dim reportPath As String = Nothing

            Dim mySelectFormula As String = Titulo
            Dim Subformula As String = Nothing


            'If eTipoPen = 1 Then
            '    mySelectFormula = "Penetraci�n por Calle y Colonia Del "
            '    LocDesPC = "Colonia: " & LocDesPC
            '    LocDesPCa = "Calle: " & LocDesPCa
            'ElseIf eTipoPen = 2 Then
            '    mySelectFormula = "Penetraci�n por Sector "
            '    LocDesPC = "Sector: " & LocDesPC
            'End If

            reportPath = RutaReportes + "\ReportePermanencia.rpt"

            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@op
            customersByCityReport.SetParameterValue(0, GloClv_tipser2)
            '@clv_colonia
            customersByCityReport.SetParameterValue(1, Locclv_colonia)
            '@clv_calle
            customersByCityReport.SetParameterValue(2, Locclv_calle)
            'Session
            customersByCityReport.SetParameterValue(3, eClv_Session)
            '@Clv_Sector
            customersByCityReport.SetParameterValue(4, eClv_Sector)
            '@Tipo
            customersByCityReport.SetParameterValue(5, oprep)
            '@clvTxtServicio
            customersByCityReport.SetParameterValue(6, Locclv_txt)

            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait

            CrystalReportViewer1.ReportSource = customersByCityReport

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    '--------- FIN DEL PRIMER REPORTE DE PENETRACI�N  -------------

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub
End Class