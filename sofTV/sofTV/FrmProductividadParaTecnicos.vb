﻿Imports sofTV.BAL
Public Class FrmProductividadParaTecnicos

    Public IdProductividadParaTecnicos As Integer

    Private Sub Frm_ProductividadParaTecnicos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        limpiarPantalla()

        If (eOpcion = "N") Then

        Else
            Dim ppt As ProductividadParaTecnico = ProductividadParaTecnico.GetOne(IdProductividadParaTecnicos)
            PuntosInicialesTextBox.Text = ppt.PuntosIniciales.ToString()
            PuntosFinalesTextBox.Text = ppt.PuntosFinales.ToString()
            ImporteTextBox.Text = String.Format("{0:0.##}", ppt.Importe) '.ToString("N2")
        End If
    End Sub

    Private Sub limpiarPantalla()
        PuntosInicialesTextBox.Text = ""
        PuntosFinalesTextBox.Text = ""
        ImporteTextBox.Text = ""
    End Sub

    Private Sub PuntosInicialesTextBox_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles PuntosInicialesTextBox.KeyPress
        If Char.IsNumber(e.KeyChar) Then
        Else
            e.Handled = False
        End If
    End Sub

    Private Sub PuntosFinalesTextBox_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles PuntosFinalesTextBox.KeyPress
        If Char.IsNumber(e.KeyChar) Then
        Else
            e.Handled = False
        End If
    End Sub

    Private Sub ImporteTextBox_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles ImporteTextBox.KeyPress
        If Char.IsNumber(e.KeyChar) Then
        Else
            e.Handled = False
        End If
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If (Not (PuntosInicialesTextBox.Text.Trim().Equals(String.Empty)) And Not (PuntosFinalesTextBox.Text.Trim().Equals("")) And Not (ImporteTextBox.Text.Trim().Equals(""))) Then
            If (ValidaEfectivo(ImporteTextBox.Text.Trim())) Then
                If (ProductividadParaTecnico.GetProductividadParaTecnicoExiste(IIf(eOpcion = "N", Nothing, IdProductividadParaTecnicos), Integer.Parse(PuntosInicialesTextBox.Text), Integer.Parse(PuntosFinalesTextBox.Text))) Then
                    MessageBox.Show("Los Datos Ya Existen Favor de Verificar")
                Else
                    If (eOpcion = "N") Then
                        ProductividadParaTecnico.Add(Nothing, Integer.Parse(PuntosInicialesTextBox.Text), Integer.Parse(PuntosFinalesTextBox.Text), Integer.Parse(ImporteTextBox.Text))
                    Else
                        ProductividadParaTecnico.Edit(IdProductividadParaTecnicos, Integer.Parse(PuntosInicialesTextBox.Text), Integer.Parse(PuntosFinalesTextBox.Text), Integer.Parse(ImporteTextBox.Text))
                    End If
                    MessageBox.Show("Los Datos Fueron Guardados Exitosamente")
                    Me.Close()
                End If
            Else
                MessageBox.Show("El Importe no es Valido")
            End If
           
        Else
            MessageBox.Show("Faltan Valores Por Asignar")
        End If
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Function ValidaEfectivo(ByVal importe As String) As Boolean
        Dim Bandera As Boolean
        Bandera = False
        Dim dec As Decimal
        If (Decimal.TryParse(importe, dec)) Then
            Bandera = True
        End If
        Return Bandera
    End Function

End Class