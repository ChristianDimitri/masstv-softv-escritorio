﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTrabInstal
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CMBlblTipSer = New System.Windows.Forms.Label()
        Me.cbxTipServ = New System.Windows.Forms.ComboBox()
        Me.CMBlblSeleccionar = New System.Windows.Forms.Label()
        Me.lbxSeleccionar = New System.Windows.Forms.ListBox()
        Me.btnLL = New System.Windows.Forms.Button()
        Me.btnL = New System.Windows.Forms.Button()
        Me.btnMM = New System.Windows.Forms.Button()
        Me.btnM = New System.Windows.Forms.Button()
        Me.CMBlblSeleccionado = New System.Windows.Forms.Label()
        Me.lbxSeleccionado = New System.Windows.Forms.ListBox()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.CMB2lblOrdInsta = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'CMBlblTipSer
        '
        Me.CMBlblTipSer.AutoSize = True
        Me.CMBlblTipSer.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblTipSer.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBlblTipSer.Location = New System.Drawing.Point(7, 10)
        Me.CMBlblTipSer.Name = "CMBlblTipSer"
        Me.CMBlblTipSer.Size = New System.Drawing.Size(154, 18)
        Me.CMBlblTipSer.TabIndex = 68
        Me.CMBlblTipSer.Text = "Tipo de Servicios  :"
        Me.CMBlblTipSer.Visible = False
        '
        'cbxTipServ
        '
        Me.cbxTipServ.DisplayMember = "Concepto"
        Me.cbxTipServ.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cbxTipServ.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxTipServ.ForeColor = System.Drawing.Color.Red
        Me.cbxTipServ.FormattingEnabled = True
        Me.cbxTipServ.Location = New System.Drawing.Point(10, 33)
        Me.cbxTipServ.Name = "cbxTipServ"
        Me.cbxTipServ.Size = New System.Drawing.Size(226, 24)
        Me.cbxTipServ.TabIndex = 70
        Me.cbxTipServ.ValueMember = "Clv_TipSerPrincipal"
        Me.cbxTipServ.Visible = False
        '
        'CMBlblSeleccionar
        '
        Me.CMBlblSeleccionar.AutoSize = True
        Me.CMBlblSeleccionar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblSeleccionar.ForeColor = System.Drawing.Color.Brown
        Me.CMBlblSeleccionar.Location = New System.Drawing.Point(29, 82)
        Me.CMBlblSeleccionar.Name = "CMBlblSeleccionar"
        Me.CMBlblSeleccionar.Size = New System.Drawing.Size(198, 16)
        Me.CMBlblSeleccionar.TabIndex = 71
        Me.CMBlblSeleccionar.Text = "Trabajos para Seleccionar:"
        '
        'lbxSeleccionar
        '
        Me.lbxSeleccionar.DisplayMember = "Descripcion"
        Me.lbxSeleccionar.FormattingEnabled = True
        Me.lbxSeleccionar.Location = New System.Drawing.Point(32, 101)
        Me.lbxSeleccionar.Name = "lbxSeleccionar"
        Me.lbxSeleccionar.ScrollAlwaysVisible = True
        Me.lbxSeleccionar.Size = New System.Drawing.Size(262, 199)
        Me.lbxSeleccionar.TabIndex = 72
        Me.lbxSeleccionar.TabStop = False
        Me.lbxSeleccionar.ValueMember = "clv_trabajos"
        '
        'btnLL
        '
        Me.btnLL.BackColor = System.Drawing.Color.DarkOrange
        Me.btnLL.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLL.Location = New System.Drawing.Point(311, 246)
        Me.btnLL.Name = "btnLL"
        Me.btnLL.Size = New System.Drawing.Size(75, 23)
        Me.btnLL.TabIndex = 76
        Me.btnLL.Text = "<<"
        Me.btnLL.UseVisualStyleBackColor = False
        '
        'btnL
        '
        Me.btnL.BackColor = System.Drawing.Color.DarkOrange
        Me.btnL.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnL.Location = New System.Drawing.Point(311, 217)
        Me.btnL.Name = "btnL"
        Me.btnL.Size = New System.Drawing.Size(75, 23)
        Me.btnL.TabIndex = 75
        Me.btnL.Text = "<"
        Me.btnL.UseVisualStyleBackColor = False
        '
        'btnMM
        '
        Me.btnMM.BackColor = System.Drawing.Color.DarkOrange
        Me.btnMM.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMM.Location = New System.Drawing.Point(311, 149)
        Me.btnMM.Name = "btnMM"
        Me.btnMM.Size = New System.Drawing.Size(75, 23)
        Me.btnMM.TabIndex = 74
        Me.btnMM.Text = ">>"
        Me.btnMM.UseVisualStyleBackColor = False
        '
        'btnM
        '
        Me.btnM.BackColor = System.Drawing.Color.DarkOrange
        Me.btnM.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnM.Location = New System.Drawing.Point(311, 120)
        Me.btnM.Name = "btnM"
        Me.btnM.Size = New System.Drawing.Size(75, 23)
        Me.btnM.TabIndex = 73
        Me.btnM.Text = ">"
        Me.btnM.UseVisualStyleBackColor = False
        '
        'CMBlblSeleccionado
        '
        Me.CMBlblSeleccionado.AutoSize = True
        Me.CMBlblSeleccionado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblSeleccionado.ForeColor = System.Drawing.Color.Brown
        Me.CMBlblSeleccionado.Location = New System.Drawing.Point(399, 82)
        Me.CMBlblSeleccionado.Name = "CMBlblSeleccionado"
        Me.CMBlblSeleccionado.Size = New System.Drawing.Size(183, 16)
        Me.CMBlblSeleccionado.TabIndex = 78
        Me.CMBlblSeleccionado.Text = "Trabajos Seleccionados:"
        '
        'lbxSeleccionado
        '
        Me.lbxSeleccionado.DisplayMember = "Descripcion"
        Me.lbxSeleccionado.FormattingEnabled = True
        Me.lbxSeleccionado.Location = New System.Drawing.Point(402, 101)
        Me.lbxSeleccionado.Name = "lbxSeleccionado"
        Me.lbxSeleccionado.ScrollAlwaysVisible = True
        Me.lbxSeleccionado.Size = New System.Drawing.Size(256, 199)
        Me.lbxSeleccionado.TabIndex = 77
        Me.lbxSeleccionado.TabStop = False
        Me.lbxSeleccionado.ValueMember = "clv_trabajos"
        '
        'btnCancelar
        '
        Me.btnCancelar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(402, 335)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(136, 36)
        Me.btnCancelar.TabIndex = 80
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = False
        '
        'btnAceptar
        '
        Me.btnAceptar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.Location = New System.Drawing.Point(134, 335)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(136, 36)
        Me.btnAceptar.TabIndex = 79
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = False
        '
        'CMB2lblOrdInsta
        '
        Me.CMB2lblOrdInsta.AutoSize = True
        Me.CMB2lblOrdInsta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMB2lblOrdInsta.ForeColor = System.Drawing.Color.Red
        Me.CMB2lblOrdInsta.Location = New System.Drawing.Point(167, 14)
        Me.CMB2lblOrdInsta.Name = "CMB2lblOrdInsta"
        Me.CMB2lblOrdInsta.Size = New System.Drawing.Size(305, 16)
        Me.CMB2lblOrdInsta.TabIndex = 69
        Me.CMB2lblOrdInsta.Text = "ORDENES DE SERVCIO DE INSTALACIÓN"
        '
        'FrmTrabInstal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(688, 387)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.CMBlblSeleccionado)
        Me.Controls.Add(Me.lbxSeleccionado)
        Me.Controls.Add(Me.btnLL)
        Me.Controls.Add(Me.btnL)
        Me.Controls.Add(Me.btnMM)
        Me.Controls.Add(Me.btnM)
        Me.Controls.Add(Me.lbxSeleccionar)
        Me.Controls.Add(Me.CMBlblSeleccionar)
        Me.Controls.Add(Me.cbxTipServ)
        Me.Controls.Add(Me.CMB2lblOrdInsta)
        Me.Controls.Add(Me.CMBlblTipSer)
        Me.Name = "FrmTrabInstal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Seleccionar Trabajos"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBlblTipSer As System.Windows.Forms.Label
    Friend WithEvents cbxTipServ As System.Windows.Forms.ComboBox
    Friend WithEvents CMBlblSeleccionar As System.Windows.Forms.Label
    Friend WithEvents lbxSeleccionar As System.Windows.Forms.ListBox
    Friend WithEvents btnLL As System.Windows.Forms.Button
    Friend WithEvents btnL As System.Windows.Forms.Button
    Friend WithEvents btnMM As System.Windows.Forms.Button
    Friend WithEvents btnM As System.Windows.Forms.Button
    Friend WithEvents CMBlblSeleccionado As System.Windows.Forms.Label
    Friend WithEvents lbxSeleccionado As System.Windows.Forms.ListBox
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents CMB2lblOrdInsta As System.Windows.Forms.Label
End Class
