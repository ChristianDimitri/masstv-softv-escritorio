﻿Imports System.Data.SqlClient

Public Class FrmSelFechaRIO

    Private Sub FrmSelFechaRIO_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.Label1.ForeColor = Color.Black
        Me.DateTimePicker1.Value = Today
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MuestraTipServEricTableAdapter.Connection = CON
        Me.MuestraTipServEricTableAdapter.Fill(Me.DataSetEric.MuestraTipServEric, 0, 0)
        CON.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
        fecha_cartera = Me.DateTimePicker1.Text
        gloServicio = ConceptoComboBox.SelectedValue
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
End Class