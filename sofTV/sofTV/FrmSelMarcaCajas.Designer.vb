﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelMarcaCajas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.AparatosComboBox = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.AceptarCButton = New System.Windows.Forms.Button()
        Me.SalirButton = New System.Windows.Forms.Button()
        Me.HDRadioBut = New System.Windows.Forms.RadioButton()
        Me.NormalRadioBut = New System.Windows.Forms.RadioButton()
        Me.CMBLabel3 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'AparatosComboBox
        '
        Me.AparatosComboBox.DisplayMember = "Descripcion"
        Me.AparatosComboBox.FormattingEnabled = True
        Me.AparatosComboBox.Location = New System.Drawing.Point(7, 88)
        Me.AparatosComboBox.Name = "AparatosComboBox"
        Me.AparatosComboBox.Size = New System.Drawing.Size(390, 21)
        Me.AparatosComboBox.TabIndex = 0
        Me.AparatosComboBox.ValueMember = "NoArticulo"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label1.Location = New System.Drawing.Point(10, 69)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(329, 16)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Selecciones la Marca del Aparato que desea :"
        '
        'AceptarCButton
        '
        Me.AceptarCButton.BackColor = System.Drawing.Color.DarkOrange
        Me.AceptarCButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.AceptarCButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AceptarCButton.ForeColor = System.Drawing.Color.Black
        Me.AceptarCButton.Location = New System.Drawing.Point(119, 128)
        Me.AceptarCButton.Name = "AceptarCButton"
        Me.AceptarCButton.Size = New System.Drawing.Size(136, 33)
        Me.AceptarCButton.TabIndex = 4
        Me.AceptarCButton.Text = "&GUARDAR"
        Me.AceptarCButton.UseVisualStyleBackColor = False
        '
        'SalirButton
        '
        Me.SalirButton.BackColor = System.Drawing.Color.DarkOrange
        Me.SalirButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.SalirButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SalirButton.ForeColor = System.Drawing.Color.Black
        Me.SalirButton.Location = New System.Drawing.Point(261, 128)
        Me.SalirButton.Name = "SalirButton"
        Me.SalirButton.Size = New System.Drawing.Size(136, 33)
        Me.SalirButton.TabIndex = 5
        Me.SalirButton.Text = "&CERRAR"
        Me.SalirButton.UseVisualStyleBackColor = False
        '
        'HDRadioBut
        '
        Me.HDRadioBut.AutoSize = True
        Me.HDRadioBut.ForeColor = System.Drawing.Color.LightSlateGray
        Me.HDRadioBut.Location = New System.Drawing.Point(214, 40)
        Me.HDRadioBut.Name = "HDRadioBut"
        Me.HDRadioBut.Size = New System.Drawing.Size(41, 17)
        Me.HDRadioBut.TabIndex = 7
        Me.HDRadioBut.Text = "HD"
        Me.HDRadioBut.UseVisualStyleBackColor = True
        '
        'NormalRadioBut
        '
        Me.NormalRadioBut.AutoSize = True
        Me.NormalRadioBut.Checked = True
        Me.NormalRadioBut.ForeColor = System.Drawing.Color.LightSlateGray
        Me.NormalRadioBut.Location = New System.Drawing.Point(126, 39)
        Me.NormalRadioBut.Name = "NormalRadioBut"
        Me.NormalRadioBut.Size = New System.Drawing.Size(58, 17)
        Me.NormalRadioBut.TabIndex = 6
        Me.NormalRadioBut.TabStop = True
        Me.NormalRadioBut.Text = "Normal"
        Me.NormalRadioBut.UseVisualStyleBackColor = True
        '
        'CMBLabel3
        '
        Me.CMBLabel3.AutoSize = True
        Me.CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CMBLabel3.Location = New System.Drawing.Point(10, 40)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(110, 16)
        Me.CMBLabel3.TabIndex = 8
        Me.CMBLabel3.Text = "Tipo Paquete :"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.HDRadioBut)
        Me.Panel1.Controls.Add(Me.CMBLabel3)
        Me.Panel1.Controls.Add(Me.NormalRadioBut)
        Me.Panel1.Controls.Add(Me.AparatosComboBox)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.AceptarCButton)
        Me.Panel1.Controls.Add(Me.SalirButton)
        Me.Panel1.Location = New System.Drawing.Point(1, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(412, 180)
        Me.Panel1.TabIndex = 9
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.LightSkyBlue
        Me.Label2.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(-5, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(425, 31)
        Me.Label2.TabIndex = 45
        Me.Label2.Text = "Marcas de Cajas Digitales"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'FrmSelMarcaCajas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(415, 183)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FrmSelMarcaCajas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catalogo Cajas Digitales"
        Me.TopMost = True
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents AparatosComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents AceptarCButton As System.Windows.Forms.Button
    Friend WithEvents SalirButton As System.Windows.Forms.Button
    Friend WithEvents HDRadioBut As System.Windows.Forms.RadioButton
    Friend WithEvents NormalRadioBut As System.Windows.Forms.RadioButton
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
