﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmControlHorasOrdenes
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_UsuarioLabel As System.Windows.Forms.Label
        Dim NombreLabel As System.Windows.Forms.Label
        Dim FechaIngresoLabel As System.Windows.Forms.Label
        Dim FechaSalidaLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim ServicioLabel As System.Windows.Forms.Label
        Dim SOLOINTERNETLabel1 As System.Windows.Forms.Label
        Dim Label14 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmControlHorasOrdenes))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.dtpFechaF = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaI = New System.Windows.Forms.DateTimePicker()
        Me.cbCuadrilla = New System.Windows.Forms.ComboBox()
        Me.btnBuscarOrd = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ESHOTELCheckBox = New System.Windows.Forms.CheckBox()
        Me.SOLOINTERNETCheckBox = New System.Windows.Forms.CheckBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.NOMBRELabel1 = New System.Windows.Forms.Label()
        Me.CALLELabel1 = New System.Windows.Forms.Label()
        Me.COLONIALabel1 = New System.Windows.Forms.Label()
        Me.NUMEROLabel1 = New System.Windows.Forms.Label()
        Me.CIUDADLabel1 = New System.Windows.Forms.Label()
        Me.Clv_TecnicoTextBox = New System.Windows.Forms.TextBox()
        Me.txtOrden = New System.Windows.Forms.TextBox()
        Me.txtUsuario = New System.Windows.Forms.TextBox()
        Me.cbxTecnico = New System.Windows.Forms.ComboBox()
        Me.CMBTextBox4 = New System.Windows.Forms.TextBox()
        Me.Clv_TipoUsuarioTextBox = New System.Windows.Forms.TextBox()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.mnuBotones = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.Consulta_PaisesBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Clv_UsuarioLabel = New System.Windows.Forms.Label()
        NombreLabel = New System.Windows.Forms.Label()
        FechaIngresoLabel = New System.Windows.Forms.Label()
        FechaSalidaLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        ServicioLabel = New System.Windows.Forms.Label()
        SOLOINTERNETLabel1 = New System.Windows.Forms.Label()
        Label14 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.mnuBotones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.mnuBotones.SuspendLayout()
        Me.SuspendLayout()
        '
        'Clv_UsuarioLabel
        '
        Clv_UsuarioLabel.AutoSize = True
        Clv_UsuarioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_UsuarioLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_UsuarioLabel.Location = New System.Drawing.Point(12, 41)
        Clv_UsuarioLabel.Name = "Clv_UsuarioLabel"
        Clv_UsuarioLabel.Size = New System.Drawing.Size(65, 15)
        Clv_UsuarioLabel.TabIndex = 2
        Clv_UsuarioLabel.Text = "Usuario :"
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.ForeColor = System.Drawing.Color.LightSlateGray
        NombreLabel.Location = New System.Drawing.Point(16, 118)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(65, 15)
        NombreLabel.TabIndex = 4
        NombreLabel.Text = "Tecnico :"
        '
        'FechaIngresoLabel
        '
        FechaIngresoLabel.AutoSize = True
        FechaIngresoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FechaIngresoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        FechaIngresoLabel.Location = New System.Drawing.Point(447, 74)
        FechaIngresoLabel.Name = "FechaIngresoLabel"
        FechaIngresoLabel.Size = New System.Drawing.Size(93, 15)
        FechaIngresoLabel.TabIndex = 10
        FechaIngresoLabel.Text = "Fecha Inicio :"
        '
        'FechaSalidaLabel
        '
        FechaSalidaLabel.AutoSize = True
        FechaSalidaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FechaSalidaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        FechaSalidaLabel.Location = New System.Drawing.Point(430, 96)
        FechaSalidaLabel.Name = "FechaSalidaLabel"
        FechaSalidaLabel.Size = New System.Drawing.Size(111, 15)
        FechaSalidaLabel.TabIndex = 12
        FechaSalidaLabel.Text = "Fecha Termino :"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(25, 88)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(54, 15)
        Label1.TabIndex = 57
        Label1.Text = "Orden :"
        '
        'ServicioLabel
        '
        ServicioLabel.AutoSize = True
        ServicioLabel.Location = New System.Drawing.Point(9, 96)
        ServicioLabel.Name = "ServicioLabel"
        ServicioLabel.Size = New System.Drawing.Size(151, 16)
        ServicioLabel.TabIndex = 18
        ServicioLabel.Text = "Servicio Asignados :"
        '
        'SOLOINTERNETLabel1
        '
        SOLOINTERNETLabel1.AutoSize = True
        SOLOINTERNETLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SOLOINTERNETLabel1.ForeColor = System.Drawing.Color.White
        SOLOINTERNETLabel1.Location = New System.Drawing.Point(300, 73)
        SOLOINTERNETLabel1.Name = "SOLOINTERNETLabel1"
        SOLOINTERNETLabel1.Size = New System.Drawing.Size(97, 15)
        SOLOINTERNETLabel1.TabIndex = 16
        SOLOINTERNETLabel1.Text = "Solo Internet :"
        '
        'Label14
        '
        Label14.AutoSize = True
        Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label14.ForeColor = System.Drawing.Color.LightSlateGray
        Label14.Location = New System.Drawing.Point(10, 160)
        Label14.Name = "Label14"
        Label14.Size = New System.Drawing.Size(73, 15)
        Label14.TabIndex = 431
        Label14.Text = "Cuadrilla :"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.dtpFechaF)
        Me.Panel1.Controls.Add(Me.dtpFechaI)
        Me.Panel1.Controls.Add(Label14)
        Me.Panel1.Controls.Add(Me.cbCuadrilla)
        Me.Panel1.Controls.Add(Me.btnBuscarOrd)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.txtOrden)
        Me.Panel1.Controls.Add(Label1)
        Me.Panel1.Controls.Add(Me.txtUsuario)
        Me.Panel1.Controls.Add(Me.cbxTecnico)
        Me.Panel1.Controls.Add(Me.CMBTextBox4)
        Me.Panel1.Controls.Add(Clv_UsuarioLabel)
        Me.Panel1.Controls.Add(NombreLabel)
        Me.Panel1.Controls.Add(FechaIngresoLabel)
        Me.Panel1.Controls.Add(FechaSalidaLabel)
        Me.Panel1.Controls.Add(Me.Clv_TipoUsuarioTextBox)
        Me.Panel1.Location = New System.Drawing.Point(0, 28)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(752, 388)
        Me.Panel1.TabIndex = 1
        Me.Panel1.TabStop = True
        '
        'dtpFechaF
        '
        Me.dtpFechaF.CustomFormat = "dd-MM-yyyy HH:mm tt"
        Me.dtpFechaF.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.dtpFechaF.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpFechaF.Location = New System.Drawing.Point(546, 96)
        Me.dtpFechaF.Name = "dtpFechaF"
        Me.dtpFechaF.Size = New System.Drawing.Size(192, 21)
        Me.dtpFechaF.TabIndex = 433
        '
        'dtpFechaI
        '
        Me.dtpFechaI.CustomFormat = "dd-MM-yyyy HH:mm tt"
        Me.dtpFechaI.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.dtpFechaI.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpFechaI.Location = New System.Drawing.Point(546, 69)
        Me.dtpFechaI.Name = "dtpFechaI"
        Me.dtpFechaI.Size = New System.Drawing.Size(192, 21)
        Me.dtpFechaI.TabIndex = 432
        '
        'cbCuadrilla
        '
        Me.cbCuadrilla.DisplayMember = "Nombre"
        Me.cbCuadrilla.Enabled = False
        Me.cbCuadrilla.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbCuadrilla.FormattingEnabled = True
        Me.cbCuadrilla.Location = New System.Drawing.Point(85, 152)
        Me.cbCuadrilla.Name = "cbCuadrilla"
        Me.cbCuadrilla.Size = New System.Drawing.Size(332, 23)
        Me.cbCuadrilla.TabIndex = 430
        Me.cbCuadrilla.ValueMember = "clv_Tecnico"
        '
        'btnBuscarOrd
        '
        Me.btnBuscarOrd.BackColor = System.Drawing.Color.DarkOrange
        Me.btnBuscarOrd.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscarOrd.Location = New System.Drawing.Point(211, 81)
        Me.btnBuscarOrd.Name = "btnBuscarOrd"
        Me.btnBuscarOrd.Size = New System.Drawing.Size(30, 22)
        Me.btnBuscarOrd.TabIndex = 174
        Me.btnBuscarOrd.Text = "..."
        Me.btnBuscarOrd.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Gray
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.TreeView1)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(ServicioLabel)
        Me.GroupBox1.Controls.Add(Me.ESHOTELCheckBox)
        Me.GroupBox1.Controls.Add(SOLOINTERNETLabel1)
        Me.GroupBox1.Controls.Add(Me.SOLOINTERNETCheckBox)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.NOMBRELabel1)
        Me.GroupBox1.Controls.Add(Me.CALLELabel1)
        Me.GroupBox1.Controls.Add(Me.COLONIALabel1)
        Me.GroupBox1.Controls.Add(Me.NUMEROLabel1)
        Me.GroupBox1.Controls.Add(Me.CIUDADLabel1)
        Me.GroupBox1.Controls.Add(Me.Clv_TecnicoTextBox)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.White
        Me.GroupBox1.Location = New System.Drawing.Point(12, 186)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(726, 184)
        Me.GroupBox1.TabIndex = 173
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Cliente"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(430, 73)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(67, 15)
        Me.Label13.TabIndex = 432
        Me.Label13.Text = "Es hotel :"
        '
        'TreeView1
        '
        Me.TreeView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView1.Location = New System.Drawing.Point(165, 99)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(382, 79)
        Me.TreeView1.TabIndex = 19
        Me.TreeView1.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(166, 107)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(55, 16)
        Me.Label2.TabIndex = 431
        Me.Label2.Text = "Label2"
        Me.Label2.Visible = False
        '
        'ESHOTELCheckBox
        '
        Me.ESHOTELCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ESHOTELCheckBox.Location = New System.Drawing.Point(503, 67)
        Me.ESHOTELCheckBox.Name = "ESHOTELCheckBox"
        Me.ESHOTELCheckBox.Size = New System.Drawing.Size(21, 24)
        Me.ESHOTELCheckBox.TabIndex = 18
        Me.ESHOTELCheckBox.TabStop = False
        '
        'SOLOINTERNETCheckBox
        '
        Me.SOLOINTERNETCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.SOLOINTERNETCheckBox.Location = New System.Drawing.Point(403, 67)
        Me.SOLOINTERNETCheckBox.Name = "SOLOINTERNETCheckBox"
        Me.SOLOINTERNETCheckBox.Size = New System.Drawing.Size(21, 24)
        Me.SOLOINTERNETCheckBox.TabIndex = 17
        Me.SOLOINTERNETCheckBox.TabStop = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(378, 41)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(31, 13)
        Me.Label11.TabIndex = 16
        Me.Label11.Text = "Col. :"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(299, 41)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(20, 13)
        Me.Label10.TabIndex = 15
        Me.Label10.Text = "# :"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(9, 41)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(58, 13)
        Me.Label9.TabIndex = 14
        Me.Label9.Text = "Dirección :"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(10, 18)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(50, 13)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Nombre :"
        '
        'NOMBRELabel1
        '
        Me.NOMBRELabel1.AutoEllipsis = True
        Me.NOMBRELabel1.BackColor = System.Drawing.Color.Gray
        Me.NOMBRELabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NOMBRELabel1.Location = New System.Drawing.Point(69, 18)
        Me.NOMBRELabel1.Name = "NOMBRELabel1"
        Me.NOMBRELabel1.Size = New System.Drawing.Size(467, 23)
        Me.NOMBRELabel1.TabIndex = 3
        '
        'CALLELabel1
        '
        Me.CALLELabel1.AutoEllipsis = True
        Me.CALLELabel1.BackColor = System.Drawing.Color.Gray
        Me.CALLELabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CALLELabel1.Location = New System.Drawing.Point(69, 41)
        Me.CALLELabel1.Name = "CALLELabel1"
        Me.CALLELabel1.Size = New System.Drawing.Size(224, 23)
        Me.CALLELabel1.TabIndex = 5
        '
        'COLONIALabel1
        '
        Me.COLONIALabel1.AutoEllipsis = True
        Me.COLONIALabel1.BackColor = System.Drawing.Color.Gray
        Me.COLONIALabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.COLONIALabel1.Location = New System.Drawing.Point(415, 41)
        Me.COLONIALabel1.Name = "COLONIALabel1"
        Me.COLONIALabel1.Size = New System.Drawing.Size(121, 23)
        Me.COLONIALabel1.TabIndex = 7
        '
        'NUMEROLabel1
        '
        Me.NUMEROLabel1.AutoEllipsis = True
        Me.NUMEROLabel1.BackColor = System.Drawing.Color.Gray
        Me.NUMEROLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NUMEROLabel1.Location = New System.Drawing.Point(325, 41)
        Me.NUMEROLabel1.Name = "NUMEROLabel1"
        Me.NUMEROLabel1.Size = New System.Drawing.Size(47, 23)
        Me.NUMEROLabel1.TabIndex = 9
        '
        'CIUDADLabel1
        '
        Me.CIUDADLabel1.AutoEllipsis = True
        Me.CIUDADLabel1.BackColor = System.Drawing.Color.Gray
        Me.CIUDADLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CIUDADLabel1.Location = New System.Drawing.Point(69, 65)
        Me.CIUDADLabel1.Name = "CIUDADLabel1"
        Me.CIUDADLabel1.Size = New System.Drawing.Size(224, 23)
        Me.CIUDADLabel1.TabIndex = 11
        '
        'Clv_TecnicoTextBox
        '
        Me.Clv_TecnicoTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TecnicoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_TecnicoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_TecnicoTextBox.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TecnicoTextBox.Location = New System.Drawing.Point(236, 129)
        Me.Clv_TecnicoTextBox.Name = "Clv_TecnicoTextBox"
        Me.Clv_TecnicoTextBox.Size = New System.Drawing.Size(24, 14)
        Me.Clv_TecnicoTextBox.TabIndex = 170
        Me.Clv_TecnicoTextBox.TabStop = False
        '
        'txtOrden
        '
        Me.txtOrden.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtOrden.Location = New System.Drawing.Point(85, 82)
        Me.txtOrden.Name = "txtOrden"
        Me.txtOrden.Size = New System.Drawing.Size(120, 21)
        Me.txtOrden.TabIndex = 58
        '
        'txtUsuario
        '
        Me.txtUsuario.Enabled = False
        Me.txtUsuario.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtUsuario.Location = New System.Drawing.Point(78, 41)
        Me.txtUsuario.Name = "txtUsuario"
        Me.txtUsuario.Size = New System.Drawing.Size(332, 21)
        Me.txtUsuario.TabIndex = 56
        '
        'cbxTecnico
        '
        Me.cbxTecnico.Enabled = False
        Me.cbxTecnico.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.cbxTecnico.FormattingEnabled = True
        Me.cbxTecnico.Location = New System.Drawing.Point(84, 118)
        Me.cbxTecnico.Name = "cbxTecnico"
        Me.cbxTecnico.Size = New System.Drawing.Size(332, 23)
        Me.cbxTecnico.TabIndex = 55
        '
        'CMBTextBox4
        '
        Me.CMBTextBox4.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox4.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox4.Location = New System.Drawing.Point(430, 41)
        Me.CMBTextBox4.Name = "CMBTextBox4"
        Me.CMBTextBox4.Size = New System.Drawing.Size(308, 19)
        Me.CMBTextBox4.TabIndex = 48
        Me.CMBTextBox4.TabStop = False
        Me.CMBTextBox4.Text = "Fechas de "
        Me.CMBTextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Clv_TipoUsuarioTextBox
        '
        Me.Clv_TipoUsuarioTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TipoUsuarioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_TipoUsuarioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_TipoUsuarioTextBox.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TipoUsuarioTextBox.Location = New System.Drawing.Point(382, 208)
        Me.Clv_TipoUsuarioTextBox.Name = "Clv_TipoUsuarioTextBox"
        Me.Clv_TipoUsuarioTextBox.Size = New System.Drawing.Size(17, 14)
        Me.Clv_TipoUsuarioTextBox.TabIndex = 19
        Me.Clv_TipoUsuarioTextBox.TabStop = False
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.DarkOrange
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.ForeColor = System.Drawing.Color.Black
        Me.btnSalir.Location = New System.Drawing.Point(758, 365)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(112, 33)
        Me.btnSalir.TabIndex = 175
        Me.btnSalir.Text = "&SALIR"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'mnuBotones
        '
        Me.mnuBotones.AddNewItem = Nothing
        Me.mnuBotones.CountItem = Nothing
        Me.mnuBotones.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.mnuBotones.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mnuBotones.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.Consulta_PaisesBindingNavigatorSaveItem})
        Me.mnuBotones.Location = New System.Drawing.Point(0, 0)
        Me.mnuBotones.MoveFirstItem = Nothing
        Me.mnuBotones.MoveLastItem = Nothing
        Me.mnuBotones.MoveNextItem = Nothing
        Me.mnuBotones.MovePreviousItem = Nothing
        Me.mnuBotones.Name = "mnuBotones"
        Me.mnuBotones.PositionItem = Nothing
        Me.mnuBotones.Size = New System.Drawing.Size(882, 25)
        Me.mnuBotones.TabIndex = 22
        Me.mnuBotones.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(74, 22)
        Me.BindingNavigatorDeleteItem.Text = "Eliminar"
        '
        'Consulta_PaisesBindingNavigatorSaveItem
        '
        Me.Consulta_PaisesBindingNavigatorSaveItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.Consulta_PaisesBindingNavigatorSaveItem.Image = CType(resources.GetObject("Consulta_PaisesBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.Consulta_PaisesBindingNavigatorSaveItem.Name = "Consulta_PaisesBindingNavigatorSaveItem"
        Me.Consulta_PaisesBindingNavigatorSaveItem.Size = New System.Drawing.Size(114, 22)
        Me.Consulta_PaisesBindingNavigatorSaveItem.Text = "Guardar datos"
        '
        'FrmControlHorasOrdenes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(882, 428)
        Me.Controls.Add(Me.mnuBotones)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.btnSalir)
        Me.Name = "FrmControlHorasOrdenes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FrmControlHorasOrdenes"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.mnuBotones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.mnuBotones.ResumeLayout(False)
        Me.mnuBotones.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents CMBTextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Clv_TipoUsuarioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents cbxTecnico As System.Windows.Forms.ComboBox
    Friend WithEvents txtOrden As System.Windows.Forms.TextBox
    Friend WithEvents txtUsuario As System.Windows.Forms.TextBox
    Friend WithEvents mnuBotones As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Consulta_PaisesBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ESHOTELCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents SOLOINTERNETCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents NOMBRELabel1 As System.Windows.Forms.Label
    Friend WithEvents CALLELabel1 As System.Windows.Forms.Label
    Friend WithEvents COLONIALabel1 As System.Windows.Forms.Label
    Friend WithEvents NUMEROLabel1 As System.Windows.Forms.Label
    Friend WithEvents CIUDADLabel1 As System.Windows.Forms.Label
    Friend WithEvents Clv_TecnicoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents btnBuscarOrd As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents cbCuadrilla As System.Windows.Forms.ComboBox
    Friend WithEvents dtpFechaF As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFechaI As System.Windows.Forms.DateTimePicker
End Class
