﻿Imports System.Data.SqlClient

Public Class FormIAPARATOS


    Private Sub SP_CONSULTAIAPARATOS(ByVal oClv_Orden As Long, oClave As Long)
        Try

        
        '@Clv_Orden Bigint=0,@Clave Bigint=0,@Contratonet bigint =0 OUTPUT,@Clv_Aparato bigint=0 OUTPUT
        Dim oContratonet As Long = 0
            Dim oClv_Aparato As Long = 0
            Dim oStatus As String = ""
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
        BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)
        BaseII.CreateMyParameter("@Contratonet", ParameterDirection.Output, SqlDbType.BigInt)
            BaseII.CreateMyParameter("@Clv_Aparato", ParameterDirection.Output, SqlDbType.BigInt)
            BaseII.CreateMyParameter("@Status", ParameterDirection.Output, SqlDbType.VarChar, 1)
        BaseII.ProcedimientoOutPut("SP_CONSULTAIAPARATOS")
        oContratonet = BaseII.dicoPar("@Contratonet").ToString
            oClv_Aparato = BaseII.dicoPar("@Clv_Aparato").ToString
            oStatus = BaseII.dicoPar("@Status").ToString
            If oStatus.Length > 0 Then
                CMBoxEstadoAparato.SelectedValue = oStatus
            End If
            If oContratonet > 0 Then
            ComboBoxPorAsignar.SelectedValue = oContratonet
        End If
        If oClv_Aparato > 0 Then
            ComboBoxAparatosDisponibles.SelectedValue = oClv_Aparato
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Llena_AparatosporAsignar(oOp As String, oTrabajo As String, oContrato As Long, oClv_Tecnico As Long, oClv_Orden As Long, oClave As Long)
        Try
            '@Op BIGINT=0,@TIPO_APARATO VARCHAR(5)='',@TRABAJO VARCHAR(10)='',@CONTRATO BIGINT=0,@CLV_TECNICO INT=0
            '@Clv_Orden bigint=0,@Clave bigint=0
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@Op", SqlDbType.VarChar, oOp, 5)
            BaseII.CreateMyParameter("@TRABAJO", SqlDbType.VarChar, oTrabajo, 10)
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, oContrato)
            BaseII.CreateMyParameter("@CLV_TECNICO", SqlDbType.Int, oClv_Tecnico)
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
            BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)

            ComboBoxPorAsignar.DataSource = BaseII.ConsultaDT("MUESTRAAPARATOS_DISCPONIBLES")
            ComboBoxPorAsignar.DisplayMember = "Descripcion"
            ComboBoxPorAsignar.ValueMember = "ContratoAnt"

            If ComboBoxPorAsignar.Items.Count > 0 Then
                ComboBoxPorAsignar.SelectedIndex = 0
            End If
            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Llena_EstadoAparato()
        Try
            '@Op BIGINT=0,@TIPO_APARATO VARCHAR(5)='',@TRABAJO VARCHAR(10)='',@CONTRATO BIGINT=0,@CLV_TECNICO INT=0
            '@Clv_Orden bigint=0,@Clave bigint=0
            BaseII.limpiaParametros()

            CMBoxEstadoAparato.DataSource = BaseII.ConsultaDT("SP_StatusAparatos")
            CMBoxEstadoAparato.DisplayMember = "Concepto"
            CMBoxEstadoAparato.ValueMember = "Clv_StatusCableModem"

            If CMBoxEstadoAparato.Items.Count > 0 Then
                CMBoxEstadoAparato.SelectedIndex = 0
            End If
            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub


    Private Sub Llena_AparatosporDisponibles(oOp As String, oTrabajo As String, oContrato As Long, oClv_Tecnico As Long, oClv_Orden As Long, oClave As Long)
        Try
            '@Op BIGINT=0,@TIPO_APARATO VARCHAR(5)='',@TRABAJO VARCHAR(10)='',@CONTRATO BIGINT=0,@CLV_TECNICO INT=0
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@Op", SqlDbType.VarChar, oOp, 5)
            BaseII.CreateMyParameter("@TRABAJO", SqlDbType.VarChar, oTrabajo, 10)
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, oContrato)
            BaseII.CreateMyParameter("@CLV_TECNICO", SqlDbType.Int, oClv_Tecnico)
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
            BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)
            ComboBoxAparatosDisponibles.DataSource = BaseII.ConsultaDT("MUESTRAAPARATOS_DISCPONIBLES")
            ComboBoxAparatosDisponibles.DisplayMember = "Descripcion"
            ComboBoxAparatosDisponibles.ValueMember = "ContratoAnt"

            If ComboBoxAparatosDisponibles.Items.Count > 0 Then
                ComboBoxAparatosDisponibles.SelectedIndex = 0
            End If
            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub SP_GuardaIAPARATOS(oClave As Long, oTrabajo As String, oClv_Orden As Long, oContratonet As Long, oClv_Aparato As Long, oOpcion As String, oStatus As String)
        '@Clave bigint=0,@Trabajo varchar(10)='',@Clv_Orden bigint=0,@Contratonet Bigint=0,@Clv_Aparato bigint=0,@Opcion varchar(10)=''
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)
        BaseII.CreateMyParameter("@Trabajo", SqlDbType.VarChar, oTrabajo, 10)
        BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
        BaseII.CreateMyParameter("@Contratonet", SqlDbType.BigInt, oContratonet)
        BaseII.CreateMyParameter("@Clv_Aparato", SqlDbType.BigInt, oClv_Aparato)
        BaseII.CreateMyParameter("@Opcion", SqlDbType.VarChar, oOpcion, 10)
        BaseII.CreateMyParameter("@Status", SqlDbType.VarChar, oStatus, 1)
        BaseII.Inserta("SP_GuardaIAPARATOS")
    End Sub

    Private Sub CONCCABMBindingNavigatorSaveItem_Click(sender As System.Object, e As System.EventArgs)

    End Sub

    Private Sub FormIAPARATOS_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)

        Me.Label4.ForeColor = Color.Black
        Me.Label1.ForeColor = Color.Black
        Me.LblEstadoAparato.ForeColor = Color.Black

        Llena_AparatosporAsignar(opcion, GLOTRABAJO, FrmOrdSer.ContratoTextBox.Text, 9999999, gloClv_Orden, GloDetClave)
        Llena_AparatosporDisponibles(opcion, GLOTRABAJO, FrmOrdSer.ContratoTextBox.Text, FrmOrdSer.Tecnico.SelectedValue, gloClv_Orden, GloDetClave)
        Llena_EstadoAparato()
        Me.Text = GLONOMTRABAJO

        SP_CONSULTAIAPARATOS(gloClv_Orden, GloDetClave)
        LblEstadoAparato.Visible = False
        CMBoxEstadoAparato.Visible = False

        If GLOTRABAJO = "CANTE" Or GLOTRABAJO = "CALNB" Or GLOTRABAJO = "CAPAR" Then
            LblEstadoAparato.Visible = True
            CMBoxEstadoAparato.Visible = True

        End If

        If opcion = "M" Or opcion = "C" Then
            If FrmOrdSer.Panel6.Enabled = False Then
                ComboBoxAparatosDisponibles.Enabled = False
                ComboBoxPorAsignar.Enabled = False
                BtnAceptar.Enabled = False
                CMBoxEstadoAparato.Enabled = False
            End If
        End If

        If GLOTRABAJO = "IANRA" Then
            GroupBox1.Visible = True
            Me.Size = New Size(477, 524)
            BtnAceptar.Location = New Point(171, 439)
            Button5.Location = New Point(313, 439)
            SP_DameDatosInstalacionInternet(gloClv_Orden, GloDetClave, ComboBoxPorAsignar.SelectedValue)
        Else
            GroupBox1.Visible = False
            Me.Size = New Size(477, 270)
            BtnAceptar.Location = New Point(171, 180)
            Button5.Location = New Point(313, 180)
        End If


    End Sub

    Private Sub BtnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles BtnAceptar.Click
        If GLOTRABAJO = "CANTE" Or GLOTRABAJO = "CALNB" Or GLOTRABAJO = "CAPAR" Then
            If CMBoxEstadoAparato.SelectedIndex = -1 Then
                MsgBox("Seleccione el Estado del Aparato a Cambiar ", MsgBoxStyle.Information, "Información")
                Exit Sub
            End If
            If ComboBoxAparatosDisponibles.SelectedIndex = -1 Then
                MsgBox("Seleccione el Aparato a Cambiar ", MsgBoxStyle.Information, "Información")
                Exit Sub
            End If
            If ComboBoxPorAsignar.SelectedIndex = -1 Then
                MsgBox("Seleccione el Nuevo Aparato ", MsgBoxStyle.Information, "Información")
                Exit Sub
            End If
        Else
            If ComboBoxAparatosDisponibles.SelectedIndex = -1 Then
                MsgBox("Seleccione el Aparato a Instalar", MsgBoxStyle.Information, "Información")
                Exit Sub
            End If
            If ComboBoxPorAsignar.SelectedIndex = -1 Then
                MsgBox("Seleccione el Aparato ", MsgBoxStyle.Information, "Información")
                Exit Sub
            End If

            If GLOTRABAJO = "IANRA" And (TextBox1.Text = "" Or TextBox2.Text = "" Or TextBox3.Text = "" Or TextBox4.Text = "" Or TextBox5.Text = "" Or TextBox6.Text = "" Or TextBox7.Text = "" Or TextBox8.Text = "") Then
                MsgBox("Debe llenar todos los campos", MsgBoxStyle.Information, "Información")
                Exit Sub
            End If

        End If
        SP_GuardaIAPARATOS(GloDetClave, GLOTRABAJO, gloClv_Orden, ComboBoxPorAsignar.SelectedValue, ComboBoxAparatosDisponibles.SelectedValue, opcion, CMBoxEstadoAparato.SelectedValue)
        If GLOTRABAJO = "IANRA" Then
            SP_GuardaDatosInstalacionInternet(gloClv_Orden, GloDetClave, GLOTRABAJO, ComboBoxPorAsignar.SelectedValue)
        End If
        Me.Close()

    End Sub

    Private Sub SP_GuardaDatosInstalacionInternet(oClv_Orden As Long, oClave As Long, oTrabajo As String, oContratonet As Long)
        '@Clave bigint=0,@Trabajo varchar(10)='',@Clv_Orden bigint=0,@Contratonet Bigint=0,@Clv_Aparato bigint=0,@Opcion varchar(10)=''
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
        BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)
        BaseII.CreateMyParameter("@Trabajo", SqlDbType.VarChar, oTrabajo, 50)
        BaseII.CreateMyParameter("@Contratonet", SqlDbType.BigInt, oContratonet)
        BaseII.CreateMyParameter("@Ip", SqlDbType.VarChar, TextBox1.Text, 50)
        BaseII.CreateMyParameter("@Pin", SqlDbType.VarChar, TextBox2.Text, 50)
        BaseII.CreateMyParameter("@DistanciaConfig", SqlDbType.VarChar, TextBox3.Text, 50)
        BaseII.CreateMyParameter("@TorreEnlace", SqlDbType.VarChar, TextBox5.Text, 50)
        BaseII.CreateMyParameter("@SectorialEnlace", SqlDbType.VarChar, TextBox4.Text, 50)
        BaseII.CreateMyParameter("@IntesidadSeñal", SqlDbType.VarChar, TextBox8.Text, 50)
        BaseII.CreateMyParameter("@VelSubida", SqlDbType.VarChar, TextBox7.Text, 50)
        BaseII.CreateMyParameter("@VelBajada", SqlDbType.VarChar, TextBox6.Text, 50)
        BaseII.Inserta("SP_GuardaDatosInstalacionInternet")
    End Sub

    Private Sub SP_DameDatosInstalacionInternet(oClv_Orden As Long, oClave As Long, oContratonet As Long)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, oClv_Orden)
        BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, oClave)
        BaseII.CreateMyParameter("@Contratonet", SqlDbType.BigInt, oContratonet)
        BaseII.CreateMyParameter("@Ip", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.CreateMyParameter("@Pin", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.CreateMyParameter("@DistanciaConfig", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.CreateMyParameter("@TorreEnlace", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.CreateMyParameter("@SectorialEnlace", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.CreateMyParameter("@IntesidadSeñal", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.CreateMyParameter("@VelSubida", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.CreateMyParameter("@VelBajada", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.ProcedimientoOutPut("SP_DameDatosInstalacionInternet")
        TextBox1.Text = BaseII.dicoPar("@Ip").ToString
        TextBox2.Text = BaseII.dicoPar("@Pin").ToString
        TextBox3.Text = BaseII.dicoPar("@DistanciaConfig").ToString
        TextBox5.Text = BaseII.dicoPar("@TorreEnlace").ToString
        TextBox4.Text = BaseII.dicoPar("@SectorialEnlace").ToString
        TextBox8.Text = BaseII.dicoPar("@IntesidadSeñal").ToString
        TextBox7.Text = BaseII.dicoPar("@VelSubida").ToString
        TextBox6.Text = BaseII.dicoPar("@VelBajada").ToString
    End Sub

    Private Sub Button5_Click(sender As System.Object, e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
End Class