﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmtblRelVendedoresSeries
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmtblRelVendedoresSeries))
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.bnRelVendedoresSeries = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbEliminar = New System.Windows.Forms.ToolStripButton()
        Me.tsbGuardar = New System.Windows.Forms.ToolStripButton()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.dgvRelVendedoresSeries = New System.Windows.Forms.DataGridView()
        Me.IdDetalle = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClvCompania = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nombreCompania = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FoliosUsados = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Serie = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UltimoFolioUsado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bnCancelar = New System.Windows.Forms.Button()
        Me.bnEliminar = New System.Windows.Forms.Button()
        Me.bnGuardar = New System.Windows.Forms.Button()
        Me.bnModificar = New System.Windows.Forms.Button()
        Me.bnAgregar = New System.Windows.Forms.Button()
        Me.CMBlabel5 = New System.Windows.Forms.Label()
        Me.CMBlabel4 = New System.Windows.Forms.Label()
        Me.CMBlabel3 = New System.Windows.Forms.Label()
        Me.CMBlabel2 = New System.Windows.Forms.Label()
        Me.CMBlabel1 = New System.Windows.Forms.Label()
        Me.tbUltimoFolioUsado = New System.Windows.Forms.TextBox()
        Me.tbSerie = New System.Windows.Forms.TextBox()
        Me.tbFoliosUsados = New System.Windows.Forms.TextBox()
        Me.cbCompanias = New System.Windows.Forms.ComboBox()
        Me.cbVendedores = New System.Windows.Forms.ComboBox()
        CType(Me.bnRelVendedoresSeries, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnRelVendedoresSeries.SuspendLayout()
        CType(Me.dgvRelVendedoresSeries, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'bnRelVendedoresSeries
        '
        Me.bnRelVendedoresSeries.AddNewItem = Nothing
        Me.bnRelVendedoresSeries.BackColor = System.Drawing.Color.WhiteSmoke
        Me.bnRelVendedoresSeries.CountItem = Nothing
        Me.bnRelVendedoresSeries.DeleteItem = Nothing
        Me.bnRelVendedoresSeries.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnRelVendedoresSeries.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbEliminar, Me.tsbGuardar})
        Me.bnRelVendedoresSeries.Location = New System.Drawing.Point(0, 0)
        Me.bnRelVendedoresSeries.MoveFirstItem = Nothing
        Me.bnRelVendedoresSeries.MoveLastItem = Nothing
        Me.bnRelVendedoresSeries.MoveNextItem = Nothing
        Me.bnRelVendedoresSeries.MovePreviousItem = Nothing
        Me.bnRelVendedoresSeries.Name = "bnRelVendedoresSeries"
        Me.bnRelVendedoresSeries.PositionItem = Nothing
        Me.bnRelVendedoresSeries.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnRelVendedoresSeries.Size = New System.Drawing.Size(717, 25)
        Me.bnRelVendedoresSeries.TabIndex = 34
        Me.bnRelVendedoresSeries.Text = "bindingNavigator1"
        '
        'tsbEliminar
        '
        Me.tsbEliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.tsbEliminar.Image = CType(resources.GetObject("tsbEliminar.Image"), System.Drawing.Image)
        Me.tsbEliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbEliminar.Name = "tsbEliminar"
        Me.tsbEliminar.Size = New System.Drawing.Size(76, 22)
        Me.tsbEliminar.Text = "&ELIMINAR"
        '
        'tsbGuardar
        '
        Me.tsbGuardar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.tsbGuardar.Image = CType(resources.GetObject("tsbGuardar.Image"), System.Drawing.Image)
        Me.tsbGuardar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbGuardar.Name = "tsbGuardar"
        Me.tsbGuardar.Size = New System.Drawing.Size(77, 22)
        Me.tsbGuardar.Text = "&GUARDAR"
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(567, 418)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(136, 36)
        Me.bnSalir.TabIndex = 35
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'dgvRelVendedoresSeries
        '
        Me.dgvRelVendedoresSeries.AllowUserToAddRows = False
        Me.dgvRelVendedoresSeries.AllowUserToDeleteRows = False
        Me.dgvRelVendedoresSeries.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvRelVendedoresSeries.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle17
        Me.dgvRelVendedoresSeries.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRelVendedoresSeries.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdDetalle, Me.ClvCompania, Me.nombreCompania, Me.FoliosUsados, Me.Serie, Me.UltimoFolioUsado})
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvRelVendedoresSeries.DefaultCellStyle = DataGridViewCellStyle18
        Me.dgvRelVendedoresSeries.Location = New System.Drawing.Point(12, 264)
        Me.dgvRelVendedoresSeries.Name = "dgvRelVendedoresSeries"
        Me.dgvRelVendedoresSeries.ReadOnly = True
        Me.dgvRelVendedoresSeries.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvRelVendedoresSeries.Size = New System.Drawing.Size(691, 125)
        Me.dgvRelVendedoresSeries.TabIndex = 36
        Me.dgvRelVendedoresSeries.TabStop = False
        '
        'IdDetalle
        '
        Me.IdDetalle.DataPropertyName = "IdDetalle"
        Me.IdDetalle.HeaderText = "IdDetalle"
        Me.IdDetalle.Name = "IdDetalle"
        Me.IdDetalle.ReadOnly = True
        Me.IdDetalle.Visible = False
        '
        'ClvCompania
        '
        Me.ClvCompania.DataPropertyName = "ClvCompania"
        Me.ClvCompania.HeaderText = "ClvCompania"
        Me.ClvCompania.Name = "ClvCompania"
        Me.ClvCompania.ReadOnly = True
        Me.ClvCompania.Visible = False
        '
        'nombreCompania
        '
        Me.nombreCompania.DataPropertyName = "nombreCompania"
        Me.nombreCompania.HeaderText = "Nombre de la Compañía"
        Me.nombreCompania.Name = "nombreCompania"
        Me.nombreCompania.ReadOnly = True
        Me.nombreCompania.Width = 200
        '
        'FoliosUsados
        '
        Me.FoliosUsados.DataPropertyName = "FoliosUsados"
        Me.FoliosUsados.HeaderText = "Folios Impresos"
        Me.FoliosUsados.Name = "FoliosUsados"
        Me.FoliosUsados.ReadOnly = True
        Me.FoliosUsados.Width = 160
        '
        'Serie
        '
        Me.Serie.DataPropertyName = "Serie"
        Me.Serie.HeaderText = "Serie"
        Me.Serie.Name = "Serie"
        Me.Serie.ReadOnly = True
        '
        'UltimoFolioUsado
        '
        Me.UltimoFolioUsado.DataPropertyName = "UltimoFolioUsado"
        Me.UltimoFolioUsado.HeaderText = "Último Folio Usado"
        Me.UltimoFolioUsado.Name = "UltimoFolioUsado"
        Me.UltimoFolioUsado.ReadOnly = True
        Me.UltimoFolioUsado.Width = 160
        '
        'bnCancelar
        '
        Me.bnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnCancelar.Location = New System.Drawing.Point(467, 210)
        Me.bnCancelar.Name = "bnCancelar"
        Me.bnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.bnCancelar.TabIndex = 32
        Me.bnCancelar.Text = "&Cancelar"
        Me.bnCancelar.UseVisualStyleBackColor = True
        '
        'bnEliminar
        '
        Me.bnEliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnEliminar.Location = New System.Drawing.Point(386, 210)
        Me.bnEliminar.Name = "bnEliminar"
        Me.bnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.bnEliminar.TabIndex = 31
        Me.bnEliminar.Text = "&Eliminar"
        Me.bnEliminar.UseVisualStyleBackColor = True
        '
        'bnGuardar
        '
        Me.bnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnGuardar.Location = New System.Drawing.Point(305, 210)
        Me.bnGuardar.Name = "bnGuardar"
        Me.bnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.bnGuardar.TabIndex = 29
        Me.bnGuardar.Text = "&Guardar"
        Me.bnGuardar.UseVisualStyleBackColor = True
        '
        'bnModificar
        '
        Me.bnModificar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnModificar.Location = New System.Drawing.Point(224, 210)
        Me.bnModificar.Name = "bnModificar"
        Me.bnModificar.Size = New System.Drawing.Size(75, 23)
        Me.bnModificar.TabIndex = 27
        Me.bnModificar.Text = "&Modificar"
        Me.bnModificar.UseVisualStyleBackColor = True
        '
        'bnAgregar
        '
        Me.bnAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnAgregar.Location = New System.Drawing.Point(143, 210)
        Me.bnAgregar.Name = "bnAgregar"
        Me.bnAgregar.Size = New System.Drawing.Size(75, 23)
        Me.bnAgregar.TabIndex = 24
        Me.bnAgregar.Text = "&Agregar"
        Me.bnAgregar.UseVisualStyleBackColor = True
        '
        'CMBlabel5
        '
        Me.CMBlabel5.AutoSize = True
        Me.CMBlabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlabel5.Location = New System.Drawing.Point(130, 166)
        Me.CMBlabel5.Name = "CMBlabel5"
        Me.CMBlabel5.Size = New System.Drawing.Size(138, 15)
        Me.CMBlabel5.TabIndex = 33
        Me.CMBlabel5.Text = "Último Folio Usado :"
        '
        'CMBlabel4
        '
        Me.CMBlabel4.AutoSize = True
        Me.CMBlabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlabel4.Location = New System.Drawing.Point(219, 139)
        Me.CMBlabel4.Name = "CMBlabel4"
        Me.CMBlabel4.Size = New System.Drawing.Size(49, 15)
        Me.CMBlabel4.TabIndex = 30
        Me.CMBlabel4.Text = "Serie :"
        '
        'CMBlabel3
        '
        Me.CMBlabel3.AutoSize = True
        Me.CMBlabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlabel3.Location = New System.Drawing.Point(155, 112)
        Me.CMBlabel3.Name = "CMBlabel3"
        Me.CMBlabel3.Size = New System.Drawing.Size(113, 15)
        Me.CMBlabel3.TabIndex = 28
        Me.CMBlabel3.Text = "Folios Impresos:"
        '
        'CMBlabel2
        '
        Me.CMBlabel2.AutoSize = True
        Me.CMBlabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlabel2.Location = New System.Drawing.Point(97, 85)
        Me.CMBlabel2.Name = "CMBlabel2"
        Me.CMBlabel2.Size = New System.Drawing.Size(171, 15)
        Me.CMBlabel2.TabIndex = 26
        Me.CMBlabel2.Text = "Nombre de la Compañía :"
        '
        'CMBlabel1
        '
        Me.CMBlabel1.AutoSize = True
        Me.CMBlabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlabel1.Location = New System.Drawing.Point(192, 56)
        Me.CMBlabel1.Name = "CMBlabel1"
        Me.CMBlabel1.Size = New System.Drawing.Size(76, 15)
        Me.CMBlabel1.TabIndex = 25
        Me.CMBlabel1.Text = "Vendedor :"
        '
        'tbUltimoFolioUsado
        '
        Me.tbUltimoFolioUsado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbUltimoFolioUsado.Location = New System.Drawing.Point(274, 160)
        Me.tbUltimoFolioUsado.Name = "tbUltimoFolioUsado"
        Me.tbUltimoFolioUsado.Size = New System.Drawing.Size(145, 21)
        Me.tbUltimoFolioUsado.TabIndex = 23
        '
        'tbSerie
        '
        Me.tbSerie.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSerie.Location = New System.Drawing.Point(274, 133)
        Me.tbSerie.Name = "tbSerie"
        Me.tbSerie.Size = New System.Drawing.Size(145, 21)
        Me.tbSerie.TabIndex = 22
        '
        'tbFoliosUsados
        '
        Me.tbFoliosUsados.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbFoliosUsados.Location = New System.Drawing.Point(274, 106)
        Me.tbFoliosUsados.Name = "tbFoliosUsados"
        Me.tbFoliosUsados.Size = New System.Drawing.Size(145, 21)
        Me.tbFoliosUsados.TabIndex = 21
        '
        'cbCompanias
        '
        Me.cbCompanias.DisplayMember = "nombreCompania"
        Me.cbCompanias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbCompanias.FormattingEnabled = True
        Me.cbCompanias.Location = New System.Drawing.Point(274, 77)
        Me.cbCompanias.Name = "cbCompanias"
        Me.cbCompanias.Size = New System.Drawing.Size(283, 23)
        Me.cbCompanias.TabIndex = 20
        Me.cbCompanias.ValueMember = "ClvCompania"
        '
        'cbVendedores
        '
        Me.cbVendedores.DisplayMember = "Nombre"
        Me.cbVendedores.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbVendedores.FormattingEnabled = True
        Me.cbVendedores.Location = New System.Drawing.Point(274, 48)
        Me.cbVendedores.Name = "cbVendedores"
        Me.cbVendedores.Size = New System.Drawing.Size(283, 23)
        Me.cbVendedores.TabIndex = 19
        Me.cbVendedores.ValueMember = "Id"
        '
        'FrmtblRelVendedoresSeries
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(717, 463)
        Me.Controls.Add(Me.bnRelVendedoresSeries)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.dgvRelVendedoresSeries)
        Me.Controls.Add(Me.bnCancelar)
        Me.Controls.Add(Me.bnEliminar)
        Me.Controls.Add(Me.bnGuardar)
        Me.Controls.Add(Me.bnModificar)
        Me.Controls.Add(Me.bnAgregar)
        Me.Controls.Add(Me.CMBlabel5)
        Me.Controls.Add(Me.CMBlabel4)
        Me.Controls.Add(Me.CMBlabel3)
        Me.Controls.Add(Me.CMBlabel2)
        Me.Controls.Add(Me.CMBlabel1)
        Me.Controls.Add(Me.tbUltimoFolioUsado)
        Me.Controls.Add(Me.tbSerie)
        Me.Controls.Add(Me.tbFoliosUsados)
        Me.Controls.Add(Me.cbCompanias)
        Me.Controls.Add(Me.cbVendedores)
        Me.Name = "FrmtblRelVendedoresSeries"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FrmtblRelVendedoresSeries"
        CType(Me.bnRelVendedoresSeries, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnRelVendedoresSeries.ResumeLayout(False)
        Me.bnRelVendedoresSeries.PerformLayout()
        CType(Me.dgvRelVendedoresSeries, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents bnRelVendedoresSeries As System.Windows.Forms.BindingNavigator
    Private WithEvents tsbEliminar As System.Windows.Forms.ToolStripButton
    Private WithEvents tsbGuardar As System.Windows.Forms.ToolStripButton
    Private WithEvents bnSalir As System.Windows.Forms.Button
    Private WithEvents dgvRelVendedoresSeries As System.Windows.Forms.DataGridView
    Private WithEvents IdDetalle As System.Windows.Forms.DataGridViewTextBoxColumn
    Private WithEvents ClvCompania As System.Windows.Forms.DataGridViewTextBoxColumn
    Private WithEvents nombreCompania As System.Windows.Forms.DataGridViewTextBoxColumn
    Private WithEvents FoliosUsados As System.Windows.Forms.DataGridViewTextBoxColumn
    Private WithEvents Serie As System.Windows.Forms.DataGridViewTextBoxColumn
    Private WithEvents UltimoFolioUsado As System.Windows.Forms.DataGridViewTextBoxColumn
    Private WithEvents bnCancelar As System.Windows.Forms.Button
    Private WithEvents bnEliminar As System.Windows.Forms.Button
    Private WithEvents bnGuardar As System.Windows.Forms.Button
    Private WithEvents bnModificar As System.Windows.Forms.Button
    Private WithEvents bnAgregar As System.Windows.Forms.Button
    Private WithEvents CMBlabel5 As System.Windows.Forms.Label
    Private WithEvents CMBlabel4 As System.Windows.Forms.Label
    Private WithEvents CMBlabel3 As System.Windows.Forms.Label
    Private WithEvents CMBlabel2 As System.Windows.Forms.Label
    Private WithEvents CMBlabel1 As System.Windows.Forms.Label
    Private WithEvents tbUltimoFolioUsado As System.Windows.Forms.TextBox
    Private WithEvents tbSerie As System.Windows.Forms.TextBox
    Private WithEvents tbFoliosUsados As System.Windows.Forms.TextBox
    Private WithEvents cbCompanias As System.Windows.Forms.ComboBox
    Private WithEvents cbVendedores As System.Windows.Forms.ComboBox
End Class
