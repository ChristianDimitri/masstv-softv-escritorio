<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMotivosCancelacionFactura
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_MotivoLabel As System.Windows.Forms.Label
        Dim DescripcionLabel As System.Windows.Forms.Label
        Dim BanderaLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMotivosCancelacionFactura))
        Me.DataSetEDGAR = New sofTV.DataSetEDGAR()
        Me.CONMOTIVOSFACTURACANCELACIONBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONMOTIVOSFACTURACANCELACIONTableAdapter = New sofTV.DataSetEDGARTableAdapters.CONMOTIVOSFACTURACANCELACIONTableAdapter()
        Me.CONMOTIVOSFACTURACANCELACIONBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.CONMOTIVOSFACTURACANCELACIONBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Clv_MotivoTextBox = New System.Windows.Forms.TextBox()
        Me.DescripcionTextBox = New System.Windows.Forms.TextBox()
        Me.BanderaTextBox = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Clv_MotivoLabel = New System.Windows.Forms.Label()
        DescripcionLabel = New System.Windows.Forms.Label()
        BanderaLabel = New System.Windows.Forms.Label()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONMOTIVOSFACTURACANCELACIONBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONMOTIVOSFACTURACANCELACIONBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONMOTIVOSFACTURACANCELACIONBindingNavigator.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Clv_MotivoLabel
        '
        Clv_MotivoLabel.AutoSize = True
        Clv_MotivoLabel.Location = New System.Drawing.Point(54, 64)
        Clv_MotivoLabel.Name = "Clv_MotivoLabel"
        Clv_MotivoLabel.Size = New System.Drawing.Size(60, 13)
        Clv_MotivoLabel.TabIndex = 2
        Clv_MotivoLabel.Text = "Clv Motivo:"
        '
        'DescripcionLabel
        '
        DescripcionLabel.AutoSize = True
        DescripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescripcionLabel.Location = New System.Drawing.Point(31, 9)
        DescripcionLabel.Name = "DescripcionLabel"
        DescripcionLabel.Size = New System.Drawing.Size(103, 18)
        DescripcionLabel.TabIndex = 4
        DescripcionLabel.Text = "Descripción:"
        '
        'BanderaLabel
        '
        BanderaLabel.AutoSize = True
        BanderaLabel.Location = New System.Drawing.Point(54, 90)
        BanderaLabel.Name = "BanderaLabel"
        BanderaLabel.Size = New System.Drawing.Size(50, 13)
        BanderaLabel.TabIndex = 6
        BanderaLabel.Text = "Bandera:"
        '
        'DataSetEDGAR
        '
        Me.DataSetEDGAR.DataSetName = "DataSetEDGAR"
        Me.DataSetEDGAR.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CONMOTIVOSFACTURACANCELACIONBindingSource
        '
        Me.CONMOTIVOSFACTURACANCELACIONBindingSource.DataMember = "CONMOTIVOSFACTURACANCELACION"
        Me.CONMOTIVOSFACTURACANCELACIONBindingSource.DataSource = Me.DataSetEDGAR
        '
        'CONMOTIVOSFACTURACANCELACIONTableAdapter
        '
        Me.CONMOTIVOSFACTURACANCELACIONTableAdapter.ClearBeforeFill = True
        '
        'CONMOTIVOSFACTURACANCELACIONBindingNavigator
        '
        Me.CONMOTIVOSFACTURACANCELACIONBindingNavigator.AddNewItem = Nothing
        Me.CONMOTIVOSFACTURACANCELACIONBindingNavigator.BindingSource = Me.CONMOTIVOSFACTURACANCELACIONBindingSource
        Me.CONMOTIVOSFACTURACANCELACIONBindingNavigator.CountItem = Nothing
        Me.CONMOTIVOSFACTURACANCELACIONBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONMOTIVOSFACTURACANCELACIONBindingNavigator.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONMOTIVOSFACTURACANCELACIONBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.CONMOTIVOSFACTURACANCELACIONBindingNavigatorSaveItem})
        Me.CONMOTIVOSFACTURACANCELACIONBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONMOTIVOSFACTURACANCELACIONBindingNavigator.MoveFirstItem = Nothing
        Me.CONMOTIVOSFACTURACANCELACIONBindingNavigator.MoveLastItem = Nothing
        Me.CONMOTIVOSFACTURACANCELACIONBindingNavigator.MoveNextItem = Nothing
        Me.CONMOTIVOSFACTURACANCELACIONBindingNavigator.MovePreviousItem = Nothing
        Me.CONMOTIVOSFACTURACANCELACIONBindingNavigator.Name = "CONMOTIVOSFACTURACANCELACIONBindingNavigator"
        Me.CONMOTIVOSFACTURACANCELACIONBindingNavigator.PositionItem = Nothing
        Me.CONMOTIVOSFACTURACANCELACIONBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONMOTIVOSFACTURACANCELACIONBindingNavigator.Size = New System.Drawing.Size(551, 25)
        Me.CONMOTIVOSFACTURACANCELACIONBindingNavigator.TabIndex = 110
        Me.CONMOTIVOSFACTURACANCELACIONBindingNavigator.TabStop = True
        Me.CONMOTIVOSFACTURACANCELACIONBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(90, 22)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'CONMOTIVOSFACTURACANCELACIONBindingNavigatorSaveItem
        '
        Me.CONMOTIVOSFACTURACANCELACIONBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONMOTIVOSFACTURACANCELACIONBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONMOTIVOSFACTURACANCELACIONBindingNavigatorSaveItem.Name = "CONMOTIVOSFACTURACANCELACIONBindingNavigatorSaveItem"
        Me.CONMOTIVOSFACTURACANCELACIONBindingNavigatorSaveItem.Size = New System.Drawing.Size(91, 22)
        Me.CONMOTIVOSFACTURACANCELACIONBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'Clv_MotivoTextBox
        '
        Me.Clv_MotivoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONMOTIVOSFACTURACANCELACIONBindingSource, "Clv_Motivo", True))
        Me.Clv_MotivoTextBox.Location = New System.Drawing.Point(126, 61)
        Me.Clv_MotivoTextBox.Name = "Clv_MotivoTextBox"
        Me.Clv_MotivoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Clv_MotivoTextBox.TabIndex = 3
        Me.Clv_MotivoTextBox.TabStop = False
        '
        'DescripcionTextBox
        '
        Me.DescripcionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONMOTIVOSFACTURACANCELACIONBindingSource, "Descripcion", True))
        Me.DescripcionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionTextBox.Location = New System.Drawing.Point(15, 42)
        Me.DescripcionTextBox.Multiline = True
        Me.DescripcionTextBox.Name = "DescripcionTextBox"
        Me.DescripcionTextBox.Size = New System.Drawing.Size(477, 212)
        Me.DescripcionTextBox.TabIndex = 100
        '
        'BanderaTextBox
        '
        Me.BanderaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONMOTIVOSFACTURACANCELACIONBindingSource, "Bandera", True))
        Me.BanderaTextBox.Location = New System.Drawing.Point(126, 87)
        Me.BanderaTextBox.Name = "BanderaTextBox"
        Me.BanderaTextBox.Size = New System.Drawing.Size(100, 20)
        Me.BanderaTextBox.TabIndex = 7
        Me.BanderaTextBox.TabStop = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(397, 323)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(135, 35)
        Me.Button1.TabIndex = 120
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.DescripcionTextBox)
        Me.Panel1.Controls.Add(DescripcionLabel)
        Me.Panel1.Controls.Add(Clv_MotivoLabel)
        Me.Panel1.Controls.Add(Me.Clv_MotivoTextBox)
        Me.Panel1.Controls.Add(Me.BanderaTextBox)
        Me.Panel1.Controls.Add(BanderaLabel)
        Me.Panel1.Location = New System.Drawing.Point(0, 28)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(532, 271)
        Me.Panel1.TabIndex = 9
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'FrmMotivosCancelacionFactura
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(551, 379)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.CONMOTIVOSFACTURACANCELACIONBindingNavigator)
        Me.Name = "FrmMotivosCancelacionFactura"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Motivos de Cancelación de Factura"
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONMOTIVOSFACTURACANCELACIONBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONMOTIVOSFACTURACANCELACIONBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONMOTIVOSFACTURACANCELACIONBindingNavigator.ResumeLayout(False)
        Me.CONMOTIVOSFACTURACANCELACIONBindingNavigator.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataSetEDGAR As sofTV.DataSetEDGAR
    Friend WithEvents CONMOTIVOSFACTURACANCELACIONBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONMOTIVOSFACTURACANCELACIONTableAdapter As sofTV.DataSetEDGARTableAdapters.CONMOTIVOSFACTURACANCELACIONTableAdapter
    Friend WithEvents CONMOTIVOSFACTURACANCELACIONBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONMOTIVOSFACTURACANCELACIONBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Clv_MotivoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DescripcionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents BanderaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
