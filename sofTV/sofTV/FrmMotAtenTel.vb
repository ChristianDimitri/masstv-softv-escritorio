Imports System.Data.SqlClient
Public Class FrmMotAtenTel
    Private descr As String = Nothing
    Private Sub damedatosbitacora()
        Try
            If eOpcion = "M" Then
                descr = Me.DescripcionTextBox.Text
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub guardabitacora(ByVal op As Integer)
        Try
            Select Case op
                Case 0
                    If eOpcion = "M" Then
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.DescripcionTextBox.Name, descr, Me.DescripcionTextBox.Text, LocClv_Ciudad)
                    ElseIf eOpcion = "N" Then
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Agrego Un Nuevo Motivo De Atencion Telefonica", "", "Agrego Un Nuevo Motivo De Atencion Telefonica: " + Me.DescripcionTextBox.Text, LocClv_Ciudad)
                    End If
                Case 1
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Elimino Motivo De Atencion Telefonica", "", "Se Elimino un Motivo De Atencion Telefonica: " + Me.DescripcionTextBox.Text, LocClv_Ciudad)
            End Select
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FrmMotAtenTel_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

    End Sub

    Private Sub FrmMotAtenTel_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Me.Button1.ForeColor = Color.White
        Me.Button1.BackColor = Color.Gray
        Dim CON As New SqlConnection(MiConexion)
        If eOpcion = "N" Then
            Me.BindingNavigatorDeleteItem.Enabled = False
        End If

        If eOpcion = "M" Then
            CON.Open()
            Me.ConMotAtenTelTableAdapter.Connection = CON
            Me.ConMotAtenTelTableAdapter.Fill(Me.DataSetEric.ConMotAtenTel, eClv_MotAtenTel, "", 2)
            CON.Close()
        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub ConMotAtenTelBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConMotAtenTelBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)

        If Me.DescripcionTextBox.Text.Length = 0 Then
            MsgBox("Captura la Descripci�n.", , "Atenci�n")
            Exit Sub
        End If

        If eOpcion = "N" Then
            CON.Open()
            Me.ConMotAtenTelTableAdapter.Connection = CON
            Me.ConMotAtenTelTableAdapter.Insert(Me.DescripcionTextBox.Text)
            CON.Close()
            guardabitacora(0)
            MsgBox(mensaje5)
            Me.Close()
        End If
        If eOpcion = "M" Then
            CON.Open()
            Me.ConMotAtenTelTableAdapter.Connection = CON
            Me.ConMotAtenTelTableAdapter.Update(eClv_MotAtenTel, Me.DescripcionTextBox.Text)
            CON.Close()
            guardabitacora(0)
            MsgBox(mensaje5)
            Me.Close()
        End If

    End Sub

    Private Sub BindingNavigatorDeleteItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        Try
            CON.Open()
            Me.ConMotAtenTelTableAdapter.Connection = CON
            Me.ConMotAtenTelTableAdapter.Delete(eClv_MotAtenTel)
            MsgBox(mensaje6)
              guardabitacora(1)
            Me.Close()
            CON.Close()
        Catch ex As Exception
            MsgBox("Error.")
        End Try
    End Sub
End Class