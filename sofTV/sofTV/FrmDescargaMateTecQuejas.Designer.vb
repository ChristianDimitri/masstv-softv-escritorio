<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDescargaMateTecQuejas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.DataSetEdgarRev2 = New sofTV.DataSetEdgarRev2
        Me.ConRel_Session_TecnicosDataGridView = New System.Windows.Forms.DataGridView
        Me.NoArticuloDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ClaveDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ConceptoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CantidadSurtidaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.MovimientoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.NoBitacoraDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ConRel_Session_Tecnicos_QBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLidia = New sofTV.DataSetLidia
        Me.DataSetEDGAR = New sofTV.DataSetEDGAR
        Me.NoBitacoraTextBox = New System.Windows.Forms.TextBox
        Me.DimeSiTieneunaBitacora_QBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MovimientoTextBox = New System.Windows.Forms.TextBox
        Me.NoArticuloTextBox = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.MuestraDescripcionArticuloBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetarnoldo = New sofTV.DataSetarnoldo
        Me.Borra_Inserta_Tempo_Por_ServicioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Borra_Inserta_Tempo_Por_ServicioTableAdapter = New sofTV.DataSetEdgarRev2TableAdapters.Borra_Inserta_Tempo_Por_ServicioTableAdapter
        Me.RespuestaTextBox = New System.Windows.Forms.TextBox
        Me.ValidaExistenciasTecnicosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button7 = New System.Windows.Forms.Button
        Me.ComboBox3 = New System.Windows.Forms.ComboBox
        Me.ComboBox2 = New System.Windows.Forms.ComboBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Button6 = New System.Windows.Forms.Button
        Me.ClaveTextBox = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.MuestraDetalleBitacoraBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label2 = New System.Windows.Forms.Label
        Me.ConceptoTextBox = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.CantidadSurtidaTextBox = New System.Windows.Forms.TextBox
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.OK = New System.Windows.Forms.Button
        Me.Button5 = New System.Windows.Forms.Button
        Me.Inserta_Valores_TempoTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Inserta_Valores_TempoTableAdapter
        Me.Inserta_Valores_TempoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CMBLabel1 = New System.Windows.Forms.Label
        Me.Muestra_Descripcion_ArticuloTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Muestra_Descripcion_ArticuloTableAdapter
        Me.Muestra_Detalle_BitacoraTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Muestra_Detalle_BitacoraTableAdapter
        Me.ValidaExistenciasTecnicosTableAdapter = New sofTV.DataSetEDGARTableAdapters.ValidaExistenciasTecnicosTableAdapter
        Me.Button4 = New System.Windows.Forms.Button
        Me.ConRel_Session_Tecnicos_QTableAdapter = New sofTV.DataSetLidiaTableAdapters.ConRel_Session_Tecnicos_QTableAdapter
        Me.Quita_SEBORROART_SALTEC_QBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Quita_SEBORROART_SALTEC_QTableAdapter = New sofTV.DataSetLidiaTableAdapters.Quita_SEBORROART_SALTEC_QTableAdapter
        Me.ValidaDuplicadoRel_Session_Tecnicos_QBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ValidaDuplicadoRel_Session_Tecnicos_QTableAdapter = New sofTV.DataSetLidiaTableAdapters.ValidaDuplicadoRel_Session_Tecnicos_QTableAdapter
        Me.Dame_Folio_QBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_Folio_QTableAdapter = New sofTV.DataSetLidiaTableAdapters.Dame_Folio_QTableAdapter
        Me.Inserta_Rel_Bitacora_QuejaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Rel_Bitacora_QuejaTableAdapter = New sofTV.DataSetLidiaTableAdapters.Inserta_Rel_Bitacora_QuejaTableAdapter
        Me.Inserta_Bitacora_tec_QBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Bitacora_tec_QTableAdapter = New sofTV.DataSetLidiaTableAdapters.Inserta_Bitacora_tec_QTableAdapter
        Me.Borra_Inserta_Tempo_QBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Borra_Inserta_Tempo_QTableAdapter = New sofTV.DataSetLidiaTableAdapters.Borra_Inserta_Tempo_QTableAdapter
        Me.INSERTA_Borra_Articulo_Bitacora_QBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.INSERTA_Borra_Articulo_Bitacora_QTableAdapter = New sofTV.DataSetLidiaTableAdapters.INSERTA_Borra_Articulo_Bitacora_QTableAdapter
        Me.DimeSiTieneunaBitacora_QTableAdapter = New sofTV.DataSetLidiaTableAdapters.DimeSiTieneunaBitacora_QTableAdapter
        Me.Revisa_Tempo_QBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Revisa_Tempo_QTableAdapter = New sofTV.DataSetLidiaTableAdapters.Revisa_Tempo_QTableAdapter
        Me.TextBox4 = New System.Windows.Forms.TextBox
        Me.Clv_tipo = New System.Windows.Forms.TextBox
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2
        Me.Inserta_RelCobraDescBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_RelCobraDescTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_RelCobraDescTableAdapter
        Me.ComboBox5 = New System.Windows.Forms.ComboBox
        Me.ComboBox4 = New System.Windows.Forms.ComboBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.Almacen = New System.Windows.Forms.ComboBox
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConRel_Session_TecnicosDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConRel_Session_Tecnicos_QBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DimeSiTieneunaBitacora_QBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraDescripcionArticuloBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Borra_Inserta_Tempo_Por_ServicioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ValidaExistenciasTecnicosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraDetalleBitacoraBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.Inserta_Valores_TempoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Quita_SEBORROART_SALTEC_QBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ValidaDuplicadoRel_Session_Tecnicos_QBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_Folio_QBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Rel_Bitacora_QuejaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Bitacora_tec_QBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Borra_Inserta_Tempo_QBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.INSERTA_Borra_Articulo_Bitacora_QBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Revisa_Tempo_QBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_RelCobraDescBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataSetEdgarRev2
        '
        Me.DataSetEdgarRev2.DataSetName = "DataSetEdgarRev2"
        Me.DataSetEdgarRev2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ConRel_Session_TecnicosDataGridView
        '
        Me.ConRel_Session_TecnicosDataGridView.AllowUserToAddRows = False
        Me.ConRel_Session_TecnicosDataGridView.AllowUserToDeleteRows = False
        Me.ConRel_Session_TecnicosDataGridView.AutoGenerateColumns = False
        Me.ConRel_Session_TecnicosDataGridView.BackgroundColor = System.Drawing.Color.White
        Me.ConRel_Session_TecnicosDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NoArticuloDataGridViewTextBoxColumn, Me.ClaveDataGridViewTextBoxColumn, Me.ConceptoDataGridViewTextBoxColumn, Me.CantidadSurtidaDataGridViewTextBoxColumn, Me.MovimientoDataGridViewTextBoxColumn, Me.NoBitacoraDataGridViewTextBoxColumn})
        Me.ConRel_Session_TecnicosDataGridView.DataSource = Me.ConRel_Session_Tecnicos_QBindingSource
        Me.ConRel_Session_TecnicosDataGridView.GridColor = System.Drawing.Color.WhiteSmoke
        Me.ConRel_Session_TecnicosDataGridView.Location = New System.Drawing.Point(51, 229)
        Me.ConRel_Session_TecnicosDataGridView.MultiSelect = False
        Me.ConRel_Session_TecnicosDataGridView.Name = "ConRel_Session_TecnicosDataGridView"
        Me.ConRel_Session_TecnicosDataGridView.ReadOnly = True
        Me.ConRel_Session_TecnicosDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.ConRel_Session_TecnicosDataGridView.Size = New System.Drawing.Size(730, 271)
        Me.ConRel_Session_TecnicosDataGridView.TabIndex = 46
        Me.ConRel_Session_TecnicosDataGridView.TabStop = False
        '
        'NoArticuloDataGridViewTextBoxColumn
        '
        Me.NoArticuloDataGridViewTextBoxColumn.DataPropertyName = "NoArticulo"
        Me.NoArticuloDataGridViewTextBoxColumn.HeaderText = ""
        Me.NoArticuloDataGridViewTextBoxColumn.Name = "NoArticuloDataGridViewTextBoxColumn"
        Me.NoArticuloDataGridViewTextBoxColumn.ReadOnly = True
        Me.NoArticuloDataGridViewTextBoxColumn.Width = 5
        '
        'ClaveDataGridViewTextBoxColumn
        '
        Me.ClaveDataGridViewTextBoxColumn.DataPropertyName = "Clave"
        Me.ClaveDataGridViewTextBoxColumn.HeaderText = "Clave"
        Me.ClaveDataGridViewTextBoxColumn.Name = "ClaveDataGridViewTextBoxColumn"
        Me.ClaveDataGridViewTextBoxColumn.ReadOnly = True
        Me.ClaveDataGridViewTextBoxColumn.Width = 125
        '
        'ConceptoDataGridViewTextBoxColumn
        '
        Me.ConceptoDataGridViewTextBoxColumn.DataPropertyName = "concepto"
        Me.ConceptoDataGridViewTextBoxColumn.HeaderText = "concepto"
        Me.ConceptoDataGridViewTextBoxColumn.Name = "ConceptoDataGridViewTextBoxColumn"
        Me.ConceptoDataGridViewTextBoxColumn.ReadOnly = True
        Me.ConceptoDataGridViewTextBoxColumn.Width = 400
        '
        'CantidadSurtidaDataGridViewTextBoxColumn
        '
        Me.CantidadSurtidaDataGridViewTextBoxColumn.DataPropertyName = "CantidadSurtida"
        Me.CantidadSurtidaDataGridViewTextBoxColumn.HeaderText = "CantidadSurtida"
        Me.CantidadSurtidaDataGridViewTextBoxColumn.Name = "CantidadSurtidaDataGridViewTextBoxColumn"
        Me.CantidadSurtidaDataGridViewTextBoxColumn.ReadOnly = True
        Me.CantidadSurtidaDataGridViewTextBoxColumn.Width = 150
        '
        'MovimientoDataGridViewTextBoxColumn
        '
        Me.MovimientoDataGridViewTextBoxColumn.DataPropertyName = "Movimiento"
        Me.MovimientoDataGridViewTextBoxColumn.HeaderText = ""
        Me.MovimientoDataGridViewTextBoxColumn.Name = "MovimientoDataGridViewTextBoxColumn"
        Me.MovimientoDataGridViewTextBoxColumn.ReadOnly = True
        Me.MovimientoDataGridViewTextBoxColumn.Width = 5
        '
        'NoBitacoraDataGridViewTextBoxColumn
        '
        Me.NoBitacoraDataGridViewTextBoxColumn.DataPropertyName = "NoBitacora"
        Me.NoBitacoraDataGridViewTextBoxColumn.HeaderText = "NoBitacora"
        Me.NoBitacoraDataGridViewTextBoxColumn.Name = "NoBitacoraDataGridViewTextBoxColumn"
        Me.NoBitacoraDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ConRel_Session_Tecnicos_QBindingSource
        '
        Me.ConRel_Session_Tecnicos_QBindingSource.DataMember = "ConRel_Session_Tecnicos_Q"
        Me.ConRel_Session_Tecnicos_QBindingSource.DataSource = Me.DataSetLidia
        '
        'DataSetLidia
        '
        Me.DataSetLidia.DataSetName = "DataSetLidia"
        Me.DataSetLidia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DataSetEDGAR
        '
        Me.DataSetEDGAR.DataSetName = "DataSetEDGAR"
        Me.DataSetEDGAR.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'NoBitacoraTextBox
        '
        Me.NoBitacoraTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DimeSiTieneunaBitacora_QBindingSource, "Folio", True))
        Me.NoBitacoraTextBox.Enabled = False
        Me.NoBitacoraTextBox.Location = New System.Drawing.Point(266, 58)
        Me.NoBitacoraTextBox.Name = "NoBitacoraTextBox"
        Me.NoBitacoraTextBox.Size = New System.Drawing.Size(96, 20)
        Me.NoBitacoraTextBox.TabIndex = 0
        Me.NoBitacoraTextBox.TabStop = False
        '
        'DimeSiTieneunaBitacora_QBindingSource
        '
        Me.DimeSiTieneunaBitacora_QBindingSource.DataMember = "DimeSiTieneunaBitacora_Q"
        Me.DimeSiTieneunaBitacora_QBindingSource.DataSource = Me.DataSetLidia
        '
        'MovimientoTextBox
        '
        Me.MovimientoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConRel_Session_Tecnicos_QBindingSource, "Movimiento", True))
        Me.MovimientoTextBox.Location = New System.Drawing.Point(628, 367)
        Me.MovimientoTextBox.Name = "MovimientoTextBox"
        Me.MovimientoTextBox.Size = New System.Drawing.Size(35, 20)
        Me.MovimientoTextBox.TabIndex = 53
        Me.MovimientoTextBox.TabStop = False
        '
        'NoArticuloTextBox
        '
        Me.NoArticuloTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConRel_Session_Tecnicos_QBindingSource, "NoArticulo", True))
        Me.NoArticuloTextBox.Location = New System.Drawing.Point(704, 350)
        Me.NoArticuloTextBox.Name = "NoArticuloTextBox"
        Me.NoArticuloTextBox.Size = New System.Drawing.Size(35, 20)
        Me.NoArticuloTextBox.TabIndex = 52
        Me.NoArticuloTextBox.TabStop = False
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label8.Location = New System.Drawing.Point(610, 5)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(111, 17)
        Me.Label8.TabIndex = 30
        Me.Label8.Text = "Cantidad"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox3
        '
        Me.TextBox3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraDescripcionArticuloBindingSource, "CLAVE", True))
        Me.TextBox3.Enabled = False
        Me.TextBox3.Location = New System.Drawing.Point(528, 434)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(46, 20)
        Me.TextBox3.TabIndex = 45
        Me.TextBox3.TabStop = False
        '
        'MuestraDescripcionArticuloBindingSource
        '
        Me.MuestraDescripcionArticuloBindingSource.DataMember = "Muestra_Descripcion_Articulo"
        Me.MuestraDescripcionArticuloBindingSource.DataSource = Me.DataSetarnoldo
        '
        'DataSetarnoldo
        '
        Me.DataSetarnoldo.DataSetName = "DataSetarnoldo"
        Me.DataSetarnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Borra_Inserta_Tempo_Por_ServicioBindingSource
        '
        Me.Borra_Inserta_Tempo_Por_ServicioBindingSource.DataMember = "Borra_Inserta_Tempo_Por_Servicio"
        Me.Borra_Inserta_Tempo_Por_ServicioBindingSource.DataSource = Me.DataSetEdgarRev2
        '
        'Borra_Inserta_Tempo_Por_ServicioTableAdapter
        '
        Me.Borra_Inserta_Tempo_Por_ServicioTableAdapter.ClearBeforeFill = True
        '
        'RespuestaTextBox
        '
        Me.RespuestaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RespuestaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ValidaExistenciasTecnicosBindingSource, "Respuesta", True))
        Me.RespuestaTextBox.ForeColor = System.Drawing.Color.White
        Me.RespuestaTextBox.Location = New System.Drawing.Point(717, 169)
        Me.RespuestaTextBox.Name = "RespuestaTextBox"
        Me.RespuestaTextBox.ReadOnly = True
        Me.RespuestaTextBox.Size = New System.Drawing.Size(38, 13)
        Me.RespuestaTextBox.TabIndex = 4
        Me.RespuestaTextBox.TabStop = False
        '
        'ValidaExistenciasTecnicosBindingSource
        '
        Me.ValidaExistenciasTecnicosBindingSource.DataMember = "ValidaExistenciasTecnicos"
        Me.ValidaExistenciasTecnicosBindingSource.DataSource = Me.DataSetEDGAR
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(223, 350)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(127, 30)
        Me.Button2.TabIndex = 49
        Me.Button2.TabStop = False
        Me.Button2.Text = "&MODIFICAR"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(166, 193)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(127, 30)
        Me.Button3.TabIndex = 6
        Me.Button3.Text = "&ELIMINAR"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label7.Location = New System.Drawing.Point(115, 5)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(489, 17)
        Me.Label7.TabIndex = 29
        Me.Label7.Text = "Descripción"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label6.Location = New System.Drawing.Point(11, 5)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(98, 17)
        Me.Label6.TabIndex = 28
        Me.Label6.Text = "Clave "
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(51, 193)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(109, 30)
        Me.Button1.TabIndex = 5
        Me.Button1.Text = "&AGREGAR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.DarkOrange
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.Location = New System.Drawing.Point(539, 53)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(182, 30)
        Me.Button7.TabIndex = 24
        Me.Button7.TabStop = False
        Me.Button7.Text = "&Cancelar Modificacion"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'ComboBox3
        '
        Me.ComboBox3.FormattingEnabled = True
        Me.ComboBox3.Location = New System.Drawing.Point(192, 166)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(471, 21)
        Me.ComboBox3.TabIndex = 3
        '
        'ComboBox2
        '
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(51, 166)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(135, 21)
        Me.ComboBox2.TabIndex = 2
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(669, 147)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(112, 16)
        Me.Label5.TabIndex = 38
        Me.Label5.Text = "Cantidad"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(48, 147)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(138, 16)
        Me.Label4.TabIndex = 37
        Me.Label4.Text = "Clave"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkOrange
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(353, 55)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(180, 27)
        Me.Button6.TabIndex = 23
        Me.Button6.TabStop = False
        Me.Button6.Text = "&Guardar Modificacion"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'ClaveTextBox
        '
        Me.ClaveTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConRel_Session_Tecnicos_QBindingSource, "Clave", True))
        Me.ClaveTextBox.Location = New System.Drawing.Point(14, 25)
        Me.ClaveTextBox.Name = "ClaveTextBox"
        Me.ClaveTextBox.Size = New System.Drawing.Size(95, 20)
        Me.ClaveTextBox.TabIndex = 25
        Me.ClaveTextBox.TabStop = False
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(192, 147)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(471, 16)
        Me.Label3.TabIndex = 36
        Me.Label3.Text = "Descripción de Articulo"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(266, 112)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(344, 21)
        Me.ComboBox1.TabIndex = 1
        '
        'MuestraDetalleBitacoraBindingSource
        '
        Me.MuestraDetalleBitacoraBindingSource.DataMember = "Muestra_Detalle_Bitacora"
        Me.MuestraDetalleBitacoraBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(73, 113)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(187, 16)
        Me.Label2.TabIndex = 34
        Me.Label2.Text = "Clasificación del Material:"
        '
        'ConceptoTextBox
        '
        Me.ConceptoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConRel_Session_Tecnicos_QBindingSource, "concepto", True))
        Me.ConceptoTextBox.Location = New System.Drawing.Point(115, 25)
        Me.ConceptoTextBox.Name = "ConceptoTextBox"
        Me.ConceptoTextBox.Size = New System.Drawing.Size(489, 20)
        Me.ConceptoTextBox.TabIndex = 26
        Me.ConceptoTextBox.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(131, 59)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(102, 16)
        Me.Label1.TabIndex = 55
        Me.Label1.Text = "No. Bitacora :"
        '
        'TextBox2
        '
        Me.TextBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraDescripcionArticuloBindingSource, "realno_articulo", True))
        Me.TextBox2.Enabled = False
        Me.TextBox2.Location = New System.Drawing.Point(511, 434)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(49, 20)
        Me.TextBox2.TabIndex = 44
        Me.TextBox2.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Button7)
        Me.Panel1.Controls.Add(Me.Button6)
        Me.Panel1.Controls.Add(Me.ClaveTextBox)
        Me.Panel1.Controls.Add(Me.ConceptoTextBox)
        Me.Panel1.Controls.Add(Me.CantidadSurtidaTextBox)
        Me.Panel1.Location = New System.Drawing.Point(65, 242)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(705, 86)
        Me.Panel1.TabIndex = 56
        '
        'CantidadSurtidaTextBox
        '
        Me.CantidadSurtidaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConRel_Session_Tecnicos_QBindingSource, "CantidadSurtida", True))
        Me.CantidadSurtidaTextBox.Location = New System.Drawing.Point(610, 25)
        Me.CantidadSurtidaTextBox.Name = "CantidadSurtidaTextBox"
        Me.CantidadSurtidaTextBox.Size = New System.Drawing.Size(111, 20)
        Me.CantidadSurtidaTextBox.TabIndex = 27
        Me.CantidadSurtidaTextBox.TabStop = False
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(669, 166)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(112, 20)
        Me.TextBox1.TabIndex = 4
        '
        'OK
        '
        Me.OK.BackColor = System.Drawing.Color.DarkOrange
        Me.OK.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.OK.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OK.Location = New System.Drawing.Point(370, 522)
        Me.OK.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.OK.Name = "OK"
        Me.OK.Size = New System.Drawing.Size(155, 33)
        Me.OK.TabIndex = 7
        Me.OK.Text = "&ACEPTAR"
        Me.OK.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(645, 522)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 9
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Inserta_Valores_TempoTableAdapter
        '
        Me.Inserta_Valores_TempoTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Valores_TempoBindingSource
        '
        Me.Inserta_Valores_TempoBindingSource.DataMember = "Inserta_Valores_Tempo"
        Me.Inserta_Valores_TempoBindingSource.DataSource = Me.DataSetarnoldo
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(48, 16)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(305, 16)
        Me.CMBLabel1.TabIndex = 33
        Me.CMBLabel1.Text = "Detalle de Entrega de Material a Tecnicos."
        '
        'Muestra_Descripcion_ArticuloTableAdapter
        '
        Me.Muestra_Descripcion_ArticuloTableAdapter.ClearBeforeFill = True
        '
        'Muestra_Detalle_BitacoraTableAdapter
        '
        Me.Muestra_Detalle_BitacoraTableAdapter.ClearBeforeFill = True
        '
        'ValidaExistenciasTecnicosTableAdapter
        '
        Me.ValidaExistenciasTecnicosTableAdapter.ClearBeforeFill = True
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkOrange
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(483, 522)
        Me.Button4.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(155, 33)
        Me.Button4.TabIndex = 8
        Me.Button4.Text = "&GUARDAR"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'ConRel_Session_Tecnicos_QTableAdapter
        '
        Me.ConRel_Session_Tecnicos_QTableAdapter.ClearBeforeFill = True
        '
        'Quita_SEBORROART_SALTEC_QBindingSource
        '
        Me.Quita_SEBORROART_SALTEC_QBindingSource.DataMember = "Quita_SEBORROART_SALTEC_Q"
        Me.Quita_SEBORROART_SALTEC_QBindingSource.DataSource = Me.DataSetLidia
        '
        'Quita_SEBORROART_SALTEC_QTableAdapter
        '
        Me.Quita_SEBORROART_SALTEC_QTableAdapter.ClearBeforeFill = True
        '
        'ValidaDuplicadoRel_Session_Tecnicos_QBindingSource
        '
        Me.ValidaDuplicadoRel_Session_Tecnicos_QBindingSource.DataMember = "ValidaDuplicadoRel_Session_Tecnicos_Q"
        Me.ValidaDuplicadoRel_Session_Tecnicos_QBindingSource.DataSource = Me.DataSetLidia
        '
        'ValidaDuplicadoRel_Session_Tecnicos_QTableAdapter
        '
        Me.ValidaDuplicadoRel_Session_Tecnicos_QTableAdapter.ClearBeforeFill = True
        '
        'Dame_Folio_QBindingSource
        '
        Me.Dame_Folio_QBindingSource.DataMember = "Dame_Folio_Q"
        Me.Dame_Folio_QBindingSource.DataSource = Me.DataSetLidia
        '
        'Dame_Folio_QTableAdapter
        '
        Me.Dame_Folio_QTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Rel_Bitacora_QuejaBindingSource
        '
        Me.Inserta_Rel_Bitacora_QuejaBindingSource.DataMember = "Inserta_Rel_Bitacora_Queja"
        Me.Inserta_Rel_Bitacora_QuejaBindingSource.DataSource = Me.DataSetLidia
        '
        'Inserta_Rel_Bitacora_QuejaTableAdapter
        '
        Me.Inserta_Rel_Bitacora_QuejaTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Bitacora_tec_QBindingSource
        '
        Me.Inserta_Bitacora_tec_QBindingSource.DataSource = Me.DataSetLidia
        Me.Inserta_Bitacora_tec_QBindingSource.Position = 0
        '
        'Inserta_Bitacora_tec_QTableAdapter
        '
        Me.Inserta_Bitacora_tec_QTableAdapter.ClearBeforeFill = True
        '
        'Borra_Inserta_Tempo_QBindingSource
        '
        Me.Borra_Inserta_Tempo_QBindingSource.DataMember = "Borra_Inserta_Tempo_Q"
        Me.Borra_Inserta_Tempo_QBindingSource.DataSource = Me.DataSetLidia
        '
        'Borra_Inserta_Tempo_QTableAdapter
        '
        Me.Borra_Inserta_Tempo_QTableAdapter.ClearBeforeFill = True
        '
        'INSERTA_Borra_Articulo_Bitacora_QBindingSource
        '
        Me.INSERTA_Borra_Articulo_Bitacora_QBindingSource.DataMember = "INSERTA_Borra_Articulo_Bitacora_Q"
        Me.INSERTA_Borra_Articulo_Bitacora_QBindingSource.DataSource = Me.DataSetLidia
        '
        'INSERTA_Borra_Articulo_Bitacora_QTableAdapter
        '
        Me.INSERTA_Borra_Articulo_Bitacora_QTableAdapter.ClearBeforeFill = True
        '
        'DimeSiTieneunaBitacora_QTableAdapter
        '
        Me.DimeSiTieneunaBitacora_QTableAdapter.ClearBeforeFill = True
        '
        'Revisa_Tempo_QBindingSource
        '
        Me.Revisa_Tempo_QBindingSource.DataMember = "Revisa_Tempo_Q"
        Me.Revisa_Tempo_QBindingSource.DataSource = Me.DataSetLidia
        '
        'Revisa_Tempo_QTableAdapter
        '
        Me.Revisa_Tempo_QTableAdapter.ClearBeforeFill = True
        '
        'TextBox4
        '
        Me.TextBox4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox4.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox4.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox4.Location = New System.Drawing.Point(12, 59)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ReadOnly = True
        Me.TextBox4.Size = New System.Drawing.Size(100, 13)
        Me.TextBox4.TabIndex = 57
        '
        'Clv_tipo
        '
        Me.Clv_tipo.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_tipo.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_tipo.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_tipo.Location = New System.Drawing.Point(645, 94)
        Me.Clv_tipo.Name = "Clv_tipo"
        Me.Clv_tipo.ReadOnly = True
        Me.Clv_tipo.Size = New System.Drawing.Size(100, 13)
        Me.Clv_tipo.TabIndex = 58
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Inserta_RelCobraDescBindingSource
        '
        Me.Inserta_RelCobraDescBindingSource.DataMember = "Inserta_RelCobraDesc"
        Me.Inserta_RelCobraDescBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Inserta_RelCobraDescTableAdapter
        '
        Me.Inserta_RelCobraDescTableAdapter.ClearBeforeFill = True
        '
        'ComboBox5
        '
        Me.ComboBox5.FormattingEnabled = True
        Me.ComboBox5.Location = New System.Drawing.Point(628, 41)
        Me.ComboBox5.Name = "ComboBox5"
        Me.ComboBox5.Size = New System.Drawing.Size(70, 21)
        Me.ComboBox5.TabIndex = 436
        Me.ComboBox5.Visible = False
        '
        'ComboBox4
        '
        Me.ComboBox4.FormattingEnabled = True
        Me.ComboBox4.Location = New System.Drawing.Point(628, 14)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(70, 21)
        Me.ComboBox4.TabIndex = 435
        Me.ComboBox4.Visible = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(182, 83)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(76, 16)
        Me.Label12.TabIndex = 434
        Me.Label12.Text = "Almacen :"
        '
        'Almacen
        '
        Me.Almacen.DisplayMember = "Nombre"
        Me.Almacen.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Almacen.FormattingEnabled = True
        Me.Almacen.Location = New System.Drawing.Point(266, 83)
        Me.Almacen.Name = "Almacen"
        Me.Almacen.Size = New System.Drawing.Size(344, 23)
        Me.Almacen.TabIndex = 433
        Me.Almacen.ValueMember = "clv_Tecnico"
        '
        'FrmDescargaMateTecQuejas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(839, 567)
        Me.Controls.Add(Me.ComboBox5)
        Me.Controls.Add(Me.ComboBox4)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Almacen)
        Me.Controls.Add(Me.Clv_tipo)
        Me.Controls.Add(Me.TextBox4)
        Me.Controls.Add(Me.ConRel_Session_TecnicosDataGridView)
        Me.Controls.Add(Me.NoBitacoraTextBox)
        Me.Controls.Add(Me.MovimientoTextBox)
        Me.Controls.Add(Me.NoArticuloTextBox)
        Me.Controls.Add(Me.TextBox3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ComboBox3)
        Me.Controls.Add(Me.ComboBox2)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.OK)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.RespuestaTextBox)
        Me.Name = "FrmDescargaMateTecQuejas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Descarga Material Técnicos"
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConRel_Session_TecnicosDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConRel_Session_Tecnicos_QBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DimeSiTieneunaBitacora_QBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraDescripcionArticuloBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Borra_Inserta_Tempo_Por_ServicioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ValidaExistenciasTecnicosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraDetalleBitacoraBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.Inserta_Valores_TempoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Quita_SEBORROART_SALTEC_QBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ValidaDuplicadoRel_Session_Tecnicos_QBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_Folio_QBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Rel_Bitacora_QuejaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Bitacora_tec_QBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Borra_Inserta_Tempo_QBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.INSERTA_Borra_Articulo_Bitacora_QBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Revisa_Tempo_QBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_RelCobraDescBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataSetEdgarRev2 As sofTV.DataSetEdgarRev2
    Friend WithEvents ConRel_Session_TecnicosDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DataSetEDGAR As sofTV.DataSetEDGAR
    Friend WithEvents NoBitacoraTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MovimientoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NoArticuloTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents MuestraDescripcionArticuloBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DataSetarnoldo As sofTV.DataSetarnoldo
    Friend WithEvents Borra_Inserta_Tempo_Por_ServicioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Borra_Inserta_Tempo_Por_ServicioTableAdapter As sofTV.DataSetEdgarRev2TableAdapters.Borra_Inserta_Tempo_Por_ServicioTableAdapter
    Friend WithEvents RespuestaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ValidaExistenciasTecnicosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents ClaveTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents MuestraDetalleBitacoraBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ConceptoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents CantidadSurtidaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents OK As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Inserta_Valores_TempoTableAdapter As sofTV.DataSetarnoldoTableAdapters.Inserta_Valores_TempoTableAdapter
    Friend WithEvents Inserta_Valores_TempoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents Muestra_Descripcion_ArticuloTableAdapter As sofTV.DataSetarnoldoTableAdapters.Muestra_Descripcion_ArticuloTableAdapter
    Friend WithEvents Muestra_Detalle_BitacoraTableAdapter As sofTV.DataSetarnoldoTableAdapters.Muestra_Detalle_BitacoraTableAdapter
    Friend WithEvents ValidaExistenciasTecnicosTableAdapter As sofTV.DataSetEDGARTableAdapters.ValidaExistenciasTecnicosTableAdapter
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents DataSetLidia As sofTV.DataSetLidia
    Friend WithEvents ConRel_Session_Tecnicos_QBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConRel_Session_Tecnicos_QTableAdapter As sofTV.DataSetLidiaTableAdapters.ConRel_Session_Tecnicos_QTableAdapter
    Friend WithEvents NoArticuloDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClaveDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ConceptoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CantidadSurtidaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MovimientoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NoBitacoraDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Quita_SEBORROART_SALTEC_QBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Quita_SEBORROART_SALTEC_QTableAdapter As sofTV.DataSetLidiaTableAdapters.Quita_SEBORROART_SALTEC_QTableAdapter
    Friend WithEvents ValidaDuplicadoRel_Session_Tecnicos_QBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ValidaDuplicadoRel_Session_Tecnicos_QTableAdapter As sofTV.DataSetLidiaTableAdapters.ValidaDuplicadoRel_Session_Tecnicos_QTableAdapter
    Friend WithEvents Dame_Folio_QBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_Folio_QTableAdapter As sofTV.DataSetLidiaTableAdapters.Dame_Folio_QTableAdapter
    Friend WithEvents Inserta_Rel_Bitacora_QuejaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Rel_Bitacora_QuejaTableAdapter As sofTV.DataSetLidiaTableAdapters.Inserta_Rel_Bitacora_QuejaTableAdapter
    Friend WithEvents Inserta_Bitacora_tec_QBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Bitacora_tec_QTableAdapter As sofTV.DataSetLidiaTableAdapters.Inserta_Bitacora_tec_QTableAdapter
    Friend WithEvents Borra_Inserta_Tempo_QBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Borra_Inserta_Tempo_QTableAdapter As sofTV.DataSetLidiaTableAdapters.Borra_Inserta_Tempo_QTableAdapter
    Friend WithEvents INSERTA_Borra_Articulo_Bitacora_QBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents INSERTA_Borra_Articulo_Bitacora_QTableAdapter As sofTV.DataSetLidiaTableAdapters.INSERTA_Borra_Articulo_Bitacora_QTableAdapter
    Friend WithEvents DimeSiTieneunaBitacora_QBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DimeSiTieneunaBitacora_QTableAdapter As sofTV.DataSetLidiaTableAdapters.DimeSiTieneunaBitacora_QTableAdapter
    Friend WithEvents Revisa_Tempo_QBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Revisa_Tempo_QTableAdapter As sofTV.DataSetLidiaTableAdapters.Revisa_Tempo_QTableAdapter
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Clv_tipo As System.Windows.Forms.TextBox
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents Inserta_RelCobraDescBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_RelCobraDescTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_RelCobraDescTableAdapter
    Friend WithEvents ComboBox5 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox4 As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Almacen As System.Windows.Forms.ComboBox
End Class
