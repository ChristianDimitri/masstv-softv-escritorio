﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text
Imports System.Windows.Forms
Imports System.Collections
Imports System.Collections.Generic
Imports System.Xml
Imports System.IO

Public Class FrmOrdSer
    Private customersByCityReport As ReportDocument
    Private LocFecEje As Boolean = False
    Private LocTec As Boolean = False
    Private LocAlm As Boolean = False
    Private LocDet As Boolean = False
    Private Cadena As String
    Private Imprime As Integer = 0
    Dim eRes As Integer = 0
    Dim eMsg As String = Nothing
    Dim bloq As Integer = 0
    Dim dime As Integer
    Dim respuesta As Integer = 0
    Dim BNDTEL As Integer = 0
    Dim valor As Integer = Nothing
    Dim BndCambioEquipo As Boolean = False
    Dim clvTecnicoDescarga As Integer
    Dim clvBitacoraDescarga As Integer

    'Variables para comparar los botones y los checkbox del Grid del Detalle de la Orden
    Dim btnGridDetOrdSer As New DataGridViewButtonCell
    Dim chkGridDetOrdSer As New DataGridViewCheckBoxCell

    'Retiro de Aparatos
    Dim RelTrabajosRetiroAparatos As New List(Of TrabajosRetiroAparato)
    Dim MaterialPendienteDeRetirar As Boolean = False
    Dim ExisteUnDetalleDeRetiro As Boolean = False
    Dim ExisteUnDetalleDeRetiroMarcado As Boolean = False
    Dim TDetallesDeRetiro As Integer = 0
    Dim TDetallesDeRetiroMarcadoS As Integer = 0

    'Bloqueo de Clientes
    Dim GloBloqueoDeCliente As Boolean = False

    Dim GloSalir As Boolean = False

    'Variable de Retiro de Orden
    Dim bndRetiroPorCambio As Boolean = False

    'Saul
    Private eRespuesta As Integer = 0
    Private eMensaje As String = Nothing
    Dim bndAcceso As Integer = 0

    Dim RTrabajoInt As Boolean = False
    Dim RTrabajoDig As Boolean = False



#Region "CONTROLES ANTERIORES"
    Private Sub BUSCA(ByVal CLAVE As Integer)
        Try

            'If IsNumeric(gloClave) = True And IsNumeric(GloClv_TipSer) = True Then
            If IsNumeric(gloClave) = True Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.CONORDSERTableAdapter.Connection = CON
                Me.CONORDSERTableAdapter.Fill(Me.NewSofTvDataSet.CONORDSER, gloClave, 0)
                CON.Close()
                CREAARBOL()
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub Borra_Cambio_Domicilio_Orden_si_no_guardo()
        Dim Con45 As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Try
            cmd = New SqlClient.SqlCommand()
            Con45.Open()
            With cmd
                .CommandText = "Borra_Cambio_Domicilio_Orden_si_no_guardo"
                .CommandTimeout = 0
                .Connection = Con45
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@clv_orden", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = gloClv_Orden
                .Parameters.Add(prm)

                Dim i As Integer = cmd.ExecuteNonQuery()
            End With
            Con45.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Borra_Camdo_si_no_guardo_orden()
        Dim con50 As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Try
            cmd = New SqlClient.SqlCommand()
            con50.Open()
            With cmd
                .CommandText = "Borra_Camdo_si_no_guardo_orden"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = con50

                Dim prm As New SqlParameter("@clv_orden", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = gloClave
                .Parameters.Add(prm)

                Dim i As Integer = cmd.ExecuteNonQuery()

            End With
            con50.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FrmOrdSer_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

        'HabilitaRetiroAparatoPorDetalle(RelTrabajosRetiroAparatos)

        Dim CON As New SqlConnection(MiConexion)
        'dim cmd as New SqlClient 
        If GLOCONTRATOSEL > 0 Then
            Me.ContratoTextBox.Text = GLOCONTRATOSEL
            If IsNumeric(Me.ContratoTextBox.Text) = True Then
                GloContratoord = CLng(Me.ContratoTextBox.Text)
                BUSCACLIENTES(0)
            Else
                Me.ContratoTextBox.Text = GloContratoord
            End If
            GLOCONTRATOSEL = 0
            GloClv_TipSer = 0
        End If
        'If GloBndTipSer = True Then
        '    GloBndTipSer = False
        '    Me.ComboBox5.SelectedValue = GloClv_TipSer
        '    Me.ComboBox5.Text = GloNom_TipSer
        '    Me.ComboBox5.FindString(GloNom_TipSer)
        '    Me.ComboBox5.Text = GloNom_TipSer
        '    Me.TextBox2.Text = GloNom_TipSer
        'End If
        If GloBndTrabajo = True Then
            GloBndTrabajo = False
            'GloClv_TipSer = 0
            If bndCAMDO = True Then
                bndCAMDO = False
                Borra_Cambio_Domicilio_Orden_si_no_guardo()
            End If
            CON.Open()
            Me.BUSCADetOrdSerTableAdapter.Connection = CON
            Me.BUSCADetOrdSerTableAdapter.Fill(Me.NewSofTvDataSet.BUSCADetOrdSer, New System.Nullable(Of Long)(CType(gloClv_Orden, Long)))
            CON.Close()
        End If
        If GloBloqueaDetalle = True Then
            GloBloqueaDetalle = False
            Me.dgvDetOrdSer.Enabled = True
        End If

        If eStatusOrdSer = "P" And eActTecnico = False Then
            Me.Tecnico.Enabled = False
            cbCuadrilla.Enabled = False
            'Me.Almacen.Enabled = False
        Else
            Me.Tecnico.Enabled = True
            cbCuadrilla.Enabled = True
            'Me.Almacen.Enabled = True
        End If

        checaBitacoraTecnico(gloClave, "O")
        If clvBitacoraDescarga > 0 Then
            Me.Tecnico.SelectedValue = clvTecnicoDescarga
            Me.Tecnico.Enabled = False
        Else
            Me.Tecnico.Enabled = True
        End If
        'Guarda el Motivo por el cual ha sido dado de baja un servicio.
        'Eric--------------------------------------------------------------------
        If GloClv_MotCan > 0 Then

            CON.Open()
            Me.InsertMotCanServTableAdapter.Connection = CON
            Me.InsertMotCanServTableAdapter.Fill(Me.DataSetEric.InsertMotCanServ, Me.Clv_OrdenTextBox.Text, GloClv_MotCan)
            CON.Close()
            GloClv_MotCan = 0

            GuardaRelOrdenUsuario()
            'CON.Open()
            Me.Validate()
            Me.CONORDSERBindingSource.EndEdit()

            ModOrdSer(Me.Clv_OrdenTextBox.Text, GloClv_TipSer, Me.ContratoTextBox.Text, Me.Fecha_SoliciutudMaskedTextBox.Value, Me.Fec_EjeTextBox1.Text, Me.Visita1TextBox1.Text, Me.Visita2TextBox1.Text, Me.StatusTextBox.Text, Me.Tecnico.SelectedValue, Me.IMPRESACheckBox.Checked, 0, Me.ObsTextBox.Text, "")
            PreEjecutaOrdSer(Clv_OrdenTextBox.Text)
            SP_GuardaOrdSerAparatos(Clv_OrdenTextBox.Text, opcion, StatusTextBox.Text, 0)

            'Cuadrilla-----------------------------------------------------------------
            If cbCuadrilla.SelectedValue > 0 Then
                NueRelOrdSerCuadrilla(Clv_OrdenTextBox.Text, cbCuadrilla.SelectedValue)
            End If
            '--------------------------------------------------------------------------

            'Me.CONORDSERTableAdapter.Connection = CON
            'Me.CONORDSERTableAdapter.Update(Me.NewSofTvDataSet.CONORDSER)
            'Me.PREEJECUTAOrdSerTableAdapter.Connection = CON
            'Me.PREEJECUTAOrdSerTableAdapter.Fill(Me.NewSofTvDataSet.PREEJECUTAOrdSer, New System.Nullable(Of Long)(CType(Me.Clv_OrdenTextBox.Text, Long)))
            'CON.Close()
            MsgBox(mensaje5)

            'Eric----------------------------------------------
            Dim CONERIC As New SqlConnection(MiConexion)
            Dim eRes As Long = 0
            Dim eMsg As String = Nothing
            CONERIC.Open()
            Me.ChecaOrdSerRetiroTableAdapter.Connection = CONERIC
            Me.ChecaOrdSerRetiroTableAdapter.Fill(Me.DataSetEric.ChecaOrdSerRetiro, CType(Me.Clv_OrdenTextBox.Text, Long), eRes, eMsg)
            CONERIC.Close()
            If eRes > 0 And IdSistema = "SA" Or IdSistema = "VA" Then
                ImprimeOrdSerRetiro(eRes)
            End If
            '------------------------------------------------------

            GloBnd = True
            GloGuardo = False
            'If opcion = "N" Then
            '    CON.Open()
            '    Me.Imprime_OrdenTableAdapter.Connection = CON
            '    Me.Imprime_OrdenTableAdapter.Fill(Me.ProcedimientosArnoldo2.Imprime_Orden, Me.Clv_OrdenTextBox.Text, Imprime)
            '    CON.Close()
            '    If Imprime = 0 Then
            '       ConfigureCrystalReports_NewXml(0, "")
            '    ElseIf Imprime = 1 Then
            '        MsgBox("La orden es de proceso Automático No se Imprimio", MsgBoxStyle.Information)
            '    End If
            'End If
            CON.Close()
            Me.Close()
        End If
        If bloq = 1 Then

            bloq = 0
            eGloContrato = Me.ContratoTextBox.Text
            FrmBloqueo.ShowDialog()
            Me.Panel1.Enabled = False
            Me.Panel3.Enabled = False
            Me.Panel6.Enabled = False
            Me.Panel7.Enabled = False
            Me.Panel8.Enabled = False

            'Como el Cliente esta bloqueado la Orden NO se debe de generar
            GloBloqueoDeCliente = True

            Me.Close()
            Me.Dispose()

        End If
        '----------------------------------------------------------------------------------------

        btnGuardaRetiroParcial.Visible = False
        btnEjecutarRetiroParcial.Visible = False

        
    End Sub

    'Private Sub BUSCACLIENTES(ByVal OP As Integer)
    '    Try
    '        Dim CON As New SqlConnection(MiConexion)

    '        If IsNumeric(Me.ContratoTextBox.Text) = True Then
    '            CON.Open()
    '            Me.BUSCLIPORCONTRATOTableAdapter.Connection = CON
    '            Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.NewSofTvDataSet.BUSCLIPORCONTRATO, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), New System.Nullable(Of Integer)(CType(0, Integer)), 0)
    '            Me.BuscaBloqueadoTableAdapter.Connection = CON
    '            Me.BuscaBloqueadoTableAdapter.Fill(Me.DataSetLidia.BuscaBloqueado, Me.ContratoTextBox.Text, NUM, num2)
    '            CON.Close()
    '            CREAARBOL()
    '            If num2 = 1 Then
    '                eGloContrato = Me.ContratoTextBox.Text
    '                bloq = 1
    '            End If
    '        End If

    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try

    'End Sub

    Private Sub CREAARBOL()

        Try
            Dim CON As New SqlConnection(MiConexion)

            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim Y As Integer = 0
            Dim epasa As Boolean = True
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' msgbox(pRow("CustomerID").ToString())
            'Next
            CON.Open()
            If IsNumeric(Me.ContratoTextBox.Text) = True Then
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)))
            Else
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(0, Long)))
            End If
            CON.Close()
            Dim pasa As Boolean = False
            Dim Net As Boolean = False
            Dim dig As Boolean = False
            Dim jNet As Integer = -1
            Dim PasaJNet As Boolean = False
            Dim jDig As Integer = -1
            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.dameSerDELCli.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                'MsgBox(Mid(FilaRow("Servicio").ToString(), 1, 19))
                If Mid(FilaRow("Servicio").ToString(), 1, 3) = "---" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Red  ' System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    Net = False
                    dig = False
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Servicio Basico" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Red  ' System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 31) = "Servicios de Televisión Digital" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Red  ' System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 21) = "Servicios de Internet" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Red  ' System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 22) = "Servicios de Telefonía" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Red  ' System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                Else
                    If Mid(FilaRow("Servicio").ToString(), 1, 14) = "Mac Cablemodem" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jNet = jNet + 1
                        pasa = False
                        Net = True
                    ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Aparato Digital" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jDig = jDig + 1
                        pasa = False
                        dig = True
                    Else
                        If Net = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jNet).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        ElseIf dig = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jDig).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        Else
                            If epasa = True Then
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                pasa = False
                                epasa = False
                            Else
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                                epasa = False
                                pasa = False
                            End If

                        End If
                    End If
                End If
                If pasa = True Then I = I + 1
            Next

            'Me.TreeView1.Nodes(0).ExpandAll()
            For Y = 0 To (I - 1)
                Me.TreeView1.Nodes(Y).ExpandAll()
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try


    End Sub

    Private Sub CREAARBOL11()

        Try
            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' Console.WriteLine(pRow("CustomerID").ToString())
            'Next

            If IsNumeric(Me.ContratoTextBox.Text) = True Then
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)))
            Else
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(0, Long)))
            End If


            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.dameSerDELCli.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                Me.TreeView1.Nodes.Add(Trim(FilaRow("Servicio").ToString()))
                I += 1
            Next
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ACTIVA(ByVal BND As Boolean)
        'Me.ComboBox4.Enabled = BND
        Me.BindingNavigatorDeleteItem.Enabled = BND
        RadioButton2.Enabled = BND
        RadioButton3.Enabled = BND
        Me.Tecnico.Enabled = BND
        cbCuadrilla.Enabled = BND
        'Me.Almacen.Enabled = BND
        Me.Fec_EjeTextBox.Enabled = BND
        Me.Visita1TextBox.Enabled = BND
        Me.Visita2TextBox.Enabled = BND
        Me.Visita3TextBox.Enabled = BND
        Me.dtpHoraVisita1.Enabled = BND
        Me.dtpHoraVisita2.Enabled = BND
        Me.dtpHoraVisita3.Enabled = BND
        'SolucionTextBox.Enabled = BND
        Panel2.Enabled = BND
        If BND = True Then
            Me.RadioButton1.Enabled = False
            Me.ContratoTextBox.Enabled = False
            Me.Button1.Enabled = False
            'Me.SplitContainer1.Enabled = False
            'Me.ComboBox3.Enabled = False
            'Me.ProblemaTextBox.Enabled = False
        End If
    End Sub



    Private Sub FrmOrdSer_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If IsNumeric(Me.Clv_OrdenTextBox.Text) = True Then
            Try
                If dameStatusOrdenQueja(CInt(Me.Clv_OrdenTextBox.Text), "O") = "P" And opcion = "M" Then
                    softv_BorraDescarga(CInt(Me.Clv_OrdenTextBox.Text), "O")
                End If

                Dim CON As New SqlConnection(MiConexion)
                Dim error2 As Integer = Nothing

                LocNo_Bitacora = 0
                CON.Open()
                Me.Valida_DetOrdenTableAdapter.Connection = CON
                Me.Valida_DetOrdenTableAdapter.Fill(Me.NewSofTvDataSet.Valida_DetOrden, New System.Nullable(Of Long)(CType(Me.Clv_OrdenTextBox.Text, Long)))
                CON.Close()
                If IsNumeric(Me.ValidacionTextBox.Text) = False Then Me.ValidacionTextBox.Text = 0
                Me.valida()
                'If Me.CONTADORTextBox.Text = 4 Then
                ' GloGuardo = True
                ' End If
                If GloGuardo = True And Me.ValidacionTextBox.Text > 0 Then
                    Dim RESP As MsgBoxResult = MsgBoxResult.Yes
                    If opcion = "C" Then
                        Exit Sub
                    End If
                    If opcion = "N" And GloBloqueoDeCliente = False And GloSalir = False Then 'Si el Cliente está Bloqueado NO se puede generar una Orden, por lo tanto no debe de preguntar si es que desea guardar.
                        RESP = MsgBox("Desea Guardar la Orden que Genero", MsgBoxStyle.YesNo)
                        If RESP = MsgBoxResult.No Then
                            GloGuardo = False
                            'GloClv_TipSer = Me.ComboBox5.SelectedValue
                            GloClv_TipSer = 0
                            'GloNom_TipSer = Me.ComboBox5.Text
                            GloNom_TipSer = ""
                            Borra_Camdo_si_no_guardo_orden()
                            CON.Open()
                            Me.CONORDSERTableAdapter.Connection = CON
                            Me.CONORDSERTableAdapter.Delete(gloClave, 0)
                            CON.Close()
                        Else
                            error2 = Checa_si_tiene_camdo(gloClave)

                            If error2 > 0 Then
                                GloBnd = True
                                MsgBox("Se Tiene Que Capturar el Nuevo Domicilio", MsgBoxStyle.Information)
                                CON.Open()
                                Me.CONORDSERTableAdapter.Connection = CON
                                Me.CONORDSERTableAdapter.Delete(gloClave, 0)
                                CON.Close()
                                Exit Sub

                            End If
                        End If
                    ElseIf Me.StatusTextBox.Text = "E" And Me.CONTADORTextBox.Text = 4 Then
                        RESP = MsgBox("La Orden ya tiene todos los datos para Ejecutarse ¿ Desea Salir sin Grabar ? ", MsgBoxStyle.YesNo)
                        If RESP = MsgBoxResult.No Then
                            e.Cancel = True
                            Exit Sub
                        End If
                    End If
                    GloBnd = True
                ElseIf GloGuardo = True And Me.ValidacionTextBox.Text = 0 Then
                    GloBnd = True
                    GloGuardo = False
                    'GloClv_TipSer = Me.ComboBox5.SelectedValue
                    'GloNom_TipSer = Me.ComboBox5.Text
                    GloClv_TipSer = 0
                    GloNom_TipSer = ""
                    CON.Open()
                    Me.CONORDSERTableAdapter.Connection = CON
                    Me.CONORDSERTableAdapter.Delete(gloClave, GloClv_TipSer)
                    CON.Close()
                End If

                ''Limpiamos el Cliente Bloqueado
                If GloBloqueoDeCliente = True Then
                    Me.CONORDSERTableAdapter.Connection = CON
                    Me.CONORDSERTableAdapter.Delete(gloClave, 0)
                End If
                GloBloqueoDeCliente = False

            Catch ex As System.Exception
                System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub



    Private Sub FrmOrdSer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Obtenemos los Trabajos y su relación con el Retiro de Aparato
        GloeStatus = "P"
        Bnd_CAPAR = False
        gloClvTipSerOrdenes = 0
        Try
            RelTrabajosRetiroAparatos = DAEquipoExtra.CargaRelTrabajosRetiroAparato
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Try
            colorea(Me, Me.Name)
            Dim CON As New SqlConnection(MiConexion)
            Dim valor As Integer = 0
            'tel_old = 0
            'tel_new = 0
            'No paresca "Es Hotel"
            TieneAcometida = False
            If IdSistema = "LO" Or IdSistema = "YU" Then
                Me.Label13.Visible = False
                Me.ESHOTELCheckBox.Visible = False
            End If

            CON.Open()
            'GloClv_TipSer = 1000
            'TODO: esta línea de código carga datos en la tabla 'DataSetarnoldo.Dame_fecha_hora_serv' Puede moverla o quitarla según sea necesario.
            Me.Dame_fecha_hora_servTableAdapter.Connection = CON
            Me.Dame_fecha_hora_servTableAdapter.Fill(Me.DataSetarnoldo.Dame_fecha_hora_serv)
            'TODO: esta línea de código carga datos en la tabla 'DataSetarnoldo.Dame_fecha_hora_serv' Puede moverla o quitarla según sea necesario.
            Locclv_folio = 0
            LocNo_Bitacora = 0
            LocValida1 = False
            Bloquea = False

            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerPrincipal' Puede moverla o quitarla según sea necesario.
            Me.DameClv_Session_TecnicosTableAdapter.Connection = CON
            Me.DameClv_Session_TecnicosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Tecnicos, clv_sessionTecnico)
            Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
            Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)
            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MUESTRATECNICOS' Puede moverla o quitarla según sea necesario.
            ''------ comento Jaime
            'Me.MUESTRATECNICOSTableAdapter.Connection = CON
            'Me.MUESTRATECNICOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATECNICOS)
            'Me.Muestra_Tecnicos_AlmacenTableAdapter.Connection = CON
            'Me.Muestra_Tecnicos_AlmacenTableAdapter.Fill(Me.DataSetarnoldo.Muestra_Tecnicos_Almacen, 0)
            '---------- fin comento 
            CON.Close()

            Me.Hora_Eje.Enabled = False
            Me.Hora_sol.Enabled = False
            Dame_fecha_hora_serv_New()
            If opcion = "N" Then
                Me.Hora_sol.Enabled = False
                Dame_fecha_hora_serv_New()
                Me.Hora_sol.Value = eGloFecha & " " & eGloHora
                MuestraRelOrdenesTecnicos(0)
                'MuestraRelOrdSerCuadrilla(0, 0)
                'MuestraAlmacenes(0)
                Me.Button3.Visible = False
                Me.CONORDSERBindingSource.AddNew()
                GloControlaReloj = 0
                Me.StatusTextBox.Text = "P"
                'FrmSelTipServicio.Show()
                Me.Fecha_SoliciutudMaskedTextBox.Text = Now
                ACTIVA(False)
                Panel1.Enabled = True
                Panel7.Enabled = True
                'Lo pUso Eric
                Panel6.Enabled = False
                Me.Panel8.Enabled = False
                Me.SectorCombo.Visible = False
                Me.SectorLabel.Visible = False

            ElseIf opcion = "C" Then

                If IsNumeric(gloClave) = True Then
                    MuestraRelOrdenesTecnicos(gloClave)
                    'MuestraRelOrdSerCuadrilla(gloClave, Tecnico.SelectedValue)
                    'MuestraAlmacenes(gloClave)
                    GloControlaReloj = 0
                    'Panel1.Enabled = False
                    Panel6.Enabled = False
                    Panel7.Enabled = False
                    Me.Panel3.Enabled = False
                    Bloquea = True
                    BUSCA(gloClave)
                    CREAARBOL()
                    Me.Button1.Enabled = False
                    Me.Panel2.Enabled = False
                    Me.Fecha_SoliciutudMaskedTextBox.Enabled = False
                    Me.CONORDSERBindingNavigator.Enabled = False
                    Me.ContratoTextBox.ReadOnly = True
                    Me.FolioTextBox.ReadOnly = True
                    Me.Fec_EjeTextBox.ReadOnly = True
                    'Me.GroupBox1.Enabled = True
                    ' Me.TreeView1.Enabled = True
                    Me.Label2.Visible = True 'Etiqueta Visible
                    Me.Label2.Text = "Se generó el número de bitácora: " & Cadena 'Etiqueta Text concatenar 
                    CON.Open()
                    Me.Muestra_no_ordenTableAdapter.Connection = CON
                    Me.Muestra_no_ordenTableAdapter.Fill(Me.DataSetarnoldo.Muestra_no_orden, CInt(gloClave))

                    'Cargamos el Detalle de la Orden al Grid
                    CargaDetOrdSer(gloClave)
                    'Habilitamos solo aquellos detalles cuyo trabajo pueda implique el retiro de algún aparato (RAPAR,RCTRL,etc.)
                    HabilitaRetiroAparatoPorDetalle(RelTrabajosRetiroAparatos)

                    'Me.BUSCADetOrdSerTableAdapter.Connection = CON
                    'Me.BUSCADetOrdSerTableAdapter.Fill(Me.NewSofTvDataSet.BUSCADetOrdSer, New System.Nullable(Of Long)(CType(gloClave, Long)))
                    'Me.MUESTRATRABAJOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATRABAJOS, New System.Nullable(Of Integer)(CType(GloClv_TipSer, Integer)))
                    'Eric
                    Me.ValidaSiEsAcometidaTableAdapter.Connection = CON
                    Me.ValidaSiEsAcometidaTableAdapter.Fill(Me.DataSetEric.ValidaSiEsAcometida, CInt(gloClave), eResAco)
                    CON.Close()
                    If (eResAco = 1 And IdSistema = "TO") Or (eResAco = 1 And IdSistema = "SA") Or (eResAco = 1 And IdSistema = "VA") Then
                        CON.Open()
                        Me.ConRelCtePlacaTableAdapter.Connection = CON
                        Me.ConRelCtePlacaTableAdapter.Fill(Me.DataSetEric.ConRelCtePlaca, Me.ContratoTextBox.Text)
                        CON.Close()
                        Me.Label4.Visible = True
                        Me.PlacaTextBox.Visible = True
                    Else
                        Me.Label4.Visible = False
                        Me.PlacaTextBox.Visible = False
                    End If

                    'JUANJO SECTORES Y TAPS
                    CHECASIACOMETIDA(gloClave)
                    If BNDORDACOMETIDA = False Then
                        Me.SectorCombo.Visible = False
                        Me.SectorLabel.Visible = False
                    Else
                        Me.SectorCombo.Visible = True
                        Me.SectorLabel.Visible = True
                        DAMESECTORORDENES(gloClave)
                        Me.SectorCombo.SelectedValue = spDimeSectorDelCliente(gloClave)
                        Me.SectorCombo.Enabled = False
                    End If
                    'FIN JUANJO SECTORES Y TAPS

                    If eResAco = 1 And IdSistema = "AG" Then
                        Llena_Tabs_Por_Contrato(Me.ContratoTextBox.Text)
                        'ConRelClienteTab(Me.ContratoTextBox.Text)
                        Me.LabelTab.Visible = True
                        'Me.TextBoxTab.Visible = True
                        Me.ComboBoxTap.Visible = True
                    End If

                    'CONSULTA LA RELACION QUE HAY ENTRE LA ORDEN Y EL USUARIO
                    CON.Open()
                    Me.Consulta_RelOrdenUsuarioTableAdapter.Connection = CON
                    Me.Consulta_RelOrdenUsuarioTableAdapter.Fill(Me.DataSetEric2.Consulta_RelOrdenUsuario, gloClave)
                    CON.Close()
                    Me.Label7.Visible = True
                    Me.GeneroLabel1.Visible = True
                    If Me.StatusTextBox.Text = "E" Then
                        Me.Label12.Visible = True
                        Me.EjecutoLabel1.Visible = True
                        GloeStatus = "E"
                    End If
                End If
                'OBTENEMOS LA CLV_TIPSER A LA QUE PERTENECE EL CLIENTE (INICIO) <JUANJO>
                gloClvTipSerOrdenes = uspDimeTipoServicioCliente(gloClave)
                'OBTENEMOS LA CLV_TIPSER A LA QUE PERTENECE EL CLIENTE (INICIO) <JUANJO>
                Dame_La_HoraEje_Orden(gloClave)
                Me.Hora_sol.Value = eGloFecha_sol & " " & eGloHora_sol
                Me.Hora_Eje.Value = eGloFecha_eje & " " & eGloHora_eje
                GloContratoord = CLng(Me.ContratoTextBox.Text)
                BUSCACLIENTES(0)
            ElseIf opcion = "M" Then
                If IsNumeric(gloClave) = True Then
                    'MuestraAlmacenes(0)
                    Me.Panel3.Enabled = False
                    ACTIVA(True)
                    Panel1.Enabled = True
                    Panel6.Enabled = True
                    Panel7.Enabled = True
                    BUSCA(gloClave)

                    If Me.StatusTextBox.Text = "E" Then ' Or Me.StatusTextBox.Text = "V" Then
                        MuestraRelOrdenesTecnicos(gloClave)
                        'MuestraRelOrdSerCuadrilla(gloClave, Tecnico.SelectedValue)
                    Else
                        MuestraRelOrdenesTecnicos(gloClave)
                        'MuestraRelOrdSerCuadrilla(gloClave, 0)
                    End If

                    CREAARBOL()
                    CON.Open()

                    'Cargamos el Detalle de la Orden al Grid
                    CargaDetOrdSer(gloClave)
                    'Habilitamos solo aquellos detalles cuyo trabajo pueda implique el retiro de algún aparato (RAPAR,RCTRL,etc.)
                    HabilitaRetiroAparatoPorDetalle(RelTrabajosRetiroAparatos)

                    'Me.BUSCADetOrdSerTableAdapter.Connection = CON
                    'Me.BUSCADetOrdSerTableAdapter.Fill(Me.NewSofTvDataSet.BUSCADetOrdSer, New System.Nullable(Of Long)(CType(gloClave, Long)))
                    Me.MUESTRATRABAJOSTableAdapter.Connection = CON
                    'Me.MUESTRATRABAJOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATRABAJOS, New System.Nullable(Of Integer)(CType(GloClv_TipSer, Integer)))
                    Me.MUESTRATRABAJOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATRABAJOS, 0)
                    'eric
                    Me.ValidaSiEsAcometidaTableAdapter.Connection = CON
                    Me.ValidaSiEsAcometidaTableAdapter.Fill(Me.DataSetEric.ValidaSiEsAcometida, Me.Clv_OrdenTextBox.Text, eResAco)
                    CON.Close()
                    If (eResAco = 1 And IdSistema = "TO") Or (eResAco = 1 And IdSistema = "SA") Or (eResAco = 1 And IdSistema = "VA") Then
                        CON.Open()
                        Me.ConRelCtePlacaTableAdapter.Connection = CON
                        Me.ConRelCtePlacaTableAdapter.Fill(Me.DataSetEric.ConRelCtePlaca, Me.ContratoTextBox.Text)
                        CON.Close()
                        Me.Label4.Visible = True
                        Me.PlacaTextBox.Visible = True
                    Else
                        Me.Label4.Visible = False
                        Me.PlacaTextBox.Visible = False
                    End If


                    If eResAco = 1 And IdSistema = "AG" Then
                        Llena_Tabs_Por_Contrato(Me.ContratoTextBox.Text)
                        ' ConRelClienteTab(Me.ContratoTextBox.Text)
                        Me.LabelTab.Visible = True
                        'Me.TextBoxTab.Visible = True
                        Me.ComboBoxTap.Visible = True
                    End If

                    If Me.StatusTextBox.Text = "E" Then
                        GloeStatus = "E"
                        Me.Label2.Visible = True 'Etiqueta Visible
                        Me.Label2.Text = "Se generó el número de bitácora: " & Cadena 'Etiqueta Text concatenar 
                        CON.Open()
                        Me.Muestra_no_ordenTableAdapter.Connection = CON
                        Me.Muestra_no_ordenTableAdapter.Fill(Me.DataSetarnoldo.Muestra_no_orden, CInt(gloClave))
                        Panel1.Enabled = False
                        Panel6.Enabled = False
                        Panel7.Enabled = False
                        Bloquea = True
                        'CONSULTA LA RELACION QUE HAY ENTRE LA ORDEN Y EL USUARIO
                        Me.Consulta_RelOrdenUsuarioTableAdapter.Connection = CON
                        Me.Consulta_RelOrdenUsuarioTableAdapter.Fill(Me.DataSetEric2.Consulta_RelOrdenUsuario, gloClave)
                        CON.Close()
                        Me.Label7.Visible = True
                        Me.GeneroLabel1.Visible = True
                        If Me.StatusTextBox.Text = "E" Then
                            Me.Label12.Visible = True
                            Me.EjecutoLabel1.Visible = True
                        End If
                        Dame_La_HoraEje_Orden(gloClave)
                        Me.Hora_sol.Value = eGloFecha_sol & " " & eGloHora_sol
                        Me.Hora_Eje.Value = eGloFecha_eje & " " & eGloHora_eje

                        'JUANJO SECTORES Y TAPS
                        CHECASIACOMETIDA(gloClave)
                        If BNDORDACOMETIDA = False Then
                            Me.SectorCombo.Visible = False
                            Me.SectorLabel.Visible = False
                        Else
                            Me.SectorCombo.Visible = True
                            Me.SectorLabel.Visible = True
                            DAMESECTORORDENES(gloClave)
                            Me.SectorCombo.SelectedValue = spDimeSectorDelCliente(gloClave)
                            Me.SectorCombo.Enabled = False
                        End If
                        'FIN JUANJO SECTORES Y TAPS

                    ElseIf Me.StatusTextBox.Text = "P" Then
                        GloeStatus = "P"
                        CON.Open()
                        Me.ValidarNuevoTableAdapter.Connection = CON
                        Me.ValidarNuevoTableAdapter.Fill(Me.DataSetarnoldo.ValidarNuevo, gloClave, 0, valor)
                        CON.Close()
                        If valor <= 0 Then
                            Panel1.Enabled = True

                            Panel6.Enabled = True
                            Panel7.Enabled = True
                            Bloquea = False
                            Timer1.Enabled = True
                            GloControlaReloj = 1
                        Else
                            Timer1.Enabled = False
                            GloControlaReloj = 0
                            Panel1.Enabled = False
                            Panel6.Enabled = False
                            Panel7.Enabled = False
                            Me.Panel8.Enabled = False
                            Bloquea = True
                            MsgBox("La orden no se puede ejecutar de forma manual ya que este tipo de orden de servicio al cliente se procesa de forma automatica ", MsgBoxStyle.Information)
                        End If
                        Me.Hora_Eje.Enabled = True
                        Dame_La_HoraEje_Orden(gloClave)
                        Me.Hora_sol.Value = eGloFecha_sol & " " & eGloHora_sol
                        Me.Hora_Eje.Value = eGloFecha_eje & " " & eGloHora_eje
                        'Me.Hora_Eje.Value = eGloFecha_eje & " " & eGloHora_eje
                        'MsgBox(FormatDateTime(Me.Hora_Eje.Value, DateFormat.ShortTime))
                        Me.StatusTextBox.Text = "E"
                        Me.Fec_EjeTextBox.Enabled = True
                        Me.Visita1TextBox.Enabled = False
                        Me.Visita2TextBox.Enabled = False
                        Me.TextBox1.Visible = False
                        Me.Fec_EjeTextBox.Focus()

                        'lineas de eric

                        If eStatusOrdSer = "P" Then
                            CON.Open()
                            Me.ValidaSiEsAcometidaTableAdapter.Connection = CON
                            Me.ValidaSiEsAcometidaTableAdapter.Fill(Me.DataSetEric.ValidaSiEsAcometida, Me.Clv_OrdenTextBox.Text, eResAco)
                            CON.Close()
                            If (eResAco = 1 And IdSistema = "TO") Or (eResAco = 1 And IdSistema = "SA") Or (eResAco = 1 And IdSistema = "VA") Then
                                Me.Label4.Visible = True
                                Me.PlacaTextBox.Visible = True
                            Else
                                Me.Label4.Visible = False
                                Me.PlacaTextBox.Visible = False
                            End If

                            If eResAco = 1 And IdSistema = "AG" Then
                                Llena_Tabs_Por_Contrato(Me.ContratoTextBox.Text)
                                'ConRelClienteTab(Me.ContratoTextBox.Text)
                                Me.LabelTab.Visible = True
                                'Me.TextBoxTab.Visible = True
                                Me.ComboBoxTap.Visible = True
                            End If

                        End If
                        'JUANJO SECTORES Y TAPS
                        CHECASIACOMETIDA(gloClave)
                        If BNDORDACOMETIDA = False Then
                            Me.SectorCombo.Visible = False
                            Me.SectorLabel.Visible = False
                        Else
                            Me.SectorCombo.Visible = True
                            Me.SectorLabel.Visible = True
                            DAMESECTORORDENES(gloClave)
                            TieneAcometida = True
                        End If
                        'FIN JUANJO SECTORES Y TAPS


                    ElseIf Me.StatusTextBox.Text = "V" Then
                        GloeStatus = "P"
                        Me.Fec_EjeTextBox.Enabled = False
                        Me.Hora_Eje.Enabled = False
                        consultaVisitasOrdenes(gloClave)
                        If IsDate(Me.Visita1TextBox1.Text) = True Then
                            Me.Visita1TextBox.Enabled = False
                            Me.dtpHoraVisita1.Enabled = False
                        End If
                        If IsDate(Me.Visita2TextBox.Text) = True Then
                            Me.Visita2TextBox.Enabled = False
                            Me.dtpHoraVisita2.Enabled = False
                        End If
                        If IsDate(Me.Visita3TextBox.Text) = True Then
                            Me.Visita3TextBox.Enabled = False
                            Me.dtpHoraVisita3.Enabled = False
                        End If

                        'JUANJO SECTORES Y TAPS
                        CHECASIACOMETIDA(gloClave)
                        If BNDORDACOMETIDA = False Then
                            Me.SectorCombo.Visible = False
                            Me.SectorLabel.Visible = False
                        Else
                            Me.SectorCombo.Visible = True
                            Me.SectorLabel.Visible = True
                            DAMESECTORORDENES(gloClave)
                            TieneAcometida = True
                        End If
                        'FIN JUANJO SECTORES Y TAPS
                    End If
                End If

                'OBTENEMOS LA CLV_TIPSER A LA QUE PERTENECE EL CLIENTE (INICIO) <JUANJO>
                gloClvTipSerOrdenes = uspDimeTipoServicioCliente(gloClave)
                'OBTENEMOS LA CLV_TIPSER A LA QUE PERTENECE EL CLIENTE (INICIO) <JUANJO>
                checaBitacoraTecnico(gloClave, "O")
                GloContratoord = CLng(Me.ContratoTextBox.Text)
                BUSCACLIENTES(0)
                If clvBitacoraDescarga > 0 Then
                    Me.Tecnico.SelectedValue = clvTecnicoDescarga
                    Me.Tecnico.Enabled = False
                Else
                    Me.Tecnico.Enabled = True
                End If
            End If

            'Me.CONDetOrdSerTableAdapter.Fill(Me.NewSofTvDataSet.CONDetOrdSer, New System.Nullable(Of Long)(CType(Me.Clv_OrdenTextBox.Text, Long)))
            If opcion = "M" Or opcion = "C" Then
                Dim cone As New SqlClient.SqlConnection(MiConexion)
                cone.Open()
                NUM = 0
                num2 = 0
                Me.BuscaBloqueadoTableAdapter.Connection = cone
                Me.BuscaBloqueadoTableAdapter.Fill(Me.DataSetLidia.BuscaBloqueado, loccontratoordenes, CInt(NUM), CInt(num2))
                cone.Close()
                If num2 = 1 Then
                    'eGloContrato = Contrato
                    bloq = 1
                End If
                ' Me.ContratoTextBox.Text = loccontratoordenes
                ChecaRelOrdenUsuario(gloClave)
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

        Me.Hora_sol.Visible = False
        Me.ComboBoxTap.Visible = False
        Me.LabelTab.Visible = False

        HabilitaRetiroAparatoPorDetalle(RelTrabajosRetiroAparatos)
        Me.BindingNavigatorDeleteItem.Visible = False
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'Me.BorraMotivoCanServTableAdapter.Connection = CON
        'Me.BorraMotivoCanServTableAdapter.Fill(Me.DataSetEric.BorraMotivoCanServ, Me.Clv_OrdenTextBox.Text, 0, 0, 0, 1)
        'CON.Close()
        Dim msgres = MsgBox("¿Realmente Deseas Cancelar?", MsgBoxStyle.YesNo)

        If msgres = MsgBoxResult.Yes Then
            BORRAAPARATOS()
            Me.CONORDSERBindingSource.CancelEdit()
            GloBnd = True

            If IsNumeric(Me.Clv_OrdenTextBox.Text) = True Then
                'BORRAMETRAJE(CLng(Me.Clv_OrdenTextBox.Text))
                BORRAAPARATOS()
            End If
            Me.Close()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim msgres = MsgBox("¿Realmente Deseas Cancelar?", MsgBoxStyle.YesNo)

        If msgres = MsgBoxResult.Yes Then
            'BORRAAPARATOS()
            'Dim CON As New SqlConnection(MiConexion)
            'CON.Open()
            'Me.CONORDSERTableAdapter.Connection = CON
            'Me.CONORDSERTableAdapter.Delete(gloClave, -1)
            'CON.Close()
            'MsgBox(mensaje6)
            'GloBnd = True
            'Me.Close()
            GloBnd = True
            GloSalir = True
            BORRAAPARATOS()

            GloGuardo = False
            'GloClv_TipSer = Me.ComboBox5.SelectedValue
            GloClv_TipSer = 0
            'GloNom_TipSer = Me.ComboBox5.Text
            GloNom_TipSer = ""
            Borra_Camdo_si_no_guardo_orden()
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.CONORDSERTableAdapter.Connection = CON
            Me.CONORDSERTableAdapter.Delete(gloClave, 0)
            CON.Close()

            Me.Close()
            Me.Dispose()

        Else
            Exit Sub
        End If
    End Sub
    Private Sub Dime_Si_Graba()
        Dim conlidia As New SqlClient.SqlConnection(MiConexion)
        Dim comando As New SqlCommand
        Dim reader As SqlDataReader
        If IsDate(Me.Visita2TextBox.Text) = True Then
            conlidia.Open()
            With comando
                .Connection = conlidia
                .CommandText = "Exec Dimesigrabaord " & CStr(gloClave) & "," & "0" & "," & "'" + CStr(Me.Visita2TextBox.Text) + "'"
                .CommandType = CommandType.Text
                .CommandTimeout = 0
                reader = comando.ExecuteReader()
                Using reader
                    While reader.Read
                        ' Process SprocResults datareader here.
                        Me.TextBox4.Text = reader.GetValue(0)
                    End While
                End Using
            End With
            conlidia.Close()
            dime = Me.TextBox4.Text
        Else
            dime = 0
        End If
    End Sub
    Private Function Checa_si_tiene_camdo(ByVal clv_orden As Long) As Integer
        Dim con60 As New SqlClient.SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Dim error1 As Integer = Nothing
        Try
            con60.Open()
            cmd = New SqlClient.SqlCommand()
            With cmd
                .CommandText = "Checa_si_tiene_camdo"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = con60

                Dim prm As New SqlParameter("@clv_orden", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = clv_orden
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@error", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)

                Dim i As Integer = cmd.ExecuteNonQuery()

                error1 = prm1.Value

            End With
            con60.Close()
            Checa_si_tiene_camdo = error1
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Function

    Private Sub CONORDSERBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONORDSERBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        Dim error2 As Integer = Nothing
        'CON.Open()

        'If Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "BPAQT" And opcion <> "N" Then
        '    validaContratoTelefonia(GloContratonet, GloClv_CablemodemSel)
        '    If BNDTEL = 0 Then
        '        MsgBox("El Cliente Tiene Sus Servicios En Baja, No Se Ejecutara La Orden")
        '        Exit Sub
        '    Else
        '        If (MsgBox("Se Pasaran A Baja Sus Servicios De Telefonia, Desea Continuar", MsgBoxStyle.YesNo)) = MsgBoxResult.No Then
        '            Exit Sub
        '        End If
        '    End If
        'End If

        

        If Mid(Trim(dgvDetOrdSer.SelectedCells(4).Value), 1, 5) = "CAPAR" Or Mid(Trim(dgvDetOrdSer.SelectedCells(4).Value), 1, 6) = "CAPAR6" Then
            If GloGuardo = False Then
                MsgBox("Seleccione el nuevo aparato digital", MsgBoxStyle.Information)
                Exit Sub
            End If
        End If
        If Mid(Trim(dgvDetOrdSer.SelectedCells(4).Value), 1, 5) = "CCAJA" Or Mid(Trim(dgvDetOrdSer.SelectedCells(4).Value), 1, 6) = "CCAJA6" Then
            If GloGuardo = False Then
                MsgBox("Seleccione la nueva caja digital", MsgBoxStyle.Information)
                Exit Sub
            End If
        End If

        If IsNumeric(Me.ContratoTextBox.Text) = True Then
            CON.Open()
            Me.Valida_DetOrdenTableAdapter.Connection = CON
            Me.Valida_DetOrdenTableAdapter.Fill(Me.NewSofTvDataSet.Valida_DetOrden, New System.Nullable(Of Long)(CType(Me.Clv_OrdenTextBox.Text, Long)))
            CON.Close()
            If Me.ValidacionTextBox.Text = 0 Then
                MsgBox("Se Requiere tener datos en el Detalle de la Orden")
                Exit Sub
            End If

            If Me.StatusTextBox.Text = "E" Then
                ValidaGuardarOrden(Clv_OrdenTextBox.Text)

                If eMsj.Length > 0 Then
                    MsgBox(eMsj, MsgBoxStyle.Information)
                    Exit Sub
                End If

            End If



            'If Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "CANUM" And opcion <> "N" Then
            '    If tel_old = 0 Or tel_new = 0 Then
            '        MsgBox("No se le a asignado numero")
            '        Exit Sub
            '    Else
            '        guardaBitacoraCANUM(newcontrato, tel_old, tel_new, 1)
            '    End If
            'End If

            'If Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "CMTCM" And opcion <> "N" Then
            '    validaContratoTelefonia(GloContratonet, GloClv_CablemodemSel)
            '    If BNDTEL = 0 Then
            '        MsgBox("El Cliente Tiene Adeudos de Telefonia, No Se Ejecutara La Orden")
            '        Exit Sub
            '        'Else
            '        '    If (MsgBox("Se Pasaran A Baja Sus Servicios De Telefonia, Desea Continuar", MsgBoxStyle.YesNo)) = MsgBoxResult.No Then
            '        '        Exit Sub
            '        '    End If
            '    End If
            'End If
        End If

        If eResAco = 1 And opcion <> "N" And IdSistema = "AG" Then
            If Me.TextBoxTab.Text.Length = 0 Then
                'MsgBox("Se requiere capturar la Tab.", MsgBoxStyle.Information)
                'Exit Sub
                Me.TextBoxTab.Text = ""
            End If
            NueRelClienteTab(Me.ContratoTextBox.Text, Me.ComboBoxTap.Text)
        End If

        If opcion <> "N" Then
            If Me.RadioButton3.Checked = True Then
                'CON.Open()
                'If Me.Visita1TextBox.Text.Trim.Length > 0 Then
                '    Dime_Si_Graba()
                'ElseIf Me.Visita2TextBox.Text.Trim.Length > 0 Then
                '    'Me.DimesigrabaordTableAdapter.Connection = CON
                '    'Me.DimesigrabaordTableAdapter.Fill(Me.DataSetLidia.dimesigrabaord, gloClave, 0, Me.Visita2TextBox.Text)
                '    Dime_Si_Graba()
                'ElseIf Me.Visita3TextBox.Text.Trim.Length > 0 Then
                '    'Me.DimesigrabaordTableAdapter.Connection = CON
                '    'Me.DimesigrabaordTableAdapter.Fill(Me.DataSetLidia.dimesigrabaord, gloClave, 0, Me.Visita2TextBox.Text)
                '    Dime_Si_Graba()
                'End If
                ' CON.Close()
            Else
                'CON.Open()
                'Me.DimesigrabaordTableAdapter.Connection = CON
                'Me.DimesigrabaordTableAdapter.Fill(Me.DataSetLidia.dimesigrabaord, gloClave, 0, Me.Fec_EjeTextBox.Text)
                Dime_Si_Graba()
                ' CON.Close()
            End If
        End If
        If dime = "2" Or dime = "0" Or opcion = "N" Then
            Try
                If IsNumeric(Me.ContratoTextBox.Text) = True Then
                    'STORED PROCEDURE Q VERIFICA SI TIENE MAS SERVICIOS EL CLIENTE
                    CON.Open()
                    Me.Valida_DetOrdenTableAdapter.Connection = CON
                    Me.Valida_DetOrdenTableAdapter.Fill(Me.NewSofTvDataSet.Valida_DetOrden, New System.Nullable(Of Long)(CType(Me.Clv_OrdenTextBox.Text, Long)))
                    CON.Close()
                    If Me.ValidacionTextBox.Text = 0 Then
                        MsgBox("Se Requiere tener datos en el Detalle de la Orden")
                        Exit Sub
                    End If

                    'VALIDAMOS SI LA ORDEN ES DE RETIRO Y SE GENERÓ POR CAMBIO DE EQUIPO
                    validaSiRetiroPorCambio(CInt(Me.Clv_OrdenTextBox.Text))

                    If ValidaOrdenRetiroAparatos() = False And bndRetiroPorCambio = False Then
                        MsgBox("Se tiene que retirar la Tarjeta y la Caja juntas ")
                        Exit Sub
                    End If

                error2 = Checa_si_tiene_camdo(CLng(Me.Clv_OrdenTextBox.Text))

                If error2 > 0 Then
                    MsgBox("Se Requiere Que Capture El Nuevo Domicilio", MsgBoxStyle.Information)
                    Exit Sub
                End If


                'AQUI VA TU CODIGO ERIC 2 DE ABRIL DE 2008
                'If Me.StatusTextBox.Text = "P" And GLOTRABAJO = "CCABM" Then
                ' If Len(Trim(Tecnico.Text)) = 0 Or IsNumeric(Tecnico.SelectedValue) = False Then
                'MsgBox("Se requiere que Seleccione el Técnico por favor", MsgBoxStyle.Information)
                'Exit Sub
                'End If
                If Me.StatusTextBox.Text <> "V" Then
                    Me.valida()
                    If Me.RadioButton1.Checked = False Then
                        If Me.CONTADORTextBox.Text = 1 Then
                            'If GloClv_TipSer = 2 Then
                            MsgBox("Se Requiere que Asigne el Cablemodem", MsgBoxStyle.Information)
                            Exit Sub
                        ElseIf Me.CONTADORTextBox.Text = 2 Then
                            'ElseIf GloClv_TipSer = 3 Then
                            MsgBox("Se Requiere que Asigne el Aparato ", MsgBoxStyle.Information)
                            Exit Sub
                            'End If
                        ElseIf Me.CONTADORTextBox.Text = 3 Then
                            MsgBox("Se Requiere Que Asigne el ATA", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                End If
                If Me.StatusTextBox.Text = "E" Then
                    Dim Pasa As Integer = 0
                    If IsDate(Mid(Me.Fec_EjeTextBox.Text, 1, 10)) = True Then
                        Dim Fecha As Date = Mid(Me.Fec_EjeTextBox.Text, 1, 10)
                        If DateValue(Fecha) >= DateValue(Me.Fecha_SoliciutudMaskedTextBox.Value) And DateValue(Fecha) <= DateValue(Me.FechaDateTimePicker.Value) Then
                            'If Fecha_SoliciutudMaskedTextBox.Value.ToShortDateString = Fec_EjeTextBox.Text Then
                            '    If CDate(Hora_sol.Value.ToShortTimeString) < CDate(Hora_Eje.Value.ToShortTimeString) Then

                            '    Else
                            '        MsgBox("La Hora de Ejecución: " + Hora_Eje.Value.ToShortTimeString + " no puede ser menor a la Hora de Solicitud: " + Hora_sol.Value.ToShortTimeString, MsgBoxStyle.Information)
                            '        Me.Hora_Eje.Value = DateTime.Now
                            '        Exit Sub
                            '    End If
                            'End If

                            Pasa = 1
                        Else
                            MsgBox("La Fecha de la Ejecución no puede ser Menor a la Fecha de Solicitud ni Mayor a la Fecha Actual ", MsgBoxStyle.Information)
                            Me.Fec_EjeTextBox.Clear()
                            Exit Sub
                        End If
                    Else
                        MsgBox("La Fecha de Ejecución es Invalida")
                        Me.Fec_EjeTextBox.Clear()
                        Exit Sub
                    End If
                    'If BndAsignacion = False Then
                    '    MsgBox("Se requiere asigne un equipo", MsgBoxStyle.Information)
                    '    Exit Sub
                    'End If
                    If IsDate(Me.Fec_EjeTextBox.Text) = False Then
                        MsgBox("Se Requiere que Capture la Fecha y la Hora de Ejecución de Forma Correcta por Favor", MsgBoxStyle.Information)
                        Exit Sub
                    End If
                    If Len(Trim(Tecnico.Text)) = 0 Or IsNumeric(Tecnico.SelectedValue) = False Then
                        MsgBox("Se requiere que Seleccione el Técnico por favor", MsgBoxStyle.Information)
                        Exit Sub
                    End If
                    If TieneAcometida = True Then
                        '''''''''''''''''''''''''''''''''''''''''''''''TieneAcometida = False
                        If Me.SectorCombo.SelectedValue = 0 Then
                            MsgBox("Se requiere que Capture el Sector", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        ACTUALIZASECTORCLIENTE(Me.ContratoTextBox.Text, Me.SectorCombo.SelectedValue)
                        TieneAcometida = False
                    End If
                ElseIf Me.StatusTextBox.Text = "V" Then
                    'Visita1
                    Dim Pasa As Integer = 0
                    If IsDate(Mid(Me.Visita1TextBox.Text, 1, 10)) = True Then
                        Dim Fecha As Date = Mid(Me.Visita1TextBox.Text, 1, 10)
                        If DateValue(Fecha) >= DateValue(Me.Fecha_SoliciutudMaskedTextBox.Value) And DateValue(Fecha) <= DateValue(Me.FechaDateTimePicker.Value) Then
                            Pasa = 1
                        Else
                            MsgBox("La Fecha de la Visita no puede ser Menor a la Fecha de Solicitud ni Mayor a la Fecha Actual ", MsgBoxStyle.Information)
                            Me.Visita1TextBox.Clear()
                            Exit Sub
                        End If
                    Else
                        MsgBox("La Fecha de Ejecución es Invalida")
                        Me.Visita1TextBox.Clear()
                        Exit Sub
                    End If
                    If IsDate(Me.Visita1TextBox.Text) = False Then
                        MsgBox("Se requiere que capture la fecha y la hora de Visita de forma correcta por favor", MsgBoxStyle.Information)
                        Exit Sub
                    End If
                    'Visita1                    
                    'Visita2
                    If IsDate(Mid(Me.Visita2TextBox.Text, 1, 10)) = True Then
                        Dim Fecha As Date = Mid(Me.Visita2TextBox.Text, 1, 10)
                        If DateValue(Fecha) >= DateValue(Me.Fecha_SoliciutudMaskedTextBox.Value) And DateValue(Fecha) <= DateValue(Me.FechaDateTimePicker.Value) Then
                            Pasa = 1
                        Else
                            MsgBox("La Fecha de la Visita no puede ser Menor a la Fecha de Solicitud ni mayor a la Fecha Actual ", MsgBoxStyle.Information)
                            Me.Visita2TextBox.Clear()
                            Exit Sub
                        End If
                    Else
                        'MsgBox("La Fecha de Ejecución es Invalida")
                        Me.Visita2TextBox.Clear()

                    End If
                    If IsDate(Me.Visita2TextBox.Text) = False Then
                        'MsgBox("Se requiere que capture la fecha y la hora de Visita de forma correcta por favor", MsgBoxStyle.Information)
                        Me.Visita2TextBox.Clear()
                    End If
                    'Visita2      
                    'Visita3
                    If IsDate(Mid(Me.Visita3TextBox.Text, 1, 10)) = True Then
                        Dim Fecha As Date = Mid(Me.Visita3TextBox.Text, 1, 10)
                        If DateValue(Fecha) >= DateValue(Me.Fecha_SoliciutudMaskedTextBox.Value) And DateValue(Fecha) <= DateValue(Me.FechaDateTimePicker.Value) Then
                            Pasa = 1
                        Else
                            MsgBox("La Fecha de la Visita no puede ser Menor a la Fecha de Solicitud ni mayor a la Fecha Actual ", MsgBoxStyle.Information)
                            Me.Visita3TextBox.Clear()
                            Exit Sub
                        End If
                    Else
                        'MsgBox("La Fecha de Ejecución es Invalida")
                        Me.Visita3TextBox.Clear()
                    End If
                    If IsDate(Me.Visita3TextBox.Text) = False Then
                        'MsgBox("Se requiere que capture la fecha y la hora de Visita de forma correcta por favor", MsgBoxStyle.Information)
                        Me.Visita3TextBox.Clear()
                    End If
                End If


                If LocValida1 = True Then

                    'Inserta_Bitacora_Tecnico()
                    'Inserta_Rel_Bitacora_Orden()
                    CON.Open()
                    ''Me.Dame_FolioTableAdapter.Connection = CON
                    ''Me.Dame_FolioTableAdapter.Fill(Me.DataSetarnoldo.Dame_Folio, gloClave, 1, Locclv_folio)
                    Locclv_folio = Dame_Folio(gloClave, 1, 0)
                    Inserta_Bitacora_tec_2(clv_sessionTecnico, CLng(gloClave), Locclv_folio, 1, Locclv_tec, GloClvUsuario, "P", "", Locclv_Alm)
                    'Me.Inserta_Bitacora_tecTableAdapter.Connection = CON
                    'Me.Inserta_Bitacora_tecTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Bitacora_tec, clv_sessionTecnico, CLng(gloClave), Locclv_folio, 1, Locclv_tec, GloClvUsuario, "P", "", LocNo_Bitacora)
                    Me.Inserta_Rel_Bitacora_OrdenTableAdapter.Connection = CON
                    Me.Inserta_Rel_Bitacora_OrdenTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Rel_Bitacora_Orden, LocNo_Bitacora, CLng(gloClave))
                    Me.Inserta_RelCobraDescTableAdapter.Connection = CON
                    Me.Inserta_RelCobraDescTableAdapter.Fill(Me.ProcedimientosArnoldo2.Inserta_RelCobraDesc, Locclv_folio, "O")
                    CON.Close()
                    LocValida1 = False
                End If
                If Bnd_CAPAR = True And Me.RadioButton3.Checked = False And Me.StatusTextBox.Text <> "P" Then
                    ChecaSiAparatoAsignado(Me.Clv_OrdenTextBox.Text, 3)
                    If Me.StatusTextBox.Text = "P" Then
                        If BndCambioEquipo = True Then
                            BndCambioEquipo = False
                            MsgBox("No se puede ejecutar la orden hasta que hayan sido asignados los equipos", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                    ElseIf Me.StatusTextBox.Text = "E" Then
                        If BndCambioEquipo = False Then
                            BndCambioEquipo = False
                            MsgBox("No se puede ejecutar la orden hasta que hayan sido asignados los equipos", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                End If


                If bndCCABM = True And Me.RadioButton3.Checked = False Then
                    ChecaSiAparatoAsignado(Me.Clv_OrdenTextBox.Text, 2)
                    If BndCambioEquipo = True Then
                        BndCambioEquipo = False
                        MsgBox("No se puede ejecutar la orden hasta que hayan sido asignados los equipos", MsgBoxStyle.Information)
                        Exit Sub
                    End If
                    CON.Open()
                    Me.Cambia_Tipo_cablemodemTableAdapter.Connection = CON
                    Me.Cambia_Tipo_cablemodemTableAdapter.Fill(Me.ProcedimientosArnoldo2.Cambia_Tipo_cablemodem, CLng(Me.Clv_OrdenTextBox.Text), LoctipoCablemdm)
                    Me.Guarda_Comentario_CCABMTableAdapter.Connection = CON
                    Me.Guarda_Comentario_CCABMTableAdapter.Fill(Me.ProcedimientosArnoldo2.Guarda_Comentario_CCABM, CLng(Me.Clv_OrdenTextBox.Text), LoctipoCablemdm)
                    CON.Close()
                End If

                'Eric
                eRes = 0
                eMsg = ""
                CON.Open()
                Me.ChecaMotivoCanServTableAdapter.Connection = CON
                Me.ChecaMotivoCanServTableAdapter.Fill(Me.DataSetEric.ChecaMotivoCanServ, Me.Clv_OrdenTextBox.Text, eRes, eMsg)
                CON.Close()
                If eRes = 1 Then

                    'If opcion = "M" And GloClv_TipSer = 1 Then
                    '    GloClv_MotCan = 0
                    '    FrmMotCan.Show()
                    '    Exit Sub
                    'End If

                    'If (opcion = "N" And GloClv_TipSer = 2) Or (opcion = "N" And GloClv_TipSer = 3) Then
                    '    GloClv_MotCan = 0
                    '    FrmMotCan.Show()
                    '    Exit Sub
                    'End If

                    If (opcion = "N") Then
                        GloClv_MotCan = 0
                        FrmMotCan.Show()
                        Exit Sub
                    End If


                End If

                ValidaCajasAsignadas()
                If Bnd_Orden = True And Me.RadioButton3.Checked = False Then
                    MsgBox("No Se Puede Ejecutar La Orden Hasta No Ser Asignados Todos Los Aparatos")
                    Bnd_Orden = False
                    Exit Sub
                End If
                '---------------------------------------
                'If (MaterialPendienteDeRetirar And DAEquipoExtra.StatusOrdSer = "P") Then
                '    MsgBox("La Orden no puede ser Ejecutada aún ya que existe Material y/o Aparatos por retirar.", MsgBoxStyle.Exclamation, "Aún no se puede ejecutar la Orden.")
                '    Exit Sub
                'End If


                'Verificamos si en la Orden hay Trabajos que impliquen Accesorios de Instalación. Si hay Accesorios de Instalación verifica que estén todos enlazados a un Artículo del Almacen.
                'If Not (DAEquipoExtra.StatusOrdSer = "P") Then
                '    Exit Sub
                'End If

                ''Validamos si es que el técnico cuenta con el Accesorio de Instalación necesario para ejecutar la Orden de Servicio.
                'If Not (DAEquipoExtra.StatusOrdSer = "P" And (DAEquipoExtra.uspValidaExistenciasTecnicoAccesoriosDeInstalacion(CType(Tecnico.SelectedValue, Integer), gloClave) = True)) Then
                '    Exit Sub
                'End If

                '*** Validaciones de Accesorios de Instalación y Retiro Parcial de Aparatos ***
                    If (DAEquipoExtra.StatusOrdSer = "P") Or (DAEquipoExtra.StatusOrdSer = "V") Then

                        If Not (DAEquipoExtra.uspValidaConfiguracionesRelTipoCajaAccesorio(gloClave) = True) Then
                            Exit Sub
                        End If

                        If Not (DAEquipoExtra.uspValidaExistenciasTecnicoAccesoriosDeInstalacion(CType(Tecnico.SelectedValue, Integer), gloClave) = True) Then
                            Exit Sub
                        End If

                        If Not (ValidaConfiguracionOrdenRetiroAparato()) Then
                            Exit Sub
                        End If

                        'Verificamos si vamos a guardar una orden de retiro o vamos a generar una nueva orden por medio de retiro parcial
                        If (ExisteUnDetalleDeRetiro) Then
                            GuardaRetiroParcialDeAparatos()
                            If RTrabajoInt = True Then
                                RTrabajoInt = False
                                Dim LocMsjR As String = SP_ValidaGuardaOrdSerAparatos(CLng(Clv_OrdenTextBox.Text), opcion, StatusTextBox.Text, 0)

                                If LocMsjR.Length > 0 Then
                                    MsgBox(LocMsjR, MsgBoxStyle.Information, "Información")
                                    Exit Sub
                                End If
                            End If
                            If TDetallesDeRetiro <> TDetallesDeRetiroMarcadoS Then

                                RTrabajoInt = False
                                RTrabajoDig = False

                                Dim i As Integer
                                For i = 0 To dgvDetOrdSer.RowCount - 1

                                    If Me.dgvDetOrdSer.Rows(i).Cells("trabajo").Value.ToString = "IANRA" Or Me.dgvDetOrdSer.Rows(i).Cells("trabajo").Value.ToString = "IROUT" Then
                                        RTrabajoInt = True
                                    End If

                                    If Me.dgvDetOrdSer.Rows(i).Cells("trabajo").Value.ToString = "IAPAR" Or Me.dgvDetOrdSer.Rows(i).Cells("trabajo").Value.ToString = "IANTE" Or Me.dgvDetOrdSer.Rows(i).Cells("trabajo").Value.ToString = "ICRCA" Or Me.dgvDetOrdSer.Rows(i).Cells("trabajo").Value.ToString = "IACDIG" Or Me.dgvDetOrdSer.Rows(i).Cells("trabajo").Value.ToString = "ECTRL" Then
                                        RTrabajoDig = True
                                    End If

                                Next

                                If RTrabajoInt = True And RTrabajoDig = True Then
                                    If Not (MsgBox("Se va a generar y ejecutar una nueva Orden de instalación parcial con los equipos y/o materiales marcados como ya Instalados. ¿Deseas seguir?", MsgBoxStyle.YesNo, "¿Deseas seguir con la Instalación parcial del material?") = MsgBoxResult.Yes) Then
                                        Exit Sub
                                    End If
                                    InstalaParcial = True
                                Else
                                    If Not (MsgBox("Se va a generar y ejecutar una nueva Orden de retiro parcial con los equipos y/o materiales marcados como ya retirados. ¿Deseas seguir?", MsgBoxStyle.YesNo, "¿Deseas seguir con la ejecución parcial del material?") = MsgBoxResult.Yes) Then
                                        Exit Sub
                                    End If
                                End If

                                EjecutaRetiroParcialDeAparatos()
                                'SAUL
                                UpdateComentarios(gloClave)
                                'SAUL(FIN)
                                RTrabajoInt = False
                                RTrabajoDig = False
                                Exit Sub
                            End If
                        End If

                    End If
                    '*** Fin de Validaciones de Accesorios de Instalación y Retiro Parcial de Aparatos ***
                    Dim LocMsj As String = SP_ValidaGuardaOrdSerAparatos(CLng(Clv_OrdenTextBox.Text), opcion, StatusTextBox.Text, 0)

                    If LocMsj.Length > 0 Then
                        MsgBox(LocMsj, MsgBoxStyle.Information, "Información")
                        Exit Sub
                    End If
                    'Eric
                    If (eResAco = 1 And opcion <> "N" And IdSistema = "TO") Or (eResAco = 1 And opcion <> "N" And IdSistema = "SA") Or (eResAco = 1 And opcion <> "N" And IdSistema = "VA") Then
                        If Me.PlacaTextBox.Text.Length > 0 Then
                            CON.Open()
                            Me.ConRelCtePlacaTableAdapter.Connection = CON
                            Me.ConRelCtePlacaTableAdapter.Insert(Me.ContratoTextBox.Text, Me.PlacaTextBox.Text, Me.Clv_OrdenTextBox.Text)
                            CON.Close()
                            GuardaRelOrdenUsuario()
                            'CON.Open()
                            Me.Validate()
                            Me.CONORDSERBindingSource.EndEdit()

                            ModOrdSer(Me.Clv_OrdenTextBox.Text, GloClv_TipSer, Me.ContratoTextBox.Text, Me.Fecha_SoliciutudMaskedTextBox.Value, Me.Fec_EjeTextBox1.Text, Me.Visita1TextBox1.Text, Me.Visita2TextBox1.Text, Me.StatusTextBox.Text, Me.Tecnico.SelectedValue, Me.IMPRESACheckBox.Checked, 0, Me.ObsTextBox.Text, "")
                            PreEjecutaOrdSer(Clv_OrdenTextBox.Text)
                            SP_GuardaOrdSerAparatos(Clv_OrdenTextBox.Text, opcion, StatusTextBox.Text, 0)

                            'Cuadrilla-----------------------------------------------------------------
                            If cbCuadrilla.SelectedValue > 0 Then
                                NueRelOrdSerCuadrilla(Clv_OrdenTextBox.Text, cbCuadrilla.SelectedValue)
                            End If
                            '--------------------------------------------------------------------------


                            MsgBox(mensaje5)
                            GloBnd = True
                            GloGuardo = False

                            'If Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 6) = "RETCA " Then
                            '    respuesta = MsgBox("El Aparato Es Entregado En Oficina: ", MsgBoxStyle.YesNo)
                            '    If respuesta = 6 Then
                            '        Inserta_Cablemodems_L()
                            '    Else
                            '        SALIDA_TECNICO_AUTO()
                            '    End If

                            'End If


                            'If opcion = "N" And IdSistema <> "VA" Then
                            '    CON.Open()
                            '    Me.Imprime_OrdenTableAdapter.Connection = CON
                            '    Me.Imprime_OrdenTableAdapter.Fill(Me.ProcedimientosArnoldo2.Imprime_Orden, Me.Clv_OrdenTextBox.Text, Imprime)
                            '    CON.Close()
                            '    If Imprime = 0 Then
                            '        ConfigureCrystalReports_NewXml(0, "")
                            '    ElseIf Imprime = 1 Then
                            '        MsgBox("La orden es de proceso Automático No se Imprimio", MsgBoxStyle.Information)
                            '    End If
                            'End If

                            Me.Close()
                        Else
                            MsgBox("Captura # de Placa.", , "Atención")
                        End If

                    Else
                        GuardaRelOrdenUsuario()
                        'CON.Open()
                        Me.Validate()
                        Me.CONORDSERBindingSource.EndEdit()

                        ModOrdSer(Me.Clv_OrdenTextBox.Text, GloClv_TipSer, Me.ContratoTextBox.Text, Me.Fecha_SoliciutudMaskedTextBox.Value, Me.Fec_EjeTextBox1.Text, Me.Visita1TextBox1.Text, Me.Visita2TextBox1.Text, Me.StatusTextBox.Text, Me.Tecnico.SelectedValue, Me.IMPRESACheckBox.Checked, 0, Me.ObsTextBox.Text, "")
                        PreEjecutaOrdSer(Clv_OrdenTextBox.Text)
                        SP_GuardaOrdSerAparatos(Clv_OrdenTextBox.Text, opcion, StatusTextBox.Text, 0)

                        If Me.StatusTextBox.Text = "V" Then
                            insertaVisitasOrdenes(gloClave, IIf(Me.Visita1TextBox1.Text = "", "01/01/1900", Me.Visita1TextBox1.Text), IIf(Me.Visita2TextBox1.Text = "", "01/01/1900", Me.Visita2TextBox1.Text), IIf(Me.Visita3TextBox1.Text = "", "01/01/1900", Me.Visita3TextBox1.Text), FormatDateTime(Me.dtpHoraVisita1.Value, DateFormat.ShortTime), FormatDateTime(Me.dtpHoraVisita3.Value, DateFormat.ShortTime), FormatDateTime(Me.dtpHoraVisita3.Value, DateFormat.ShortTime))
                        End If

                        'Cuadrilla-----------------------------------------------------------------
                        If cbCuadrilla.SelectedValue > 0 Then
                            NueRelOrdSerCuadrilla(Clv_OrdenTextBox.Text, cbCuadrilla.SelectedValue)
                        End If
                        '--------------------------------------------------------------------------

                        'Me.CONORDSERTableAdapter.Connection = CON
                        'Me.CONORDSERTableAdapter.Update(Me.NewSofTvDataSet.CONORDSER)
                        ''MODORDSER(CLng(Me.Clv_OrdenTextBox.Text), GloClv_TipSer, CLng(Me.ContratoTextBox.Text), Me.Fecha_SoliciutudMaskedTextBox.Text, Me.Fec_EjeTextBox.Text, "01/01/1900", "01/01/1900", "E", Me.Tecnico.SelectedValue, False, 0, "", "")

                        'Me.PREEJECUTAOrdSerTableAdapter.Connection = CON
                        'Me.PREEJECUTAOrdSerTableAdapter.Fill(Me.NewSofTvDataSet.PREEJECUTAOrdSer, New System.Nullable(Of Long)(CType(Me.Clv_OrdenTextBox.Text, Long)))
                        'CON.Close()
                        ' MsgBox(FormatDateTime(Me.Hora_Eje.Value, DateFormat.ShortTime))
                        If opcion <> "N" And opcion <> "C" And Me.StatusTextBox.Text <> "V" Then
                            Pon_La_HoraEje_Orden(Me.Clv_OrdenTextBox.Text, Mid(Me.Fec_EjeTextBox.Text, 1, 10), FormatDateTime(Me.Hora_Eje.Value, DateFormat.ShortTime))
                        End If
                        ' MsgBox(FormatDateTime(Me.Hora_Eje.Value, DateFormat.LongTime))
                        'MsgBox(Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 6))
                        'If Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 6) = "RETCA " Then
                        '    respuesta = MsgBox("El Aparato Es Entregado En Oficina: ", MsgBoxStyle.YesNo)
                        '    If respuesta = 6 Then
                        '        Inserta_Cablemodems_L()
                        '    Else
                        '        SALIDA_TECNICO_AUTO()
                        '    End If

                        'End If

                        MsgBox(mensaje5)


                        'Eric----------------------------------------------
                        Dim CONERIC As New SqlConnection(MiConexion)
                        Dim eRes As Long = 0
                        Dim eMsg As String = Nothing


                        CONERIC.Open()
                        Me.ChecaOrdSerRetiroTableAdapter.Connection = CONERIC
                        Me.ChecaOrdSerRetiroTableAdapter.Fill(Me.DataSetEric.ChecaOrdSerRetiro, CType(Me.Clv_OrdenTextBox.Text, Long), eRes, eMsg)
                        CONERIC.Close()
                        If (eRes > 0 And IdSistema = "SA") Or (eRes > 0 And IdSistema = "VA") Then
                            ImprimeOrdSerRetiro(eRes)
                        End If

                        '------------------------------------------------------

                        GloBnd = True
                        GloGuardo = False

                        If checaSiImprimeOrden() = True Then
                            ConfigureCrystalReports_NewXml(0, "")
                        End If
                        'If opcion = "N" Then
                        '    CON.Open()
                        '    Me.Imprime_OrdenTableAdapter.Connection = CON
                        '    Me.Imprime_OrdenTableAdapter.Fill(Me.ProcedimientosArnoldo2.Imprime_Orden, Me.Clv_OrdenTextBox.Text, Imprime)
                        '    CON.Close()
                        '    If Imprime = 0 And IdSistema <> "VA" Then
                        '        ConfigureCrystalReports_NewXml(0, "")
                        '    ElseIf Imprime = 1 Then
                        '        MsgBox("La orden es de proceso Automático No se Imprimio", MsgBoxStyle.Information)
                        '    End If
                        'End If
                        Me.Close()
                    End If



                Else
                    MsgBox(mensaje7)
                End If

                'CON.Close()
            Catch ex As System.Exception
                System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try
        Else
            MsgBox("No Se Puede Grabar,la Fecha de Ejecución No puede ser de Meses Anteriores ", MsgBoxStyle.Information)
            Me.TextBox4.Clear()
        End If
    End Sub

    Private Sub SP_GuardaOrdSerAparatos(ByVal oClv_Orden As Long, ByVal oOpcion As String, ByVal oStatus As String, ByVal oOp2 As Integer)
        '@CLV_ORDEN BIGINT=0,	@OPCION VARCHAR(10)='',	@STATUS VARCHAR(10)='',	@OP2 INT        
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_ORDEN", SqlDbType.BigInt, oClv_Orden)
        BaseII.CreateMyParameter("@OPCION", SqlDbType.VarChar, oOpcion, 10)
        BaseII.CreateMyParameter("@STATUS", SqlDbType.VarChar, oStatus, 10)
        BaseII.CreateMyParameter("@OP2", SqlDbType.Int, oOp2)
        BaseII.Inserta("SP_GuardaOrdSerAparatos")
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click

        Try

            If dameStatusOrdenQueja(CInt(Me.Clv_OrdenTextBox.Text), "O") = "P" And opcion = "M" Then
                Dim res = MsgBox("¿Deseas salir sin guardar la Descarga de Material?", MsgBoxStyle.YesNo)
                If res = MsgBoxResult.Yes Then
                    softv_BorraDescarga(CInt(Me.Clv_OrdenTextBox.Text), "O")
                Else
                    Exit Sub
                End If
            End If

            'Solo si la Orden está pendiente mostramos la pregunta
            'SAUL
            bndAcceso = 0
            uspAutorizadoQuejasOrdenes()
            If bndAcceso = 0 Then
                eRespuesta = 0
                eMensaje = ""
                uspValidaOrdenes()
                If eRespuesta = 1 And opcion = "N" Then
                    GloSalir = True
                    BORRAAPARATOS()
                    'Las Órdenes solo se borran si es que la Opción es "NUEVA"
                    If (opcion = "N") Then
                        GloGuardo = False
                        GloClv_TipSer = 0
                        GloNom_TipSer = ""
                        Borra_Camdo_si_no_guardo_orden()
                        Dim CON As New SqlConnection(MiConexion)
                        CON.Open()
                        Me.CONORDSERTableAdapter.Connection = CON
                        Me.CONORDSERTableAdapter.Delete(gloClave, 0)
                        CON.Close()
                    End If
                End If
            Else
                If (DAEquipoExtra.StatusOrdSer = "P") And opcion <> "C" And StatusTextBox.Text <> "E" Then
                    Dim msgres = MsgBox("¿Deseas Salir sin Guardar la Orden de Servicio?", MsgBoxStyle.YesNo)
                    If msgres = MsgBoxResult.Yes Then
                        GloSalir = True
                        BORRAAPARATOS()

                        'Las Órdenes solo se borran si es que la Opción es "NUEVA"
                        If (opcion = "N") Then

                            GloGuardo = False
                            'GloClv_TipSer = Me.ComboBox5.SelectedValue
                            GloClv_TipSer = 0
                            'GloNom_TipSer = Me.ComboBox5.Text
                            GloNom_TipSer = ""
                            Borra_Camdo_si_no_guardo_orden()
                            Dim CON As New SqlConnection(MiConexion)
                            CON.Open()
                            Me.CONORDSERTableAdapter.Connection = CON
                            Me.CONORDSERTableAdapter.Delete(gloClave, 0)
                            CON.Close()

                        End If

                    Else
                        Exit Sub
                    End If
                End If
            End If
            eRespuesta = 0
            eMensaje = ""
            '(Fin)

            Me.CONORDSERBindingSource.CancelEdit()
            GloBnd = True

            If Me.RadioButton1.Checked And opcion <> "C" Then
                BORRAAPARATOS()
            End If
            If IsNumeric(Me.Clv_OrdenTextBox.Text) = True Then
                If opcion = "N" Then

                    Dim CON As New SqlConnection(MiConexion)
                    CON.Open()
                    Me.Grabar_det_ordenTableAdapter.Connection = CON
                    Me.Grabar_det_ordenTableAdapter.Fill(Me.DataSetarnoldo.grabar_det_orden, CInt(Me.Clv_OrdenTextBox.Text))
                    CON.Close()
                End If
                'BORRAMETRAJE(CLng(Me.Clv_OrdenTextBox.Text))
            End If

            Me.Close()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub



    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        If Me.RadioButton2.Checked = True Then
            Me.StatusTextBox.Text = "E"
            GloControlaReloj = 1
            Me.Fec_EjeTextBox.Enabled = True
            Me.Hora_Eje.Enabled = True
            Me.Visita1TextBox.Enabled = False
            Me.Visita2TextBox.Enabled = False
            Me.Visita3TextBox.Enabled = False
            Me.dtpHoraVisita1.Enabled = False
            Me.dtpHoraVisita2.Enabled = False
            Me.dtpHoraVisita3.Enabled = False
            Me.Fec_EjeTextBox.Focus()
            '           Me.TextBox1.Visible = False
            ''Me.Label4.Visible = True
            ''Me.Label4.Text = "Ejecución : "
        End If

        'If Me.StatusTextBox.Text <> "E" Then
        '    Me.StatusTextBox.Text = "E"
        '    Me.Fecha_EjecucionMaskedTextBox.Visible = True
        '    Me.Label4.Visible = True
        '    Me.Label4.Text = "Ejecución : "
        '    If IsDate(Me.Fecha_SoliciutudMaskedTextBox.Text) = True Then
        '        Me.Fecha_EjecucionMaskedTextBox.MinDate = Me.Fecha_SoliciutudMaskedTextBox.Text
        '        Me.DameUltimo_dia_del_MesTableAdapter.Fill(Me.NewSofTvDataSet.DameUltimo_dia_del_Mes, New System.Nullable(Of Integer)(CType(Month(Me.Fecha_SoliciutudMaskedTextBox.Text), Integer)), New System.Nullable(Of Integer)(CType(Year(Me.Fecha_SoliciutudMaskedTextBox.Text), Integer)))
        '        Me.Fecha_EjecucionMaskedTextBox.MaxDate = FechaDateTimePicker.Value
        '    End If
        'ElseIf Me.StatusTextBox.Text = "E" Then
        '    Me.Fecha_EjecucionMaskedTextBox.Visible = True
        '    Me.Label4.Visible = True
        '    Me.Label4.Text = "Ejecución : "
        'End If
    End Sub

    Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton3.CheckedChanged
        If Me.RadioButton3.Checked = True Then
            GloControlaReloj = 0
            Me.Panel5.BackColor = Color.WhiteSmoke
            Me.Panel6.BackColor = Color.WhiteSmoke
            'Me.Panel9.BackColor = Color.WhiteSmoke
            Me.StatusTextBox.Text = "V"
            '    Me.TextBox1.Visible = False
            Me.Fec_EjeTextBox.Enabled = False
            Me.Hora_Eje.Enabled = False
            Me.Visita1TextBox.Enabled = True
            Me.Visita2TextBox.Enabled = True
            Me.Visita3TextBox.Enabled = True
            Me.dtpHoraVisita1.Enabled = True
            Me.dtpHoraVisita2.Enabled = True
            Me.dtpHoraVisita3.Enabled = True
            If IsDate(Me.Visita1TextBox1.Text) = True Then
                Me.Visita1TextBox.Enabled = False
                Me.dtpHoraVisita1.Enabled = False
            End If
            If IsDate(Me.Visita2TextBox.Text) = True Then
                Me.Visita2TextBox.Enabled = False
                Me.dtpHoraVisita2.Enabled = False
            End If
            If IsDate(Me.Visita3TextBox.Text) = True Then
                Me.Visita3TextBox.Enabled = False
                Me.dtpHoraVisita3.Enabled = False
            End If
            Me.Visita1TextBox.Focus()
            ''Me.Label4.Visible = True
            ''Me.Label4.Text = "Visita : "
        End If
        'Me.StatusTextBox.Text = "V"
        'If Me.StatusTextBox.Text <> "V" Then
        '    Me.Fecha_EjecucionMaskedTextBox.Visible = True
        '    Me.Label4.Visible = True
        '    Me.StatusTextBox.Text = "V"
        '    Me.Label4.Text = "Visita : "
        '    If IsDate(Me.Fecha_SoliciutudMaskedTextBox.Text) = True Then
        '        Me.Fecha_EjecucionMaskedTextBox.MinDate = Me.Fecha_SoliciutudMaskedTextBox.Text
        '        Me.DameUltimo_dia_del_MesTableAdapter.Fill(Me.NewSofTvDataSet.DameUltimo_dia_del_Mes, New System.Nullable(Of Integer)(CType(Month(Me.Fecha_SoliciutudMaskedTextBox.Text), Integer)), New System.Nullable(Of Integer)(CType(Year(Me.Fecha_SoliciutudMaskedTextBox.Text), Integer)))
        '        Me.Fecha_EjecucionMaskedTextBox.MaxDate = FechaDateTimePicker.Value
        '    End If
        'ElseIf Me.StatusTextBox.Text = "V" Then
        '    Me.Fecha_EjecucionMaskedTextBox.Visible = True
        '   
        '    Me.Label4.Visible = True
        '    Me.Label4.Text = "Visita : "
        'End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        GLOCONTRATOSEL = 0
        'op = 3
        GloClv_TipSer = 0
        'FrmSelCliente.Show()
        Dim frmsc As FrmSelCliente = New FrmSelCliente()
        frmsc.ShowDialog()
        If (frmsc.DialogResult = Windows.Forms.DialogResult.OK) Then
            If (frmsc.CONTRATOLabel1.Text.Length > 0) Then
                Me.ContratoTextBox.Text = frmsc.CONTRATOLabel1.Text

                If IsNumeric(Me.ContratoTextBox.Text) = True And CInt(Me.ContratoTextBox.Text) > 0 Then
                    If checaClienteActivo(CInt(Me.ContratoTextBox.Text)) = False Then
                        MsgBox("El status del contrato no es instalado", MsgBoxStyle.Information)
                        Me.ContratoTextBox.Clear()
                        Exit Sub
                    End If

                    'SAUL
                    bndAcceso = 0
                    uspAutorizadoQuejasOrdenes()
                    If bndAcceso = 0 Then
                        eRespuesta = 0
                        eMensaje = ""
                        uspValidaOrdenes()
                        If eRespuesta = 1 And opcion = "N" Then
                            MsgBox(eMensaje)
                            Me.Button9.Enabled = False
                            CONORDSERBindingNavigatorSaveItem.Enabled = False
                        Else
                            Me.Button9.Enabled = True
                            CONORDSERBindingNavigatorSaveItem.Enabled = True
                        End If
                        eRespuesta = 0
                        eMensaje = ""
                    End If
                End If
                '(Fin)
            End If
        End If
    End Sub

    Private Sub uspValidaOrdenes()

        If IsNumeric(Me.ContratoTextBox.Text) = False Then
            Me.ContratoTextBox.Text = 0
        End If
        If Me.ContratoTextBox.Text.Length = 0 Then
            Me.ContratoTextBox.Text = 0
        End If

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, Me.ContratoTextBox.Text)
        BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@Res", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@Msg", SqlDbType.VarChar, 150, ParameterDirection.Output, "")
        'ByVal prmNombre As String, ByRef prmTipo As SqlDbType, ByRef prmTamanio As Integer, ByRef prmDireccion As ParameterDirection, ByRef prmValor As Object
        BaseII.ProcedimientoOutPut("uspValidaOrdenes")
        eRespuesta = CInt(BaseII.dicoPar("@Res").ToString)
        eMensaje = BaseII.dicoPar("@Msg").ToString
    End Sub

    Private Sub ConfigureCrystalReports(ByVal op As String, ByVal Titulo As String)
        Try
            Dim CON As New SqlConnection(MiConexion)


            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0"
            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
            Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"
            Dim Impresora As String = Nothing
            Dim a As Integer = 0



            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim mySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"


            Dim reportPath As String = Nothing

            If IdSistema = "AG" Then

                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBueno.rpt"
            ElseIf IdSistema = "TO" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCabStar.rpt"
            ElseIf IdSistema = "SA" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoTvRey.rpt"
            ElseIf IdSistema = "VA" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCosmo.rpt"
            ElseIf IdSistema = "LO" Or IdSistema = "YU" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoLogitel.rpt"
            End If


            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Clv_TipSer int
            customersByCityReport.SetParameterValue(0, 0)
            ',@op1 smallint
            customersByCityReport.SetParameterValue(1, 1)
            ',@op2 smallint
            customersByCityReport.SetParameterValue(2, 0)
            ',@op3 smallint
            customersByCityReport.SetParameterValue(3, 0)
            ',@op4 smallint,
            customersByCityReport.SetParameterValue(4, 0)
            '@op5 smallint
            customersByCityReport.SetParameterValue(5, 0)
            ',@StatusPen bit
            customersByCityReport.SetParameterValue(6, 0)
            ',@StatusEje bit
            customersByCityReport.SetParameterValue(7, 0)
            ',@StatusVis bit,
            customersByCityReport.SetParameterValue(8, 0)
            '@Clv_OrdenIni bigint
            customersByCityReport.SetParameterValue(9, CLng(Me.Clv_OrdenTextBox.Text))
            ',@Clv_OrdenFin bigint
            customersByCityReport.SetParameterValue(10, CLng(Me.Clv_OrdenTextBox.Text))
            ',@Fec1Ini Datetime
            customersByCityReport.SetParameterValue(11, "01/01/1900")
            ',@Fec1Fin Datetime,
            customersByCityReport.SetParameterValue(12, "01/01/1900")
            '@Fec2Ini Datetime
            customersByCityReport.SetParameterValue(13, "01/01/1900")
            ',@Fec2Fin Datetime
            customersByCityReport.SetParameterValue(14, "01/01/1900")
            ',@Clv_Trabajo int
            customersByCityReport.SetParameterValue(15, 0)
            ',@Clv_Colonia int
            customersByCityReport.SetParameterValue(16, 0)
            ',@OpOrden int
            customersByCityReport.SetParameterValue(17, OpOrdenar)

            mySelectFormula = "Orden " & GloNom_TipSer
            customersByCityReport.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"
            CON.Open()
            Me.Dame_Impresora_OrdenesTableAdapter.Connection = CON
            Me.Dame_Impresora_OrdenesTableAdapter.Fill(Me.DataSetarnoldo.Dame_Impresora_Ordenes, Impresora, a)
            CON.Close()
            If a = 1 Then
                MsgBox("No se tiene asignada una Impresora de Ordenes de Servicio", MsgBoxStyle.Information)
                Exit Sub
            Else
                customersByCityReport.PrintOptions.PrinterName = Impresora
                customersByCityReport.PrintToPrinter(1, True, 1, 1)
            End If
            '--SetDBLogonForReport(connectionInfo)

            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ConfigureCrystalReports_NewXml(ByVal op As String, ByVal Titulo As String)
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0"
            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
            Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"
            Dim Impresora As String = Nothing
            Dim a As Integer = 0

            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword

            Dim mySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"


            Dim reportPath As String = Nothing

            uspDimeTrabajo()

            If IdSistema = "TO" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCabStar.rpt"
            ElseIf IdSistema = "SA" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoTvRey.rpt"
            ElseIf IdSistema = "AG" Then
                If GloTrabajoR = 1 Then
                    reportPath = RutaReportes + "\ReporteFormatoOrdenesServRetiro.rpt"
                Else
                    reportPath = RutaReportes + "\ReporteFormatoOrdenesServBueno.rpt"
                End If
            ElseIf IdSistema = "VA" Then
                    reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCosmo.rpt"
            ElseIf IdSistema = "LO" Then
                    reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoLogitel.rpt"
                End If


                Dim cnn As New SqlConnection(MiConexion)
                Dim cmd As New SqlCommand("ReporteAreaTecnicaOrdSer1", cnn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandTimeout = 0

                Dim parametro1 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
                parametro1.Direction = ParameterDirection.Input
                parametro1.Value = 0
                cmd.Parameters.Add(parametro1)

                Dim parametro2 As New SqlParameter("@op1", SqlDbType.SmallInt)
                parametro2.Direction = ParameterDirection.Input
                parametro2.Value = 1
                cmd.Parameters.Add(parametro2)

                Dim parametro3 As New SqlParameter("@op2", SqlDbType.SmallInt)
                parametro3.Direction = ParameterDirection.Input
                parametro3.Value = Op2
                cmd.Parameters.Add(parametro3)

                Dim parametro4 As New SqlParameter("@op3", SqlDbType.SmallInt)
                parametro4.Direction = ParameterDirection.Input
                parametro4.Value = Op3
                cmd.Parameters.Add(parametro4)

                Dim parametro5 As New SqlParameter("@op4", SqlDbType.SmallInt)
                parametro5.Direction = ParameterDirection.Input
                parametro5.Value = Op4
                cmd.Parameters.Add(parametro5)

                Dim parametro6 As New SqlParameter("@op5", SqlDbType.SmallInt)
                parametro6.Direction = ParameterDirection.Input
                parametro6.Value = Op5
                cmd.Parameters.Add(parametro6)

                Dim parametro7 As New SqlParameter("@StatusPen", SqlDbType.Bit)
                parametro7.Direction = ParameterDirection.Input
                If StatusPen = "1" Then
                    parametro7.Value = True
                Else
                    parametro7.Value = False
                End If
                cmd.Parameters.Add(parametro7)

                Dim parametro8 As New SqlParameter("@StatusEje", SqlDbType.Bit)
                parametro8.Direction = ParameterDirection.Input
                If StatusEje = "1" Then
                    parametro8.Value = True
                Else
                    parametro8.Value = False
                End If
                cmd.Parameters.Add(parametro8)

                Dim parametro9 As New SqlParameter("@StatusVis", SqlDbType.Bit)
                parametro9.Direction = ParameterDirection.Input
                If StatusVis = "1" Then
                    parametro9.Value = True
                Else
                    parametro9.Value = False
                End If
                cmd.Parameters.Add(parametro9)

                Dim parametro10 As New SqlParameter("@Clv_OrdenIni", SqlDbType.BigInt)
                parametro10.Direction = ParameterDirection.Input
                parametro10.Value = CLng(Me.Clv_OrdenTextBox.Text)
                cmd.Parameters.Add(parametro10)

                Dim parametro11 As New SqlParameter("@Clv_OrdenFin", SqlDbType.BigInt)
                parametro11.Direction = ParameterDirection.Input
                parametro11.Value = CLng(Me.Clv_OrdenTextBox.Text)
                cmd.Parameters.Add(parametro11)

                Dim parametro12 As New SqlParameter("@Fec1Ini", SqlDbType.DateTime)
                parametro12.Direction = ParameterDirection.Input
                parametro12.Value = Fec1Ini
                cmd.Parameters.Add(parametro12)

                Dim parametro13 As New SqlParameter("@Fec1Fin", SqlDbType.DateTime)
                parametro13.Direction = ParameterDirection.Input
                parametro13.Value = Fec1Fin
                cmd.Parameters.Add(parametro13)

                Dim parametro14 As New SqlParameter("@Fec2Ini", SqlDbType.DateTime)
                parametro14.Direction = ParameterDirection.Input
                parametro14.Value = Fec2Ini
                cmd.Parameters.Add(parametro14)

                Dim parametro15 As New SqlParameter("@Fec2Fin", SqlDbType.DateTime)
                parametro15.Direction = ParameterDirection.Input
                parametro15.Value = Fec2Fin
                cmd.Parameters.Add(parametro15)

                Dim parametro16 As New SqlParameter("@Clv_Trabajo", SqlDbType.Int)
                parametro16.Direction = ParameterDirection.Input
                parametro16.Value = nclv_trabajo
                cmd.Parameters.Add(parametro16)

                Dim parametro17 As New SqlParameter("@Clv_Colonia", SqlDbType.Int)
                parametro17.Direction = ParameterDirection.Input
                parametro17.Value = nClv_colonia
                cmd.Parameters.Add(parametro17)

                Dim parametro18 As New SqlParameter("@OpOrden", SqlDbType.Int)
                parametro18.Direction = ParameterDirection.Input
                parametro18.Value = OpOrdenar
                cmd.Parameters.Add(parametro18)
                Dim parametro22 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                parametro22.Direction = ParameterDirection.Input
                parametro22.Value = 0
                cmd.Parameters.Add(parametro22)
                Dim parametro23 As New SqlParameter("@ResVisita", SqlDbType.Int)
                parametro23.Direction = ParameterDirection.Input
                parametro23.Value = 0
                cmd.Parameters.Add(parametro23)

                Dim parametro24 As New SqlParameter("@op6", SqlDbType.SmallInt)
                parametro24.Direction = ParameterDirection.Input
                parametro24.Value = 0
                cmd.Parameters.Add(parametro24)

                Dim parametro25 As New SqlParameter("@clvDepto", SqlDbType.BigInt)
                parametro25.Direction = ParameterDirection.Input
                parametro25.Value = 0
                cmd.Parameters.Add(parametro25)

                Dim parametro26 As New SqlParameter("@clvTipoServicioDigital", SqlDbType.Int)
                parametro26.Direction = ParameterDirection.Input
                parametro26.Value = 0
                cmd.Parameters.Add(parametro26)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet()

                da.Fill(ds)
                ds.Tables(0).TableName = "ReporteAreaTecnicaOrdSer"
                ds.Tables(1).TableName = "DameDatosGenerales_2"
            ds.Tables(2).TableName = "Cosulta_OrdRetiro"
            ds.Tables(3).TableName = "TablaDatos"
            ds.Tables(4).TableName = "tabla5Queja"
            ds.Tables(5).TableName = "tblUltOrdQueja"

                customersByCityReport.Load(reportPath)
                customersByCityReport.SetDataSource(ds)

            '    Dim CON12 As New SqlConnection(MiConexion)
            '    CON12.Open()
            '    Me.Dame_Impresora_OrdenesTableAdapter.Connection = CON12
            'Me.Dame_Impresora_OrdenesTableAdapter.Fill(Me.NewsoftvDataSet2.Dame_Impresora_Ordenes, Impresora, a)
            '    CON12.Dispose()
            '    CON12.Close()
                If a = 1 Then
                    MsgBox("No se tiene asignada una Impresora de Ordenes de Servicio", MsgBoxStyle.Information)
                    Exit Sub
                Else
                customersByCityReport.PrintOptions.PrinterName = ImpresoraOrd
                    customersByCityReport.PrintToPrinter(1, True, 1, 1)
                End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Me.Clv_TecnicoTextBox.Text = Me.ComboBox1.SelectedValue
    End Sub

    Private Sub ContratoTextBox_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ContratoTextBox.GotFocus
        Me.ContratoTextBox.SelectionStart = 0
        Me.ContratoTextBox.SelectionLength = Len(Me.ContratoTextBox.Text)
    End Sub

    Private Sub ContratoTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles ContratoTextBox.KeyPress
        e.KeyChar = Chr(ValidaKey(ContratoTextBox, Asc(LCase(e.KeyChar)), "N"))
        If Asc(e.KeyChar) = 13 Then
            If Me.ContratoTextBox.Text.Length > 0 Then
                If IsNumeric(Me.ContratoTextBox.Text) = True And CInt(Me.ContratoTextBox.Text) > 0 Then
                    If checaClienteActivo(CInt(Me.ContratoTextBox.Text)) = False Then
                        MsgBox("El status del contrato no es instalado", MsgBoxStyle.Information)
                        Me.ContratoTextBox.Clear()
                        Exit Sub
                    End If

                    GloContratoord = CLng(Me.ContratoTextBox.Text)
                    BUSCACLIENTES(0)
                    'SAUL
                    bndAcceso = 0
                    uspAutorizadoQuejasOrdenes()
                    If bndAcceso = 0 Then
                        eRespuesta = 0
                        eMensaje = ""
                        uspValidaOrdenes()
                        If eRespuesta = 1 And opcion = "N" Then
                            MsgBox(eMensaje)
                            Me.Button9.Enabled = False
                            CONORDSERBindingNavigatorSaveItem.Enabled = False
                        Else
                            Me.Button9.Enabled = True
                            CONORDSERBindingNavigatorSaveItem.Enabled = True
                        End If
                        eRespuesta = 0
                        eMensaje = ""
                    End If
                    '(Fin) 
                Else
                    Me.ContratoTextBox.Text = GloContratoord
                End If
            End If
        End If
    End Sub

    'Private Sub ContratoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContratoTextBox.TextChanged
    '    If IsNumeric(Me.ContratoTextBox.Text) = True Then
    '        GloContratoord = CLng(Me.ContratoTextBox.Text)
    '    Else
    '        Me.ContratoTextBox.Text = GloContratoord

    '    End If
    '    'Me.BUSCACLIENTES(0)

    'End Sub

    Private Sub ContratoTextBox_LostFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContratoTextBox.LostFocus
        If IsNumeric(Me.ContratoTextBox.Text) = True Then
            GloContratoord = CLng(Me.ContratoTextBox.Text)
            BUSCACLIENTES(0)
            'Else
            '    Me.ContratoTextBox.Text = GloContratoord
        End If
    End Sub

    Private Sub Clv_OrdenTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_OrdenTextBox.TextChanged

        If IsNumeric(Me.Clv_OrdenTextBox.Text) = True Then
            gloClave = Me.Clv_OrdenTextBox.Text
            dimebitacora()
        Else
            Me.Clv_OrdenTextBox.Text = gloClave
        End If
    End Sub


    Private Sub StatusTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StatusTextBox.TextChanged
        If Me.StatusTextBox.Text = "P" Then
            If Me.RadioButton1.Checked = False Then Me.RadioButton1.Checked = True
        ElseIf Me.StatusTextBox.Text = "E" Then
            If Me.RadioButton2.Checked = False Then
                Me.RadioButton2.Checked = True
                Me.TextBox1.Visible = False
                'Me.Fecha_EjecucionMaskedTextBox.Visible = True
                ''Me.Label4.Visible = True
                ''Me.Label4.Text = "Ejecución : "
            End If
        ElseIf Me.StatusTextBox.Text = "V" Then
            If Me.RadioButton3.Checked = False Then
                Me.RadioButton3.Checked = True
                Me.TextBox1.Visible = False

                'Me.Fecha_EjecucionMaskedTextBox.Visible = True
                '' Me.Label4.Visible = True
                '' Me.Label4.Text = "Visita : "
            End If

        End If
    End Sub



    Private Sub Fecha_SoliciutudMaskedTextBox_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fecha_SoliciutudMaskedTextBox.ValueChanged
        If Me.StatusTextBox.Text = "P" Then
            'If IsDate(Me.Fecha_SoliciutudMaskedTextBox.Text) = True Then
            ' Me.Fecha_EjecucionMaskedTextBox.MinDate = Me.Fecha_SoliciutudMaskedTextBox.Text
            ' Me.DameUltimo_dia_del_MesTableAdapter.Fill(Me.NewSofTvDataSet.DameUltimo_dia_del_Mes, New System.Nullable(Of Integer)(CType(Month(Me.Fecha_SoliciutudMaskedTextBox.Text), Integer)), New System.Nullable(Of Integer)(CType(Year(Me.Fecha_SoliciutudMaskedTextBox.Text), Integer)))
            ' Me.Fecha_EjecucionMaskedTextBox.MaxDate = FechaDateTimePicker.Value
            'End If
        End If
    End Sub

    Private Sub ComboBox5_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox5.SelectedIndexChanged
        'If Me.ComboBox5.SelectedValue <> Nothing Then
        '    GloClv_TipSer = Me.ComboBox5.SelectedValue
        '    Me.TextBox2.Text = Me.ComboBox5.Text
        '    GloNom_TipSer = Me.ComboBox5.Text
        'End If
    End Sub


    Private Sub Fec_EjeTextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fec_EjeTextBox1.TextChanged
        Me.Fec_EjeTextBox.Text = Me.Fec_EjeTextBox1.Text
    End Sub

    Private Sub Visita1TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Visita1TextBox1.TextChanged
        Me.Visita1TextBox.Text = Me.Visita1TextBox1.Text
    End Sub

    Private Sub Visita2TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Visita2TextBox1.TextChanged
        Me.Visita2TextBox.Text = Me.Visita2TextBox1.Text
    End Sub

    Private Sub Fec_EjeTextBox_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Fec_EjeTextBox.GotFocus
        Fec_EjeTextBox.SelectionStart = 0
        Fec_EjeTextBox.SelectionLength = Len(Fec_EjeTextBox.Text)
    End Sub



    Private Sub Fec_EjeTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Fec_EjeTextBox.TextChanged
        If IsDate(Mid(Me.Fec_EjeTextBox.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.Fec_EjeTextBox.Text, 1, 10)
            If DateValue(Fecha) = DateValue("01/01/1900") Then
                Me.Fec_EjeTextBox.Clear()
            End If
        End If
        If IsDate(Me.Fec_EjeTextBox.Text) = True Then
            Me.Fec_EjeTextBox1.Text = Me.Fec_EjeTextBox.Text
            Panel5.BackColor = Color.WhiteSmoke
            If Len(Trim(Me.Fec_EjeTextBox1.Text)) = 10 Then
                LocFecEje = True
            End If
        Else
            LocFecEje = False
        End If
    End Sub

    Private Sub Visita1TextBox_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Visita1TextBox.GotFocus
        Visita1TextBox.SelectionStart = 0
        Visita1TextBox.SelectionLength = Len(Visita1TextBox.Text)
    End Sub


    Private Sub Visita1TextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Visita1TextBox.TextChanged
        If IsDate(Mid(Me.Visita1TextBox.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.Visita1TextBox.Text, 1, 10)
            If DateValue(Fecha) = DateValue("01/01/1900") Then
                Me.Visita1TextBox.Clear()
            End If
        End If
        If IsDate(Me.Visita1TextBox.Text) = True Then
            Me.Visita1TextBox1.Text = Me.Visita1TextBox.Text
        End If
    End Sub

    Private Sub Visita2TextBox_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Visita2TextBox.GotFocus
        Visita2TextBox.SelectionStart = 0
        Visita2TextBox.SelectionLength = Len(Visita2TextBox.Text)
    End Sub

    Private Sub Visita2TextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Visita2TextBox.TextChanged
        If IsDate(Mid(Me.Visita2TextBox.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.Visita2TextBox.Text, 1, 10)
            If DateValue(Fecha) = DateValue("01/01/1900") Then
                Me.Visita2TextBox.Clear()
            End If
        End If
        If IsDate(Me.Visita2TextBox.Text) = True Then
            Me.Visita2TextBox1.Text = Me.Visita2TextBox.Text
        End If
    End Sub

    Private Sub Visita3TextBox_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Visita3TextBox.GotFocus
        Visita3TextBox.SelectionStart = 0
        Visita3TextBox.SelectionLength = Len(Visita3TextBox.Text)
    End Sub

    Private Sub Visita3TextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Visita3TextBox.TextChanged
        If IsDate(Mid(Me.Visita3TextBox.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.Visita3TextBox.Text, 1, 10)
            If DateValue(Fecha) = DateValue("01/01/1900") Then
                Me.Visita3TextBox.Clear()
            End If
        End If
        If IsDate(Me.Visita3TextBox.Text) = True Then
            Me.Visita3TextBox1.Text = Me.Visita3TextBox.Text
        End If
    End Sub

    Private Sub Button9_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click

        Dim clavedetordSer As Long = 0
        If Me.StatusTextBox.Text = "P" Then
            If IsNumeric(Me.ContratoTextBox.Text) = False Then
                MsgBox(mensaje7)
                Exit Sub
            End If
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.Validate()
            Me.CONORDSERBindingSource.EndEdit()
            Me.CONORDSERTableAdapter.Connection = CON
            Me.CONORDSERTableAdapter.Update(Me.NewSofTvDataSet.CONORDSER)

            'Este no Me.CONDetOrdSerTableAdapter.Insert(Me.Clv_OrdenTextBox.Text, Me.Clv_TrabajoTextBox.Text, Me.ObsTextBox1.Text, Me.SeRealizaCheckBox.Checked, clavedetordSer)
            gloClv_Orden = Me.Clv_OrdenTextBox.Text
            Contrato = Me.ContratoTextBox.Text
            CON.Close()
            FrmDetOrSer.ShowDialog()

            'Cargamos el Detalle de la Orden al Grid
            CargaDetOrdSer(gloClave)
            'Habilitamos solo aquellos detalles cuyo trabajo pueda implique el retiro de algún aparato (RAPAR,RCTRL,etc.)
            HabilitaRetiroAparatoPorDetalle(RelTrabajosRetiroAparatos)
            If Me.dgvDetOrdSer.Rows.Count > 0 Then
                Me.ContratoTextBox.Enabled = False
                Me.Button1.Enabled = False
            End If
        Else
            MsgBox("Solo se puede agregar Servicios al Cliente cuando esta con Status de Pendiente", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub Button2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If IsNumeric(Me.Clv_OrdenTextBox.Text) = True Then
            Valida_DetOrdenDelete(CInt(Me.Clv_OrdenTextBox.Text), 1)

            If valor > 0 Then
                If IsNumeric(Me.dgvDetOrdSer.CurrentRow.Cells("clave").Value) = True Then
                    Me.ClaveTextBox.Text = Me.dgvDetOrdSer.CurrentRow.Cells("clave").Value
                End If
                If IsNumeric(Me.ClaveTextBox.Text) = True Then

                    Dim CON As New SqlConnection(MiConexion)
                    CON.Open()

                    Me.BorraMotivoCanServTableAdapter.Connection = CON
                    Me.BorraMotivoCanServTableAdapter.Fill(Me.DataSetEric.BorraMotivoCanServ, Me.Clv_OrdenTextBox.Text, 0, Me.ClaveTextBox.Text, 0, 2)

                    Me.CONDetOrdSerTableAdapter.Connection = CON
                    Me.CONDetOrdSerTableAdapter.Delete(Me.ClaveTextBox.Text)


                    'Cargamos el Detalle de la Orden al Grid
                    CargaDetOrdSer(gloClave)
                    'Habilitamos solo aquellos detalles cuyo trabajo pueda implique el retiro de algún aparato (RAPAR,RCTRL,etc.)
                    HabilitaRetiroAparatoPorDetalle(RelTrabajosRetiroAparatos)

                    CON.Close()
                    Me.ClaveTextBox.Text = 0
                End If
            End If
        End If
        If Me.dgvDetOrdSer.Rows.Count = 0 Then
            Me.ContratoTextBox.Enabled = True
            Me.Button1.Enabled = True
        End If

    End Sub
    Private Sub dame_clv_tipser(ByVal clv_txt As String)
        Dim CON As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand
        cmd = New SqlClient.SqlCommand()
        Try
            If GloClv_TipSer > 0 Then
                GloClv_TipSer = 0
            End If
            CON.Open()
            With cmd
                .CommandText = "Dame_tipo_servicio_trabajo"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = CON

                Dim prm As New SqlParameter("@clv_txt", SqlDbType.VarChar, 10)
                Dim prm1 As New SqlParameter("@clv_tipser", SqlDbType.Int)

                prm.Direction = ParameterDirection.Input
                prm1.Direction = ParameterDirection.Output

                prm.Value = clv_txt
                prm1.Value = 0

                .Parameters.Add(prm)
                .Parameters.Add(prm1)

                Dim i As Integer = cmd.ExecuteNonQuery()

                GloClv_TipSer = prm1.Value
            End With
            CON.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BUSCADetOrdSerDataGridView_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles BUSCADetOrdSerDataGridView.CellClick

        Dim Pasa As Integer = 0
        If GloControlaReloj = 0 Then
            Pasa = 0
        Else
            If LocTec = True And LocFecEje = True Then
                Pasa = 0
            Else
                Pasa = 1
            End If
        End If

        Locclv_tec = Me.Tecnico.SelectedValue
        ''dame_clv_tipser(

        If Pasa = 0 Then
            If IsNumeric(BUSCADetOrdSerDataGridView.SelectedCells(0).Value) = True Then
                GloDetClave = BUSCADetOrdSerDataGridView.SelectedCells(0).Value
                Contrato = Me.ContratoTextBox.Text
                gloClv_Orden = Me.Clv_OrdenTextBox.Text
                GLOTRABAJO = RTrim(LTrim(Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 6)))
                dame_clv_tipser(GLOTRABAJO)
                If Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "CAMDO" Or Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "CADIG" _
                    Or Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "CADIG6" Or Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "CANET" Then
                    Me.BUSCADetOrdSerDataGridView.Enabled = False
                    FrmCAMDO.Show()
                ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "CONEX" Then

                    Me.BUSCADetOrdSerDataGridView.Enabled = False
                    FrmCONEX.Show()
                ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "CEXTE" Then

                    Me.BUSCADetOrdSerDataGridView.Enabled = False
                    FrmCEXTE.Show()
                ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "CANEX" Then

                    Me.BUSCADetOrdSerDataGridView.Enabled = False
                    FrmCANEX.Show()
                ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "BCABM" Then

                    Me.BUSCADetOrdSerDataGridView.Enabled = False
                    FrmRelCablemodemClientes.Show()
                ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "BAPAR" Or Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "BAPAR6" Then
                    Me.BUSCADetOrdSerDataGridView.Enabled = False
                    FrmRelCablemodemClientesDigital.Show()
                ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "ICABM" Then
                    If Me.StatusTextBox.Text = "P" Then
                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        'FrmRelCablemodemClientes.Show()
                        FrmICABMAsigna.Show()
                    ElseIf Me.StatusTextBox.Text = "E" Then
                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        'GloClv_TipSer = 2
                        FrmICABMAsigna.Show()
                    End If
                ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 6) = "ICABMT" Then
                    If Me.StatusTextBox.Text = "P" Then
                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        'FrmRelCablemodemClientes.Show()
                        FrmICABMAsigna.Show()
                    ElseIf Me.StatusTextBox.Text = "E" Then
                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        'GloClv_TipSer = 2
                        FrmICABMAsigna.Show()
                    End If
                ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "IAPAR" Or Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "RIAPA" _
                    Or Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "IAPAR6" Or Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "RIAPA6" Then
                    If Me.StatusTextBox.Text = "P" Then
                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        'FrmRelCablemodemClientesDigital.Show()
                        FrmIAPARAsigna.Show()
                    ElseIf Me.StatusTextBox.Text = "E" Then
                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        'GloClv_TipSer = 3
                        FrmIAPARAsigna.Show()
                    End If
                ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "CCABM" Then
                    If Me.StatusTextBox.Text = "P" Then
                        MsgBox("Sólo se puede Asignar el Aparato hasta que la Orden sea Ejecutada", , "Atención")
                    ElseIf Me.StatusTextBox.Text = "E" Then
                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        FrmICABMAsigna.Show()
                    End If
                    ''---CAMBIO DE CM A MTA
                ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 6) = "CMAMTA" Then
                    If Me.StatusTextBox.Text = "P" Then
                        MsgBox("Sólo se puede Asignar el Aparato hasta que la Orden sea Ejecutada", , "Atención")
                    ElseIf Me.StatusTextBox.Text = "E" Then
                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        FrmICABMAsigna.Show()
                    End If
                ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "CMTCM" Then
                    If Me.StatusTextBox.Text = "P" Then
                        MsgBox("Sólo se puede Asignar el Aparato hasta que la Orden sea Ejecutada", , "Atención")
                    ElseIf Me.StatusTextBox.Text = "E" Then
                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        FrmICABMAsigna.Show()
                    End If
                    ''------------
                ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "CCABT" Then
                    If Me.StatusTextBox.Text = "P" Then
                        MsgBox("Sólo se puede Asignar el Aparato hasta que la Orden sea Ejecutada", , "Atención")
                    ElseIf Me.StatusTextBox.Text = "E" Then
                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        FrmICABMAsigna.Show()
                    End If
                    ''------------
                ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "ICAJA" Or Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "ICAJA6" Then
                    If opcion = "N" Then
                        MsgBox("Proceda a guardar la orden antes de asignar una Caja Digital", MsgBoxStyle.Information)
                        Exit Sub
                    End If
                    ContratoCajasDig = Me.ContratoTextBox.Text
                    'Dim RES = MsgBox("Primero debe seleccionar la marca de la Caja Digital", MsgBoxStyle.OkCancel)
                    'If RES = MsgBoxResult.Ok Then

                    If Me.StatusTextBox.Text = "P" Then
                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        OpICAJA = "E"
                        FrmSelCajasDisponiblesvb.Show()
                    ElseIf Me.StatusTextBox.Text = "E" Then
                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        OpICAJA = "P"
                        FrmSelCajasDisponiblesvb.Show()
                    End If
                    'Else
                    '    Exit Sub
                    'End If
                ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "CCAJA" Or Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "CCAJA6" Then
                    If Me.StatusTextBox.Text = "P" Then
                        MsgBox("Sólo se puede Asignar el Aparato hasta que la Orden sea Ejecutada", , "Atención")
                    ElseIf Me.StatusTextBox.Text = "E" Then
                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        FrmIAPARAsigna.Show()
                    End If
                ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "CAPAR" Or Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "CAPAR6" Then
                    If Me.StatusTextBox.Text = "P" Then
                        MsgBox("Sólo se puede Asignar el Aparato hasta que la Orden sea Ejecutada", , "Atención")
                    ElseIf Me.StatusTextBox.Text = "E" Then
                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        FrmIAPARAsigna.Show()
                    End If

                    'ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "CANUM" Then
                    '    If Me.StatusTextBox.Text = "P" Then
                    '        MsgBox("Sólo se puede Asignar el Numero hasta que la Orden sea Ejecutada", , "Atención")
                    '    ElseIf Me.StatusTextBox.Text = "E" Then
                    '        Me.BUSCADetOrdSerDataGridView.Enabled = False
                    '        eGloContrato = Me.ContratoTextBox.Text
                    '        STATUSORDEN = Me.StatusTextBox.Text
                    '        FrmSelecNumTelNew.Show()
                    '    End If

                ElseIf (Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "IPAQU" Or Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "BPAQU" _
                        Or Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "DPAQU" Or Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "RPAQU" _
                        Or Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "BPAQT") Then
                    'If Me.StatusTextBox.Text = "E" Then
                    Me.BUSCADetOrdSerDataGridView.Enabled = False
                    FrmrRelPaquetesdelCliente.Show()
                    'End If
                ElseIf (Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 6)) = "IPAQUT" Or (Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 6)) = "BSEDI" _
                    Or (Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5)) = "ASDIG" Or (Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5)) = "DSDIG" _
                    Or (Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5)) = "RSDIG" Then
                    'If Me.StatusTextBox.Text = "E" Then
                    Me.BUSCADetOrdSerDataGridView.Enabled = False
                    FrmrRelPaquetesdelCliente.Show()
                    'End If
                ElseIf ((Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "IPAQD" Or Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "BPAQD" _
                        Or Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "DPAQD" Or Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "RPAQD") _
                        Or (Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "IPAQD6" Or Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "BPAQD6" _
                        Or Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "DPAQD6" Or Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "RPAQD6")) Then
                    'If Me.StatusTextBox.Text = "E" Then
                    Me.BUSCADetOrdSerDataGridView.Enabled = False
                    FrmrRelPaquetesdelClienteDigital.Show()
                    'End If
                ElseIf (Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "IANTE" Or Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "CANTE") Then



                    If Me.StatusTextBox.Text = "P" Then
                        MsgBox("Sólo se puede Asignar la Antena hasta que la Orden sea Ejecutada", , "Atención")
                    ElseIf Me.StatusTextBox.Text = "E" Then
                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        FrmIANTE.Show()
                    End If

                End If
            End If
        Else
            MsgBox("Primero Capture los Datos Solicitados ", MsgBoxStyle.Information)
        End If
    End Sub




    Private Sub valida()
        Dim OP As Integer = 0
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            'If GloClv_TipSer = 2 Then
            '    OP = 0
            'ElseIf GloClv_TipSer = 3 Then
            '    OP = 1
            'End If
            OP = 0
            Me.ValidaTrabajosTableAdapter.Connection = CON
            Me.ValidaTrabajosTableAdapter.Fill(Me.NewSofTvDataSet.ValidaTrabajos, New System.Nullable(Of Long)(CType(Me.Clv_OrdenTextBox.Text, Long)), New System.Nullable(Of Integer)(CType(OP, Integer)))
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If GloControlaReloj = 1 Then
            If LocFecEje = False Then
                If Me.Panel5.BackColor = Color.WhiteSmoke Then
                    Me.Panel5.BackColor = Color.Gold
                    If Me.Fec_EjeTextBox.Focused = False Then
                        Me.Fec_EjeTextBox.Focus()
                    End If
                Else
                    Me.Panel5.BackColor = Color.WhiteSmoke
                End If


            ElseIf LocTec = False Then
                If Me.Panel6.BackColor = Color.WhiteSmoke Then
                    Me.Panel6.BackColor = Color.Gold
                    If Me.Tecnico.Focused = False Then
                        Me.Tecnico.Focus()
                    End If
                Else
                    Me.Panel6.BackColor = Color.WhiteSmoke
                End If
                'ElseIf LocAlm = False Then
                '    If Me.Panel9.BackColor = Color.WhiteSmoke Then
                '        Me.Panel9.BackColor = Color.Gold
                '        If Me.Almacen.Focused = False Then
                '            Me.Almacen.Focus()
                '        End If
                '    Else
                '        Me.Panel9.BackColor = Color.WhiteSmoke
                '    End If
            End If
        End If
    End Sub

    Private Sub Tecnico_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Tecnico.SelectedIndexChanged
        If Len(Trim(Tecnico.Text)) > 0 And IsNumeric(Tecnico.SelectedValue) = True Then
            LocTec = True
            Panel6.BackColor = Color.WhiteSmoke
            MuestraRelOrdSerCuadrilla(gloClave, Me.Tecnico.SelectedValue)
        Else
            LocTec = False
        End If
    End Sub

    Private Sub Tecnico_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Tecnico.TextChanged
        If Len(Trim(Tecnico.Text)) > 0 And IsNumeric(Tecnico.SelectedValue) = True Then
            LocTec = True
            Panel6.BackColor = Color.WhiteSmoke
        Else
            LocTec = False
        End If
    End Sub


    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'If IsNumeric(Almacen.SelectedValue) = True Then
        '    Locclv_Alm = Me.Almacen.SelectedValue
        If IsNumeric(Me.Tecnico.SelectedValue) = True Then
            Locclv_tec = Me.Tecnico.SelectedValue
            gLOVERgUARDA = 0
            If Me.CONORDSERBindingNavigator.Enabled = False Then
                gLOVERgUARDA = 1
            End If
            gloClv_Orden = Me.Clv_OrdenTextBox.Text
            Contrato = Me.ContratoTextBox.Text
            'Me.DameClv_Session_TecnicosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Tecnicos, clv_sessionTecnico)

            Dim frm As New SoftvNew.FrmDescargaMaterialTec()
            frm.Clv_Orden = gloClv_Orden
            frm.IdTecnico = Locclv_tec
            frm.tipoDescarga = "O"
            If opcion = "C" Then
                frm.pnlDescargaMaterial.Enabled = False
                frm.btnGuardar.Enabled = False
            End If

            frm.ShowDialog()

        Else
            MsgBox("Seleccione el Tecnico por favor")
        End If
        'Else
        'MsgBox("Seleccione un SubAlmacen por favor")
        'End If
    End Sub



    Private Sub dimebitacora()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Label3.Visible = True
            Me.FolioTextBox.Visible = True
            Me.DimeSiTieneunaBitacoraTableAdapter.Connection = CON
            Me.DimeSiTieneunaBitacoraTableAdapter.Fill(Me.DataSetEdgarRev2.DimeSiTieneunaBitacora, New System.Nullable(Of Long)(CType(Me.Clv_OrdenTextBox.Text, Long)))
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ESHOTELLabel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ImprimeOrdSerRetiro(ByVal ClvOrden As Long)

        Try
            Dim CON As New SqlConnection(MiConexion)

            Dim Titulo As String = Nothing
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0"
            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
            Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"
            Dim Impresora As String = Nothing
            Dim a As Integer = 0



            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim mySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"


            Dim reportPath As String = Nothing

            If IdSistema = "AG" Then

                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBueno.rpt"
            ElseIf IdSistema = "TO" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCabStar.rpt"
            ElseIf IdSistema = "SA" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoTvRey.rpt"
            ElseIf IdSistema = "VA" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCosmo.rpt"
            ElseIf IdSistema = "LO" Or IdSistema = "YU" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoLogitel.rpt"
            End If


            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Clv_TipSer int
            customersByCityReport.SetParameterValue(0, 0)
            ',@op1 smallint
            customersByCityReport.SetParameterValue(1, 1)
            ',@op2 smallint
            customersByCityReport.SetParameterValue(2, 0)
            ',@op3 smallint
            customersByCityReport.SetParameterValue(3, 0)
            ',@op4 smallint,
            customersByCityReport.SetParameterValue(4, 0)
            '@op5 smallint
            customersByCityReport.SetParameterValue(5, 0)
            ',@StatusPen bit
            customersByCityReport.SetParameterValue(6, 1)
            ',@StatusEje bit
            customersByCityReport.SetParameterValue(7, 0)
            ',@StatusVis bit,
            customersByCityReport.SetParameterValue(8, 0)
            '@Clv_OrdenIni bigint
            customersByCityReport.SetParameterValue(9, ClvOrden)
            ',@Clv_OrdenFin bigint
            customersByCityReport.SetParameterValue(10, ClvOrden)
            ',@Fec1Ini Datetime
            customersByCityReport.SetParameterValue(11, "01/01/1900")
            ',@Fec1Fin Datetime,
            customersByCityReport.SetParameterValue(12, "01/01/1900")
            '@Fec2Ini Datetime
            customersByCityReport.SetParameterValue(13, "01/01/1900")
            ',@Fec2Fin Datetime
            customersByCityReport.SetParameterValue(14, "01/01/1900")
            ',@Clv_Trabajo int
            customersByCityReport.SetParameterValue(15, 0)
            ',@Clv_Colonia int
            customersByCityReport.SetParameterValue(16, 0)
            ',@OpOrden int
            customersByCityReport.SetParameterValue(17, OpOrdenar)





            mySelectFormula = "Orden " & GloNom_TipSer
            customersByCityReport.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"
            CON.Open()
            Me.Dame_Impresora_OrdenesTableAdapter.Connection = CON
            Me.Dame_Impresora_OrdenesTableAdapter.Fill(Me.DataSetarnoldo.Dame_Impresora_Ordenes, Impresora, a)
            CON.Close()
            If a = 1 Then
                MsgBox("No se tiene asignada una Impresora de Ordenes de Servicio", MsgBoxStyle.Information)
                Exit Sub
            Else
                customersByCityReport.PrintOptions.PrinterName = Impresora
                customersByCityReport.PrintToPrinter(1, True, 1, 1)
            End If
            '--SetDBLogonForReport(connectionInfo)

            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub GuardaRelOrdenUsuario()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.NueRelOrdenUsuarioTableAdapter.Connection = CON
        Me.NueRelOrdenUsuarioTableAdapter.Fill(Me.DataSetEric.NueRelOrdenUsuario, CLng(Me.Clv_OrdenTextBox.Text), GloClvUsuario, Me.StatusTextBox.Text)
        CON.Close()
    End Sub

    Private Sub ChecaRelOrdenUsuario(ByVal Clv_Orden As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ChecaRelOrdenUsuario", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Orden
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            conexion.Dispose()
        Catch ex As Exception
            conexion.Close()
            conexion.Dispose()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub ConRelClienteTab(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ConRelClienteTab", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Tab", SqlDbType.VarChar, 50)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            Me.TextBoxTab.Text = parametro2.Value.ToString
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub Llena_Tabs_Por_Contrato(ByVal Contrato As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim str As New StringBuilder

        str.Append("Exec DameTaps_PorContrato ")
        str.Append(CStr(Contrato))

        Dim dataAdapter As New SqlDataAdapter(str.ToString(), con)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try

            con.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.ComboBoxTap.DataSource = bindingSource

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
            con.Close()
            con.Dispose()
        End Try
    End Sub


    Private Sub NueRelClienteTab(ByVal Contrato As Long, ByVal Tab As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueRelClienteTab", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Tab", SqlDbType.VarChar, 50)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Tab
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub
    Private Sub MuestraRelOrdenesTecnicos(ByVal Clv_Orden As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraRelOrdenesTecnicos ")
        strSQL.Append(CStr(Clv_Orden))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.Tecnico.DataSource = bindingSource
        Catch ex As Exception
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub MuestraRelOrdSerCuadrilla(ByVal Clv_Orden As Long, ByVal Clv_Tecnico As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraRelOrdSerCuadrilla " + Clv_Orden.ToString() + ", " + Clv_Tecnico.ToString())


        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            cbCuadrilla.DataSource = bindingSource
        Catch ex As Exception
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    'Private Sub MuestraAlmacenes(ByVal Clv_Orden As Long)
    '    Dim conexion As New SqlConnection(MiConexion)
    '    Dim strSQL As New StringBuilder
    '    strSQL.Append("EXEC MuestraAlmacenes ")
    '    strSQL.Append(CStr(Clv_Orden))

    '    Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
    '    Dim dataTable As New DataTable
    '    Dim bindingSource As New BindingSource

    '    Try
    '        conexion.Open()
    '        dataAdapter.Fill(dataTable)
    '        bindingSource.DataSource = dataTable
    '        Me.Almacen.DisplayMember = "almacen"
    '        Me.Almacen.ValueMember = "id"
    '        Me.Almacen.DataSource = bindingSource
    '    Catch ex As Exception
    '    Finally
    '        conexion.Close()
    '        conexion.Dispose()
    '    End Try

    'End Sub
    Private Sub SALIDA_TECNICO_AUTO()

        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("SALIDA_TECNICO_AUTO", con)
        com.CommandType = CommandType.StoredProcedure


        Dim para2 As New SqlParameter("@CLV_TECNICO", SqlDbType.Int)
        Dim para3 As New SqlParameter("@Addres", SqlDbType.VarChar, 250)


        para2.Direction = ParameterDirection.Input
        para3.Direction = ParameterDirection.Input
        If IsNumeric(Me.Tecnico.SelectedValue) = True Then
            Locclv_tec = Me.Tecnico.SelectedValue
            para2.Value = Locclv_tec
        End If
        para3.Value = GloMacCablemodemSel

        com.Parameters.Add(para2)
        com.Parameters.Add(para3)

        Try
            con.Open()
            com.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.ToString)
        Finally
            con.Dispose()
            con.Close()
        End Try

    End Sub

    Private Sub Inserta_Cablemodems_L()

        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("Inserta_Cablemodems_L", con)
        com.CommandType = CommandType.StoredProcedure

        Dim para3 As New SqlParameter("@addres", SqlDbType.VarChar, 250)
        Dim para4 As New SqlParameter("@sucursal", SqlDbType.VarChar, 50)
        Dim para5 As New SqlParameter("@usuario", SqlDbType.Int)

        para3.Direction = ParameterDirection.Input
        para4.Direction = ParameterDirection.Input
        para5.Direction = ParameterDirection.Input

        para3.Value = GloMacCablemodemSel
        para4.Value = GloSucursal
        para5.Value = eClv_Usuario

        com.Parameters.Add(para3)
        com.Parameters.Add(para4)
        com.Parameters.Add(para5)

        Try
            con.Open()
            com.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.ToString)
        Finally
            con.Dispose()
            con.Close()
        End Try

    End Sub
    Private Sub validaContratoTelefonia(ByVal contratonet As Long, ByVal clv_cablemodem As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("validaContratoTelefonia", con)
        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = contratonet
        com.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Clv_cablemodem", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Input
        par2.Value = clv_cablemodem
        com.Parameters.Add(par2)

        Dim par4 As New SqlParameter("@BND", SqlDbType.Int)
        par4.Direction = ParameterDirection.Output
        com.Parameters.Add(par4)

        Try
            con.Open()
            com.ExecuteNonQuery()
            BNDTEL = CInt(par4.Value)
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            con.Close()
            con.Dispose()
            GloContratonet = 0
        End Try
    End Sub
    '------------------   Almacen web --------------
    Private Sub Inserta_Bitacora_Tecnico()
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("Inserta_Bitacora_Tecnico", con)
        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@CLV_SESSION", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = clv_sessionTecnico
        com.Parameters.Add(par1)

        Dim par4 As New SqlParameter("@CLV_TECNICO", SqlDbType.Int)
        par4.Direction = ParameterDirection.Input
        par4.Value = Locclv_tec
        com.Parameters.Add(par4)

        Try
            con.Open()
            com.ExecuteNonQuery()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            con.Close()
            con.Dispose()

        End Try
    End Sub
    Public Sub Inserta_Rel_Bitacora_Orden()
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("Inserta_Rel_Bitacora_Orden", con)
        com.CommandType = CommandType.StoredProcedure

        Dim par2 As New SqlParameter("@clv_orden", SqlDbType.Int)
        par2.Direction = ParameterDirection.Input
        par2.Value = CLng(gloClave)
        com.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@tipo", SqlDbType.VarChar, 1)
        par3.Direction = ParameterDirection.Input
        par3.Value = "O"
        com.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@contrato", SqlDbType.Int)
        par4.Direction = ParameterDirection.Input
        par4.Value = Contrato
        com.Parameters.Add(par4)

        Try
            con.Open()
            com.ExecuteNonQuery()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            con.Close()
            con.Dispose()

        End Try
    End Sub
    'Private Sub MODORDSER(ByVal Clv_Orden As Long, ByVal Clv_TipSer As Integer, ByVal Contrato As Long, ByVal Fec_Sol As String, ByVal Fec_Eje As String, ByVal Visita1 As String, ByVal Visita2 As String, ByVal Status As String, ByVal Clv_Tecnico As Integer, ByVal IMPRESA As Boolean, ByVal Clv_FACTURA As Long, ByVal Obs As String, ByVal ListadeArticulos As String)

    '    Dim con As New SqlConnection(MiConexion)
    '    Dim com As New SqlCommand("MODORDSER", con)
    '    com.CommandType = CommandType.StoredProcedure

    '    Dim par1 As New SqlParameter("@clv_orden", SqlDbType.BigInt)
    '    Dim par2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
    '    Dim par3 As New SqlParameter("@Contrato", SqlDbType.BigInt)
    '    Dim par4 As New SqlParameter("@Fec_Sol", SqlDbType.DateTime)
    '    Dim par5 As New SqlParameter("@Fec_Eje", SqlDbType.DateTime)
    '    Dim par6 As New SqlParameter("@Visita1", SqlDbType.DateTime)
    '    Dim par7 As New SqlParameter("@Visita2", SqlDbType.DateTime)
    '    Dim par8 As New SqlParameter("@Status", SqlDbType.VarChar, 1)
    '    Dim par9 As New SqlParameter("@Clv_Tecnico", SqlDbType.Int)
    '    Dim par10 As New SqlParameter("@IMPRESA", SqlDbType.Bit)
    '    Dim par11 As New SqlParameter("@Clv_FACTURA", SqlDbType.BigInt)
    '    Dim par12 As New SqlParameter("@Obs", SqlDbType.VarChar, 255)
    '    Dim par13 As New SqlParameter("@ListadeArticulos", SqlDbType.VarChar, 1)

    '    par1.Direction = ParameterDirection.Input
    '    par2.Direction = ParameterDirection.Input
    '    par3.Direction = ParameterDirection.Input
    '    par4.Direction = ParameterDirection.Input
    '    par5.Direction = ParameterDirection.Input
    '    par6.Direction = ParameterDirection.Input
    '    par7.Direction = ParameterDirection.Input
    '    par8.Direction = ParameterDirection.Input
    '    par9.Direction = ParameterDirection.Input
    '    par10.Direction = ParameterDirection.Input
    '    par11.Direction = ParameterDirection.Input
    '    par12.Direction = ParameterDirection.Input
    '    par13.Direction = ParameterDirection.Input

    '    par1.Value = Clv_Orden
    '    par2.Value = Clv_TipSer
    '    par3.Value = Contrato
    '    par4.Value = Fec_Sol
    '    par5.Value = Fec_Eje
    '    par6.Value = Visita1
    '    par7.Value = Visita2
    '    par8.Value = Status
    '    par9.Value = Clv_Tecnico
    '    par10.Value = IMPRESA
    '    par11.Value = Clv_FACTURA
    '    par12.Value = Obs
    '    par13.Value = ListadeArticulos

    '    com.Parameters.Add(par1)
    '    com.Parameters.Add(par2)
    '    com.Parameters.Add(par3)
    '    com.Parameters.Add(par4)
    '    com.Parameters.Add(par5)
    '    com.Parameters.Add(par6)
    '    com.Parameters.Add(par7)
    '    com.Parameters.Add(par8)
    '    com.Parameters.Add(par9)
    '    com.Parameters.Add(par10)
    '    com.Parameters.Add(par11)
    '    com.Parameters.Add(par12)
    '    com.Parameters.Add(par13)


    '    Try
    '        con.Open()
    '        com.ExecuteNonQuery()

    '    Catch ex As Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    Finally
    '        con.Close()
    '        con.Dispose()

    '    End Try

    'End Sub

    Private Sub Pon_La_HoraEje_Orden(ByVal eClv_Orden As Long, ByVal eFecha As String, ByVal eHora As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("Pon_La_HoraEje_Orden", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = eClv_Orden
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Fecha", SqlDbType.VarChar, 10)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = eFecha
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Hora", SqlDbType.VarChar, 13)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = eHora
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    ''''*********-------------    FIN   -----------****************
    Private Sub guardaBitacoraCANUM(ByVal contrato As Long, ByVal tel_old As Integer, ByVal tel_nes As Integer, ByVal OP As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("guardaBitacoraCANUM", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@tel_old", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = tel_old
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@tel_new", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = tel_nes
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@OP", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = OP
        comando.Parameters.Add(parametro4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    'Private Sub Almacen_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If Len(Trim(Almacen.Text)) > 0 And IsNumeric(Almacen.SelectedValue) = True Then
    '        LocAlm = True
    '        Panel9.BackColor = Color.WhiteSmoke
    '    Else
    '        LocAlm = False
    '    End If
    'End Sub

    'Private Sub Almacen_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    If Len(Trim(Almacen.Text)) > 0 And IsNumeric(Almacen.SelectedValue) = True Then
    '        LocAlm = True
    '        Panel9.BackColor = Color.WhiteSmoke
    '    Else
    '        LocAlm = False
    '    End If
    'End Sub

    Private Sub Inserta_Bitacora_tec_2(ByVal Clv_Session As Long, ByVal Clv_Orden As Long, ByRef clv_folio As Integer, ByVal clv_categoria As Integer, ByVal clv_tecnico As Integer, ByVal clv_usuario As String, ByVal Status As String, ByVal obs As String, ByVal idAlmacen As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("Inserta_Bitacora_tec_2", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Session
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_Orden
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@clv_folio", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = clv_folio
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@clv_categoria", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = clv_categoria
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@clv_tecnico", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = clv_tecnico
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@clv_usuario", SqlDbType.VarChar, 50)
        parametro6.Direction = ParameterDirection.Input
        parametro6.Value = clv_usuario
        comando.Parameters.Add(parametro6)

        Dim parametro7 As New SqlParameter("@Status", SqlDbType.VarChar, 50)
        parametro7.Direction = ParameterDirection.Input
        parametro7.Value = Status
        comando.Parameters.Add(parametro7)

        Dim parametro8 As New SqlParameter("@obs", SqlDbType.VarChar, 200)
        parametro8.Direction = ParameterDirection.Input
        parametro8.Value = obs
        comando.Parameters.Add(parametro8)

        Dim parametro9 As New SqlParameter("@no_bitacora", SqlDbType.BigInt)
        parametro9.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro9)

        Dim parametro10 As New SqlParameter("@idAlmacen", SqlDbType.Int)
        parametro10.Direction = ParameterDirection.Input
        parametro10.Value = idAlmacen
        comando.Parameters.Add(parametro10)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            LocNo_Bitacora = CLng(parametro9.Value)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub
    Private Sub BORRAMETRAJE(ByVal CLV_ORDEN As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BORRAMETRAJE", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@CLV_ORDEN", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = CLV_ORDEN
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub Valida_DetOrdenDelete(ByVal CLV_ORDEN As Long, ByVal OP As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("Valida_DetOrdenDelete", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@CLV_ORDEN", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = CLV_ORDEN
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@OP", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = OP
        comando.Parameters.Add(parametro2)

        Dim parametro1 As New SqlParameter("@error", SqlDbType.Int)
        parametro1.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro1)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            valor = CInt(parametro1.Value)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub
    Private Sub BUSCACLIENTES(ByVal OP As Integer)

        'Limpiamos como Bloqueado
        num2 = 0

        Dim contrato1 As Long
        Dim CON As New SqlConnection(MiConexion)
        Dim com As SqlCommand = New SqlCommand("BUSCLIPORCONTRATO_2", CON)
        Dim tabla As DataTable = New DataTable
        Dim da As SqlDataAdapter = New SqlDataAdapter(com)
        If OP = 0 Then
            If IsNumeric(Me.ContratoTextBox.Text) = True Then
                contrato1 = Long.Parse(Me.ContratoTextBox.Text)
            Else
                MsgBox("El contrato introducido no es correcto, favor de verificarlo", MsgBoxStyle.Information)
                Return
            End If
        End If
        com.CommandType = CommandType.StoredProcedure
        com.Parameters.Add(New SqlParameter("@CONTRATO", contrato1))
        com.Parameters.Add(New SqlParameter("@NOMBRE", ""))
        com.Parameters.Add(New SqlParameter("@CALLE", ""))
        com.Parameters.Add(New SqlParameter("@NUMERO", ""))
        com.Parameters.Add(New SqlParameter("@CIUDAD", ""))
        com.Parameters.Add(New SqlParameter("@OP", OP))
        com.Parameters.Add(New SqlParameter("@Clv_TipSer", 0))
        com.Parameters.Add(New SqlParameter("@Telefono", ""))
        Try
            CON.Open()
            da.Fill(tabla)
            'LLena  Controles 
            Me.NOMBRELabel1.Text = tabla.Rows(0)(1).ToString
            Me.CALLELabel1.Text = tabla.Rows(0)(2).ToString
            Me.COLONIALabel1.Text = tabla.Rows(0)(3).ToString
            Me.NUMEROLabel1.Text = tabla.Rows(0)(4).ToString
            Me.CIUDADLabel1.Text = tabla.Rows(0)(5).ToString
            Me.SOLOINTERNETCheckBox.Checked = Convert.ToBoolean(tabla.Rows(0)(6).ToString)
            Me.ESHOTELCheckBox.Checked = Convert.ToBoolean(tabla.Rows(0)(6).ToString)
            '
            Me.BuscaBloqueadoTableAdapter.Connection = CON
            Me.BuscaBloqueadoTableAdapter.Fill(Me.DataSetLidia.BuscaBloqueado, Me.ContratoTextBox.Text, NUM, num2)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            CON.Close()
        End Try
        TxtRefOxxo.Text = SP_REFERENCIA_OXXO(Me.ContratoTextBox.Text)
        CREAARBOL()
        If num2 = 1 Then

            eGloContrato = Me.ContratoTextBox.Text
            bloq = 1
        End If
    End Sub

    Private Sub ValidaCajasAsignadas()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("VALIDADECODERS", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro1 As New SqlParameter("@CLV_ORDEN", SqlDbType.Int)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = gloClave
        comando.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@BND_ORDEN", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            Bnd_Orden = parametro2.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub BORRAAPARATOS()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BORRAAPARATOS", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro1 As New SqlParameter("@CLV_ORDEN", SqlDbType.Int)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = gloClave
        comando.Parameters.Add(parametro1)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub ChecaSiAparatoAsignado(ByVal CLV_ORDSERC As Integer, ByVal TIPSERC As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("ChecaSiAparatoAsignado", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@CLV_ORDEN", SqlDbType.Int)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = CLV_ORDSERC
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@CLV_TIPSER", SqlDbType.Int)
        PRM2.Direction = ParameterDirection.Input
        PRM2.Value = TIPSERC
        CMD.Parameters.Add(PRM2)

        Dim PRM3 As New SqlParameter("@BndCcabm", SqlDbType.Bit)
        PRM3.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM3)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            BndCambioEquipo = PRM3.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub ModOrdSer(ByVal Clv_Orden As Long, ByVal Clv_TipSer As Integer, ByVal Contrato As Long, ByVal Fec_Sol As String, ByVal Fec_Eje As String, ByVal Visita1 As String, _
                          ByVal Visita2 As String, ByVal Status As Char, ByVal Clv_Tecnico As Integer, ByVal Impresa As Boolean, ByVal Clv_Factura As String, ByVal Obs As String, _
                          ByVal ListaDeArticulos As Char)

        If Fec_Sol.Length = 0 Then Fec_Sol = "01/01/1900"
        If Fec_Eje.Length = 0 Then Fec_Eje = "01/01/1900"
        If Visita1.Length = 0 Then Visita1 = "01/01/1900"
        If Visita2.Length = 0 Then Visita2 = "01/01/1900"
        If Clv_Factura.Length = 0 Then Clv_Factura = 0


        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("MODORDSER", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Orden
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_TipSer
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Contrato
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Fec_Sol", SqlDbType.DateTime)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = DateTime.Parse(Fec_Sol)
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Fec_Eje", SqlDbType.DateTime)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = DateTime.Parse(Fec_Eje)
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@Visita1", SqlDbType.DateTime)
        parametro6.Direction = ParameterDirection.Input
        parametro6.Value = DateTime.Parse(Visita1)
        comando.Parameters.Add(parametro6)

        Dim parametro7 As New SqlParameter("@Visita2", SqlDbType.DateTime)
        parametro7.Direction = ParameterDirection.Input
        parametro7.Value = DateTime.Parse(Visita2)
        comando.Parameters.Add(parametro7)

        Dim parametro8 As New SqlParameter("@Status", SqlDbType.VarChar, 1)
        parametro8.Direction = ParameterDirection.Input
        parametro8.Value = Status
        comando.Parameters.Add(parametro8)

        Dim parametro9 As New SqlParameter("@Clv_Tecnico", SqlDbType.Int)
        parametro9.Direction = ParameterDirection.Input
        parametro9.Value = Clv_Tecnico
        comando.Parameters.Add(parametro9)

        Dim parametro10 As New SqlParameter("@Impresa", SqlDbType.Bit)
        parametro10.Direction = ParameterDirection.Input
        parametro10.Value = Impresa
        comando.Parameters.Add(parametro10)

        Dim parametro11 As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        parametro11.Direction = ParameterDirection.Input
        parametro11.Value = CLng(Clv_Factura)
        comando.Parameters.Add(parametro11)

        Dim parametro12 As New SqlParameter("@Obs", SqlDbType.VarChar, 255)
        parametro12.Direction = ParameterDirection.Input
        parametro12.Value = Obs
        comando.Parameters.Add(parametro12)

        Dim parametro13 As New SqlParameter("@ListaDeArticulos", SqlDbType.VarChar, 1)
        parametro13.Direction = ParameterDirection.Input
        parametro13.Value = ListaDeArticulos
        comando.Parameters.Add(parametro13)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try


    End Sub

    Private Sub PreEjecutaOrdSer(ByVal Clv_Orden As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("PREEJECUTAOrdSer", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Orden
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try



    End Sub

    Private Sub Hora_sol_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Hora_sol.ValueChanged

    End Sub

    Private Sub BUSCADetOrdSerDataGridView_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles BUSCADetOrdSerDataGridView.CellContentClick

    End Sub

    Private Sub ValidaGuardarOrden(ByVal Clv_Orden As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaGuardarOrden", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = Clv_Orden
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        par2.Direction = ParameterDirection.Output
        comando.Parameters.Add(par2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eMsj = ""
            eMsj = par2.Value.ToString()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub NueRelOrdSerCuadrilla(ByVal Clv_Orden As Long, ByVal Clv_Tecnico As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueRelOrdSerCuadrilla", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = Clv_Orden
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Clv_Tecnico", SqlDbType.Int)
        par2.Direction = ParameterDirection.Input
        par2.Value = Clv_Tecnico
        comando.Parameters.Add(par2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally

            conexion.Close()
            conexion.Dispose()

        End Try

    End Sub

    Private Sub consultaVisitasOrdenes(ByVal prmClvOrden As Long)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("consultaVisitasOrdenes", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@ClvOrden", prmClvOrden)

        Dim READER As SqlDataReader

        Try
            CON.Open()
            READER = CMD.ExecuteReader()
            While READER.Read
                ordenGloFechaVisita1 = READER(0).ToString
                ordenGloFechaVisita2 = READER(1).ToString
                ordenGloFechaVisita3 = READER(2).ToString
                ordenGloHoraVisita1 = READER(0).ToString & " " & READER(3).ToString
                ordenGloHoraVisita2 = READER(1).ToString & " " & READER(4).ToString
                ordenGloHoraVisita3 = READER(2).ToString & " " & READER(5).ToString
                Me.Visita1TextBox.Text = ordenGloFechaVisita1
                Me.Visita2TextBox.Text = ordenGloFechaVisita2
                Me.Visita3TextBox.Text = ordenGloFechaVisita3
                If ordenGloFechaVisita1 <> "" Then
                    Me.dtpHoraVisita1.Value = ordenGloHoraVisita1
                End If
                If ordenGloFechaVisita2 <> "" Then
                    Me.dtpHoraVisita2.Value = ordenGloHoraVisita2
                End If
                If ordenGloFechaVisita3 <> "" Then
                    Me.dtpHoraVisita3.Value = ordenGloHoraVisita3
                End If
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub insertaVisitasOrdenes(ByVal prmClvOrden As Long, ByVal prmFechaVisita1 As Date, ByVal prmFechaVisita2 As Date, ByVal prmFechaVisita3 As Date, _
                                      ByVal prmHoraVisita1 As String, ByVal prmHoraVisita2 As String, ByVal prmHoraVisita3 As String)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("insertaVisitasOrdenes", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@clvOrden", prmClvOrden)
        CMD.Parameters.AddWithValue("@fechaVisita1", prmFechaVisita1)
        CMD.Parameters.AddWithValue("@fechaVisita2", prmFechaVisita2)
        CMD.Parameters.AddWithValue("@fechaVisita3", prmFechaVisita3)
        CMD.Parameters.AddWithValue("@horaVisita1", prmHoraVisita1)
        CMD.Parameters.AddWithValue("@horaVisita2", prmHoraVisita2)
        CMD.Parameters.AddWithValue("@horaVisita3", prmHoraVisita3)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub CHECASIACOMETIDA(ByVal CLV_ORDSER As Long)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("CHECASIACOMETIDA", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@CLV_ORDEN", CLV_ORDSER)
        CMD.Parameters.AddWithValue("@BNDACOMETIDA", BNDORDACOMETIDA).Direction = ParameterDirection.Output

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            BNDORDACOMETIDA = CMD.Parameters("@BNDACOMETIDA").Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub DAMESECTORORDENES(ByVal pRMClvOrden As Long)
        Dim CON As New SqlConnection(MiConexion)
        Dim STRSQL As New StringBuilder

        STRSQL.Append("EXEC DAMESECTORORDENES ")
        STRSQL.Append(CStr(pRMClvOrden))

        Dim DA As New SqlDataAdapter(STRSQL.ToString, CON)
        Dim DT As New DataTable
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.SectorCombo.DataSource = BS.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub ACTUALIZASECTORCLIENTE(ByVal ContratoTap As Long, ByVal CLVSECTOR As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("ACTUALIZASECTORCLIENTE", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@CONTRATO", ContratoTap)
        CMD.Parameters.AddWithValue("@CLV_SECTOR", CLVSECTOR)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub checaBitacoraTecnico(ByVal prmClvOrden As Integer, ByVal prmTipoDescarga As String)
        Dim CON As New SqlConnection(Globals.DataAccess.GlobalConectionString)
        Dim CMD As New SqlCommand("checaBitacoraTecnico", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@clvOrden", prmClvOrden)
        CMD.Parameters.AddWithValue("@tipoDescarga", prmTipoDescarga)

        Dim READER As SqlDataReader

        Try
            CON.Open()

            READER = CMD.ExecuteReader()
            If READER.HasRows Then
                While (READER.Read)
                    clvTecnicoDescarga = READER(0).ToString
                    clvBitacoraDescarga = READER(1).ToString
                End While
            Else
                clvTecnicoDescarga = 0
                clvBitacoraDescarga = 0
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub


    Private Function spDimeSectorDelCliente(ByVal prmClvOrden As Long) As Integer
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("spDimeSectorDelCliente", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@clvOrden", prmClvOrden)

        Dim READER As SqlDataReader

        Try
            CON.Open()

            READER = CMD.ExecuteReader()
            If READER.HasRows Then
                While (READER.Read)
                    spDimeSectorDelCliente = READER(0).ToString
                End While
            Else
                spDimeSectorDelCliente = 0
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Function
#End Region

#Region "GUI del Material Extra y Relación del Detalle de la Orden con el Retiro del Aparato"

    'Carga el Detalle de la Orden para el nuevo GridView
    Private Sub CargaDetOrdSer(ByVal Clv_Orden As Integer)
        Dim dt As DataTable
        dt = DAEquipoExtra.BUSCADetOrdSer(Clv_Orden)
        Try
            dgvDetOrdSer.DataSource = dt
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    'Evento que maneja el Botón de Acción en el Detalle de la Orden
    Private Sub dgvDetOrdSer_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetOrdSer.CellClick

        Dim TrabajoSeleccionado As String = ""
        Dim Pasa As Integer = 0

        If GloControlaReloj = 0 Then
            Pasa = 0
        Else
            If LocTec = True And LocFecEje = True Then
                Pasa = 0
            Else
                Pasa = 1
            End If
        End If

        Locclv_tec = Me.Tecnico.SelectedValue


        If Pasa = 0 Then
            If dgvDetOrdSer.RowCount > 0 Then

                'Acciones para el Botón
                If (dgvDetOrdSer.CurrentCell.GetType.Equals(btnGridDetOrdSer.GetType)) Then

                    GloDetClave = dgvDetOrdSer.CurrentRow.Cells("clave").Value
                    Contrato = Me.ContratoTextBox.Text
                    gloClv_Orden = Me.Clv_OrdenTextBox.Text
                    GLOTRABAJO = dgvDetOrdSer.CurrentRow.Cells("trabajo").Value
                    TrabajoSeleccionado = dgvDetOrdSer.CurrentRow.Cells("trabajo").Value

                    dame_clv_tipser(GLOTRABAJO)
                    GloeClave = GloDetClave
                    GloeClv_Orden = gloClv_Orden
                    GloeTrabajo = GLOTRABAJO
                    GloeClv_Trabajo = 0
                    GloeOp = 0

                    If TrabajoSeleccionado = "CAMDO" Or TrabajoSeleccionado = "CADIG" Or TrabajoSeleccionado = "CANET" Or TrabajoSeleccionado = "CADIG6" Then
                        Me.dgvDetOrdSer.Enabled = False
                        FrmCAMDO.ShowDialog()
                    ElseIf TrabajoSeleccionado = "CONEX" Then

                        Me.dgvDetOrdSer.Enabled = False
                        FrmCONEX.ShowDialog()
                    ElseIf TrabajoSeleccionado = "CEXTE" Then

                        Me.dgvDetOrdSer.Enabled = False
                        FrmCEXTE.ShowDialog()
                    ElseIf TrabajoSeleccionado = "CANEX" Then

                        Me.dgvDetOrdSer.Enabled = False
                        FrmCANEX.ShowDialog()
                    ElseIf TrabajoSeleccionado = "BCABM" Then

                        Me.dgvDetOrdSer.Enabled = False
                        FrmRelCablemodemClientes.ShowDialog()
                    ElseIf TrabajoSeleccionado = "BAPAR" Or TrabajoSeleccionado = "BAPAR6" Then
                        If GloeStatus = "E" Then
                            FormMuestra_Que_Hize_Orden_Servicio.ShowDialog()
                        Else
                            Me.dgvDetOrdSer.Enabled = False
                            FrmRelCablemodemClientesDigital.ShowDialog()
                        End If
                    ElseIf TrabajoSeleccionado = "ICABM" Then
                        If Me.StatusTextBox.Text = "P" Then
                            Me.dgvDetOrdSer.Enabled = False
                            'FrmRelCablemodemClientes.Show()
                            FrmICABMAsigna.ShowDialog()
                        ElseIf Me.StatusTextBox.Text = "E" Then
                            Me.dgvDetOrdSer.Enabled = False
                            'GloClv_TipSer = 2
                            FrmICABMAsigna.ShowDialog()
                        End If
                    ElseIf TrabajoSeleccionado = "ICABMT" Then
                        If Me.StatusTextBox.Text = "P" Then
                            Me.dgvDetOrdSer.Enabled = False
                            'FrmRelCablemodemClientes.Show()
                            FrmICABMAsigna.ShowDialog()
                        ElseIf Me.StatusTextBox.Text = "E" Then
                            Me.dgvDetOrdSer.Enabled = False
                            'GloClv_TipSer = 2
                            FrmICABMAsigna.ShowDialog()
                        End If
                    ElseIf TrabajoSeleccionado = "IAPAR" Or TrabajoSeleccionado = "RIAPA" Or TrabajoSeleccionado = "IAPAR6" Or TrabajoSeleccionado = "RIAPA6" Then
                        If GloeStatus = "E" Then
                            FormMuestra_Que_Hize_Orden_Servicio.ShowDialog()
                        ElseIf Me.StatusTextBox.Text = "P" Then
                            'Me.dgvDetOrdSer.Enabled = False
                            'FrmRelCablemodemClientesDigital.Show()
                            'FrmIAPARAsigna.ShowDialog()
                            MsgBox("Sólo se puede Asignar el Aparato hasta que la Orden sea Ejecutada", , "Atención")
                        ElseIf Me.StatusTextBox.Text = "E" Then
                            Me.dgvDetOrdSer.Enabled = False
                            'GloClv_TipSer = 3
                            FrmIAPARAsigna.ShowDialog()
                        End If
                    ElseIf TrabajoSeleccionado = "CCABM" Then
                        If Me.StatusTextBox.Text = "P" Then
                            MsgBox("Sólo se puede Asignar el Aparato hasta que la Orden sea Ejecutada", , "Atención")
                        ElseIf Me.StatusTextBox.Text = "E" Then
                            Me.dgvDetOrdSer.Enabled = False
                            FrmICABMAsigna.ShowDialog()
                        End If
                        ''---CAMBIO DE CM A MTA
                    ElseIf TrabajoSeleccionado = "CMAMTA" Then
                        If Me.StatusTextBox.Text = "P" Then
                            MsgBox("Sólo se puede Asignar el Aparato hasta que la Orden sea Ejecutada", , "Atención")
                        ElseIf Me.StatusTextBox.Text = "E" Then
                            Me.dgvDetOrdSer.Enabled = False
                            FrmICABMAsigna.ShowDialog()
                        End If
                    ElseIf TrabajoSeleccionado = "CMTCM" Then
                        If Me.StatusTextBox.Text = "P" Then
                            MsgBox("Sólo se puede Asignar el Aparato hasta que la Orden sea Ejecutada", , "Atención")
                        ElseIf Me.StatusTextBox.Text = "E" Then
                            Me.dgvDetOrdSer.Enabled = False
                            FrmICABMAsigna.ShowDialog()
                        End If
                        ''------------
                    ElseIf TrabajoSeleccionado = "CCABT" Then
                        If Me.StatusTextBox.Text = "P" Then
                            MsgBox("Sólo se puede Asignar el Aparato hasta que la Orden sea Ejecutada", , "Atención")
                        ElseIf Me.StatusTextBox.Text = "E" Then
                            Me.dgvDetOrdSer.Enabled = False
                            FrmICABMAsigna.ShowDialog()
                        End If
                        ''------------
                    ElseIf TrabajoSeleccionado = "ICAJA" Or TrabajoSeleccionado = "ICAJA6" Then
                        If opcion = "N" Then
                            MsgBox("Proceda a guardar la orden antes de asignar una Caja Digital", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        ContratoCajasDig = Me.ContratoTextBox.Text

                        If Me.StatusTextBox.Text = "P" Then
                            Me.dgvDetOrdSer.Enabled = False
                            OpICAJA = "E"
                            FrmSelCajasDisponiblesvb.ShowDialog()
                        ElseIf Me.StatusTextBox.Text = "E" Then
                            Me.dgvDetOrdSer.Enabled = False
                            OpICAJA = "P"
                            FrmSelCajasDisponiblesvb.ShowDialog()
                        End If
                        'Else
                        '    Exit Sub
                        'End If
                    ElseIf TrabajoSeleccionado = "CCAJA" Or TrabajoSeleccionado = "CCAJA6" Then
                        If GloeStatus = "E" Then
                            FormMuestra_Que_Hize_Orden_Servicio.ShowDialog()
                        ElseIf Me.StatusTextBox.Text = "P" Then
                            MsgBox("Sólo se puede Asignar el Aparato hasta que la Orden sea Ejecutada", , "Atención")
                        ElseIf Me.StatusTextBox.Text = "E" Then
                            Me.dgvDetOrdSer.Enabled = False
                            FrmIAPARAsigna.ShowDialog()
                        End If
                    ElseIf TrabajoSeleccionado = "CAPAR" Or TrabajoSeleccionado = "CAPAR6" Then
                        If GloeStatus = "E" Then
                            FormMuestra_Que_Hize_Orden_Servicio.ShowDialog()
                        ElseIf Me.StatusTextBox.Text = "P" Then
                            MsgBox("Sólo se puede Asignar el Aparato hasta que la Orden sea Ejecutada", , "Atención")
                        ElseIf Me.StatusTextBox.Text = "E" Then
                            Me.dgvDetOrdSer.Enabled = False
                            FrmIAPARAsigna.ShowDialog()
                        End If

                    ElseIf (TrabajoSeleccionado = "IPAQU" Or TrabajoSeleccionado = "BPAQU" Or TrabajoSeleccionado = "DPAQU" Or TrabajoSeleccionado = "RPAQU" Or TrabajoSeleccionado = "BPAQT") Then
                        'If Me.StatusTextBox.Text = "E" Then
                        Me.dgvDetOrdSer.Enabled = False
                        FrmrRelPaquetesdelCliente.ShowDialog()
                        'End If
                    ElseIf TrabajoSeleccionado = "IPAQUT" Or TrabajoSeleccionado = "BSEDI" Or (TrabajoSeleccionado) = "ASDIG" Or (TrabajoSeleccionado) = "DSDIG" Or (TrabajoSeleccionado) = "RSDIG" Then
                        'If Me.StatusTextBox.Text = "E" Then
                        Me.dgvDetOrdSer.Enabled = False
                        FrmrRelPaquetesdelCliente.ShowDialog()
                        'End If
                    ElseIf (TrabajoSeleccionado = "IPAQD" Or TrabajoSeleccionado = "BPAQD" Or TrabajoSeleccionado = "DPAQD" Or TrabajoSeleccionado = "RPAQD" _
                            Or TrabajoSeleccionado = "IPAQD6" Or TrabajoSeleccionado = "BPAQD6" Or TrabajoSeleccionado = "DPAQD6" Or TrabajoSeleccionado = "RPAQD6") Then
                        'If Me.StatusTextBox.Text = "E" Then
                        Me.dgvDetOrdSer.Enabled = False
                        FrmrRelPaquetesdelClienteDigital.ShowDialog()
                        'End If
                    ElseIf (TrabajoSeleccionado = "IANTE" Or TrabajoSeleccionado = "CANTE") Then

                        If GloeStatus = "E" Then
                            FormMuestra_Que_Hize_Orden_Servicio.ShowDialog()
                        ElseIf Me.StatusTextBox.Text = "P" Then
                            MsgBox("Sólo se puede Asignar la Antena hasta que la Orden sea Ejecutada", , "Atención")
                        ElseIf Me.StatusTextBox.Text = "E" Then
                            Me.dgvDetOrdSer.Enabled = False
                            FrmIANTE.ShowDialog()
                        End If
                    ElseIf (TrabajoSeleccionado = "IANRA" Or TrabajoSeleccionado = "IROUT") Then
                        If Me.StatusTextBox.Text = "E" Then
                            'Me.BUSCADetOrdSerDataGridView.Enabled = False
                            'GloClv_TipSer = 2
                            Dim FRM1 As New FormIAPARATOS
                            FRM1.ShowDialog()
                            Actuliza_Detalle()
                        ElseIf Me.StatusTextBox.Text = "P" Then
                            Dim FrM1 As New FormIAPARATOS_SELECCION
                            FrM1.ShowDialog()
                            Actuliza_Detalle()
                        End If
                    ElseIf (TrabajoSeleccionado = "CANRA" Or TrabajoSeleccionado = "CROUT") Then
                        'ANTENAS , LNB , TARJETAS 
                        If Me.StatusTextBox.Text = "E" Then
                            'Me.BUSCADetOrdSerDataGridView.Enabled = False
                            'GloClv_TipSer = 2
                            Dim FRM1 As New FormIAPARATOS
                            FRM1.ShowDialog()
                            Actuliza_Detalle()
                        End If
                    ElseIf (TrabajoSeleccionado = "RANRA" Or TrabajoSeleccionado = "RROUT") Then
                        Button2.Enabled = True
                        Dim FrM1 As New FormIAPARATOS_SELECCION
                        FrM1.ShowDialog()
                        Actuliza_Detalle()
                    End If



                    End If

                End If
        Else
            If (DAEquipoExtra.StatusOrdSer = "P") Then
                MsgBox("Primero Capture los Datos Solicitados ", MsgBoxStyle.Information)
            End If
        End If
    End Sub

    Private Sub Actuliza_Detalle()
        Dim CON As New SqlConnection(MiConexion)
        Try

            CON.Open()
            Me.BUSCADetOrdSerTableAdapter.Connection = CON
            Me.BUSCADetOrdSerTableAdapter.Fill(Me.NewSofTvDataSet.BUSCADetOrdSer, New System.Nullable(Of Long)(CType(Me.Clv_OrdenTextBox.Text, Long)))

        Catch ex As Exception
        Finally
            CON.Close()
        End Try
    End Sub

    'Función que identifica si es que un Trabajo está ligado a un Retiro de Aparato/Equipo/Material. Si así habilita la casilla de SeRetiroAParato
    Private Sub HabilitaRetiroAparatoPorDetalle(ByVal ListaDeTrabajos As List(Of TrabajosRetiroAparato))

        Dim i As Integer
        Dim Trabajo As String = ""

        Dim dgvTextBoxCell As New Windows.Forms.DataGridViewTextBoxCell
        Dim dgvCheckBoxCell As New Windows.Forms.DataGridViewCheckBoxCell


        Try
            'Recorremos los Trabajos del Detalle de la Orden
            For i = 0 To dgvDetOrdSer.RowCount - 1
                Try
                    'Obtenemos el Trabajo específico
                    Trabajo = dgvDetOrdSer.Item("trabajo", i).Value.ToString

                    'Verificamos si es que el trabajo está en la lista de la relación de trabajos con su retiro de aparato
                    For Each RelTrabajo As TrabajosRetiroAparato In ListaDeTrabajos

                        'Si la Orden se encuentra como Pendiente, validamos cada detalle de la Orden y su Trabajo
                        If (DAEquipoExtra.StatusOrdSer = "P") Or (DAEquipoExtra.StatusOrdSer = "V") Then
                            Try
                                If Trabajo = RelTrabajo.Trabajo Then
                                    'El trabajo es de retiro de aparato
                                    If RelTrabajo.AplicaRetiroAparato = True Then
                                        'Habilitamos la casilla para ser marcada
                                        dgvDetOrdSer.Item("seretiro", i).ReadOnly = False
                                        If (dgvDetOrdSer.Item("seretiro", i).Value = CheckState.Unchecked) Then
                                            MaterialPendienteDeRetirar = True
                                        End If
                                        'El trabajo NO es de retiro de aparato
                                    ElseIf RelTrabajo.AplicaRetiroAparato = False Then
                                        'Bloqueamos la edición de la casilla
                                        dgvDetOrdSer.Item("seretiro", i).ReadOnly = True
                                        dgvDetOrdSer.Item("seretiro", i).Value = 0
                                        dgvDetOrdSer.Item("seretiro", i).Style.BackColor = Color.Gray
                                    Else
                                        'Bloqueamos la edición de la casilla
                                        dgvDetOrdSer.Item("seretiro", i).ReadOnly = True
                                        dgvDetOrdSer.Item("seretiro", i).Value = 0
                                        dgvDetOrdSer.Item("seretiro", i).Style.BackColor = Color.Gray
                                    End If
                                End If
                            Catch ex As Exception
                                MsgBox(ex.Message)
                                Exit For
                            End Try
                            'Si la Orden ya fue ejecutada ya no se pueden realizar Cambios sobre su detalle
                        ElseIf (DAEquipoExtra.StatusOrdSer = "E") Then
                            dgvDetOrdSer.Item("seretiro", i).ReadOnly = True
                            'ElseIf (DAEquipoExtra.StatusOrdSer = "V") Then
                            '    dgvDetOrdSer.Item("seretiro", i).ReadOnly = True
                        End If

                    Next
                Catch ex As Exception
                    MsgBox(ex.Message)
                    Exit For
                End Try
            Next

            'Verificamos si se encontró por lo menos un detalle de trabajo de retiro de Equipo pendiente por encontrar.
            'btnGuardaRetiroParcial.Visible = MaterialPendienteDeRetirar
            'btnEjecutarRetiroParcial.Visible = MaterialPendienteDeRetirar

            'Si la Orden ya está ejecutada ya no se va a poder realizar retiros parciales de Material
            'If (DAEquipoExtra.StatusOrdSer = "E" Or DAEquipoExtra.StatusOrdSer = "V") Then
            'btnGuardaRetiroParcial.Visible = False
            'btnGuardaRetiroParcial.Visible = False
            'End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    'Private Function ValidaTrabajoRetiroAparato(ByVal Trabajo As String) As Boolean
    '    Dim AplicaRetiro As Boolean = False
    '    For Each RelTrabajo As TrabajosRetiroAparato In RelTrabajosRetiroAparatos
    '        Try
    '            If Trabajo = RelTrabajo.Trabajo Then
    '                If RelTrabajo.AplicaRetiro = True Then
    '                    AplicaRetiro = True
    '                ElseIf RelTrabajo.AplicaRetiro = False Then
    '                    AplicaRetiro = False
    '                Else
    '                    AplicaRetiro = False
    '                End If
    '            End If
    '        Catch ex As Exception
    '            MsgBox(ex.Message)
    '            Exit For
    '        End Try
    '    Next

    '    Return AplicaRetiro
    'End Function

    'Convierte el detalle de los Trabajos que impliquen el retiro de algún Aparato/Equipo/Material en XML para ser enviados a una rela-ción interna en BD

    Private Function RecorreDetalleXML() As String


        Dim sw As New StringWriter()
        Dim w As New XmlTextWriter(sw)
        Dim i As Integer
        Dim strXML As String = ""
        Dim chkGridDetOrdSer As New DataGridViewCheckBoxCell

        Try

            w.Formatting = Formatting.Indented
            w.WriteStartElement("Detalles")
            'Barremos las Columnas de Se Retiro por Cada Detalle de la Orden
            For i = 0 To dgvDetOrdSer.RowCount - 1


                'Filtramos solo aquellos detalles que sí apliquen Retiro de Aparato
                If (dgvDetOrdSer.Item("seretiro", i).ReadOnly = False) Then
                    w.WriteStartElement("Detalle")
                    w.WriteAttributeString("Clv_DetOrdSer", Me.dgvDetOrdSer.Rows(i).Cells("clave").Value.ToString)
                    w.WriteAttributeString("Trabajo", Me.dgvDetOrdSer.Rows(i).Cells("trabajo").Value.ToString)
                    w.WriteAttributeString("SeRetiroAparato", Me.dgvDetOrdSer.Rows(i).Cells("seretiro").Value.ToString)
                    w.WriteEndElement()

                    If (Me.dgvDetOrdSer.Rows(i).Cells("trabajo").Value.ToString = "IANRA" Or Me.dgvDetOrdSer.Rows(i).Cells("trabajo").Value.ToString = "IROUT") And Me.dgvDetOrdSer.Rows(i).Cells("seretiro").Value.ToString = True Then
                        RTrabajoInt = True
                    End If

                End If

            Next

            w.WriteEndElement()
            w.Close()

            strXML = ""
            strXML = sw.ToString()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try

        Return strXML
    End Function

    'btnGuaraRetiroParcial
    Private Sub btnGuardaRetiroParcial_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardaRetiroParcial.Click
        Try
            'Obtenemos los Detalles que aplican Retiro en XML
            If (dgvDetOrdSer.RowCount > 0) Then

                Dim xmldgvDetOrdSer As String = ""

                'Obtenemos el Detallado de la Orden en XML
                xmldgvDetOrdSer = RecorreDetalleXML()

                'Guardamos la relación
                Try
                    MaterialPendienteDeRetirar = (DAEquipoExtra.AgregaRelDetOrdSerRetiroAparato(xmldgvDetOrdSer, ContratoTextBox.Text, DAEquipoExtra.StatusOrdSer))
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try

            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    'btnEjecutarRetiroParcial (Ejecuta el Retiro Parcial de los Aparatos/Equipos/Material)
    Private Sub btnEjecutarRetiroParcial_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEjecutarRetiroParcial.Click

        If Not (MsgBox("Se va a generar y ejecutar una nueva Orden con los equipos y/o materiales marcados como ya retirados. ¿Deseas seguir?", MsgBoxStyle.YesNo, "¿Deseas seguir con la ejecución parcial del material?") = MsgBoxResult.Yes) Then
            Exit Sub
        End If

        Try
            'Obtenemos los Detalles que aplican Retiro en XML
            If (dgvDetOrdSer.RowCount > 0) Then

                Dim xmldgvDetOrdSer As String = ""

                'Obtenemos el Detallado de la Orden en XML
                xmldgvDetOrdSer = RecorreDetalleXML()

                'Guardamos la relación
                Try
                    DAEquipoExtra.EjecutarRelDetOrdSerRetiroAparato(xmldgvDetOrdSer, ContratoTextBox.Text, DAEquipoExtra.StatusOrdSer, Me.Tecnico.SelectedValue)

                    'Refrescamos el Detalle de la Orden
                    'Cargamos el Detalle de la Orden al Grid
                    CargaDetOrdSer(gloClave)
                    'Habilitamos solo aquellos detalles cuyo trabajo pueda implique el retiro de algún aparato (RAPAR,RCTRL,etc.)
                    HabilitaRetiroAparatoPorDetalle(RelTrabajosRetiroAparatos)

                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try

            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    'Función que recorre el Grid en busca de trabajos que implique Retiro de Aparato
    Public Function ExisteDetalleDeRetiroAparato() As Boolean

        Dim ExisteUnDetalleDeRetiro As Boolean = False
        Dim i As Integer

        Try

            'Barremos las Columnas de Se Retiro por Cada Detalle de la Orden
            For i = 0 To dgvDetOrdSer.RowCount - 1

                'Filtramos solo aquellos detalles que sí apliquen Retiro de Aparato
                If (dgvDetOrdSer.Item("seretiro", i).ReadOnly = False) Then
                    ExisteUnDetalleDeRetiro = True
                End If

            Next


        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try

        Return ExisteUnDetalleDeRetiro

    End Function

    Public Function DimeSiMarcoRetiro(ByVal eClaveBusca As Long) As Boolean
        DimeSiMarcoRetiro = False
        ' If ExisteUnDetalleDeRetiro = True And ExisteUnDetalleDeRetiroMarcado = True Then
        'ExisteUnDetalleDeRetiro = False 
        'ExisteUnDetalleDeRetiroMarcado = False
        'TDetallesDeRetiro = 0
        'TDetallesDeRetiroMarcadoS = 0
        Dim eseretiro As Boolean = False
        Dim Clave As Long = 0
        Dim i As Integer
        Dim Trabajo As String = ""
        dgvDetOrdSer.EndEdit()
        Try
            'Recorremos los Trabajos del Detalle de la Orden
            For i = 0 To dgvDetOrdSer.RowCount - 1
                Try
                    'Obtenemos el Trabajo específico
                    Trabajo = dgvDetOrdSer.Item("trabajo", i).Value.ToString
                    eClave = dgvDetOrdSer.Item("clave", i).Value.ToString
                    eseretiro = dgvDetOrdSer.Item("seretiro", i).Value.ToString
                    'Si la Orden ya fue ejecutada ya no se pueden realizar Cambios sobre su detalle

                    If eClave = eClaveBusca Then
                        If eseretiro = True Then
                            DimeSiMarcoRetiro = True
                        End If
                        Exit For
                    End If

                Catch ex As Exception
                    MsgBox(ex.Message)
                    Exit For
                End Try
            Next

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        '  End If
    End Function

    'Valida  Orden RetiroAparatos las Cajas y Tarjetas se tiene que Retirar siempre Juntas 
    Public Function ValidaOrdenRetiroAparatos() As Boolean
        ValidaOrdenRetiroAparatos = True
        ' If ExisteUnDetalleDeRetiro = True And ExisteUnDetalleDeRetiroMarcado = True Then
        'ExisteUnDetalleDeRetiro = False 
        'ExisteUnDetalleDeRetiroMarcado = False
        'TDetallesDeRetiro = 0
        'TDetallesDeRetiroMarcadoS = 0
        Dim eseretiro As Boolean = False
        Dim eClave As Long = 0
        Dim eClaveBusca As Long = 0
        Dim i As Integer
        Dim Trabajo As String = ""
        dgvDetOrdSer.EndEdit()
        Try
            'Recorremos los Trabajos del Detalle de la Orden
            For i = 0 To dgvDetOrdSer.RowCount - 1
                Try
                    'Obtenemos el Trabajo específico
                    Trabajo = dgvDetOrdSer.Item("trabajo", i).Value.ToString
                    eClave = dgvDetOrdSer.Item("clave", i).Value.ToString
                    eseretiro = dgvDetOrdSer.Item("seretiro", i).Value.ToString
                    'Si la Orden ya fue ejecutada ya no se pueden realizar Cambios sobre su detalle
                    If (DAEquipoExtra.StatusOrdSer = "P") Then
                        If ((Trabajo = "RAPAR" Or Trabajo = "RCAJA") Or (Trabajo = "RAPAR6" Or Trabajo = "RCAJA6")) And eseretiro = True Then
                            ' MsgBox("eseretiro")
                            eClaveBusca = USPDameDetOrserClave(Clv_OrdenTextBox.Text, eClave)
                            If DimeSiMarcoRetiro(eClaveBusca) = False Then
                                'MsgBox("Se tiene que retirar la Tarjeta y la Caja juntas ")
                                ValidaOrdenRetiroAparatos = False
                                Exit For
                            End If
                        End If
                    End If


                Catch ex As Exception
                    MsgBox(ex.Message)
                    'Exit For
                End Try
            Next

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        '  End If
    End Function

    'Carga Configuracion Orden RetiroAparatos
    Private Sub CargaConfiguracionOrdenRetiroAparatos(ByVal ListaDeTrabajos As List(Of TrabajosRetiroAparato))

        ExisteUnDetalleDeRetiro = False
        ExisteUnDetalleDeRetiroMarcado = False
        TDetallesDeRetiro = 0
        TDetallesDeRetiroMarcadoS = 0

        Dim i As Integer
        Dim Trabajo As String = ""
        dgvDetOrdSer.EndEdit()
        Try
            'Recorremos los Trabajos del Detalle de la Orden
            For i = 0 To dgvDetOrdSer.RowCount - 1
                Try
                    'Obtenemos el Trabajo específico
                    Trabajo = dgvDetOrdSer.Item("trabajo", i).Value.ToString

                    'Verificamos si es que el trabajo está en la lista de la relación de trabajos con su retiro de aparato
                    For Each RelTrabajo As TrabajosRetiroAparato In ListaDeTrabajos

                        'Si la Orden se encuentra como Pendiente, validamos cada detalle de la Orden y su Trabajo
                        If (DAEquipoExtra.StatusOrdSer = "P") Or (DAEquipoExtra.StatusOrdSer = "V") Then
                            Try
                                If Trabajo = RelTrabajo.Trabajo Then
                                    'El trabajo es de retiro de aparato
                                    If RelTrabajo.AplicaRetiroAparato = True Then
                                        ExisteUnDetalleDeRetiro = True
                                        TDetallesDeRetiro = TDetallesDeRetiro + 1
                                        'Habilitamos la casilla para ser marcada
                                        If (dgvDetOrdSer.Item("seretiro", i).Value = True) Then
                                            ExisteUnDetalleDeRetiroMarcado = True
                                            TDetallesDeRetiroMarcadoS = TDetallesDeRetiroMarcadoS + 1
                                        End If
                                        'El trabajo NO es de retiro de aparato
                                    End If
                                End If
                            Catch ex As Exception
                                MsgBox(ex.Message)
                                Exit For
                            End Try

                            'Si la Orden ya fue ejecutada ya no se pueden realizar Cambios sobre su detalle
                        ElseIf (DAEquipoExtra.StatusOrdSer = "E") Then
                            ExisteUnDetalleDeRetiro = False
                            ExisteUnDetalleDeRetiroMarcado = False
                            'ElseIf (DAEquipoExtra.StatusOrdSer = "V") Then
                            '    ExisteUnDetalleDeRetiro = False
                            '    ExisteUnDetalleDeRetiroMarcado = False
                        End If

                    Next
                Catch ex As Exception
                    MsgBox(ex.Message)
                    Exit For
                End Try
            Next

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    'Función que valida si se puede guardar una Orden dependiendo si es que cuenta con detalle de retiro de aparato y si es que está por lo menos una casilla marcada.
    Private Function ValidaConfiguracionOrdenRetiroAparato() As Boolean

        Dim Guardar As Boolean = False
        Try
            CargaConfiguracionOrdenRetiroAparatos(RelTrabajosRetiroAparatos)

            RTrabajoInt = False
            RTrabajoDig = False

            Dim i As Integer
            For i = 0 To dgvDetOrdSer.RowCount - 1

                If Me.dgvDetOrdSer.Rows(i).Cells("trabajo").Value.ToString = "IANRA" Or Me.dgvDetOrdSer.Rows(i).Cells("trabajo").Value.ToString = "IROUT" Then
                    RTrabajoInt = True
                End If

                If Me.dgvDetOrdSer.Rows(i).Cells("trabajo").Value.ToString = "IAPAR" Or Me.dgvDetOrdSer.Rows(i).Cells("trabajo").Value.ToString = "IANTE" Or Me.dgvDetOrdSer.Rows(i).Cells("trabajo").Value.ToString = "ICRCA" Or Me.dgvDetOrdSer.Rows(i).Cells("trabajo").Value.ToString = "IACDIG" Or Me.dgvDetOrdSer.Rows(i).Cells("trabajo").Value.ToString = "ECTRL" Then
                    RTrabajoDig = True
                End If

            Next

            If RTrabajoInt = True And RTrabajoDig = False Then
                ExisteUnDetalleDeRetiro = False
            ElseIf RTrabajoInt = False And RTrabajoDig = True Then
                ExisteUnDetalleDeRetiro = False
            End If

            RTrabajoInt = False
            RTrabajoDig = False

            If ExisteUnDetalleDeRetiro = True Then
                If ExisteUnDetalleDeRetiroMarcado = True Then
                    Guardar = True
                Else
                    Guardar = False
                    MsgBox("La Orden cuenta con trabajos de retiro ó instalación de aparatos que aún no se han marcado." & Chr(13) & "Antes de guardar un Retiro ó una Instalación Parcial debes de marcar mínimo un detalle.", MsgBoxStyle.Information)
                End If
            Else
                Guardar = True
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

        

        Return Guardar
    End Function

    'GuardaRetiroParcial
    Private Sub GuardaRetiroParcialDeAparatos()

        Try
            'Obtenemos los Detalles que aplican Retiro en XML
            If (dgvDetOrdSer.RowCount > 0) Then

                Dim xmldgvDetOrdSer As String = ""

                'Obtenemos el Detallado de la Orden en XML
                xmldgvDetOrdSer = RecorreDetalleXML()

                'Guardamos la relación
                Try
                    MaterialPendienteDeRetirar = (DAEquipoExtra.AgregaRelDetOrdSerRetiroAparato(xmldgvDetOrdSer, ContratoTextBox.Text, DAEquipoExtra.StatusOrdSer))
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try

            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    'EjecutaRetiroParcialDeAparatos
    Private Sub EjecutaRetiroParcialDeAparatos()

        Try
            'Obtenemos los Detalles que aplican Retiro en XML
            If (dgvDetOrdSer.RowCount > 0) Then

                Dim xmldgvDetOrdSer As String = ""

                'Obtenemos el Detallado de la Orden en XML
                xmldgvDetOrdSer = RecorreDetalleXML()

                'Guardamos la relación
                Try
                    DAEquipoExtra.EjecutarRelDetOrdSerRetiroAparato(xmldgvDetOrdSer, ContratoTextBox.Text, DAEquipoExtra.StatusOrdSer, Me.Tecnico.SelectedValue)
                    'Refrescamos el Detalle de la Orden
                    'Cargamos el Detalle de la Orden al Grid
                    CargaDetOrdSer(gloClave)
                    'Habilitamos solo aquellos detalles cuyo trabajo pueda implique el retiro de algún aparato (RAPAR,RCTRL,etc.)
                    HabilitaRetiroAparatoPorDetalle(RelTrabajosRetiroAparatos)
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try

            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub
#End Region

    Private Function uspDimeTipoServicioCliente(ByVal prmClvOrden As Long) As Integer
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clvOrden", SqlDbType.BigInt, prmClvOrden)
        BaseII.CreateMyParameter("@clvTipSer", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("uspDimeTipoServicioCliente")

        uspDimeTipoServicioCliente = CInt(BaseII.dicoPar("@clvTipSer").ToString())
    End Function
   
    Private Sub ContratoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContratoTextBox.TextChanged

    End Sub
    'SAUL 
    Private Sub UpdateComentarios(ByVal clv_orden As Long)
        Dim con60 As New SqlClient.SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Dim error1 As Integer = Nothing
        Try
            con60.Open()
            cmd = New SqlClient.SqlCommand()
            With cmd
                .CommandText = "UPsCOMENTARIOS"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = con60

                Dim prm As New SqlParameter("@CLV_ORDENAUX", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = clv_orden
                .Parameters.Add(prm)

                cmd.ExecuteNonQuery()

            End With
            con60.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    'SAUL (FIN)

    'SAUL
    Private Sub uspAutorizadoQuejasOrdenes()
        'GloUsuario-bndOxxo
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Usuario", SqlDbType.VarChar, GloUsuario, 10)
        BaseII.CreateMyParameter("@bnd", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("uspAutorizadoQuejasOrdenes")
        bndAcceso = CInt(BaseII.dicoPar("@bnd").ToString)
    End Sub
    '(Fin)

#Region "Valida Status Cliente Activo (JUANJO)"
    Private Function checaClienteActivo(ByVal prmContrato As Integer) As Boolean
        Dim orden As New classOrdenesQuejas
        orden.contrato = prmContrato
        checaClienteActivo = orden.uspChecaSiClienteActivo()
    End Function
#End Region

#Region "VALIDA SI IMPRIME QUEJA"
    Private Function checaSiImprimeOrden() As Boolean
        Dim imprimeQueja As New classValidaciones

        imprimeQueja.sistema = "SOFTV"
        imprimeQueja.ordenQueja = "ORDEN"
        checaSiImprimeOrden = imprimeQueja.uspChecaSiImprimeOrdenQueja()
    End Function
#End Region

#Region "VALIDA SI LA ORDEN ES DE RETIRO POR CAMBIO"
    Private Sub validaSiRetiroPorCambio(ByVal prmClvOrden As Integer)
        Dim ordenRetiro As New classValidaciones

        ordenRetiro.clvOrden = prmClvOrden
        bndRetiroPorCambio = ordenRetiro.uspValidaSiRetiroPorCambio()
    End Sub
#End Region

    Private Sub uspDimeTrabajo()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CInt(Me.ContratoTextBox.Text))
        BaseII.CreateMyParameter("@Orden", SqlDbType.Int, gloClave)
        BaseII.CreateMyParameter("@Desco", ParameterDirection.Output, SqlDbType.Int)
        'ByVal prmNombre As String, ByRef prmTipo As SqlDbType, ByRef prmTamanio As Integer, ByRef prmDireccion As ParameterDirection, ByRef prmValor As Object
        BaseII.ProcedimientoOutPut("uspDimeTrabajo")
        GloTrabajoR = CInt(BaseII.dicoPar("@Desco").ToString)
    End Sub

    Private Sub dgvDetOrdSer_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetOrdSer.CellContentClick

    End Sub

    Public Function SP_ValidaGuardaOrdSerAparatos(ByVal oClv_Orden As Long, ByVal oOpcion As String, ByVal oStatus As String, ByVal oOp2 As Integer) As String
        SP_ValidaGuardaOrdSerAparatos = ""
        '@CLV_ORDEN BIGINT=0,	@OPCION VARCHAR(10)='',	@STATUS VARCHAR(10)='',	@OP2 INT,	@Msj varchar(400)='' output
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_ORDEN", SqlDbType.BigInt, oClv_Orden)
        BaseII.CreateMyParameter("@OPCION", SqlDbType.VarChar, oOpcion, 10)
        BaseII.CreateMyParameter("@STATUS", SqlDbType.VarChar, oStatus, 10)
        BaseII.CreateMyParameter("@OP2", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@Msj", ParameterDirection.Output, SqlDbType.VarChar, 400)
        BaseII.ProcedimientoOutPut("SP_ValidaGuardaOrdSerAparatos")
        SP_ValidaGuardaOrdSerAparatos = BaseII.dicoPar("@Msj").ToString
    End Function

End Class