Imports System.Data.SqlClient
Imports System.Text

Public Class FrmCamServCte
    Dim eRes As Integer = 0
    Dim eMsg As String = Nothing
    Dim eClv_ServicioOld As Integer = 0
    Dim eClv_ServicioNew As Integer = 0
    Dim eContratoAux As Long = 0
    Dim eContratoNet As Long = 0
    Dim eClv_TipSer As Integer = 0
    Dim eMonto As Decimal = 0
    Private Bnd As Boolean = False


    Private Sub FrmCamServCte_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Try
            Dim CON As New SqlConnection(MiConexion)

            If eContrato > 0 Then

                'DataGridViewActual.DataSource = Nothing
                'DataGridViewPosible.DataSource = Nothing

                Me.TextBox1.Text = eContrato
                eContrato = 0
                CON.Open()
                Me.MuestraTipServEricTableAdapter.Connection = CON
                Me.MuestraTipServEricTableAdapter.Fill(Me.DataSetEric.MuestraTipServEric, CInt(Me.TextBox1.Text), 10)

                Me.DameClientesActivosTableAdapter.Connection = CON
                Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, Me.TextBox1.Text, "", "", "", "", 0)
                CON.Close()

                'If IdSistema = "LO" Or IdSistema = "YU" Or IdSistema = "AG" Then
                DameServCliente(CInt(Me.ConceptoComboBox.SelectedValue), CLng(Me.TextBox1.Text))
                'Else
                '    BuscarServCte()
                'End If

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub FrmCamServCte_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim CON As New SqlConnection(MiConexion)

            'If IdSistema = "LO" Or IdSistema = "YU" Or IdSistema = "AG" Then

            Me.GroupBoxActual.Visible = True
            Me.GroupBoxPosible.Visible = True
            'End If

            colorea(Me, Me.Name)
            If eOpcion = "N" Then
                CON.Open()
                Me.MuestraTipServEricTableAdapter.Connection = CON
                Me.MuestraTipServEricTableAdapter.Fill(Me.DataSetEric.MuestraTipServEric, 0, 0)
                CON.Close()
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Try
            eContrato = 0
            BrwSelContrato.Show()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        
    End Sub

    'Private Sub BuscarServCte()

    '    Try

    '        Dim CON As New SqlConnection(MiConexion)

    '        If Me.ConceptoComboBox.SelectedValue > 0 Then
    '            CON.Open()
    '            eRes = 0
    '            Me.DameServCteTableAdapter.Connection = CON
    '            Me.DameServCteTableAdapter.Fill(Me.DataSetEric.DameServCte, Me.TextBox1.Text, Me.ConceptoComboBox.SelectedValue, eRes, eMsg)
    '            CON.Close()
    '            If eRes = 1 Then
    '                MsgBox(eMsg)
    '            End If
    '        End If

    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try

    'End Sub

    'Private Sub BuscarServCtePosibles()
    '    Try

    '        If ContratoNetTextBox.Text.Length = 0 Then Exit Sub

    '        Dim CON As New SqlConnection(MiConexion)
    '        If eRes = 1 Then
    '            eContrato = 0
    '            CON.Open()
    '            Me.DameServPosiblesCteTableAdapter.Connection = CON
    '            Me.DameServPosiblesCteTableAdapter.Fill(Me.DataSetEric.DameServPosiblesCte, 0, 0, 0)
    '            CON.Close()
    '            MsgBox(eMsg)
    '        Else
    '            CON.Open()
    '            Me.DameServPosiblesCteTableAdapter.Connection = CON
    '            Me.DameServPosiblesCteTableAdapter.Fill(Me.DataSetEric.DameServPosiblesCte, Me.ContratoNetTextBox.Text, Me.ConceptoComboBox.SelectedValue, Me.Clv_ServicioTextBox.Text)
    '            CON.Close()
    '        End If


    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try
    'End Sub


    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        Try
            Dim CON As New SqlConnection(MiConexion)

            If IsNumeric(Me.TextBox1.Text) = True Then

                If Asc(e.KeyChar) = 13 Then
                    eContratoAux = Me.TextBox1.Text
                    CON.Open()
                    Me.MuestraTipServEricTableAdapter.Connection = CON
                    Me.MuestraTipServEricTableAdapter.Fill(Me.DataSetEric.MuestraTipServEric, CInt(Me.TextBox1.Text), 10)

                    Me.DameClientesActivosTableAdapter.Connection = CON
                    Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, eContratoAux, "", "", "", "", 0)
                    CON.Close()

                    'If IdSistema = "LO" Or IdSistema = "YU" Or IdSistema = "AG" Then
                    DameServCliente(CInt(Me.ConceptoComboBox.SelectedValue), eContratoAux)
                    'Else
                    '    BuscarServCte()
                    'End If

                End If

            End If



        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            Bnd = True
            Me.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        Try
            Dim CON As New SqlConnection(MiConexion)


            'If IdSistema = "LO" Or IdSistema = "YU" Or IdSistema = "AG" Then
            '    If DataGridViewActual.Rows.Count = 0 Or DataGridViewPosible.Rows.Count = 0 Then
            '        MsgBox("Selecciona el servicio a cambiar.")
            '        Exit Sub
            '    End If
            'Else
            '    If Me.DameServCteDataGridView.RowCount = 0 Or Me.DameServPosiblesCteDataGridView.RowCount = 0 Or IsNumeric(Me.ConceptoComboBox.SelectedValue) = False Then
            '        MsgBox("Selecciona el servicio a cambiar.")
            '        Exit Sub
            '    End If
            'End If


            If DataGridViewActual.Rows.Count = 0 Then
                MsgBox("Selecciona el servicio actual.")
                Exit Sub
            End If

            If DataGridViewPosible.Rows.Count = 0 Then
                MsgBox("Selecciona un posible servicio.")
                Exit Sub
            End If



            'If IdSistema = "LO" Or IdSistema = "YU" Or IdSistema = "AG" Then
            eClv_ServicioOld = DataGridViewActual.SelectedCells(4).Value
            eClv_ServicioNew = DataGridViewPosible.SelectedCells(0).Value
            eContratoAux = DataGridViewActual.SelectedCells(0).Value
            eContratoNet = DataGridViewActual.SelectedCells(1).Value
            eClv_TipSer = DataGridViewActual.SelectedCells(3).Value
            eMonto = DataGridViewPosible.SelectedCells(2).Value
            'Else
            'eClv_ServicioOld = CInt(Me.Clv_ServicioTextBox.Text)
            'eClv_ServicioNew = CInt(Me.Clv_ServicioTextBox1.Text)
            'eContratoAux = CLng(Me.ContratoTextBox.Text)
            'If IsNumeric(Me.ContratoNetTextBox.Text) = True Then eContratoNet = Me.ContratoNetTextBox.Text Else eContratoNet = 0
            'eClv_TipSer = CInt(Me.ConceptoComboBox.SelectedValue)
            'End If

            ValidaCambioServCliente(eContratoAux, eClv_TipSer, String.Empty)
            If eRes = 1 Then
                MsgBox(eMsg, MsgBoxStyle.Information)

                Exit Sub
            End If

            CambiaServCliente(eContratoAux, eContratoNet, eClv_TipSer, eClv_ServicioOld, eClv_ServicioNew, eMonto)
            bitsist(GloUsuario, eContratoAux, LocGloSistema, Me.Text, "", "Cambio de Servicio de: " + CStr(eClv_ServicioOld) + " a " + CStr(eClv_ServicioNew), GloSucursal, LocClv_Ciudad)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try


    End Sub





    Private Sub ValidaCambioServCliente(ByVal Contrato As Long, ByVal Clv_TipSer As Integer, ByVal Mac As String)


        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaCambioServCliente", conexion)
        comando.CommandType = CommandType.StoredProcedure
        Try
            Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
            parametro.Direction = ParameterDirection.Input
            parametro.Value = Contrato
            comando.Parameters.Add(parametro)

            Dim parametro1 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
            parametro1.Direction = ParameterDirection.Input
            parametro1.Value = Clv_TipSer
            comando.Parameters.Add(parametro1)

            Dim parametro2 As New SqlParameter("@Mac", SqlDbType.VarChar, 50)
            parametro2.Direction = ParameterDirection.Input
            parametro2.Value = Mac
            comando.Parameters.Add(parametro2)

            Dim parametro3 As New SqlParameter("@Res", SqlDbType.Int)
            parametro3.Direction = ParameterDirection.Output
            parametro3.Value = 0
            comando.Parameters.Add(parametro3)

            Dim parametro4 As New SqlParameter("@Msg", SqlDbType.VarChar, 150)
            parametro4.Direction = ParameterDirection.Output
            parametro4.Value = String.Empty
            comando.Parameters.Add(parametro4)


            eRes = 0
            eMsg = String.Empty
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            eRes = CInt(parametro3.Value.ToString)
            eMsg = parametro4.Value.ToString

        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub DameServCliente(ByVal Clv_TipSer As Long, ByVal Contrato As Integer)

        ValidaCambioServCliente(Contrato, Clv_TipSer, String.Empty)

        Dim conexion As New SqlConnection(MiConexion)
        Try
            Dim strSQL As New StringBuilder
            strSQL.Append("EXEC DameServClienteTwister ")
            strSQL.Append(CStr(Clv_TipSer) & ", ")
            strSQL.Append(CStr(Contrato))

            Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
            Dim dataTable As New DataTable
            Dim bindingSource As New BindingSource

            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.DataGridViewActual.DataSource = bindingSource

            If eRes = 1 Then
                MsgBox(eMsg, MsgBoxStyle.Information)
                DameServPosiblesCte(0, 0, 0)
            Else
                Refresca()
            End If

        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub DameServPosiblesCte(ByVal Clv_TipSer As Integer, ByVal Clv_UnicaNet As Long, ByVal Contrato As Long)

        Dim conexion As New SqlConnection(MiConexion)

        Try
            Dim strSQL As New StringBuilder
            strSQL.Append("EXEC DameServPosiblesCteTwister ")
            strSQL.Append(CStr(Clv_TipSer) & ", ")
            strSQL.Append(CStr(Clv_UnicaNet) & ", ")
            strSQL.Append(CStr(Contrato))

            Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
            Dim dataTable As New DataTable
            Dim bindingSource As New BindingSource

            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.DataGridViewPosible.DataSource = bindingSource

            'If Me.DataGridViewPosible.RowCount = 0 Then
            '    MsgBox("Existe un Cambio Pendiente por Realizar para este Paquete, Finalice el Cambio para Poder hacer un Nuevo Cambio", vbInformation)
            '    '    Me.DataGridViewPosible.Columns(0).Visible = False
            '    '    Me.DataGridViewPosible.Columns(1).Width = 150
            '    '    Me.DataGridViewPosible.Columns(2).Width = 100
            'End If

        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub CambiaServCliente(ByVal Contrato As Long, ByVal ContratoNet As Long, ByVal Clv_TipSer As Integer, ByVal Clv_ServOld As Integer, ByVal Clv_ServNew As Long, ByVal Monto As Decimal)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("CambiaServCliente", conexion)
        comando.CommandType = CommandType.StoredProcedure
        Try

            Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
            parametro.Direction = ParameterDirection.Input
            parametro.Value = Contrato
            comando.Parameters.Add(parametro)

            Dim parametro2 As New SqlParameter("@ContratoNet", SqlDbType.BigInt)
            parametro2.Direction = ParameterDirection.Input
            parametro2.Value = ContratoNet
            comando.Parameters.Add(parametro2)

            Dim parametro3 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
            parametro3.Direction = ParameterDirection.Input
            parametro3.Value = Clv_TipSer
            comando.Parameters.Add(parametro3)

            Dim parametro4 As New SqlParameter("@Clv_ServOld", SqlDbType.Int)
            parametro4.Direction = ParameterDirection.Input
            parametro4.Value = Clv_ServOld
            comando.Parameters.Add(parametro4)

            Dim parametro5 As New SqlParameter("@Clv_ServNew", SqlDbType.Int)
            parametro5.Direction = ParameterDirection.Input
            parametro5.Value = Clv_ServNew
            comando.Parameters.Add(parametro5)

            Dim parametro6 As New SqlParameter("@Monto", SqlDbType.Decimal)
            parametro6.Direction = ParameterDirection.Input
            parametro6.Value = Monto
            comando.Parameters.Add(parametro6)

            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            eContrato = 0
            MsgBox(mensaje5)
            Me.Close()
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub DataGridViewActual_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridViewActual.CellClick
        Refresca()
    End Sub

    Private Sub DataGridViewActual_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridViewActual.CellContentClick
        Refresca()
    End Sub


    Private Sub Refresca()
        Try
            If Me.DataGridViewActual.Rows.Count > 0 Then
                eContratoAux = DataGridViewActual.SelectedCells(0).Value
                eClv_UnicaNet = DataGridViewActual.SelectedCells(2).Value
                eClv_TipSer = DataGridViewActual.SelectedCells(3).Value
                eClv_Servicio = DataGridViewActual.SelectedCells(4).Value

                DameServPosiblesCte(eClv_TipSer, eClv_UnicaNet, eContratoAux)
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub





    'Private Sub ConceptoComboBox_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    '    Try

    '        If Bnd = False Then

    '            Dim CON As New SqlConnection(MiConexion)

    '            If IsNumeric(Me.TextBox1.Text) = True Then
    '                CON.Open()
    '                Me.DameClientesActivosTableAdapter.Connection = CON
    '                Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, Me.TextBox1.Text, "", "", "", "", 0)
    '                CON.Close()
    '                'If IdSistema = "LO" Or IdSistema = "YU" Or IdSistema = "AG" Then
    '                DameServCliente(CInt(Me.ConceptoComboBox.SelectedValue), CLng(Me.TextBox1.Text))
    '                'Else
    '                '   BuscarServCte()
    '                'End If

    '            End If
    '        End If


    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try

    'End Sub


    Private Sub ConceptoComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConceptoComboBox.SelectedIndexChanged
        If ConceptoComboBox.Text.Length = 0 Then
            Exit Sub
        End If
        If TextBox1.Text.Length = 0 Then
            Exit Sub
        End If

        DameServCliente(CInt(Me.ConceptoComboBox.SelectedValue), TextBox1.Text)
    End Sub

    Private Sub TextBox1_TextChanged(sender As System.Object, e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub
End Class