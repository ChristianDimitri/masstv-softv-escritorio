<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BRWCNRDIGX
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ConsecutivoLabel As System.Windows.Forms.Label
        Dim Numero_de_contratoLabel As System.Windows.Forms.Label
        Dim Mac_addresLabel As System.Windows.Forms.Label
        Dim PaqueteLabel As System.Windows.Forms.Label
        Dim ComandoLabel As System.Windows.Forms.Label
        Dim ResultadoLabel As System.Windows.Forms.Label
        Dim Descripcion_transaccionLabel As System.Windows.Forms.Label
        Dim Clv_OrdenLabel As System.Windows.Forms.Label
        Dim Fec_SolLabel As System.Windows.Forms.Label
        Dim Fec_EjeLabel As System.Windows.Forms.Label
        Dim StatusLabel As System.Windows.Forms.Label
        Dim Label12 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.BUSCACNRDIGBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.ConsecutivoLabel1 = New System.Windows.Forms.Label()
        Me.Numero_de_contratoLabel1 = New System.Windows.Forms.Label()
        Me.Mac_addresLabel1 = New System.Windows.Forms.Label()
        Me.c = New System.Windows.Forms.Label()
        Me.ComandoLabel1 = New System.Windows.Forms.Label()
        Me.ResultadoLabel1 = New System.Windows.Forms.Label()
        Me.Descripcion_transaccionLabel1 = New System.Windows.Forms.Label()
        Me.Clv_OrdenLabel1 = New System.Windows.Forms.Label()
        Me.Fec_SolLabel1 = New System.Windows.Forms.Label()
        Me.Fec_EjeLabel1 = New System.Windows.Forms.Label()
        Me.StatusLabel1 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.DamePermisosFormTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.DamePermisosFormTableAdapter()
        Me.DamePermisosFormBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BUSCACNRDIGTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.BUSCACNRDIGTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        ConsecutivoLabel = New System.Windows.Forms.Label()
        Numero_de_contratoLabel = New System.Windows.Forms.Label()
        Mac_addresLabel = New System.Windows.Forms.Label()
        PaqueteLabel = New System.Windows.Forms.Label()
        ComandoLabel = New System.Windows.Forms.Label()
        ResultadoLabel = New System.Windows.Forms.Label()
        Descripcion_transaccionLabel = New System.Windows.Forms.Label()
        Clv_OrdenLabel = New System.Windows.Forms.Label()
        Fec_SolLabel = New System.Windows.Forms.Label()
        Fec_EjeLabel = New System.Windows.Forms.Label()
        StatusLabel = New System.Windows.Forms.Label()
        Label12 = New System.Windows.Forms.Label()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.BUSCACNRDIGBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.DamePermisosFormBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ConsecutivoLabel
        '
        ConsecutivoLabel.AutoSize = True
        ConsecutivoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ConsecutivoLabel.Location = New System.Drawing.Point(8, 39)
        ConsecutivoLabel.Name = "ConsecutivoLabel"
        ConsecutivoLabel.Size = New System.Drawing.Size(92, 15)
        ConsecutivoLabel.TabIndex = 5
        ConsecutivoLabel.Text = "Consecutivo :"
        '
        'Numero_de_contratoLabel
        '
        Numero_de_contratoLabel.AutoSize = True
        Numero_de_contratoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Numero_de_contratoLabel.Location = New System.Drawing.Point(29, 68)
        Numero_de_contratoLabel.Name = "Numero_de_contratoLabel"
        Numero_de_contratoLabel.Size = New System.Drawing.Size(69, 15)
        Numero_de_contratoLabel.TabIndex = 7
        Numero_de_contratoLabel.Text = "Contrato :"
        '
        'Mac_addresLabel
        '
        Mac_addresLabel.AutoSize = True
        Mac_addresLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Mac_addresLabel.Location = New System.Drawing.Point(9, 96)
        Mac_addresLabel.Name = "Mac_addresLabel"
        Mac_addresLabel.Size = New System.Drawing.Size(97, 15)
        Mac_addresLabel.TabIndex = 11
        Mac_addresLabel.Text = "Mac Address :"
        '
        'PaqueteLabel
        '
        PaqueteLabel.AutoSize = True
        PaqueteLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PaqueteLabel.Location = New System.Drawing.Point(9, 145)
        PaqueteLabel.Name = "PaqueteLabel"
        PaqueteLabel.Size = New System.Drawing.Size(112, 15)
        PaqueteLabel.TabIndex = 13
        PaqueteLabel.Text = "Servicio Digital :"
        '
        'ComandoLabel
        '
        ComandoLabel.AutoSize = True
        ComandoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ComandoLabel.Location = New System.Drawing.Point(15, 196)
        ComandoLabel.Name = "ComandoLabel"
        ComandoLabel.Size = New System.Drawing.Size(76, 15)
        ComandoLabel.TabIndex = 15
        ComandoLabel.Text = "Comando :"
        '
        'ResultadoLabel
        '
        ResultadoLabel.AutoSize = True
        ResultadoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ResultadoLabel.Location = New System.Drawing.Point(11, 225)
        ResultadoLabel.Name = "ResultadoLabel"
        ResultadoLabel.Size = New System.Drawing.Size(80, 15)
        ResultadoLabel.TabIndex = 17
        ResultadoLabel.Text = "Resultado :"
        '
        'Descripcion_transaccionLabel
        '
        Descripcion_transaccionLabel.AutoSize = True
        Descripcion_transaccionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Descripcion_transaccionLabel.Location = New System.Drawing.Point(14, 254)
        Descripcion_transaccionLabel.Name = "Descripcion_transaccionLabel"
        Descripcion_transaccionLabel.Size = New System.Drawing.Size(91, 15)
        Descripcion_transaccionLabel.TabIndex = 19
        Descripcion_transaccionLabel.Text = "Descripción :"
        '
        'Clv_OrdenLabel
        '
        Clv_OrdenLabel.AutoSize = True
        Clv_OrdenLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_OrdenLabel.Location = New System.Drawing.Point(5, 381)
        Clv_OrdenLabel.Name = "Clv_OrdenLabel"
        Clv_OrdenLabel.Size = New System.Drawing.Size(93, 15)
        Clv_OrdenLabel.TabIndex = 21
        Clv_OrdenLabel.Text = "Clave Orden :"
        '
        'Fec_SolLabel
        '
        Fec_SolLabel.AutoSize = True
        Fec_SolLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fec_SolLabel.Location = New System.Drawing.Point(14, 410)
        Fec_SolLabel.Name = "Fec_SolLabel"
        Fec_SolLabel.Size = New System.Drawing.Size(114, 15)
        Fec_SolLabel.TabIndex = 23
        Fec_SolLabel.Text = "Fecha Solicitud :"
        '
        'Fec_EjeLabel
        '
        Fec_EjeLabel.AutoSize = True
        Fec_EjeLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fec_EjeLabel.Location = New System.Drawing.Point(7, 439)
        Fec_EjeLabel.Name = "Fec_EjeLabel"
        Fec_EjeLabel.Size = New System.Drawing.Size(121, 15)
        Fec_EjeLabel.TabIndex = 25
        Fec_EjeLabel.Text = "Fecha Ejecución :"
        '
        'StatusLabel
        '
        StatusLabel.AutoSize = True
        StatusLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        StatusLabel.Location = New System.Drawing.Point(73, 496)
        StatusLabel.Name = "StatusLabel"
        StatusLabel.Size = New System.Drawing.Size(55, 15)
        StatusLabel.TabIndex = 27
        StatusLabel.Text = "Status :"
        '
        'Label12
        '
        Label12.AutoSize = True
        Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label12.Location = New System.Drawing.Point(19, 464)
        Label12.Name = "Label12"
        Label12.Size = New System.Drawing.Size(109, 15)
        Label12.TabIndex = 29
        Label12.Text = "Fecha Habilitar:"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Chocolate
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(502, 694)
        Me.DataGridView1.TabIndex = 1
        Me.DataGridView1.TabStop = False
        '
        'TextBox3
        '
        Me.TextBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(22, 265)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(139, 22)
        Me.TextBox3.TabIndex = 8
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(19, 247)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(89, 15)
        Me.Label5.TabIndex = 15
        Me.Label5.Text = "Mac Adress :"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(22, 203)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(88, 23)
        Me.Button1.TabIndex = 7
        Me.Button1.Text = "&Buscar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.DarkOrange
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.ForeColor = System.Drawing.Color.Black
        Me.Button7.Location = New System.Drawing.Point(22, 114)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(88, 23)
        Me.Button7.TabIndex = 5
        Me.Button7.Text = "&Buscar"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'TextBox2
        '
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(22, 176)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(89, 22)
        Me.TextBox2.TabIndex = 6
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(22, 87)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(88, 22)
        Me.TextBox1.TabIndex = 4
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(18, 16)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(130, 24)
        Me.CMBLabel1.TabIndex = 1
        Me.CMBLabel1.Text = "Buscar  Por :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(19, 69)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(92, 15)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Consecutivo :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(19, 158)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 15)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Contrato :"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Chocolate
        Me.Panel1.Controls.Add(Label12)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(ConsecutivoLabel)
        Me.Panel1.Controls.Add(Me.ConsecutivoLabel1)
        Me.Panel1.Controls.Add(Numero_de_contratoLabel)
        Me.Panel1.Controls.Add(Me.Numero_de_contratoLabel1)
        Me.Panel1.Controls.Add(Mac_addresLabel)
        Me.Panel1.Controls.Add(Me.Mac_addresLabel1)
        Me.Panel1.Controls.Add(PaqueteLabel)
        Me.Panel1.Controls.Add(Me.c)
        Me.Panel1.Controls.Add(ComandoLabel)
        Me.Panel1.Controls.Add(Me.ComandoLabel1)
        Me.Panel1.Controls.Add(ResultadoLabel)
        Me.Panel1.Controls.Add(Me.ResultadoLabel1)
        Me.Panel1.Controls.Add(Descripcion_transaccionLabel)
        Me.Panel1.Controls.Add(Me.Descripcion_transaccionLabel1)
        Me.Panel1.Controls.Add(Clv_OrdenLabel)
        Me.Panel1.Controls.Add(Me.Clv_OrdenLabel1)
        Me.Panel1.Controls.Add(Fec_SolLabel)
        Me.Panel1.Controls.Add(Me.Fec_SolLabel1)
        Me.Panel1.Controls.Add(Fec_EjeLabel)
        Me.Panel1.Controls.Add(Me.Fec_EjeLabel1)
        Me.Panel1.Controls.Add(StatusLabel)
        Me.Panel1.Controls.Add(Me.StatusLabel1)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Location = New System.Drawing.Point(763, 23)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(252, 531)
        Me.Panel1.TabIndex = 20
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.Color.SandyBrown
        Me.Label13.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCACNRDIGBindingSource, "fecha_habilitar", True))
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(133, 464)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(100, 23)
        Me.Label13.TabIndex = 30
        '
        'BUSCACNRDIGBindingSource
        '
        Me.BUSCACNRDIGBindingSource.DataMember = "BUSCACNRDIG"
        Me.BUSCACNRDIGBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ConsecutivoLabel1
        '
        Me.ConsecutivoLabel1.BackColor = System.Drawing.Color.SandyBrown
        Me.ConsecutivoLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCACNRDIGBindingSource, "consecutivo", True))
        Me.ConsecutivoLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConsecutivoLabel1.Location = New System.Drawing.Point(102, 35)
        Me.ConsecutivoLabel1.Name = "ConsecutivoLabel1"
        Me.ConsecutivoLabel1.Size = New System.Drawing.Size(100, 23)
        Me.ConsecutivoLabel1.TabIndex = 6
        '
        'Numero_de_contratoLabel1
        '
        Me.Numero_de_contratoLabel1.BackColor = System.Drawing.Color.SandyBrown
        Me.Numero_de_contratoLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCACNRDIGBindingSource, "numero_de_contrato", True))
        Me.Numero_de_contratoLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Numero_de_contratoLabel1.Location = New System.Drawing.Point(102, 64)
        Me.Numero_de_contratoLabel1.Name = "Numero_de_contratoLabel1"
        Me.Numero_de_contratoLabel1.Size = New System.Drawing.Size(100, 23)
        Me.Numero_de_contratoLabel1.TabIndex = 8
        '
        'Mac_addresLabel1
        '
        Me.Mac_addresLabel1.BackColor = System.Drawing.Color.SandyBrown
        Me.Mac_addresLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCACNRDIGBindingSource, "mac_address", True))
        Me.Mac_addresLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Mac_addresLabel1.Location = New System.Drawing.Point(9, 117)
        Me.Mac_addresLabel1.Name = "Mac_addresLabel1"
        Me.Mac_addresLabel1.Size = New System.Drawing.Size(223, 23)
        Me.Mac_addresLabel1.TabIndex = 12
        '
        'c
        '
        Me.c.BackColor = System.Drawing.Color.SandyBrown
        Me.c.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCACNRDIGBindingSource, "paquete", True))
        Me.c.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.c.Location = New System.Drawing.Point(11, 164)
        Me.c.Name = "c"
        Me.c.Size = New System.Drawing.Size(221, 23)
        Me.c.TabIndex = 14
        '
        'ComandoLabel1
        '
        Me.ComandoLabel1.BackColor = System.Drawing.Color.SandyBrown
        Me.ComandoLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCACNRDIGBindingSource, "comando", True))
        Me.ComandoLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComandoLabel1.Location = New System.Drawing.Point(96, 193)
        Me.ComandoLabel1.Name = "ComandoLabel1"
        Me.ComandoLabel1.Size = New System.Drawing.Size(136, 23)
        Me.ComandoLabel1.TabIndex = 16
        '
        'ResultadoLabel1
        '
        Me.ResultadoLabel1.BackColor = System.Drawing.Color.SandyBrown
        Me.ResultadoLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCACNRDIGBindingSource, "resultado", True))
        Me.ResultadoLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ResultadoLabel1.Location = New System.Drawing.Point(96, 222)
        Me.ResultadoLabel1.Name = "ResultadoLabel1"
        Me.ResultadoLabel1.Size = New System.Drawing.Size(29, 23)
        Me.ResultadoLabel1.TabIndex = 18
        '
        'Descripcion_transaccionLabel1
        '
        Me.Descripcion_transaccionLabel1.BackColor = System.Drawing.Color.SandyBrown
        Me.Descripcion_transaccionLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCACNRDIGBindingSource, "descripcion_transaccion", True))
        Me.Descripcion_transaccionLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Descripcion_transaccionLabel1.Location = New System.Drawing.Point(18, 275)
        Me.Descripcion_transaccionLabel1.Name = "Descripcion_transaccionLabel1"
        Me.Descripcion_transaccionLabel1.Size = New System.Drawing.Size(214, 95)
        Me.Descripcion_transaccionLabel1.TabIndex = 20
        '
        'Clv_OrdenLabel1
        '
        Me.Clv_OrdenLabel1.BackColor = System.Drawing.Color.SandyBrown
        Me.Clv_OrdenLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCACNRDIGBindingSource, "Clv_Orden", True))
        Me.Clv_OrdenLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_OrdenLabel1.Location = New System.Drawing.Point(102, 377)
        Me.Clv_OrdenLabel1.Name = "Clv_OrdenLabel1"
        Me.Clv_OrdenLabel1.Size = New System.Drawing.Size(100, 23)
        Me.Clv_OrdenLabel1.TabIndex = 22
        '
        'Fec_SolLabel1
        '
        Me.Fec_SolLabel1.BackColor = System.Drawing.Color.SandyBrown
        Me.Fec_SolLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCACNRDIGBindingSource, "Fec_Sol", True))
        Me.Fec_SolLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fec_SolLabel1.Location = New System.Drawing.Point(133, 406)
        Me.Fec_SolLabel1.Name = "Fec_SolLabel1"
        Me.Fec_SolLabel1.Size = New System.Drawing.Size(100, 23)
        Me.Fec_SolLabel1.TabIndex = 24
        '
        'Fec_EjeLabel1
        '
        Me.Fec_EjeLabel1.BackColor = System.Drawing.Color.SandyBrown
        Me.Fec_EjeLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCACNRDIGBindingSource, "Fec_Eje", True))
        Me.Fec_EjeLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fec_EjeLabel1.Location = New System.Drawing.Point(133, 435)
        Me.Fec_EjeLabel1.Name = "Fec_EjeLabel1"
        Me.Fec_EjeLabel1.Size = New System.Drawing.Size(100, 23)
        Me.Fec_EjeLabel1.TabIndex = 26
        '
        'StatusLabel1
        '
        Me.StatusLabel1.BackColor = System.Drawing.Color.SandyBrown
        Me.StatusLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCACNRDIGBindingSource, "Status", True))
        Me.StatusLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusLabel1.Location = New System.Drawing.Point(133, 493)
        Me.StatusLabel1.Name = "StatusLabel1"
        Me.StatusLabel1.Size = New System.Drawing.Size(100, 23)
        Me.StatusLabel1.TabIndex = 28
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(3, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(154, 20)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Datos del Servicio"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(829, 693)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 36)
        Me.Button5.TabIndex = 3
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'ComboBox2
        '
        Me.ComboBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Items.AddRange(New Object() {"E - Orden con Status Ejecutado", "P - Orden con Status Pendiente"})
        Me.ComboBox2.Location = New System.Drawing.Point(22, 527)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(209, 23)
        Me.ComboBox2.TabIndex = 14
        '
        'ComboBox1
        '
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Items.AddRange(New Object() {"0 - Pendiente", "1 - Procesada", "2 - Transación Fallida"})
        Me.ComboBox1.Location = New System.Drawing.Point(22, 355)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(209, 24)
        Me.ComboBox1.TabIndex = 10
        '
        'Button9
        '
        Me.Button9.BackColor = System.Drawing.Color.DarkOrange
        Me.Button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button9.ForeColor = System.Drawing.Color.Black
        Me.Button9.Location = New System.Drawing.Point(22, 733)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(88, 23)
        Me.Button9.TabIndex = 19
        Me.Button9.Text = "&Buscar"
        Me.Button9.UseVisualStyleBackColor = False
        '
        'TextBox8
        '
        Me.TextBox8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox8.Location = New System.Drawing.Point(22, 706)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New System.Drawing.Size(118, 22)
        Me.TextBox8.TabIndex = 18
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(19, 688)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(121, 15)
        Me.Label10.TabIndex = 30
        Me.Label10.Text = "Fecha Ejecución :"
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.DarkOrange
        Me.Button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.Black
        Me.Button8.Location = New System.Drawing.Point(22, 644)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(88, 23)
        Me.Button8.TabIndex = 17
        Me.Button8.Text = "&Buscar"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'TextBox7
        '
        Me.TextBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox7.Location = New System.Drawing.Point(22, 617)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(118, 22)
        Me.TextBox7.TabIndex = 16
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(19, 599)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(114, 15)
        Me.Label9.TabIndex = 27
        Me.Label9.Text = "Fecha Solicitud :"
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkOrange
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.Black
        Me.Button6.Location = New System.Drawing.Point(22, 554)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(88, 23)
        Me.Button6.TabIndex = 15
        Me.Button6.Text = "&Buscar"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(19, 509)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(55, 15)
        Me.Label8.TabIndex = 24
        Me.Label8.Text = "Status :"
        '
        'TextBox5
        '
        Me.TextBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox5.Location = New System.Drawing.Point(22, 443)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(89, 22)
        Me.TextBox5.TabIndex = 12
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkOrange
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.Black
        Me.Button4.Location = New System.Drawing.Point(22, 470)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(88, 23)
        Me.Button4.TabIndex = 13
        Me.Button4.Text = "&Buscar"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(19, 425)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(93, 15)
        Me.Label7.TabIndex = 21
        Me.Label7.Text = "Clave Orden :"
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(22, 382)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(88, 23)
        Me.Button3.TabIndex = 11
        Me.Button3.Text = "&Buscar"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(19, 337)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(80, 15)
        Me.Label6.TabIndex = 18
        Me.Label6.Text = "Resultado :"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Location = New System.Drawing.Point(1, 23)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button10)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox4)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label11)
        Me.SplitContainer1.Panel1.Controls.Add(Me.ComboBox2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.ComboBox1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button9)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox8)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label10)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button8)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox7)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label9)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button6)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label8)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button4)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label7)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label6)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button7)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label2)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.DataGridView1)
        Me.SplitContainer1.Size = New System.Drawing.Size(757, 694)
        Me.SplitContainer1.SplitterDistance = 251
        Me.SplitContainer1.TabIndex = 22
        Me.SplitContainer1.TabStop = False
        '
        'Button10
        '
        Me.Button10.BackColor = System.Drawing.Color.DarkOrange
        Me.Button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button10.ForeColor = System.Drawing.Color.Black
        Me.Button10.Location = New System.Drawing.Point(22, 822)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(88, 23)
        Me.Button10.TabIndex = 21
        Me.Button10.Text = "&Buscar"
        Me.Button10.UseVisualStyleBackColor = False
        '
        'TextBox4
        '
        Me.TextBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox4.Location = New System.Drawing.Point(22, 794)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(118, 22)
        Me.TextBox4.TabIndex = 20
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(19, 777)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(113, 15)
        Me.Label11.TabIndex = 35
        Me.Label11.Text = "Fecha Habilitar :"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(22, 292)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(88, 23)
        Me.Button2.TabIndex = 9
        Me.Button2.Text = "&Buscar"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button11
        '
        Me.Button11.BackColor = System.Drawing.Color.DarkOrange
        Me.Button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button11.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button11.ForeColor = System.Drawing.Color.Black
        Me.Button11.Location = New System.Drawing.Point(829, 559)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(136, 36)
        Me.Button11.TabIndex = 0
        Me.Button11.Text = "&NUEVO"
        Me.Button11.UseVisualStyleBackColor = False
        Me.Button11.Visible = False
        '
        'Button12
        '
        Me.Button12.BackColor = System.Drawing.Color.DarkOrange
        Me.Button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button12.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button12.ForeColor = System.Drawing.Color.Black
        Me.Button12.Location = New System.Drawing.Point(829, 598)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(136, 36)
        Me.Button12.TabIndex = 1
        Me.Button12.Text = "&CONSULTA"
        Me.Button12.UseVisualStyleBackColor = False
        '
        'Button13
        '
        Me.Button13.BackColor = System.Drawing.Color.DarkOrange
        Me.Button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button13.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button13.ForeColor = System.Drawing.Color.Black
        Me.Button13.Location = New System.Drawing.Point(829, 640)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(136, 36)
        Me.Button13.TabIndex = 2
        Me.Button13.Text = "&MODIFICA"
        Me.Button13.UseVisualStyleBackColor = False
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'DamePermisosFormTableAdapter
        '
        Me.DamePermisosFormTableAdapter.ClearBeforeFill = True
        '
        'DamePermisosFormBindingSource
        '
        Me.DamePermisosFormBindingSource.DataMember = "DamePermisosForm"
        Me.DamePermisosFormBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'BUSCACNRDIGTableAdapter
        '
        Me.BUSCACNRDIGTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'BRWCNRDIGX
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1016, 741)
        Me.Controls.Add(Me.Button13)
        Me.Controls.Add(Me.Button12)
        Me.Controls.Add(Me.Button11)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Name = "BRWCNRDIGX"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "CNR Digital"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.BUSCACNRDIGBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        CType(Me.DamePermisosFormBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents ConsecutivoLabel1 As System.Windows.Forms.Label
    Friend WithEvents Numero_de_contratoLabel1 As System.Windows.Forms.Label
    Friend WithEvents Mac_addresLabel1 As System.Windows.Forms.Label
    Friend WithEvents c As System.Windows.Forms.Label
    Friend WithEvents ComandoLabel1 As System.Windows.Forms.Label
    Friend WithEvents ResultadoLabel1 As System.Windows.Forms.Label
    Friend WithEvents Descripcion_transaccionLabel1 As System.Windows.Forms.Label
    Friend WithEvents Clv_OrdenLabel1 As System.Windows.Forms.Label
    Friend WithEvents Fec_SolLabel1 As System.Windows.Forms.Label
    Friend WithEvents Fec_EjeLabel1 As System.Windows.Forms.Label
    Friend WithEvents StatusLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents DamePermisosFormTableAdapter As sofTV.NewSofTvDataSetTableAdapters.DamePermisosFormTableAdapter
    Friend WithEvents DamePermisosFormBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents BUSCACNRDIGBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BUSCACNRDIGTableAdapter As sofTV.NewSofTvDataSetTableAdapters.BUSCACNRDIGTableAdapter
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
