Imports System.Data.SqlClient
Imports System.Text
Public Class FrmRepPermanencia

    Private Sub MuestraTipServEric()

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder()
        strSQL.Append("EXEC MuestraTipServEric 0,0")

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.ComboBoxTipSer.DataSource = bindingSource.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub MuestraMesesIni()

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder()
        strSQL.Append("EXEC MuestraMeses")

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.ComboBoxMesIni.DataSource = bindingSource.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub MuestraMesesFin()

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder()
        strSQL.Append("EXEC MuestraMeses")

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.ComboBoxMesFin.DataSource = bindingSource.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub MuestraAniosIni()

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder()
        strSQL.Append("EXEC MuestraAnios")

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.ComboBoxAnioIni.DataSource = bindingSource.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub


    Private Sub MuestraAniosFin()

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder()
        strSQL.Append("EXEC MuestraAnios")

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.ComboBoxAnioFin.DataSource = bindingSource.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub DameSession()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("Dame_clv_session_Reportes", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eClv_Session = CLng(parametro.Value.ToString())
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub FrmRepPermanencia_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        MuestraTipServEric()
        MuestraMesesIni()
        MuestraMesesFin()
        MuestraAniosIni()
        MuestraAniosFin()
        DameSession()
        If eOpVentas = 69 Then
            Me.Text = "Reporte de Permanencia"
        ElseIf eOpVentas = 70 Then
            Me.Text = "Reporte de Resumen de Ventas"
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eTipSer = Me.ComboBoxTipSer.SelectedValue
        eServicio = Me.ComboBoxTipSer.Text
        eMesIni = Me.ComboBoxMesIni.SelectedValue
        eStrMesIni = Me.ComboBoxMesIni.Text
        eAnioIni = Me.ComboBoxAnioIni.SelectedValue
        eMesFin = Me.ComboBoxMesFin.SelectedValue
        eStrMesFin = Me.ComboBoxMesFin.Text
        eAnioFin = Me.ComboBoxAnioFin.SelectedValue

        If (eMesIni > eMesFin And eAnioIni = eAnioFin) Or (eAnioIni > eAnioFin) Then
            MsgBox("La fecha inicial no puede ser mayor a la fecha final.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        FrmSelServicioE.Show()
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
End Class