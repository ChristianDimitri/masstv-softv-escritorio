﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSeleccionaColoniaImpresionTarjetas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lbColoniasASeleccionar = New System.Windows.Forms.ListBox()
        Me.lbColoniasSeleccionadas = New System.Windows.Forms.ListBox()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btSeleccionarUnaColonia = New System.Windows.Forms.Button()
        Me.btnSeleccionarTodasColonias = New System.Windows.Forms.Button()
        Me.btnDeseleccionarUnaColonia = New System.Windows.Forms.Button()
        Me.btnDeseleccionarTodasColonias = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lbColoniasASeleccionar
        '
        Me.lbColoniasASeleccionar.DisplayMember = "NOMBRE"
        Me.lbColoniasASeleccionar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbColoniasASeleccionar.FormattingEnabled = True
        Me.lbColoniasASeleccionar.ItemHeight = 15
        Me.lbColoniasASeleccionar.Location = New System.Drawing.Point(15, 56)
        Me.lbColoniasASeleccionar.Name = "lbColoniasASeleccionar"
        Me.lbColoniasASeleccionar.Size = New System.Drawing.Size(225, 319)
        Me.lbColoniasASeleccionar.TabIndex = 0
        Me.lbColoniasASeleccionar.ValueMember = "CLVCOLONIA"
        '
        'lbColoniasSeleccionadas
        '
        Me.lbColoniasSeleccionadas.DisplayMember = "NOMBRE"
        Me.lbColoniasSeleccionadas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbColoniasSeleccionadas.FormattingEnabled = True
        Me.lbColoniasSeleccionadas.ItemHeight = 15
        Me.lbColoniasSeleccionadas.Location = New System.Drawing.Point(292, 56)
        Me.lbColoniasSeleccionadas.Name = "lbColoniasSeleccionadas"
        Me.lbColoniasSeleccionadas.Size = New System.Drawing.Size(244, 319)
        Me.lbColoniasSeleccionadas.TabIndex = 1
        Me.lbColoniasSeleccionadas.ValueMember = "CLVCOLONIA"
        '
        'btnAceptar
        '
        Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.Location = New System.Drawing.Point(296, 382)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(117, 33)
        Me.btnAceptar.TabIndex = 2
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(419, 383)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(117, 33)
        Me.btnCancelar.TabIndex = 3
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btSeleccionarUnaColonia
        '
        Me.btSeleccionarUnaColonia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btSeleccionarUnaColonia.Location = New System.Drawing.Point(246, 129)
        Me.btSeleccionarUnaColonia.Name = "btSeleccionarUnaColonia"
        Me.btSeleccionarUnaColonia.Size = New System.Drawing.Size(40, 22)
        Me.btSeleccionarUnaColonia.TabIndex = 4
        Me.btSeleccionarUnaColonia.Text = ">"
        Me.btSeleccionarUnaColonia.UseVisualStyleBackColor = True
        '
        'btnSeleccionarTodasColonias
        '
        Me.btnSeleccionarTodasColonias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSeleccionarTodasColonias.Location = New System.Drawing.Point(246, 157)
        Me.btnSeleccionarTodasColonias.Name = "btnSeleccionarTodasColonias"
        Me.btnSeleccionarTodasColonias.Size = New System.Drawing.Size(40, 22)
        Me.btnSeleccionarTodasColonias.TabIndex = 5
        Me.btnSeleccionarTodasColonias.Text = ">>"
        Me.btnSeleccionarTodasColonias.UseVisualStyleBackColor = True
        '
        'btnDeseleccionarUnaColonia
        '
        Me.btnDeseleccionarUnaColonia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeseleccionarUnaColonia.Location = New System.Drawing.Point(246, 295)
        Me.btnDeseleccionarUnaColonia.Name = "btnDeseleccionarUnaColonia"
        Me.btnDeseleccionarUnaColonia.Size = New System.Drawing.Size(40, 22)
        Me.btnDeseleccionarUnaColonia.TabIndex = 6
        Me.btnDeseleccionarUnaColonia.Text = "<"
        Me.btnDeseleccionarUnaColonia.UseVisualStyleBackColor = True
        '
        'btnDeseleccionarTodasColonias
        '
        Me.btnDeseleccionarTodasColonias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeseleccionarTodasColonias.Location = New System.Drawing.Point(246, 267)
        Me.btnDeseleccionarTodasColonias.Name = "btnDeseleccionarTodasColonias"
        Me.btnDeseleccionarTodasColonias.Size = New System.Drawing.Size(40, 22)
        Me.btnDeseleccionarTodasColonias.TabIndex = 7
        Me.btnDeseleccionarTodasColonias.Text = "<<"
        Me.btnDeseleccionarTodasColonias.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 27)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(155, 15)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Colonias a Seleccionar"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(289, 27)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(161, 15)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Colonias Seleccionadas"
        '
        'frmSeleccionaColoniaImpresionTarjetas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(547, 428)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnDeseleccionarTodasColonias)
        Me.Controls.Add(Me.btnDeseleccionarUnaColonia)
        Me.Controls.Add(Me.btnSeleccionarTodasColonias)
        Me.Controls.Add(Me.btSeleccionarUnaColonia)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.lbColoniasSeleccionadas)
        Me.Controls.Add(Me.lbColoniasASeleccionar)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSeleccionaColoniaImpresionTarjetas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selección de Colonias Para Impresión de Tarjetas"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lbColoniasASeleccionar As System.Windows.Forms.ListBox
    Friend WithEvents lbColoniasSeleccionadas As System.Windows.Forms.ListBox
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btSeleccionarUnaColonia As System.Windows.Forms.Button
    Friend WithEvents btnSeleccionarTodasColonias As System.Windows.Forms.Button
    Friend WithEvents btnDeseleccionarUnaColonia As System.Windows.Forms.Button
    Friend WithEvents btnDeseleccionarTodasColonias As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
