﻿Imports System.Data.SqlClient
Public Class FrmTrabajos
    Dim clave As String = Nothing
    Dim Descripcion As String = Nothing
    Dim Puntos As String = Nothing
    Dim TipoServicio As String = Nothing
    Dim Secobradescarga As String = Nothing
    Dim SICA As String = Nothing
    Dim Cobra As Integer = 0
    Private Sub DameDatosBitacora()
        Try
            If opcion = "M" Then
                clave = Me.TRABAJOTextBox.Text
                Descripcion = Me.DESCRIPCIONTextBox.Text
                Puntos = Me.PUNTOSTextBox.Text
                If Me.RadioButton1.Checked = True Then
                    TipoServicio = "S"
                Else
                    TipoServicio = "Q"
                End If
                If IsNumeric(Me.Se_cobraTextBox.Text) = True Then
                    If CLng(Me.Se_cobraTextBox.Text) = 1 Then
                        Secobradescarga = "True"
                    Else
                        Secobradescarga = "False"
                    End If
                Else
                    Secobradescarga = "False"
                End If
                If Me.SICACheckBox.CheckState = CheckState.Checked Then
                    SICA = "True"
                Else
                    SICA = "False"
                End If
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub guardadatosbitacora(ByVal op As Integer)
        Try
            Dim validacion1 As String = Nothing
            Dim validacion2 As String = Nothing
            Dim Validacion3 As String = Nothing

            Select Case op
                Case 0
                    If opcion = "N" Then
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Generó Un Nuevo Trabajo", "", "Se Generó Un Nuevo Trabajo: " + Me.TRABAJOTextBox.Text, LocClv_Ciudad)
                    ElseIf opcion = "M" Then

                        'clave = Me.TRABAJOTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.TRABAJOTextBox.Name + ": " + Me.TRABAJOTextBox.Text, clave, Me.TRABAJOTextBox.Text, LocClv_Ciudad)
                        'Descripcion = Me.DESCRIPCIONTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.DESCRIPCIONTextBox.Name + ": " + Me.TRABAJOTextBox.Text, Descripcion, Me.DESCRIPCIONTextBox.Text, LocClv_Ciudad)
                        'Puntos = Me.PUNTOSTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.PUNTOSTextBox.Name + ": " + Me.TRABAJOTextBox.Text, Puntos, Me.PUNTOSTextBox.Text, LocClv_Ciudad)
                        'Tipo de Servicio
                        If Me.RadioButton1.Checked = True Then
                            validacion1 = "S"
                        Else
                            validacion1 = "Q"
                        End If
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "TipoServicio" + ": " + Me.TRABAJOTextBox.Text, validacion1, TipoServicio, LocClv_Ciudad)
                        'Se cobra
                        If Me.Se_cobraTextBox.Text.Length = 0 Then
                            Me.Se_cobraTextBox.Text = 0
                        End If

                        If CLng(Me.Se_cobraTextBox.Text) = 1 Then
                            'Secobradescarga = "True"
                            validacion2 = "True"
                        Else
                            'Secobradescarga = "False"
                            validacion2 = "False"
                        End If
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Secobradecarga" + ": " + Me.TRABAJOTextBox.Text, Secobradescarga, validacion2, LocClv_Ciudad)
                        'SICA
                        If Me.SICACheckBox.CheckState = CheckState.Checked Then
                            '    SICA = "True"
                            Validacion3 = "True"
                        Else
                            '   SICA = "False"
                            Validacion3 = "False"
                        End If
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "SICA" + ": " + Me.TRABAJOTextBox.Text, SICA, Validacion3, LocClv_Ciudad)
                    End If
                Case 1
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Elimino Un Trabajo" + ": " + Me.TRABAJOTextBox.Text, "", "Se Elimino Un Trabajo: " + Me.TRABAJOTextBox.Text, LocClv_Ciudad)
            End Select

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub CONTRABAJOSBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONTRABAJOSBindingNavigatorSaveItem.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If RadioButton1.Checked = True Then
                Me.TipoTextBox.Text = "S"
            Else
                Me.TipoTextBox.Text = "Q"
            End If
            Me.Validate()
            Me.CONTRABAJOSBindingSource.EndEdit()
            Me.CONTRABAJOSTableAdapter.Connection = CON
            Me.CONTRABAJOSTableAdapter.Update(Me.NewSofTvDataSet.CONTRABAJOS)
            Me.BORRelacion_Trabajos_ServiciosTableAdapter.Connection = CON
            Me.BORRelacion_Trabajos_ServiciosTableAdapter.Fill(Me.NewSofTvDataSet.BORRelacion_Trabajos_Servicios, New System.Nullable(Of Integer)(CType(Me.Clv_TrabajoTextBox.Text, Integer)))
            Me.NUERelacion_Trabajos_ServiciosTableAdapter.Connection = CON
            Me.NUERelacion_Trabajos_ServiciosTableAdapter.Fill(Me.NewSofTvDataSet.NUERelacion_Trabajos_Servicios, New System.Nullable(Of Integer)(CType(Me.Clv_TrabajoTextBox.Text, Integer)), New System.Nullable(Of Integer)(CType(Me.ComboBox1.SelectedValue, Integer)))
            Me.Inserta_Rel_Trabajo_Cobro_DescTableAdapter.Connection = CON
            Me.Inserta_Rel_Trabajo_Cobro_DescTableAdapter.Fill(Me.ProcedimientosArnoldo2.Inserta_Rel_Trabajo_Cobro_Desc, CLng(Clv_TrabajoTextBox.Text), Cobra)

            If tbPuntosCuadrilla.Text.Length = 0 Then tbPuntosCuadrilla.Text = "0"
            If IsNumeric(tbPuntosCuadrilla.Text) = False Then tbPuntosCuadrilla.Text = "0"
            NueRelTrabajosCuadrilla(Clv_TrabajoTextBox.Text, tbPuntosCuadrilla.Text)

            guardadatosbitacora(0)
            MsgBox(mensaje5)
            GloBnd = True
            CON.Close()
            Me.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show("La clave que Tecleo ya Exíste")
        End Try
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Me.CONTRABAJOSBindingSource.CancelEdit()
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        bitsist(GloUsuario, 0, LocGloSistema, "Catálogo de Trabajos", "", "Eliminó Trabajo:" + CStr(Me.Clv_TrabajoTextBox.Text), GloSucursal, LocClv_Ciudad)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.CONTRABAJOSTableAdapter.Connection = CON
        Me.CONTRABAJOSTableAdapter.Delete(gloClave)
        Me.BORRelacion_Trabajos_ServiciosTableAdapter.Connection = CON
        Me.BORRelacion_Trabajos_ServiciosTableAdapter.Fill(Me.NewSofTvDataSet.BORRelacion_Trabajos_Servicios, New System.Nullable(Of Integer)(CType(Me.Clv_TrabajoTextBox.Text, Integer)))
        guardadatosbitacora(1)
        GloBnd = True
        CON.Close()
        Me.Close()
    End Sub


    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        If RadioButton1.Checked = True Then
            Me.TipoTextBox.Text = "S"
        Else
            Me.TipoTextBox.Text = "Q"
        End If
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        If RadioButton2.Checked = True Then
            Me.TipoTextBox.Text = "Q"
        Else
            Me.TipoTextBox.Text = "S"
        End If
    End Sub

    Private Sub BUSCA(ByVal CLAVE As Integer)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.CONTRABAJOSTableAdapter.Connection = CON
            Me.CONTRABAJOSTableAdapter.Fill(Me.NewSofTvDataSet.CONTRABAJOS, New System.Nullable(Of Integer)(CType(CLAVE, Integer)))
            Me.Consulta_Rel_Trabajo_Cobro_DescTableAdapter.Connection = CON
            Me.Consulta_Rel_Trabajo_Cobro_DescTableAdapter.Fill(Me.ProcedimientosArnoldo2.consulta_Rel_Trabajo_Cobro_Desc, CLng(CLAVE))
            DameDatosBitacora()
            ConRelTrabajosCuadrilla(CLAVE)
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub FrmTrabajos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If IdSistema = "AG" Or IdSistema = "VA" Then
            Me.CMBLabel2.Visible = False
            Me.CheckBox1.Visible = False
        End If
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If opcion = "N" Then
            Me.CONTRABAJOSBindingSource.AddNew()
            Panel1.Enabled = True
        ElseIf opcion = "C" Then
            Me.TipoTextBox.Visible = False
            Panel1.Enabled = False
            BUSCA(gloClave)
        ElseIf opcion = "M" Then
            Panel1.Enabled = True
            BUSCA(gloClave)

        End If
        Me.Clv_TipSerTextBox.Text = GloClv_TipSer
        Me.MuestraServiciosTODOSTableAdapter.Connection = CON
        Me.MuestraServiciosTODOSTableAdapter.Fill(Me.NewSofTvDataSet.MuestraServiciosTODOS, GloClv_TipSer)
        Me.MUESTRARelacion_Trabajos_ServiciosTableAdapter.Connection = CON
        Me.MUESTRARelacion_Trabajos_ServiciosTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRARelacion_Trabajos_Servicios, Me.Clv_TrabajoTextBox.Text)
        CON.Close()
    End Sub

    Private Sub Clv_TrabajoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_TrabajoTextBox.TextChanged
        gloClave = Me.Clv_TrabajoTextBox.Text
    End Sub

    Private Sub TipoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TipoTextBox.TextChanged
        If Me.TipoTextBox.Text = "S" Then
            If Me.RadioButton1.Checked = False Then Me.RadioButton1.Checked = True
        Else
            If Me.RadioButton2.Checked = False Then Me.RadioButton2.Checked = True
        End If
    End Sub

    Private Sub CobranzaCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CobranzaCheckBox.CheckedChanged
        If Me.CobranzaCheckBox.Checked = True Then
            Me.Panel3.Visible = False
        Else
            Me.Panel3.Visible = False
        End If
    End Sub

    Private Sub DESCRIPCIONTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles DESCRIPCIONTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(DESCRIPCIONTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub


    Private Sub DESCRIPCIONTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DESCRIPCIONTextBox.TextChanged

    End Sub

    Private Sub Se_cobraTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Se_cobraTextBox.TextChanged
        If IsNumeric(Me.Se_cobraTextBox.Text) = True Then
            If CLng(Me.Se_cobraTextBox.Text) = 1 Then
                Me.CheckBox1.Checked = True
            Else
                Me.CheckBox1.Checked = False
            End If
        End If
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If Me.CheckBox1.Checked = True Then
            Cobra = 1
        ElseIf Me.CheckBox1.Checked = False Then
            Cobra = 0
        End If
    End Sub

    Private Sub ConRelTrabajosCuadrilla(ByVal Clv_Trabajo As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ConRelTrabajosCuadrilla", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0
        Dim reader As SqlDataReader

        Dim par1 As New SqlParameter("@Clv_Trabajo", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = Clv_Trabajo
        comando.Parameters.Add(par1)

        Try
            conexion.Open()
            reader = comando.ExecuteReader()
            While (reader.Read())
                tbPuntosCuadrilla.Text = reader(0).ToString()
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try


    End Sub


    Private Sub NueRelTrabajosCuadrilla(ByVal Clv_Trabajo As Integer, ByVal PuntosCuadrilla As Decimal)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueRelTrabajosCuadrilla", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Clv_Trabajo", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = Clv_Trabajo
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@PuntosCuadrilla", SqlDbType.Decimal)
        par2.Direction = ParameterDirection.Input
        par2.Value = PuntosCuadrilla
        comando.Parameters.Add(par2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

End Class