﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTrabajos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_TrabajoLabel As System.Windows.Forms.Label
        Dim TRABAJOLabel As System.Windows.Forms.Label
        Dim Clv_TipSerLabel As System.Windows.Forms.Label
        Dim DESCRIPCIONLabel As System.Windows.Forms.Label
        Dim PUNTOSLabel As System.Windows.Forms.Label
        Dim CMBCobranzaLabel As System.Windows.Forms.Label
        Dim TipoLabel As System.Windows.Forms.Label
        Dim CMBSICALabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim CLV_TRABAJOLabel1 As System.Windows.Forms.Label
        Dim Se_cobraLabel As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmTrabajos))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.tbPuntosCuadrilla = New System.Windows.Forms.TextBox()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.DESCRIPCIONTextBox = New System.Windows.Forms.TextBox()
        Me.CONTRABAJOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.CLV_TRABAJOTextBox1 = New System.Windows.Forms.TextBox()
        Me.MUESTRARelacionTrabajosServiciosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.MuestraServiciosTODOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.CONTRABAJOSBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.CONTRABAJOSBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Clv_TrabajoTextBox = New System.Windows.Forms.TextBox()
        Me.TRABAJOTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_TipSerTextBox = New System.Windows.Forms.TextBox()
        Me.PUNTOSTextBox = New System.Windows.Forms.TextBox()
        Me.CobranzaCheckBox = New System.Windows.Forms.CheckBox()
        Me.TipoTextBox = New System.Windows.Forms.TextBox()
        Me.SICACheckBox = New System.Windows.Forms.CheckBox()
        Me.MuestraServiciosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button5 = New System.Windows.Forms.Button()
        Me.CONTRABAJOSTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONTRABAJOSTableAdapter()
        Me.MuestraServiciosTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MuestraServiciosTableAdapter()
        Me.MUESTRARelacion_Trabajos_ServiciosTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRARelacion_Trabajos_ServiciosTableAdapter()
        Me.MuestraServiciosTODOSTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MuestraServiciosTODOSTableAdapter()
        Me.NUERelacion_Trabajos_ServiciosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NUERelacion_Trabajos_ServiciosTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.NUERelacion_Trabajos_ServiciosTableAdapter()
        Me.BORRelacion_Trabajos_ServiciosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BORRelacion_Trabajos_ServiciosTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.BORRelacion_Trabajos_ServiciosTableAdapter()
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2()
        Me.Inserta_Rel_Trabajo_Cobro_DescBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Rel_Trabajo_Cobro_DescTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_Rel_Trabajo_Cobro_DescTableAdapter()
        Me.Consulta_Rel_Trabajo_Cobro_DescBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Consulta_Rel_Trabajo_Cobro_DescTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.consulta_Rel_Trabajo_Cobro_DescTableAdapter()
        Me.Se_cobraTextBox = New System.Windows.Forms.TextBox()
        Clv_TrabajoLabel = New System.Windows.Forms.Label()
        TRABAJOLabel = New System.Windows.Forms.Label()
        Clv_TipSerLabel = New System.Windows.Forms.Label()
        DESCRIPCIONLabel = New System.Windows.Forms.Label()
        PUNTOSLabel = New System.Windows.Forms.Label()
        CMBCobranzaLabel = New System.Windows.Forms.Label()
        TipoLabel = New System.Windows.Forms.Label()
        CMBSICALabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        CLV_TRABAJOLabel1 = New System.Windows.Forms.Label()
        Se_cobraLabel = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.CONTRABAJOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.MUESTRARelacionTrabajosServiciosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraServiciosTODOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.CONTRABAJOSBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONTRABAJOSBindingNavigator.SuspendLayout()
        CType(Me.MuestraServiciosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NUERelacion_Trabajos_ServiciosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BORRelacion_Trabajos_ServiciosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Rel_Trabajo_Cobro_DescBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_Rel_Trabajo_Cobro_DescBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Clv_TrabajoLabel
        '
        Clv_TrabajoLabel.AutoSize = True
        Clv_TrabajoLabel.BackColor = System.Drawing.Color.WhiteSmoke
        Clv_TrabajoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_TrabajoLabel.ForeColor = System.Drawing.Color.WhiteSmoke
        Clv_TrabajoLabel.Location = New System.Drawing.Point(261, 90)
        Clv_TrabajoLabel.Name = "Clv_TrabajoLabel"
        Clv_TrabajoLabel.Size = New System.Drawing.Size(50, 15)
        Clv_TrabajoLabel.TabIndex = 0
        Clv_TrabajoLabel.Text = "Clave :"
        '
        'TRABAJOLabel
        '
        TRABAJOLabel.AutoSize = True
        TRABAJOLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TRABAJOLabel.ForeColor = System.Drawing.Color.LightSlateGray
        TRABAJOLabel.Location = New System.Drawing.Point(135, 42)
        TRABAJOLabel.Name = "TRABAJOLabel"
        TRABAJOLabel.Size = New System.Drawing.Size(50, 15)
        TRABAJOLabel.TabIndex = 2
        TRABAJOLabel.Text = "Clave :"
        '
        'Clv_TipSerLabel
        '
        Clv_TipSerLabel.AutoSize = True
        Clv_TipSerLabel.BackColor = System.Drawing.Color.WhiteSmoke
        Clv_TipSerLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_TipSerLabel.ForeColor = System.Drawing.Color.WhiteSmoke
        Clv_TipSerLabel.Location = New System.Drawing.Point(211, 69)
        Clv_TipSerLabel.Name = "Clv_TipSerLabel"
        Clv_TipSerLabel.Size = New System.Drawing.Size(121, 15)
        Clv_TipSerLabel.TabIndex = 4
        Clv_TipSerLabel.Text = "Clv Tipo Servicio :"
        '
        'DESCRIPCIONLabel
        '
        DESCRIPCIONLabel.AutoSize = True
        DESCRIPCIONLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DESCRIPCIONLabel.ForeColor = System.Drawing.Color.LightSlateGray
        DESCRIPCIONLabel.Location = New System.Drawing.Point(98, 69)
        DESCRIPCIONLabel.Name = "DESCRIPCIONLabel"
        DESCRIPCIONLabel.Size = New System.Drawing.Size(91, 15)
        DESCRIPCIONLabel.TabIndex = 6
        DESCRIPCIONLabel.Text = "Descripción :"
        '
        'PUNTOSLabel
        '
        PUNTOSLabel.AutoSize = True
        PUNTOSLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PUNTOSLabel.ForeColor = System.Drawing.Color.LightSlateGray
        PUNTOSLabel.Location = New System.Drawing.Point(108, 119)
        PUNTOSLabel.Name = "PUNTOSLabel"
        PUNTOSLabel.Size = New System.Drawing.Size(77, 15)
        PUNTOSLabel.TabIndex = 8
        PUNTOSLabel.Text = "Individual :"
        '
        'CMBCobranzaLabel
        '
        CMBCobranzaLabel.AutoSize = True
        CMBCobranzaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBCobranzaLabel.ForeColor = System.Drawing.Color.DarkOrange
        CMBCobranzaLabel.Location = New System.Drawing.Point(162, 281)
        CMBCobranzaLabel.Name = "CMBCobranzaLabel"
        CMBCobranzaLabel.Size = New System.Drawing.Size(356, 16)
        CMBCobranzaLabel.TabIndex = 10
        CMBCobranzaLabel.Text = "Se Relacciona con Algún Concepto de  Cobranza :"
        CMBCobranzaLabel.Visible = False
        '
        'TipoLabel
        '
        TipoLabel.AutoSize = True
        TipoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        TipoLabel.Location = New System.Drawing.Point(3, 0)
        TipoLabel.Name = "TipoLabel"
        TipoLabel.Size = New System.Drawing.Size(51, 18)
        TipoLabel.TabIndex = 12
        TipoLabel.Text = "Tipo :"
        '
        'CMBSICALabel
        '
        CMBSICALabel.AutoSize = True
        CMBSICALabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBSICALabel.ForeColor = System.Drawing.Color.DarkOrange
        CMBSICALabel.Location = New System.Drawing.Point(77, 251)
        CMBSICALabel.Name = "CMBSICALabel"
        CMBSICALabel.Size = New System.Drawing.Size(587, 16)
        CMBSICALabel.TabIndex = 16
        CMBSICALabel.Text = "Se Relacciona con el Proceso Automatico de Cablemodems ó Aparatos Digitales   :"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(16, 9)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(164, 15)
        Label1.TabIndex = 7
        Label1.Text = "Concepto de Cobranza  :"
        '
        'CLV_TRABAJOLabel1
        '
        CLV_TRABAJOLabel1.AutoSize = True
        CLV_TRABAJOLabel1.BackColor = System.Drawing.Color.WhiteSmoke
        CLV_TRABAJOLabel1.Location = New System.Drawing.Point(23, 34)
        CLV_TRABAJOLabel1.Name = "CLV_TRABAJOLabel1"
        CLV_TRABAJOLabel1.Size = New System.Drawing.Size(82, 13)
        CLV_TRABAJOLabel1.TabIndex = 7
        CLV_TRABAJOLabel1.Text = "CLV TRABAJO:"
        CLV_TRABAJOLabel1.Visible = False
        '
        'Se_cobraLabel
        '
        Se_cobraLabel.AutoSize = True
        Se_cobraLabel.Location = New System.Drawing.Point(305, 511)
        Se_cobraLabel.Name = "Se_cobraLabel"
        Se_cobraLabel.Size = New System.Drawing.Size(51, 13)
        Se_cobraLabel.TabIndex = 151
        Se_cobraLabel.Text = "se cobra:"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(112, 146)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(73, 15)
        Label2.TabIndex = 135
        Label2.Text = "Cuadrilla :"
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Label3.Location = New System.Drawing.Point(121, 95)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(51, 15)
        Label3.TabIndex = 136
        Label3.Text = "Puntos"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Label3)
        Me.Panel1.Controls.Add(Label2)
        Me.Panel1.Controls.Add(Me.tbPuntosCuadrilla)
        Me.Panel1.Controls.Add(Me.CMBLabel2)
        Me.Panel1.Controls.Add(Me.CheckBox1)
        Me.Panel1.Controls.Add(Me.DESCRIPCIONTextBox)
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.CONTRABAJOSBindingNavigator)
        Me.Panel1.Controls.Add(Clv_TrabajoLabel)
        Me.Panel1.Controls.Add(Me.Clv_TrabajoTextBox)
        Me.Panel1.Controls.Add(TRABAJOLabel)
        Me.Panel1.Controls.Add(Me.TRABAJOTextBox)
        Me.Panel1.Controls.Add(Clv_TipSerLabel)
        Me.Panel1.Controls.Add(Me.Clv_TipSerTextBox)
        Me.Panel1.Controls.Add(DESCRIPCIONLabel)
        Me.Panel1.Controls.Add(PUNTOSLabel)
        Me.Panel1.Controls.Add(Me.PUNTOSTextBox)
        Me.Panel1.Controls.Add(CMBCobranzaLabel)
        Me.Panel1.Controls.Add(Me.CobranzaCheckBox)
        Me.Panel1.Controls.Add(Me.TipoTextBox)
        Me.Panel1.Controls.Add(CMBSICALabel)
        Me.Panel1.Controls.Add(Me.SICACheckBox)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(770, 382)
        Me.Panel1.TabIndex = 0
        '
        'tbPuntosCuadrilla
        '
        Me.tbPuntosCuadrilla.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbPuntosCuadrilla.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbPuntosCuadrilla.Location = New System.Drawing.Point(190, 140)
        Me.tbPuntosCuadrilla.Name = "tbPuntosCuadrilla"
        Me.tbPuntosCuadrilla.Size = New System.Drawing.Size(104, 21)
        Me.tbPuntosCuadrilla.TabIndex = 134
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.CMBLabel2.Location = New System.Drawing.Point(498, 210)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(250, 16)
        Me.CMBLabel2.TabIndex = 133
        Me.CMBLabel2.Text = "Se Cobra La Descarga De Material"
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.CheckBox1.ForeColor = System.Drawing.Color.Black
        Me.CheckBox1.Location = New System.Drawing.Point(477, 210)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox1.TabIndex = 132
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'DESCRIPCIONTextBox
        '
        Me.DESCRIPCIONTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DESCRIPCIONTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONTRABAJOSBindingSource, "DESCRIPCION", True))
        Me.DESCRIPCIONTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DESCRIPCIONTextBox.Location = New System.Drawing.Point(191, 66)
        Me.DESCRIPCIONTextBox.Name = "DESCRIPCIONTextBox"
        Me.DESCRIPCIONTextBox.Size = New System.Drawing.Size(459, 21)
        Me.DESCRIPCIONTextBox.TabIndex = 0
        '
        'CONTRABAJOSBindingSource
        '
        Me.CONTRABAJOSBindingSource.DataMember = "CONTRABAJOS"
        Me.CONTRABAJOSBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(CLV_TRABAJOLabel1)
        Me.Panel3.Controls.Add(Me.CLV_TRABAJOTextBox1)
        Me.Panel3.Controls.Add(Label1)
        Me.Panel3.Controls.Add(Me.ComboBox1)
        Me.Panel3.Location = New System.Drawing.Point(138, 311)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(609, 71)
        Me.Panel3.TabIndex = 131
        Me.Panel3.Visible = False
        '
        'CLV_TRABAJOTextBox1
        '
        Me.CLV_TRABAJOTextBox1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CLV_TRABAJOTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MUESTRARelacionTrabajosServiciosBindingSource, "CLV_TRABAJO", True))
        Me.CLV_TRABAJOTextBox1.Location = New System.Drawing.Point(111, 31)
        Me.CLV_TRABAJOTextBox1.Name = "CLV_TRABAJOTextBox1"
        Me.CLV_TRABAJOTextBox1.Size = New System.Drawing.Size(100, 20)
        Me.CLV_TRABAJOTextBox1.TabIndex = 8
        Me.CLV_TRABAJOTextBox1.TabStop = False
        Me.CLV_TRABAJOTextBox1.Visible = False
        '
        'MUESTRARelacionTrabajosServiciosBindingSource
        '
        Me.MUESTRARelacionTrabajosServiciosBindingSource.DataMember = "MUESTRARelacion_Trabajos_Servicios"
        Me.MUESTRARelacionTrabajosServiciosBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'ComboBox1
        '
        Me.ComboBox1.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.MUESTRARelacionTrabajosServiciosBindingSource, "CLV_SERVICIO", True))
        Me.ComboBox1.DataSource = Me.MuestraServiciosTODOSBindingSource
        Me.ComboBox1.DisplayMember = "Descripcion"
        Me.ComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(193, 9)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(294, 23)
        Me.ComboBox1.TabIndex = 6
        Me.ComboBox1.TabStop = False
        Me.ComboBox1.ValueMember = "Clv_Servicio"
        '
        'MuestraServiciosTODOSBindingSource
        '
        Me.MuestraServiciosTODOSBindingSource.DataMember = "MuestraServiciosTODOS"
        Me.MuestraServiciosTODOSBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Wheat
        Me.Panel2.Controls.Add(Me.RadioButton2)
        Me.Panel2.Controls.Add(Me.RadioButton1)
        Me.Panel2.Controls.Add(TipoLabel)
        Me.Panel2.ForeColor = System.Drawing.Color.Silver
        Me.Panel2.Location = New System.Drawing.Point(191, 180)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(230, 59)
        Me.Panel2.TabIndex = 19
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton2.ForeColor = System.Drawing.Color.OrangeRed
        Me.RadioButton2.Location = New System.Drawing.Point(140, 23)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(73, 24)
        Me.RadioButton2.TabIndex = 20
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "Queja"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Checked = True
        Me.RadioButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton1.ForeColor = System.Drawing.Color.OrangeRed
        Me.RadioButton1.Location = New System.Drawing.Point(14, 23)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(89, 24)
        Me.RadioButton1.TabIndex = 10
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Servicio"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'CONTRABAJOSBindingNavigator
        '
        Me.CONTRABAJOSBindingNavigator.AddNewItem = Nothing
        Me.CONTRABAJOSBindingNavigator.BindingSource = Me.CONTRABAJOSBindingSource
        Me.CONTRABAJOSBindingNavigator.CountItem = Nothing
        Me.CONTRABAJOSBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONTRABAJOSBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.BindingNavigatorDeleteItem, Me.CONTRABAJOSBindingNavigatorSaveItem})
        Me.CONTRABAJOSBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONTRABAJOSBindingNavigator.MoveFirstItem = Nothing
        Me.CONTRABAJOSBindingNavigator.MoveLastItem = Nothing
        Me.CONTRABAJOSBindingNavigator.MoveNextItem = Nothing
        Me.CONTRABAJOSBindingNavigator.MovePreviousItem = Nothing
        Me.CONTRABAJOSBindingNavigator.Name = "CONTRABAJOSBindingNavigator"
        Me.CONTRABAJOSBindingNavigator.PositionItem = Nothing
        Me.CONTRABAJOSBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONTRABAJOSBindingNavigator.Size = New System.Drawing.Size(770, 25)
        Me.CONTRABAJOSBindingNavigator.TabIndex = 100
        Me.CONTRABAJOSBindingNavigator.TabStop = True
        Me.CONTRABAJOSBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(90, 22)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(79, 22)
        Me.ToolStripButton1.Text = "&CANCELAR"
        '
        'CONTRABAJOSBindingNavigatorSaveItem
        '
        Me.CONTRABAJOSBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONTRABAJOSBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONTRABAJOSBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONTRABAJOSBindingNavigatorSaveItem.Name = "CONTRABAJOSBindingNavigatorSaveItem"
        Me.CONTRABAJOSBindingNavigatorSaveItem.Size = New System.Drawing.Size(91, 22)
        Me.CONTRABAJOSBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'Clv_TrabajoTextBox
        '
        Me.Clv_TrabajoTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TrabajoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_TrabajoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONTRABAJOSBindingSource, "Clv_Trabajo", True))
        Me.Clv_TrabajoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_TrabajoTextBox.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TrabajoTextBox.Location = New System.Drawing.Point(317, 126)
        Me.Clv_TrabajoTextBox.Name = "Clv_TrabajoTextBox"
        Me.Clv_TrabajoTextBox.Size = New System.Drawing.Size(104, 14)
        Me.Clv_TrabajoTextBox.TabIndex = 100
        Me.Clv_TrabajoTextBox.TabStop = False
        '
        'TRABAJOTextBox
        '
        Me.TRABAJOTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TRABAJOTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TRABAJOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONTRABAJOSBindingSource, "TRABAJO", True))
        Me.TRABAJOTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TRABAJOTextBox.Location = New System.Drawing.Point(191, 36)
        Me.TRABAJOTextBox.MaxLength = 6
        Me.TRABAJOTextBox.Name = "TRABAJOTextBox"
        Me.TRABAJOTextBox.Size = New System.Drawing.Size(104, 21)
        Me.TRABAJOTextBox.TabIndex = 1
        Me.TRABAJOTextBox.TabStop = False
        '
        'Clv_TipSerTextBox
        '
        Me.Clv_TipSerTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TipSerTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_TipSerTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONTRABAJOSBindingSource, "Clv_TipSer", True))
        Me.Clv_TipSerTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_TipSerTextBox.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TipSerTextBox.Location = New System.Drawing.Point(414, 126)
        Me.Clv_TipSerTextBox.Name = "Clv_TipSerTextBox"
        Me.Clv_TipSerTextBox.Size = New System.Drawing.Size(104, 14)
        Me.Clv_TipSerTextBox.TabIndex = 50
        Me.Clv_TipSerTextBox.TabStop = False
        '
        'PUNTOSTextBox
        '
        Me.PUNTOSTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PUNTOSTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONTRABAJOSBindingSource, "PUNTOS", True))
        Me.PUNTOSTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PUNTOSTextBox.Location = New System.Drawing.Point(191, 113)
        Me.PUNTOSTextBox.Name = "PUNTOSTextBox"
        Me.PUNTOSTextBox.Size = New System.Drawing.Size(104, 21)
        Me.PUNTOSTextBox.TabIndex = 1
        '
        'CobranzaCheckBox
        '
        Me.CobranzaCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONTRABAJOSBindingSource, "Cobranza", True))
        Me.CobranzaCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CobranzaCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CobranzaCheckBox.Location = New System.Drawing.Point(524, 281)
        Me.CobranzaCheckBox.Name = "CobranzaCheckBox"
        Me.CobranzaCheckBox.Size = New System.Drawing.Size(104, 24)
        Me.CobranzaCheckBox.TabIndex = 5
        Me.CobranzaCheckBox.TabStop = False
        Me.CobranzaCheckBox.Visible = False
        '
        'TipoTextBox
        '
        Me.TipoTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TipoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TipoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONTRABAJOSBindingSource, "Tipo", True))
        Me.TipoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TipoTextBox.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.TipoTextBox.Location = New System.Drawing.Point(495, 180)
        Me.TipoTextBox.Name = "TipoTextBox"
        Me.TipoTextBox.Size = New System.Drawing.Size(23, 14)
        Me.TipoTextBox.TabIndex = 130
        Me.TipoTextBox.TabStop = False
        Me.TipoTextBox.Text = "S"
        '
        'SICACheckBox
        '
        Me.SICACheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONTRABAJOSBindingSource, "SICA", True))
        Me.SICACheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.SICACheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SICACheckBox.Location = New System.Drawing.Point(670, 251)
        Me.SICACheckBox.Name = "SICACheckBox"
        Me.SICACheckBox.Size = New System.Drawing.Size(77, 24)
        Me.SICACheckBox.TabIndex = 50
        '
        'MuestraServiciosBindingSource
        '
        Me.MuestraServiciosBindingSource.DataMember = "MuestraServicios"
        Me.MuestraServiciosBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(646, 406)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 150
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'CONTRABAJOSTableAdapter
        '
        Me.CONTRABAJOSTableAdapter.ClearBeforeFill = True
        '
        'MuestraServiciosTableAdapter
        '
        Me.MuestraServiciosTableAdapter.ClearBeforeFill = True
        '
        'MUESTRARelacion_Trabajos_ServiciosTableAdapter
        '
        Me.MUESTRARelacion_Trabajos_ServiciosTableAdapter.ClearBeforeFill = True
        '
        'MuestraServiciosTODOSTableAdapter
        '
        Me.MuestraServiciosTODOSTableAdapter.ClearBeforeFill = True
        '
        'NUERelacion_Trabajos_ServiciosBindingSource
        '
        Me.NUERelacion_Trabajos_ServiciosBindingSource.DataMember = "NUERelacion_Trabajos_Servicios"
        Me.NUERelacion_Trabajos_ServiciosBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NUERelacion_Trabajos_ServiciosTableAdapter
        '
        Me.NUERelacion_Trabajos_ServiciosTableAdapter.ClearBeforeFill = True
        '
        'BORRelacion_Trabajos_ServiciosBindingSource
        '
        Me.BORRelacion_Trabajos_ServiciosBindingSource.DataMember = "BORRelacion_Trabajos_Servicios"
        Me.BORRelacion_Trabajos_ServiciosBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'BORRelacion_Trabajos_ServiciosTableAdapter
        '
        Me.BORRelacion_Trabajos_ServiciosTableAdapter.ClearBeforeFill = True
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Inserta_Rel_Trabajo_Cobro_DescBindingSource
        '
        Me.Inserta_Rel_Trabajo_Cobro_DescBindingSource.DataMember = "Inserta_Rel_Trabajo_Cobro_Desc"
        Me.Inserta_Rel_Trabajo_Cobro_DescBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Inserta_Rel_Trabajo_Cobro_DescTableAdapter
        '
        Me.Inserta_Rel_Trabajo_Cobro_DescTableAdapter.ClearBeforeFill = True
        '
        'Consulta_Rel_Trabajo_Cobro_DescBindingSource
        '
        Me.Consulta_Rel_Trabajo_Cobro_DescBindingSource.DataMember = "consulta_Rel_Trabajo_Cobro_Desc"
        Me.Consulta_Rel_Trabajo_Cobro_DescBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Consulta_Rel_Trabajo_Cobro_DescTableAdapter
        '
        Me.Consulta_Rel_Trabajo_Cobro_DescTableAdapter.ClearBeforeFill = True
        '
        'Se_cobraTextBox
        '
        Me.Se_cobraTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_Rel_Trabajo_Cobro_DescBindingSource, "se_cobra", True))
        Me.Se_cobraTextBox.Location = New System.Drawing.Point(362, 508)
        Me.Se_cobraTextBox.Name = "Se_cobraTextBox"
        Me.Se_cobraTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Se_cobraTextBox.TabIndex = 152
        '
        'FrmTrabajos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(803, 444)
        Me.Controls.Add(Se_cobraLabel)
        Me.Controls.Add(Me.Se_cobraTextBox)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "FrmTrabajos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Trabajos al Cliente"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.CONTRABAJOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.MUESTRARelacionTrabajosServiciosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraServiciosTODOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.CONTRABAJOSBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONTRABAJOSBindingNavigator.ResumeLayout(False)
        Me.CONTRABAJOSBindingNavigator.PerformLayout()
        CType(Me.MuestraServiciosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NUERelacion_Trabajos_ServiciosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BORRelacion_Trabajos_ServiciosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Rel_Trabajo_Cobro_DescBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_Rel_Trabajo_Cobro_DescBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONTRABAJOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONTRABAJOSTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONTRABAJOSTableAdapter
    Friend WithEvents CONTRABAJOSBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONTRABAJOSBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Clv_TrabajoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_TipSerTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PUNTOSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CobranzaCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents TipoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents MUESTRARelacionTrabajosServiciosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraServiciosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraServiciosTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MuestraServiciosTableAdapter
    Friend WithEvents MUESTRARelacion_Trabajos_ServiciosTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRARelacion_Trabajos_ServiciosTableAdapter
    Friend WithEvents MuestraServiciosTODOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraServiciosTODOSTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MuestraServiciosTODOSTableAdapter
    Friend WithEvents NUERelacion_Trabajos_ServiciosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NUERelacion_Trabajos_ServiciosTableAdapter As sofTV.NewSofTvDataSetTableAdapters.NUERelacion_Trabajos_ServiciosTableAdapter
    Friend WithEvents BORRelacion_Trabajos_ServiciosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BORRelacion_Trabajos_ServiciosTableAdapter As sofTV.NewSofTvDataSetTableAdapters.BORRelacion_Trabajos_ServiciosTableAdapter
    Friend WithEvents CLV_TRABAJOTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents Inserta_Rel_Trabajo_Cobro_DescBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Rel_Trabajo_Cobro_DescTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_Rel_Trabajo_Cobro_DescTableAdapter
    Friend WithEvents Consulta_Rel_Trabajo_Cobro_DescBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_Rel_Trabajo_Cobro_DescTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.consulta_Rel_Trabajo_Cobro_DescTableAdapter
    Friend WithEvents Se_cobraTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents tbPuntosCuadrilla As System.Windows.Forms.TextBox
    Public WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Public WithEvents TRABAJOTextBox As System.Windows.Forms.TextBox
    Public WithEvents DESCRIPCIONTextBox As System.Windows.Forms.TextBox
    Public WithEvents SICACheckBox As System.Windows.Forms.CheckBox
    Public WithEvents Panel2 As System.Windows.Forms.Panel
End Class
