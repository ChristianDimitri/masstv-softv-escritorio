﻿Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine

Imports sofTV.Base



Public Class FrmRegresaAparatos
    Dim CONSULTA As New CBase
    Dim BuscaMac As String = vbNullString
    Dim BuscaOrden As String = vbNullString
    Dim BuscaClvApa As String = vbNullString
    Dim macUorden As Integer = 0


    Public Function Llena_GRID(ByVal eTipoAparato As String, ByVal eMAC As String, ByVal eCLV_ORDEN As Long, ByVal eOp As Integer) As DataTable
        Try

            '@TipoAparato VARCHAR(250),@MAC VARCHAR(250),@CLV_ORDEN BIGINT,@OP INT
            Dim parametro(4) As SqlParameter
            parametro(0) = New SqlParameter("@TipoAparato", SqlDbType.VarChar, 250)
            parametro(0).Value = eTipoAparato

            parametro(1) = New SqlParameter("@MAC", SqlDbType.VarChar, 250)
            parametro(1).Value = eMAC

            parametro(2) = New SqlParameter("@CLV_ORDEN", SqlDbType.BigInt)
            parametro(2).Value = eCLV_ORDEN

            parametro(3) = New SqlParameter("@OP", SqlDbType.Int)
            parametro(3).Value = eOp

            Return CONSULTA.consultarDT("LLENA_APARATOS_REGRESAR", parametro)
        Catch ex As Exception

        End Try

    End Function

    Public Function Llena_GRID_Buscar(ByVal eTipoAparato As String, ByVal eMAC As String, ByVal eCLV_ORDEN As Long, ByVal eOp As Integer) As DataTable
        Try

            '@TipoAparato VARCHAR(250),@MAC VARCHAR(250),@CLV_ORDEN BIGINT,@OP INT
            Dim parametro(4) As SqlParameter
            parametro(0) = New SqlParameter("@ClvAparato", SqlDbType.VarChar, 250)
            parametro(0).Value = eTipoAparato

            parametro(1) = New SqlParameter("@MAC", SqlDbType.VarChar, 250)
            parametro(1).Value = eMAC

            parametro(2) = New SqlParameter("@CLV_ORDEN", SqlDbType.BigInt)
            parametro(2).Value = eCLV_ORDEN

            parametro(3) = New SqlParameter("@OP", SqlDbType.Int)
            parametro(3).Value = eOp

            Return CONSULTA.consultarDT("LLENA_APARATOS_REGRESAR_BUSQUEDA", parametro)
        Catch ex As Exception

        End Try

    End Function



    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()

    End Sub




    Private Sub FrmRegresaAparatos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Label1.ForeColor = Color.Black
        Label2.ForeColor = Color.Black
        Label3.ForeColor = Color.Black
        Try
            DataGridView1.DataSource = Llena_GRID("", "", 0, 0)

        Catch ex As Exception

        End Try
    End Sub

    Private Sub Guarda_Combo(ByVal eCLV_CABLEMODEM As Long, ByVal eMac As String, ByVal eTipoAparato As String, ByVal eEstadoAparato As String)

        ''Antes de mandar el Aparato al Almacen se debe preguntar si está seguro de que es ese el aparato
        'If Not (MsgBox("El Aparato será enviado al Almacén ¿deseas continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes) Then
        '    Exit Sub
        'End If

        Try
            Dim resp As MsgBoxResult = vbNo

            'eEstadoAparato = "B"

            'resp = MsgBox("¿El Aparato está en  Buen Estado?", MsgBoxStyle.YesNo, "¡Pregunta Importante!")

            'If resp = vbNo Then
            '    eEstadoAparato = "D"
            'Else
            '    eEstadoAparato = "B"
            'End If

            Using conexion As New SqlConnection(MiConexion)
                conexion.Open()
                '@CLV_CABLEMODEM BIGINT,@Mac VARCHAR(250),@TipoAparato VARCHAR(5),@EstadoAparato VARCHAR(5)
                Dim cmm As New SqlCommand()
                cmm.Connection = conexion
                cmm.CommandText = "MANDALO_AL_ALMACEN"
                cmm.CommandType = CommandType.StoredProcedure

                cmm.Parameters.Add("@CLV_CABLEMODEM", SqlDbType.BigInt)
                cmm.Parameters("@CLV_CABLEMODEM").Value = eCLV_CABLEMODEM

                cmm.Parameters.Add("@Mac", SqlDbType.VarChar, 250)
                cmm.Parameters("@Mac").Value = eMac

                cmm.Parameters.Add("@TipoAparato", SqlDbType.VarChar, 5)
                cmm.Parameters("@TipoAparato").Value = eTipoAparato

                cmm.Parameters.Add("@EstadoAparato", SqlDbType.VarChar, 5)
                cmm.Parameters("@EstadoAparato").Value = eEstadoAparato

                cmm.ExecuteNonQuery() 'Obtengo el numero de registros afectados
            End Using

        Catch ex As Exception

        End Try

    End Sub

    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        'Try

        '    If e.ColumnIndex = 5 And e.RowIndex >= 0 Then
        '        Dim eClv_Cablemodem As Long = 0
        '        Dim eMac As String = Nothing
        '        Dim eTipoAparato As String = Nothing
        '        Dim eEstadoAparato As String = Nothing

        '        Dim row As Integer = Convert.ToInt32(Me.DataGridView1.CurrentRow.Index.ToString)
        '        eClv_Cablemodem = Me.DataGridView1.Rows(row).Cells(1).Value.ToString
        '        eMac = Me.DataGridView1.Rows(row).Cells(2).Value.ToString
        '        eTipoAparato = Me.DataGridView1.Rows(row).Cells(6).Value.ToString
        '        eEstadoAparato = Me.DataGridView1.Rows(row).Cells(7).Value.ToString
        '        Guarda_Combo(eClv_Cablemodem, eMac, eTipoAparato, eEstadoAparato)

        '        DataGridView1.DataSource = Llena_GRID("", "", 0, 0)


        '    End If

        'Catch ex As Exception
        '    MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        'End Try
    End Sub

    'Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    '    Try

    '        If e.ColumnIndex = 4 And e.RowIndex >= 0 Then
    '            Dim eClv_Cablemodem As Long = 0
    '            Dim eMac As String = Nothing
    '            Dim eTipoAparato As String = Nothing
    '            Dim eEstadoAparato As String = Nothing

    '            Dim row As Integer = Convert.ToInt32(Me.DataGridView1.CurrentRow.Index.ToString)
    '            eClv_Cablemodem = Me.DataGridView1.Rows(row).Cells(1).Value.ToString
    '            eMac = Me.DataGridView1.Rows(row).Cells(2).Value.ToString
    '            eTipoAparato = Me.DataGridView1.Rows(row).Cells(6).Value.ToString
    '            eEstadoAparato = Me.DataGridView1.Rows(row).Cells(7).Value.ToString
    '            Guarda_Combo(eClv_Cablemodem, eMac, eTipoAparato, eEstadoAparato)

    '            DataGridView1.DataSource = Llena_GRID("", "", 0, 0)


    '        End If

    '    Catch ex As Exception
    '        MsgBox(ex.Message, MsgBoxStyle.Exclamation)
    '    End Try
    'End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try

        
        Dim cnn As New SqlConnection(MiConexion)
            Dim cmd As New SqlCommand("Listado_APARATOS_REGRESAR", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0
            'cmd.Parameters.AddWithValue("@fechaInicio", CDate(frmssm.dtpFechaInicio.Value))
            '    'cmd.Parameters.AddWithValue("@Clv_SessionUsuarios", eClv_Session)
            '    'cmd.Parameters.AddWithValue("@semanas", CInt(frmssm.nudSemanas.Value))
        Dim da As New SqlDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
            ds.Tables(0).TableName = "tbl_ListadoDeAparatosARegresar"
            'ds.Tables(1).TableName = "Detalle"
        Dim frmic As New FrmImprimirCentralizada()
            Dim rd As New ReportDocument()
            Dim mySelectFormula As String = "  Listado de Aparatos  "
            Dim Subformula As String = ""

           
            rd.Load(RutaReportes + "\rptListadoDeAparatosARegresar.rpt")
            rd.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            rd.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            rd.DataDefinition.FormulaFields("SubTitulo").Text = "'" & Subformula & "'"
        rd.SetDataSource(ds)
        frmic.rd = rd
            frmic.ShowDialog()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnRegresarAlmacen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegresarAlmacen.Click
        Try
            Dim bndHayElementos As Boolean = False

            For Each dgr1 As DataGridViewRow In DataGridView1.Rows
                If dgr1.Cells("colRegresarAlmacen").Value = True Then
                    bndHayElementos = True
                    Exit For
                End If
            Next

            If bndHayElementos = False Then
                MsgBox("Seleccione al menos un registro de la lista", MsgBoxStyle.Information)
                Exit Sub
            End If

            If Not (MsgBox("Los Aparatos seleccionados serán enviados al Almacén ¿deseas continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes) Then
                Exit Sub
            End If

            Dim eClv_Cablemodem As Long = 0
            Dim eMac As String = Nothing
            Dim eTipoAparato As String = Nothing
            Dim eEstadoAparato As String = Nothing

            'Dim row As Integer = Convert.ToInt32(Me.DataGridView1.CurrentRow.Index.ToString)
            For Each dgr As DataGridViewRow In DataGridView1.Rows
                If dgr.Cells("colRegresarAlmacen").Value = True Then
                    eClv_Cablemodem = dgr.Cells("Clv_Cablemodem").Value.ToString
                    eMac = dgr.Cells("Mac").Value.ToString
                    eTipoAparato = dgr.Cells("colTipoAparato").Value.ToString
                    If dgr.Cells("colEstadoAparato").Value = True Then
                        eEstadoAparato = "B"
                    ElseIf dgr.Cells("colEstadoAparato").Value = False Then
                        eEstadoAparato = "D"
                    End If
                    Guarda_Combo(eClv_Cablemodem, eMac, eTipoAparato, eEstadoAparato)
                End If
            Next
            DataGridView1.DataSource = Llena_GRID("", "", 0, 0)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Trim(tbBuscaMac.Text) = "" Then
            MsgBox("Introduce la Mac a Buscar")
        Else
            macUorden = 1
            BuscaMac = tbBuscaMac.Text
            'BuscaOrden = ""
            BuscaClvApa = ""
            DataGridView1.DataSource = Llena_GRID_Buscar(BuscaClvApa, BuscaMac, BuscaOrden, macUorden)
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Trim(tbBuscaOrden.Text) = "" Then
            MsgBox("Introduce el No. Orden a Buscar")
        Else
            BuscaMac = ""
            BuscaClvApa = ""
            BuscaOrden = tbBuscaOrden.Text
            macUorden = 2
            DataGridView1.DataSource = Llena_GRID_Buscar(BuscaClvApa, BuscaMac, BuscaOrden, macUorden)
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        DataGridView1.DataSource = Llena_GRID("", "", 0, 0)
    End Sub

    Private Sub Label3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label3.Click

    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If Trim(tbClvAparato.Text) = "" Then
            MsgBox("Introduce la Clave Aparato a Buscar")
        Else
            BuscaMac = ""
            BuscaClvApa = tbClvAparato.Text
            'BuscaOrden = tbBuscaOrden.Text
            macUorden = 3
            DataGridView1.DataSource = Llena_GRID_Buscar(BuscaClvApa, BuscaMac, BuscaOrden, macUorden)
        End If
    End Sub
End Class