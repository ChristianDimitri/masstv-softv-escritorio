﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwDepartamentos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim NombreLabel As System.Windows.Forms.Label
        Dim Clv_calleLabel1 As System.Windows.Forms.Label
        Me.dgvDepartamentos = New System.Windows.Forms.DataGridView()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.btnConsulta = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnBuscaDescripcion = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.CMBlblDescripcionDepto = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblClvDepto = New System.Windows.Forms.Label()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.CMBlblTitulo = New System.Windows.Forms.Label()
        Me.CMBLblDescripcion = New System.Windows.Forms.Label()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.Clave = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDescripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        NombreLabel = New System.Windows.Forms.Label()
        Clv_calleLabel1 = New System.Windows.Forms.Label()
        CType(Me.dgvDepartamentos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.ForeColor = System.Drawing.Color.White
        NombreLabel.Location = New System.Drawing.Point(17, 68)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(91, 15)
        NombreLabel.TabIndex = 3
        NombreLabel.Text = "Descripción :"
        '
        'Clv_calleLabel1
        '
        Clv_calleLabel1.AutoSize = True
        Clv_calleLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_calleLabel1.ForeColor = System.Drawing.Color.White
        Clv_calleLabel1.Location = New System.Drawing.Point(17, 36)
        Clv_calleLabel1.Name = "Clv_calleLabel1"
        Clv_calleLabel1.Size = New System.Drawing.Size(50, 15)
        Clv_calleLabel1.TabIndex = 1
        Clv_calleLabel1.Text = "Clave :"
        '
        'dgvDepartamentos
        '
        Me.dgvDepartamentos.AllowUserToAddRows = False
        Me.dgvDepartamentos.AllowUserToDeleteRows = False
        Me.dgvDepartamentos.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvDepartamentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDepartamentos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clave, Me.colDescripcion})
        Me.dgvDepartamentos.Location = New System.Drawing.Point(273, 6)
        Me.dgvDepartamentos.Name = "dgvDepartamentos"
        Me.dgvDepartamentos.ReadOnly = True
        Me.dgvDepartamentos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDepartamentos.Size = New System.Drawing.Size(596, 729)
        Me.dgvDepartamentos.TabIndex = 46
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.DarkOrange
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.ForeColor = System.Drawing.Color.Black
        Me.btnSalir.Location = New System.Drawing.Point(875, 688)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(136, 36)
        Me.btnSalir.TabIndex = 6
        Me.btnSalir.Text = "&SALIR"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'btnModificar
        '
        Me.btnModificar.BackColor = System.Drawing.Color.Orange
        Me.btnModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnModificar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnModificar.ForeColor = System.Drawing.Color.Black
        Me.btnModificar.Location = New System.Drawing.Point(875, 93)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(136, 36)
        Me.btnModificar.TabIndex = 4
        Me.btnModificar.Text = "&MODIFICAR"
        Me.btnModificar.UseVisualStyleBackColor = False
        '
        'btnConsulta
        '
        Me.btnConsulta.BackColor = System.Drawing.Color.Orange
        Me.btnConsulta.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnConsulta.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConsulta.ForeColor = System.Drawing.Color.Black
        Me.btnConsulta.Location = New System.Drawing.Point(875, 51)
        Me.btnConsulta.Name = "btnConsulta"
        Me.btnConsulta.Size = New System.Drawing.Size(136, 36)
        Me.btnConsulta.TabIndex = 3
        Me.btnConsulta.Text = "&CONSULTA"
        Me.btnConsulta.UseVisualStyleBackColor = False
        '
        'btnNuevo
        '
        Me.btnNuevo.BackColor = System.Drawing.Color.Orange
        Me.btnNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuevo.ForeColor = System.Drawing.Color.Black
        Me.btnNuevo.Location = New System.Drawing.Point(875, 9)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(136, 36)
        Me.btnNuevo.TabIndex = 2
        Me.btnNuevo.Text = "&NUEVO"
        Me.btnNuevo.UseVisualStyleBackColor = False
        '
        'btnBuscaDescripcion
        '
        Me.btnBuscaDescripcion.BackColor = System.Drawing.Color.DarkOrange
        Me.btnBuscaDescripcion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuscaDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscaDescripcion.ForeColor = System.Drawing.Color.Black
        Me.btnBuscaDescripcion.Location = New System.Drawing.Point(15, 96)
        Me.btnBuscaDescripcion.Name = "btnBuscaDescripcion"
        Me.btnBuscaDescripcion.Size = New System.Drawing.Size(88, 23)
        Me.btnBuscaDescripcion.TabIndex = 1
        Me.btnBuscaDescripcion.Text = "&Buscar"
        Me.btnBuscaDescripcion.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel1.Controls.Add(Me.CMBlblDescripcionDepto)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(NombreLabel)
        Me.Panel1.Controls.Add(Clv_calleLabel1)
        Me.Panel1.Controls.Add(Me.lblClvDepto)
        Me.Panel1.Location = New System.Drawing.Point(6, 367)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(261, 299)
        Me.Panel1.TabIndex = 44
        '
        'CMBlblDescripcionDepto
        '
        Me.CMBlblDescripcionDepto.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBlblDescripcionDepto.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBlblDescripcionDepto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblDescripcionDepto.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CMBlblDescripcionDepto.Location = New System.Drawing.Point(20, 86)
        Me.CMBlblDescripcionDepto.Multiline = True
        Me.CMBlblDescripcionDepto.Name = "CMBlblDescripcionDepto"
        Me.CMBlblDescripcionDepto.ReadOnly = True
        Me.CMBlblDescripcionDepto.Size = New System.Drawing.Size(207, 78)
        Me.CMBlblDescripcionDepto.TabIndex = 6
        Me.CMBlblDescripcionDepto.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(3, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(206, 20)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Datos del Departamento"
        '
        'lblClvDepto
        '
        Me.lblClvDepto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClvDepto.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lblClvDepto.Location = New System.Drawing.Point(75, 36)
        Me.lblClvDepto.Name = "lblClvDepto"
        Me.lblClvDepto.Size = New System.Drawing.Size(100, 23)
        Me.lblClvDepto.TabIndex = 2
        '
        'txtDescripcion
        '
        Me.txtDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescripcion.Location = New System.Drawing.Point(15, 69)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(243, 21)
        Me.txtDescripcion.TabIndex = 0
        '
        'CMBlblTitulo
        '
        Me.CMBlblTitulo.AutoSize = True
        Me.CMBlblTitulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblTitulo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBlblTitulo.Location = New System.Drawing.Point(9, 9)
        Me.CMBlblTitulo.Name = "CMBlblTitulo"
        Me.CMBlblTitulo.Size = New System.Drawing.Size(260, 24)
        Me.CMBlblTitulo.TabIndex = 41
        Me.CMBlblTitulo.Text = "Buscar Departamento Por :"
        '
        'CMBLblDescripcion
        '
        Me.CMBLblDescripcion.AutoSize = True
        Me.CMBLblDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLblDescripcion.Location = New System.Drawing.Point(12, 51)
        Me.CMBLblDescripcion.Name = "CMBLblDescripcion"
        Me.CMBLblDescripcion.Size = New System.Drawing.Size(91, 15)
        Me.CMBLblDescripcion.TabIndex = 43
        Me.CMBLblDescripcion.Text = "Descripción :"
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'btnEliminar
        '
        Me.btnEliminar.BackColor = System.Drawing.Color.Orange
        Me.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminar.ForeColor = System.Drawing.Color.Black
        Me.btnEliminar.Location = New System.Drawing.Point(875, 358)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(136, 36)
        Me.btnEliminar.TabIndex = 5
        Me.btnEliminar.Text = "&ELIMINAR"
        Me.btnEliminar.UseVisualStyleBackColor = False
        '
        'Clave
        '
        Me.Clave.DataPropertyName = "clvDepartamento"
        Me.Clave.HeaderText = "Clave Depto"
        Me.Clave.Name = "Clave"
        Me.Clave.ReadOnly = True
        '
        'colDescripcion
        '
        Me.colDescripcion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colDescripcion.DataPropertyName = "Descripcion"
        Me.colDescripcion.HeaderText = "Descripción"
        Me.colDescripcion.Name = "colDescripcion"
        Me.colDescripcion.ReadOnly = True
        '
        'BrwDepartamentos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1016, 741)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.dgvDepartamentos)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.btnConsulta)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.btnBuscaDescripcion)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.CMBlblTitulo)
        Me.Controls.Add(Me.CMBLblDescripcion)
        Me.Name = "BrwDepartamentos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Departamentos"
        CType(Me.dgvDepartamentos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvDepartamentos As System.Windows.Forms.DataGridView
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents btnConsulta As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents btnBuscaDescripcion As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents CMBlblDescripcionDepto As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblClvDepto As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents CMBlblTitulo As System.Windows.Forms.Label
    Friend WithEvents CMBLblDescripcion As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents Clave As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDescripcion As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
