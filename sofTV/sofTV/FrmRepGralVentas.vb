Imports System.Data.SqlClient
Public Class FrmRepGralVentas

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        If Me.RadioButton1.Checked = True Then
            MostrarGrupoVentasTodos()
            Me.GroupBox2.Enabled = True
            Me.GroupBox3.Enabled = False
            Me.GroupBox4.Enabled = False
        End If
    End Sub


    Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton3.CheckedChanged
        If Me.RadioButton3.Checked = True Then
            MostrarGrupoVentasTodos()
            Me.GroupBox2.Enabled = True
            Me.GroupBox3.Enabled = True
            Me.GroupBox4.Enabled = True
        End If
    End Sub

    Private Sub RadioButton4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton4.CheckedChanged
        If Me.RadioButton4.Checked = True Then
            MostrarGrupoVentas()
            Me.GroupBox2.Enabled = False
            Me.GroupBox3.Enabled = False
            Me.GroupBox4.Enabled = False
        End If
    End Sub



    Private Sub RadioButton5_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton5.CheckedChanged
        If Me.RadioButton5.Checked = True Then
            MostrarGrupoVentasTodos()
            Me.GroupBox2.Enabled = True
            Me.GroupBox3.Enabled = True
            Me.GroupBox4.Enabled = False
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            gloClvTipSerReportes = 0
            eTipSer = Me.ConceptoComboBox.SelectedValue
            gloClvTipSerReportes = eTipSer
            eServicio = Me.ConceptoComboBox.Text
            eClv_Grupo = Me.GrupoComboBox.SelectedValue
            eGrupo = Me.GrupoComboBox.Text
            eFechaIni = Me.DateTimePicker1.Value
            eFechaFin = Me.DateTimePicker2.Value

            'COMISIONES
            If Me.RadioButton1.Checked = True Then
                If eClv_Grupo = 1 Then
                    eOpVentas = 1
                    FrmSelVendedor.Show()
                ElseIf eClv_Grupo > 1 Then
                    eOpVentas = 49
                    FrmSelUsuario.Show()
                ElseIf eClv_Grupo = 0 Then
                    eOpVentas = 58
                    FrmSelUsuariosVentas.Show()
                End If
                'STATUS DE VENTAS
            ElseIf Me.RadioButton3.Checked = True Then
                If Me.CheckBox1.Checked = True Then
                    eCont = 1
                Else
                    eCont = 0
                End If
                If Me.CheckBox2.Checked = True Then
                    eInst = 1
                Else
                    eInst = 0
                End If
                If Me.CheckBox3.Checked = True Then
                    eSusp = 1
                Else
                    eSusp = 0
                End If
                If Me.CheckBox4.Checked = True Then
                    eDesc = 1
                Else
                    eDesc = 0
                End If
                If Me.CheckBox5.Checked = True Then
                    eBaja = 1
                Else
                    eBaja = 0
                End If
                If Me.CheckBox6.Checked = True Then
                    eFuera = 1
                Else
                    eFuera = 0
                End If
                If Me.CheckBox7.Checked = True Then
                    eTempo = 1
                Else
                    eTempo = 0
                End If

                If eCont = 0 And eInst = 0 And eSusp = 0 And eDesc = 0 And eBaja = 0 And eFuera = 0 And eTempo = 0 Then
                    MsgBox("Selecciona al Menos un Status para la Venta.")
                    Exit Sub
                End If

                If eClv_Grupo = 1 Then
                    eOpVentas = 3
                    FrmSelVendedor.Show()
                ElseIf eClv_Grupo > 1 Then
                    eOpVentas = 53
                    FrmSelUsuario.Show()
                ElseIf eClv_Grupo = 0 Then
                    eOpVentas = 60
                    FrmSelUsuariosVentas.Show()
                End If
                'FOLIOS FALTANTES
            ElseIf Me.RadioButton4.Checked = True Then
                eClv_Grupo = 1
                If eClv_Grupo = 1 Then
                    eOpVentas = 4
                    eOp = 4
                    FrmSelVendedor.Show()
                End If
                'NUMERO DE VENTAS
            ElseIf Me.RadioButton5.Checked = True Then

                If eClv_Grupo = 1 Then
                    eOpVentas = 47
                    FrmSelVendedor.Show()
                ElseIf eClv_Grupo > 1 Then
                    eOpVentas = 48
                    FrmSelUsuario.Show()
                ElseIf eClv_Grupo = 0 Then
                    eOpVentas = 59
                    FrmSelUsuariosVentas.Show()
                End If


            End If


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub FrmRepGralVentas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            colorea(Me, Me.Name)
            RadioButton4.Visible = False
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            MostrarGrupoVentasTodos()
            Me.MuestraTipServEricTableAdapter.Connection = CON
            Me.MuestraTipServEricTableAdapter.Fill(Me.DataSetEric2.MuestraTipServEric, 0, 0)
            CON.Close()
            Me.DateTimePicker1.Value = Today
            Me.DateTimePicker2.Value = Today
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Me.RadioButton1.Visible = False
    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
    End Sub

    Private Sub DateTimePicker2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker2.ValueChanged
        Me.DateTimePicker1.MaxDate = Me.DateTimePicker2.Value
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub MostrarGrupoVentas()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConGrupoVentasTableAdapter.Connection = CON
        Me.ConGrupoVentasTableAdapter.Fill(Me.DataSetEric2.ConGrupoVentas, 0, 3)
        CON.Close()
    End Sub

    Private Sub MostrarGrupoVentasTodos()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConGrupoVentasTableAdapter.Connection = CON
        Me.ConGrupoVentasTableAdapter.Fill(Me.DataSetEric2.ConGrupoVentas, 0, 4)
        CON.Close()
    End Sub
End Class