﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports sofTV.Base

Public Class FrmReimpresionSeries

    Dim serie As String
    Dim inicio As Integer
    Dim fin As Integer
    Dim existentes As Integer

    Dim consulta As New CBase

    Private Sub LlenaCombo()

        Dim dt As DataTable = consulta.consultarDT("SP_SerieFolio", Nothing)

        If dt.Rows.Count > 0 Then
            Cmb_serie.DataSource = dt
            Cmb_serie.DisplayMember = "Serie"
            Cmb_serie.ValueMember = "Clave"
        End If

    End Sub

    Private Sub ReimpresionFolios(ByVal serie As String, ByVal folio_inicio As Integer, ByVal folio_fin As Integer)
        Try
            Using conexion As New SqlConnection(MiConexion)
                conexion.Open()
                Dim cmm As New SqlCommand()
                cmm.Connection = conexion
                cmm.CommandText = "SP_ReimpresionFolios"
                cmm.CommandType = CommandType.StoredProcedure

                cmm.Parameters.Add("@serie", SqlDbType.NVarChar, 150)
                cmm.Parameters.Add("@folio_inicio", SqlDbType.Int)
                cmm.Parameters.Add("@folio_fin", SqlDbType.Int)

                cmm.Parameters("@serie").Value = serie
                cmm.Parameters("@folio_inicio").Value = folio_inicio
                cmm.Parameters("@folio_fin").Value = folio_fin

                cmm.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try

    End Sub

    Private Function FoliosExistentes(ByVal vserie As String) As Integer

        Dim parametro(1) As SqlParameter

        parametro(0) = New SqlParameter("@serie", SqlDbType.NVarChar, 150)
        parametro(0).Direction = ParameterDirection.Input
        parametro(0).Value = vserie

        Dim registro As DataTable = consulta.consultarDT("SP_ReimpresionFoliosExistentes", parametro)

        If registro.Rows.Count > 0 Then
            Return Convert.ToInt32(registro.Rows(0)("Existentes").ToString())
        Else
            MsgBox("No hay folios existentes", MsgBoxStyle.Information)
            Return 0
        End If

    End Function

    Private Sub FrmReimpresionSeries_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        LlenaCombo()
    End Sub

    Private Sub Btn_ReImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_ReImprimir.Click

        serie = Cmb_serie.GetItemText(Cmb_serie.SelectedItem).ToString()

        If (Cmb_serie.SelectedValue = 0 Or Not IsNumeric(Txt_inicio.Text.ToString()) Or Not IsNumeric(Txt_fin.Text.ToString())) Then
            MsgBox("Inserta datos enteros y selecciona una serie", MsgBoxStyle.Information)
        Else

            inicio = Convert.ToInt32(Txt_inicio.Text.ToString())
            fin = Convert.ToInt32(Txt_fin.Text.ToString())
            existentes = FoliosExistentes(serie)

            If inicio <= 0 Or inicio > fin Then
                MsgBox("El folio de inicio debe ser mayor a cero y menor o igual al folio final", MsgBoxStyle.Information)
            Else
                If fin < inicio Or fin > existentes Then
                    MsgBox("El folio final debe ser mayor o igual al folio de inicio y menor a " & existentes.ToString(), MsgBoxStyle.Information)
                Else
                    ReimpresionFolios(serie, inicio, fin)

                    LocGloOpRep = 21
                    GloSeries = serie

                    FrmImprimirFac.Show()
                End If
            End If

        End If

        LocGloOpRep = 0
        Me.Close()
    End Sub

    Private Sub Btn_Salir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Salir.Click
        Me.Close()
    End Sub
End Class