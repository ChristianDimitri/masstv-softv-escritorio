﻿Public Class ClassCompanias

#Region "Variables Globales"
    Public bndInsercion As Integer
    Public newClvCompania As Long
    Public bndRfc As Integer
#End Region

#Region "Propiedades"
    Private _clvCompania As Long
    Public Property clvCompania As Long
        Get
            Return _clvCompania
        End Get
        Set(ByVal valor As Long)
            _clvCompania = valor
        End Set
    End Property

    Private _nombreCompania As String
    Public Property nombreCompania As String
        Get
            Return _nombreCompania
        End Get
        Set(ByVal valor As String)
            _nombreCompania = valor
        End Set
    End Property

    Private _clvEmpresaMizar As Integer
    Public Property clvEmpresaMizar As Integer
        Get
            Return _clvEmpresaMizar
        End Get
        Set(ByVal valor As Integer)
            _clvEmpresaMizar = valor
        End Set
    End Property

    Private _activo As Boolean
    Public Property activo As Boolean
        Get
            Return _activo
        End Get
        Set(ByVal valor As Boolean)
            _activo = valor
        End Set
    End Property

    Private _op As Integer
    Public Property op As Integer
        Get
            Return _op
        End Get
        Set(ByVal valor As Integer)
            _op = valor
        End Set
    End Property

    Private _bnd As Integer
    Public Property bnd As Integer
        Get
            Return _bnd
        End Get
        Set(ByVal valor As Integer)
            _bnd = valor
        End Set
    End Property

    Private _rfc As String
    Public Property rfc As String
        Get
            Return _rfc
        End Get
        Set(ByVal valor As String)
            _rfc = valor
        End Set
    End Property

    Private _clvTipServ As Integer
    Public Property clvTipServ As Integer
        Get
            Return _clvTipServ
        End Get
        Set(ByVal valor As Integer)
            _clvTipServ = valor
        End Set
    End Property
#End Region

#Region "Constructores"
    Public Sub New()
        _clvCompania = 0
        _nombreCompania = ""
        _clvEmpresaMizar = 0
        _activo = False
        _op = 0
        _bnd = 0
        _rfc = ""
        _clvTipServ = 0
    End Sub
#End Region

#Region "Métodos"
    Dim companias As New BaseIII

    Public Sub uspInsertaTblCompanias()
        Try
            companias.limpiaParametros()
            companias.CreateMyParameter("@clvCompania", SqlDbType.BigInt, _clvCompania)
            companias.CreateMyParameter("@nombreCompania", SqlDbType.VarChar, _nombreCompania, 250)
            companias.CreateMyParameter("@clvEmpresaMizar", SqlDbType.Int, _clvEmpresaMizar, _activo)
            companias.CreateMyParameter("@activo", SqlDbType.Bit, _activo)
            companias.CreateMyParameter("@op", SqlDbType.Int, _op)
            companias.CreateMyParameter("@bnd", ParameterDirection.Output, SqlDbType.Int)
            companias.CreateMyParameter("@clvComp", ParameterDirection.Output, SqlDbType.BigInt)
            companias.CreateMyParameter("@bndRfc", ParameterDirection.Output, SqlDbType.Int)

            companias.ProcedimientoOutPut("uspInsertaTblCompanias")
            bndInsercion = CInt(companias.dicoPar("@bnd").ToString())
            newClvCompania = CLng(companias.dicoPar("@clvComp").ToString())
            bndRfc = CInt(companias.dicoPar("@bndRfc").ToString())
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function uspConsultaTblCompanias() As DataTable
        Try
            companias.limpiaParametros()
            companias.CreateMyParameter("@clvCompania", SqlDbType.BigInt, _clvCompania)
            companias.CreateMyParameter("@nombreCompania", SqlDbType.VarChar, _nombreCompania, 250)
            companias.CreateMyParameter("@rfc", SqlDbType.VarChar, _rfc, 50)
            companias.CreateMyParameter("@op", SqlDbType.Int, _op)

            uspConsultaTblCompanias = companias.ConsultaDT("uspConsultaTblCompanias")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function uspConsultaCompaniasMizar() As DataTable
        Try
            companias.limpiaParametros()

            uspConsultaCompaniasMizar = companias.ConsultaDT("uspConsultaCompaniasMizar")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function uspConsultaTipServ() As DataTable
        Try
            companias.limpiaParametros()

            uspConsultaTipServ = companias.ConsultaDT("uspConsultaTipServ")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function uspInsertaTblRelCompaniaServicio() As Integer
        Try
            companias.limpiaParametros()
            companias.CreateMyParameter("@clvCompania", SqlDbType.BigInt, _clvCompania)
            companias.CreateMyParameter("@clvTipServ", SqlDbType.Int, _clvTipServ)
            companias.CreateMyParameter("@bndCompanias", ParameterDirection.Output, SqlDbType.Int)

            companias.ProcedimientoOutPut("uspInsertaTblRelCompaniaServicio")
            uspInsertaTblRelCompaniaServicio = CInt(companias.dicoPar("@bndCompanias").ToString())
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function uspConsultaTblRelCompaniaServicio() As DataTable
        Try
            companias.limpiaParametros()
            companias.CreateMyParameter("@clvCompania", SqlDbType.BigInt, _clvCompania)

            uspConsultaTblRelCompaniaServicio = companias.ConsultaDT("uspConsultaTblRelCompaniaServicio")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub uspEliminaTblRelCompaniaServicio()
        Try
            companias.limpiaParametros()
            companias.CreateMyParameter("@clvCompania", SqlDbType.BigInt, _clvCompania)
            companias.CreateMyParameter("@clvTipServ", SqlDbType.Int, _clvTipServ)
            companias.CreateMyParameter("@op", SqlDbType.Int, _op)

            companias.Inserta("uspEliminaTblRelCompaniaServicio")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region
End Class
