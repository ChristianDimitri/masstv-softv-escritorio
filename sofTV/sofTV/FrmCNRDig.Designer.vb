<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCNRDig
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ConsecutivoLabel As System.Windows.Forms.Label
        Dim Numero_de_contratoLabel As System.Windows.Forms.Label
        Dim CLV_UNICANETLabel As System.Windows.Forms.Label
        Dim Mac_addressLabel As System.Windows.Forms.Label
        Dim PaqueteLabel As System.Windows.Forms.Label
        Dim ComandoLabel As System.Windows.Forms.Label
        Dim ResultadoLabel As System.Windows.Forms.Label
        Dim Descripcion_transaccionLabel As System.Windows.Forms.Label
        Dim Fecha_ejecucionLabel As System.Windows.Forms.Label
        Dim Clv_OrdenLabel As System.Windows.Forms.Label
        Dim Fecha_habilitarLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCNRDig))
        Me.ConCNRDigBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ConCNRDigBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.ConCNRDigBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.ConsecutivoTextBox = New System.Windows.Forms.TextBox()
        Me.Numero_de_contratoTextBox = New System.Windows.Forms.TextBox()
        Me.CLV_UNICANETTextBox = New System.Windows.Forms.TextBox()
        Me.Mac_addressTextBox = New System.Windows.Forms.TextBox()
        Me.PaqueteTextBox = New System.Windows.Forms.TextBox()
        Me.Descripcion_transaccionTextBox = New System.Windows.Forms.TextBox()
        Me.Fecha_ejecucionDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.Clv_OrdenTextBox = New System.Windows.Forms.TextBox()
        Me.Fecha_habilitarDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.ComandoTextBox = New System.Windows.Forms.TextBox()
        Me.ResultadoTextBox = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.ConCNRDigTableAdapter = New sofTV.DataSetEricTableAdapters.ConCNRDigTableAdapter()
        ConsecutivoLabel = New System.Windows.Forms.Label()
        Numero_de_contratoLabel = New System.Windows.Forms.Label()
        CLV_UNICANETLabel = New System.Windows.Forms.Label()
        Mac_addressLabel = New System.Windows.Forms.Label()
        PaqueteLabel = New System.Windows.Forms.Label()
        ComandoLabel = New System.Windows.Forms.Label()
        ResultadoLabel = New System.Windows.Forms.Label()
        Descripcion_transaccionLabel = New System.Windows.Forms.Label()
        Fecha_ejecucionLabel = New System.Windows.Forms.Label()
        Clv_OrdenLabel = New System.Windows.Forms.Label()
        Fecha_habilitarLabel = New System.Windows.Forms.Label()
        CType(Me.ConCNRDigBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ConCNRDigBindingNavigator.SuspendLayout()
        CType(Me.ConCNRDigBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ConsecutivoLabel
        '
        ConsecutivoLabel.AutoSize = True
        ConsecutivoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ConsecutivoLabel.Location = New System.Drawing.Point(31, 55)
        ConsecutivoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        ConsecutivoLabel.Name = "ConsecutivoLabel"
        ConsecutivoLabel.Size = New System.Drawing.Size(118, 20)
        ConsecutivoLabel.TabIndex = 2
        ConsecutivoLabel.Text = "Consecutivo:"
        '
        'Numero_de_contratoLabel
        '
        Numero_de_contratoLabel.AutoSize = True
        Numero_de_contratoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Numero_de_contratoLabel.Location = New System.Drawing.Point(32, 23)
        Numero_de_contratoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Numero_de_contratoLabel.Name = "Numero_de_contratoLabel"
        Numero_de_contratoLabel.Size = New System.Drawing.Size(184, 20)
        Numero_de_contratoLabel.TabIndex = 4
        Numero_de_contratoLabel.Text = "Número de Contrato:"
        '
        'CLV_UNICANETLabel
        '
        CLV_UNICANETLabel.AutoSize = True
        CLV_UNICANETLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CLV_UNICANETLabel.Location = New System.Drawing.Point(31, 122)
        CLV_UNICANETLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        CLV_UNICANETLabel.Name = "CLV_UNICANETLabel"
        CLV_UNICANETLabel.Size = New System.Drawing.Size(149, 20)
        CLV_UNICANETLabel.TabIndex = 6
        CLV_UNICANETLabel.Text = "CLV UNICANET:"
        '
        'Mac_addressLabel
        '
        Mac_addressLabel.AutoSize = True
        Mac_addressLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Mac_addressLabel.Location = New System.Drawing.Point(95, 74)
        Mac_addressLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Mac_addressLabel.Name = "Mac_addressLabel"
        Mac_addressLabel.Size = New System.Drawing.Size(125, 20)
        Mac_addressLabel.TabIndex = 8
        Mac_addressLabel.Text = "Mac Address:"
        '
        'PaqueteLabel
        '
        PaqueteLabel.AutoSize = True
        PaqueteLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PaqueteLabel.Location = New System.Drawing.Point(139, 122)
        PaqueteLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        PaqueteLabel.Name = "PaqueteLabel"
        PaqueteLabel.Size = New System.Drawing.Size(83, 20)
        PaqueteLabel.TabIndex = 10
        PaqueteLabel.Text = "Paquete:"
        '
        'ComandoLabel
        '
        ComandoLabel.AutoSize = True
        ComandoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ComandoLabel.Location = New System.Drawing.Point(128, 175)
        ComandoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        ComandoLabel.Name = "ComandoLabel"
        ComandoLabel.Size = New System.Drawing.Size(93, 20)
        ComandoLabel.TabIndex = 12
        ComandoLabel.Text = "Comando:"
        '
        'ResultadoLabel
        '
        ResultadoLabel.AutoSize = True
        ResultadoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ResultadoLabel.Location = New System.Drawing.Point(121, 230)
        ResultadoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        ResultadoLabel.Name = "ResultadoLabel"
        ResultadoLabel.Size = New System.Drawing.Size(99, 20)
        ResultadoLabel.TabIndex = 14
        ResultadoLabel.Text = "Resultado:"
        '
        'Descripcion_transaccionLabel
        '
        Descripcion_transaccionLabel.AutoSize = True
        Descripcion_transaccionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Descripcion_transaccionLabel.Location = New System.Drawing.Point(31, 90)
        Descripcion_transaccionLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Descripcion_transaccionLabel.Name = "Descripcion_transaccionLabel"
        Descripcion_transaccionLabel.Size = New System.Drawing.Size(216, 20)
        Descripcion_transaccionLabel.TabIndex = 16
        Descripcion_transaccionLabel.Text = "descripcion transaccion:"
        '
        'Fecha_ejecucionLabel
        '
        Fecha_ejecucionLabel.AutoSize = True
        Fecha_ejecucionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_ejecucionLabel.Location = New System.Drawing.Point(31, 158)
        Fecha_ejecucionLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Fecha_ejecucionLabel.Name = "Fecha_ejecucionLabel"
        Fecha_ejecucionLabel.Size = New System.Drawing.Size(180, 20)
        Fecha_ejecucionLabel.TabIndex = 18
        Fecha_ejecucionLabel.Text = "Fecha de Ejecución:"
        '
        'Clv_OrdenLabel
        '
        Clv_OrdenLabel.AutoSize = True
        Clv_OrdenLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_OrdenLabel.Location = New System.Drawing.Point(31, 191)
        Clv_OrdenLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Clv_OrdenLabel.Name = "Clv_OrdenLabel"
        Clv_OrdenLabel.Size = New System.Drawing.Size(99, 20)
        Clv_OrdenLabel.TabIndex = 20
        Clv_OrdenLabel.Text = "Clv Orden:"
        '
        'Fecha_habilitarLabel
        '
        Fecha_habilitarLabel.AutoSize = True
        Fecha_habilitarLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_habilitarLabel.Location = New System.Drawing.Point(27, 287)
        Fecha_habilitarLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Fecha_habilitarLabel.Name = "Fecha_habilitarLabel"
        Fecha_habilitarLabel.Size = New System.Drawing.Size(187, 20)
        Fecha_habilitarLabel.TabIndex = 22
        Fecha_habilitarLabel.Text = "Fecha para Habilitar:"
        '
        'ConCNRDigBindingNavigator
        '
        Me.ConCNRDigBindingNavigator.AddNewItem = Nothing
        Me.ConCNRDigBindingNavigator.BindingSource = Me.ConCNRDigBindingSource
        Me.ConCNRDigBindingNavigator.CountItem = Nothing
        Me.ConCNRDigBindingNavigator.DeleteItem = Nothing
        Me.ConCNRDigBindingNavigator.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConCNRDigBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ConCNRDigBindingNavigatorSaveItem})
        Me.ConCNRDigBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.ConCNRDigBindingNavigator.MoveFirstItem = Nothing
        Me.ConCNRDigBindingNavigator.MoveLastItem = Nothing
        Me.ConCNRDigBindingNavigator.MoveNextItem = Nothing
        Me.ConCNRDigBindingNavigator.MovePreviousItem = Nothing
        Me.ConCNRDigBindingNavigator.Name = "ConCNRDigBindingNavigator"
        Me.ConCNRDigBindingNavigator.PositionItem = Nothing
        Me.ConCNRDigBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ConCNRDigBindingNavigator.Size = New System.Drawing.Size(793, 28)
        Me.ConCNRDigBindingNavigator.TabIndex = 7
        Me.ConCNRDigBindingNavigator.TabStop = True
        Me.ConCNRDigBindingNavigator.Text = "BindingNavigator1"
        '
        'ConCNRDigBindingSource
        '
        Me.ConCNRDigBindingSource.DataMember = "ConCNRDig"
        Me.ConCNRDigBindingSource.DataSource = Me.DataSetEric
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ConCNRDigBindingNavigatorSaveItem
        '
        Me.ConCNRDigBindingNavigatorSaveItem.Image = CType(resources.GetObject("ConCNRDigBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.ConCNRDigBindingNavigatorSaveItem.Name = "ConCNRDigBindingNavigatorSaveItem"
        Me.ConCNRDigBindingNavigatorSaveItem.Size = New System.Drawing.Size(117, 25)
        Me.ConCNRDigBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'ConsecutivoTextBox
        '
        Me.ConsecutivoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCNRDigBindingSource, "consecutivo", True))
        Me.ConsecutivoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConsecutivoTextBox.Location = New System.Drawing.Point(288, 52)
        Me.ConsecutivoTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ConsecutivoTextBox.Name = "ConsecutivoTextBox"
        Me.ConsecutivoTextBox.ReadOnly = True
        Me.ConsecutivoTextBox.Size = New System.Drawing.Size(265, 26)
        Me.ConsecutivoTextBox.TabIndex = 3
        Me.ConsecutivoTextBox.TabStop = False
        '
        'Numero_de_contratoTextBox
        '
        Me.Numero_de_contratoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCNRDigBindingSource, "numero_de_contrato", True))
        Me.Numero_de_contratoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Numero_de_contratoTextBox.Location = New System.Drawing.Point(240, 16)
        Me.Numero_de_contratoTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Numero_de_contratoTextBox.Name = "Numero_de_contratoTextBox"
        Me.Numero_de_contratoTextBox.Size = New System.Drawing.Size(265, 26)
        Me.Numero_de_contratoTextBox.TabIndex = 0
        '
        'CLV_UNICANETTextBox
        '
        Me.CLV_UNICANETTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCNRDigBindingSource, "CLV_UNICANET", True))
        Me.CLV_UNICANETTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CLV_UNICANETTextBox.Location = New System.Drawing.Point(288, 118)
        Me.CLV_UNICANETTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CLV_UNICANETTextBox.Name = "CLV_UNICANETTextBox"
        Me.CLV_UNICANETTextBox.ReadOnly = True
        Me.CLV_UNICANETTextBox.Size = New System.Drawing.Size(265, 26)
        Me.CLV_UNICANETTextBox.TabIndex = 7
        Me.CLV_UNICANETTextBox.TabStop = False
        '
        'Mac_addressTextBox
        '
        Me.Mac_addressTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCNRDigBindingSource, "mac_address", True))
        Me.Mac_addressTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Mac_addressTextBox.Location = New System.Drawing.Point(240, 66)
        Me.Mac_addressTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Mac_addressTextBox.Name = "Mac_addressTextBox"
        Me.Mac_addressTextBox.Size = New System.Drawing.Size(265, 26)
        Me.Mac_addressTextBox.TabIndex = 1
        '
        'PaqueteTextBox
        '
        Me.PaqueteTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCNRDigBindingSource, "paquete", True))
        Me.PaqueteTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PaqueteTextBox.Location = New System.Drawing.Point(240, 114)
        Me.PaqueteTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.PaqueteTextBox.Name = "PaqueteTextBox"
        Me.PaqueteTextBox.Size = New System.Drawing.Size(265, 26)
        Me.PaqueteTextBox.TabIndex = 2
        Me.PaqueteTextBox.TabStop = False
        '
        'Descripcion_transaccionTextBox
        '
        Me.Descripcion_transaccionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCNRDigBindingSource, "descripcion_transaccion", True))
        Me.Descripcion_transaccionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Descripcion_transaccionTextBox.Location = New System.Drawing.Point(288, 86)
        Me.Descripcion_transaccionTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Descripcion_transaccionTextBox.Name = "Descripcion_transaccionTextBox"
        Me.Descripcion_transaccionTextBox.ReadOnly = True
        Me.Descripcion_transaccionTextBox.Size = New System.Drawing.Size(265, 26)
        Me.Descripcion_transaccionTextBox.TabIndex = 17
        Me.Descripcion_transaccionTextBox.TabStop = False
        '
        'Fecha_ejecucionDateTimePicker
        '
        Me.Fecha_ejecucionDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.ConCNRDigBindingSource, "fecha_ejecucion", True))
        Me.Fecha_ejecucionDateTimePicker.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fecha_ejecucionDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Fecha_ejecucionDateTimePicker.Location = New System.Drawing.Point(288, 153)
        Me.Fecha_ejecucionDateTimePicker.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Fecha_ejecucionDateTimePicker.Name = "Fecha_ejecucionDateTimePicker"
        Me.Fecha_ejecucionDateTimePicker.Size = New System.Drawing.Size(265, 26)
        Me.Fecha_ejecucionDateTimePicker.TabIndex = 19
        Me.Fecha_ejecucionDateTimePicker.TabStop = False
        '
        'Clv_OrdenTextBox
        '
        Me.Clv_OrdenTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCNRDigBindingSource, "Clv_Orden", True))
        Me.Clv_OrdenTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_OrdenTextBox.Location = New System.Drawing.Point(288, 187)
        Me.Clv_OrdenTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_OrdenTextBox.Name = "Clv_OrdenTextBox"
        Me.Clv_OrdenTextBox.ReadOnly = True
        Me.Clv_OrdenTextBox.Size = New System.Drawing.Size(265, 26)
        Me.Clv_OrdenTextBox.TabIndex = 21
        Me.Clv_OrdenTextBox.TabStop = False
        '
        'Fecha_habilitarDateTimePicker
        '
        Me.Fecha_habilitarDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.ConCNRDigBindingSource, "fecha_habilitar", True))
        Me.Fecha_habilitarDateTimePicker.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fecha_habilitarDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Fecha_habilitarDateTimePicker.Location = New System.Drawing.Point(240, 279)
        Me.Fecha_habilitarDateTimePicker.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Fecha_habilitarDateTimePicker.Name = "Fecha_habilitarDateTimePicker"
        Me.Fecha_habilitarDateTimePicker.Size = New System.Drawing.Size(145, 26)
        Me.Fecha_habilitarDateTimePicker.TabIndex = 6
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.ComboBox1.Location = New System.Drawing.Point(379, 169)
        Me.ComboBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(127, 24)
        Me.ComboBox1.TabIndex = 4
        '
        'ComboBox2
        '
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.ComboBox2.Items.AddRange(New Object() {"0", "1"})
        Me.ComboBox2.Location = New System.Drawing.Point(379, 224)
        Me.ComboBox2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(127, 24)
        Me.ComboBox2.TabIndex = 5
        '
        'ComandoTextBox
        '
        Me.ComandoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCNRDigBindingSource, "comando", True))
        Me.ComandoTextBox.Location = New System.Drawing.Point(240, 170)
        Me.ComandoTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComandoTextBox.Name = "ComandoTextBox"
        Me.ComandoTextBox.ReadOnly = True
        Me.ComandoTextBox.Size = New System.Drawing.Size(119, 22)
        Me.ComandoTextBox.TabIndex = 26
        Me.ComandoTextBox.TabStop = False
        '
        'ResultadoTextBox
        '
        Me.ResultadoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCNRDigBindingSource, "resultado", True))
        Me.ResultadoTextBox.Location = New System.Drawing.Point(240, 225)
        Me.ResultadoTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ResultadoTextBox.Name = "ResultadoTextBox"
        Me.ResultadoTextBox.ReadOnly = True
        Me.ResultadoTextBox.Size = New System.Drawing.Size(119, 22)
        Me.ResultadoTextBox.TabIndex = 27
        Me.ResultadoTextBox.TabStop = False
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(596, 384)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(181, 44)
        Me.Button1.TabIndex = 10
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.CheckBox2)
        Me.Panel1.Controls.Add(Me.CheckBox1)
        Me.Panel1.Controls.Add(Mac_addressLabel)
        Me.Panel1.Controls.Add(Me.Fecha_habilitarDateTimePicker)
        Me.Panel1.Controls.Add(Me.ResultadoTextBox)
        Me.Panel1.Controls.Add(Fecha_habilitarLabel)
        Me.Panel1.Controls.Add(Me.ComandoTextBox)
        Me.Panel1.Controls.Add(ResultadoLabel)
        Me.Panel1.Controls.Add(PaqueteLabel)
        Me.Panel1.Controls.Add(Me.ComboBox2)
        Me.Panel1.Controls.Add(ComandoLabel)
        Me.Panel1.Controls.Add(Me.ComboBox1)
        Me.Panel1.Controls.Add(Me.PaqueteTextBox)
        Me.Panel1.Controls.Add(Me.Mac_addressTextBox)
        Me.Panel1.Controls.Add(Me.Numero_de_contratoTextBox)
        Me.Panel1.Controls.Add(Numero_de_contratoLabel)
        Me.Panel1.Location = New System.Drawing.Point(16, 52)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(744, 325)
        Me.Panel1.TabIndex = 0
        Me.Panel1.TabStop = True
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox2.Location = New System.Drawing.Point(631, 117)
        Me.CheckBox2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(82, 24)
        Me.CheckBox2.TabIndex = 3
        Me.CheckBox2.Text = "DEM2"
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.Location = New System.Drawing.Point(532, 117)
        Me.CheckBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(82, 24)
        Me.CheckBox1.TabIndex = 2
        Me.CheckBox1.Text = "DEM1"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'ConCNRDigTableAdapter
        '
        Me.ConCNRDigTableAdapter.ClearBeforeFill = True
        '
        'FrmCNRDig
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(793, 439)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(ConsecutivoLabel)
        Me.Controls.Add(Me.ConsecutivoTextBox)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(CLV_UNICANETLabel)
        Me.Controls.Add(Me.CLV_UNICANETTextBox)
        Me.Controls.Add(Descripcion_transaccionLabel)
        Me.Controls.Add(Me.Descripcion_transaccionTextBox)
        Me.Controls.Add(Fecha_ejecucionLabel)
        Me.Controls.Add(Me.Fecha_ejecucionDateTimePicker)
        Me.Controls.Add(Clv_OrdenLabel)
        Me.Controls.Add(Me.Clv_OrdenTextBox)
        Me.Controls.Add(Me.ConCNRDigBindingNavigator)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmCNRDig"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "CNR Digital"
        CType(Me.ConCNRDigBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ConCNRDigBindingNavigator.ResumeLayout(False)
        Me.ConCNRDigBindingNavigator.PerformLayout()
        CType(Me.ConCNRDigBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents ConCNRDigBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConCNRDigTableAdapter As sofTV.DataSetEricTableAdapters.ConCNRDigTableAdapter
    Friend WithEvents ConCNRDigBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents ConCNRDigBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ConsecutivoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Numero_de_contratoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CLV_UNICANETTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Mac_addressTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PaqueteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Descripcion_transaccionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Fecha_ejecucionDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Clv_OrdenTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Fecha_habilitarDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents ComandoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ResultadoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
End Class
