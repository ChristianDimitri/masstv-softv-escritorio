<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmVisitas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_visitaLabel As System.Windows.Forms.Label
        Dim DescripcionLabel As System.Windows.Forms.Label
        Dim VendedorLabel As System.Windows.Forms.Label
        Dim FechaLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmVisitas))
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2()
        Me.COnsulta_CatalogoVisitasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.COnsulta_CatalogoVisitasTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.COnsulta_CatalogoVisitasTableAdapter()
        Me.COnsulta_CatalogoVisitasBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.BtnEliminar = New System.Windows.Forms.ToolStripButton()
        Me.BtnGuardar = New System.Windows.Forms.ToolStripButton()
        Me.Clv_visitaTextBox = New System.Windows.Forms.TextBox()
        Me.DescripcionTextBox = New System.Windows.Forms.TextBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.FechaDTP = New System.Windows.Forms.DateTimePicker()
        Me.VendedorComboBox = New System.Windows.Forms.ComboBox()
        Clv_visitaLabel = New System.Windows.Forms.Label()
        DescripcionLabel = New System.Windows.Forms.Label()
        VendedorLabel = New System.Windows.Forms.Label()
        FechaLabel = New System.Windows.Forms.Label()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.COnsulta_CatalogoVisitasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.COnsulta_CatalogoVisitasBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.COnsulta_CatalogoVisitasBindingNavigator.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Clv_visitaLabel
        '
        Clv_visitaLabel.AutoSize = True
        Clv_visitaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_visitaLabel.ForeColor = System.Drawing.Color.SlateGray
        Clv_visitaLabel.Location = New System.Drawing.Point(84, 19)
        Clv_visitaLabel.Name = "Clv_visitaLabel"
        Clv_visitaLabel.Size = New System.Drawing.Size(85, 15)
        Clv_visitaLabel.TabIndex = 2
        Clv_visitaLabel.Text = "Clave Visita:"
        '
        'DescripcionLabel
        '
        DescripcionLabel.AutoSize = True
        DescripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescripcionLabel.ForeColor = System.Drawing.Color.LightSlateGray
        DescripcionLabel.Location = New System.Drawing.Point(3, 57)
        DescripcionLabel.Name = "DescripcionLabel"
        DescripcionLabel.Size = New System.Drawing.Size(166, 15)
        DescripcionLabel.TabIndex = 4
        DescripcionLabel.Text = "Descripción de la Visita :"
        '
        'VendedorLabel
        '
        VendedorLabel.AutoSize = True
        VendedorLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        VendedorLabel.ForeColor = System.Drawing.Color.LightSlateGray
        VendedorLabel.Location = New System.Drawing.Point(84, 142)
        VendedorLabel.Name = "VendedorLabel"
        VendedorLabel.Size = New System.Drawing.Size(76, 15)
        VendedorLabel.TabIndex = 6
        VendedorLabel.Text = "Vendedor :"
        '
        'FechaLabel
        '
        FechaLabel.AutoSize = True
        FechaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FechaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        FechaLabel.Location = New System.Drawing.Point(106, 180)
        FechaLabel.Name = "FechaLabel"
        FechaLabel.Size = New System.Drawing.Size(54, 15)
        FechaLabel.TabIndex = 8
        FechaLabel.Text = "Fecha :"
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'COnsulta_CatalogoVisitasBindingSource
        '
        Me.COnsulta_CatalogoVisitasBindingSource.DataMember = "COnsulta_CatalogoVisitas"
        Me.COnsulta_CatalogoVisitasBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'COnsulta_CatalogoVisitasTableAdapter
        '
        Me.COnsulta_CatalogoVisitasTableAdapter.ClearBeforeFill = True
        '
        'COnsulta_CatalogoVisitasBindingNavigator
        '
        Me.COnsulta_CatalogoVisitasBindingNavigator.AddNewItem = Nothing
        Me.COnsulta_CatalogoVisitasBindingNavigator.BindingSource = Me.COnsulta_CatalogoVisitasBindingSource
        Me.COnsulta_CatalogoVisitasBindingNavigator.CountItem = Nothing
        Me.COnsulta_CatalogoVisitasBindingNavigator.DeleteItem = Nothing
        Me.COnsulta_CatalogoVisitasBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorSeparator1, Me.BindingNavigatorSeparator2, Me.BtnEliminar, Me.BtnGuardar})
        Me.COnsulta_CatalogoVisitasBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.COnsulta_CatalogoVisitasBindingNavigator.MoveFirstItem = Nothing
        Me.COnsulta_CatalogoVisitasBindingNavigator.MoveLastItem = Nothing
        Me.COnsulta_CatalogoVisitasBindingNavigator.MoveNextItem = Nothing
        Me.COnsulta_CatalogoVisitasBindingNavigator.MovePreviousItem = Nothing
        Me.COnsulta_CatalogoVisitasBindingNavigator.Name = "COnsulta_CatalogoVisitasBindingNavigator"
        Me.COnsulta_CatalogoVisitasBindingNavigator.PositionItem = Nothing
        Me.COnsulta_CatalogoVisitasBindingNavigator.Size = New System.Drawing.Size(665, 25)
        Me.COnsulta_CatalogoVisitasBindingNavigator.TabIndex = 0
        Me.COnsulta_CatalogoVisitasBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'BtnEliminar
        '
        Me.BtnEliminar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BtnEliminar.Image = CType(resources.GetObject("BtnEliminar.Image"), System.Drawing.Image)
        Me.BtnEliminar.Name = "BtnEliminar"
        Me.BtnEliminar.RightToLeftAutoMirrorImage = True
        Me.BtnEliminar.Size = New System.Drawing.Size(70, 22)
        Me.BtnEliminar.Text = "Eliminar"
        '
        'BtnGuardar
        '
        Me.BtnGuardar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BtnGuardar.Image = CType(resources.GetObject("BtnGuardar.Image"), System.Drawing.Image)
        Me.BtnGuardar.Name = "BtnGuardar"
        Me.BtnGuardar.Size = New System.Drawing.Size(101, 22)
        Me.BtnGuardar.Text = "Guardar datos"
        '
        'Clv_visitaTextBox
        '
        Me.Clv_visitaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.COnsulta_CatalogoVisitasBindingSource, "clv_visita", True))
        Me.Clv_visitaTextBox.Enabled = False
        Me.Clv_visitaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_visitaTextBox.ForeColor = System.Drawing.Color.Black
        Me.Clv_visitaTextBox.Location = New System.Drawing.Point(175, 18)
        Me.Clv_visitaTextBox.Name = "Clv_visitaTextBox"
        Me.Clv_visitaTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Clv_visitaTextBox.TabIndex = 3
        '
        'DescripcionTextBox
        '
        Me.DescripcionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.COnsulta_CatalogoVisitasBindingSource, "Descripcion", True))
        Me.DescripcionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionTextBox.ForeColor = System.Drawing.Color.Black
        Me.DescripcionTextBox.Location = New System.Drawing.Point(175, 44)
        Me.DescripcionTextBox.Multiline = True
        Me.DescripcionTextBox.Name = "DescripcionTextBox"
        Me.DescripcionTextBox.Size = New System.Drawing.Size(395, 74)
        Me.DescripcionTextBox.TabIndex = 5
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.Orange
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(517, 323)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(136, 36)
        Me.Button3.TabIndex = 7
        Me.Button3.Text = "&SALIR"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.FechaDTP)
        Me.Panel1.Controls.Add(FechaLabel)
        Me.Panel1.Controls.Add(Me.VendedorComboBox)
        Me.Panel1.Controls.Add(VendedorLabel)
        Me.Panel1.Controls.Add(DescripcionLabel)
        Me.Panel1.Controls.Add(Me.DescripcionTextBox)
        Me.Panel1.Controls.Add(Clv_visitaLabel)
        Me.Panel1.Controls.Add(Me.Clv_visitaTextBox)
        Me.Panel1.Location = New System.Drawing.Point(12, 50)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(641, 231)
        Me.Panel1.TabIndex = 8
        '
        'FechaDTP
        '
        Me.FechaDTP.Location = New System.Drawing.Point(175, 180)
        Me.FechaDTP.Name = "FechaDTP"
        Me.FechaDTP.Size = New System.Drawing.Size(395, 20)
        Me.FechaDTP.TabIndex = 9
        '
        'VendedorComboBox
        '
        Me.VendedorComboBox.FormattingEnabled = True
        Me.VendedorComboBox.Location = New System.Drawing.Point(175, 136)
        Me.VendedorComboBox.Name = "VendedorComboBox"
        Me.VendedorComboBox.Size = New System.Drawing.Size(395, 21)
        Me.VendedorComboBox.TabIndex = 7
        '
        'FrmVisitas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(665, 389)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.COnsulta_CatalogoVisitasBindingNavigator)
        Me.Name = "FrmVisitas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Visitas"
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.COnsulta_CatalogoVisitasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.COnsulta_CatalogoVisitasBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.COnsulta_CatalogoVisitasBindingNavigator.ResumeLayout(False)
        Me.COnsulta_CatalogoVisitasBindingNavigator.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents COnsulta_CatalogoVisitasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents COnsulta_CatalogoVisitasTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.COnsulta_CatalogoVisitasTableAdapter
    Friend WithEvents COnsulta_CatalogoVisitasBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BtnEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BtnGuardar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Clv_visitaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DescripcionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents FechaDTP As System.Windows.Forms.DateTimePicker
    Friend WithEvents VendedorComboBox As System.Windows.Forms.ComboBox
End Class
