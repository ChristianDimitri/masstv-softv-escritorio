﻿Imports sofTV.BAL
Imports System.Data.SqlClient
Imports System.Text
Public Class frmReporteLlamadasTel
    Dim opcionReporte As Integer
    Dim bandera As Boolean




    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Locbndrep = 1000

        If Me.TipoPersona1RadioButton.Checked Then
            TipoPersona = 1

        ElseIf Me.TipoPersona2RadioButton.Checked Then
            TipoPersona = 2
        End If

        If Me.CheckBox1.Checked = True Then
            OpcionReporteLlamada = 1
        Else

            OpcionReporteLlamada = 0
        End If


        TipoServico_ReporteLlamada = ConceptoComboBox.SelectedValue
        Clave_Motivo = MotivoComboBox.SelectedValue
        Fecha1 = DateTimePicker1.Value
        fecha2 = DateTimePicker2.Value
        Locbndrep = 1000
        eOpVentas = 1000
        FrmImprimirComision.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub MotivoComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MotivoComboBox.SelectedIndexChanged

    End Sub

    Private Sub frmReporteLlamadasTel_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        LlenaComboMotAtenTel()
        LimpiaRadioButton()
        LlenaComboTipoServicio()
        bandera = False
        GroupBox5.Enabled = False

    End Sub

    Private Sub TipoPersona2RadioButton_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TipoPersona2RadioButton.CheckedChanged
        'CheckBox2.Enabled = False
        'GroupBox1.Enabled = False
        GroupBox6.Enabled = False
        GroupBox5.Enabled = True
    End Sub

    Private Sub TipoPersona1RadioButton_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TipoPersona1RadioButton.CheckedChanged
        CheckBox1.Enabled = True
        'CheckBox2.Enabled = True
        'GroupBox1.Enabled = True
        GroupBox6.Enabled = True
        GroupBox5.Enabled = True
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged

    End Sub

    Private Sub Validar(ByVal caja As CheckBox)

        If CheckBox1.Checked = True And CheckBox1.Name <> caja.Name Then
            bandera = True
            Return
            ' ElseIf CheckBox2.Checked = True And CheckBox2.Name <> caja.Name Then
            bandera = True
        Else
            bandera = False
            Return
        End If

        Return
    End Sub
    Private Sub LimpiaRadioButton()

        TipoPersona1RadioButton.Checked = False
        TipoPersona2RadioButton.Checked = False
        'GroupBox1.Enabled = False
        GroupBox5.Enabled = False
        CheckBox1.Enabled = True
        'CheckBox2.Enabled = True

    End Sub

    Private Sub LlenaComboMotAtenTel()
        MotivoComboBox.DataSource = MotAtenTel.GetAll()
        opcionReporte = 1

    End Sub
    Private Sub LlenaComboTipoServicio()

        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder

        StrSQL.Append("EXEC MuestraTipSerPrincipal ")


        Dim DA As New SqlDataAdapter(StrSQL.ToString, CON)
        Dim DT As New DataTable
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.ConceptoComboBox.DataSource = BS.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub
End Class