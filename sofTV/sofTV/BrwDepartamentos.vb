﻿Public Class BrwDepartamentos

    'INSTANCIAMOS UNA VARIABLE DEL TIPO DE LA CLASE DE LOS DEPARTAMENTOS PARA HACER USO DE ELLA EN EL PROCESO DEL BROWSE
    Dim Depto As New ClassDepartamentos

    Private Sub BrwDepartamentos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        'SE LLENA EL DATA GRID (dgvDepartamentos) --LA OPCION 1 ES PARA LLENAR EL GRID-- (INICIO)
        llenaGrid(0, String.Empty, 1)
        'SE LLENA EL DATA GRID (dgvDepartamentos) --LA OPCION 1 ES PARA LLENAR EL GRID-- (FIN)
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    'SE MANDA LLENAR EL GRID; LLAMANDO LA FUNCIÓN QUE DEVUELVE UN DT DE LA CLASE DEPARTAMENTOS
    Private Sub llenaGrid(ByVal prmClvDepto As Long, ByVal prmDescripcion As String, ByVal prmOpcion As Integer)
        'SE VALIDA SI LA DESCRIPCIÓN VA VACÍA; SE MANDA LLENAR EL GRID CON TODA LA INFORMACIÓN --LA OPCIÓN 2 ES PARA BUSCAR POR DESCRIPCIÓN-- (INICIO)
        If prmOpcion = 2 Then
            If Len(prmDescripcion) = 0 Then
                prmOpcion = 1
                prmDescripcion = String.Empty
            End If
        End If
        'SE VALIDA SI LA DESCRIPCIÓN VA VACÍA; SE MANDA LLENAR EL GRID CON TODA LA INFORMACIÓN --LA OPCIÓN 2 ES PARA BUSCAR POR DESCRIPCIÓN-- (FIN)

        'LLENAMOS EL GRID CON LA ONFORMACIÓN SOLICITADA (INICIO)
        Me.dgvDepartamentos.DataSource = Depto.uspConsultaTblDepartamentos(prmClvDepto, prmDescripcion, prmOpcion)
        'LLENAMOS EL GRID CON LA ONFORMACIÓN SOLICITADA (FIN)
    End Sub

    Private Sub btnBuscaDescripcion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscaDescripcion.Click
        'SE LLENA EL DATA GRID (dgvDepartamentos) --LA OPCION 2 ES PARA LLENAR EL GRID FILTRANDO LA DESCRIPCIÓN-- (INICIO)
        llenaGrid(0, Me.txtDescripcion.Text, 2)
        'SE LLENA EL DATA GRID (dgvDepartamentos) --LA OPCION 2 ES PARA LLENAR EL GRID FILTRANDO LA DESCRIPCIÓN-- (FIN)
    End Sub

    'LLENAMOS LAS ETIQUETAS DE INFORMACIÓN EN EL BROWSE
    Private Sub llenaEtiquetas()
        Try
            Me.lblClvDepto.Text = Me.dgvDepartamentos.SelectedCells(0).Value.ToString
            Me.CMBLblDescripcion.Text = Me.dgvDepartamentos.SelectedCells(1).Value.ToString
        Catch ex As Exception

        End Try
    End Sub

    'MANDAMOS LAS VARIABLES REQUERIDAS AL FORMULARIO DE LOS DEPARTAMENTOS
    Private Sub mandaFrmDepartamentos(ByVal prmClvDepto As Long, ByVal prmDescripcion As String, ByVal prmOpcionAlta As String)
        Dim frmDeptos As New FrmDepartamentos

        frmDeptos.publicClvDepto = prmClvDepto
        frmDeptos.publicDescripcionDepto = prmDescripcion
        frmDeptos.publicOpcionDepto = prmOpcionAlta

        frmDeptos.ShowDialog()
    End Sub

    'MANDAMOS ELIMINAR EL DEPARTAMENTO SELECCIONADO
    Private Sub eliminaDepartamento(ByVal prmClvDepto As Long)
        If Depto.uspEliminaTblDepartamentos(prmClvDepto) = False Then
            MsgBox("El Departamento a Eliminar está Registrado en un Trabajo", MsgBoxStyle.Information)
            Exit Sub
        Else
            MsgBox("Registro Eliminado Satisfactoriamente", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        'SE MANDA LLAMAR EL FORMULARIO PARA DAR DE ALTA UN NUEVO REGISTRO --"N" NUEVO-- (INICIO)
        mandaFrmDepartamentos(0, "", "N")
        'SE MANDA LLAMAR EL FORMULARIO PARA DAR DE ALTA UN NUEVO REGISTRO --"N" NUEVO-- (FIN)

        'ACTUALIZAMOS EL GRID (INICIO)
        llenaGrid(0, String.Empty, 1)
        'ACTUALIZAMOS EL GRID (FIN)
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        If Me.dgvDepartamentos.Rows.Count > 0 Then
            If IsNumeric(Me.dgvDepartamentos.SelectedCells(0).Value) = 0 Then
                MsgBox("Seleccione un Registro Válido", MsgBoxStyle.Information)
                Exit Sub
            End If

            'SE MANDA LLAMAR EL FORMULARIO PARA MODIFICAR UN REGISTRO --"M" MODIFICAR-- (INICIO)
            mandaFrmDepartamentos(CLng(Me.dgvDepartamentos.SelectedCells(0).Value), Me.dgvDepartamentos.SelectedCells(1).Value.ToString, "M")
            'SE MANDA LLAMAR EL FORMULARIO PARA MODIFICAR UN REGISTRO --"M" MODIFICAR-- (FIN)

            'ACTUALIZAMOS EL GRID (INICIO)
            llenaGrid(0, String.Empty, 1)
            'ACTUALIZAMOS EL GRID (FIN)
        End If
    End Sub

    Private Sub btnConsulta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsulta.Click
        If Me.dgvDepartamentos.Rows.Count > 0 Then
            If IsNumeric(Me.dgvDepartamentos.SelectedCells(0).Value) = 0 Then
                MsgBox("Seleccione un Registro Válido", MsgBoxStyle.Information)
                Exit Sub
            End If

            'SE MANDA LLAMAR EL FORMULARIO PARA CONSULTAR UN REGISTRO --"C" CONSULTAR-- (INICIO)
            mandaFrmDepartamentos(CLng(Me.dgvDepartamentos.SelectedCells(0).Value), Me.dgvDepartamentos.SelectedCells(1).Value.ToString, "C")
            'SE MANDA LLAMAR EL FORMULARIO PARA CONSULTAR UN REGISTRO --"C" CONSULTAR-- (FIN)
        End If
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        If IsNumeric(Me.dgvDepartamentos.SelectedCells(0).Value) = 0 Then
            MsgBox("Seleccione un Registro Válido", MsgBoxStyle.Information)
            Exit Sub
        End If

        'SE MANDA ELIMINAR EL REGISTRO SELECCIONADO (INICIO)
        eliminaDepartamento(CLng(Me.dgvDepartamentos.SelectedCells(0).Value))
        'SE MANDA ELIMINAR EL REGISTRO SELECCIONADO (INICIO)

        'ACTUALIZAMOS EL GRID (INICIO)
        llenaGrid(0, String.Empty, 1)
        'ACTUALIZAMOS EL GRID (FIN)
    End Sub

    Private Sub dgvDepartamentos_CurrentCellChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvDepartamentos.CurrentCellChanged
        'SE MANDAN LLENAR LA ETIQUETAS CON EL VALOR ACTUAL SELECCIONADO DEL GRID
        llenaEtiquetas()
    End Sub
End Class