Imports System.Data.SqlClient

Module Module3

    Public guardabitacora As sofTV.ProcedimientosArnoldo2.Inserta_MovSistDataTable
    Public guardabitacorabuena As sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_MovSistTableAdapter

    Public Function TipoAccesoPerfiles(ByVal NomInterno As String, ByVal NomControl As String, ByVal clv_tipousuario As Integer) As Boolean 'BUSCA EN UN FORMULARIO
        Dim conexion As New SqlConnection(MiConexion)
        Dim Comando As New SqlCommand("TipoAccesoPerfiles", conexion)
        Comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@NomInterno", SqlDbType.VarChar, 100)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = NomInterno
        Comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@NomControl", SqlDbType.VarChar, 100)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = NomControl
        Comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@clv_tipousuario", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = clv_tipousuario
        Comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Valor", SqlDbType.VarChar, 10)
        parametro4.Direction = ParameterDirection.Output
        Comando.Parameters.Add(parametro4)

        Try
            conexion.Open()
            Comando.ExecuteNonQuery()
            TipoAccesoPerfiles = CBool(parametro4.Value)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Function

    Public Function TipoAccesoMenusPerfiles(ByVal clv_tipousuario As Integer, ByVal NomMenu As String) As Boolean
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("TipoAccesoMenusPerfiles", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@clv_tipousuario", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = clv_tipousuario
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@NomMenu", SqlDbType.VarChar, 250)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = NomMenu
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Valor", SqlDbType.VarChar, 10)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            TipoAccesoMenusPerfiles = CBool(parametro3.Value)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Function

    Public Function dameStatusOrdenQueja(ByVal prmClvOrden As Integer, ByVal prmTipoDescarga As String) As String
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("dameStatusOrdenQueja", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@clvOrden", prmClvOrden)
        CMD.Parameters.AddWithValue("@tipoDescarga", prmTipoDescarga)

        Dim READER As SqlDataReader

        Try
            CON.Open()

            READER = CMD.ExecuteReader()
            While (READER.Read)
                dameStatusOrdenQueja = READER(0).ToString
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Function
    Public Sub softv_BorraDescarga(ByVal prmClvOrden As Integer, ByVal prmTipoDescarga As String)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("softv_BorraDescarga", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@clvOrden", prmClvOrden)
        CMD.Parameters.AddWithValue("@tipoDescarga", prmTipoDescarga)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Public Sub colorea(ByVal formulario As Form, ByVal NomInterno As String) 'BUSCA EN UN FORMULARIO
        Try
            Dim boton As Button
            Dim panel As Panel
            Dim label As Label
            Dim split As SplitContainer
            Dim var As String
            Dim data As DataGridView
            Dim TAB As TabControl
            Dim Menupal As MenuStrip
            'Dim text As TextBox
            'Dim CHECK As CheckBox
            'Dim RADIO As RadioButton
            'Dim BN As BindingNavigator


            'nuevo = New sofTV.DataSetLidia.NUEVOControlDataTable
            'NuevoCtr = New sofTV.DataSetLidiaTableAdapters.NUEVOControlTableAdapter
            'borra = New sofTV.DataSetLidia.BORRARControlDataTable
            'borraCtr = New sofTV.DataSetLidiaTableAdapters.BORRARControlTableAdapter
            'busca = New sofTV.DataSetLidia.BUSCAControlDataTable
            'buscaCtr = New sofTV.DataSetLidiaTableAdapters.BUSCAControlTableAdapter
            'Dim CON30 As New SqlConnection(MiConexion)


            For Each ctl As Control In formulario.Controls
                'MsgBox(formulario.Controls.Count)
                'BOTONES--------------------------------------------------------
                var = Mid(ctl.Name, 1, 3)

                If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                    boton = New Button
                    boton = ctl
                    boton.BackColor = System.Drawing.Color.FromArgb(ColorBut)
                    boton.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
                    'boton.Enabled = TipoAccesoPerfiles(NomInterno, ctl.Name, GloTipoUsuario)
                    'If boton.Text <> "&Buscar" Then
                    '    'BUSCA PARA EVITAR REDUNDANCIA EN CONTROLES
                    '    'Using CON30 As New SqlConnection(MiConexion)
                    '    '    buscaCtr.Connection = CON30
                    '    '    buscaCtr.Fill(busca, 1, formulario.Name, boton.Text, boton.Name, salida)
                    '    'End Using
                    '    'If salida = 0 Then
                    '    '    Using CON30 As New SqlConnection(MiConexion)
                    '    '        NuevoCtr.Connection = CON30
                    '    '        NuevoCtr.Fill(nuevo, 1, formulario.Name, boton.Text, boton.Name, GloTipoUsuario, boton.Location.Y, boton.Location.X)
                    '    '    End Using
                    '    '    'ElseIf salida > 0 Then
                    '    '    '    borraCtr.Fill(borra, boton.Text, boton.Name, formulario.Name, 1)
                    '    'End If
                    'End If
                    boton = Nothing
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.MenuStrip) Then
                    'MENU-------------------------------------------------------
                    Menupal = New MenuStrip
                    Menupal = ctl
                    Menupal.BackColor = System.Drawing.Color.FromArgb(ColorMenu)
                    RecorrerEstructuraMenu(Menupal)
                    Menupal = Nothing
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then

                    'SPLIT -----------------------------------------------------------

                    split = New SplitContainer
                    split = ctl
                    'split.Panel1.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                    'split.Panel2.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                    bwrpanel(split.Panel1, formulario)
                    bwrpanel(split.Panel2, formulario)
                    split = Nothing
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) And ctl.BackColor <> Color.WhiteSmoke Then
                    'PANEL---------------------------------------------------------------
                    panel = New Panel
                    panel = ctl
                    panel.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                    panel.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                    bwrpanel(panel, formulario)
                    panel = Nothing

                ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) And ctl.BackColor = Color.WhiteSmoke Then
                    'PANEL QUE NO CAMBIA-----------------------------------------------------------------
                    panel = New Panel
                    panel = ctl
                    bwrpanel(panel, formulario)
                    panel = Nothing
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) And var = "CMB" Then

                    'PANEL CAMBIA--------------------------------------------------------------------

                    panel = New Panel
                    panel = ctl
                    panel.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                    panel = Nothing
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.Label) And var <> "CMB" And ctl.BackColor <> Color.WhiteSmoke Then

                    'LABEL QUE NO CAMBIA----------------------------------------------------

                    label = New Label
                    label = ctl
                    label.ForeColor = System.Drawing.Color.FromArgb(ColorLetraLabel)
                    label.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                    label = Nothing
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.Label) And var = "CMB" Then

                    'LABEL QUE CAMBIAN----------------------------------------------------------

                    label = New Label
                    label = ctl
                    label.ForeColor = Color.Black
                    label.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                    label = Nothing

                ElseIf ctl.GetType Is GetType(System.Windows.Forms.Label) And var = "red" Then
                    'LABEL QUE CAMBIAN----------------------------------------------------------

                    label = New Label
                    label = ctl
                    label.ForeColor = Color.Red
                    label.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                    label = Nothing
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.DataGridView) Then


                    'DATA GRID----------------------------------------------------------------

                    data = New DataGridView
                    data = ctl
                    data.ColumnHeadersDefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                    'data.ColumnHeadersDefaultCellStyle.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                    data.BackgroundColor = System.Drawing.Color.FromArgb(ColorGrid)
                    'BUSCA PARA EVITAR REDUNDANCIA EN CONTROLES
                    'Using CON30 As New SqlConnection(MiConexion)
                    '    buscaCtr.Connection = CON30
                    '    buscaCtr.Fill(busca, 1, formulario.Name, data.Name, data.Name, salida)
                    'End Using
                    'If salida = 0 Then
                    '    Using CON30 As New SqlConnection(MiConexion)
                    '        NuevoCtr.Connection = CON30
                    '        NuevoCtr.Fill(nuevo, 1, formulario.Name, data.Name, data.Name, GloTipoUsuario, data.Location.Y, data.Location.X)
                    '    End Using
                    '    'ElseIf salida > 0 Then
                    '    '    borraCtr.Fill(borra, data.Name, data.Name, formulario.Name, 1)
                    'End If
                    data = Nothing
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabControl) Then

                    'TAB CONTROL----------------------------------------------------------------

                    TAB = New TabControl
                    TAB = ctl
                    TAB.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                    tabbwr(TAB, formulario)
                    TAB = Nothing
                End If
                '''PARA LLENAR NOMBRES DE CONTROLES------------------------------------------------
                'If ctl.GetType Is GetType(System.Windows.Forms.TextBox) Then
                '    text = New TextBox
                '    text = ctl
                '    'BUSCA PARA EVITAR REDUNDANCIA EN CONTROLES
                '    Using CON30 As New SqlConnection(MiConexion)
                '        buscaCtr.Connection = CON30
                '        buscaCtr.Fill(busca, 1, formulario.Name, text.Name, text.Name, salida)
                '    End Using
                '    If salida = 0 Then
                '        Using CON30 As New SqlConnection(MiConexion)
                '            NuevoCtr.Connection = CON30
                '            NuevoCtr.Fill(nuevo, 1, formulario.Name, text.Name, text.Name, GloTipoUsuario, text.Location.Y, text.Location.X)
                '        End Using
                '        'ElseIf salida > 0 Then
                '        '    borraCtr.Fill(borra, text.Text, text.Name, formulario.Name, 1)
                '    End If
                '    text = Nothing
                'End If

                'If ctl.GetType Is GetType(System.Windows.Forms.CheckBox) Then
                '    CHECK = New CheckBox
                '    CHECK = ctl
                '    'BUSCA PARA EVITAR REDUNDANCIA EN CONTROLES
                '    Using CON30 As New SqlConnection(MiConexion)
                '        buscaCtr.Connection = CON30
                '        buscaCtr.Fill(busca, 1, formulario.Name, CHECK.Name, CHECK.Name, salida)
                '    End Using
                '    If salida = 0 Then
                '        Using CON30 As New SqlConnection(MiConexion)
                '            NuevoCtr.Connection = CON30
                '            NuevoCtr.Fill(nuevo, 1, formulario.Name, CHECK.Name, CHECK.Name, GloTipoUsuario, CHECK.Location.Y, CHECK.Location.X)
                '        End Using
                '        'ElseIf salida > 0 Then
                '        '    borraCtr.Fill(borra, CHECK.Name, CHECK.Name, formulario.Name, 1)
                '    End If
                '    CHECK = Nothing
                'End If
                'If ctl.GetType Is GetType(System.Windows.Forms.RadioButton) Then
                '    RADIO = New RadioButton
                '    RADIO = ctl
                '    'BUSCA PARA EVITAR REDUNDANCIA EN CONTROLES
                '    Using CON30 As New SqlConnection(MiConexion)
                '        buscaCtr.Connection = CON30
                '        buscaCtr.Fill(busca, 1, formulario.Name, RADIO.Name, RADIO.Name, salida)
                '    End Using
                '    If salida = 0 Then
                '        Using CON30 As New SqlConnection(MiConexion)
                '            NuevoCtr.Connection = CON30
                '            NuevoCtr.Fill(nuevo, 1, formulario.Name, RADIO.Name, RADIO.Name, GloTipoUsuario, RADIO.Location.Y, RADIO.Location.X)
                '        End Using
                '        'ElseIf salida > 0 Then
                '        '    borraCtr.Fill(borra, RADIO.Text, RADIO.Name, formulario.Name, 1)
                '    End If
                '    RADIO = Nothing
                'End If

                'If ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                '    BN = New BindingNavigator
                '    BN = ctl
                '    For Each BIN As ToolStripButton In BN.Items
                '        NuevoCtr.Fill(nuevo, 1, formulario.Name, BIN.Text, BIN.Name, GloTipoUsuario, BIN.Location.Y, BIN.Location.X)
                '    Next
                '    BN = Nothing
                'End If
                '--------------------------------------------------------------------------------------

            Next
            formulario.BackColor = System.Drawing.Color.FromArgb(ColorForm)
            formulario.ForeColor = System.Drawing.Color.FromArgb(ColorLetraForm)
            'CON30.Close()
        Catch ex As ApplicationException
            'MsgBox(ex.Message)
        End Try
    End Sub

    'Public Sub colorea(ByVal formulario As Form) 'BUSCA EN UN FORMULARIO
    '    Try
    '        Dim boton As Button
    '        Dim panel As Panel
    '        Dim label As Label
    '        Dim split As SplitContainer
    '        Dim var As String
    '        Dim data As DataGridView
    '        Dim TAB As TabControl
    '        Dim Menupal As MenuStrip
    '        Dim text As TextBox
    '        Dim CHECK As CheckBox
    '        Dim RADIO As RadioButton
    '        Dim BN As BindingNavigator


    '        nuevo = New sofTV.DataSetLidia.NUEVOControlDataTable
    '        NuevoCtr = New sofTV.DataSetLidiaTableAdapters.NUEVOControlTableAdapter
    '        borra = New sofTV.DataSetLidia.BORRARControlDataTable
    '        borraCtr = New sofTV.DataSetLidiaTableAdapters.BORRARControlTableAdapter
    '        busca = New sofTV.DataSetLidia.BUSCAControlDataTable
    '        buscaCtr = New sofTV.DataSetLidiaTableAdapters.BUSCAControlTableAdapter
    '        'Dim CON30 As New SqlConnection(MiConexion)


    '        For Each ctl As Control In formulario.Controls
    '            'BOTONES--------------------------------------------------------
    '            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
    '                boton = New Button
    '                boton = ctl
    '                boton.BackColor = System.Drawing.Color.FromArgb(ColorBut)
    '                boton.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
    '                If boton.Text <> "&Buscar" Then
    '                    'BUSCA PARA EVITAR REDUNDANCIA EN CONTROLES
    '                    Using CON30 As New SqlConnection(MiConexion)
    '                        buscaCtr.Connection = CON30
    '                        buscaCtr.Fill(busca, 1, formulario.Name, boton.Text, boton.Name, salida)
    '                    End Using
    '                    If salida = 0 Then
    '                        Using CON30 As New SqlConnection(MiConexion)
    '                            NuevoCtr.Connection = CON30
    '                            NuevoCtr.Fill(nuevo, 1, formulario.Name, boton.Text, boton.Name, GloTipoUsuario, boton.Location.Y, boton.Location.X)
    '                        End Using
    '                        'ElseIf salida > 0 Then
    '                        '    borraCtr.Fill(borra, boton.Text, boton.Name, formulario.Name, 1)
    '                    End If
    '                End If
    '                boton = Nothing
    '            End If

    '            'MENU------------------------------------------------------------

    '            If ctl.GetType Is GetType(System.Windows.Forms.MenuStrip) Then
    '                Menupal = New MenuStrip
    '                Menupal = ctl
    '                Menupal.BackColor = System.Drawing.Color.FromArgb(ColorMenu)
    '                RecorrerEstructuraMenu(Menupal)
    '                Menupal = Nothing
    '            End If

    '            'SPLIT -----------------------------------------------------------
    '            If ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
    '                split = New SplitContainer
    '                split = ctl
    '                'split.Panel1.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
    '                'split.Panel2.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
    '                bwrpanel(split.Panel1, formulario)
    '                bwrpanel(split.Panel2, formulario)
    '                split = Nothing
    '            End If

    '            'PANEL---------------------------------------------------------------
    '            If ctl.GetType Is GetType(System.Windows.Forms.Panel) And ctl.BackColor <> Color.WhiteSmoke Then
    '                panel = New Panel
    '                panel = ctl
    '                panel.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
    '                panel.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
    '                bwrpanel(panel, formulario)
    '                panel = Nothing

    '            End If

    '            'PANEL QUE NO CAMBIA-----------------------------------------------------------------

    '            If ctl.GetType Is GetType(System.Windows.Forms.Panel) And ctl.BackColor = Color.WhiteSmoke Then
    '                panel = New Panel
    '                panel = ctl
    '                bwrpanel(panel, formulario)
    '                panel = Nothing
    '            End If
    '            var = Mid(ctl.Name, 1, 3)

    '            'PANEL CAMBIA--------------------------------------------------------------------
    '            If ctl.GetType Is GetType(System.Windows.Forms.Panel) And var = "CMB" Then
    '                panel = New Panel
    '                panel = ctl
    '                panel.BackColor = System.Drawing.Color.FromArgb(ColorForm)
    '                panel = Nothing
    '            End If

    '            'LABEL QUE NO CAMBIA----------------------------------------------------
    '            If ctl.GetType Is GetType(System.Windows.Forms.Label) And var <> "CMB" And ctl.BackColor <> Color.WhiteSmoke Then
    '                label = New Label
    '                label = ctl
    '                label.ForeColor = System.Drawing.Color.FromArgb(ColorLetraLabel)
    '                label.BackColor = System.Drawing.Color.FromArgb(ColorForm)
    '                label = Nothing
    '            End If

    '            'LABEL QUE CAMBIAN----------------------------------------------------------
    '            If ctl.GetType Is GetType(System.Windows.Forms.Label) And var = "CMB" Then
    '                label = New Label
    '                label = ctl
    '                label.ForeColor = Color.Black
    '                label.BackColor = System.Drawing.Color.FromArgb(ColorForm)
    '                label = Nothing

    '            End If
    '            'LABEL QUE CAMBIAN----------------------------------------------------------
    '            If ctl.GetType Is GetType(System.Windows.Forms.Label) And var = "red" Then
    '                label = New Label
    '                label = ctl
    '                label.ForeColor = Color.Red
    '                label.BackColor = System.Drawing.Color.FromArgb(ColorForm)
    '                label = Nothing
    '            End If


    '            'DATA GRID----------------------------------------------------------------
    '            If ctl.GetType Is GetType(System.Windows.Forms.DataGridView) Then
    '                data = New DataGridView
    '                data = ctl
    '                data.ColumnHeadersDefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
    '                'data.ColumnHeadersDefaultCellStyle.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
    '                data.BackgroundColor = System.Drawing.Color.FromArgb(ColorGrid)
    '                'BUSCA PARA EVITAR REDUNDANCIA EN CONTROLES
    '                Using CON30 As New SqlConnection(MiConexion)
    '                    buscaCtr.Connection = CON30
    '                    buscaCtr.Fill(busca, 1, formulario.Name, data.Name, data.Name, salida)
    '                End Using
    '                If salida = 0 Then
    '                    Using CON30 As New SqlConnection(MiConexion)
    '                        NuevoCtr.Connection = CON30
    '                        NuevoCtr.Fill(nuevo, 1, formulario.Name, data.Name, data.Name, GloTipoUsuario, data.Location.Y, data.Location.X)
    '                    End Using
    '                    'ElseIf salida > 0 Then
    '                    '    borraCtr.Fill(borra, data.Name, data.Name, formulario.Name, 1)
    '                End If
    '                data = Nothing
    '            End If

    '            'TAB CONTROL----------------------------------------------------------------
    '            If ctl.GetType Is GetType(System.Windows.Forms.TabControl) Then
    '                TAB = New TabControl
    '                TAB = ctl
    '                TAB.BackColor = System.Drawing.Color.FromArgb(ColorForm)
    '                tabbwr(TAB, formulario)
    '                TAB = Nothing
    '            End If
    '            ''PARA LLENAR NOMBRES DE CONTROLES------------------------------------------------
    '            If ctl.GetType Is GetType(System.Windows.Forms.TextBox) Then
    '                text = New TextBox
    '                text = ctl
    '                'BUSCA PARA EVITAR REDUNDANCIA EN CONTROLES
    '                Using CON30 As New SqlConnection(MiConexion)
    '                    buscaCtr.Connection = CON30
    '                    buscaCtr.Fill(busca, 1, formulario.Name, text.Name, text.Name, salida)
    '                End Using
    '                If salida = 0 Then
    '                    Using CON30 As New SqlConnection(MiConexion)
    '                        NuevoCtr.Connection = CON30
    '                        NuevoCtr.Fill(nuevo, 1, formulario.Name, text.Name, text.Name, GloTipoUsuario, text.Location.Y, text.Location.X)
    '                    End Using
    '                    'ElseIf salida > 0 Then
    '                    '    borraCtr.Fill(borra, text.Text, text.Name, formulario.Name, 1)
    '                End If
    '                text = Nothing
    '            End If

    '            If ctl.GetType Is GetType(System.Windows.Forms.CheckBox) Then
    '                CHECK = New CheckBox
    '                CHECK = ctl
    '                'BUSCA PARA EVITAR REDUNDANCIA EN CONTROLES
    '                Using CON30 As New SqlConnection(MiConexion)
    '                    buscaCtr.Connection = CON30
    '                    buscaCtr.Fill(busca, 1, formulario.Name, CHECK.Name, CHECK.Name, salida)
    '                End Using
    '                If salida = 0 Then
    '                    Using CON30 As New SqlConnection(MiConexion)
    '                        NuevoCtr.Connection = CON30
    '                        NuevoCtr.Fill(nuevo, 1, formulario.Name, CHECK.Name, CHECK.Name, GloTipoUsuario, CHECK.Location.Y, CHECK.Location.X)
    '                    End Using
    '                    'ElseIf salida > 0 Then
    '                    '    borraCtr.Fill(borra, CHECK.Name, CHECK.Name, formulario.Name, 1)
    '                End If
    '                CHECK = Nothing
    '            End If
    '            If ctl.GetType Is GetType(System.Windows.Forms.RadioButton) Then
    '                RADIO = New RadioButton
    '                RADIO = ctl
    '                'BUSCA PARA EVITAR REDUNDANCIA EN CONTROLES
    '                Using CON30 As New SqlConnection(MiConexion)
    '                    buscaCtr.Connection = CON30
    '                    buscaCtr.Fill(busca, 1, formulario.Name, RADIO.Name, RADIO.Name, salida)
    '                End Using
    '                If salida = 0 Then
    '                    Using CON30 As New SqlConnection(MiConexion)
    '                        NuevoCtr.Connection = CON30
    '                        NuevoCtr.Fill(nuevo, 1, formulario.Name, RADIO.Name, RADIO.Name, GloTipoUsuario, RADIO.Location.Y, RADIO.Location.X)
    '                    End Using
    '                    'ElseIf salida > 0 Then
    '                    '    borraCtr.Fill(borra, RADIO.Text, RADIO.Name, formulario.Name, 1)
    '                End If
    '                RADIO = Nothing
    '            End If

    '            'If ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
    '            '    BN = New BindingNavigator
    '            '    BN = ctl
    '            '    For Each BIN As ToolStripButton In BN.Items
    '            '        NuevoCtr.Fill(nuevo, 1, formulario.Name, BIN.Text, BIN.Name, GloTipoUsuario, BIN.Location.Y, BIN.Location.X)
    '            '    Next
    '            '    BN = Nothing
    '            'End If
    '            '--------------------------------------------------------------------------------------
    '        Next
    '        formulario.BackColor = System.Drawing.Color.FromArgb(ColorForm)
    '        formulario.ForeColor = System.Drawing.Color.FromArgb(ColorLetraForm)
    '        'CON30.Close()
    '    Catch ex As ApplicationException
    '        MsgBox(ex.Message)
    '    End Try
    'End Sub


    Public Sub ColoreaFrm(ByVal Formulario As Form)
        FrmClientes.Button1.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmClientes.Button1.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmClientes.Button2.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmClientes.Button2.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmClientes.Button3.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmClientes.Button3.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmClientes.Button4.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmClientes.Button4.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmClientes.Button29.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmClientes.Button29.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)

        FrmClientes.Button5.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmClientes.Button5.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmClientes.Button6.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmClientes.Button6.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmClientes.Button7.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmClientes.Button7.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmClientes.Button8.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmClientes.Button8.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)

        'FrmClientes.Button9.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        'FrmClientes.Button9.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmClientes.Button10.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmClientes.Button10.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmClientes.Button11.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmClientes.Button11.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmClientes.Button12.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmClientes.Button12.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)

        FrmClientes.Button13.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmClientes.Button13.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmClientes.Button14.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmClientes.Button14.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmClientes.Button15.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmClientes.Button15.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        'FrmClientes.Button16.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        'FrmClientes.Button16.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)

        'FrmClientes.Button18.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        'FrmClientes.Button18.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        'FrmClientes.Button19.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        'FrmClientes.Button19.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmClientes.Button20.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmClientes.Button20.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmClientes.Button21.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmClientes.Button21.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        'FrmClientes.Button22.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        'FrmClientes.Button22.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        'FrmClientes.Button23.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        'FrmClientes.Button23.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmClientes.Button24.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmClientes.Button24.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmClientes.Button17.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmClientes.Button17.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        'FrmClientes.Button25.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        'FrmClientes.Button25.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        'FrmClientes.Button26.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        'FrmClientes.Button26.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmClientes.Button27.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmClientes.Button27.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)

        FrmClientes.Button28.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmClientes.Button28.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)

        FrmClientes.Button30.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmClientes.Button30.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)

        FrmClientes.Button31.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmClientes.Button31.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)

        FrmClientes.BtnEstadoDeCuenta.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmClientes.BtnEstadoDeCuenta.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)


        'FrmClientes.CMBTextBox1.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        'FrmClientes.CMBTextBox1.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        'FrmClientes.CMBTextBox2.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        'FrmClientes.CMBTextBox2.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        'FrmClientes.CMBTextBox3.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        'FrmClientes.CMBTextBox3.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        'FrmClientes.CMBTextBox4.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        'FrmClientes.CMBTextBox4.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)

        FrmClientes.CMBTextBox5.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmClientes.CMBTextBox5.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmClientes.CMBTextBox28.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmClientes.CMBTextBox28.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        'FrmClientes.Label11.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        'FrmClientes.Label11.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        'FrmClientes.Label37.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        'FrmClientes.Label37.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)

        'FrmClientes.Label19.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        'FrmClientes.Label19.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        'FrmClientes.Label20.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        'FrmClientes.Label20.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)

        FrmClientes.Label38.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmClientes.Label38.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmClientes.Label46.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmClientes.Label46.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmClientes.Label47.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmClientes.Label47.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        'FrmClientes.ULTIMO_ANIOLabel.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        'FrmClientes.ULTIMO_ANIOLabel.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        'FrmClientes.ULTIMO_MESLabel.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        'FrmClientes.ULTIMO_MESLabel.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)


    End Sub

    Public Sub ColoreaServicios()
        FrmServicios.Button5.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmServicios.Button5.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmServicios.CMBTextBox1.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmServicios.CMBTextBox1.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmServicios.CMBTextBox2.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmServicios.CMBTextBox2.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmServicios.CMBTextBox28.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmServicios.CMBTextBox28.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmServicios.CMBTextBox3.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmServicios.CMBTextBox3.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmServicios.CMBTextBox4.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmServicios.CMBTextBox4.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmServicios.CMBTextBox5.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmServicios.CMBTextBox5.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)

        FrmServicios.CMBTextBox6.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmServicios.CMBTextBox6.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)

        FrmServicios.CMBTextBox7.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmServicios.CMBTextBox7.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)

        FrmServicios.Cancelar1.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmServicios.Cancelar1.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)

        FrmServicios.Agregar1.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmServicios.Agregar1.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)

        FrmServicios.Modificar1.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmServicios.Modificar1.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)

        FrmServicios.Eliminar1.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmServicios.Eliminar1.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)

        FrmServicios.Guardar1.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmServicios.Guardar1.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)

        FrmServicios.CMBLabel10.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmServicios.CMBLabel10.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)

        FrmServicios.TextBox1.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmServicios.TextBox1.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)

        FrmServicios.CMBSale_en_CarteraLabel.BackColor = System.Drawing.Color.FromArgb(ColorForm)
        FrmServicios.CMBSale_en_CarteraLabel.ForeColor = Color.Black
        FrmServicios.CMBAplicanComLabel.BackColor = System.Drawing.Color.FromArgb(ColorForm)
        FrmServicios.CMBAplicanComLabel.ForeColor = Color.Black

        FrmServicios.CMBEs_PrincipalLabel.BackColor = System.Drawing.Color.FromArgb(ColorForm)
        FrmServicios.CMBEs_PrincipalLabel.ForeColor = Color.Black

        FrmServicios.CMBGenera_OrdenLabel.BackColor = System.Drawing.Color.FromArgb(ColorForm)

        FrmServicios.CMBGenera_OrdenLabel.ForeColor = Color.Black
        FrmServicios.DataGridView1.ColumnHeadersDefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
        FrmServicios.DataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(ColorGrid)

        FrmServicios.CONGeneralDescuentoNetDataGridView.ColumnHeadersDefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
        FrmServicios.CONGeneralDescuentoNetDataGridView.BackgroundColor = System.Drawing.Color.FromArgb(ColorGrid)
        FrmServicios.Dame_ContratacionParcialDataGridView.ColumnHeadersDefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
        FrmServicios.Dame_ContratacionParcialDataGridView.BackgroundColor = System.Drawing.Color.FromArgb(ColorGrid)


    End Sub
    Public Sub coloreaSer_Tel()
        FrmServiciosTelefonia.Button5.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmServiciosTelefonia.Button5.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmServiciosTelefonia.CMBTextBox3.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmServiciosTelefonia.CMBTextBox3.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmServiciosTelefonia.TextBox13.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmServiciosTelefonia.TextBox13.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        'FrmServiciosTelefonia.TextBox16.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        'FrmServiciosTelefonia.TextBox16.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        'FrmServiciosTelefonia.TextBox19.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        'FrmServiciosTelefonia.TextBox19.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmServiciosTelefonia.CMBTextBox7.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmServiciosTelefonia.CMBTextBox7.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        'FrmServiciosTelefonia.CMBTextBox11.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        'FrmServiciosTelefonia.CMBTextBox11.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        'FrmServiciosTelefonia.CMBTextBox12.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        'FrmServiciosTelefonia.CMBTextBox12.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmServiciosTelefonia.TextBox6.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmServiciosTelefonia.TextBox6.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmServiciosTelefonia.CMBTextBox4.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmServiciosTelefonia.CMBTextBox4.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmServiciosTelefonia.CMBTextBox5.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmServiciosTelefonia.CMBTextBox5.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmServiciosTelefonia.CMBTextBox6.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmServiciosTelefonia.CMBTextBox6.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmServiciosTelefonia.CMBTextBoxDias.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmServiciosTelefonia.CMBTextBoxDias.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmServiciosTelefonia.CMBTextBox9.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmServiciosTelefonia.CMBTextBox9.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmServiciosTelefonia.CMBTextBox28.BackColor = System.Drawing.Color.FromArgb(ColorBut)
        FrmServiciosTelefonia.CMBTextBox28.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
        FrmServiciosTelefonia.CMBSale_en_CarteraLabel.BackColor = System.Drawing.Color.FromArgb(ColorForm)
        FrmServiciosTelefonia.CMBSale_en_CarteraLabel.ForeColor = Color.Black
        FrmServiciosTelefonia.CMBAplicanComLabel.BackColor = System.Drawing.Color.FromArgb(ColorForm)
        FrmServiciosTelefonia.CMBAplicanComLabel.ForeColor = Color.Black
        FrmServiciosTelefonia.CMBEs_PrincipalLabel.BackColor = System.Drawing.Color.FromArgb(ColorForm)
        FrmServiciosTelefonia.CMBEs_PrincipalLabel.ForeColor = Color.Black
        FrmServiciosTelefonia.CMBGenera_OrdenLabel.BackColor = System.Drawing.Color.FromArgb(ColorForm)
        FrmServiciosTelefonia.CMBGenera_OrdenLabel.ForeColor = Color.Black
        FrmServiciosTelefonia.Consultar_Rel_ServiciosDigitales_Tel_TMPDataGridView.ColumnHeadersDefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
        FrmServiciosTelefonia.Consultar_Rel_ServiciosDigitales_Tel_TMPDataGridView.BackgroundColor = System.Drawing.Color.FromArgb(ColorGrid)
       

    End Sub

End Module
