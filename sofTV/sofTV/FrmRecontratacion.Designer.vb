﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRecontratacion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmRecontratacion))
        Me.tcServicios = New System.Windows.Forms.TabControl()
        Me.tpDIG = New System.Windows.Forms.TabPage()
        Me.lblDIG = New System.Windows.Forms.Label()
        Me.panelDIG = New System.Windows.Forms.Panel()
        Me.cmbTipoAparatoDth = New System.Windows.Forms.ComboBox()
        Me.lblTipoAparatoDth = New System.Windows.Forms.Label()
        Me.btnDIGCancelar = New System.Windows.Forms.Button()
        Me.btnDIGAceptar = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.comboSerDIG = New System.Windows.Forms.ComboBox()
        Me.bnDIGServicio = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbEliminarSDIG = New System.Windows.Forms.ToolStripButton()
        Me.tsbAgregarSDIG = New System.Windows.Forms.ToolStripButton()
        Me.bnDIGTarjeta = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbEliminarTDIG = New System.Windows.Forms.ToolStripButton()
        Me.tsbAgregarTDIG = New System.Windows.Forms.ToolStripButton()
        Me.tvDIG = New System.Windows.Forms.TreeView()
        Me.tpNET = New System.Windows.Forms.TabPage()
        Me.lblNET = New System.Windows.Forms.Label()
        Me.panelCNET = New System.Windows.Forms.Panel()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.comboTipoSer = New System.Windows.Forms.ComboBox()
        Me.btnCNETCancelar = New System.Windows.Forms.Button()
        Me.btnCNETAceptar = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.comboTipoAs = New System.Windows.Forms.ComboBox()
        Me.comboTipoCa = New System.Windows.Forms.ComboBox()
        Me.panelSNET = New System.Windows.Forms.Panel()
        Me.btnSNETCancelar = New System.Windows.Forms.Button()
        Me.btnSNETAceptar = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.comboSerNET = New System.Windows.Forms.ComboBox()
        Me.bnNETServicio = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbEliminarSNET = New System.Windows.Forms.ToolStripButton()
        Me.tsbAgregarSNET = New System.Windows.Forms.ToolStripButton()
        Me.bnNETCablemodem = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbEliminarNET = New System.Windows.Forms.ToolStripButton()
        Me.tsbAgregarNET = New System.Windows.Forms.ToolStripButton()
        Me.tvNET = New System.Windows.Forms.TreeView()
        Me.tpTV = New System.Windows.Forms.TabPage()
        Me.lblTV = New System.Windows.Forms.Label()
        Me.panelTV = New System.Windows.Forms.Panel()
        Me.btnTVCancelar = New System.Windows.Forms.Button()
        Me.btnTVAceptar = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.comboSerTV = New System.Windows.Forms.ComboBox()
        Me.txtConPago = New System.Windows.Forms.TextBox()
        Me.txtSinPago = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.bnTV = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbEliminarTV = New System.Windows.Forms.ToolStripButton()
        Me.tsbAgregarTV = New System.Windows.Forms.ToolStripButton()
        Me.tvTV = New System.Windows.Forms.TreeView()
        Me.tpDIGCable = New System.Windows.Forms.TabPage()
        Me.bnDIGCableServicio = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbEliminarSDIGCable = New System.Windows.Forms.ToolStripButton()
        Me.tsbAgregarSDIGCable = New System.Windows.Forms.ToolStripButton()
        Me.bnDIGCableTarjeta = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbEliminarTDIGCable = New System.Windows.Forms.ToolStripButton()
        Me.tsbAgregarTDIGCable = New System.Windows.Forms.ToolStripButton()
        Me.lblDIGCable = New System.Windows.Forms.Label()
        Me.panelDIGCable = New System.Windows.Forms.Panel()
        Me.cmbTipoAparatoCable = New System.Windows.Forms.ComboBox()
        Me.lblTipoAparatoCable = New System.Windows.Forms.Label()
        Me.btnDIGCableCancelar = New System.Windows.Forms.Button()
        Me.btnDIGCableAceptar = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.comboSerDIGCable = New System.Windows.Forms.ComboBox()
        Me.tvDIGCable = New System.Windows.Forms.TreeView()
        Me.btnNETCancelar = New System.Windows.Forms.Button()
        Me.btnNETAceptar = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.comboSerNET2023 = New System.Windows.Forms.ComboBox()
        Me.CMBLabel5 = New System.Windows.Forms.Label()
        Me.CMBGroupBox1 = New System.Windows.Forms.GroupBox()
        Me.CheckBoxSoloInternet = New System.Windows.Forms.CheckBox()
        Me.LabelCelular = New System.Windows.Forms.Label()
        Me.LabelTelefono = New System.Windows.Forms.Label()
        Me.LabelCiudad = New System.Windows.Forms.Label()
        Me.LabelColonia = New System.Windows.Forms.Label()
        Me.LabelNumero = New System.Windows.Forms.Label()
        Me.LabelCalle = New System.Windows.Forms.Label()
        Me.LabelNombre = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.TextBoxContrato = New System.Windows.Forms.TextBox()
        Me.ButtonBuscar = New System.Windows.Forms.Button()
        Me.bnRecontratar = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbRecon = New System.Windows.Forms.ToolStripButton()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter5 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter6 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter7 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter8 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.tcServicios.SuspendLayout()
        Me.tpDIG.SuspendLayout()
        Me.panelDIG.SuspendLayout()
        CType(Me.bnDIGServicio, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnDIGServicio.SuspendLayout()
        CType(Me.bnDIGTarjeta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnDIGTarjeta.SuspendLayout()
        Me.tpNET.SuspendLayout()
        Me.panelCNET.SuspendLayout()
        Me.panelSNET.SuspendLayout()
        CType(Me.bnNETServicio, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnNETServicio.SuspendLayout()
        CType(Me.bnNETCablemodem, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnNETCablemodem.SuspendLayout()
        Me.tpTV.SuspendLayout()
        Me.panelTV.SuspendLayout()
        CType(Me.bnTV, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnTV.SuspendLayout()
        Me.tpDIGCable.SuspendLayout()
        CType(Me.bnDIGCableServicio, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnDIGCableServicio.SuspendLayout()
        CType(Me.bnDIGCableTarjeta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnDIGCableTarjeta.SuspendLayout()
        Me.panelDIGCable.SuspendLayout()
        Me.CMBGroupBox1.SuspendLayout()
        CType(Me.bnRecontratar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnRecontratar.SuspendLayout()
        Me.SuspendLayout()
        '
        'tcServicios
        '
        Me.tcServicios.Controls.Add(Me.tpDIG)
        Me.tcServicios.Controls.Add(Me.tpNET)
        Me.tcServicios.Controls.Add(Me.tpTV)
        Me.tcServicios.Controls.Add(Me.tpDIGCable)
        Me.tcServicios.Enabled = False
        Me.tcServicios.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tcServicios.Location = New System.Drawing.Point(63, 293)
        Me.tcServicios.Name = "tcServicios"
        Me.tcServicios.SelectedIndex = 0
        Me.tcServicios.Size = New System.Drawing.Size(680, 368)
        Me.tcServicios.TabIndex = 0
        '
        'tpDIG
        '
        Me.tpDIG.Controls.Add(Me.lblDIG)
        Me.tpDIG.Controls.Add(Me.panelDIG)
        Me.tpDIG.Controls.Add(Me.bnDIGServicio)
        Me.tpDIG.Controls.Add(Me.bnDIGTarjeta)
        Me.tpDIG.Controls.Add(Me.tvDIG)
        Me.tpDIG.Location = New System.Drawing.Point(4, 24)
        Me.tpDIG.Name = "tpDIG"
        Me.tpDIG.Padding = New System.Windows.Forms.Padding(3)
        Me.tpDIG.Size = New System.Drawing.Size(672, 340)
        Me.tpDIG.TabIndex = 2
        Me.tpDIG.Text = "Tv Digital DTH"
        Me.tpDIG.UseVisualStyleBackColor = True
        '
        'lblDIG
        '
        Me.lblDIG.ForeColor = System.Drawing.Color.Red
        Me.lblDIG.Location = New System.Drawing.Point(3, 313)
        Me.lblDIG.Name = "lblDIG"
        Me.lblDIG.Size = New System.Drawing.Size(666, 24)
        Me.lblDIG.TabIndex = 18
        '
        'panelDIG
        '
        Me.panelDIG.BackColor = System.Drawing.Color.WhiteSmoke
        Me.panelDIG.Controls.Add(Me.cmbTipoAparatoDth)
        Me.panelDIG.Controls.Add(Me.lblTipoAparatoDth)
        Me.panelDIG.Controls.Add(Me.btnDIGCancelar)
        Me.panelDIG.Controls.Add(Me.btnDIGAceptar)
        Me.panelDIG.Controls.Add(Me.Label9)
        Me.panelDIG.Controls.Add(Me.comboSerDIG)
        Me.panelDIG.Location = New System.Drawing.Point(97, 89)
        Me.panelDIG.Name = "panelDIG"
        Me.panelDIG.Size = New System.Drawing.Size(472, 178)
        Me.panelDIG.TabIndex = 16
        Me.panelDIG.Visible = False
        '
        'cmbTipoAparatoDth
        '
        Me.cmbTipoAparatoDth.DisplayMember = "Descripcion"
        Me.cmbTipoAparatoDth.FormattingEnabled = True
        Me.cmbTipoAparatoDth.Location = New System.Drawing.Point(89, 90)
        Me.cmbTipoAparatoDth.Name = "cmbTipoAparatoDth"
        Me.cmbTipoAparatoDth.Size = New System.Drawing.Size(288, 23)
        Me.cmbTipoAparatoDth.TabIndex = 11
        Me.cmbTipoAparatoDth.ValueMember = "NoArticulo"
        '
        'lblTipoAparatoDth
        '
        Me.lblTipoAparatoDth.AutoSize = True
        Me.lblTipoAparatoDth.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoAparatoDth.Location = New System.Drawing.Point(92, 68)
        Me.lblTipoAparatoDth.Name = "lblTipoAparatoDth"
        Me.lblTipoAparatoDth.Size = New System.Drawing.Size(293, 16)
        Me.lblTipoAparatoDth.TabIndex = 10
        Me.lblTipoAparatoDth.Text = "Seleccione el tipo de aparato a instalar : "
        '
        'btnDIGCancelar
        '
        Me.btnDIGCancelar.Location = New System.Drawing.Point(278, 131)
        Me.btnDIGCancelar.Name = "btnDIGCancelar"
        Me.btnDIGCancelar.Size = New System.Drawing.Size(99, 27)
        Me.btnDIGCancelar.TabIndex = 9
        Me.btnDIGCancelar.Text = "&Cancelar"
        Me.btnDIGCancelar.UseVisualStyleBackColor = True
        '
        'btnDIGAceptar
        '
        Me.btnDIGAceptar.Location = New System.Drawing.Point(89, 131)
        Me.btnDIGAceptar.Name = "btnDIGAceptar"
        Me.btnDIGAceptar.Size = New System.Drawing.Size(99, 27)
        Me.btnDIGAceptar.TabIndex = 8
        Me.btnDIGAceptar.Text = "&Aceptar"
        Me.btnDIGAceptar.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(92, 11)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(245, 16)
        Me.Label9.TabIndex = 7
        Me.Label9.Text = "Seleccione el paquete a asignar : "
        '
        'comboSerDIG
        '
        Me.comboSerDIG.DisplayMember = "Descripcion"
        Me.comboSerDIG.FormattingEnabled = True
        Me.comboSerDIG.Location = New System.Drawing.Point(89, 33)
        Me.comboSerDIG.Name = "comboSerDIG"
        Me.comboSerDIG.Size = New System.Drawing.Size(288, 23)
        Me.comboSerDIG.TabIndex = 6
        Me.comboSerDIG.ValueMember = "Clv_Servicio"
        '
        'bnDIGServicio
        '
        Me.bnDIGServicio.AddNewItem = Nothing
        Me.bnDIGServicio.CountItem = Nothing
        Me.bnDIGServicio.DeleteItem = Nothing
        Me.bnDIGServicio.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnDIGServicio.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbEliminarSDIG, Me.tsbAgregarSDIG})
        Me.bnDIGServicio.Location = New System.Drawing.Point(3, 28)
        Me.bnDIGServicio.MoveFirstItem = Nothing
        Me.bnDIGServicio.MoveLastItem = Nothing
        Me.bnDIGServicio.MoveNextItem = Nothing
        Me.bnDIGServicio.MovePreviousItem = Nothing
        Me.bnDIGServicio.Name = "bnDIGServicio"
        Me.bnDIGServicio.PositionItem = Nothing
        Me.bnDIGServicio.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnDIGServicio.Size = New System.Drawing.Size(666, 25)
        Me.bnDIGServicio.TabIndex = 15
        Me.bnDIGServicio.Text = "BindingNavigator1"
        '
        'tsbEliminarSDIG
        '
        Me.tsbEliminarSDIG.Image = CType(resources.GetObject("tsbEliminarSDIG.Image"), System.Drawing.Image)
        Me.tsbEliminarSDIG.Name = "tsbEliminarSDIG"
        Me.tsbEliminarSDIG.RightToLeftAutoMirrorImage = True
        Me.tsbEliminarSDIG.Size = New System.Drawing.Size(125, 22)
        Me.tsbEliminarSDIG.Text = "Eliminar Servicio"
        '
        'tsbAgregarSDIG
        '
        Me.tsbAgregarSDIG.Image = CType(resources.GetObject("tsbAgregarSDIG.Image"), System.Drawing.Image)
        Me.tsbAgregarSDIG.Name = "tsbAgregarSDIG"
        Me.tsbAgregarSDIG.Size = New System.Drawing.Size(127, 22)
        Me.tsbAgregarSDIG.Text = "Agregar Servicio"
        '
        'bnDIGTarjeta
        '
        Me.bnDIGTarjeta.AddNewItem = Nothing
        Me.bnDIGTarjeta.CountItem = Nothing
        Me.bnDIGTarjeta.DeleteItem = Nothing
        Me.bnDIGTarjeta.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnDIGTarjeta.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbEliminarTDIG, Me.tsbAgregarTDIG})
        Me.bnDIGTarjeta.Location = New System.Drawing.Point(3, 3)
        Me.bnDIGTarjeta.MoveFirstItem = Nothing
        Me.bnDIGTarjeta.MoveLastItem = Nothing
        Me.bnDIGTarjeta.MoveNextItem = Nothing
        Me.bnDIGTarjeta.MovePreviousItem = Nothing
        Me.bnDIGTarjeta.Name = "bnDIGTarjeta"
        Me.bnDIGTarjeta.PositionItem = Nothing
        Me.bnDIGTarjeta.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnDIGTarjeta.Size = New System.Drawing.Size(666, 25)
        Me.bnDIGTarjeta.TabIndex = 14
        Me.bnDIGTarjeta.Text = "BindingNavigator1"
        '
        'tsbEliminarTDIG
        '
        Me.tsbEliminarTDIG.Image = CType(resources.GetObject("tsbEliminarTDIG.Image"), System.Drawing.Image)
        Me.tsbEliminarTDIG.Name = "tsbEliminarTDIG"
        Me.tsbEliminarTDIG.RightToLeftAutoMirrorImage = True
        Me.tsbEliminarTDIG.Size = New System.Drawing.Size(104, 22)
        Me.tsbEliminarTDIG.Text = "Eliminar Caja"
        '
        'tsbAgregarTDIG
        '
        Me.tsbAgregarTDIG.Image = CType(resources.GetObject("tsbAgregarTDIG.Image"), System.Drawing.Image)
        Me.tsbAgregarTDIG.Name = "tsbAgregarTDIG"
        Me.tsbAgregarTDIG.Size = New System.Drawing.Size(106, 22)
        Me.tsbAgregarTDIG.Text = "Agregar Caja"
        '
        'tvDIG
        '
        Me.tvDIG.Location = New System.Drawing.Point(73, 64)
        Me.tvDIG.Name = "tvDIG"
        Me.tvDIG.Size = New System.Drawing.Size(521, 235)
        Me.tvDIG.TabIndex = 0
        '
        'tpNET
        '
        Me.tpNET.Controls.Add(Me.lblNET)
        Me.tpNET.Controls.Add(Me.panelCNET)
        Me.tpNET.Controls.Add(Me.panelSNET)
        Me.tpNET.Controls.Add(Me.bnNETServicio)
        Me.tpNET.Controls.Add(Me.bnNETCablemodem)
        Me.tpNET.Controls.Add(Me.tvNET)
        Me.tpNET.Location = New System.Drawing.Point(4, 24)
        Me.tpNET.Name = "tpNET"
        Me.tpNET.Padding = New System.Windows.Forms.Padding(3)
        Me.tpNET.Size = New System.Drawing.Size(672, 340)
        Me.tpNET.TabIndex = 1
        Me.tpNET.Text = "Internet"
        Me.tpNET.UseVisualStyleBackColor = True
        '
        'lblNET
        '
        Me.lblNET.ForeColor = System.Drawing.Color.Red
        Me.lblNET.Location = New System.Drawing.Point(3, 313)
        Me.lblNET.Name = "lblNET"
        Me.lblNET.Size = New System.Drawing.Size(666, 24)
        Me.lblNET.TabIndex = 17
        '
        'panelCNET
        '
        Me.panelCNET.BackColor = System.Drawing.Color.WhiteSmoke
        Me.panelCNET.Controls.Add(Me.Label17)
        Me.panelCNET.Controls.Add(Me.comboTipoSer)
        Me.panelCNET.Controls.Add(Me.btnCNETCancelar)
        Me.panelCNET.Controls.Add(Me.btnCNETAceptar)
        Me.panelCNET.Controls.Add(Me.Label7)
        Me.panelCNET.Controls.Add(Me.Label6)
        Me.panelCNET.Controls.Add(Me.comboTipoAs)
        Me.panelCNET.Controls.Add(Me.comboTipoCa)
        Me.panelCNET.Location = New System.Drawing.Point(95, 93)
        Me.panelCNET.Name = "panelCNET"
        Me.panelCNET.Size = New System.Drawing.Size(472, 178)
        Me.panelCNET.TabIndex = 15
        Me.panelCNET.Visible = False
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(80, 84)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(94, 15)
        Me.Label17.TabIndex = 15
        Me.Label17.Text = "Tipo Servicio:"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'comboTipoSer
        '
        Me.comboTipoSer.DisplayMember = "Concepto"
        Me.comboTipoSer.FormattingEnabled = True
        Me.comboTipoSer.Location = New System.Drawing.Point(196, 76)
        Me.comboTipoSer.Name = "comboTipoSer"
        Me.comboTipoSer.Size = New System.Drawing.Size(202, 23)
        Me.comboTipoSer.TabIndex = 14
        Me.comboTipoSer.ValueMember = "Clv_TipSerInternet"
        '
        'btnCNETCancelar
        '
        Me.btnCNETCancelar.Location = New System.Drawing.Point(333, 131)
        Me.btnCNETCancelar.Name = "btnCNETCancelar"
        Me.btnCNETCancelar.Size = New System.Drawing.Size(99, 27)
        Me.btnCNETCancelar.TabIndex = 13
        Me.btnCNETCancelar.Text = "&Cancelar"
        Me.btnCNETCancelar.UseVisualStyleBackColor = True
        '
        'btnCNETAceptar
        '
        Me.btnCNETAceptar.Location = New System.Drawing.Point(228, 131)
        Me.btnCNETAceptar.Name = "btnCNETAceptar"
        Me.btnCNETAceptar.Size = New System.Drawing.Size(99, 27)
        Me.btnCNETAceptar.TabIndex = 12
        Me.btnCNETAceptar.Text = "&Aceptar"
        Me.btnCNETAceptar.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(80, 53)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(113, 15)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Tipo Asignación:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(65, 24)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(128, 15)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Tipo Cablemodem:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'comboTipoAs
        '
        Me.comboTipoAs.DisplayMember = "Tipo"
        Me.comboTipoAs.FormattingEnabled = True
        Me.comboTipoAs.Location = New System.Drawing.Point(196, 47)
        Me.comboTipoAs.Name = "comboTipoAs"
        Me.comboTipoAs.Size = New System.Drawing.Size(202, 23)
        Me.comboTipoAs.TabIndex = 9
        Me.comboTipoAs.ValueMember = "Id"
        '
        'comboTipoCa
        '
        Me.comboTipoCa.DisplayMember = "Tipo"
        Me.comboTipoCa.FormattingEnabled = True
        Me.comboTipoCa.Location = New System.Drawing.Point(196, 16)
        Me.comboTipoCa.Name = "comboTipoCa"
        Me.comboTipoCa.Size = New System.Drawing.Size(202, 23)
        Me.comboTipoCa.TabIndex = 8
        Me.comboTipoCa.ValueMember = "ID"
        '
        'panelSNET
        '
        Me.panelSNET.BackColor = System.Drawing.Color.WhiteSmoke
        Me.panelSNET.Controls.Add(Me.btnSNETCancelar)
        Me.panelSNET.Controls.Add(Me.btnSNETAceptar)
        Me.panelSNET.Controls.Add(Me.Label11)
        Me.panelSNET.Controls.Add(Me.comboSerNET)
        Me.panelSNET.Location = New System.Drawing.Point(98, 96)
        Me.panelSNET.Name = "panelSNET"
        Me.panelSNET.Size = New System.Drawing.Size(472, 178)
        Me.panelSNET.TabIndex = 16
        Me.panelSNET.Visible = False
        '
        'btnSNETCancelar
        '
        Me.btnSNETCancelar.Location = New System.Drawing.Point(333, 131)
        Me.btnSNETCancelar.Name = "btnSNETCancelar"
        Me.btnSNETCancelar.Size = New System.Drawing.Size(99, 27)
        Me.btnSNETCancelar.TabIndex = 13
        Me.btnSNETCancelar.Text = "&Cancelar"
        Me.btnSNETCancelar.UseVisualStyleBackColor = True
        '
        'btnSNETAceptar
        '
        Me.btnSNETAceptar.Location = New System.Drawing.Point(228, 131)
        Me.btnSNETAceptar.Name = "btnSNETAceptar"
        Me.btnSNETAceptar.Size = New System.Drawing.Size(99, 27)
        Me.btnSNETAceptar.TabIndex = 12
        Me.btnSNETAceptar.Text = "&Aceptar"
        Me.btnSNETAceptar.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(124, 47)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(66, 15)
        Me.Label11.TabIndex = 7
        Me.Label11.Text = "Servicio: "
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'comboSerNET
        '
        Me.comboSerNET.DisplayMember = "Descripcion"
        Me.comboSerNET.FormattingEnabled = True
        Me.comboSerNET.Location = New System.Drawing.Point(196, 42)
        Me.comboSerNET.Name = "comboSerNET"
        Me.comboSerNET.Size = New System.Drawing.Size(202, 23)
        Me.comboSerNET.TabIndex = 6
        Me.comboSerNET.ValueMember = "Clv_Servicio"
        '
        'bnNETServicio
        '
        Me.bnNETServicio.AddNewItem = Nothing
        Me.bnNETServicio.CountItem = Nothing
        Me.bnNETServicio.DeleteItem = Nothing
        Me.bnNETServicio.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnNETServicio.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbEliminarSNET, Me.tsbAgregarSNET})
        Me.bnNETServicio.Location = New System.Drawing.Point(3, 28)
        Me.bnNETServicio.MoveFirstItem = Nothing
        Me.bnNETServicio.MoveLastItem = Nothing
        Me.bnNETServicio.MoveNextItem = Nothing
        Me.bnNETServicio.MovePreviousItem = Nothing
        Me.bnNETServicio.Name = "bnNETServicio"
        Me.bnNETServicio.PositionItem = Nothing
        Me.bnNETServicio.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnNETServicio.Size = New System.Drawing.Size(666, 25)
        Me.bnNETServicio.TabIndex = 16
        Me.bnNETServicio.Text = "BindingNavigator1"
        '
        'tsbEliminarSNET
        '
        Me.tsbEliminarSNET.Image = CType(resources.GetObject("tsbEliminarSNET.Image"), System.Drawing.Image)
        Me.tsbEliminarSNET.Name = "tsbEliminarSNET"
        Me.tsbEliminarSNET.RightToLeftAutoMirrorImage = True
        Me.tsbEliminarSNET.Size = New System.Drawing.Size(125, 22)
        Me.tsbEliminarSNET.Text = "Eliminar Servicio"
        '
        'tsbAgregarSNET
        '
        Me.tsbAgregarSNET.Image = CType(resources.GetObject("tsbAgregarSNET.Image"), System.Drawing.Image)
        Me.tsbAgregarSNET.Name = "tsbAgregarSNET"
        Me.tsbAgregarSNET.Size = New System.Drawing.Size(127, 22)
        Me.tsbAgregarSNET.Text = "Agregar Servicio"
        '
        'bnNETCablemodem
        '
        Me.bnNETCablemodem.AddNewItem = Nothing
        Me.bnNETCablemodem.CountItem = Nothing
        Me.bnNETCablemodem.DeleteItem = Nothing
        Me.bnNETCablemodem.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnNETCablemodem.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbEliminarNET, Me.tsbAgregarNET})
        Me.bnNETCablemodem.Location = New System.Drawing.Point(3, 3)
        Me.bnNETCablemodem.MoveFirstItem = Nothing
        Me.bnNETCablemodem.MoveLastItem = Nothing
        Me.bnNETCablemodem.MoveNextItem = Nothing
        Me.bnNETCablemodem.MovePreviousItem = Nothing
        Me.bnNETCablemodem.Name = "bnNETCablemodem"
        Me.bnNETCablemodem.PositionItem = Nothing
        Me.bnNETCablemodem.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnNETCablemodem.Size = New System.Drawing.Size(666, 25)
        Me.bnNETCablemodem.TabIndex = 14
        Me.bnNETCablemodem.Text = "BindingNavigator1"
        '
        'tsbEliminarNET
        '
        Me.tsbEliminarNET.Image = CType(resources.GetObject("tsbEliminarNET.Image"), System.Drawing.Image)
        Me.tsbEliminarNET.Name = "tsbEliminarNET"
        Me.tsbEliminarNET.RightToLeftAutoMirrorImage = True
        Me.tsbEliminarNET.Size = New System.Drawing.Size(156, 22)
        Me.tsbEliminarNET.Text = "Eliminar Cablemodem"
        '
        'tsbAgregarNET
        '
        Me.tsbAgregarNET.Image = CType(resources.GetObject("tsbAgregarNET.Image"), System.Drawing.Image)
        Me.tsbAgregarNET.Name = "tsbAgregarNET"
        Me.tsbAgregarNET.Size = New System.Drawing.Size(158, 22)
        Me.tsbAgregarNET.Text = "Agregar Cablemodem"
        '
        'tvNET
        '
        Me.tvNET.Location = New System.Drawing.Point(73, 64)
        Me.tvNET.Name = "tvNET"
        Me.tvNET.Size = New System.Drawing.Size(521, 235)
        Me.tvNET.TabIndex = 0
        '
        'tpTV
        '
        Me.tpTV.Controls.Add(Me.lblTV)
        Me.tpTV.Controls.Add(Me.panelTV)
        Me.tpTV.Controls.Add(Me.bnTV)
        Me.tpTV.Controls.Add(Me.tvTV)
        Me.tpTV.Location = New System.Drawing.Point(4, 24)
        Me.tpTV.Name = "tpTV"
        Me.tpTV.Padding = New System.Windows.Forms.Padding(3)
        Me.tpTV.Size = New System.Drawing.Size(672, 340)
        Me.tpTV.TabIndex = 0
        Me.tpTV.Text = "Televisión"
        Me.tpTV.UseVisualStyleBackColor = True
        '
        'lblTV
        '
        Me.lblTV.ForeColor = System.Drawing.Color.Red
        Me.lblTV.Location = New System.Drawing.Point(3, 313)
        Me.lblTV.Name = "lblTV"
        Me.lblTV.Size = New System.Drawing.Size(666, 24)
        Me.lblTV.TabIndex = 18
        '
        'panelTV
        '
        Me.panelTV.Controls.Add(Me.btnTVCancelar)
        Me.panelTV.Controls.Add(Me.btnTVAceptar)
        Me.panelTV.Controls.Add(Me.Label3)
        Me.panelTV.Controls.Add(Me.comboSerTV)
        Me.panelTV.Controls.Add(Me.txtConPago)
        Me.panelTV.Controls.Add(Me.txtSinPago)
        Me.panelTV.Controls.Add(Me.Label2)
        Me.panelTV.Controls.Add(Me.Label1)
        Me.panelTV.Location = New System.Drawing.Point(97, 89)
        Me.panelTV.Name = "panelTV"
        Me.panelTV.Size = New System.Drawing.Size(472, 178)
        Me.panelTV.TabIndex = 14
        Me.panelTV.Visible = False
        '
        'btnTVCancelar
        '
        Me.btnTVCancelar.Location = New System.Drawing.Point(333, 131)
        Me.btnTVCancelar.Name = "btnTVCancelar"
        Me.btnTVCancelar.Size = New System.Drawing.Size(99, 27)
        Me.btnTVCancelar.TabIndex = 7
        Me.btnTVCancelar.Text = "&Cancelar"
        Me.btnTVCancelar.UseVisualStyleBackColor = True
        '
        'btnTVAceptar
        '
        Me.btnTVAceptar.Location = New System.Drawing.Point(228, 131)
        Me.btnTVAceptar.Name = "btnTVAceptar"
        Me.btnTVAceptar.Size = New System.Drawing.Size(99, 27)
        Me.btnTVAceptar.TabIndex = 6
        Me.btnTVAceptar.Text = "&Aceptar"
        Me.btnTVAceptar.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(124, 30)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(66, 15)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Servicio: "
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'comboSerTV
        '
        Me.comboSerTV.DisplayMember = "Descripcion"
        Me.comboSerTV.FormattingEnabled = True
        Me.comboSerTV.Location = New System.Drawing.Point(196, 22)
        Me.comboSerTV.Name = "comboSerTV"
        Me.comboSerTV.Size = New System.Drawing.Size(202, 23)
        Me.comboSerTV.TabIndex = 4
        Me.comboSerTV.ValueMember = "Clv_Servicio"
        '
        'txtConPago
        '
        Me.txtConPago.Location = New System.Drawing.Point(196, 78)
        Me.txtConPago.Name = "txtConPago"
        Me.txtConPago.Size = New System.Drawing.Size(100, 21)
        Me.txtConPago.TabIndex = 3
        '
        'txtSinPago
        '
        Me.txtSinPago.Location = New System.Drawing.Point(196, 51)
        Me.txtSinPago.Name = "txtSinPago"
        Me.txtSinPago.Size = New System.Drawing.Size(100, 21)
        Me.txtSinPago.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(100, 84)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(90, 15)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "TV con pago:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(104, 57)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(86, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "TV sin pago:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'bnTV
        '
        Me.bnTV.AddNewItem = Nothing
        Me.bnTV.CountItem = Nothing
        Me.bnTV.DeleteItem = Nothing
        Me.bnTV.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnTV.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbEliminarTV, Me.tsbAgregarTV})
        Me.bnTV.Location = New System.Drawing.Point(3, 3)
        Me.bnTV.MoveFirstItem = Nothing
        Me.bnTV.MoveLastItem = Nothing
        Me.bnTV.MoveNextItem = Nothing
        Me.bnTV.MovePreviousItem = Nothing
        Me.bnTV.Name = "bnTV"
        Me.bnTV.PositionItem = Nothing
        Me.bnTV.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnTV.Size = New System.Drawing.Size(666, 25)
        Me.bnTV.TabIndex = 13
        Me.bnTV.Text = "BindingNavigator1"
        '
        'tsbEliminarTV
        '
        Me.tsbEliminarTV.Image = CType(resources.GetObject("tsbEliminarTV.Image"), System.Drawing.Image)
        Me.tsbEliminarTV.Name = "tsbEliminarTV"
        Me.tsbEliminarTV.RightToLeftAutoMirrorImage = True
        Me.tsbEliminarTV.Size = New System.Drawing.Size(125, 22)
        Me.tsbEliminarTV.Text = "Eliminar Servicio"
        '
        'tsbAgregarTV
        '
        Me.tsbAgregarTV.Image = CType(resources.GetObject("tsbAgregarTV.Image"), System.Drawing.Image)
        Me.tsbAgregarTV.Name = "tsbAgregarTV"
        Me.tsbAgregarTV.Size = New System.Drawing.Size(127, 22)
        Me.tsbAgregarTV.Text = "Agregar Servicio"
        '
        'tvTV
        '
        Me.tvTV.Location = New System.Drawing.Point(73, 64)
        Me.tvTV.Name = "tvTV"
        Me.tvTV.Size = New System.Drawing.Size(521, 235)
        Me.tvTV.TabIndex = 4
        '
        'tpDIGCable
        '
        Me.tpDIGCable.Controls.Add(Me.bnDIGCableServicio)
        Me.tpDIGCable.Controls.Add(Me.bnDIGCableTarjeta)
        Me.tpDIGCable.Controls.Add(Me.lblDIGCable)
        Me.tpDIGCable.Controls.Add(Me.panelDIGCable)
        Me.tpDIGCable.Controls.Add(Me.tvDIGCable)
        Me.tpDIGCable.Location = New System.Drawing.Point(4, 24)
        Me.tpDIGCable.Name = "tpDIGCable"
        Me.tpDIGCable.Size = New System.Drawing.Size(672, 340)
        Me.tpDIGCable.TabIndex = 3
        Me.tpDIGCable.Text = "Tv Digital Cable"
        Me.tpDIGCable.UseVisualStyleBackColor = True
        '
        'bnDIGCableServicio
        '
        Me.bnDIGCableServicio.AddNewItem = Nothing
        Me.bnDIGCableServicio.CountItem = Nothing
        Me.bnDIGCableServicio.DeleteItem = Nothing
        Me.bnDIGCableServicio.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnDIGCableServicio.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbEliminarSDIGCable, Me.tsbAgregarSDIGCable})
        Me.bnDIGCableServicio.Location = New System.Drawing.Point(0, 25)
        Me.bnDIGCableServicio.MoveFirstItem = Nothing
        Me.bnDIGCableServicio.MoveLastItem = Nothing
        Me.bnDIGCableServicio.MoveNextItem = Nothing
        Me.bnDIGCableServicio.MovePreviousItem = Nothing
        Me.bnDIGCableServicio.Name = "bnDIGCableServicio"
        Me.bnDIGCableServicio.PositionItem = Nothing
        Me.bnDIGCableServicio.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnDIGCableServicio.Size = New System.Drawing.Size(672, 25)
        Me.bnDIGCableServicio.TabIndex = 25
        Me.bnDIGCableServicio.Text = "BindingNavigator1"
        '
        'tsbEliminarSDIGCable
        '
        Me.tsbEliminarSDIGCable.Image = CType(resources.GetObject("tsbEliminarSDIGCable.Image"), System.Drawing.Image)
        Me.tsbEliminarSDIGCable.Name = "tsbEliminarSDIGCable"
        Me.tsbEliminarSDIGCable.RightToLeftAutoMirrorImage = True
        Me.tsbEliminarSDIGCable.Size = New System.Drawing.Size(125, 22)
        Me.tsbEliminarSDIGCable.Text = "Eliminar Servicio"
        '
        'tsbAgregarSDIGCable
        '
        Me.tsbAgregarSDIGCable.Image = CType(resources.GetObject("tsbAgregarSDIGCable.Image"), System.Drawing.Image)
        Me.tsbAgregarSDIGCable.Name = "tsbAgregarSDIGCable"
        Me.tsbAgregarSDIGCable.Size = New System.Drawing.Size(127, 22)
        Me.tsbAgregarSDIGCable.Text = "Agregar Servicio"
        '
        'bnDIGCableTarjeta
        '
        Me.bnDIGCableTarjeta.AddNewItem = Nothing
        Me.bnDIGCableTarjeta.CountItem = Nothing
        Me.bnDIGCableTarjeta.DeleteItem = Nothing
        Me.bnDIGCableTarjeta.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnDIGCableTarjeta.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbEliminarTDIGCable, Me.tsbAgregarTDIGCable})
        Me.bnDIGCableTarjeta.Location = New System.Drawing.Point(0, 0)
        Me.bnDIGCableTarjeta.MoveFirstItem = Nothing
        Me.bnDIGCableTarjeta.MoveLastItem = Nothing
        Me.bnDIGCableTarjeta.MoveNextItem = Nothing
        Me.bnDIGCableTarjeta.MovePreviousItem = Nothing
        Me.bnDIGCableTarjeta.Name = "bnDIGCableTarjeta"
        Me.bnDIGCableTarjeta.PositionItem = Nothing
        Me.bnDIGCableTarjeta.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnDIGCableTarjeta.Size = New System.Drawing.Size(672, 25)
        Me.bnDIGCableTarjeta.TabIndex = 24
        Me.bnDIGCableTarjeta.Text = "BindingNavigator1"
        '
        'tsbEliminarTDIGCable
        '
        Me.tsbEliminarTDIGCable.Image = CType(resources.GetObject("tsbEliminarTDIGCable.Image"), System.Drawing.Image)
        Me.tsbEliminarTDIGCable.Name = "tsbEliminarTDIGCable"
        Me.tsbEliminarTDIGCable.RightToLeftAutoMirrorImage = True
        Me.tsbEliminarTDIGCable.Size = New System.Drawing.Size(104, 22)
        Me.tsbEliminarTDIGCable.Text = "Eliminar Caja"
        '
        'tsbAgregarTDIGCable
        '
        Me.tsbAgregarTDIGCable.Image = CType(resources.GetObject("tsbAgregarTDIGCable.Image"), System.Drawing.Image)
        Me.tsbAgregarTDIGCable.Name = "tsbAgregarTDIGCable"
        Me.tsbAgregarTDIGCable.Size = New System.Drawing.Size(106, 22)
        Me.tsbAgregarTDIGCable.Text = "Agregar Caja"
        '
        'lblDIGCable
        '
        Me.lblDIGCable.ForeColor = System.Drawing.Color.Red
        Me.lblDIGCable.Location = New System.Drawing.Point(3, 315)
        Me.lblDIGCable.Name = "lblDIGCable"
        Me.lblDIGCable.Size = New System.Drawing.Size(666, 24)
        Me.lblDIGCable.TabIndex = 23
        '
        'panelDIGCable
        '
        Me.panelDIGCable.BackColor = System.Drawing.Color.WhiteSmoke
        Me.panelDIGCable.Controls.Add(Me.cmbTipoAparatoCable)
        Me.panelDIGCable.Controls.Add(Me.lblTipoAparatoCable)
        Me.panelDIGCable.Controls.Add(Me.btnDIGCableCancelar)
        Me.panelDIGCable.Controls.Add(Me.btnDIGCableAceptar)
        Me.panelDIGCable.Controls.Add(Me.Label5)
        Me.panelDIGCable.Controls.Add(Me.comboSerDIGCable)
        Me.panelDIGCable.Location = New System.Drawing.Point(97, 91)
        Me.panelDIGCable.Name = "panelDIGCable"
        Me.panelDIGCable.Size = New System.Drawing.Size(472, 178)
        Me.panelDIGCable.TabIndex = 22
        Me.panelDIGCable.Visible = False
        '
        'cmbTipoAparatoCable
        '
        Me.cmbTipoAparatoCable.DisplayMember = "Descripcion"
        Me.cmbTipoAparatoCable.FormattingEnabled = True
        Me.cmbTipoAparatoCable.Location = New System.Drawing.Point(89, 89)
        Me.cmbTipoAparatoCable.Name = "cmbTipoAparatoCable"
        Me.cmbTipoAparatoCable.Size = New System.Drawing.Size(288, 23)
        Me.cmbTipoAparatoCable.TabIndex = 13
        Me.cmbTipoAparatoCable.ValueMember = "NoArticulo"
        '
        'lblTipoAparatoCable
        '
        Me.lblTipoAparatoCable.AutoSize = True
        Me.lblTipoAparatoCable.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoAparatoCable.Location = New System.Drawing.Point(92, 67)
        Me.lblTipoAparatoCable.Name = "lblTipoAparatoCable"
        Me.lblTipoAparatoCable.Size = New System.Drawing.Size(293, 16)
        Me.lblTipoAparatoCable.TabIndex = 12
        Me.lblTipoAparatoCable.Text = "Seleccione el tipo de aparato a instalar : "
        '
        'btnDIGCableCancelar
        '
        Me.btnDIGCableCancelar.Location = New System.Drawing.Point(278, 131)
        Me.btnDIGCableCancelar.Name = "btnDIGCableCancelar"
        Me.btnDIGCableCancelar.Size = New System.Drawing.Size(99, 27)
        Me.btnDIGCableCancelar.TabIndex = 9
        Me.btnDIGCableCancelar.Text = "&Cancelar"
        Me.btnDIGCableCancelar.UseVisualStyleBackColor = True
        '
        'btnDIGCableAceptar
        '
        Me.btnDIGCableAceptar.Location = New System.Drawing.Point(89, 131)
        Me.btnDIGCableAceptar.Name = "btnDIGCableAceptar"
        Me.btnDIGCableAceptar.Size = New System.Drawing.Size(99, 27)
        Me.btnDIGCableAceptar.TabIndex = 8
        Me.btnDIGCableAceptar.Text = "&Aceptar"
        Me.btnDIGCableAceptar.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(92, 12)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(245, 16)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Seleccione el paquete a asignar : "
        '
        'comboSerDIGCable
        '
        Me.comboSerDIGCable.DisplayMember = "Descripcion"
        Me.comboSerDIGCable.FormattingEnabled = True
        Me.comboSerDIGCable.Location = New System.Drawing.Point(89, 34)
        Me.comboSerDIGCable.Name = "comboSerDIGCable"
        Me.comboSerDIGCable.Size = New System.Drawing.Size(288, 23)
        Me.comboSerDIGCable.TabIndex = 6
        Me.comboSerDIGCable.ValueMember = "Clv_Servicio"
        '
        'tvDIGCable
        '
        Me.tvDIGCable.Location = New System.Drawing.Point(73, 66)
        Me.tvDIGCable.Name = "tvDIGCable"
        Me.tvDIGCable.Size = New System.Drawing.Size(521, 235)
        Me.tvDIGCable.TabIndex = 19
        '
        'btnNETCancelar
        '
        Me.btnNETCancelar.Location = New System.Drawing.Point(319, 174)
        Me.btnNETCancelar.Name = "btnNETCancelar"
        Me.btnNETCancelar.Size = New System.Drawing.Size(99, 27)
        Me.btnNETCancelar.TabIndex = 9
        Me.btnNETCancelar.Text = "&Cancelar"
        Me.btnNETCancelar.UseVisualStyleBackColor = True
        '
        'btnNETAceptar
        '
        Me.btnNETAceptar.Location = New System.Drawing.Point(214, 174)
        Me.btnNETAceptar.Name = "btnNETAceptar"
        Me.btnNETAceptar.Size = New System.Drawing.Size(99, 27)
        Me.btnNETAceptar.TabIndex = 8
        Me.btnNETAceptar.Text = "&Aceptar"
        Me.btnNETAceptar.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(133, 75)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Servicio: "
        '
        'comboSerNET2023
        '
        Me.comboSerNET2023.FormattingEnabled = True
        Me.comboSerNET2023.Location = New System.Drawing.Point(205, 67)
        Me.comboSerNET2023.Name = "comboSerNET2023"
        Me.comboSerNET2023.Size = New System.Drawing.Size(202, 21)
        Me.comboSerNET2023.TabIndex = 6
        '
        'CMBLabel5
        '
        Me.CMBLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel5.Location = New System.Drawing.Point(77, 62)
        Me.CMBLabel5.Name = "CMBLabel5"
        Me.CMBLabel5.Size = New System.Drawing.Size(80, 23)
        Me.CMBLabel5.TabIndex = 21
        Me.CMBLabel5.Text = "Contrato:"
        Me.CMBLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'CMBGroupBox1
        '
        Me.CMBGroupBox1.Controls.Add(Me.CheckBoxSoloInternet)
        Me.CMBGroupBox1.Controls.Add(Me.LabelCelular)
        Me.CMBGroupBox1.Controls.Add(Me.LabelTelefono)
        Me.CMBGroupBox1.Controls.Add(Me.LabelCiudad)
        Me.CMBGroupBox1.Controls.Add(Me.LabelColonia)
        Me.CMBGroupBox1.Controls.Add(Me.LabelNumero)
        Me.CMBGroupBox1.Controls.Add(Me.LabelCalle)
        Me.CMBGroupBox1.Controls.Add(Me.LabelNombre)
        Me.CMBGroupBox1.Controls.Add(Me.Label8)
        Me.CMBGroupBox1.Controls.Add(Me.Label10)
        Me.CMBGroupBox1.Controls.Add(Me.Label12)
        Me.CMBGroupBox1.Controls.Add(Me.Label13)
        Me.CMBGroupBox1.Controls.Add(Me.Label14)
        Me.CMBGroupBox1.Controls.Add(Me.Label15)
        Me.CMBGroupBox1.Controls.Add(Me.Label16)
        Me.CMBGroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBGroupBox1.Location = New System.Drawing.Point(63, 100)
        Me.CMBGroupBox1.Name = "CMBGroupBox1"
        Me.CMBGroupBox1.Size = New System.Drawing.Size(673, 174)
        Me.CMBGroupBox1.TabIndex = 20
        Me.CMBGroupBox1.TabStop = False
        Me.CMBGroupBox1.Text = "Datos del Cliente"
        '
        'CheckBoxSoloInternet
        '
        Me.CheckBoxSoloInternet.AutoSize = True
        Me.CheckBoxSoloInternet.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxSoloInternet.Enabled = False
        Me.CheckBoxSoloInternet.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBoxSoloInternet.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CheckBoxSoloInternet.Location = New System.Drawing.Point(469, 20)
        Me.CheckBoxSoloInternet.Name = "CheckBoxSoloInternet"
        Me.CheckBoxSoloInternet.Size = New System.Drawing.Size(108, 19)
        Me.CheckBoxSoloInternet.TabIndex = 16
        Me.CheckBoxSoloInternet.Text = "Sólo Internet"
        Me.CheckBoxSoloInternet.UseVisualStyleBackColor = True
        Me.CheckBoxSoloInternet.Visible = False
        '
        'LabelCelular
        '
        Me.LabelCelular.Location = New System.Drawing.Point(150, 115)
        Me.LabelCelular.Name = "LabelCelular"
        Me.LabelCelular.Size = New System.Drawing.Size(300, 19)
        Me.LabelCelular.TabIndex = 15
        Me.LabelCelular.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelTelefono
        '
        Me.LabelTelefono.Location = New System.Drawing.Point(150, 134)
        Me.LabelTelefono.Name = "LabelTelefono"
        Me.LabelTelefono.Size = New System.Drawing.Size(300, 19)
        Me.LabelTelefono.TabIndex = 14
        Me.LabelTelefono.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelCiudad
        '
        Me.LabelCiudad.Location = New System.Drawing.Point(150, 96)
        Me.LabelCiudad.Name = "LabelCiudad"
        Me.LabelCiudad.Size = New System.Drawing.Size(300, 19)
        Me.LabelCiudad.TabIndex = 13
        Me.LabelCiudad.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelColonia
        '
        Me.LabelColonia.Location = New System.Drawing.Point(150, 77)
        Me.LabelColonia.Name = "LabelColonia"
        Me.LabelColonia.Size = New System.Drawing.Size(300, 19)
        Me.LabelColonia.TabIndex = 12
        Me.LabelColonia.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelNumero
        '
        Me.LabelNumero.Location = New System.Drawing.Point(150, 58)
        Me.LabelNumero.Name = "LabelNumero"
        Me.LabelNumero.Size = New System.Drawing.Size(300, 19)
        Me.LabelNumero.TabIndex = 11
        Me.LabelNumero.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelCalle
        '
        Me.LabelCalle.Location = New System.Drawing.Point(150, 39)
        Me.LabelCalle.Name = "LabelCalle"
        Me.LabelCalle.Size = New System.Drawing.Size(300, 19)
        Me.LabelCalle.TabIndex = 10
        Me.LabelCalle.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelNombre
        '
        Me.LabelNombre.Location = New System.Drawing.Point(150, 20)
        Me.LabelNombre.Name = "LabelNombre"
        Me.LabelNombre.Size = New System.Drawing.Size(300, 19)
        Me.LabelNombre.TabIndex = 9
        Me.LabelNombre.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label8.Location = New System.Drawing.Point(51, 115)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(100, 19)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Celular: "
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label10
        '
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label10.Location = New System.Drawing.Point(51, 134)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(100, 19)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "Teléfono: "
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label12
        '
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label12.Location = New System.Drawing.Point(51, 96)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(100, 19)
        Me.Label12.TabIndex = 5
        Me.Label12.Text = "Ciudad: "
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label13
        '
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label13.Location = New System.Drawing.Point(51, 77)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(100, 19)
        Me.Label13.TabIndex = 4
        Me.Label13.Text = "Colonia: "
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label14
        '
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label14.Location = New System.Drawing.Point(51, 58)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(101, 19)
        Me.Label14.TabIndex = 3
        Me.Label14.Text = "#: "
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label15
        '
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label15.Location = New System.Drawing.Point(51, 39)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(100, 19)
        Me.Label15.TabIndex = 2
        Me.Label15.Text = "Calle: "
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label16
        '
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label16.Location = New System.Drawing.Point(51, 20)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(100, 19)
        Me.Label16.TabIndex = 1
        Me.Label16.Text = "Nombre: "
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'TextBoxContrato
        '
        Me.TextBoxContrato.CausesValidation = False
        Me.TextBoxContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxContrato.Location = New System.Drawing.Point(163, 64)
        Me.TextBoxContrato.Name = "TextBoxContrato"
        Me.TextBoxContrato.Size = New System.Drawing.Size(100, 21)
        Me.TextBoxContrato.TabIndex = 19
        '
        'ButtonBuscar
        '
        Me.ButtonBuscar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonBuscar.Location = New System.Drawing.Point(269, 62)
        Me.ButtonBuscar.Name = "ButtonBuscar"
        Me.ButtonBuscar.Size = New System.Drawing.Size(43, 23)
        Me.ButtonBuscar.TabIndex = 18
        Me.ButtonBuscar.Text = "..."
        Me.ButtonBuscar.UseVisualStyleBackColor = True
        '
        'bnRecontratar
        '
        Me.bnRecontratar.AddNewItem = Nothing
        Me.bnRecontratar.CountItem = Nothing
        Me.bnRecontratar.DeleteItem = Nothing
        Me.bnRecontratar.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnRecontratar.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbRecon})
        Me.bnRecontratar.Location = New System.Drawing.Point(0, 0)
        Me.bnRecontratar.MoveFirstItem = Nothing
        Me.bnRecontratar.MoveLastItem = Nothing
        Me.bnRecontratar.MoveNextItem = Nothing
        Me.bnRecontratar.MovePreviousItem = Nothing
        Me.bnRecontratar.Name = "bnRecontratar"
        Me.bnRecontratar.PositionItem = Nothing
        Me.bnRecontratar.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnRecontratar.Size = New System.Drawing.Size(829, 25)
        Me.bnRecontratar.TabIndex = 22
        Me.bnRecontratar.Text = "BindingNavigator1"
        '
        'tsbRecon
        '
        Me.tsbRecon.Image = CType(resources.GetObject("tsbRecon.Image"), System.Drawing.Image)
        Me.tsbRecon.Name = "tsbRecon"
        Me.tsbRecon.Size = New System.Drawing.Size(101, 22)
        Me.tsbRecon.Text = "Recontratar"
        '
        'btnSalir
        '
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(681, 689)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(136, 36)
        Me.btnSalir.TabIndex = 23
        Me.btnSalir.Text = "&SALIR"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter5
        '
        Me.Muestra_ServiciosDigitalesTableAdapter5.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter6
        '
        Me.Muestra_ServiciosDigitalesTableAdapter6.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter7
        '
        Me.Muestra_ServiciosDigitalesTableAdapter7.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter8
        '
        Me.Muestra_ServiciosDigitalesTableAdapter8.ClearBeforeFill = True
        '
        'FrmRecontratacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(829, 737)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.bnRecontratar)
        Me.Controls.Add(Me.CMBLabel5)
        Me.Controls.Add(Me.CMBGroupBox1)
        Me.Controls.Add(Me.TextBoxContrato)
        Me.Controls.Add(Me.ButtonBuscar)
        Me.Controls.Add(Me.tcServicios)
        Me.Name = "FrmRecontratacion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Recontratación Clientes"
        Me.tcServicios.ResumeLayout(False)
        Me.tpDIG.ResumeLayout(False)
        Me.tpDIG.PerformLayout()
        Me.panelDIG.ResumeLayout(False)
        Me.panelDIG.PerformLayout()
        CType(Me.bnDIGServicio, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnDIGServicio.ResumeLayout(False)
        Me.bnDIGServicio.PerformLayout()
        CType(Me.bnDIGTarjeta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnDIGTarjeta.ResumeLayout(False)
        Me.bnDIGTarjeta.PerformLayout()
        Me.tpNET.ResumeLayout(False)
        Me.tpNET.PerformLayout()
        Me.panelCNET.ResumeLayout(False)
        Me.panelCNET.PerformLayout()
        Me.panelSNET.ResumeLayout(False)
        Me.panelSNET.PerformLayout()
        CType(Me.bnNETServicio, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnNETServicio.ResumeLayout(False)
        Me.bnNETServicio.PerformLayout()
        CType(Me.bnNETCablemodem, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnNETCablemodem.ResumeLayout(False)
        Me.bnNETCablemodem.PerformLayout()
        Me.tpTV.ResumeLayout(False)
        Me.tpTV.PerformLayout()
        Me.panelTV.ResumeLayout(False)
        Me.panelTV.PerformLayout()
        CType(Me.bnTV, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnTV.ResumeLayout(False)
        Me.bnTV.PerformLayout()
        Me.tpDIGCable.ResumeLayout(False)
        Me.tpDIGCable.PerformLayout()
        CType(Me.bnDIGCableServicio, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnDIGCableServicio.ResumeLayout(False)
        Me.bnDIGCableServicio.PerformLayout()
        CType(Me.bnDIGCableTarjeta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnDIGCableTarjeta.ResumeLayout(False)
        Me.bnDIGCableTarjeta.PerformLayout()
        Me.panelDIGCable.ResumeLayout(False)
        Me.panelDIGCable.PerformLayout()
        Me.CMBGroupBox1.ResumeLayout(False)
        Me.CMBGroupBox1.PerformLayout()
        CType(Me.bnRecontratar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnRecontratar.ResumeLayout(False)
        Me.bnRecontratar.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tcServicios As System.Windows.Forms.TabControl
    Friend WithEvents tpTV As System.Windows.Forms.TabPage
    Friend WithEvents tpNET As System.Windows.Forms.TabPage
    Friend WithEvents tpDIG As System.Windows.Forms.TabPage
    Friend WithEvents tvTV As System.Windows.Forms.TreeView
    Friend WithEvents tvNET As System.Windows.Forms.TreeView
    Friend WithEvents tvDIG As System.Windows.Forms.TreeView
    Friend WithEvents bnTV As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbEliminarTV As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbAgregarTV As System.Windows.Forms.ToolStripButton
    Friend WithEvents bnNETCablemodem As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbEliminarNET As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbAgregarNET As System.Windows.Forms.ToolStripButton
    Friend WithEvents bnDIGServicio As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbEliminarSDIG As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbAgregarSDIG As System.Windows.Forms.ToolStripButton
    Friend WithEvents bnDIGTarjeta As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbEliminarTDIG As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbAgregarTDIG As System.Windows.Forms.ToolStripButton
    Friend WithEvents panelTV As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents comboSerTV As System.Windows.Forms.ComboBox
    Friend WithEvents txtConPago As System.Windows.Forms.TextBox
    Friend WithEvents txtSinPago As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnTVCancelar As System.Windows.Forms.Button
    Friend WithEvents btnTVAceptar As System.Windows.Forms.Button
    Friend WithEvents panelCNET As System.Windows.Forms.Panel
    Friend WithEvents btnCNETCancelar As System.Windows.Forms.Button
    Friend WithEvents btnCNETAceptar As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents comboTipoAs As System.Windows.Forms.ComboBox
    Friend WithEvents comboTipoCa As System.Windows.Forms.ComboBox
    Friend WithEvents panelDIG As System.Windows.Forms.Panel
    Friend WithEvents btnDIGCancelar As System.Windows.Forms.Button
    Friend WithEvents btnDIGAceptar As System.Windows.Forms.Button
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents comboSerDIG As System.Windows.Forms.ComboBox
    Friend WithEvents btnNETCancelar As System.Windows.Forms.Button
    Friend WithEvents btnNETAceptar As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents comboSerNET2023 As System.Windows.Forms.ComboBox
    Friend WithEvents bnNETServicio As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbEliminarSNET As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbAgregarSNET As System.Windows.Forms.ToolStripButton
    Friend WithEvents panelSNET As System.Windows.Forms.Panel
    Friend WithEvents btnSNETCancelar As System.Windows.Forms.Button
    Friend WithEvents btnSNETAceptar As System.Windows.Forms.Button
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents comboSerNET As System.Windows.Forms.ComboBox
    Friend WithEvents CMBLabel5 As System.Windows.Forms.Label
    Friend WithEvents CMBGroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBoxSoloInternet As System.Windows.Forms.CheckBox
    Friend WithEvents LabelCelular As System.Windows.Forms.Label
    Friend WithEvents LabelTelefono As System.Windows.Forms.Label
    Friend WithEvents LabelCiudad As System.Windows.Forms.Label
    Friend WithEvents LabelColonia As System.Windows.Forms.Label
    Friend WithEvents LabelNumero As System.Windows.Forms.Label
    Friend WithEvents LabelCalle As System.Windows.Forms.Label
    Friend WithEvents LabelNombre As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents TextBoxContrato As System.Windows.Forms.TextBox
    Friend WithEvents ButtonBuscar As System.Windows.Forms.Button
    Friend WithEvents lblTV As System.Windows.Forms.Label
    Friend WithEvents lblNET As System.Windows.Forms.Label
    Friend WithEvents lblDIG As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents comboTipoSer As System.Windows.Forms.ComboBox
    Friend WithEvents bnRecontratar As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbRecon As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents tpDIGCable As System.Windows.Forms.TabPage
    Friend WithEvents lblDIGCable As System.Windows.Forms.Label
    Friend WithEvents panelDIGCable As System.Windows.Forms.Panel
    Friend WithEvents btnDIGCableCancelar As System.Windows.Forms.Button
    Friend WithEvents btnDIGCableAceptar As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents comboSerDIGCable As System.Windows.Forms.ComboBox
    Friend WithEvents tvDIGCable As System.Windows.Forms.TreeView
    Friend WithEvents bnDIGCableServicio As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbEliminarSDIGCable As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbAgregarSDIGCable As System.Windows.Forms.ToolStripButton
    Friend WithEvents bnDIGCableTarjeta As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbEliminarTDIGCable As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbAgregarTDIGCable As System.Windows.Forms.ToolStripButton
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter5 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter6 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Dame_clv_EmpresaTableAdapter As New sofTV.DataSetarnoldoTableAdapters.Dame_clv_EmpresaTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter7 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents ChecaServicioContratadoTableAdapter As sofTV.DataSetEDGARTableAdapters.ChecaServicioContratadoTableAdapter
    Friend WithEvents Selecciona_Impresora_SucursalTableAdapter As New sofTV.DataSetarnoldoTableAdapters.Selecciona_Impresora_SucursalTableAdapter
    Friend WithEvents cmbTipoAparatoDth As System.Windows.Forms.ComboBox
    Friend WithEvents lblTipoAparatoDth As System.Windows.Forms.Label
    Friend WithEvents cmbTipoAparatoCable As System.Windows.Forms.ComboBox
    Friend WithEvents lblTipoAparatoCable As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter8 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
