﻿Imports sofTV.BAL

Public Class BrwIngresosDesglosados
    Dim AplicaFiltro As Boolean = False
    Dim CargaGrupos As Boolean = False
    Dim dT As New DataTable
    Structure ColumnasGrid
        Dim idxClave As Integer
        Dim idxConcepto As Integer
    End Structure

    Private Sub BrwIngresosDesglosados_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If AplicaFiltro = True Then
            AplicaFiltro = False
            If cbCompania.Text.Length = 0 Then
                Exit Sub
            End If
            If cbxGrupo.Text.Length = 0 Then
                Exit Sub
            End If
            uspSeleccionaIngresosDesglozados(2, 0, cbxGrupo.SelectedValue, "")
        End If
        'If AplicaFiltro Then
        '    AplicaFiltro = False
        '    'Me.dgvConceptos.DataSource = FiltraIngresos(0)
        '    Me.dgvConceptos.DataSource = FiltraIngresos(2)
        '    ocultaColumnas(Me.dgvConceptos)
        '    If Me.dgvConceptos.RowCount > 0 Then
        '        SeleccionaFila(Me.dgvConceptos, 1)
        '    End If
        'End If
    End Sub

    Private Sub BrwIngresosDesglosados_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        colorea(Me, Me.Name)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, 0)
        cbCompania.DataSource = BaseII.ConsultaDT("MUESTRAtblCompanias")

        dT.Columns.Add("Clave")
        dT.Columns.Add("Concepto")

        'colorea(Me, Me.Name)


        'Dim GrupoConcepto As New GrupoIngreso

        'CargaGrupos = False
        'With Me.cbxGrupo
        '    .DataSource = GrupoConcepto.GetAll
        '    .ValueMember = "IdGrupoIngresos"
        '    .DisplayMember = "Concepto"
        'End With
        'CargaGrupos = True
    End Sub

    Private Sub ocultaColumnas(ByRef dgv As DataGridView)

        For Each dc As DataGridViewColumn In dgv.Columns

            If (dc.Name = "Clave") Then
                dc.ReadOnly = True
                dc.Visible = True
            ElseIf (dc.Name = "Concepto") Then
                dc.ReadOnly = True
                dc.Visible = True
                dc.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            End If
        Next

    End Sub

    Private Sub SeleccionaFila(ByVal dgv As DataGridView, ByVal Fila As Integer)
        Try
            Dim i As Integer = 0

            If Fila = 0 Then
                i = dgv.CurrentRow.Index
            End If

            Dim indexGrid As ColumnasGrid = obtenerIdexGridTrabajos(dgv)

            Me.IdGrupoLabel1.Text = dgv.Item(indexGrid.idxClave, i).Value
            Me.ConceptoLabel1.Text = dgv.Item(indexGrid.idxConcepto, i).Value
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "Atencion!!!")
        End Try
    End Sub

    Private Function obtenerIdexGridTrabajos(ByVal dgv As DataGridView) As ColumnasGrid
        Dim resultado As New ColumnasGrid
        Dim indx As Integer = 0


        For Each dc As DataGridViewColumn In dgv.Columns
            If (dc.Name = "Clave") Then
                resultado.idxClave = indx
                indx = indx + 1
            ElseIf (dc.Name = "Concepto") Then
                resultado.idxConcepto = indx
                indx = indx + 1
            Else
                indx = indx + 1
            End If
        Next
        Return resultado
    End Function

    Private Sub dgvConceptos_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvConceptos.CellClick
        SeleccionaFila(Me.dgvConceptos, 0)
    End Sub

    'Public Function FiltraIngresos(ByVal prmOpcion As Integer) As DataTable
    '    Try
    '        BaseII.limpiaParametros()
    '        BaseII.CreateMyParameter("@Opcion", SqlDbType.BigInt, prmOpcion)
    '        Select Case prmOpcion
    '            Case 1
    '                BaseII.CreateMyParameter("@IdIngreso", SqlDbType.BigInt, CInt(Me.txtClave.Text))
    '            Case 2
    '                Dim IDGpoIngreso As Integer = 0 'IIf(, 0, Me.cbxGrupo.SelectedValue)

    '                If IsNumeric(Me.cbxGrupo.SelectedValue) = True Then
    '                    IDGpoIngreso = Me.cbxGrupo.SelectedValue
    '                End If
    '                BaseII.CreateMyParameter("@IdGrupoIngresos", SqlDbType.BigInt, IDGpoIngreso)
    '            Case 3
    '                BaseII.CreateMyParameter("@Concepto", SqlDbType.NVarChar, Me.txtConcepto.Text, 150)

    '        End Select

    '        Dim tblConceptos As DataTable = BaseII.ConsultaDT("uspSeleccionaIngresosDesglozados")
    '        Return tblConceptos
    '    Catch ex As Exception
    '        MsgBox(ex.Message, vbInformation, "Filtra Ingresos")
    '    End Try
    'End Function

    Private Sub cbxGrupo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxGrupo.SelectedIndexChanged

        If cbxGrupo.Text.Length = 0 Then
            Exit Sub
        End If

        GloClvGrupo = cbxGrupo.SelectedValue
        uspSeleccionaIngresosDesglozados(2, 0, cbxGrupo.SelectedValue, "")

        'If CargaGrupos Then
        '    Me.dgvConceptos.DataSource = FiltraIngresos(2)
        '    ocultaColumnas(Me.dgvConceptos)
        '    If Me.dgvConceptos.RowCount > 0 Then
        '        SeleccionaFila(Me.dgvConceptos, 1)
        '    End If
        'End If
    End Sub

    Private Sub btnBuscarID_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscarID.Click
        If cbCompania.Text.Length = 0 Then
            Exit Sub
        End If
        If cbxGrupo.Text.Length = 0 Then
            Exit Sub
        End If
        If txtClave.Text.Length = 0 Then
            MessageBox.Show("Captura una Clave.")
            Exit Sub
        End If
        If IsNumeric(txtClave.Text) = False Then
            MessageBox.Show("Captura una Clave numérica.")
            Exit Sub
        End If
        uspSeleccionaIngresosDesglozados(1, txtClave.Text, cbxGrupo.SelectedValue, "")
        'If IsNumeric(Me.txtClave.Text) Then
        '    AplicaFiltro = True
        '    Me.dgvConceptos.DataSource = FiltraIngresos(1)
        '    ocultaColumnas(Me.dgvConceptos)
        '    If Me.dgvConceptos.RowCount > 0 Then
        '        SeleccionaFila(Me.dgvConceptos, 1)
        '    End If
        'End If
    End Sub

    Private Sub btnConcepto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConcepto.Click
        If cbCompania.Text.Length = 0 Then
            Exit Sub
        End If
        If cbxGrupo.Text.Length = 0 Then
            Exit Sub
        End If
        If txtConcepto.Text.Length = 0 Then
            MessageBox.Show("Captura un Concepto.")
            Exit Sub
        End If
        uspSeleccionaIngresosDesglozados(3, 0, cbxGrupo.SelectedValue, txtConcepto.Text)
        'AplicaFiltro = True
        'Me.dgvConceptos.DataSource = FiltraIngresos(3)
        'ocultaColumnas(Me.dgvConceptos)
        'If Me.dgvConceptos.RowCount > 0 Then
        '    SeleccionaFila(Me.dgvConceptos, 1)
        'End If
    End Sub

    Private Sub SalirButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalirButton.Click
        Me.close()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        If cbxGrupo.Text.Length = 0 Then
            MessageBox.Show("Selecciona un Grupo.")
            Exit Sub
        End If
        opcion = "N"
        AplicaFiltro = True
        FrmIngresosDesglozados.Clave = 0
        FrmIngresosDesglozados.Show()
    End Sub

    Private Sub ConsultarButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarButton.Click
        If dgvConceptos.Rows.Count = 0 Then
            MessageBox.Show("Selecciona registro a Consultar.")
            Exit Sub
        End If
        opcion = "C"
        AplicaFiltro = True
        FrmIngresosDesglozados.Clave = dgvConceptos.SelectedCells(0).Value
        FrmIngresosDesglozados.IdConceptoText.Text = dgvConceptos.SelectedCells(0).Value
        FrmIngresosDesglozados.Show()
    End Sub

    Private Sub ModificarButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ModificarButton.Click
        If dgvConceptos.Rows.Count = 0 Then
            MessageBox.Show("Selecciona registro a Modificar.")
            Exit Sub
        End If
        opcion = "M"
        AplicaFiltro = True
        FrmIngresosDesglozados.Clave = dgvConceptos.SelectedCells(0).Value
        FrmIngresosDesglozados.IdConceptoText.Text = dgvConceptos.SelectedCells(0).Value
        FrmIngresosDesglozados.Show()
    End Sub


    Private Sub cbCompania_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbCompania.SelectedIndexChanged
        If cbCompania.Text.Length = 0 Then
            Exit Sub
        End If
        GloClvCompania = cbCompania.SelectedValue

        cbxGrupo.Text = ""
        dgvConceptos.DataSource = dT

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLVCOMPANIA", SqlDbType.Int, GloClvCompania)
        cbxGrupo.DataSource = BaseII.ConsultaDT("Softv_GetGrupoIngreso")
    End Sub

    Private Sub uspSeleccionaIngresosDesglozados(ByVal Opcion As Integer, ByVal IdIngreso As Integer, ByVal IdGrupoIngresos As Integer, ByVal Concepto As String)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Opcion", SqlDbType.Int, Opcion)
        BaseII.CreateMyParameter("@IdIngreso", SqlDbType.Int, IdIngreso)
        BaseII.CreateMyParameter("@IdGrupoIngresos", SqlDbType.Int, IdGrupoIngresos)
        BaseII.CreateMyParameter("@Concepto", SqlDbType.VarChar, Concepto, 150)
        dgvConceptos.DataSource = BaseII.ConsultaDT("uspSeleccionaIngresosDesglozados")
    End Sub


End Class