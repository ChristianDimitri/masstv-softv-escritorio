﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelMesAno
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CMBLblMes1 = New System.Windows.Forms.Label()
        Me.Mes1NumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Ano1NumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.CMBLabel3 = New System.Windows.Forms.Label()
        Me.Ano2NumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.CMBLabel4 = New System.Windows.Forms.Label()
        Me.Mes2NumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.CMBLabel5 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        CType(Me.Mes1NumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ano1NumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ano2NumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Mes2NumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBLblMes1
        '
        Me.CMBLblMes1.AutoSize = True
        Me.CMBLblMes1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLblMes1.Location = New System.Drawing.Point(166, 43)
        Me.CMBLblMes1.Name = "CMBLblMes1"
        Me.CMBLblMes1.Size = New System.Drawing.Size(40, 18)
        Me.CMBLblMes1.TabIndex = 2
        Me.CMBLblMes1.Text = "Mes"
        '
        'Mes1NumericUpDown
        '
        Me.Mes1NumericUpDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Mes1NumericUpDown.Location = New System.Drawing.Point(159, 64)
        Me.Mes1NumericUpDown.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.Mes1NumericUpDown.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.Mes1NumericUpDown.Name = "Mes1NumericUpDown"
        Me.Mes1NumericUpDown.Size = New System.Drawing.Size(71, 24)
        Me.Mes1NumericUpDown.TabIndex = 3
        Me.Mes1NumericUpDown.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(245, 43)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(42, 18)
        Me.CMBLabel1.TabIndex = 4
        Me.CMBLabel1.Text = "Año "
        '
        'Ano1NumericUpDown
        '
        Me.Ano1NumericUpDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Ano1NumericUpDown.Location = New System.Drawing.Point(236, 64)
        Me.Ano1NumericUpDown.Maximum = New Decimal(New Integer() {2050, 0, 0, 0})
        Me.Ano1NumericUpDown.Minimum = New Decimal(New Integer() {2011, 0, 0, 0})
        Me.Ano1NumericUpDown.Name = "Ano1NumericUpDown"
        Me.Ano1NumericUpDown.Size = New System.Drawing.Size(71, 24)
        Me.Ano1NumericUpDown.TabIndex = 5
        Me.Ano1NumericUpDown.Value = New Decimal(New Integer() {2011, 0, 0, 0})
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(38, 66)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(115, 18)
        Me.CMBLabel2.TabIndex = 6
        Me.CMBLabel2.Text = "Rango Inicial :"
        '
        'CMBLabel3
        '
        Me.CMBLabel3.AutoSize = True
        Me.CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.Location = New System.Drawing.Point(38, 135)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(108, 18)
        Me.CMBLabel3.TabIndex = 11
        Me.CMBLabel3.Text = "Rango Final :"
        '
        'Ano2NumericUpDown
        '
        Me.Ano2NumericUpDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Ano2NumericUpDown.Location = New System.Drawing.Point(236, 133)
        Me.Ano2NumericUpDown.Maximum = New Decimal(New Integer() {2050, 0, 0, 0})
        Me.Ano2NumericUpDown.Minimum = New Decimal(New Integer() {2011, 0, 0, 0})
        Me.Ano2NumericUpDown.Name = "Ano2NumericUpDown"
        Me.Ano2NumericUpDown.Size = New System.Drawing.Size(71, 24)
        Me.Ano2NumericUpDown.TabIndex = 10
        Me.Ano2NumericUpDown.Value = New Decimal(New Integer() {2011, 0, 0, 0})
        '
        'CMBLabel4
        '
        Me.CMBLabel4.AutoSize = True
        Me.CMBLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel4.Location = New System.Drawing.Point(245, 112)
        Me.CMBLabel4.Name = "CMBLabel4"
        Me.CMBLabel4.Size = New System.Drawing.Size(42, 18)
        Me.CMBLabel4.TabIndex = 9
        Me.CMBLabel4.Text = "Año "
        '
        'Mes2NumericUpDown
        '
        Me.Mes2NumericUpDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Mes2NumericUpDown.Location = New System.Drawing.Point(159, 133)
        Me.Mes2NumericUpDown.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.Mes2NumericUpDown.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.Mes2NumericUpDown.Name = "Mes2NumericUpDown"
        Me.Mes2NumericUpDown.Size = New System.Drawing.Size(71, 24)
        Me.Mes2NumericUpDown.TabIndex = 8
        Me.Mes2NumericUpDown.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'CMBLabel5
        '
        Me.CMBLabel5.AutoSize = True
        Me.CMBLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel5.Location = New System.Drawing.Point(166, 112)
        Me.CMBLabel5.Name = "CMBLabel5"
        Me.CMBLabel5.Size = New System.Drawing.Size(40, 18)
        Me.CMBLabel5.TabIndex = 7
        Me.CMBLabel5.Text = "Mes"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(94, 214)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 12
        Me.Button1.Text = "&ACEPTAR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(236, 214)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 36)
        Me.Button5.TabIndex = 13
        Me.Button5.Text = "&CERRAR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'FrmSelMesAno
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(393, 262)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.CMBLabel3)
        Me.Controls.Add(Me.Ano2NumericUpDown)
        Me.Controls.Add(Me.CMBLabel4)
        Me.Controls.Add(Me.Mes2NumericUpDown)
        Me.Controls.Add(Me.CMBLabel5)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.Ano1NumericUpDown)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.Mes1NumericUpDown)
        Me.Controls.Add(Me.CMBLblMes1)
        Me.MaximizeBox = False
        Me.Name = "FrmSelMesAno"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Seleccione el Rango"
        CType(Me.Mes1NumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ano1NumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ano2NumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Mes2NumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBLblMes1 As System.Windows.Forms.Label
    Friend WithEvents Mes1NumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents Ano1NumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents Ano2NumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents CMBLabel4 As System.Windows.Forms.Label
    Friend WithEvents Mes2NumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents CMBLabel5 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
End Class
