<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmContratacionCombo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.CMBBtnAceptar = New System.Windows.Forms.Button
        Me.CMBBtnSalir = New System.Windows.Forms.Button
        Me.CMBLbl1 = New System.Windows.Forms.Label
        Me.Cmb1 = New System.Windows.Forms.ComboBox
        Me.MuestraCombosclContratanBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Procedimientosarnoldo4 = New sofTV.Procedimientosarnoldo4
        Me.Muestra_Combos_clContratanTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Muestra_Combos_clContratanTableAdapter
        CType(Me.MuestraCombosclContratanBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBBtnAceptar
        '
        Me.CMBBtnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBBtnAceptar.Location = New System.Drawing.Point(115, 93)
        Me.CMBBtnAceptar.Name = "CMBBtnAceptar"
        Me.CMBBtnAceptar.Size = New System.Drawing.Size(104, 29)
        Me.CMBBtnAceptar.TabIndex = 2
        Me.CMBBtnAceptar.Text = "&GUARDAR"
        Me.CMBBtnAceptar.UseVisualStyleBackColor = True
        '
        'CMBBtnSalir
        '
        Me.CMBBtnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBBtnSalir.Location = New System.Drawing.Point(240, 93)
        Me.CMBBtnSalir.Name = "CMBBtnSalir"
        Me.CMBBtnSalir.Size = New System.Drawing.Size(104, 29)
        Me.CMBBtnSalir.TabIndex = 3
        Me.CMBBtnSalir.Text = "&SALIR"
        Me.CMBBtnSalir.UseVisualStyleBackColor = True
        '
        'CMBLbl1
        '
        Me.CMBLbl1.AutoSize = True
        Me.CMBLbl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLbl1.Location = New System.Drawing.Point(12, 21)
        Me.CMBLbl1.Name = "CMBLbl1"
        Me.CMBLbl1.Size = New System.Drawing.Size(161, 16)
        Me.CMBLbl1.TabIndex = 4
        Me.CMBLbl1.Text = "Seleccione El Combo:"
        '
        'Cmb1
        '
        Me.Cmb1.DataSource = Me.MuestraCombosclContratanBindingSource
        Me.Cmb1.DisplayMember = "Combo"
        Me.Cmb1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cmb1.FormattingEnabled = True
        Me.Cmb1.Location = New System.Drawing.Point(12, 40)
        Me.Cmb1.Name = "Cmb1"
        Me.Cmb1.Size = New System.Drawing.Size(336, 24)
        Me.Cmb1.TabIndex = 5
        Me.Cmb1.ValueMember = "clv_descuento"
        '
        'MuestraCombosclContratanBindingSource
        '
        Me.MuestraCombosclContratanBindingSource.DataMember = "Muestra_Combos_clContratan"
        Me.MuestraCombosclContratanBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Procedimientosarnoldo4
        '
        Me.Procedimientosarnoldo4.DataSetName = "Procedimientosarnoldo4"
        Me.Procedimientosarnoldo4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Muestra_Combos_clContratanTableAdapter
        '
        Me.Muestra_Combos_clContratanTableAdapter.ClearBeforeFill = True
        '
        'FrmContratacionCombo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(366, 139)
        Me.Controls.Add(Me.Cmb1)
        Me.Controls.Add(Me.CMBLbl1)
        Me.Controls.Add(Me.CMBBtnSalir)
        Me.Controls.Add(Me.CMBBtnAceptar)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmContratacionCombo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selecciona Combos"
        CType(Me.MuestraCombosclContratanBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBBtnAceptar As System.Windows.Forms.Button
    Friend WithEvents CMBBtnSalir As System.Windows.Forms.Button
    Friend WithEvents CMBLbl1 As System.Windows.Forms.Label
    Friend WithEvents Cmb1 As System.Windows.Forms.ComboBox
    Friend WithEvents MuestraCombosclContratanBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Procedimientosarnoldo4 As sofTV.Procedimientosarnoldo4
    Friend WithEvents Muestra_Combos_clContratanTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Muestra_Combos_clContratanTableAdapter
End Class
