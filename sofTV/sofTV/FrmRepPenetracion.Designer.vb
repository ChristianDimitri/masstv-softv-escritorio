<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRepPenetracion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim TotalLabel As System.Windows.Forms.Label
        Dim CLabel As System.Windows.Forms.Label
        Dim ILabel As System.Windows.Forms.Label
        Dim DLabel As System.Windows.Forms.Label
        Dim SLabel As System.Windows.Forms.Label
        Dim BLabel As System.Windows.Forms.Label
        Dim FLabel As System.Windows.Forms.Label
        Dim SalidasTapsLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.CMBLabel7 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.OpDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescripcionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CatalogoReportesPenetracionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetarnoldo = New sofTV.DataSetarnoldo()
        Me.ComboBox4 = New System.Windows.Forms.ComboBox()
        Me.MuestraTipSerPrincipalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.MuestraTipSerPrincipalTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MuestraTipSerPrincipalTableAdapter()
        Me.Catalogo_Reportes_PenetracionTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Catalogo_Reportes_PenetracionTableAdapter()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.pnlControlesOcultos = New System.Windows.Forms.Panel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.SalidasTapsTextBox = New System.Windows.Forms.TextBox()
        Me.Dame_Datos_Rep_PBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TotalTextBox = New System.Windows.Forms.TextBox()
        Me.CTextBox = New System.Windows.Forms.TextBox()
        Me.ITextBox = New System.Windows.Forms.TextBox()
        Me.DTextBox = New System.Windows.Forms.TextBox()
        Me.STextBox = New System.Windows.Forms.TextBox()
        Me.BTextBox = New System.Windows.Forms.TextBox()
        Me.FTextBox = New System.Windows.Forms.TextBox()
        Me.Dame_Datos_Rep_PTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Dame_Datos_Rep_PTableAdapter()
        Me.Borrar_Session_ServiciosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Borrar_Session_ServiciosTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Borrar_Session_ServiciosTableAdapter()
        Me.DameClv_Session_ServiciosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameClv_Session_ServiciosTableAdapter = New sofTV.DataSetarnoldoTableAdapters.DameClv_Session_ServiciosTableAdapter()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        TotalLabel = New System.Windows.Forms.Label()
        CLabel = New System.Windows.Forms.Label()
        ILabel = New System.Windows.Forms.Label()
        DLabel = New System.Windows.Forms.Label()
        SLabel = New System.Windows.Forms.Label()
        BLabel = New System.Windows.Forms.Label()
        FLabel = New System.Windows.Forms.Label()
        SalidasTapsLabel = New System.Windows.Forms.Label()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CatalogoReportesPenetracionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTipSerPrincipalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.Dame_Datos_Rep_PBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Borrar_Session_ServiciosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameClv_Session_ServiciosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TotalLabel
        '
        TotalLabel.AutoSize = True
        TotalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TotalLabel.Location = New System.Drawing.Point(6, 35)
        TotalLabel.Name = "TotalLabel"
        TotalLabel.Size = New System.Drawing.Size(107, 13)
        TotalLabel.TabIndex = 0
        TotalLabel.Text = "Total de Clientes:"
        '
        'CLabel
        '
        CLabel.AutoSize = True
        CLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CLabel.Location = New System.Drawing.Point(15, 63)
        CLabel.Name = "CLabel"
        CLabel.Size = New System.Drawing.Size(79, 13)
        CLabel.TabIndex = 2
        CLabel.Text = "Contratados:"
        '
        'ILabel
        '
        ILabel.AutoSize = True
        ILabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ILabel.Location = New System.Drawing.Point(15, 93)
        ILabel.Name = "ILabel"
        ILabel.Size = New System.Drawing.Size(69, 13)
        ILabel.TabIndex = 4
        ILabel.Text = "Instalados:"
        '
        'DLabel
        '
        DLabel.AutoSize = True
        DLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DLabel.Location = New System.Drawing.Point(15, 126)
        DLabel.Name = "DLabel"
        DLabel.Size = New System.Drawing.Size(99, 13)
        DLabel.TabIndex = 6
        DLabel.Text = "Desconectados:"
        '
        'SLabel
        '
        SLabel.AutoSize = True
        SLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SLabel.Location = New System.Drawing.Point(15, 154)
        SLabel.Name = "SLabel"
        SLabel.Size = New System.Drawing.Size(83, 13)
        SLabel.TabIndex = 8
        SLabel.Text = "Suspendidos:"
        '
        'BLabel
        '
        BLabel.AutoSize = True
        BLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        BLabel.Location = New System.Drawing.Point(15, 182)
        BLabel.Name = "BLabel"
        BLabel.Size = New System.Drawing.Size(36, 13)
        BLabel.TabIndex = 10
        BLabel.Text = "Baja:"
        '
        'FLabel
        '
        FLabel.AutoSize = True
        FLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FLabel.Location = New System.Drawing.Point(15, 211)
        FLabel.Name = "FLabel"
        FLabel.Size = New System.Drawing.Size(91, 13)
        FLabel.TabIndex = 12
        FLabel.Text = "Fuera de Area:"
        '
        'SalidasTapsLabel
        '
        SalidasTapsLabel.AutoSize = True
        SalidasTapsLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SalidasTapsLabel.Location = New System.Drawing.Point(22, 241)
        SalidasTapsLabel.Name = "SalidasTapsLabel"
        SalidasTapsLabel.Size = New System.Drawing.Size(84, 13)
        SalidasTapsLabel.TabIndex = 14
        SalidasTapsLabel.Text = "Salidas Taps:"
        '
        'CMBLabel7
        '
        Me.CMBLabel7.AutoSize = True
        Me.CMBLabel7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel7.Location = New System.Drawing.Point(9, 6)
        Me.CMBLabel7.Name = "CMBLabel7"
        Me.CMBLabel7.Size = New System.Drawing.Size(154, 18)
        Me.CMBLabel7.TabIndex = 27
        Me.CMBLabel7.Text = "Tipo de Servicios  :"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.DarkKhaki
        Me.DataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Olive
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.DarkKhaki
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.OpDataGridViewTextBoxColumn, Me.DescripcionDataGridViewTextBoxColumn})
        Me.DataGridView1.DataSource = Me.CatalogoReportesPenetracionBindingSource
        Me.DataGridView1.Location = New System.Drawing.Point(-41, -2)
        Me.DataGridView1.MultiSelect = False
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(300, 203)
        Me.DataGridView1.TabIndex = 24
        Me.DataGridView1.TabStop = False
        '
        'OpDataGridViewTextBoxColumn
        '
        Me.OpDataGridViewTextBoxColumn.DataPropertyName = "op"
        Me.OpDataGridViewTextBoxColumn.HeaderText = "op"
        Me.OpDataGridViewTextBoxColumn.Name = "OpDataGridViewTextBoxColumn"
        Me.OpDataGridViewTextBoxColumn.ReadOnly = True
        Me.OpDataGridViewTextBoxColumn.Visible = False
        '
        'DescripcionDataGridViewTextBoxColumn
        '
        Me.DescripcionDataGridViewTextBoxColumn.DataPropertyName = "Descripcion"
        Me.DescripcionDataGridViewTextBoxColumn.HeaderText = "Reportes"
        Me.DescripcionDataGridViewTextBoxColumn.Name = "DescripcionDataGridViewTextBoxColumn"
        Me.DescripcionDataGridViewTextBoxColumn.ReadOnly = True
        Me.DescripcionDataGridViewTextBoxColumn.Width = 258
        '
        'CatalogoReportesPenetracionBindingSource
        '
        Me.CatalogoReportesPenetracionBindingSource.DataMember = "Catalogo_Reportes_Penetracion"
        Me.CatalogoReportesPenetracionBindingSource.DataSource = Me.DataSetarnoldo
        '
        'DataSetarnoldo
        '
        Me.DataSetarnoldo.DataSetName = "DataSetarnoldo"
        Me.DataSetarnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ComboBox4
        '
        Me.ComboBox4.DataSource = Me.MuestraTipSerPrincipalBindingSource
        Me.ComboBox4.DisplayMember = "Concepto"
        Me.ComboBox4.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ComboBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox4.ForeColor = System.Drawing.Color.Red
        Me.ComboBox4.FormattingEnabled = True
        Me.ComboBox4.Location = New System.Drawing.Point(12, 27)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(259, 24)
        Me.ComboBox4.TabIndex = 25
        Me.ComboBox4.ValueMember = "Clv_TipSerPrincipal"
        '
        'MuestraTipSerPrincipalBindingSource
        '
        Me.MuestraTipSerPrincipalBindingSource.DataMember = "MuestraTipSerPrincipal"
        Me.MuestraTipSerPrincipalBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.CrystalReportViewer1)
        Me.Panel1.Location = New System.Drawing.Point(286, 6)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(727, 674)
        Me.Panel1.TabIndex = 28
        '
        'CrystalReportViewer1
        '
        Me.CrystalReportViewer1.ActiveViewIndex = -1
        Me.CrystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CrystalReportViewer1.DisplayGroupTree = False
        Me.CrystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrystalReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
        Me.CrystalReportViewer1.SelectionFormula = ""
        Me.CrystalReportViewer1.Size = New System.Drawing.Size(727, 674)
        Me.CrystalReportViewer1.TabIndex = 1
        Me.CrystalReportViewer1.TabStop = False
        Me.CrystalReportViewer1.ViewTimeSelectionFormula = ""
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Olive
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(499, 687)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(226, 33)
        Me.Button1.TabIndex = 29
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'MuestraTipSerPrincipalTableAdapter
        '
        Me.MuestraTipSerPrincipalTableAdapter.ClearBeforeFill = True
        '
        'Catalogo_Reportes_PenetracionTableAdapter
        '
        Me.Catalogo_Reportes_PenetracionTableAdapter.ClearBeforeFill = True
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.DataGridView1)
        Me.Panel2.Location = New System.Drawing.Point(12, 57)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(259, 199)
        Me.Panel2.TabIndex = 30
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.pnlControlesOcultos)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.TextBox3)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.TextBox2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(SalidasTapsLabel)
        Me.GroupBox1.Controls.Add(Me.SalidasTapsTextBox)
        Me.GroupBox1.Controls.Add(TotalLabel)
        Me.GroupBox1.Controls.Add(Me.TotalTextBox)
        Me.GroupBox1.Controls.Add(CLabel)
        Me.GroupBox1.Controls.Add(Me.CTextBox)
        Me.GroupBox1.Controls.Add(ILabel)
        Me.GroupBox1.Controls.Add(Me.ITextBox)
        Me.GroupBox1.Controls.Add(DLabel)
        Me.GroupBox1.Controls.Add(Me.DTextBox)
        Me.GroupBox1.Controls.Add(SLabel)
        Me.GroupBox1.Controls.Add(Me.STextBox)
        Me.GroupBox1.Controls.Add(BLabel)
        Me.GroupBox1.Controls.Add(Me.BTextBox)
        Me.GroupBox1.Controls.Add(FLabel)
        Me.GroupBox1.Controls.Add(Me.FTextBox)
        Me.GroupBox1.Enabled = False
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(252, 683)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(10, 10)
        Me.GroupBox1.TabIndex = 31
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Resultados."
        Me.GroupBox1.Visible = False
        '
        'pnlControlesOcultos
        '
        Me.pnlControlesOcultos.Location = New System.Drawing.Point(9, 236)
        Me.pnlControlesOcultos.Name = "pnlControlesOcultos"
        Me.pnlControlesOcultos.Size = New System.Drawing.Size(232, 187)
        Me.pnlControlesOcultos.TabIndex = 32
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(220, 402)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(21, 16)
        Me.Label6.TabIndex = 24
        Me.Label6.Text = "%"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(220, 348)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(21, 16)
        Me.Label5.TabIndex = 23
        Me.Label5.Text = "%"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(220, 295)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(21, 16)
        Me.Label4.TabIndex = 22
        Me.Label4.Text = "%"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(9, 399)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(209, 22)
        Me.TextBox3.TabIndex = 21
        Me.TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(6, 383)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(235, 13)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Penetración en Base a Clientes Activos:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 329)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(239, 13)
        Me.Label2.TabIndex = 19
        Me.Label2.Text = "Penetración en Base al Total de Cartera:"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(9, 345)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(209, 22)
        Me.TextBox2.TabIndex = 18
        Me.TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 279)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(180, 13)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Penetración Máxima Por Casa:"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(9, 295)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(209, 22)
        Me.TextBox1.TabIndex = 16
        Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'SalidasTapsTextBox
        '
        Me.SalidasTapsTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Dame_Datos_Rep_PBindingSource, "SalidasTaps", True))
        Me.SalidasTapsTextBox.Location = New System.Drawing.Point(117, 236)
        Me.SalidasTapsTextBox.Name = "SalidasTapsTextBox"
        Me.SalidasTapsTextBox.Size = New System.Drawing.Size(100, 22)
        Me.SalidasTapsTextBox.TabIndex = 15
        '
        'Dame_Datos_Rep_PBindingSource
        '
        Me.Dame_Datos_Rep_PBindingSource.DataMember = "Dame_Datos_Rep_P"
        Me.Dame_Datos_Rep_PBindingSource.DataSource = Me.DataSetarnoldo
        '
        'TotalTextBox
        '
        Me.TotalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Dame_Datos_Rep_PBindingSource, "Total_Activos", True))
        Me.TotalTextBox.Location = New System.Drawing.Point(119, 35)
        Me.TotalTextBox.Name = "TotalTextBox"
        Me.TotalTextBox.Size = New System.Drawing.Size(98, 22)
        Me.TotalTextBox.TabIndex = 1
        '
        'CTextBox
        '
        Me.CTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Dame_Datos_Rep_PBindingSource, "Contratados", True))
        Me.CTextBox.Location = New System.Drawing.Point(119, 63)
        Me.CTextBox.Name = "CTextBox"
        Me.CTextBox.Size = New System.Drawing.Size(98, 22)
        Me.CTextBox.TabIndex = 3
        '
        'ITextBox
        '
        Me.ITextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Dame_Datos_Rep_PBindingSource, "Instalados", True))
        Me.ITextBox.Location = New System.Drawing.Point(119, 93)
        Me.ITextBox.Name = "ITextBox"
        Me.ITextBox.Size = New System.Drawing.Size(98, 22)
        Me.ITextBox.TabIndex = 5
        '
        'DTextBox
        '
        Me.DTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Dame_Datos_Rep_PBindingSource, "Desconectados", True))
        Me.DTextBox.Location = New System.Drawing.Point(119, 121)
        Me.DTextBox.Name = "DTextBox"
        Me.DTextBox.Size = New System.Drawing.Size(98, 22)
        Me.DTextBox.TabIndex = 7
        '
        'STextBox
        '
        Me.STextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Dame_Datos_Rep_PBindingSource, "Suspendidos", True))
        Me.STextBox.Location = New System.Drawing.Point(119, 149)
        Me.STextBox.Name = "STextBox"
        Me.STextBox.Size = New System.Drawing.Size(99, 22)
        Me.STextBox.TabIndex = 9
        '
        'BTextBox
        '
        Me.BTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Dame_Datos_Rep_PBindingSource, "Baja", True))
        Me.BTextBox.Location = New System.Drawing.Point(119, 177)
        Me.BTextBox.Name = "BTextBox"
        Me.BTextBox.Size = New System.Drawing.Size(99, 22)
        Me.BTextBox.TabIndex = 11
        '
        'FTextBox
        '
        Me.FTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Dame_Datos_Rep_PBindingSource, "FueraArea", True))
        Me.FTextBox.Location = New System.Drawing.Point(119, 205)
        Me.FTextBox.Name = "FTextBox"
        Me.FTextBox.Size = New System.Drawing.Size(99, 22)
        Me.FTextBox.TabIndex = 13
        '
        'Dame_Datos_Rep_PTableAdapter
        '
        Me.Dame_Datos_Rep_PTableAdapter.ClearBeforeFill = True
        '
        'Borrar_Session_ServiciosBindingSource
        '
        Me.Borrar_Session_ServiciosBindingSource.DataMember = "Borrar_Session_Servicios"
        Me.Borrar_Session_ServiciosBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Borrar_Session_ServiciosTableAdapter
        '
        Me.Borrar_Session_ServiciosTableAdapter.ClearBeforeFill = True
        '
        'DameClv_Session_ServiciosBindingSource
        '
        Me.DameClv_Session_ServiciosBindingSource.DataMember = "DameClv_Session_Servicios"
        Me.DameClv_Session_ServiciosBindingSource.DataSource = Me.DataSetarnoldo
        '
        'DameClv_Session_ServiciosTableAdapter
        '
        Me.DameClv_Session_ServiciosTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'FrmRepPenetracion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1016, 732)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.CMBLabel7)
        Me.Controls.Add(Me.ComboBox4)
        Me.Name = "FrmRepPenetracion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reportes de Penetración"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CatalogoReportesPenetracionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTipSerPrincipalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.Dame_Datos_Rep_PBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Borrar_Session_ServiciosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameClv_Session_ServiciosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBLabel7 As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents ComboBox4 As System.Windows.Forms.ComboBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents MuestraTipSerPrincipalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipSerPrincipalTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MuestraTipSerPrincipalTableAdapter
    Friend WithEvents CatalogoReportesPenetracionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DataSetarnoldo As sofTV.DataSetarnoldo
    Friend WithEvents Catalogo_Reportes_PenetracionTableAdapter As sofTV.DataSetarnoldoTableAdapters.Catalogo_Reportes_PenetracionTableAdapter
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Dame_Datos_Rep_PBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_Datos_Rep_PTableAdapter As sofTV.DataSetarnoldoTableAdapters.Dame_Datos_Rep_PTableAdapter
    Friend WithEvents TotalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ITextBox As System.Windows.Forms.TextBox
    Friend WithEvents DTextBox As System.Windows.Forms.TextBox
    Friend WithEvents STextBox As System.Windows.Forms.TextBox
    Friend WithEvents BTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Borrar_Session_ServiciosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Borrar_Session_ServiciosTableAdapter As sofTV.DataSetarnoldoTableAdapters.Borrar_Session_ServiciosTableAdapter
    Friend WithEvents DameClv_Session_ServiciosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameClv_Session_ServiciosTableAdapter As sofTV.DataSetarnoldoTableAdapters.DameClv_Session_ServiciosTableAdapter
    Friend WithEvents SalidasTapsTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents OpDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescripcionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Private WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents pnlControlesOcultos As System.Windows.Forms.Panel
End Class
