﻿Public Class frmTipoInstalaciones
#Region "Variables"
    Public locDescripcionTipo, locOpcionTipo As String
    Public locClaveTipo As Integer
    Public locActivoTipo As Boolean
#End Region

#Region "Controles"
    Private Sub frmTipoInstalaciones_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If locOpcionTipo <> "Nuevo" Then
            Me.txtClaveTipo.Text = locClaveTipo
            Me.txtDescripcionTipo.Text = locDescripcionTipo
            Me.cbxActivoTipo.Checked = locActivoTipo
        End If
        If locOpcionTipo = "Consultar" Then
            Me.txtDescripcionTipo.ReadOnly = True
            Me.cbxActivoTipo.Enabled = False
            Me.btnGuardar.Enabled = False
        Else
            Me.txtDescripcionTipo.ReadOnly = False
            Me.cbxActivoTipo.Enabled = True
            Me.btnGuardar.Enabled = True
        End If
    End Sub

    Private Sub btnGuardar_Click(sender As System.Object, e As System.EventArgs) Handles btnGuardar.Click
        If Me.txtDescripcionTipo.Text.Length = 0 Then
            MsgBox("¡Capture una Descripción para continuar!", MsgBoxStyle.Information)
            Exit Sub
        End If

        If locOpcionTipo = "Nuevo" Then
            If inserta(Me.txtDescripcionTipo.Text, Me.cbxActivoTipo.Checked) = False Then
                MsgBox("¡Ya existe un registro en la lista con este nombre!", MsgBoxStyle.Information)
                Exit Sub
            Else
                MsgBox("¡Registro almacenado exitosamente!", MsgBoxStyle.Information)
            End If
        ElseIf locOpcionTipo = "Modificar" Then
            modificar(CInt(Me.txtClaveTipo.Text), Me.txtDescripcionTipo.Text, Me.cbxActivoTipo.Checked)
            MsgBox("¡Registro actualizado exitosamente!", MsgBoxStyle.Information)
        End If
        Me.Close()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub
#End Region

#Region "Métodos"
    Dim tipoInstalacion As classTipoInstalacion
    Dim dtTipoInstalacion As DataTable

    Private Function inserta(ByVal prmDescripcion As String, ByVal prmActivo As Boolean) As Boolean  'MÉTODO DE INSERCIÓN A LA TABLA
        tipoInstalacion = New classTipoInstalacion
        dtTipoInstalacion = New DataTable
        tipoInstalacion.descripcionTipo = prmDescripcion
        tipoInstalacion.activoTipo = prmActivo
        dtTipoInstalacion = tipoInstalacion.uspInsertaTblTipoInstalacion()

        If dtTipoInstalacion.Rows.Count > 0 Then
            Me.txtClaveTipo.Text = dtTipoInstalacion.Rows(0)("idTipoInstalacion").ToString()
            inserta = CBool(dtTipoInstalacion.Rows(0)("bndInsertar").ToString())
        End If
    End Function

    Private Sub modificar(ByVal prmClaveTipo As Integer, ByVal prmDescripcion As String, ByVal prmActivo As Boolean)
        tipoInstalacion = New classTipoInstalacion
        tipoInstalacion.idTipoInstalacion = prmClaveTipo
        tipoInstalacion.descripcionTipo = prmDescripcion
        tipoInstalacion.activoTipo = prmActivo
        tipoInstalacion.uspModificaTblTipoInstalacion()
    End Sub
#End Region
End Class