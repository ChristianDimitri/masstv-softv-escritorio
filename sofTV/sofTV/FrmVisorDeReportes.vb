﻿Public Class FrmVisorDeReportes

    Private Sub FrmVisorDeReportes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Cargamos el Reporte que se nos haya pasado
        Try

            'Cargamos el ReportDocument en el Visor
            CrystalReportViewer1.ReportSource = rDocument
            CrystalReportViewer1.Zoom(80)

        Catch ex As Exception

            MsgBox("", MsgBoxStyle.Critical)
            MsgBox(ex.Message, MsgBoxStyle.Critical)

        End Try

    End Sub
    Private Sub FrmVisorDeReportes_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed

        'Liberamos Recursos
        rDocument.Dispose()
        Me.Dispose()

    End Sub

End Class