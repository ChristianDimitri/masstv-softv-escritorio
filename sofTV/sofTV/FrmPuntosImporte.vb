﻿Imports System.Text
Imports System.Data.SqlClient

Public Class FrmPuntosImporte

    Private Sub ConPuntosImporte()
        Dim conexion As New SqlConnection(MiConexion)
        Dim sBuilder As New StringBuilder("EXEC ConPuntosImporte")
        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
        Dim dTable As New DataTable
        Dim bSource As New BindingSource

        Try
            dAdapter.Fill(dTable)
            bSource.DataSource = dTable
            dgPuntos.DataSource = bSource
        Catch ex As Exception
            MsgBox(ex.InnerException, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub NuePuntosImporte(ByVal Puntos As Integer, ByVal Importe As Decimal)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NuePuntosImporte", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Puntos", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = Puntos
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Importe", SqlDbType.Decimal)
        par2.Direction = ParameterDirection.Input
        par2.Value = Importe
        comando.Parameters.Add(par2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.InnerException.ToString(), MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub BorPuntosImporte(ByVal Id As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorPuntosImporte", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Id", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = Id
        comando.Parameters.Add(par1)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.InnerException.ToString(), MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub ConPuntosImporteAdicional()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ConPuntosImporteAdicional", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0
        Dim reader As SqlDataReader

        Try
            conexion.Open()
            reader = comando.ExecuteReader
            While (reader.Read())
                tbPuntosAdicionales.Text = reader(0).ToString()
            End While
        Catch ex As Exception
            MsgBox(ex.InnerException.ToString(), MsgBoxStyle.Exclamation)
        End Try


    End Sub

    Private Sub NuePuntosImporteAdicional(ByVal Importe As Decimal)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NuePuntosImporteAdicional", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Importe", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = Importe
        comando.Parameters.Add(par1)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            MsgBox("Se ha guardado con éxito el importe a punto adicional.", MsgBoxStyle.Information)
        Catch ex As Exception
            MsgBox(ex.InnerException.ToString(), MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub ConPuntosImporteFacturacion()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ConPuntosImporteFacturacion", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0
        Dim reader As SqlDataReader

        Try
            conexion.Open()
            reader = comando.ExecuteReader
            While (reader.Read())
                tbPuntosFactura.Text = reader(0).ToString()
                tbImporteFacturas.Text = reader(1).ToString()
            End While
        Catch ex As Exception
            MsgBox(ex.InnerException.ToString(), MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub NuePuntosImporteFacturacion(ByVal Puntos As Integer, ByVal Importe As Decimal)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NuePuntosImporteFacturacion", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Puntos", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = Puntos
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Importe", SqlDbType.Decimal)
        par2.Direction = ParameterDirection.Input
        par2.Value = Importe
        comando.Parameters.Add(par2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            MsgBox("Se ha guardado con éxito.", MsgBoxStyle.Information)
        Catch ex As Exception
            MsgBox(ex.InnerException.ToString(), MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub bnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnAgregar.Click
        If tbPuntos.Text.Length = 0 Then
            MsgBox("Captura el número de puntos.", MsgBoxStyle.Information)
            Exit Sub
        End If
        If tbImporte.Text.Length = 0 Then
            MsgBox("Captura el importe.", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(tbPuntos.Text) = False Then
            MsgBox("Captura el número de puntos.", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(tbImporte.Text) = False Then
            MsgBox("Captura el importe.", MsgBoxStyle.Information)
            Exit Sub
        End If

        NuePuntosImporte(tbPuntos.Text, tbImporte.Text)
        ConPuntosImporte()
        tbPuntos.Clear()
        tbImporte.Clear()

    End Sub

    Private Sub bnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnEliminar.Click
        If dgPuntos.Rows.Count = 0 Then
            MsgBox("Selecciona un registro a eliminar")
            Exit Sub
        End If
        BorPuntosImporte(dgPuntos.SelectedCells.Item(0).Value)
        ConPuntosImporte()
    End Sub

    Private Sub bnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnGuardar.Click
        If tbPuntosAdicionales.Text.Length = 0 Then
            MsgBox("Caputa el importe a puntos adicionales.", MsgBoxStyle.Information)
            Exit Sub
        End If
        NuePuntosImporteAdicional(tbPuntosAdicionales.Text)

    End Sub

    Private Sub bnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnSalir.Click
        Me.Close()

    End Sub

    Private Sub FrmPuntosImporte_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        ConPuntosImporte()
        ConPuntosImporteAdicional()
        ConPuntosImporteFacturacion()
    End Sub


    Private Sub bnGuardarFacturas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnGuardarFacturas.Click

        If tbImporteFacturas.Text.Length = 0 Then
            MsgBox("Captura importe.", MsgBoxStyle.Information)
            Exit Sub
        End If

        If IsNumeric(tbImporteFacturas.Text) = False Then tbImporteFacturas.Text = "0"

        If tbPuntosFactura.Text.Length = 0 Then
            MsgBox("Captura puntos.", MsgBoxStyle.Information)
            Exit Sub
        End If

        If IsNumeric(tbPuntosFactura.Text) = False Then tbPuntosFactura.Text = "0"

        NuePuntosImporteFacturacion(tbPuntosFactura.Text, tbImporteFacturas.Text)

    End Sub
End Class