Imports System.Data.SqlClient
Public Class BrwColonias

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub BuscaColonia(ByVal op As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            If op = 0 Then
                If IsNumeric(Me.TextBox1.Text) = True Then
                    Me.BusColoniaTableAdapter.Connection = CON
                    Me.BusColoniaTableAdapter.Fill(Me.NewSofTvDataSet.BusColonia, New System.Nullable(Of Integer)(CType(Me.TextBox1.Text, Integer)), "", New System.Nullable(Of Integer)(CType(0, Integer)))
                Else
                    MsgBox("La Busqueda no se Puede realizar con datos en blanco")
                End If
            ElseIf op = 1 Then
                Me.BusColoniaTableAdapter.Connection = CON
                Me.BusColoniaTableAdapter.Fill(Me.NewSofTvDataSet.BusColonia, New System.Nullable(Of Integer)(CType(0, Integer)), Me.TextBox2.Text, New System.Nullable(Of Integer)(CType(1, Integer)))
            Else
                Me.BusColoniaTableAdapter.Connection = CON
                Me.BusColoniaTableAdapter.Fill(Me.NewSofTvDataSet.BusColonia, New System.Nullable(Of Integer)(CType(0, Integer)), "", New System.Nullable(Of Integer)(CType(3, Integer)))
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()

    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Me.BuscaColonia(0)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.BuscaColonia(1)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BuscaColonia(0)
        End If
    End Sub



    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BuscaColonia(1)
        End If
    End Sub



    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opcion = "N"
        FrmColonias.Show()
    End Sub

    Private Sub consultar()
        If GloClv_COLONIA > 0 Then
            opcion = "C"
            FrmColonias.Show()
        Else
            MsgBox("Seleccione una Colonia")
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.DataGridView1.RowCount > 0 Then
            consultar()
        Else
            MsgBox(mensaje2)
        End If
    End Sub

    Private Sub modificar()
        If GloClv_COLONIA > 0 Then
            opcion = "M"
            FrmColonias.Show()
        Else
            MsgBox("Seleccione una Colonia")
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If Me.DataGridView1.RowCount > 0 Then
            modificar()
        Else
            MsgBox(mensaje1)
        End If
    End Sub



    Private Sub Clv_calleLabel2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Clv_calleLabel2.TextChanged
        GloClv_COLONIA = Me.Clv_calleLabel2.Text
    End Sub

    Private Sub BrwColonias_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            Me.BuscaColonia(3)
        End If
    End Sub

    Private Sub BrwColonias_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.DamePermisosFormTableAdapter.Fill(Me.NewSofTvDataSet.DamePermisosForm, GloTipoUsuario, Me.Name, 1, glolec, gloescr, gloctr)
        If gloescr = 1 Then
            Me.Button2.Enabled = False
            Me.Button4.Enabled = False
        End If
        Me.BuscaColonia(3)
    End Sub


    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If Button3.Enabled = True Then
            consultar()
        ElseIf Button4.Enabled = True Then
            modificar()
        End If
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click

    End Sub
End Class