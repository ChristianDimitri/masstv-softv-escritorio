Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine

Public Class FrmSelEstado_Mensajes
    Dim LiLocBndC As Integer
    Dim LiLocBndB As Integer
    Dim LiLocBndI As Integer
    Dim LiLocBndD As Integer
    Dim LiLocBndS As Integer
    Dim LiLocBndF As Integer
    Dim LiLocBndDT As Integer
    Dim LiPeriodo1 As Integer
    Dim LiPeriodo2 As Integer
    Dim ultimo_mes As Integer = 0
    Dim Ultimo_anio As Integer = 0
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim clave As String


        LiLocBndI = 1


        If Me.CheckBox8.Checked = True Then
            If Me.ComboBox1.Text = "" Then
                MsgBox("Se Debe de Seleccionar un Mes ", MsgBoxStyle.Information)
                Exit Sub
            Else
                clave = Me.ComboBox1.SelectedValue
            End If
        Else
            clave = 0
        End If

        If Me.TextBox3.Text.Trim.Length > 0 Then
            If CInt(Me.TextBox3.Text) >= 2007 Then
                Ultimo_anio = Me.TextBox3.Text.Trim
            End If
        End If

        If Me.TextBox1.Text.Trim.Length = 0 Then
            MsgBox("Debes de Capturar un Mensaje", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.CheckBox1.Checked = False And Me.CheckBox2.Checked = False And Me.CheckBox3.Checked = False And Me.CheckBox4.Checked = False And Me.CheckBox5.Checked = False And Me.CheckBox6.Checked = False And Me.CheckBox7.Checked = False Then
            MsgBox("Primero selecciona minimo un tipo de Status", MsgBoxStyle.Information)
        Else
            If Programacion <> 27 Then
                gloClvTipSerReportes = 0
                Dim consulta As String
                consulta = "EXEC Mensaje_TiposCliente_Ciudad2 " + CStr(LocClv_session) + "," + CStr(GloClv_tipser2) + "," + CStr(LiLocBndC) + "," + CStr(LiLocBndB) + "," + CStr(LiLocBndI) + "," + CStr(LiLocBndD) + "," + CStr(LiLocBndS) + "," + CStr(LiLocBndF) + ",0,1 " + "," + CStr(LocValidaHab) + "," + CStr(LiPeriodo1) + "," + CStr(LiPeriodo2) + ",'" + CStr(Me.TextBox1.Text.Trim) + "'," + CStr(clave) + "," + CStr(Ultimo_anio) + "," + CStr(CInt(cbxMorosos.CheckState)) + "," + CStr(CInt(cbxDesconexion.CheckState)) + "," + CStr(CInt(cbxPrimerPago.CheckState)) + "," + CStr(CInt(cbxPagoNormal.CheckState))
                Using CON As New SqlConnection(MiConexion)
                    CON.Open()
                    ' LocOp = 22
                    Dim comando As SqlClient.SqlCommand
                    comando = New SqlClient.SqlCommand
                    With comando
                        .Connection = CON

                        .CommandText = consulta
                        '.CommandText = "EXEC Mensaje_TiposCliente_Ciudad " + LocClv_session + "," + GloClv_tipser2 + "," + LiLocBndC + "," + LiLocBndB + "," + LiLocBndI + "," + LiLocBndD + "," + LiLocBndS + "," + LiLocBndF + ",0,1 " + "," + LocValidaHab + "," + LiPeriodo1 + "," + LiPeriodo2 + "," + Me.TextBox1.Text.Trim + "," + ultimo_mes + "," + Ultimo_anio
                        .CommandType = CommandType.Text
                        .CommandTimeout = 0
                        .ExecuteReader()
                    End With
                    CON.Close()
                End Using
                MsgBox("El Proceso se ha Realizado con �xito", MsgBoxStyle.Information)
                bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Mando Un Mensaje Varios", "", "Mensaje: " + Me.TextBox1.Text, LocClv_Ciudad)

                Me.Close()
            ElseIf Programacion = 27 Then
                Dim consulta As String

                Mensaje_Prog = Me.TextBox1.Text.Trim
                Using ConLidia As New SqlConnection(MiConexion)
                    ConLidia.Open()
                    Dim comando3 As New SqlClient.SqlCommand
                    '(@clv_Session bigint,@op int,@conectado bit,@baja bit,@Insta bit,@Desconect bit,@Susp bit,@Fuera bit,@DescTmp bit,
                    '@orden int,@Habilita int,@periodo1 bit,@periodo2 bit,@mensaje varchar(max),@ultimo_mes int,@ultimo_anio int,@seleccion varchar(max) output)  
                    With comando3
                        .Connection = ConLidia
                        .CommandText = "Mensajes_Programacion "
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = 0
                        ' Create a SqlParameter for each parameter in the stored procedure.
                        Dim prm As New SqlParameter("@Clv_session", SqlDbType.BigInt)
                        Dim prm1 As New SqlParameter("@Op", SqlDbType.Int)
                        Dim prm2 As New SqlParameter("@Conectado", SqlDbType.Bit)
                        Dim prm25 As New SqlParameter("@Baja", SqlDbType.Bit)
                        Dim prm3 As New SqlParameter("@Insta", SqlDbType.Bit)
                        Dim prm4 As New SqlParameter("@Desconect", SqlDbType.Bit)
                        Dim prm5 As New SqlParameter("@Susp", SqlDbType.Bit)
                        Dim prm6 As New SqlParameter("@Fuera", SqlDbType.Bit)
                        Dim prm7 As New SqlParameter("@DescTmp", SqlDbType.Bit)
                        Dim prm8 As New SqlParameter("@Orden", SqlDbType.Int)
                        Dim prm9 As New SqlParameter("@Habilita", SqlDbType.Int)
                        Dim prm10 As New SqlParameter("@Periodo1", SqlDbType.Bit)
                        Dim prm11 As New SqlParameter("@Periodo2", SqlDbType.Bit)
                        Dim prm13 As New SqlParameter("@Ultimo_Mes", SqlDbType.Int)
                        Dim prm14 As New SqlParameter("@Ultimo_anio", SqlDbType.Int)
                        Dim prm15 As New SqlParameter("@Seleccion", SqlDbType.VarChar, 180000)

                        prm.Direction = ParameterDirection.Input
                        prm1.Direction = ParameterDirection.Input
                        prm2.Direction = ParameterDirection.Input
                        prm25.Direction = ParameterDirection.Input
                        prm3.Direction = ParameterDirection.Input
                        prm4.Direction = ParameterDirection.Input
                        prm5.Direction = ParameterDirection.Input
                        prm6.Direction = ParameterDirection.Input
                        prm7.Direction = ParameterDirection.Input
                        prm8.Direction = ParameterDirection.Input
                        prm9.Direction = ParameterDirection.Input
                        prm10.Direction = ParameterDirection.Input
                        prm11.Direction = ParameterDirection.Input
                        prm13.Direction = ParameterDirection.Input
                        prm14.Direction = ParameterDirection.Input
                        prm15.Direction = ParameterDirection.Output


                        prm.Value = LocClv_session
                        prm1.Value = GloClv_tipser2
                        prm2.Value = LiLocBndC
                        prm25.Value = LiLocBndB
                        prm3.Value = LiLocBndI
                        prm4.Value = LiLocBndD
                        prm5.Value = LiLocBndS
                        prm6.Value = LiLocBndF
                        prm7.Value = 0
                        prm8.Value = 1
                        prm9.Value = LocValidaHab
                        prm10.Value = LiPeriodo1
                        prm11.Value = LiPeriodo2
                        prm13.Value = clave
                        prm14.Value = Ultimo_anio
                        prm15.Value = ""

                        .Parameters.Add(prm)
                        .Parameters.Add(prm1)
                        .Parameters.Add(prm2)
                        .Parameters.Add(prm25)
                        .Parameters.Add(prm3)
                        .Parameters.Add(prm4)
                        .Parameters.Add(prm5)
                        .Parameters.Add(prm6)
                        .Parameters.Add(prm7)
                        .Parameters.Add(prm8)
                        .Parameters.Add(prm9)
                        .Parameters.Add(prm10)
                        .Parameters.Add(prm11)
                        .Parameters.Add(prm13)
                        .Parameters.Add(prm14)
                        .Parameters.Add(prm15)

                        Dim i As Integer = comando3.ExecuteNonQuery()
                        consulta_Prog = prm15.Value
                    End With
                    ConLidia.Close()
                End Using
                FrmProgramacion_msjs.Show()
                End If

        End If

    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If Me.CheckBox1.CheckState = CheckState.Checked Then
            LiLocBndC = 1
            'Me.Panel2.Visible = True
        Else
            LiLocBndC = 0
            If Me.CheckBox2.CheckState = CheckState.Unchecked And Me.CheckBox3.CheckState = CheckState.Unchecked Then

            End If
        End If
    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If Me.CheckBox2.CheckState = CheckState.Checked Then
            LiLocBndB = 1
            'Me.Panel2.Visible = True
        Else
            LiLocBndB = 0
            If Me.CheckBox1.CheckState = CheckState.Unchecked And Me.CheckBox3.CheckState = CheckState.Unchecked Then
                ' Me.Panel2.Visible = False
            End If

        End If
    End Sub

    Private Sub CheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox3.CheckedChanged
        If Me.CheckBox3.CheckState = CheckState.Checked Then
            LiLocBndI = 1
            'Me.Panel2.Visible = True
        Else
            LiLocBndI = 0
            If Me.CheckBox1.CheckState = CheckState.Unchecked And Me.CheckBox2.CheckState = CheckState.Unchecked Then
                'Me.Panel2.Visible = False
            End If
        End If
    End Sub

    Private Sub CheckBox4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox4.CheckedChanged
        If Me.CheckBox4.CheckState = CheckState.Checked Then
            LiLocBndD = 1
        Else
            LiLocBndD = 0
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Private Sub CheckBox5_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox5.CheckedChanged
        If Me.CheckBox5.CheckState = CheckState.Checked Then
            LiLocBndS = 1
        Else
            LiLocBndS = 0
        End If
    End Sub

    Private Sub CheckBox6_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox6.CheckedChanged
        If Me.CheckBox6.CheckState = CheckState.Checked Then
            LiLocBndF = 1
        Else
            LiLocBndF = 0
        End If
    End Sub


    Private Sub FrmSelEstado_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetLidia.Muestra_Meses' Puede moverla o quitarla seg�n sea necesario.
        Dim cone1 As New SqlClient.SqlConnection(MiConexion)
        cone1.Open()
        Me.Muestra_MesesTableAdapter.Connection = cone1
        Me.Muestra_MesesTableAdapter.Fill(Me.DataSetLidia.Muestra_Meses)
        cone1.Close()
        colorea(Me, Me.Name)
        LiLocBndC = 0
        LiLocBndB = 0
        LiLocBndI = 0
        LiLocBndD = 0
        LiLocBndS = 0
        LiLocBndF = 0
        LiLocBndDT = 0
        Me.ComboBox1.Text = ""
        Me.ComboBox1.SelectedValue = 0
        Me.CheckBox7.Visible = True
        If Me.CheckBox8.Checked = False Then
            Me.ComboBox1.Enabled = False
        End If
        If Me.CheckBox9.Checked = False Then
            Me.TextBox3.ReadOnly = True
        End If
        If Me.CheckBox10.Checked = True Then
            LiPeriodo1 = 1
        Else
            LiPeriodo1 = 0
        End If
        If Me.CheckBox11.Checked = True Then
            LiPeriodo2 = 1
        Else
            LiPeriodo2 = 0
        End If
        If Programacion = 27 Then
            Me.Button2.Text = "&PROGRAMAR MENSAJE"
            cbxDesconexion.Enabled = False
            cbxMorosos.Enabled = False
            cbxPagoNormal.Enabled = False
            cbxPrimerPago.Enabled = False
        Else
            Me.Button2.Text = "&ENVIAR MENSAJE"
            cbxDesconexion.Enabled = True
            cbxMorosos.Enabled = True
            cbxPagoNormal.Enabled = True
            cbxPrimerPago.Enabled = True
        End If

    End Sub

    Private Sub CheckBox7_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox7.CheckedChanged
        If Me.CheckBox7.CheckState = CheckState.Checked Then
            LiLocBndDT = 1
        Else
            LiLocBndDT = 0
        End If
    End Sub

    Private Sub CheckBox8_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox8.CheckedChanged
        If Me.CheckBox8.Checked = False Then
            Me.ComboBox1.Enabled = False
        ElseIf Me.CheckBox8.Checked = True Then
            Me.ComboBox1.Enabled = True
        End If
    End Sub

    Private Sub CheckBox9_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox9.CheckedChanged
        If Me.CheckBox9.Checked = False Then
            Me.TextBox3.ReadOnly = True
        ElseIf Me.CheckBox9.Checked = True Then
            Me.TextBox3.ReadOnly = False
        End If
    End Sub
    Private Sub CheckBox10_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox10.CheckedChanged
        If Me.CheckBox10.Checked = True Then
            LiPeriodo1 = 1
        Else
            LiPeriodo1 = 0
        End If
    End Sub

    Private Sub CheckBox11_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox11.CheckedChanged
        If Me.CheckBox11.Checked = True Then
            LiPeriodo2 = 1
        Else
            LiPeriodo2 = 0
        End If
    End Sub

    Private Sub cbxMorosos_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxMorosos.CheckedChanged
        If cbxMorosos.Checked = True Then
            CheckBox8.Enabled = False
            CheckBox9.Enabled = False
        End If
        If cbxMorosos.Checked = False And cbxDesconexion.Checked = False And cbxPrimerPago.Checked = False And cbxPagoNormal.Checked = False Then
            CheckBox8.Enabled = True
            CheckBox9.Enabled = True
        End If
    End Sub

    Private Sub cbxDesconexion_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxDesconexion.CheckedChanged
        If cbxDesconexion.Checked = True Then
            CheckBox8.Enabled = False
            CheckBox9.Enabled = False
        End If
        If cbxMorosos.Checked = False And cbxDesconexion.Checked = False And cbxPrimerPago.Checked = False And cbxPagoNormal.Checked = False Then
            CheckBox8.Enabled = True
            CheckBox9.Enabled = True
        End If
    End Sub

    Private Sub cbxPrimerPago_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxPrimerPago.CheckedChanged
        If cbxPrimerPago.Checked = True Then
            CheckBox8.Enabled = False
            CheckBox9.Enabled = False
        End If
        If cbxMorosos.Checked = False And cbxDesconexion.Checked = False And cbxPrimerPago.Checked = False And cbxPagoNormal.Checked = False Then
            CheckBox8.Enabled = True
            CheckBox9.Enabled = True
        End If
    End Sub

    Private Sub cbxPagoNormal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxPagoNormal.CheckedChanged
        If cbxPagoNormal.Checked = True Then
            CheckBox8.Enabled = False
            CheckBox9.Enabled = False
        End If
        If cbxMorosos.Checked = False And cbxDesconexion.Checked = False And cbxPrimerPago.Checked = False And cbxPagoNormal.Checked = False Then
            CheckBox8.Enabled = True
            CheckBox9.Enabled = True
        End If
    End Sub

    Private Sub btnReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReporte.Click
        Dim Ruta As String = Nothing
        Dim DS As New DataTable()
        Dim clave As String
        LiLocBndI = 1
        If Me.CheckBox8.Checked = True Then
            If Me.ComboBox1.Text = "" Then
                MsgBox("Se Debe de Seleccionar un Mes ", MsgBoxStyle.Information)
                Exit Sub
            Else
                clave = Me.ComboBox1.SelectedValue
            End If
        Else
            clave = 0
        End If
        If Me.TextBox3.Text.Trim.Length > 0 Then
            If CInt(Me.TextBox3.Text) >= 2007 Then
                Ultimo_anio = Me.TextBox3.Text.Trim
            End If
        End If
        'If Me.TextBox1.Text.Trim.Length = 0 Then
        '    MsgBox("Debes de Capturar un Mensaje", MsgBoxStyle.Information)
        '    Exit Sub
        'End If
        If Me.CheckBox1.Checked = False And Me.CheckBox2.Checked = False And Me.CheckBox3.Checked = False And Me.CheckBox4.Checked = False And Me.CheckBox5.Checked = False And Me.CheckBox6.Checked = False And Me.CheckBox7.Checked = False Then
            MsgBox("Primero selecciona minimo un tipo de Status", MsgBoxStyle.Information)
        End If

        Dim Imprimir As New FrmImprimirCentralizada
        Dim reportdocument As New ReportDocument
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_Session", SqlDbType.BigInt, LocClv_session)
        BaseII.CreateMyParameter("@op", SqlDbType.Int, GloClv_tipser2)
        BaseII.CreateMyParameter("@conectado", SqlDbType.Bit, LiLocBndC)
        BaseII.CreateMyParameter("@baja", SqlDbType.Bit, LiLocBndB)
        BaseII.CreateMyParameter("@Insta", SqlDbType.Bit, LiLocBndI)
        BaseII.CreateMyParameter("@Desconect", SqlDbType.Bit, LiLocBndD)
        BaseII.CreateMyParameter("@Susp", SqlDbType.Bit, LiLocBndS)
        BaseII.CreateMyParameter("@Fuera", SqlDbType.Bit, LiLocBndF)
        BaseII.CreateMyParameter("@DescTmp", SqlDbType.Bit, 0)
        BaseII.CreateMyParameter("@orden", SqlDbType.Int, 1)
        BaseII.CreateMyParameter("@Habilita", SqlDbType.Int, LocValidaHab)
        BaseII.CreateMyParameter("@periodo1", SqlDbType.Bit, LiPeriodo1)
        BaseII.CreateMyParameter("@periodo2", SqlDbType.Bit, LiPeriodo2)
        BaseII.CreateMyParameter("@mensaje", SqlDbType.VarChar, CStr(Me.TextBox1.Text.Trim))
        BaseII.CreateMyParameter("@ultimo_mes", SqlDbType.Int, CInt(Me.ComboBox1.SelectedValue))
        BaseII.CreateMyParameter("@ultimo_anio", SqlDbType.Int, Ultimo_anio)
        BaseII.CreateMyParameter("@Morosos", SqlDbType.Bit, CInt(cbxMorosos.CheckState))
        BaseII.CreateMyParameter("@Desconexion", SqlDbType.Bit, CInt(cbxDesconexion.CheckState))
        BaseII.CreateMyParameter("@PrimerPago", SqlDbType.Bit, CInt(cbxPrimerPago.CheckState))
        BaseII.CreateMyParameter("@PagoNormal", SqlDbType.Bit, CInt(cbxPagoNormal.CheckState))
        Ruta = RutaReportes + "\ReporteMensajes.rpt"
        reportdocument.Load(Ruta)
        DS = BaseII.ConsultaDT("uspReporteMensajes")
        DS.TableName = "uspReporteMensajes"
        reportdocument.SetDataSource(DS)
        reportdocument.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
        reportdocument.DataDefinition.FormulaFields("Titulo").Text = "'" & "Reporte del envio de mensajes" & "'"
        Imprimir.rd = reportdocument
        Imprimir.ShowDialog()
    End Sub
End Class