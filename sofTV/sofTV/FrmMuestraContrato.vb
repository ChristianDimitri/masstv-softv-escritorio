Imports System.Data.SqlClient
Imports System.Text
Public Class FrmMuestraContrato
    Public Guardar As Boolean = False

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        If (eBndAutorizacionNombreRepetido = True) Then
            Locbndguardar = True
            eBndEntraDire = True
            FrmAutorizacion.NombreAutorizado = True
            Me.Close()
        Else
            FrmAutorizacion.OpcionValidar = "NOMBRE"
            FrmAutorizacion.ShowDialog()
            Me.Close()
        End If

        
    End Sub

    Private Sub FrmMuestraContrato_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        'MuestraNombreContrato(eNombre)
        MuestraNombreContratoSeparado(eNombre, eApellidoPaterno, eApellidoMaterno)
    End Sub

    Private Sub MuestraNombreContrato(ByVal Nombre As String)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.MuestraNombreContratoTableAdapter.Connection = CON
            Me.MuestraNombreContratoTableAdapter.Fill(Me.DataSetEric2.MuestraNombreContrato, Nombre)
            CON.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub MuestraNombreContratoSeparado(ByVal Nombre As String, ByVal ApellidoPaterno As String, ByVal ApellidoMaterno As String)
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder

        StrSQL.Append("EXEC MuestraNombreContratoSeparado ")
        StrSQL.Append("'" & Nombre & "', ")
        StrSQL.Append("'" & ApellidoPaterno & "', ")
        StrSQL.Append("'" & ApellidoMaterno & "'")

        Dim DA As New SqlDataAdapter(StrSQL.ToString, CON)
        Dim DT As New DataTable
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.MuestraNombreContratoDataGridView.DataSource = BS.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub
    
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Locbndguardar = False
        eBndEntraDire = False
        Me.Close()
    End Sub

    Private Sub MuestraNombreContratoDataGridView_CellContentDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles MuestraNombreContratoDataGridView.CellContentDoubleClick

        'Mandamos Mostrar los Datos Generales del Cliente
        If (Me.MuestraNombreContratoDataGridView.RowCount > 0) Then
            Try
                'FrmDatosGeneralesClientes.Contrato = Me.dgvIFE.CurrentRow.Cells(0).Value
                'FrmDatosGeneralesClientes.ShowDialog()
                'FrmDatosGeneralesClientes.Contrato = 0
                'Me.DataGridView1.SelectedCells.Item(0).Value()
                If IsNumeric(Me.MuestraNombreContratoDataGridView.CurrentRow.Cells(0).Value) = True Then
                    Module1.Contrato = Me.MuestraNombreContratoDataGridView.CurrentRow.Cells(0).Value
                    OpcionCli = "C"
                    Dim PantallaClientes As New FrmClientes()
                    PantallaClientes.ShowDialog()
                Else
                    MsgBox("Seleccione un Cliente para poder Consultar ", MsgBoxStyle.Information)
                End If

            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If

    End Sub

    Private Sub MuestraNombreContratoDataGridView_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles MuestraNombreContratoDataGridView.CellDoubleClick
        'Mandamos Mostrar los Datos Generales del Cliente
        If (Me.MuestraNombreContratoDataGridView.RowCount > 0) Then
            Try
                'FrmDatosGeneralesClientes.Contrato = Me.dgvIFE.CurrentRow.Cells(0).Value
                'FrmDatosGeneralesClientes.ShowDialog()
                'FrmDatosGeneralesClientes.Contrato = 0
                'Me.DataGridView1.SelectedCells.Item(0).Value()
                If IsNumeric(Me.MuestraNombreContratoDataGridView.CurrentRow.Cells(0).Value) = True Then
                    Module1.Contrato = Me.MuestraNombreContratoDataGridView.CurrentRow.Cells(0).Value
                    OpcionCli = "C"
                    Dim PantallaClientes As New FrmClientes()
                    colorea(PantallaClientes, PantallaClientes.Name)
                    PantallaClientes.ShowDialog()
                Else
                    MsgBox("Seleccione un Cliente para poder Consultar ", MsgBoxStyle.Information)
                End If

            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub
End Class