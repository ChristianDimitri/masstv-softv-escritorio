Imports System.Data.SqlClient
Imports System.Text

Public Class BrwCamServCte

    Dim eRes As Integer = 0
    Dim eMsg As String = Nothing

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eOpcion = "N"
        FrmCamServCte.Show()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        If IsNumeric(Me.TextBox1.Text) = True Then
            If Len(Me.TextBox1.Text) > 0 Then
                BUSCACAMSERVPORSEPARADO(0, Me.TextBox1.Text, "", "", "", 0, "", "", 2)
            Else
                MsgBox("Ingrese un Contrato para una b�squeda m�s precisa", MsgBoxStyle.Information)
                BUSCACAMSERVPORSEPARADO(0, 0, "", "", "", 0, "", "", 0)
            End If
            TextBox1.Text = ""
        Else
            MsgBox("Ingrese un Contrato v�lido", MsgBoxStyle.Information)
            Exit Sub
        End If
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If InStr(1, "0123456789,-" & Chr(8), e.KeyChar) = 0 Then 'VALIDACI�N PARA QUE S�LO ADMITA VALORES NUM�RICOS EN EL TEXTBOX (txtCP)
            e.KeyChar = ""
        End If
        If Asc(e.KeyChar) = 13 Then
            If IsNumeric(Me.TextBox1.Text) = True Then
                If Len(Me.TextBox1.Text) > 0 Then
                    BUSCACAMSERVPORSEPARADO(0, Me.TextBox1.Text, "", "", "", 0, "", "", 2)
                Else
                    MsgBox("Ingrese un Contrato para una b�squeda m�s precisa", MsgBoxStyle.Information)
                    BUSCACAMSERVPORSEPARADO(0, 0, "", "", "", 0, "", "", 0)
                End If
                TextBox1.Text = ""
            Else
                MsgBox("Ingrese un Contrato v�lido", MsgBoxStyle.Information)
                Exit Sub
            End If
        End If
    End Sub


    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If Len(Me.TextBox2.Text) = 0 And Len(Me.APaternoTextBox.Text) = 0 And Len(Me.AMaternoTextBox.Text) = 0 Then
            MsgBox("Ingrese datos v�lidos para una b�squeda m�s precisa", MsgBoxStyle.Information)
            BUSCACAMSERVPORSEPARADO(0, 0, "", "", "", 0, "", "", 0)
        Else
            BUSCACAMSERVPORSEPARADO(0, 0, Me.TextBox2.Text, APaternoTextBox.Text, AMaternoTextBox.Text, 0, "", "", 3)
            TextBox2.Text = ""
            APaternoTextBox.Text = ""
            AMaternoTextBox.Text = ""
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If Len(Me.TextBox2.Text) = 0 And Len(Me.APaternoTextBox.Text) = 0 And Len(Me.AMaternoTextBox.Text) = 0 Then
                MsgBox("Ingrese datos v�lidos para una b�squeda m�s precisa", MsgBoxStyle.Information)
                BUSCACAMSERVPORSEPARADO(0, 0, "", "", "", 0, "", "", 0)
            Else
                BUSCACAMSERVPORSEPARADO(0, 0, Me.TextBox2.Text, APaternoTextBox.Text, AMaternoTextBox.Text, 0, "", "", 3)
                TextBox2.Text = ""
                APaternoTextBox.Text = ""
                AMaternoTextBox.Text = ""
            End If
        End If
    End Sub


    Private Sub BrwCamServCte_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        ''Me.ConCambioServClienteTableAdapter.Connection = CON
        ''Me.ConCambioServClienteTableAdapter.Fill(Me.DataSetEric.ConCambioServCliente, 0, 0, "", 0, 0)
        'CON.Close()
    End Sub

    Private Sub BrwCamServCte_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CON As New SqlConnection(MiConexion)
        colorea(Me, Me.Name)
        CON.Open()
        'Me.ConCambioServClienteTableAdapter.Connection = CON
        'Me.ConCambioServClienteTableAdapter.Fill(Me.DataSetEric.ConCambioServCliente, 0, 0, "", 0, 0)
        BUSCACAMSERVPORSEPARADO(0, 0, "", "", "", 0, "", "", 0)
        CON.Close()
    End Sub

    Private Sub RealizarTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RealizarTextBox.TextChanged

        If Me.RealizarTextBox.Text = 3 Then
            Me.Label5.Text = "Servicio Actual :"
            Me.Label6.Text = "Servicio Anterior :"
        End If
        If Me.RealizarTextBox.Text = 1 Then
            Me.Label5.Text = "Servicio Solicitado :"
            Me.Label6.Text = "Servicio Actual :"
        End If

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Try
            If Me.ConCambioServClienteDataGridView.RowCount > 0 Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.BorCambioServClienteTableAdapter.Connection = CON
                Me.BorCambioServClienteTableAdapter.Fill(Me.DataSetEric.BorCambioServCliente, Me.ClaveTextBox.Text, 0, eRes, eMsg)
                CON.Close()
                If eRes = 1 Then
                    MsgBox(eMsg)
                Else
                    MsgBox(mensaje6)
                    Refrescar()
                End If


            Else
                MsgBox("Selecciona un Cambio de Servicio.", , "Atenci�n")
            End If


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If Me.ConCambioServClienteDataGridView.RowCount > 0 Then
            If Me.RealizarTextBox.Text = 1 Then
                eContrato = Me.ContratoLabel2.Text
                eTipSer = Me.Clv_TipSerTextBox.Text
                eOpcion = "M"
                FrmCamServCte.Show()
            Else
                MsgBox("S�lo se pueden Modificar Procesos Activos.")
            End If
        Else
            MsgBox("Selecciona un Cliente.", , "Atenci�n")
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
    End Sub

    Private Sub Refrescar()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'Me.ConCambioServClienteTableAdapter.Connection = CON
        'Me.ConCambioServClienteTableAdapter.Fill(Me.DataSetEric.ConCambioServCliente, 0, 0, "", 0, 0)
        BUSCACAMSERVPORSEPARADO(0, 0, "", "", "", 0, "", "", 0)
        CON.Close()
    End Sub
    Private Sub BUSCACAMSERVPORSEPARADO(ByVal ClaveCli As Integer, ByVal ContratoCli As Integer, ByVal NombreCli As String, ByVal APaternoCli As String, ByVal AMaternoCli As String, _
                                        ByVal Clv_TipSer As Integer, ByVal SetUpBox As String, ByVal Tarjeta As String, ByVal OpCli As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder

        StrSQL.Append("EXEC ConCambioServClientePORSEPARADO ")
        StrSQL.Append(CStr(ClaveCli) & ",")
        StrSQL.Append(CStr(ContratoCli) & ",")
        StrSQL.Append("'" & NombreCli & "',")
        StrSQL.Append("'" & APaternoCli & "',")
        StrSQL.Append("'" & AMaternoCli & "',")
        StrSQL.Append(CStr(Clv_TipSer) & ",")
        StrSQL.Append("'" & SetUpBox & "', ")
        StrSQL.Append("'" & Tarjeta & "', ")
        StrSQL.Append(CStr(OpCli))


        Dim DA As New SqlDataAdapter(StrSQL.ToString, CON)
        Dim DT As New DataTable
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.ConCambioServClienteDataGridView.DataSource = BS.DataSource
            Llena_detalle()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub Llena_detalle()
        Try
            Me.ClaveTextBox.Text = Me.ConCambioServClienteDataGridView.SelectedCells(0).Value
            ContratoLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(1).Value
            NombreLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(2).Value
            'ConceptoLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(5).Value
            Label1.Text = Me.ConCambioServClienteDataGridView.SelectedCells(3).Value
            Servicio_ActualLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(4).Value
            Fecha_SolLabel1.Text = Me.ConCambioServClienteDataGridView.SelectedCells(5).Value

            StatusLabel2.Text = Me.ConCambioServClienteDataGridView.SelectedCells(6).Value

        Catch ex As Exception

        End Try
    End Sub

    Private Sub ConCambioServClienteDataGridView_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles ConCambioServClienteDataGridView.CellClick
       
        Llena_detalle()
    End Sub

    Private Sub ConCambioServClienteDataGridView_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles ConCambioServClienteDataGridView.CellContentClick

    End Sub


    Private Sub StatusLabel2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StatusLabel2.Click

    End Sub
    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub

    Private Sub Button3_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Len(Me.TxtSetUpBox.Text) = 0 And Len(Me.TxtTarjeta.Text) = 0 Then
            MsgBox("Ingrese datos v�lidos para una b�squeda m�s precisa", MsgBoxStyle.Information)
            BUSCACAMSERVPORSEPARADO(0, 0, "", "", "", 0, "", "", 0)
        Else
            BUSCACAMSERVPORSEPARADO(0, 0, "", "", "", 0, Me.TxtSetUpBox.Text, Me.TxtTarjeta.Text, 4)
            TxtSetUpBox.Text = ""
            TxtTarjeta.Text = ""
        End If
    End Sub

    Private Sub ConCambioServClienteDataGridView_CurrentCellChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ConCambioServClienteDataGridView.CurrentCellChanged
        Llena_detalle()
    End Sub

    Private Sub APaternoTextBox_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles APaternoTextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If Len(Me.TextBox2.Text) = 0 And Len(Me.APaternoTextBox.Text) = 0 And Len(Me.AMaternoTextBox.Text) = 0 Then
                MsgBox("Ingrese datos v�lidos para una b�squeda m�s precisa", MsgBoxStyle.Information)
                BUSCACAMSERVPORSEPARADO(0, 0, "", "", "", 0, "", "", 0)
            Else
                BUSCACAMSERVPORSEPARADO(0, 0, Me.TextBox2.Text, APaternoTextBox.Text, AMaternoTextBox.Text, 0, "", "", 3)
                TextBox2.Text = ""
                APaternoTextBox.Text = ""
                AMaternoTextBox.Text = ""
            End If
        End If
    End Sub

    Private Sub AMaternoTextBox_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles AMaternoTextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If Len(Me.TextBox2.Text) = 0 And Len(Me.APaternoTextBox.Text) = 0 And Len(Me.AMaternoTextBox.Text) = 0 Then
                MsgBox("Ingrese datos v�lidos para una b�squeda m�s precisa", MsgBoxStyle.Information)
                BUSCACAMSERVPORSEPARADO(0, 0, "", "", "", 0, "", "", 0)
            Else
                BUSCACAMSERVPORSEPARADO(0, 0, Me.TextBox2.Text, APaternoTextBox.Text, AMaternoTextBox.Text, 0, "", "", 3)
                TextBox2.Text = ""
                APaternoTextBox.Text = ""
                AMaternoTextBox.Text = ""
            End If
        End If
    End Sub

    Private Sub TxtSetUpBox_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TxtSetUpBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If Len(Me.TxtSetUpBox.Text) = 0 And Len(Me.TxtTarjeta.Text) = 0 Then
                MsgBox("Ingrese datos v�lidos para una b�squeda m�s precisa", MsgBoxStyle.Information)
                BUSCACAMSERVPORSEPARADO(0, 0, "", "", "", 0, "", "", 0)
            Else
                BUSCACAMSERVPORSEPARADO(0, 0, "", "", "", 0, Me.TxtSetUpBox.Text, Me.TxtTarjeta.Text, 4)
                TxtSetUpBox.Text = ""
                TxtTarjeta.Text = ""
            End If
        End If
    End Sub

    Private Sub TxtTarjeta_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TxtTarjeta.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If Len(Me.TxtSetUpBox.Text) = 0 And Len(Me.TxtTarjeta.Text) = 0 Then
                MsgBox("Ingrese datos v�lidos para una b�squeda m�s precisa", MsgBoxStyle.Information)
                BUSCACAMSERVPORSEPARADO(0, 0, "", "", "", 0, "", "", 0)
            Else
                BUSCACAMSERVPORSEPARADO(0, 0, "", "", "", 0, Me.TxtSetUpBox.Text, Me.TxtTarjeta.Text, 4)
                TxtSetUpBox.Text = ""
                TxtTarjeta.Text = ""
            End If
        End If
    End Sub
End Class