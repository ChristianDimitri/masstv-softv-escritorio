<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSubContrato
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim NOMBRELabel As System.Windows.Forms.Label
        Dim CALLELabel As System.Windows.Forms.Label
        Dim COLONIALabel As System.Windows.Forms.Label
        Dim NUMEROLabel As System.Windows.Forms.Label
        Dim CIUDADLabel As System.Windows.Forms.Label
        Dim SOLOINTERNETLabel As System.Windows.Forms.Label
        Dim ESHOTELLabel As System.Windows.Forms.Label
        Dim CONTRATOLabel2 As System.Windows.Forms.Label
        Dim NOMBRELabel2 As System.Windows.Forms.Label
        Dim COLONIALabel2 As System.Windows.Forms.Label
        Dim NUMEROLabel2 As System.Windows.Forms.Label
        Dim CIUDADLabel2 As System.Windows.Forms.Label
        Dim SOLOINTERNETLabel1 As System.Windows.Forms.Label
        Dim ESHOTELLabel1 As System.Windows.Forms.Label
        Dim CONTRATOLabel As System.Windows.Forms.Label
        Dim CALLELabel2 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmSubContrato))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.CONTRATOTextBox = New System.Windows.Forms.TextBox()
        Me.ConContratoMaestroBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ToolStripButton3 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.ESHOTELCheckBox = New System.Windows.Forms.CheckBox()
        Me.SOLOINTERNETCheckBox = New System.Windows.Forms.CheckBox()
        Me.CIUDADLabel1 = New System.Windows.Forms.Label()
        Me.NUMEROLabel1 = New System.Windows.Forms.Label()
        Me.COLONIALabel1 = New System.Windows.Forms.Label()
        Me.CALLELabel1 = New System.Windows.Forms.Label()
        Me.NOMBRELabel1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.CALLELabel3 = New System.Windows.Forms.Label()
        Me.ConSubContratoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONTRATOLabel3 = New System.Windows.Forms.Label()
        Me.ESHOTELCheckBox1 = New System.Windows.Forms.CheckBox()
        Me.NOMBRELabel3 = New System.Windows.Forms.Label()
        Me.SOLOINTERNETCheckBox1 = New System.Windows.Forms.CheckBox()
        Me.COLONIALabel3 = New System.Windows.Forms.Label()
        Me.CIUDADLabel3 = New System.Windows.Forms.Label()
        Me.NUMEROLabel3 = New System.Windows.Forms.Label()
        Me.BindingNavigator2 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton4 = New System.Windows.Forms.ToolStripButton()
        Me.ConSubContratoDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewCheckBoxColumn1 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewCheckBoxColumn2 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.ConContratoMaestroTableAdapter = New sofTV.DataSetEricTableAdapters.ConContratoMaestroTableAdapter()
        Me.ConSubContratoTableAdapter = New sofTV.DataSetEricTableAdapters.ConSubContratoTableAdapter()
        Me.BorContratoMaestroBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorContratoMaestroTableAdapter = New sofTV.DataSetEricTableAdapters.BorContratoMaestroTableAdapter()
        Me.NueSubContratoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NueSubContratoTableAdapter = New sofTV.DataSetEricTableAdapters.NueSubContratoTableAdapter()
        Me.NueContratoMaestroBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NueContratoMaestroTableAdapter = New sofTV.DataSetEricTableAdapters.NueContratoMaestroTableAdapter()
        Me.ValidaBorMaestroBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ValidaBorMaestroTableAdapter = New sofTV.DataSetEricTableAdapters.ValidaBorMaestroTableAdapter()
        NOMBRELabel = New System.Windows.Forms.Label()
        CALLELabel = New System.Windows.Forms.Label()
        COLONIALabel = New System.Windows.Forms.Label()
        NUMEROLabel = New System.Windows.Forms.Label()
        CIUDADLabel = New System.Windows.Forms.Label()
        SOLOINTERNETLabel = New System.Windows.Forms.Label()
        ESHOTELLabel = New System.Windows.Forms.Label()
        CONTRATOLabel2 = New System.Windows.Forms.Label()
        NOMBRELabel2 = New System.Windows.Forms.Label()
        COLONIALabel2 = New System.Windows.Forms.Label()
        NUMEROLabel2 = New System.Windows.Forms.Label()
        CIUDADLabel2 = New System.Windows.Forms.Label()
        SOLOINTERNETLabel1 = New System.Windows.Forms.Label()
        ESHOTELLabel1 = New System.Windows.Forms.Label()
        CONTRATOLabel = New System.Windows.Forms.Label()
        CALLELabel2 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        CType(Me.ConContratoMaestroBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.ConSubContratoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingNavigator2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator2.SuspendLayout()
        CType(Me.ConSubContratoDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorContratoMaestroBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NueSubContratoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NueContratoMaestroBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ValidaBorMaestroBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'NOMBRELabel
        '
        NOMBRELabel.AutoSize = True
        NOMBRELabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NOMBRELabel.Location = New System.Drawing.Point(12, 95)
        NOMBRELabel.Name = "NOMBRELabel"
        NOMBRELabel.Size = New System.Drawing.Size(66, 15)
        NOMBRELabel.TabIndex = 2
        NOMBRELabel.Text = "Nombre :"
        '
        'CALLELabel
        '
        CALLELabel.AutoSize = True
        CALLELabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CALLELabel.Location = New System.Drawing.Point(30, 130)
        CALLELabel.Name = "CALLELabel"
        CALLELabel.Size = New System.Drawing.Size(48, 15)
        CALLELabel.TabIndex = 4
        CALLELabel.Text = "Calle :"
        '
        'COLONIALabel
        '
        COLONIALabel.AutoSize = True
        COLONIALabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        COLONIALabel.Location = New System.Drawing.Point(14, 164)
        COLONIALabel.Name = "COLONIALabel"
        COLONIALabel.Size = New System.Drawing.Size(64, 15)
        COLONIALabel.TabIndex = 6
        COLONIALabel.Text = "Colonia :"
        '
        'NUMEROLabel
        '
        NUMEROLabel.AutoSize = True
        NUMEROLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NUMEROLabel.Location = New System.Drawing.Point(374, 130)
        NUMEROLabel.Name = "NUMEROLabel"
        NUMEROLabel.Size = New System.Drawing.Size(23, 15)
        NUMEROLabel.TabIndex = 8
        NUMEROLabel.Text = "# :"
        '
        'CIUDADLabel
        '
        CIUDADLabel.AutoSize = True
        CIUDADLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CIUDADLabel.Location = New System.Drawing.Point(18, 198)
        CIUDADLabel.Name = "CIUDADLabel"
        CIUDADLabel.Size = New System.Drawing.Size(60, 15)
        CIUDADLabel.TabIndex = 10
        CIUDADLabel.Text = "Ciudad :"
        '
        'SOLOINTERNETLabel
        '
        SOLOINTERNETLabel.AutoSize = True
        SOLOINTERNETLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SOLOINTERNETLabel.Location = New System.Drawing.Point(589, 95)
        SOLOINTERNETLabel.Name = "SOLOINTERNETLabel"
        SOLOINTERNETLabel.Size = New System.Drawing.Size(97, 15)
        SOLOINTERNETLabel.TabIndex = 12
        SOLOINTERNETLabel.Text = "Solo Internet :"
        '
        'ESHOTELLabel
        '
        ESHOTELLabel.AutoSize = True
        ESHOTELLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ESHOTELLabel.Location = New System.Drawing.Point(617, 125)
        ESHOTELLabel.Name = "ESHOTELLabel"
        ESHOTELLabel.Size = New System.Drawing.Size(69, 15)
        ESHOTELLabel.TabIndex = 14
        ESHOTELLabel.Text = "Es Hotel :"
        ESHOTELLabel.Visible = False
        '
        'CONTRATOLabel2
        '
        CONTRATOLabel2.AutoSize = True
        CONTRATOLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CONTRATOLabel2.Location = New System.Drawing.Point(6, 26)
        CONTRATOLabel2.Name = "CONTRATOLabel2"
        CONTRATOLabel2.Size = New System.Drawing.Size(69, 15)
        CONTRATOLabel2.TabIndex = 1
        CONTRATOLabel2.Text = "Contrato :"
        '
        'NOMBRELabel2
        '
        NOMBRELabel2.AutoSize = True
        NOMBRELabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NOMBRELabel2.Location = New System.Drawing.Point(13, 58)
        NOMBRELabel2.Name = "NOMBRELabel2"
        NOMBRELabel2.Size = New System.Drawing.Size(66, 15)
        NOMBRELabel2.TabIndex = 3
        NOMBRELabel2.Text = "Nombre :"
        '
        'COLONIALabel2
        '
        COLONIALabel2.AutoSize = True
        COLONIALabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        COLONIALabel2.Location = New System.Drawing.Point(16, 124)
        COLONIALabel2.Name = "COLONIALabel2"
        COLONIALabel2.Size = New System.Drawing.Size(64, 15)
        COLONIALabel2.TabIndex = 7
        COLONIALabel2.Text = "Colonia :"
        '
        'NUMEROLabel2
        '
        NUMEROLabel2.AutoSize = True
        NUMEROLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NUMEROLabel2.Location = New System.Drawing.Point(342, 92)
        NUMEROLabel2.Name = "NUMEROLabel2"
        NUMEROLabel2.Size = New System.Drawing.Size(23, 15)
        NUMEROLabel2.TabIndex = 9
        NUMEROLabel2.Text = "# :"
        '
        'CIUDADLabel2
        '
        CIUDADLabel2.AutoSize = True
        CIUDADLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CIUDADLabel2.Location = New System.Drawing.Point(19, 156)
        CIUDADLabel2.Name = "CIUDADLabel2"
        CIUDADLabel2.Size = New System.Drawing.Size(60, 15)
        CIUDADLabel2.TabIndex = 11
        CIUDADLabel2.Text = "Ciudad :"
        '
        'SOLOINTERNETLabel1
        '
        SOLOINTERNETLabel1.AutoSize = True
        SOLOINTERNETLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SOLOINTERNETLabel1.Location = New System.Drawing.Point(145, 199)
        SOLOINTERNETLabel1.Name = "SOLOINTERNETLabel1"
        SOLOINTERNETLabel1.Size = New System.Drawing.Size(97, 15)
        SOLOINTERNETLabel1.TabIndex = 13
        SOLOINTERNETLabel1.Text = "Solo Internet :"
        '
        'ESHOTELLabel1
        '
        ESHOTELLabel1.AutoSize = True
        ESHOTELLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ESHOTELLabel1.Location = New System.Drawing.Point(279, 196)
        ESHOTELLabel1.Name = "ESHOTELLabel1"
        ESHOTELLabel1.Size = New System.Drawing.Size(69, 15)
        ESHOTELLabel1.TabIndex = 15
        ESHOTELLabel1.Text = "Es Hotel :"
        ESHOTELLabel1.Visible = False
        '
        'CONTRATOLabel
        '
        CONTRATOLabel.AutoSize = True
        CONTRATOLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CONTRATOLabel.Location = New System.Drawing.Point(9, 64)
        CONTRATOLabel.Name = "CONTRATOLabel"
        CONTRATOLabel.Size = New System.Drawing.Size(69, 15)
        CONTRATOLabel.TabIndex = 19
        CONTRATOLabel.Text = "Contrato :"
        '
        'CALLELabel2
        '
        CALLELabel2.AutoSize = True
        CALLELabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CALLELabel2.Location = New System.Drawing.Point(31, 92)
        CALLELabel2.Name = "CALLELabel2"
        CALLELabel2.Size = New System.Drawing.Size(48, 15)
        CALLELabel2.TabIndex = 5
        CALLELabel2.Text = "Calle :"
        '
        'Button4
        '
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(858, 686)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(136, 36)
        Me.Button4.TabIndex = 2
        Me.Button4.Text = "&SALIR"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(CONTRATOLabel)
        Me.GroupBox1.Controls.Add(Me.CONTRATOTextBox)
        Me.GroupBox1.Controls.Add(Me.BindingNavigator1)
        Me.GroupBox1.Controls.Add(ESHOTELLabel)
        Me.GroupBox1.Controls.Add(Me.ESHOTELCheckBox)
        Me.GroupBox1.Controls.Add(SOLOINTERNETLabel)
        Me.GroupBox1.Controls.Add(Me.SOLOINTERNETCheckBox)
        Me.GroupBox1.Controls.Add(CIUDADLabel)
        Me.GroupBox1.Controls.Add(Me.CIUDADLabel1)
        Me.GroupBox1.Controls.Add(NUMEROLabel)
        Me.GroupBox1.Controls.Add(Me.NUMEROLabel1)
        Me.GroupBox1.Controls.Add(COLONIALabel)
        Me.GroupBox1.Controls.Add(Me.COLONIALabel1)
        Me.GroupBox1.Controls.Add(CALLELabel)
        Me.GroupBox1.Controls.Add(Me.CALLELabel1)
        Me.GroupBox1.Controls.Add(NOMBRELabel)
        Me.GroupBox1.Controls.Add(Me.NOMBRELabel1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.Black
        Me.GroupBox1.Location = New System.Drawing.Point(151, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(730, 227)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Contrato Maestro"
        '
        'CONTRATOTextBox
        '
        Me.CONTRATOTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CONTRATOTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CONTRATOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConContratoMaestroBindingSource, "CONTRATO", True))
        Me.CONTRATOTextBox.Location = New System.Drawing.Point(84, 64)
        Me.CONTRATOTextBox.Name = "CONTRATOTextBox"
        Me.CONTRATOTextBox.ReadOnly = True
        Me.CONTRATOTextBox.Size = New System.Drawing.Size(100, 15)
        Me.CONTRATOTextBox.TabIndex = 20
        Me.CONTRATOTextBox.TabStop = False
        '
        'ConContratoMaestroBindingSource
        '
        Me.ConContratoMaestroBindingSource.DataMember = "ConContratoMaestro"
        Me.ConContratoMaestroBindingSource.DataSource = Me.DataSetEric
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Nothing
        Me.BindingNavigator1.CountItem = Nothing
        Me.BindingNavigator1.DeleteItem = Nothing
        Me.BindingNavigator1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton3, Me.ToolStripButton2})
        Me.BindingNavigator1.Location = New System.Drawing.Point(3, 18)
        Me.BindingNavigator1.MoveFirstItem = Nothing
        Me.BindingNavigator1.MoveLastItem = Nothing
        Me.BindingNavigator1.MoveNextItem = Nothing
        Me.BindingNavigator1.MovePreviousItem = Nothing
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Nothing
        Me.BindingNavigator1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.BindingNavigator1.Size = New System.Drawing.Size(724, 25)
        Me.BindingNavigator1.TabIndex = 0
        Me.BindingNavigator1.TabStop = True
        Me.BindingNavigator1.Text = "BindingNavigator1"
        '
        'ToolStripButton3
        '
        Me.ToolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton3.Image = CType(resources.GetObject("ToolStripButton3.Image"), System.Drawing.Image)
        Me.ToolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton3.Name = "ToolStripButton3"
        Me.ToolStripButton3.Size = New System.Drawing.Size(74, 22)
        Me.ToolStripButton3.Text = "ELIMINAR"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.Image = CType(resources.GetObject("ToolStripButton2.Image"), System.Drawing.Image)
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(89, 22)
        Me.ToolStripButton2.Text = "&AGREGAR"
        '
        'ESHOTELCheckBox
        '
        Me.ESHOTELCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.ConContratoMaestroBindingSource, "ESHOTEL", True))
        Me.ESHOTELCheckBox.Enabled = False
        Me.ESHOTELCheckBox.Location = New System.Drawing.Point(692, 122)
        Me.ESHOTELCheckBox.Name = "ESHOTELCheckBox"
        Me.ESHOTELCheckBox.Size = New System.Drawing.Size(20, 24)
        Me.ESHOTELCheckBox.TabIndex = 15
        Me.ESHOTELCheckBox.TabStop = False
        Me.ESHOTELCheckBox.Visible = False
        '
        'SOLOINTERNETCheckBox
        '
        Me.SOLOINTERNETCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.ConContratoMaestroBindingSource, "SOLOINTERNET", True))
        Me.SOLOINTERNETCheckBox.Enabled = False
        Me.SOLOINTERNETCheckBox.Location = New System.Drawing.Point(692, 92)
        Me.SOLOINTERNETCheckBox.Name = "SOLOINTERNETCheckBox"
        Me.SOLOINTERNETCheckBox.Size = New System.Drawing.Size(20, 24)
        Me.SOLOINTERNETCheckBox.TabIndex = 13
        Me.SOLOINTERNETCheckBox.TabStop = False
        '
        'CIUDADLabel1
        '
        Me.CIUDADLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConContratoMaestroBindingSource, "CIUDAD", True))
        Me.CIUDADLabel1.Location = New System.Drawing.Point(84, 190)
        Me.CIUDADLabel1.Name = "CIUDADLabel1"
        Me.CIUDADLabel1.Size = New System.Drawing.Size(419, 23)
        Me.CIUDADLabel1.TabIndex = 11
        Me.CIUDADLabel1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'NUMEROLabel1
        '
        Me.NUMEROLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConContratoMaestroBindingSource, "NUMERO", True))
        Me.NUMEROLabel1.Location = New System.Drawing.Point(403, 122)
        Me.NUMEROLabel1.Name = "NUMEROLabel1"
        Me.NUMEROLabel1.Size = New System.Drawing.Size(100, 23)
        Me.NUMEROLabel1.TabIndex = 9
        Me.NUMEROLabel1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'COLONIALabel1
        '
        Me.COLONIALabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConContratoMaestroBindingSource, "COLONIA", True))
        Me.COLONIALabel1.Location = New System.Drawing.Point(84, 156)
        Me.COLONIALabel1.Name = "COLONIALabel1"
        Me.COLONIALabel1.Size = New System.Drawing.Size(419, 23)
        Me.COLONIALabel1.TabIndex = 7
        Me.COLONIALabel1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'CALLELabel1
        '
        Me.CALLELabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConContratoMaestroBindingSource, "CALLE", True))
        Me.CALLELabel1.Location = New System.Drawing.Point(84, 122)
        Me.CALLELabel1.Name = "CALLELabel1"
        Me.CALLELabel1.Size = New System.Drawing.Size(279, 23)
        Me.CALLELabel1.TabIndex = 5
        Me.CALLELabel1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'NOMBRELabel1
        '
        Me.NOMBRELabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConContratoMaestroBindingSource, "NOMBRE", True))
        Me.NOMBRELabel1.Location = New System.Drawing.Point(84, 87)
        Me.NOMBRELabel1.Name = "NOMBRELabel1"
        Me.NOMBRELabel1.Size = New System.Drawing.Size(419, 23)
        Me.NOMBRELabel1.TabIndex = 3
        Me.NOMBRELabel1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.GroupBox3)
        Me.GroupBox2.Controls.Add(Me.BindingNavigator2)
        Me.GroupBox2.Controls.Add(Me.ConSubContratoDataGridView)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.Color.Black
        Me.GroupBox2.Location = New System.Drawing.Point(34, 260)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(960, 410)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Sub Contrato(s)"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.CALLELabel3)
        Me.GroupBox3.Controls.Add(Me.CONTRATOLabel3)
        Me.GroupBox3.Controls.Add(ESHOTELLabel1)
        Me.GroupBox3.Controls.Add(CONTRATOLabel2)
        Me.GroupBox3.Controls.Add(Me.ESHOTELCheckBox1)
        Me.GroupBox3.Controls.Add(Me.NOMBRELabel3)
        Me.GroupBox3.Controls.Add(SOLOINTERNETLabel1)
        Me.GroupBox3.Controls.Add(NOMBRELabel2)
        Me.GroupBox3.Controls.Add(Me.SOLOINTERNETCheckBox1)
        Me.GroupBox3.Controls.Add(CALLELabel2)
        Me.GroupBox3.Controls.Add(CIUDADLabel2)
        Me.GroupBox3.Controls.Add(Me.COLONIALabel3)
        Me.GroupBox3.Controls.Add(Me.CIUDADLabel3)
        Me.GroupBox3.Controls.Add(COLONIALabel2)
        Me.GroupBox3.Controls.Add(NUMEROLabel2)
        Me.GroupBox3.Controls.Add(Me.NUMEROLabel3)
        Me.GroupBox3.Location = New System.Drawing.Point(480, 103)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(460, 234)
        Me.GroupBox3.TabIndex = 17
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Datos del Cliente:"
        '
        'CALLELabel3
        '
        Me.CALLELabel3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConSubContratoBindingSource, "CALLE", True))
        Me.CALLELabel3.Location = New System.Drawing.Point(81, 84)
        Me.CALLELabel3.Name = "CALLELabel3"
        Me.CALLELabel3.Size = New System.Drawing.Size(248, 23)
        Me.CALLELabel3.TabIndex = 6
        Me.CALLELabel3.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'ConSubContratoBindingSource
        '
        Me.ConSubContratoBindingSource.DataMember = "ConSubContrato"
        Me.ConSubContratoBindingSource.DataSource = Me.DataSetEric
        '
        'CONTRATOLabel3
        '
        Me.CONTRATOLabel3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConSubContratoBindingSource, "CONTRATO", True))
        Me.CONTRATOLabel3.Location = New System.Drawing.Point(81, 18)
        Me.CONTRATOLabel3.Name = "CONTRATOLabel3"
        Me.CONTRATOLabel3.Size = New System.Drawing.Size(100, 23)
        Me.CONTRATOLabel3.TabIndex = 2
        Me.CONTRATOLabel3.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'ESHOTELCheckBox1
        '
        Me.ESHOTELCheckBox1.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.ConSubContratoBindingSource, "ESHOTEL", True))
        Me.ESHOTELCheckBox1.Enabled = False
        Me.ESHOTELCheckBox1.Location = New System.Drawing.Point(354, 193)
        Me.ESHOTELCheckBox1.Name = "ESHOTELCheckBox1"
        Me.ESHOTELCheckBox1.Size = New System.Drawing.Size(15, 24)
        Me.ESHOTELCheckBox1.TabIndex = 16
        Me.ESHOTELCheckBox1.TabStop = False
        Me.ESHOTELCheckBox1.Visible = False
        '
        'NOMBRELabel3
        '
        Me.NOMBRELabel3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConSubContratoBindingSource, "NOMBRE", True))
        Me.NOMBRELabel3.Location = New System.Drawing.Point(81, 50)
        Me.NOMBRELabel3.Name = "NOMBRELabel3"
        Me.NOMBRELabel3.Size = New System.Drawing.Size(355, 23)
        Me.NOMBRELabel3.TabIndex = 4
        Me.NOMBRELabel3.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'SOLOINTERNETCheckBox1
        '
        Me.SOLOINTERNETCheckBox1.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.ConSubContratoBindingSource, "SOLOINTERNET", True))
        Me.SOLOINTERNETCheckBox1.Enabled = False
        Me.SOLOINTERNETCheckBox1.Location = New System.Drawing.Point(245, 194)
        Me.SOLOINTERNETCheckBox1.Name = "SOLOINTERNETCheckBox1"
        Me.SOLOINTERNETCheckBox1.Size = New System.Drawing.Size(16, 24)
        Me.SOLOINTERNETCheckBox1.TabIndex = 14
        Me.SOLOINTERNETCheckBox1.TabStop = False
        '
        'COLONIALabel3
        '
        Me.COLONIALabel3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConSubContratoBindingSource, "COLONIA", True))
        Me.COLONIALabel3.Location = New System.Drawing.Point(81, 116)
        Me.COLONIALabel3.Name = "COLONIALabel3"
        Me.COLONIALabel3.Size = New System.Drawing.Size(355, 23)
        Me.COLONIALabel3.TabIndex = 8
        Me.COLONIALabel3.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'CIUDADLabel3
        '
        Me.CIUDADLabel3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConSubContratoBindingSource, "CIUDAD", True))
        Me.CIUDADLabel3.Location = New System.Drawing.Point(81, 148)
        Me.CIUDADLabel3.Name = "CIUDADLabel3"
        Me.CIUDADLabel3.Size = New System.Drawing.Size(355, 23)
        Me.CIUDADLabel3.TabIndex = 12
        Me.CIUDADLabel3.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'NUMEROLabel3
        '
        Me.NUMEROLabel3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConSubContratoBindingSource, "NUMERO", True))
        Me.NUMEROLabel3.Location = New System.Drawing.Point(367, 84)
        Me.NUMEROLabel3.Name = "NUMEROLabel3"
        Me.NUMEROLabel3.Size = New System.Drawing.Size(69, 23)
        Me.NUMEROLabel3.TabIndex = 10
        Me.NUMEROLabel3.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'BindingNavigator2
        '
        Me.BindingNavigator2.AddNewItem = Nothing
        Me.BindingNavigator2.CountItem = Nothing
        Me.BindingNavigator2.DeleteItem = Nothing
        Me.BindingNavigator2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigator2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.ToolStripButton4})
        Me.BindingNavigator2.Location = New System.Drawing.Point(3, 18)
        Me.BindingNavigator2.MoveFirstItem = Nothing
        Me.BindingNavigator2.MoveLastItem = Nothing
        Me.BindingNavigator2.MoveNextItem = Nothing
        Me.BindingNavigator2.MovePreviousItem = Nothing
        Me.BindingNavigator2.Name = "BindingNavigator2"
        Me.BindingNavigator2.PositionItem = Nothing
        Me.BindingNavigator2.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.BindingNavigator2.Size = New System.Drawing.Size(954, 25)
        Me.BindingNavigator2.TabIndex = 1
        Me.BindingNavigator2.TabStop = True
        Me.BindingNavigator2.Text = "BindingNavigator2"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(74, 22)
        Me.ToolStripButton1.Text = "&ELIMINAR"
        '
        'ToolStripButton4
        '
        Me.ToolStripButton4.Image = CType(resources.GetObject("ToolStripButton4.Image"), System.Drawing.Image)
        Me.ToolStripButton4.Name = "ToolStripButton4"
        Me.ToolStripButton4.Size = New System.Drawing.Size(89, 22)
        Me.ToolStripButton4.Text = "&AGREGAR"
        '
        'ConSubContratoDataGridView
        '
        Me.ConSubContratoDataGridView.AllowUserToAddRows = False
        Me.ConSubContratoDataGridView.AllowUserToDeleteRows = False
        Me.ConSubContratoDataGridView.AutoGenerateColumns = False
        Me.ConSubContratoDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ConSubContratoDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.ConSubContratoDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewCheckBoxColumn1, Me.DataGridViewCheckBoxColumn2})
        Me.ConSubContratoDataGridView.DataSource = Me.ConSubContratoBindingSource
        Me.ConSubContratoDataGridView.Location = New System.Drawing.Point(17, 54)
        Me.ConSubContratoDataGridView.Name = "ConSubContratoDataGridView"
        Me.ConSubContratoDataGridView.ReadOnly = True
        Me.ConSubContratoDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.ConSubContratoDataGridView.Size = New System.Drawing.Size(445, 338)
        Me.ConSubContratoDataGridView.TabIndex = 0
        Me.ConSubContratoDataGridView.TabStop = False
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "CONTRATO"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Contrato"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "NOMBRE"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Nombre"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 250
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "CALLE"
        Me.DataGridViewTextBoxColumn3.HeaderText = "CALLE"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "COLONIA"
        Me.DataGridViewTextBoxColumn4.HeaderText = "COLONIA"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "NUMERO"
        Me.DataGridViewTextBoxColumn5.HeaderText = "NUMERO"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "CIUDAD"
        Me.DataGridViewTextBoxColumn6.HeaderText = "CIUDAD"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewCheckBoxColumn1
        '
        Me.DataGridViewCheckBoxColumn1.DataPropertyName = "SOLOINTERNET"
        Me.DataGridViewCheckBoxColumn1.HeaderText = "SOLOINTERNET"
        Me.DataGridViewCheckBoxColumn1.Name = "DataGridViewCheckBoxColumn1"
        Me.DataGridViewCheckBoxColumn1.ReadOnly = True
        Me.DataGridViewCheckBoxColumn1.Visible = False
        '
        'DataGridViewCheckBoxColumn2
        '
        Me.DataGridViewCheckBoxColumn2.DataPropertyName = "ESHOTEL"
        Me.DataGridViewCheckBoxColumn2.HeaderText = "ESHOTEL"
        Me.DataGridViewCheckBoxColumn2.Name = "DataGridViewCheckBoxColumn2"
        Me.DataGridViewCheckBoxColumn2.ReadOnly = True
        Me.DataGridViewCheckBoxColumn2.Visible = False
        '
        'ConContratoMaestroTableAdapter
        '
        Me.ConContratoMaestroTableAdapter.ClearBeforeFill = True
        '
        'ConSubContratoTableAdapter
        '
        Me.ConSubContratoTableAdapter.ClearBeforeFill = True
        '
        'BorContratoMaestroBindingSource
        '
        Me.BorContratoMaestroBindingSource.DataMember = "BorContratoMaestro"
        Me.BorContratoMaestroBindingSource.DataSource = Me.DataSetEric
        '
        'BorContratoMaestroTableAdapter
        '
        Me.BorContratoMaestroTableAdapter.ClearBeforeFill = True
        '
        'NueSubContratoBindingSource
        '
        Me.NueSubContratoBindingSource.DataMember = "NueSubContrato"
        Me.NueSubContratoBindingSource.DataSource = Me.DataSetEric
        '
        'NueSubContratoTableAdapter
        '
        Me.NueSubContratoTableAdapter.ClearBeforeFill = True
        '
        'NueContratoMaestroBindingSource
        '
        Me.NueContratoMaestroBindingSource.DataMember = "NueContratoMaestro"
        Me.NueContratoMaestroBindingSource.DataSource = Me.DataSetEric
        '
        'NueContratoMaestroTableAdapter
        '
        Me.NueContratoMaestroTableAdapter.ClearBeforeFill = True
        '
        'ValidaBorMaestroBindingSource
        '
        Me.ValidaBorMaestroBindingSource.DataMember = "ValidaBorMaestro"
        Me.ValidaBorMaestroBindingSource.DataSource = Me.DataSetEric
        '
        'ValidaBorMaestroTableAdapter
        '
        Me.ValidaBorMaestroTableAdapter.ClearBeforeFill = True
        '
        'FrmSubContrato
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1016, 734)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.GroupBox2)
        Me.Name = "FrmSubContrato"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Relación de Contrato Maestro con SubContrato(s)"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.ConContratoMaestroBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.ConSubContratoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingNavigator2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator2.ResumeLayout(False)
        Me.BindingNavigator2.PerformLayout()
        CType(Me.ConSubContratoDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorContratoMaestroBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NueSubContratoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NueContratoMaestroBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ValidaBorMaestroBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents ConContratoMaestroBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConContratoMaestroTableAdapter As sofTV.DataSetEricTableAdapters.ConContratoMaestroTableAdapter
    Friend WithEvents ESHOTELCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents SOLOINTERNETCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents CIUDADLabel1 As System.Windows.Forms.Label
    Friend WithEvents NUMEROLabel1 As System.Windows.Forms.Label
    Friend WithEvents COLONIALabel1 As System.Windows.Forms.Label
    Friend WithEvents CALLELabel1 As System.Windows.Forms.Label
    Friend WithEvents NOMBRELabel1 As System.Windows.Forms.Label
    Friend WithEvents ConSubContratoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConSubContratoTableAdapter As sofTV.DataSetEricTableAdapters.ConSubContratoTableAdapter
    Friend WithEvents ESHOTELCheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents SOLOINTERNETCheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents CIUDADLabel3 As System.Windows.Forms.Label
    Friend WithEvents NUMEROLabel3 As System.Windows.Forms.Label
    Friend WithEvents COLONIALabel3 As System.Windows.Forms.Label
    Friend WithEvents NOMBRELabel3 As System.Windows.Forms.Label
    Friend WithEvents CONTRATOLabel3 As System.Windows.Forms.Label
    Friend WithEvents ConSubContratoDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents BorContratoMaestroBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorContratoMaestroTableAdapter As sofTV.DataSetEricTableAdapters.BorContratoMaestroTableAdapter
    Friend WithEvents NueSubContratoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NueSubContratoTableAdapter As sofTV.DataSetEricTableAdapters.NueSubContratoTableAdapter
    Friend WithEvents NueContratoMaestroBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NueContratoMaestroTableAdapter As sofTV.DataSetEricTableAdapters.NueContratoMaestroTableAdapter
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigator2 As System.Windows.Forms.BindingNavigator
    Friend WithEvents ToolStripButton4 As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONTRATOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CALLELabel3 As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn1 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn2 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents ToolStripButton3 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ValidaBorMaestroBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ValidaBorMaestroTableAdapter As sofTV.DataSetEricTableAdapters.ValidaBorMaestroTableAdapter
End Class
