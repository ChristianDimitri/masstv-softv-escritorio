<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmServiciosDigitales
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Clv_ServicioDigitalLabel As System.Windows.Forms.Label
        Dim NombreLabel As System.Windows.Forms.Label
        Dim Precio_UnitarioLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmServiciosDigitales))
        Me.DataSetLidia2 = New sofTV.DataSetLidia2
        Me.Consulta_ServiciosDigitalesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Consulta_ServiciosDigitalesTableAdapter = New sofTV.DataSetLidia2TableAdapters.Consulta_ServiciosDigitalesTableAdapter
        Me.Consulta_ServiciosDigitalesBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton
        Me.Consulta_ServiciosDigitalesBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton
        Me.Clv_ServicioDigitalTextBox = New System.Windows.Forms.TextBox
        Me.NombreTextBox = New System.Windows.Forms.TextBox
        Me.Precio_UnitarioTextBox = New System.Windows.Forms.TextBox
        Me.Button5 = New System.Windows.Forms.Button
        Clv_ServicioDigitalLabel = New System.Windows.Forms.Label
        NombreLabel = New System.Windows.Forms.Label
        Precio_UnitarioLabel = New System.Windows.Forms.Label
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_ServiciosDigitalesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_ServiciosDigitalesBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Consulta_ServiciosDigitalesBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'Clv_ServicioDigitalLabel
        '
        Clv_ServicioDigitalLabel.AutoSize = True
        Clv_ServicioDigitalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_ServicioDigitalLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_ServicioDigitalLabel.Location = New System.Drawing.Point(23, 55)
        Clv_ServicioDigitalLabel.Name = "Clv_ServicioDigitalLabel"
        Clv_ServicioDigitalLabel.Size = New System.Drawing.Size(151, 15)
        Clv_ServicioDigitalLabel.TabIndex = 2
        Clv_ServicioDigitalLabel.Text = "Clave Servicio Digital :"
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.ForeColor = System.Drawing.Color.LightSlateGray
        NombreLabel.Location = New System.Drawing.Point(108, 95)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(66, 15)
        NombreLabel.TabIndex = 4
        NombreLabel.Text = "Nombre :"
        '
        'Precio_UnitarioLabel
        '
        Precio_UnitarioLabel.AutoSize = True
        Precio_UnitarioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Precio_UnitarioLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Precio_UnitarioLabel.Location = New System.Drawing.Point(63, 142)
        Precio_UnitarioLabel.Name = "Precio_UnitarioLabel"
        Precio_UnitarioLabel.Size = New System.Drawing.Size(111, 15)
        Precio_UnitarioLabel.TabIndex = 6
        Precio_UnitarioLabel.Text = "Precio Unitario :"
        '
        'DataSetLidia2
        '
        Me.DataSetLidia2.DataSetName = "DataSetLidia2"
        Me.DataSetLidia2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Consulta_ServiciosDigitalesBindingSource
        '
        Me.Consulta_ServiciosDigitalesBindingSource.DataMember = "Consulta_ServiciosDigitales"
        Me.Consulta_ServiciosDigitalesBindingSource.DataSource = Me.DataSetLidia2
        '
        'Consulta_ServiciosDigitalesTableAdapter
        '
        Me.Consulta_ServiciosDigitalesTableAdapter.ClearBeforeFill = True
        '
        'Consulta_ServiciosDigitalesBindingNavigator
        '
        Me.Consulta_ServiciosDigitalesBindingNavigator.AddNewItem = Nothing
        Me.Consulta_ServiciosDigitalesBindingNavigator.BindingSource = Me.Consulta_ServiciosDigitalesBindingSource
        Me.Consulta_ServiciosDigitalesBindingNavigator.CountItem = Nothing
        Me.Consulta_ServiciosDigitalesBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.Consulta_ServiciosDigitalesBindingNavigator.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Consulta_ServiciosDigitalesBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.Consulta_ServiciosDigitalesBindingNavigatorSaveItem})
        Me.Consulta_ServiciosDigitalesBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.Consulta_ServiciosDigitalesBindingNavigator.MoveFirstItem = Nothing
        Me.Consulta_ServiciosDigitalesBindingNavigator.MoveLastItem = Nothing
        Me.Consulta_ServiciosDigitalesBindingNavigator.MoveNextItem = Nothing
        Me.Consulta_ServiciosDigitalesBindingNavigator.MovePreviousItem = Nothing
        Me.Consulta_ServiciosDigitalesBindingNavigator.Name = "Consulta_ServiciosDigitalesBindingNavigator"
        Me.Consulta_ServiciosDigitalesBindingNavigator.PositionItem = Nothing
        Me.Consulta_ServiciosDigitalesBindingNavigator.Size = New System.Drawing.Size(575, 25)
        Me.Consulta_ServiciosDigitalesBindingNavigator.TabIndex = 4
        Me.Consulta_ServiciosDigitalesBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(77, 22)
        Me.BindingNavigatorDeleteItem.Text = "Eliminar"
        '
        'Consulta_ServiciosDigitalesBindingNavigatorSaveItem
        '
        Me.Consulta_ServiciosDigitalesBindingNavigatorSaveItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.Consulta_ServiciosDigitalesBindingNavigatorSaveItem.Image = CType(resources.GetObject("Consulta_ServiciosDigitalesBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.Consulta_ServiciosDigitalesBindingNavigatorSaveItem.Name = "Consulta_ServiciosDigitalesBindingNavigatorSaveItem"
        Me.Consulta_ServiciosDigitalesBindingNavigatorSaveItem.Size = New System.Drawing.Size(121, 22)
        Me.Consulta_ServiciosDigitalesBindingNavigatorSaveItem.Text = "Guardar datos"
        '
        'Clv_ServicioDigitalTextBox
        '
        Me.Clv_ServicioDigitalTextBox.BackColor = System.Drawing.Color.White
        Me.Clv_ServicioDigitalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ServiciosDigitalesBindingSource, "clv_ServicioDigital", True))
        Me.Clv_ServicioDigitalTextBox.Location = New System.Drawing.Point(187, 52)
        Me.Clv_ServicioDigitalTextBox.Name = "Clv_ServicioDigitalTextBox"
        Me.Clv_ServicioDigitalTextBox.ReadOnly = True
        Me.Clv_ServicioDigitalTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Clv_ServicioDigitalTextBox.TabIndex = 5
        '
        'NombreTextBox
        '
        Me.NombreTextBox.BackColor = System.Drawing.Color.White
        Me.NombreTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ServiciosDigitalesBindingSource, "Nombre", True))
        Me.NombreTextBox.Location = New System.Drawing.Point(187, 94)
        Me.NombreTextBox.Name = "NombreTextBox"
        Me.NombreTextBox.Size = New System.Drawing.Size(350, 20)
        Me.NombreTextBox.TabIndex = 1
        '
        'Precio_UnitarioTextBox
        '
        Me.Precio_UnitarioTextBox.BackColor = System.Drawing.Color.White
        Me.Precio_UnitarioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ServiciosDigitalesBindingSource, "Precio_Unitario", True))
        Me.Precio_UnitarioTextBox.Location = New System.Drawing.Point(187, 141)
        Me.Precio_UnitarioTextBox.Name = "Precio_UnitarioTextBox"
        Me.Precio_UnitarioTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Precio_UnitarioTextBox.TabIndex = 2
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(418, 206)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 36)
        Me.Button5.TabIndex = 3
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'FrmServiciosDigitales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(575, 261)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Clv_ServicioDigitalLabel)
        Me.Controls.Add(Me.Clv_ServicioDigitalTextBox)
        Me.Controls.Add(NombreLabel)
        Me.Controls.Add(Me.NombreTextBox)
        Me.Controls.Add(Precio_UnitarioLabel)
        Me.Controls.Add(Me.Precio_UnitarioTextBox)
        Me.Controls.Add(Me.Consulta_ServiciosDigitalesBindingNavigator)
        Me.MaximizeBox = False
        Me.Name = "FrmServiciosDigitales"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Servicios Digitales"
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_ServiciosDigitalesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_ServiciosDigitalesBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Consulta_ServiciosDigitalesBindingNavigator.ResumeLayout(False)
        Me.Consulta_ServiciosDigitalesBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataSetLidia2 As sofTV.DataSetLidia2
    Friend WithEvents Consulta_ServiciosDigitalesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_ServiciosDigitalesTableAdapter As sofTV.DataSetLidia2TableAdapters.Consulta_ServiciosDigitalesTableAdapter
    Friend WithEvents Consulta_ServiciosDigitalesBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Consulta_ServiciosDigitalesBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Clv_ServicioDigitalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NombreTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Precio_UnitarioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
End Class
