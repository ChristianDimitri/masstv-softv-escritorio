﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwTarifasPagare
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.NuevoButton = New System.Windows.Forms.Button()
        Me.ModificarButton = New System.Windows.Forms.Button()
        Me.EliminarButton = New System.Windows.Forms.Button()
        Me.SalirButton = New System.Windows.Forms.Button()
        Me.TipServCombo = New System.Windows.Forms.ComboBox()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.CostosDataGrid = New System.Windows.Forms.DataGridView()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TIPSER = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Articulo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Costo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Principal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Adicional = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        CType(Me.CostosDataGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'NuevoButton
        '
        Me.NuevoButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.NuevoButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NuevoButton.Location = New System.Drawing.Point(756, 12)
        Me.NuevoButton.Name = "NuevoButton"
        Me.NuevoButton.Size = New System.Drawing.Size(122, 35)
        Me.NuevoButton.TabIndex = 1
        Me.NuevoButton.Text = "&Nuevo"
        Me.NuevoButton.UseVisualStyleBackColor = True
        '
        'ModificarButton
        '
        Me.ModificarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ModificarButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ModificarButton.Location = New System.Drawing.Point(756, 53)
        Me.ModificarButton.Name = "ModificarButton"
        Me.ModificarButton.Size = New System.Drawing.Size(122, 34)
        Me.ModificarButton.TabIndex = 2
        Me.ModificarButton.Text = "&Modificar"
        Me.ModificarButton.UseVisualStyleBackColor = True
        '
        'EliminarButton
        '
        Me.EliminarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.EliminarButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EliminarButton.Location = New System.Drawing.Point(756, 93)
        Me.EliminarButton.Name = "EliminarButton"
        Me.EliminarButton.Size = New System.Drawing.Size(122, 34)
        Me.EliminarButton.TabIndex = 3
        Me.EliminarButton.Text = "&Eliminar"
        Me.EliminarButton.UseVisualStyleBackColor = True
        '
        'SalirButton
        '
        Me.SalirButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.SalirButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SalirButton.Location = New System.Drawing.Point(756, 583)
        Me.SalirButton.Name = "SalirButton"
        Me.SalirButton.Size = New System.Drawing.Size(122, 34)
        Me.SalirButton.TabIndex = 4
        Me.SalirButton.Text = "&Salir"
        Me.SalirButton.UseVisualStyleBackColor = True
        '
        'TipServCombo
        '
        Me.TipServCombo.DisplayMember = "CONCEPTO"
        Me.TipServCombo.FormattingEnabled = True
        Me.TipServCombo.Location = New System.Drawing.Point(4, 81)
        Me.TipServCombo.Name = "TipServCombo"
        Me.TipServCombo.Size = New System.Drawing.Size(146, 21)
        Me.TipServCombo.TabIndex = 0
        Me.TipServCombo.ValueMember = "CLV_TIPSER"
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(8, 18)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(106, 20)
        Me.CMBLabel1.TabIndex = 5
        Me.CMBLabel1.Text = "Buscar por :"
        '
        'CostosDataGrid
        '
        Me.CostosDataGrid.AllowUserToAddRows = False
        Me.CostosDataGrid.AllowUserToDeleteRows = False
        Me.CostosDataGrid.BackgroundColor = System.Drawing.SystemColors.Window
        Me.CostosDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.CostosDataGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.TIPSER, Me.Articulo, Me.Costo, Me.Principal, Me.Adicional})
        Me.CostosDataGrid.GridColor = System.Drawing.SystemColors.Window
        Me.CostosDataGrid.Location = New System.Drawing.Point(156, 9)
        Me.CostosDataGrid.Name = "CostosDataGrid"
        Me.CostosDataGrid.ReadOnly = True
        Me.CostosDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.CostosDataGrid.Size = New System.Drawing.Size(594, 608)
        Me.CostosDataGrid.TabIndex = 6
        '
        'ID
        '
        Me.ID.DataPropertyName = "id"
        Me.ID.HeaderText = "ID COSTO"
        Me.ID.Name = "ID"
        Me.ID.ReadOnly = True
        Me.ID.Visible = False
        '
        'TIPSER
        '
        Me.TIPSER.DataPropertyName = "TIPSER"
        Me.TIPSER.HeaderText = "Tipo de Servicio"
        Me.TIPSER.Name = "TIPSER"
        Me.TIPSER.ReadOnly = True
        '
        'Articulo
        '
        Me.Articulo.DataPropertyName = "Articulo"
        Me.Articulo.HeaderText = "Descripción Artículo"
        Me.Articulo.Name = "Articulo"
        Me.Articulo.ReadOnly = True
        Me.Articulo.Width = 150
        '
        'Costo
        '
        Me.Costo.DataPropertyName = "Costo"
        Me.Costo.HeaderText = "Costo Artículo"
        Me.Costo.Name = "Costo"
        Me.Costo.ReadOnly = True
        '
        'Principal
        '
        Me.Principal.DataPropertyName = "Principal"
        Me.Principal.HeaderText = "Renta Principal"
        Me.Principal.Name = "Principal"
        Me.Principal.ReadOnly = True
        '
        'Adicional
        '
        Me.Adicional.DataPropertyName = "Adicional"
        Me.Adicional.HeaderText = "Renta Adicional"
        Me.Adicional.Name = "Adicional"
        Me.Adicional.ReadOnly = True
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(1, 62)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(131, 16)
        Me.CMBLabel2.TabIndex = 7
        Me.CMBLabel2.Text = "Tipo de Servicio :"
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'BrwTarifasPagare
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(883, 629)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.CostosDataGrid)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.TipServCombo)
        Me.Controls.Add(Me.SalirButton)
        Me.Controls.Add(Me.EliminarButton)
        Me.Controls.Add(Me.ModificarButton)
        Me.Controls.Add(Me.NuevoButton)
        Me.Name = "BrwTarifasPagare"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Costo Aparato Renta"
        CType(Me.CostosDataGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents NuevoButton As System.Windows.Forms.Button
    Friend WithEvents ModificarButton As System.Windows.Forms.Button
    Friend WithEvents EliminarButton As System.Windows.Forms.Button
    Friend WithEvents SalirButton As System.Windows.Forms.Button
    Friend WithEvents TipServCombo As System.Windows.Forms.ComboBox
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents CostosDataGrid As System.Windows.Forms.DataGridView
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TIPSER As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Articulo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Costo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Principal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Adicional As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
