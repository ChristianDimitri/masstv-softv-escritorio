Imports System.Data.SqlClient
Public Class FrmConsulta_Prog
    Dim job_id As String, namejob As String, hab As Integer
    Dim descrip As String, fecha_crea As Date, fecha_mod As Date, id_prog As Integer
    Dim name_prog As String, hab_p As Integer, frec_type As Integer, frec_interval As Integer
    Dim frec_sub_type As Integer, frec_sub_inter As Integer, frec_rel_inter As Integer, frec_rec_fac As Integer
    Dim fecha_ini As Integer, fecha_fin As Integer, hr_ini As Integer, hr_fin As Integer, dias_semana As Integer
    Dim horas As String
    Private Sub FrmConsulta_Prog_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Button1.BackColor = Color.Gray
        Me.Button2.BackColor = Color.Gray
        Me.GroupBox1.BackColor = Color.Gray
        Me.GroupBox2.BackColor = Color.Gray
        Me.GroupBox3.BackColor = Color.Gray
        Me.GroupBox4.BackColor = Color.Gray
        Dim conlidia As New SqlClient.SqlConnection(MiConexion)
        conlidia.Open()
        'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetLidia.Muestra_Tipo_Frecuancia' Puede moverla o quitarla seg�n sea necesario.
        Me.Muestra_Tipo_FrecuanciaTableAdapter.Connection = conlidia
        Me.Muestra_Tipo_FrecuanciaTableAdapter.Fill(Me.DataSetLidia.Muestra_Tipo_Frecuancia)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetLidia.Muestra_Programacion' Puede moverla o quitarla seg�n sea necesario.
        Me.Muestra_ProgramacionTableAdapter.Connection = conlidia
        Me.Muestra_ProgramacionTableAdapter.Fill(Me.DataSetLidia.Muestra_Programacion)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetLidia.Muestra_WeekDay' Puede moverla o quitarla seg�n sea necesario.
        Me.Muestra_WeekDayTableAdapter.Connection = conlidia
        Me.Muestra_WeekDayTableAdapter.Fill(Me.DataSetLidia.Muestra_WeekDay)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetLidia.Muestra_Ordinales' Puede moverla o quitarla seg�n sea necesario.
        Me.Muestra_OrdinalesTableAdapter.Connection = conlidia
        Me.Muestra_OrdinalesTableAdapter.Fill(Me.DataSetLidia.Muestra_Ordinales)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetLidia.Muestra_TipoIntervalo' Puede moverla o quitarla seg�n sea necesario.
        Me.Muestra_TipoIntervaloTableAdapter.Connection = conlidia
        Me.Muestra_TipoIntervaloTableAdapter.Fill(Me.DataSetLidia.Muestra_TipoIntervalo)
        conlidia.Close()
        If opcion = "C" Then
            Me.Button1.Enabled = False
            ' Me.TabControl1.Enabled = False
            Me.CheckBox8.Checked = True
            Me.DateTimePicker7.Enabled = False
            Me.NumericUpDown6.Enabled = False
            Me.DateTimePicker3.Enabled = False
            Me.ComboBox4.Enabled = False
            Me.DateTimePicker4.Enabled = False
            Me.DateTimePicker5.Enabled = False
            Me.RadioButton5.Checked = True
            Me.RadioButton4.Checked = True
        End If
     
        If opcion = "C" Or opcion = "M" Then
            Dim conlidia2 As New SqlClient.SqlConnection(MiConexion)
            conlidia2.Open()
            Dim comando As New SqlClient.SqlCommand
            ''==== CONSULTA TRABAJOS 
            comando = New SqlClient.SqlCommand
            With comando
                .Connection = conlidia2
                .CommandText = "Consulta_Programaciones "
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0

                '(@job_id varchar(200),@namejob varchar(200) output,@hab int output,
                '@descrip varchar(300) output,@fecha_crea datetime output,@fecha_mod datetime output,@id_prog int output,
                '@name_prog varchar(200) output,@hab_p int output,@frec_type int output,@frec_interval int output,
                '@frec_sub_type int output,@frec_sub_inter int output,@frec_rel_inter int output,@frec_rec_fac int output,
                '@fecha_ini int output,@fecha_fin int output,@hr_ini int output,@hr_fin int output)

                ' Create a SqlParameter for each parameter in the stored procedure.
                Dim prm As New SqlParameter("@Job_id", SqlDbType.VarChar, 200)
                Dim prm1 As New SqlParameter("@Namejob", SqlDbType.VarChar, 200)
                Dim prm2 As New SqlParameter("@Hab", SqlDbType.Int)
                Dim prm3 As New SqlParameter("@Descrip", SqlDbType.VarChar, 300)
                Dim prm4 As New SqlParameter("@Fecha_crea", SqlDbType.DateTime)
                Dim prm5 As New SqlParameter("@Fecha_mod", SqlDbType.DateTime)
                Dim prm6 As New SqlParameter("@id_prog", SqlDbType.Int)
                Dim prm7 As New SqlParameter("@name_prog", SqlDbType.VarChar, 200)
                Dim prm8 As New SqlParameter("@hab_p", SqlDbType.Int)
                Dim prm9 As New SqlParameter("@frec_type", SqlDbType.Int)
                Dim prm10 As New SqlParameter("@frec_interval", SqlDbType.Int)
                Dim prm11 As New SqlParameter("@frec_sub_type", SqlDbType.Int)
                Dim prm12 As New SqlParameter("@frec_sub_inter", SqlDbType.Int)
                Dim prm13 As New SqlParameter("@frec_rel_inter", SqlDbType.Int)
                Dim prm14 As New SqlParameter("@frec_rec_fac", SqlDbType.Int)
                Dim prm15 As New SqlParameter("@fecha_ini", SqlDbType.Int)
                Dim prm16 As New SqlParameter("@fecha_fin", SqlDbType.Int)
                Dim prm17 As New SqlParameter("@hr_ini", SqlDbType.Int)
                Dim prm18 As New SqlParameter("@hr_fin", SqlDbType.Int)

                prm.Direction = ParameterDirection.Input
                prm1.Direction = ParameterDirection.Output
                prm2.Direction = ParameterDirection.Output
                prm3.Direction = ParameterDirection.Output
                prm4.Direction = ParameterDirection.Output
                prm5.Direction = ParameterDirection.Output
                prm6.Direction = ParameterDirection.Output
                prm7.Direction = ParameterDirection.Output
                prm8.Direction = ParameterDirection.Output
                prm9.Direction = ParameterDirection.Output
                prm10.Direction = ParameterDirection.Output
                prm11.Direction = ParameterDirection.Output
                prm12.Direction = ParameterDirection.Output
                prm13.Direction = ParameterDirection.Output
                prm14.Direction = ParameterDirection.Output
                prm15.Direction = ParameterDirection.Output
                prm16.Direction = ParameterDirection.Output
                prm17.Direction = ParameterDirection.Output
                prm18.Direction = ParameterDirection.Output

                prm.Value = gloJobId
                prm1.Value = 0
                prm2.Value = 0
                prm3.Value = 0
                prm4.Value = 0
                prm5.Value = 0
                prm6.Value = 0
                prm7.Value = 0
                prm8.Value = 0
                prm9.Value = 0
                prm10.Value = 0
                prm11.Value = 0
                prm12.Value = 0
                prm13.Value = 0
                prm14.Value = 0
                prm15.Value = 0
                prm16.Value = 0
                prm17.Value = 0
                prm18.Value = 0
                .Parameters.Add(prm)
                .Parameters.Add(prm1)
                .Parameters.Add(prm2)
                .Parameters.Add(prm3)
                .Parameters.Add(prm4)
                .Parameters.Add(prm5)
                .Parameters.Add(prm6)
                .Parameters.Add(prm7)
                .Parameters.Add(prm8)
                .Parameters.Add(prm9)
                .Parameters.Add(prm10)
                .Parameters.Add(prm11)
                .Parameters.Add(prm12)
                .Parameters.Add(prm13)
                .Parameters.Add(prm14)
                .Parameters.Add(prm15)
                .Parameters.Add(prm16)
                .Parameters.Add(prm17)
                .Parameters.Add(prm18)
                Dim i As Integer = comando.ExecuteNonQuery()
                namejob = prm1.Value
                hab = prm2.Value
                descrip = prm3.Value
                fecha_crea = prm4.Value
                fecha_mod = prm5.Value
                id_prog = prm6.Value
                name_prog = prm7.Value
                hab_p = prm8.Value
                frec_type = prm9.Value
                frec_interval = prm10.Value
                frec_sub_type = prm11.Value
                frec_sub_inter = prm12.Value
                frec_rel_inter = prm13.Value
                frec_rec_fac = prm14.Value
                fecha_ini = prm15.Value
                fecha_fin = prm16.Value
                hr_ini = prm17.Value
                hr_fin = prm18.Value

            End With
            conlidia2.Close()

        End If
        Me.TextBox1.Text = name_prog
        If frec_rec_fac = 0 Then
            frec_rec_fac = 1
        End If
        If frec_rel_inter = 0 Then
            frec_rel_inter = 1
        End If
        If frec_interval = 0 Then
            frec_interval = 1
        End If
        Select Case frec_type
            Case 1
                Me.TabControl1.SelectedIndex = 0
                Me.DateTimePicker2.Text = CDate(Mid(fecha_ini, 7, 2) + "/" + Mid(fecha_ini, 5, 2) + "/" + Mid(fecha_ini, 1, 4))
                horas = hr_ini.ToString.PadLeft(6, "0")
                Me.DateTimePicker1.Text = CDate(Mid(horas, 1, 2) + ":" + Mid(horas, 3, 2) + ":" + Mid(horas, 5, 2))

            Case 4
                Me.TabControl1.SelectedIndex = 1
                Me.NumericUpDown1.Value = frec_interval
            Case 8
                Me.TabControl1.SelectedIndex = 2
                Me.NumericUpDown2.Value = frec_rec_fac
                Checa_dias2(frec_interval)
            Case 16
                Me.TabControl1.SelectedIndex = 3
                Me.NumericUpDown4.Value = frec_rec_fac
                Me.NumericUpDown3.Value = frec_interval
                Me.RadioButton1.Checked = True
            Case 32
                Me.TabControl1.SelectedIndex = 3
                Me.NumericUpDown5.Value = frec_rec_fac
                Me.ComboBox2.SelectedValue = frec_rel_inter
                Me.ComboBox3.SelectedValue = frec_interval
                Me.RadioButton2.Checked = True

        End Select



        If frec_sub_type = 1 Then
            horas = hr_ini.ToString.PadLeft(6, "0")
            Me.DateTimePicker3.Text = CDate(Mid(hr_ini, 1, 2) + ":" + Mid(hr_ini, 3, 2) + ":" + Mid(hr_ini, 5, 2))
            Me.RadioButton4.Checked = True
            Me.RadioButton4.Checked = True
        ElseIf frec_sub_type > 1 Then
            horas = hr_ini.ToString.PadLeft(6, "0")
            Me.NumericUpDown6.Value = frec_sub_inter
            Me.ComboBox4.SelectedValue = frec_sub_type
            Me.RadioButton3.Checked = True
            Me.DateTimePicker4.Text = CDate(Mid(horas, 1, 2) + ":" + Mid(horas, 3, 2) + ":" + Mid(horas, 5, 2))
            horas = hr_fin.ToString.PadLeft(6, "0")
            Me.DateTimePicker5.Text = CDate(Mid(horas, 1, 2) + ":" + Mid(horas, 3, 2) + ":" + Mid(horas, 5, 2))
        End If

        Me.DateTimePicker8.Value = CDate(Mid(fecha_ini, 7, 2) + "/" + Mid(fecha_ini, 5, 2) + "/" + Mid(fecha_ini, 1, 4))

        If fecha_fin = 99991231 Then
            Me.RadioButton5.Checked = True
        Else
            Me.RadioButton6.Checked = True
            Me.DateTimePicker7.Value = CDate(Mid(fecha_fin, 7, 2) + "/" + Mid(fecha_fin, 5, 2) + "/" + Mid(fecha_fin, 1, 4))
        End If

        Me.CheckBox8.Checked = hab_p

        If opcion = "C" Then
            Me.GroupBox1.Enabled = False
            Me.GroupBox2.Enabled = False
            Me.GroupBox3.Enabled = False
            Me.GroupBox4.Enabled = False
        End If

    End Sub



    Private Sub RadioButton4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton4.CheckedChanged
        If Me.RadioButton4.Checked = False Then
            Me.DateTimePicker3.Enabled = False
            Me.NumericUpDown6.Enabled = True
            Me.ComboBox4.Enabled = True
            Me.DateTimePicker4.Enabled = True
            Me.DateTimePicker5.Enabled = True
        ElseIf Me.RadioButton4.Checked = True Then
            Me.DateTimePicker3.Enabled = True
            Me.NumericUpDown6.Enabled = False
            Me.ComboBox4.Enabled = False
            Me.DateTimePicker4.Enabled = False
            Me.DateTimePicker5.Enabled = False
        Else
            Me.DateTimePicker3.Enabled = False
            Me.NumericUpDown6.Enabled = False
            Me.ComboBox4.Enabled = False
            Me.DateTimePicker4.Enabled = False
            Me.DateTimePicker5.Enabled = False
        End If
    End Sub

    Private Sub RadioButton6_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton6.CheckedChanged
        If Me.RadioButton6.Checked = False Then
            Me.DateTimePicker7.Enabled = False
        ElseIf Me.RadioButton6.Enabled = True Then
            Me.DateTimePicker7.Enabled = True
        End If
    End Sub

    Private Sub RadioButton5_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton5.CheckedChanged
        If Me.RadioButton5.Checked = False Then
            Me.DateTimePicker7.Enabled = True
        ElseIf Me.RadioButton5.Checked = True Then
            Me.DateTimePicker7.Enabled = False
        End If

    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        If Me.RadioButton1.Checked = True Then
            Me.NumericUpDown3.Enabled = True
            Me.NumericUpDown4.Enabled = True
            Me.Label10.Enabled = True
            Me.Label9.Enabled = True
            frec_type = 16
        Else
            Me.NumericUpDown3.Enabled = False
            Me.NumericUpDown4.Enabled = False
            Me.Label10.Enabled = False
            Me.Label9.Enabled = False
            'frec_type = 32
        End If
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        If Me.RadioButton2.Checked = True Then
            Me.ComboBox2.Enabled = True
            Me.ComboBox3.Enabled = True
            Me.NumericUpDown5.Enabled = True
            Me.Label11.Enabled = True
            Me.Label12.Enabled = True
            frec_type = 32
        Else
            Me.ComboBox2.Enabled = False
            Me.ComboBox3.Enabled = False
            Me.NumericUpDown5.Enabled = False
            Me.Label11.Enabled = False
            Me.Label12.Enabled = False
            'frec_type = 16
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If frec_type = 0 Then
            MsgBox("Se Debe de de Seleccionar una Programaci�n ", MsgBoxStyle.Information)
            Exit Sub
        Else
            If frec_type = 1 Then
                DateTimePicker2.Format = DateTimePickerFormat.Custom
                DateTimePicker2.CustomFormat = "yyyyMMdd"
                fecha_ini = CInt(DateTimePicker2.Text)
                DateTimePicker2.Format = DateTimePickerFormat.Short
                DateTimePicker1.Format = DateTimePickerFormat.Custom
                DateTimePicker1.CustomFormat = "HHmmss"
                hr_ini = CInt(DateTimePicker1.Text)
                DateTimePicker1.Format = DateTimePickerFormat.Time
                modifica_trabajo()
                Exit Sub

            ElseIf frec_type = 4 Then
                ' DIAS    ====================
                frec_interval = Me.NumericUpDown1.Value
            ElseIf frec_type = 8 Then  ' SEMANAS   =============
                frec_rec_fac = Me.NumericUpDown2.Value
                Checa_dias(Me.TabPage3)
                frec_interval = dias_semana
            ElseIf frec_type = 32 Then  ' MESES    ===============
                frec_rec_fac = Me.NumericUpDown5.Value
                frec_interval = CStr(Me.ComboBox3.SelectedValue)
                frec_sub_inter = Me.ComboBox2.SelectedValue
            ElseIf frec_type = 16 Then
                frec_interval = CStr(Me.NumericUpDown3.Value)
                frec_rec_fac = Me.NumericUpDown4.Value
            End If

            If Me.RadioButton3.Checked = False And Me.RadioButton4.Checked = True Then
                DateTimePicker3.Format = DateTimePickerFormat.Custom
                DateTimePicker3.CustomFormat = "HHmmss"
                hr_ini = CInt(Me.DateTimePicker3.Text)
                DateTimePicker3.Format = DateTimePickerFormat.Time
            ElseIf Me.RadioButton3.Checked = True And Me.RadioButton4.Checked = False Then
                If Me.ComboBox4.Text = " " Then
                    MsgBox("Seleccione Horas/Minutos en el Combo ")
                    Exit Sub
                End If
                frec_sub_inter = Me.NumericUpDown6.Value
                DateTimePicker4.Format = DateTimePickerFormat.Custom
                DateTimePicker4.CustomFormat = "HHmmss"
                hr_ini = CInt(Me.DateTimePicker4.Text)
                DateTimePicker4.Format = DateTimePickerFormat.Time

                DateTimePicker5.Format = DateTimePickerFormat.Custom
                DateTimePicker5.CustomFormat = "HHmmss"
                hr_fin = CInt(Me.DateTimePicker5.Text)
                DateTimePicker5.Format = DateTimePickerFormat.Time
                frec_sub_type = Me.ComboBox4.SelectedValue

            End If

            DateTimePicker8.Format = DateTimePickerFormat.Custom
            DateTimePicker8.CustomFormat = "yyyyMMdd"
            fecha_ini = CInt(Me.DateTimePicker8.Text)
            DateTimePicker8.Format = DateTimePickerFormat.Short

            If Me.RadioButton6.Checked = True Then
                DateTimePicker7.Format = DateTimePickerFormat.Custom
                DateTimePicker7.CustomFormat = "yyyyMMdd"
                fecha_fin = CInt(Me.DateTimePicker7.Text)
                DateTimePicker7.Format = DateTimePickerFormat.Short
            End If
            modifica_trabajo()
        End If
    End Sub
    Private Sub modifica_trabajo()
        Dim conlidia2 As New SqlClient.SqlConnection(MiConexion)
        conlidia2.Open()
        Dim comando As New SqlClient.SqlCommand
        ''==== MODIFICA TRABAJOS 
        comando = New SqlClient.SqlCommand
        With comando
            .Connection = conlidia2
            .CommandText = "Modifica_Programacion "
            .CommandType = CommandType.StoredProcedure
            .CommandTimeout = 0

            ' Create a SqlParameter for each parameter in the stored procedure.
            Dim prm As New SqlParameter("@Job_id", SqlDbType.VarChar, 200)
            Dim prm1 As New SqlParameter("@Namejob", SqlDbType.VarChar, 200)
            Dim prm2 As New SqlParameter("@Hab", SqlDbType.Int)
            Dim prm3 As New SqlParameter("@Descrip", SqlDbType.VarChar, 300)
            Dim prm4 As New SqlParameter("@Fecha_crea", SqlDbType.DateTime)
            Dim prm5 As New SqlParameter("@Fecha_mod", SqlDbType.DateTime)
            Dim prm6 As New SqlParameter("@id_prog", SqlDbType.Int)
            Dim prm7 As New SqlParameter("@name_prog", SqlDbType.VarChar, 200)
            Dim prm8 As New SqlParameter("@hab_p", SqlDbType.Int)
            Dim prm9 As New SqlParameter("@frec_type", SqlDbType.Int)
            Dim prm10 As New SqlParameter("@frec_interval", SqlDbType.Int)
            Dim prm11 As New SqlParameter("@frec_sub_type", SqlDbType.Int)
            Dim prm12 As New SqlParameter("@frec_sub_inter", SqlDbType.Int)
            Dim prm13 As New SqlParameter("@frec_rel_inter", SqlDbType.Int)
            Dim prm14 As New SqlParameter("@frec_rec_fac", SqlDbType.Int)
            Dim prm15 As New SqlParameter("@fecha_ini", SqlDbType.Int)
            Dim prm16 As New SqlParameter("@fecha_fin", SqlDbType.Int)
            Dim prm17 As New SqlParameter("@hr_ini", SqlDbType.Int)
            Dim prm18 As New SqlParameter("@hr_fin", SqlDbType.Int)

            prm.Direction = ParameterDirection.Input
            prm1.Direction = ParameterDirection.Input
            prm2.Direction = ParameterDirection.Input
            prm3.Direction = ParameterDirection.Input
            prm4.Direction = ParameterDirection.Input
            prm5.Direction = ParameterDirection.Input
            prm6.Direction = ParameterDirection.Input
            prm7.Direction = ParameterDirection.Input
            prm8.Direction = ParameterDirection.Input
            prm9.Direction = ParameterDirection.Input
            prm10.Direction = ParameterDirection.Input
            prm11.Direction = ParameterDirection.Input
            prm12.Direction = ParameterDirection.Input
            prm13.Direction = ParameterDirection.Input
            prm14.Direction = ParameterDirection.Input
            prm15.Direction = ParameterDirection.Input
            prm16.Direction = ParameterDirection.Input
            prm17.Direction = ParameterDirection.Input
            prm18.Direction = ParameterDirection.Input

            prm.Value = gloJobId
            prm1.Value = namejob
            prm2.Value = hab
            prm3.Value = descrip
            prm4.Value = fecha_crea
            prm5.Value = fecha_mod
            prm6.Value = id_prog
            prm7.Value = name_prog
            prm8.Value = hab_p
            prm9.Value = frec_type
            prm10.Value = frec_interval
            prm11.Value = frec_sub_type
            prm12.Value = frec_sub_inter
            prm13.Value = frec_rel_inter
            prm14.Value = frec_rec_fac
            prm15.Value = fecha_ini
            prm16.Value = fecha_fin
            prm17.Value = hr_ini
            prm18.Value = hr_fin

            .Parameters.Add(prm)
            .Parameters.Add(prm1)
            .Parameters.Add(prm2)
            .Parameters.Add(prm3)
            .Parameters.Add(prm4)
            .Parameters.Add(prm5)
            .Parameters.Add(prm6)
            .Parameters.Add(prm7)
            .Parameters.Add(prm8)
            .Parameters.Add(prm9)
            .Parameters.Add(prm10)
            .Parameters.Add(prm11)
            .Parameters.Add(prm12)
            .Parameters.Add(prm13)
            .Parameters.Add(prm14)
            .Parameters.Add(prm15)
            .Parameters.Add(prm16)
            .Parameters.Add(prm17)
            .Parameters.Add(prm18)

            Dim i As Integer = comando.ExecuteNonQuery()

        End With
        conlidia2.Close()
        MsgBox("El Proceso Ha sido Generado con �xito", MsgBoxStyle.Information)
        bec_bnd = True
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub


    Private Sub CheckBox8_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox8.CheckedChanged
        If Me.CheckBox8.Checked = False Then
            hab_p = 0
            hab = 0
        ElseIf Me.CheckBox8.Checked = True Then
            hab_p = 1
            hab = 1
        Else
        End If
    End Sub
    Private Sub Checa_dias(ByVal formulario As TabPage) 'Panel
        Dim var As Integer
        Dim CHECK As CheckBox
        For Each ctm As Control In formulario.Controls

            If ctm.GetType Is GetType(System.Windows.Forms.CheckBox) Then
                CHECK = New CheckBox
                CHECK = ctm
                If CHECK.Checked = True Then
                    If dias_semana = 0 Then
                        var = CInt(Mid(CHECK.Name, 9, 1))
                        var = 2 ^ (var - 1)
                        dias_semana = var
                    Else
                        var = CInt(Mid(CHECK.Name, 9, 1))
                        var = 2 ^ (var - 1)
                        dias_semana = dias_semana + var
                    End If
                End If
                CHECK = Nothing
            End If
        Next
    End Sub
    Private Sub Checa_dias2(ByVal numero As Integer) 'Panel
        If numero >= 64 Then
            Me.CheckBox6.Checked = True
            numero = numero - 64
        End If
        If numero >= 32 Then
            Me.CheckBox5.Checked = True
            numero = numero - 32
        End If
        If numero >= 16 Then
            Me.CheckBox4.Checked = True
            numero = numero - 16
        End If
        If numero >= 8 Then
            Me.CheckBox3.Checked = True
            numero = numero - 8
        End If
        If numero >= 4 Then
            Me.CheckBox2.Checked = True
            numero = numero - 4
        End If
        If numero >= 2 Then
            Me.CheckBox1.Checked = True
            numero = numero - 2
        End If
        If numero = 1 Then
            Me.CheckBox7.Checked = True
        End If
    End Sub

  
    Private Sub TabControl1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabControl1.SelectedIndexChanged
        Dim I As Integer = Me.TabControl1.SelectedIndex

        If opcion = "C" Then
            Select Case frec_type
                Case 1
                    Me.TabControl1.SelectedIndex = 0
                Case 4
                    Me.TabControl1.SelectedIndex = 1
                Case 8
                    Me.TabControl1.SelectedIndex = 2
                Case 16
                    Me.TabControl1.SelectedIndex = 3
                Case 32
                    Me.TabControl1.SelectedIndex = 3
            End Select

        Else
            Select Case I
                Case 0
                    Me.DateTimePicker1.Visible = True
                    Me.DateTimePicker2.Visible = True
                    Me.GroupBox2.Enabled = False
                    Me.GroupBox3.Enabled = False
                    Me.Label2.Visible = True
                    Me.Label3.Visible = True
                    frec_type = 1
                Case 1
                    Me.DateTimePicker1.Visible = False
                    Me.DateTimePicker2.Visible = False
                    Me.Label2.Visible = False
                    Me.Label3.Visible = False
                    Me.GroupBox2.Enabled = True
                    Me.GroupBox3.Enabled = True
                    frec_type = 4
                Case 2

                    Me.DateTimePicker1.Visible = False
                    Me.DateTimePicker2.Visible = False
                    Me.Label2.Visible = False
                    Me.Label3.Visible = False
                    Me.GroupBox2.Enabled = True
                    Me.GroupBox3.Enabled = True
                    frec_type = 8
                Case 3
                    If Me.RadioButton1.Checked = True Then
                        Me.NumericUpDown5.Enabled = False
                        Me.ComboBox2.Enabled = False
                        Me.ComboBox3.Enabled = False
                        frec_type = 16
                    Else
                        Me.NumericUpDown4.Enabled = False
                        Me.NumericUpDown3.Enabled = False
                        frec_type = 32
                    End If
                    Me.DateTimePicker1.Visible = False
                    Me.DateTimePicker2.Visible = False
                    Me.Label2.Visible = False
                    Me.Label3.Visible = False

                    Me.GroupBox2.Enabled = True
                    Me.GroupBox3.Enabled = True


            End Select

        End If
    End Sub


End Class