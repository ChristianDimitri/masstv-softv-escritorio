﻿Public Class FrmListOrdenExt

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If cbxListado.Checked = False And cbxOrden.Checked = False Then
            Exit Sub
        End If
        If cbxListado.Checked = True Then
            Me.Close()
            FrmImprimirContrato.Show()
        End If
        If cbxOrden.Checked = True Then
            opcionarea = 9999
            Me.Close()
            FrmImprimirContrato.Show()
        End If
    End Sub
    Private Sub cbxListado_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxListado.CheckedChanged
        If cbxListado.Checked = True Then
            cbxOrden.Checked = False
        End If
    End Sub
    Private Sub cbxOrden_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxOrden.CheckedChanged
        If cbxOrden.Checked = True Then
            cbxListado.Checked = False
        End If
    End Sub
    Private Sub FrmListOrdenExt_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
    End Sub


    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub
End Class