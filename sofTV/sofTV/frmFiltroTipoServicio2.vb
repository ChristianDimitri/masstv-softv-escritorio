﻿Public Class frmFiltroTipoServicio2

    Private Sub frmFiltroTipoServicio_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        llenaComboTipoServicio()
    End Sub

#Region "Métodos"
    Private Sub llenaComboTipoServicio()
        Try
            BaseII.limpiaParametros()
            Me.cmbTipoServicio2.DataSource = BaseII.ConsultaDT("uspFiltroTipServ2")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If IsNumeric(Me.cmbTipoServicio2.SelectedValue) = True Then
            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
        Else
            MsgBox("Seleccione al menos un tipo de Servicio", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub
End Class