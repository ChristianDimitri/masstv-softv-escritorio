﻿Imports System.Data.SqlClient

Public Class FrmCortesia
    Dim bndterminar As Boolean = False
    Dim MTablas As String = "TiposCortesias"
    Dim Mvalues As String = "Clv_Cortesia,Descripcion"
    Dim MValores As String = ""
    Dim MCondicion As String = ""
    Dim MTipMov As Integer = 0
    Dim MBNDIDENTITY As Boolean = False
    Dim MClv_ID As Long = 0

    Public Sub Desplegar(ByVal Tablas As String, ByVal values As String, ByVal Valores As String, ByVal Condicion As String, ByVal TipMov As Integer, ByVal BNDIDENTITY As Boolean)
        Dim conn As New SqlConnection(MiConexion)
        Try

            'Dim con As New SqlConnection(MiConexion)
            'Try
            '    DimeTipSer_CualEsPrincipal = 0

            '    Dim reader As SqlDataReader
            '    Dim cmd As New SqlCommand("DimeTipSer_CualEsPrincipal", con)
            '    cmd.CommandType = CommandType.StoredProcedure
            '    con.Open()
            '    reader = cmd.ExecuteReader()
            '    Using reader
            '        While reader.Read
            '            DimeTipSer_CualEsPrincipal = reader.GetValue(0)
            '        End While
            '    End Using

            '    con.Close()
            'Catch ex As Exception
            '    DimeTipSer_CualEsPrincipal = 1
            '    MsgBox(ex.Message)
            '    con.Close()
            'End Try


            'conn.Open()
            'Dim comando As New SqlClient.SqlCommand("UP_PROCESA_TODO", conn)
            'comando.CommandType = CommandType.StoredProcedure
            'comando.CommandTimeout = 0
            'Dim Adaptador As New SqlDataReader
            'Adaptador.SelectCommand = comando

            Dim reader As SqlDataReader
            Dim cmd As New SqlCommand("UP_PROCESA_TODO", conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@Tablas", SqlDbType.VarChar, 150).Value = Tablas
            cmd.Parameters.Add("@values", SqlDbType.VarChar, 250).Value = values
            cmd.Parameters.Add("@Valores", SqlDbType.VarChar, 250).Value = Valores
            cmd.Parameters.Add("@Condicion", SqlDbType.VarChar, 250).Value = Condicion
            cmd.Parameters.Add("@TipMov", SqlDbType.Int).Value = TipMov
            cmd.Parameters.Add("@BNDIDENTITY", SqlDbType.Bit).Value = BNDIDENTITY
            cmd.Parameters.Add("@CLV_ID", SqlDbType.BigInt).Value = 0
            conn.Open()
            reader = cmd.ExecuteReader()

            Using reader
                While reader.Read
                    Me.Id_Tel.Text = reader.GetValue(0)
                    Me.Clave.Text = reader.GetValue(0)
                    Me.Descripcion.Text = reader.GetValue(1)
                End While
            End Using
            conn.Close()
        Catch ex As Exception
            If conn.State <> ConnectionState.Closed Then conn.Close()
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Sub INSERCION(ByVal Tablas As String, ByVal values As String, ByVal Valores As String, ByVal Condicion As String, ByVal TipMov As Integer, ByVal BNDIDENTITY As Boolean)
        Dim conn As New SqlConnection(MiConexion)
        Try
            bndterminar = False
            'Dim con As New SqlConnection(MiConexion)
            'Try
            '    DimeTipSer_CualEsPrincipal = 0

            '    Dim reader As SqlDataReader
            '    Dim cmd As New SqlCommand("DimeTipSer_CualEsPrincipal", con)
            '    cmd.CommandType = CommandType.StoredProcedure
            '    con.Open()
            '    reader = cmd.ExecuteReader()
            '    Using reader
            '        While reader.Read
            '            DimeTipSer_CualEsPrincipal = reader.GetValue(0)
            '        End While
            '    End Using

            '    con.Close()
            'Catch ex As Exception
            '    DimeTipSer_CualEsPrincipal = 1
            '    MsgBox(ex.Message)
            '    con.Close()
            'End Try


            'conn.Open()
            'Dim comando As New SqlClient.SqlCommand("UP_PROCESA_TODO", conn)
            'comando.CommandType = CommandType.StoredProcedure
            'comando.CommandTimeout = 0
            'Dim Adaptador As New SqlDataReader
            'Adaptador.SelectCommand = comando


            Dim cmd As New SqlCommand("UP_PROCESA_TODO", conn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim prm1 As New SqlParameter("@Tablas", SqlDbType.VarChar, 150)
            prm1.Direction = ParameterDirection.Input
            prm1.Value = Tablas
            cmd.Parameters.Add(prm1)

            Dim prm2 As New SqlParameter("@values", SqlDbType.VarChar, 250)
            prm2.Direction = ParameterDirection.Input
            prm2.Value = values
            cmd.Parameters.Add(prm2)

            Dim prm3 As New SqlParameter("@Valores", SqlDbType.VarChar, 250)
            prm3.Direction = ParameterDirection.Input
            prm3.Value = Valores
            cmd.Parameters.Add(prm3)

            Dim prm4 As New SqlParameter("@Condicion", SqlDbType.VarChar, 250)
            prm4.Direction = ParameterDirection.Input
            prm4.Value = Condicion
            cmd.Parameters.Add(prm4)

            Dim prm5 As New SqlParameter("@TipMov", SqlDbType.Int)
            prm5.Direction = ParameterDirection.Input
            prm5.Value = TipMov
            cmd.Parameters.Add(prm5)

            Dim prm6 As New SqlParameter("@BNDIDENTITY", SqlDbType.Bit)
            prm6.Direction = ParameterDirection.Input
            If BNDIDENTITY = True Then
                prm6.Value = 1
            Else
                prm6.Value = 0
            End If
            cmd.Parameters.Add(prm6)

            Dim prm7 As New SqlParameter("@CLV_ID", SqlDbType.BigInt)
            prm7.Direction = ParameterDirection.Output
            prm7.Value = 0
            cmd.Parameters.Add(prm7)


            'cmd.Parameters.Add("@Tablas", SqlDbType.VarChar, 150).Value = Tablas
            'cmd.Parameters.Add("@values", SqlDbType.VarChar, 250).Value = values
            'cmd.Parameters.Add("@Valores", SqlDbType.VarChar, 250).Value = Valores
            'cmd.Parameters.Add("@Condicion", SqlDbType.VarChar, 250).Value = Condicion
            'cmd.Parameters.Add("@TipMov", SqlDbType.Int).Value = TipMov
            'cmd.Parameters.Add("@BNDIDENTITY", SqlDbType.Bit).Value = BNDIDENTITY
            'cmd.Parameters.Add("@CLV_ID", SqlDbType.BigInt).Value = 0
            conn.Open()
            Dim i As Integer = cmd.ExecuteNonQuery
            If MBNDIDENTITY = True Then
                Id_Tel.Text = prm7.Value
            End If

            'Using reader
            '    While reader.Read
            '        Me.Id_Tel.Text = reader.GetValue(0)
            '        Me.Clave.Text = reader.GetValue(1)
            '        Me.Descripcion.Text = reader.GetValue(2)
            '    End While
            'End Using
            conn.Close()
            If OpcionClasificacionLlamadas = "M" Or OpcionClasificacionLlamadas = "N" Then
                bec_bnd = True
                Me.Close()
            End If
        Catch ex As Exception
            bndterminar = True
            If conn.State <> ConnectionState.Closed Then conn.Close()
            MsgBox(ex.Message)

        End Try
    End Sub

    Private Sub BLOQUEA(ByVal BND As Boolean)
        'Me.Clave.Enabled = BND
        Me.Descripcion.Enabled = BND
    End Sub

    Private Sub FrmPaquetesGallery_Tel_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

    End Sub


    Private Sub FrmTipoCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.Label4.ForeColor = Color.Black
        If OpcionClasificacionLlamadas = "C" Then
            MTipMov = 0
            MCondicion = "Clv_Cortesia = " & ID_ClasificacionLlamadas
            Desplegar(MTablas, Mvalues, MValores, MCondicion, MTipMov, MBNDIDENTITY)
            btnGuardar.Enabled = False
            BLOQUEA(False)
        ElseIf OpcionClasificacionLlamadas = "N" Then
            Id_Tel.Text = 0
            Clave.Text = ""
            Descripcion.Text = ""
            BLOQUEA(True)
            btnGuardar.Enabled = True
        ElseIf OpcionClasificacionLlamadas = "M" Then
            MTipMov = 0
            MCondicion = "Clv_Cortesia = " & ID_ClasificacionLlamadas
            Desplegar(MTablas, Mvalues, MValores, MCondicion, MTipMov, MBNDIDENTITY)
            BLOQUEA(True)
            btnGuardar.Enabled = True
        End If

    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Try



            If Len(Me.Descripcion.Text) = 0 Then
                MsgBox("Se Requiere la Descripción", MsgBoxStyle.Information)
                Exit Sub
            End If
            If OpcionClasificacionLlamadas = "N" Then
                MTipMov = 1
                Mvalues = "Descripcion"
                MValores = "$" & Me.Descripcion.Text & "$"
                MBNDIDENTITY = True
                INSERCION(MTablas, Mvalues, MValores, MCondicion, MTipMov, MBNDIDENTITY)
            ElseIf OpcionClasificacionLlamadas = "M" Then
                MTipMov = 2
                MCondicion = "Clv_Cortesia= " & Me.Id_Tel.Text
                Mvalues = "Descripcion"
                MValores = "$" & Me.Descripcion.Text & "$"
                MBNDIDENTITY = False
                INSERCION(MTablas, Mvalues, MValores, MCondicion, MTipMov, MBNDIDENTITY)
            End If


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub


End Class