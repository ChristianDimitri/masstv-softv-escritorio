﻿Imports System.Data.SqlClient
Public Class DASectores

    'Regresamos los 
    Public Shared Function CargaSectores() As DataTable

        Dim conn As New SqlConnection(MiConexion)
        Dim dt As New DataTable
        Try

            conn.Open()

            Dim comando As New SqlClient.SqlCommand("MuestraSectorPenetracion", conn)
            comando.CommandType = CommandType.StoredProcedure

            Dim adaptador As New SqlDataAdapter
            adaptador.SelectCommand = comando
            adaptador.Fill(dt)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conn.Close()
            conn.Dispose()
        End Try

        Return dt
    End Function

End Class
