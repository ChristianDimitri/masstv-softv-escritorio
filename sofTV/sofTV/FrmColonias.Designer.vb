<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmColonias
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_ColoniaLabel As System.Windows.Forms.Label
        Dim NombreLabel As System.Windows.Forms.Label
        Dim Clv_ServicioTipoLabel As System.Windows.Forms.Label
        Dim CPLabel As System.Windows.Forms.Label
        Dim Clv_TipoLabel As System.Windows.Forms.Label
        Dim FechaEntregaLabel As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim NOMBRELabel1 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmColonias))
        Me.Clv_ColoniaTextBox = New System.Windows.Forms.TextBox()
        Me.ConColoniasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.NombreTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_ServicioTipoTextBox = New System.Windows.Forms.TextBox()
        Me.CPTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_TipoTextBox = New System.Windows.Forms.TextBox()
        Me.FechaEntregaTextBox = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Tipo_ColoniasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.MuestraTiposServicioTvBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConColoniasBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.ConColoniasBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.ConColoniasTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.ConColoniasTableAdapter()
        Me.Tipo_ColoniasTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.Tipo_ColoniasTableAdapter()
        Me.MuestraTiposServicioTvTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MuestraTiposServicioTvTableAdapter()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.ComboBox3 = New System.Windows.Forms.ComboBox()
        Me.MuestraCiudadesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.TreeView2 = New System.Windows.Forms.TreeView()
        Me.ComboBox4 = New System.Windows.Forms.ComboBox()
        Me.MuestraTipSerPrincipalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.MuestraTipSerPrincipalTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MuestraTipSerPrincipalTableAdapter()
        Me.MuestraCiudadesTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MuestraCiudadesTableAdapter()
        Me.CONCVECOLCIUBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONCVECOLCIUTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONCVECOLCIUTableAdapter()
        Me.CONRelColoniasSerBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONRelColoniasSerTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONRelColoniasSerTableAdapter()
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2()
        Me.Inserta_Rel_Colonia_ServicioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Rel_Colonia_ServicioTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_Rel_Colonia_ServicioTableAdapter()
        Me.Consulta_Rel_Colonia_ServicioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Consulta_Rel_Colonia_ServicioTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Consulta_Rel_Colonia_ServicioTableAdapter()
        Clv_ColoniaLabel = New System.Windows.Forms.Label()
        NombreLabel = New System.Windows.Forms.Label()
        Clv_ServicioTipoLabel = New System.Windows.Forms.Label()
        CPLabel = New System.Windows.Forms.Label()
        Clv_TipoLabel = New System.Windows.Forms.Label()
        FechaEntregaLabel = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        NOMBRELabel1 = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        Label4 = New System.Windows.Forms.Label()
        CType(Me.ConColoniasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.Tipo_ColoniasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTiposServicioTvBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConColoniasBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ConColoniasBindingNavigator.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.MuestraCiudadesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.MuestraTipSerPrincipalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONCVECOLCIUBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONRelColoniasSerBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Rel_Colonia_ServicioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_Rel_Colonia_ServicioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Clv_ColoniaLabel
        '
        Clv_ColoniaLabel.AutoSize = True
        Clv_ColoniaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_ColoniaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_ColoniaLabel.Location = New System.Drawing.Point(151, 52)
        Clv_ColoniaLabel.Name = "Clv_ColoniaLabel"
        Clv_ColoniaLabel.Size = New System.Drawing.Size(50, 15)
        Clv_ColoniaLabel.TabIndex = 2
        Clv_ColoniaLabel.Text = "Clave :"
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.ForeColor = System.Drawing.Color.LightSlateGray
        NombreLabel.Location = New System.Drawing.Point(135, 77)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(66, 15)
        NombreLabel.TabIndex = 4
        NombreLabel.Text = "Nombre :"
        '
        'Clv_ServicioTipoLabel
        '
        Clv_ServicioTipoLabel.AutoSize = True
        Clv_ServicioTipoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_ServicioTipoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_ServicioTipoLabel.Location = New System.Drawing.Point(404, 211)
        Clv_ServicioTipoLabel.Name = "Clv_ServicioTipoLabel"
        Clv_ServicioTipoLabel.Size = New System.Drawing.Size(144, 15)
        Clv_ServicioTipoLabel.TabIndex = 6
        Clv_ServicioTipoLabel.Text = "Tipo Servicio de Tv.  :"
        Clv_ServicioTipoLabel.Visible = False
        '
        'CPLabel
        '
        CPLabel.AutoSize = True
        CPLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CPLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CPLabel.Location = New System.Drawing.Point(97, 106)
        CPLabel.Name = "CPLabel"
        CPLabel.Size = New System.Drawing.Size(104, 15)
        CPLabel.TabIndex = 8
        CPLabel.Text = "Codigo Postal :"
        '
        'Clv_TipoLabel
        '
        Clv_TipoLabel.AutoSize = True
        Clv_TipoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_TipoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_TipoLabel.Location = New System.Drawing.Point(85, 139)
        Clv_TipoLabel.Name = "Clv_TipoLabel"
        Clv_TipoLabel.Size = New System.Drawing.Size(116, 15)
        Clv_TipoLabel.TabIndex = 10
        Clv_TipoLabel.Text = "Tipo de Colonia :"
        '
        'FechaEntregaLabel
        '
        FechaEntregaLabel.AutoSize = True
        FechaEntregaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FechaEntregaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        FechaEntregaLabel.Location = New System.Drawing.Point(97, 164)
        FechaEntregaLabel.Name = "FechaEntregaLabel"
        FechaEntregaLabel.Size = New System.Drawing.Size(104, 15)
        FechaEntregaLabel.TabIndex = 12
        FechaEntregaLabel.Text = "Fecha Entrega:"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(3, 95)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(259, 15)
        Label2.TabIndex = 6
        Label2.Text = "Ciudades a las que pertenece Colonia :"
        '
        'NOMBRELabel1
        '
        NOMBRELabel1.AutoSize = True
        NOMBRELabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NOMBRELabel1.ForeColor = System.Drawing.Color.LightSlateGray
        NOMBRELabel1.Location = New System.Drawing.Point(13, 20)
        NOMBRELabel1.Name = "NOMBRELabel1"
        NOMBRELabel1.Size = New System.Drawing.Size(163, 15)
        NOMBRELabel1.TabIndex = 4
        NOMBRELabel1.Text = "Seleccione una Ciudad :"
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Label3.Location = New System.Drawing.Point(3, 95)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(272, 15)
        Label3.TabIndex = 6
        Label3.Text = "Tipos de Servicio que hay en la Colonia  :"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Label4.Location = New System.Drawing.Point(15, 20)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(193, 15)
        Label4.TabIndex = 4
        Label4.Text = "Seleccione un Tipo Servicio :"
        '
        'Clv_ColoniaTextBox
        '
        Me.Clv_ColoniaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_ColoniaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConColoniasBindingSource, "Clv_Colonia", True))
        Me.Clv_ColoniaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_ColoniaTextBox.Location = New System.Drawing.Point(207, 50)
        Me.Clv_ColoniaTextBox.Name = "Clv_ColoniaTextBox"
        Me.Clv_ColoniaTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Clv_ColoniaTextBox.TabIndex = 3
        Me.Clv_ColoniaTextBox.TabStop = False
        '
        'ConColoniasBindingSource
        '
        Me.ConColoniasBindingSource.DataMember = "ConColonias"
        Me.ConColoniasBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'NombreTextBox
        '
        Me.NombreTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NombreTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.NombreTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConColoniasBindingSource, "Nombre", True))
        Me.NombreTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreTextBox.Location = New System.Drawing.Point(207, 77)
        Me.NombreTextBox.Name = "NombreTextBox"
        Me.NombreTextBox.Size = New System.Drawing.Size(375, 21)
        Me.NombreTextBox.TabIndex = 0
        '
        'Clv_ServicioTipoTextBox
        '
        Me.Clv_ServicioTipoTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_ServicioTipoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_ServicioTipoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConColoniasBindingSource, "Clv_ServicioTipo", True))
        Me.Clv_ServicioTipoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_ServicioTipoTextBox.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_ServicioTipoTextBox.Location = New System.Drawing.Point(340, 109)
        Me.Clv_ServicioTipoTextBox.Name = "Clv_ServicioTipoTextBox"
        Me.Clv_ServicioTipoTextBox.Size = New System.Drawing.Size(22, 13)
        Me.Clv_ServicioTipoTextBox.TabIndex = 7
        Me.Clv_ServicioTipoTextBox.TabStop = False
        '
        'CPTextBox
        '
        Me.CPTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CPTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConColoniasBindingSource, "CP", True))
        Me.CPTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CPTextBox.Location = New System.Drawing.Point(207, 104)
        Me.CPTextBox.Name = "CPTextBox"
        Me.CPTextBox.Size = New System.Drawing.Size(100, 21)
        Me.CPTextBox.TabIndex = 1
        '
        'Clv_TipoTextBox
        '
        Me.Clv_TipoTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TipoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_TipoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConColoniasBindingSource, "Clv_Tipo", True))
        Me.Clv_TipoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_TipoTextBox.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TipoTextBox.Location = New System.Drawing.Point(334, 164)
        Me.Clv_TipoTextBox.Name = "Clv_TipoTextBox"
        Me.Clv_TipoTextBox.Size = New System.Drawing.Size(55, 14)
        Me.Clv_TipoTextBox.TabIndex = 11
        Me.Clv_TipoTextBox.TabStop = False
        '
        'FechaEntregaTextBox
        '
        Me.FechaEntregaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FechaEntregaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConColoniasBindingSource, "FechaEntrega", True))
        Me.FechaEntregaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FechaEntregaTextBox.Location = New System.Drawing.Point(207, 162)
        Me.FechaEntregaTextBox.Name = "FechaEntregaTextBox"
        Me.FechaEntregaTextBox.Size = New System.Drawing.Size(84, 21)
        Me.FechaEntregaTextBox.TabIndex = 3
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.CheckBox1)
        Me.Panel1.Controls.Add(Me.ComboBox1)
        Me.Panel1.Controls.Add(Me.ComboBox2)
        Me.Panel1.Controls.Add(Me.ConColoniasBindingNavigator)
        Me.Panel1.Controls.Add(Me.Clv_ColoniaTextBox)
        Me.Panel1.Controls.Add(Clv_ColoniaLabel)
        Me.Panel1.Controls.Add(Me.FechaEntregaTextBox)
        Me.Panel1.Controls.Add(FechaEntregaLabel)
        Me.Panel1.Controls.Add(NombreLabel)
        Me.Panel1.Controls.Add(Me.Clv_TipoTextBox)
        Me.Panel1.Controls.Add(Me.NombreTextBox)
        Me.Panel1.Controls.Add(Clv_TipoLabel)
        Me.Panel1.Controls.Add(Clv_ServicioTipoLabel)
        Me.Panel1.Controls.Add(Me.CPTextBox)
        Me.Panel1.Controls.Add(Me.Clv_ServicioTipoTextBox)
        Me.Panel1.Controls.Add(CPLabel)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1016, 384)
        Me.Panel1.TabIndex = 0
        Me.Panel1.TabStop = True
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.CheckBox1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CheckBox1.Location = New System.Drawing.Point(523, 133)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(117, 19)
        Me.CheckBox1.TabIndex = 17
        Me.CheckBox1.Text = "Tiene Servicio"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'ComboBox1
        '
        Me.ComboBox1.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.ConColoniasBindingSource, "Clv_Tipo", True))
        Me.ComboBox1.DataSource = Me.Tipo_ColoniasBindingSource
        Me.ComboBox1.DisplayMember = "Concepto"
        Me.ComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(207, 133)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(197, 23)
        Me.ComboBox1.TabIndex = 2
        Me.ComboBox1.ValueMember = "clave"
        '
        'Tipo_ColoniasBindingSource
        '
        Me.Tipo_ColoniasBindingSource.DataMember = "Tipo_Colonias"
        Me.Tipo_ColoniasBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'ComboBox2
        '
        Me.ComboBox2.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.ConColoniasBindingSource, "Clv_ServicioTipo", True))
        Me.ComboBox2.DataSource = Me.MuestraTiposServicioTvBindingSource
        Me.ComboBox2.DisplayMember = "TipSerTv"
        Me.ComboBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(554, 208)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(182, 23)
        Me.ComboBox2.TabIndex = 16
        Me.ComboBox2.TabStop = False
        Me.ComboBox2.ValueMember = "Clv_TipSerTv"
        Me.ComboBox2.Visible = False
        '
        'MuestraTiposServicioTvBindingSource
        '
        Me.MuestraTiposServicioTvBindingSource.DataMember = "MuestraTiposServicioTv"
        Me.MuestraTiposServicioTvBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'ConColoniasBindingNavigator
        '
        Me.ConColoniasBindingNavigator.AddNewItem = Nothing
        Me.ConColoniasBindingNavigator.BindingSource = Me.ConColoniasBindingSource
        Me.ConColoniasBindingNavigator.CountItem = Nothing
        Me.ConColoniasBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.ConColoniasBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripLabel1, Me.BindingNavigatorDeleteItem, Me.ConColoniasBindingNavigatorSaveItem})
        Me.ConColoniasBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.ConColoniasBindingNavigator.MoveFirstItem = Nothing
        Me.ConColoniasBindingNavigator.MoveLastItem = Nothing
        Me.ConColoniasBindingNavigator.MoveNextItem = Nothing
        Me.ConColoniasBindingNavigator.MovePreviousItem = Nothing
        Me.ConColoniasBindingNavigator.Name = "ConColoniasBindingNavigator"
        Me.ConColoniasBindingNavigator.PositionItem = Nothing
        Me.ConColoniasBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ConColoniasBindingNavigator.Size = New System.Drawing.Size(1016, 25)
        Me.ConColoniasBindingNavigator.TabIndex = 10
        Me.ConColoniasBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(88, 22)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        Me.BindingNavigatorDeleteItem.Visible = False
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(72, 22)
        Me.ToolStripLabel1.Text = "&CANCELAR"
        '
        'ConColoniasBindingNavigatorSaveItem
        '
        Me.ConColoniasBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConColoniasBindingNavigatorSaveItem.Image = CType(resources.GetObject("ConColoniasBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.ConColoniasBindingNavigatorSaveItem.Name = "ConColoniasBindingNavigatorSaveItem"
        Me.ConColoniasBindingNavigatorSaveItem.Size = New System.Drawing.Size(88, 22)
        Me.ConColoniasBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(868, 662)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 11
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'ConColoniasTableAdapter
        '
        Me.ConColoniasTableAdapter.ClearBeforeFill = True
        '
        'Tipo_ColoniasTableAdapter
        '
        Me.Tipo_ColoniasTableAdapter.ClearBeforeFill = True
        '
        'MuestraTiposServicioTvTableAdapter
        '
        Me.MuestraTiposServicioTvTableAdapter.ClearBeforeFill = True
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.TreeView1)
        Me.Panel2.Controls.Add(Me.ComboBox3)
        Me.Panel2.Controls.Add(Label2)
        Me.Panel2.Controls.Add(NOMBRELabel1)
        Me.Panel2.Controls.Add(Me.Button2)
        Me.Panel2.Controls.Add(Me.Button1)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Enabled = False
        Me.Panel2.Location = New System.Drawing.Point(12, 292)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(656, 333)
        Me.Panel2.TabIndex = 4
        Me.Panel2.TabStop = True
        '
        'TreeView1
        '
        Me.TreeView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView1.ForeColor = System.Drawing.Color.Black
        Me.TreeView1.Location = New System.Drawing.Point(6, 113)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(356, 200)
        Me.TreeView1.TabIndex = 8
        Me.TreeView1.TabStop = False
        '
        'ComboBox3
        '
        Me.ComboBox3.DataSource = Me.MuestraCiudadesBindingSource
        Me.ComboBox3.DisplayMember = "NOMBRE"
        Me.ComboBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox3.FormattingEnabled = True
        Me.ComboBox3.Location = New System.Drawing.Point(22, 38)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(320, 23)
        Me.ComboBox3.TabIndex = 4
        Me.ComboBox3.ValueMember = "Clv_Ciudad"
        '
        'MuestraCiudadesBindingSource
        '
        Me.MuestraCiudadesBindingSource.DataMember = "MuestraCiudades"
        Me.MuestraCiudadesBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkRed
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(378, 149)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(96, 30)
        Me.Button2.TabIndex = 6
        Me.Button2.Text = "Quitar"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkRed
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(378, 113)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(96, 30)
        Me.Button1.TabIndex = 5
        Me.Button1.Text = "Agregar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.OrangeRed
        Me.Label1.Location = New System.Drawing.Point(3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(339, 20)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Relacción de la Colonia con las Ciudades"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.TreeView2)
        Me.Panel3.Controls.Add(Me.ComboBox4)
        Me.Panel3.Controls.Add(Label3)
        Me.Panel3.Controls.Add(Label4)
        Me.Panel3.Controls.Add(Me.Button3)
        Me.Panel3.Controls.Add(Me.Button4)
        Me.Panel3.Controls.Add(Me.Label5)
        Me.Panel3.Enabled = False
        Me.Panel3.Location = New System.Drawing.Point(517, 292)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(487, 329)
        Me.Panel3.TabIndex = 7
        Me.Panel3.TabStop = True
        '
        'TreeView2
        '
        Me.TreeView2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView2.Location = New System.Drawing.Point(8, 115)
        Me.TreeView2.Name = "TreeView2"
        Me.TreeView2.Size = New System.Drawing.Size(356, 200)
        Me.TreeView2.TabIndex = 8
        Me.TreeView2.TabStop = False
        '
        'ComboBox4
        '
        Me.ComboBox4.DataSource = Me.MuestraTipSerPrincipalBindingSource
        Me.ComboBox4.DisplayMember = "Concepto"
        Me.ComboBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox4.FormattingEnabled = True
        Me.ComboBox4.Location = New System.Drawing.Point(18, 38)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(320, 23)
        Me.ComboBox4.TabIndex = 7
        Me.ComboBox4.ValueMember = "Clv_TipSerPrincipal"
        '
        'MuestraTipSerPrincipalBindingSource
        '
        Me.MuestraTipSerPrincipalBindingSource.DataMember = "MuestraTipSerPrincipal"
        Me.MuestraTipSerPrincipalBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkRed
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.Location = New System.Drawing.Point(376, 149)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(96, 30)
        Me.Button3.TabIndex = 9
        Me.Button3.Text = "Quitar"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkRed
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.White
        Me.Button4.Location = New System.Drawing.Point(376, 113)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(96, 30)
        Me.Button4.TabIndex = 8
        Me.Button4.Text = "Agregar"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.OrangeRed
        Me.Label5.Location = New System.Drawing.Point(3, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(400, 20)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Relacción de la Colonia con los Tipos de Servicio"
        '
        'MuestraTipSerPrincipalTableAdapter
        '
        Me.MuestraTipSerPrincipalTableAdapter.ClearBeforeFill = True
        '
        'MuestraCiudadesTableAdapter
        '
        Me.MuestraCiudadesTableAdapter.ClearBeforeFill = True
        '
        'CONCVECOLCIUBindingSource
        '
        Me.CONCVECOLCIUBindingSource.DataMember = "CONCVECOLCIU"
        Me.CONCVECOLCIUBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CONCVECOLCIUTableAdapter
        '
        Me.CONCVECOLCIUTableAdapter.ClearBeforeFill = True
        '
        'CONRelColoniasSerBindingSource
        '
        Me.CONRelColoniasSerBindingSource.DataMember = "CONRelColoniasSer"
        Me.CONRelColoniasSerBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CONRelColoniasSerTableAdapter
        '
        Me.CONRelColoniasSerTableAdapter.ClearBeforeFill = True
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Inserta_Rel_Colonia_ServicioBindingSource
        '
        Me.Inserta_Rel_Colonia_ServicioBindingSource.DataMember = "Inserta_Rel_Colonia_Servicio"
        Me.Inserta_Rel_Colonia_ServicioBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Inserta_Rel_Colonia_ServicioTableAdapter
        '
        Me.Inserta_Rel_Colonia_ServicioTableAdapter.ClearBeforeFill = True
        '
        'Consulta_Rel_Colonia_ServicioBindingSource
        '
        Me.Consulta_Rel_Colonia_ServicioBindingSource.DataMember = "Consulta_Rel_Colonia_Servicio"
        Me.Consulta_Rel_Colonia_ServicioBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Consulta_Rel_Colonia_ServicioTableAdapter
        '
        Me.Consulta_Rel_Colonia_ServicioTableAdapter.ClearBeforeFill = True
        '
        'FrmColonias
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1028, 753)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "FrmColonias"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Datos de la Colonia"
        CType(Me.ConColoniasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.Tipo_ColoniasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTiposServicioTvBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConColoniasBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ConColoniasBindingNavigator.ResumeLayout(False)
        Me.ConColoniasBindingNavigator.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.MuestraCiudadesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.MuestraTipSerPrincipalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONCVECOLCIUBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONRelColoniasSerBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Rel_Colonia_ServicioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_Rel_Colonia_ServicioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents ConColoniasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConColoniasTableAdapter As sofTV.NewSofTvDataSetTableAdapters.ConColoniasTableAdapter
    Friend WithEvents Clv_ColoniaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NombreTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_ServicioTipoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CPTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_TipoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FechaEntregaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents ConColoniasBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripLabel1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ConColoniasBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Tipo_ColoniasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Tipo_ColoniasTableAdapter As sofTV.NewSofTvDataSetTableAdapters.Tipo_ColoniasTableAdapter
    Friend WithEvents MuestraTiposServicioTvBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTiposServicioTvTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MuestraTiposServicioTvTableAdapter
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents TreeView2 As System.Windows.Forms.TreeView
    Friend WithEvents ComboBox4 As System.Windows.Forms.ComboBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents MuestraTipSerPrincipalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipSerPrincipalTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MuestraTipSerPrincipalTableAdapter
    Friend WithEvents MuestraCiudadesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraCiudadesTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MuestraCiudadesTableAdapter
    Friend WithEvents CONCVECOLCIUBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONCVECOLCIUTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONCVECOLCIUTableAdapter
    Friend WithEvents CONRelColoniasSerBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONRelColoniasSerTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONRelColoniasSerTableAdapter
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents Inserta_Rel_Colonia_ServicioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Rel_Colonia_ServicioTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_Rel_Colonia_ServicioTableAdapter
    Friend WithEvents Consulta_Rel_Colonia_ServicioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_Rel_Colonia_ServicioTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Consulta_Rel_Colonia_ServicioTableAdapter
End Class
