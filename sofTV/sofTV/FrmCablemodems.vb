﻿Imports System.Data.SqlClient
Public Class FrmCablemodems
    Dim statuscablm As String = Nothing
    Dim CLV_CABLEMODEM As Integer

    Private Sub damedatosbitacora()
        Try
            If opcion = "M" Then
                statuscablm = Me.ComboBox2.Text
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub guardabitacora()
        Try
            If opcion = "M" Then
                'statuscablm = Me.ComboBox2.Text
                bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "", statuscablm, Me.ComboBox2.Text, LocClv_Ciudad)
            End If
        Catch ex As Exception

        End Try
    End Sub


    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub



    Private Sub BUSCA(ByVal CLV_CABLEMODEM As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.CONAPARATOSTableAdapter.Connection = CON
            Me.CONAPARATOSTableAdapter.Fill(Me.NewSofTvDataSet.CONAPARATOS, New System.Nullable(Of Integer)(CType(CLV_CABLEMODEM, Integer)))

            If Me.TipoAparatoTextBox.Text = "C" Then
                Me.ConTecnicoCatCableModemTableAdapter.Connection = CON
                Me.ConTecnicoCatCableModemTableAdapter.Fill(Me.DataSetEric.ConTecnicoCatCableModem, Me.mac.Text)
            End If

            If Me.TipoAparatoTextBox.Text = "D" Or Me.TipoAparatoTextBox.Text = "E" Then
                Me.ConTecnicoCatCableModemTableAdapter.Connection = CON
                Me.ConTecnicoCatCableModemTableAdapter.Fill(Me.DataSetEric.ConTecnicoCatCableModem, Me.MaskedTextBox1.Text)
            End If
            damedatosbitacora()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub CONAPARATOSBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONAPARATOSBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        Dim eRes As Integer = 0
        Dim eMsg As String = Nothing
        CON.Open()
        'If GloTipoAparato = "C" Then
        '    If Me.mac.Text.Length = 14 Then
        '        Try
        '            'Me.Validate()
        '            'Me.CONAPARATOSBindingSource.EndEdit()
        '            'Me.CONAPARATOSTableAdapter.Connection = CON
        '            'Me.CONAPARATOSTableAdapter.Update(Me.NewSofTvDataSet.CONAPARATOS)
        '            MsgBox(mensaje5)
        '            GloBnd = True
        '            Me.Close()
        '        Catch ex As System.Exception
        '            MsgBox("La Mac de Cablemodem ya exíste ", MsgBoxStyle.Information)

        '        End Try
        '    Else
        '        MsgBox("Captura los 12 Dígitos de la MAC", , "Atención")
        '    End If
        'Else

        '    Try
        '        'Me.Validate()
        '        'Me.CONAPARATOSBindingSource.EndEdit()
        '        'Me.CONAPARATOSTableAdapter.Connection = CON
        '        'Me.CONAPARATOSTableAdapter.Update(Me.NewSofTvDataSet.CONAPARATOS)
        '        MsgBox(mensaje5)
        '        GloBnd = True
        '        Me.Close()
        '    Catch ex As System.Exception
        '        MsgBox("La Mac de Cablemodem ya exíste ", MsgBoxStyle.Information)

        '    End Try
        'End If

        eRes = 0
        eMsg = ""

        If Me.TipoAparatoTextBox.Text = "C" Then
            Me.ModCatCableModemTableAdapter.Connection = CON
            Me.ModCatCableModemTableAdapter.Fill(Me.DataSetEric.ModCatCableModem, Me.mac.Text, Me.ComboBox2.SelectedValue, eRes, eMsg)
        End If

        If Me.TipoAparatoTextBox.Text = "D" Or Me.TipoAparatoTextBox.Text = "E" Then
            Me.ModCatCableModemTableAdapter.Connection = CON
            Me.ModCatCableModemTableAdapter.Fill(Me.DataSetEric.ModCatCableModem, Me.MaskedTextBox1.Text, Me.ComboBox2.SelectedValue, eRes, eMsg)
        End If

        If eRes > 0 Then
            MsgBox(eMsg)
        End If
        guardabitacora()

        CON.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.CONAPARATOSTableAdapter.Connection = CON
        Me.CONAPARATOSTableAdapter.Delete(GloGClv_Cablemodem)
        GloBnd = True
        Me.Close()
    End Sub
    Private Function DAMECONTRATOS_CABLEMODEM(ByVal CLV_CABLEMODEM As Integer) As DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_CABLEMODEM", SqlDbType.Int, CInt(CLV_CABLEMODEM))
        Return BaseII.ConsultaDT("DAMECONTRATOS_CABLEMODEM")
    End Function

    Public Sub CREAARBOL()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim DTable As New DataTable
            'Me.DAMECONTRATOS_CABLEMODEMTableAdapter.Connection = CON
            'Me.DAMECONTRATOS_CABLEMODEMTableAdapter.Fill(Me.NewSofTvDataSet.DAMECONTRATOS_CABLEMODEM, New System.Nullable(Of Integer)(CType(Me.Clv_CableModemTextBox.Text, Integer)))
            CLV_CABLEMODEM = Me.Clv_CableModemTextBox.Text
            DTable = DAMECONTRATOS_CABLEMODEM(CLV_CABLEMODEM)
            Dim FilaRow As DataRow
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In DTable.Rows
                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0

                Me.TreeView1.Nodes.Add(Trim(FilaRow("CONTRATO").ToString()), Trim(FilaRow("CONTRATO").ToString()))
                Me.TreeView1.Nodes(I).Tag = Trim(FilaRow("CONTRATO").ToString())
                I += 1

            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub


    Private Sub FrmCablemodems_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.TiposAparatos' Puede moverla o quitarla según sea necesario.
        Me.TiposAparatosTableAdapter.Connection = CON
        Me.TiposAparatosTableAdapter.Fill(Me.NewSofTvDataSet.TiposAparatos)
        'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.TipoCablemodem' Puede moverla o quitarla según sea necesario.

        'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.StatusAparatos' Puede moverla o quitarla según sea necesario.
        Me.StatusAparatosTableAdapter.Connection = CON
        Me.StatusAparatosTableAdapter.Fill(Me.NewSofTvDataSet.StatusAparatos)

        Me.Panel1.Enabled = True
        If opcion = "N" Then
            Me.CONAPARATOSBindingSource.AddNew()
            Me.ComboBox2.FindString("Disponible")
            Me.ComboBox2.Text = "Disponible"
            Me.ComboBox2.SelectedValue = "D"
            Me.EstadoAparatoTextBox.Text = "D"
        ElseIf opcion = "C" Then
            Me.BUSCA(GloGClv_Cablemodem)
            Me.Panel1.Enabled = False
            Me.CREAARBOL()
        ElseIf opcion = "M" Then
            Me.BUSCA(GloGClv_Cablemodem)
            Me.CREAARBOL()
        End If
        Me.TipoAparatoTextBox.Text = GloTipoAparato
        CON.Close()
    End Sub


    Private Sub Clv_CableModemTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_CableModemTextBox.TextChanged
        GloGClv_Cablemodem = Me.Clv_CableModemTextBox.Text
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        Me.EstadoAparatoTextBox.Text = Me.ComboBox2.SelectedValue
    End Sub

    Private Sub TipoAparatoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TipoAparatoTextBox.TextChanged
        If Me.TipoAparatoTextBox.Text = "C" Then
            Me.Panel2.Visible = True
            Me.mac.TabStop = True
            Me.Panel3.Visible = False
            Me.MaskedTextBox1.TabStop = False
            Me.mac.Focus()
        ElseIf Me.TipoAparatoTextBox.Text = "D" Then
            Me.Panel2.Visible = False
            Me.mac.TabStop = False
            Me.Panel3.Visible = True
            Me.MaskedTextBox1.TabStop = True
            Me.MaskedTextBox1.Focus()
        ElseIf Me.TipoAparatoTextBox.Text = "E" Then
            Me.Panel2.Visible = False
            Me.mac.TabStop = False
            Me.Panel3.Visible = True
            Me.MaskedTextBox1.TabStop = True
            Me.MaskedTextBox1.Focus()
        End If
    End Sub

    Private Sub Fecha_solicitudTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fecha_solicitudTextBox.TextChanged
        If IsDate(Mid(Me.Fecha_solicitudTextBox.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.Fecha_solicitudTextBox.Text, 1, 10)
            If DateValue(Fecha) = DateValue("01/01/1900") Then
                Me.Fecha_solicitudTextBox.Clear()
            End If
        End If
    End Sub

    Private Sub Fecha_instalacioTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fecha_instalacioTextBox.TextChanged
        If IsDate(Mid(Me.Fecha_instalacioTextBox.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.Fecha_instalacioTextBox.Text, 1, 10)
            If DateValue(Fecha) = DateValue("01/01/1900") Then
                Me.Fecha_instalacioTextBox.Clear()
            End If
        End If
    End Sub

    Private Sub Fecha_bajaTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fecha_bajaTextBox.TextChanged
        If IsDate(Mid(Me.Fecha_bajaTextBox.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.Fecha_bajaTextBox.Text, 1, 10)
            If DateValue(Fecha) = DateValue("01/01/1900") Then
                Me.Fecha_bajaTextBox.Clear()
            End If
        End If
    End Sub

    Private Sub TreeView1_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView1.AfterSelect

    End Sub
End Class