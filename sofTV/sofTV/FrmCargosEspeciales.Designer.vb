<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCargosEspeciales
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim ServicioLabel As System.Windows.Forms.Label
        Dim SOLOINTERNETLabel1 As System.Windows.Forms.Label
        Dim ContratoLabel As System.Windows.Forms.Label
        Dim Clv_TecnicoLabel As System.Windows.Forms.Label
        Dim IMPRESALabel As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim Clv_servicioLabel As System.Windows.Forms.Label
        Dim MontoLabel As System.Windows.Forms.Label
        Dim Clv_tipserLabel As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim MOTIVOLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCargosEspeciales))
        Me.ESHOTELLabel1 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.ContratoTextBox = New System.Windows.Forms.TextBox
        Me.ConsultacobroespBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Procedimientosarnoldo4 = New sofTV.Procedimientosarnoldo4
        Me.ComboBox5 = New System.Windows.Forms.ComboBox
        Me.CMBGroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.TreeView1 = New System.Windows.Forms.TreeView
        Me.NOMBRELabel1 = New System.Windows.Forms.Label
        Me.BUSCLIPORCONTRATOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric2 = New sofTV.DataSetEric2
        Me.CALLELabel1 = New System.Windows.Forms.Label
        Me.COLONIALabel1 = New System.Windows.Forms.Label
        Me.NUMEROLabel1 = New System.Windows.Forms.Label
        Me.CIUDADLabel1 = New System.Windows.Forms.Label
        Me.SOLOINTERNETCheckBox = New System.Windows.Forms.CheckBox
        Me.ESHOTELCheckBox = New System.Windows.Forms.CheckBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Clv_TecnicoTextBox = New System.Windows.Forms.TextBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.IMPRESACheckBox = New System.Windows.Forms.CheckBox
        Me.Button5 = New System.Windows.Forms.Button
        Me.Inserta_Rel_cobroespBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.Inserta_Rel_cobroespBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton
        Me.Inserta_Rel_cobroespBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Inserta_Rel_cobroespTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Inserta_Rel_cobroespTableAdapter
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Consulta_cobroespTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.consulta_cobroespTableAdapter
        Me.Clv_servicioTextBox = New System.Windows.Forms.TextBox
        Me.MontoTextBox = New System.Windows.Forms.TextBox
        Me.Clv_tipserTextBox = New System.Windows.Forms.TextBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.MOTIVOTextBox = New System.Windows.Forms.TextBox
        Me.ComboBoxServicio = New System.Windows.Forms.ComboBox
        Me.TextBoxDescripcion = New System.Windows.Forms.TextBox
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker
        Me.Valida_cargos_especialesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Valida_cargos_especialesTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Valida_cargos_especialesTableAdapter
        Me.TextBoxClvServicio = New System.Windows.Forms.TextBox
        Me.BUSCLIPORCONTRATOTableAdapter1 = New sofTV.DataSetEric2TableAdapters.BUSCLIPORCONTRATOTableAdapter
        Me.DameSerDELCliBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameSerDELCliTableAdapter1 = New sofTV.DataSetEric2TableAdapters.dameSerDELCliTableAdapter
        Me.TextBox3 = New System.Windows.Forms.TextBox
        ServicioLabel = New System.Windows.Forms.Label
        SOLOINTERNETLabel1 = New System.Windows.Forms.Label
        ContratoLabel = New System.Windows.Forms.Label
        Clv_TecnicoLabel = New System.Windows.Forms.Label
        IMPRESALabel = New System.Windows.Forms.Label
        Label4 = New System.Windows.Forms.Label
        Clv_servicioLabel = New System.Windows.Forms.Label
        MontoLabel = New System.Windows.Forms.Label
        Clv_tipserLabel = New System.Windows.Forms.Label
        Label5 = New System.Windows.Forms.Label
        MOTIVOLabel = New System.Windows.Forms.Label
        CType(Me.ConsultacobroespBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CMBGroupBox1.SuspendLayout()
        CType(Me.BUSCLIPORCONTRATOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Rel_cobroespBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Inserta_Rel_cobroespBindingNavigator.SuspendLayout()
        CType(Me.Inserta_Rel_cobroespBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.Valida_cargos_especialesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameSerDELCliBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ServicioLabel
        '
        ServicioLabel.AutoSize = True
        ServicioLabel.Location = New System.Drawing.Point(70, 100)
        ServicioLabel.Name = "ServicioLabel"
        ServicioLabel.Size = New System.Drawing.Size(151, 16)
        ServicioLabel.TabIndex = 18
        ServicioLabel.Text = "Servicio Asignados :"
        '
        'SOLOINTERNETLabel1
        '
        SOLOINTERNETLabel1.AutoSize = True
        SOLOINTERNETLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SOLOINTERNETLabel1.ForeColor = System.Drawing.Color.White
        SOLOINTERNETLabel1.Location = New System.Drawing.Point(300, 73)
        SOLOINTERNETLabel1.Name = "SOLOINTERNETLabel1"
        SOLOINTERNETLabel1.Size = New System.Drawing.Size(97, 15)
        SOLOINTERNETLabel1.TabIndex = 16
        SOLOINTERNETLabel1.Text = "Solo Internet :"
        '
        'ContratoLabel
        '
        ContratoLabel.AutoSize = True
        ContratoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ContratoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ContratoLabel.Location = New System.Drawing.Point(33, 69)
        ContratoLabel.Name = "ContratoLabel"
        ContratoLabel.Size = New System.Drawing.Size(65, 15)
        ContratoLabel.TabIndex = 413
        ContratoLabel.Text = "Contrato:"
        '
        'Clv_TecnicoLabel
        '
        Clv_TecnicoLabel.AutoSize = True
        Clv_TecnicoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_TecnicoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_TecnicoLabel.Location = New System.Drawing.Point(170, 90)
        Clv_TecnicoLabel.Name = "Clv_TecnicoLabel"
        Clv_TecnicoLabel.Size = New System.Drawing.Size(65, 15)
        Clv_TecnicoLabel.TabIndex = 414
        Clv_TecnicoLabel.Text = "Técnico :"
        '
        'IMPRESALabel
        '
        IMPRESALabel.AutoSize = True
        IMPRESALabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        IMPRESALabel.ForeColor = System.Drawing.Color.DarkOrange
        IMPRESALabel.Location = New System.Drawing.Point(238, 119)
        IMPRESALabel.Name = "IMPRESALabel"
        IMPRESALabel.Size = New System.Drawing.Size(202, 18)
        IMPRESALabel.TabIndex = 415
        IMPRESALabel.Text = "La Orden ya fue Impresa :"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Label4.Location = New System.Drawing.Point(52, 42)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(46, 15)
        Label4.TabIndex = 427
        Label4.Text = "Clave:"
        '
        'Clv_servicioLabel
        '
        Clv_servicioLabel.AutoSize = True
        Clv_servicioLabel.Location = New System.Drawing.Point(208, 649)
        Clv_servicioLabel.Name = "Clv_servicioLabel"
        Clv_servicioLabel.Size = New System.Drawing.Size(63, 13)
        Clv_servicioLabel.TabIndex = 428
        Clv_servicioLabel.Text = "clv servicio:"
        '
        'MontoLabel
        '
        MontoLabel.AutoSize = True
        MontoLabel.Location = New System.Drawing.Point(408, 646)
        MontoLabel.Name = "MontoLabel"
        MontoLabel.Size = New System.Drawing.Size(39, 13)
        MontoLabel.TabIndex = 429
        MontoLabel.Text = "monto:"
        '
        'Clv_tipserLabel
        '
        Clv_tipserLabel.AutoSize = True
        Clv_tipserLabel.Location = New System.Drawing.Point(25, 649)
        Clv_tipserLabel.Name = "Clv_tipserLabel"
        Clv_tipserLabel.Size = New System.Drawing.Size(52, 13)
        Clv_tipserLabel.TabIndex = 430
        Clv_tipserLabel.Text = "clv tipser:"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label5.ForeColor = System.Drawing.Color.LightSlateGray
        Label5.Location = New System.Drawing.Point(453, 45)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(148, 15)
        Label5.TabIndex = 434
        Label5.Text = "Fecha de Generación:"
        '
        'MOTIVOLabel
        '
        MOTIVOLabel.AutoSize = True
        MOTIVOLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        MOTIVOLabel.Location = New System.Drawing.Point(17, 23)
        MOTIVOLabel.Name = "MOTIVOLabel"
        MOTIVOLabel.Size = New System.Drawing.Size(53, 15)
        MOTIVOLabel.TabIndex = 425
        MOTIVOLabel.Text = "Motivo:"
        '
        'ESHOTELLabel1
        '
        Me.ESHOTELLabel1.AutoSize = True
        Me.ESHOTELLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ESHOTELLabel1.ForeColor = System.Drawing.Color.White
        Me.ESHOTELLabel1.Location = New System.Drawing.Point(430, 73)
        Me.ESHOTELLabel1.Name = "ESHOTELLabel1"
        Me.ESHOTELLabel1.Size = New System.Drawing.Size(67, 15)
        Me.ESHOTELLabel1.TabIndex = 17
        Me.ESHOTELLabel1.Text = "Es hotel :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label6.Location = New System.Drawing.Point(461, 72)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(140, 15)
        Me.Label6.TabIndex = 436
        Me.Label6.Text = "Fecha de Aplicación:"
        '
        'ContratoTextBox
        '
        Me.ContratoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ContratoTextBox.CausesValidation = False
        Me.ContratoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConsultacobroespBindingSource, "cliente", True))
        Me.ContratoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContratoTextBox.Location = New System.Drawing.Point(103, 63)
        Me.ContratoTextBox.Name = "ContratoTextBox"
        Me.ContratoTextBox.Size = New System.Drawing.Size(104, 21)
        Me.ContratoTextBox.TabIndex = 1
        '
        'ConsultacobroespBindingSource
        '
        Me.ConsultacobroespBindingSource.DataMember = "consulta_cobroesp"
        Me.ConsultacobroespBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Procedimientosarnoldo4
        '
        Me.Procedimientosarnoldo4.DataSetName = "Procedimientosarnoldo4"
        Me.Procedimientosarnoldo4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ComboBox5
        '
        Me.ComboBox5.DisplayMember = "Concepto"
        Me.ComboBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox5.FormattingEnabled = True
        Me.ComboBox5.Location = New System.Drawing.Point(117, 63)
        Me.ComboBox5.Name = "ComboBox5"
        Me.ComboBox5.Size = New System.Drawing.Size(29, 21)
        Me.ComboBox5.TabIndex = 419
        Me.ComboBox5.TabStop = False
        Me.ComboBox5.ValueMember = "Clv_TipSerPrincipal"
        '
        'CMBGroupBox1
        '
        Me.CMBGroupBox1.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBGroupBox1.Controls.Add(Me.Label1)
        Me.CMBGroupBox1.Controls.Add(Me.TreeView1)
        Me.CMBGroupBox1.Controls.Add(Me.NOMBRELabel1)
        Me.CMBGroupBox1.Controls.Add(Me.CALLELabel1)
        Me.CMBGroupBox1.Controls.Add(Me.COLONIALabel1)
        Me.CMBGroupBox1.Controls.Add(Me.NUMEROLabel1)
        Me.CMBGroupBox1.Controls.Add(Me.CIUDADLabel1)
        Me.CMBGroupBox1.Controls.Add(Me.SOLOINTERNETCheckBox)
        Me.CMBGroupBox1.Controls.Add(Me.ESHOTELCheckBox)
        Me.CMBGroupBox1.Controls.Add(Me.Label2)
        Me.CMBGroupBox1.Controls.Add(ServicioLabel)
        Me.CMBGroupBox1.Controls.Add(Me.ESHOTELLabel1)
        Me.CMBGroupBox1.Controls.Add(SOLOINTERNETLabel1)
        Me.CMBGroupBox1.Controls.Add(Me.Label11)
        Me.CMBGroupBox1.Controls.Add(Me.Label10)
        Me.CMBGroupBox1.Controls.Add(Me.Label9)
        Me.CMBGroupBox1.Controls.Add(Me.Label8)
        Me.CMBGroupBox1.Controls.Add(Me.Clv_TecnicoTextBox)
        Me.CMBGroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBGroupBox1.ForeColor = System.Drawing.Color.White
        Me.CMBGroupBox1.Location = New System.Drawing.Point(3, 5)
        Me.CMBGroupBox1.Name = "CMBGroupBox1"
        Me.CMBGroupBox1.Size = New System.Drawing.Size(756, 204)
        Me.CMBGroupBox1.TabIndex = 418
        Me.CMBGroupBox1.TabStop = False
        Me.CMBGroupBox1.Text = "Datos del Cliente"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(18, 78)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 450
        Me.Label1.Text = "Ciudad :"
        '
        'TreeView1
        '
        Me.TreeView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView1.Location = New System.Drawing.Point(227, 99)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(382, 99)
        Me.TreeView1.TabIndex = 19
        Me.TreeView1.TabStop = False
        '
        'NOMBRELabel1
        '
        Me.NOMBRELabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NOMBRELabel1.Location = New System.Drawing.Point(66, 14)
        Me.NOMBRELabel1.Name = "NOMBRELabel1"
        Me.NOMBRELabel1.Size = New System.Drawing.Size(625, 23)
        Me.NOMBRELabel1.TabIndex = 437
        Me.NOMBRELabel1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'BUSCLIPORCONTRATOBindingSource
        '
        Me.BUSCLIPORCONTRATOBindingSource.DataMember = "BUSCLIPORCONTRATO"
        Me.BUSCLIPORCONTRATOBindingSource.DataSource = Me.DataSetEric2
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric2"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CALLELabel1
        '
        Me.CALLELabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CALLELabel1.Location = New System.Drawing.Point(66, 42)
        Me.CALLELabel1.Name = "CALLELabel1"
        Me.CALLELabel1.Size = New System.Drawing.Size(227, 23)
        Me.CALLELabel1.TabIndex = 439
        Me.CALLELabel1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'COLONIALabel1
        '
        Me.COLONIALabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.COLONIALabel1.Location = New System.Drawing.Point(486, 42)
        Me.COLONIALabel1.Name = "COLONIALabel1"
        Me.COLONIALabel1.Size = New System.Drawing.Size(205, 23)
        Me.COLONIALabel1.TabIndex = 441
        Me.COLONIALabel1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'NUMEROLabel1
        '
        Me.NUMEROLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NUMEROLabel1.Location = New System.Drawing.Point(325, 42)
        Me.NUMEROLabel1.Name = "NUMEROLabel1"
        Me.NUMEROLabel1.Size = New System.Drawing.Size(112, 23)
        Me.NUMEROLabel1.TabIndex = 443
        Me.NUMEROLabel1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'CIUDADLabel1
        '
        Me.CIUDADLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CIUDADLabel1.Location = New System.Drawing.Point(66, 68)
        Me.CIUDADLabel1.Name = "CIUDADLabel1"
        Me.CIUDADLabel1.Size = New System.Drawing.Size(223, 23)
        Me.CIUDADLabel1.TabIndex = 445
        Me.CIUDADLabel1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'SOLOINTERNETCheckBox
        '
        Me.SOLOINTERNETCheckBox.Enabled = False
        Me.SOLOINTERNETCheckBox.Location = New System.Drawing.Point(398, 67)
        Me.SOLOINTERNETCheckBox.Name = "SOLOINTERNETCheckBox"
        Me.SOLOINTERNETCheckBox.Size = New System.Drawing.Size(31, 24)
        Me.SOLOINTERNETCheckBox.TabIndex = 447
        '
        'ESHOTELCheckBox
        '
        Me.ESHOTELCheckBox.Enabled = False
        Me.ESHOTELCheckBox.Location = New System.Drawing.Point(500, 67)
        Me.ESHOTELCheckBox.Name = "ESHOTELCheckBox"
        Me.ESHOTELCheckBox.Size = New System.Drawing.Size(31, 24)
        Me.ESHOTELCheckBox.TabIndex = 449
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(258, 108)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(55, 16)
        Me.Label2.TabIndex = 431
        Me.Label2.Text = "Label2"
        Me.Label2.Visible = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(449, 52)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(31, 13)
        Me.Label11.TabIndex = 16
        Me.Label11.Text = "Col. :"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(300, 50)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(20, 13)
        Me.Label10.TabIndex = 15
        Me.Label10.Text = "# :"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(9, 52)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(58, 13)
        Me.Label9.TabIndex = 14
        Me.Label9.Text = "Dirección :"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(14, 22)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(50, 13)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Nombre :"
        '
        'Clv_TecnicoTextBox
        '
        Me.Clv_TecnicoTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TecnicoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_TecnicoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_TecnicoTextBox.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TecnicoTextBox.Location = New System.Drawing.Point(236, 129)
        Me.Clv_TecnicoTextBox.Name = "Clv_TecnicoTextBox"
        Me.Clv_TecnicoTextBox.Size = New System.Drawing.Size(24, 14)
        Me.Clv_TecnicoTextBox.TabIndex = 170
        Me.Clv_TecnicoTextBox.TabStop = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(210, 62)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(30, 22)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "..."
        Me.Button1.UseVisualStyleBackColor = False
        '
        'ComboBox1
        '
        Me.ComboBox1.DisplayMember = "nombre"
        Me.ComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(180, 87)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(432, 23)
        Me.ComboBox1.TabIndex = 417
        Me.ComboBox1.TabStop = False
        Me.ComboBox1.ValueMember = "clave"
        '
        'IMPRESACheckBox
        '
        Me.IMPRESACheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.IMPRESACheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IMPRESACheckBox.Location = New System.Drawing.Point(397, 116)
        Me.IMPRESACheckBox.Name = "IMPRESACheckBox"
        Me.IMPRESACheckBox.Size = New System.Drawing.Size(165, 24)
        Me.IMPRESACheckBox.TabIndex = 416
        Me.IMPRESACheckBox.TabStop = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(665, 462)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 5
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Inserta_Rel_cobroespBindingNavigator
        '
        Me.Inserta_Rel_cobroespBindingNavigator.AddNewItem = Nothing
        Me.Inserta_Rel_cobroespBindingNavigator.BindingSource = Me.Inserta_Rel_cobroespBindingSource
        Me.Inserta_Rel_cobroespBindingNavigator.CountItem = Nothing
        Me.Inserta_Rel_cobroespBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.Inserta_Rel_cobroespBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.Inserta_Rel_cobroespBindingNavigatorSaveItem})
        Me.Inserta_Rel_cobroespBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.Inserta_Rel_cobroespBindingNavigator.MoveFirstItem = Nothing
        Me.Inserta_Rel_cobroespBindingNavigator.MoveLastItem = Nothing
        Me.Inserta_Rel_cobroespBindingNavigator.MoveNextItem = Nothing
        Me.Inserta_Rel_cobroespBindingNavigator.MovePreviousItem = Nothing
        Me.Inserta_Rel_cobroespBindingNavigator.Name = "Inserta_Rel_cobroespBindingNavigator"
        Me.Inserta_Rel_cobroespBindingNavigator.PositionItem = Nothing
        Me.Inserta_Rel_cobroespBindingNavigator.Size = New System.Drawing.Size(821, 25)
        Me.Inserta_Rel_cobroespBindingNavigator.TabIndex = 4
        Me.Inserta_Rel_cobroespBindingNavigator.TabStop = True
        Me.Inserta_Rel_cobroespBindingNavigator.Text = "BindingNavigator1"
        '
        'Inserta_Rel_cobroespBindingSource
        '
        Me.Inserta_Rel_cobroespBindingSource.DataMember = "Inserta_Rel_cobroesp"
        Me.Inserta_Rel_cobroespBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(63, 22)
        Me.BindingNavigatorDeleteItem.Text = "Eliminar"
        Me.BindingNavigatorDeleteItem.Visible = False
        '
        'Inserta_Rel_cobroespBindingNavigatorSaveItem
        '
        Me.Inserta_Rel_cobroespBindingNavigatorSaveItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.Inserta_Rel_cobroespBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Inserta_Rel_cobroespBindingNavigatorSaveItem.Image = CType(resources.GetObject("Inserta_Rel_cobroespBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.Inserta_Rel_cobroespBindingNavigatorSaveItem.Name = "Inserta_Rel_cobroespBindingNavigatorSaveItem"
        Me.Inserta_Rel_cobroespBindingNavigatorSaveItem.Size = New System.Drawing.Size(121, 22)
        Me.Inserta_Rel_cobroespBindingNavigatorSaveItem.Text = "Guardar datos"
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(629, 41)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(90, 21)
        Me.TextBox1.TabIndex = 3
        '
        'Inserta_Rel_cobroespTableAdapter
        '
        Me.Inserta_Rel_cobroespTableAdapter.ClearBeforeFill = True
        '
        'TextBox2
        '
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox2.CausesValidation = False
        Me.TextBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConsultacobroespBindingSource, "clv_cobro", True))
        Me.TextBox2.Enabled = False
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(103, 36)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(104, 21)
        Me.TextBox2.TabIndex = 426
        Me.TextBox2.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.CMBGroupBox1)
        Me.Panel1.Controls.Add(Me.IMPRESACheckBox)
        Me.Panel1.Controls.Add(IMPRESALabel)
        Me.Panel1.Controls.Add(Clv_TecnicoLabel)
        Me.Panel1.Controls.Add(Me.ComboBox1)
        Me.Panel1.Location = New System.Drawing.Point(20, 107)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(764, 212)
        Me.Panel1.TabIndex = 428
        '
        'Consulta_cobroespTableAdapter
        '
        Me.Consulta_cobroespTableAdapter.ClearBeforeFill = True
        '
        'Clv_servicioTextBox
        '
        Me.Clv_servicioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConsultacobroespBindingSource, "clv_servicio", True))
        Me.Clv_servicioTextBox.Location = New System.Drawing.Point(277, 646)
        Me.Clv_servicioTextBox.Name = "Clv_servicioTextBox"
        Me.Clv_servicioTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Clv_servicioTextBox.TabIndex = 429
        '
        'MontoTextBox
        '
        Me.MontoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConsultacobroespBindingSource, "monto", True))
        Me.MontoTextBox.Location = New System.Drawing.Point(453, 643)
        Me.MontoTextBox.Name = "MontoTextBox"
        Me.MontoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.MontoTextBox.TabIndex = 430
        '
        'Clv_tipserTextBox
        '
        Me.Clv_tipserTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConsultacobroespBindingSource, "clv_tipser", True))
        Me.Clv_tipserTextBox.Location = New System.Drawing.Point(83, 646)
        Me.Clv_tipserTextBox.Name = "Clv_tipserTextBox"
        Me.Clv_tipserTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Clv_tipserTextBox.TabIndex = 431
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(MOTIVOLabel)
        Me.GroupBox1.Controls.Add(Me.MOTIVOTextBox)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.GroupBox1.Location = New System.Drawing.Point(20, 322)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(764, 131)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(467, 41)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(156, 24)
        Me.Label3.TabIndex = 436
        Me.Label3.Text = "Cargo($):"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'MOTIVOTextBox
        '
        Me.MOTIVOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConsultacobroespBindingSource, "MOTIVO", True))
        Me.MOTIVOTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MOTIVOTextBox.Location = New System.Drawing.Point(16, 41)
        Me.MOTIVOTextBox.MaxLength = 250
        Me.MOTIVOTextBox.Multiline = True
        Me.MOTIVOTextBox.Name = "MOTIVOTextBox"
        Me.MOTIVOTextBox.Size = New System.Drawing.Size(437, 64)
        Me.MOTIVOTextBox.TabIndex = 2
        '
        'ComboBoxServicio
        '
        Me.ComboBoxServicio.DisplayMember = "DESCRIPCION"
        Me.ComboBoxServicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxServicio.FormattingEnabled = True
        Me.ComboBoxServicio.Location = New System.Drawing.Point(284, 2)
        Me.ComboBoxServicio.Name = "ComboBoxServicio"
        Me.ComboBoxServicio.Size = New System.Drawing.Size(12, 23)
        Me.ComboBoxServicio.TabIndex = 0
        Me.ComboBoxServicio.TabStop = False
        Me.ComboBoxServicio.ValueMember = "CLV_SERVICIO"
        Me.ComboBoxServicio.Visible = False
        '
        'TextBoxDescripcion
        '
        Me.TextBoxDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxDescripcion.Location = New System.Drawing.Point(302, 2)
        Me.TextBoxDescripcion.Name = "TextBoxDescripcion"
        Me.TextBoxDescripcion.ReadOnly = True
        Me.TextBoxDescripcion.Size = New System.Drawing.Size(10, 21)
        Me.TextBoxDescripcion.TabIndex = 428
        Me.TextBoxDescripcion.TabStop = False
        Me.TextBoxDescripcion.Visible = False
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Enabled = False
        Me.DateTimePicker1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(604, 42)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(135, 21)
        Me.DateTimePicker1.TabIndex = 433
        Me.DateTimePicker1.TabStop = False
        Me.DateTimePicker1.Value = New Date(2008, 7, 29, 0, 0, 0, 0)
        '
        'Valida_cargos_especialesBindingSource
        '
        Me.Valida_cargos_especialesBindingSource.DataMember = "Valida_cargos_especiales"
        Me.Valida_cargos_especialesBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Valida_cargos_especialesTableAdapter
        '
        Me.Valida_cargos_especialesTableAdapter.ClearBeforeFill = True
        '
        'TextBoxClvServicio
        '
        Me.TextBoxClvServicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxClvServicio.Location = New System.Drawing.Point(20, 0)
        Me.TextBoxClvServicio.Name = "TextBoxClvServicio"
        Me.TextBoxClvServicio.ReadOnly = True
        Me.TextBoxClvServicio.Size = New System.Drawing.Size(10, 21)
        Me.TextBoxClvServicio.TabIndex = 427
        Me.TextBoxClvServicio.TabStop = False
        '
        'BUSCLIPORCONTRATOTableAdapter1
        '
        Me.BUSCLIPORCONTRATOTableAdapter1.ClearBeforeFill = True
        '
        'DameSerDELCliBindingSource1
        '
        Me.DameSerDELCliBindingSource1.DataMember = "dameSerDELCli"
        Me.DameSerDELCliBindingSource1.DataSource = Me.DataSetEric2
        '
        'DameSerDELCliTableAdapter1
        '
        Me.DameSerDELCliTableAdapter1.ClearBeforeFill = True
        '
        'TextBox3
        '
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(604, 67)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(135, 21)
        Me.TextBox3.TabIndex = 437
        '
        'FrmCargosEspeciales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(821, 513)
        Me.Controls.Add(Me.TextBox3)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Inserta_Rel_cobroespBindingNavigator)
        Me.Controls.Add(Me.ComboBoxServicio)
        Me.Controls.Add(Label5)
        Me.Controls.Add(Me.TextBoxDescripcion)
        Me.Controls.Add(Me.DateTimePicker1)
        Me.Controls.Add(Me.TextBoxClvServicio)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Clv_tipserLabel)
        Me.Controls.Add(Me.Clv_tipserTextBox)
        Me.Controls.Add(MontoLabel)
        Me.Controls.Add(Me.MontoTextBox)
        Me.Controls.Add(Clv_servicioLabel)
        Me.Controls.Add(Me.Clv_servicioTextBox)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Label4)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.ContratoTextBox)
        Me.Controls.Add(Me.ComboBox5)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(ContratoLabel)
        Me.Name = "FrmCargosEspeciales"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cargos Especiales"
        CType(Me.ConsultacobroespBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CMBGroupBox1.ResumeLayout(False)
        Me.CMBGroupBox1.PerformLayout()
        CType(Me.BUSCLIPORCONTRATOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Rel_cobroespBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Inserta_Rel_cobroespBindingNavigator.ResumeLayout(False)
        Me.Inserta_Rel_cobroespBindingNavigator.PerformLayout()
        CType(Me.Inserta_Rel_cobroespBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.Valida_cargos_especialesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameSerDELCliBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ContratoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox5 As System.Windows.Forms.ComboBox
    Friend WithEvents CMBGroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Clv_TecnicoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents IMPRESACheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Procedimientosarnoldo4 As sofTV.Procedimientosarnoldo4
    Friend WithEvents Inserta_Rel_cobroespBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Rel_cobroespTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Inserta_Rel_cobroespTableAdapter
    Friend WithEvents Inserta_Rel_cobroespBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Inserta_Rel_cobroespBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents BUSCLIPORCONTRATOTableAdapter As sofTV.NewSofTvDataSetTableAdapters.BUSCLIPORCONTRATOTableAdapter
    Friend WithEvents DameSerDELCliBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameSerDELCliTableAdapter As sofTV.NewSofTvDataSetTableAdapters.dameSerDELCliTableAdapter
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents ConsultacobroespBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_cobroespTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.consulta_cobroespTableAdapter
    Friend WithEvents Clv_servicioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MontoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_tipserTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Valida_cargos_especialesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Valida_cargos_especialesTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Valida_cargos_especialesTableAdapter
    Friend WithEvents MOTIVOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxClvServicio As System.Windows.Forms.TextBox
    Friend WithEvents ComboBoxServicio As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric2
    Friend WithEvents BUSCLIPORCONTRATOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BUSCLIPORCONTRATOTableAdapter1 As sofTV.DataSetEric2TableAdapters.BUSCLIPORCONTRATOTableAdapter
    Friend WithEvents NOMBRELabel1 As System.Windows.Forms.Label
    Friend WithEvents CALLELabel1 As System.Windows.Forms.Label
    Friend WithEvents COLONIALabel1 As System.Windows.Forms.Label
    Friend WithEvents NUMEROLabel1 As System.Windows.Forms.Label
    Friend WithEvents CIUDADLabel1 As System.Windows.Forms.Label
    Friend WithEvents SOLOINTERNETCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents ESHOTELCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents DameSerDELCliBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents DameSerDELCliTableAdapter1 As sofTV.DataSetEric2TableAdapters.dameSerDELCliTableAdapter
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents ESHOTELLabel1 As System.Windows.Forms.Label
End Class
