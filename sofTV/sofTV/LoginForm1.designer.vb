<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class LoginForm1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CiudadLabel As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(LoginForm1))
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.VerAccesoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.VerAccesoTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.VerAccesoTableAdapter()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Muestra_EmpresaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetarnoldo = New sofTV.DataSetarnoldo()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PasswordTextBox = New System.Windows.Forms.TextBox()
        Me.Cancel = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.UsernameTextBox = New System.Windows.Forms.TextBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.OK = New System.Windows.Forms.Button()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.DataSetEDGAR = New sofTV.DataSetEDGAR()
        Me.DameDatosGeneralesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameDatosGeneralesTableAdapter = New sofTV.DataSetEDGARTableAdapters.DameDatosGeneralesTableAdapter()
        Me.CiudadTextBox = New System.Windows.Forms.TextBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.MUESTRAIMAGENBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLidia = New sofTV.DataSetLidia()
        Me.Muestra_EmpresaTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Muestra_EmpresaTableAdapter()
        Me.DAMECAJA_SUCURSALBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DAMECAJA_SUCURSALTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.DAMECAJA_SUCURSALTableAdapter()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.DimeTipSer_CualEsPrincipalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEdgarRev2 = New sofTV.DataSetEdgarRev2()
        Me.DAMECLAVEUSUARIOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DAMECLAVEUSUARIOTableAdapter = New sofTV.DataSetEdgarRev2TableAdapters.DAMECLAVEUSUARIOTableAdapter()
        Me.DimeTipSer_CualEsPrincipalTableAdapter = New sofTV.DataSetEdgarRev2TableAdapters.DimeTipSer_CualEsPrincipalTableAdapter()
        Me.MUESTRAIMAGENTableAdapter = New sofTV.DataSetLidiaTableAdapters.MUESTRAIMAGENTableAdapter()
        Me.DameRutaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameRutaTableAdapter = New sofTV.DataSetLidiaTableAdapters.DameRutaTableAdapter()
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.DameIdBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameIdTableAdapter = New sofTV.DataSetEricTableAdapters.DameIdTableAdapter()
        Me.VerAccesoAdminBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.VerAccesoAdminTableAdapter = New sofTV.DataSetEricTableAdapters.VerAccesoAdminTableAdapter()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.ConGeneralRelBDBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConGeneralRelBDTableAdapter = New sofTV.DataSetEricTableAdapters.ConGeneralRelBDTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        CiudadLabel = New System.Windows.Forms.Label()
        Label4 = New System.Windows.Forms.Label()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VerAccesoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Muestra_EmpresaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameDatosGeneralesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAIMAGENBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAMECAJA_SUCURSALBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DimeTipSer_CualEsPrincipalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAMECLAVEUSUARIOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameRutaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameIdBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VerAccesoAdminBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConGeneralRelBDBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CiudadLabel
        '
        CiudadLabel.AutoSize = True
        CiudadLabel.Location = New System.Drawing.Point(320, 269)
        CiudadLabel.Name = "CiudadLabel"
        CiudadLabel.Size = New System.Drawing.Size(61, 16)
        CiudadLabel.TabIndex = 20
        CiudadLabel.Text = "Ciudad:"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.Location = New System.Drawing.Point(586, 255)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(131, 16)
        Label4.TabIndex = 24
        Label4.Text = "Clv_TipoServicio:"
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'VerAccesoBindingSource
        '
        Me.VerAccesoBindingSource.DataMember = "VerAcceso"
        Me.VerAccesoBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'VerAccesoTableAdapter
        '
        Me.VerAccesoTableAdapter.ClearBeforeFill = True
        '
        'Label3
        '
        Me.Label3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Muestra_EmpresaBindingSource, "nombre", True))
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label3.Image = CType(resources.GetObject("Label3.Image"), System.Drawing.Image)
        Me.Label3.Location = New System.Drawing.Point(313, 138)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(391, 42)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Sistemas Administrativos para Televisión Restringida."
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Muestra_EmpresaBindingSource
        '
        Me.Muestra_EmpresaBindingSource.DataMember = "Muestra_Empresa"
        Me.Muestra_EmpresaBindingSource.DataSource = Me.DataSetarnoldo
        '
        'DataSetarnoldo
        '
        Me.DataSetarnoldo.DataSetName = "DataSetarnoldo"
        Me.DataSetarnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label2.Image = CType(resources.GetObject("Label2.Image"), System.Drawing.Image)
        Me.Label2.Location = New System.Drawing.Point(366, 92)
        Me.Label2.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(88, 16)
        Me.Label2.TabIndex = 19
        Me.Label2.Text = "Pasaporte :"
        '
        'PasswordTextBox
        '
        Me.PasswordTextBox.Location = New System.Drawing.Point(454, 89)
        Me.PasswordTextBox.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.PasswordTextBox.Name = "PasswordTextBox"
        Me.PasswordTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.PasswordTextBox.Size = New System.Drawing.Size(164, 22)
        Me.PasswordTextBox.TabIndex = 1
        '
        'Cancel
        '
        Me.Cancel.BackColor = System.Drawing.Color.DarkOrange
        Me.Cancel.BackgroundImage = CType(resources.GetObject("Cancel.BackgroundImage"), System.Drawing.Image)
        Me.Cancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Cancel.ForeColor = System.Drawing.Color.White
        Me.Cancel.Location = New System.Drawing.Point(559, 207)
        Me.Cancel.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.Cancel.Name = "Cancel"
        Me.Cancel.Size = New System.Drawing.Size(135, 28)
        Me.Cancel.TabIndex = 3
        Me.Cancel.Text = "Cancelar"
        Me.Cancel.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label1.Image = CType(resources.GetObject("Label1.Image"), System.Drawing.Image)
        Me.Label1.Location = New System.Drawing.Point(366, 60)
        Me.Label1.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(54, 16)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Login :"
        '
        'UsernameTextBox
        '
        Me.UsernameTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.UsernameTextBox.Location = New System.Drawing.Point(454, 57)
        Me.UsernameTextBox.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.UsernameTextBox.Name = "UsernameTextBox"
        Me.UsernameTextBox.Size = New System.Drawing.Size(164, 22)
        Me.UsernameTextBox.TabIndex = 0
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(313, 136)
        Me.PictureBox3.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(391, 65)
        Me.PictureBox3.TabIndex = 14
        Me.PictureBox3.TabStop = False
        '
        'OK
        '
        Me.OK.BackColor = System.Drawing.Color.DarkOrange
        Me.OK.BackgroundImage = CType(resources.GetObject("OK.BackgroundImage"), System.Drawing.Image)
        Me.OK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.OK.ForeColor = System.Drawing.Color.White
        Me.OK.Location = New System.Drawing.Point(354, 207)
        Me.OK.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.OK.Name = "OK"
        Me.OK.Size = New System.Drawing.Size(135, 28)
        Me.OK.TabIndex = 2
        Me.OK.Text = "Aceptar"
        Me.OK.UseVisualStyleBackColor = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(314, 46)
        Me.PictureBox2.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(391, 80)
        Me.PictureBox2.TabIndex = 13
        Me.PictureBox2.TabStop = False
        '
        'DataSetEDGAR
        '
        Me.DataSetEDGAR.DataSetName = "DataSetEDGAR"
        Me.DataSetEDGAR.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DameDatosGeneralesBindingSource
        '
        Me.DameDatosGeneralesBindingSource.DataMember = "DameDatosGenerales"
        Me.DameDatosGeneralesBindingSource.DataSource = Me.DataSetEDGAR
        '
        'DameDatosGeneralesTableAdapter
        '
        Me.DameDatosGeneralesTableAdapter.ClearBeforeFill = True
        '
        'CiudadTextBox
        '
        Me.CiudadTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameDatosGeneralesBindingSource, "Ciudad", True))
        Me.CiudadTextBox.Location = New System.Drawing.Point(387, 266)
        Me.CiudadTextBox.Name = "CiudadTextBox"
        Me.CiudadTextBox.Size = New System.Drawing.Size(100, 22)
        Me.CiudadTextBox.TabIndex = 21
        Me.CiudadTextBox.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PictureBox4.DataBindings.Add(New System.Windows.Forms.Binding("Image", Me.MUESTRAIMAGENBindingSource, "IMAGEN", True))
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(68, 46)
        Me.PictureBox4.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(236, 195)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox4.TabIndex = 22
        Me.PictureBox4.TabStop = False
        '
        'MUESTRAIMAGENBindingSource
        '
        Me.MUESTRAIMAGENBindingSource.DataMember = "MUESTRAIMAGEN"
        Me.MUESTRAIMAGENBindingSource.DataSource = Me.DataSetLidia
        '
        'DataSetLidia
        '
        Me.DataSetLidia.DataSetName = "DataSetLidia"
        Me.DataSetLidia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Muestra_EmpresaTableAdapter
        '
        Me.Muestra_EmpresaTableAdapter.ClearBeforeFill = True
        '
        'DAMECAJA_SUCURSALBindingSource
        '
        Me.DAMECAJA_SUCURSALBindingSource.DataMember = "DAMECAJA_SUCURSAL"
        Me.DAMECAJA_SUCURSALBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'DAMECAJA_SUCURSALTableAdapter
        '
        Me.DAMECAJA_SUCURSALTableAdapter.ClearBeforeFill = True
        '
        'TextBox1
        '
        Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DimeTipSer_CualEsPrincipalBindingSource, "Clv_TipSer", True))
        Me.TextBox1.Location = New System.Drawing.Point(589, 274)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(104, 22)
        Me.TextBox1.TabIndex = 23
        Me.TextBox1.TabStop = False
        '
        'DimeTipSer_CualEsPrincipalBindingSource
        '
        Me.DimeTipSer_CualEsPrincipalBindingSource.DataMember = "DimeTipSer_CualEsPrincipal"
        Me.DimeTipSer_CualEsPrincipalBindingSource.DataSource = Me.DataSetEdgarRev2
        '
        'DataSetEdgarRev2
        '
        Me.DataSetEdgarRev2.DataSetName = "DataSetEdgarRev2"
        Me.DataSetEdgarRev2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DAMECLAVEUSUARIOBindingSource
        '
        Me.DAMECLAVEUSUARIOBindingSource.DataMember = "DAMECLAVEUSUARIO"
        Me.DAMECLAVEUSUARIOBindingSource.DataSource = Me.DataSetEdgarRev2
        '
        'DAMECLAVEUSUARIOTableAdapter
        '
        Me.DAMECLAVEUSUARIOTableAdapter.ClearBeforeFill = True
        '
        'DimeTipSer_CualEsPrincipalTableAdapter
        '
        Me.DimeTipSer_CualEsPrincipalTableAdapter.ClearBeforeFill = True
        '
        'MUESTRAIMAGENTableAdapter
        '
        Me.MUESTRAIMAGENTableAdapter.ClearBeforeFill = True
        '
        'DameRutaBindingSource
        '
        Me.DameRutaBindingSource.DataMember = "DameRuta"
        Me.DameRutaBindingSource.DataSource = Me.DataSetLidia
        '
        'DameRutaTableAdapter
        '
        Me.DameRutaTableAdapter.ClearBeforeFill = True
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DameIdBindingSource
        '
        Me.DameIdBindingSource.DataMember = "DameId"
        Me.DameIdBindingSource.DataSource = Me.DataSetEric
        '
        'DameIdTableAdapter
        '
        Me.DameIdTableAdapter.ClearBeforeFill = True
        '
        'VerAccesoAdminBindingSource
        '
        Me.VerAccesoAdminBindingSource.DataMember = "VerAccesoAdmin"
        Me.VerAccesoAdminBindingSource.DataSource = Me.DataSetEric
        '
        'VerAccesoAdminTableAdapter
        '
        Me.VerAccesoAdminTableAdapter.ClearBeforeFill = True
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label5.Image = CType(resources.GetObject("Label5.Image"), System.Drawing.Image)
        Me.Label5.Location = New System.Drawing.Point(313, 178)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(391, 21)
        Me.Label5.TabIndex = 25
        Me.Label5.Text = "SofTV"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ConGeneralRelBDBindingSource
        '
        Me.ConGeneralRelBDBindingSource.DataMember = "ConGeneralRelBD"
        Me.ConGeneralRelBDBindingSource.DataSource = Me.DataSetEric
        '
        'ConGeneralRelBDTableAdapter
        '
        Me.ConGeneralRelBDTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'LoginForm1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(737, 309)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Label4)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.PictureBox4)
        Me.Controls.Add(CiudadLabel)
        Me.Controls.Add(Me.CiudadTextBox)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.PasswordTextBox)
        Me.Controls.Add(Me.Cancel)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.UsernameTextBox)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.OK)
        Me.Controls.Add(Me.PictureBox2)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "LoginForm1"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Acceso al Sistema"
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VerAccesoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Muestra_EmpresaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameDatosGeneralesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAIMAGENBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAMECAJA_SUCURSALBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DimeTipSer_CualEsPrincipalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAMECLAVEUSUARIOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameRutaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameIdBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VerAccesoAdminBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConGeneralRelBDBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents VerAccesoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents VerAccesoTableAdapter As sofTV.NewSofTvDataSetTableAdapters.VerAccesoTableAdapter
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents PasswordTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Cancel As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents UsernameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents OK As System.Windows.Forms.Button
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents DataSetEDGAR As sofTV.DataSetEDGAR
    Friend WithEvents DameDatosGeneralesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameDatosGeneralesTableAdapter As sofTV.DataSetEDGARTableAdapters.DameDatosGeneralesTableAdapter
    Friend WithEvents CiudadTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents DataSetLidia As sofTV.DataSetLidia
    Friend WithEvents MUESTRAIMAGENBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAIMAGENTableAdapter As sofTV.DataSetLidiaTableAdapters.MUESTRAIMAGENTableAdapter
    Friend WithEvents DataSetarnoldo As sofTV.DataSetarnoldo
    Friend WithEvents Muestra_EmpresaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_EmpresaTableAdapter As sofTV.DataSetarnoldoTableAdapters.Muestra_EmpresaTableAdapter
    Friend WithEvents DataSetEdgarRev2 As sofTV.DataSetEdgarRev2
    Friend WithEvents DAMECLAVEUSUARIOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMECLAVEUSUARIOTableAdapter As sofTV.DataSetEdgarRev2TableAdapters.DAMECLAVEUSUARIOTableAdapter
    Friend WithEvents DAMECAJA_SUCURSALBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMECAJA_SUCURSALTableAdapter As sofTV.NewSofTvDataSetTableAdapters.DAMECAJA_SUCURSALTableAdapter
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents DimeTipSer_CualEsPrincipalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DimeTipSer_CualEsPrincipalTableAdapter As sofTV.DataSetEdgarRev2TableAdapters.DimeTipSer_CualEsPrincipalTableAdapter
    Friend WithEvents DameRutaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameRutaTableAdapter As sofTV.DataSetLidiaTableAdapters.DameRutaTableAdapter
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents DameIdBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameIdTableAdapter As sofTV.DataSetEricTableAdapters.DameIdTableAdapter
    Friend WithEvents VerAccesoAdminBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents VerAccesoAdminTableAdapter As sofTV.DataSetEricTableAdapters.VerAccesoAdminTableAdapter
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents ConGeneralRelBDBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConGeneralRelBDTableAdapter As sofTV.DataSetEricTableAdapters.ConGeneralRelBDTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter

End Class
