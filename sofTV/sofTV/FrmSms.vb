Imports System.Net
Imports System.IO
Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class FrmSms
    Dim cont As Integer
    Private req As WebRequest = Nothing
    Private rsp As WebResponse = Nothing
    Private cuentaMensajes As Integer = 0
    Private contratado As Integer = 0
    Private suspendido As Integer = 0
    Private cancelado As Integer = 0
    Private Temporales As Integer = 0
    Private instalado As Integer = 0
    Private desconectado As Integer = 0
    Private fueraArea As Integer = 0
    Private periodo1 As Integer = 0
    Private periodo2 As Integer = 0


    Private Sub FrmSms_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ProgressBar1.Minimum = 0
        ProgressBar1.Value = 0
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Button2.Enabled = False
        Button3.Enabled = False
        If Me.CheckBox1.Checked = False And Me.CheckBox3.Checked = False And Me.CheckBox6.Checked = False And Me.CheckBox4.Checked = False And Me.CheckBox5.Checked = False And Me.CheckBox2.Checked = False And Me.CheckBox7.Checked = False Then
            MsgBox("Selecciona al menos un Status.", , "Atenci�n")
            Exit Sub
        End If
        If Me.CheckBox9.Checked = False And Me.CheckBox8.Checked = False Then
            MsgBox("Selecciona al menos un Periodo.", , "Atenci�n")
            Exit Sub
        End If
        If CheckBox1.Checked = True Then
            contratado = 1
       End If
        If CheckBox5.Checked = True Then
            suspendido = 1
        End If
        If CheckBox2.Checked = True Then
            cancelado = 1
        End If
        If CheckBox7.Checked = True Then
            Temporales = 1
        End If
        If CheckBox3.Checked = True Then
            instalado = 1
        End If
        If CheckBox4.Checked = True Then
            desconectado = 1
        End If
        If CheckBox6.Checked = True Then
            fueraArea = 1
        End If
        If CheckBox8.Checked = True Then
            periodo1 = 1
        End If
        If CheckBox9.Checked = True Then
            periodo2 = 1
        End If
        Me.BackgroundWorker1.RunWorkerAsync()
        Me.Refresh()
    End Sub

    Private Sub Post(ByVal usuario As String, ByVal contrasena As String, ByVal tablaClientes As DataTable, ByVal mensaje As String)
        Dim i As Integer = 0
     Do While (i < tablaClientes.Rows.Count)
            Dim numero As String = tablaClientes.Rows(i)("Celular").ToString
            Dim xml As String = "<?xml version='1.0' encoding='iso-8859-1' ?>"
            xml = xml & "<sms>"
            xml = xml & "<user>" + usuario + "</user>"
            xml = xml & "<password>" + contrasena + "</password>"
            xml = xml & "<dst><num>+52" + numero + "</num></dst>"
            xml = xml & "<txt>" + mensaje + "</txt>"
            xml = xml & "</sms>"
            Dim uri As String = ("http://sms.lleida.net/xmlapi/smsgw.cgi?xml=" + xml)
            Dim con As SqlConnection = New SqlConnection(MiConexion)
            con.Open()
            Try
                req = WebRequest.Create(uri)
                req.Method = WebRequestMethods.Http.Post
                req.ContentType = "text/xml"
                rsp = CType(req.GetResponse, HttpWebResponse)
                Dim responseStream As Stream = rsp.GetResponseStream
                Dim reader As StreamReader = New StreamReader(responseStream)
                File.Delete("C:\respuesta.xml")
                Dim sw As StreamWriter = File.CreateText("C:\respuesta.xml")
                sw.Write(reader.ReadToEnd)
                reader.Close()
                sw.Close()
                Dim status As Integer = Convert.ToInt32(Me.obtenerStatus("C:\respuesta.xml"))
                Try
                    Dim com As SqlCommand = New SqlCommand("insertaBitacoraSms", con)
                    com.CommandType = CommandType.StoredProcedure
                    com.CommandTimeout = 0
                    com.Parameters.Add(New SqlParameter("@Contrato", Convert.ToInt64(tablaClientes.Rows(i)("Contrato").ToString)))
                    com.Parameters.Add(New SqlParameter("@estado", status))
                    com.Parameters.Add(New SqlParameter("@Telefono", numero))
                    com.ExecuteNonQuery()
                Catch ex As Exception
                    Windows.Forms.MessageBox.Show(ex.Message, "Error")
                End Try
            Catch ex As Exception
                MessageBox.Show(ex.Message, "ERROR", MessageBoxButtons.OK)
            Finally
                If (Not (rsp) Is Nothing) Then
                    rsp.GetResponseStream.Close()
                End If
                con.Close()
            End Try
            i = i + 1
            Me.BackgroundWorker1.ReportProgress((cont * 100) / i)
            cuentaMensajes = cuentaMensajes + 1
        Loop
    End Sub

    Private Function obtenerStatus(ByVal file As String) As String
        Dim reader As StreamReader = New StreamReader(file)
        Dim ret As String = reader.ReadToEnd
        reader.Close()
        Dim posI As Integer = ret.IndexOf("<status>") + 8
        Dim posF As Integer = ret.IndexOf("</status>")
        Dim status As String = ret.Substring(posI, posF - posI)
        Return status
    End Function

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Private Sub NotifyIcon1_MouseDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles NotifyIcon1.MouseDoubleClick
        Me.Show()
        Me.Refresh()
        Me.WindowState = FormWindowState.Normal
        NotifyIcon1.Visible = False
    End Sub

    Private Sub FrmSms_Resize(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Resize
        If (Me.WindowState = FormWindowState.Minimized) Then
            Me.NotifyIcon1.Icon = Me.Icon
            Me.Visible = False
            NotifyIcon1.Visible = True
        End If
    End Sub

    Private Sub EnviarSms()
        Dim tabla As DataTable = New DataTable
        Dim con As SqlConnection = New SqlConnection(MiConexion)
        Dim mensaje As String = TextBox1.Text.ToString
        Try
            Dim com As SqlCommand = New SqlCommand("MandarSMSmasivo", con)
            con.Open()
            com.CommandType = CommandType.StoredProcedure
            com.CommandTimeout = 0
            com.Parameters.Add(New SqlParameter("@Clv_Session", Convert.ToInt64(LocClv_session)))
            com.Parameters.Add(New SqlParameter("@Con", contratado))
            com.Parameters.Add(New SqlParameter("@Ins", instalado))
            com.Parameters.Add(New SqlParameter("@Sus", suspendido))
            com.Parameters.Add(New SqlParameter("@Des", cancelado))
            com.Parameters.Add(New SqlParameter("@Baj", desconectado))
            com.Parameters.Add(New SqlParameter("@Fue", fueraArea))
            com.Parameters.Add(New SqlParameter("@Tem", Temporales))
            com.Parameters.Add(New SqlParameter("@Per1", periodo1))
            com.Parameters.Add(New SqlParameter("@Per2", periodo2))
            Dim da As SqlDataAdapter = New SqlDataAdapter(com)
            da.Fill(tabla)
            If tabla.Rows.Count > 0 Then
                Try
                    Dim ins As SqlCommand = New SqlCommand("INSERT INTO Master_Mensaje (Fecha, Mensaje) VALUES (getdate(), '" + mensaje + "')", con)
                    ins.CommandType = CommandType.Text
                    ins.ExecuteNonQuery()
                Catch ex As Exception
                    Windows.Forms.MessageBox.Show(ex.Message, "Error")
                End Try
            End If
        Catch ex As Exception
            Windows.Forms.MessageBox.Show(ex.Message, "Error")
        Finally
            con.Close()
        End Try
        If tabla.Rows.Count = 0 Then
            Windows.Forms.MessageBox.Show("No se encontraron clientes. Favor de intentar de nuevo.", "Error")
        Else
            cont = tabla.Rows.Count
            'Me.Post("GIGACABLE", "GIGA07", tabla, mensaje)
            Me.Post("asd", "asd", tabla, mensaje)
        End If
    End Sub

    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        Me.EnviarSms()
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs)
        MsgBox(cuentaMensajes + "Mensaje(s) Enviado(s) con �xito", MsgBoxStyle.Information)
    End Sub

    Private Sub BackgroundWorker1_ProgressChanged(ByVal sender As System.Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorker1.ProgressChanged
        If ProgressBar1.Value = 0 Then
            ProgressBar1.Maximum = cont
        End If
        If Me.ProgressBar1.Maximum = Me.ProgressBar1.Value Then
            Me.ProgressBar1.Value = 0
            Button2.Enabled = True
            Button3.Enabled = True
        Else
            Me.ProgressBar1.Value = Me.ProgressBar1.Value + 1
        End If
        Me.Refresh()
    End Sub
End Class