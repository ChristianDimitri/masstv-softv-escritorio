﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCompanias
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtNombreCompania = New System.Windows.Forms.TextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.cbxActivo = New System.Windows.Forms.CheckBox()
        Me.CMBlblActivo = New System.Windows.Forms.Label()
        Me.CMBlblRfc = New System.Windows.Forms.Label()
        Me.cmbRfc = New System.Windows.Forms.ComboBox()
        Me.txtClvCompania = New System.Windows.Forms.TextBox()
        Me.CMBlbClvCompania = New System.Windows.Forms.Label()
        Me.CMBlblNombreCompania = New System.Windows.Forms.Label()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.CMBLABEL = New System.Windows.Forms.Label()
        Me.gbxServicios = New System.Windows.Forms.GroupBox()
        Me.cmbServicios = New System.Windows.Forms.ComboBox()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.dgvServicios = New System.Windows.Forms.DataGridView()
        Me.idRelCompaniaServicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Servicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clvTipServ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clvCompania = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.GroupBox3.SuspendLayout()
        Me.gbxServicios.SuspendLayout()
        CType(Me.dgvServicios, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtNombreCompania
        '
        Me.txtNombreCompania.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNombreCompania.Location = New System.Drawing.Point(149, 53)
        Me.txtNombreCompania.Multiline = True
        Me.txtNombreCompania.Name = "txtNombreCompania"
        Me.txtNombreCompania.Size = New System.Drawing.Size(417, 56)
        Me.txtNombreCompania.TabIndex = 0
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.cbxActivo)
        Me.GroupBox3.Controls.Add(Me.CMBlblActivo)
        Me.GroupBox3.Controls.Add(Me.CMBlblRfc)
        Me.GroupBox3.Controls.Add(Me.cmbRfc)
        Me.GroupBox3.Controls.Add(Me.txtClvCompania)
        Me.GroupBox3.Controls.Add(Me.CMBlbClvCompania)
        Me.GroupBox3.Controls.Add(Me.txtNombreCompania)
        Me.GroupBox3.Controls.Add(Me.CMBlblNombreCompania)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(575, 171)
        Me.GroupBox3.TabIndex = 151
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Datos Compañia"
        '
        'cbxActivo
        '
        Me.cbxActivo.AutoSize = True
        Me.cbxActivo.Location = New System.Drawing.Point(149, 148)
        Me.cbxActivo.Name = "cbxActivo"
        Me.cbxActivo.Size = New System.Drawing.Size(15, 14)
        Me.cbxActivo.TabIndex = 2
        Me.cbxActivo.UseVisualStyleBackColor = True
        '
        'CMBlblActivo
        '
        Me.CMBlblActivo.AutoSize = True
        Me.CMBlblActivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblActivo.Location = New System.Drawing.Point(85, 147)
        Me.CMBlblActivo.Name = "CMBlblActivo"
        Me.CMBlblActivo.Size = New System.Drawing.Size(56, 15)
        Me.CMBlblActivo.TabIndex = 149
        Me.CMBlblActivo.Text = "Activo  :"
        '
        'CMBlblRfc
        '
        Me.CMBlblRfc.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblRfc.Location = New System.Drawing.Point(44, 107)
        Me.CMBlblRfc.Name = "CMBlblRfc"
        Me.CMBlblRfc.Size = New System.Drawing.Size(99, 32)
        Me.CMBlblRfc.TabIndex = 148
        Me.CMBlblRfc.Text = "Razón Social Compañía  :"
        '
        'cmbRfc
        '
        Me.cmbRfc.DisplayMember = "razon_social"
        Me.cmbRfc.FormattingEnabled = True
        Me.cmbRfc.Location = New System.Drawing.Point(149, 115)
        Me.cmbRfc.Name = "cmbRfc"
        Me.cmbRfc.Size = New System.Drawing.Size(417, 24)
        Me.cmbRfc.TabIndex = 1
        Me.cmbRfc.ValueMember = "id_compania"
        '
        'txtClvCompania
        '
        Me.txtClvCompania.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClvCompania.Location = New System.Drawing.Point(149, 21)
        Me.txtClvCompania.Name = "txtClvCompania"
        Me.txtClvCompania.ReadOnly = True
        Me.txtClvCompania.Size = New System.Drawing.Size(128, 26)
        Me.txtClvCompania.TabIndex = 146
        Me.txtClvCompania.TabStop = False
        '
        'CMBlbClvCompania
        '
        Me.CMBlbClvCompania.AutoSize = True
        Me.CMBlbClvCompania.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlbClvCompania.Location = New System.Drawing.Point(93, 27)
        Me.CMBlbClvCompania.Name = "CMBlbClvCompania"
        Me.CMBlbClvCompania.Size = New System.Drawing.Size(50, 15)
        Me.CMBlbClvCompania.TabIndex = 145
        Me.CMBlbClvCompania.Text = "Clave :"
        '
        'CMBlblNombreCompania
        '
        Me.CMBlblNombreCompania.AutoSize = True
        Me.CMBlblNombreCompania.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblNombreCompania.Location = New System.Drawing.Point(50, 53)
        Me.CMBlblNombreCompania.Name = "CMBlblNombreCompania"
        Me.CMBlblNombreCompania.Size = New System.Drawing.Size(91, 15)
        Me.CMBlblNombreCompania.TabIndex = 143
        Me.CMBlblNombreCompania.Text = "Descripción :"
        '
        'btnSalir
        '
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(451, 366)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(136, 36)
        Me.btnSalir.TabIndex = 1
        Me.btnSalir.Text = "&SALIR"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.Location = New System.Drawing.Point(309, 366)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(136, 36)
        Me.btnGuardar.TabIndex = 0
        Me.btnGuardar.Text = "&GUARDAR"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'CMBLABEL
        '
        Me.CMBLABEL.AutoSize = True
        Me.CMBLABEL.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLABEL.ForeColor = System.Drawing.Color.Brown
        Me.CMBLABEL.Location = New System.Drawing.Point(957, 697)
        Me.CMBLABEL.Name = "CMBLABEL"
        Me.CMBLABEL.Size = New System.Drawing.Size(0, 16)
        Me.CMBLABEL.TabIndex = 150
        '
        'gbxServicios
        '
        Me.gbxServicios.Controls.Add(Me.cmbServicios)
        Me.gbxServicios.Controls.Add(Me.btnEliminar)
        Me.gbxServicios.Controls.Add(Me.btnAgregar)
        Me.gbxServicios.Controls.Add(Me.dgvServicios)
        Me.gbxServicios.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxServicios.Location = New System.Drawing.Point(12, 189)
        Me.gbxServicios.Name = "gbxServicios"
        Me.gbxServicios.Size = New System.Drawing.Size(575, 171)
        Me.gbxServicios.TabIndex = 152
        Me.gbxServicios.TabStop = False
        Me.gbxServicios.Text = "Servicios"
        '
        'cmbServicios
        '
        Me.cmbServicios.DisplayMember = "Concepto"
        Me.cmbServicios.FormattingEnabled = True
        Me.cmbServicios.Location = New System.Drawing.Point(9, 59)
        Me.cmbServicios.Name = "cmbServicios"
        Me.cmbServicios.Size = New System.Drawing.Size(214, 24)
        Me.cmbServicios.TabIndex = 150
        Me.cmbServicios.ValueMember = "Clv_TipSer"
        '
        'btnEliminar
        '
        Me.btnEliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminar.Location = New System.Drawing.Point(127, 104)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(96, 23)
        Me.btnEliminar.TabIndex = 151
        Me.btnEliminar.Text = "&Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnAgregar
        '
        Me.btnAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregar.Location = New System.Drawing.Point(9, 104)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(96, 23)
        Me.btnAgregar.TabIndex = 150
        Me.btnAgregar.Text = "&Agregar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'dgvServicios
        '
        Me.dgvServicios.AllowUserToAddRows = False
        Me.dgvServicios.AllowUserToDeleteRows = False
        Me.dgvServicios.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgvServicios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvServicios.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.idRelCompaniaServicio, Me.Servicio, Me.clvTipServ, Me.clvCompania})
        Me.dgvServicios.Location = New System.Drawing.Point(229, 15)
        Me.dgvServicios.Name = "dgvServicios"
        Me.dgvServicios.ReadOnly = True
        Me.dgvServicios.RowHeadersVisible = False
        Me.dgvServicios.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvServicios.Size = New System.Drawing.Size(335, 150)
        Me.dgvServicios.TabIndex = 0
        '
        'idRelCompaniaServicio
        '
        Me.idRelCompaniaServicio.DataPropertyName = "idRelCompaniaServ"
        Me.idRelCompaniaServicio.HeaderText = "Id"
        Me.idRelCompaniaServicio.Name = "idRelCompaniaServicio"
        Me.idRelCompaniaServicio.ReadOnly = True
        Me.idRelCompaniaServicio.Visible = False
        '
        'Servicio
        '
        Me.Servicio.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Servicio.DataPropertyName = "Servicio"
        Me.Servicio.HeaderText = "Servicio"
        Me.Servicio.Name = "Servicio"
        Me.Servicio.ReadOnly = True
        '
        'clvTipServ
        '
        Me.clvTipServ.DataPropertyName = "clvTipServ"
        Me.clvTipServ.HeaderText = "Clave Servicio"
        Me.clvTipServ.Name = "clvTipServ"
        Me.clvTipServ.ReadOnly = True
        Me.clvTipServ.Visible = False
        '
        'clvCompania
        '
        Me.clvCompania.DataPropertyName = "clvCompania"
        Me.clvCompania.HeaderText = "Compania"
        Me.clvCompania.Name = "clvCompania"
        Me.clvCompania.ReadOnly = True
        Me.clvCompania.Visible = False
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'FrmCompanias
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(599, 412)
        Me.Controls.Add(Me.gbxServicios)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.CMBLABEL)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnSalir)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmCompanias"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Pantalla de Captura de la Compañia"
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.gbxServicios.ResumeLayout(False)
        CType(Me.dgvServicios, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtNombreCompania As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents CMBlblNombreCompania As System.Windows.Forms.Label
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents CMBLABEL As System.Windows.Forms.Label
    Friend WithEvents CMBlbClvCompania As System.Windows.Forms.Label
    Friend WithEvents txtClvCompania As System.Windows.Forms.TextBox
    Friend WithEvents CMBlblRfc As System.Windows.Forms.Label
    Friend WithEvents cmbRfc As System.Windows.Forms.ComboBox
    Friend WithEvents cbxActivo As System.Windows.Forms.CheckBox
    Friend WithEvents CMBlblActivo As System.Windows.Forms.Label
    Friend WithEvents gbxServicios As System.Windows.Forms.GroupBox
    Friend WithEvents cmbServicios As System.Windows.Forms.ComboBox
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents dgvServicios As System.Windows.Forms.DataGridView
    Friend WithEvents idRelCompaniaServicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Servicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clvTipServ As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clvCompania As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
