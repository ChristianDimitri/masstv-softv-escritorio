﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmIANTE
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmIANTE))
        Me.panelIANTE = New System.Windows.Forms.Panel()
        Me.CMBAntenaPor = New System.Windows.Forms.Label()
        Me.cbAntenasPorAsignar = New System.Windows.Forms.ComboBox()
        Me.CMBAntena = New System.Windows.Forms.Label()
        Me.cbAntenasDisponibles = New System.Windows.Forms.ComboBox()
        Me.bnIANTE = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbBorrar = New System.Windows.Forms.ToolStripButton()
        Me.tsbGuardar = New System.Windows.Forms.ToolStripButton()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.panelIANTE.SuspendLayout()
        CType(Me.bnIANTE, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnIANTE.SuspendLayout()
        Me.SuspendLayout()
        '
        'panelIANTE
        '
        Me.panelIANTE.BackColor = System.Drawing.Color.WhiteSmoke
        Me.panelIANTE.Controls.Add(Me.CMBAntenaPor)
        Me.panelIANTE.Controls.Add(Me.cbAntenasPorAsignar)
        Me.panelIANTE.Controls.Add(Me.CMBAntena)
        Me.panelIANTE.Controls.Add(Me.cbAntenasDisponibles)
        Me.panelIANTE.Controls.Add(Me.bnIANTE)
        Me.panelIANTE.Location = New System.Drawing.Point(12, 12)
        Me.panelIANTE.Name = "panelIANTE"
        Me.panelIANTE.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.panelIANTE.Size = New System.Drawing.Size(506, 186)
        Me.panelIANTE.TabIndex = 35
        '
        'CMBAntenaPor
        '
        Me.CMBAntenaPor.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBAntenaPor.ForeColor = System.Drawing.Color.SlateGray
        Me.CMBAntenaPor.Location = New System.Drawing.Point(37, 68)
        Me.CMBAntenaPor.Name = "CMBAntenaPor"
        Me.CMBAntenaPor.Size = New System.Drawing.Size(139, 15)
        Me.CMBAntenaPor.TabIndex = 48
        Me.CMBAntenaPor.Text = "Antenas por Asignar:"
        Me.CMBAntenaPor.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cbAntenasPorAsignar
        '
        Me.cbAntenasPorAsignar.DisplayMember = "MacCablemodem"
        Me.cbAntenasPorAsignar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cbAntenasPorAsignar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbAntenasPorAsignar.FormattingEnabled = True
        Me.cbAntenasPorAsignar.Location = New System.Drawing.Point(182, 60)
        Me.cbAntenasPorAsignar.Name = "cbAntenasPorAsignar"
        Me.cbAntenasPorAsignar.Size = New System.Drawing.Size(281, 23)
        Me.cbAntenasPorAsignar.TabIndex = 47
        Me.cbAntenasPorAsignar.ValueMember = "ContratoAnt"
        '
        'CMBAntena
        '
        Me.CMBAntena.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBAntena.ForeColor = System.Drawing.Color.SlateGray
        Me.CMBAntena.Location = New System.Drawing.Point(34, 124)
        Me.CMBAntena.Name = "CMBAntena"
        Me.CMBAntena.Size = New System.Drawing.Size(142, 15)
        Me.CMBAntena.TabIndex = 46
        Me.CMBAntena.Text = "Antenas Disponibles:"
        Me.CMBAntena.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cbAntenasDisponibles
        '
        Me.cbAntenasDisponibles.DisplayMember = "MacCablemodem"
        Me.cbAntenasDisponibles.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cbAntenasDisponibles.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbAntenasDisponibles.FormattingEnabled = True
        Me.cbAntenasDisponibles.Location = New System.Drawing.Point(182, 116)
        Me.cbAntenasDisponibles.Name = "cbAntenasDisponibles"
        Me.cbAntenasDisponibles.Size = New System.Drawing.Size(281, 23)
        Me.cbAntenasDisponibles.TabIndex = 44
        Me.cbAntenasDisponibles.ValueMember = "Clv_Cablemodem"
        '
        'bnIANTE
        '
        Me.bnIANTE.AddNewItem = Nothing
        Me.bnIANTE.CountItem = Nothing
        Me.bnIANTE.DeleteItem = Me.tsbBorrar
        Me.bnIANTE.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbBorrar, Me.tsbGuardar})
        Me.bnIANTE.Location = New System.Drawing.Point(0, 0)
        Me.bnIANTE.MoveFirstItem = Nothing
        Me.bnIANTE.MoveLastItem = Nothing
        Me.bnIANTE.MoveNextItem = Nothing
        Me.bnIANTE.MovePreviousItem = Nothing
        Me.bnIANTE.Name = "bnIANTE"
        Me.bnIANTE.PositionItem = Nothing
        Me.bnIANTE.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnIANTE.Size = New System.Drawing.Size(506, 25)
        Me.bnIANTE.TabIndex = 40
        '
        'tsbBorrar
        '
        Me.tsbBorrar.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tsbBorrar.Image = CType(resources.GetObject("tsbBorrar.Image"), System.Drawing.Image)
        Me.tsbBorrar.Name = "tsbBorrar"
        Me.tsbBorrar.RightToLeftAutoMirrorImage = True
        Me.tsbBorrar.Size = New System.Drawing.Size(140, 22)
        Me.tsbBorrar.Text = "&BORRAR Aparato"
        '
        'tsbGuardar
        '
        Me.tsbGuardar.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tsbGuardar.Image = CType(resources.GetObject("tsbGuardar.Image"), System.Drawing.Image)
        Me.tsbGuardar.Name = "tsbGuardar"
        Me.tsbGuardar.Size = New System.Drawing.Size(87, 22)
        Me.tsbGuardar.Text = "&ACEPTAR"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(401, 228)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 36
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'FrmIANTE
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(549, 276)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.panelIANTE)
        Me.Name = "FrmIANTE"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Asignación de Antena"
        Me.panelIANTE.ResumeLayout(False)
        Me.panelIANTE.PerformLayout()
        CType(Me.bnIANTE, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnIANTE.ResumeLayout(False)
        Me.bnIANTE.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents panelIANTE As System.Windows.Forms.Panel
    Friend WithEvents bnIANTE As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbBorrar As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbGuardar As System.Windows.Forms.ToolStripButton
    Friend WithEvents CMBAntena As System.Windows.Forms.Label
    Friend WithEvents cbAntenasDisponibles As System.Windows.Forms.ComboBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents CMBAntenaPor As System.Windows.Forms.Label
    Friend WithEvents cbAntenasPorAsignar As System.Windows.Forms.ComboBox
End Class
