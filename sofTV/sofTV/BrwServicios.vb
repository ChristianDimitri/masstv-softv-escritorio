Imports System.IO
Imports System.Data.SqlClient

Public Class BrwServicios
    Dim fileExistEric As Boolean = False
    Dim opt As Integer

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub BrwServicios_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            BUSCA(Me.ComboBox4.SelectedValue, 2)
        End If


        'fileExistEric = My.Computer.FileSystem.FileExists(RutaReportes + "\SofPa3.txt")
        'If fileExistEric = True Then
        '    fileExistEric = False
        '    File.Delete(RutaReportes + "\SofPa3.txt")
        '    BUSCA(Me.ComboBox4.SelectedValue, 2)
        'End If
    End Sub

    Private Sub BrwServicios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetEDGAR.MuestraTipSerPrincipal_SER' Puede moverla o quitarla seg�n sea necesario.
        Me.MuestraTipSerPrincipal_SERTableAdapter.Connection = CON
        Me.MuestraTipSerPrincipal_SERTableAdapter.Fill(Me.DataSetEDGAR.MuestraTipSerPrincipal_SER)

        'Me.DamePermisosFormTableAdapter.Fill(Me.NewSofTvDataSet.DamePermisosForm, GloTipoUsuario, Me.Name, 1, glolec, gloescr, gloctr)
        Me.MUESTRA_TIPOCLIENTESTableAdapter.Connection = CON
        Me.MUESTRA_TIPOCLIENTESTableAdapter.Fill(Me.DataSetarnoldo.MUESTRA_TIPOCLIENTES, 0)
        CON.Close()
        If gloescr = 1 Then
            Me.Button2.Enabled = False
            Me.Button4.Enabled = False
        End If
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerPrincipal' Puede moverla o quitarla seg�n sea necesario.
        'Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)
        BUSCA(Me.ComboBox4.SelectedValue, 2)
        Me.Button6.Visible = False


    End Sub


    Private Sub BUSCA(ByVal CLV_TIPSER As Integer, ByVal OP As Integer)
        Dim CON3 As New SqlConnection(MiConexion)

        CON3.Open()
        Try
          
            If OP = 0 Then
                If Len(Trim(Me.TextBox1.Text)) > 0 Then
                    Me.BUSCASERVICIOSTableAdapter.Connection = CON3
                    Me.BUSCASERVICIOSTableAdapter.Fill(Me.NewSofTvDataSet.BUSCASERVICIOS, New System.Nullable(Of Integer)(CType(CLV_TIPSER, Integer)), 0, "", Me.TextBox1.Text, New System.Nullable(Of Integer)(CType(0, Integer)))
                    Me.BUSCASERVICIOS2TableAdapter.Connection = CON3
                    Me.BUSCASERVICIOS2TableAdapter.Fill(Me.Procedimientosarnoldo4.BUSCASERVICIOS2, New System.Nullable(Of Integer)(CType(CLV_TIPSER, Integer)), 0, "", Me.TextBox1.Text, New System.Nullable(Of Integer)(CType(0, Integer)))
                    If IsNumeric(Me.Clv_calleLabel2.Text) = True And IsNumeric(Me.ComboBox1.SelectedValue) = True Then
                        If Me.ComboBox4.SelectedValue = 5 Then
                            Me.BUSCADET_SERVICOSTELTableAdapter.Connection = CON3
                            Me.BUSCADET_SERVICOSTELTableAdapter.Fill(Me.DataSetLidia2.BUSCADET_SERVICOSTEL, Me.Clv_calleLabel2.Text, Me.ComboBox1.SelectedValue)
                        Else
                            Me.BUSCAREL_TARIFADOS_SERVICIOSTableAdapter.Connection = CON3
                            Me.BUSCAREL_TARIFADOS_SERVICIOSTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAREL_TARIFADOS_SERVICIOS, New System.Nullable(Of Integer)(CType(Me.Clv_calleLabel2.Text, Integer)), 0, Me.ComboBox1.SelectedValue)
                        End If


                    End If
                Else
                    MsgBox("La Busqueda no se puede realizar con datos invalidos", MsgBoxStyle.Information)
                End If
            ElseIf OP = 1 Then
                If Len(Trim(Me.TextBox2.Text)) > 0 Then
                    Me.BUSCASERVICIOSTableAdapter.Connection = CON3
                    Me.BUSCASERVICIOSTableAdapter.Fill(Me.NewSofTvDataSet.BUSCASERVICIOS, New System.Nullable(Of Integer)(CType(CLV_TIPSER, Integer)), 0, Me.TextBox2.Text, "", New System.Nullable(Of Integer)(CType(1, Integer)))
                    Me.BUSCASERVICIOS2TableAdapter.Connection = CON3
                    Me.BUSCASERVICIOS2TableAdapter.Fill(Me.Procedimientosarnoldo4.BUSCASERVICIOS2, New System.Nullable(Of Integer)(CType(CLV_TIPSER, Integer)), 0, Me.TextBox2.Text, "", New System.Nullable(Of Integer)(CType(1, Integer)))
                    
                    If IsNumeric(Me.Clv_calleLabel2.Text) = True And IsNumeric(Me.ComboBox1.SelectedValue) = True Then
                        If Me.ComboBox4.SelectedValue = 5 Then
                            Me.BUSCADET_SERVICOSTELTableAdapter.Connection = CON3
                            Me.BUSCADET_SERVICOSTELTableAdapter.Fill(Me.DataSetLidia2.BUSCADET_SERVICOSTEL, Me.Clv_calleLabel2.Text, Me.ComboBox1.SelectedValue)
                        Else
                            Me.BUSCAREL_TARIFADOS_SERVICIOSTableAdapter.Connection = CON3
                            Me.BUSCAREL_TARIFADOS_SERVICIOSTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAREL_TARIFADOS_SERVICIOS, New System.Nullable(Of Integer)(CType(Me.Clv_calleLabel2.Text, Integer)), 0, Me.ComboBox1.SelectedValue)
                        End If
                    End If
                    Else
                        MsgBox("La Busqueda no se puede realizar con datos invalidos", MsgBoxStyle.Information)
                    End If
            Else
                Me.BUSCASERVICIOSTableAdapter.Connection = CON3
                Me.BUSCASERVICIOSTableAdapter.Fill(Me.NewSofTvDataSet.BUSCASERVICIOS, New System.Nullable(Of Integer)(CType(CLV_TIPSER, Integer)), 0, "", "", New System.Nullable(Of Integer)(CType(2, Integer)))
                Me.BUSCASERVICIOS2TableAdapter.Connection = CON3
                Me.BUSCASERVICIOS2TableAdapter.Fill(Me.Procedimientosarnoldo4.BUSCASERVICIOS2, New System.Nullable(Of Integer)(CType(CLV_TIPSER, Integer)), 0, "", "", New System.Nullable(Of Integer)(CType(2, Integer)))
                If IsNumeric(Me.Clv_calleLabel2.Text) = True And IsNumeric(Me.ComboBox1.SelectedValue) = True Then
                    If Me.ComboBox4.SelectedValue = 5 Then
                        Me.BUSCADET_SERVICOSTELTableAdapter.Connection = CON3
                        Me.BUSCADET_SERVICOSTELTableAdapter.Fill(Me.DataSetLidia2.BUSCADET_SERVICOSTEL, Me.Clv_calleLabel2.Text, Me.ComboBox1.SelectedValue)
                    Else
                        Me.BUSCAREL_TARIFADOS_SERVICIOSTableAdapter.Connection = CON3
                        Me.BUSCAREL_TARIFADOS_SERVICIOSTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAREL_TARIFADOS_SERVICIOS, New System.Nullable(Of Integer)(CType(Me.Clv_calleLabel2.Text, Integer)), 0, Me.ComboBox1.SelectedValue)
                    End If
                End If
                End If
            Me.TextBox1.Clear()
            Me.TextBox2.Clear()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON3.Close()
    End Sub

    Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.SelectedIndexChanged
        GloClv_TipSer = Me.ComboBox4.SelectedValue
        BUSCA(Me.ComboBox4.SelectedValue, 2)
        If Me.ComboBox4.SelectedValue = 5 Then
            Me.Panel2.Show()
            Me.Panel3.Hide()
        Else
            Me.Panel2.Hide()
            Me.Panel3.Show()
        End If
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        BUSCA(Me.ComboBox4.SelectedValue, 0)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        BUSCA(Me.ComboBox4.SelectedValue, 1)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            BUSCA(Me.ComboBox4.SelectedValue, 0)
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            BUSCA(Me.ComboBox4.SelectedValue, 1)
        End If
    End Sub

    Private Sub Clv_calleLabel2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Clv_calleLabel2.TextChanged
        GloClv_Servicio = Me.Clv_calleLabel2.Text
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opcion = "N"
        GloClv_TipSer = Me.ComboBox4.SelectedValue
        GloClv_Servicio = 0 'Me.Clv_calleLabel2.Text
        If Me.ComboBox4.SelectedValue <> 4 And Me.ComboBox4.SelectedValue <> 5 Then
            FrmServicios.Show()
        ElseIf Me.ComboBox4.SelectedValue = 4 Then
            Dim txt As String
            Dim myProcess As New Process()

            Using sw As StreamWriter = File.CreateText("C:\SofConex.txt")
                txt = MiConexion
                sw.Write(txt)
                sw.Close()
            End Using

            Using sw As StreamWriter = File.CreateText(RutaReportes + "\SofPa2.txt")
                txt = "FrmServicioPPE" + "," + CStr(opcion) + "," + CStr("0") + "," + CStr("0")
                sw.Write(txt)
                sw.Close()
            End Using
            Dim myProcessStartInfo As New ProcessStartInfo(RutaReportes + "\SoftvPPE.application")
            myProcess.StartInfo = myProcessStartInfo
            myProcess.Start()
        ElseIf Me.ComboBox4.SelectedValue = 5 Then
            FrmServiciosTelefonia.Show()
        End If

    End Sub
    Private Sub consultar()
        Dim CON2 As New SqlConnection(MiConexion)
        CON2.Open()
        If Me.Clv_calleLabel2.Text > 0 Then
            opcion = "C"
            GloClv_TipSer = Me.ComboBox4.SelectedValue
            GloClv_Servicio = Me.Clv_calleLabel2.Text

            If Me.ComboBox4.SelectedValue <> 4 And Me.ComboBox4.SelectedValue <> 5 Then
                FrmServicios.Show()
            ElseIf Me.ComboBox4.SelectedValue = 4 Then
                Me.DameClv_PPETableAdapter.Connection = CON2
                Me.DameClv_PPETableAdapter.Fill(Me.DataSetEric.DameClv_PPE, Me.Clv_calleLabel2.Text)
                Dim txt As String
                Dim myProcess As New Process()
                Using sw As StreamWriter = File.CreateText("C:\SofConex.txt")
                    txt = MiConexion
                    sw.Write(txt)
                    sw.Close()
                End Using
                Using sw As StreamWriter = File.CreateText(RutaReportes + "\SofPa2.txt")
                    txt = "FrmServicioPPE" + "," + CStr(opcion) + "," + CStr(GloClv_Servicio) + "," + CStr(Me.Clv_PPELabel1.Text)
                    sw.Write(txt)
                    sw.Close()
                End Using
                Dim myProcessStartInfo As New ProcessStartInfo(RutaReportes + "\SoftvPPE.application")
                myProcess.StartInfo = myProcessStartInfo
                myProcess.Start()
            ElseIf Me.ComboBox4.SelectedValue = 5 Then
                FrmServiciosTelefonia.Show()
            End If
        Else
            MsgBox(mensaje1, MsgBoxStyle.Information)
        End If
        CON2.Close()

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.DataGridView1.RowCount > 0 Then
            consultar()
        Else
            MsgBox(mensaje2)
        End If
    End Sub
    Private Sub Modificar()
        Dim CON2 As New SqlConnection(MiConexion)
        CON2.Open()
        If Me.Clv_calleLabel2.Text > 0 Then
            opcion = "M"
            GloClv_TipSer = Me.ComboBox4.SelectedValue
            GloClv_Servicio = Me.Clv_calleLabel2.Text

            If Me.ComboBox4.SelectedValue <> 4 And Me.ComboBox4.SelectedValue <> 5 Then
                opcFrm = 9
                Acceso_TipoServicios.Show()
            ElseIf Me.ComboBox4.SelectedValue = 4 Then
                Me.DameClv_PPETableAdapter.Connection = CON2
                Me.DameClv_PPETableAdapter.Fill(Me.DataSetEric.DameClv_PPE, Me.Clv_calleLabel2.Text)
                Dim txt As String
                Dim myProcess As New Process()
                Using sw As StreamWriter = File.CreateText("C:\SofConex.txt")
                    txt = MiConexion
                    sw.Write(txt)
                    sw.Close()
                End Using
                Using sw As StreamWriter = File.CreateText(RutaReportes + "\SofPa2.txt")
                    txt = "FrmServicioPPE" + "," + CStr(opcion) + "," + CStr(GloClv_Servicio) + "," + CStr(Me.Clv_PPELabel1.Text)
                    sw.Write(txt)
                    sw.Close()
                End Using
                Dim myProcessStartInfo As New ProcessStartInfo(RutaReportes + "\SoftvPPE.application")
                myProcess.StartInfo = myProcessStartInfo
                myProcess.Start()
            ElseIf Me.ComboBox4.SelectedValue = 5 Then
                FrmServiciosTelefonia.Show()
            End If
        Else
            MsgBox(mensaje1, MsgBoxStyle.Information)
        End If
        CON2.Close()
    End Sub
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If Me.DataGridView1.RowCount > 0 Then
            Modificar()
        Else
            MsgBox(mensaje1)
        End If

    End Sub

    Private Sub DataGridView1_CurrentCellChanged(sender As Object, e As System.EventArgs) Handles DataGridView1.CurrentCellChanged
        Try
            Me.CLV_TXTLabel1.Text = Me.DataGridView1.SelectedCells(1).Value.ToString
            Me.CMBNombreTextBox.Text = Me.DataGridView1.SelectedCells(2).Value.ToString
            Me.PrecioLabel1.Text = Me.DataGridView1.SelectedCells(3).Value.ToString
            Me.Clv_calleLabel2.Text = Me.DataGridView1.SelectedCells(0).Value.ToString
        Catch ex As Exception

        End Try
    End Sub


    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If Button3.Enabled = True Then
            consultar()
        ElseIf Button4.Enabled = True Then
            Modificar()
        End If
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Dim CON As New SqlConnection(MiConexion)
        If Me.ComboBox4.SelectedValue = 5 Then
            CON.Open()
            Me.BUSCADET_SERVICOSTELTableAdapter.Connection = CON
            Me.BUSCADET_SERVICOSTELTableAdapter.Fill(Me.DataSetLidia2.BUSCADET_SERVICOSTEL, GloClv_Servicio, Me.ComboBox1.SelectedValue)
            CON.Close()
        Else
            CON.Open()
            Me.BUSCAREL_TARIFADOS_SERVICIOSTableAdapter.Connection = CON
            Me.BUSCAREL_TARIFADOS_SERVICIOSTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAREL_TARIFADOS_SERVICIOS, New System.Nullable(Of Integer)(CType(GloClv_Servicio, Integer)), 0, Me.ComboBox1.SelectedValue)
            CON.Close()
        End If
    End Sub


    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        Dim CON As New SqlConnection(MiConexion)
        If Me.ComboBox4.SelectedValue = 5 Then
            CON.Open()
            Me.BUSCADET_SERVICOSTELTableAdapter.Connection = CON
            Me.BUSCADET_SERVICOSTELTableAdapter.Fill(Me.DataSetLidia2.BUSCADET_SERVICOSTEL, GloClv_Servicio, Me.ComboBox1.SelectedValue)
            CON.Close()
        Else
            CON.Open()
            Me.BUSCAREL_TARIFADOS_SERVICIOSTableAdapter.Connection = CON
            Me.BUSCAREL_TARIFADOS_SERVICIOSTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAREL_TARIFADOS_SERVICIOS, New System.Nullable(Of Integer)(CType(GloClv_Servicio, Integer)), 0, Me.ComboBox1.SelectedValue)
            CON.Close()
        End If
    End Sub

    Private Sub Clv_calleLabel2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_calleLabel2.Click

    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Dim cmd As New SqlClient.SqlCommand
        Dim Cone As New SqlClient.SqlConnection(MiConexion)
        Dim sale As Integer
        If Me.ComboBox4.SelectedValue <> 5 Then
            Cone.Open()
            With cmd
                .CommandText = "Dime_Si_Cartera"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = Cone
                Dim Prm As New SqlParameter("@clv_servicio", SqlDbType.Int)
                Dim Prm2 As New SqlParameter("@sale", SqlDbType.Int)
                Prm.Direction = ParameterDirection.Input
                Prm2.Direction = ParameterDirection.Output
                Prm.Value = Me.Clv_calleLabel2.Text
                Prm2.Value = 0
                .Parameters.Add(Prm)
                .Parameters.Add(Prm2)
                Dim i As Integer = .ExecuteNonQuery
                sale = Prm2.Value
            End With
            Cone.Close()
            If sale = 1 Then
                GloClv_Servicio = Me.Clv_calleLabel2.Text
                LocreportServicio = True
                FrmImprimirContrato.Show()
            ElseIf sale = 0 Then
                MsgBox("Este Servicio no se Puede Imprimir", MsgBoxStyle.Information)
            End If
        ElseIf Me.ComboBox4.SelectedValue = 5 Then
            MsgBox("Este Servicio no se Puede Imprimir", MsgBoxStyle.Information)
        End If
       
    End Sub

  
    
    Private Sub SplitContainer1_Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles SplitContainer1.Panel1.Paint

    End Sub
End Class