<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmConsultaCobro
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle25 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.ClvSessionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLVSERVICIODataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClvllavedelservicioDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClvUnicaNetDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLAVEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MACCABLEMODEM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DESCORTADataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Pagos_Adelantados = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.TvAdicDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MesesCortesiaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MesesApagarDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImporteDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PeriodoPagadoIniDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PeriodoPagadoFinDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PuntosAplicadosOtrosDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PuntosAplicadosAntDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescuentoNet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Des_Otr_Ser_Misma_Categoria = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BonificacionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImporteAdicionalDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnaDetalleDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DiasBonificaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MesesBonificarDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImporteBonificaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UltimoMesDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UltimoanioDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AdelantadoDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DESCRIPCIONDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLV_DETALLE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DameDetalleBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.SumaDetalleDataGridView = New System.Windows.Forms.DataGridView()
        Me.ClvSessionDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PosicionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NivelDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescripcionDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SumaDetalleBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Clv_SessionTextBox = New System.Windows.Forms.TextBox()
        Me.DameDetalleTableAdapter = New sofTV.DataSetEricTableAdapters.DameDetalleTableAdapter()
        Me.SumaDetalleTableAdapter = New sofTV.DataSetEricTableAdapters.SumaDetalleTableAdapter()
        Me.CobraBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CobraTableAdapter = New sofTV.DataSetEricTableAdapters.CobraTableAdapter()
        Me.BUSCLIPORCONTRATO_FACBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BUSCLIPORCONTRATO_FACTableAdapter = New sofTV.DataSetEricTableAdapters.BUSCLIPORCONTRATO_FACTableAdapter()
        Me.BorraClv_SessionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorraClv_SessionTableAdapter = New sofTV.DataSetEricTableAdapters.BorraClv_SessionTableAdapter()
        Me.Button3 = New System.Windows.Forms.Button()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameDetalleBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SumaDetalleDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SumaDetalleBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CobraBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BUSCLIPORCONTRATO_FACBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorraClv_SessionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.DarkOrange
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ClvSessionDataGridViewTextBoxColumn, Me.CLVSERVICIODataGridViewTextBoxColumn, Me.ClvllavedelservicioDataGridViewTextBoxColumn, Me.ClvUnicaNetDataGridViewTextBoxColumn, Me.CLAVEDataGridViewTextBoxColumn, Me.MACCABLEMODEM, Me.DESCORTADataGridViewTextBoxColumn, Me.Pagos_Adelantados, Me.TvAdicDataGridViewTextBoxColumn, Me.MesesCortesiaDataGridViewTextBoxColumn, Me.MesesApagarDataGridViewTextBoxColumn, Me.ImporteDataGridViewTextBoxColumn, Me.PeriodoPagadoIniDataGridViewTextBoxColumn, Me.PeriodoPagadoFinDataGridViewTextBoxColumn, Me.PuntosAplicadosOtrosDataGridViewTextBoxColumn, Me.PuntosAplicadosAntDataGridViewTextBoxColumn, Me.PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn, Me.DescuentoNet, Me.Des_Otr_Ser_Misma_Categoria, Me.BonificacionDataGridViewTextBoxColumn, Me.ImporteAdicionalDataGridViewTextBoxColumn, Me.ColumnaDetalleDataGridViewTextBoxColumn, Me.DiasBonificaDataGridViewTextBoxColumn, Me.MesesBonificarDataGridViewTextBoxColumn, Me.ImporteBonificaDataGridViewTextBoxColumn, Me.UltimoMesDataGridViewTextBoxColumn, Me.UltimoanioDataGridViewTextBoxColumn, Me.AdelantadoDataGridViewCheckBoxColumn, Me.DESCRIPCIONDataGridViewTextBoxColumn, Me.CLV_DETALLE})
        Me.DataGridView1.DataSource = Me.DameDetalleBindingSource
        Me.DataGridView1.GridColor = System.Drawing.Color.DimGray
        Me.DataGridView1.Location = New System.Drawing.Point(6, 12)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(998, 256)
        Me.DataGridView1.TabIndex = 188
        Me.DataGridView1.TabStop = False
        '
        'ClvSessionDataGridViewTextBoxColumn
        '
        Me.ClvSessionDataGridViewTextBoxColumn.DataPropertyName = "Clv_Session"
        Me.ClvSessionDataGridViewTextBoxColumn.HeaderText = "Clv_Session"
        Me.ClvSessionDataGridViewTextBoxColumn.Name = "ClvSessionDataGridViewTextBoxColumn"
        Me.ClvSessionDataGridViewTextBoxColumn.ReadOnly = True
        Me.ClvSessionDataGridViewTextBoxColumn.Visible = False
        '
        'CLVSERVICIODataGridViewTextBoxColumn
        '
        Me.CLVSERVICIODataGridViewTextBoxColumn.DataPropertyName = "CLV_SERVICIO"
        Me.CLVSERVICIODataGridViewTextBoxColumn.HeaderText = "CLV_SERVICIO"
        Me.CLVSERVICIODataGridViewTextBoxColumn.Name = "CLVSERVICIODataGridViewTextBoxColumn"
        Me.CLVSERVICIODataGridViewTextBoxColumn.ReadOnly = True
        Me.CLVSERVICIODataGridViewTextBoxColumn.Visible = False
        '
        'ClvllavedelservicioDataGridViewTextBoxColumn
        '
        Me.ClvllavedelservicioDataGridViewTextBoxColumn.DataPropertyName = "Clv_llavedelservicio"
        Me.ClvllavedelservicioDataGridViewTextBoxColumn.HeaderText = "Clv_llavedelservicio"
        Me.ClvllavedelservicioDataGridViewTextBoxColumn.Name = "ClvllavedelservicioDataGridViewTextBoxColumn"
        Me.ClvllavedelservicioDataGridViewTextBoxColumn.ReadOnly = True
        Me.ClvllavedelservicioDataGridViewTextBoxColumn.Visible = False
        '
        'ClvUnicaNetDataGridViewTextBoxColumn
        '
        Me.ClvUnicaNetDataGridViewTextBoxColumn.DataPropertyName = "Clv_UnicaNet"
        Me.ClvUnicaNetDataGridViewTextBoxColumn.HeaderText = "Clv_UnicaNet"
        Me.ClvUnicaNetDataGridViewTextBoxColumn.Name = "ClvUnicaNetDataGridViewTextBoxColumn"
        Me.ClvUnicaNetDataGridViewTextBoxColumn.ReadOnly = True
        Me.ClvUnicaNetDataGridViewTextBoxColumn.Visible = False
        '
        'CLAVEDataGridViewTextBoxColumn
        '
        Me.CLAVEDataGridViewTextBoxColumn.DataPropertyName = "CLAVE"
        Me.CLAVEDataGridViewTextBoxColumn.HeaderText = "CLAVE"
        Me.CLAVEDataGridViewTextBoxColumn.Name = "CLAVEDataGridViewTextBoxColumn"
        Me.CLAVEDataGridViewTextBoxColumn.ReadOnly = True
        Me.CLAVEDataGridViewTextBoxColumn.Visible = False
        '
        'MACCABLEMODEM
        '
        Me.MACCABLEMODEM.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.MACCABLEMODEM.DataPropertyName = "MACCABLEMODEM"
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.MACCABLEMODEM.DefaultCellStyle = DataGridViewCellStyle2
        Me.MACCABLEMODEM.HeaderText = "Aparato"
        Me.MACCABLEMODEM.Name = "MACCABLEMODEM"
        Me.MACCABLEMODEM.ReadOnly = True
        Me.MACCABLEMODEM.Width = 81
        '
        'DESCORTADataGridViewTextBoxColumn
        '
        Me.DESCORTADataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.DESCORTADataGridViewTextBoxColumn.DataPropertyName = "DESCORTA"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DESCORTADataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle3
        Me.DESCORTADataGridViewTextBoxColumn.HeaderText = "Concepto"
        Me.DESCORTADataGridViewTextBoxColumn.Name = "DESCORTADataGridViewTextBoxColumn"
        Me.DESCORTADataGridViewTextBoxColumn.ReadOnly = True
        Me.DESCORTADataGridViewTextBoxColumn.Width = 92
        '
        'Pagos_Adelantados
        '
        Me.Pagos_Adelantados.DataPropertyName = "Pagos_Adelantados"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Pagos_Adelantados.DefaultCellStyle = DataGridViewCellStyle4
        Me.Pagos_Adelantados.HeaderText = "Pagos Adelantados"
        Me.Pagos_Adelantados.Name = "Pagos_Adelantados"
        Me.Pagos_Adelantados.ReadOnly = True
        Me.Pagos_Adelantados.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Pagos_Adelantados.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'TvAdicDataGridViewTextBoxColumn
        '
        Me.TvAdicDataGridViewTextBoxColumn.DataPropertyName = "tvAdic"
        Me.TvAdicDataGridViewTextBoxColumn.HeaderText = "Tv's Extra"
        Me.TvAdicDataGridViewTextBoxColumn.Name = "TvAdicDataGridViewTextBoxColumn"
        Me.TvAdicDataGridViewTextBoxColumn.ReadOnly = True
        Me.TvAdicDataGridViewTextBoxColumn.Width = 50
        '
        'MesesCortesiaDataGridViewTextBoxColumn
        '
        Me.MesesCortesiaDataGridViewTextBoxColumn.DataPropertyName = "Meses_Cortesia"
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MesesCortesiaDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle5
        Me.MesesCortesiaDataGridViewTextBoxColumn.HeaderText = "Meses Cortesía"
        Me.MesesCortesiaDataGridViewTextBoxColumn.Name = "MesesCortesiaDataGridViewTextBoxColumn"
        Me.MesesCortesiaDataGridViewTextBoxColumn.ReadOnly = True
        Me.MesesCortesiaDataGridViewTextBoxColumn.Width = 70
        '
        'MesesApagarDataGridViewTextBoxColumn
        '
        Me.MesesApagarDataGridViewTextBoxColumn.DataPropertyName = "mesesApagar"
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.Format = "N0"
        Me.MesesApagarDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle6
        Me.MesesApagarDataGridViewTextBoxColumn.HeaderText = "Meses a Pagar"
        Me.MesesApagarDataGridViewTextBoxColumn.Name = "MesesApagarDataGridViewTextBoxColumn"
        Me.MesesApagarDataGridViewTextBoxColumn.ReadOnly = True
        Me.MesesApagarDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MesesApagarDataGridViewTextBoxColumn.Width = 70
        '
        'ImporteDataGridViewTextBoxColumn
        '
        Me.ImporteDataGridViewTextBoxColumn.DataPropertyName = "importe"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.Format = "C2"
        DataGridViewCellStyle7.NullValue = Nothing
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ImporteDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle7
        Me.ImporteDataGridViewTextBoxColumn.HeaderText = "Importe"
        Me.ImporteDataGridViewTextBoxColumn.Name = "ImporteDataGridViewTextBoxColumn"
        Me.ImporteDataGridViewTextBoxColumn.ReadOnly = True
        Me.ImporteDataGridViewTextBoxColumn.Width = 80
        '
        'PeriodoPagadoIniDataGridViewTextBoxColumn
        '
        Me.PeriodoPagadoIniDataGridViewTextBoxColumn.DataPropertyName = "periodoPagadoIni"
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PeriodoPagadoIniDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle8
        Me.PeriodoPagadoIniDataGridViewTextBoxColumn.HeaderText = "Periodo Pagado Inicial"
        Me.PeriodoPagadoIniDataGridViewTextBoxColumn.Name = "PeriodoPagadoIniDataGridViewTextBoxColumn"
        Me.PeriodoPagadoIniDataGridViewTextBoxColumn.ReadOnly = True
        Me.PeriodoPagadoIniDataGridViewTextBoxColumn.Width = 80
        '
        'PeriodoPagadoFinDataGridViewTextBoxColumn
        '
        Me.PeriodoPagadoFinDataGridViewTextBoxColumn.DataPropertyName = "periodoPagadoFin"
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PeriodoPagadoFinDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle9
        Me.PeriodoPagadoFinDataGridViewTextBoxColumn.HeaderText = "Periodo Pagado Final"
        Me.PeriodoPagadoFinDataGridViewTextBoxColumn.Name = "PeriodoPagadoFinDataGridViewTextBoxColumn"
        Me.PeriodoPagadoFinDataGridViewTextBoxColumn.ReadOnly = True
        Me.PeriodoPagadoFinDataGridViewTextBoxColumn.Width = 80
        '
        'PuntosAplicadosOtrosDataGridViewTextBoxColumn
        '
        Me.PuntosAplicadosOtrosDataGridViewTextBoxColumn.DataPropertyName = "PuntosAplicadosOtros"
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.Format = "N0"
        DataGridViewCellStyle10.NullValue = Nothing
        Me.PuntosAplicadosOtrosDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle10
        Me.PuntosAplicadosOtrosDataGridViewTextBoxColumn.HeaderText = "Puntos Aplicados por  Pago Oportuno"
        Me.PuntosAplicadosOtrosDataGridViewTextBoxColumn.Name = "PuntosAplicadosOtrosDataGridViewTextBoxColumn"
        Me.PuntosAplicadosOtrosDataGridViewTextBoxColumn.ReadOnly = True
        Me.PuntosAplicadosOtrosDataGridViewTextBoxColumn.Width = 80
        '
        'PuntosAplicadosAntDataGridViewTextBoxColumn
        '
        Me.PuntosAplicadosAntDataGridViewTextBoxColumn.DataPropertyName = "puntosAplicadosAnt"
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.Format = "N0"
        DataGridViewCellStyle11.NullValue = Nothing
        Me.PuntosAplicadosAntDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle11
        Me.PuntosAplicadosAntDataGridViewTextBoxColumn.HeaderText = "Puntos Aplicados por Antigüedad"
        Me.PuntosAplicadosAntDataGridViewTextBoxColumn.Name = "PuntosAplicadosAntDataGridViewTextBoxColumn"
        Me.PuntosAplicadosAntDataGridViewTextBoxColumn.ReadOnly = True
        Me.PuntosAplicadosAntDataGridViewTextBoxColumn.Width = 80
        '
        'PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn
        '
        Me.PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn.DataPropertyName = "PuntosAplicadosPagoAdelantado"
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.Format = "N0"
        DataGridViewCellStyle12.NullValue = Nothing
        Me.PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle12
        Me.PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn.HeaderText = "Puntos Aplicados por Pagos Adelantados"
        Me.PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn.Name = "PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn"
        Me.PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn.ReadOnly = True
        Me.PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn.Width = 80
        '
        'DescuentoNet
        '
        Me.DescuentoNet.DataPropertyName = "DescuentoNet"
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.Format = "N0"
        DataGridViewCellStyle13.NullValue = Nothing
        Me.DescuentoNet.DefaultCellStyle = DataGridViewCellStyle13
        Me.DescuentoNet.HeaderText = "Puntos Combo"
        Me.DescuentoNet.Name = "DescuentoNet"
        Me.DescuentoNet.ReadOnly = True
        Me.DescuentoNet.Width = 60
        '
        'Des_Otr_Ser_Misma_Categoria
        '
        Me.Des_Otr_Ser_Misma_Categoria.DataPropertyName = "Des_Otr_Ser_Misma_Categoria"
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.Format = "N0"
        DataGridViewCellStyle14.NullValue = Nothing
        Me.Des_Otr_Ser_Misma_Categoria.DefaultCellStyle = DataGridViewCellStyle14
        Me.Des_Otr_Ser_Misma_Categoria.HeaderText = "Puntos Ppe"
        Me.Des_Otr_Ser_Misma_Categoria.Name = "Des_Otr_Ser_Misma_Categoria"
        Me.Des_Otr_Ser_Misma_Categoria.ReadOnly = True
        Me.Des_Otr_Ser_Misma_Categoria.Width = 60
        '
        'BonificacionDataGridViewTextBoxColumn
        '
        Me.BonificacionDataGridViewTextBoxColumn.DataPropertyName = "bonificacion"
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BonificacionDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle15
        Me.BonificacionDataGridViewTextBoxColumn.HeaderText = "bonificacion"
        Me.BonificacionDataGridViewTextBoxColumn.Name = "BonificacionDataGridViewTextBoxColumn"
        Me.BonificacionDataGridViewTextBoxColumn.ReadOnly = True
        Me.BonificacionDataGridViewTextBoxColumn.Visible = False
        '
        'ImporteAdicionalDataGridViewTextBoxColumn
        '
        Me.ImporteAdicionalDataGridViewTextBoxColumn.DataPropertyName = "importeAdicional"
        Me.ImporteAdicionalDataGridViewTextBoxColumn.HeaderText = "importeAdicional"
        Me.ImporteAdicionalDataGridViewTextBoxColumn.Name = "ImporteAdicionalDataGridViewTextBoxColumn"
        Me.ImporteAdicionalDataGridViewTextBoxColumn.ReadOnly = True
        Me.ImporteAdicionalDataGridViewTextBoxColumn.Visible = False
        '
        'ColumnaDetalleDataGridViewTextBoxColumn
        '
        Me.ColumnaDetalleDataGridViewTextBoxColumn.DataPropertyName = "columnaDetalle"
        Me.ColumnaDetalleDataGridViewTextBoxColumn.HeaderText = "columnaDetalle"
        Me.ColumnaDetalleDataGridViewTextBoxColumn.Name = "ColumnaDetalleDataGridViewTextBoxColumn"
        Me.ColumnaDetalleDataGridViewTextBoxColumn.ReadOnly = True
        Me.ColumnaDetalleDataGridViewTextBoxColumn.Visible = False
        '
        'DiasBonificaDataGridViewTextBoxColumn
        '
        Me.DiasBonificaDataGridViewTextBoxColumn.DataPropertyName = "DiasBonifica"
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DiasBonificaDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle16
        Me.DiasBonificaDataGridViewTextBoxColumn.HeaderText = "DiasBonifica"
        Me.DiasBonificaDataGridViewTextBoxColumn.Name = "DiasBonificaDataGridViewTextBoxColumn"
        Me.DiasBonificaDataGridViewTextBoxColumn.ReadOnly = True
        Me.DiasBonificaDataGridViewTextBoxColumn.Visible = False
        '
        'MesesBonificarDataGridViewTextBoxColumn
        '
        Me.MesesBonificarDataGridViewTextBoxColumn.DataPropertyName = "mesesBonificar"
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MesesBonificarDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle17
        Me.MesesBonificarDataGridViewTextBoxColumn.HeaderText = "mesesBonificar"
        Me.MesesBonificarDataGridViewTextBoxColumn.Name = "MesesBonificarDataGridViewTextBoxColumn"
        Me.MesesBonificarDataGridViewTextBoxColumn.ReadOnly = True
        Me.MesesBonificarDataGridViewTextBoxColumn.Visible = False
        '
        'ImporteBonificaDataGridViewTextBoxColumn
        '
        Me.ImporteBonificaDataGridViewTextBoxColumn.DataPropertyName = "importeBonifica"
        DataGridViewCellStyle18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ImporteBonificaDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle18
        Me.ImporteBonificaDataGridViewTextBoxColumn.HeaderText = "Importe a Bonificar"
        Me.ImporteBonificaDataGridViewTextBoxColumn.Name = "ImporteBonificaDataGridViewTextBoxColumn"
        Me.ImporteBonificaDataGridViewTextBoxColumn.ReadOnly = True
        '
        'UltimoMesDataGridViewTextBoxColumn
        '
        Me.UltimoMesDataGridViewTextBoxColumn.DataPropertyName = "Ultimo_Mes"
        Me.UltimoMesDataGridViewTextBoxColumn.HeaderText = "Ultimo_Mes"
        Me.UltimoMesDataGridViewTextBoxColumn.Name = "UltimoMesDataGridViewTextBoxColumn"
        Me.UltimoMesDataGridViewTextBoxColumn.ReadOnly = True
        Me.UltimoMesDataGridViewTextBoxColumn.Visible = False
        '
        'UltimoanioDataGridViewTextBoxColumn
        '
        Me.UltimoanioDataGridViewTextBoxColumn.DataPropertyName = "Ultimo_anio"
        Me.UltimoanioDataGridViewTextBoxColumn.HeaderText = "Ultimo_anio"
        Me.UltimoanioDataGridViewTextBoxColumn.Name = "UltimoanioDataGridViewTextBoxColumn"
        Me.UltimoanioDataGridViewTextBoxColumn.ReadOnly = True
        Me.UltimoanioDataGridViewTextBoxColumn.Visible = False
        '
        'AdelantadoDataGridViewCheckBoxColumn
        '
        Me.AdelantadoDataGridViewCheckBoxColumn.DataPropertyName = "Adelantado"
        Me.AdelantadoDataGridViewCheckBoxColumn.HeaderText = "Adelantado"
        Me.AdelantadoDataGridViewCheckBoxColumn.Name = "AdelantadoDataGridViewCheckBoxColumn"
        Me.AdelantadoDataGridViewCheckBoxColumn.ReadOnly = True
        Me.AdelantadoDataGridViewCheckBoxColumn.Visible = False
        '
        'DESCRIPCIONDataGridViewTextBoxColumn
        '
        Me.DESCRIPCIONDataGridViewTextBoxColumn.DataPropertyName = "DESCRIPCION"
        Me.DESCRIPCIONDataGridViewTextBoxColumn.HeaderText = ""
        Me.DESCRIPCIONDataGridViewTextBoxColumn.Name = "DESCRIPCIONDataGridViewTextBoxColumn"
        Me.DESCRIPCIONDataGridViewTextBoxColumn.ReadOnly = True
        Me.DESCRIPCIONDataGridViewTextBoxColumn.Visible = False
        Me.DESCRIPCIONDataGridViewTextBoxColumn.Width = 5
        '
        'CLV_DETALLE
        '
        Me.CLV_DETALLE.DataPropertyName = "Clv_Detalle"
        Me.CLV_DETALLE.HeaderText = "CLV_DETALLE"
        Me.CLV_DETALLE.Name = "CLV_DETALLE"
        Me.CLV_DETALLE.ReadOnly = True
        Me.CLV_DETALLE.Width = 5
        '
        'DameDetalleBindingSource
        '
        Me.DameDetalleBindingSource.DataMember = "DameDetalle"
        Me.DameDetalleBindingSource.DataSource = Me.DataSetEric
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SumaDetalleDataGridView
        '
        Me.SumaDetalleDataGridView.AllowUserToAddRows = False
        Me.SumaDetalleDataGridView.AllowUserToDeleteRows = False
        DataGridViewCellStyle19.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SumaDetalleDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle19
        Me.SumaDetalleDataGridView.AutoGenerateColumns = False
        Me.SumaDetalleDataGridView.BackgroundColor = System.Drawing.Color.White
        Me.SumaDetalleDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.SumaDetalleDataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle20.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.SumaDetalleDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle20
        Me.SumaDetalleDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.SumaDetalleDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ClvSessionDataGridViewTextBoxColumn1, Me.PosicionDataGridViewTextBoxColumn, Me.NivelDataGridViewTextBoxColumn, Me.DescripcionDataGridViewTextBoxColumn1, Me.TotalDataGridViewTextBoxColumn})
        Me.SumaDetalleDataGridView.DataSource = Me.SumaDetalleBindingSource
        DataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle23.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.SumaDetalleDataGridView.DefaultCellStyle = DataGridViewCellStyle23
        Me.SumaDetalleDataGridView.GridColor = System.Drawing.Color.WhiteSmoke
        Me.SumaDetalleDataGridView.Location = New System.Drawing.Point(514, 250)
        Me.SumaDetalleDataGridView.MultiSelect = False
        Me.SumaDetalleDataGridView.Name = "SumaDetalleDataGridView"
        Me.SumaDetalleDataGridView.RightToLeft = System.Windows.Forms.RightToLeft.No
        DataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle24.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.SumaDetalleDataGridView.RowHeadersDefaultCellStyle = DataGridViewCellStyle24
        DataGridViewCellStyle25.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SumaDetalleDataGridView.RowsDefaultCellStyle = DataGridViewCellStyle25
        Me.SumaDetalleDataGridView.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SumaDetalleDataGridView.Size = New System.Drawing.Size(490, 132)
        Me.SumaDetalleDataGridView.TabIndex = 200
        Me.SumaDetalleDataGridView.TabStop = False
        '
        'ClvSessionDataGridViewTextBoxColumn1
        '
        Me.ClvSessionDataGridViewTextBoxColumn1.DataPropertyName = "Clv_Session"
        Me.ClvSessionDataGridViewTextBoxColumn1.HeaderText = "Clv_Session"
        Me.ClvSessionDataGridViewTextBoxColumn1.Name = "ClvSessionDataGridViewTextBoxColumn1"
        Me.ClvSessionDataGridViewTextBoxColumn1.Visible = False
        '
        'PosicionDataGridViewTextBoxColumn
        '
        Me.PosicionDataGridViewTextBoxColumn.DataPropertyName = "Posicion"
        Me.PosicionDataGridViewTextBoxColumn.HeaderText = "Posicion"
        Me.PosicionDataGridViewTextBoxColumn.Name = "PosicionDataGridViewTextBoxColumn"
        Me.PosicionDataGridViewTextBoxColumn.Visible = False
        '
        'NivelDataGridViewTextBoxColumn
        '
        Me.NivelDataGridViewTextBoxColumn.DataPropertyName = "Nivel"
        Me.NivelDataGridViewTextBoxColumn.HeaderText = "Nivel"
        Me.NivelDataGridViewTextBoxColumn.Name = "NivelDataGridViewTextBoxColumn"
        Me.NivelDataGridViewTextBoxColumn.Visible = False
        '
        'DescripcionDataGridViewTextBoxColumn1
        '
        Me.DescripcionDataGridViewTextBoxColumn1.DataPropertyName = "Descripcion"
        DataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle21.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle21.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle21.SelectionBackColor = System.Drawing.Color.WhiteSmoke
        Me.DescripcionDataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle21
        Me.DescripcionDataGridViewTextBoxColumn1.HeaderText = "Descripcion"
        Me.DescripcionDataGridViewTextBoxColumn1.Name = "DescripcionDataGridViewTextBoxColumn1"
        Me.DescripcionDataGridViewTextBoxColumn1.Width = 250
        '
        'TotalDataGridViewTextBoxColumn
        '
        Me.TotalDataGridViewTextBoxColumn.DataPropertyName = "Total"
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle22.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle22.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle22.SelectionBackColor = System.Drawing.Color.WhiteSmoke
        Me.TotalDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle22
        Me.TotalDataGridViewTextBoxColumn.HeaderText = "Total"
        Me.TotalDataGridViewTextBoxColumn.Name = "TotalDataGridViewTextBoxColumn"
        Me.TotalDataGridViewTextBoxColumn.Width = 190
        '
        'SumaDetalleBindingSource
        '
        Me.SumaDetalleBindingSource.DataMember = "SumaDetalle"
        Me.SumaDetalleBindingSource.DataSource = Me.DataSetEric
        '
        'Clv_SessionTextBox
        '
        Me.Clv_SessionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameDetalleBindingSource, "Clv_Session", True))
        Me.Clv_SessionTextBox.Location = New System.Drawing.Point(912, 445)
        Me.Clv_SessionTextBox.Name = "Clv_SessionTextBox"
        Me.Clv_SessionTextBox.ReadOnly = True
        Me.Clv_SessionTextBox.Size = New System.Drawing.Size(10, 20)
        Me.Clv_SessionTextBox.TabIndex = 201
        Me.Clv_SessionTextBox.TabStop = False
        '
        'DameDetalleTableAdapter
        '
        Me.DameDetalleTableAdapter.ClearBeforeFill = True
        '
        'SumaDetalleTableAdapter
        '
        Me.SumaDetalleTableAdapter.ClearBeforeFill = True
        '
        'CobraBindingSource
        '
        Me.CobraBindingSource.DataMember = "Cobra"
        Me.CobraBindingSource.DataSource = Me.DataSetEric
        '
        'CobraTableAdapter
        '
        Me.CobraTableAdapter.ClearBeforeFill = True
        '
        'BUSCLIPORCONTRATO_FACBindingSource
        '
        Me.BUSCLIPORCONTRATO_FACBindingSource.DataMember = "BUSCLIPORCONTRATO_FAC"
        Me.BUSCLIPORCONTRATO_FACBindingSource.DataSource = Me.DataSetEric
        '
        'BUSCLIPORCONTRATO_FACTableAdapter
        '
        Me.BUSCLIPORCONTRATO_FACTableAdapter.ClearBeforeFill = True
        '
        'BorraClv_SessionBindingSource
        '
        Me.BorraClv_SessionBindingSource.DataMember = "BorraClv_Session"
        Me.BorraClv_SessionBindingSource.DataSource = Me.DataSetEric
        '
        'BorraClv_SessionTableAdapter
        '
        Me.BorraClv_SessionTableAdapter.ClearBeforeFill = True
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkRed
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.Location = New System.Drawing.Point(841, 431)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(139, 42)
        Me.Button3.TabIndex = 507
        Me.Button3.Text = "&SALIR"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'FrmConsultaCobro
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(1016, 499)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Clv_SessionTextBox)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.SumaDetalleDataGridView)
        Me.Name = "FrmConsultaCobro"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Consultar Cobro"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameDetalleBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SumaDetalleDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SumaDetalleBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CobraBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BUSCLIPORCONTRATO_FACBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorraClv_SessionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents DameDetalleBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameDetalleTableAdapter As sofTV.DataSetEricTableAdapters.DameDetalleTableAdapter
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents SumaDetalleBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SumaDetalleTableAdapter As sofTV.DataSetEricTableAdapters.SumaDetalleTableAdapter
    Friend WithEvents SumaDetalleDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents ClvSessionDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PosicionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NivelDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescripcionDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TotalDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_SessionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CobraBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CobraTableAdapter As sofTV.DataSetEricTableAdapters.CobraTableAdapter
    Friend WithEvents BUSCLIPORCONTRATO_FACBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BUSCLIPORCONTRATO_FACTableAdapter As sofTV.DataSetEricTableAdapters.BUSCLIPORCONTRATO_FACTableAdapter
    Friend WithEvents BorraClv_SessionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorraClv_SessionTableAdapter As sofTV.DataSetEricTableAdapters.BorraClv_SessionTableAdapter
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents ClvSessionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CLVSERVICIODataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClvllavedelservicioDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClvUnicaNetDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CLAVEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MACCABLEMODEM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DESCORTADataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Pagos_Adelantados As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents TvAdicDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MesesCortesiaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MesesApagarDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImporteDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PeriodoPagadoIniDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PeriodoPagadoFinDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PuntosAplicadosOtrosDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PuntosAplicadosAntDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescuentoNet As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Des_Otr_Ser_Misma_Categoria As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BonificacionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImporteAdicionalDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColumnaDetalleDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DiasBonificaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MesesBonificarDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImporteBonificaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UltimoMesDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UltimoanioDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AdelantadoDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DESCRIPCIONDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CLV_DETALLE As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
