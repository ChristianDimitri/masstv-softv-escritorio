﻿Imports System.Data.SqlClient
Imports System.Text
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Public Class BrwTiempoPorServicio
    Private customersByCityReport As ReportDocument
    Dim OpServicio As Integer = 0
    Dim op As String = Nothing
    Dim oprep As String = " 5"
    Dim Clv_Tecnico As String = "0"
    Dim clave_trabajo As String = "0"
    Dim bandera As Boolean
    Dim opcionreporte As Integer
    Dim CLV_Orden1 As String, CLV_Orden2 As String, CLV_Queja1 As String, CLV_Queja2 As String, Fec1Ini As String = "01/01/1900",
     Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900",
     Fec2Fin As String = "01/01/1900"

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged

        If Me.CheckBox1.Checked = True Then
            If bandera = False Then
                If Var_reporteTiempo = 0 Then
                    Validar(CheckBox1)
                    GloBusca_NumOrden = True
                    GloRepAreaTec = "O"
                    GloEjecutadas = 1
                    FrmSelOrdSer.Show()
                    'GloRepAreaTec = Nothing
                    opcionreporte = 0
                    Me.NUMINILbl.Text = GLONUMCLV_ORDEN_INI
                    Me.NUMFINLBL.Text = GLONUMCLV_ORDEN_FIN
                Else
                    Validar(CheckBox1)
                    GloBusca_NumQueja = True
                    GloRepAreaTec = "Q"
                    GloEjecutadas = 1
                    FrmSelOrdSer.Show()
                    opcionreporte = 0
                    Me.NUMINILbl.Text = GLONUMCLVQUEJAS_INI
                    Me.NUMFINLBL.Text = GLONUMCLVQUEJAS_FIN
                End If
            Else
                CheckBox1.Checked = False
            End If
        Else
            If CheckBox1.Checked = False And CheckBox2.Checked = False And CheckBox3.Checked = False And CheckBox4.Checked = False And CheckBox5.Checked = False Then
                bandera = False
            End If
            CheckBox1.Checked = False
            Me.NUMINILbl.Text = 0
            Me.NUMFINLBL.Text = 0
            End If

    End Sub

    Private Sub BrwTiempoPorServicio_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

        If GloBusca_NumOrden = True Then
            GloBusca_NumOrden = False
            Me.NUMFINLBL.Text = GLONUMCLV_ORDEN_FIN
            Me.NUMINILbl.Text = GLONUMCLV_ORDEN_INI
            GLONUMCLV_ORDEN_FIN = 0
            GLONUMCLV_ORDEN_INI = 0


        ElseIf GloBusca_NumQueja = True Then
            GloBusca_NumQueja = False
            Me.NUMFINLBL.Text = GLONUMCLVQUEJAS_FIN
            Me.NUMINILbl.Text = GLONUMCLVQUEJAS_INI
            GLONUMCLV_ORDEN_FIN = 0
            GLONUMCLV_ORDEN_INI = 0
        End If
    End Sub


    Private Sub BrwTiempoPorServicio_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Var_reporteTiempo = 0 Then

            'LlenaComBoTipoServicio()
            llenaComboTipoServicio2()
            Limpia()
            CLV_Orden1 = "0"
            CLV_Orden2 = "0"
            clave_trabajo = "0"
            Clv_Tecnico = "0"
        Else

            'LlenaComBoTipoServicio()
            llenaComboTipoServicio2()
            CheckBox1.Text = "Numero de Queja"
            Limpia()
            CLV_Queja1 = "0"
            CLV_Queja2 = "0"
            clave_trabajo = "0"
            Clv_Tecnico = "0"

        End If
        fechaSol1.Value = DateTime.Now
        fechaSol2.Value = DateTime.Now
        fechaeje1.Value = DateTime.Now
        fechaeje2.Value = DateTime.Now

        colorea(Me, Me.Name)
    End Sub
    Private Sub Limpia()
        'txtOrden1.Enabled = False
        'txtOrden2.Enabled = False
        fechaSol1.Enabled = False
        fechaSol2.Enabled = False
        fechaeje1.Enabled = False
        fechaeje2.Enabled = False
        CmbTecnico.Enabled = False
        CmbTrabajo.Enabled = False
        'CmbTipSer.Enabled = False

    End Sub
    Private Sub LlenaComBoTipoServicio()

        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder

        StrSQL.Append("EXEC MuestraTipSerPrincipal ")


        Dim DA As New SqlDataAdapter(StrSQL.ToString, CON)
        Dim DT As New DataTable
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.CmbTipSer.DataSource = BS.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try

    End Sub
    Private Sub LlenaComBoTrabajo(ByVal OpServicio As Integer)
        If Var_reporteTiempo = 0 Then
            Dim CON As New SqlConnection(MiConexion)
            Dim StrSQL As New StringBuilder

            StrSQL.Append(" EXEC MUESTRATRABAJOSORDENES ")

            StrSQL.Append(CStr(OpServicio))

            Dim DA As New SqlDataAdapter(StrSQL.ToString, CON)
            Dim DT As New DataTable
            Dim BS As New BindingSource

            Try
                CON.Open()
                DA.Fill(DT)
                BS.DataSource = DT
                Me.CmbTrabajo.DataSource = BS.DataSource
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information)
            Finally
                CON.Close()
                CON.Dispose()
            End Try
        Else

            Dim CON As New SqlConnection(MiConexion)
            Dim StrSQL As New StringBuilder

            StrSQL.Append(" EXEC MUESTRATRABAJOSQUEJASPORTIEMPO ")

            StrSQL.Append(CStr(OpServicio))

            Dim DA As New SqlDataAdapter(StrSQL.ToString, CON)
            Dim DT As New DataTable
            Dim BS As New BindingSource

            Try
                CON.Open()
                DA.Fill(DT)
                BS.DataSource = DT
                Me.CmbTrabajo.DataSource = BS.DataSource
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information)
            Finally
                CON.Close()
                CON.Dispose()
            End Try

        End If


    End Sub
    Private Sub LlenaTecnico(ByVal OpTecnico As Integer, ByVal prmClvTipSer As Integer)

        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder

        StrSQL.Append("EXEC uspMuestraTecnicosAlmacenReportes ")

        StrSQL.Append(CStr(OpTecnico))
        StrSQL.Append("," & CStr(prmClvTipSer))

        Dim DA As New SqlDataAdapter(StrSQL.ToString, CON)
        Dim DT As New DataTable
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.CmbTecnico.DataSource = BS.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try

    End Sub
    Private Sub ConfigureCrystalReports_NewXml(ByVal op As String, ByVal Titulo As String, ByVal opcionreporte As Integer)
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            clave_trabajo = "0"
            Dim reportPath As String = Nothing
            Dim mySelectFormula As String = Titulo

            If Me.NUMINILbl.Text.Length > 0 Then
                If IsNumeric(Me.NUMINILbl.Text) = True Then
                    'If CLng(Me.txtOrden1.Text) > 0 Then
                    CLV_Orden1 = CInt(Me.NUMINILbl.Text)
                    'End If
                End If
            End If
            If Me.NUMFINLBL.Text.Length > 0 Then
                If IsNumeric(Me.NUMFINLBL.Text) = True Then
                    ' If CLng(Me.NUMFINLBL.Text) > 0 Then
                    CLV_Orden2 = CInt(NUMFINLBL.Text)
                    'End If
                End If
            End If


            If Me.fechaSol1.Text.Length > 0 Then
                Fec1Ini = fechaSol1.Value
            End If
            If Me.fechaSol2.Text.Length > 0 Then
                Fec1Fin = fechaSol2.Value
            End If

            If Me.fechaeje1.Text.Length > 0 Then
                Fec2Ini = fechaeje1.Value

            End If
            If Me.fechaeje2.Text.Length > 0 Then
                Fec2Ini = fechaeje2.Value
            End If



            reportPath = RutaReportes + "\ReportAgendaTecnicoRangoTiempo.rpt"

            Dim cnn As New SqlConnection(MiConexion)
            Dim cmd As New SqlCommand("DameAgendaTecnicoTrabajoPorTiempo", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0

            If IsNumeric(Me.NUMINILbl.Text) = True Then CLV_Orden1 = CStr(Me.NUMINILbl.Text)
            Dim parametro1 As New SqlParameter("@CLV_Orden1", SqlDbType.Int)
            parametro1.Direction = ParameterDirection.Input
            parametro1.Value = CLV_Orden1
            cmd.Parameters.Add(parametro1)
            If IsNumeric(Me.NUMFINLBL.Text) = True Then CLV_Orden2 = CStr(Me.NUMFINLBL.Text)
            Dim parametro2 As New SqlParameter("@CLV_Orden2", SqlDbType.Int)
            parametro2.Direction = ParameterDirection.Input
            parametro2.Value = CLV_Orden2
            cmd.Parameters.Add(parametro2)

            Clv_Tecnico = CmbTecnico.SelectedValue

            'If IsNumeric(Me.CmbTecnico.Text) = True Then
            '    Clv_Tecnico = CmbTecnico.SelectedValue
            'Else
            '    Clv_Tecnico = "0"
            'End If
            If Clv_Tecnico = "" Then
                Clv_Tecnico = "0"
            End If

            Dim parametro3 As New SqlParameter("@CLV_TECNICO", SqlDbType.SmallInt)
            parametro3.Direction = ParameterDirection.Input
            parametro3.Value = Clv_Tecnico
            cmd.Parameters.Add(parametro3)


            If IsDate(Me.fechaSol1.Text) = True Then Fec1Ini = Me.fechaSol1.Text
            Fec1Ini = FormatDateTime(Me.fechaSol1.Text, DateFormat.ShortDate)
            Dim parametro4 As New SqlParameter("@Fec1Ini", SqlDbType.DateTime)
            parametro4.Direction = ParameterDirection.Input
            parametro4.Value = Fec1Ini
            cmd.Parameters.Add(parametro4)

            If IsDate(Me.fechaSol2.Text) = True Then Fec2Ini = Me.fechaSol2.Text
            Fec2Ini = FormatDateTime(Me.fechaSol2.Text, DateFormat.ShortDate)
            Dim parametro5 As New SqlParameter("@Fec2Ini", SqlDbType.DateTime)
            parametro5.Direction = ParameterDirection.Input
            parametro5.Value = Fec2Ini
            cmd.Parameters.Add(parametro5)

            If IsDate(Me.fechaeje1.Text) = True Then Fec1Fin = Me.fechaeje1.Text
            Fec1Fin = FormatDateTime(Me.fechaeje1.Text, DateFormat.ShortDate)
            Dim parametro6 As New SqlParameter("@Fec1Eje", SqlDbType.DateTime)
            parametro6.Direction = ParameterDirection.Input
            parametro6.Value = Fec1Fin
            cmd.Parameters.Add(parametro6)

            If IsDate(Me.fechaeje2.Text) = True Then Fec2Ini = Me.fechaeje2.Text
            Fec2Fin = FormatDateTime(Me.fechaeje2.Text, DateFormat.ShortDate)
            Dim parametro7 As New SqlParameter("@Fec2Eje", SqlDbType.DateTime)
            parametro7.Direction = ParameterDirection.Input
            parametro7.Value = Fec2Fin
            cmd.Parameters.Add(parametro7)

            If Me.CheckBox5.Checked Then
                clave_trabajo = CmbTrabajo.SelectedValue

                If IsNumeric(Me.CmbTrabajo.SelectedValue) = True Then
                    clave_trabajo = CmbTrabajo.SelectedValue
                Else
                    clave_trabajo = "0"
                End If
            Else
                clave_trabajo = "0"
            End If

            Dim parametro8 As New SqlParameter("@Clv_Trabajo", SqlDbType.Int)
            parametro8.Direction = ParameterDirection.Input
            parametro8.Value = clave_trabajo
            cmd.Parameters.Add(parametro8)

            ' If IsNumeric(Me.CLV_COLONIA.Text) = True Then nClv_colonia = CStr(Me.CLV_COLONIA.Text)
            Dim parametro9 As New SqlParameter("@op", SqlDbType.Int)
            parametro9.Direction = ParameterDirection.Input
            parametro9.Value = opcionreporte
            cmd.Parameters.Add(parametro9)

            Dim parametro10 As New SqlParameter("@clvTipSer", SqlDbType.Int)
            parametro10.Direction = ParameterDirection.Input
            parametro10.Value = CInt(Me.CmbTipSer.SelectedValue)
            cmd.Parameters.Add(parametro10)

            Dim da As New SqlDataAdapter(cmd)
            Dim data1 As New DataTable()
            da.Fill(data1)
            Dim ds As New DataSet()

            customersByCityReport.Load(reportPath)

            mySelectFormula = "Listado de Tiempo de Ordenes Ejecutadas Por Trabajo Realizado"

            customersByCityReport.DataDefinition.FormulaFields("empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloSucursal & "'"
            ' customersByCityReport.DataDefinition.FormulaFields("nomservicio").Text = "'" & Me.ComboBox4.Text & " con status " & Status & "'"


            data1.TableName = "DameAgendaTecnicoTrabajoPorTiempo"
            ds.Tables.Add(data1)
            customersByCityReport.SetDataSource(ds)
            cmbTipoServicio.ReportSource = customersByCityReport

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ConfigureCrystalReportsQuejas_NewXml(ByVal op As String, ByVal Titulo As String, ByVal opcionreporte As Integer)
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            Dim reportPath As String = Nothing
            Dim mySelectFormula As String = Titulo

            If Me.NUMINILbl.Text.Length > 0 Then
                If IsNumeric(Me.NUMINILbl.Text) = True Then
                    'If CLng(Me.txtOrden1.Text) > 0 Then
                    CLV_Queja1 = CInt(Me.NUMINILbl.Text)
                    'End If
                End If
            End If
            If Me.NUMFINLBL.Text.Length > 0 Then
                If IsNumeric(Me.NUMFINLBL.Text) = True Then
                    ' If CLng(Me.NUMFINLBL.Text) > 0 Then
                    CLV_Queja2 = CInt(NUMFINLBL.Text)
                    'End If
                End If
            End If

            If Me.fechaSol1.Text.Length > 0 Then
                Fec1Ini = fechaSol1.Value
            End If
            If Me.fechaSol2.Text.Length > 0 Then
                Fec1Fin = fechaSol2.Value
            End If

            If Me.fechaeje1.Text.Length > 0 Then
                Fec2Ini = fechaeje1.Value

            End If
            If Me.fechaeje2.Text.Length > 0 Then
                Fec2Fin = fechaeje2.Value
            End If

            reportPath = RutaReportes + "\ReportAgendaQuejasRangoTiempo.rpt"

            Dim cnn As New SqlConnection(MiConexion)
            Dim cmd As New SqlCommand("DameAgendaQuejasTrabajoPorTiempo", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0


            Dim parametro1 As New SqlParameter("@CLV_Queja1", SqlDbType.Int)
            parametro1.Direction = ParameterDirection.Input
            parametro1.Value = CLV_Queja1
            cmd.Parameters.Add(parametro1)

            Dim parametro2 As New SqlParameter("@CLV_Queja2", SqlDbType.Int)
            parametro2.Direction = ParameterDirection.Input
            parametro2.Value = CLV_Queja2
            cmd.Parameters.Add(parametro2)

            Clv_Tecnico = CmbTecnico.SelectedValue


            If Clv_Tecnico = "" Then
                Clv_Tecnico = "0"
            End If

            Dim parametro3 As New SqlParameter("@CLV_TECNICO", SqlDbType.SmallInt)
            parametro3.Direction = ParameterDirection.Input
            parametro3.Value = Clv_Tecnico
            cmd.Parameters.Add(parametro3)


            If IsDate(Me.fechaSol1.Text) = True Then Fec1Ini = Me.fechaSol1.Text
            Fec1Ini = FormatDateTime(Me.fechaSol1.Text, DateFormat.ShortDate)
            Dim parametro4 As New SqlParameter("@Fec1Ini", SqlDbType.DateTime)
            parametro4.Direction = ParameterDirection.Input
            parametro4.Value = Fec1Ini
            cmd.Parameters.Add(parametro4)

            If IsDate(Me.fechaSol2.Text) = True Then Fec2Ini = Me.fechaSol2.Text
            Fec2Ini = FormatDateTime(Me.fechaSol2.Text, DateFormat.ShortDate)
            Dim parametro5 As New SqlParameter("@Fec2Ini", SqlDbType.DateTime)
            parametro5.Direction = ParameterDirection.Input
            parametro5.Value = Fec2Ini
            cmd.Parameters.Add(parametro5)

            If IsDate(Me.fechaeje1.Text) = True Then Fec1Fin = Me.fechaeje1.Text
            Fec1Fin = FormatDateTime(Me.fechaeje1.Text, DateFormat.ShortDate)
            Dim parametro6 As New SqlParameter("@Fec1Eje", SqlDbType.DateTime)
            parametro6.Direction = ParameterDirection.Input
            parametro6.Value = Fec1Fin
            cmd.Parameters.Add(parametro6)

            If IsDate(Me.fechaeje2.Text) = True Then Fec2Ini = Me.fechaeje2.Text
            Fec2Fin = FormatDateTime(Me.fechaeje2.Text, DateFormat.ShortDate)
            Dim parametro7 As New SqlParameter("@Fec2Eje", SqlDbType.DateTime)
            parametro7.Direction = ParameterDirection.Input
            parametro7.Value = Fec2Fin
            cmd.Parameters.Add(parametro7)

            clave_trabajo = CmbTrabajo.SelectedValue

            If clave_trabajo = "" Then

                clave_trabajo = "0"
            End If

            Dim parametro8 As New SqlParameter("@Clv_Trabajo", SqlDbType.Int)
            parametro8.Direction = ParameterDirection.Input
            parametro8.Value = clave_trabajo
            cmd.Parameters.Add(parametro8)


            Dim parametro9 As New SqlParameter("@op", SqlDbType.Int)
            parametro9.Direction = ParameterDirection.Input
            parametro9.Value = opcionreporte
            cmd.Parameters.Add(parametro9)

            Dim parametro10 As New SqlParameter("@clvTipSer", SqlDbType.Int)
            parametro10.Direction = ParameterDirection.Input
            parametro10.Value = CInt(Me.CmbTipSer.SelectedValue)
            cmd.Parameters.Add(parametro10)

            Dim da As New SqlDataAdapter(cmd)
            Dim data1 As New DataTable()
            da.Fill(data1)
            Dim ds As New DataSet()

            customersByCityReport.Load(reportPath)

            mySelectFormula = "Listado de Tiempo de Quejas Ejecutadas Por Trabajo Realizado"

            customersByCityReport.DataDefinition.FormulaFields("empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloSucursal & "'"
            ' customersByCityReport.DataDefinition.FormulaFields("nomservicio").Text = "'" & Me.ComboBox4.Text & " con status " & Status & "'"


            data1.TableName = "DameAgendaQuejasTrabajoPorTiempo"
            ds.Tables.Add(data1)
            customersByCityReport.SetDataSource(ds)
            cmbTipoServicio.ReportSource = customersByCityReport

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.CheckBox1.Checked = False And Me.CheckBox2.Checked = False And Me.CheckBox3.Checked = False And Me.CheckBox4.Checked = False And Me.CheckBox5.Checked = False Then
            MsgBox("Seleccione al menos un filtro", MsgBoxStyle.Information, )
            Exit Sub
        End If

        Select Case Var_reporteTiempo
            Case 0
                If CDate(fechaeje1.Text) > CDate(fechaeje2.Text) Then
                    MsgBox("La Fecha de ejecución no puede ser Mayor a la Fecha Actual", MsgBoxStyle.Exclamation, "Atención")
                ElseIf CDate(fechaSol1.Text) > CDate(fechaSol2.Text) Then

                    MsgBox("La Fecha de Solicitud no puede ser Mayor a la Fecha Actual", MsgBoxStyle.Exclamation, "Atención")
                Else
                    ConfigureCrystalReports_NewXml(1, "", opcionreporte)
                End If

            Case 1
                If CDate(fechaeje1.Text) > CDate(fechaeje2.Text) Then
                    MsgBox("La Fecha de ejecución no puede ser Mayor a la Fecha Actual", MsgBoxStyle.Exclamation, "Atención")
                ElseIf CDate(fechaSol1.Text) > CDate(fechaSol2.Text) Then

                    MsgBox("La Fecha de Solicitud no puede ser Mayor a la Fecha Actual", MsgBoxStyle.Exclamation, "Atención")
                Else
                    ConfigureCrystalReportsQuejas_NewXml(1, "", opcionreporte)
                End If

        End Select


    End Sub

    Private Sub Validar(ByVal caja As CheckBox)



        If CheckBox1.Name = caja.Name And CheckBox2.Checked = False And CheckBox3.Checked = False And CheckBox4.Checked = False And CheckBox5.Checked = False Then
            bandera = True
            Return
        ElseIf CheckBox2.Name = caja.Name And CheckBox1.Checked = False And CheckBox3.Checked = False And CheckBox4.Checked = False And CheckBox5.Checked = False Then
            bandera = True
            Return
        ElseIf CheckBox3.Name = caja.Name And CheckBox2.Checked = False And CheckBox1.Checked = False And CheckBox4.Checked = False And CheckBox5.Checked = False Then
            bandera = True
            Return
        ElseIf CheckBox4.Name = caja.Name And CheckBox2.Checked = False And CheckBox3.Checked = False And CheckBox1.Checked = False And CheckBox5.Checked = False Then
            bandera = True
            Return
        ElseIf CheckBox5.Name = caja.Name And CheckBox2.Checked = False And CheckBox3.Checked = False And CheckBox4.Checked = False And CheckBox1.Checked = False Then
            bandera = True
            Return
        End If

        'If CheckBox1.Checked = True And CheckBox1.Name <> caja.Name Then
        '    bandera = True
        '    Return
        'ElseIf CheckBox2.Checked = True And CheckBox2.Name <> caja.Name Then
        '    bandera = True
        '    Return
        'ElseIf CheckBox3.Checked = True And CheckBox3.Name <> caja.Name Then
        '    bandera = True
        '    Return
        'ElseIf CheckBox4.Checked = True And CheckBox4.Name <> caja.Name Then
        '    bandera = True
        '    Return
        'ElseIf CheckBox5.Checked = True And CheckBox5.Name <> caja.Name Then
        '    bandera = True
        '    Return
        'Else
        '    bandera = False
        '    Return
        'End If


    End Sub

    Private Sub CheckBox2_CheckedChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If CheckBox2.Checked = True Then

            If bandera = False Then
                Validar(CheckBox2)
                fechaSol1.Enabled = True
                fechaSol2.Enabled = True
                opcionreporte = 1
            Else
                CheckBox2.Checked = False
            End If
        Else
            If CheckBox1.Checked = False And CheckBox2.Checked = False And CheckBox3.Checked = False And CheckBox4.Checked = False And CheckBox5.Checked = False Then
                bandera = False

            End If

            fechaSol1.Enabled = False
            fechaSol2.Enabled = False
        End If
    End Sub

    Private Sub CheckBox3_CheckedChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If CheckBox3.Checked = True Then

            If bandera = False Then
                Validar(CheckBox3)
                fechaeje1.Enabled = True
                fechaeje2.Enabled = True
                opcionreporte = 2

            Else
                CheckBox3.Checked = False
            End If
        Else
            If CheckBox1.Checked = False And CheckBox2.Checked = False And CheckBox3.Checked = False And CheckBox4.Checked = False And CheckBox5.Checked = False Then
                bandera = False
            End If
            fechaeje1.Enabled = False
            fechaeje2.Enabled = False
            End If


    End Sub

    Private Sub CheckBox4_CheckedChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If CheckBox4.CheckState = CheckState.Checked Then




            If bandera = False Then
                Validar(CheckBox4)
                LlenaTecnico(3, CInt(Me.CmbTipSer.SelectedValue))
                CmbTecnico.Enabled = True
                Clv_Tecnico = CmbTecnico.SelectedValue

                opcionreporte = 3
            Else
                CheckBox4.Checked = False
            End If
        Else
            If CheckBox1.Checked = False And CheckBox2.Checked = False And CheckBox3.Checked = False And CheckBox4.Checked = False And CheckBox5.Checked = False Then
                bandera = False
            End If
            CmbTecnico.Enabled = False

        End If
    End Sub

    Private Sub CheckBox5_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox5.CheckedChanged
        If CheckBox5.CheckState = CheckState.Checked Then

            If bandera = False Then
                Validar(CheckBox5)
                CmbTrabajo.Enabled = True
                'CmbTipSer.Enabled = True
                opcionreporte = 4
                OpServicio = CmbTipSer.SelectedValue
                LlenaComBoTrabajo(OpServicio)
                clave_trabajo = CmbTrabajo.SelectedValue
            Else
                CheckBox5.Checked = False
            End If
        Else
            If CheckBox1.Checked = False And CheckBox2.Checked = False And CheckBox3.Checked = False And CheckBox4.Checked = False And CheckBox5.Checked = False Then
                bandera = False

            End If
            CmbTrabajo.Enabled = False
            'CmbTipSer.Enabled = False
        End If
    End Sub
    'Private Sub CheckBox5_CheckedChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox5.CheckedChanged
    '    If CheckBox5.CheckState = CheckState.Checked Then



    '        If bandera = False Then
    '            Validar(CheckBox5)
    '            CmbTrabajo.Enabled = True
    '            CmbTipSer.Enabled = True
    '            opcionreporte = 4
    '            OpServicio = CmbTipSer.SelectedValue
    '            LlenaComBoTrabajo(OpServicio)
    '            clave_trabajo = CmbTrabajo.SelectedValue
    '        Else
    '            CheckBox5.Checked = False
    '        End If
    '    Else
    '        If CheckBox1.Checked = False And CheckBox2.Checked = False And CheckBox3.Checked = False And CheckBox4.Checked = False And CheckBox5.Checked = False Then
    '            bandera = False
    '        End If
    '        CmbTrabajo.Enabled = False
    '        CmbTipSer.Enabled = False
    '    End If
    'End Sub

    Private Sub CheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox3.CheckedChanged
        If CheckBox3.Checked = True Then


            If bandera = False Then
                Validar(CheckBox3)
                fechaeje1.Enabled = True
                fechaeje2.Enabled = True
                opcionreporte = 2
            Else
                CheckBox3.Checked = False
            End If
        Else
            If CheckBox1.Checked = False And CheckBox2.Checked = False And CheckBox3.Checked = False And CheckBox4.Checked = False And CheckBox5.Checked = False Then
                bandera = False
            End If
            fechaeje1.Enabled = False
            fechaeje2.Enabled = False
        End If
    End Sub

    Private Sub CheckBox4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox4.CheckedChanged
        If CheckBox4.CheckState = CheckState.Checked Then

            If bandera = False Then
                Validar(CheckBox4)
                LlenaTecnico(3, CInt(Me.CmbTipSer.SelectedValue))
                CmbTecnico.Enabled = True
                Clv_Tecnico = CmbTecnico.SelectedValue

                opcionreporte = 3
            Else
                CheckBox4.Checked = False
            End If
        Else
            If CheckBox1.Checked = False And CheckBox2.Checked = False And CheckBox3.Checked = False And CheckBox4.Checked = False And CheckBox5.Checked = False Then
                bandera = False
            End If
            CmbTecnico.Enabled = False

        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub BrwTiempoPorServicio_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        GloEjecutadas = 0
    End Sub

    Private Sub Panel4_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel4.Paint

    End Sub

#Region "Métodos"
    Private Sub llenaComboTipoServicio2()
        Try
            BaseII.limpiaParametros()
            Me.CmbTipSer.DataSource = BaseII.ConsultaDT("uspFiltroTipServ")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub llenaComboTrabajos2(ByVal prmTipSer As Integer)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_TipSer", SqlDbType.Int, prmTipSer)
            Me.CmbTrabajo.DataSource = BaseII.ConsultaDT("MUESTRATRABAJOSORDENES")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

    Private Sub CmbTipSer_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmbTipSer.SelectedIndexChanged
        If IsNumeric(Me.CmbTipSer.SelectedValue) = True Then
            llenaComboTrabajos2(CInt(Me.CmbTipSer.SelectedValue))
            LlenaTecnico(3, CInt(Me.CmbTipSer.SelectedValue))
            Clv_Tecnico = CmbTecnico.SelectedValue
        End If
    End Sub
End Class