﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmGrupoIngresos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.PosicionText = New System.Windows.Forms.TextBox()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.IdGrupoText = New System.Windows.Forms.TextBox()
        Me.CMBLabel5 = New System.Windows.Forms.Label()
        Me.ExitButton = New System.Windows.Forms.Button()
        Me.SaveButton = New System.Windows.Forms.Button()
        Me.ConceptoText = New System.Windows.Forms.TextBox()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.CMBLabel1000 = New System.Windows.Forms.Label()
        Me.tbIdProgramacion = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(82, 89)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(80, 16)
        Me.CMBLabel2.TabIndex = 24
        Me.CMBLabel2.Text = "Posición : "
        '
        'PosicionText
        '
        Me.PosicionText.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PosicionText.Location = New System.Drawing.Point(168, 84)
        Me.PosicionText.Name = "PosicionText"
        Me.PosicionText.Size = New System.Drawing.Size(124, 21)
        Me.PosicionText.TabIndex = 2
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(76, 63)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(86, 16)
        Me.CMBLabel1.TabIndex = 21
        Me.CMBLabel1.Text = "Concepto : "
        '
        'IdGrupoText
        '
        Me.IdGrupoText.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IdGrupoText.Location = New System.Drawing.Point(168, 32)
        Me.IdGrupoText.Name = "IdGrupoText"
        Me.IdGrupoText.ReadOnly = True
        Me.IdGrupoText.Size = New System.Drawing.Size(124, 21)
        Me.IdGrupoText.TabIndex = 0
        Me.IdGrupoText.TabStop = False
        '
        'CMBLabel5
        '
        Me.CMBLabel5.AutoSize = True
        Me.CMBLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel5.Location = New System.Drawing.Point(102, 37)
        Me.CMBLabel5.Name = "CMBLabel5"
        Me.CMBLabel5.Size = New System.Drawing.Size(60, 16)
        Me.CMBLabel5.TabIndex = 20
        Me.CMBLabel5.Text = "Clave : "
        '
        'ExitButton
        '
        Me.ExitButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ExitButton.Location = New System.Drawing.Point(542, 151)
        Me.ExitButton.Name = "ExitButton"
        Me.ExitButton.Size = New System.Drawing.Size(140, 35)
        Me.ExitButton.TabIndex = 5
        Me.ExitButton.Text = "&Cancelar"
        Me.ExitButton.UseVisualStyleBackColor = True
        '
        'SaveButton
        '
        Me.SaveButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SaveButton.Location = New System.Drawing.Point(374, 151)
        Me.SaveButton.Name = "SaveButton"
        Me.SaveButton.Size = New System.Drawing.Size(142, 35)
        Me.SaveButton.TabIndex = 4
        Me.SaveButton.Text = "&Guardar"
        Me.SaveButton.UseVisualStyleBackColor = True
        '
        'ConceptoText
        '
        Me.ConceptoText.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConceptoText.Location = New System.Drawing.Point(168, 58)
        Me.ConceptoText.Margin = New System.Windows.Forms.Padding(2)
        Me.ConceptoText.Name = "ConceptoText"
        Me.ConceptoText.Size = New System.Drawing.Size(502, 21)
        Me.ConceptoText.TabIndex = 1
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'CMBLabel1000
        '
        Me.CMBLabel1000.AutoSize = True
        Me.CMBLabel1000.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1000.Location = New System.Drawing.Point(28, 116)
        Me.CMBLabel1000.Name = "CMBLabel1000"
        Me.CMBLabel1000.Size = New System.Drawing.Size(134, 16)
        Me.CMBLabel1000.TabIndex = 26
        Me.CMBLabel1000.Text = "Id Programación : "
        '
        'tbIdProgramacion
        '
        Me.tbIdProgramacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbIdProgramacion.Location = New System.Drawing.Point(168, 111)
        Me.tbIdProgramacion.Name = "tbIdProgramacion"
        Me.tbIdProgramacion.Size = New System.Drawing.Size(124, 21)
        Me.tbIdProgramacion.TabIndex = 3
        '
        'FrmGrupoIngresos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(692, 202)
        Me.Controls.Add(Me.CMBLabel1000)
        Me.Controls.Add(Me.tbIdProgramacion)
        Me.Controls.Add(Me.ConceptoText)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.PosicionText)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.IdGrupoText)
        Me.Controls.Add(Me.CMBLabel5)
        Me.Controls.Add(Me.ExitButton)
        Me.Controls.Add(Me.SaveButton)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MaximizeBox = False
        Me.Name = "FrmGrupoIngresos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Capture el Grupo"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Public WithEvents PosicionText As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Public WithEvents IdGrupoText As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel5 As System.Windows.Forms.Label
    Public WithEvents ExitButton As System.Windows.Forms.Button
    Public WithEvents SaveButton As System.Windows.Forms.Button
    Friend WithEvents ConceptoText As System.Windows.Forms.TextBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents CMBLabel1000 As System.Windows.Forms.Label
    Public WithEvents tbIdProgramacion As System.Windows.Forms.TextBox
End Class
