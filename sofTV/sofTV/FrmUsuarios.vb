﻿Imports System.Data.SqlClient
Public Class FrmUsuarios
    Private clv_usuario As String = Nothing
    Private nombre As String = Nothing
    Private domicilio As String = Nothing
    Private colonia As String = Nothing
    Private activa As String = Nothing
    Private pasaporte As String = Nothing
    Private tipousuario As String = Nothing
    Private email As String = Nothing
    Private fechaingreso As String = Nothing
    Private fechabaja As String = Nothing
    Private CATV As String = Nothing
    Private facturacion As String = Nothing
    Private GrupoVentas As String = Nothing
    Private eClv_Grupo As Integer = 0

    Private FECHA As Date

    Private Sub damedatosbitacora()
        Try
            If opcion = "M" Then
                clv_usuario = Me.Clv_UsuarioTextBox.Text
                nombre = Me.NombreTextBox.Text
                domicilio = Me.DomicilioTextBox.Text
                colonia = Me.ColoniaTextBox.Text
                If Me.ActivoCheckBox.CheckState = CheckState.Checked Then
                    activa = "True"
                Else
                    activa = "False"
                End If
                pasaporte = Me.PasaporteTextBox.Text
                tipousuario = Me.ComboBox1.Text
                email = Me.EmailTextBox.Text
                fechaingreso = Me.FechaIngresoMaskedTextBox.Text
                fechabaja = Me.FechaSalidaMaskedTextBox.Text
                If Me.CATVCheckBox.CheckState = CheckState.Checked Then
                    CATV = "True"
                Else
                    CATV = "False"
                End If
                If Me.FacturacionCheckBox.CheckState = CheckState.Checked Then
                    facturacion = "True"
                Else
                    facturacion = "False"
                End If
                GrupoVentas = Me.GrupoComboBox.Text
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub guardadatosbitacora(ByVal op As Integer)
        Try
            Dim validacion1 As String = Nothing
            Dim validacion2 As String = Nothing
            Dim validacion3 As String = Nothing
            Select Case op
                Case 0
                    If opcion = "M" Then
                        'clv_usuario = Me.Clv_UsuarioTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Clave Usuario: " + CStr(Me.ClaveLabel1.Text) + " / " + Me.Clv_UsuarioTextBox.Name, clv_usuario, Me.Clv_UsuarioTextBox.Text, LocClv_Ciudad)
                        'nombre = Me.NombreTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Clave Usuario: " + CStr(Me.ClaveLabel1.Text) + " / " + Me.NombreTextBox.Name, nombre, Me.NombreTextBox.Text, LocClv_Ciudad)
                        'domicilio = Me.DomicilioTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Clave Usuario: " + CStr(Me.ClaveLabel1.Text) + " / " + Me.DomicilioTextBox.Name, domicilio, Me.DomicilioTextBox.Text, LocClv_Ciudad)
                        'colonia = Me.ColoniaTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Clave Usuario: " + CStr(Me.ClaveLabel1.Text) + " / " + Me.ColoniaTextBox.Name, colonia, Me.ColoniaTextBox.Text, LocClv_Ciudad)
                        'Activo
                        If Me.ActivoCheckBox.CheckState = CheckState.Checked Then
                            'activa = "True"
                            validacion1 = "True"
                        Else
                            'activa = "False"
                            validacion1 = "False"
                        End If
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Clave Usuario: " + CStr(Me.ClaveLabel1.Text) + " / usuarioactivo", activa, validacion1, LocClv_Ciudad)
                        'pasaporte = Me.PasaporteTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Clave Usuario: " + CStr(Me.ClaveLabel1.Text) + " / " + Me.PasaporteTextBox.Name, pasaporte, Me.PasaporteTextBox.Text, LocClv_Ciudad)
                        'tipousuario = Me.ComboBox1.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Clave Usuario: " + CStr(Me.ClaveLabel1.Text) + " / Tipousuario", tipousuario, Me.ComboBox1.Text, LocClv_Ciudad)
                        'email = Me.EmailTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Clave Usuario: " + CStr(Me.ClaveLabel1.Text) + " / " + Me.EmailTextBox.Name, email, Me.EmailTextBox.Text, LocClv_Ciudad)
                        'fechaingreso = Me.FechaIngresoMaskedTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Clave Usuario: " + CStr(Me.ClaveLabel1.Text) + " / " + Me.FechaIngresoMaskedTextBox.Name, fechaingreso, Me.FechaIngresoMaskedTextBox.Text, LocClv_Ciudad)
                        'fechabaja = Me.FechaSalidaMaskedTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Clave Usuario: " + CStr(Me.ClaveLabel1.Text) + " / " + Me.FechaSalidaMaskedTextBox.Name, fechabaja, Me.FechaSalidaMaskedTextBox.Text, LocClv_Ciudad)
                        'CATV
                        If Me.CATVCheckBox.CheckState = CheckState.Checked Then
                            'CATV = "True"
                            validacion2 = "True"
                        Else
                            'CATV = "False"
                            validacion2 = "False"
                        End If
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Clave Usuario: " + CStr(Me.ClaveLabel1.Text) + " / Controldeclientes", CATV, validacion2, LocClv_Ciudad)

                        'Facturacion
                        If Me.FacturacionCheckBox.CheckState = CheckState.Checked Then
                            'facturacion = "True"
                            validacion3 = "True"
                        Else
                            'facturacion = "False"
                            validacion3 = "False"
                        End If
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Clave Usuario: " + CStr(Me.ClaveLabel1.Text) + " / Facturacion", facturacion, validacion3, LocClv_Ciudad)
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Clave Usuario: " + CStr(Me.ClaveLabel1.Text) + " / Grupo Ventas", GrupoVentas, Me.GrupoComboBox.Text, LocClv_Ciudad)

                    ElseIf opcion = "N" Then
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Agrego un nuevo Usuario", "", "Agrego Un Nuevo Usuario: " + Me.Clv_UsuarioTextBox.Text, LocClv_Ciudad)
                    End If
                Case 1
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Elimino Un Usuario", "", "Clave Usuario: " + CStr(Me.ClaveLabel1.Text), LocClv_Ciudad)
            End Select
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub



    Private Sub BUSCA(ByVal CLAVE As Integer)
        Try
            'Dim CON As New SqlConnection(MiConexion)
            'CON.Open()
            'Me.CONUSUARIOSTableAdapter.Connection = CON
            'Me.CONUSUARIOSTableAdapter.Fill(Me.NewSofTvDataSet.CONUSUARIOS, New System.Nullable(Of Integer)(CType(CLAVE, Integer)))
            'CON.Close()
            CONUSUARIOS(CLAVE)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.CONUSUARIOSTableAdapter.Connection = CON
        Me.CONUSUARIOSTableAdapter.Delete(gloClave)
        If UCase(Me.ComboBox1.Text) = UCase("Control Total") Then
            Me.ConRelUsuarioEmailTableAdapter.Connection = CON
            Me.ConRelUsuarioEmailTableAdapter.Delete(Me.Clv_UsuarioTextBox.Text)
        End If
        guardadatosbitacora(1)
        GloBnd = True
        CON.Close()
        Me.Close()
    End Sub

    Private Sub CONUSUARIOSBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONUSUARIOSBindingNavigatorSaveItem.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.Validate()
            MODUSUARIOS(gloClave, Me.Clv_UsuarioTextBox.Text, Me.NombreTextBox.Text, Me.DomicilioTextBox.Text, Me.ColoniaTextBox.Text, _
                        Me.dtpFechaIngreso.Value, Me.dtpFechaSalida.Value, Me.ActivoCheckBox.Checked, Me.PasaporteTextBox.Text, _
                         CInt(Me.ComboBox1.SelectedValue), Me.CATVCheckBox.Checked, Me.FacturacionCheckBox.Checked, 0, 0)
            'Me.CONUSUARIOSBindingSource.EndEdit()
            'Me.CONUSUARIOSTableAdapter.Connection = CON
            'Me.CONUSUARIOSTableAdapter.Update(Me.NewSofTvDataSet.CONUSUARIOS)
            If UCase(Me.ComboBox1.Text) = UCase("Control Total") Then
                Me.ConRelUsuarioEmailTableAdapter.Connection = CON
                Me.ConRelUsuarioEmailTableAdapter.Insert(Me.Clv_UsuarioTextBox.Text, Me.EmailTextBox.Text)
            End If
            Me.NueRelUsuarioGrupoVentasTableAdapter.Connection = CON
            Me.NueRelUsuarioGrupoVentasTableAdapter.Fill(Me.DataSetEric2.NueRelUsuarioGrupoVentas, Me.Clv_UsuarioTextBox.Text, CInt(Me.GrupoComboBox.SelectedValue))
            MsgBox(mensaje5)
            guardadatosbitacora(0)
            GloBnd = True
            CON.Close()
            Me.Close()
        Catch ex As System.Exception
            'System.Windows.Forms.MessageBox.Show(ex.Message)
            System.Windows.Forms.MessageBox.Show("La Clave del Usuario ya exíste., Teclee Otra por favor")
        End Try
    End Sub

    Private Sub FrmUsuarios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MUESTRATIPOUSUARIOS' Puede moverla o quitarla según sea necesario.
        Me.MUESTRATIPOUSUARIOSTableAdapter.Connection = CON
        Me.MUESTRATIPOUSUARIOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATIPOUSUARIOS)
        If opcion = "N" Then
            'Me.CONUSUARIOSBindingSource.AddNew()
            gloClave = NUEUSUARIOS("", "", "", "", CDate(Today), CDate(Today), 0, "", 0, 0, 0, 0, 0)
            ASIGNAfeCHA()
            Me.FechaIngresoMaskedTextBox.Text = Me.FECHA
            Panel1.Enabled = True
            BuscaRelUsuarioGrupoVentas()
            
        ElseIf opcion = "C" Then
            Panel1.Enabled = False
            BUSCA(gloClave)
            If UCase(Me.ComboBox1.Text) = UCase("Control Total") Then
                Me.ConRelUsuarioEmailTableAdapter.Connection = CON
                Me.ConRelUsuarioEmailTableAdapter.Fill(Me.DataSetEric.ConRelUsuarioEmail, Me.Clv_UsuarioTextBox.Text)
                Me.Label1.Visible = True
                Me.EmailTextBox.Visible = True
            Else
                Me.EmailTextBox.Visible = False
                Me.Label1.Visible = False
            End If
            BuscaRelUsuarioGrupoVentas()
            
        ElseIf opcion = "M" Then
            Panel1.Enabled = True
            BUSCA(gloClave)
            If UCase(Me.ComboBox1.Text) = UCase("Control Total") Then
                Me.ConRelUsuarioEmailTableAdapter.Connection = CON
                Me.ConRelUsuarioEmailTableAdapter.Fill(Me.DataSetEric.ConRelUsuarioEmail, Me.Clv_UsuarioTextBox.Text)
                Me.Label1.Visible = True
                Me.EmailTextBox.Visible = True

            Else
                Me.EmailTextBox.Visible = False
                Me.Label1.Visible = False
            End If
            BuscaRelUsuarioGrupoVentas()
            damedatosbitacora()
        End If
        CON.Close()
    End Sub
    Private Sub BuscaRelUsuarioGrupoVentas()
        Dim CON2 As New SqlConnection(MiConexion)
        If opcion = "N" Then
            CON2.Open()
            Me.ConGrupoVentasTableAdapter.Connection = CON2
            Me.ConGrupoVentasTableAdapter.Fill(Me.DataSetEric2.ConGrupoVentas, 0, 2)
            CON2.Close()
        ElseIf opcion = "C" Or opcion = "M" Then
            CON2.Open()
            Me.ConRelUsuarioGrupoVentasTableAdapter.Connection = CON2
            Me.ConRelUsuarioGrupoVentasTableAdapter.Fill(Me.DataSetEric2.ConRelUsuarioGrupoVentas, Me.Clv_UsuarioTextBox.Text, eClv_Grupo)
            CON2.Close()
            CON2.Open()
            Me.ConGrupoVentasTableAdapter.Connection = CON2
            Me.ConGrupoVentasTableAdapter.Fill(Me.DataSetEric2.ConGrupoVentas, CInt(eClv_Grupo), 1)
            CON2.Close()
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        BORUSUARIOS(gloClave)
        Me.Close()
    End Sub

    Private Sub ClaveLabel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClaveLabel1.Click
        'gloClave = Me.ClaveLabel1.Text
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Me.CONUSUARIOSBindingSource.CancelEdit()
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub ASIGNAfeCHA()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            FECHA = Today
            Me.DAMEFECHADELSERVIDORTableAdapter.Connection = CON
            Me.DAMEFECHADELSERVIDORTableAdapter.Fill(Me.NewSofTvDataSet.DAMEFECHADELSERVIDOR, FECHA)
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Clv_TipoUsuarioTextBox.Text = Me.ComboBox1.SelectedValue
        If UCase(Me.ComboBox1.Text) = UCase("Control Total") Then
            Me.ConRelUsuarioEmailTableAdapter.Connection = CON
            Me.ConRelUsuarioEmailTableAdapter.Fill(Me.DataSetEric.ConRelUsuarioEmail, Me.Clv_UsuarioTextBox.Text)
            Me.Label1.Visible = True
            Me.EmailTextBox.Visible = True
        Else
            Me.EmailTextBox.Visible = False
            Me.Label1.Visible = False
        End If
        CON.Close()
    End Sub

    Private Sub NombreTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NombreTextBox.KeyPress
        'MsgBox(e.KeyChar & " " & Asc(e.KeyChar) & " " & Asc(LCase(e.KeyChar)))
        e.KeyChar = Chr((ValidaKey(Me.NombreTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub NombreTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NombreTextBox.TextChanged

    End Sub

    Private Sub DomicilioTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles DomicilioTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.DomicilioTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub DomicilioTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DomicilioTextBox.TextChanged

    End Sub

    Private Sub ColoniaTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles ColoniaTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.ColoniaTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub ColoniaTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ColoniaTextBox.TextChanged

    End Sub

    Private Sub MODUSUARIOS(ByVal prmClave As Integer, ByVal prmClvUsuario As String, ByVal prmNombre As String, ByVal prmDomicilio As String, _
                            ByVal prmColonia As String, ByVal prmFechaIngreso As Date, ByVal prmFechaSalida As Date, ByVal prmActivo As Boolean, _
                            ByVal prmPasaporte As String, ByVal prmClvTipoUsuario As Integer, ByVal prmCatv As Boolean, ByVal prmFacturacion As Boolean, _
                            ByVal prmBoletos As Boolean, ByVal prmMizar As Integer)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clave", SqlDbType.Int, prmClave)
            BaseII.CreateMyParameter("@Clv_Usuario", SqlDbType.VarChar, prmClvUsuario, 5)
            BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, prmNombre, 100)
            BaseII.CreateMyParameter("@Domicilio", SqlDbType.VarChar, prmDomicilio, 60)
            BaseII.CreateMyParameter("@Colonia", SqlDbType.VarChar, prmColonia, 50)
            BaseII.CreateMyParameter("@FechaIngreso", SqlDbType.DateTime, prmFechaIngreso)
            BaseII.CreateMyParameter("@FechaSalida", SqlDbType.DateTime, prmFechaSalida)
            BaseII.CreateMyParameter("@Activo", SqlDbType.Bit, prmActivo)
            BaseII.CreateMyParameter("@Pasaporte", SqlDbType.VarChar, prmPasaporte, 100)
            BaseII.CreateMyParameter("@Clv_TipoUsuario", SqlDbType.Int, prmClvTipoUsuario)
            BaseII.CreateMyParameter("@CATV", SqlDbType.Bit, prmCatv)
            BaseII.CreateMyParameter("@Facturacion", SqlDbType.Bit, prmFacturacion)
            BaseII.CreateMyParameter("@Boletos", SqlDbType.Bit, prmBoletos)
            BaseII.CreateMyParameter("@Mizar_AN", SqlDbType.Int, prmMizar)
            BaseII.Inserta("MODUSUARIOS")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub CONUSUARIOS(ByVal prmClave As Integer)
        Try
            Dim DT As New DataTable

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clave", SqlDbType.Int, prmClave)
            DT = BaseII.ConsultaDT("CONUSUARIOS")

            For Each row As DataRow In DT.Rows
                gloClave = CInt(row("Clave").ToString)
                Me.ClaveLabel1.Text = CInt(row("Clave").ToString)
                Me.Clv_UsuarioTextBox.Text = row("Clv_Usuario").ToString
                Me.NombreTextBox.Text = row("Nombre").ToString
                Me.DomicilioTextBox.Text = row("Domicilio").ToString
                Me.ColoniaTextBox.Text = row("Colonia").ToString
                Me.dtpFechaIngreso.Text = CDate(row("FechaIngreso").ToString)
                Me.dtpFechaSalida.Text = CDate(row("FechaSalida").ToString)
                Me.ActivoCheckBox.Checked = CBool(row("Activo").ToString)
                Me.PasaporteTextBox.Text = row("Pasaporte").ToString
                Me.ComboBox1.SelectedValue = CInt(row("Clv_TipoUsuario").ToString)
                Me.CATVCheckBox.Checked = CBool(row("CATV").ToString)
                Me.FacturacionCheckBox.Checked = CBool(row("Facturacion").ToString)
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function NUEUSUARIOS(ByVal prmClvUsuario As String, ByVal prmNombre As String, ByVal prmDomicilio As String, _
                            ByVal prmColonia As String, ByVal prmFechaIngreso As Date, ByVal prmFechaSalida As Date, ByVal prmActivo As Boolean, _
                            ByVal prmPasaporte As String, ByVal prmClvTipoUsuario As Integer, ByVal prmCatv As Boolean, ByVal prmFacturacion As Boolean, _
                            ByVal prmBoletos As Boolean, ByVal prmMizar As Integer) As Integer
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Usuario", SqlDbType.VarChar, prmClvUsuario, 5)
            BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, prmNombre, 100)
            BaseII.CreateMyParameter("@Domicilio", SqlDbType.VarChar, prmDomicilio, 60)
            BaseII.CreateMyParameter("@Colonia", SqlDbType.VarChar, prmColonia, 50)
            BaseII.CreateMyParameter("@FechaIngreso", SqlDbType.DateTime, prmFechaIngreso)
            BaseII.CreateMyParameter("@FechaSalida", SqlDbType.DateTime, prmFechaSalida)
            BaseII.CreateMyParameter("@Activo", SqlDbType.Bit, prmActivo)
            BaseII.CreateMyParameter("@Pasaporte", SqlDbType.VarChar, prmPasaporte, 100)
            BaseII.CreateMyParameter("@Clv_TipoUsuario", SqlDbType.Int, prmClvTipoUsuario)
            BaseII.CreateMyParameter("@CATV", SqlDbType.Bit, prmCatv)
            BaseII.CreateMyParameter("@Facturacion", SqlDbType.Bit, prmFacturacion)
            BaseII.CreateMyParameter("@Boletos", SqlDbType.Bit, prmBoletos)
            BaseII.CreateMyParameter("@Mizar_AN", SqlDbType.Int, prmMizar)
            BaseII.CreateMyParameter("@Clave", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("NUEUSUARIOS")

            NUEUSUARIOS = CInt(BaseII.dicoPar("@Clave").ToString())
            '@Clave int OUTPUT
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub BORUSUARIOS(ByVal prmClave As Integer)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clave", SqlDbType.Int, prmClave)
            BaseII.Inserta("BORUSUARIOS")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class