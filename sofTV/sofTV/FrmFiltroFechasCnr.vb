﻿Public Class FrmFiltroFechasCnr

    Private Sub rbPorContrato_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbPorContrato.CheckedChanged
        If Me.rbPorContrato.Checked Then
            Me.pnlContrato.Enabled = True
            Me.pnlFechas.Enabled = False
        End If
    End Sub

    Private Sub rbPorFechas_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbPorFechas.CheckedChanged
        If Me.rbPorFechas.Checked Then
            Me.txtContrato.Clear()
            Me.pnlContrato.Enabled = False
            Me.pnlFechas.Enabled = True
            Me.txtContrato.Text = 0
        End If
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub FrmFechasCobroMaterial_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.dtpFechaFin.MaxDate = Today
        Me.dtpFechaIni.MaxDate = Me.dtpFechaFin.Value
    End Sub

    Private Sub dtpFechaFin_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFechaFin.ValueChanged
        Me.dtpFechaFin.MaxDate = Today
        Me.dtpFechaIni.MaxDate = Me.dtpFechaFin.Value
    End Sub

    Private Sub btnGenerar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerar.Click
        If Me.dtpFechaFin.Value < Me.dtpFechaIni.Value Then
            MsgBox("La Fecha Final no puede ser menor a la Fecha Inicial", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.rbPorContrato.Checked Then
            If IsNumeric(Me.txtContrato.Text) = False Then
                MsgBox("Ingrese un Contrato válido", MsgBoxStyle.Information)
                Exit Sub
            End If
        End If
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub
End Class