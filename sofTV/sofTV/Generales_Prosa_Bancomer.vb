Imports System.Text
Imports System.Data.SqlClient
Public Class Generales_Prosa_Bancomer

    Private Sub CMBBtnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBBtnSalir.Click
        Me.Close()
    End Sub


    Private Sub CMBBtnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBBtnGuardar.Click
        Dim NomComercio As New StringBuilder
        Dim NOMBRE As String = Nothing
        If Me.TxtAfiliacion.Text.Length < 7 Then
            MsgBox("El N�mero De Afiliaci�n Debe Ser Al Menos De 7 Car�cteres", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Me.TxtNomComercio.Text.Length = 0 Then
            MsgBox("Captura el Nombre del Comercio.", MsgBoxStyle.Information)
            Exit Sub
        End If
        '    NomComercio.Append(Me.TxtNomComercio.Text)
        '    NomComercio.Insert(Me.TxtNomComercio.Text.Length, " ", 10)
        '    NOMBRE = NomComercio.ToString
        '    'MsgBox(NomComercio.ToString, MsgBoxStyle.Information)
        'Else
        NOMBRE = Me.TxtNomComercio.Text
        'End If
        Guarda(Me.TxtAfiliacion.Text, NOMBRE)
        Me.Close()
    End Sub
    Private Sub Busca()
        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Try
            cmd = New SqlClient.SqlCommand()
            con.Open()
            With cmd
                .CommandText = "Consulta_Grales_Prosa_bancomer"
                .Connection = con
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm1 As New SqlParameter("@op", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = 0
                .Parameters.Add(prm1)

                Dim lector As SqlDataReader = .ExecuteReader()

                While lector.Read()
                    Me.TxtAfiliacion.Text = lector(0).ToString()
                    Me.TxtNomComercio.Text = lector(1).ToString()
                End While
            End With
            con.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Guarda(ByVal Afiliacion As String, ByVal NomCOmercio As String)
        Dim con As New SqlClient.SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Try
            cmd = New SqlClient.SqlCommand()
            con.Open()
            With cmd
                .CommandText = "Nuevo_Grales_Prosa_bancomer"
                .Connection = con
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                '@No_afiliacion varchar(7),@nom_comercio varchar(10)
                Dim prm As New SqlParameter("@No_afiliacion", SqlDbType.VarChar, 7)
                prm.Direction = ParameterDirection.Input
                prm.Value = Afiliacion
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@nom_comercio", SqlDbType.VarChar, 250)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = NomCOmercio
                .Parameters.Add(prm1)

                Dim i As Integer = .ExecuteNonQuery()
            End With
            con.Close()
            MsgBox("Se Guardo Con Exito", MsgBoxStyle.Information)
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Generales_Prosa_Bancomer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Busca()
    End Sub

    Private Sub TxtAfiliacion_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtAfiliacion.KeyPress
        e.KeyChar = ChrW(ValidaKey(TxtAfiliacion, Asc(e.KeyChar), "N"))
    End Sub
End Class