Imports System.Data.SqlClient
Public Class FrmCatalogoNoTel
    Private error1 As Integer = Nothing
    Public KeyAscii As Short
    Private Sub validacion(ByVal tel As String)
        Dim con1 As New SqlConnection(MiConexion)
        Dim cmd7 As New SqlClient.SqlCommand()
        Try
            con1.Open()
            cmd7 = New SqlClient.SqlCommand()
            With cmd7
                .CommandText = "validacion_telefonos"
                .CommandTimeout = 0
                .Connection = con1
                .CommandType = CommandType.StoredProcedure

                Dim prm1 As New SqlParameter("@clave", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = CLng(Me.ClaveTextBox.Text)
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@tel", SqlDbType.VarChar, 30)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = tel
                .Parameters.Add(prm2)

                Dim prm3 As New SqlParameter("@error", SqlDbType.Int)
                prm3.Direction = ParameterDirection.Output
                prm3.Value = 0
                .Parameters.Add(prm3)

                Dim ia As Integer = cmd7.ExecuteNonQuery()

                error1 = prm3.Value
            End With
            con1.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Function SoloNumeros(ByVal Keyascii As Short) As Short
        If InStr("1234567890", Chr(Keyascii)) = 0 Then
            SoloNumeros = 0
        Else
            SoloNumeros = Keyascii
        End If
        Select Case Keyascii
            Case 8
                SoloNumeros = Keyascii
            Case 13
                SoloNumeros = Keyascii
        End Select
    End Function

    Private Sub Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Click
        Dim fecha1 As String = Nothing
        Dim fecha2 As String = Nothing
        Dim fecha3 As String = Nothing
        'Dim validacion As Integer = Nothing

        If Me.Numero_telTextBox.Text = "" Then
            MsgBox("No Se Ha Capturado El Num�ro de Tel�fono", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Me.StatusComboBox.Text = "" Then
            MsgBox("Seleccione El Status Del Num�ro Telefonico", MsgBoxStyle.Information)
            Exit Sub
        End If

        If LocOpTel = "N" Then
            validacion(Me.Numero_telTextBox.Text)
            If error1 = 0 Then
                Dim CON2 As New SqlConnection(MiConexion)
                Me.Validate()
                Me.Consulta_Cat_Num_TelefonoBindingSource.EndEdit()
                CON2.Open()
                Me.Consulta_Cat_Num_TelefonoTableAdapter.Connection = CON2
                Me.Consulta_Cat_Num_TelefonoTableAdapter.Update(Me.Procedimientosarnoldo4.Consulta_Cat_Num_Telefono)
                CON2.Close()
                MsgBox("Se Guardo Con Exito", MsgBoxStyle.Information)
                LocBndTel = True
                Me.Close()
            ElseIf error1 = 1 Then
                MsgBox("El Numero Telefonico ya Existe", MsgBoxStyle.Information)
                Exit Sub
            End If
        ElseIf LocOpTel = "M" Then
            validacion(Me.Numero_telTextBox.Text)
            If error1 = 0 Then
                Dim CON5 As New SqlConnection(MiConexion)
                Dim cmd As New SqlClient.SqlCommand
                cmd = New SqlClient.SqlCommand
                CON5.Open()
                With cmd
                    .CommandText = "Modifica_Cat_Num_Telefono"
                    .Connection = CON5
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0
                    '] (@clv_telefono bigint,@notelefono varchar(max),@status varchar(1),@Ultima_fecha_Asig datetime,@Ultima_fecha_Liberacion datetime,}
                    '@Ultimo_cliente_asigno bigint,@Fecha_cuarentena datetime)
                    Dim prm As New SqlParameter("@clv_telefono", SqlDbType.BigInt)
                    Dim prm1 As New SqlParameter("@notelefono", SqlDbType.VarChar)
                    Dim prm2 As New SqlParameter("@status", SqlDbType.VarChar, 1)
                    Dim prm3 As New SqlParameter("@Ultima_fecha_Asig", SqlDbType.DateTime)
                    Dim prm4 As New SqlParameter("@Ultima_fecha_Liberacion", SqlDbType.DateTime)
                    Dim prm5 As New SqlParameter("@Ultimo_cliente_asigno", SqlDbType.BigInt)
                    Dim prm6 As New SqlParameter("@Fecha_cuarentena", SqlDbType.DateTime)
                    Dim prm7 As New SqlParameter("@Estado", SqlDbType.VarChar, 1)
                    prm.Direction = ParameterDirection.Input
                    prm1.Direction = ParameterDirection.Input
                    prm2.Direction = ParameterDirection.Input
                    prm3.Direction = ParameterDirection.Input
                    prm4.Direction = ParameterDirection.Input
                    prm5.Direction = ParameterDirection.Input
                    prm6.Direction = ParameterDirection.Input
                    prm7.Direction = ParameterDirection.Input

                    If Me.Ultima_FechaAsigTextBox.Text = "" Then
                        fecha1 = "01/01/1900"
                    Else
                        fecha1 = Me.Ultima_FechaAsigTextBox.Text
                    End If

                    If Me.Ultima_Fecha_LiberacionTextBox.Text = "" Then
                        fecha2 = "01/01/1900"
                    Else
                        fecha2 = Me.Ultima_Fecha_LiberacionTextBox.Text
                    End If

                    If Me.Fecha_CuarentenaTextBox.Text = "" Then
                        fecha3 = "01/01/1900"
                    Else
                        fecha3 = Me.Fecha_CuarentenaTextBox.Text
                    End If

                    prm.Value = CLng(Me.ClaveTextBox.Text)
                    prm1.Value = Me.Numero_telTextBox.Text
                    prm2.Value = Me.StatusTextBox.Text
                    prm3.Value = fecha1
                    prm4.Value = fecha2
                    If IsNumeric(Me.Ult_Cliente_AsignadoTextBox.Text) = True Then
                        prm5.Value = CLng(Me.Ult_Cliente_AsignadoTextBox.Text)
                    Else
                        prm5.Value = 0
                    End If
                    prm6.Value = fecha3
                    prm7.Value = Me.ComboBox1.SelectedValue

                    .Parameters.Add(prm)
                    .Parameters.Add(prm1)
                    .Parameters.Add(prm2)
                    .Parameters.Add(prm3)
                    .Parameters.Add(prm4)
                    .Parameters.Add(prm5)
                    .Parameters.Add(prm6)
                    .Parameters.Add(prm7)

                    Dim i As Integer = cmd.ExecuteNonQuery()
                End With
                CON5.Close()
                MsgBox("Se Guardo Con Exito", MsgBoxStyle.Information)
                LocBndTel = True
                Me.Close()
            ElseIf error1 = 1 Then
                MsgBox("El Numero Telefonico Ya Existe", MsgBoxStyle.Information)
                Exit Sub
            End If
        End If



    End Sub

    Private Sub Busca(ByVal Clave As Long)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Muestra_Status_No_TelefonicoTableAdapter.Connection = CON
        Me.Muestra_Status_No_TelefonicoTableAdapter.Fill(Me.Procedimientosarnoldo4.Muestra_Status_No_Telefonico, 0)
        'Me.StatusComboBox.Text = ""
        Me.Muestra_Estado_No_TelefonicoTableAdapter.Connection = CON
        Me.Muestra_Estado_No_TelefonicoTableAdapter.Fill(Me.DataSetLidia2.Muestra_Estado_No_Telefonico, 0)
        Me.Consulta_Cat_Num_TelefonoTableAdapter.Connection = CON
        Me.Consulta_Cat_Num_TelefonoTableAdapter.Fill(Me.Procedimientosarnoldo4.Consulta_Cat_Num_Telefono, Clave)
        CON.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub


    Private Sub FrmCatalogoNoTel_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Dim CON1 As New SqlConnection(MiConexion)
        Select Case LocOpTel
            Case "N"
                Me.Consulta_Cat_Num_TelefonoBindingSource.AddNew()
                CON1.Open()
                Me.Muestra_Status_No_TelefonicoTableAdapter.Connection = CON1
                Me.Muestra_Status_No_TelefonicoTableAdapter.Fill(Me.Procedimientosarnoldo4.Muestra_Status_No_Telefonico, 0)
                Me.StatusComboBox.SelectedValue = "D"
                Me.Muestra_Estado_No_TelefonicoTableAdapter.Connection = CON1
                Me.Muestra_Estado_No_TelefonicoTableAdapter.Fill(Me.DataSetLidia2.Muestra_Estado_No_Telefonico, 0)
                Me.ComboBox1.SelectedValue = "P"
                CON1.Close()
            Case "C"
                Me.CMBPanel1.Enabled = False
                Busca(LocClv_Tel)
            Case "M"
                Busca(LocClv_Tel)
        End Select

    End Sub

    Private Sub StatusComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StatusComboBox.SelectedIndexChanged
        If Me.StatusComboBox.Text <> "" Then
            Me.StatusTextBox.Text = Me.StatusComboBox.SelectedValue
        End If
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON3 As New SqlConnection(MiConexion)
        CON3.Open()
        Me.Consulta_Cat_Num_TelefonoTableAdapter.Connection = CON3
        Me.Consulta_Cat_Num_TelefonoTableAdapter.Delete(LocClv_Tel)
        MsgBox("Se Borro Con Exito", MsgBoxStyle.Information)
        CON3.Close()
        LocBndTel = True
        Me.Close()
    End Sub


    Private Sub StatusTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StatusTextBox.TextChanged
        If LocOpTel = "M" Then
            Me.StatusComboBox.SelectedValue = Me.StatusTextBox.Text
        End If
    End Sub

    Private Sub Numero_telTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Numero_telTextBox.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
    End Sub

End Class
