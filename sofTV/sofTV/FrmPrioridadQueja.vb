﻿Public Class FrmPrioridadQueja

    Private _proClave As Integer = 0
    Public Property proClave As Integer
        Get
            Return _proClave
        End Get
        Set(ByVal value As Integer)
            _proClave = value
        End Set
    End Property

    Private _proConcepto As String
    Public Property proConcepto As String
        Get
            Return _proConcepto
        End Get
        Set(ByVal value As String)
            _proConcepto = value
        End Set
    End Property

   
    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Dim Clave As Integer = 0
        Dim Concepto As String = Me.txtConcepto.Text

        If (Me.txtClave.Text <> "") And (IsNumeric(Me.txtClave.Text)) Then
            Clave = CType(Me.txtClave.Text, Integer)
        End If

        Select Case opcion
            Case "N"
                sofTV.BAL.PrioridadQueja.Add(Clave, Concepto)
            Case "M"
                sofTV.BAL.PrioridadQueja.Edit(Clave, Concepto)
        End Select
        Me.Close()
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Dim Clave As Integer = 0

        If (Me.txtClave.Text <> "") And (IsNumeric(Me.txtClave.Text)) Then
            Clave = CType(Me.txtClave.Text, Integer)
        End If
        sofTV.BAL.PrioridadQueja.Delete(Clave)
        Me.Close()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub FrmPrioridadQueja_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Select Case opcion
            Case "N"
                Me.txtClave.Text = "0"
                Me.txtConcepto.Enabled = True
                Me.btnEliminar.Enabled = False
                Me.btnGuardar.Enabled = True
            Case "C"
                Me.txtConcepto.Enabled = False
                Me.btnEliminar.Enabled = False
                Me.btnGuardar.Enabled = False
                Me.txtClave.Text = Me.proClave.ToString
                Me.txtConcepto.Text = Me.proConcepto
            Case "M"
                Me.txtConcepto.Enabled = True
                Me.btnEliminar.Enabled = True
                Me.btnGuardar.Enabled = True
                Me.txtClave.Text = Me.proClave.ToString
                Me.txtConcepto.Text = Me.proConcepto
        End Select
        

    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
End Class