﻿Imports System.Data.SqlClient

Public Class FrmTiposColonia
    Private nombretipo As String = Nothing
    Private Sub damedatosbitacora()
        Try
            If opcion = "M" Then
                nombretipo = Me.NombreTextBox.Text
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub guardabitacora(ByVal op As Integer)
        Select Case op
            Case 0
                If opcion = "N" Then
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se guardo un nuevo tipo de colonia", "", "Se capturo un nuevo tipo de colonia: " + Me.NombreTextBox.Text, LocClv_Ciudad)
                ElseIf opcion = "M" Then
                    'nombretipo = Me.NombreTextBox.Text
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.NombreTextBox.Name, nombretipo, Me.NombreTextBox.Text, LocClv_Ciudad)
                End If
            Case 1
                bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Elimino Un Tipo de colonia", "", "Se Elimino Un Tipo de Colonia:" + Me.NombreTextBox.Text, LocClv_Ciudad)
        End Select

    End Sub


    Private Sub BUSCA(ByVal CLAVE As Integer)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.CONTipo_Colonias1TableAdapter.Connection = CON
            Me.CONTipo_Colonias1TableAdapter.Fill(Me.NewSofTvDataSet.CONTipo_Colonias1, New System.Nullable(Of Integer)(CType(CLAVE, Integer)))
            damedatosbitacora()
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub FrmTiposColonia_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If opcion = "N" Then
            Me.CONTipoColonias1BindingSource.AddNew()
            Panel1.Enabled = True
        ElseIf opcion = "C" Then
            Panel1.Enabled = False
            BUSCA(gloClave)
        ElseIf opcion = "M" Then
            Panel1.Enabled = True
            BUSCA(gloClave)
        End If
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Me.CONTipoColonias1BindingSource.CancelEdit()
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub CONCIUDADESBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONCIUDADESBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.CONTipoColonias1BindingSource.EndEdit()
        Me.CONTipo_Colonias1TableAdapter.Connection = CON
        Me.CONTipo_Colonias1TableAdapter.Update(Me.NewSofTvDataSet.CONTipo_Colonias1)
        MsgBox(mensaje5)
        guardabitacora(0)
        GloBnd = True
        CON.Close()
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.CONTipo_Colonias1TableAdapter.Connection = CON
        Me.CONTipo_Colonias1TableAdapter.Delete(gloClave)
        guardabitacora(1)
        GloBnd = True
        CON.Close()
        Me.Close()
    End Sub

    Private Sub Clv_CiudadTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_CiudadTextBox.TextChanged
        gloClave = Me.Clv_CiudadTextBox.Text
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub NombreTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NombreTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(NombreTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub NombreTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NombreTextBox.TextChanged

    End Sub
End Class