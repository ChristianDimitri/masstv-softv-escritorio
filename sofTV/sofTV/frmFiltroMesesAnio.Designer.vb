﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFiltroMesesAnio
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbxMesAnio = New System.Windows.Forms.GroupBox()
        Me.lblEjemploAnio = New System.Windows.Forms.Label()
        Me.txtInsertaAnio = New System.Windows.Forms.TextBox()
        Me.cmbMeses = New System.Windows.Forms.ComboBox()
        Me.lblAnio = New System.Windows.Forms.Label()
        Me.lblMes = New System.Windows.Forms.Label()
        Me.bntAceptar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.gbxMesAnio.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxMesAnio
        '
        Me.gbxMesAnio.Controls.Add(Me.lblEjemploAnio)
        Me.gbxMesAnio.Controls.Add(Me.txtInsertaAnio)
        Me.gbxMesAnio.Controls.Add(Me.cmbMeses)
        Me.gbxMesAnio.Controls.Add(Me.lblAnio)
        Me.gbxMesAnio.Controls.Add(Me.lblMes)
        Me.gbxMesAnio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxMesAnio.Location = New System.Drawing.Point(12, 12)
        Me.gbxMesAnio.Name = "gbxMesAnio"
        Me.gbxMesAnio.Size = New System.Drawing.Size(321, 103)
        Me.gbxMesAnio.TabIndex = 0
        Me.gbxMesAnio.TabStop = False
        '
        'lblEjemploAnio
        '
        Me.lblEjemploAnio.AutoSize = True
        Me.lblEjemploAnio.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.lblEjemploAnio.Location = New System.Drawing.Point(140, 65)
        Me.lblEjemploAnio.Name = "lblEjemploAnio"
        Me.lblEjemploAnio.Size = New System.Drawing.Size(71, 16)
        Me.lblEjemploAnio.TabIndex = 4
        Me.lblEjemploAnio.Text = "(ej. 2012)"
        '
        'txtInsertaAnio
        '
        Me.txtInsertaAnio.Location = New System.Drawing.Point(57, 62)
        Me.txtInsertaAnio.MaxLength = 4
        Me.txtInsertaAnio.Name = "txtInsertaAnio"
        Me.txtInsertaAnio.Size = New System.Drawing.Size(77, 22)
        Me.txtInsertaAnio.TabIndex = 3
        '
        'cmbMeses
        '
        Me.cmbMeses.DisplayMember = "nombreMes"
        Me.cmbMeses.FormattingEnabled = True
        Me.cmbMeses.Location = New System.Drawing.Point(55, 21)
        Me.cmbMeses.Name = "cmbMeses"
        Me.cmbMeses.Size = New System.Drawing.Size(258, 24)
        Me.cmbMeses.TabIndex = 2
        Me.cmbMeses.ValueMember = "numMes"
        '
        'lblAnio
        '
        Me.lblAnio.AutoSize = True
        Me.lblAnio.Location = New System.Drawing.Point(8, 65)
        Me.lblAnio.Name = "lblAnio"
        Me.lblAnio.Size = New System.Drawing.Size(43, 16)
        Me.lblAnio.TabIndex = 1
        Me.lblAnio.Text = "Año :"
        '
        'lblMes
        '
        Me.lblMes.AutoSize = True
        Me.lblMes.Location = New System.Drawing.Point(6, 25)
        Me.lblMes.Name = "lblMes"
        Me.lblMes.Size = New System.Drawing.Size(45, 16)
        Me.lblMes.TabIndex = 0
        Me.lblMes.Text = "Mes :"
        '
        'bntAceptar
        '
        Me.bntAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bntAceptar.Location = New System.Drawing.Point(31, 121)
        Me.bntAceptar.Name = "bntAceptar"
        Me.bntAceptar.Size = New System.Drawing.Size(110, 33)
        Me.bntAceptar.TabIndex = 1
        Me.bntAceptar.Text = "&Aceptar"
        Me.bntAceptar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(196, 121)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(110, 33)
        Me.btnCancelar.TabIndex = 2
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'frmFiltroMesesAnio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(344, 163)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.bntAceptar)
        Me.Controls.Add(Me.gbxMesAnio)
        Me.Name = "frmFiltroMesesAnio"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Seleccione Mes y Año"
        Me.gbxMesAnio.ResumeLayout(False)
        Me.gbxMesAnio.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxMesAnio As System.Windows.Forms.GroupBox
    Friend WithEvents lblAnio As System.Windows.Forms.Label
    Friend WithEvents lblMes As System.Windows.Forms.Label
    Friend WithEvents bntAceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Public WithEvents txtInsertaAnio As System.Windows.Forms.TextBox
    Public WithEvents cmbMeses As System.Windows.Forms.ComboBox
    Friend WithEvents lblEjemploAnio As System.Windows.Forms.Label
End Class
