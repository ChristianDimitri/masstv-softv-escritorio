<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelDatosP
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.MuestraColoniaRepBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetarnoldo = New sofTV.DataSetarnoldo()
        Me.ComboBox3 = New System.Windows.Forms.ComboBox()
        Me.MuestraCalleAsociada2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraCallesAsociadasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.ElegirServicioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ElegirServicioTableAdapter = New sofTV.DataSetarnoldoTableAdapters.ElegirServicioTableAdapter()
        Me.Muestra_Colonia_RepTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Muestra_Colonia_RepTableAdapter()
        Me.Muestra_Calles_AsociadasTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Muestra_Calles_AsociadasTableAdapter()
        Me.Muestra_Calle_Asociada2TableAdapter = New sofTV.DataSetarnoldoTableAdapters.Muestra_Calle_Asociada2TableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        CType(Me.MuestraColoniaRepBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraCalleAsociada2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraCallesAsociadasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.ElegirServicioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ComboBox2
        '
        Me.ComboBox2.DataSource = Me.MuestraColoniaRepBindingSource
        Me.ComboBox2.DisplayMember = "Nombre"
        Me.ComboBox2.Enabled = False
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(92, 135)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(256, 21)
        Me.ComboBox2.TabIndex = 1
        Me.ComboBox2.ValueMember = "clv_colonia"
        '
        'MuestraColoniaRepBindingSource
        '
        Me.MuestraColoniaRepBindingSource.DataMember = "Muestra_Colonia_Rep"
        Me.MuestraColoniaRepBindingSource.DataSource = Me.DataSetarnoldo
        '
        'DataSetarnoldo
        '
        Me.DataSetarnoldo.DataSetName = "DataSetarnoldo"
        Me.DataSetarnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ComboBox3
        '
        Me.ComboBox3.DataSource = Me.MuestraCalleAsociada2BindingSource
        Me.ComboBox3.DisplayMember = "Calle"
        Me.ComboBox3.FormattingEnabled = True
        Me.ComboBox3.Location = New System.Drawing.Point(92, 197)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(268, 21)
        Me.ComboBox3.TabIndex = 2
        Me.ComboBox3.ValueMember = "clv_calle"
        '
        'MuestraCalleAsociada2BindingSource
        '
        Me.MuestraCalleAsociada2BindingSource.DataMember = "Muestra_Calle_Asociada2"
        Me.MuestraCalleAsociada2BindingSource.DataSource = Me.DataSetarnoldo
        '
        'MuestraCallesAsociadasBindingSource
        '
        Me.MuestraCallesAsociadasBindingSource.DataMember = "Muestra_Calles_Asociadas"
        Me.MuestraCallesAsociadasBindingSource.DataSource = Me.DataSetarnoldo
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ComboBox1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 22)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(370, 82)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Tipo de Servicio."
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.ElegirServicioBindingSource
        Me.ComboBox1.DisplayMember = "Descripcion"
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(28, 40)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(320, 26)
        Me.ComboBox1.TabIndex = 1
        Me.ComboBox1.ValueMember = "clv_txt"
        '
        'ElegirServicioBindingSource
        '
        Me.ElegirServicioBindingSource.DataMember = "ElegirServicio"
        Me.ElegirServicioBindingSource.DataSource = Me.DataSetarnoldo
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(21, 136)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(65, 16)
        Me.CMBLabel1.TabIndex = 4
        Me.CMBLabel1.Text = "Colonia:"
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(37, 198)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(48, 16)
        Me.CMBLabel2.TabIndex = 5
        Me.CMBLabel2.Text = "Calle:"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Orange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(75, 274)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(136, 36)
        Me.Button2.TabIndex = 6
        Me.Button2.Text = "&ACEPTAR"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Orange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(224, 274)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 7
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'ElegirServicioTableAdapter
        '
        Me.ElegirServicioTableAdapter.ClearBeforeFill = True
        '
        'Muestra_Colonia_RepTableAdapter
        '
        Me.Muestra_Colonia_RepTableAdapter.ClearBeforeFill = True
        '
        'Muestra_Calles_AsociadasTableAdapter
        '
        Me.Muestra_Calles_AsociadasTableAdapter.ClearBeforeFill = True
        '
        'Muestra_Calle_Asociada2TableAdapter
        '
        Me.Muestra_Calle_Asociada2TableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'FrmSelDatosP
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(409, 339)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ComboBox3)
        Me.Controls.Add(Me.ComboBox2)
        Me.Name = "FrmSelDatosP"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Datos Por Calle"
        CType(Me.MuestraColoniaRepBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraCalleAsociada2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraCallesAsociadasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.ElegirServicioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ElegirServicioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DataSetarnoldo As sofTV.DataSetarnoldo
    Friend WithEvents ElegirServicioTableAdapter As sofTV.DataSetarnoldoTableAdapters.ElegirServicioTableAdapter
    Friend WithEvents MuestraColoniaRepBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Colonia_RepTableAdapter As sofTV.DataSetarnoldoTableAdapters.Muestra_Colonia_RepTableAdapter
    Friend WithEvents MuestraCallesAsociadasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Calles_AsociadasTableAdapter As sofTV.DataSetarnoldoTableAdapters.Muestra_Calles_AsociadasTableAdapter
    Friend WithEvents MuestraCalleAsociada2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Calle_Asociada2TableAdapter As sofTV.DataSetarnoldoTableAdapters.Muestra_Calle_Asociada2TableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
