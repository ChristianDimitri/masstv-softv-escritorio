﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTipoInstalaciones
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.gbxRegistrosNuevos = New System.Windows.Forms.GroupBox()
        Me.cbxActivoTipo = New System.Windows.Forms.CheckBox()
        Me.txtDescripcionTipo = New System.Windows.Forms.TextBox()
        Me.lblDescripcionTipo = New System.Windows.Forms.Label()
        Me.txtClaveTipo = New System.Windows.Forms.TextBox()
        Me.lblTipoInstalacion = New System.Windows.Forms.Label()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.gbxRegistrosNuevos.SuspendLayout()
        Me.SuspendLayout()
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'gbxRegistrosNuevos
        '
        Me.gbxRegistrosNuevos.Controls.Add(Me.cbxActivoTipo)
        Me.gbxRegistrosNuevos.Controls.Add(Me.txtDescripcionTipo)
        Me.gbxRegistrosNuevos.Controls.Add(Me.lblDescripcionTipo)
        Me.gbxRegistrosNuevos.Controls.Add(Me.txtClaveTipo)
        Me.gbxRegistrosNuevos.Controls.Add(Me.lblTipoInstalacion)
        Me.gbxRegistrosNuevos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxRegistrosNuevos.Location = New System.Drawing.Point(12, 12)
        Me.gbxRegistrosNuevos.Name = "gbxRegistrosNuevos"
        Me.gbxRegistrosNuevos.Size = New System.Drawing.Size(401, 152)
        Me.gbxRegistrosNuevos.TabIndex = 0
        Me.gbxRegistrosNuevos.TabStop = False
        Me.gbxRegistrosNuevos.Text = "Registros"
        '
        'cbxActivoTipo
        '
        Me.cbxActivoTipo.AutoSize = True
        Me.cbxActivoTipo.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cbxActivoTipo.Location = New System.Drawing.Point(48, 124)
        Me.cbxActivoTipo.Name = "cbxActivoTipo"
        Me.cbxActivoTipo.Size = New System.Drawing.Size(78, 20)
        Me.cbxActivoTipo.TabIndex = 4
        Me.cbxActivoTipo.Text = "Activo :"
        Me.cbxActivoTipo.UseVisualStyleBackColor = True
        '
        'txtDescripcionTipo
        '
        Me.txtDescripcionTipo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcionTipo.Location = New System.Drawing.Point(111, 59)
        Me.txtDescripcionTipo.Multiline = True
        Me.txtDescripcionTipo.Name = "txtDescripcionTipo"
        Me.txtDescripcionTipo.Size = New System.Drawing.Size(284, 50)
        Me.txtDescripcionTipo.TabIndex = 3
        '
        'lblDescripcionTipo
        '
        Me.lblDescripcionTipo.AutoSize = True
        Me.lblDescripcionTipo.Location = New System.Drawing.Point(8, 62)
        Me.lblDescripcionTipo.Name = "lblDescripcionTipo"
        Me.lblDescripcionTipo.Size = New System.Drawing.Size(99, 16)
        Me.lblDescripcionTipo.TabIndex = 2
        Me.lblDescripcionTipo.Text = "Descripción :"
        '
        'txtClaveTipo
        '
        Me.txtClaveTipo.Location = New System.Drawing.Point(111, 21)
        Me.txtClaveTipo.Name = "txtClaveTipo"
        Me.txtClaveTipo.ReadOnly = True
        Me.txtClaveTipo.Size = New System.Drawing.Size(100, 22)
        Me.txtClaveTipo.TabIndex = 1
        '
        'lblTipoInstalacion
        '
        Me.lblTipoInstalacion.AutoSize = True
        Me.lblTipoInstalacion.Location = New System.Drawing.Point(15, 24)
        Me.lblTipoInstalacion.Name = "lblTipoInstalacion"
        Me.lblTipoInstalacion.Size = New System.Drawing.Size(92, 16)
        Me.lblTipoInstalacion.TabIndex = 0
        Me.lblTipoInstalacion.Text = "Clave Tipo :"
        '
        'btnGuardar
        '
        Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.Location = New System.Drawing.Point(87, 170)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(108, 35)
        Me.btnGuardar.TabIndex = 1
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(233, 170)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(108, 35)
        Me.btnCancelar.TabIndex = 2
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'frmTipoInstalaciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(425, 217)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.gbxRegistrosNuevos)
        Me.Name = "frmTipoInstalaciones"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Tipo Instalación"
        Me.gbxRegistrosNuevos.ResumeLayout(False)
        Me.gbxRegistrosNuevos.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents gbxRegistrosNuevos As System.Windows.Forms.GroupBox
    Friend WithEvents cbxActivoTipo As System.Windows.Forms.CheckBox
    Friend WithEvents txtDescripcionTipo As System.Windows.Forms.TextBox
    Friend WithEvents lblDescripcionTipo As System.Windows.Forms.Label
    Friend WithEvents txtClaveTipo As System.Windows.Forms.TextBox
    Friend WithEvents lblTipoInstalacion As System.Windows.Forms.Label
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
End Class
