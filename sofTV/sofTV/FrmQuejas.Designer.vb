<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmQuejas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.cbEnProceso = New System.Windows.Forms.CheckBox()
        Me.VisitaCheckBox = New System.Windows.Forms.CheckBox()
        Me.EjecutadasCheckBox = New System.Windows.Forms.CheckBox()
        Me.PendientesCheckBox = New System.Windows.Forms.CheckBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.lblProblemaSeleccionado = New System.Windows.Forms.Label()
        Me.lblProblema = New System.Windows.Forms.Label()
        Me.cbxClasifPorblema = New System.Windows.Forms.CheckBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.CheckBox7 = New System.Windows.Forms.CheckBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.CheckBox6 = New System.Windows.Forms.CheckBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.CLV_COLONIA = New System.Windows.Forms.Label()
        Me.CLV_TRABAJO = New System.Windows.Forms.Label()
        Me.NOMCOLONIA = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.FECEJEFIN = New System.Windows.Forms.Label()
        Me.FECEJEINI = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.FECSOLFIN = New System.Windows.Forms.Label()
        Me.FECSOLINI = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.NUMFINLBL = New System.Windows.Forms.Label()
        Me.NUMINILbl = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.CheckBox5 = New System.Windows.Forms.CheckBox()
        Me.CheckBox4 = New System.Windows.Forms.CheckBox()
        Me.CheckBox3 = New System.Windows.Forms.CheckBox()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.NOMTRABAJO = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.MuestraTipSerPrincipalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.NewsoftvDataSet1 = New sofTV.NewsoftvDataSet1()
        Me.RadioButton3 = New System.Windows.Forms.RadioButton()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.RadioButton4 = New System.Windows.Forms.RadioButton()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.MuestraTipSerPrincipalTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MuestraTipSerPrincipalTableAdapter()
        Me.CatalogodeReportesAreaTecnicaTableAdapter = New sofTV.NewsoftvDataSet1TableAdapters.CatalogodeReportesAreaTecnicaTableAdapter()
        Me.CatalogodeReportesAreaTecnicaBindingSource2 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button3 = New System.Windows.Forms.Button()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.MuestraTipSerPrincipal2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLidia2 = New sofTV.DataSetLidia2()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.MuestraTipSerPrincipal2TableAdapter = New sofTV.DataSetLidia2TableAdapters.MuestraTipSerPrincipal2TableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.btnImpFormato = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Panel2.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.MuestraTipSerPrincipalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.CatalogodeReportesAreaTecnicaBindingSource2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTipSerPrincipal2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Olive
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(846, 673)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(160, 63)
        Me.Button2.TabIndex = 14
        Me.Button2.Text = "Imprimir &Quejas "
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Olive
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(311, 673)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(175, 63)
        Me.Button1.TabIndex = 13
        Me.Button1.Text = "Imprimir &Listado"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.DarkKhaki
        Me.Panel2.Controls.Add(Me.cbEnProceso)
        Me.Panel2.Controls.Add(Me.VisitaCheckBox)
        Me.Panel2.Controls.Add(Me.EjecutadasCheckBox)
        Me.Panel2.Controls.Add(Me.PendientesCheckBox)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Location = New System.Drawing.Point(9, 60)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(274, 84)
        Me.Panel2.TabIndex = 1
        Me.Panel2.TabStop = True
        '
        'cbEnProceso
        '
        Me.cbEnProceso.AutoSize = True
        Me.cbEnProceso.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cbEnProceso.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbEnProceso.Location = New System.Drawing.Point(148, 61)
        Me.cbEnProceso.Name = "cbEnProceso"
        Me.cbEnProceso.Size = New System.Drawing.Size(105, 20)
        Me.cbEnProceso.TabIndex = 4
        Me.cbEnProceso.Text = "&En Proceso"
        Me.cbEnProceso.UseVisualStyleBackColor = True
        '
        'VisitaCheckBox
        '
        Me.VisitaCheckBox.AutoSize = True
        Me.VisitaCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.VisitaCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VisitaCheckBox.Location = New System.Drawing.Point(22, 61)
        Me.VisitaCheckBox.Name = "VisitaCheckBox"
        Me.VisitaCheckBox.Size = New System.Drawing.Size(95, 20)
        Me.VisitaCheckBox.TabIndex = 3
        Me.VisitaCheckBox.Text = "&Con Visita"
        Me.VisitaCheckBox.UseVisualStyleBackColor = True
        '
        'EjecutadasCheckBox
        '
        Me.EjecutadasCheckBox.AutoSize = True
        Me.EjecutadasCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.EjecutadasCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EjecutadasCheckBox.Location = New System.Drawing.Point(148, 37)
        Me.EjecutadasCheckBox.Name = "EjecutadasCheckBox"
        Me.EjecutadasCheckBox.Size = New System.Drawing.Size(103, 20)
        Me.EjecutadasCheckBox.TabIndex = 2
        Me.EjecutadasCheckBox.Text = "&Ejecutadas"
        Me.EjecutadasCheckBox.UseVisualStyleBackColor = True
        '
        'PendientesCheckBox
        '
        Me.PendientesCheckBox.AutoSize = True
        Me.PendientesCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.PendientesCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PendientesCheckBox.Location = New System.Drawing.Point(22, 37)
        Me.PendientesCheckBox.Name = "PendientesCheckBox"
        Me.PendientesCheckBox.Size = New System.Drawing.Size(103, 20)
        Me.PendientesCheckBox.TabIndex = 1
        Me.PendientesCheckBox.Text = "&Pendientes"
        Me.PendientesCheckBox.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.DarkGoldenrod
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(3, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(271, 26)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Seleccione el Status  :"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.DarkKhaki
        Me.Panel4.Controls.Add(Me.lblProblemaSeleccionado)
        Me.Panel4.Controls.Add(Me.lblProblema)
        Me.Panel4.Controls.Add(Me.cbxClasifPorblema)
        Me.Panel4.Controls.Add(Me.TextBox1)
        Me.Panel4.Controls.Add(Me.Label7)
        Me.Panel4.Controls.Add(Me.CheckBox7)
        Me.Panel4.Controls.Add(Me.Label6)
        Me.Panel4.Controls.Add(Me.CheckBox6)
        Me.Panel4.Controls.Add(Me.Label8)
        Me.Panel4.Controls.Add(Me.CLV_COLONIA)
        Me.Panel4.Controls.Add(Me.CLV_TRABAJO)
        Me.Panel4.Controls.Add(Me.NOMCOLONIA)
        Me.Panel4.Controls.Add(Me.Label20)
        Me.Panel4.Controls.Add(Me.Label18)
        Me.Panel4.Controls.Add(Me.FECEJEFIN)
        Me.Panel4.Controls.Add(Me.FECEJEINI)
        Me.Panel4.Controls.Add(Me.Label15)
        Me.Panel4.Controls.Add(Me.Label16)
        Me.Panel4.Controls.Add(Me.FECSOLFIN)
        Me.Panel4.Controls.Add(Me.FECSOLINI)
        Me.Panel4.Controls.Add(Me.Label11)
        Me.Panel4.Controls.Add(Me.Label12)
        Me.Panel4.Controls.Add(Me.NUMFINLBL)
        Me.Panel4.Controls.Add(Me.NUMINILbl)
        Me.Panel4.Controls.Add(Me.Label5)
        Me.Panel4.Controls.Add(Me.Label4)
        Me.Panel4.Controls.Add(Me.CheckBox5)
        Me.Panel4.Controls.Add(Me.CheckBox4)
        Me.Panel4.Controls.Add(Me.CheckBox3)
        Me.Panel4.Controls.Add(Me.CheckBox2)
        Me.Panel4.Controls.Add(Me.Label3)
        Me.Panel4.Controls.Add(Me.CheckBox1)
        Me.Panel4.Controls.Add(Me.NOMTRABAJO)
        Me.Panel4.Location = New System.Drawing.Point(9, 150)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(274, 439)
        Me.Panel4.TabIndex = 4
        Me.Panel4.TabStop = True
        '
        'lblProblemaSeleccionado
        '
        Me.lblProblemaSeleccionado.Location = New System.Drawing.Point(69, 404)
        Me.lblProblemaSeleccionado.Name = "lblProblemaSeleccionado"
        Me.lblProblemaSeleccionado.Size = New System.Drawing.Size(195, 31)
        Me.lblProblemaSeleccionado.TabIndex = 201
        '
        'lblProblema
        '
        Me.lblProblema.AutoSize = True
        Me.lblProblema.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProblema.ForeColor = System.Drawing.Color.Gainsboro
        Me.lblProblema.Location = New System.Drawing.Point(9, 408)
        Me.lblProblema.Name = "lblProblema"
        Me.lblProblema.Size = New System.Drawing.Size(63, 13)
        Me.lblProblema.TabIndex = 200
        Me.lblProblema.Text = "Problema:"
        '
        'cbxClasifPorblema
        '
        Me.cbxClasifPorblema.AutoSize = True
        Me.cbxClasifPorblema.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cbxClasifPorblema.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxClasifPorblema.Location = New System.Drawing.Point(72, 382)
        Me.cbxClasifPorblema.Name = "cbxClasifPorblema"
        Me.cbxClasifPorblema.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.cbxClasifPorblema.Size = New System.Drawing.Size(171, 19)
        Me.cbxClasifPorblema.TabIndex = 199
        Me.cbxClasifPorblema.Text = "Clasificación Problema"
        Me.cbxClasifPorblema.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Enabled = False
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(4, 51)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(123, 21)
        Me.TextBox1.TabIndex = 198
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Gainsboro
        Me.Label7.Location = New System.Drawing.Point(6, 33)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(63, 13)
        Me.Label7.TabIndex = 197
        Me.Label7.Text = "Contrato :"
        '
        'CheckBox7
        '
        Me.CheckBox7.AutoSize = True
        Me.CheckBox7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox7.Location = New System.Drawing.Point(166, 31)
        Me.CheckBox7.Name = "CheckBox7"
        Me.CheckBox7.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CheckBox7.Size = New System.Drawing.Size(77, 19)
        Me.CheckBox7.TabIndex = 196
        Me.CheckBox7.Text = "Contrato"
        Me.CheckBox7.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Gainsboro
        Me.Label6.Location = New System.Drawing.Point(9, 361)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(90, 13)
        Me.Label6.TabIndex = 194
        Me.Label6.Text = "Departamento:"
        '
        'CheckBox6
        '
        Me.CheckBox6.AutoSize = True
        Me.CheckBox6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox6.Location = New System.Drawing.Point(129, 335)
        Me.CheckBox6.Name = "CheckBox6"
        Me.CheckBox6.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CheckBox6.Size = New System.Drawing.Size(114, 19)
        Me.CheckBox6.TabIndex = 9
        Me.CheckBox6.Text = "Departamento"
        Me.CheckBox6.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(97, 361)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(167, 13)
        Me.Label8.TabIndex = 195
        '
        'CLV_COLONIA
        '
        Me.CLV_COLONIA.ForeColor = System.Drawing.Color.DarkKhaki
        Me.CLV_COLONIA.Location = New System.Drawing.Point(6, 319)
        Me.CLV_COLONIA.Name = "CLV_COLONIA"
        Me.CLV_COLONIA.Size = New System.Drawing.Size(65, 13)
        Me.CLV_COLONIA.TabIndex = 192
        '
        'CLV_TRABAJO
        '
        Me.CLV_TRABAJO.ForeColor = System.Drawing.Color.DarkKhaki
        Me.CLV_TRABAJO.Location = New System.Drawing.Point(17, 317)
        Me.CLV_TRABAJO.Name = "CLV_TRABAJO"
        Me.CLV_TRABAJO.Size = New System.Drawing.Size(65, 13)
        Me.CLV_TRABAJO.TabIndex = 191
        '
        'NOMCOLONIA
        '
        Me.NOMCOLONIA.Location = New System.Drawing.Point(80, 299)
        Me.NOMCOLONIA.Name = "NOMCOLONIA"
        Me.NOMCOLONIA.Size = New System.Drawing.Size(189, 33)
        Me.NOMCOLONIA.TabIndex = 190
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.Gainsboro
        Me.Label20.Location = New System.Drawing.Point(9, 306)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(61, 13)
        Me.Label20.TabIndex = 189
        Me.Label20.Text = "Colonia  :"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.Gainsboro
        Me.Label18.Location = New System.Drawing.Point(9, 253)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(65, 13)
        Me.Label18.TabIndex = 187
        Me.Label18.Text = "Servicio  :"
        '
        'FECEJEFIN
        '
        Me.FECEJEFIN.Location = New System.Drawing.Point(196, 198)
        Me.FECEJEFIN.Name = "FECEJEFIN"
        Me.FECEJEFIN.Size = New System.Drawing.Size(65, 13)
        Me.FECEJEFIN.TabIndex = 186
        '
        'FECEJEINI
        '
        Me.FECEJEINI.Location = New System.Drawing.Point(67, 198)
        Me.FECEJEINI.Name = "FECEJEINI"
        Me.FECEJEINI.Size = New System.Drawing.Size(65, 13)
        Me.FECEJEINI.TabIndex = 185
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Gainsboro
        Me.Label15.Location = New System.Drawing.Point(138, 198)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(52, 13)
        Me.Label15.TabIndex = 184
        Me.Label15.Text = "Hasta  :"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.Gainsboro
        Me.Label16.Location = New System.Drawing.Point(9, 198)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(55, 13)
        Me.Label16.TabIndex = 183
        Me.Label16.Text = "Desde  :"
        '
        'FECSOLFIN
        '
        Me.FECSOLFIN.Location = New System.Drawing.Point(196, 151)
        Me.FECSOLFIN.Name = "FECSOLFIN"
        Me.FECSOLFIN.Size = New System.Drawing.Size(65, 13)
        Me.FECSOLFIN.TabIndex = 182
        '
        'FECSOLINI
        '
        Me.FECSOLINI.Location = New System.Drawing.Point(67, 151)
        Me.FECSOLINI.Name = "FECSOLINI"
        Me.FECSOLINI.Size = New System.Drawing.Size(65, 13)
        Me.FECSOLINI.TabIndex = 181
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Gainsboro
        Me.Label11.Location = New System.Drawing.Point(138, 151)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(52, 13)
        Me.Label11.TabIndex = 180
        Me.Label11.Text = "Hasta  :"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Gainsboro
        Me.Label12.Location = New System.Drawing.Point(9, 151)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(55, 13)
        Me.Label12.TabIndex = 179
        Me.Label12.Text = "Desde  :"
        '
        'NUMFINLBL
        '
        Me.NUMFINLBL.Location = New System.Drawing.Point(196, 100)
        Me.NUMFINLBL.Name = "NUMFINLBL"
        Me.NUMFINLBL.Size = New System.Drawing.Size(65, 21)
        Me.NUMFINLBL.TabIndex = 178
        '
        'NUMINILbl
        '
        Me.NUMINILbl.Location = New System.Drawing.Point(67, 100)
        Me.NUMINILbl.Name = "NUMINILbl"
        Me.NUMINILbl.Size = New System.Drawing.Size(65, 21)
        Me.NUMINILbl.TabIndex = 177
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Gainsboro
        Me.Label5.Location = New System.Drawing.Point(138, 102)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(52, 13)
        Me.Label5.TabIndex = 176
        Me.Label5.Text = "Hasta  :"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Gainsboro
        Me.Label4.Location = New System.Drawing.Point(9, 102)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(55, 13)
        Me.Label4.TabIndex = 174
        Me.Label4.Text = "Desde  :"
        '
        'CheckBox5
        '
        Me.CheckBox5.AutoSize = True
        Me.CheckBox5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox5.Location = New System.Drawing.Point(171, 274)
        Me.CheckBox5.Name = "CheckBox5"
        Me.CheckBox5.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CheckBox5.Size = New System.Drawing.Size(72, 19)
        Me.CheckBox5.TabIndex = 8
        Me.CheckBox5.Text = "Colonia"
        Me.CheckBox5.UseVisualStyleBackColor = True
        '
        'CheckBox4
        '
        Me.CheckBox4.AutoSize = True
        Me.CheckBox4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox4.Location = New System.Drawing.Point(97, 218)
        Me.CheckBox4.Name = "CheckBox4"
        Me.CheckBox4.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CheckBox4.Size = New System.Drawing.Size(146, 19)
        Me.CheckBox4.TabIndex = 7
        Me.CheckBox4.Text = "Servicios al Cliente"
        Me.CheckBox4.UseVisualStyleBackColor = True
        '
        'CheckBox3
        '
        Me.CheckBox3.AutoSize = True
        Me.CheckBox3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox3.Location = New System.Drawing.Point(114, 172)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CheckBox3.Size = New System.Drawing.Size(129, 19)
        Me.CheckBox3.TabIndex = 6
        Me.CheckBox3.Text = "Fecha Ejecucion"
        Me.CheckBox3.UseVisualStyleBackColor = True
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox2.Location = New System.Drawing.Point(121, 126)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CheckBox2.Size = New System.Drawing.Size(122, 19)
        Me.CheckBox2.TabIndex = 5
        Me.CheckBox2.Text = "Fecha Solicitud"
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.DarkGoldenrod
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(0, 2)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(274, 26)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Busqueda Por  :"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.Location = New System.Drawing.Point(107, 78)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CheckBox1.Size = New System.Drawing.Size(136, 19)
        Me.CheckBox1.TabIndex = 4
        Me.CheckBox1.Text = "Numero de Queja"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'NOMTRABAJO
        '
        Me.NOMTRABAJO.Location = New System.Drawing.Point(75, 240)
        Me.NOMTRABAJO.Name = "NOMTRABAJO"
        Me.NOMTRABAJO.Size = New System.Drawing.Size(189, 35)
        Me.NOMTRABAJO.TabIndex = 188
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.CrystalReportViewer1)
        Me.Panel1.Location = New System.Drawing.Point(289, 9)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(712, 650)
        Me.Panel1.TabIndex = 32
        '
        'CrystalReportViewer1
        '
        Me.CrystalReportViewer1.ActiveViewIndex = -1
        Me.CrystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CrystalReportViewer1.DisplayGroupTree = False
        Me.CrystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrystalReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
        Me.CrystalReportViewer1.SelectionFormula = ""
        Me.CrystalReportViewer1.Size = New System.Drawing.Size(712, 650)
        Me.CrystalReportViewer1.TabIndex = 0
        Me.CrystalReportViewer1.TabStop = False
        Me.CrystalReportViewer1.ViewTimeSelectionFormula = ""
        '
        'MuestraTipSerPrincipalBindingSource
        '
        Me.MuestraTipSerPrincipalBindingSource.DataMember = "MuestraTipSerPrincipal"
        Me.MuestraTipSerPrincipalBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton2.Location = New System.Drawing.Point(22, 53)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(120, 19)
        Me.RadioButton2.TabIndex = 11
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "Colonia y Calle"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.DarkGoldenrod
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(271, 26)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Ordenar Reporte por :"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Checked = True
        Me.RadioButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton1.Location = New System.Drawing.Point(22, 29)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(78, 19)
        Me.RadioButton1.TabIndex = 10
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Contrato"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'NewsoftvDataSet1
        '
        Me.NewsoftvDataSet1.DataSetName = "NewsoftvDataSet1"
        Me.NewsoftvDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'RadioButton3
        '
        Me.RadioButton3.AutoSize = True
        Me.RadioButton3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton3.Location = New System.Drawing.Point(33, 105)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(125, 20)
        Me.RadioButton3.TabIndex = 3
        Me.RadioButton3.Text = "Clave Tecnica"
        Me.RadioButton3.UseVisualStyleBackColor = True
        Me.RadioButton3.Visible = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.Olive
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.White
        Me.Button5.Location = New System.Drawing.Point(61, 703)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(165, 33)
        Me.Button5.TabIndex = 15
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'RadioButton4
        '
        Me.RadioButton4.AutoSize = True
        Me.RadioButton4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton4.Location = New System.Drawing.Point(22, 78)
        Me.RadioButton4.Name = "RadioButton4"
        Me.RadioButton4.Size = New System.Drawing.Size(123, 19)
        Me.RadioButton4.TabIndex = 12
        Me.RadioButton4.TabStop = True
        Me.RadioButton4.Text = "Fecha Solicitud"
        Me.RadioButton4.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.DarkKhaki
        Me.Panel3.Controls.Add(Me.RadioButton4)
        Me.Panel3.Controls.Add(Me.RadioButton3)
        Me.Panel3.Controls.Add(Me.RadioButton2)
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Controls.Add(Me.RadioButton1)
        Me.Panel3.Location = New System.Drawing.Point(9, 595)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(274, 101)
        Me.Panel3.TabIndex = 10
        Me.Panel3.TabStop = True
        '
        'MuestraTipSerPrincipalTableAdapter
        '
        Me.MuestraTipSerPrincipalTableAdapter.ClearBeforeFill = True
        '
        'CatalogodeReportesAreaTecnicaTableAdapter
        '
        Me.CatalogodeReportesAreaTecnicaTableAdapter.ClearBeforeFill = True
        '
        'CatalogodeReportesAreaTecnicaBindingSource2
        '
        Me.CatalogodeReportesAreaTecnicaBindingSource2.DataMember = "CatalogodeReportesAreaTecnica"
        Me.CatalogodeReportesAreaTecnicaBindingSource2.DataSource = Me.NewsoftvDataSet1
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.Olive
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.Location = New System.Drawing.Point(492, 673)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(182, 63)
        Me.Button3.TabIndex = 35
        Me.Button3.Text = "Imprimir Listado &Detallado "
        Me.Button3.UseVisualStyleBackColor = False
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.MuestraTipSerPrincipal2BindingSource
        Me.ComboBox1.DisplayMember = "Concepto"
        Me.ComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.ForeColor = System.Drawing.Color.Red
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(6, 28)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(277, 24)
        Me.ComboBox1.TabIndex = 37
        Me.ComboBox1.ValueMember = "Clv_TipSerPrincipal"
        '
        'MuestraTipSerPrincipal2BindingSource
        '
        Me.MuestraTipSerPrincipal2BindingSource.DataMember = "MuestraTipSerPrincipal2"
        Me.MuestraTipSerPrincipal2BindingSource.DataSource = Me.DataSetLidia2
        '
        'DataSetLidia2
        '
        Me.DataSetLidia2.DataSetName = "DataSetLidia2"
        Me.DataSetLidia2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label9.Location = New System.Drawing.Point(9, 9)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(127, 16)
        Me.Label9.TabIndex = 36
        Me.Label9.Text = "Tipo de Servicio:"
        '
        'MuestraTipSerPrincipal2TableAdapter
        '
        Me.MuestraTipSerPrincipal2TableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'btnImpFormato
        '
        Me.btnImpFormato.BackColor = System.Drawing.Color.Olive
        Me.btnImpFormato.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnImpFormato.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImpFormato.ForeColor = System.Drawing.Color.White
        Me.btnImpFormato.Location = New System.Drawing.Point(680, 673)
        Me.btnImpFormato.Name = "btnImpFormato"
        Me.btnImpFormato.Size = New System.Drawing.Size(160, 63)
        Me.btnImpFormato.TabIndex = 38
        Me.btnImpFormato.Text = "Imprimir &Formato"
        Me.btnImpFormato.UseVisualStyleBackColor = False
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'FrmQuejas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1025, 748)
        Me.Controls.Add(Me.btnImpFormato)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Panel3)
        Me.Name = "FrmQuejas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Area Técnica Quejas"
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.MuestraTipSerPrincipalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.CatalogodeReportesAreaTecnicaBindingSource2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTipSerPrincipal2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents VisitaCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents EjecutadasCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents PendientesCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents CLV_COLONIA As System.Windows.Forms.Label
    Friend WithEvents CLV_TRABAJO As System.Windows.Forms.Label
    Friend WithEvents NOMCOLONIA As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents FECEJEFIN As System.Windows.Forms.Label
    Friend WithEvents FECEJEINI As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents FECSOLFIN As System.Windows.Forms.Label
    Friend WithEvents FECSOLINI As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents NUMFINLBL As System.Windows.Forms.Label
    Friend WithEvents NUMINILbl As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents CheckBox5 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox4 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents NOMTRABAJO As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents MuestraTipSerPrincipalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents NewsoftvDataSet1 As sofTV.NewsoftvDataSet1
    Friend WithEvents RadioButton3 As System.Windows.Forms.RadioButton
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents RadioButton4 As System.Windows.Forms.RadioButton
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents MuestraTipSerPrincipalTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MuestraTipSerPrincipalTableAdapter
    Friend WithEvents CatalogodeReportesAreaTecnicaTableAdapter As sofTV.NewsoftvDataSet1TableAdapters.CatalogodeReportesAreaTecnicaTableAdapter
    Friend WithEvents CatalogodeReportesAreaTecnicaBindingSource2 As System.Windows.Forms.BindingSource
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents CheckBox6 As System.Windows.Forms.CheckBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents CheckBox7 As System.Windows.Forms.CheckBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents DataSetLidia2 As sofTV.DataSetLidia2
    Friend WithEvents MuestraTipSerPrincipal2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipSerPrincipal2TableAdapter As sofTV.DataSetLidia2TableAdapters.MuestraTipSerPrincipal2TableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Private WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents cbEnProceso As System.Windows.Forms.CheckBox
    Friend WithEvents btnImpFormato As System.Windows.Forms.Button
    Friend WithEvents lblProblema As System.Windows.Forms.Label
    Friend WithEvents cbxClasifPorblema As System.Windows.Forms.CheckBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Public WithEvents lblProblemaSeleccionado As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
