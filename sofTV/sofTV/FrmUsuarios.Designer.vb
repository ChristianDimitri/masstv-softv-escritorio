﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmUsuarios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_UsuarioLabel As System.Windows.Forms.Label
        Dim NombreLabel As System.Windows.Forms.Label
        Dim DomicilioLabel As System.Windows.Forms.Label
        Dim ColoniaLabel As System.Windows.Forms.Label
        Dim FechaIngresoLabel As System.Windows.Forms.Label
        Dim FechaSalidaLabel As System.Windows.Forms.Label
        Dim ActivoLabel As System.Windows.Forms.Label
        Dim PasaporteLabel As System.Windows.Forms.Label
        Dim Clv_TipoUsuarioLabel As System.Windows.Forms.Label
        Dim CATVLabel As System.Windows.Forms.Label
        Dim FacturacionLabel As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmUsuarios))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.dtpFechaSalida = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaIngreso = New System.Windows.Forms.DateTimePicker()
        Me.GrupoComboBox = New System.Windows.Forms.ComboBox()
        Me.ConGrupoVentasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric2 = New sofTV.DataSetEric2()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.EmailTextBox = New System.Windows.Forms.TextBox()
        Me.ConRelUsuarioEmailBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.MUESTRATIPOUSUARIOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.CMBTextBox4 = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.CATVCheckBox = New System.Windows.Forms.CheckBox()
        Me.FacturacionCheckBox = New System.Windows.Forms.CheckBox()
        Me.CONUSUARIOSBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.CONUSUARIOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.CONUSUARIOSBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.ClaveLabel1 = New System.Windows.Forms.Label()
        Me.Clv_UsuarioTextBox = New System.Windows.Forms.TextBox()
        Me.NombreTextBox = New System.Windows.Forms.TextBox()
        Me.DomicilioTextBox = New System.Windows.Forms.TextBox()
        Me.ColoniaTextBox = New System.Windows.Forms.TextBox()
        Me.FechaIngresoMaskedTextBox = New System.Windows.Forms.MaskedTextBox()
        Me.FechaSalidaMaskedTextBox = New System.Windows.Forms.MaskedTextBox()
        Me.ActivoCheckBox = New System.Windows.Forms.CheckBox()
        Me.PasaporteTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_TipoUsuarioTextBox = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.DAMEFECHADELSERVIDORBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONUSUARIOSTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONUSUARIOSTableAdapter()
        Me.DAMEFECHADELSERVIDORTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.DAMEFECHADELSERVIDORTableAdapter()
        Me.MUESTRATIPOUSUARIOSTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRATIPOUSUARIOSTableAdapter()
        Me.ConRelUsuarioEmailTableAdapter = New sofTV.DataSetEricTableAdapters.ConRelUsuarioEmailTableAdapter()
        Me.ConGrupoVentasTableAdapter = New sofTV.DataSetEric2TableAdapters.ConGrupoVentasTableAdapter()
        Me.NueRelUsuarioGrupoVentasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NueRelUsuarioGrupoVentasTableAdapter = New sofTV.DataSetEric2TableAdapters.NueRelUsuarioGrupoVentasTableAdapter()
        Me.ConRelUsuarioGrupoVentasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConRelUsuarioGrupoVentasTableAdapter = New sofTV.DataSetEric2TableAdapters.ConRelUsuarioGrupoVentasTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Clv_UsuarioLabel = New System.Windows.Forms.Label()
        NombreLabel = New System.Windows.Forms.Label()
        DomicilioLabel = New System.Windows.Forms.Label()
        ColoniaLabel = New System.Windows.Forms.Label()
        FechaIngresoLabel = New System.Windows.Forms.Label()
        FechaSalidaLabel = New System.Windows.Forms.Label()
        ActivoLabel = New System.Windows.Forms.Label()
        PasaporteLabel = New System.Windows.Forms.Label()
        Clv_TipoUsuarioLabel = New System.Windows.Forms.Label()
        CATVLabel = New System.Windows.Forms.Label()
        FacturacionLabel = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.ConGrupoVentasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConRelUsuarioEmailBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRATIPOUSUARIOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.CONUSUARIOSBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONUSUARIOSBindingNavigator.SuspendLayout()
        CType(Me.CONUSUARIOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAMEFECHADELSERVIDORBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NueRelUsuarioGrupoVentasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConRelUsuarioGrupoVentasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Clv_UsuarioLabel
        '
        Clv_UsuarioLabel.AutoSize = True
        Clv_UsuarioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_UsuarioLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_UsuarioLabel.Location = New System.Drawing.Point(76, 68)
        Clv_UsuarioLabel.Name = "Clv_UsuarioLabel"
        Clv_UsuarioLabel.Size = New System.Drawing.Size(50, 15)
        Clv_UsuarioLabel.TabIndex = 2
        Clv_UsuarioLabel.Text = "Clave :"
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.ForeColor = System.Drawing.Color.LightSlateGray
        NombreLabel.Location = New System.Drawing.Point(60, 94)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(66, 15)
        NombreLabel.TabIndex = 4
        NombreLabel.Text = "Nombre :"
        '
        'DomicilioLabel
        '
        DomicilioLabel.AutoSize = True
        DomicilioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DomicilioLabel.ForeColor = System.Drawing.Color.LightSlateGray
        DomicilioLabel.Location = New System.Drawing.Point(50, 120)
        DomicilioLabel.Name = "DomicilioLabel"
        DomicilioLabel.Size = New System.Drawing.Size(76, 15)
        DomicilioLabel.TabIndex = 6
        DomicilioLabel.Text = "Domicilio :"
        '
        'ColoniaLabel
        '
        ColoniaLabel.AutoSize = True
        ColoniaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ColoniaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ColoniaLabel.Location = New System.Drawing.Point(62, 147)
        ColoniaLabel.Name = "ColoniaLabel"
        ColoniaLabel.Size = New System.Drawing.Size(64, 15)
        ColoniaLabel.TabIndex = 8
        ColoniaLabel.Text = "Colonia :"
        '
        'FechaIngresoLabel
        '
        FechaIngresoLabel.AutoSize = True
        FechaIngresoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FechaIngresoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        FechaIngresoLabel.Location = New System.Drawing.Point(762, 96)
        FechaIngresoLabel.Name = "FechaIngresoLabel"
        FechaIngresoLabel.Size = New System.Drawing.Size(63, 15)
        FechaIngresoLabel.TabIndex = 10
        FechaIngresoLabel.Text = "Ingreso :"
        '
        'FechaSalidaLabel
        '
        FechaSalidaLabel.AutoSize = True
        FechaSalidaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FechaSalidaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        FechaSalidaLabel.Location = New System.Drawing.Point(781, 123)
        FechaSalidaLabel.Name = "FechaSalidaLabel"
        FechaSalidaLabel.Size = New System.Drawing.Size(44, 15)
        FechaSalidaLabel.TabIndex = 12
        FechaSalidaLabel.Text = "Baja :"
        '
        'ActivoLabel
        '
        ActivoLabel.AutoSize = True
        ActivoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ActivoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ActivoLabel.Location = New System.Drawing.Point(74, 176)
        ActivoLabel.Name = "ActivoLabel"
        ActivoLabel.Size = New System.Drawing.Size(52, 15)
        ActivoLabel.TabIndex = 14
        ActivoLabel.Text = "Activo :"
        '
        'PasaporteLabel
        '
        PasaporteLabel.AutoSize = True
        PasaporteLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PasaporteLabel.ForeColor = System.Drawing.Color.LightSlateGray
        PasaporteLabel.Location = New System.Drawing.Point(46, 204)
        PasaporteLabel.Name = "PasaporteLabel"
        PasaporteLabel.Size = New System.Drawing.Size(80, 15)
        PasaporteLabel.TabIndex = 16
        PasaporteLabel.Text = "Pasaporte :"
        '
        'Clv_TipoUsuarioLabel
        '
        Clv_TipoUsuarioLabel.AutoSize = True
        Clv_TipoUsuarioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_TipoUsuarioLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_TipoUsuarioLabel.Location = New System.Drawing.Point(29, 237)
        Clv_TipoUsuarioLabel.Name = "Clv_TipoUsuarioLabel"
        Clv_TipoUsuarioLabel.Size = New System.Drawing.Size(97, 15)
        Clv_TipoUsuarioLabel.TabIndex = 18
        Clv_TipoUsuarioLabel.Text = "Tipo Usuario :"
        '
        'CATVLabel
        '
        CATVLabel.AutoSize = True
        CATVLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CATVLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CATVLabel.Location = New System.Drawing.Point(27, 38)
        CATVLabel.Name = "CATVLabel"
        CATVLabel.Size = New System.Drawing.Size(137, 15)
        CATVLabel.TabIndex = 20
        CATVLabel.Text = "Control de Clientes :"
        '
        'FacturacionLabel
        '
        FacturacionLabel.AutoSize = True
        FacturacionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FacturacionLabel.ForeColor = System.Drawing.Color.LightSlateGray
        FacturacionLabel.Location = New System.Drawing.Point(78, 68)
        FacturacionLabel.Name = "FacturacionLabel"
        FacturacionLabel.Size = New System.Drawing.Size(86, 15)
        FacturacionLabel.TabIndex = 22
        FacturacionLabel.Text = "Facturación:"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(25, 272)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(101, 15)
        Label2.TabIndex = 54
        Label2.Text = "Grupo Ventas :"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.dtpFechaSalida)
        Me.Panel1.Controls.Add(Me.dtpFechaIngreso)
        Me.Panel1.Controls.Add(Label2)
        Me.Panel1.Controls.Add(Me.GrupoComboBox)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.EmailTextBox)
        Me.Panel1.Controls.Add(Me.ComboBox1)
        Me.Panel1.Controls.Add(Me.CMBTextBox4)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.CONUSUARIOSBindingNavigator)
        Me.Panel1.Controls.Add(Me.ClaveLabel1)
        Me.Panel1.Controls.Add(Clv_UsuarioLabel)
        Me.Panel1.Controls.Add(Me.Clv_UsuarioTextBox)
        Me.Panel1.Controls.Add(NombreLabel)
        Me.Panel1.Controls.Add(Me.NombreTextBox)
        Me.Panel1.Controls.Add(DomicilioLabel)
        Me.Panel1.Controls.Add(Me.DomicilioTextBox)
        Me.Panel1.Controls.Add(ColoniaLabel)
        Me.Panel1.Controls.Add(Me.ColoniaTextBox)
        Me.Panel1.Controls.Add(FechaIngresoLabel)
        Me.Panel1.Controls.Add(Me.FechaIngresoMaskedTextBox)
        Me.Panel1.Controls.Add(FechaSalidaLabel)
        Me.Panel1.Controls.Add(Me.FechaSalidaMaskedTextBox)
        Me.Panel1.Controls.Add(ActivoLabel)
        Me.Panel1.Controls.Add(Me.ActivoCheckBox)
        Me.Panel1.Controls.Add(PasaporteLabel)
        Me.Panel1.Controls.Add(Me.PasaporteTextBox)
        Me.Panel1.Controls.Add(Clv_TipoUsuarioLabel)
        Me.Panel1.Controls.Add(Me.Clv_TipoUsuarioTextBox)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(992, 551)
        Me.Panel1.TabIndex = 0
        Me.Panel1.TabStop = True
        '
        'dtpFechaSalida
        '
        Me.dtpFechaSalida.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaSalida.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaSalida.Location = New System.Drawing.Point(831, 123)
        Me.dtpFechaSalida.Name = "dtpFechaSalida"
        Me.dtpFechaSalida.Size = New System.Drawing.Size(102, 20)
        Me.dtpFechaSalida.TabIndex = 56
        '
        'dtpFechaIngreso
        '
        Me.dtpFechaIngreso.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaIngreso.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaIngreso.Location = New System.Drawing.Point(831, 96)
        Me.dtpFechaIngreso.Name = "dtpFechaIngreso"
        Me.dtpFechaIngreso.Size = New System.Drawing.Size(102, 20)
        Me.dtpFechaIngreso.TabIndex = 55
        '
        'GrupoComboBox
        '
        Me.GrupoComboBox.DataSource = Me.ConGrupoVentasBindingSource
        Me.GrupoComboBox.DisplayMember = "Grupo"
        Me.GrupoComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GrupoComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GrupoComboBox.FormattingEnabled = True
        Me.GrupoComboBox.Location = New System.Drawing.Point(128, 269)
        Me.GrupoComboBox.Name = "GrupoComboBox"
        Me.GrupoComboBox.Size = New System.Drawing.Size(270, 23)
        Me.GrupoComboBox.TabIndex = 53
        Me.GrupoComboBox.ValueMember = "Clv_Grupo"
        '
        'ConGrupoVentasBindingSource
        '
        Me.ConGrupoVentasBindingSource.DataMember = "ConGrupoVentas"
        Me.ConGrupoVentasBindingSource.DataSource = Me.DataSetEric2
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric2"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label1.Location = New System.Drawing.Point(452, 216)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 15)
        Me.Label1.TabIndex = 52
        Me.Label1.Text = "Email :"
        Me.Label1.Visible = False
        '
        'EmailTextBox
        '
        Me.EmailTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EmailTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConRelUsuarioEmailBindingSource, "Email", True))
        Me.EmailTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EmailTextBox.Location = New System.Drawing.Point(455, 232)
        Me.EmailTextBox.MaxLength = 50
        Me.EmailTextBox.Name = "EmailTextBox"
        Me.EmailTextBox.Size = New System.Drawing.Size(329, 21)
        Me.EmailTextBox.TabIndex = 51
        Me.EmailTextBox.Visible = False
        '
        'ConRelUsuarioEmailBindingSource
        '
        Me.ConRelUsuarioEmailBindingSource.DataMember = "ConRelUsuarioEmail"
        Me.ConRelUsuarioEmailBindingSource.DataSource = Me.DataSetEric
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.MUESTRATIPOUSUARIOSBindingSource
        Me.ComboBox1.DisplayMember = "Descripcion"
        Me.ComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.ForeColor = System.Drawing.Color.Red
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(128, 231)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(270, 23)
        Me.ComboBox1.TabIndex = 6
        Me.ComboBox1.ValueMember = "clv_tipousuario"
        '
        'MUESTRATIPOUSUARIOSBindingSource
        '
        Me.MUESTRATIPOUSUARIOSBindingSource.DataMember = "MUESTRATIPOUSUARIOS"
        Me.MUESTRATIPOUSUARIOSBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CMBTextBox4
        '
        Me.CMBTextBox4.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox4.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox4.Location = New System.Drawing.Point(719, 68)
        Me.CMBTextBox4.Name = "CMBTextBox4"
        Me.CMBTextBox4.Size = New System.Drawing.Size(232, 19)
        Me.CMBTextBox4.TabIndex = 48
        Me.CMBTextBox4.TabStop = False
        Me.CMBTextBox4.Text = "Fechas de "
        Me.CMBTextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.CATVCheckBox)
        Me.GroupBox1.Controls.Add(Me.FacturacionCheckBox)
        Me.GroupBox1.Controls.Add(FacturacionLabel)
        Me.GroupBox1.Controls.Add(CATVLabel)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.DarkOrange
        Me.GroupBox1.Location = New System.Drawing.Point(131, 327)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(380, 110)
        Me.GroupBox1.TabIndex = 7
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Acceso a Modulos"
        '
        'CATVCheckBox
        '
        Me.CATVCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CATVCheckBox.Location = New System.Drawing.Point(170, 34)
        Me.CATVCheckBox.Name = "CATVCheckBox"
        Me.CATVCheckBox.Size = New System.Drawing.Size(104, 24)
        Me.CATVCheckBox.TabIndex = 7
        '
        'FacturacionCheckBox
        '
        Me.FacturacionCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FacturacionCheckBox.Location = New System.Drawing.Point(170, 64)
        Me.FacturacionCheckBox.Name = "FacturacionCheckBox"
        Me.FacturacionCheckBox.Size = New System.Drawing.Size(104, 24)
        Me.FacturacionCheckBox.TabIndex = 8
        '
        'CONUSUARIOSBindingNavigator
        '
        Me.CONUSUARIOSBindingNavigator.AddNewItem = Nothing
        Me.CONUSUARIOSBindingNavigator.BindingSource = Me.CONUSUARIOSBindingSource
        Me.CONUSUARIOSBindingNavigator.CountItem = Nothing
        Me.CONUSUARIOSBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONUSUARIOSBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.BindingNavigatorDeleteItem, Me.CONUSUARIOSBindingNavigatorSaveItem})
        Me.CONUSUARIOSBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONUSUARIOSBindingNavigator.MoveFirstItem = Nothing
        Me.CONUSUARIOSBindingNavigator.MoveLastItem = Nothing
        Me.CONUSUARIOSBindingNavigator.MoveNextItem = Nothing
        Me.CONUSUARIOSBindingNavigator.MovePreviousItem = Nothing
        Me.CONUSUARIOSBindingNavigator.Name = "CONUSUARIOSBindingNavigator"
        Me.CONUSUARIOSBindingNavigator.PositionItem = Nothing
        Me.CONUSUARIOSBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONUSUARIOSBindingNavigator.Size = New System.Drawing.Size(992, 25)
        Me.CONUSUARIOSBindingNavigator.TabIndex = 9
        Me.CONUSUARIOSBindingNavigator.TabStop = True
        Me.CONUSUARIOSBindingNavigator.Text = "BindingNavigator1"
        '
        'CONUSUARIOSBindingSource
        '
        Me.CONUSUARIOSBindingSource.DataMember = "CONUSUARIOS"
        Me.CONUSUARIOSBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(90, 22)
        Me.BindingNavigatorDeleteItem.Text = "ELIMINAR"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(79, 22)
        Me.ToolStripButton1.Text = "&CANCELAR"
        '
        'CONUSUARIOSBindingNavigatorSaveItem
        '
        Me.CONUSUARIOSBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONUSUARIOSBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONUSUARIOSBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONUSUARIOSBindingNavigatorSaveItem.Name = "CONUSUARIOSBindingNavigatorSaveItem"
        Me.CONUSUARIOSBindingNavigatorSaveItem.Size = New System.Drawing.Size(91, 22)
        Me.CONUSUARIOSBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'ClaveLabel1
        '
        Me.ClaveLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClaveLabel1.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.ClaveLabel1.Location = New System.Drawing.Point(128, 40)
        Me.ClaveLabel1.Name = "ClaveLabel1"
        Me.ClaveLabel1.Size = New System.Drawing.Size(104, 23)
        Me.ClaveLabel1.TabIndex = 1
        '
        'Clv_UsuarioTextBox
        '
        Me.Clv_UsuarioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_UsuarioTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Clv_UsuarioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_UsuarioTextBox.Location = New System.Drawing.Point(128, 66)
        Me.Clv_UsuarioTextBox.MaxLength = 5
        Me.Clv_UsuarioTextBox.Name = "Clv_UsuarioTextBox"
        Me.Clv_UsuarioTextBox.Size = New System.Drawing.Size(104, 21)
        Me.Clv_UsuarioTextBox.TabIndex = 0
        '
        'NombreTextBox
        '
        Me.NombreTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NombreTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreTextBox.Location = New System.Drawing.Point(128, 92)
        Me.NombreTextBox.Name = "NombreTextBox"
        Me.NombreTextBox.Size = New System.Drawing.Size(332, 21)
        Me.NombreTextBox.TabIndex = 1
        '
        'DomicilioTextBox
        '
        Me.DomicilioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DomicilioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DomicilioTextBox.Location = New System.Drawing.Point(128, 118)
        Me.DomicilioTextBox.Name = "DomicilioTextBox"
        Me.DomicilioTextBox.Size = New System.Drawing.Size(389, 21)
        Me.DomicilioTextBox.TabIndex = 2
        '
        'ColoniaTextBox
        '
        Me.ColoniaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ColoniaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ColoniaTextBox.Location = New System.Drawing.Point(128, 145)
        Me.ColoniaTextBox.Name = "ColoniaTextBox"
        Me.ColoniaTextBox.Size = New System.Drawing.Size(332, 21)
        Me.ColoniaTextBox.TabIndex = 3
        '
        'FechaIngresoMaskedTextBox
        '
        Me.FechaIngresoMaskedTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FechaIngresoMaskedTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FechaIngresoMaskedTextBox.Location = New System.Drawing.Point(831, 94)
        Me.FechaIngresoMaskedTextBox.Mask = "00/00/0000"
        Me.FechaIngresoMaskedTextBox.Name = "FechaIngresoMaskedTextBox"
        Me.FechaIngresoMaskedTextBox.Size = New System.Drawing.Size(86, 21)
        Me.FechaIngresoMaskedTextBox.TabIndex = 11
        Me.FechaIngresoMaskedTextBox.TabStop = False
        Me.FechaIngresoMaskedTextBox.ValidatingType = GetType(Date)
        Me.FechaIngresoMaskedTextBox.Visible = False
        '
        'FechaSalidaMaskedTextBox
        '
        Me.FechaSalidaMaskedTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FechaSalidaMaskedTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FechaSalidaMaskedTextBox.Location = New System.Drawing.Point(831, 121)
        Me.FechaSalidaMaskedTextBox.Mask = "00/00/0000"
        Me.FechaSalidaMaskedTextBox.Name = "FechaSalidaMaskedTextBox"
        Me.FechaSalidaMaskedTextBox.Size = New System.Drawing.Size(86, 21)
        Me.FechaSalidaMaskedTextBox.TabIndex = 13
        Me.FechaSalidaMaskedTextBox.TabStop = False
        Me.FechaSalidaMaskedTextBox.ValidatingType = GetType(Date)
        Me.FechaSalidaMaskedTextBox.Visible = False
        '
        'ActivoCheckBox
        '
        Me.ActivoCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ActivoCheckBox.Location = New System.Drawing.Point(128, 172)
        Me.ActivoCheckBox.Name = "ActivoCheckBox"
        Me.ActivoCheckBox.Size = New System.Drawing.Size(104, 24)
        Me.ActivoCheckBox.TabIndex = 4
        '
        'PasaporteTextBox
        '
        Me.PasaporteTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PasaporteTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PasaporteTextBox.Location = New System.Drawing.Point(128, 202)
        Me.PasaporteTextBox.Name = "PasaporteTextBox"
        Me.PasaporteTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.PasaporteTextBox.Size = New System.Drawing.Size(104, 21)
        Me.PasaporteTextBox.TabIndex = 5
        '
        'Clv_TipoUsuarioTextBox
        '
        Me.Clv_TipoUsuarioTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TipoUsuarioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_TipoUsuarioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONUSUARIOSBindingSource, "Clv_TipoUsuario", True))
        Me.Clv_TipoUsuarioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_TipoUsuarioTextBox.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TipoUsuarioTextBox.Location = New System.Drawing.Point(432, 235)
        Me.Clv_TipoUsuarioTextBox.Name = "Clv_TipoUsuarioTextBox"
        Me.Clv_TipoUsuarioTextBox.Size = New System.Drawing.Size(17, 14)
        Me.Clv_TipoUsuarioTextBox.TabIndex = 19
        Me.Clv_TipoUsuarioTextBox.TabStop = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(868, 477)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 10
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'DAMEFECHADELSERVIDORBindingSource
        '
        Me.DAMEFECHADELSERVIDORBindingSource.DataMember = "DAMEFECHADELSERVIDOR"
        Me.DAMEFECHADELSERVIDORBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CONUSUARIOSTableAdapter
        '
        Me.CONUSUARIOSTableAdapter.ClearBeforeFill = True
        '
        'DAMEFECHADELSERVIDORTableAdapter
        '
        Me.DAMEFECHADELSERVIDORTableAdapter.ClearBeforeFill = True
        '
        'MUESTRATIPOUSUARIOSTableAdapter
        '
        Me.MUESTRATIPOUSUARIOSTableAdapter.ClearBeforeFill = True
        '
        'ConRelUsuarioEmailTableAdapter
        '
        Me.ConRelUsuarioEmailTableAdapter.ClearBeforeFill = True
        '
        'ConGrupoVentasTableAdapter
        '
        Me.ConGrupoVentasTableAdapter.ClearBeforeFill = True
        '
        'NueRelUsuarioGrupoVentasBindingSource
        '
        Me.NueRelUsuarioGrupoVentasBindingSource.DataMember = "NueRelUsuarioGrupoVentas"
        Me.NueRelUsuarioGrupoVentasBindingSource.DataSource = Me.DataSetEric2
        '
        'NueRelUsuarioGrupoVentasTableAdapter
        '
        Me.NueRelUsuarioGrupoVentasTableAdapter.ClearBeforeFill = True
        '
        'ConRelUsuarioGrupoVentasBindingSource
        '
        Me.ConRelUsuarioGrupoVentasBindingSource.DataMember = "ConRelUsuarioGrupoVentas"
        Me.ConRelUsuarioGrupoVentasBindingSource.DataSource = Me.DataSetEric2
        '
        'ConRelUsuarioGrupoVentasTableAdapter
        '
        Me.ConRelUsuarioGrupoVentasTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'FrmUsuarios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1024, 585)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "FrmUsuarios"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Usuarios"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.ConGrupoVentasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConRelUsuarioEmailBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRATIPOUSUARIOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.CONUSUARIOSBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONUSUARIOSBindingNavigator.ResumeLayout(False)
        Me.CONUSUARIOSBindingNavigator.PerformLayout()
        CType(Me.CONUSUARIOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAMEFECHADELSERVIDORBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NueRelUsuarioGrupoVentasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConRelUsuarioGrupoVentasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONUSUARIOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONUSUARIOSTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONUSUARIOSTableAdapter
    Friend WithEvents ClaveLabel1 As System.Windows.Forms.Label
    Friend WithEvents Clv_UsuarioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NombreTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DomicilioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ColoniaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FechaIngresoMaskedTextBox As System.Windows.Forms.MaskedTextBox
    Friend WithEvents FechaSalidaMaskedTextBox As System.Windows.Forms.MaskedTextBox
    Friend WithEvents ActivoCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents PasaporteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_TipoUsuarioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CATVCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents FacturacionCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents CONUSUARIOSBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONUSUARIOSBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents CMBTextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents DAMEFECHADELSERVIDORBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMEFECHADELSERVIDORTableAdapter As sofTV.NewSofTvDataSetTableAdapters.DAMEFECHADELSERVIDORTableAdapter
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents MUESTRATIPOUSUARIOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRATIPOUSUARIOSTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRATIPOUSUARIOSTableAdapter
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents ConRelUsuarioEmailBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConRelUsuarioEmailTableAdapter As sofTV.DataSetEricTableAdapters.ConRelUsuarioEmailTableAdapter
    Friend WithEvents EmailTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric2
    Friend WithEvents ConGrupoVentasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConGrupoVentasTableAdapter As sofTV.DataSetEric2TableAdapters.ConGrupoVentasTableAdapter
    Friend WithEvents GrupoComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents NueRelUsuarioGrupoVentasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NueRelUsuarioGrupoVentasTableAdapter As sofTV.DataSetEric2TableAdapters.NueRelUsuarioGrupoVentasTableAdapter
    Friend WithEvents ConRelUsuarioGrupoVentasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConRelUsuarioGrupoVentasTableAdapter As sofTV.DataSetEric2TableAdapters.ConRelUsuarioGrupoVentasTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents dtpFechaSalida As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFechaIngreso As System.Windows.Forms.DateTimePicker
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
