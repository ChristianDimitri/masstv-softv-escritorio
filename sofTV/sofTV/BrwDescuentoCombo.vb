Imports System.Data.SqlClient
Public Class BrwDescuentoCombo
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim CON As New SqlConnection(MiConexion)
        eOpcion = "N"
        FrmDescuentoCombo.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Me.ConDescuentoComboDataGridView.RowCount = 0 Then
            MsgBox("Selecciona un Combo a Modificar.")
            Exit Sub
        End If
        eOpcion = "M"
        eNombreCombo = Me.ComboTextBox.Text
        FrmDescuentoCombo.Show()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
    End Sub

    Private Sub BrwDescuentoCombo_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Try
            If eBndCombo = True Then
                eBndCombo = False
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.ConDescuentoComboTableAdapter.Connection = CON
                Me.ConDescuentoComboTableAdapter.Fill(Me.DataSetEric2.ConDescuentoCombo, "", 0)
                CON.Close()
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub BrwDescuentoCombo_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            colorea(Me, Me.Name)
            Me.Label1.ForeColor = Color.Black
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.ConDescuentoComboTableAdapter.Connection = CON
            Me.ConDescuentoComboTableAdapter.Fill(Me.DataSetEric2.ConDescuentoCombo, "", 0)
            CON.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If Me.ConDescuentoComboDataGridView.RowCount = 0 Then
            MsgBox("Selecciona un Combo a Consultar.")
            Exit Sub
        End If
        eOpcion = "C"
        eNombreCombo = Me.ComboTextBox.Text
        FrmDescuentoCombo.Show()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Try
            If Me.TextBox1.Text.Length = 0 Then
                Exit Sub
            End If
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.ConDescuentoComboTableAdapter.Connection = CON
            Me.ConDescuentoComboTableAdapter.Fill(Me.DataSetEric2.ConDescuentoCombo, Me.TextBox1.Text, 1)
            CON.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown
        If e.KeyData = Keys.Enter Then
            Try
                If Me.TextBox1.Text.Length = 0 Then
                    Exit Sub
                End If
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.ConDescuentoComboTableAdapter.Connection = CON
                Me.ConDescuentoComboTableAdapter.Fill(Me.DataSetEric2.ConDescuentoCombo, Me.TextBox1.Text, 1)
                CON.Close()
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub
End Class