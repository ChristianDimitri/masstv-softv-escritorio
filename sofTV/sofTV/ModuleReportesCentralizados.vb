﻿Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Xml

Module ModuleReportesCentralizados



    Public Sub ReporteComicionesDeVendedores()
        eOpVentas = 2309
        Dim frmssm As New FrmSelSemanasMetas()
        If (frmssm.ShowDialog() = DialogResult.OK) Then

            Dim frmsv As New FrmSelVendedor()
            If (frmsv.ShowDialog() = DialogResult.OK) Then
                Try
                    Dim cnn As New SqlConnection(MiConexion)
                    Dim cmd As New SqlCommand("Softv_ReporteIncentivoPorVentasDeContratos", cnn)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("@fechaInicio", CDate(frmssm.dtpFechaInicio.Value))
                    cmd.Parameters.AddWithValue("@Clv_SessionUsuarios", eClv_Session)
                    cmd.Parameters.AddWithValue("@semanas", CInt(frmssm.nudSemanas.Value))
                    Dim da As New SqlDataAdapter(cmd)
                    Dim ds As New DataSet()
                    da.Fill(ds)
                    ds.Tables(0).TableName = "Fechas"
                    ds.Tables(1).TableName = "Detalle"
                    Dim frmic As New FrmImprimirCentralizada()
                    Dim rd As New ReportDocument()
                    rd.Load(RutaReportes + "\ReporteIncentivoPorVentaDeContrato.rpt")
                    rd.SetDataSource(ds)
                    frmic.rd = rd
                    frmic.Show()
                Catch ex As Exception

                End Try
            End If
        End If
    End Sub


    Public Sub ReporteClientesSuspendidosYaPagaron()
      
        Try
            Dim cnn As New SqlConnection(MiConexion)
            Dim cmd As New SqlCommand("uspReporteClientesSuspendidosYaPagaron", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0
            cmd.Parameters.AddWithValue("@fechainicio", CDate(eFechaIni))
            cmd.Parameters.AddWithValue("@fechafin", CDate(eFechaFin))
            cmd.Parameters.AddWithValue("@clvTipSer", gloClvTipSerReportes)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet()
            da.Fill(ds)
            ds.Tables(0).TableName = "FacturaFiscal"
            Dim frmi As New FrmImprimirCentralizada()
            frmi.Text = "Reporte de Clientes Suspendidos Que Ya Pagaron"
            Dim rd As New ReportDocument()
            rd.Load(RutaReportes + "\ReporteClientesSuspendidosYaPagaron.rpt")
            rd.SetDataSource(ds)
            rd.SetParameterValue("FechaIni", eFechaIni)
            rd.SetParameterValue("FechaFin", eFechaFin)
            rd.SetParameterValue("NombreEmpresa", GloEmpresa)
            rd.SetParameterValue("NombreCiudad", GloCiudadEmpresa)
            frmi.rd = rd
            frmi.Show()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub


    Public Sub ReporteAparatoRecibidoCajero(ByVal xml As String, ByVal FI As DateTime, ByVal FF As DateTime)
        Try
            Dim cnn As New SqlConnection(MiConexion)
            Dim cmd As New SqlCommand("uspReporteEquipoRecibidoCajera", cnn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.Add("@xml", SqlDbType.Xml)
            cmd.Parameters("@xml").Value = xml

            cmd.Parameters.Add("@FechaInicio", SqlDbType.Date)
            cmd.Parameters("@FechaInicio").Value = FI

            cmd.Parameters.Add("@FechaFin", SqlDbType.Date)
            cmd.Parameters("@FechaFin").Value = FF


            cmd.Parameters.Add("@clvTipSer", SqlDbType.Int)
            cmd.Parameters("@clvTipSer").Value = gloClvTipSerReportes

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet()
            da.Fill(ds)
            ds.Tables(0).TableName = "Datos"
            'ds.Tables(1).TableName = "Detalle"
            Dim frmic As New FrmImprimirCentralizada()
            Dim rd As New ReportDocument()
            rd.Load(RutaReportes + "\rptReporteEquiposRecibidosCajeros.rpt")
            rd.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            rd.DataDefinition.FormulaFields("SubTitulo").Text = "'" & "" & "'"
            rd.DataDefinition.FormulaFields("Titulo").Text = "'" & "Listado de Equipos Recibidos por Cajero(a)" & "'"
            rd.SetDataSource(ds)
            frmic.rd = rd
            frmic.Show()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub


End Module
