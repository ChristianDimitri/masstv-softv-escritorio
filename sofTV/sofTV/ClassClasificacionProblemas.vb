﻿Imports System.Data.SqlClient
Imports System.Collections.Generic

Public Class ClassClasificacionProblemas

#Region "Propiedades"

    Private _clvProblema As Long
    Public Property clvProblema As Long
        Get
            Return _clvProblema
        End Get
        Set(ByVal value As Long)
            _clvProblema = value
        End Set
    End Property

    Private _Descripcion As String
    Public Property Descripcion As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property

    Private _Activo As Boolean
    Public Property Activo As Boolean
        Get
            Return _Activo
        End Get
        Set(ByVal value As Boolean)
            _Activo = value
        End Set
    End Property

    Private _OpAccion As Integer
    Public Property OpAccion As Integer
        Get
            Return _OpAccion
        End Get
        Set(ByVal value As Integer)
            _OpAccion = value
        End Set
    End Property

    Private _OpBusqueda As Integer
    Public Property OpBusqueda As Integer
        Get
            Return _OpBusqueda
        End Get
        Set(ByVal value As Integer)
            _OpBusqueda = value
        End Set
    End Property

    Private _clvLlamada As Long
    Public Property clvLlamada As Long
        Get
            Return _clvLlamada
        End Get
        Set(ByVal value As Long)
            _clvLlamada = value
        End Set
    End Property

    Private _clvQueja As Long
    Public Property clvQueja As Long
        Get
            Return _clvQueja
        End Get
        Set(ByVal value As Long)
            _clvQueja = value
        End Set
    End Property

    Private _clvTipSer As Integer
    Public Property clvTipSer As Integer
        Get
            Return _clvTipSer
        End Get
        Set(ByVal value As Integer)
            _clvTipSer = value
        End Set
    End Property
#End Region

#Region "Constructores"
    Public Sub New()
        _clvProblema = 0
        _Descripcion = ""
        _Activo = False
        _OpAccion = 0
        _OpBusqueda = 0
        _clvLlamada = 0
        _clvQueja = 0
        _clvTipSer = 0
    End Sub
#End Region

#Region "Metodos"

    Dim Clasif As New BaseIII

    Public Function uspInsertaTblClasificacionProblemas() As Boolean
        Try
            Clasif.limpiaParametros()
            Clasif.CreateMyParameter("@clvProblema", SqlDbType.BigInt, _clvProblema)
            Clasif.CreateMyParameter("@descripcion", SqlDbType.VarChar, _Descripcion, 250)
            Clasif.CreateMyParameter("@activo", SqlDbType.Bit, _Activo)
            Clasif.CreateMyParameter("@opAccion", SqlDbType.Int, _OpAccion)
            Clasif.CreateMyParameter("@bndInsertar", ParameterDirection.Output, SqlDbType.Bit)
            Clasif.ProcedimientoOutPut("uspInsertaTblClasificacionProblemas")

            uspInsertaTblClasificacionProblemas = CBool(Clasif.dicoPar("@bndInsertar").ToString())
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function uspConsultaTblClasificacionProblemas() As DataTable
        Try
            Clasif.limpiaParametros()
            Clasif.CreateMyParameter("@clvProblema", SqlDbType.BigInt, _clvProblema)
            Clasif.CreateMyParameter("@descripcion", SqlDbType.VarChar, _Descripcion, 250)
            Clasif.CreateMyParameter("@opBusqueda", SqlDbType.Int, _OpBusqueda)
            uspConsultaTblClasificacionProblemas = Clasif.ConsultaDT("uspConsultaTblClasificacionProblemas")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub uspEliminaTblClasificacionProblemas()
        Try
            Clasif.limpiaParametros()
            Clasif.CreateMyParameter("@clvProblema", SqlDbType.BigInt, _clvProblema)
            Clasif.Inserta("uspEliminaTblClasificacionProblemas")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub uspInseraTblRelQuejaProblema()
        Try
            Clasif.limpiaParametros()
            Clasif.CreateMyParameter("@clvLlamada", SqlDbType.BigInt, _clvLlamada)
            Clasif.CreateMyParameter("@clvQueja", SqlDbType.BigInt, _clvQueja)
            Clasif.CreateMyParameter("@clvProblema", SqlDbType.BigInt, _clvProblema)
            Clasif.CreateMyParameter("@opAccion", SqlDbType.Int, _OpAccion)
            Clasif.Inserta("uspInseraTblRelQuejaProblema")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function uspConsultaTblRelQuejaProblema() As Long
        Try
            Clasif.limpiaParametros()
            Clasif.CreateMyParameter("@clvLlamada", SqlDbType.BigInt, _clvLlamada)
            Clasif.CreateMyParameter("@clvQueja", SqlDbType.BigInt, _clvQueja)
            Clasif.CreateMyParameter("@opAccion", SqlDbType.Int, _OpAccion)
            Clasif.CreateMyParameter("@clvProblema", ParameterDirection.Output, SqlDbType.BigInt)
            Clasif.ProcedimientoOutPut("uspConsultaTblRelQuejaProblema")

            uspConsultaTblRelQuejaProblema = CLng(Clasif.dicoPar("@clvProblema").ToString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function uspConsultaComentarioAgenda() As String
        Try
            Dim DT As New DataTable
            Clasif.limpiaParametros()
            Clasif.CreateMyParameter("@clvQueja", SqlDbType.BigInt, _clvQueja)
            DT = Clasif.ConsultaDT("uspConsultaComentarioAgenda")

            For Each DR As DataRow In DT.Rows
                uspConsultaComentarioAgenda = DR("comentario").ToString()
            Next

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function uspChecaCuantasOrdenesQuejas() As DataTable
        Clasif.limpiaParametros()
        Clasif.CreateMyParameter("@opAccion", SqlDbType.Int, _OpAccion)
        Clasif.CreateMyParameter("@clvTipSer", SqlDbType.Int, _clvTipSer)
        uspChecaCuantasOrdenesQuejas = Clasif.ConsultaDT("uspChecaCuantasOrdenesQuejas")
    End Function
#End Region

#Region "Métodos Personalizados"
    'Private Sub Consultar()
    '    _clvProblema = 0
    '    _Descripcion = String.Empty
    '    _OpBusqueda = 1
    '    uspConsultaTblClasificacionProblemas()
    'End Sub

    'Private Sub Consultar(ByVal prmClvProblema As Long)
    '    _clvProblema = prmClvProblema
    '    _Descripcion = String.Empty
    '    _OpBusqueda = 2
    '    uspConsultaTblClasificacionProblemas()
    'End Sub

    'Private Sub Consultar(ByVal prmDescripcion As String)
    '    _clvProblema = 0
    '    _Descripcion = prmDescripcion
    '    _OpBusqueda = 3
    '    uspConsultaTblClasificacionProblemas()
    'End Sub
#End Region
End Class
