<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCajasAdicionales
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CmbLabel1 = New System.Windows.Forms.Label()
        Me.CajasUpDown = New System.Windows.Forms.NumericUpDown()
        Me.Button1 = New System.Windows.Forms.Button()
        CType(Me.CajasUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CmbLabel1
        '
        Me.CmbLabel1.AutoSize = True
        Me.CmbLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CmbLabel1.Location = New System.Drawing.Point(62, 5)
        Me.CmbLabel1.Name = "CmbLabel1"
        Me.CmbLabel1.Size = New System.Drawing.Size(189, 20)
        Me.CmbLabel1.TabIndex = 0
        Me.CmbLabel1.Text = "No. Cajas Solicitadas :"
        '
        'CajasUpDown
        '
        Me.CajasUpDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CajasUpDown.Location = New System.Drawing.Point(117, 38)
        Me.CajasUpDown.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.CajasUpDown.Name = "CajasUpDown"
        Me.CajasUpDown.Size = New System.Drawing.Size(59, 35)
        Me.CajasUpDown.TabIndex = 1
        Me.CajasUpDown.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Orange
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Button1.Location = New System.Drawing.Point(78, 111)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(135, 33)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "&Aceptar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'FrmCajasAdicionales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(294, 156)
        Me.ControlBox = False
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.CajasUpDown)
        Me.Controls.Add(Me.CmbLabel1)
        Me.Name = "FrmCajasAdicionales"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cajas Adicionales"
        CType(Me.CajasUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CmbLabel1 As System.Windows.Forms.Label
    Friend WithEvents CajasUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
