<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMotAtenTel
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DescripcionLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMotAtenTel))
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.ConMotAtenTelBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConMotAtenTelTableAdapter = New sofTV.DataSetEricTableAdapters.ConMotAtenTelTableAdapter()
        Me.Clv_MotivoTextBox = New System.Windows.Forms.TextBox()
        Me.DescripcionTextBox = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.ConMotAtenTelBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ConMotAtenTelBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        DescripcionLabel = New System.Windows.Forms.Label()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConMotAtenTelBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.ConMotAtenTelBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ConMotAtenTelBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'DescripcionLabel
        '
        DescripcionLabel.AutoSize = True
        DescripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescripcionLabel.Location = New System.Drawing.Point(50, 39)
        DescripcionLabel.Name = "DescripcionLabel"
        DescripcionLabel.Size = New System.Drawing.Size(99, 16)
        DescripcionLabel.TabIndex = 4
        DescripcionLabel.Text = "Descripción :"
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ConMotAtenTelBindingSource
        '
        Me.ConMotAtenTelBindingSource.DataMember = "ConMotAtenTel"
        Me.ConMotAtenTelBindingSource.DataSource = Me.DataSetEric
        '
        'ConMotAtenTelTableAdapter
        '
        Me.ConMotAtenTelTableAdapter.ClearBeforeFill = True
        '
        'Clv_MotivoTextBox
        '
        Me.Clv_MotivoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConMotAtenTelBindingSource, "Clv_Motivo", True))
        Me.Clv_MotivoTextBox.Location = New System.Drawing.Point(493, 206)
        Me.Clv_MotivoTextBox.Name = "Clv_MotivoTextBox"
        Me.Clv_MotivoTextBox.ReadOnly = True
        Me.Clv_MotivoTextBox.Size = New System.Drawing.Size(10, 20)
        Me.Clv_MotivoTextBox.TabIndex = 3
        Me.Clv_MotivoTextBox.TabStop = False
        '
        'DescripcionTextBox
        '
        Me.DescripcionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DescripcionTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.DescripcionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConMotAtenTelBindingSource, "Descripcion", True))
        Me.DescripcionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionTextBox.Location = New System.Drawing.Point(53, 69)
        Me.DescripcionTextBox.Multiline = True
        Me.DescripcionTextBox.Name = "DescripcionTextBox"
        Me.DescripcionTextBox.Size = New System.Drawing.Size(503, 82)
        Me.DescripcionTextBox.TabIndex = 5
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(420, 195)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.ConMotAtenTelBindingNavigator)
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(Me.Clv_MotivoTextBox)
        Me.Panel1.Controls.Add(Me.DescripcionTextBox)
        Me.Panel1.Controls.Add(DescripcionLabel)
        Me.Panel1.Location = New System.Drawing.Point(1, 1)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(590, 258)
        Me.Panel1.TabIndex = 7
        '
        'ConMotAtenTelBindingNavigator
        '
        Me.ConMotAtenTelBindingNavigator.AddNewItem = Nothing
        Me.ConMotAtenTelBindingNavigator.BindingSource = Me.ConMotAtenTelBindingSource
        Me.ConMotAtenTelBindingNavigator.CountItem = Nothing
        Me.ConMotAtenTelBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.ConMotAtenTelBindingNavigator.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConMotAtenTelBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.ConMotAtenTelBindingNavigatorSaveItem})
        Me.ConMotAtenTelBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.ConMotAtenTelBindingNavigator.MoveFirstItem = Nothing
        Me.ConMotAtenTelBindingNavigator.MoveLastItem = Nothing
        Me.ConMotAtenTelBindingNavigator.MoveNextItem = Nothing
        Me.ConMotAtenTelBindingNavigator.MovePreviousItem = Nothing
        Me.ConMotAtenTelBindingNavigator.Name = "ConMotAtenTelBindingNavigator"
        Me.ConMotAtenTelBindingNavigator.PositionItem = Nothing
        Me.ConMotAtenTelBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ConMotAtenTelBindingNavigator.Size = New System.Drawing.Size(590, 25)
        Me.ConMotAtenTelBindingNavigator.TabIndex = 1
        Me.ConMotAtenTelBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(90, 22)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ConMotAtenTelBindingNavigatorSaveItem
        '
        Me.ConMotAtenTelBindingNavigatorSaveItem.Image = CType(resources.GetObject("ConMotAtenTelBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.ConMotAtenTelBindingNavigatorSaveItem.Name = "ConMotAtenTelBindingNavigatorSaveItem"
        Me.ConMotAtenTelBindingNavigatorSaveItem.Size = New System.Drawing.Size(91, 22)
        Me.ConMotAtenTelBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'FrmMotAtenTel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(592, 261)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "FrmMotAtenTel"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Motivo de Atención Telefónica"
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConMotAtenTelBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.ConMotAtenTelBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ConMotAtenTelBindingNavigator.ResumeLayout(False)
        Me.ConMotAtenTelBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents ConMotAtenTelBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConMotAtenTelTableAdapter As sofTV.DataSetEricTableAdapters.ConMotAtenTelTableAdapter
    Friend WithEvents Clv_MotivoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DescripcionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents ConMotAtenTelBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ConMotAtenTelBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
End Class
