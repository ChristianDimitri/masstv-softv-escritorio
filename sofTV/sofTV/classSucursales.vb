﻿Public Class classSucursales
#Region "Variables Globales"
#End Region

#Region "Propiedades"
    Private _clvCompaniaSoftv As Long
    Public Property clvCompaniaSoftv As Long
        Get
            Return _clvCompaniaSoftv
        End Get
        Set(ByVal valor As Long)
            _clvCompaniaSoftv = valor
        End Set
    End Property

    Private _clvSucursal As Integer
    Public Property clvSucursal As Integer
        Get
            Return _clvSucursal
        End Get
        Set(ByVal valor As Integer)
            _clvSucursal = valor
        End Set
    End Property

    Private _calle As String
    Public Property calle As String
        Get
            Return _calle
        End Get
        Set(ByVal valor As String)
            _calle = valor
        End Set
    End Property

    Private _numero As String
    Public Property numero As String
        Get
            Return _numero
        End Get
        Set(ByVal valor As String)
            _numero = valor
        End Set
    End Property

    Private _colonia As String
    Public Property colonia As String
        Get
            Return _colonia
        End Get
        Set(ByVal valor As String)
            _colonia = valor
        End Set
    End Property

    Private _cp As Integer
    Public Property cp As Integer
        Get
            Return _cp
        End Get
        Set(ByVal valor As Integer)
            _cp = valor
        End Set
    End Property

    Private _municipio As String
    Public Property municipio As String
        Get
            Return _municipio
        End Get
        Set(ByVal valor As String)
            _municipio = valor
        End Set
    End Property

    Private _ciudad As String
    Public Property ciudad As String
        Get
            Return _ciudad
        End Get
        Set(ByVal valor As String)
            _ciudad = valor
        End Set
    End Property

    Private _telefono As String
    Public Property telefono As String
        Get
            Return _telefono
        End Get
        Set(ByVal valor As String)
            _telefono = valor
        End Set
    End Property

    Private _serieTicket As String
    Public Property serieTicket As String
        Get
            Return _serieTicket
        End Get
        Set(ByVal valor As String)
            _serieTicket = valor
        End Set
    End Property

    Private _ultFolioUsado As Integer
    Public Property ultFolioUsado As Integer
        Get
            Return _ultFolioUsado
        End Get
        Set(ByVal valor As Integer)
            _ultFolioUsado = valor
        End Set
    End Property

    Private _clvSerieFacFiscal As String
    Public Property clvSerieFacFiscal As String
        Get
            Return _clvSerieFacFiscal
        End Get
        Set(ByVal valor As String)
            _clvSerieFacFiscal = valor
        End Set
    End Property

    Private _clvSerieFacGlobal As String
    Public Property clvSerieFacGlobal As String
        Get
            Return _clvSerieFacGlobal
        End Get
        Set(ByVal valor As String)
            _clvSerieFacGlobal = valor
        End Set
    End Property

    Private _idRelacion As Integer
    Public Property idRelacion As Integer
        Get
            Return _idRelacion
        End Get
        Set(ByVal valor As Integer)
            _idRelacion = valor
        End Set
    End Property
#End Region

#Region "Constructores"
    Public Sub New()
        _clvCompaniaSoftv = 0
        _clvSucursal = 0
        _calle = ""
        _numero = ""
        _colonia = ""
        _cp = 0
        _municipio = ""
        _ciudad = ""
        _telefono = ""
        _serieTicket = ""
        _ultFolioUsado = 0
        _clvSerieFacFiscal = ""
        _clvSerieFacGlobal = ""
        _idRelacion = 0
    End Sub
#End Region

#Region "Métodos"
    Dim sucursales As New BaseIII

    Public Function uspMuestraSerieFolioMizar() As DataTable
        Try
            sucursales.limpiaParametros()
            sucursales.CreateMyParameter("@clvCompaniaSoftv", SqlDbType.Int, _clvCompaniaSoftv)

            uspMuestraSerieFolioMizar = sucursales.ConsultaDT("uspMuestraSerieFolioMizar")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function uspConsultaCompaniasActivas() As DataTable
        Try
            sucursales.limpiaParametros()
            uspConsultaCompaniasActivas = sucursales.ConsultaDT("uspConsultaCompaniasActivas")
        Catch ex As Exception

        End Try
    End Function

    Public Sub uspInsertaTblRelSucursalDatosGenerales()
        Try
            sucursales.limpiaParametros()
            sucursales.CreateMyParameter("@clvSucursal", SqlDbType.Int, _clvSucursal)
            sucursales.CreateMyParameter("@calle", SqlDbType.VarChar, _calle, 250)
            sucursales.CreateMyParameter("@numero", SqlDbType.VarChar, _numero, 100)
            sucursales.CreateMyParameter("@colonia", SqlDbType.VarChar, _colonia, 250)
            sucursales.CreateMyParameter("@cp", SqlDbType.Int, _cp)
            sucursales.CreateMyParameter("@municipio", SqlDbType.VarChar, _municipio, 250)
            sucursales.CreateMyParameter("@ciudad", SqlDbType.VarChar, _ciudad, 250)
            sucursales.CreateMyParameter("@telefono", SqlDbType.VarChar, _telefono, 250)

            sucursales.Inserta("uspInsertaTblRelSucursalDatosGenerales")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function uspConsultaTblRelSucursalDatosGenerales() As DataTable
        Try
            sucursales.limpiaParametros()
            sucursales.CreateMyParameter("@clvSucursal", SqlDbType.Int, _clvSucursal)
            uspConsultaTblRelSucursalDatosGenerales = sucursales.ConsultaDT("uspConsultaTblRelSucursalDatosGenerales")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function uspInsertatblRelSucursalDatosFiscales() As DataTable
        Try
            sucursales.limpiaParametros()
            sucursales.CreateMyParameter("@clvSucursal", SqlDbType.Int, _clvSucursal)
            sucursales.CreateMyParameter("@clvCompania", SqlDbType.Int, _clvCompaniaSoftv)
            sucursales.CreateMyParameter("@serieTicket", SqlDbType.VarChar, _serieTicket, 50)
            sucursales.CreateMyParameter("@ultFolioUsado", SqlDbType.Int, _ultFolioUsado)
            sucursales.CreateMyParameter("@clvSerieFacFiscal", SqlDbType.VarChar, _clvSerieFacFiscal, 50)
            sucursales.CreateMyParameter("@clvSerieFacGlobal", SqlDbType.VarChar, _clvSerieFacGlobal, 50)

            uspInsertatblRelSucursalDatosFiscales = sucursales.ConsultaDT("uspInsertatblRelSucursalDatosFiscales")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function uspConsultatblRelSucursalDatosFiscales() As DataTable
        Try
            sucursales.limpiaParametros()
            sucursales.CreateMyParameter("@clvSucursal", SqlDbType.Int, _clvSucursal)

            uspConsultatblRelSucursalDatosFiscales = sucursales.ConsultaDT("uspConsultatblRelSucursalDatosFiscales")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub uspEliminatblRelSucursalDatosFiscales()
        Try
            sucursales.limpiaParametros()
            sucursales.CreateMyParameter("@idRelacion", SqlDbType.Int, _idRelacion)

            sucursales.Inserta("uspEliminatblRelSucursalDatosFiscales")
        Catch ex As Exception

        End Try
    End Sub
#End Region

End Class
