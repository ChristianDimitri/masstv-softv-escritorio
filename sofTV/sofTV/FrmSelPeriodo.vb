Imports System.Data.SqlClient

Public Class FrmSelPeriodo


    Private Sub MueveSeleccion_TAbla(ByVal clv_Session As Long, ByVal clv_id As Integer, ByVal op As Integer)
        Dim cmd As New SqlClient.SqlCommand
        Dim CON4 As New SqlConnection(MiConexion)
        Try
            If IsNumeric(clv_Session) = False Then clv_Session = 0
            If clv_Session > 0 Then

                CON4.Open()
                With cmd
                    .CommandText = "MUEVE_Selecciona"
                    .CommandTimeout = 0
                    .CommandType = CommandType.StoredProcedure
                    .Connection = CON4
                    Dim prm As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                    Dim prm2 As New SqlParameter("@Clv_Periodo", SqlDbType.Int)
                    Dim prm3 As New SqlParameter("@op", SqlDbType.Int)
                    prm.Direction = ParameterDirection.Input
                    prm2.Direction = ParameterDirection.Input
                    prm3.Direction = ParameterDirection.Input
                    prm.Value = clv_Session
                    prm2.Value = clv_id
                    prm3.Value = op
                    .Parameters.Add(prm)
                    .Parameters.Add(prm2)
                    .Parameters.Add(prm3)
                    Dim i As Integer = .ExecuteNonQuery
                End With
                CON4.Close()
            End If


        Catch ex As System.Exception
            If CON4.State <> ConnectionState.Closed Then CON4.Close()
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub Inicializa_Seleccion_TAbla(ByVal clv_Session As Long, ByVal query As String)
        Dim cmd As New SqlClient.SqlCommand
        Dim CON4 As New SqlConnection(MiConexion)
        Try
            If IsNumeric(clv_Session) = False Then clv_Session = 0
            If clv_Session > 0 Then

                CON4.Open()
                With cmd
                    .CommandText = "Llena_Tabla_Selecciona1"
                    .CommandTimeout = 0
                    .CommandType = CommandType.StoredProcedure
                    .Connection = CON4
                    Dim prm As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                    Dim prm2 As New SqlParameter("@QueryTablaOrigen", SqlDbType.VarChar, 250)
                    prm.Direction = ParameterDirection.Input
                    prm2.Direction = ParameterDirection.Input
                    prm.Value = clv_Session
                    prm2.Value = query
                    .Parameters.Add(prm)
                    .Parameters.Add(prm2)
                    Dim i As Integer = .ExecuteNonQuery
                End With
                CON4.Close()
            End If


        Catch ex As System.Exception
            If CON4.State <> ConnectionState.Closed Then CON4.Close()
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Guarda_Seleccion_TAbla(ByVal clv_Session As Long, ByVal Tabla As String)
        Dim cmd As New SqlClient.SqlCommand
        Dim CON4 As New SqlConnection(MiConexion)
        Try
            If IsNumeric(clv_Session) = False Then clv_Session = 0
            If clv_Session > 0 Then

                CON4.Open()
                With cmd
                    .CommandText = "GUARDA_TABLA_Selecciona"
                    .CommandTimeout = 0
                    .CommandType = CommandType.StoredProcedure
                    .Connection = CON4
                    Dim prm As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                    Dim prm2 As New SqlParameter("@Tabla_Guardar", SqlDbType.VarChar, 150)
                    prm.Direction = ParameterDirection.Input
                    prm2.Direction = ParameterDirection.Input
                    prm.Value = clv_Session
                    prm2.Value = Tabla
                    .Parameters.Add(prm)
                    .Parameters.Add(prm2)
                    Dim i As Integer = .ExecuteNonQuery
                End With
                CON4.Close()
            End If


        Catch ex As System.Exception
            If CON4.State <> ConnectionState.Closed Then CON4.Close()
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub



    Private Sub LLENA_LISBOX2(ByVal CLV_SESSION As Long)

        Dim sw As Integer = 0
        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("MUESTRATabla_Selecciona2", con)
        Me.ListBox2.Items.Clear()
        Me.ListBox4.Items.Clear()
        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New Data.SqlClient.SqlParameter( _
                "@Clv_Session", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = CLV_SESSION
        cmd.Parameters.Add(prm)

        con.Open()

        Dim reader As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Try
            While reader.Read()

                'Llenamos los TxtBox con los datos respectivos
                'Me.ListBox1.ItemsInsert(reader(0).ToString, reader(1).ToString)
                Me.ListBox2.Items.Add(reader(1).ToString)
                Me.ListBox4.Items.Add(reader(0).ToString)
                sw = 1
            End While
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            reader.Close()
        End Try
        con.Close()
        'If sw = 0 Then
        '    Me.ComboBox2.SelectedValue = 0
        '    Me.ComboBox3.SelectedValue = 0
        'End If

    End Sub


    Private Sub LLENA_LISBOX1(ByVal CLV_SESSION As Long)

        Dim sw As Integer = 0
        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("MUESTRATabla_Selecciona1", con)
        Me.ListBox1.Items.Clear()
        Me.ListBox3.Items.Clear()
        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New Data.SqlClient.SqlParameter( _
                "@Clv_Session", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = CLV_SESSION
        cmd.Parameters.Add(prm)

        con.Open()

        Dim reader As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Try
            While reader.Read()

                'Llenamos los TxtBox con los datos respectivos
                'Me.ListBox1.ItemsInsert(reader(0).ToString, reader(1).ToString)
                Me.ListBox1.Items.Add(reader(1).ToString)
                Me.ListBox3.Items.Add(reader(0).ToString)
                sw = 1
            End While
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            reader.Close()
        End Try
        con.Close()
        'If sw = 0 Then
        '    Me.ComboBox2.SelectedValue = 0
        '    Me.ComboBox3.SelectedValue = 0
        'End If

    End Sub


    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If Me.CheckBox1.CheckState = CheckState.Checked Then
            LocPeriodo1 = True
        Else
            LocPeriodo1 = False
        End If


    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If Me.CheckBox2.CheckState = CheckState.Checked Then
            LocPeriodo2 = True
        Else
            LocPeriodo2 = False
        End If
    End Sub

    Private Sub FrmSelPeriodo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        'Inicializa_Seleccion_TAbla(LocClv_session, "SELECT Clv_colonia,nombre FROM Colonias")
        Inicializa_Seleccion_TAbla(LocClv_session, "SELECT Clv_Periodo,Descripcion FROM CatalogoPeriodosCorte Where Habilitar=0 Order by Descripcion")
        Me.LLENA_LISBOX1(LocClv_session)
        LocPeriodo1 = False
        LocPeriodo2 = False
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If Me.ListBox2.Items.Count = 0 Then
            MsgBox("Seleccione al menos un Periodo", MsgBoxStyle.Information)
            Exit Sub
        Else
            Guarda_Seleccion_TAbla(LocClv_session, "Tabla_Seleccion3")
            Select Case LocOp
                Case 1
                    bndReportC = True
                Case 5
                    bndReport2 = True
                Case 3
                    If bnd_Canc_Sin_Mens = False Then
                        bndfechareport = True
                    ElseIf bnd_Canc_Sin_Mens = True Then
                        bnd_Canc_Sin_Mens = False
                        bnd_Canc_Sin_Mens_buena = True
                    End If
                Case 4
                    bndReport = True
                Case 6
                    GloBndEtiqueta = True
                Case 7
                    If GloOpEtiqueta <> "1" Then
                        If IdSistema = "SA" And (GloOpEtiqueta = "3" Or GloOpEtiqueta = "4" Or GloOpEtiqueta = "1") Then
                            bndAvisos2 = True
                        Else
                            FrmSelRecord.Show()
                        End If
                    ElseIf GloOpEtiqueta = "1" Then
                        FrmTelsi.Show()
                        'Else
                        '    bndAvisos2 = True
                        'End If
                    End If

                Case 8
                    GloBndSelBanco = True
                Case 9
                    bndReportA = True
                Case 10
                    LocServicios = True
                Case 20
                    'If bec_bnd = False Then
                    Locreportcity = True
                    'ElseIf bec_bnd = True Then

                    'End If
                Case 21
                    bndfechareport2 = True
                Case 22
                    GloBndSelBanco = True
                Case 25
                    Locreportcity = True
                Case 30
                    LocBndRepMix = True
                    LocGloOpRep = 2
                    FrmImprimirFac.Show()
                Case 35
                    'Locbndrepcontspago = True
                    FrmSelFechas.Show()
                Case 90
                    'LReportecombo1 = True
                    'FrmImprimirContrato.Show()
                    FrmOpcionRepCombos.Show()
            End Select
            If LEdo_Cuenta = True Or LEdo_Cuenta2 = True Then
                ' bec_bnd = False
                FrmImprimirContrato.Show()
                'LocreportEstado = True
            End If
        End If
        Me.Close()

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.ListBox3.SelectedIndex = Me.ListBox1.SelectedIndex
        If IsNumeric(Me.ListBox3.Text) = True Then
            Me.MueveSeleccion_TAbla(LocClv_session, Me.ListBox3.Text, 2)
            Me.LLENA_LISBOX2(LocClv_session)
            Me.LLENA_LISBOX1(LocClv_session)
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.ListBox3.SelectedIndex = Me.ListBox1.SelectedIndex
        If Me.ListBox3.Items.Count > 0 Then
            Me.MueveSeleccion_TAbla(LocClv_session, 0, 22)
            Me.LLENA_LISBOX2(LocClv_session)
            Me.LLENA_LISBOX1(LocClv_session)
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.ListBox4.SelectedIndex = Me.ListBox2.SelectedIndex
        If Me.ListBox4.Items.Count > 0 Then
            Me.MueveSeleccion_TAbla(LocClv_session, 0, 11)
            Me.LLENA_LISBOX2(LocClv_session)
            Me.LLENA_LISBOX1(LocClv_session)
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.ListBox4.SelectedIndex = Me.ListBox2.SelectedIndex
        If IsNumeric(Me.ListBox4.Text) = True Then
            Me.MueveSeleccion_TAbla(LocClv_session, Me.ListBox4.Text, 1)
            Me.LLENA_LISBOX2(LocClv_session)
            Me.LLENA_LISBOX1(LocClv_session)
        End If
    End Sub
End Class