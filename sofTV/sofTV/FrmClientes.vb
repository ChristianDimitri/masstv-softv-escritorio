﻿Imports System.Data.SqlClient
Imports System.Drawing.Drawing2D
Imports System.Text
Imports System.Collections.Generic

Public Class FrmClientes
    'Se Utiliza para la validacion de:-----------------------------------------------------
    '1. No Agregar más de 1 servicio principal a una Caja/Tarjeta
    '2. Si el cliente es de pagos recurrentes, tenga sus datos bancarios debidamente llenos
    '3. Cuando es Nuevo Cliente, verifique que el nombre de Cliente no exista en la BD

    '--------------------------------------------------------------------------------------

    '''====
    Dim cortesiatv As Boolean = False
    Dim cortesiadig As Boolean = False
    Dim cortesiaint As Boolean = False
    Dim BanderaCambio As Integer = 0
    Dim LocClv_Calletmp As Integer = 0
    Dim loc_clv_Sector As Integer = 0
    'Dim LocBndAct As Boolean = False



    ''''''''''Variables Bitacoras'''''''''
    Private locclv_servicio As Integer = 0
    Private contratobit As Integer = 0
    Private nombre_cliente As String = Nothing
    Private Clv_Callebit As Integer = 0
    Private NUMERO_casabit As String = Nothing
    Private ENTRECALLESbit As String = Nothing
    Private Clv_Coloniabit As Integer = 0
    Private CodigoPostalbit As String = Nothing
    Private TELEFONObit As String = Nothing
    Private CELULARbit As String = Nothing
    Private DESGLOSA_Ivabit As Boolean = False
    Private SoloInternetbit As Boolean = False
    Private eshotelbit As Boolean = False
    Private clv_Ciudadbit As Integer = 0
    Private Email As String = Nothing
    Private clv_sectorbit As Integer = 0
    Private Clv_Periodobit As Integer = 0
    Private clv_tipo_pagobit As Integer = 0
    Private NombrePaqueteElimino As String = Nothing
    Private NombreMAC As String = Nothing
    Private GloOp As Integer = 0
    Private LocGloContratoAnt As Long = 0
    Private LocGloContratoNet As Long = 0
    Private LocGloClv_unianet As Long = 0
    Private GLoPaqueteAgrega As String = Nothing

    '''Digital e Internet'''''''''''
    Private status As String = Nothing
    Private fecha_solicitud As String = Nothing
    Private fecha_instalacio As String = Nothing
    Private fecha_suspension As String = Nothing
    Private fecha_baja As String = Nothing
    Private fecha_Fuera_Area As String = Nothing
    Private FECHA_ULT_PAGO As String = Nothing
    Private PrimerMensualidad As Boolean = False
    Private ultimo_mes As Integer = 0
    Private ultimo_anio As Integer = 0
    Private primerMesAnt As Boolean = False
    Private statusAnt As String = Nothing
    Private facturaAnt As String = Nothing
    Private GENERAOSINSTA As Boolean = False
    Private factura As String = Nothing
    Private Clv_Vendedor As Integer = 0
    Private Clv_Promocion As Integer = 0
    Private Obs As String = Nothing
    Private DESCRIPCION As String = Nothing
    Private Cortesia As Boolean = False
    Private Descuento As String = Nothing
    Private statusTarjeta As String = Nothing
    Private Activacion As String = Nothing
    Private Suspension As String = Nothing
    Private Baja As String = Nothing
    Private serenta As String = Nothing
    Private macasignada As String = Nothing

    'Internet
    Private Locmarca As String = Nothing
    Private LocTipoApar As String = Nothing
    Private LocTipservcabl As String = Nothing
    Private LocTipoCablemodem As String = Nothing
    Private LocTranspaso As String = Nothing
    Private Loc1pago As String = Nothing
    Private Loc2pago As String = Nothing
    Private BndEsInternet As Boolean = False

    'Tv Básica
    Private LocVendedorTv As String = Nothing
    Private LocTVSINPAGO As Integer = 0
    Private LocTVCONPAGO As Integer = 0
    Private LocFec_ULT_PAG_ANT As String = Nothing
    Private LocClv_MOTCAN As Integer = 0
    Private LocPRIMERMESANT As Boolean = False
    Private LocClv_TipoServicioTV As Integer = 0
    Private LocpuntosAcumulados As Integer = 0
    Private LocTipSerTv As String = Nothing
    Private LocMOTCAN As String = Nothing

    Private validacion As Integer = 0

    ''''''''''Fin Variables Bitacora '''''''''''''''
    Private BndMini As Short = 0
    Private lOC_CONTRATONETDIG As Long = 0
    Private loc_Clv_InicaDig As Long = 0
    Private bndbitacora As Boolean = False
    Private NomDig As Char
    Private BndDClientes As Boolean = False
    Private BndRobaSeñal As Boolean = False
    Private ClvTipSer As Integer = 3
    Private Sub Asigna_taps()
        If IdSistema = "LO" Or IdSistema = "VA" Or IdSistema = "YU" Then
            Me.NOMBRETextBox.TabIndex = 1000
            Me.CONTRATOTextBox.TabIndex = 1001
            Me.TextBox4.TabIndex = 0
            Me.TextBox7.TabIndex = 1
            Me.TextBox27.TabIndex = 2

            'Comunes
            Me.CALLEComboBox.TabIndex = 3
            Me.NUMEROTextBox.TabIndex = 4
            Me.TxtNumeroInt.TabIndex = 5
            Me.ENTRECALLESTextBox.TabIndex = 6
            Me.CodigoPostalTextBox.TabIndex = 7
            Me.COLONIAComboBox.TabIndex = 8
            Me.ComboBox15.TabIndex = 9
            Me.CIUDADComboBox.TabIndex = 10
            Me.SoloInternetCheckBox.TabIndex = 11
            Me.DESGLOSA_IvaCheckBox.TabIndex = 12
            Me.TELEFONOTextBox.TabIndex = 13
            Me.CELULARTextBox.TabIndex = 14
            Me.EmailTextBox.TabIndex = 15
            Me.TextBox28.TabIndex = 16
            Me.ComboBox7.TabIndex = 17

        Else
            Me.NOMBRETextBox.TabIndex = 0
            Me.CONTRATOTextBox.TabIndex = 1001
            Me.TextBox4.TabIndex = 1002
            Me.TextBox7.TabIndex = 1003
            Me.TextBox27.TabIndex = 1004

            'Comunes
            Me.CALLEComboBox.TabIndex = 3
            Me.NUMEROTextBox.TabIndex = 4
            Me.TxtNumeroInt.TabIndex = 5
            Me.ENTRECALLESTextBox.TabIndex = 6
            Me.CodigoPostalTextBox.TabIndex = 7
            Me.COLONIAComboBox.TabIndex = 8
            Me.ComboBox15.TabIndex = 9
            Me.CIUDADComboBox.TabIndex = 10
            Me.SoloInternetCheckBox.TabIndex = 11
            Me.DESGLOSA_IvaCheckBox.TabIndex = 12
            Me.TELEFONOTextBox.TabIndex = 13
            Me.CELULARTextBox.TabIndex = 14
            Me.EmailTextBox.TabIndex = 15
            Me.TextBox28.TabIndex = 16
            Me.ComboBox7.TabIndex = 17
        End If
    End Sub
    Private Sub Guarda_No_Int(ByVal contrato As Long, ByVal noInt As String)
        Dim CONar As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Try
            cmd = New SqlCommand()
            CONar.Open()
            'Nuevo_Rel_Contrato_NoInt (@contrato bigint,@NoInt varchar(50))
            With cmd
                .CommandText = "Nuevo_Rel_Contrato_NoInt"
                .Connection = CONar
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@contrato", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = contrato
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@NoInt", SqlDbType.VarChar, 50)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = noInt
                .Parameters.Add(prm1)

                Dim ia As Integer = .ExecuteNonQuery()
            End With
            CONar.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Consulta_No_Int(ByVal contrato As Long)
        Dim COnar As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Try
            cmd = New SqlCommand()
            COnar.Open()
            'Consulta_No_Int (@contrato bigint)
            With cmd
                .CommandText = "Consulta_No_Int"
                .Connection = COnar
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@contrato", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = contrato
                .Parameters.Add(prm)

                Dim reader As SqlDataReader = .ExecuteReader()
                While reader.Read()
                    Me.TxtNumeroInt.Text = reader.GetValue(0).ToString
                End While
            End With
            COnar.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Agenda_Clientes()
        If IdSistema = "LO" Or IdSistema = "YU" Then
            Dim CON As New SqlConnection(MiConexion)
            Dim contador As Integer = 0
            CON.Open()
            'AQUI DEBE DE IR LO DE LA AGENDA
            Dim conlidia As New SqlClient.SqlConnection(MiConexion)
            Dim comando As New SqlClient.SqlCommand
            conlidia.Open()
            With comando
                'Checa_citas2 (@Contrato BIGINT,@Clv_cita bigint,@op int,@error int output)
                .CommandText = "Checa_citas2"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = conlidia
                Dim prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = CLng(Me.CONTRATOTextBox.Text)
                .Parameters.Add(prm)

                Dim prm2 As New SqlParameter("@Clv_cita", SqlDbType.BigInt)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = 0
                .Parameters.Add(prm2)

                Dim prm3 As New SqlParameter("@op", SqlDbType.Int)
                prm3.Direction = ParameterDirection.Input
                prm3.Value = 0
                .Parameters.Add(prm3)

                Dim prm4 As New SqlParameter("@error", SqlDbType.Int)
                prm4.Direction = ParameterDirection.Output
                prm4.Value = 0
                .Parameters.Add(prm4)

                Dim i As Integer = comando.ExecuteNonQuery
                contador = prm4.Value
            End With
            conlidia.Close()
            If contador > 0 Then
                LocBndAgendaClientes = True
                LocGloContratoIni = CLng(Me.CONTRATOTextBox.Text)
                FrmAgendaRapida.Show()
                'ElseIf contador > 0 Then
                '    Me.Impresion_Contrato()
            End If
        End If
    End Sub

    Private Sub CONSULTARCLIENTEBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)


        Me.asiganacalle()
        Me.asignaciudad()
        Me.asignacolonia()
        If OpcionCli = "N" Then
            If Len(Trim(Me.NOMBRETextBox.Text)) = 0 Then
                MsgBox("Se Requiere el Nombre ", MsgBoxStyle.Information)
                Exit Sub
            End If
            If IsNumeric(Me.Clv_CalleTextBox.Text) = False Then
                MsgBox("Seleccione la Calle", MsgBoxStyle.Information)
                Exit Sub
            End If
            If Len(Trim(Me.ENTRECALLESTextBox.Text)) = 0 Then
                MsgBox("Se Requiere las Entre Calles ", MsgBoxStyle.Information)
                Exit Sub
            End If
            If Len(Trim(Me.NUMEROTextBox.Text)) = 0 Then
                MsgBox("Se Requiere el Numero ", MsgBoxStyle.Information)
                Exit Sub
            End If
            If IsNumeric(Me.Clv_ColoniaTextBox.Text) = False Then
                MsgBox("Seleccione la Colonia", MsgBoxStyle.Information)
                Exit Sub
            End If
            If IsNumeric(Me.Clv_CiudadTextBox.Text) = False Then
                MsgBox("Seleccione la Ciudad", MsgBoxStyle.Information)
                Exit Sub
            End If
        End If
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.CONSULTARCLIENTEBindingSource.EndEdit()
        Me.CONSULTARCLIENTETableAdapter.Connection = CON
        Me.CONSULTARCLIENTETableAdapter.Update(Me.NewSofTvDataSet.CONSULTARCLIENTE)
        CON.Close()

    End Sub

    Private Sub FrmClientes_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'Estas dos Líneas las Puso Eric. Actualizan Las fechas.
        Dim CON As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand
        Dim Locerror_dir As Integer = 0

        

        '--No se Que hace
        eEntraUM = True
        eEntraUMB = False

        If GloCmdBanco = 1 Then
            GloCmdBanco = 0
            Me.buscarelaccion()
        End If
        If GloOpPermiso = 0 And GloPermisoCortesia = 1 Then
            Me.ComboBox7.Enabled = True
            Me.ComboBox7.Focus()
            'ElseIf GloOpPermiso = 1 And GloPermisoCortesia = 1 Then
            '    Me.CortesiaCheckBox.Enabled = True
            '    Me.CortesiaCheckBox.Focus()
        ElseIf GloOpPermiso = 2 And GloPermisoCortesia = 1 Then
            Me.CortesiaCheckBox1.Enabled = True
            Me.CortesiaCheckBox1.Focus()
            'ElseIf GloOpPermiso = 3 And GloPermisoCortesia = 1 Then
            '    Me.CortesiaCheckBox2.Enabled = True
            '    Me.CortesiaCheckBox2.Focus()
        Else
            If GloTipoUsuario = 40 Then
                Me.ComboBox7.Enabled = True
                'Me.CortesiaCheckBox.Enabled = True
                Me.CortesiaCheckBox1.Enabled = True
                'Me.CortesiaCheckBox2.Enabled = True
            Else
                Me.ComboBox7.Enabled = False
                'Me.CortesiaCheckBox.Enabled = False
                Me.CortesiaCheckBox1.Enabled = False
                'Me.CortesiaCheckBox2.Enabled = False
            End If

        End If
        If bndcambiocable = True Then
            bndcambiocable = False
            frmInternet2.Hide()
            frmInternet2.Show()
            frmctr.Hide()
            frmctr.Crear_Arbol()
            frmctr.Show()
            If (IdSistema = "LO" Or IdSistema = "YU") And (OpcionCli = "C" Or OpcionCli = "M") Then
                DameComboYRenta(CLng(Me.CONTRATOTextBox.Text))
            End If
        End If

        'If GLOMOVNET > 0 And LocBndAct = False Then
        '    LocBndAct = True
        '    If IdSistema = "LO" And (OpcionCli = "C" Or OpcionCli = "M") Then
        '        DameComboYRenta(CLng(Me.CONTRATOTextBox.Text))
        '    End If
        'End If
        'If GLOMOVNET = 0 Then
        '    LocBndAct = False
        'End If


        '--No se Que hace
        If GloClv_Servicio > 0 And GLOMOVNET = 1 Then
            CON.Open()
            locclv_servicio = GloClv_Servicio
            GloClv_Servicio = 0
            Me.Validate()
            'eCabModPropio Fué reemplazado por True
            Me.CONSULTACLIENTESNETTableAdapter.Connection = CON
            Me.CONSULTACLIENTESNETTableAdapter.Insert(Contrato, "P", GloClv_Cablemodem, 0, False, False, 1, "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", GloTipoCablemodem, "", eCabModPropio, LoContratonet)
            GloContratonet_Nuevo = LoContratonet
            Me.CONSULTACLIENTESNETTableAdapter.Connection = CON
            Me.CONSULTACLIENTESNETTableAdapter.Update(Me.NewSofTvDataSet.CONSULTACLIENTESNET)
            Me.CONSULTACONTNETTableAdapter.Connection = CON
            Me.CONSULTACONTNETTableAdapter.Insert(LoContratonet, locclv_servicio, "C", "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", True, 0, 0, False, "C", "", True, "", 0, 0, "", "", False, LoClv_Unicanet)
            GloClv_UnicaNet_Nuevo = LoClv_Unicanet
            Me.CONSULTACONTNETTableAdapter.Connection = CON
            Me.CONSULTACONTNETTableAdapter.Update(Me.NewSofTvDataSet.CONSULTACONTNET)
            Me.GUARDARRel_ContNet_UsuariosTableAdapter.Connection = CON
            Me.GUARDARRel_ContNet_UsuariosTableAdapter.Fill(Me.DataSetEDGAR.GUARDARRel_ContNet_Usuarios, GloClv_UnicaNet_Nuevo, GloClvUsuario)
            'PEriodo
            Me.DIMEQUEPERIODODECORTETableAdapter.Connection = CON
            Me.DIMEQUEPERIODODECORTETableAdapter.Fill(Me.DataSetLidia.DIMEQUEPERIODODECORTE, Contrato)
            Me.CONSULTARCLIENTETableAdapter.Connection = CON
            Me.CONSULTARCLIENTETableAdapter.Fill(Me.NewSofTvDataSet.CONSULTARCLIENTE, Me.CONTRATOTextBox.Text)
            CON.Close()
            CREAARBOLDIGITAL2()
            'Me.Panel5.Visible = False
            'Me.Panel6.Visible = True
            Me.VerAparatodelClienteTableAdapter.Connection = CON
            Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(LoContratonet, Long))
            Me.CONSULTACLIENTESNETTableAdapter.Connection = CON
            Me.CONSULTACLIENTESNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(LoContratonet, Long))
            Me.CONSULTACONTNETTableAdapter.Connection = CON
            Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(LoClv_Unicanet, Long)))
            Me.CONRel_ContNet_UsuariosTableAdapter.Connection = CON
            Me.CONRel_ContNet_UsuariosTableAdapter.Fill(Me.DataSetEDGAR.CONRel_ContNet_Usuarios, LoClv_Unicanet)
            CON.Close()
            'Me.TreeView1.ExpandAll()


            cmd = New SqlClient.SqlCommand()
            CON.Open()
            With cmd
                .CommandText = "Dame_Mac_CableDeco"
                .Connection = CON
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                '@contratonet bigint,@clv_unicanet bigint, @op int,@Mac varchar(max) output
                Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
                Dim prm1 As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
                Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
                Dim prm3 As New SqlParameter("@Mac", SqlDbType.VarChar, 200)

                prm.Direction = ParameterDirection.Input
                prm1.Direction = ParameterDirection.Input
                prm2.Direction = ParameterDirection.Input
                prm3.Direction = ParameterDirection.Output

                prm.Value = LoContratonet
                prm1.Value = 0
                prm2.Value = 1
                prm3.Value = ""

                .Parameters.Add(prm)
                .Parameters.Add(prm1)
                .Parameters.Add(prm2)
                .Parameters.Add(prm3)

                Dim i As Integer = cmd.ExecuteNonQuery()

                NombreMAC = prm3.Value

            End With
            CON.Close()

            '    'Aqui stoy
            '    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Agrego un Cablemodem Nuevo: " + NombreMAC, " ", " Se Agrego un Cablemodem Nuevo ", LocClv_Ciudad)


            '    '--Dim lnivel As Integer = 0
            '    'Dim etree As System.Windows.Forms.TreeViewEventArgs = Me.TreeView1.Nodes
            '    'For lnivel = 0 To etree.Node.Level
            '    'Next
            GloClv_Servicio = 0
            GloClv_Cablemodem = 0
            GLOMOVNET = 0

        ElseIf GloClv_Servicio > 0 And GLOMOVNET = 2 Then
            CON.Open()
            locclv_servicio = GloClv_Servicio
            GloClv_Servicio = 0
            LoContratonet = GloContratonet
            Me.Validate()
            '    '--Me.CONSULTACLIENTESNETTableAdapter.Insert(Contrato, "P", GloClv_Cablemodem, 0, False, False, 1, "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", 1, "", False, LoContratonet)
            '    '--Me.CONSULTACLIENTESNETTableAdapter.Update(Me.NewSofTvDataSet.CONSULTACLIENTESNET)

            Me.HABILITACABLEMODEMTableAdapter.Connection = CON
            Me.HABILITACABLEMODEMTableAdapter.Fill(Me.NewSofTvDataSet.HABILITACABLEMODEM, New System.Nullable(Of Long)(CType(GloContratonet, Long)))
            Me.CONSULTACONTNETTableAdapter.Connection = CON
            Me.CONSULTACONTNETTableAdapter.Insert(GloContratonet, locclv_servicio, "C", "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", True, 0, 0, False, "C", "", True, "", 0, 0, "", "", False, LoClv_Unicanet)
            Me.CONSULTACONTNETTableAdapter.Connection = CON
            Me.CONSULTACONTNETTableAdapter.Update(Me.NewSofTvDataSet.CONSULTACONTNET)
            Me.GUARDARRel_ContNet_UsuariosTableAdapter.Connection = CON
            Me.GUARDARRel_ContNet_UsuariosTableAdapter.Fill(Me.DataSetEDGAR.GUARDARRel_ContNet_Usuarios, LoClv_Unicanet, GloClvUsuario)
            'PEriodo
            Me.DIMEQUEPERIODODECORTETableAdapter.Connection = CON
            Me.DIMEQUEPERIODODECORTETableAdapter.Fill(Me.DataSetLidia.DIMEQUEPERIODODECORTE, GloContratonet)
            Me.CONSULTARCLIENTETableAdapter.Connection = CON
            Me.CONSULTARCLIENTETableAdapter.Fill(Me.NewSofTvDataSet.CONSULTARCLIENTE, Me.CONTRATOTextBox.Text)
            CON.Close()
            CREAARBOLDIGITAL2()
            '    Me.Panel5.Visible = False
            '    Me.Panel6.Visible = True
            CON.Open()
            Me.VerAparatodelClienteTableAdapter.Connection = CON
            Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(LoContratonet, Long))
            Me.CONSULTACLIENTESNETTableAdapter.Connection = CON
            Me.CONSULTACLIENTESNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(LoContratonet, Long))
            Me.CONSULTACONTNETTableAdapter.Connection = CON
            Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(LoClv_Unicanet, Long)))
            Me.CONRel_ContNet_UsuariosTableAdapter.Connection = CON
            Me.CONRel_ContNet_UsuariosTableAdapter.Fill(Me.DataSetEDGAR.CONRel_ContNet_Usuarios, LoClv_Unicanet)
            CON.Close()
            '    Me.TreeView1.ExpandAll()

            cmd = New SqlClient.SqlCommand()
            CON.Open()
            With cmd
                .CommandText = "Dame_Mac_CableDeco"
                .Connection = CON
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                '@contratonet bigint,@clv_unicanet bigint, @op int,@Mac varchar(max) output
                Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
                Dim prm1 As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
                Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
                Dim prm3 As New SqlParameter("@Mac", SqlDbType.VarChar, 200)

                prm.Direction = ParameterDirection.Input
                prm1.Direction = ParameterDirection.Input
                prm2.Direction = ParameterDirection.Input
                prm3.Direction = ParameterDirection.Output

                prm.Value = LoContratonet
                prm1.Value = 0
                prm2.Value = 1
                prm3.Value = ""

                .Parameters.Add(prm)
                .Parameters.Add(prm1)
                .Parameters.Add(prm2)
                .Parameters.Add(prm3)

                Dim i As Integer = cmd.ExecuteNonQuery()

                NombreMAC = prm3.Value

            End With
            CON.Close()

            '    'Aqui stoy
            '    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Agrego un Cablemodem Nuevo: " + NombreMAC, " ", "Se Agrego un Cablemodem Nuevo", LocClv_Ciudad)


            '    '--Dim lnivel As Integer = 0
            '    'Dim etree As System.Windows.Forms.TreeViewEventArgs = Me.TreeView1.Nodes
            '    'For lnivel = 0 To etree.Node.Level
            '    'Next
            GloClv_Servicio = 0
            GloClv_Cablemodem = 0
            GLOMOVNET = 0

        End If
        If bloqueado = 1 Then
            bloqueado = 0
            NUM = 0
            num2 = 0
            CON.Open()
            Me.BuscaBloqueadoTableAdapter.Connection = CON
            Me.BuscaBloqueadoTableAdapter.Fill(Me.DataSetLidia.BuscaBloqueado, Contrato, NUM, num2)
            CON.Close()
            If NUM = 0 Then
                Me.Button20.Visible = True
                Me.Button20.Text = "Bloqueo de Cliente" 'aqui no ha hecho nada
            ElseIf num2 = 1 Then
                Me.Button20.Visible = True
                eGloContrato = Me.CONTRATOTextBox.Text
                FrmBloqueo.Show()
                Me.Button20.Text = "Cliente Bloqueado" 'aqui es porque bloqueo el cliente 
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "BloqueodeCliente", " ", "Bloqueo de Cliente", LocClv_Ciudad)
                glopar = "M"
            ElseIf num2 = 0 Then
                Me.Button20.Visible = True
                Me.Button20.Text = "Cliente Bloqueado Anteriormente" ' aqui es porque desbloqueo el cliente 
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "DesbloqueodeCliente", " ", "Desbloqueo de Cliente", LocClv_Ciudad)
                glopar = "M"
            End If

        End If

        If eRobo = True Then
            eRobo = False
            'Eric
            eGloContrato = Me.CONTRATOTextBox.Text
            CON.Open()
            Me.ChecaRoboDeSeñalTableAdapter.Connection = CON
            Me.ChecaRoboDeSeñalTableAdapter.Fill(Me.DataSetEric.ChecaRoboDeSeñal, eGloContrato, eRespuesta)
            CON.Close()
            If eRespuesta = 1 Then
                Me.Button21.BackColor = Color.Red
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "RobodeSeñal", " ", "Robo de Señal", LocClv_Ciudad)
            Else
                Me.Button21.BackColor = Me.Button1.BackColor
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "CancelarRobodeSeñal", " ", "Cancelar Robo de Señal", LocClv_Ciudad)
            End If
            '------------------------------------------------------------------------------------------------
        End If


        'Eric-------------------------------------------
        If eGloDescuento = 1 And eGloTipSerDesc = 1 Then
            eGloDescuento = 0
            eGloTipSerDesc = 0
            eTipSer = 1
            eContrato = Me.CONTRATOTextBox.Text
            FrmRelCteDescuento.Show()
        End If

        If eGloDescuento = 1 And eGloTipSerDesc = 2 Then
            eGloDescuento = 0
            eGloTipSerDesc = 0
            eTipSer = 2
            eClv_UnicaNet = GloClvUnicaNet
            FrmRelCteDescuento.Show()
        End If

        If eGloDescuento = 1 And (eGloTipSerDesc = 3 Or eGloTipSerDesc = 6) Then
            eTipSer = eGloTipSerDesc
            eGloDescuento = 0
            eGloTipSerDesc = 0
            eClv_UnicaNetDig = loc_Clv_InicaDig
            FrmRelCteDescuento.Show()
        End If

        'If eBndDesc = True And eTipSer = 1 Then
        '    eBndDesc = False
        '    BuscaDescTV()
        'End If

        'If eBndDesc = True And eTipSer = 2 Then
        '    eBndDesc = False
        '    BuscaDescNet()
        'End If

        If eBndDesc = True And (eTipSer = 3 Or eTipSer = 6) Then
            eBndDesc = False
            BuscaDescDig()
        End If

        If LocbndContratacionCombo = True Then
            'muestra_telefonia()
            LocbndContratacionCombo = False
            If Tiene_Tel = 1 Then
                globndTel = True
                Equip_Int = False
                Equip_tel = True
                optTel = 5
                GloBndNum = True
                frmctr.Activar()
                muestra_telefonia()
                'If Tiene_Tv = 1 Then 'Por si tambien tiene Basico
                '    If IsNumeric(Me.CONTRATOTextBox.Text) = True Then
                '        buscaCONCLIENTETV()
                '        damedatostv_2(Me.CONTRATOTextBox.Text)
                '        Me.ToolStripButton2.Enabled = True
                '        Me.ToolStripButton3.Enabled = True
                '        Me.Button29.Enabled = True
                '        Me.CONCLIENTETVBindingNavigator.Items(4).Visible = False
                '        Me.Panel3.Enabled = True
                '        Me.Panel2.Enabled = True
                '        Me.TVCONPAGONumericUpDown.Enabled = True
                '        Me.TVSINPAGONumericUpDown.Enabled = True
                '        FrmTvAdicionales.Show()
                '    End If
                'End If
                If Me.Button8.Visible = True Then
                    If Me.Button8.Enabled = False Then Me.Button8.Enabled = True
                    If Me.Button8.Text = "&Telefonia" Then
                        Me.Button8.Enabled = False
                    End If
                End If
                If Me.Button7.Visible = True Then
                    If Me.Button7.Enabled = False Then Me.Button7.Enabled = True
                    If Me.Button7.Text = "&Telefonia" Then
                        Me.Button7.Enabled = False
                    End If
                End If
                If Me.Button11.Visible = True Then
                    If Me.Button11.Enabled = False Then Me.Button11.Enabled = True
                    If Me.Button11.Text = "&Telefonia" Then
                        Me.Button11.Enabled = False
                    End If
                End If
                If Me.Button28.Visible = True Then
                    If Me.Button28.Enabled = False Then Me.Button28.Enabled = True
                    If Me.Button28.Text = "&Telefonia" Then
                        Me.Button28.Enabled = False
                    End If
                End If

            ElseIf Tiene_Tv = 1 Then

                'If IsNumeric(Me.CONTRATOTextBox.Text) = True Then
                '    buscaCONCLIENTETV()
                '    damedatostv_2(Me.CONTRATOTextBox.Text)
                '    Me.ToolStripButton2.Enabled = True
                '    Me.ToolStripButton3.Enabled = True
                '    Me.Button29.Enabled = True
                '    Me.CONCLIENTETVBindingNavigator.Items(4).Visible = False
                '    Me.Panel3.Enabled = True
                '    Me.Panel2.Enabled = True
                '    Me.TVCONPAGONumericUpDown.Enabled = True
                '    Me.TVSINPAGONumericUpDown.Enabled = True
                'End If

                If Me.Button8.Visible = True Then
                    If Me.Button8.Enabled = False Then Me.Button8.Enabled = True
                    If Me.Button8.Text = "&Televisión" Then
                        Me.Button8.Enabled = False
                    End If
                End If
                If Me.Button7.Visible = True Then
                    If Me.Button7.Enabled = False Then Me.Button7.Enabled = True
                    If Me.Button7.Text = "&Televisión" Then
                        Me.Button7.Enabled = False
                    End If
                End If
                If Me.Button11.Visible = True Then
                    If Me.Button11.Enabled = False Then Me.Button11.Enabled = True
                    If Me.Button11.Text = "&Televisión" Then
                        Me.Button11.Enabled = False
                    End If
                End If
                If Me.Button28.Visible = True Then
                    If Me.Button28.Enabled = False Then Me.Button28.Enabled = True
                    If Me.Button28.Text = "&Televisión" Then
                        Me.Button28.Enabled = False
                    End If
                End If
            End If

            If Tiene_Dig = 1 Then
                MuestraServicios_digital(eClv_TipSer, lOC_CONTRATONETDIG)
            End If

            If (IdSistema = "LO" Or IdSistema = "YU") And (OpcionCli = "C" Or OpcionCli = "M") Then
                DameComboYRenta(CLng(Me.CONTRATOTextBox.Text))
            End If






            'actualizo los forms de abajo para que c ba lo generado
        End If


        'Valida nombre del cliente--------------------------------------------------------------------------------------------
        If OpcionCli = "N" And eBndEntraNombre = True Then

            eBndEntraNombre = False


            eResValida = 0
            ValidaNombreUsuarioSeparado(Me.TextBox27.Text, Me.TextBox4.Text, Me.TextBox7.Text)
            If eResValida = 1 Then
                'Checamos si es que el Usuario no ha autorizado ya el guardar este Contrato
                If GloTipoUsuario = 40 Then
                    Locbndguardar = False
                    MsgBox(eMsgValida, MsgBoxStyle.Exclamation)
                    eNombre = ""
                    eNombre = Me.TextBox27.Text
                    eApellidoPaterno = Me.TextBox4.Text
                    eApellidoMaterno = Me.TextBox7.Text

                    Locbndguardar = False
                    eBndEntraDire = False
                    FrmMuestraContrato.ShowDialog()
                    Exit Sub
                ElseIf (FrmAutorizacion.NombreAutorizado = False) Then
                    Locbndguardar = False
                    MsgBox(eMsgValida, MsgBoxStyle.Exclamation)
                    eNombre = ""
                    eNombre = Me.TextBox27.Text
                    eApellidoPaterno = Me.TextBox4.Text
                    eApellidoMaterno = Me.TextBox7.Text

                    Locbndguardar = False
                    eBndEntraDire = False
                    FrmMuestraContrato.ShowDialog()
                    Exit Sub
                End If


            End If
        End If
        '---------------------------------------------------------------------------------------------------------------------

        'Valida dirección del cliente-----------------------------------------------------------------------------------------
        If OpcionCli = "N" And eBndEntraDire = True Then

            eBndEntraDire = False

            If Locclv_session2 = 0 Then
                Me.Dame_clv_session_clientesTableAdapter.Connection = CON
                Me.Dame_clv_session_clientesTableAdapter.Fill(Me.ProcedimientosArnoldo2.Dame_clv_session_clientes, Locclv_session2)
                borra_valida_direccion(Locclv_session2)
            End If
            Me.Valida_Direccion1TableAdapter.Connection = CON
            Me.Valida_Direccion1TableAdapter.Fill(Me.ProcedimientosArnoldo2.Valida_Direccion1, Locclv_session2, CLng(Me.COLONIAComboBox.SelectedValue), CLng(Me.CALLEComboBox.SelectedValue), Me.NUMEROTextBox.Text, Locerror_dir)

            If Locerror_dir = 1 And Locbnd2clientes = False Then
                If GloTipoUsuario = 40 Then
                    Locbndguardar = False
                    MsgBox("Esta Direccion Ya Esta Asignada a Otro(s) Cliente(s)", MsgBoxStyle.Information)
                    FrmMuestraDireccion.ShowDialog()
                    Exit Sub
                ElseIf (FrmAutorizacion.DireccionAutorizado = False) Then
                    Locbndguardar = False
                    MsgBox("Esta Direccion Ya Esta Asignada a Otro(s) Cliente(s)", MsgBoxStyle.Information)
                    FrmMuestraDireccion.ShowDialog()
                    Exit Sub
                End If
            End If

        End If
        '-----------------------------------------------------------------------------------------------------------------------------

        If Locbndguardar = True Then
            Locbndguardar = False
            CON.Open()
            Me.Validate()
            Me.CONSULTARCLIENTEBindingSource.EndEdit()
            Me.CONSULTARCLIENTETableAdapter.Connection = CON
            Me.CONSULTARCLIENTETableAdapter.Update(Me.NewSofTvDataSet.CONSULTARCLIENTE)
            CON.Close()
            'If Me.SoloInternetCheckBox.Checked = True Then
            '    Me.SoloInternetCheckBox.Enabled = False
            'End If
            GUARDARRel_Clientes_TiposClientesGuarda()
            Guarda_No_Int(Me.CONTRATOTextBox.Text, Me.TxtNumeroInt.Text)
            'JUANJO GUARDA TIPO Y SUB TIPO DE CLIENTE
            ActualizaDatosTipoCliente(Me.CONTRATOTextBox.Text, Me.TipoClienteCombo.SelectedValue, Me.TipoSubClienteCombo.SelectedValue, Me.GeneroCombo.SelectedValue, Me.Referencia1Text.Text, Me.Referencia2Text.Text)
            'FIN JUANJO GUARDA TIPO Y SUB TIPO DE CLIENTE
            'If IdSistema <> "AG" And IdSistema <> "SA" Then
            'Apellidos(2)
            GuardaClientesApellidos()
            'End If

            'JUANJO CLIENTES PENSIONADOS
            If Me.ComboBox7.SelectedValue = 6 Or Me.ComboBox7.Text = "INSEN" Then
                NueClienteInsen(Me.CONTRATOTextBox.Text, Me.ComboBox7.SelectedValue, Me.NoInsenTextBox.Text, 1)
            Else
                NueClienteInsen(Me.CONTRATOTextBox.Text, Me.ComboBox7.SelectedValue, Me.NoInsenTextBox.Text, 2)
            End If
            'FIN JUANJO CLIENTES PENSIONADOS

            'GuadarDatosAdicionales-----------------------------------------------------------------------------
            NueRelClientesDatosAdicionales(CONTRATOTextBox.Text, cbCasaPropia.Checked, tbTelefonoTrabajo.Text, tbIFE.Text)
            '---------------------------------------------------------------------------------------------------

            'JUANJO ACTUALIZA SECTOR DEL CLIENTE
            ACTUALIZASECTORCLIENTE(Me.CONTRATOTextBox.Text, Me.cmbSector.SelectedValue)
            'JUANJO ACTUALIZA SECTOR DEL CLIENTE FIN

            'SE ACTUALIZA EL TIPO DE INSTALACION DEL CLIENTE (INICIO)
            guardaRelacionTipoInstalacion(CLng(Me.CONTRATOTextBox.Text), Me.cmbTipoInstalacion.SelectedValue)
            'SE ACTUALIZA EL TIPO DE INSTALACION DEL CLIENTE (FIN)

            MsgBox("Se ha Guardado con Exíto", MsgBoxStyle.Information)
            guardabitacora()
            'GuardaCuenta()
            BuscaCuenta()
            'Checa_si_quiere_combos()
            eGuardarCliente = True
            'If IdSistema <> "AG" And IdSistema <> "SA" Then
            '    'Apellidos(2)
            '    GuardaClientesApellidos()
            'End If
        End If
        If Locbndvalcliente = True Then
            Locbndvalcliente = False
            dame_datos()
            CON.Open()
            Me.Valida_servicioTvTableAdapter.Connection = CON
            Me.Valida_servicioTvTableAdapter.Fill(Me.ProcedimientosArnoldo2.Valida_servicioTv, Contrato, validacion)
            CON.Close()
            'If validacion > 0 Then
            '    damedatostv(Contrato)
            'End If
            'colorea(Me, Me.Name)
        End If
        '-------------------------------------------------
        
    End Sub


    Private Sub Activa_controles()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim cont, contv, cont2 As Integer
        Me.Button12.Enabled = True
        Me.Panel7.Enabled = True
        'CONCLIENTETVBindingNavigator.Enabled = True
        If IsNumeric(Me.ValidaTextBox.Text) = False Then Me.ValidaTextBox.Text = 0
        If IsNumeric(Me.ValidaTextBox1.Text) = False Then Me.ValidaTextBox1.Text = 0
        If Me.ValidaTextBox.Text > 0 Then
            Me.Button4.Enabled = True
        Else
            Me.Button4.Enabled = False
        End If
        If Me.ValidaTextBox1.Text > 0 Then
            Me.Button6.Enabled = True
        Else
            Me.Button6.Enabled = False
        End If
        Me.Button1.Enabled = True
        Me.Button3.Enabled = True
        Me.Button2.Enabled = True

        'Me.Panel4.Enabled = True
        'If Me.Button28.Visible = True And IdSistema = "LO" Then
        '    Me.Button8.Enabled = False
        '    Me.Button28.Enabled = True
        'Else
        '    Me.Button8.Enabled = True
        '    If IdSistema = "YU" Then
        '        Me.Button8.Enabled = False
        '        Me.Button7.Enabled = True
        '        Me.Button28.Enabled = True
        '    End If

        'End If
        ''Prueba_Edgar_13/12/2008
        Me.Valida_facturasTableAdapter.Connection = CON
        Me.Valida_facturasTableAdapter.Fill(Me.DataSetarnoldo.valida_facturas, Me.CONTRATOTextBox.Text, cont2)
        If cont2 > 0 Then
            cont2 = 0
            Me.Button1.Enabled = True
        Else
            Me.Button1.Enabled = False
        End If

        If Me.Button8.Text = "&Tv Digital" Or Me.Button8.Text = "&Premium" Then
            Me.CONTARCLIENTESTableAdapter.Connection = CON
            Me.CONTARCLIENTESTableAdapter.Fill(DataSetLidia.CONTARCLIENTES, lOC_CONTRATONETDIG, 1, cont)
            Me.CONTARCLIENTESTableAdapter.Connection = CON
            Me.CONTARCLIENTESTableAdapter.Fill(Me.DataSetLidia.CONTARCLIENTES, Contrato, 2, contv)
            'If cont > 0 Or contv > 0 Then
            '    Me.Button7.Enabled = True
            'End If
        ElseIf Me.Button8.Text = "&Television" Then
            Me.CONTARCLIENTESTableAdapter.Connection = CON
            Me.CONTARCLIENTESTableAdapter.Fill(DataSetLidia.CONTARCLIENTES, lOC_CONTRATONETDIG, 1, cont)
            Me.CONTARCLIENTESTableAdapter.Connection = CON
            Me.CONTARCLIENTESTableAdapter.Fill(Me.DataSetLidia.CONTARCLIENTES, Contrato, 3, contv)
            'If cont > 0 Or contv > 0 Then
            '    Me.Button11.Enabled = True
            '    If IdSistema = "SA" Or IdSistema = "VA" Then
            '        Me.Button8.Enabled = True
            '    End If
            'End If

        End If
        Me.Button8.Enabled = False
        If Me.Button7.Visible = True Then Me.Button7.Enabled = True
        If Me.Button11.Visible = True Then Me.Button11.Enabled = True
        If Me.Button28.Visible = True Then Me.Button28.Enabled = True

        ' Me.Button11.Enabled = False
        CON.Close()
    End Sub

    Private Sub Desactiva_controles()
        'Me.Panel4.Enabled = False
        Me.Button12.Enabled = False
        'Me.CONCLIENTETVBindingNavigator.Enabled = False
        Me.Panel7.Enabled = False
        Me.Button7.Enabled = False
        Me.Button8.Enabled = False
        Me.Button11.Enabled = False
        Me.Button28.Enabled = False
    End Sub


    Private Sub FrmClientes_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim CON As New SqlConnection(MiConexion)
        Dim CMd As New SqlClient.SqlCommand

        CON.Open()
        GloBnd = True
        If GloGuardarNet = True Then
            Me.BorraNetPor_NoGRaboTableAdapter.Connection = CON
            Me.BorraNetPor_NoGRaboTableAdapter.Fill(Me.NewSofTvDataSet.BorraNetPor_NoGRabo, New System.Nullable(Of Long)(CType(GloContratonet_Nuevo, Long)), New System.Nullable(Of Long)(CType(0, Long)))
            GloContratonet_Nuevo = 0
            GloGuardarNet = False
        End If
        If GloGuardarDig = True Then
            Me.BorraDigPor_NoGRaboTableAdapter.Connection = CON
            Me.BorraDigPor_NoGRaboTableAdapter.Fill(Me.NewSofTvDataSet.BorraDigPor_NoGRabo, New System.Nullable(Of Long)(CType(GloContratoDig_Nuevo, Long)), New System.Nullable(Of Long)(CType(0, Long)))
            GloGuardarDig = False
        End If
        CON.Close()
        If IsNumeric(Me.CONTRATOTextBox.Text) = True Then
            Genera_Ordenes_Logic(CInt(Me.CONTRATOTextBox.Text))
            Agenda_Clientes()
        End If
        'If IdSistema = "VA" And (OpcionCli = "N" Or OpcionCli = "M") And IsNumeric(Me.CONTRATOTextBox.Text) = True Then
        '    CON.Open()
        '    With CMd
        '        .CommandText = "Determina_Edo_Cuenta"
        '        .CommandTimeout = 0
        '        .CommandType = CommandType.StoredProcedure
        '        .Connection = CON
        '        Dim Prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
        '        Prm.Direction = ParameterDirection.Input
        '        Prm.Value = Me.CONTRATOTextBox.Text
        '        .Parameters.Add(Prm)
        '        Dim i As Integer = CMd.ExecuteNonQuery
        '    End With
        '    CON.Close()
        'End If
        frmctr.WindowState = FormWindowState.Minimized
        frmInternet2.WindowState = FormWindowState.Minimized

        FrmAutorizacion.IFEAutorizado = False
        FrmAutorizacion.NombreAutorizado = False
        FrmAutorizacion.DireccionAutorizado = False

    End Sub

    Private Sub GuardaModificame_cablemodem()
        If BanderaCambio = 1 Then
            BanderaCambio = 0
            Dim CON2 As New SqlConnection(MiConexion)
            Dim CMd As New SqlClient.SqlCommand
            CON2.Open()
            With CMd
                .CommandText = "Modificame_Cablemodem"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = CON2
                Dim Prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                Prm.Direction = ParameterDirection.Input
                Prm.Value = Me.CONTRATOTextBox.Text
                .Parameters.Add(Prm)

                Prm = New SqlParameter("@ContratoNet", SqlDbType.BigInt)
                Prm.Direction = ParameterDirection.Input
                Prm.Value = LocGloContratoNet
                .Parameters.Add(Prm)

                Prm = New SqlParameter("@Mac", SqlDbType.VarChar, 50)
                Prm.Direction = ParameterDirection.Input
                Prm.Value = Me.Label41.Text
                .Parameters.Add(Prm)

                Prm = New SqlParameter("@Tipo_Aparato", SqlDbType.Int)
                Prm.Direction = ParameterDirection.Input
                Prm.Value = 1
                .Parameters.Add(Prm)

                Prm = New SqlParameter("@Respuesta", SqlDbType.VarChar, 250)
                Prm.Direction = ParameterDirection.Input
                Prm.Value = ""
                .Parameters.Add(Prm)

                Dim i As Integer = CMd.ExecuteNonQuery
            End With
            CON2.Close()
        End If
    End Sub


    'Private Sub GuardaModificame_cablemodem_Internet()
    '    If BanderaCambio = 1 Then
    '        BanderaCambio = 0
    '        Dim CON2 As New SqlConnection(MiConexion)
    '        Dim CMd As New SqlClient.SqlCommand
    '        CON2.Open()
    '        With CMd
    '            .CommandText = "Modificame_Cablemodem"
    '            .CommandTimeout = 0
    '            .CommandType = CommandType.StoredProcedure
    '            .Connection = CON2
    '            Dim Prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
    '            Prm.Direction = ParameterDirection.Input
    '            Prm.Value = Me.CONTRATOTextBox.Text
    '            .Parameters.Add(Prm)

    '            Prm = New SqlParameter("@ContratoNet", SqlDbType.BigInt)
    '            Prm.Direction = ParameterDirection.Input
    '            Prm.Value = LocGloContratoNet
    '            .Parameters.Add(Prm)

    '            Prm = New SqlParameter("@Mac", SqlDbType.VarChar, 50)
    '            Prm.Direction = ParameterDirection.Input
    '            Prm.Value = Me.MACCABLEMODEMLabel1.Text
    '            .Parameters.Add(Prm)

    '            Prm = New SqlParameter("@Tipo_Aparato", SqlDbType.Int)
    '            Prm.Direction = ParameterDirection.Input
    '            Prm.Value = 0
    '            .Parameters.Add(Prm)

    '            Prm = New SqlParameter("@Respuesta", SqlDbType.VarChar, 250)
    '            Prm.Direction = ParameterDirection.Input
    '            Prm.Value = ""
    '            .Parameters.Add(Prm)

    '            Dim i As Integer = CMd.ExecuteNonQuery
    '        End With
    '        CON2.Close()
    '    End If
    'End Sub

    'Private Sub HABILITA_CTRS()
    '    ''PROCEDIMIENTO TIPOS DE SERVICIOS 
    '    Dim CONq As New SqlConnection(MiConexion)
    '    CONq.Open()
    '    Dim HAB_TV, cont, conttv As Integer
    '    Dim HAB_TVDIG As Integer
    '    Dim HAB_INTERNET As Integer
    '    Me.DAMESTATUSHABTableAdapter.Connection = CONq
    '    Me.DAMESTATUSHABTableAdapter.Fill(Me.DataSetLidia.DAMESTATUSHAB, 1, HAB_TV)
    '    Me.DAMESTATUSHABTableAdapter.Connection = CONq
    '    Me.DAMESTATUSHABTableAdapter.Fill(Me.DataSetLidia.DAMESTATUSHAB, 2, HAB_INTERNET)
    '    Me.DAMESTATUSHABTableAdapter.Connection = CONq
    '    Me.DAMESTATUSHABTableAdapter.Fill(Me.DataSetLidia.DAMESTATUSHAB, 3, HAB_TVDIG)
    '    CONq.Close()

    '    If HAB_TV = 0 And Me.SoloInternetCheckBox.Checked = False Then
    '        Me.Button8.Visible = True
    '        Me.Button7.Visible = False
    '        Me.Button11.Visible = False
    '        Me.Button8.Text = "&Televisión"
    '        Me.Panel2.Visible = True
    '        Me.Panel7.Visible = False
    '        Me.Panel4.Visible = False
    '        Me.SoloInternetCheckBox.Enabled = False
    '    ElseIf HAB_TVDIG = 0 And Me.SoloInternetCheckBox.Checked = False Then
    '        Me.Button8.Visible = True
    '        Me.Button7.Visible = False
    '        Me.Button11.Visible = False
    '        If IdSistema = "SA" Or IdSistema = "VA" Then
    '            Me.Button8.Text = "&Premium"
    '        Else
    '            Me.Button8.Text = "&Tv Digital"
    '        End If
    '        Me.Panel7.Visible = True
    '        Me.Panel4.Visible = False
    '        Me.SoloInternetCheckBox.Enabled = False
    '        If IsNumeric(Me.CONTRATOTextBox.Text) = False Then
    '            Contrato = 0
    '        Else
    '            Contrato = Me.CONTRATOTextBox.Text
    '        End If
    '        frmctr.MdiParent = Me
    '        frmInternet2.MdiParent = Me
    '        'frmInternet2.Show()
    '        'frmctr.Show()
    '        frmInternet2.Hide()

    '        frmctr.Hide()
    '    ElseIf HAB_INTERNET = 0 And (Me.Button8.Text <> "&Tv Digital" And Me.Button8.Text <> "&Premium") Then
    '        Me.Button8.Visible = True
    '        Me.Button8.Text = "&Internet"
    '        Me.Button7.Visible = False
    '        Me.Panel2.Visible = False
    '        'Me.Panel4.Visible = True
    '        Me.SplitContainer1.Enabled = True
    '        Me.Button11.Visible = False
    '        Me.SoloInternetCheckBox.Enabled = False
    '        If IsNumeric(Me.CONTRATOTextBox.Text) = False Then
    '            Contrato = 0
    '        Else
    '            Contrato = Me.CONTRATOTextBox.Text
    '        End If
    '        frmctr.MdiParent = Me
    '        frmInternet2.MdiParent = Me
    '        'frmInternet2.Show()
    '        'frmctr.Show()
    '        frmInternet2.Hide()

    '        frmctr.Hide()
    '        'frmctr.TreeView1.ExpandAll()
    '    ElseIf HAB_TVDIG = 0 And Me.Button8.Text = "&Televisión" Then
    '        Me.Button7.Visible = True
    '        Me.Button11.Visible = False
    '        If IdSistema = "SA" Or IdSistema = "VA" Then
    '            Me.Button7.Text = "&Premium"
    '        Else
    '            Me.Button7.Text = "&Tv Digital"
    '        End If
    '        If IdSistema = "SA" Or IdSistema = "VA" Then
    '            If BndMini = 0 Then Button8.Enabled = True
    '            'Button11.Enabled = True
    '            If OpcionCli <> "N" Then
    '                If BndMini = 0 Then Me.Button7.Enabled = True
    '                'Else
    '                Button11.Enabled = True
    '            End If
    '        Else
    '            Me.Button7.Enabled = True
    '        End If
    '        If IsNumeric(Me.CONTRATOTextBox.Text) = False Then
    '            Contrato = 0
    '        Else
    '            Contrato = Me.CONTRATOTextBox.Text
    '        End If
    '        frmctr.MdiParent = Me
    '        frmInternet2.MdiParent = Me
    '        frmInternet2.Show()
    '        frmctr.Show()
    '        frmInternet2.Hide()
    '        frmctr.Hide()
    '    ElseIf HAB_INTERNET = 0 And (Me.Button8.Text = "&Tv Digital" Or Me.Button8.Text = "&Televisión" Or Me.Button8.Text = "&Premium") Then
    '        Me.Button7.Visible = True
    '        Me.Button11.Visible = False
    '        Me.Button7.Text = "&Internet"
    '        Dim CONx As New SqlConnection(MiConexion)
    '        CONx.Open()
    '        Me.CONTARCLIENTESTableAdapter.Connection = CONx
    '        Me.CONTARCLIENTESTableAdapter.Fill(DataSetLidia.CONTARCLIENTES, Contrato, 3, cont)
    '        Me.CONTARCLIENTESTableAdapter.Connection = CONx
    '        Me.CONTARCLIENTESTableAdapter.Fill(Me.DataSetLidia.CONTARCLIENTES, Contrato, 2, conttv)
    '        CONx.Close()
    '        If cont > 0 Or conttv > 0 Then
    '            Me.Button7.Enabled = True
    '        End If
    '        Me.SoloInternetCheckBox.Enabled = True
    '        If IsNumeric(Me.CONTRATOTextBox.Text) = False Then
    '            Contrato = 0
    '        Else
    '            Contrato = Me.CONTRATOTextBox.Text
    '        End If
    '        frmctr.MdiParent = Me
    '        frmInternet2.MdiParent = Me
    '        frmInternet2.Show()
    '        frmctr.Show()
    '        frmInternet2.Hide()
    '        frmctr.Hide()
    '    ElseIf HAB_INTERNET = 0 And (Me.Button7.Text = "&Tv Digital" Or Me.Button7.Text = "&Premium") Then
    '        Me.Button11.Visible = True
    '        Me.Button11.Text = "&Internet"
    '        Me.SoloInternetCheckBox.Enabled = True
    '    ElseIf HAB_INTERNET = 0 And Me.Button8.Text = "&Internet" Then
    '        Me.SoloInternetCheckBox.Enabled = False
    '        Me.Panel2.Hide()
    '        Me.Panel4.Hide()
    '        Me.Panel7.Hide()
    '        If IsNumeric(Me.CONTRATOTextBox.Text) = False Then
    '            Contrato = 0
    '        Else
    '            Contrato = Me.CONTRATOTextBox.Text
    '        End If
    '        frmctr.MdiParent = Me
    '        frmInternet2.MdiParent = Me
    '        frmInternet2.Show()
    '        frmctr.Show()
    '        frmctr.TreeView1.ExpandAll()
    '    End If
    'End Sub



    Private Sub HABILITA_CTRS()
        Dim cont, conttv As Integer
        Dim ContadorBotones As Integer = 4
        Dim Contadorcuantos As Integer = 0
        Dim Loc_HAB_TV As Integer = 1
        Dim Loc_HAB_TVDIG As Integer = 1
        Dim Loc_HAB_INTERNET As Integer = 1
        Dim Loc_HAB_Telefonia As Integer = 1
        Dim Loc_HAB_TVDIGCable As Integer = 1

        Loc_HAB_TV = HAB_TV
        Loc_HAB_TVDIG = HAB_TVDIG
        Loc_HAB_INTERNET = HAB_INTERNET
        Loc_HAB_Telefonia = HAB_Telefonia
        Loc_HAB_TVDIGCable = HAB_TVDIGCable

        'Vamos a inicializar

        Me.Button8.Visible = False
        Me.Button7.Visible = False
        Me.Button11.Visible = False
        Me.Button28.Visible = False

        If Loc_HAB_TV = 0 Then Contadorcuantos = Contadorcuantos + 1
        If Loc_HAB_TVDIG = 0 Then Contadorcuantos = Contadorcuantos + 1
        If Loc_HAB_INTERNET = 0 Then Contadorcuantos = Contadorcuantos + 1
        If Loc_HAB_Telefonia = 0 Then Contadorcuantos = Contadorcuantos + 1
        If Loc_HAB_TVDIGCable = 0 Then Contadorcuantos = Contadorcuantos + 1

        'cuales Servicios es Principal
        If IsNumeric(Me.CONTRATOTextBox.Text) = False Then
            Contrato = 0
        Else
            Contrato = Me.CONTRATOTextBox.Text
        End If


        If Me.SoloInternetCheckBox.Checked = False Or Me.SoloInternetCheckBox.Checked = True Then
            'Clienes que NO son de Solointernet
            If Me.SoloInternetCheckBox.Checked = False Then
                Select Case Servicio_Principal
                    Case 1
                        BndEsInternet = False
                        Me.Panel7.Visible = False
                        Me.Button8.Visible = True
                        Me.Button8.Enabled = False
                        Me.Button8.Text = "&Televisión"
                        'Me.SoloInternetCheckBox.Enabled = False
                        frmctr.Hide()
                        frmInternet2.Hide()
                        frmctr.Hide()
                        frmInternet2.Hide()
                        Loc_HAB_TV = 2
                        Contadorcuantos = Contadorcuantos - 1
                    Case 2
                        Me.Button8.Visible = True
                        Me.Button8.Enabled = False
                        Me.Button8.Text = "&Internet"
                        Me.Button11.Visible = False
                        'Me.SoloInternetCheckBox.Enabled = False
                        BndEsInternet = True
                        frmctr.MdiParent = Me
                        frmInternet2.MdiParent = Me
                        frmctr.WindowState = FormWindowState.Normal
                        frmInternet2.Show()
                        frmctr.Show()
                        frmctr.Boton_Internet()
                        frmctr.TreeView1.ExpandAll()
                        Loc_HAB_INTERNET = 2
                        Contadorcuantos = Contadorcuantos - 1
                    Case 3
                        eClv_TipSer = 3
                        Me.Button8.Visible = True
                        Me.Button8.Enabled = False
                        Me.Button8.Text = "&Tv Digital MMDS"
                        Me.Panel12.Visible = False
                        Me.Panel7.Visible = True
                        'Me.SoloInternetCheckBox.Enabled = False
                        If IsNumeric(Me.CONTRATOTextBox.Text) = False Then
                            Contrato = 0
                        Else
                            Contrato = Me.CONTRATOTextBox.Text
                        End If
                        CREAARBOLDIGITAL2()
                        frmctr.MdiParent = Me
                        frmInternet2.MdiParent = Me
                        frmInternet2.Hide()
                        Loc_HAB_TVDIG = 2
                        Contadorcuantos = Contadorcuantos - 1
                    Case 6
                        eClv_TipSer = 6
                        Me.Button8.Visible = True
                        Me.Button8.Enabled = False
                        Me.Button8.Text = "Tv Digital &Cable"
                        Me.Panel12.Visible = False
                        Me.Panel7.Visible = True
                        'Me.SoloInternetCheckBox.Enabled = False
                        If IsNumeric(Me.CONTRATOTextBox.Text) = False Then
                            Contrato = 0
                        Else
                            Contrato = Me.CONTRATOTextBox.Text
                        End If
                        CREARARBOLDIGITALCABLE()
                        frmctr.MdiParent = Me
                        frmInternet2.MdiParent = Me
                        frmInternet2.Hide()
                        Loc_HAB_TVDIGCable = 2
                        Contadorcuantos = Contadorcuantos - 1
                    Case 5
                        Me.Button8.Visible = True
                        Me.Button8.Enabled = False
                        Me.Button8.Text = "&Telefonia"
                        muestra_telefonia()
                        Loc_HAB_Telefonia = 2
                        Contadorcuantos = Contadorcuantos - 1
                End Select
            Else
                '------------------------------------------------------------------------------------------Solo Internet
                Me.Button8.Visible = True
                Me.Button8.Enabled = False
                Me.Button8.Text = "&Internet"
                Me.Button11.Visible = False
                'Me.SoloInternetCheckBox.Enabled = False
                BndEsInternet = True
                frmctr.MdiParent = Me
                frmInternet2.MdiParent = Me
                frmctr.WindowState = FormWindowState.Normal
                frmInternet2.Show()
                frmctr.Show()
                frmctr.Boton_Internet()
                frmctr.TreeView1.ExpandAll()
                Loc_HAB_INTERNET = 2
                Contadorcuantos = Contadorcuantos - 1
                If (IdSistema <> "LO" And IdSistema <> "YU") Then Exit Sub
            End If
            For I As Integer = 1 To Contadorcuantos
                If Loc_HAB_TV = 0 Then
                    If I = 1 Then
                        Me.Button7.Visible = True
                        Me.Button7.Enabled = True
                        Me.Button7.Text = "&Televisión"
                    ElseIf I = 2 Then
                        Me.Button11.Visible = True
                        Me.Button11.Enabled = True
                        Me.Button11.Text = "&Televisión"
                    ElseIf I = 3 Then
                        Me.Button28.Visible = True
                        Me.Button28.Enabled = True
                        Me.Button28.Text = "&Televisión"
                    End If
                    Loc_HAB_TV = 2
                ElseIf Loc_HAB_INTERNET = 0 Then
                    If I = 1 Then
                        Me.Button7.Visible = True
                        Me.Button7.Enabled = True
                        Me.Button7.Text = "&Internet"
                    ElseIf I = 2 Then
                        Me.Button11.Visible = True
                        Me.Button11.Enabled = True
                        Me.Button11.Text = "&Internet"
                    ElseIf I = 3 Then
                        Me.Button28.Visible = True
                        Me.Button28.Enabled = True
                        Me.Button28.Text = "&Internet"
                    End If
                    Loc_HAB_INTERNET = 2

                ElseIf Loc_HAB_TVDIG = 0 Then
                    If I = 1 Then
                        Me.Button7.Visible = True
                        Me.Button7.Enabled = True
                        Me.Button7.Text = "&Tv Digital MMDS"
                    ElseIf I = 2 Then
                        Me.Button11.Visible = True
                        Me.Button11.Enabled = True
                        Me.Button11.Text = "&Tv Digital MMDS"
                    ElseIf I = 3 Then
                        Me.Button28.Visible = True
                        Me.Button28.Enabled = True
                        Me.Button28.Text = "&Tv Digital MMDS"
                    End If
                    Loc_HAB_TVDIG = 2

                ElseIf Loc_HAB_TVDIGCable = 0 Then
                    If I = 1 Then
                        Me.Button7.Visible = True
                        Me.Button7.Enabled = True
                        Me.Button7.Text = "Tv Digital &Cable"
                    ElseIf I = 2 Then
                        Me.Button11.Visible = True
                        Me.Button11.Enabled = True
                        Me.Button11.Text = "Tv Digital &Cable"
                    ElseIf I = 3 Then
                        Me.Button28.Visible = True
                        Me.Button28.Enabled = True
                        Me.Button28.Text = "Tv Digital &Cable"
                    End If
                    Loc_HAB_TVDIGCable = 2
                ElseIf Loc_HAB_Telefonia = 0 Then
                    If I = 1 Then
                        Me.Button7.Visible = True
                        Me.Button7.Enabled = True
                        Me.Button7.Text = "&Telefonia"
                    ElseIf I = 2 Then
                        Me.Button11.Visible = True
                        Me.Button11.Enabled = True
                        Me.Button11.Text = "&Telefonia"
                    ElseIf I = 3 Then
                        Me.Button28.Visible = True
                        Me.Button28.Enabled = True
                        Me.Button28.Text = "&Telefonia"
                    End If
                    Loc_HAB_Telefonia = 2
                End If
            Next
        End If
    End Sub


    Private Sub bloqueado1(ByRef contrato As Integer)
        Dim cone As New SqlClient.SqlConnection(MiConexion)
        cone.Open()
        NUM = 0
        num2 = 0
        Me.BuscaBloqueadoTableAdapter.Connection = cone
        Me.BuscaBloqueadoTableAdapter.Fill(Me.DataSetLidia.BuscaBloqueado, contrato, NUM, num2)
        cone.Close()
        If num2 = 1 Then
            eGloContrato = contrato
            FrmBloqueo.Show()
            ' MsgBox("El Cliente " + Me.ContratoTextBox.Text + " Ha Sido Bloqueado por lo que no se Podrá Llevar a cabo la Orden ", MsgBoxStyle.Exclamation)
            Me.Panel1.Enabled = False
            'Me.Panel3.Enabled = False
            'Me.Panel4.Enabled = False
            'Me.Panel6.Enabled = False
            Me.Panel7.Enabled = False
            Me.Panel8.Enabled = False
        End If
    End Sub
    Private Sub PintarFondo(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs)

        Dim GradientePanel As New LinearGradientBrush(New RectangleF(0, 0, ctlMDI.Width, ctlMDI.Height), Color.WhiteSmoke, Color.WhiteSmoke, LinearGradientMode.Vertical)

        e.Graphics.FillRectangle(GradientePanel, New RectangleF(0, 0, ctlMDI.Width, ctlMDI.Height))

    End Sub
    Private Function UspValidarCliente(ByVal PRMCONTRATO As Long) As DataTable
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, PRMCONTRATO)
            UspValidarCliente = BaseII.ConsultaDT("UspValidarCliente")
        Catch ex As Exception
            Throw ex
        End Try
    End Function



    Private Sub FrmClientes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
       
        eClv_TipSer = 3
        ValidaTipoUsuario(0, GloUsuario, "")

        'MsgBox(Today.ToString)
        Asigna_taps()
        ColoreaFrm(Me)
        Dim ctl As Control
        

        For Each ctl In Me.Controls

            Try
                
                ctlMDI = CType(ctl, MdiClient)

                ' Asignamos el color de fondo
                ctlMDI.BackColor = Color.AntiqueWhite

                'Aquí asignamos el manejador para pintar el fondo con degradados o lo que
                'queramos. Si solo queremos cambiar el color de fondo no hace falta, ni las funciones siguientes tampoco
                AddHandler ctlMDI.Paint, AddressOf PintarFondo

            Catch ex As InvalidCastException

            End Try

        Next



        frmctr = New FrmCtrl_ServiciosCli
        'frmTelefonia = New FrmClientesTel
        frmInternet2 = New FrmInternet

        Try
            De_Internet = False
            'frmctr.MdiParent = Me
            'frmInternet2.MdiParent = Me
            'frmctr.WindowState = FormWindowState.Normal
            'frmInternet2.Show()
            'frmInternet2.Hide()
            'frmctr.Show()
            'frmctr.Hide()
            'Me.Button28.Visible = False
            Me.Button30.Visible = False
            If IdSistema = "AG" Or IdSistema = "VA" Then
                'If IdSistema = "VA" Then
                Me.Panel11.Visible = True
                'End If
                Me.Button29.Visible = False

                frmInternet2.NumericUpDown1.Enabled = False
            ElseIf IdSistema <> "AG" And IdSistema <> "SA" Then
                Me.Panel11.Visible = True
            End If

            If IdSistema = "SA" Then
                Me.Button29.Visible = False
            ElseIf IdSistema = "LO" Or IdSistema = "YU" Then

                Me.Button30.Visible = True
                Me.Panel13.Visible = True
                'Me.Button28.Visible = True
                Me.DESGLOSA_IvaCheckBox.Visible = False
                'Me.SoloInternetCheckBox.Visible = False
                'Me.Panel2.Visible = True
                'Me.SoloInternetCheckBox.Checked = True
            End If



            Dim CON As New SqlConnection(MiConexion)

            'Banderas que se usan para cuando un contrato tiene el Mismo Nombre ó el Mismo Domicilio
            'La bandera apagada indica que si guardará, en caso cotrario si la bandera está prendida no lo hará
            eBndMismoNombre = False
            If Locbnd2clientes = True Then
                Locbnd2clientes = False
            End If
            If bnddir = True Then
                bnddir = False
            End If

            'Llenar variables para la bitacora del sistema
            'dame_datos()
            'ésta Linea la puso Eric, cualquier cosa, ya sabe...
            BndMini = 0
            eEntraUM = True
            eEntraUMB = False
            CON.Open()
            Me.DAMEFECHADELSERVIDOR_2TableAdapter.Connection = CON
            Me.DAMEFECHADELSERVIDOR_2TableAdapter.Fill(Me.DataSetEdgarRev2.DAMEFECHADELSERVIDOR_2)
            'TODO: esta línea de código carga datos en la tabla 'DataSetEDGAR.MuestraPromotoresTv' Puede moverla o quitarla según sea necesario.
            Me.MuestraPromotoresTvTableAdapter.Connection = CON
            Me.MuestraPromotoresTvTableAdapter.Fill(Me.DataSetEDGAR.MuestraPromotoresTv)
            'TODO: esta línea de código carga datos en la tabla 'DataSetEDGAR.MuestraPromotoresNet' Puede moverla o quitarla según sea necesario.
            Me.MuestraPromotoresNetTableAdapter.Connection = CON
            Me.MuestraPromotoresNetTableAdapter.Fill(Me.DataSetEDGAR.MuestraPromotoresNet)
            CON.Close()
            'TODO: esta línea de código carga datos en la tabla 'DataSetLidia.DameFechaHabilitar' Puede moverla o quitarla según sea necesario.
            'Me.DameFechaHabilitarTableAdapter.Fill(Me.DataSetLidia.DameFechaHabilitar)
            eGuardarCliente = False

            Dim CONT, CONTV As Integer
            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MUESTRA_A_DIGITAL' Puede moverla o quitarla según sea necesario.
            CON.Open()

            If IdSistema <> "VA" Then
                Me.ComboBox11.Enabled = False
            ElseIf IdSistema = "VA" Then
                Me.MUESTRATABSTableAdapter.Connection = CON
                Me.MUESTRATABSTableAdapter.Fill(Me.DataSetarnoldo.MUESTRATABS, 0)
                Me.CMBLabel10.Text = "Cajas Digitales Asignadas al Cliente:"
                Me.CMBLabel32.Text = "Seleccione la Caja Digital que le va a Asignar el Paquete"
                Me.CMBLabel53.Text = "Status Caja"
                Me.CMBLabel54.Text = "Datos de la Caja Digital"
                Me.ToolStripButton16.Text = "Agregar Caja Digital"
                Me.ToolStripButton15.Text = "Quitar Caja Digital"
            End If

            Me.MUESTRACatalogoPeriodosCorteTableAdapter.Connection = CON
            Me.MUESTRACatalogoPeriodosCorteTableAdapter.Fill(Me.DataSetEDGAR.MUESTRACatalogoPeriodosCorte, 0)
            Me.MUESTRA_TIPOCLIENTESTableAdapter.Connection = CON
            Me.MUESTRA_TIPOCLIENTESTableAdapter.Fill(Me.DataSetEDGAR.MUESTRA_TIPOCLIENTES, 0)
            Me.MUESTRA_A_DIGITALTableAdapter.Connection = CON
            Me.MUESTRA_A_DIGITALTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRA_A_DIGITAL)
            Pantalla = Me.Name
            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MuestraTipoPromocion' Puede moverla o quitarla según sea necesario.
            'Me.MuestraTipoPromocionTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipoPromocion)
            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MuestraPromotores' Puede moverla o quitarla según sea necesario.
            Me.MuestraPromotoresTableAdapter.Connection = CON
            Me.MuestraPromotoresTableAdapter.Fill(Me.NewSofTvDataSet.MuestraPromotores)
            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.StatusCableModem' Puede moverla o quitarla según sea necesario.
            Me.StatusCableModemTableAdapter.Connection = CON
            Me.StatusCableModemTableAdapter.Fill(Me.NewSofTvDataSet.StatusCableModem)
            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.TipoCablemodem' Puede moverla o quitarla según sea necesario.
            Me.TipoCablemodemTableAdapter.Connection = CON
            Me.TipoCablemodemTableAdapter.Fill(Me.NewSofTvDataSet.TipoCablemodem)
            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerInternet' Puede moverla o quitarla según sea necesario.
            Me.MuestraTipSerInternetTableAdapter.Connection = CON
            Me.MuestraTipSerInternetTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerInternet)
            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.StatusNet' Puede moverla o quitarla según sea necesario.
            Me.StatusNetTableAdapter.Connection = CON
            Me.StatusNetTableAdapter.Fill(Me.NewSofTvDataSet.StatusNet)
            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.TiposAparatos' Puede moverla o quitarla según sea necesario.
            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.StatusBasico' Puede moverla o quitarla según sea necesario.
            Me.StatusBasicoTableAdapter.Connection = CON
            Me.StatusBasicoTableAdapter.Fill(Me.NewSofTvDataSet.StatusBasico)
            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MuestraMotivoCancelacion' Puede moverla o quitarla según sea necesario.
            Me.MuestraMotivoCancelacionTableAdapter.Connection = CON
            Me.MuestraMotivoCancelacionTableAdapter.Fill(Me.NewSofTvDataSet.MuestraMotivoCancelacion)
            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MuestraTiposServicioTv' Puede moverla o quitarla según sea necesario.
            Me.MuestraTiposServicioTvTableAdapter.Connection = CON
            Me.MuestraTiposServicioTvTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTiposServicioTv)
            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MUESTRACALLES' Puede moverla o quitarla según sea necesario.
            'Me.MuestraServiciosTableAdapter.Fill(Me.NewSofTvDataSet.MuestraServicios, 3)

            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.CIUDADES' Puede moverla o quitarla según sea necesario.
            Me.CIUDADESTableAdapter.Connection = CON
            Me.CIUDADESTableAdapter.Fill(Me.NewSofTvDataSet.CIUDADES)
            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.CALLES' Puede moverla o quitarla según sea necesario.
            Me.MUESTRACALLESTableAdapter.Connection = CON
            Me.MUESTRACALLESTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACALLES)
            CON.Close()
            Dim CON2 As New SqlConnection(MiConexion)
            CON2.Open()
            Me.Clv_CalleTextBox.Text = Me.CALLEComboBox.SelectedValue
            Me.DAMECOLONIA_CALLETableAdapter.Connection = CON2
            Me.DAMECOLONIA_CALLETableAdapter.Fill(Me.NewSofTvDataSet.DAMECOLONIA_CALLE, New System.Nullable(Of Integer)(CType(Me.CALLEComboBox.SelectedValue, Integer)))
            CON2.Close()

            'JUANJO TIPOS CLIENTES
            MUESTRATIPOSCLIENTE()
            MUESTRASUBTIPOSCLIENTE(Me.TipoClienteCombo.SelectedValue)
            CONSULTAGENERO()
            'FIN JUANJO TIPOS CLIENTES

            ' Me.Clv_ColoniaTextBox.Text = Me.COLONIAComboBox.SelectedValue
            'Me.Clv_CiudadTextBox.Text = Me.CIUDADComboBox.SelectedValue
            'Me.MOTCANComboBox.Text = ""
            'Me.MOTCANComboBox.SelectedValue = 0
            Me.Bucacontrato(Contrato)
            Me.Panel7.Enabled = True
            'Me.Panel2.Enabled = True
            'Me.Panel4.Enabled = True
            'Me.SplitContainer1.Enabled = True
            Me.SplitContainer2.Enabled = True
            Me.BindingNavigator5.Enabled = True
            Me.BindingNavigator6.Enabled = True
            'Me.Button9.Enabled = True
            Me.HABILITA_CTRS()



            If OpcionCli = "N" Then
                CON.Open()
                'Me.CONSULTARCLIENTETableAdapter.Connection = CON
                Me.SoloInternetCheckBox.Tag = "N"
                Me.CONSULTARCLIENTEBindingSource.AddNew()
                Me.CONSULTARCLIENTEBindingNavigator.Enabled = True
                Me.Panel1.Enabled = True
                Me.SoloInternetCheckBox.Enabled = True
                Me.SoloInternetCheckBox.Checked = False
                Me.SoloInternetCheckBox.Tag = ""
                Me.EshotelCheckBox.Checked = False
                Me.DESGLOSA_IvaCheckBox.Checked = False
                'Me.Button16.Enabled = False
                Me.Button29.Enabled = False
                'Por default Enabled=False
                'If IdSistema = "SA" Then
                'Me.TVSINPAGONumericUpDown.Enabled = True
                'Me.TVCONPAGONumericUpDown.Enabled = True
                'Else
                '    Me.TVSINPAGONumericUpDown.Enabled = False
                '    Me.TVCONPAGONumericUpDown.Enabled = False
                'End If
                Me.Button20.Visible = True
                Me.Button20.Text = "Bloqueo de Cliente"
                glopar = "N"
                'Eric
                Me.Button21.Enabled = False
                CON.Close()
                'En Cuernava todos Son de solo Internet
                'If IdSistema = "LO" Then
                ''  Me.SoloInternetCheckBox.Checked = True
                'End If
                'JUANJO SECTORES Y TAPS
                DAMESECTORORDENES(Me.COLONIAComboBox.SelectedValue)
                DAMETAPYSECTORCLIENTE(Me.CONTRATOTextBox.Text)
                llenaComboTipoInstalacion(2)
                consultaRelacionTipoInstalacion(CLng(Me.CONTRATOTextBox.Text))
            ElseIf OpcionCli = "C" Then
                Me.CONSULTARCLIENTEBindingNavigator.Enabled = False
                Me.Panel1.Enabled = False
                'Me.Panel7.Enabled = False
                'Me.Panel2.Enabled = False
                'Me.Panel4.Enabled = False
                'Me.SplitContainer1.Panel2.Enabled = False
                'Me.SplitContainer1.Panel1.Enabled = True
                Me.SplitContainer2.Panel2.Enabled = False
                Me.SplitContainer2.Panel1.Enabled = True
                'Me.Button9.Enabled = False
                'Me.Button19.Enabled = False
                Me.BindingNavigator5.Enabled = False
                Me.BindingNavigator6.Enabled = False

                'Eric
                If IdSistema = "TO" Or IdSistema = "SA" Or IdSistema = "VA" Then
                    CON.Open()
                    Me.ConRelCtePlacaTableAdapter.Connection = CON
                    Me.ConRelCtePlacaTableAdapter.Fill(Me.DataSetEric.ConRelCtePlaca, Me.CONTRATOTextBox.Text)
                    'Me.Label32.Visible = True
                    'Me.PlacaTextBox.Visible = True
                    Me.Button21.Visible = True
                    CON.Close()
                ElseIf IdSistema = "LO" Or IdSistema = "YU" Then
                    BuscaCuenta()
                End If

                buscarelaccion()

                'JUANJO SECTORES Y TAPS
                DAMESECTORORDENES(Me.COLONIAComboBox.SelectedValue)
                DAMETAPYSECTORCLIENTE(Me.CONTRATOTextBox.Text)
                llenaComboTipoInstalacion(2)
                consultaRelacionTipoInstalacion(CLng(Me.CONTRATOTextBox.Text))
            ElseIf OpcionCli = "M" Then
                CON.Open()
                Me.CONSULTARCLIENTEBindingNavigator.Enabled = True
                Me.CONTARCLIENTESTableAdapter.Connection = CON
                Me.CONTARCLIENTESTableAdapter.Fill(DataSetLidia.CONTARCLIENTES, Contrato, 6, CONT)
                Me.CONTARCLIENTESTableAdapter.Connection = CON
                Me.CONTARCLIENTESTableAdapter.Fill(Me.DataSetLidia.CONTARCLIENTES, Contrato, 7, CONTV)
                CON.Close()
                If Me.ComboBox7.SelectedValue > 0 Then
                    clv_tipo_pagobit = Me.ComboBox7.SelectedValue
                End If
                'Las Siguientes 5 Líneas las puso Eric. Cualquier cosa, sobrees de él!

                'If Me.ComboBox1.Text.Length > 0 Then
                '    Me.Button16.Enabled = True
                'Else
                '    Me.Button16.Enabled = False
                'End If
                CON.Open()
                Me.Valida_SiahiOrdSerTableAdapter.Connection = CON
                Me.Valida_SiahiOrdSerTableAdapter.Fill(Me.NewSofTvDataSet.Valida_SiahiOrdSer, New System.Nullable(Of Long)(CType(Contrato, Long)))
                Me.Valida_SiahiQuejasTableAdapter.Connection = CON
                Me.Valida_SiahiQuejasTableAdapter.Fill(Me.NewSofTvDataSet.Valida_SiahiQuejas, New System.Nullable(Of Long)(CType(Contrato, Long)))
                CON.Close()
                'Eric
                If IdSistema = "TO" Or IdSistema = "SA" Or IdSistema = "VA" Then
                    CON.Open()
                    Me.ConRelCtePlacaTableAdapter.Connection = CON
                    Me.ConRelCtePlacaTableAdapter.Fill(Me.DataSetEric.ConRelCtePlaca, Me.CONTRATOTextBox.Text)
                    'Me.Label32.Visible = True
                    'Me.PlacaTextBox.Visible = True
                    Me.Button21.Visible = True
                    CON.Close()
                ElseIf IdSistema = "LO" Or IdSistema = "YU" Then
                    BuscaCuenta()
                End If
                If GloTipoUsuario = 40 Then
                    'Digital
                    Me.Label41.ReadOnly = True
                    'Me.MACCABLEMODEMLabel1.ReadOnly = False
                    Me.TextBox16.ReadOnly = False
                    Me.TextBox15.ReadOnly = False
                    Me.TextBox14.ReadOnly = False
                    Me.TextBox13.ReadOnly = False
                    Me.TextBox12.ReadOnly = False
                    'Me.TextBox10.ReadOnly = False
                    Me.TextBox3.ReadOnly = False
                    Me.TextBox9.ReadOnly = False
                    Me.ComboBox10.Enabled = True
                    Me.TextBox23.ReadOnly = False
                    Me.TextBox32.ReadOnly = False
                    Me.TextBox33.ReadOnly = False
                    Me.ComboBox13.Enabled = True
                    'Ésta lineas la puso Eric
                    Me.Button17.Enabled = False
                    Me.CheckBox1.Enabled = True
                    Me.CheckBox1.Enabled = True
                    Me.TextBox11.ReadOnly = False
                    Me.TextBox3.ReadOnly = False
                    Me.ComboBox9.Enabled = True
                    '-------------------------
                    'INTERNET
                    frmInternet2.Fecha_solicitudTextBox1.ReadOnly = False
                    frmInternet2.Fecha_suspensionTextBox.ReadOnly = False
                    frmInternet2.Fecha_SuspencionTextBox.ReadOnly = False
                    frmInternet2.Fecha_TraspasoTextBox.ReadOnly = False
                    frmInternet2.Fecha_Fuera_AreaTextBox.ReadOnly = False
                    frmInternet2.Fecha_BajaTextBox1.ReadOnly = False
                    frmInternet2.Fecha_bajaTextBox.ReadOnly = False
                    frmInternet2.Fecha_ActivacionTextBox.ReadOnly = False
                    frmInternet2.Fecha_instalacioTextBox.ReadOnly = False
                    frmInternet2.PrimerMensualidadCheckBox.Enabled = True
                    frmInternet2.CortesiaCheckBox2.Enabled = True
                    'frmTelefonia.CortesiaCheckBox.Enabled = True

                    'Me.
                    'Me.
                    'Me.
                    'Me.
                    'Me.
                    'Me.
                    'Me.
                    'Me.
                    'Me.
                    'Me.

                    'frminternet2.
                    'frminternet2.
                    'frminternet2.
                    'frminternet2.
                    'frminternet2.
                    'frminternet2.
                    'frminternet2.
                    'frminternet2.
                    'frminternet2.
                    'frminternet2.
                    frmInternet2.Ultimo_mesTextBox1.ReadOnly = False
                    frmInternet2.TextBox5.ReadOnly = False
                    frmInternet2.Ultimo_anioTextBox1.ReadOnly = False
                    frmInternet2.ComboBox5.Enabled = True
                    frmInternet2.ComboBox2.Enabled = True
                    frmInternet2.FECHA_ULT_PAGOTextBox1.ReadOnly = False
                    frmInternet2.TextBox5.ReadOnly = False
                    frmInternet2.ComboBox6.Enabled = True
                    frmInternet2.Button18.Enabled = True  'Ésta linea la puso Eric
                    frmInternet2.PrimerMensualidadCheckBox.Enabled = True
                    '------------------------
                    'Tv
                    'Me.ComboBox16.Enabled = True
                    'Me.TextBox2.ReadOnly = False
                    'Me.FECHA_ULT_PAGOTextBox.ReadOnly = False
                    'Me.PRIMERMENSUALIDACheckBox.Enabled = True
                    'Me.ComboBox1.Enabled = True
                    'Me.ULTIMO_MESTextBox.ReadOnly = False
                    'Me.TextBox2.ReadOnly = False
                    'Me.ULTIMO_ANIOTextBox.ReadOnly = False
                    'Me.FECHA_SOLICITUDTextBox.ReadOnly = False
                    'Me.FECHA_INSTTextBox.ReadOnly = False
                    'Me.FECHA_CORTETextBox.ReadOnly = False
                    'Me.FECHACANCOUTAREATextBox.ReadOnly = False
                    'Me.FECHA_CANCELACIOTextBox.ReadOnly = False
                    'Me.MOTCANComboBox.Enabled = True
                    'Me.TipSerTvComboBox.Enabled = True
                    'Éstas Líneas las puso Eric
                    'Me.TVCONPAGONumericUpDown.Enabled = True
                    'Me.TVSINPAGONumericUpDown.Enabled = True
                    'Me.Button16.Enabled = False
                    'Me.CortesiaCheckBox.Enabled = True
                    '--------------------------
                End If

                'If CONT = 0 And CONTV = 0 Then
                '    Me.SoloInternetCheckBox.Enabled = True
                'ElseIf CONT > 0 And CONTV > 0 Then
                '    Me.SoloInternetCheckBox.Enabled = False
                'End If
                'If CONTV = 0 Then
                '    Me.SoloInternetCheckBox.Enabled = True
                'Else
                '    Me.SoloInternetCheckBox.Enabled = False
                'End If
                If NUM = 0 Then
                    Me.Button20.Visible = True
                    Me.Button20.Text = "Bloqueo de Cliente"
                    glopar = "N"
                ElseIf num2 = 1 And NUM <> 0 Then
                    Me.Button20.Visible = True
                    Me.Button20.Text = "Cliente Bloqueado"
                    glopar = "M"
                    Me.Panel1.Enabled = False
                    Me.Panel7.Enabled = False
                    'Me.Panel4.Enabled = False
                    'Me.CONCLIENTETVBindingNavigator.Enabled = False
                    eGloContrato = Contrato
                    bloqueado = 1
                    'FrmBloqueo.Show()
                    'Dim myProcess As New Process()
                    'Dim myProcessStartInfo As New ProcessStartInfo("C:\exes\softv\reportes\SoftvPPE.exe", "msjbloq" + "," + CStr(Contrato))
                    'myProcess.StartInfo = myProcessStartInfo
                    'myProcess.Start()
                ElseIf num2 = 0 And NUM <> 0 Then
                    Me.Button20.Visible = True
                    Me.Button20.Text = "Cliente Bloqueado Anteriormente"
                    glopar = "M"
                    Me.Panel1.Enabled = True
                Else
                    Me.Panel1.Enabled = True
                End If

                buscarelaccion()
                'JUANJO SECTORES Y TAPS
                DAMESECTORORDENES(Me.COLONIAComboBox.SelectedValue)
                DAMETAPYSECTORCLIENTE(Me.CONTRATOTextBox.Text)
                llenaComboTipoInstalacion(2)
                consultaRelacionTipoInstalacion(CLng(Me.CONTRATOTextBox.Text))
            End If

            If Me.ComboBox7.SelectedValue = 6 Or Me.ComboBox7.Text = "INSEN" Then
                Me.InsenPanel.Visible = True
            Else
                Me.InsenPanel.Visible = False
            End If

            If OpcionCli = "M" Or OpcionCli = "C" Then
                'buscaCONCLIENTETV()
                Me.Button29.Enabled = True
                'Me.Valida_servicioTvTableAdapter.Connection = CON
                'Me.Valida_servicioTvTableAdapter.Fill(Me.ProcedimientosArnoldo2.Valida_servicioTv, Contrato, validacion)
                'If validacion > 0 Then
                '    damedatostv(Contrato)
                'End If
                CONTIPOSCLIENTE(Contrato)

                'JUANJO CLIENTES PENSIONADOS
                If Me.ComboBox7.SelectedValue = 6 Or Me.ComboBox7.Text = "INSEN" Then
                    ConClienteInsen(Me.CONTRATOTextBox.Text, Me.ComboBox7.SelectedValue)
                End If
                'FIN JUANJO CLIENTES PENSIONADOS

                'Datos Adicionales------------------------------------
                ConRelClientesDatosAdicionales(CONTRATOTextBox.Text)
                '------------------------------------------------------

            End If

            'If IsNumeric(Me.CONTRATOTextBox1.Text) = False Then
            '    Me.CONCLIENTETVBindingNavigator.Items(4).Visible = True
            '    Me.ToolStripButton2.Enabled = False
            '    Me.ToolStripButton3.Enabled = False
            '    Me.Panel3.Enabled = False
            'Else
            '    Me.CONCLIENTETVBindingNavigator.Items(4).Visible = False
            '    Me.Panel3.Enabled = True
            'End If
            'Eric
            CON.Open()
            eGloContrato = Me.CONTRATOTextBox.Text
            Me.ChecaRoboDeSeñalTableAdapter.Connection = CON
            Me.ChecaRoboDeSeñalTableAdapter.Fill(Me.DataSetEric.ChecaRoboDeSeñal, eGloContrato, eRespuesta)
            CON.Close()
            If eRespuesta = 1 Then
                Me.Button21.BackColor = Color.Red
                If IdSistema = "SA" Then
                    FrmRoboDeSeñal.Show()
                End If
            End If
            '------------------------------------------------------------------------------------------------

            If Me.SoloInternetCheckBox.Checked = True And IdSistema = "VA" Then
                If Me.ComboBox7.SelectedValue = 3 Then
                    frmInternet2.Hide()
                    frmInternet2.Show()
                End If
            End If
            'If Me.Button28.Visible = True Then
            '    Me.Button28.Enabled = False
            'End If

            'If opcion = "C" Or opcion = "M" Then
            '    bloqueado1(CInt(Me.CONTRATOTextBox.Text))
            'End If

            If (IdSistema = "LO" Or IdSistema = "YU") And (OpcionCli = "C" Or OpcionCli = "M") Then
                DameComboYRenta(CLng(Me.CONTRATOTextBox.Text))
                If IsNumeric(Me.CONTRATOTextBox.Text) = True Then
                    If Me.CONTRATOTextBox.Text > 0 Then
                        Me.BtnEstadoDeCuenta.Visible = True
                    End If
                End If
            End If
            If GloTipoUsuario <> 40 Then
                Me.ComboBox20.Enabled = False
            ElseIf Me.ComboBox20.Enabled = True Then
            End If
            'Muestra_Usuarios(GloClvUnicaNet, 2)
            Me.btnHisDes.Visible = False

            If OpcionCli = "M" Or OpcionCli = "C" Then
                DAMEClienteClvTipSer(CONTRATOTextBox.Text)
                If ClvTipSer = 6 Then
                    MuestraServicioDigitalTvCable()
                End If
            End If

            BuscaDescDig()
            Dim DT As New DataTable
            DT = UspValidarCliente(Contrato)
            If DT.Rows(0)(0).ToString = "1" Then
                BindingNavigator5.Enabled = False
                BindingNavigator6.Enabled = False
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub buscarelaccion()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.CONTRATOTextBox.Text) = True Then
            Me.CONRel_Clientes_TiposClientesTableAdapter.Connection = CON
            Me.CONRel_Clientes_TiposClientesTableAdapter.Fill(Me.DataSetEDGAR.CONRel_Clientes_TiposClientes, Me.CONTRATOTextBox.Text)
        End If
        CON.Close()
    End Sub

    Private Sub asignacolonia()
        Me.Clv_ColoniaTextBox.Text = Me.COLONIAComboBox.SelectedValue
    End Sub

    Private Sub asiganacalle()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            LocClv_Calletmp = Me.CALLEComboBox.SelectedValue
            Me.Clv_CalleTextBox.Text = Me.CALLEComboBox.SelectedValue
            Me.DAMECOLONIA_CALLETableAdapter.Connection = CON
            Me.DAMECOLONIA_CALLETableAdapter.Fill(Me.NewSofTvDataSet.DAMECOLONIA_CALLE, New System.Nullable(Of Integer)(CType(Me.CALLEComboBox.SelectedValue, Integer)))
            Me.Clv_ColoniaTextBox.Text = Me.COLONIAComboBox.SelectedValue
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub asignaciudad()
        Me.Clv_CiudadTextBox.Text = Me.CIUDADComboBox.SelectedValue
    End Sub



    Private Sub Bucacontrato(ByVal txtcontrato As Long)
        If OpcionCli <> "N" Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Try
                Me.CONSULTARCLIENTETableAdapter.Connection = CON
                Me.CONSULTARCLIENTETableAdapter.Fill(Me.NewSofTvDataSet.CONSULTARCLIENTE, New System.Nullable(Of Long)(CType(txtcontrato, Long)))
                CON.Close()
                CON.Open()
                Me.BuscaBloqueadoTableAdapter.Connection = CON
                Me.BuscaBloqueadoTableAdapter.Fill(Me.DataSetLidia.BuscaBloqueado, Contrato, NUM, num2)
                CON.Close()
                Consulta_No_Int(Me.CONTRATOTextBox.Text)
                TxtRefOxxo.Text = SP_REFERENCIA_OXXO(Contrato)
                If IdSistema = "AG" Then
                    'Apellidos(1)
                    ConClientesApellidos(Contrato)
                End If
            Catch ex As System.Exception
                CON.Close()
                System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub guarda_Cliente()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.asiganacalle()
        Me.asignaciudad()
        Me.asignacolonia()
        If OpcionCli = "N" Then
            If Len(Trim(Me.NOMBRETextBox.Text)) = 0 Then
                MsgBox("Se Requiere el Nombre ", MsgBoxStyle.Information)
                Exit Sub
            End If
            If IsNumeric(Me.Clv_CalleTextBox.Text) = False Then
                MsgBox("Seleccione la Calle", MsgBoxStyle.Information)
                Exit Sub
            End If
            If Len(Trim(Me.ENTRECALLESTextBox.Text)) = 0 Then
                MsgBox("Se Requiere las Entre Calles ", MsgBoxStyle.Information)
                Exit Sub
            End If
            If Len(Trim(Me.NUMEROTextBox.Text)) = 0 Then
                MsgBox("Se Requiere el Numero ", MsgBoxStyle.Information)
                Exit Sub
            End If
            If IsNumeric(Me.Clv_ColoniaTextBox.Text) = False Then
                MsgBox("Seleccione la Colonia", MsgBoxStyle.Information)
                Exit Sub
            End If
            If IsNumeric(Me.Clv_CiudadTextBox.Text) = False Then
                MsgBox("Seleccione la Ciudad", MsgBoxStyle.Information)
                Exit Sub
            End If
        End If

        Me.Validate()
        Me.CONSULTARCLIENTEBindingSource.EndEdit()
        Me.CONSULTARCLIENTETableAdapter.Connection = CON
        Me.CONSULTARCLIENTETableAdapter.Update(Me.NewSofTvDataSet.CONSULTARCLIENTE)





        CON.Close()
    End Sub
    Private Sub borra_valida_direccion(ByVal clv_session As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Try
            cmd = New SqlClient.SqlCommand()
            con.Open()
            With cmd
                .CommandText = "Borra_Valida_direccion"
                .Connection = con
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = clv_session
                .Parameters.Add(prm)

                Dim ia As Integer = cmd.ExecuteNonQuery()

            End With
            con.Close()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)

        End Try
    End Sub
    Private Sub Apellidos(ByVal opc As Integer)


        Dim conApe As New SqlConnection(MiConexion)
        Dim CmdAp As New SqlCommand
        conApe.Open()
        With CmdAp
            .CommandText = "Administra_Apellidos_Clientes"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = conApe

            Dim Pmt As New SqlParameter("@Contrato", SqlDbType.BigInt)
            Pmt.Direction = ParameterDirection.Input
            If IsNumeric(Me.CONTRATOTextBox.Text) = False Then
                Pmt.Value = 0
            Else
                Pmt.Value = Me.CONTRATOTextBox.Text
            End If

            Dim Pmt2 As New SqlParameter("@Nombre", SqlDbType.VarChar, 150)
            Pmt2.Direction = ParameterDirection.InputOutput
            Pmt2.Value = Me.TextBox27.Text

            Dim Pmt3 As New SqlParameter("@ApellidoP", SqlDbType.VarChar, 150)
            Pmt3.Direction = ParameterDirection.InputOutput
            Pmt3.Value = Me.TextBox4.Text

            Dim Pmt4 As New SqlParameter("@ApellidoM", SqlDbType.VarChar, 150)
            Pmt4.Direction = ParameterDirection.InputOutput
            Pmt4.Value = Me.TextBox7.Text

            Dim Pmt5 As New SqlParameter("@opc", SqlDbType.Int)
            Pmt5.Direction = ParameterDirection.Input
            Pmt5.Value = opc

            .Parameters.Add(Pmt)
            .Parameters.Add(Pmt2)
            .Parameters.Add(Pmt3)
            .Parameters.Add(Pmt4)
            .Parameters.Add(Pmt5)

            .ExecuteNonQuery()
            If opc = 1 Then
                Me.TextBox27.Text = Pmt2.Value
                Me.TextBox4.Text = Pmt3.Value
                Me.TextBox7.Text = Pmt4.Value
            End If

        End With
    End Sub

    Private Sub Checa_si_quiere_combos()

        'Dim op As Integer = 0
        'If Dime_Si_Hay_Combos() = 1 Then
        '    If IdSistema = "LO" Or IdSistema = "YU" Or IdSistema = "AG" Then
        '        op = MsgBox("Desea Contratar Un Combo", MsgBoxStyle.YesNo)
        '        If op = 6 Then 'si Desea El Combo
        '            'Edgar 11Mayo2009
        '            'ChecaTelefonosDisponibles()
        '            'If eResValida = 1 Then
        '            '    MsgBox(eMsgValida, MsgBoxStyle.Information)
        '            '    Exit Sub
        '            'End If
        '            'Edgar 11Mayo2009
        '            LocContratoLog = CLng(Me.CONTRATOTextBox.Text)
        '            FrmContratacionCombo.Show()
        '        End If
        '    End If
        'End If

        If SoloInternetCheckBox.Checked = True Then Exit Sub
        If MessageBox.Show("¿Deseas contratar un Combo?", "Atención", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then

            'GloIdCompania = ComboBoxCompanias.SelectedValue

            SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
            'SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
            SoftvMod.VariablesGlobales.MiConexion = MiConexion
            SoftvMod.VariablesGlobales.Contrato = CInt(CONTRATOTextBox.Text)
            SoftvMod.VariablesGlobales.Clv_Usuario = GloUsuario
            SoftvMod.VariablesGlobales.Clv_TipoCliente = CInt(ComboBox7.SelectedValue)
            'SoftvMod.VariablesGlobales.IdCompania = CInt(ComboBoxCompanias.SelectedValue)
            'SoftvMod.VariablesGlobales.TvsinPago = GloTvSinPago
            'SoftvMod.VariablesGlobales.TvconPago = GloTvConpago
            'Dim frm As New SoftvMod.Teziutlan.FrmComboPantallaDeClientesTezi
            Dim frm As New SoftvMod.Torreon.FrmComboPantallaDeClientes
            frm.ShowDialog()
            If frm.SiContrataCombo = True Then
                HABILITA_CTRS()
                'buscaCONCLIENTETV()
            End If
        End If

    End Sub

    Private Sub CONSULTARCLIENTEBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONSULTARCLIENTEBindingNavigatorSaveItem.Click



        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'Me.asiganacalle()
        'Me.asignaciudad()
        'Me.asignacolonia()
        Dim Locerror_dir As Integer = 0

        If IdSistema = "AG" Then
            Me.NOMBRETextBox.Text = Me.TextBox27.Text + " " + Me.TextBox4.Text + " " + Me.TextBox7.Text
        End If
        Me.Clv_CiudadTextBox.Text = Me.CIUDADComboBox.SelectedValue
        'Me.Clv_ColoniaTextBox.Text = Me.COLONIAComboBox.SelectedValue
        'Me.Clv_CalleTextBox.Text = Me.CALLEComboBox.SelectedValue
        If OpcionCli = "N" Then
            If cmbSector.SelectedIndex <= 0 Then
                MsgBox("Se Requiere que Seleccione el  Sector ", MsgBoxStyle.Information)
                Exit Sub
            End If
            If Len(Trim(Me.NOMBRETextBox.Text)) = 0 Then
                MsgBox("Se Requiere el Nombre ", MsgBoxStyle.Information)
                Exit Sub
            End If
            If IsNumeric(Me.Clv_CalleTextBox.Text) = False Then
                MsgBox("Seleccione la Calle", MsgBoxStyle.Information)
                Exit Sub
            End If
            If Len(Trim(Me.ENTRECALLESTextBox.Text)) = 0 Then
                MsgBox("Se Requiere las Entre Calles ", MsgBoxStyle.Information)
                Exit Sub
            End If
            If Len(Trim(Me.NUMEROTextBox.Text)) = 0 Then
                MsgBox("Se Requiere el Numero ", MsgBoxStyle.Information)
                Exit Sub
            End If
            If IsNumeric(Me.Clv_ColoniaTextBox.Text) = False Then
                MsgBox("Seleccione la Colonia", MsgBoxStyle.Information)
                Exit Sub
            End If
            If IsNumeric(Me.Clv_CiudadTextBox.Text) = False Then
                MsgBox("Seleccione la Ciudad", MsgBoxStyle.Information)
                Exit Sub
            End If
        End If
        If Len(Trim(Me.ComboBox7.Text)) = 0 Then
            MsgBox("Seleccione el Tipo de Cobro", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.ComboBox7.SelectedValue) = False Then
            MsgBox("Seleccione el Tipo de Cobro", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.TipoClienteCombo.SelectedValue) = False Then
            MsgBox("Seleccione el Tipo de Cliente", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.TipoSubClienteCombo.SelectedValue) = False Then
            MsgBox("Seleccione el Sub-Tipo de Cliente", MsgBoxStyle.Information)
            Exit Sub
        End If
        If (Me.ComboBox7.SelectedValue = 6 Or Me.ComboBox7.Text = "INSEN") And Len(Me.NoInsenTextBox.Text) = 0 Then
            MsgBox("Capture el Numero de la Tarjeta del Pensionado", MsgBoxStyle.Information)
            Me.InsenPanel.BackColor = Color.Yellow
            Exit Sub
        End If
        If Me.cmbSector.SelectedValue = 0 Then
            MsgBox("Seleccione el Sector al que pertenece el cliente", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Me.cmbTipoInstalacion.SelectedValue = 0 Then
            MsgBox("Seleccione el Tipo de Instalación del Cliente", MsgBoxStyle.Information)
            Exit Sub
        End If


        '---    Validación de Datos Duplicados          ---
        'Modificado: 29/06/2011 - 4:12 p.m.
        'Anteriormente NO estaba validando que se revisaran los datos duplicados en la Modificación, solo cuando era nuevo
        If OpcionCli = "M" Then
            'IFE
            If tbIFE.Text.Length = 0 Then
                MsgBox("Captura el número de IFE del cliente.", MsgBoxStyle.Information)
                Exit Sub
            End If
            ValidaIFE(tbIFE.Text)
            If eMsj.Length > 0 Then
                eIFE = tbIFE.Text
                'Checamos si es que el IFE no se ha autorizado para este Cliente
                If GloTipoUsuario = 40 Then
                    MsgBox(eMsj & Chr(13) & Chr(13) & "A continuación se mostrarán el(los) Cliente(s)", MsgBoxStyle.Exclamation)
                    FrmMuestraIFE.Guardar = False
                    FrmMuestraIFE.ShowDialog()
                    'Si se selección el botón de rechazar cancelamos todo
                    If FrmMuestraIFE.Guardar = False Then
                        Exit Sub
                    End If
                ElseIf (FrmAutorizacion.IFEAutorizado = False) Then
                    MsgBox(eMsj & Chr(13) & Chr(13) & "A continuación se mostrarán el(los) Cliente(s)", MsgBoxStyle.Exclamation)
                    FrmMuestraIFE.Guardar = False
                    FrmMuestraIFE.ShowDialog()
                    'Si se selección el botón de rechazar cancelamos todo
                    If FrmMuestraIFE.Guardar = False Then
                        Exit Sub
                    End If
                End If
            End If

            'DIRECCIÓN
            If (ValidaDireccionDuplicadaContratoModificar(Locclv_session2, CLng(Me.COLONIAComboBox.SelectedValue), CLng(Me.CALLEComboBox.SelectedValue), Me.NUMEROTextBox.Text, CONTRATOTextBox.Text)) Then
                'Verificamos si es que el Domicilio no se ha autorizado para este Cliente

                If GloTipoUsuario = 40 Then
                    MsgBox("Esta Direccion Ya Esta Asignada a Otro(s) Cliente(s)" & Chr(13) & Chr(13) & "A continuación se mostrarán el(los) Cliente(s)", MsgBoxStyle.Information)
                    FrmMuestraDireccion.Guardar = False
                    FrmMuestraDireccion.ShowDialog()
                    'Si se selección el botón de rechazar cancelamos todo
                    If (FrmMuestraDireccion.Guardar = False) Then
                        Exit Sub
                    End If
                ElseIf (FrmAutorizacion.DireccionAutorizado = False) Then
                    MsgBox("Esta Direccion Ya Esta Asignada a Otro(s) Cliente(s)" & Chr(13) & Chr(13) & "A continuación se mostrarán el(los) Cliente(s)", MsgBoxStyle.Information)
                    FrmMuestraDireccion.Guardar = False
                    FrmMuestraDireccion.ShowDialog()
                    'Si se selección el botón de rechazar cancelamos todo
                    If (FrmMuestraDireccion.Guardar = False) Then
                        Exit Sub
                    End If
                End If
            End If

            'NOMBRE
            If (ValidaNombreDuplicadoContratoModificar(Me.TextBox27.Text, Me.TextBox4.Text, Me.TextBox7.Text, CONTRATOTextBox.Text)) Then
                'Verificamos si es que el Nombre no se ha aturizado para este Contrato

                If GloTipoUsuario = 40 Then
                    MsgBox("Ya existe un cliente con el nombre de " & Me.TextBox27.Text & " " & Me.TextBox4.Text & " " & Me.TextBox7.Text & Chr(13) & Chr(13) & "A continuación se mostrarán el(los) Cliente(s)", MsgBoxStyle.Exclamation)
                    eNombre = ""
                    eNombre = Me.TextBox27.Text
                    eApellidoPaterno = Me.TextBox4.Text
                    eApellidoMaterno = Me.TextBox7.Text
                    FrmMuestraContrato.Guardar = False
                    FrmMuestraContrato.ShowDialog()
                    If (FrmMuestraContrato.Guardar = False) Then
                        Exit Sub
                    End If
                ElseIf (FrmAutorizacion.NombreAutorizado = False) Then
                    MsgBox("Ya existe un cliente con el nombre de " & Me.TextBox27.Text & " " & Me.TextBox4.Text & " " & Me.TextBox7.Text & Chr(13) & Chr(13) & "A continuación se mostrarán el(los) Cliente(s)", MsgBoxStyle.Exclamation)
                    eNombre = ""
                    eNombre = Me.TextBox27.Text
                    eApellidoPaterno = Me.TextBox4.Text
                    eApellidoMaterno = Me.TextBox7.Text
                    FrmMuestraContrato.Guardar = False
                    FrmMuestraContrato.ShowDialog()
                    If (FrmMuestraContrato.Guardar = False) Then
                        Exit Sub
                    End If
                End If
            End If
        End If


        If OpcionCli = "N" Then


            'IFE
            If tbIFE.Text.Length = 0 Then
                MsgBox("Captura el número de IFE del cliente.", MsgBoxStyle.Information)
                Exit Sub
            End If
            ValidaIFE(tbIFE.Text)
            If eMsj.Length > 0 Then
                eIFE = tbIFE.Text
                'Checamos si es que el IFE no se ha autorizado para este Cliente
                If GloTipoUsuario = 40 Then
                    MsgBox(eMsj & Chr(13) & Chr(13) & "A continuación se mostrarán el(los) Cliente(s)", MsgBoxStyle.Exclamation)
                    FrmMuestraIFE.Guardar = False
                    FrmMuestraIFE.ShowDialog()
                    'Si se selección el botón de rechazar cancelamos todo
                    If FrmMuestraIFE.Guardar = False Then
                        Exit Sub
                    End If
                ElseIf (FrmAutorizacion.IFEAutorizado = False) Then
                    MsgBox(eMsj & Chr(13) & Chr(13) & "A continuación se mostrarán el(los) Cliente(s)", MsgBoxStyle.Exclamation)
                    FrmMuestraIFE.Guardar = False
                    FrmMuestraIFE.ShowDialog()
                    'Si se selección el botón de rechazar cancelamos todo
                    If FrmMuestraIFE.Guardar = False Then
                        Exit Sub
                    End If
                End If
            End If



            loc_clv_Sector = cmbSector.SelectedValue
            'Validación Nombre------------------------------------------------------------------------------
            eResValida = 0
            ValidaNombreUsuarioSeparado(Me.TextBox27.Text, Me.TextBox4.Text, Me.TextBox7.Text)
            If eResValida = 1 Then
                'Verificamos si es que el Nombre no se ha aturizado para este Contrato
                If GloTipoUsuario = 40 Then
                    MsgBox(eMsgValida, MsgBoxStyle.Exclamation)
                    eNombre = ""
                    eNombre = Me.TextBox27.Text
                    eApellidoPaterno = Me.TextBox4.Text
                    eApellidoMaterno = Me.TextBox7.Text
                    FrmMuestraContrato.ShowDialog()
                    Exit Sub
                ElseIf (FrmAutorizacion.NombreAutorizado = False) Then
                    MsgBox(eMsgValida, MsgBoxStyle.Exclamation)
                    eNombre = ""
                    eNombre = Me.TextBox27.Text
                    eApellidoPaterno = Me.TextBox4.Text
                    eApellidoMaterno = Me.TextBox7.Text
                    FrmMuestraContrato.ShowDialog()
                    Exit Sub
                End If
            End If

            'Validacion Calles-------------------------------------------------------------------------------
            If Locclv_session2 = 0 Then
                Me.Dame_clv_session_clientesTableAdapter.Connection = CON
                Me.Dame_clv_session_clientesTableAdapter.Fill(Me.ProcedimientosArnoldo2.Dame_clv_session_clientes, Locclv_session2)
                borra_valida_direccion(Locclv_session2)
            End If
            'Me.Valida_Direccion1TableAdapter.Connection = CON
            'Me.Valida_Direccion1TableAdapter.Fill(Me.ProcedimientosArnoldo2.Valida_Direccion1, Locclv_session2, CLng(Me.COLONIAComboBox.SelectedValue), CLng(Me.CALLEComboBox.SelectedValue), Me.NUMEROTextBox.Text, Locerror_dir)
            Locerror_dir = Valida_Direccion1(Locclv_session2, CLng(Me.COLONIAComboBox.SelectedValue), CLng(Me.CALLEComboBox.SelectedValue), Me.NUMEROTextBox.Text)

            If Locerror_dir = 1 And Locbnd2clientes = False Then
                If bnddir = False Then
                    'bnddir = True
                    'Verificamos si es que el Domicilio no se ha autorizado para este Cliente
                    If GloTipoUsuario = 40 Then
                        MsgBox("Esta Direccion Ya Esta Asignada a Otro(s) Cliente(s)", MsgBoxStyle.Information)
                        FrmMuestraDireccion.ShowDialog()
                        Exit Sub
                    ElseIf (FrmAutorizacion.DireccionAutorizado = False) Then
                        MsgBox("Esta Direccion Ya Esta Asignada a Otro(s) Cliente(s)", MsgBoxStyle.Information)
                        FrmMuestraDireccion.ShowDialog()
                        Exit Sub
                    End If

                ElseIf bnddir = True Then
                    Me.Validate()
                    Me.CONSULTARCLIENTEBindingSource.EndEdit()
                    Me.CONSULTARCLIENTETableAdapter.Connection = CON
                    Me.CONSULTARCLIENTETableAdapter.Update(Me.NewSofTvDataSet.CONSULTARCLIENTE)
                    'If Me.SoloInternetCheckBox.Checked = True Then
                    '    Me.SoloInternetCheckBox.Enabled = False
                    'End If
                    CON.Close()
                    GUARDARRel_Clientes_TiposClientesGuarda()
                    Guarda_No_Int(Me.CONTRATOTextBox.Text, Me.TxtNumeroInt.Text)
                    'JUANJO GUARDA TIPO Y SUB TIPO DE CLIENTE
                    ActualizaDatosTipoCliente(Me.CONTRATOTextBox.Text, Me.TipoClienteCombo.SelectedValue, Me.TipoSubClienteCombo.SelectedValue, Me.GeneroCombo.SelectedValue, Me.Referencia1Text.Text, Me.Referencia2Text.Text)
                    'FIN JUANJO GUARDA TIPO Y SUB TIPO DE CLIENTE

                    'JUANJO CLIENTES PENSIONADOS
                    If Me.ComboBox7.SelectedValue = 6 Or Me.ComboBox7.Text = "INSEN" Then
                        NueClienteInsen(Me.CONTRATOTextBox.Text, Me.ComboBox7.SelectedValue, Me.NoInsenTextBox.Text, 1)
                    Else
                        NueClienteInsen(Me.CONTRATOTextBox.Text, Me.ComboBox7.SelectedValue, Me.NoInsenTextBox.Text, 2)
                    End If
                    'FIN JUANJO CLIENTES PENSIONADOS

                    'GuadarDatosAdicionales-----------------------------------------------------------------------------
                    NueRelClientesDatosAdicionales(CONTRATOTextBox.Text, cbCasaPropia.Checked, tbTelefonoTrabajo.Text, tbIFE.Text)
                    '---------------------------------------------------------------------------------------------------

                    MsgBox("Se ha Guardado con Éxito", MsgBoxStyle.Information)
                    guardabitacora()
                    BuscaCuenta()
                    'If Me.SoloInternetCheckBox.CheckState = False Then
                    '    Checa_si_quiere_combos()
                    'End If
                    eGuardarCliente = True
                    ACTUALIZASECTORCLIENTE(Me.CONTRATOTextBox.Text, loc_clv_Sector)
                    DAMETAPYSECTORCLIENTE(Me.CONTRATOTextBox.Text)
                    'SE ACTUALIZA EL TIPO DE INSTALACION DEL CLIENTE (INICIO)
                    guardaRelacionTipoInstalacion(CLng(Me.CONTRATOTextBox.Text), Me.cmbTipoInstalacion.SelectedValue)
                    'SE ACTUALIZA EL TIPO DE INSTALACION DEL CLIENTE (FIN)
                End If
            ElseIf Locerror_dir = 0 Then
                Me.Validate()
                Me.CONSULTARCLIENTEBindingSource.EndEdit()
                Me.CONSULTARCLIENTETableAdapter.Connection = CON
                Me.CONSULTARCLIENTETableAdapter.Update(Me.NewSofTvDataSet.CONSULTARCLIENTE)
                'If Me.SoloInternetCheckBox.Checked = True Then
                '    Me.SoloInternetCheckBox.Enabled = False
                'End If
                CON.Close()
                GUARDARRel_Clientes_TiposClientesGuarda()
                Guarda_No_Int(Me.CONTRATOTextBox.Text, Me.TxtNumeroInt.Text)
                'JUANJO GUARDA TIPO Y SUB TIPO DE CLIENTE
                ActualizaDatosTipoCliente(Me.CONTRATOTextBox.Text, Me.TipoClienteCombo.SelectedValue, Me.TipoSubClienteCombo.SelectedValue, Me.GeneroCombo.SelectedValue, Me.Referencia1Text.Text, Me.Referencia2Text.Text)
                'FIN JUANJO GUARDA TIPO Y SUB TIPO DE CLIENTE

                'JUANJO CLIENTES PENSIONADOS
                If Me.ComboBox7.SelectedValue = 6 Or Me.ComboBox7.Text = "INSEN" Then
                    NueClienteInsen(Me.CONTRATOTextBox.Text, Me.ComboBox7.SelectedValue, Me.NoInsenTextBox.Text, 1)
                Else
                    NueClienteInsen(Me.CONTRATOTextBox.Text, Me.ComboBox7.SelectedValue, Me.NoInsenTextBox.Text, 2)
                End If
                'FIN JUANJO CLIENTES PENSIONADOS

                'GuadarDatosAdicionales-----------------------------------------------------------------------------
                NueRelClientesDatosAdicionales(CONTRATOTextBox.Text, cbCasaPropia.Checked, tbTelefonoTrabajo.Text, tbIFE.Text)
                '---------------------------------------------------------------------------------------------------

                MsgBox("Se ha Guardado con Exíto", MsgBoxStyle.Information)
                guardabitacora()
                BuscaCuenta()
                'If Me.SoloInternetCheckBox.CheckState = False Then
                '    Checa_si_quiere_combos()
                'End If
                eGuardarCliente = True
                ACTUALIZASECTORCLIENTE(Me.CONTRATOTextBox.Text, loc_clv_Sector)
                DAMETAPYSECTORCLIENTE(Me.CONTRATOTextBox.Text)
                'SE ACTUALIZA EL TIPO DE INSTALACION DEL CLIENTE (INICIO)
                guardaRelacionTipoInstalacion(CLng(Me.CONTRATOTextBox.Text), Me.cmbTipoInstalacion.SelectedValue)
                'SE ACTUALIZA EL TIPO DE INSTALACION DEL CLIENTE (FIN)
            ElseIf Locerror_dir = 1 And Locbnd2clientes = True Then
                Me.Validate()
                Me.CONSULTARCLIENTEBindingSource.EndEdit()
                Me.CONSULTARCLIENTETableAdapter.Connection = CON
                Me.CONSULTARCLIENTETableAdapter.Update(Me.NewSofTvDataSet.CONSULTARCLIENTE)
                'If Me.SoloInternetCheckBox.Checked = True Then
                '    Me.SoloInternetCheckBox.Enabled = False
                'End If
                CON.Close()
                GUARDARRel_Clientes_TiposClientesGuarda()
                Guarda_No_Int(Me.CONTRATOTextBox.Text, Me.TxtNumeroInt.Text)
                'JUANJO GUARDA TIPO Y SUB TIPO DE CLIENTE
                ActualizaDatosTipoCliente(Me.CONTRATOTextBox.Text, Me.TipoClienteCombo.SelectedValue, Me.TipoSubClienteCombo.SelectedValue, Me.GeneroCombo.SelectedValue, Me.Referencia1Text.Text, Me.Referencia2Text.Text)
                'FIN JUANJO GUARDA TIPO Y SUB TIPO DE CLIENTE

                'JUANJO CLIENTES PENSIONADOS
                If Me.ComboBox7.SelectedValue = 6 Or Me.ComboBox7.Text = "INSEN" Then
                    NueClienteInsen(Me.CONTRATOTextBox.Text, Me.ComboBox7.SelectedValue, Me.NoInsenTextBox.Text, 1)
                Else
                    NueClienteInsen(Me.CONTRATOTextBox.Text, Me.ComboBox7.SelectedValue, Me.NoInsenTextBox.Text, 2)
                End If
                'FIN JUANJO CLIENTES PENSIONADOS

                'GuadarDatosAdicionales-----------------------------------------------------------------------------
                NueRelClientesDatosAdicionales(CONTRATOTextBox.Text, cbCasaPropia.Checked, tbTelefonoTrabajo.Text, tbIFE.Text)
                '---------------------------------------------------------------------------------------------------


                MsgBox("Se ha Guardado con Exíto", MsgBoxStyle.Information)
                guardabitacora()
                BuscaCuenta()
                'If Me.SoloInternetCheckBox.CheckState = False Then
                '    Checa_si_quiere_combos()
                'End If
                eGuardarCliente = True
                ACTUALIZASECTORCLIENTE(Me.CONTRATOTextBox.Text, loc_clv_Sector)
                DAMETAPYSECTORCLIENTE(Me.CONTRATOTextBox.Text)
                'SE ACTUALIZA EL TIPO DE INSTALACION DEL CLIENTE (INICIO)
                guardaRelacionTipoInstalacion(CLng(Me.CONTRATOTextBox.Text), Me.cmbTipoInstalacion.SelectedValue)
                'SE ACTUALIZA EL TIPO DE INSTALACION DEL CLIENTE (FIN)
            End If
            ACTUALIZASECTORCLIENTE(Me.CONTRATOTextBox.Text, loc_clv_Sector)
            DAMETAPYSECTORCLIENTE(Me.CONTRATOTextBox.Text)
            'SE ACTUALIZA EL TIPO DE INSTALACION DEL CLIENTE (INICIO)
            guardaRelacionTipoInstalacion(CLng(Me.CONTRATOTextBox.Text), Me.cmbTipoInstalacion.SelectedValue)
            'SE ACTUALIZA EL TIPO DE INSTALACION DEL CLIENTE (FIN)
        Else
            Me.Validate()
            Me.CONSULTARCLIENTEBindingSource.EndEdit()
            Me.CONSULTARCLIENTETableAdapter.Connection = CON
            Me.CONSULTARCLIENTETableAdapter.Update(Me.NewSofTvDataSet.CONSULTARCLIENTE)
            'If Me.SoloInternetCheckBox.Checked = True Then
            '    Me.SoloInternetCheckBox.Enabled = False
            'End If
            CON.Close()
            GUARDARRel_Clientes_TiposClientesGuarda()
            bndbitacora = True
            guardabitacora()
            Guarda_No_Int(Me.CONTRATOTextBox.Text, Me.TxtNumeroInt.Text)
            'JUANJO GUARDA TIPO Y SUB TIPO DE CLIENTE
            ActualizaDatosTipoCliente(Me.CONTRATOTextBox.Text, Me.TipoClienteCombo.SelectedValue, Me.TipoSubClienteCombo.SelectedValue, Me.GeneroCombo.SelectedValue, Me.Referencia1Text.Text, Me.Referencia2Text.Text)
            'FIN JUANJO GUARDA TIPO Y SUB TIPO DE CLIENTE

            'JUANJO CLIENTES PENSIONADOS
            If Me.ComboBox7.SelectedValue = 6 Or Me.ComboBox7.Text = "INSEN" Then
                NueClienteInsen(Me.CONTRATOTextBox.Text, Me.ComboBox7.SelectedValue, Me.NoInsenTextBox.Text, 1)
            Else
                NueClienteInsen(Me.CONTRATOTextBox.Text, Me.ComboBox7.SelectedValue, Me.NoInsenTextBox.Text, 2)
            End If
            'FIN JUANJO CLIENTES PENSIONADOS

            'GuadarDatosAdicionales-----------------------------------------------------------------------------
            NueRelClientesDatosAdicionales(CONTRATOTextBox.Text, cbCasaPropia.Checked, tbTelefonoTrabajo.Text, tbIFE.Text)
            '---------------------------------------------------------------------------------------------------

            MsgBox("Se ha Guardado con Exíto", MsgBoxStyle.Information)
            eGuardarCliente = True
            Me.Button29.Enabled = True
        End If
        ACTUALIZASECTORCLIENTE(Me.CONTRATOTextBox.Text, Me.cmbSector.SelectedValue)
        'SE ACTUALIZA EL TIPO DE INSTALACION DEL CLIENTE (INICIO)
        guardaRelacionTipoInstalacion(CLng(Me.CONTRATOTextBox.Text), Me.cmbTipoInstalacion.SelectedValue)
        'SE ACTUALIZA EL TIPO DE INSTALACION DEL CLIENTE (FIN)
        'Fin VAlidacion Calles
        CON.Close()

        If IdSistema = "AG" Then
            'Apellidos(2)
            GuardaClientesApellidos()
        End If



        'If OpcionCli = "M" Then
        '    eResValida = 0
        '    ValidaDatosBancarios(CInt(Me.CONTRATOTextBox.Text), CLng(Me.ComboBox7.SelectedValue))
        '    If eResValida = 1 Then
        '        MsgBox(eMsgValida, MsgBoxStyle.Exclamation)
        '    End If
        'End If
        'GuardaCuenta()

    End Sub
    Private Sub dame_datos()
        Try
            Dim comando As New SqlClient.SqlCommand
            Dim con89 As New SqlClient.SqlConnection(MiConexion)
            con89.Open()
            If OpcionCli = "M" Then

                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = con89
                    .CommandText = "CONSULTARCLIENTE_bitacora"
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0
                    ' Create a SqlParameter for each parameter in the stored procedure.
                    '@CONTRATO BIGINT,@contrato1 bigint output, @NOMBRE varchar(max) output, @Clv_Calle bigint output,
                    '@NUMERO varchar(max) output, @ENTRECALLES varchar(max) output, @Clv_Colonia bigint output,@CodigoPostal varchar(50) output,
                    ' @TELEFONO varchar(30) output, @CELULAR varchar(50) output, @DESGLOSA_Iva bit output, @SoloInternet bit output, 
                    '@eshotel bit output,@clv_Ciudad int output,@Email varchar(50) output, @clv_sector int output,@Clv_Periodo int output
                    Dim prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                    Dim prm1 As New SqlParameter("@contrato1", SqlDbType.BigInt)
                    Dim prm2 As New SqlParameter("@NOMBRE", SqlDbType.VarChar, 300)
                    Dim prm3 As New SqlParameter("@Clv_Calle", SqlDbType.BigInt)
                    Dim prm4 As New SqlParameter("@NUMERO", SqlDbType.VarChar, 300)
                    Dim prm5 As New SqlParameter("@ENTRECALLES", SqlDbType.VarChar, 300)
                    Dim prm6 As New SqlParameter("@Clv_Colonia", SqlDbType.BigInt)
                    Dim prm7 As New SqlParameter("@CodigoPostal", SqlDbType.VarChar, 50)
                    Dim prm8 As New SqlParameter("@TELEFONO", SqlDbType.VarChar, 30)
                    Dim prm9 As New SqlParameter("@CELULAR", SqlDbType.VarChar, 50)
                    Dim prm10 As New SqlParameter("@DESGLOSA_Iva", SqlDbType.Bit)
                    Dim prm11 As New SqlParameter("@SoloInternet", SqlDbType.Bit)
                    Dim prm12 As New SqlParameter("@eshotel", SqlDbType.Bit)
                    Dim prm13 As New SqlParameter("@clv_Ciudad", SqlDbType.Int)
                    Dim prm14 As New SqlParameter("@Email", SqlDbType.VarChar, 50)
                    Dim prm15 As New SqlParameter("@clv_sector", SqlDbType.Int)
                    Dim prm16 As New SqlParameter("@Clv_Periodo", SqlDbType.Int)



                    prm.Direction = ParameterDirection.Input
                    prm1.Direction = ParameterDirection.Output
                    prm2.Direction = ParameterDirection.Output
                    prm3.Direction = ParameterDirection.Output
                    prm4.Direction = ParameterDirection.Output
                    prm5.Direction = ParameterDirection.Output
                    prm6.Direction = ParameterDirection.Output
                    prm7.Direction = ParameterDirection.Output
                    prm8.Direction = ParameterDirection.Output
                    prm9.Direction = ParameterDirection.Output
                    prm10.Direction = ParameterDirection.Output
                    prm11.Direction = ParameterDirection.Output
                    prm12.Direction = ParameterDirection.Output
                    prm13.Direction = ParameterDirection.Output
                    prm14.Direction = ParameterDirection.Output
                    prm15.Direction = ParameterDirection.Output
                    prm16.Direction = ParameterDirection.Output


                    prm.Value = Contrato
                    prm1.Value = 0
                    prm2.Value = " "
                    prm3.Value = 0
                    prm4.Value = " "
                    prm5.Value = " "
                    prm6.Value = 0
                    prm7.Value = " "
                    prm8.Value = " "
                    prm9.Value = " "
                    prm10.Value = False
                    prm11.Value = False
                    prm12.Value = False
                    prm13.Value = 0
                    prm14.Value = " "
                    prm15.Value = " "
                    prm16.Value = 0

                    .Parameters.Add(prm)
                    .Parameters.Add(prm1)
                    .Parameters.Add(prm2)
                    .Parameters.Add(prm3)
                    .Parameters.Add(prm4)
                    .Parameters.Add(prm5)
                    .Parameters.Add(prm6)
                    .Parameters.Add(prm7)
                    .Parameters.Add(prm8)
                    .Parameters.Add(prm9)
                    .Parameters.Add(prm10)
                    .Parameters.Add(prm11)
                    .Parameters.Add(prm12)
                    .Parameters.Add(prm13)
                    .Parameters.Add(prm14)
                    .Parameters.Add(prm15)
                    .Parameters.Add(prm16)

                    Dim i As Integer = comando.ExecuteNonQuery()
                    contratobit = prm1.Value
                    nombre_cliente = prm2.Value
                    Clv_Callebit = prm3.Value
                    NUMERO_casabit = prm4.Value
                    ENTRECALLESbit = prm5.Value
                    Clv_Coloniabit = prm6.Value
                    CodigoPostalbit = prm7.Value
                    TELEFONObit = prm8.Value
                    CELULARbit = prm9.Value
                    DESGLOSA_Ivabit = prm10.Value
                    SoloInternetbit = prm11.Value
                    eshotelbit = prm12.Value
                    clv_Ciudadbit = prm13.Value
                    Email = prm14.Value
                    clv_sectorbit = prm15.Value
                    Clv_Periodobit = prm16.Value


                End With

                con89.Close()

            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        'Fin de llenado variables de la bitacora del sistema

    End Sub
    Private Sub damedatosdig2()
        If OpcionCli = "M" Then
            statusTarjeta = Me.ComboBox13.Text
            Activacion = Me.TextBox33.Text
            Suspension = Me.TextBox32.Text

            Baja = Me.TextBox23.Text
            Obs = Me.TextBox29.Text
            If Me.CheckBox2.CheckState = CheckState.Checked Then
                serenta = "True"
            ElseIf Me.CheckBox2.CheckState = CheckState.Unchecked Then
                serenta = "False"
            End If
        End If
    End Sub

    'Private Sub damedatostv_2(ByVal contrato As Integer)
    '    Try

    '        Dim comando As New SqlClient.SqlCommand
    '        Dim con95 As New SqlConnection(MiConexion)
    '        con95.Open()

    '        status = ""
    '        LocTVSINPAGO = 0
    '        LocTVCONPAGO = 0
    '        ultimo_mes = 0
    '        ultimo_anio = 0
    '        FECHA_ULT_PAGO = ""
    '        fecha_solicitud = ""
    '        fecha_suspension = ""
    '        fecha_Fuera_Area = ""
    '        fecha_instalacio = ""
    '        fecha_baja = ""
    '        LocFec_ULT_PAG_ANT = ""
    '        PrimerMensualidad = False
    '        LocClv_MOTCAN = 0
    '        LocPRIMERMESANT = False
    '        LocClv_TipoServicioTV = 0
    '        LocpuntosAcumulados = 0
    '        Obs = ""
    '        LocTipSerTv = ""
    '        LocMOTCAN = ""
    '        Cortesia = False
    '        factura = ""
    '        Descuento = 0
    '        Clv_Vendedor = 0


    '        comando = New SqlClient.SqlCommand

    '        With comando
    '            .Connection = con95
    '            .CommandText = "CONCLIENTETV_oledb"
    '            .CommandType = CommandType.StoredProcedure
    '            .CommandTimeout = 0

    '            Dim prm As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
    '            Dim prm1 As New SqlParameter("@STATUS", SqlDbType.VarChar, 1)
    '            Dim prm2 As New SqlParameter("@TVSINPAGO", SqlDbType.BigInt)
    '            Dim prm3 As New SqlParameter("@TVCONPAGO", SqlDbType.BigInt)
    '            Dim prm4 As New SqlParameter("@ULTIMO_MES", SqlDbType.Int)
    '            Dim prm5 As New SqlParameter("@ULTIMO_ANIO", SqlDbType.Int)
    '            Dim prm6 As New SqlParameter("@FECHA_ULT_PAGO", SqlDbType.VarChar, 20)
    '            Dim prm7 As New SqlParameter("@FECHA_SOLICITUD", SqlDbType.VarChar, 20)
    '            Dim prm8 As New SqlParameter("@FECHA_CANCELACIO", SqlDbType.VarChar, 20)
    '            Dim prm9 As New SqlParameter("@FECHACANCOUTAREA", SqlDbType.VarChar, 20)
    '            Dim prm10 As New SqlParameter("@FECHA_INST", SqlDbType.VarChar, 20)
    '            Dim prm11 As New SqlParameter("@FECHA_CORTE", SqlDbType.VarChar, 20)
    '            Dim prm12 As New SqlParameter("@Fec_ULT_PAG_ANT", SqlDbType.VarChar, 20)
    '            Dim prm13 As New SqlParameter("@PRIMERMENSUALIDA", SqlDbType.Bit)
    '            Dim prm14 As New SqlParameter("@Clv_MOTCAN", SqlDbType.Int)
    '            Dim prm15 As New SqlParameter("@PRIMERMESANT", SqlDbType.Bit)
    '            Dim prm16 As New SqlParameter("@Clv_TipoServicioTV", SqlDbType.Int)
    '            Dim prm17 As New SqlParameter("@puntosAcumulados", SqlDbType.Int)
    '            Dim prm18 As New SqlParameter("@obs", SqlDbType.VarChar, 500)
    '            Dim prm19 As New SqlParameter("@TipSerTv", SqlDbType.VarChar, 10)
    '            Dim prm20 As New SqlParameter("@MOTCAN", SqlDbType.VarChar, 500)
    '            Dim prm21 As New SqlParameter("@Cortesia", SqlDbType.Bit)
    '            Dim prm22 As New SqlParameter("@factura", SqlDbType.VarChar, 50)

    '            prm.Direction = ParameterDirection.Input
    '            prm1.Direction = ParameterDirection.Output
    '            prm2.Direction = ParameterDirection.Output
    '            prm3.Direction = ParameterDirection.Output
    '            prm4.Direction = ParameterDirection.Output
    '            prm5.Direction = ParameterDirection.Output
    '            prm6.Direction = ParameterDirection.Output
    '            prm7.Direction = ParameterDirection.Output
    '            prm8.Direction = ParameterDirection.Output
    '            prm9.Direction = ParameterDirection.Output
    '            prm10.Direction = ParameterDirection.Output
    '            prm11.Direction = ParameterDirection.Output
    '            prm12.Direction = ParameterDirection.Output
    '            prm13.Direction = ParameterDirection.Output
    '            prm14.Direction = ParameterDirection.Output
    '            prm15.Direction = ParameterDirection.Output
    '            prm16.Direction = ParameterDirection.Output
    '            prm17.Direction = ParameterDirection.Output
    '            prm18.Direction = ParameterDirection.Output
    '            prm19.Direction = ParameterDirection.Output
    '            prm20.Direction = ParameterDirection.Output
    '            prm21.Direction = ParameterDirection.Output
    '            prm22.Direction = ParameterDirection.Output

    '            prm.Value = contrato
    '            prm1.Value = ""
    '            prm2.Value = 0
    '            prm3.Value = 0
    '            prm4.Value = 0
    '            prm5.Value = 0
    '            prm6.Value = ""
    '            prm7.Value = ""
    '            prm8.Value = ""
    '            prm9.Value = ""
    '            prm10.Value = ""
    '            prm11.Value = ""
    '            prm12.Value = ""
    '            prm13.Value = False
    '            prm14.Value = 0
    '            prm15.Value = False
    '            prm16.Value = 0
    '            prm17.Value = 0
    '            prm18.Value = ""
    '            prm19.Value = ""
    '            prm20.Value = ""
    '            prm21.Value = False
    '            prm22.Value = ""



    '            .Parameters.Add(prm)
    '            .Parameters.Add(prm1)
    '            .Parameters.Add(prm2)
    '            .Parameters.Add(prm3)
    '            .Parameters.Add(prm4)
    '            .Parameters.Add(prm5)
    '            .Parameters.Add(prm6)
    '            .Parameters.Add(prm7)
    '            .Parameters.Add(prm8)
    '            .Parameters.Add(prm9)
    '            .Parameters.Add(prm10)
    '            .Parameters.Add(prm11)
    '            .Parameters.Add(prm12)
    '            .Parameters.Add(prm13)
    '            .Parameters.Add(prm14)
    '            .Parameters.Add(prm15)
    '            .Parameters.Add(prm16)
    '            .Parameters.Add(prm17)
    '            .Parameters.Add(prm18)
    '            .Parameters.Add(prm19)
    '            .Parameters.Add(prm20)
    '            .Parameters.Add(prm21)
    '            .Parameters.Add(prm22)


    '            Dim i As Integer = comando.ExecuteNonQuery()
    '            status = prm1.Value
    '            LocTVSINPAGO = prm2.Value
    '            LocTVCONPAGO = prm3.Value
    '            ultimo_mes = prm4.Value
    '            ultimo_anio = prm5.Value
    '            FECHA_ULT_PAGO = prm6.Value
    '            fecha_solicitud = prm7.Value
    '            fecha_suspension = prm8.Value
    '            fecha_Fuera_Area = prm9.Value
    '            fecha_instalacio = prm10.Value
    '            fecha_baja = prm11.Value
    '            LocFec_ULT_PAG_ANT = prm12.Value
    '            PrimerMensualidad = prm13.Value
    '            LocClv_MOTCAN = prm14.Value
    '            LocPRIMERMESANT = prm15.Value
    '            LocClv_TipoServicioTV = prm16.Value
    '            LocpuntosAcumulados = prm17.Value
    '            Obs = prm18.Value
    '            LocTipSerTv = prm19.Value
    '            LocMOTCAN = prm20.Value
    '            Cortesia = prm21.Value
    '            factura = prm22.Value


    '        End With
    '        'Descuento = Me.DescuentoLabel2.Text
    '        ''Clv_Vendedor = Me.ComboBox16.SelectedValue
    '        'LocVendedorTv = Me.ComboBox16.Text
    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try

    'End Sub


    'Private Sub damedatostv(ByVal contrato As Integer)
    '    Try
    '        If OpcionCli = "M" Then
    '            Dim comando As New SqlClient.SqlCommand
    '            Dim con95 As New SqlConnection(MiConexion)
    '            con95.Open()

    '            status = ""
    '            LocTVSINPAGO = 0
    '            LocTVCONPAGO = 0
    '            ultimo_mes = 0
    '            ultimo_anio = 0
    '            FECHA_ULT_PAGO = ""
    '            fecha_solicitud = ""
    '            fecha_suspension = ""
    '            fecha_Fuera_Area = ""
    '            fecha_instalacio = ""
    '            fecha_baja = ""
    '            LocFec_ULT_PAG_ANT = ""
    '            PrimerMensualidad = False
    '            LocClv_MOTCAN = 0
    '            LocPRIMERMESANT = False
    '            LocClv_TipoServicioTV = 0
    '            LocpuntosAcumulados = 0
    '            Obs = ""
    '            LocTipSerTv = ""
    '            LocMOTCAN = ""
    '            Cortesia = False
    '            factura = ""
    '            Descuento = 0
    '            Clv_Vendedor = 0


    '            comando = New SqlClient.SqlCommand

    '            With comando
    '                .Connection = con95
    '                .CommandText = "CONCLIENTETV_oledb"
    '                .CommandType = CommandType.StoredProcedure
    '                .CommandTimeout = 0

    '                Dim prm As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
    '                Dim prm1 As New SqlParameter("@STATUS", SqlDbType.VarChar, 1)
    '                Dim prm2 As New SqlParameter("@TVSINPAGO", SqlDbType.BigInt)
    '                Dim prm3 As New SqlParameter("@TVCONPAGO", SqlDbType.BigInt)
    '                Dim prm4 As New SqlParameter("@ULTIMO_MES", SqlDbType.Int)
    '                Dim prm5 As New SqlParameter("@ULTIMO_ANIO", SqlDbType.Int)
    '                Dim prm6 As New SqlParameter("@FECHA_ULT_PAGO", SqlDbType.VarChar, 20)
    '                Dim prm7 As New SqlParameter("@FECHA_SOLICITUD", SqlDbType.VarChar, 20)
    '                Dim prm8 As New SqlParameter("@FECHA_CANCELACIO", SqlDbType.VarChar, 20)
    '                Dim prm9 As New SqlParameter("@FECHACANCOUTAREA", SqlDbType.VarChar, 20)
    '                Dim prm10 As New SqlParameter("@FECHA_INST", SqlDbType.VarChar, 20)
    '                Dim prm11 As New SqlParameter("@FECHA_CORTE", SqlDbType.VarChar, 20)
    '                Dim prm12 As New SqlParameter("@Fec_ULT_PAG_ANT", SqlDbType.VarChar, 20)
    '                Dim prm13 As New SqlParameter("@PRIMERMENSUALIDA", SqlDbType.Bit)
    '                Dim prm14 As New SqlParameter("@Clv_MOTCAN", SqlDbType.Int)
    '                Dim prm15 As New SqlParameter("@PRIMERMESANT", SqlDbType.Bit)
    '                Dim prm16 As New SqlParameter("@Clv_TipoServicioTV", SqlDbType.Int)
    '                Dim prm17 As New SqlParameter("@puntosAcumulados", SqlDbType.Int)
    '                Dim prm18 As New SqlParameter("@obs", SqlDbType.VarChar, 500)
    '                Dim prm19 As New SqlParameter("@TipSerTv", SqlDbType.VarChar, 10)
    '                Dim prm20 As New SqlParameter("@MOTCAN", SqlDbType.VarChar, 500)
    '                Dim prm21 As New SqlParameter("@Cortesia", SqlDbType.Bit)
    '                Dim prm22 As New SqlParameter("@factura", SqlDbType.VarChar, 50)

    '                prm.Direction = ParameterDirection.Input
    '                prm1.Direction = ParameterDirection.Output
    '                prm2.Direction = ParameterDirection.Output
    '                prm3.Direction = ParameterDirection.Output
    '                prm4.Direction = ParameterDirection.Output
    '                prm5.Direction = ParameterDirection.Output
    '                prm6.Direction = ParameterDirection.Output
    '                prm7.Direction = ParameterDirection.Output
    '                prm8.Direction = ParameterDirection.Output
    '                prm9.Direction = ParameterDirection.Output
    '                prm10.Direction = ParameterDirection.Output
    '                prm11.Direction = ParameterDirection.Output
    '                prm12.Direction = ParameterDirection.Output
    '                prm13.Direction = ParameterDirection.Output
    '                prm14.Direction = ParameterDirection.Output
    '                prm15.Direction = ParameterDirection.Output
    '                prm16.Direction = ParameterDirection.Output
    '                prm17.Direction = ParameterDirection.Output
    '                prm18.Direction = ParameterDirection.Output
    '                prm19.Direction = ParameterDirection.Output
    '                prm20.Direction = ParameterDirection.Output
    '                prm21.Direction = ParameterDirection.Output
    '                prm22.Direction = ParameterDirection.Output

    '                prm.Value = contrato
    '                prm1.Value = ""
    '                prm2.Value = 0
    '                prm3.Value = 0
    '                prm4.Value = 0
    '                prm5.Value = 0
    '                prm6.Value = ""
    '                prm7.Value = ""
    '                prm8.Value = ""
    '                prm9.Value = ""
    '                prm10.Value = ""
    '                prm11.Value = ""
    '                prm12.Value = ""
    '                prm13.Value = False
    '                prm14.Value = 0
    '                prm15.Value = False
    '                prm16.Value = 0
    '                prm17.Value = 0
    '                prm18.Value = ""
    '                prm19.Value = ""
    '                prm20.Value = ""
    '                prm21.Value = False
    '                prm22.Value = ""



    '                .Parameters.Add(prm)
    '                .Parameters.Add(prm1)
    '                .Parameters.Add(prm2)
    '                .Parameters.Add(prm3)
    '                .Parameters.Add(prm4)
    '                .Parameters.Add(prm5)
    '                .Parameters.Add(prm6)
    '                .Parameters.Add(prm7)
    '                .Parameters.Add(prm8)
    '                .Parameters.Add(prm9)
    '                .Parameters.Add(prm10)
    '                .Parameters.Add(prm11)
    '                .Parameters.Add(prm12)
    '                .Parameters.Add(prm13)
    '                .Parameters.Add(prm14)
    '                .Parameters.Add(prm15)
    '                .Parameters.Add(prm16)
    '                .Parameters.Add(prm17)
    '                .Parameters.Add(prm18)
    '                .Parameters.Add(prm19)
    '                .Parameters.Add(prm20)
    '                .Parameters.Add(prm21)
    '                .Parameters.Add(prm22)


    '                Dim i As Integer = comando.ExecuteNonQuery()
    '                status = prm1.Value
    '                LocTVSINPAGO = prm2.Value
    '                LocTVCONPAGO = prm3.Value
    '                ultimo_mes = prm4.Value
    '                ultimo_anio = prm5.Value
    '                FECHA_ULT_PAGO = prm6.Value
    '                fecha_solicitud = prm7.Value
    '                fecha_suspension = prm8.Value
    '                fecha_Fuera_Area = prm9.Value
    '                fecha_instalacio = prm10.Value
    '                fecha_baja = prm11.Value
    '                LocFec_ULT_PAG_ANT = prm12.Value
    '                PrimerMensualidad = prm13.Value
    '                LocClv_MOTCAN = prm14.Value
    '                LocPRIMERMESANT = prm15.Value
    '                LocClv_TipoServicioTV = prm16.Value
    '                LocpuntosAcumulados = prm17.Value
    '                Obs = prm18.Value
    '                LocTipSerTv = prm19.Value
    '                LocMOTCAN = prm20.Value
    '                Cortesia = prm21.Value
    '                factura = prm22.Value


    '            End With
    '            'Descuento = Me.DescuentoLabel2.Text
    '            ''Clv_Vendedor = Me.ComboBox16.SelectedValue
    '            'LocVendedorTv = Me.ComboBox16.Text
    '        End If
    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try

    'End Sub



    'Private Sub guardabitacoratv()
    '    Try
    '        Dim valida1 As String = Nothing
    '        Dim valida2 As String = Nothing
    '        Dim valida3 As String = Nothing
    '        If OpcionCli = "M" And validacion > 0 Then


    '            'status = prm1.Value
    '            bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Status: ", status, Me.ComboBox1.SelectedValue, LocClv_Ciudad)
    '            'LocTVSINPAGO = prm2.Value
    '            bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Televisiones Sin Pago: ", LocTVSINPAGO, Me.TVSINPAGONumericUpDown.Value, LocClv_Ciudad)
    '            'LocTVCONPAGO = prm3.Value
    '            bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Televisiones Con Pago: ", LocTVCONPAGO, Me.TVCONPAGONumericUpDown.Value, LocClv_Ciudad)
    '            'ultimo_mes = prm4.Value

    '            bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Ultimo Mes: ", ultimo_mes, Me.ULTIMO_MESTextBox.Text, LocClv_Ciudad)
    '            'ultimo_anio = prm5.Value
    '            bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Ultimo año: ", ultimo_anio, Me.ULTIMO_ANIOTextBox.Text, LocClv_Ciudad)
    '            'FECHA_ULT_PAGO = prm6.Value
    '            If FECHA_ULT_PAGO = "01/01/1900" Then FECHA_ULT_PAGO = ""
    '            bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Fecha De Ultimo Pago: ", FECHA_ULT_PAGO, Me.FECHA_ULT_PAGOTextBox.Text, LocClv_Ciudad)
    '            'fecha_solicitud = prm7.Value
    '            If fecha_solicitud = "01/01/1900" Then fecha_solicitud = ""
    '            bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Fecha De Solicitud: ", fecha_solicitud, Me.FECHA_SOLICITUDTextBox.Text, LocClv_Ciudad)
    '            'fecha_suspension = prm8.Value
    '            If fecha_suspension = "01/01/1900" Then fecha_suspension = ""
    '            bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Fecha De Cancelacion: ", fecha_suspension, Me.FECHA_CANCELACIOTextBox.Text, LocClv_Ciudad)
    '            'fecha_Fuera_Area = prm9.Value
    '            If fecha_Fuera_Area = "01/01/1900" Then fecha_Fuera_Area = ""
    '            bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Fecha De Fuera De Area: ", fecha_Fuera_Area, Me.FECHACANCOUTAREATextBox.Text, LocClv_Ciudad)
    '            'fecha_instalacio = prm10.Value
    '            If fecha_instalacio = "01/01/1900" Then fecha_instalacio = ""
    '            bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Fecha De Instalacion: ", fecha_instalacio, Me.FECHA_INSTTextBox.Text, LocClv_Ciudad)
    '            'fecha_baja = prm11.Value
    '            If fecha_baja = "01/01/1900" Then fecha_baja = ""
    '            bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Fecha De Corte: ", fecha_baja, Me.FECHA_CORTETextBox.Text, LocClv_Ciudad)
    '            ''LocFec_ULT_PAG_ANT = prm12.Value
    '            'bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", Me.FECHA_CORTETextBox.Name, fecha_baja, Me.FECHA_CORTETextBox.Text, LocClv_Ciudad)
    '            'PrimerMensualidad = prm13.Value

    '            If Me.PRIMERMENSUALIDACheckBox.CheckState = CheckState.Checked Then
    '                valida1 = "True"
    '            ElseIf Me.PRIMERMENSUALIDACheckBox.CheckState = CheckState.Unchecked Then
    '                valida1 = "False"
    '            End If
    '            bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Primer Mensualidad: ", PrimerMensualidad, valida1, LocClv_Ciudad)

    '            'LocClv_MOTCAN = prm14.Value
    '            bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Motivo de Cancelacion: ", LocClv_MOTCAN, Me.MOTCANComboBox.SelectedValue, LocClv_Ciudad)
    '            'LocPRIMERMESANT = prm15.Value
    '            'LocClv_TipoServicioTV = prm16.Value
    '            bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Servicio: ", LocClv_TipoServicioTV, Me.TipSerTvComboBox.SelectedValue, LocClv_Ciudad)
    '            'LocpuntosAcumulados = prm17.Value
    '            'Obs = prm18.Value
    '            bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Observaciones: ", Obs, Me.ObsTextBox.Text, LocClv_Ciudad)
    '            'LocTipSerTv = prm19.Value
    '            'LocMOTCAN = prm20.Value
    '            'Cortesia = prm21.Value
    '            If Me.CortesiaCheckBox.CheckState = CheckState.Checked Then
    '                valida2 = "True"
    '            ElseIf Me.CortesiaCheckBox.CheckState = CheckState.Unchecked Then
    '                valida2 = "False"
    '            End If
    '            bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Cortesia: ", Cortesia, valida2, LocClv_Ciudad)
    '            'factura = prm22.Value
    '            bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Factura: ", factura, Me.FacturaTextBox1.Text, LocClv_Ciudad)
    '            'Descuento = Me.DescuentoLabel2.Text
    '            bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Descuento: ", Descuento, Me.DescuentoLabel2.Text, LocClv_Ciudad)
    '            'Clv_Vendedor = Me.ComboBox16.SelectedValue
    '            bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Servicio de Televisión: " + CStr(Me.TipSerTvComboBox.Text) + " - " + "Vendedor: ", LocVendedorTv, Me.ComboBox16.Text, LocClv_Ciudad)

    '        End If

    '    Catch ex As Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try
    'End Sub
    Private Sub damedatosdig(ByVal clv_unicanet As Integer)
        Try
            If OpcionCli = "M" Then

                Dim comando As New SqlClient.SqlCommand
                Dim con90 As New SqlConnection(MiConexion)
                con90.Open()

                status = ""
                fecha_solicitud = ""
                fecha_instalacio = ""
                fecha_suspension = ""
                fecha_baja = ""
                fecha_Fuera_Area = ""
                FECHA_ULT_PAGO = ""
                PrimerMensualidad = False
                ultimo_mes = 0
                ultimo_anio = 0
                primerMesAnt = False
                statusAnt = ""
                facturaAnt = ""
                GENERAOSINSTA = False
                factura = ""
                Clv_Vendedor = 0
                Clv_Promocion = 0
                Email = ""
                Obs = ""
                DESCRIPCION = ""
                Cortesia = False



                comando = New SqlClient.SqlCommand

                With comando
                    .Connection = con90
                    .CommandText = "CONSULTACONTDIG_Oledb"
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0

                    '@CLV_UNICANET BIGINT, @status varchar(1) output, @fecha_solicitud varchar(20) output, @fecha_instalacio varchar(20) output, 
                    '@fecha_suspension varchar(20) output , @fecha_baja varchar(20) output, @fecha_Fuera_Area varchar(20) output, @FECHA_ULT_PAGO varchar(max) output , 
                    '@PrimerMensualidad bit output, @ultimo_mes int output, @ultimo_anio int output, @primerMesAnt bit output ,
                    ' @statusAnt varchar(50) output, @facturaAnt varchar(50) output, @GENERAOSINSTA bit output, @factura varchar(50) output,
                    ' @Clv_Vendedor int output, @Clv_Promocion int output, @Email varchar(50) output, @Obs varchar(250) output,
                    '@DESCRIPCION varchar(250) output,@Cortesia bit output

                    Dim prm As New SqlParameter("@CLV_UNICANET", SqlDbType.BigInt)
                    Dim prm1 As New SqlParameter("@status", SqlDbType.VarChar, 1)
                    Dim prm2 As New SqlParameter("@fecha_solicitud", SqlDbType.VarChar, 20)
                    Dim prm3 As New SqlParameter("@fecha_instalacio", SqlDbType.VarChar, 20)
                    Dim prm4 As New SqlParameter("@fecha_suspension", SqlDbType.VarChar, 20)
                    Dim prm5 As New SqlParameter("@fecha_baja", SqlDbType.VarChar, 20)
                    Dim prm6 As New SqlParameter("@fecha_Fuera_Area", SqlDbType.VarChar, 20)
                    Dim prm7 As New SqlParameter("@FECHA_ULT_PAGO", SqlDbType.VarChar, 20)
                    Dim prm8 As New SqlParameter("@PrimerMensualidad", SqlDbType.Bit)
                    Dim prm9 As New SqlParameter("@ultimo_mes", SqlDbType.Int)
                    Dim prm10 As New SqlParameter("@ultimo_anio", SqlDbType.Int)
                    Dim prm11 As New SqlParameter("@primerMesAnt", SqlDbType.Bit)
                    Dim prm12 As New SqlParameter("@statusAnt", SqlDbType.VarChar, 50)
                    Dim prm13 As New SqlParameter("@facturaAnt", SqlDbType.VarChar, 50)
                    Dim prm14 As New SqlParameter("@GENERAOSINSTA", SqlDbType.Bit)
                    Dim prm15 As New SqlParameter("@factura", SqlDbType.VarChar, 50)
                    Dim prm16 As New SqlParameter("@Clv_Vendedor", SqlDbType.Int)
                    Dim prm17 As New SqlParameter("@Clv_Promocion", SqlDbType.Int)
                    Dim prm18 As New SqlParameter("@Email", SqlDbType.VarChar, 50)
                    Dim prm19 As New SqlParameter("@Obs", SqlDbType.VarChar, 250)
                    Dim prm20 As New SqlParameter("@DESCRIPCION", SqlDbType.VarChar, 250)
                    Dim prm21 As New SqlParameter("@Cortesia", SqlDbType.Bit)
                    Dim prm22 As New SqlParameter("@Motivo", SqlDbType.VarChar, 500)


                    prm.Direction = ParameterDirection.Input
                    prm1.Direction = ParameterDirection.Output
                    prm2.Direction = ParameterDirection.Output
                    prm3.Direction = ParameterDirection.Output
                    prm4.Direction = ParameterDirection.Output
                    prm5.Direction = ParameterDirection.Output
                    prm6.Direction = ParameterDirection.Output
                    prm7.Direction = ParameterDirection.Output
                    prm8.Direction = ParameterDirection.Output
                    prm9.Direction = ParameterDirection.Output
                    prm10.Direction = ParameterDirection.Output
                    prm11.Direction = ParameterDirection.Output
                    prm12.Direction = ParameterDirection.Output
                    prm13.Direction = ParameterDirection.Output
                    prm14.Direction = ParameterDirection.Output
                    prm15.Direction = ParameterDirection.Output
                    prm16.Direction = ParameterDirection.Output
                    prm17.Direction = ParameterDirection.Output
                    prm18.Direction = ParameterDirection.Output
                    prm19.Direction = ParameterDirection.Output
                    prm20.Direction = ParameterDirection.Output
                    prm21.Direction = ParameterDirection.Output
                    prm22.Direction = ParameterDirection.Output



                    prm.Value = clv_unicanet
                    prm1.Value = " "
                    prm2.Value = " "
                    prm3.Value = " "
                    prm4.Value = " "
                    prm5.Value = " "
                    prm6.Value = " "
                    prm7.Value = " "
                    prm8.Value = False
                    prm9.Value = 0
                    prm10.Value = 0
                    prm11.Value = False
                    prm12.Value = " "
                    prm13.Value = " "
                    prm14.Value = False
                    prm15.Value = " "
                    prm16.Value = 0
                    prm17.Value = 0
                    prm18.Value = " "
                    prm19.Value = " "
                    prm20.Value = " "
                    prm21.Value = False
                    prm22.Value = " "


                    .Parameters.Add(prm)
                    .Parameters.Add(prm1)
                    .Parameters.Add(prm2)
                    .Parameters.Add(prm3)
                    .Parameters.Add(prm4)
                    .Parameters.Add(prm5)
                    .Parameters.Add(prm6)
                    .Parameters.Add(prm7)
                    .Parameters.Add(prm8)
                    .Parameters.Add(prm9)
                    .Parameters.Add(prm10)
                    .Parameters.Add(prm11)
                    .Parameters.Add(prm12)
                    .Parameters.Add(prm13)
                    .Parameters.Add(prm14)
                    .Parameters.Add(prm15)
                    .Parameters.Add(prm16)
                    .Parameters.Add(prm17)
                    .Parameters.Add(prm18)
                    .Parameters.Add(prm19)
                    .Parameters.Add(prm20)
                    .Parameters.Add(prm21)
                    .Parameters.Add(prm22)


                    Dim i As Integer = comando.ExecuteNonQuery()
                    status = prm1.Value
                    fecha_solicitud = prm2.Value
                    fecha_instalacio = prm3.Value
                    fecha_suspension = prm4.Value
                    fecha_baja = prm5.Value
                    fecha_Fuera_Area = prm6.Value
                    FECHA_ULT_PAGO = prm7.Value
                    PrimerMensualidad = prm8.Value
                    ultimo_mes = prm9.Value
                    ultimo_anio = prm10.Value
                    primerMesAnt = prm11.Value
                    statusAnt = prm12.Value
                    facturaAnt = prm13.Value
                    GENERAOSINSTA = prm14.Value
                    factura = prm15.Value
                    Clv_Vendedor = prm16.Value
                    Clv_Promocion = prm17.Value
                    Email = prm18.Value
                    Obs = prm19.Value
                    DESCRIPCION = prm20.Value
                    Cortesia = prm21.Value
                    Me.Label48.Text = prm22.Value


                End With
                Descuento = Me.DescuentoLabel1.Text
                con90.Close()

            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try


    End Sub
    'Private Sub damedatosnet2()
    '    If OpcionCli = "M" Then

    '        Locmarca = Me.MARCALabel1.Text
    '        LocTipoApar = Me.TIPOAPARATOLabel1.Text
    '        statusTarjeta = Me.ComboBox2.Text
    '        LocTipservcabl = Me.ComboBox3.Text
    '        LocTipoCablemodem = Me.ComboBox4.Text
    '        Obs = Me.ObsTextBox1.Text
    '        Activacion = Me.Fecha_ActivacionTextBox.Text
    '        Suspension = Me.Fecha_suspensionTextBox.Text
    '        LocTranspaso = Me.Fecha_TraspasoTextBox.Text
    '        Baja = Me.Fecha_bajaTextBox.Text

    '        If Me.Ventacablemodem1CheckBox.CheckState = CheckState.Checked Then
    '            Loc1pago = "True"
    '        ElseIf Me.Ventacablemodem1CheckBox.CheckState = CheckState.Unchecked Then
    '            Loc1pago = "False"
    '        End If

    '        If Me.Ventacablemodem2CheckBox.CheckState = CheckState.Checked Then
    '            Loc2pago = "True"
    '        ElseIf Me.Ventacablemodem2CheckBox.CheckState = CheckState.Unchecked Then
    '            Loc2pago = "False"
    '        End If

    '        If Me.SeRentaCheckBox.CheckState = CheckState.Checked Then
    '            serenta = "True"
    '        ElseIf Me.SeRentaCheckBox.CheckState = CheckState.Unchecked Then
    '            serenta = "False"
    '        End If

    '    End If
    'End Sub
    'Private Sub damedatosnet(ByVal clv_unicanet As Integer)
    '    Try
    '        If OpcionCli = "M" Then

    '            Dim comando As New SqlClient.SqlCommand
    '            Dim con90 As New SqlConnection(MiConexion)
    '            con90.Open()

    '            status = ""
    '            fecha_solicitud = ""
    '            fecha_instalacio = ""
    '            fecha_suspension = ""
    '            fecha_baja = ""
    '            fecha_Fuera_Area = ""
    '            FECHA_ULT_PAGO = ""
    '            PrimerMensualidad = False
    '            ultimo_mes = 0
    '            ultimo_anio = 0
    '            primerMesAnt = False
    '            statusAnt = ""
    '            facturaAnt = ""
    '            GENERAOSINSTA = False
    '            factura = ""
    '            Clv_Vendedor = 0
    '            Clv_Promocion = 0
    '            Email = ""
    '            Obs = ""
    '            DESCRIPCION = ""
    '            Cortesia = False



    '            comando = New SqlClient.SqlCommand

    '            With comando
    '                .Connection = con90
    '                .CommandText = "CONSULTACONTNET_Oledb"
    '                .CommandType = CommandType.StoredProcedure
    '                .CommandTimeout = 0

    '                '@CLV_UNICANET BIGINT, @status varchar(1) output, @fecha_solicitud varchar(20) output, @fecha_instalacio varchar(20) output, 
    '                '@fecha_suspension varchar(20) output , @fecha_baja varchar(20) output, @fecha_Fuera_Area varchar(20) output, @FECHA_ULT_PAGO varchar(max) output , 
    '                '@PrimerMensualidad bit output, @ultimo_mes int output, @ultimo_anio int output, @primerMesAnt bit output ,
    '                ' @statusAnt varchar(50) output, @facturaAnt varchar(50) output, @GENERAOSINSTA bit output, @factura varchar(50) output,
    '                ' @Clv_Vendedor int output, @Clv_Promocion int output, @Email varchar(50) output, @Obs varchar(250) output,
    '                '@DESCRIPCION varchar(250) output,@Cortesia bit output

    '                Dim prm As New SqlParameter("@CLV_UNICANET", SqlDbType.BigInt)
    '                Dim prm1 As New SqlParameter("@status", SqlDbType.VarChar, 1)
    '                Dim prm2 As New SqlParameter("@fecha_solicitud", SqlDbType.VarChar, 20)
    '                Dim prm3 As New SqlParameter("@fecha_instalacio", SqlDbType.VarChar, 20)
    '                Dim prm4 As New SqlParameter("@fecha_suspension", SqlDbType.VarChar, 20)
    '                Dim prm5 As New SqlParameter("@fecha_baja", SqlDbType.VarChar, 20)
    '                Dim prm6 As New SqlParameter("@fecha_Fuera_Area", SqlDbType.VarChar, 20)
    '                Dim prm7 As New SqlParameter("@FECHA_ULT_PAGO", SqlDbType.VarChar, 20)
    '                Dim prm8 As New SqlParameter("@PrimerMensualidad", SqlDbType.Bit)
    '                Dim prm9 As New SqlParameter("@ultimo_mes", SqlDbType.Int)
    '                Dim prm10 As New SqlParameter("@ultimo_anio", SqlDbType.Int)
    '                Dim prm11 As New SqlParameter("@primerMesAnt", SqlDbType.Bit)
    '                Dim prm12 As New SqlParameter("@statusAnt", SqlDbType.VarChar, 50)
    '                Dim prm13 As New SqlParameter("@facturaAnt", SqlDbType.VarChar, 50)
    '                Dim prm14 As New SqlParameter("@GENERAOSINSTA", SqlDbType.Bit)
    '                Dim prm15 As New SqlParameter("@factura", SqlDbType.VarChar, 50)
    '                Dim prm16 As New SqlParameter("@Clv_Vendedor", SqlDbType.Int)
    '                Dim prm17 As New SqlParameter("@Clv_Promocion", SqlDbType.Int)
    '                Dim prm18 As New SqlParameter("@Email", SqlDbType.VarChar, 50)
    '                Dim prm19 As New SqlParameter("@Obs", SqlDbType.VarChar, 250)
    '                Dim prm20 As New SqlParameter("@DESCRIPCION", SqlDbType.VarChar, 250)
    '                Dim prm21 As New SqlParameter("@Cortesia", SqlDbType.Bit)
    '                Dim prm22 As New SqlParameter("@motivo", SqlDbType.VarChar, 500)



    '                prm.Direction = ParameterDirection.Input
    '                prm1.Direction = ParameterDirection.Output
    '                prm2.Direction = ParameterDirection.Output
    '                prm3.Direction = ParameterDirection.Output
    '                prm4.Direction = ParameterDirection.Output
    '                prm5.Direction = ParameterDirection.Output
    '                prm6.Direction = ParameterDirection.Output
    '                prm7.Direction = ParameterDirection.Output
    '                prm8.Direction = ParameterDirection.Output
    '                prm9.Direction = ParameterDirection.Output
    '                prm10.Direction = ParameterDirection.Output
    '                prm11.Direction = ParameterDirection.Output
    '                prm12.Direction = ParameterDirection.Output
    '                prm13.Direction = ParameterDirection.Output
    '                prm14.Direction = ParameterDirection.Output
    '                prm15.Direction = ParameterDirection.Output
    '                prm16.Direction = ParameterDirection.Output
    '                prm17.Direction = ParameterDirection.Output
    '                prm18.Direction = ParameterDirection.Output
    '                prm19.Direction = ParameterDirection.Output
    '                prm20.Direction = ParameterDirection.Output
    '                prm21.Direction = ParameterDirection.Output
    '                prm22.Direction = ParameterDirection.Output



    '                prm.Value = clv_unicanet
    '                prm1.Value = " "
    '                prm2.Value = " "
    '                prm3.Value = " "
    '                prm4.Value = " "
    '                prm5.Value = " "
    '                prm6.Value = " "
    '                prm7.Value = " "
    '                prm8.Value = False
    '                prm9.Value = 0
    '                prm10.Value = 0
    '                prm11.Value = False
    '                prm12.Value = " "
    '                prm13.Value = " "
    '                prm14.Value = False
    '                prm15.Value = " "
    '                prm16.Value = 0
    '                prm17.Value = 0
    '                prm18.Value = " "
    '                prm19.Value = " "
    '                prm20.Value = " "
    '                prm21.Value = False
    '                prm22.Value = " "

    '                .Parameters.Add(prm)
    '                .Parameters.Add(prm1)
    '                .Parameters.Add(prm2)
    '                .Parameters.Add(prm3)
    '                .Parameters.Add(prm4)
    '                .Parameters.Add(prm5)
    '                .Parameters.Add(prm6)
    '                .Parameters.Add(prm7)
    '                .Parameters.Add(prm8)
    '                .Parameters.Add(prm9)
    '                .Parameters.Add(prm10)
    '                .Parameters.Add(prm11)
    '                .Parameters.Add(prm12)
    '                .Parameters.Add(prm13)
    '                .Parameters.Add(prm14)
    '                .Parameters.Add(prm15)
    '                .Parameters.Add(prm16)
    '                .Parameters.Add(prm17)
    '                .Parameters.Add(prm18)
    '                .Parameters.Add(prm19)
    '                .Parameters.Add(prm20)
    '                .Parameters.Add(prm21)
    '                .Parameters.Add(prm22)


    '                Dim i As Integer = comando.ExecuteNonQuery()
    '                status = prm1.Value
    '                fecha_solicitud = prm2.Value
    '                fecha_instalacio = prm3.Value
    '                fecha_suspension = prm4.Value
    '                fecha_baja = prm5.Value
    '                fecha_Fuera_Area = prm6.Value
    '                FECHA_ULT_PAGO = prm7.Value
    '                PrimerMensualidad = prm8.Value
    '                ultimo_mes = prm9.Value
    '                ultimo_anio = prm10.Value
    '                primerMesAnt = prm11.Value
    '                statusAnt = prm12.Value
    '                facturaAnt = prm13.Value
    '                GENERAOSINSTA = prm14.Value
    '                factura = prm15.Value
    '                Clv_Vendedor = prm16.Value
    '                Clv_Promocion = prm17.Value
    '                Email = prm18.Value
    '                Obs = prm19.Value
    '                DESCRIPCION = prm20.Value
    '                Cortesia = prm21.Value
    '                Me.Label49.Text = prm22.Value

    '            End With
    '            Descuento = Me.DescuentoLabel1.Text
    '            con90.Close()
    '        End If
    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try


    'End Sub

    Private Sub guardabitacoradig(ByVal op As Integer)
        Try
            Dim CON10 As New SqlConnection(MiConexion)
            Dim cmd As New SqlClient.SqlCommand
            If OpcionCli = "M" Then
                Dim valida1 As String = Nothing
                Dim Valida2 As String = Nothing
                Dim Valida3 As String = Nothing
                Dim valida4 As String = Nothing
                Dim valida5 As String = Nothing

                Select Case op
                    Case 0

                        cmd = New SqlClient.SqlCommand()
                        CON10.Open()
                        With cmd
                            .CommandText = "Dame_Mac_CableDeco"
                            .Connection = CON10
                            .CommandTimeout = 0
                            .CommandType = CommandType.StoredProcedure

                            '@contratonet bigint,@clv_unicanet bigint, @op int,@Mac varchar(max) output
                            Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
                            Dim prm1 As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
                            Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
                            Dim prm3 As New SqlParameter("@Mac", SqlDbType.VarChar, 200)

                            prm.Direction = ParameterDirection.Input
                            prm1.Direction = ParameterDirection.Input
                            prm2.Direction = ParameterDirection.Input
                            prm3.Direction = ParameterDirection.Output

                            prm.Value = LoContratonet
                            prm1.Value = 0
                            prm2.Value = 3
                            prm3.Value = ""

                            .Parameters.Add(prm)
                            .Parameters.Add(prm1)
                            .Parameters.Add(prm2)
                            .Parameters.Add(prm3)

                            Dim i As Integer = cmd.ExecuteNonQuery()

                            NombreMAC = prm3.Value

                        End With
                        CON10.Close()

                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Agrego un Servicio Digital Al Equipo: " + NombreMAC, " ", "Se agrego el Paquete Digital:" & GLoPaqueteAgrega, LocClv_Ciudad)

                        LocGloContratoNet = 0
                    Case 1
                        cmd = New SqlClient.SqlCommand()
                        CON10.Open()
                        With cmd
                            .CommandText = "Dame_Mac_CableDeco"
                            .Connection = CON10
                            .CommandTimeout = 0
                            .CommandType = CommandType.StoredProcedure

                            '@contratonet bigint,@clv_unicanet bigint, @op int,@Mac varchar(max) output
                            Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
                            Dim prm1 As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
                            Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
                            Dim prm3 As New SqlParameter("@Mac", SqlDbType.VarChar, 200)

                            prm.Direction = ParameterDirection.Input
                            prm1.Direction = ParameterDirection.Input
                            prm2.Direction = ParameterDirection.Input
                            prm3.Direction = ParameterDirection.Output

                            prm.Value = 0
                            prm1.Value = loc_Clv_InicaDig
                            prm2.Value = 4
                            prm3.Value = ""

                            .Parameters.Add(prm)
                            .Parameters.Add(prm1)
                            .Parameters.Add(prm2)
                            .Parameters.Add(prm3)

                            Dim i As Integer = cmd.ExecuteNonQuery()

                            NombreMAC = prm3.Value

                        End With
                        CON10.Close()


                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Elimino Un Servicio Digital Al Equipo: " + NombreMAC, " ", "Se Elimino el Paquete Digital:" & NombrePaqueteElimino, LocClv_Ciudad)

                    Case 2
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Elimino el Equipo Digital: " + NombrePaqueteElimino, " ", "Se Elimino el Equipo", LocClv_Ciudad)
                    Case 3

                        cmd = New SqlClient.SqlCommand()
                        CON10.Open()
                        With cmd
                            .CommandText = "Dame_Mac_CableDeco"
                            .Connection = CON10
                            .CommandTimeout = 0
                            .CommandType = CommandType.StoredProcedure

                            '@contratonet bigint,@clv_unicanet bigint, @op int,@Mac varchar(max) output
                            Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
                            Dim prm1 As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
                            Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
                            Dim prm3 As New SqlParameter("@Mac", SqlDbType.VarChar, 200)

                            prm.Direction = ParameterDirection.Input
                            prm1.Direction = ParameterDirection.Input
                            prm2.Direction = ParameterDirection.Input
                            prm3.Direction = ParameterDirection.Output

                            prm.Value = LoContratonet
                            prm1.Value = 0
                            prm2.Value = 3
                            prm3.Value = ""

                            .Parameters.Add(prm)
                            .Parameters.Add(prm1)
                            .Parameters.Add(prm2)
                            .Parameters.Add(prm3)

                            Dim i As Integer = cmd.ExecuteNonQuery()

                            NombreMAC = prm3.Value

                        End With
                        CON10.Close()

                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Agrego un Equipo Digital: " + NombreMAC, " ", "Se Agrego Un Equipo Digital", LocClv_Ciudad)

                    Case 4

                        'ContDig
                        cmd = New SqlClient.SqlCommand()
                        CON10.Open()
                        With cmd
                            .CommandText = "Dame_Mac_CableDeco"
                            .Connection = CON10
                            .CommandTimeout = 0
                            .CommandType = CommandType.StoredProcedure

                            '@contratonet bigint,@clv_unicanet bigint, @op int,@Mac varchar(max) output
                            Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
                            Dim prm1 As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
                            Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
                            Dim prm3 As New SqlParameter("@Mac", SqlDbType.VarChar, 200)

                            prm.Direction = ParameterDirection.Input
                            prm1.Direction = ParameterDirection.Input
                            prm2.Direction = ParameterDirection.Input
                            prm3.Direction = ParameterDirection.Output

                            prm.Value = 0
                            prm1.Value = LocGloClv_unianet
                            prm2.Value = GloOp
                            prm3.Value = ""

                            .Parameters.Add(prm)
                            .Parameters.Add(prm1)
                            .Parameters.Add(prm2)
                            .Parameters.Add(prm3)

                            Dim i As Integer = cmd.ExecuteNonQuery()

                            NombreMAC = prm3.Value

                        End With
                        CON10.Close()


                        cmd = New SqlClient.SqlCommand
                        CON10.Open()
                        With cmd
                            .CommandText = "Dame_Servicio_Cab_Deco"
                            .Connection = CON10
                            .CommandTimeout = 0
                            .CommandType = CommandType.StoredProcedure
                            '@clv_unicanet bigint, @op int,@Paquete varchar(max) output
                            Dim prm As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
                            Dim prm1 As New SqlParameter("@op", SqlDbType.Int)
                            Dim prm2 As New SqlParameter("@Paquete", SqlDbType.VarChar, 200)

                            prm.Direction = ParameterDirection.Input
                            prm1.Direction = ParameterDirection.Input
                            prm2.Direction = ParameterDirection.Output

                            prm.Value = LocGloClv_unianet
                            prm1.Value = GloOp
                            prm2.Value = ""

                            .Parameters.Add(prm)
                            .Parameters.Add(prm1)
                            .Parameters.Add(prm2)

                            Dim i As Integer = cmd.ExecuteNonQuery()

                            NombrePaqueteElimino = prm2.Value

                        End With
                        CON10.Close()


                        'status = prm1.Value
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Status: ", status, Me.ComboBox10.SelectedValue, LocClv_Ciudad)
                        'fecha_solicitud = prm2.Value
                        If fecha_solicitud = "01/01/1900" Then fecha_solicitud = ""
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Fecha de solicitud: ", fecha_solicitud, Me.TextBox16.Text, LocClv_Ciudad)
                        'fecha_instalacio = prm3.Value
                        If fecha_instalacio = "01/01/1900" Then fecha_instalacio = ""
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Fecha de instalacion: ", fecha_instalacio, Me.TextBox15.Text, LocClv_Ciudad)
                        'fecha_suspension = prm4.Value
                        If fecha_suspension = "01/01/1900" Then fecha_suspension = ""
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Fecha de suspension: ", fecha_suspension, Me.TextBox14.Text, LocClv_Ciudad)
                        'fecha_baja = prm5.Value
                        If fecha_baja = "01/01/1900" Then fecha_baja = ""
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Fecha De Baja: ", fecha_baja, Me.TextBox13.Text, LocClv_Ciudad)
                        'fecha_Fuera_Area = prm6.Value
                        If fecha_Fuera_Area = "01/01/1900" Then fecha_Fuera_Area = ""
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Fecha de Fuera Area: ", fecha_Fuera_Area, Me.TextBox12.Text, LocClv_Ciudad)
                        'FECHA_ULT_PAGO = prm7.Value
                        If FECHA_ULT_PAGO = "01/01/1900" Then FECHA_ULT_PAGO = ""
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Fecha De Ultimo Pago: ", FECHA_ULT_PAGO, Me.TextBox11.Text, LocClv_Ciudad)
                        'PrimerMensualidad = prm8.Value
                        If Me.CheckBox1.CheckState = CheckState.Checked Then
                            valida1 = "True"
                        ElseIf Me.CheckBox1.CheckState = CheckState.Unchecked Then
                            valida1 = "False"
                        End If
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Primer Mensualidad: ", PrimerMensualidad, valida1, LocClv_Ciudad)
                        'ultimo_mes = prm9.Value
                        If Me.TextBox3.Text = "" Or IsNumeric(Me.TextBox10.Text) = False Then
                            Valida2 = "0"
                        Else
                            Valida2 = Me.TextBox10.Text
                        End If
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Ultimo mes: ", ultimo_mes, Valida2, LocClv_Ciudad)
                        'ultimo_anio = prm10.Value
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Ultimo año: ", ultimo_anio, Me.TextBox9.Text, LocClv_Ciudad)
                        'primerMesAnt = prm11.Value
                        'statusAnt = prm12.Value
                        'facturaAnt = prm13.Value
                        'GENERAOSINSTA = prm14.Value
                        'factura = prm15.Value
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Factura: ", factura, Me.TextBox8.Text, LocClv_Ciudad)
                        'Clv_Vendedor = prm16.Value
                        If Me.ComboBox9.Text = "" Then
                            Valida3 = "0"
                        Else
                            Valida3 = CStr(Me.ComboBox9.SelectedValue)
                        End If
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Vendedor: ", Clv_Vendedor, Valida3, LocClv_Ciudad)
                        'Clv_Promocion = prm17.Value
                        If Me.ComboBox8.Text = "" Then
                            valida4 = "0"
                        Else
                            valida4 = CStr(Me.ComboBox8.SelectedValue)
                        End If
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Promocion: ", Clv_Promocion, valida4, LocClv_Ciudad)
                        'Email = prm18.Value
                        'Obs = prm19.Value
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Observaciones: ", Obs, Me.TextBox6.Text, LocClv_Ciudad)
                        'DESCRIPCION = prm20.Value
                        'Cortesia = prm21.Value
                        'If Me.CortesiaCheckBox.CheckState = CheckState.Checked Then
                        '    valida5 = "True"
                        'ElseIf Me.CortesiaCheckBox.CheckState = CheckState.Unchecked Then
                        '    valida5 = "False"
                        'End If
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Cortesia: ", Cortesia, valida5, LocClv_Ciudad)
                        'descuento
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Descuento: ", Descuento, Me.DescuentoLabel1.Text, LocClv_Ciudad)
                    Case 5
                        'ClientesDig

                        cmd = New SqlClient.SqlCommand()
                        CON10.Open()
                        With cmd
                            .CommandText = "Dame_Mac_CableDeco"
                            .Connection = CON10
                            .CommandTimeout = 0
                            .CommandType = CommandType.StoredProcedure

                            '@contratonet bigint,@clv_unicanet bigint, @op int,@Mac varchar(max) output
                            Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
                            Dim prm1 As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
                            Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
                            Dim prm3 As New SqlParameter("@Mac", SqlDbType.VarChar, 200)

                            prm.Direction = ParameterDirection.Input
                            prm1.Direction = ParameterDirection.Input
                            prm2.Direction = ParameterDirection.Input
                            prm3.Direction = ParameterDirection.Output

                            prm.Value = LocGloContratoNet
                            prm1.Value = 0
                            prm2.Value = GloOp
                            prm3.Value = ""

                            .Parameters.Add(prm)
                            .Parameters.Add(prm1)
                            .Parameters.Add(prm2)
                            .Parameters.Add(prm3)

                            Dim i As Integer = cmd.ExecuteNonQuery()

                            NombreMAC = prm3.Value

                        End With
                        CON10.Close()

                        'statusTarjeta = Me.ComboBox13.Text
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + "Status: ", statusTarjeta, Me.ComboBox13.Text, LocClv_Ciudad)
                        'Activacion = Me.TextBox33.Text
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + "Fecha de Activacion: ", Activacion, Me.TextBox33.Text, LocClv_Ciudad)
                        'Suspension = Me.TextBox32.Text
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + "Fecha De Suspension: ", Suspension, Me.TextBox32.Text, LocClv_Ciudad)
                        'Baja = Me.TextBox23.Text
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + "Fecha  De  Baja: ", Baja, Me.TextBox23.Text, LocClv_Ciudad)
                        'Obs = Me.TextBox29.Text
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + "Observaciones: ", Obs, Me.TextBox29.Text, LocClv_Ciudad)
                        'serenta
                        If Me.CheckBox2.CheckState = CheckState.Checked Then
                            valida1 = "True"
                        ElseIf Me.CheckBox2.CheckState = CheckState.Unchecked Then
                            valida1 = "False"
                        End If
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Tarjeta: " + NombreMAC + " - " + "Se Renta: ", serenta, valida1, LocClv_Ciudad)

                End Select
            ElseIf OpcionCli = "N" Then
                Select Case op
                    Case 0
                        LoContratonet = LocGloContratoNet
                        cmd = New SqlClient.SqlCommand()
                        CON10.Open()
                        With cmd
                            .CommandText = "Dame_Mac_CableDeco"
                            .Connection = CON10
                            .CommandTimeout = 0
                            .CommandType = CommandType.StoredProcedure

                            '@contratonet bigint,@clv_unicanet bigint, @op int,@Mac varchar(max) output
                            Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
                            Dim prm1 As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
                            Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
                            Dim prm3 As New SqlParameter("@Mac", SqlDbType.VarChar, 200)

                            prm.Direction = ParameterDirection.Input
                            prm1.Direction = ParameterDirection.Input
                            prm2.Direction = ParameterDirection.Input
                            prm3.Direction = ParameterDirection.Output

                            prm.Value = LoContratonet
                            prm1.Value = 0
                            prm2.Value = 3
                            prm3.Value = ""

                            .Parameters.Add(prm)
                            .Parameters.Add(prm1)
                            .Parameters.Add(prm2)
                            .Parameters.Add(prm3)

                            Dim i As Integer = cmd.ExecuteNonQuery()

                            NombreMAC = prm3.Value

                        End With
                        CON10.Close()

                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Agrego un Servicio Digital Al Equipo: " + NombreMAC, " ", "Se agrego el Paquete Digital:" & GLoPaqueteAgrega, LocClv_Ciudad)

                    Case 1
                        cmd = New SqlClient.SqlCommand()
                        CON10.Open()
                        With cmd
                            .CommandText = "Dame_Mac_CableDeco"
                            .Connection = CON10
                            .CommandTimeout = 0
                            .CommandType = CommandType.StoredProcedure

                            '@contratonet bigint,@clv_unicanet bigint, @op int,@Mac varchar(max) output
                            Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
                            Dim prm1 As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
                            Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
                            Dim prm3 As New SqlParameter("@Mac", SqlDbType.VarChar, 200)

                            prm.Direction = ParameterDirection.Input
                            prm1.Direction = ParameterDirection.Input
                            prm2.Direction = ParameterDirection.Input
                            prm3.Direction = ParameterDirection.Output

                            prm.Value = 0
                            prm1.Value = loc_Clv_InicaDig
                            prm2.Value = 4
                            prm3.Value = ""

                            .Parameters.Add(prm)
                            .Parameters.Add(prm1)
                            .Parameters.Add(prm2)
                            .Parameters.Add(prm3)

                            Dim i As Integer = cmd.ExecuteNonQuery()

                            NombreMAC = prm3.Value

                        End With
                        CON10.Close()


                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Elimino Un Servicio Digital Al Equipo: " + NombreMAC, " ", "Se Elimino el Paquete Digital:" & NombrePaqueteElimino, LocClv_Ciudad)


                    Case 2
                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Elimino un Equipo Digital: " + NombrePaqueteElimino, " ", "Se Elimino un Equipo Digital. ", LocClv_Ciudad)
                    Case 3
                        cmd = New SqlClient.SqlCommand()
                        CON10.Open()
                        With cmd
                            .CommandText = "Dame_Mac_CableDeco"
                            .Connection = CON10
                            .CommandTimeout = 0
                            .CommandType = CommandType.StoredProcedure

                            '@contratonet bigint,@clv_unicanet bigint, @op int,@Mac varchar(max) output
                            Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
                            Dim prm1 As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
                            Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
                            Dim prm3 As New SqlParameter("@Mac", SqlDbType.VarChar, 200)

                            prm.Direction = ParameterDirection.Input
                            prm1.Direction = ParameterDirection.Input
                            prm2.Direction = ParameterDirection.Input
                            prm3.Direction = ParameterDirection.Output

                            prm.Value = LoContratonet
                            prm1.Value = 0
                            prm2.Value = 3
                            prm3.Value = ""

                            .Parameters.Add(prm)
                            .Parameters.Add(prm1)
                            .Parameters.Add(prm2)
                            .Parameters.Add(prm3)

                            Dim i As Integer = cmd.ExecuteNonQuery()

                            NombreMAC = prm3.Value

                        End With
                        CON10.Close()

                        bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Agrego un Equipo Digital: " + NombreMAC, " ", "Se Agrego Un Nuevo Equipo Digital", LocClv_Ciudad)

                End Select

            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub UspBorraRelAparatosClientes(ByVal PRMCONTRATO As Long, ByVal PRMCONTRATONET As Long)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, PRMCONTRATO)
            BaseII.CreateMyParameter("@CONTRATONET", SqlDbType.BigInt, PRMCONTRATONET)
            BaseII.Inserta("UspBorraRelAparatosClientes")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Private Sub guardabitacoranet(ByVal op As Integer)
    '    Try
    '        Dim CON10 As New SqlConnection(MiConexion)
    '        Dim cmd As New SqlClient.SqlCommand()

    '        If OpcionCli = "M" Then
    '            Dim valida1 As String = Nothing
    '            Dim Valida2 As String = Nothing
    '            Dim Valida3 As String = Nothing
    '            Dim valida4 As String = Nothing
    '            Dim valida5 As String = Nothing

    '            Select Case op
    '                'Case 0
    '                '    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Agrego un Paquete Digital", " ", "Se agrego el Paquete Digital:" & GLoPaqueteAgrega, LocClv_Ciudad)
    '                'Case 1
    '                '    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Elimino un paquete Digital", " ", "Se Elimino el Paquete Digital:" & NombrePaqueteElimino, LocClv_Ciudad)
    '                Case 2


    '                    cmd = New SqlClient.SqlCommand()
    '                    CON10.Open()
    '                    With cmd
    '                        .CommandText = "Dame_Mac_CableDeco"
    '                        .Connection = CON10
    '                        .CommandTimeout = 0
    '                        .CommandType = CommandType.StoredProcedure

    '                        '@contratonet bigint,@clv_unicanet bigint, @op int,@Mac varchar(max) output
    '                        Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
    '                        Dim prm1 As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
    '                        Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
    '                        Dim prm3 As New SqlParameter("@Mac", SqlDbType.VarChar, 200)

    '                        prm.Direction = ParameterDirection.Input
    '                        prm1.Direction = ParameterDirection.Input
    '                        prm2.Direction = ParameterDirection.Input
    '                        prm3.Direction = ParameterDirection.Output

    '                        prm.Value = 0
    '                        prm1.Value = LocGloClv_unianet
    '                        prm2.Value = GloOp
    '                        prm3.Value = ""

    '                        .Parameters.Add(prm)
    '                        .Parameters.Add(prm1)
    '                        .Parameters.Add(prm2)
    '                        .Parameters.Add(prm3)

    '                        Dim i As Integer = cmd.ExecuteNonQuery()

    '                        NombreMAC = prm3.Value

    '                    End With
    '                    CON10.Close()


    '                    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Elimino Un Servicio De Internet: " + NombreMAC, " ", "Se Elimino El Servicio de Itnernet : " & NombrePaqueteElimino, LocClv_Ciudad)
    '                    'Case 3
    '                    '    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Agrego un Equipo Digital", " ", "Se Agrego Un Equipo Digital Con El ContratoNEt:" & GloContratoDig_Nuevo, LocClv_Ciudad)
    '                Case 4
    '                    'COntNet

    '                    cmd = New SqlClient.SqlCommand()
    '                    CON10.Open()
    '                    With cmd
    '                        .CommandText = "Dame_Mac_CableDeco"
    '                        .Connection = CON10
    '                        .CommandTimeout = 0
    '                        .CommandType = CommandType.StoredProcedure

    '                        '@contratonet bigint,@clv_unicanet bigint, @op int,@Mac varchar(max) output
    '                        Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
    '                        Dim prm1 As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
    '                        Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
    '                        Dim prm3 As New SqlParameter("@Mac", SqlDbType.VarChar, 200)

    '                        prm.Direction = ParameterDirection.Input
    '                        prm1.Direction = ParameterDirection.Input
    '                        prm2.Direction = ParameterDirection.Input
    '                        prm3.Direction = ParameterDirection.Output

    '                        prm.Value = 0
    '                        prm1.Value = LocGloClv_unianet
    '                        prm2.Value = GloOp
    '                        prm3.Value = ""

    '                        .Parameters.Add(prm)
    '                        .Parameters.Add(prm1)
    '                        .Parameters.Add(prm2)
    '                        .Parameters.Add(prm3)

    '                        Dim i As Integer = cmd.ExecuteNonQuery()

    '                        NombreMAC = prm3.Value

    '                    End With
    '                    CON10.Close()

    '                    cmd = New SqlClient.SqlCommand
    '                    CON10.Open()
    '                    With cmd
    '                        .CommandText = "Dame_Servicio_Cab_Deco"
    '                        .Connection = CON10
    '                        .CommandTimeout = 0
    '                        .CommandType = CommandType.StoredProcedure
    '                        '@clv_unicanet bigint, @op int,@Paquete varchar(max) output
    '                        Dim prm As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
    '                        Dim prm1 As New SqlParameter("@op", SqlDbType.Int)
    '                        Dim prm2 As New SqlParameter("@Paquete", SqlDbType.VarChar, 200)

    '                        prm.Direction = ParameterDirection.Input
    '                        prm1.Direction = ParameterDirection.Input
    '                        prm2.Direction = ParameterDirection.Output

    '                        prm.Value = LocGloClv_unianet
    '                        prm1.Value = GloOp
    '                        prm2.Value = ""

    '                        .Parameters.Add(prm)
    '                        .Parameters.Add(prm1)
    '                        .Parameters.Add(prm2)

    '                        Dim i As Integer = cmd.ExecuteNonQuery()

    '                        NombrePaqueteElimino = prm2.Value

    '                    End With
    '                    CON10.Close()

    '                    ''status = prm1.Value
    '                    'bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Status: ", status, Me.ComboBox5.SelectedValue, LocClv_Ciudad)
    '                    ''fecha_solicitud = prm2.Value
    '                    'If fecha_solicitud = "01/01/1900" Then fecha_solicitud = ""
    '                    'bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Fecha De solicitud: ", fecha_solicitud, Me.Fecha_solicitudTextBox1.Text, LocClv_Ciudad)
    '                    ''fecha_instalacio = prm3.Value
    '                    'If fecha_instalacio = "01/01/1900" Then fecha_instalacio = ""
    '                    'bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Fecha De Instalacion: ", fecha_instalacio, Me.Fecha_instalacioTextBox.Text, LocClv_Ciudad)
    '                    ''fecha_suspension = prm4.Value
    '                    'If fecha_suspension = "01/01/1900" Then fecha_suspension = ""
    '                    'bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Fecha De Suspension: ", fecha_suspension, Me.Fecha_suspensionTextBox.Text, LocClv_Ciudad)
    '                    ''fecha_baja = prm5.Value
    '                    'If fecha_baja = "01/01/1900" Then fecha_baja = ""
    '                    'bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Fecha De Baja: ", fecha_baja, Me.Fecha_bajaTextBox.Text, LocClv_Ciudad)
    '                    ''fecha_Fuera_Area = prm6.Value
    '                    'If fecha_Fuera_Area = "01/01/1900" Then fecha_Fuera_Area = ""
    '                    'bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Fecha De Fuera De Area: ", fecha_Fuera_Area, Me.Fecha_Fuera_AreaTextBox.Text, LocClv_Ciudad)
    '                    ''FECHA_ULT_PAGO = prm7.Value
    '                    'If FECHA_ULT_PAGO = "01/01/1900" Then FECHA_ULT_PAGO = ""
    '                    'bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Fecha De Ultimo Pago: ", FECHA_ULT_PAGO, Me.FECHA_ULT_PAGOTextBox1.Text, LocClv_Ciudad)
    '                    ''PrimerMensualidad = prm8.Value
    '                    'If Me.PrimerMensualidadCheckBox.CheckState = CheckState.Checked Then
    '                    '    valida1 = "True"
    '                    'ElseIf Me.PrimerMensualidadCheckBox.CheckState = CheckState.Unchecked Then
    '                    '    valida1 = "False"
    '                    'End If
    '                    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Primer Mensualidad: ", PrimerMensualidad, valida1, LocClv_Ciudad)
    '                    'ultimo_mes = prm9.Value
    '                    If Me.Ultimo_mesTextBox1.Text = "0" Or IsNumeric(Me.Ultimo_mesTextBox1.Text) = False Then
    '                        Valida2 = "0"
    '                    Else
    '                        Valida2 = Me.Ultimo_mesTextBox1.Text
    '                    End If
    '                    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Ultimo mes: ", ultimo_mes, Valida2, LocClv_Ciudad)
    '                    'ultimo_anio = prm10.Value
    '                    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Ultimo año: ", ultimo_anio, Me.Ultimo_anioTextBox1.Text, LocClv_Ciudad)
    '                    'primerMesAnt = prm11.Value
    '                    'statusAnt = prm12.Value
    '                    'facturaAnt = prm13.Value
    '                    'GENERAOSINSTA = prm14.Value
    '                    'factura = prm15.Value
    '                    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Factura: ", factura, Me.FacturaTextBox.Text, LocClv_Ciudad)
    '                    'Clv_Vendedor = prm16.Value
    '                    If Me.ComboBox6.Text = "" Then
    '                        Valida3 = "0"
    '                    Else
    '                        Valida3 = CStr(Me.ComboBox6.SelectedValue)
    '                    End If
    '                    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Vendedor: ", Clv_Vendedor, Valida3, LocClv_Ciudad)
    '                    'Clv_Promocion = prm17.Value
    '                    'If Me.ComboBox8.Text = "" Then
    '                    '    valida4 = "0"
    '                    'Else
    '                    '    valida4 = CStr(Me.ComboBox8.SelectedValue)
    '                    'End If
    '                    'bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Clv_Promocion" + "-" + NombrePaqueteElimino, Clv_Promocion, valida4, LocClv_Ciudad)
    '                    'Email = prm18.Value
    '                    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Email: ", Email, Me.EmailTextBox1.Text, LocClv_Ciudad)
    '                    'Obs = prm19.Value
    '                    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Observaciones: ", Obs, Me.ObsTextBox2.Text, LocClv_Ciudad)
    '                    'DESCRIPCION = prm20.Value
    '                    'Cortesia = prm21.Value
    '                    If Me.CortesiaCheckBox2.CheckState = CheckState.Checked Then
    '                        valida5 = "True"
    '                    ElseIf Me.CortesiaCheckBox2.CheckState = CheckState.Unchecked Then
    '                        valida5 = "False"
    '                    End If
    '                    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Cortesia: ", Cortesia, valida5, LocClv_Ciudad)
    '                    'descuento
    '                    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Descuento: ", Descuento, Me.DescuentoLabel3.Text, LocClv_Ciudad)
    '                Case 5

    '                    cmd = New SqlClient.SqlCommand()
    '                    CON10.Open()
    '                    With cmd
    '                        .CommandText = "Dame_Mac_CableDeco"
    '                        .Connection = CON10
    '                        .CommandTimeout = 0
    '                        .CommandType = CommandType.StoredProcedure

    '                        '@contratonet bigint,@clv_unicanet bigint, @op int,@Mac varchar(max) output
    '                        Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
    '                        Dim prm1 As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
    '                        Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
    '                        Dim prm3 As New SqlParameter("@Mac", SqlDbType.VarChar, 200)

    '                        prm.Direction = ParameterDirection.Input
    '                        prm1.Direction = ParameterDirection.Input
    '                        prm2.Direction = ParameterDirection.Input
    '                        prm3.Direction = ParameterDirection.Output

    '                        prm.Value = LocGloContratoNet
    '                        prm1.Value = 0
    '                        prm2.Value = GloOp
    '                        prm3.Value = ""

    '                        .Parameters.Add(prm)
    '                        .Parameters.Add(prm1)
    '                        .Parameters.Add(prm2)
    '                        .Parameters.Add(prm3)

    '                        Dim i As Integer = cmd.ExecuteNonQuery()

    '                        NombreMAC = prm3.Value

    '                    End With
    '                    CON10.Close()


    '                    'Locmarca = Me.MARCALabel1.Text
    '                    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Marca: ", Locmarca, Me.MARCALabel1.Text, LocClv_Ciudad)
    '                    'LocTipoApar = Me.TIPOAPARATOLabel1.Text
    '                    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Tipo de Aparato: ", LocTipoApar, Me.TIPOAPARATOLabel1.Text, LocClv_Ciudad)
    '                    'statusTarjeta = Me.ComboBox2.Text
    '                    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Status: ", statusTarjeta, Me.ComboBox2.Text, LocClv_Ciudad)
    '                    'LocTipservcabl = Me.ComboBox3.Text
    '                    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Tipo Servicio : ", LocTipservcabl, Me.ComboBox3.Text, LocClv_Ciudad)
    '                    'LocTipoCablemodem = Me.ComboBox4.Text
    '                    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Tipo de Cablemodem: ", LocTipoCablemodem, Me.ComboBox4.Text, LocClv_Ciudad)
    '                    'Obs = Me.ObsTextBox1.Text
    '                    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Observaciones: ", Obs, Me.ObsTextBox1.Text, LocClv_Ciudad)
    '                    'Activacion = Me.Fecha_ActivacionTextBox.Text
    '                    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Fecha De Activacion: ", Activacion, Me.Fecha_ActivacionTextBox.Text, LocClv_Ciudad)
    '                    'Suspension = Me.Fecha_suspensionTextBox.Text
    '                    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Fecha de Supension: ", Suspension, Me.Fecha_suspensionTextBox.Text, LocClv_Ciudad)
    '                    'LocTranspaso = Me.Fecha_TraspasoTextBox.Text
    '                    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Fecha De Transpaso: ", LocTranspaso, Me.Fecha_TraspasoTextBox.Text, LocClv_Ciudad)
    '                    'Baja = Me.Fecha_bajaTextBox.Text
    '                    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Fecha De Baja: ", Baja, Me.Fecha_bajaTextBox.Text, LocClv_Ciudad)

    '                    '1er Pago
    '                    If Me.Ventacablemodem1CheckBox.CheckState = CheckState.Checked Then
    '                        valida1 = "True"
    '                    ElseIf Me.Ventacablemodem1CheckBox.CheckState = CheckState.Unchecked Then
    '                        valida1 = "False"
    '                    End If

    '                    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Se Vende El Cablemodem 1er Pago: ", Loc1pago, valida1, LocClv_Ciudad)

    '                    '2do pago Loc2pago

    '                    If Me.Ventacablemodem2CheckBox.CheckState = CheckState.Checked Then
    '                        Valida2 = "True"
    '                    ElseIf Me.Ventacablemodem2CheckBox.CheckState = CheckState.Unchecked Then
    '                        Valida2 = "False"
    '                    End If

    '                    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Se Vende El Cablemodem 2do Pago: ", Loc2pago, Valida2, LocClv_Ciudad)

    '                    'Se Renta serenta
    '                    If Me.SeRentaCheckBox.CheckState = CheckState.Checked Then
    '                        Valida3 = "True"
    '                    ElseIf Me.SeRentaCheckBox.CheckState = CheckState.Unchecked Then
    '                        Valida3 = "False"
    '                    End If

    '                    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Se Renta El Cablemodem: ", serenta, Valida3, LocClv_Ciudad)
    '            End Select
    '        End If
    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try


    'End Sub
    Private Sub guardabitacora()
        Try
            If OpcionCli = "M" Then

                Dim solo_internet As String = Nothing
                Dim desglosa_iva As String = Nothing
                Dim clv_loc As String = Nothing
                'If bndbitacora = True Then
                '    bndbitacora = False
                '    dame_datos()
                'End If


                'Validaciones de nulos
                If IsDBNull(nombre_cliente) = True Then
                    nombre_cliente = ""
                End If
                If IsDBNull(Clv_Callebit) = True Then
                    Clv_Callebit = 0
                End If
                If IsDBNull(NUMERO_casabit) = True Then
                    NUMERO_casabit = ""
                End If
                If IsDBNull(CodigoPostalbit) = True Then
                    CodigoPostalbit = ""
                End If

                If IsDBNull(ENTRECALLESbit) = True Then
                    ENTRECALLESbit = ""
                End If

                If IsDBNull(Clv_Coloniabit) = True Then
                    Clv_Coloniabit = 0
                End If

                If IsDBNull(Clv_Periodobit) = True Then
                    Clv_Periodobit = 0
                End If

                If IsDBNull(clv_Ciudadbit) = True Then
                    clv_Ciudadbit = 0
                End If

                If IsDBNull(clv_sectorbit) = True Then
                    clv_sectorbit = 0
                End If

                'If IsDBNull(Me.ComboBox7.Tag) = True Then
                '    Me.ComboBox7.Tag = 0
                'End If    Tipo de Pago

                If IsDBNull(SoloInternetbit) = True Then
                    SoloInternetbit = 0
                End If

                If IsDBNull(DESGLOSA_Ivabit) = True Then
                    DESGLOSA_Ivabit = 0
                End If

                If IsDBNull(TELEFONObit) = True Then
                    TELEFONObit = ""
                End If
                If IsDBNull(CELULARbit) = True Then
                    CELULARbit = ""
                End If
                If IsDBNull(Email) = True Then
                    Email = ""
                End If

                If Me.SoloInternetCheckBox.CheckState = CheckState.Checked Then
                    solo_internet = "True"
                Else
                    solo_internet = "False"
                End If

                If Me.DESGLOSA_IvaCheckBox.CheckState = CheckState.Checked Then
                    desglosa_iva = "True"
                Else
                    desglosa_iva = "False"
                End If

                If Me.ComboBox11.Text = "" Then
                    clv_loc = "0"
                Else
                    clv_loc = CStr(Me.ComboBox11.SelectedValue)

                End If


                ' fin de validacion nulos
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Datos Generales Del Cliente" + " - " + " Nombre:", nombre_cliente, Me.NOMBRETextBox.Text, LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Datos Generales Del Cliente" + " - " + "Calle:", Clv_Callebit, CStr(Me.CALLEComboBox.SelectedValue), LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Datos Generales Del Cliente" + " - " + "Numero:", NUMERO_casabit, Me.NUMEROTextBox.Text, LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Datos Generales Del Cliente" + " - " + "Código Postal:", CodigoPostalbit, Me.CodigoPostalTextBox.Text, LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Datos Generales Del Cliente" + " - " + "Entre Calles:", ENTRECALLESbit, Me.ENTRECALLESTextBox.Text, LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Datos Generales Del Cliente" + " - " + "Colonia:", Clv_Coloniabit, CStr(Me.COLONIAComboBox.SelectedValue), LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Datos Generales Del Cliente" + " - " + "Periodo de corte:", Clv_Periodobit, Me.ComboBox15.SelectedValue, LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Datos Generales Del Cliente" + " - " + "Ciudad: ", clv_Ciudadbit, Me.CIUDADComboBox.SelectedValue, LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Datos Generales Del Cliente" + " - " + "Clave De Localizacion:", clv_sectorbit, clv_loc, LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Datos Generales Del Cliente" + " - " + "Tipo De Pago:", clv_tipo_pagobit, Me.ComboBox7.SelectedValue, LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Datos Generales Del Cliente" + " - " + "Solo Internet: ", SoloInternetbit, solo_internet, LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Datos Generales Del Cliente" + " - " + "Desglosa Iva:", DESGLOSA_Ivabit, desglosa_iva, LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Datos Generales Del Cliente" + " - " + "Telefono: ", TELEFONObit, Me.TELEFONOTextBox.Text, LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Datos Generales Del Cliente" + " - " + "Celular: ", CELULARbit, Me.CELULARTextBox.Text, LocClv_Ciudad)
                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Datos Generales Del Cliente" + " - " + "Email: ", Email, Me.EmailTextBox.Text, LocClv_Ciudad)

            ElseIf OpcionCli = "N" Then
                If IsNumeric(Me.CONTRATOTextBox.Text) = True Then
                    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Capturaron Los Datos Generales Del Cliente", " ", "Se Capturo Los Datos Generales Del Cliente", LocClv_Ciudad)
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub BindingNavigatorAddNewItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub



    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Me.CONSULTARCLIENTEBindingSource.CancelEdit()
    End Sub

    Private Sub CALLEComboBox_SelectedIndexChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CALLEComboBox.SelectedIndexChanged
        If IsNumeric(Me.CALLEComboBox.SelectedValue) = True Then
            If LocClv_Calletmp <> Me.CALLEComboBox.SelectedValue Then
                Me.asiganacalle()
                BndDClientes = True
            End If

            If Me.COLONIAComboBox.Items.Count > 0 Then
                If Me.COLONIAComboBox.SelectedValue > 0 Then
                    DAMESECTORORDENES(Me.COLONIAComboBox.SelectedValue)
                End If
            End If

        End If
    End Sub

    Private Sub COLONIAComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles COLONIAComboBox.SelectedIndexChanged
        Me.asignacolonia()
        If Me.COLONIAComboBox.Items.Count > 0 Then
            If Me.COLONIAComboBox.SelectedValue > 0 Then
                DAMESECTORORDENES(Me.COLONIAComboBox.SelectedValue)
            End If
        End If
    End Sub

    Private Sub CIUDADComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CIUDADComboBox.SelectedIndexChanged
        Me.asignaciudad()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        'Esto lo puso Eric
        GloPermisoCortesia = 0
        eResValida = 0
        If IsNumeric(Me.CONTRATOTextBox.Text) = False Then Me.CONTRATOTextBox.Text = 0
        ValidaDatosBancarios(CInt(Me.CONTRATOTextBox.Text), CLng(Me.ComboBox7.SelectedValue))
        'Genera_Ordenes_Logic(CInt(Me.CONTRATOTextBox.Text))
        If eResValida = 1 Then
            MsgBox(eMsgValida, MsgBoxStyle.Exclamation)
        End If

        Dime_Checa_Servicios_Cliente(Contrato)
        'CliTiene_Internet = 0
        'CliTiene_Tv = 0
        'CliTiene_Dig = 0
        'CliTiene_Tel = 0
        'If ContratoCombo = False And CliTiene_Internet = 1 And CliTiene_Tel = 0 And CliTiene_Tv = 0 And CliTiene_Dig = 0 And Me.SoloInternetCheckBox.Checked = False And Contrato > 0 Then

        '    Dim Resp = MsgBox("El Cliente solo cuenta con el Servicio de Internet, ¿Deseas guardarlo del Tipo: SOLO INTERNET?", MsgBoxStyle.YesNo, "El Sistema Detectó un Cliente de: Solo Internet")

        '    If Resp = DialogResult.No Then
        '        MsgBox("Puedes seleccionar otros servicios ó colocarle la palomita de SOLO INTERNET y proceder a guardarlo.", MsgBoxStyle.Information)
        '        Exit Sub

        '    ElseIf Resp = DialogResult.Yes Then
        '        If Me.SoloInternetCheckBox.CheckState = False Then

        '            'Colocamos la Palomita automáticamente
        '            Me.SoloInternetCheckBox.Checked = True

        '            'Guardamos el Cliente de Tipo Solo Internet
        '            HazClienteDeTipoSoloInternet(Contrato)
        '            'Deshabilitamos el botón de Telefonía para que no pueda agregar servicios, ya que no esta validado incialmente
        '            Me.Button28.Visible = False
        '        End If
        '    End If
        'End If
        Me.Close()
    End Sub

    Private Sub Genera_Ordenes_Logic(ByVal Contrato As Long)
        If IdSistema = "LO" Or IdSistema = "YU" Then
            If OpcionCli = "N" Or OpcionCli = "M" Then
                Dim con As New SqlConnection(MiConexion)
                Dim cmd As New SqlCommand()
                Try
                    cmd = New SqlCommand()
                    con.Open()
                    With cmd
                        .CommandText = "Genera_Ordenes"
                        .Connection = con
                        .CommandTimeout = 0
                        .CommandType = CommandType.StoredProcedure

                        Dim prm As New SqlParameter("@contrato", SqlDbType.BigInt)
                        prm.Direction = ParameterDirection.Input
                        prm.Value = Contrato
                        .Parameters.Add(prm)

                        Dim ia As Integer = .ExecuteNonQuery()
                    End With
                    con.Close()
                Catch ex As Exception
                    System.Windows.Forms.MessageBox.Show(ex.Message)
                End Try
            End If
        End If
    End Sub
    Private Sub SoloInternetCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SoloInternetCheckBox.CheckedChanged
        If Me.SoloInternetCheckBox.Checked = True And Me.SoloInternetCheckBox.Enabled = True And Me.SoloInternetCheckBox.Tag <> "N" Then
            If IsNumeric(Me.DigitalTextBox.Text) = False Then Me.DigitalTextBox.Text = 0
            If IsNumeric(Me.BasicoTextBox.Text) = False Then Me.BasicoTextBox.Text = 0
            If Me.DigitalTextBox.Text > 0 Or Me.BasicoTextBox.Text > 0 Then
                MsgBox("No Puede ser un Cliente de Solo Internet ya tiene otros Servicios ", MsgBoxStyle.Information)
                Me.SoloInternetCheckBox.Checked = False
                Exit Sub
            End If
            Me.Button8.Visible = True
            Me.Button8.Text = "&Internet"
            Me.Button7.Visible = False

            'Me.Panel2.Visible = False
            'Me.Panel7.Visible = False
            'Me.Panel4.Visible = True

            'Me.Panel2.Hide()
            'Me.Panel4.Hide()
            Me.Panel7.Hide()
            'Contrato = Me.CONTRATOTextBox.Text
            'frmctr.MdiParent = Me
            'frmctr.WindowState = FormWindowState.Normal
            'frmctr.Show()
            'frmInternet2.Show()
            'frmInternet2.Panel5.Enabled = False
            'frmInternet2.Panel6.Enabled = False
            'frmctr.TreeView1.ExpandAll()

            BndEsInternet = True
            frmctr.MdiParent = Me
            frmInternet2.MdiParent = Me
            frmctr.WindowState = FormWindowState.Normal
            frmInternet2.Show()
            frmInternet2.Panel5.Enabled = False
            frmInternet2.Panel6.Enabled = False
            frmctr.Show()
            frmctr.Boton_Internet()
            frmctr.TreeView1.ExpandAll()
            'Me.Button9.Enabled = True
            'Me.SplitContainer1.Enabled = True
            Me.Button11.Visible = False
        ElseIf Me.SoloInternetCheckBox.Enabled = False And (Me.Button8.Text = "&Televisión" Or Me.Button8.Text = "&Tv Digital" Or Me.Button8.Text = "&Premium") = False And Me.SoloInternetCheckBox.Tag <> "N" Then
            Me.Button8.Visible = True
            Me.Button8.Text = "&Internet"
            Me.Button7.Visible = False
            'Me.Panel2.Visible = False
            'Me.Panel7.Visible = False
            'Me.Panel4.Visible = True
            'Me.SplitContainer1.Enabled = True
            Me.Button11.Visible = False
            'Me.Panel2.Hide()
            'Me.Panel4.Hide()
            Me.Panel7.Hide()
            'Contrato = Me.CONTRATOTextBox.Text
            frmctr.MdiParent = Me
            frmctr.WindowState = FormWindowState.Normal
            frmctr.Show()
            frmctr.TreeView1.ExpandAll()
        ElseIf Me.SoloInternetCheckBox.Tag <> "N" Then


            'Me.SoloInternetCheckBox.Checked
            Me.HABILITA_CTRS()
        End If
    End Sub

    Public Sub CREAARBOL()

        'Try
        '    Dim I As Integer = 0
        '    Dim X As Integer = 0
        '    ' Assumes that customerConnection is a valid SqlConnection object.
        '    ' Assumes that orderConnection is a valid OleDbConnection object.
        '    'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
        '    '  "SELECT * FROM dbo.Customers", customerConnection)''
        '    Dim CON2 As New SqlConnection(MiConexion)
        '    CON2.Open()
        '    'Dim customerOrders As DataSet = New DataSet()
        '    'custAdapter.Fill(customerOrders, "Customers")
        '    ' 
        '    'Dim pRow, cRow As DataRow
        '    'For Each pRow In customerOrders.Tables("Customers").Rows
        '    ' Console.WriteLine(pRow("CustomerID").ToString())
        '    'Next
        '    Me.HaberServicios_CliTableAdapter.Connection = CON2
        '    Me.HaberServicios_CliTableAdapter.Fill(Me.NewSofTvDataSet.HaberServicios_Cli, New System.Nullable(Of Long)(CType(Contrato, Long)))
        '    Me.MUESTRACABLEMODEMSDELCLITableAdapter.Connection = CON2
        '    Me.MUESTRACABLEMODEMSDELCLITableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACABLEMODEMSDELCLI, New System.Nullable(Of Long)(CType(Contrato, Long)))
        '    Dim FilaRow As DataRow
        '    Dim FilacontNet As DataRow
        '    'Me.TreeView1.Nodes.Clear()
        '    'For Each FilaRow In Me.NewSofTvDataSet.MUESTRACABLEMODEMSDELCLI.Rows

        '    '    'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
        '    '    X = 0
        '    '    Me.TreeView1.Nodes.Add(Trim(FilaRow("CONTRATONET").ToString()), Trim(FilaRow("MACCABLEMODEM").ToString()))
        '    '    Me.TreeView1.Nodes(I).Tag = Trim(FilaRow("CONTRATONET").ToString())
        '    '    Me.MUESTRACONTNETTableAdapter.Connection = CON2
        '    '    Me.MUESTRACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTNET, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)))
        '    '    Me.TreeView1.Nodes(I).ForeColor = Color.Black
        '    '    For Each FilacontNet In Me.NewSofTvDataSet.MUESTRACONTNET.Rows
        '    '        Me.TreeView1.Nodes(I).Nodes.Add(Trim(FilacontNet("CLV_UNICANET").ToString()), Trim(FilacontNet("DESCRIPCION").ToString()) & Trim(FilacontNet("STATUS").ToString()))
        '    '        Me.TreeView1.Nodes(I).Nodes(X).Tag = Trim(FilacontNet("CLV_UNICANET").ToString())
        '    '        If Trim(FilacontNet("STATUS").ToString()) = "Suspendido" Then
        '    '            Me.TreeView1.Nodes(I).Nodes(X).ForeColor = Color.Olive
        '    '        ElseIf Trim(FilacontNet("STATUS").ToString()) = "Instalado" Or Trim(FilacontNet("STATUS").ToString()) = "Contratado" Then
        '    '            Me.TreeView1.Nodes(I).Nodes(X).ForeColor = Color.Navy
        '    '        Else
        '    '            Me.TreeView1.Nodes(I).Nodes(X).ForeColor = Color.Red
        '    '        End If
        '    '        X += 1
        '    '    Next
        '    '    I += 1
        '    'Next
        '    CON2.Close()
        '    'Me.TreeView1.ExpandAll()

        '    Me.CREAARBOLDIGITAL2()
        'Catch ex As System.Exception
        '    System.Windows.Forms.MessageBox.Show(ex.Message)
        'End Try

    End Sub

    Public Sub CREAARBOLDIGITAL2()


        Try

            If eClv_TipSer = 6 Then
                CREARARBOLDIGITALCABLE()
                Exit Sub
            End If

            Dim A As Integer = 0 'Antenas
            Dim I As Integer = 0 'Cajas
            Dim X As Integer = 0 'Paquetes
            Dim CON3 As New SqlConnection(MiConexion)

            Dim dTableClientesAnt As New DataTable
            Dim dRowClientesAnt As DataRow
            Dim dTableClientesDig As New DataTable
            Dim dRowClientesDig As DataRow


            CON3.Open()

            Me.HaberServicios_CliTableAdapter.Connection = CON3
            Me.HaberServicios_CliTableAdapter.Fill(Me.NewSofTvDataSet.HaberServicios_Cli, New System.Nullable(Of Long)(CType(Contrato, Long)))

            'Me.MUESTRADIGITALDELCLITableAdapter.Connection = CON3
            'Me.MUESTRADIGITALDELCLITableAdapter.Fill(Me.NewSofTvDataSet.MUESTRADIGITALDELCLI, New System.Nullable(Of Long)(CType(Contrato, Long)))
            MUESTRADIGITALDELCLI(Contrato)
            dTableClientesAnt = MuestraClientesAnt(Contrato)

            Dim FilaRow As DataRow
            Dim FilacontNet As DataRow
            Me.TreeView3.Nodes.Clear()

            For Each dRowClientesAnt In dTableClientesAnt.Rows

                I = 0

                Me.TreeView3.Nodes.Add(Trim(dRowClientesAnt("ContratoAnt").ToString()), Trim(dRowClientesAnt("MacCablemodem").ToString()))
                Me.TreeView3.Nodes(A).Tag = Trim(dRowClientesAnt("ContratoAnt").ToString())

                dTableClientesDig = MuestraClientesDig(Trim(dRowClientesAnt("ContratoAnt").ToString()))

                For Each dRowClientesDig In dTableClientesDig.Rows

                    X = 0
                    Me.TreeView3.Nodes(A).Nodes.Add(Trim(dRowClientesDig("ContratoNet").ToString()), Trim(dRowClientesDig("MacCablemodem").ToString()))
                    Me.TreeView3.Nodes(A).Nodes(I).Tag = Trim(dRowClientesDig("ContratoNet").ToString())

                    Me.MUESTRACONTDIGTableAdapter.Connection = CON3
                    Me.MUESTRACONTDIGTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTDIG, New System.Nullable(Of Long)(CType(Trim(dRowClientesDig("ContratoNet").ToString()), Long)))

                    Me.TreeView3.Nodes(A).Nodes(I).ForeColor = Color.Black

                    For Each FilacontNet In Me.NewSofTvDataSet.MUESTRACONTDIG.Rows
                        Me.TreeView3.Nodes(A).Nodes(I).Nodes.Add(Trim(FilacontNet("CLV_UNICANET").ToString()), Trim(FilacontNet("DESCRIPCION").ToString()) & " " & Trim(FilacontNet("STATUS").ToString()))
                        Me.TreeView3.Nodes(A).Nodes(I).Nodes(X).Tag = Trim(FilacontNet("CLV_UNICANET").ToString())
                        If Trim(FilacontNet("STATUS").ToString()) = "Suspendido" Then
                            Me.TreeView3.Nodes(A).Nodes(I).Nodes(X).ForeColor = Color.Olive
                        ElseIf Trim(FilacontNet("STATUS").ToString()) = "Instalado" Or Trim(FilacontNet("STATUS").ToString()) = "Contratado" Then
                            Me.TreeView3.Nodes(A).Nodes(I).Nodes(X).ForeColor = Color.Navy
                        Else
                            Me.TreeView3.Nodes(A).Nodes(I).Nodes(X).ForeColor = Color.Red
                        End If
                        X += 1
                    Next
                    I += 1
                Next
                A += 1
            Next

            If IsNumeric(ComboBox14.SelectedValue) = True Then
                If ComboBox14.SelectedValue > 0 Then
                    MuestraServicios_digital(eClv_TipSer, ComboBox14.SelectedValue)
                End If
            End If

            Me.TreeView3.ExpandAll()
            CON3.Close()

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub CREARARBOLDIGITALCABLE()
        Try
            Dim CON As New SqlConnection(MiConexion)
            Dim I As Integer = 0 'Cajas
            Dim X As Integer = 0 'Paquetes

            Dim dTableClientesDig As New DataTable
            Dim dRowClientesDig As DataRow
            Dim dTableContDig As New DataTable
            Dim dRowContDig As DataRow

            TreeView3.Nodes.Clear()

            MUESTRADIGITALDELCLICABLE(Contrato)
            dTableClientesDig = MuestraClientesDigCable(Contrato)

            For Each dRowClientesDig In dTableClientesDig.Rows
                X = 0
                Me.TreeView3.Nodes.Add(Trim(dRowClientesDig("ContratoNet").ToString()), Trim(dRowClientesDig("MacCablemodem").ToString()))
                Me.TreeView3.Nodes(I).Tag = Trim(dRowClientesDig("ContratoNet").ToString())

                dTableContDig = MUESTRACONTDIGCABLE(dRowClientesDig("ContratoNet").ToString())

                Me.TreeView3.Nodes(I).ForeColor = Color.Black

                For Each dRowContDig In dTableContDig.Rows
                    Me.TreeView3.Nodes(I).Nodes.Add(Trim(dRowContDig("CLV_UNICANET").ToString()), Trim(dRowContDig("DESCRIPCION").ToString()) & " " & Trim(dRowContDig("STATUS").ToString()))
                    Me.TreeView3.Nodes(I).Nodes(X).Tag = Trim(dRowContDig("CLV_UNICANET").ToString())
                    If Trim(dRowContDig("STATUS").ToString()) = "Suspendido" Then
                        Me.TreeView3.Nodes(I).Nodes(X).ForeColor = Color.Olive
                    ElseIf Trim(dRowContDig("STATUS").ToString()) = "Instalado" Or Trim(dRowContDig("STATUS").ToString()) = "Contratado" Then
                        Me.TreeView3.Nodes(I).Nodes(X).ForeColor = Color.Navy
                    Else
                        Me.TreeView3.Nodes(I).Nodes(X).ForeColor = Color.Red
                    End If
                    X += 1
                Next
                I += 1
            Next

            If IsNumeric(ComboBox14.SelectedValue) = True Then
                If ComboBox14.SelectedValue > 0 Then
                    MuestraServicios_digital(eClv_TipSer, ComboBox14.SelectedValue)
                End If
            End If

            Me.TreeView3.ExpandAll()

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub



    Public Sub CREAARBOLDIGITAL()

        'Try
        '    Dim I As Integer = 0
        '    Dim X As Integer = 0
        '    ' Assumes that customerConnection is a valid SqlConnection object.
        '    ' Assumes that orderConnection is a valid OleDbConnection object.
        '    'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
        '    '  "SELECT * FROM dbo.Customers", customerConnection)''

        '    'Dim customerOrders As DataSet = New DataSet()
        '    'custAdapter.Fill(customerOrders, "Customers")
        '    ' 
        '    'Dim pRow, cRow As DataRow
        '    'For Each pRow In customerOrders.Tables("Customers").Rows
        '    ' Console.WriteLine(pRow("CustomerID").ToString())
        '    'Next


        '    Me.MUESTRADIGITALDELCLITableAdapter.Fill(Me.NewSofTvDataSet.MUESTRADIGITALDELCLI, New System.Nullable(Of Long)(CType(Contrato, Long)))
        '    Dim FilaRow As DataRow
        '    Dim FilacontNet As DataRow
        '    Me.TreeView2.Nodes.Clear()
        '    For Each FilaRow In Me.NewSofTvDataSet.MUESTRADIGITALDELCLI.Rows

        '        'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
        '        X = 0
        '        Me.TreeView2.Nodes.Add(Trim(FilaRow("CONTRATONET").ToString()), Trim(FilaRow("MACCABLEMODEM").ToString()))
        '        Me.TreeView2.Nodes(I).Tag = Trim(FilaRow("CONTRATONET").ToString())
        '        Me.MUESTRACONTDIGTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTDIG, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)))
        '        Me.TreeView2.Nodes(I).ForeColor = Color.Black
        '        For Each FilacontNet In Me.NewSofTvDataSet.MUESTRACONTDIG.Rows
        '            Me.TreeView2.Nodes(I).Nodes.Add(Trim(FilacontNet("CLV_UNICANET").ToString()), Trim(FilacontNet("DESCRIPCION").ToString()) & " " & Trim(FilacontNet("STATUS").ToString()))
        '            Me.TreeView2.Nodes(I).Nodes(X).Tag = Trim(FilacontNet("CLV_UNICANET").ToString())
        '            If Trim(FilacontNet("STATUS").ToString()) = "Suspendido" Then
        '                Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Olive
        '            ElseIf Trim(FilacontNet("STATUS").ToString()) = "Instalado" Or Trim(FilacontNet("STATUS").ToString()) = "Contratado" Then
        '                Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Navy
        '            Else
        '                Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Red
        '            End If
        '            X += 1
        '        Next
        '        I += 1
        '    Next
        'Me.TreeView2.ExpandAll()
        Me.CREAARBOLDIGITAL2()



        'Catch ex As System.Exception
        '    System.Windows.Forms.MessageBox.Show(ex.Message)
        'End Try

    End Sub

    'Private Sub buscaCONCLIENTETV()
    '    Dim cont, ppal As Integer
    '    Try
    '        Dim CON As New SqlConnection(MiConexion)
    '        CON.Open()
    '        Me.CONCLIENTETVTableAdapter.Connection = CON
    '        Me.CONCLIENTETVTableAdapter.Fill(Me.NewSofTvDataSet.CONCLIENTETV, New System.Nullable(Of Long)(CType(Me.CONTRATOTextBox.Text, Long)))
    '        'Me.Dime_Si_ESMiniBasicoTableAdapter.Connection = CON
    '        'Me.Dime_Si_ESMiniBasicoTableAdapter.Fill(Me.DataSetEDGAR.Dime_Si_ESMiniBasico, New System.Nullable(Of Long)(CType(Me.CONTRATOTextBox.Text, Long)), BndMini)
    '        'If (IdSistema = "SA" Or IdSistema = "VA") And BndMini = 1 Then
    '        '    'Si el Cliente tiene un Servicio MiniBasico no Puede Tener Otros Servicios
    '        '    If Me.Button7.Visible = True Then Me.Button7.Enabled = False
    '        '    If Me.Button8.Visible = True Then Me.Button8.Enabled = False
    '        '    If Me.Button11.Visible = True Then Me.Button11.Enabled = False
    '        '    If Me.Button28.Visible = True Then Me.Button28.Enabled = False
    '        'End If
    '        If (Me.FacturaTextBox1.Text.Trim.Length) > 0 And GloTipoUsuario <> 40 Then
    '            Me.TipSerTvComboBox.Enabled = False
    '        End If
    '        Me.PrimerMesCLIENTESTableAdapter.Connection = CON
    '        Me.PrimerMesCLIENTESTableAdapter.Fill(Me.DataSetLidia.PrimerMesCLIENTES, Me.CONTRATOTextBox.Text, 2, cont, ppal)
    '        If cont = 1 Then
    '            Me.TVCONPAGONumericUpDown.Enabled = False
    '            Me.TVSINPAGONumericUpDown.Enabled = False
    '        End If
    '        Me.CONRel_ClientesTv_UsuariosTableAdapter.Connection = CON
    '        Me.CONRel_ClientesTv_UsuariosTableAdapter.Fill(Me.DataSetEDGAR.CONRel_ClientesTv_Usuarios, Me.CONTRATOTextBox.Text)
    '        Me.ConRel_ClientesTv_VendedorTableAdapter.Connection = CON
    '        Me.ConRel_ClientesTv_VendedorTableAdapter.Fill(Me.DataSetEDGAR.ConRel_ClientesTv_Vendedor, Me.CONTRATOTextBox.Text)
    '        CON.Close()
    '        CREAARBOL()

    '        'Eric-----------------DESCUENTO AL CLIENTE
    '        BuscaDescTV()
    '        '---------------------
    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try
    'End Sub



    'Private Sub hablitaserviciostv()

    '    Me.CONCLIENTETVBindingSource.AddNew()
    '    Me.ComboBox16.Text = ""
    '    Me.ComboBox16.SelectedValue = 0
    '    Me.ToolStripButton2.Enabled = True
    '    Me.ToolStripButton3.Enabled = True
    '    Me.CONTRATOTextBox1.Text = Me.CONTRATOTextBox.Text
    '    Me.Panel3.Enabled = True
    '    Me.PRIMERMENSUALIDACheckBox.Checked = True
    '    Me.STATUSTextBox.Text = "C"
    '    Me.FECHA_SOLICITUDTextBox.Text = Format(Now, "dd/MMM/yyyy")
    '    Me.Clv_TipoServicioTVTextBox.Text = 1
    '    Me.Clv_MOTCANTextBox.Text = 0
    '    'Me.TipSerTvComboBox.Text = "Basico"
    '    Me.MOTCANComboBox.SelectedValue = 0
    '    Me.MOTCANComboBox.Text = "Ninguno"
    '    Me.TipSerTvComboBox.SelectedIndex = 0
    '    If Me.MOTCANComboBox.Items.Count > 0 Then
    '        Me.MOTCANComboBox.SelectedIndex = 0
    '    End If

    'End Sub

    'Private Sub boton_BasicoTV()
    '    'Éstas Lines las puso Eric
    '    If Me.ComboBox1.Text.Length = 0 Then
    '        Me.TVSINPAGONumericUpDown.Enabled = True
    '        Me.TVCONPAGONumericUpDown.Enabled = True
    '    End If
    '    If Me.ComboBox1.Text.Length = 0 And OpcionCli = "M" Then
    '        'Me.TVSINPAGONumericUpDown.Enabled = False
    '        Me.Button16.Enabled = False
    '    End If


    '    Dim resp As MsgBoxResult = MsgBoxResult.Cancel
    '    Me.ToolStripButton4.Enabled = False
    '    If OpcionCli = "N" Then
    '        Me.TVCONPAGONumericUpDown.Enabled = True
    '    End If

    '    If IsNumeric(Me.CONTRATOTextBox.Text) = True Then
    '        If Me.CONTRATOTextBox.Text > 0 Then
    '            hablitaserviciostv()
    '            If Me.TipSerTvComboBox.SelectedValue > 0 Then
    '                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Agregó un Servicio de Televisión", "", "Se Agrego un Servicio de Televisión: " + Me.TipSerTvComboBox.Text, LocClv_Ciudad)
    '            End If

    '        Else
    '            resp = MsgBox("No sean Guardado los Datos Personales del Clientes. ¿ Deseas Guardar ?", MsgBoxStyle.OkCancel)
    '            If resp = MsgBoxResult.Ok Then
    '                guarda_Cliente()
    '                hablitaserviciostv()
    '                If Me.TipSerTvComboBox.SelectedValue > 0 Then
    '                    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Agregó un Servicio de Televisión", "", "Se Agrego un Servicio de Televisión: " + Me.TipSerTvComboBox.Text, LocClv_Ciudad)
    '                End If

    '            End If
    '        End If
    '    Else
    '        resp = MsgBox("No sean Guardado los Datos Personales del Clientes. ¿ Deseas Guardar ?", MsgBoxStyle.OkCancel)
    '        If resp = MsgBoxResult.Ok Then
    '            guarda_Cliente()
    '            hablitaserviciostv()
    '            If Me.TipSerTvComboBox.SelectedValue > 0 Then
    '                bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Agregó un Servicio de Televisión", "", "Se Agrego un Servicio de Televisión: " + Me.TipSerTvComboBox.Text, LocClv_Ciudad)
    '            End If

    '        End If
    '    End If

    '    'Éstas Líneas... así es, también las puso Eric
    '    Me.CortesiaCheckBox.Checked = False
    '    If GloTipoUsuario = 40 Then
    '        Me.CortesiaCheckBox.Enabled = True
    '    Else
    '        Me.TextBox2.Enabled = False
    '        Me.CortesiaCheckBox.Enabled = False
    '    End If

    'End Sub


    Private Sub ToolStripButton4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)


        DIME_SITELYNET(Contrato)
        If Dime_Si_Hay_Combos() = 1 Then
            If IdSistema = "LO" Or IdSistema = "YU" Then
                If gLONET_1 = 0 And gLOTEL_1 = 0 And CliTiene_Tv = 0 And CliTiene_Dig = 0 Then
                    Dim op As Integer = 0

                    op = MsgBox("Desea Contratar Un Combo", MsgBoxStyle.YesNo)
                    If op = 6 Then 'si Desea El Combo
                        LocContratoLog = CLng(Contrato)
                        FrmContratacionCombo.Show()
                        'Else
                        '    boton_BasicoTV()
                    End If
                    'Else
                    '    boton_BasicoTV()
                End If
                'Else
                '    boton_BasicoTV()
            End If

        End If


    End Sub

    'Private Sub ToolStripButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If IsNumeric(Me.CONTRATOTextBox.Text) = True Then
    '        If Me.CONTRATOTextBox.Text > 0 Then
    '            'VALIDA FECHAS==========================================
    '            If Me.ComboBox1.Text = "Contratado" Then
    '                If IsDate(Me.FECHA_SOLICITUDTextBox.Text) = False Then
    '                    MsgBox("Se Requiere que se Capture la Fecha de Contratación", MsgBoxStyle.Exclamation, "Atención")
    '                ElseIf CDate(Me.FECHA_SOLICITUDTextBox.Text) > CDate(Me.TextBox1.Text) Then
    '                    MsgBox("La Fecha de Contratación no puede ser Mayor a la Fecha Actual", MsgBoxStyle.Exclamation, "Atención")
    '                Else
    '                    GuardaTv()
    '                End If
    '            End If
    '            If Me.ComboBox1.Text = "Suspendido" Or Me.ComboBox1.Text = "Desconectado" Then
    '                If IsDate(Me.FECHA_CORTETextBox.Text) = False Then
    '                    MsgBox("Se Requiere que se Capture la Fecha de Suspensión o Desconexión", MsgBoxStyle.Exclamation, "Atención")
    '                ElseIf (CDate(Me.FECHA_SOLICITUDTextBox.Text) <= CDate(Me.FECHA_CORTETextBox.Text)) And (CDate(Me.FECHA_CORTETextBox.Text) <= CDate(Me.TextBox1.Text)) Then
    '                    GuardaTv()
    '                Else
    '                    MsgBox("La Fecha de Suspensión o Desconexión debe Encontrarse entre la Fecha de Contratción y la Fecha Actual", MsgBoxStyle.Exclamation, "Atención")
    '                End If
    '            End If
    '            If Me.ComboBox1.Text = "Instalado" Then
    '                If IsDate(Me.FECHA_INSTTextBox.Text) = False Then
    '                    MsgBox("Se Requiere que se Capture la Fecha de Instalación", MsgBoxStyle.Exclamation, "Atención")
    '                ElseIf Me.ULTIMO_MESTextBox.Text = "" Or CInt(Me.ULTIMO_MESTextBox.Text) > 12 Or CInt(Me.ULTIMO_MESTextBox.Text) = 0 Then
    '                    MsgBox("Se Requiere que Capture el Ùltimo Mes", MsgBoxStyle.Exclamation, "Atención")
    '                ElseIf (Me.ULTIMO_ANIOTextBox.Text).Length <> 4 Then
    '                    MsgBox("Se Requiere que Capture el Último Año", MsgBoxStyle.Exclamation, "Atención")
    '                ElseIf (CDate(Me.FECHA_SOLICITUDTextBox.Text) <= CDate(Me.FECHA_INSTTextBox.Text)) And (CDate(Me.FECHA_INSTTextBox.Text) <= CDate(Me.TextBox1.Text)) Then
    '                    GuardaTv()
    '                Else
    '                    MsgBox("La Fecha de Instalación debe Encontrarse entre la Fecha de Contratción y la Fecha Actual", MsgBoxStyle.Exclamation, "Atención")
    '                End If
    '            End If
    '            If Me.ComboBox1.Text = "Fuera de Area" Then
    '                If IsDate(Me.FECHACANCOUTAREATextBox.Text) = False Then
    '                    MsgBox("Se Requiere que se Capture la Fecha de Fuera de Área", MsgBoxStyle.Exclamation, "Atención")
    '                ElseIf (CDate(Me.FECHA_SOLICITUDTextBox.Text) <= CDate(Me.FECHACANCOUTAREATextBox.Text)) And (CDate(Me.FECHACANCOUTAREATextBox.Text) <= CDate(Me.TextBox1.Text)) Then
    '                    GuardaTv()
    '                Else
    '                    MsgBox("La Fecha de Fuera de Área debe Encontrarse entre la Fecha de Contratción y la Fecha Actual", MsgBoxStyle.Exclamation, "Atención")
    '                End If
    '            End If
    '            If Me.ComboBox1.Text = "Baja" Then
    '                If IsDate(Me.FECHA_CANCELACIOTextBox.Text) = False Then
    '                    MsgBox("Se Requiere que se Capture la Fecha de Baja", MsgBoxStyle.Information, "Atención")
    '                ElseIf (CDate(Me.FECHA_SOLICITUDTextBox.Text) <= CDate(Me.FECHA_CANCELACIOTextBox.Text)) And (CDate(Me.FECHA_CANCELACIOTextBox.Text) <= CDate(Me.TextBox1.Text)) Then
    '                    GuardaTv()

    '                Else
    '                    MsgBox("La Fecha de Baja debe Encontrarse entre la Fecha de Contratción y la Fecha Actual", MsgBoxStyle.Exclamation, "Atención")
    '                End If
    '            End If

    '            If Me.ComboBox1.Text = "Desconectado Temporal" Then
    '                If IsDate(Me.FECHA_CORTETextBox.Text) = False Then
    '                    MsgBox("Se Requiere que se Capture la Fecha de Corte de Servicio", MsgBoxStyle.Information, "Atención")
    '                ElseIf (CDate(Me.FECHA_SOLICITUDTextBox.Text) <= CDate(Me.FECHA_CORTETextBox.Text)) And (CDate(Me.FECHA_CORTETextBox.Text) <= CDate(Me.TextBox1.Text)) Then
    '                    GuardaTv()

    '                Else
    '                    MsgBox("La Fecha de Baja debe Encontrarse entre la Fecha de Contratción y la Fecha Actual", MsgBoxStyle.Exclamation, "Atención")
    '                End If
    '            End If

    '        Else
    '            MsgBox("Primero debe Guardar los Datos del Cliente", MsgBoxStyle.Information)
    '        End If
    '    Else
    '        MsgBox("Primero debe Guardar los Datos del Cliente", MsgBoxStyle.Information)
    '    End If
    '    'Actualiza_usuario(GloClvUnicaNet, CInt(Me.ComboBox18.SelectedValue), 1)
    'End Sub

    'Private Sub GuardaTv()
    '    Dim CON As New SqlConnection(MiConexion)
    '    CON.Open()

    '    Me.Validate()
    '    Me.CONCLIENTETVBindingSource.EndEdit()
    '    Me.CONCLIENTETVTableAdapter.Connection = CON
    '    Me.CONCLIENTETVTableAdapter.Update(Me.NewSofTvDataSet.CONCLIENTETV)
    '    Me.HaberServicios_CliTableAdapter.Connection = CON
    '    Me.HaberServicios_CliTableAdapter.Fill(Me.NewSofTvDataSet.HaberServicios_Cli, New System.Nullable(Of Long)(CType(Contrato, Long)))
    '    Me.GUARDARRel_ClientesTv_UsuariosTableAdapter.Connection = CON
    '    Me.GUARDARRel_ClientesTv_UsuariosTableAdapter.Fill(Me.DataSetEDGAR.GUARDARRel_ClientesTv_Usuarios, Me.CONTRATOTextBox.Text, GloClvUsuario)
    '    Me.Dime_Si_ESMiniBasicoTableAdapter.Connection = CON
    '    Me.Dime_Si_ESMiniBasicoTableAdapter.Fill(Me.DataSetEDGAR.Dime_Si_ESMiniBasico, New System.Nullable(Of Long)(CType(Me.CONTRATOTextBox.Text, Long)), BndMini)
    '    If Me.Button7.Text = "&Internet" Then
    '        Me.Button7.Enabled = True
    '    End If
    '    If Me.Button11.Text = "&Internet" Then
    '        Me.Button11.Enabled = True
    '        If (IdSistema = "SA" Or IdSistema = "VA") And BndMini = 0 Then
    '            Me.Button7.Enabled = True
    '        End If
    '    End If
    '    Me.DIMEQUEPERIODODECORTETableAdapter.Connection = CON
    '    Me.DIMEQUEPERIODODECORTETableAdapter.Fill(Me.DataSetLidia.DIMEQUEPERIODODECORTE, Me.CONTRATOTextBox.Text)
    '    Me.CONSULTARCLIENTETableAdapter.Connection = CON
    '    Me.CONSULTARCLIENTETableAdapter.Fill(Me.NewSofTvDataSet.CONSULTARCLIENTE, New System.Nullable(Of Long)(CType(Me.CONTRATOTextBox.Text, Long)))
    '    Me.Inserta_Rel_cortesia_FechaTableAdapter.Connection = CON
    '    Me.Inserta_Rel_cortesia_FechaTableAdapter.Fill(Me.Procedimientosarnoldo4.Inserta_Rel_cortesia_Fecha, CLng(Contrato), cortesiatv, 1)
    '    'guardabitacoratv()
    '    'damedatostv(Contrato)
    '    CON.Close()
    '    MsgBox("Se ha Guardado los Datos del Servicio de Television con Exíto", MsgBoxStyle.Information)
    'End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.CONCLIENTETVBindingSource.CancelEdit()
    End Sub

    'Private Sub TipSerTvComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Me.Clv_TipoServicioTVTextBox.Text = Me.TipSerTvComboBox.SelectedValue
    'End Sub

    Private Sub STATUSNOMComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    'Private Sub MOTCANComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Me.Clv_MOTCANTextBox.Text = Me.MOTCANComboBox.SelectedValue
    'End Sub

    Private Sub STATUSNOMComboBox_SelectedIndexChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    'Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Me.STATUSTextBox.Text = Me.ComboBox1.SelectedValue
    'End Sub

    'Private Sub STATUSTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Select Case Me.STATUSTextBox.Text
    '        Case "C"
    '            Me.ComboBox1.SelectedIndex = 0
    '        Case "I"
    '            Me.ComboBox1.SelectedIndex = 1
    '        Case "D"
    '            Me.ComboBox1.SelectedIndex = 2
    '        Case "S"
    '            Me.ComboBox1.SelectedIndex = 3
    '        Case "F"
    '            Me.ComboBox1.SelectedIndex = 4
    '        Case "B"
    '            Me.ComboBox1.SelectedIndex = 5
    '    End Select
    'End Sub

    'Private Sub Clv_TipoServicioTVTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_TipoServicioTVTextBox.TextChanged
    ' Me.TipSerTvComboBox.SelectedIndex = 0
    'End Sub

    'Private Sub FECHA_INSTTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try

    '        If DateValue(Me.FECHA_INSTTextBox.Text) = DateValue("01/01/1900") Then
    '            Me.FECHA_INSTTextBox.Text = ""

    '        End If
    '    Catch
    '    End Try
    'End Sub

    'Private Sub FECHA_CORTETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If DateValue(Me.FECHA_CORTETextBox.Text) = DateValue("01/01/1900") Then
    '            Me.FECHA_CORTETextBox.Text = ""
    '        End If
    '    Catch
    '    End Try
    'End Sub

    'Private Sub FECHACANCOUTAREATextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If DateValue(Me.FECHACANCOUTAREATextBox.Text) = DateValue("01/01/1900") Then
    '            Me.FECHACANCOUTAREATextBox.Text = ""
    '        End If
    '    Catch
    '    End Try
    'End Sub

    'Private Sub FECHA_CANCELACIOTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If DateValue(Me.FECHA_CANCELACIOTextBox.Text) = DateValue("01/01/1900") Then
    '            Me.FECHA_CANCELACIOTextBox.Text = ""
    '        End If
    '    Catch
    '    End Try
    'End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        If Me.Button8.Visible = True Then Me.Button8.Enabled = False
        If Me.Button7.Visible = True Then Me.Button7.Enabled = True
        If Me.Button11.Visible = True Then Me.Button11.Enabled = True
        If Me.Button28.Visible = True Then Me.Button28.Enabled = True


        If Me.Button8.Text = "&Internet" Then
            BndEsInternet = True
            frmctr.MdiParent = Me
            frmInternet2.MdiParent = Me
            frmctr.WindowState = FormWindowState.Normal
            frmInternet2.Show()
            frmctr.Show()
            frmctr.Boton_Internet()
            frmctr.TreeView1.ExpandAll()
            Me.Panel7.Hide()
        ElseIf Me.Button8.Text = "&Televisión" Then
            BndEsInternet = False
            Me.Panel7.Visible = False
            frmctr.Hide()
            frmInternet2.Hide()
        ElseIf Me.Button8.Text = "&Tv Digital" Or Me.Button8.Text = "&Premium" Or Me.Button8.Text = "&Tv Digital MMDS" Then
            eClv_TipSer = 3
            BndEsInternet = False
            CREAARBOLDIGITAL2()
            Panel7.Visible = True
            frmctr.Hide()
            frmInternet2.Hide()

            If IsNumeric(ComboBox14.SelectedValue) = True Then
                If ComboBox14.SelectedValue > 0 Then
                    MuestraServicios_digital(eClv_TipSer, ComboBox14.SelectedValue)
                End If
            End If
        ElseIf Me.Button8.Text = "Tv Digital &Cable" Then
            eClv_TipSer = 6
            BndEsInternet = False
            CREARARBOLDIGITALCABLE()
            Panel7.Visible = True
            frmctr.Hide()
            frmInternet2.Hide()
            If IsNumeric(ComboBox14.SelectedValue) = True Then
                If ComboBox14.SelectedValue > 0 Then
                    MuestraServicios_digital(eClv_TipSer, ComboBox14.SelectedValue)
                End If
            End If
        ElseIf Me.Button8.Text = "&Telefonia" Then
            muestra_telefonia()
        End If

        eEntraUM = True
        eEntraUMB = False

    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        If Me.Button8.Visible = True Then Me.Button8.Enabled = True
        If Me.Button7.Visible = True Then Me.Button7.Enabled = False
        If Me.Button11.Visible = True Then Me.Button11.Enabled = True
        If Me.Button28.Visible = True Then Me.Button28.Enabled = True
        eClv_TipSer = 6

        If Me.Button7.Text = "&Internet" Then
            BndEsInternet = True
            frmctr.MdiParent = Me
            frmInternet2.MdiParent = Me
            frmctr.WindowState = FormWindowState.Normal
            frmInternet2.Show()
            frmctr.Show()
            frmctr.Boton_Internet()
            frmctr.TreeView1.ExpandAll()
            Me.Panel7.Hide()
        ElseIf Me.Button7.Text = "&Televisión" Then
            BndEsInternet = False
            Me.Panel7.Visible = False
            frmctr.Hide()
            frmInternet2.Hide()
        ElseIf Me.Button7.Text = "&Tv Digital" Or Me.Button7.Text = "&Premium" Or Me.Button7.Text = "&Tv Digital MMDS" Then
            eClv_TipSer = 3
            BndEsInternet = False
            CREAARBOLDIGITAL2()
            Panel7.Visible = True
            frmctr.Hide()
            frmInternet2.Hide()

            If IsNumeric(ComboBox14.SelectedValue) = True Then
                If ComboBox14.SelectedValue > 0 Then
                    MuestraServicios_digital(eClv_TipSer, ComboBox14.SelectedValue)
                End If
            End If
        ElseIf Me.Button7.Text = "Tv Digital &Cable" Then
            eClv_TipSer = 6
            BndEsInternet = False
            CREARARBOLDIGITALCABLE()
            Panel7.Visible = True
            frmctr.Hide()
            frmInternet2.Hide()

            If IsNumeric(ComboBox14.SelectedValue) = True Then
                If ComboBox14.SelectedValue > 0 Then
                    MuestraServicios_digital(eClv_TipSer, ComboBox14.SelectedValue)
                End If
            End If
        ElseIf Me.Button7.Text = "&Telefonia" Then
            muestra_telefonia()
        End If

        eEntraUM = True
        eEntraUMB = False

        'If Me.Button7.Text = "&Internet" Then

        '    BndEsInternet = True
        '    frmctr.MdiParent = Me
        '    frmInternet2.MdiParent = Me
        '    frmctr.WindowState = FormWindowState.Normal
        '    frmInternet2.Show()
        '    frmctr.Show()
        '    frmctr.Boton_Internet()
        '    frmctr.TreeView1.ExpandAll()

        '    Me.Panel2.Hide()
        '    Me.Panel4.Hide()
        '    Me.Panel7.Hide()
        '    '--Controlar el Efecto de los botones de Television y Internet
        '    If Me.Button28.Enabled = False And Me.Button28.Visible = True And IdSistema = "LO" Then
        '        Me.Button28.Enabled = True
        '        Button8.Enabled = False
        '    End If
        '    If Me.Button28.Visible = True And Me.Button8.Visible = True Then
        '        Button8.Enabled = True
        '    End If

        '    Me.Button8.Enabled = True
        '    Me.Button7.Enabled = False
        '    Me.Button28.Enabled = True

        'ElseIf Me.Button7.Text = "&Tv Digital" Or Me.Button7.Text = "&Premium" Then
        '    BndEsInternet = False
        '    Me.Panel2.Visible = False
        '    Me.Panel4.Visible = False
        '    Me.Panel7.Visible = True
        '    frmctr.Hide()
        '    'No lo Usas frmTelefonia.Hide()
        '    frmInternet2.Hide()

        '    If IsNumeric(ComboBox14.SelectedValue) = True Then
        '        If ComboBox14.SelectedValue > 0 Then
        '            Dim CON As New SqlConnection(MiConexion)
        '            CON.Open()
        '            Me.MuestraServicios_digitalTableAdapter.Connection = CON
        '            Me.MuestraServicios_digitalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraServicios_digital, 3, Me.ComboBox14.SelectedValue)
        '            CON.Close()
        '            BuscaDescDig()
        '        End If
        '    End If
        'End If
        ''Eric----------
        'BuscaDescTV()
        ''--------------
        ''Estas dos Líneas las Puso Eric. Actualizan Las fechas.
        'eEntraUM = True
        'eEntraUMB = False

    End Sub

    Private Sub Clv_ColoniaTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_ColoniaTextBox.TextChanged
        Try

            If IsNumeric(Me.COLONIAComboBox.SelectedValue) = True And Me.COLONIAComboBox.SelectedValue > 0 Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.MuestraCVECOLCIUTableAdapter.Connection = CON
                Me.MuestraCVECOLCIUTableAdapter.Fill(Me.NewSofTvDataSet.MuestraCVECOLCIU, New System.Nullable(Of Integer)(CType(Me.COLONIAComboBox.SelectedValue, Integer)))
                CON.Close()
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub cargaviewtree()




    End Sub


    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim contv As Integer

        'Try
        ' Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, New System.Nullable(Of Long)(CType(CONTRATOToolStripTextBox.Text, Long)))
        ' Catch ex As System.Exception
        ' System.Windows.Forms.MessageBox.Show(ex.Message)
        ' End Try
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim resp As MsgBoxResult = MsgBoxResult.Cancel

            If GloGuardarNet = True Then
                MsgBox("Primero debe guardar el nuevo cable módem")
                Exit Sub
            End If
            If Contrato > 0 Then
                GLOMOVNET = 0
                GloClv_Cablemodem = 0
                GloClv_Servicio = 0
                GloClv_TipSer = 2
                If IdSistema <> "VA" Then
                    resp = MsgBox("¿ El Cliente cuenta Cablemodem Propio ? ", MsgBoxStyle.YesNoCancel)
                End If
                If resp = MsgBoxResult.Yes Then
                    'Linea de Eric
                    eCabModPropio = False
                    FrmCabModPropio.Show()
                    Me.CONTARCLIENTESTableAdapter.Connection = CON
                    Me.CONTARCLIENTESTableAdapter.Fill(Me.DataSetLidia.CONTARCLIENTES, GloContratoNet2, 0, contv)
                    'If contv = 0 Then
                    '    Me.SoloInternetCheckBox.Enabled = True
                    'Else
                    '    Me.SoloInternetCheckBox.Enabled = False
                    'End If
                ElseIf resp = MsgBoxResult.No Then
                    'Linea de Eric
                    eCabModPropio = True
                    resp = MsgBox("¿ El modem es Alámbrico ? ", MsgBoxStyle.YesNoCancel)
                    If resp = MsgBoxResult.Yes Then
                        GloTipoCablemodem = 1
                        ' Me.ComboBox4.Text = "Alambrico"
                    ElseIf resp = MsgBoxResult.No Then
                        GloTipoCablemodem = 2
                        ' Me.ComboBox4.Text = "Inalambrico"
                    End If

                    FrmSelServicios.Show()
                    'Me.SoloInternetCheckBox.Enabled = False
                    'Me.DIMEQUEPERIODODECORTETableAdapter.Connection = CON
                    ''Me.DIMEQUEPERIODODECORTETableAdapter.Fill(Me.DataSetLidia.DIMEQUEPERIODODECORTE, Me.CONTRATOTextBox.Text)
                    'Me.CONSULTARCLIENTETableAdapter.Connection = CON
                    'Me.CONSULTARCLIENTETableAdapter.Fill(Me.NewSofTvDataSet.CONSULTARCLIENTE, Me.CONTRATOTextBox.Text)
                    GLOMOVNET = 1
                Else
                    GLOMOVNET = 3
                End If
            Else
                Dim resp2 As MsgBoxResult = MsgBoxResult.Yes
                resp2 = MsgBox("No se han Guardado los Datos del Clientes ¿ Deseas Guardar ? ", MsgBoxStyle.YesNoCancel)
                If resp2 = MsgBoxResult.Yes Then
                    Me.asiganacalle()
                    Me.asignaciudad()
                    Me.asignacolonia()
                    If OpcionCli = "N" Then
                        If Len(Trim(Me.NOMBRETextBox.Text)) = 0 Then
                            MsgBox("Se Requiere el Nombre ", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        If IsNumeric(Me.Clv_CalleTextBox.Text) = False Then
                            MsgBox("Seleccione la Calle", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        If Len(Trim(Me.ENTRECALLESTextBox.Text)) = 0 Then
                            MsgBox("Se Requiere las Entre Calles ", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        If Len(Trim(Me.NUMEROTextBox.Text)) = 0 Then
                            MsgBox("Se Requiere el Numero ", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        If IsNumeric(Me.Clv_ColoniaTextBox.Text) = False Then
                            MsgBox("Seleccione la Colonia", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        If IsNumeric(Me.Clv_CiudadTextBox.Text) = False Then
                            MsgBox("Seleccione la Ciudad", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If

                    Me.Validate()
                    Me.CONSULTARCLIENTEBindingSource.EndEdit()
                    Me.CONSULTARCLIENTETableAdapter.Connection = CON
                    Me.CONSULTARCLIENTETableAdapter.Update(Me.NewSofTvDataSet.CONSULTARCLIENTE)

                    '--MsgBox("Se ha Guardado con Exíto", MsgBoxStyle.Information)
                End If
            End If
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub CONTRATOTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONTRATOTextBox.TextChanged
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            If IsNumeric(Me.CONTRATOTextBox.Text) = True Then
                Contrato = Me.CONTRATOTextBox.Text
                If Contrato > 0 Then
                    Me.HaberServicios_CliTableAdapter.Connection = CON
                    Me.HaberServicios_CliTableAdapter.Fill(Me.NewSofTvDataSet.HaberServicios_Cli, New System.Nullable(Of Long)(CType(Contrato, Long)))
                    GloContratoVer = Contrato
                    Me.Valida_SiahiOrdSerTableAdapter.Connection = CON
                    Me.Valida_SiahiOrdSerTableAdapter.Fill(Me.NewSofTvDataSet.Valida_SiahiOrdSer, New System.Nullable(Of Long)(CType(Contrato, Long)))
                    Me.Valida_SiahiQuejasTableAdapter.Connection = CON
                    Me.Valida_SiahiQuejasTableAdapter.Fill(Me.NewSofTvDataSet.Valida_SiahiQuejas, New System.Nullable(Of Long)(CType(Contrato, Long)))
                    Me.Activa_controles()
                    If Locbndguardar = True Then DAMETAPYSECTORCLIENTE(Me.CONTRATOTextBox.Text)
                Else
                    Me.BasicoTextBox.Text = 0
                    Me.InternetTextBox.Text = 0
                    Me.DigitalTextBox.Text = 0
                    GloContratoVer = 0
                    Me.Desactiva_controles()
                End If
            End If
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub TreeView1_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs)
        Dim habilita As String
        Dim contv As Integer
        'Muestra_Usuarios(GloClvUnicaNet, 2)
        'Me.Label5.Visible = False
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            If e.Node.Level = 0 Then
                If IsNumeric(e.Node.Tag) = True Then
                    NombrePaqueteElimino = e.Node.Text
                    LocGloContratoNet = e.Node.Tag
                    GloOp = 1

                    'Panel6.Enabled = True
                    'Panel5.Enabled = True
                    'Me.Panel6.Visible = False
                    'Me.Panel5.Visible = True
                    Me.VerAparatodelClienteTableAdapter.Connection = CON
                    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))
                    Me.CONSULTACLIENTESNETTableAdapter.Connection = CON
                    Me.CONSULTACLIENTESNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))
                    Me.DameFechaHabilitarTableAdapter.Connection = CON
                    Me.DameFechaHabilitarTableAdapter.Fill(Me.DataSetLidia.DameFechaHabilitar, CType(e.Node.Tag, Long), 1, habilita)
                    'Muestra_Usuarios(GloClvUnicaNet, 2)
                    'If habilita <> "01/01/1900" Then
                    '    Me.Label5.Visible = True
                    '    Me.Label5.Text = (" El Servicio es de Prueba y Vence en:" & " " & habilita)
                    '    Me.Label5.BackColor = Color.Yellow
                    '    Me.Label5.ForeColor = Color.Red
                    'End If
                    GloContratoNet2 = e.Node.Tag
                Else
                    Me.VerAparatodelClienteTableAdapter.Connection = CON
                    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                End If
                'damedatosnet2()
            Else
                'Panel6.Enabled = True
                'Panel5.Enabled = True
                'Me.Panel5.Visible = False
                'Me.Panel6.Visible = True


                If IsNumeric(e.Node.Tag) Then
                    cortesiaint = False
                    GloClvUnicaNet = e.Node.Tag
                    LocGloClv_unianet = GloClvUnicaNet
                    GloOp = 2
                    NombrePaqueteElimino = e.Node.Text
                    'damedatosnet(GloClvUnicaNet)
                    Me.CONSULTACONTNETTableAdapter.Connection = CON
                    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                    Me.CONRel_ContNet_UsuariosTableAdapter.Connection = CON
                    Me.CONRel_ContNet_UsuariosTableAdapter.Fill(Me.DataSetEDGAR.CONRel_ContNet_Usuarios, GloClvUnicaNet)

                    'Eric------------DESCUENTO AL CLIENTE
                    'BuscaDescNet()

                    '-----------------------------------

                Else
                    Me.CONSULTACONTNETTableAdapter.Connection = CON
                    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                    Me.CONRel_ContNet_UsuariosTableAdapter.Connection = CON
                    Me.CONRel_ContNet_UsuariosTableAdapter.Fill(Me.DataSetEDGAR.CONRel_ContNet_Usuarios, 0)
                End If



            End If
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try


    End Sub



    'Private Sub ToolStripButton6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If Me.ComboBox2.Text = "Instalado" Then
    '        If IsDate(Me.Fecha_ActivacionTextBox.Text) = False Then
    '            MsgBox("Se Requiere que se Capture la Fecha de Activación de Manera Correcta", MsgBoxStyle.Information)
    '        Else
    '            guardanet2()
    '        End If
    '    ElseIf Me.ComboBox2.Text = "Baja" Then
    '        If IsDate(Me.Fecha_bajaTextBox.Text) = False Then
    '            MsgBox("Se Requiere que se Capture la Fecha de Baja", MsgBoxStyle.Information)
    '        Else
    '            guardanet2()
    '        End If
    '    ElseIf Me.ComboBox2.Text = "Traspasado" Then
    '        If IsDate(Me.Fecha_TraspasoTextBox.Text) = False Then
    '            MsgBox("Se Requiere que se Capture la Fecha de Activación", MsgBoxStyle.Information)
    '        Else
    '            guardanet2()
    '        End If
    '    ElseIf Me.ComboBox2.Text = "Por Asignar" Then
    '        guardanet2()
    '    End If
    'End Sub
    'Private Sub guardanet2()
    '    Dim CON As New SqlConnection(MiConexion)
    '    CON.Open()
    '    Me.Validate()
    '    Me.CONSULTACLIENTESNETBindingSource.EndEdit()
    '    Me.CONSULTACLIENTESNETTableAdapter.Connection = CON
    '    Me.CONSULTACLIENTESNETTableAdapter.Update(Me.NewSofTvDataSet.CONSULTACLIENTESNET)
    '    guardabitacoranet(5)
    '    CON.Close()
    '    GuardaModificame_cablemodem_Internet()
    '    Me.CREAARBOL()
    '    MsgBox("Se ha Guardado con Exíto", MsgBoxStyle.Information)
    '    damedatosnet2()
    'End Sub

    Private Sub ToolStripButton5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.CONSULTACLIENTESNETBindingSource.CancelEdit()
    End Sub

    'Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Me.StatusTextBox1.Text = Me.ComboBox2.SelectedValue

    'End Sub

    'Private Sub ComboBox3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Me.Clv_TipoServicioTextBox.Text = ComboBox3.SelectedValue
    'End Sub

    'Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Me.Tipo_CablemodemTextBox.Text = ComboBox4.SelectedValue
    'End Sub

    'Private Sub ComboBox6_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Me.Clv_VendedorTextBox.Text = Me.ComboBox6.SelectedValue
    'End Sub

    'Private Sub ComboBox7_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    'Me.Clv_PromocionTextBox.Text = Me.ComboBox7.SelectedValue
    ' End Sub

    'Private Sub ToolStripButton8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    'VALIDACIONES DE FECHAS
    '    If Me.ComboBox5.Text = "Contratado" Then
    '        If IsDate(Me.Fecha_solicitudTextBox1.Text) = False Then
    '            MsgBox("Se Requiere que se Capture la Fecha de Contratación", MsgBoxStyle.Information, "Atención")
    '        ElseIf CDate(Me.Fecha_solicitudTextBox1.Text) > CDate(Me.TextBox1.Text) Then
    '            MsgBox("La Fecha de Contratación no puede ser Mayor a la Fecha Actual", MsgBoxStyle.Information, "Atención")
    '        Else
    '            guardanet()
    '        End If
    '    End If
    '    If Me.ComboBox5.Text = "Suspendido" Or Me.ComboBox5.Text = "Desconectado" Then
    '        If IsDate(Me.Fecha_suspensionTextBox.Text) = False Then
    '            MsgBox("Se Requiere que se Capture la Fecha de Suspensión o Desconexión", MsgBoxStyle.Information, "Atención")
    '        ElseIf (CDate(Me.Fecha_solicitudTextBox1.Text) <= CDate(Me.Fecha_suspensionTextBox.Text)) And (CDate(Me.Fecha_suspensionTextBox.Text) <= CDate(Me.TextBox1.Text)) Then
    '            guardanet()
    '        Else
    '            MsgBox("La Fecha de Suspensión o Desconexión debe Encontrarse entre la Fecha de Contratción y la Fecha Actual", MsgBoxStyle.Exclamation, "Atención")
    '        End If
    '    End If
    '    If Me.ComboBox5.Text = "Instalado" Then
    '        If IsDate(Me.Fecha_instalacioTextBox.Text) = False Then
    '            MsgBox("Se Requiere que se Capture la Fecha de Instalación", MsgBoxStyle.Information, "Atención")
    '        ElseIf Me.Ultimo_mesTextBox1.Text = "" Or CInt(Me.Ultimo_mesTextBox1.Text) = 0 Or CInt(Me.Ultimo_mesTextBox1.Text) > 12 Then
    '            MsgBox("Se Requiere que Capture el Último Mes", MsgBoxStyle.Exclamation, "Atención")
    '        ElseIf (Me.Ultimo_anioTextBox1.Text).Length <> 4 Then
    '            MsgBox("Se Requiere que Capture el Último Año", MsgBoxStyle.Exclamation, "Atención")
    '        ElseIf (CDate(Me.Fecha_solicitudTextBox1.Text) <= CDate(Me.Fecha_instalacioTextBox.Text)) And (CDate(Me.Fecha_instalacioTextBox.Text) <= CDate(Me.TextBox1.Text)) Then
    '            guardanet()
    '        Else
    '            MsgBox("La Fecha de Instalación debe Encontrarse entre la Fecha de Contratción y la Fecha Actual", MsgBoxStyle.Information, "Atención")

    '        End If
    '    End If
    '    If Me.ComboBox5.Text = "Fuera de Area" Then
    '        If IsDate(Me.Fecha_Fuera_AreaTextBox.Text) = False Then
    '            MsgBox("Se Requiere que se Capture la Fecha de Fuera de Área", MsgBoxStyle.Exclamation, "Atención")
    '        ElseIf (CDate(Me.Fecha_solicitudTextBox1.Text) <= CDate(Me.Fecha_Fuera_AreaTextBox.Text)) And (CDate(Me.Fecha_Fuera_AreaTextBox.Text) <= CDate(Me.TextBox1.Text)) Then
    '            guardanet()
    '        Else
    '            MsgBox("La Fecha de Fuera de Área debe Encontrarse entre la Fecha de Contratción y la Fecha Actual", MsgBoxStyle.Information, "Atención")
    '        End If
    '    End If
    '    If Me.ComboBox5.Text = "Baja" Then
    '        If IsDate(Me.Fecha_bajaTextBox.Text) = False Then
    '            MsgBox("Se Requiere que se Capture la Fecha de Baja", MsgBoxStyle.Exclamation, "Atención")
    '        ElseIf (CDate(Me.Fecha_solicitudTextBox1.Text) <= CDate(Me.Fecha_bajaTextBox.Text)) And (CDate(Me.Fecha_bajaTextBox.Text) <= CDate(Me.TextBox1.Text)) Then
    '            guardanet()
    '        Else
    '            MsgBox("La Fecha de Baja debe Encontrarse entre la Fecha de Contratción y la Fecha Actual", MsgBoxStyle.Information, "Atención")
    '        End If
    '    End If
    '    'Actualiza_usuario(GloClvUnicaNet, CInt(Me.ComboBox1.SelectedValue), 3)
    'End Sub
    'Private Sub guardanet()


    '    Dim CON As New SqlConnection(MiConexion)
    '    CON.Open()
    '    Me.Validate()
    '    Me.CONSULTACONTNETBindingSource.EndEdit()
    '    Me.CONSULTACONTNETTableAdapter.Connection = CON
    '    Me.CONSULTACONTNETTableAdapter.Update(Me.NewSofTvDataSet.CONSULTACONTNET)
    '    Me.Inserta_Rel_cortesia_FechaTableAdapter.Connection = CON
    '    Me.Inserta_Rel_cortesia_FechaTableAdapter.Fill(Me.Procedimientosarnoldo4.Inserta_Rel_cortesia_Fecha, GloClvUnicaNet, cortesiaint, 2)
    '    guardabitacoranet(4)
    '    CON.Close()
    '    Me.CREAARBOL()
    '    damedatosnet(GloClvUnicaNet)
    '    GloGuardarNet = False
    '    MsgBox("Se ha Guardado con Exíto", MsgBoxStyle.Information)
    'End Sub
    Private Sub ToolStripButton7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.CONSULTACONTNETBindingSource.CancelEdit()
    End Sub
    'Private Sub Fecha_ActivacionTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If DateValue(Me.Fecha_ActivacionTextBox.Text) = DateValue("01/01/1900") Then
    '            Me.Fecha_ActivacionTextBox.Text = ""
    '        End If
    '    Catch
    '    End Try
    'End Sub

    'Private Sub Fecha_SuspencionTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If DateValue(Me.Fecha_SuspencionTextBox.Text) = DateValue("01/01/1900") Then
    '            Me.Fecha_SuspencionTextBox.Text = ""
    '        End If
    '    Catch
    '    End Try
    'End Sub

    'Private Sub Fecha_TraspasoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If DateValue(Me.Fecha_TraspasoTextBox.Text) = DateValue("01/01/1900") Then
    '            Me.Fecha_TraspasoTextBox.Text = ""
    '        End If
    '    Catch
    '    End Try
    'End Sub

    'Private Sub Fecha_BajaTextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If DateValue(Me.Fecha_BajaTextBox1.Text) = DateValue("01/01/1900") Then
    '            Me.Fecha_BajaTextBox1.Text = ""
    '        End If
    '    Catch
    '    End Try
    'End Sub

    'Private Sub Fecha_solicitudTextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If DateValue(Me.Fecha_solicitudTextBox1.Text) = DateValue("01/01/1900") Then
    '            Me.Fecha_solicitudTextBox1.Text = ""
    '        End If
    '    Catch
    '    End Try
    'End Sub

    'Private Sub Fecha_instalacioTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If DateValue(Me.Fecha_instalacioTextBox.Text) = DateValue("01/01/1900") Then
    '            Me.Fecha_instalacioTextBox.Text = ""
    '        End If
    '    Catch
    '    End Try
    'End Sub

    'Private Sub Fecha_suspensionTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If DateValue(Me.Fecha_suspensionTextBox.Text) = DateValue("01/01/1900") Then
    '            Me.Fecha_suspensionTextBox.Text = ""
    '        End If
    '    Catch
    '    End Try
    'End Sub

    'Private Sub Fecha_bajaTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If DateValue(Me.Fecha_bajaTextBox.Text) = DateValue("01/01/1900") Then
    '            Me.Fecha_bajaTextBox.Text = ""
    '        End If
    '    Catch
    '    End Try
    'End Sub

    'Private Sub Fecha_Fuera_AreaTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If DateValue(Me.Fecha_Fuera_AreaTextBox.Text) = DateValue("01/01/1900") Then
    '            Me.Fecha_Fuera_AreaTextBox.Text = ""
    '        End If
    '    Catch
    '    End Try
    'End Sub

    'Private Sub FECHA_ULT_PAGOTextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If DateValue(Me.FECHA_ULT_PAGOTextBox1.Text) = DateValue("01/01/1900") Then
    '            Me.FECHA_ULT_PAGOTextBox1.Text = ""
    '        End If
    '    Catch
    '    End Try
    'End Sub





    Private Sub Manda_Agrega_Cablemodem()
        'Try
        ' Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, New System.Nullable(Of Long)(CType(CONTRATOToolStripTextBox.Text, Long)))
        ' Catch ex As System.Exception
        ' System.Windows.Forms.MessageBox.Show(ex.Message)
        ' End Try
        Try
            Dim CON As New SqlConnection(MiConexion)
            Dim cmd As New SqlClient.SqlCommand



            Dim resp As MsgBoxResult = MsgBoxResult.Cancel
            If Contrato > 0 Then
                If GloGuardarDig = True Then
                    MsgBox("Primero asigne el paquete y guarde ", MsgBoxStyle.Information)
                    Exit Sub
                End If
                GLOMOVDIG = 1
                GloClv_Cablemodem = 0
                'Me.BindingNavigator6.Enabled = True
                Dim LOCCLV_CABLEMODEM As Integer = 0
                LOCCLV_CABLEMODEM = GloClv_Cablemodem
                If GLOMOVDIG = 1 Then
                    GloGuardarDig = True
                    If Me.Button8.Text = "&Tv Digital" Or Me.Button8.Text = "&Premium" Then
                        Me.Button7.Enabled = True
                    End If
                    'If Me.Button8.Text = "&Television" Then
                    '    Me.Button7.Enabled = True
                    'End If
                    'Me.CONSULTACLIENTESDIGTableAdapter.Connection = CON
                    'Me.CONSULTACLIENTESDIGTableAdapter.Insert(Contrato, "P", LOCCLV_CABLEMODEM, 0, False, False, "01/01/1900", "01/01/1900", "01/01/1900", "", True, LoContratonet)

                    CON.Open()
                    With cmd
                        .CommandText = "NUEVOCLIENTESDIG"
                        .Connection = CON
                        .CommandTimeout = 0
                        .CommandType = CommandType.StoredProcedure
                        '@Contrato bigint, @Status varchar(1), @Clv_CableModem int, @Clv_Usuario int,
                        ' @ventacablemodem1 bit, @ventacablemodem2 bit,  @Fecha_Activacion datetime, 
                        '@Fecha_Suspencion datetime, @Fecha_Baja datetime,@Obs varchar(150),@SeRenta bit,@CONTRATONET BIGINT OUTPUT

                        Dim prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                        Dim prm1 As New SqlParameter("@Status", SqlDbType.VarChar, 1)
                        Dim prm2 As New SqlParameter("@clv_cablemodem", SqlDbType.Int)
                        Dim prm3 As New SqlParameter("@clv_usuario", SqlDbType.Int)
                        Dim prm4 As New SqlParameter("@ventacablemodem1", SqlDbType.Bit)
                        Dim prm5 As New SqlParameter("@ventacablemodem2", SqlDbType.Bit)
                        Dim prm6 As New SqlParameter("@Fecha_Activacion", SqlDbType.DateTime)
                        Dim prm7 As New SqlParameter("@Fecha_suspencion", SqlDbType.DateTime)
                        Dim prm8 As New SqlParameter("@Fecha_baja", SqlDbType.DateTime)
                        Dim prm9 As New SqlParameter("@Obs", SqlDbType.VarChar, 150)
                        Dim prm10 As New SqlParameter("@SeRenta", SqlDbType.Bit)
                        Dim prm11 As New SqlParameter("@ContratoNet", SqlDbType.BigInt)

                        prm.Direction = ParameterDirection.Input
                        prm1.Direction = ParameterDirection.Input
                        prm2.Direction = ParameterDirection.Input
                        prm3.Direction = ParameterDirection.Input
                        prm4.Direction = ParameterDirection.Input
                        prm5.Direction = ParameterDirection.Input
                        prm6.Direction = ParameterDirection.Input
                        prm7.Direction = ParameterDirection.Input
                        prm8.Direction = ParameterDirection.Input
                        prm9.Direction = ParameterDirection.Input
                        prm10.Direction = ParameterDirection.Input
                        prm11.Direction = ParameterDirection.Output

                        'Contrato, "P", LOCCLV_CABLEMODEM, 0, False, False, "01/01/1900", "01/01/1900", "01/01/1900",
                        ' "", True, LoContratonet
                        prm.Value = Contrato
                        prm1.Value = "P"
                        prm2.Value = LOCCLV_CABLEMODEM
                        prm3.Value = 0
                        prm4.Value = False
                        prm5.Value = False
                        prm6.Value = "01/01/1900"
                        prm7.Value = "01/01/1900"
                        prm8.Value = "01/01/1900"
                        prm9.Value = ""
                        prm10.Value = True
                        prm11.Value = 0

                        .Parameters.Add(prm)
                        .Parameters.Add(prm1)
                        .Parameters.Add(prm2)
                        .Parameters.Add(prm3)
                        .Parameters.Add(prm4)
                        .Parameters.Add(prm5)
                        .Parameters.Add(prm6)
                        .Parameters.Add(prm7)
                        .Parameters.Add(prm8)
                        .Parameters.Add(prm9)
                        .Parameters.Add(prm10)
                        .Parameters.Add(prm11)

                        Dim i As Integer = cmd.ExecuteNonQuery()

                        LoContratonet = prm11.Value
                    End With
                    CON.Close()

                    'Miguel Alemán----------------------------------------------------------------------------------------------------
                    If eClv_TipSer = 3 Then
                        NueClientesAnt(Contrato, LoContratonet)
                    End If
                    NUEClientesDigTipSer(LoContratonet, eClv_TipSer)
                    '-------------------------------------------------------------------------------------------------------------------------

                    guardabitacoradig(3)


                    GloContratoDig_Nuevo = LoContratonet
                    CON.Open()
                    Me.CONSULTACLIENTESDIGTableAdapter.Connection = CON
                    Me.CONSULTACLIENTESDIGTableAdapter.Update(Me.NewSofTvDataSet.CONSULTACLIENTESDIG)
                    Me.CREAARBOLDIGITAL2()
                    Me.MUESTRA_A_DIGITALTableAdapter.Connection = CON
                    Me.MUESTRA_A_DIGITALTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRA_A_DIGITAL)
                    Muestra_Usuarios(loc_Clv_InicaDig, 3)

                    CON.Close()
                    'Me.SoloInternetCheckBox.Enabled = False
                    'Aqui Wey


                    Me.CONTRATONETTextBox2.Text = 0
                    Me.MACCABLEMODEMTextBox.Text = ""
                    CON.Open()
                    Me.MUESTRADIGITALDELCLI_porAparatoTableAdapter.Connection = CON
                    Me.MUESTRADIGITALDELCLI_porAparatoTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRADIGITALDELCLI_porAparato, New System.Nullable(Of Long)(CType(Contrato, Long)), New System.Nullable(Of Long)(CType(LoContratonet, Long)))
                    CON.Close()
                    Me.ComboBox14.Text = Me.MACCABLEMODEMTextBox.Text
                    Me.ComboBox14.SelectedValue = LoContratonet
                    Me.ComboBox14.FindStringExact(Me.MACCABLEMODEMTextBox.Text, 0)
                    'MsgBox(Me.ContratoNetTextBox.Text, MsgBoxStyle.Information)
                    'MsgBox(Me.ContratoNetTextBox1.Text, MsgBoxStyle.Information)

                    'Me.CONTRATONETTextBox2.Text = 0
                    'Me.MACCABLEMODEMTextBox.Text = ""
                    'LOCCLV_CABLEMODEM = 0
                    'GloClv_Cablemodem = 0
                End If
            Else
                Dim resp2 As MsgBoxResult = MsgBoxResult.Yes
                resp2 = MsgBox("No se han Guardado los Datos del Clientes ¿ Deseas Guardar ? ", MsgBoxStyle.YesNoCancel)
                If resp2 = MsgBoxResult.Yes Then
                    Me.asiganacalle()
                    Me.asignaciudad()
                    Me.asignacolonia()
                    If OpcionCli = "N" Then
                        If Len(Trim(Me.NOMBRETextBox.Text)) = 0 Then
                            MsgBox("Se Requiere el Nombre ", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        If IsNumeric(Me.Clv_CalleTextBox.Text) = False Then
                            MsgBox("Seleccione la Calle", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        If Len(Trim(Me.ENTRECALLESTextBox.Text)) = 0 Then
                            MsgBox("Se Requiere las Entre Calles ", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        If Len(Trim(Me.NUMEROTextBox.Text)) = 0 Then
                            MsgBox("Se Requiere el Numero ", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        If IsNumeric(Me.Clv_ColoniaTextBox.Text) = False Then
                            MsgBox("Seleccione la Colonia", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        If IsNumeric(Me.Clv_CiudadTextBox.Text) = False Then
                            MsgBox("Seleccione la Ciudad", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If

                    Me.Validate()
                    Me.CONSULTARCLIENTEBindingSource.EndEdit()
                    CON.Open()
                    Me.CONSULTARCLIENTETableAdapter.Connection = CON
                    Me.CONSULTARCLIENTETableAdapter.Update(Me.NewSofTvDataSet.CONSULTARCLIENTE)
                    CON.Close()
                    '--MsgBox("Se ha Guardado con Exíto", MsgBoxStyle.Information)
                End If
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub TreeView2_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs)
        Try
            If e.Node.Level = 0 Then
                If IsNumeric(e.Node.Tag) = True Then
                    lOC_CONTRATONETDIG = e.Node.Tag
                Else
                    lOC_CONTRATONETDIG = 0
                End If
            Else
                lOC_CONTRATONETDIG = 0
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click

        If Me.Button8.Visible = True Then Me.Button8.Enabled = True
        If Me.Button7.Visible = True Then Me.Button7.Enabled = True
        If Me.Button11.Visible = True Then Me.Button11.Enabled = False
        If Me.Button28.Visible = True Then Me.Button28.Enabled = True


        If Me.Button11.Text = "&Internet" Then
            BndEsInternet = True
            frmctr.MdiParent = Me
            frmInternet2.MdiParent = Me
            frmctr.WindowState = FormWindowState.Normal
            frmInternet2.Show()
            frmctr.Show()
            frmctr.Boton_Internet()
            frmctr.TreeView1.ExpandAll()
            Me.Panel7.Hide()

        ElseIf Me.Button11.Text = "&Televisión" Then
            BndEsInternet = False
            Me.Panel7.Visible = False
            frmctr.Hide()
            frmInternet2.Hide()
        ElseIf Me.Button11.Text = "&Tv Digital" Or Me.Button11.Text = "&Premium" Or Me.Button11.Text = "&Tv Digital MMDS" Then
            eClv_TipSer = 3
            BndEsInternet = False
            CREAARBOLDIGITAL2()
            Panel7.Visible = True
            frmctr.Hide()
            frmInternet2.Hide()

            If IsNumeric(ComboBox14.SelectedValue) = True Then
                If ComboBox14.SelectedValue > 0 Then
                    MuestraServicios_digital(eClv_TipSer, ComboBox14.SelectedValue)
                End If
            End If
        ElseIf Me.Button11.Text = "Tv Digital &Cable" Then
            eClv_TipSer = 6
            BndEsInternet = False
            CREARARBOLDIGITALCABLE()
            Panel7.Visible = True
            frmctr.Hide()
            frmInternet2.Hide()
            If IsNumeric(ComboBox14.SelectedValue) = True Then
                If ComboBox14.SelectedValue > 0 Then
                    MuestraServicios_digital(eClv_TipSer, ComboBox14.SelectedValue)
                End If
            End If
        ElseIf Me.Button11.Text = "&Telefonia" Then
            muestra_telefonia()
        End If

        eEntraUM = True
        eEntraUMB = False

    End Sub

    Private Sub ComboBox8_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox8.SelectedIndexChanged
        Me.TextBox21.Text = Me.ComboBox8.SelectedValue
    End Sub



    Private Sub AgregarPaquete()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim Clv_unicanetDig As Long = 0
        Dim locdigcontratonet As Long = 0
        Dim locdigClv_servicio As Long = 0
        Dim RESP As MsgBoxResult = MsgBoxResult.No
        locdigClv_servicio = Me.ComboBox12.SelectedValue
        locdigcontratonet = Me.ComboBox14.SelectedValue
        If Len(Me.AparatosComboBox.Text) > 0 And Len(Me.ComboBox12.Text) > 0 Then
            eResValida = 0
            Me.ValidaDigitalTableAdapter.Connection = CON
            Me.ValidaDigitalTableAdapter.Fill(Me.NewSofTvDataSet.ValidaDigital, locdigcontratonet, locdigClv_servicio)
            LocGloContratoNet = locdigcontratonet
            ValidaDigital2(locdigcontratonet, locdigClv_servicio)
            If eResValida = 1 Then
                MsgBox(eMsgValida, MsgBoxStyle.Information)
                Exit Sub
            End If
            If Me.RespuestaTextBox.Text = 1 Then
                Me.CONSULTACONTDIGTableAdapter.Connection = CON
                Me.CONSULTACONTDIGTableAdapter.Insert(locdigcontratonet, locdigClv_servicio, "C", "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", True, 0, 0, False, "", "", True, "", 0, 0, "", "", Me.CortesiaCheckBox1.CheckState, Clv_unicanetDig)
                'JUANJO ASIGNACION DE APARATO
                GuardaRelClientesAparatos(Me.CONTRATOTextBox.Text, locdigcontratonet, locdigClv_servicio, Me.AparatosComboBox.SelectedValue)
                'FIN JUANJO ASIGNACION DE APARATO
                loc_Clv_InicaDig = Clv_unicanetDig
                Me.GUARDARRel_ContDig_UsuariosTableAdapter.Connection = CON
                Me.GUARDARRel_ContDig_UsuariosTableAdapter.Fill(Me.DataSetEDGAR.GUARDARRel_ContDig_Usuarios, Clv_unicanetDig, GloClvUsuario)
                'Me.SoloInternetCheckBox.Enabled = False
                Me.CREAARBOLDIGITAL2()
                Me.HaberServicios_CliTableAdapter.Connection = CON
                Me.HaberServicios_CliTableAdapter.Fill(Me.NewSofTvDataSet.HaberServicios_Cli, New System.Nullable(Of Long)(CType(Contrato, Long)))
                NomDig = ""
                lOC_CONTRATONETDIG = 0
                locdigClv_servicio = 0
                locdigcontratonet = 0
                Me.DIMEQUEPERIODODECORTETableAdapter.Connection = CON
                Me.DIMEQUEPERIODODECORTETableAdapter.Fill(Me.DataSetLidia.DIMEQUEPERIODODECORTE, Me.CONTRATOTextBox.Text)
                'Me.AsignaPeriodoTableAdapter.Fill(Me.DataSetLidia.AsignaPeriodo, Me.CONTRATOTextBox.Text)
                Me.CONSULTARCLIENTETableAdapter.Connection = CON
                Me.CONSULTARCLIENTETableAdapter.Fill(Me.NewSofTvDataSet.CONSULTARCLIENTE, Me.CONTRATOTextBox.Text)
                Muestra_Usuarios(loc_Clv_InicaDig, 3)

            ElseIf Me.RespuestaTextBox.Text = 0 Then
                MsgBox("El Paquete ya esta en la lista ", MsgBoxStyle.Information)
            ElseIf Me.RespuestaTextBox.Text = 4 Then
                MsgBox("El Aparato Digital esta dado de Baja por lo cual no se puede agregar el paquete ", MsgBoxStyle.Information)
            End If
        Else
            MsgBox("Se Requiere que Seleccione el Equipo Digital y Tambien Seleccione el Paquete ", MsgBoxStyle.Information)
        End If
        CON.Close()
    End Sub
    Public Sub Consulta_cliente()
        Dim ConCon As New SqlConnection(MiConexion)
        ConCon.Open()
        Me.CONSULTARCLIENTETableAdapter.Connection = ConCon
        Me.CONSULTARCLIENTETableAdapter.Fill(Me.NewSofTvDataSet.CONSULTARCLIENTE, Me.CONTRATOTextBox.Text)
        ConCon.Close()

    End Sub
    Private Sub agregar_Paquete()
        Me.CortesiaCheckBox1.Checked = False
        Me.Panel10.Visible = True
        Me.Panel10.Enabled = True
        Me.Panel8.Enabled = False
        Me.Panel9.Enabled = False
        DimeSiEsPrincipal()
        ConsultaAparatosContratacion()
    End Sub


    Private Sub Button14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button14.Click

        If GloGuardarDig = True Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.BorraDigPor_NoGRaboTableAdapter.Connection = CON
            Me.BorraDigPor_NoGRaboTableAdapter.Fill(Me.NewSofTvDataSet.BorraDigPor_NoGRabo, New System.Nullable(Of Long)(CType(GloContratoDig_Nuevo, Long)), New System.Nullable(Of Long)(CType(0, Long)))
            CON.Close()
            GloGuardarDig = False
            Me.CREAARBOLDIGITAL2()
        End If
        Me.Panel10.Visible = False
        Me.Panel10.Enabled = False

    End Sub

    Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button13.Click
        GloGuardarDig = False
        GLoPaqueteAgrega = Me.ComboBox12.Text
        AgregarPaquete()
        ConsultaRelClientesAparatos(lOC_CONTRATONETDIG)
        Me.Panel10.Visible = False
        Me.Panel10.Enabled = False
        Me.Button11.Enabled = True
        If IsNumeric(ComboBox14.SelectedValue) = True Then
            If ComboBox14.SelectedValue > 0 Then
                MuestraServicios_digital(eClv_TipSer, ComboBox14.SelectedValue)
            End If
        End If

        If eClv_TipSer = 3 Then
            If Button8.Text = "&Tv Digital MMDS" Then Button8.Enabled = False
            If Button7.Text = "&Tv Digital MMDS" Then Button7.Enabled = False
            If Button11.Text = "&Tv Digital MMDS" Then Button11.Enabled = False
            If Button28.Text = "&Tv Digital MMDS" Then Button28.Enabled = False
            If Button8.Text = "Tv Digital &Cable" Then Button8.Enabled = True
            If Button7.Text = "Tv Digital &Cable" Then Button7.Enabled = True
            If Button11.Text = "Tv Digital &Cable" Then Button11.Enabled = True
            If Button28.Text = "Tv Digital &Cable" Then Button28.Enabled = True
        ElseIf eClv_TipSer = 6 Then
            If Button8.Text = "&Tv Digital MMDS" Then Button8.Enabled = True
            If Button7.Text = "&Tv Digital MMDS" Then Button7.Enabled = True
            If Button11.Text = "&Tv Digital MMDS" Then Button11.Enabled = True
            If Button28.Text = "&Tv Digital MMDS" Then Button28.Enabled = True
            If Button8.Text = "Tv Digital &Cable" Then Button8.Enabled = False
            If Button7.Text = "Tv Digital &Cable" Then Button7.Enabled = False
            If Button11.Text = "Tv Digital &Cable" Then Button11.Enabled = False
            If Button28.Text = "Tv Digital &Cable" Then Button28.Enabled = False
        End If

    End Sub

    Private Sub Quitar_Equipo()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim cont As Integer
        Dim contv As Integer
        If lOC_CONTRATONETDIG > 0 Then
            Me.CONTARCLIENTESTableAdapter.Connection = CON
            Me.CONTARCLIENTESTableAdapter.Fill(DataSetLidia.CONTARCLIENTES, lOC_CONTRATONETDIG, 1, cont)
            Me.CONTARCLIENTESTableAdapter.Connection = CON
            Me.CONTARCLIENTESTableAdapter.Fill(Me.DataSetLidia.CONTARCLIENTES, Contrato, 3, contv)
            If cont = 0 And contv = 0 Then
                If Me.Button7.Text = "&Internet" Then
                    Me.SoloInternetCheckBox.Enabled = True
                    Me.Button7.Enabled = False
                End If
                If Me.Button11.Text = "&Internet" Then
                    Me.Button11.Enabled = False
                    Me.SoloInternetCheckBox.Enabled = True
                End If
            End If
            If cont = 0 Then
                guardabitacoradig(2)
                Me.CONSULTACLIENTESDIGTableAdapter.Connection = CON
                Me.CONSULTACLIENTESDIGTableAdapter.Delete(Contrato, lOC_CONTRATONETDIG)
                UspBorraRelAparatosClientes(Contrato, lOC_CONTRATONETDIG)
                GloGuardarDig = False
            ElseIf cont > 0 Then
                MsgBox("Primero Debes Eliminar Los Paquetes ", MsgBoxStyle.Information)
            End If
            Me.CREAARBOLDIGITAL2()
            Me.MUESTRA_A_DIGITALTableAdapter.Connection = CON
            Me.MUESTRA_A_DIGITALTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRA_A_DIGITAL)
        End If
        CON.Close()
    End Sub

    Private Sub ToolStripButton16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton16.Click

        'Miguel Alemán---------------------------------------------------------------------------
        VALIDAClientesDig(Contrato, eClv_TipSer)

        If eMsj.Length > 0 Then
            MessageBox.Show(eMsj)
            Exit Sub
        End If
        '------------------------------------------------------------------------------------------------

        VALIDAContratacionPaquete(Contrato, lOC_CONTRATONETDIG, loc_Clv_InicaDig)
        If eMsj.Length > 0 Then
            MessageBox.Show(eMsj)
            Exit Sub
        End If

        Manda_Agrega_Cablemodem()

    End Sub

    Private Sub ToolStripButton15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton15.Click
        Quitar_Equipo()
    End Sub



    Private Sub ToolStripButton14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton14.Click
        'Miguel Alemán---------------------------------------------------------------------------
        VALIDAClientesDig(Contrato, eClv_TipSer)

        If eMsj.Length > 0 Then
            MessageBox.Show(eMsj)
            Exit Sub
        End If
        '------------------------------------------------------------------------------------------------

        VALIDAContratacionPaquete(Contrato, lOC_CONTRATONETDIG, loc_Clv_InicaDig)
        If eMsj.Length > 0 Then
            MessageBox.Show(eMsj)
            Exit Sub
        End If

        agregar_Paquete()
    End Sub

    Private Sub TreeView3_AfterSelect_1(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView3.AfterSelect

        If eClv_TipSer = 6 Then
            TreeView3AfterSelectCable(e)
            Exit Sub
        End If

        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim habilita As String
        Me.CMB2Label5.Visible = False

        Try
            If e.Node.Level = 0 Then
                If IsNumeric(e.Node.Tag) = True Then

                    LocGloContratoAnt = e.Node.Tag

                    ConClientesAnt(LocGloContratoAnt)

                    Panel8.Visible = False
                    Panel9.Visible = True
                    panelClientesAnt.Visible = True

                End If
            ElseIf e.Node.Level = 1 Then
                If IsNumeric(e.Node.Tag) = True Then
                    lOC_CONTRATONETDIG = e.Node.Tag
                    LocGloContratoNet = e.Node.Tag

                    'IndiceSel = Me.TreeView3.Nodes.IndexOf 
                    GloOp = 3
                    'NombreMAC = Me.TreeView3.Nodes.Item(IndiceSel).Text
                    'MsgBox(IndiceSel.ToString, MsgBoxStyle.Information)
                    NombrePaqueteElimino = e.Node.Text
                    Me.CONSULTACLIENTESDIGTableAdapter.Connection = CON
                    Me.CONSULTACLIENTESDIGTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACLIENTESDIG, Contrato, lOC_CONTRATONETDIG)

                    'Muestra_Usuarios(lOC_CONTRATONETDIG, 3)
                    'Me.DameFechaHabilitarTableAdapter.Fill(Me.DataSetLidia.DameFechaHabilitar)
                    NoSerieCajaText.Text = ""
                    Me.VerAparatodelClientedigTableAdapter.Connection = CON
                    Me.VerAparatodelClientedigTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelClientedig, lOC_CONTRATONETDIG)
                    Muestra_Usuarios(loc_Clv_InicaDig, 3)
                    ConsultaRelClientesAparatos(lOC_CONTRATONETDIG)

                    NoSerieCajaText.Text = UspVerAparatodelClientedig(lOC_CONTRATONETDIG)

                    Me.Panel9.Enabled = True
                    Panel8.Visible = False
                    Panel9.Visible = True
                    panelClientesAnt.Visible = False

                    Me.DameFechaHabilitarTableAdapter.Connection = CON
                    Me.DameFechaHabilitarTableAdapter.Fill(Me.DataSetLidia.DameFechaHabilitar, lOC_CONTRATONETDIG, 0, habilita)
                    If habilita <> "01/01/1900" Then
                        Me.CMB2Label5.Visible = True
                        Me.CMB2Label5.Text = (" El Servicio es de Prueba y Vence en:" & " " & habilita)
                        Me.CMB2Label5.BackColor = Color.Yellow
                        Me.CMB2Label5.ForeColor = Color.Red
                    End If

                    Me.ComboBox14.Text = e.Node.Text
                    Me.ComboBox14.SelectedValue = lOC_CONTRATONETDIG
                    Me.ComboBox14.FindStringExact(e.Node.Text, 0)

                    damedatosdig2()
                Else
                    lOC_CONTRATONETDIG = 0
                End If
            ElseIf e.Node.Level = 2 Then
                If IsNumeric(e.Node.Tag) = True Then
                    cortesiadig = False
                    GloOp = 4
                    loc_Clv_InicaDig = e.Node.Tag
                    NombrePaqueteElimino = e.Node.Text
                    LocGloClv_unianet = loc_Clv_InicaDig
                    damedatosdig(loc_Clv_InicaDig)
                    'loc_Clv_InicaDig
                   



                    Me.CONSULTACONTDIGTableAdapter.Connection = CON
                    Me.CONSULTACONTDIGTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTDIG, loc_Clv_InicaDig)
                    Me.CONRel_ContDig_UsuariosTableAdapter.Connection = CON
                    Me.CONRel_ContDig_UsuariosTableAdapter.Fill(Me.DataSetEDGAR.CONRel_ContDig_Usuarios, loc_Clv_InicaDig)
                    Muestra_Usuarios(loc_Clv_InicaDig, 3)



                    Me.Panel8.Enabled = True
                    Panel9.Visible = False
                    Panel8.Visible = True
                    If GloTipoUsuario = 40 Then
                        Me.ComboBox20.Enabled = True
                    Else
                        Me.ComboBox20.Enabled = False
                    End If
                    panelClientesAnt.Visible = False
                    Me.TextBox21.Visible = False

                    If Me.Label12.Text = "Econo-pack" Then
                        Me.CortesiaCheckBox1.Enabled = False
                    End If


                    'Eric------------------------ DESCUENTO AL CLIENTE
                    BuscaDescDig()
                    '---------------------------------
                    

                Else
                    loc_Clv_InicaDig = 0
                End If
            Else

                lOC_CONTRATONETDIG = 0
            End If
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try


    End Sub

    Private Sub TreeView3AfterSelectCable(ByVal e As System.Windows.Forms.TreeViewEventArgs)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim habilita As String
        Me.CMB2Label5.Visible = False

        Try
            If e.Node.Level = 0 Then
                If IsNumeric(e.Node.Tag) = True Then
                    lOC_CONTRATONETDIG = e.Node.Tag
                    LocGloContratoNet = e.Node.Tag

                    'IndiceSel = Me.TreeView3.Nodes.IndexOf 
                    GloOp = 3
                    'NombreMAC = Me.TreeView3.Nodes.Item(IndiceSel).Text
                    'MsgBox(IndiceSel.ToString, MsgBoxStyle.Information)
                    NombrePaqueteElimino = e.Node.Text
                    Me.CONSULTACLIENTESDIGTableAdapter.Connection = CON
                    Me.CONSULTACLIENTESDIGTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACLIENTESDIG, Contrato, lOC_CONTRATONETDIG)

                    'Muestra_Usuarios(lOC_CONTRATONETDIG, 3)
                    'Me.DameFechaHabilitarTableAdapter.Fill(Me.DataSetLidia.DameFechaHabilitar)
                    NoSerieCajaText.Text = ""
                    Me.VerAparatodelClientedigTableAdapter.Connection = CON
                    Me.VerAparatodelClientedigTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelClientedig, lOC_CONTRATONETDIG)
                    Muestra_Usuarios(loc_Clv_InicaDig, 3)
                    ConsultaRelClientesAparatos(lOC_CONTRATONETDIG)

                    NoSerieCajaText.Text = UspVerAparatodelClientedig(lOC_CONTRATONETDIG)

                    Me.Panel9.Enabled = True
                    Panel8.Visible = False
                    Panel9.Visible = True
                    panelClientesAnt.Visible = False

                    Me.DameFechaHabilitarTableAdapter.Connection = CON
                    Me.DameFechaHabilitarTableAdapter.Fill(Me.DataSetLidia.DameFechaHabilitar, lOC_CONTRATONETDIG, 0, habilita)
                    If habilita <> "01/01/1900" Then
                        Me.CMB2Label5.Visible = True
                        Me.CMB2Label5.Text = (" El Servicio es de Prueba y Vence en:" & " " & habilita)
                        Me.CMB2Label5.BackColor = Color.Yellow
                        Me.CMB2Label5.ForeColor = Color.Red
                    End If

                    Me.ComboBox14.Text = e.Node.Text
                    Me.ComboBox14.SelectedValue = lOC_CONTRATONETDIG
                    Me.ComboBox14.FindStringExact(e.Node.Text, 0)

                    damedatosdig2()
                Else
                    lOC_CONTRATONETDIG = 0
                End If
            ElseIf e.Node.Level = 1 Then
                If IsNumeric(e.Node.Tag) = True Then
                    cortesiadig = False
                    GloOp = 4
                    loc_Clv_InicaDig = e.Node.Tag
                    NombrePaqueteElimino = e.Node.Text
                    LocGloClv_unianet = loc_Clv_InicaDig
                    damedatosdig(loc_Clv_InicaDig)
                    'loc_Clv_InicaDig



                    Me.CONSULTACONTDIGTableAdapter.Connection = CON
                    Me.CONSULTACONTDIGTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTDIG, loc_Clv_InicaDig)
                    Me.CONRel_ContDig_UsuariosTableAdapter.Connection = CON
                    Me.CONRel_ContDig_UsuariosTableAdapter.Fill(Me.DataSetEDGAR.CONRel_ContDig_Usuarios, loc_Clv_InicaDig)
                    Muestra_Usuarios(loc_Clv_InicaDig, 3)



                    Me.Panel8.Enabled = True
                    Panel9.Visible = False
                    Panel8.Visible = True
                    If GloTipoUsuario = 40 Then
                        Me.ComboBox20.Enabled = True
                    Else
                        Me.ComboBox20.Enabled = False
                    End If
                    panelClientesAnt.Visible = False
                    Me.TextBox21.Visible = False
                    'Eric------------------------ DESCUENTO AL CLIENTE
                    BuscaDescDig()
                    '---------------------------------

                Else
                    loc_Clv_InicaDig = 0
                End If
            Else

                lOC_CONTRATONETDIG = 0
            End If
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub TextBox20_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox20.TextChanged

    End Sub

    Private Sub ComboBox13_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox13.SelectedIndexChanged
        Me.TextBox26.Text = Me.ComboBox13.SelectedValue
    End Sub

    Private Sub ComboBox10_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox10.SelectedIndexChanged
        Me.TextBox20.Text = Me.ComboBox10.SelectedValue
        If Me.ComboBox10.Text = "Contratado" Then
            Me.CheckBox1.Checked = True
        Else
            Me.CheckBox1.Checked = False
        End If
    End Sub

    Private Sub ComboBox9_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox9.SelectedIndexChanged
        Me.TextBox22.Text = Me.ComboBox9.SelectedValue
    End Sub

    Private Sub ToolStripButton10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton10.Click
        If Me.CortesiaCheckBox1.Checked And Me.TipoCortesiaComboBox.SelectedValue = 0 Then
            MsgBox("Se Requiere Seleccionar un Tipo de Cortesia", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.ComboBox10.Text = "Contratado" Then
            If IsDate(Me.TextBox16.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Contratación", MsgBoxStyle.Information, "Atención")
            ElseIf CDate(Me.TextBox16.Text) > CDate(Me.TextBox1.Text) Then
                MsgBox("La Fecha de Contratación no puede ser Mayor a la Fecha Actual", MsgBoxStyle.Information, "Atención")
            Else
                guardadig()
            End If
        End If
        If Me.ComboBox10.Text = "Suspendido" Or Me.ComboBox10.Text = "Desconectado" Then
            If IsDate(Me.TextBox14.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Suspensión o Desconexión", MsgBoxStyle.Information, "Atención")
            ElseIf (CDate(Me.TextBox16.Text) <= CDate(Me.TextBox14.Text)) And (CDate(Me.TextBox14.Text) <= CDate(Me.TextBox1.Text)) Then
                guardadig()
            Else
                MsgBox("La Fecha de Suspensión o Desconexión debe Encontrarse entre la Fecha de Contratción y la Fecha Actual", MsgBoxStyle.Information, "Atención")
            End If
        End If
        If Me.ComboBox10.Text = "Instalado" Then
            If IsDate(Me.TextBox15.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Instalación", MsgBoxStyle.Information, "Atención")
            ElseIf Me.TextBox10.Text = "" Or CInt(Me.TextBox10.Text) > 12 Or CInt(Me.TextBox10.Text) = 0 Then
                MsgBox("Se Requiere que Capture el Ùltimo Mes", MsgBoxStyle.Exclamation, "Atención")
            ElseIf (Me.TextBox9.Text).Length <> 4 Then
                MsgBox("Se Requiere que Capture el Último Año", MsgBoxStyle.Exclamation, "Atención")
            ElseIf (CDate(Me.TextBox16.Text) <= CDate(Me.TextBox15.Text)) And (CDate(Me.TextBox15.Text) <= CDate(Me.TextBox1.Text)) Then
                guardadig()
            Else
                MsgBox("La Fecha de Instalación debe Encontrarse entre la Fecha de Contratción y la Fecha Actual", MsgBoxStyle.Information, "Atención")
            End If
        End If
        If Me.ComboBox10.Text = "Fuera de Area" Then
            If IsDate(Me.TextBox12.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Fuera de Área", MsgBoxStyle.Exclamation, "Atención")
            ElseIf (CDate(Me.TextBox16.Text) <= CDate(Me.TextBox12.Text)) And (CDate(Me.TextBox12.Text) <= CDate(Me.TextBox1.Text)) Then
                guardadig()
            Else
                MsgBox("La Fecha de Fuera de Área debe Encontrarse entre la Fecha de Contratción y la Fecha Actual", MsgBoxStyle.Information, "Atención")
            End If
        End If
        If Me.ComboBox10.Text = "Baja" Then
            If IsDate(Me.TextBox13.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Baja", MsgBoxStyle.Information, "Atención")
            ElseIf (CDate(Me.TextBox16.Text) <= CDate(Me.TextBox13.Text)) And (CDate(Me.TextBox13.Text) <= CDate(Me.TextBox1.Text)) Then
                guardadig()
            Else
                MsgBox("La Fecha de Baja debe Encontrarse entre la Fecha de Contratción y la Fecha Actual", MsgBoxStyle.Information, "Atención")
            End If
        End If

        'Actualiza_usuario(GloClvUnicaNet, CInt(Me.ComboBox19.SelectedValue), 3)

    End Sub
    Private Sub guardadig()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.CONSULTACONTDIGBindingSource.EndEdit()
        Me.CONSULTACONTDIGTableAdapter.Connection = CON
        Me.CONSULTACONTDIGTableAdapter.Update(Me.NewSofTvDataSet.CONSULTACONTDIG)
        Me.Inserta_Rel_cortesia_FechaTableAdapter.Connection = CON
        Me.Inserta_Rel_cortesia_FechaTableAdapter.Fill(Me.Procedimientosarnoldo4.Inserta_Rel_cortesia_Fecha, loc_Clv_InicaDig, cortesiadig, 3)

        guardabitacoradig(4)
        CON.Close()
        Me.CREAARBOLDIGITAL2()
        damedatosdig(loc_Clv_InicaDig)
        Actualiza_usuario(loc_Clv_InicaDig, CLng(Me.ComboBox20.SelectedValue), 3)
        'JUANJO TIPOS CORTESIAS
        NUETIPOCORTESIA(loc_Clv_InicaDig, Me.TipoCortesiaComboBox.SelectedValue, 3, Me.CortesiaCheckBox1.CheckState)
        'FIN JUANJO TIPOS CORTESIAS
        MsgBox("Se ha Guardado con Exíto", MsgBoxStyle.Information)
    End Sub

    Private Sub ToolStripButton12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton12.Click
        If Me.ComboBox13.Text = "Instalado" Then
            If IsDate(Me.TextBox33.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Activación", MsgBoxStyle.Information)
            Else
                guardardig2()
            End If
        ElseIf Me.ComboBox13.Text = "Baja" Then
            If IsDate(Me.TextBox23.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Baja", MsgBoxStyle.Information)
            Else
                guardardig2()
            End If
        ElseIf Me.ComboBox13.Text = "Traspasado" Then
            If IsDate(Me.TextBox33.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Activación", MsgBoxStyle.Information)
            Else
                guardardig2()
            End If
        ElseIf ComboBox13.Text = "Por Asignar" Then
            guardardig2()
        End If
    End Sub
    Private Sub guardardig2()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.CONSULTACLIENTESDIGBindingSource.EndEdit()
        Me.CONSULTACLIENTESDIGTableAdapter.Connection = CON
        Me.CONSULTACLIENTESDIGTableAdapter.Update(Me.NewSofTvDataSet.CONSULTACLIENTESDIG)

        'Me.CONSULTACLIENTESDIGTableAdapter.Update(
        guardabitacoradig(5)
        CON.Close()
        Me.GuardaModificame_cablemodem()
        Me.CREAARBOLDIGITAL2()
        damedatosdig2()
        Actualiza_usuario(loc_Clv_InicaDig, CLng(Me.ComboBox20.SelectedValue), 3)
        MsgBox("Se ha Guardado con Exíto", MsgBoxStyle.Information)
    End Sub
    Private Sub ToolStripButton9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton9.Click
        Me.CONSULTACONTDIGBindingSource.CancelEdit()
    End Sub

    Private Sub ToolStripButton11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton11.Click
        Me.CONSULTACLIENTESDIGBindingSource.CancelEdit()
    End Sub
    Private Sub ToolStripButton13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton13.Click
        Dim cont, cont2 As Integer
        Dim PPAL As Boolean
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(loc_Clv_InicaDig) = True Then
            If loc_Clv_InicaDig > 0 Then
                Me.PrimerMesCLIENTESTableAdapter.Connection = CON
                Me.PrimerMesCLIENTESTableAdapter.Fill(Me.DataSetLidia.PrimerMesCLIENTES, loc_Clv_InicaDig, 1, cont, PPAL)
                Me.CONTARCLIENTESTableAdapter.Connection = CON
                Me.CONTARCLIENTESTableAdapter.Fill(Me.DataSetLidia.CONTARCLIENTES, lOC_CONTRATONETDIG, 4, cont2)
                If cont = 1 Then
                    If PPAL = False Then
                        guardabitacoradig(1)
                        Me.CONSULTACONTDIGTableAdapter.Connection = CON
                        Me.CONSULTACONTDIGTableAdapter.Delete(loc_Clv_InicaDig)
                        loc_Clv_InicaDig = 0
                        CREAARBOLDIGITAL2()
                    End If
                    If PPAL = True Then
                        If cont2 > 0 Then
                            MsgBox("No se Puede Eliminar el Paquete Principal Sin Antes Haber Eliminado los Paquetes Adicionales", MsgBoxStyle.Information)
                        ElseIf cont2 = 0 Then
                            guardabitacoradig(1)
                            Me.CONSULTACONTDIGTableAdapter.Connection = CON
                            Me.CONSULTACONTDIGTableAdapter.Delete(loc_Clv_InicaDig)
                            loc_Clv_InicaDig = 0
                            CREAARBOLDIGITAL2()
                        End If
                    End If

                ElseIf cont = 0 Then
                    MsgBox("El Paquete No Puede ser Eliminado", MsgBoxStyle.Information)
                ElseIf IsNumeric(loc_Clv_InicaDig) = False Then
                    MsgBox("Seleccione el Paquete que desea quitar ", MsgBoxStyle.Information)
                End If

            ElseIf loc_Clv_InicaDig = 0 Then
                MsgBox("Escoja un Paquete a Eliminar", MsgBoxStyle.Information)
            End If
        End If
        CON.Close()
    End Sub

    Private Sub ComboBox14_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox14.SelectedIndexChanged
        If IsNumeric(ComboBox14.SelectedValue) = True Then
            If ComboBox14.SelectedValue > 0 Then
                MuestraServicios_digital(eClv_TipSer, Me.ComboBox14.SelectedValue)
            End If
        End If
    End Sub


    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        VerBrwOrdSer.Show()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        VerBRWQUEJAS.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Contrato = CLng(Me.CONTRATOTextBox.Text)
        'FrmDatosFiscales.Show()
        If IsNumeric(Contrato) = True Then
            If CLng(Contrato) > 0 Then
                FacturacionDigitalSoftv.ClassCFDI.MiConexion = MiConexion
                FacturacionDigitalSoftv.ClassCFDI.LocClv_Ciudad = ""
                FacturacionDigitalSoftv.ClassCFDI.OpcionCli = OpcionCli
                FacturacionDigitalSoftv.ClassCFDI.GloContrato = Contrato
                Dim frm As New FacturacionDigitalSoftv.FrmDatosFiscales
                frm.ShowDialog()
            End If
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        FrmDatosBanco.Show()
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        eGloContrato = Me.CONTRATOTextBox.Text
        If IdSistema = "LO" Or IdSistema = "YU" Then
            FrmConsultaCobro2.Show()
        Else
            FrmConsultaCobro.Show()
        End If

    End Sub

    'Private Sub FECHA_ULT_PAGOTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If DateValue(Me.FECHA_ULT_PAGOTextBox.Text) = DateValue("01/01/1900") Then
    '            Me.FECHA_ULT_PAGOTextBox.Text = ""
    '        End If
    '    Catch
    '    End Try
    'End Sub


    Private Sub BasicoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BasicoTextBox.TextChanged
        If IsNumeric(Me.BasicoTextBox.Text) = True Then
            If Me.BasicoTextBox.Text > 0 Then
                Me.Button11.Enabled = True
            End If
        End If
    End Sub

    Private Sub DigitalTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DigitalTextBox.TextChanged
        If IsNumeric(Me.DigitalTextBox.Text) = True Then
            If Me.DigitalTextBox.Text > 0 Then
                Me.Button11.Enabled = True
            End If
        End If
    End Sub

    Private Sub TextBox33_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox33.TextChanged

        If IsDate(Mid(Me.TextBox33.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.TextBox33.Text, 1, 10)
            Try
                If DateValue(Fecha) = DateValue("01/01/1900") Then
                    Me.TextBox33.Clear()
                End If
            Catch
            End Try
        End If

    End Sub

    Private Sub TextBox32_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox32.TextChanged

        If IsDate(Mid(Me.TextBox32.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.TextBox32.Text, 1, 10)
            Try
                If DateValue(Fecha) = DateValue("01/01/1900") Then
                    Me.TextBox32.Clear()
                End If
            Catch
            End Try
        End If

    End Sub

    Private Sub TextBox23_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox23.TextChanged

        If IsDate(Mid(Me.TextBox23.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.TextBox23.Text, 1, 10)
            Try
                If DateValue(Fecha) = DateValue("01/01/1900") Then
                    Me.TextBox23.Clear()
                End If
            Catch
            End Try
        End If

    End Sub

    Private Sub TextBox16_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox16.TextChanged

        If IsDate(Mid(Me.TextBox16.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.TextBox16.Text, 1, 10)
            Try
                If DateValue(Fecha) = DateValue("01/01/1900") Then
                    Me.TextBox16.Clear()
                End If
            Catch
            End Try
        End If

    End Sub

    Private Sub TextBox15_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox15.TextChanged

        If IsDate(Mid(Me.TextBox15.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.TextBox15.Text, 1, 10)
            Try
                If DateValue(Fecha) = DateValue("01/01/1900") Then
                    Me.TextBox15.Clear()
                End If
            Catch
            End Try
        End If

    End Sub

    Private Sub TextBox14_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox14.TextChanged

        If IsDate(Mid(Me.TextBox14.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.TextBox14.Text, 1, 10)
            Try
                If DateValue(Fecha) = DateValue("01/01/1900") Then
                    Me.TextBox14.Clear()
                End If
            Catch
            End Try
        End If
    End Sub

    Private Sub TextBox13_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox13.TextChanged

        If IsDate(Mid(Me.TextBox13.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.TextBox13.Text, 1, 10)
            Try
                If DateValue(Fecha) = DateValue("01/01/1900") Then
                    Me.TextBox13.Clear()
                End If
            Catch
            End Try
        End If

    End Sub

    Private Sub TextBox12_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox12.TextChanged

        If IsDate(Mid(Me.TextBox12.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.TextBox12.Text, 1, 10)
            Try
                If DateValue(Fecha) = DateValue("01/01/1900") Then
                    Me.TextBox12.Clear()
                End If
            Catch
            End Try
        End If

    End Sub

    Private Sub TextBox11_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox11.TextChanged

        If IsDate(Mid(Me.TextBox11.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.TextBox11.Text, 1, 10)
            Try
                If DateValue(Fecha) = DateValue("01/01/1900") Then
                    Me.TextBox11.Clear()
                End If
            Catch
            End Try
        End If

    End Sub


    Private Sub NOMBRETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NOMBRETextBox.TextChanged
        BndDClientes = True
    End Sub

    Private Sub GUARDARRel_Clientes_TiposClientesGuarda()
        Try

            If IsNumeric(Me.CONTRATOTextBox.Text) = True Then
                If Len(Trim(Me.ComboBox7.Text)) > 0 Then
                    If IsNumeric(Me.ComboBox7.SelectedValue) = True Then
                        Dim CON As New SqlConnection(MiConexion)
                        CON.Open()
                        Me.GUARDARRel_Clientes_TiposClientesTableAdapter.Connection = CON
                        Me.GUARDARRel_Clientes_TiposClientesTableAdapter.Fill(Me.DataSetEDGAR.GUARDARRel_Clientes_TiposClientes, Me.CONTRATOTextBox.Text, Me.ComboBox7.SelectedValue)
                        CON.Close()
                    End If
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If IsNumeric(Contrato) = True And Contrato > 0 Then
            GloOpFacturas = 3
            Bwr_FacturasCancelar.Show()
        Else
            MsgBox("Seleccione un Cliente por favor", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub Button12_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click
        If Contrato > 0 Then
            My.Forms.FrmLoginCte.Show()
        Else
            MsgBox("Primero Guarda el Cliente", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub ComboBox7_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox7.LostFocus
        GloOpPermiso = 0
        GloPermisoCortesia = 0
        Me.ComboBox7.Enabled = False
    End Sub

    Private Sub ComboBox7_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox7.SelectedIndexChanged
        If IsNumeric(Me.ComboBox7.SelectedValue) = True Then eClv_TipoCliente = CInt(Me.ComboBox7.SelectedValue)
        If Me.ComboBox7.SelectedValue = 6 Or Me.ComboBox7.Text = "INSEN" Then
            Me.InsenPanel.Visible = True
            Me.NoInsenTextBox.Clear()
            ConClienteInsen(Me.CONTRATOTextBox.Text, Me.ComboBox7.SelectedValue)
        Else
            Me.InsenPanel.Visible = False
        End If
        'If IdSistema = "VA" Then
        If IsNumeric(Me.ComboBox7.SelectedValue) = True And Me.ComboBox7.SelectedValue = 3 Then

            'Me.Button25.Visible = True
            'Me.Button26.Visible = True
            Me.Button27.Visible = True

            If BndEsInternet = True Then
                frmInternet2.Hide()
                frmInternet2.Show()
            End If

        ElseIf IsNumeric(Me.ComboBox7.SelectedValue) = True And Me.ComboBox7.SelectedValue <> 3 Then
            'Me.Button25.Visible = False
            'Me.Button26.Visible = False
            Me.Button27.Visible = False
        End If
        'End If
    End Sub

    Private Sub Button15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button15.Click
        GloOpPermiso = 0
        GloPermisoCortesia = 0
        bnd_tipopago = True
        FrmAccesopoUsuario.Show()
    End Sub

    Private Sub Button16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        GloOpPermiso = 1
        GloPermisoCortesia = 0
        FrmAccesopoUsuario.Show()
    End Sub

    Private Sub Button17_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button17.Click
        GloOpPermiso = 2
        GloPermisoCortesia = 0
        FrmAccesopoUsuario.Show()
    End Sub

    Private Sub Button18_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        GloOpPermiso = 3
        GloPermisoCortesia = 0
        FrmAccesopoUsuario.Show()
    End Sub
    Private Sub Button24_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button24.Click
        If Me.CortesiaCheckBox1.Checked = False Then

            eTipSer = eClv_TipSer
            'If eAccesoAdmin = True Then
            '    eClv_UnicaNetDig = loc_Clv_InicaDig
            '    FrmRelCteDescuento.Show()
            'Else
            eGloDescuento = 0
            eGloTipSerDesc = eClv_TipSer
            FrmAccesopoUsuario.Show()
            'End If
        Else
            MsgBox("No Puede Aplicar un Descuento, ya que cuenta con una Cortesía", , "Atención")
        End If
    End Sub

    'Private Sub CortesiaCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If Me.CortesiaCheckBox.CheckState = CheckState.Checked Then
    '        cortesiatv = True
    '    Else
    '        cortesiatv = False
    '    End If
    'End Sub

    'Private Sub CortesiaCheckBox_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim eRes As Integer = 0
    '    Dim eMsg As String = Nothing
    '    Dim CON As New SqlConnection(MiConexion)
    '    CON.Open()
    '    Me.ChecaRelCteDescuentoTableAdapter.Connection = CON
    '    Me.ChecaRelCteDescuentoTableAdapter.Fill(Me.DataSetEric.ChecaRelCteDescuento, Me.CONTRATOTextBox.Text, 1, eRes, eMsg)
    '    CON.Close()
    '    If eRes = 1 Then
    '        MsgBox(eMsg)
    '        Me.CortesiaCheckBox.Checked = False
    '    End If
    'End Sub

    'Private Sub CortesiaCheckBox_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs)
    '    If GloTipoUsuario = 40 Then
    '        Me.CortesiaCheckBox.Enabled = True
    '    Else
    '        Me.CortesiaCheckBox.Enabled = False
    '    End If
    'End Sub

    Private Sub CortesiaCheckBox1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CortesiaCheckBox1.Click
        Dim eRes As Integer = 0
        Dim eMsg As String = Nothing
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ChecaRelCteDescuentoTableAdapter.Connection = CON
        Me.ChecaRelCteDescuentoTableAdapter.Fill(Me.DataSetEric.ChecaRelCteDescuento, loc_Clv_InicaDig, 3, eRes, eMsg)
        CON.Close()
        If eRes = 1 Then
            MsgBox(eMsg)
            Me.CortesiaCheckBox1.Checked = False
        End If
    End Sub

    Private Sub CortesiaCheckBox1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles CortesiaCheckBox1.LostFocus
        If GloTipoUsuario = 40 Then
            Me.CortesiaCheckBox1.Enabled = True
        Else
            Me.CortesiaCheckBox1.Enabled = False
        End If
    End Sub

    'Private Sub CortesiaCheckBox2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim eRes As Integer = 0
    '    Dim eMsg As String = Nothing
    '    Dim CON As New SqlConnection(MiConexion)
    '    CON.Open()
    '    Me.ChecaRelCteDescuentoTableAdapter.Connection = CON
    '    Me.ChecaRelCteDescuentoTableAdapter.Fill(Me.DataSetEric.ChecaRelCteDescuento, GloClvUnicaNet, 2, eRes, eMsg)
    '    CON.Close()
    '    If eRes = 1 Then
    '        MsgBox(eMsg)
    '        Me.CortesiaCheckBox2.Checked = False
    '    End If
    'End Sub

    'Private Sub CortesiaCheckBox2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs)
    '    If GloTipoUsuario = 40 Then
    '        Me.CortesiaCheckBox2.Enabled = True
    '    Else
    '        Me.CortesiaCheckBox2.Enabled = False
    '    End If
    'End Sub



    'Private Sub Button19_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim CONT, cont2 As Integer
    '    Dim PPAL As Boolean
    '    Dim CON As New SqlConnection(MiConexion)
    '    CON.Open()
    '    If Me.Panel6.Visible = True Then
    '        Me.PrimerMesCLIENTESTableAdapter.Connection = CON
    '        Me.PrimerMesCLIENTESTableAdapter.Fill(Me.DataSetLidia.PrimerMesCLIENTES, GloClvUnicaNet, 0, CONT, PPAL)
    '        Me.CONTARCLIENTESTableAdapter.Connection = CON
    '        Me.CONTARCLIENTESTableAdapter.Fill(Me.DataSetLidia.CONTARCLIENTES, GloContratoNet2, 5, cont2)

    '        If CONT = 1 Then
    '            If PPAL = False Then
    '                guardabitacoranet(2)
    '                Me.CONSULTACONTNETTableAdapter.Connection = CON
    '                Me.CONSULTACONTNETTableAdapter.Delete(GloClvUnicaNet)
    '                Me.CREAARBOL()
    '            End If
    '            If PPAL = True Then
    '                If cont2 > 0 Then
    '                    MsgBox("No se Puede Eliminar el Paquete Principal Sin Antes Haber Eliminado los Paquetes Adicionales", MsgBoxStyle.Information)
    '                ElseIf cont2 = 0 Then
    '                    guardabitacoranet(2)
    '                    Me.CONSULTACONTNETTableAdapter.Connection = CON
    '                    Me.CONSULTACONTNETTableAdapter.Delete(GloClvUnicaNet)
    '                    Me.CREAARBOL()
    '                End If
    '            End If


    '        ElseIf CONT = 0 Then
    '            MsgBox("No Se Puede Eliminar El Servicio ", MsgBoxStyle.Information)
    '        End If
    '    End If
    '    If Me.Panel5.Visible = True Then
    '        Me.CONTARCLIENTESTableAdapter.Connection = CON
    '        Me.CONTARCLIENTESTableAdapter.Fill(DataSetLidia.CONTARCLIENTES, GloContratoNet2, 0, CONT)
    '        If CONT = 0 Then
    '            guardabitacoranet(2)
    '            Me.CONSULTACLIENTESNETTableAdapter.Connection = CON
    '            Me.CONSULTACLIENTESNETTableAdapter.Delete(Contrato, GloContratoNet2)
    '        ElseIf CONT > 0 Then
    '            MsgBox("Primero Debes Eliminar Los Servicios ", MsgBoxStyle.Information)
    '        End If
    '        Me.CREAARBOL()
    '    End If
    '    CON.Close()
    'End Sub


    Private Sub NUMEROTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NUMEROTextBox.TextChanged
        BndDClientes = True
    End Sub

    Private Sub ENTRECALLESTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ENTRECALLESTextBox.TextChanged
        BndDClientes = True
    End Sub

    Private Sub TELEFONOTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TELEFONOTextBox.KeyPress
        'e.KeyChar = ChrW(ValidaKey(TELEFONOTextBox, Asc(e.KeyChar), "N"))
        Dim caracter As Char = e.KeyChar
        If (Char.IsNumber(caracter)) Or _
                (caracter = ChrW(Keys.Back)) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub TELEFONOTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TELEFONOTextBox.TextChanged
        BndDClientes = True
    End Sub




    Private Sub ObsTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Label29_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    'Private Sub ComboBox5_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If Me.ComboBox5.Text = "Contratado" Then
    '        Me.PrimerMensualidadCheckBox.Checked = True
    '    Else
    '        Me.PrimerMensualidadCheckBox.Checked = False
    '    End If
    'End Sub


    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        eEntraUMB = True
        eEntraUM = False
    End Sub

    'Private Sub TextBox2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs)
    '    If Me.TextBox2.Text.Length < 3 Then
    '        MsgBox("Captura el Mes.", , "Atención")
    '        Me.TextBox2.Focus()
    '    End If
    'End Sub

    'Private Sub TextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If eEntraUMB = True Then
    '        If Me.TextBox2.Text.Length = 3 Then

    '            If Me.TextBox2.Text = "ENE" Then
    '                Me.ULTIMO_MESTextBox.Text = 1
    '            ElseIf Me.TextBox2.Text = "FEB" Then
    '                Me.ULTIMO_MESTextBox.Text = 2
    '            ElseIf Me.TextBox2.Text = "MAR" Then
    '                Me.ULTIMO_MESTextBox.Text = 3
    '            ElseIf Me.TextBox2.Text = "ABR" Then
    '                Me.ULTIMO_MESTextBox.Text = 4
    '            ElseIf Me.TextBox2.Text = "MAY" Then
    '                Me.ULTIMO_MESTextBox.Text = 5
    '            ElseIf Me.TextBox2.Text = "JUN" Then
    '                Me.ULTIMO_MESTextBox.Text = 6
    '            ElseIf Me.TextBox2.Text = "JUL" Then
    '                Me.ULTIMO_MESTextBox.Text = 7
    '            ElseIf Me.TextBox2.Text = "AGO" Then
    '                Me.ULTIMO_MESTextBox.Text = 8
    '            ElseIf Me.TextBox2.Text = "SEP" Then
    '                Me.ULTIMO_MESTextBox.Text = 9
    '            ElseIf Me.TextBox2.Text = "OCT" Then
    '                Me.ULTIMO_MESTextBox.Text = 10
    '            ElseIf Me.TextBox2.Text = "NOV" Then
    '                Me.ULTIMO_MESTextBox.Text = 11
    '            ElseIf Me.TextBox2.Text = "DIC" Then
    '                Me.ULTIMO_MESTextBox.Text = 12
    '            Else
    '                MsgBox("El Mes Tecleado es Incorrecto.", , "Error")
    '                Me.TextBox2.Clear()
    '            End If

    '        End If
    '    End If
    'End Sub



    Private Sub ULTIMO_MESTextBox_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        eEntraUM = True
        eEntraUMB = False
    End Sub

    'Private Sub ULTIMO_MESTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If eEntraUM = True Then
    '        If IsNumeric(ULTIMO_MESTextBox.Text) = True Then
    '            If Me.ULTIMO_MESTextBox.Text = 1 Then
    '                Me.TextBox2.Text = "ENE"
    '            ElseIf Me.ULTIMO_MESTextBox.Text = 2 Then
    '                Me.TextBox2.Text = "FEB"
    '            ElseIf Me.ULTIMO_MESTextBox.Text = 3 Then
    '                Me.TextBox2.Text = "MAR"
    '            ElseIf Me.ULTIMO_MESTextBox.Text = 4 Then
    '                Me.TextBox2.Text = "ABR"
    '            ElseIf Me.ULTIMO_MESTextBox.Text = 5 Then
    '                Me.TextBox2.Text = "MAY"
    '            ElseIf Me.ULTIMO_MESTextBox.Text = 6 Then
    '                Me.TextBox2.Text = "JUN"
    '            ElseIf Me.ULTIMO_MESTextBox.Text = 7 Then
    '                Me.TextBox2.Text = "JUL"
    '            ElseIf Me.ULTIMO_MESTextBox.Text = 8 Then
    '                Me.TextBox2.Text = "AGO"
    '            ElseIf Me.ULTIMO_MESTextBox.Text = 9 Then
    '                Me.TextBox2.Text = "SEP"
    '            ElseIf Me.ULTIMO_MESTextBox.Text = 10 Then
    '                Me.TextBox2.Text = "OCT"
    '            ElseIf Me.ULTIMO_MESTextBox.Text = 11 Then
    '                Me.TextBox2.Text = "NOV"
    '            ElseIf Me.ULTIMO_MESTextBox.Text = 12 Then
    '                Me.TextBox2.Text = "DIC"
    '            Else
    '                Me.TextBox2.Text = "---"
    '            End If
    '        End If
    '    End If
    'End Sub


    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        eEntraUMB = True
        eEntraUM = False
    End Sub

    Private Sub TextBox3_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox3.LostFocus
        If Me.TextBox3.Text.Length < 3 Then
            MsgBox("Captura el Mes.", , "Atención")
            Me.TextBox3.Focus()
        End If
    End Sub

    Private Sub TextBox3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox3.TextChanged
        If eEntraUMB = True Then
            If Me.TextBox3.Text.Length = 3 Then

                If Me.TextBox3.Text = "ENE" Then
                    Me.TextBox10.Text = 1
                ElseIf Me.TextBox3.Text = "FEB" Then
                    Me.TextBox10.Text = 2
                ElseIf Me.TextBox3.Text = "MAR" Then
                    Me.TextBox10.Text = 3
                ElseIf Me.TextBox3.Text = "ABR" Then
                    Me.TextBox10.Text = 4
                ElseIf Me.TextBox3.Text = "MAY" Then
                    Me.TextBox10.Text = 5
                ElseIf Me.TextBox3.Text = "JUN" Then
                    Me.TextBox10.Text = 6
                ElseIf Me.TextBox3.Text = "JUL" Then
                    Me.TextBox10.Text = 7
                ElseIf Me.TextBox3.Text = "AGO" Then
                    Me.TextBox10.Text = 8
                ElseIf Me.TextBox3.Text = "SEP" Then
                    Me.TextBox10.Text = 9
                ElseIf Me.TextBox3.Text = "OCT" Then
                    Me.TextBox10.Text = 10
                ElseIf Me.TextBox3.Text = "NOV" Then
                    Me.TextBox10.Text = 11
                ElseIf Me.TextBox3.Text = "DIC" Then
                    Me.TextBox10.Text = 12
                Else
                    MsgBox("El Mes Tecleado es Incorrecto.", , "Error")
                    Me.TextBox3.Clear()
                End If

            End If
        End If
    End Sub

    Private Sub TextBox10_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox10.Click
        eEntraUM = True
        eEntraUMB = False
    End Sub

    Private Sub TextBox10_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox10.TextChanged
        If eEntraUM = True Then
            If IsNumeric(TextBox10.Text) = True Then
                If Me.TextBox10.Text = 1 Then
                    Me.TextBox3.Text = "ENE"
                ElseIf Me.TextBox10.Text = 2 Then
                    Me.TextBox3.Text = "FEB"
                ElseIf Me.TextBox10.Text = 3 Then
                    Me.TextBox3.Text = "MAR"
                ElseIf Me.TextBox10.Text = 4 Then
                    Me.TextBox3.Text = "ABR"
                ElseIf Me.TextBox10.Text = 5 Then
                    Me.TextBox3.Text = "MAY"
                ElseIf Me.TextBox10.Text = 6 Then
                    Me.TextBox3.Text = "JUN"
                ElseIf Me.TextBox10.Text = 7 Then
                    Me.TextBox3.Text = "JUL"
                ElseIf Me.TextBox10.Text = 8 Then
                    Me.TextBox3.Text = "AGO"
                ElseIf Me.TextBox10.Text = 9 Then
                    Me.TextBox3.Text = "SEP"
                ElseIf Me.TextBox10.Text = 10 Then
                    Me.TextBox3.Text = "OCT"
                ElseIf Me.TextBox10.Text = 11 Then
                    Me.TextBox3.Text = "NOV"
                ElseIf Me.TextBox10.Text = 12 Then
                    Me.TextBox3.Text = "DIC"
                Else
                    Me.TextBox3.Text = "---"
                End If
            End If
        End If
    End Sub


    Private Sub TextBox5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        eEntraUMB = True
        eEntraUM = False
    End Sub

    'Private Sub TextBox5_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs)
    '    If Me.TextBox5.Text.Length < 3 Then
    '        MsgBox("Captura el Mes.", , "Atención")
    '        Me.TextBox5.Focus()
    '    End If
    'End Sub

    'Private Sub TextBox5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If eEntraUMB = True Then
    '        If Me.TextBox5.Text.Length = 3 Then

    '            If Me.TextBox5.Text = "ENE" Then
    '                Me.Ultimo_mesTextBox1.Text = 1
    '            ElseIf Me.TextBox5.Text = "FEB" Then
    '                Me.Ultimo_mesTextBox1.Text = 2
    '            ElseIf Me.TextBox5.Text = "MAR" Then
    '                Me.Ultimo_mesTextBox1.Text = 3
    '            ElseIf Me.TextBox5.Text = "ABR" Then
    '                Me.Ultimo_mesTextBox1.Text = 4
    '            ElseIf Me.TextBox5.Text = "MAY" Then
    '                Me.Ultimo_mesTextBox1.Text = 5
    '            ElseIf Me.TextBox5.Text = "JUN" Then
    '                Me.Ultimo_mesTextBox1.Text = 6
    '            ElseIf Me.TextBox5.Text = "JUL" Then
    '                Me.Ultimo_mesTextBox1.Text = 7
    '            ElseIf Me.TextBox5.Text = "AGO" Then
    '                Me.Ultimo_mesTextBox1.Text = 8
    '            ElseIf Me.TextBox5.Text = "SEP" Then
    '                Me.Ultimo_mesTextBox1.Text = 9
    '            ElseIf Me.TextBox5.Text = "OCT" Then
    '                Me.Ultimo_mesTextBox1.Text = 10
    '            ElseIf Me.TextBox5.Text = "NOV" Then
    '                Me.Ultimo_mesTextBox1.Text = 11
    '            ElseIf Me.TextBox5.Text = "DIC" Then
    '                Me.Ultimo_mesTextBox1.Text = 12
    '            Else
    '                MsgBox("El Mes Tecleado es Incorrecto.", , "Error")
    '                Me.TextBox5.Clear()
    '            End If

    '        End If
    '    End If
    'End Sub

    Private Sub Ultimo_mesTextBox1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        eEntraUM = True
        eEntraUMB = False
    End Sub

    'Private Sub Ultimo_mesTextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If eEntraUM = True Then
    '        If IsNumeric(Ultimo_mesTextBox1.Text) = True Then
    '            If Me.Ultimo_mesTextBox1.Text = 1 Then
    '                Me.TextBox5.Text = "ENE"
    '            ElseIf Me.Ultimo_mesTextBox1.Text = 2 Then
    '                Me.TextBox5.Text = "FEB"
    '            ElseIf Me.Ultimo_mesTextBox1.Text = 3 Then
    '                Me.TextBox5.Text = "MAR"
    '            ElseIf Me.Ultimo_mesTextBox1.Text = 4 Then
    '                Me.TextBox5.Text = "ABR"
    '            ElseIf Me.Ultimo_mesTextBox1.Text = 5 Then
    '                Me.TextBox5.Text = "MAY"
    '            ElseIf Me.Ultimo_mesTextBox1.Text = 6 Then
    '                Me.TextBox5.Text = "JUN"
    '            ElseIf Me.Ultimo_mesTextBox1.Text = 7 Then
    '                Me.TextBox5.Text = "JUL"
    '            ElseIf Me.Ultimo_mesTextBox1.Text = 8 Then
    '                Me.TextBox5.Text = "AGO"
    '            ElseIf Me.Ultimo_mesTextBox1.Text = 9 Then
    '                Me.TextBox5.Text = "SEP"
    '            ElseIf Me.Ultimo_mesTextBox1.Text = 10 Then
    '                Me.TextBox5.Text = "OCT"
    '            ElseIf Me.Ultimo_mesTextBox1.Text = 11 Then
    '                Me.TextBox5.Text = "NOV"
    '            ElseIf Me.Ultimo_mesTextBox1.Text = 12 Then
    '                Me.TextBox5.Text = "DIC"
    '            Else
    '                Me.TextBox5.Text = "---"
    '            End If
    '        End If
    '    End If
    'End Sub

    Private Sub Button11_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Button11.KeyPress
        'Estas dos Líneas las Puso Eric. Actualizan Las fechas.
        eEntraUM = True
        eEntraUMB = False
    End Sub

    Private Sub Button7_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Button7.KeyPress
        'Estas dos Líneas las Puso Eric. Actualizan Las fechas.
        eEntraUM = True
        eEntraUMB = False
    End Sub

    Private Sub Button8_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Button8.KeyPress
        'Estas dos Líneas las Puso Eric. Actualizan Las fechas.
        eEntraUM = True
        eEntraUMB = False
    End Sub

    Private Sub PRIMERMENSUALIDACheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged

    End Sub

    Private Sub PrimerMensualidadCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    'Private Sub CortesiaCheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If Me.CortesiaCheckBox2.CheckState = CheckState.Checked Then
    '        cortesiaint = True
    '    ElseIf Me.CortesiaCheckBox2.CheckState = CheckState.Unchecked Then
    '        cortesiaint = False
    '    End If
    'End Sub

    Private Sub Button20_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button20.Click
        'opcFrm = 12
        'bloqueado = 1

        Dim bloquear As New SoftvPPE.Main
        SoftvPPE.Module1.MyConection = MiConexion
        SoftvPPE.Module1.opcfrm = glopar
        bloquear.formulario = "FrmBloqueo"
        bloquear.contrato = CInt(Me.CONTRATOTextBox.Text)
        bloquear.Hide()
        bloquear.Show()

        'Acceso_TipoServicios.Show() 
    End Sub

    Private Sub Button21_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button21.Click
        opcFrm = 14
        eOpcion = OpcionCli
        eGloContrato = Me.CONTRATOTextBox.Text
        'eRobo = True
        Acceso_TipoServicios.Show()
    End Sub

    'Private Sub Button22_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If Me.CortesiaCheckBox.Checked = False Then
    '        eTipSer = 1
    '        If eAccesoAdmin = True Then
    '            eContrato = Me.CONTRATOTextBox.Text
    '            FrmRelCteDescuento.Show()
    '        Else
    '            eGloDescuento = 0
    '            eGloTipSerDesc = 2
    '            FrmAccesopoUsuario.Show()
    '        End If
    '    Else
    '        MsgBox("No Puede Aplicar un Descuento, ya que cuenta con una Cortesía", , "Atención")
    '    End If
    'End Sub

    'Private Sub Button23_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If Me.CortesiaCheckBox2.Checked = False Then
    '        eTipSer = 2
    '        If eAccesoAdmin = True Then
    '            eClv_UnicaNet = GloClvUnicaNet
    '            FrmRelCteDescuento.Show()
    '        Else
    '            eGloDescuento = 0
    '            eGloTipSerDesc = 2
    '            FrmAccesopoUsuario.Show()
    '        End If
    '    Else
    '        MsgBox("No Puede Aplicar un Descuento, ya que cuenta con una Cortesía", , "Atención")
    '    End If
    'End Sub


    Private Sub CortesiaCheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CortesiaCheckBox1.CheckedChanged
        If Me.CortesiaCheckBox1.CheckState = CheckState.Checked Then
            cortesiadig = True
            Me.TipoCortesiaComboBox.Visible = True
            MUESTRATIPOCORTESIA()
            DAMETIPOCORTESIACLIENTE(loc_Clv_InicaDig, 3)
        ElseIf Me.CortesiaCheckBox1.CheckState = CheckState.Unchecked Then
            cortesiadig = False
            Me.TipoCortesiaComboBox.Visible = False
        End If
    End Sub

    'Private Sub BuscaDescTV()
    '    Dim CON2 As New SqlConnection(MiConexion)
    '    Dim eRes As Integer = 0
    '    Dim eMsg As String = Nothing
    '    If IsNumeric(Me.CONTRATOTextBox.Text) = True Then
    '        CON2.Open()
    '        Me.ChecaRelCteDescuentoTableAdapter.Connection = CON2
    '        Me.ChecaRelCteDescuentoTableAdapter.Fill(Me.DataSetEric.ChecaRelCteDescuento, Me.CONTRATOTextBox.Text, 1, eRes, eMsg)
    '        CON2.Close()
    '        If eRes = 1 Then
    '            CON2.Open()
    '            Me.ConRelCteDescuentoTableAdapter.Connection = CON2
    '            Me.ConRelCteDescuentoTableAdapter.Fill(Me.DataSetEric.ConRelCteDescuento, Me.CONTRATOTextBox.Text, 1)
    '            CON2.Close()
    '            Me.DescuentoLabel2.Visible = True
    '            Me.Label40.Visible = True
    '        Else
    '            Me.DescuentoLabel2.Visible = False
    '            Me.Label40.Visible = False
    '        End If
    '    End If
    'End Sub

    'Private Sub BuscaDescNet()
    '    Dim CON2 As New SqlConnection(MiConexion)
    '    Dim eRes As Integer = 0
    '    Dim eMsg As String = Nothing
    '    If GloClvUnicaNet > 0 Then
    '        CON2.Open()
    '        Me.ChecaRelCteDescuentoTableAdapter.Connection = CON2
    '        Me.ChecaRelCteDescuentoTableAdapter.Fill(Me.DataSetEric.ChecaRelCteDescuento, GloClvUnicaNet, 2, eRes, eMsg)
    '        CON2.Close()
    '        If eRes = 1 Then
    '            CON2.Open()
    '            Me.ConRelCteDescuentoTableAdapter.Connection = CON2
    '            Me.ConRelCteDescuentoTableAdapter.Fill(Me.DataSetEric.ConRelCteDescuento, GloClvUnicaNet, 2)
    '            CON2.Close()
    '            'Me.DescuentoLabel3.Visible = True
    '            'Me.Label44.Visible = True
    '            'Else
    '            '    Me.DescuentoLabel3.Visible = False
    '            '    Me.Label44.Visible = False
    '        End If
    '    End If
    'End Sub

    Private Sub BuscaDescDig()
        Dim CON2 As New SqlConnection(MiConexion)
        Dim eRes As Integer = 0
        Dim eMsg As String = Nothing
        If loc_Clv_InicaDig > 0 Then
            CON2.Open()
            Me.ChecaRelCteDescuentoTableAdapter.Connection = CON2
            Me.ChecaRelCteDescuentoTableAdapter.Fill(Me.DataSetEric.ChecaRelCteDescuento, loc_Clv_InicaDig, eClv_TipSer, eRes, eMsg)
            CON2.Close()
            If eRes = 1 Then
                CON2.Open()
                Me.ConRelCteDescuentoTableAdapter.Connection = CON2
                Me.ConRelCteDescuentoTableAdapter.Fill(Me.DataSetEric.ConRelCteDescuento, loc_Clv_InicaDig, eClv_TipSer)
                CON2.Close()
                Me.DescuentoLabel1.Visible = True
                Me.Label39.Visible = True
            Else
                Me.DescuentoLabel1.Visible = False
                Me.Label39.Visible = False
            End If
        End If
    End Sub

    Private Sub Button27_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button27.Click
        LServicio = Me.Label12.Text
        LClv_Servicio = CInt(Me.Label45.Text)
        'LTip_Ser = 3
        LTip_Ser = eClv_TipSer
        LClvUnica = loc_Clv_InicaDig
        FrmPreciosHoteles.Show()
    End Sub

    'Private Sub Button25_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    LClv_Servicio = CInt(Me.TipSerTvComboBox.SelectedValue)
    '    LServicio = Me.TipSerTvComboBox.Text
    '    LTXTServ = Me.TipSerTvComboBox.SelectedValue
    '    LTip_Ser = 1
    '    LClvUnica = Contrato
    '    FrmPreciosHoteles.Show()
    'End Sub

    'Private Sub Button26_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    LClv_Servicio = CInt(Me.Clv_ServicioTextBox.Text)
    '    LServicio = Me.DESCRIPCIONLabel1.Text
    '    LTip_Ser = 2
    '    LClvUnica = GloClvUnicaNet
    '    FrmPreciosHoteles.Show()
    'End Sub


    'Private Sub FECHA_SOLICITUDTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If DateValue(Me.FECHA_SOLICITUDTextBox.Text) = DateValue("01/01/1900") Then
    '            Me.FECHA_SOLICITUDTextBox.Text = ""
    '        End If
    '    Catch
    '    End Try
    'End Sub


    Private Sub muestra_telefonia()
        'Contrato = Me.CONTRATOTextBox.Text
        'FrmCtrl_ServiciosCli.Show()
        'Me.Button8.Enabled = True
        'Me.Button7.Enabled = True
        'Me.Button28.Enabled = False
        BndEsInternet = True
        frmctr.MdiParent = Me
        frmInternet2.MdiParent = Me
        frmctr.WindowState = FormWindowState.Normal
        frmInternet2.Show()
        frmctr.Show()
        frmctr.Boton_Telefonia()
        frmctr.TreeView1.ExpandAll()
        'Me.Panel2.Hide()
        'Me.Panel4.Hide()
        Me.Panel7.Hide()
        'If Me.Button8.Enabled = False And Me.Button28.Visible = True And IdSistema = "LO" Then
        ' Me.Button8.Enabled = True
        'Me.Button28.Enabled = False
        'End If

    End Sub

    Private Sub Button28_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button28.Click

        If Me.Button8.Visible = True Then Me.Button8.Enabled = True
        If Me.Button7.Visible = True Then Me.Button7.Enabled = True
        If Me.Button11.Visible = True Then Me.Button11.Enabled = True
        If Me.Button28.Visible = True Then Me.Button28.Enabled = False



        If Me.Button28.Text = "&Internet" Then
            BndEsInternet = True
            frmctr.MdiParent = Me
            frmInternet2.MdiParent = Me
            frmctr.WindowState = FormWindowState.Normal
            frmInternet2.Show()
            frmctr.Show()
            frmctr.Boton_Internet()
            frmctr.TreeView1.ExpandAll()
            Me.Panel7.Hide()
        ElseIf Me.Button28.Text = "&Televisión" Then
            BndEsInternet = False
            Me.Panel7.Visible = False
            frmctr.Hide()
            frmInternet2.Hide()
        ElseIf Me.Button28.Text = "&Tv Digital" Or Me.Button28.Text = "&Premium" Or Me.Button28.Text = "&Tv Digital MMDS" Then
            eClv_TipSer = 3
            BndEsInternet = False
            CREAARBOLDIGITAL2()
            Panel7.Visible = True
            frmctr.Hide()
            frmInternet2.Hide()

            If IsNumeric(ComboBox14.SelectedValue) = True Then
                If ComboBox14.SelectedValue > 0 Then
                    MuestraServicios_digital(eClv_TipSer, ComboBox14.SelectedValue)
                End If
            End If
        ElseIf Me.Button28.Text = "Tv Digital &Cable" Then
            eClv_TipSer = 6
            BndEsInternet = False
            CREARARBOLDIGITALCABLE()
            Panel7.Visible = True
            frmctr.Hide()
            frmInternet2.Hide()
            If IsNumeric(ComboBox14.SelectedValue) = True Then
                If ComboBox14.SelectedValue > 0 Then
                    MuestraServicios_digital(eClv_TipSer, ComboBox14.SelectedValue)
                End If
            End If
        ElseIf Me.Button28.Text = "&Telefonia" Then
            muestra_telefonia()
        End If

        eEntraUM = True
        eEntraUMB = False

    End Sub

    Private Sub Label41_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Label41_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Label41.KeyPress
        BanderaCambio = 1
    End Sub

    Private Sub Label41_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label41.TextChanged

    End Sub

    Private Sub BindingNavigator4_RefreshItems(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigator4.RefreshItems

    End Sub

    Private Sub MACCABLEMODEMLabel1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        BanderaCambio = 1
    End Sub

    Private Sub MACCABLEMODEMLabel1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Button29_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button29.Click
        FrmReferencias.Show()
    End Sub

    Private Sub ValidaDigital2(ByVal ContratoNet As Long, ByVal Clv_Servicio As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaDigital2", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@ContratoNet", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = ContratoNet
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_Servicio
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Res", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Output
        parametro3.Value = 0
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Msg", SqlDbType.Char, 250)
        parametro4.Direction = ParameterDirection.Output
        parametro4.Value = 0
        comando.Parameters.Add(parametro4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eResValida = CInt(parametro3.Value)
            eMsgValida = parametro4.Value
            conexion.Close()
        Catch ex As Exception

            conexion.Close()
            MsgBox(ex.Message)


        End Try



    End Sub



    Private Sub ValidaDatosBancarios(ByVal Contrato As Long, ByVal Clv_TipoCliente As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaDatosBancarios", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_TipoCliente", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_TipoCliente
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Res", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Output
        parametro3.Value = 0
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Msg", SqlDbType.VarChar, 500)
        parametro4.Direction = ParameterDirection.Output
        parametro4.Value = ""
        comando.Parameters.Add(parametro4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eResValida = CInt(parametro3.Value.ToString)
            eMsgValida = parametro4.Value.ToString
            conexion.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
            conexion.Close()
        End Try



    End Sub

    Private Sub ValidaNombreUsuario(ByVal Nombre As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaNombreUsuario", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Nombre", SqlDbType.VarChar, 250)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Nombre
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Res", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Output
        parametro2.Value = 0
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Msg", SqlDbType.VarChar, 150)
        parametro3.Direction = ParameterDirection.Output
        parametro3.Value = ""
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eResValida = CInt(parametro2.Value.ToString)
            eMsgValida = parametro3.Value.ToString
            conexion.Close()
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message)
        End Try


    End Sub


    Private Sub CELULARTextBox_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles CELULARTextBox.KeyPress
        'e.KeyChar = ChrW(ValidaKey(Me.CELULARTextBox, Asc(e.KeyChar), "N"))
        Dim caracter As Char = e.KeyChar
        If (Char.IsNumber(caracter)) Or _
                (caracter = ChrW(Keys.Back)) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub TextBox7_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox7.TextChanged

    End Sub

    Private Sub Button30_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button30.Click
        FrmRefBan.Show()
    End Sub

    Private Sub BuscaCuenta()
        If IdSistema = "LO" Or IdSistema = "YU" Then
            'If Contrato = Nothing Or Contrato = 0 Then
            '    Contrato = CLng(Me.CONTRATOTextBox.Text)
            'End If
            If OpcionCli = "N" Then
                Contrato = CLng(Me.CONTRATOTextBox.Text)
            End If

            Dim con As New SqlConnection(MiConexion)
            Dim reader As SqlDataReader
            Dim cmd As New SqlCommand("CONSULTA_RelCuenta", con)
            cmd.CommandType = CommandType.StoredProcedure

            Dim prm As New SqlParameter( _
                      "@Contrato", SqlDbType.BigInt)
            prm.Direction = ParameterDirection.Input
            prm.Value = Contrato
            cmd.Parameters.Add(prm)

            Try
                con.Open()
                reader = cmd.ExecuteReader()
                Using reader
                    While reader.Read
                        Me.TextBox28.Text = reader.GetValue(0)
                    End While
                End Using
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
            con.Close()
        End If
    End Sub

    Private Sub GuardaCuenta()
        If OpcionCli <> "N" Then
            Dim con As New SqlConnection(MiConexion)
            Dim cmd As New SqlCommand("GUARDA_RelCuenta", con)
            cmd.CommandType = CommandType.StoredProcedure

            Dim prm As New SqlParameter( _
                      "@Contrato", SqlDbType.BigInt)
            prm.Direction = ParameterDirection.Input
            prm.Value = Contrato
            cmd.Parameters.Add(prm)

            prm = New SqlParameter( _
                     "@Cuenta", SqlDbType.VarChar, 50)
            prm.Direction = ParameterDirection.Input
            prm.Value = Me.TextBox28.Text
            cmd.Parameters.Add(prm)

            Try
                con.Open()
                Dim i As Integer = cmd.ExecuteNonQuery()

            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
            con.Close()
        End If
    End Sub

    Private Sub Panel11_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel11.Paint

    End Sub

    Private Sub DameComboYRenta(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("DameComboYRenta", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@DescCombo", SqlDbType.VarChar, 150)
        parametro2.Direction = ParameterDirection.Output
        parametro2.Value = String.Empty
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            Me.LabelComboYRenta.Text = parametro2.Value.ToString
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub


    Private Sub BtnEstadoDeCuenta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnEstadoDeCuenta.Click
        LocGloContratoIni = Me.CONTRATOTextBox.Text
        LocGloContratoFin = Me.CONTRATOTextBox.Text
        LocbndImpresionEdoCuentaLog = True
        FrmImprimirContrato.Show()
    End Sub

    Private Sub Button31_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button31.Click
        FrmClienteObs.Show()
    End Sub
    Private Sub Muestra_Usuarios(ByVal clv_unicanet As Long, ByVal tipo_serv As Integer)
        If OpcionCli = "C" Then
            Me.ComboBox20.Enabled = False
        End If
        Dim con As New SqlConnection(MiConexion)
        Dim str As New StringBuilder

        str.Append("Exec Muestra_Usuarios ")
        str.Append(CStr(clv_unicanet) & ", ")
        str.Append(CStr(tipo_serv))

        Dim dataadapter As New SqlDataAdapter(str.ToString, con)
        Dim datatable As New DataTable
        Dim binding As New BindingSource

        Try
            con.Open()
            dataadapter.Fill(datatable)
            binding.DataSource = datatable
            Me.ComboBox20.DataSource = binding
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            con.Close()
            con.Dispose()
        End Try


    End Sub
    Private Sub Actualiza_usuario(ByVal clv_unicanet As Long, ByVal clv_usuario As Integer, ByVal tipo_serv As Integer)
        If OpcionCli = "M" Then
            Dim con As New SqlConnection(MiConexion)
            Dim str As New StringBuilder

            str.Append("Exec Actualiza_usuarios ")
            str.Append(CStr(clv_unicanet) & ", ")
            str.Append(CStr(clv_usuario) & ", ")
            str.Append(CStr(tipo_serv))

            Dim dataadapter As New SqlDataAdapter(str.ToString, con)
            Dim datatable As New DataTable
            Dim binding As New BindingSource

            Try
                con.Open()
                dataadapter.Fill(datatable)
                binding.DataSource = datatable
            Catch ex As Exception
                System.Windows.Forms.MessageBox.Show(ex.Message)
            Finally
                con.Close()
                con.Dispose()
            End Try
        End If
    End Sub

    Private Sub ComboBox17_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Muestra_Usuarios(GloClvUnicaNet)
    End Sub

    Private Sub FECHA_ULT_PAGOLabel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ComboBox18_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Muestra_Usuarios(GloClvUnicaNet)
    End Sub

    Private Sub Panel6_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs)

    End Sub


    Private Sub btnHisDes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHisDes.Click
        Dim desconexiones As BitacoraSuspendidos = New BitacoraSuspendidos
        desconexiones.Show()
    End Sub


    Public Sub RefrescaDig()
        If Me.Button8.Visible = True Then Me.Button8.Enabled = True
        If Me.Button7.Visible = True Then Me.Button7.Enabled = True
        If Me.Button11.Visible = True Then Me.Button11.Enabled = False
        If Me.Button28.Visible = True Then Me.Button28.Enabled = True


        If Me.Button11.Text = "&Internet" Then
            BndEsInternet = True
            frmctr.MdiParent = Me
            frmInternet2.MdiParent = Me
            frmctr.WindowState = FormWindowState.Normal
            frmInternet2.Show()
            frmctr.Show()
            frmctr.Boton_Internet()
            frmctr.TreeView1.ExpandAll()
            'Me.Panel2.Hide()
            'Me.Panel4.Hide()
            Me.Panel7.Hide()
            'If Me.Button28.Enabled = False And Me.Button28.Visible = True And (IdSistema = "LO" Or IdSistema = "YU") Then
            '    Me.Button28.Enabled = True
            '    Button8.Enabled = False
            'End If
        ElseIf Me.Button11.Text = "&Televisión" Then

            BndEsInternet = False
            'Me.Panel2.Visible = True
            'Me.Panel4.Visible = False
            Me.Panel7.Visible = False
            frmctr.Hide()
            'No lo Usas frmTelefonia.Hide()
            frmInternet2.Hide()

        ElseIf Me.Button11.Text = "&Tv Digital" Or Me.Button11.Text = "&Premium" Then
            BndEsInternet = False
            'Me.Panel2.Visible = False
            'Me.Panel4.Visible = False
            Me.Panel7.Visible = True
            frmctr.Hide()
            'No lo Usas frmTelefonia.Hide()
            frmInternet2.Hide()

            If IsNumeric(ComboBox14.SelectedValue) = True Then
                If ComboBox14.SelectedValue > 0 Then
                    MuestraServicios_digital(eClv_TipSer, ComboBox14.SelectedValue)
                End If
            End If
        ElseIf Me.Button11.Text = "&Telefonia" Then
            muestra_telefonia()
        End If
        'Eric----------
        'BuscaDescTV()
        '--------------
        'Estas dos Líneas las Puso Eric. Actualizan Las fechas.
        eEntraUM = True
        eEntraUMB = False

        ''Contrato = Me.CONTRATOTextBox.Text
        'BndEsInternet = True
        'frmctr.MdiParent = Me
        'frmInternet2.MdiParent = Me
        'frmctr.WindowState = FormWindowState.Normal
        'frmInternet2.Show()
        'frmctr.Show()
        'frmctr.TreeView1.ExpandAll()

        'Me.Panel2.Hide()
        'Me.Panel4.Hide()
        'Me.Panel7.Hide()

        ''Eric---------------
        'BuscaDescNet()
        ''-------------------

        ''Estas dos Líneas las Puso Eric. Actualizan Las fechas.
        'eEntraUM = True
        'eEntraUMB = False

        ''Me.Panel7.Visible = False
        ''Me.Panel2.Visible = False
        ''Me.Panel4.Visible = True
        ''If IsNumeric(ComboBox14.SelectedValue) = True Then
        ''    If ComboBox14.SelectedValue > 0 Then
        ''        Me.MuestraServicios_digitalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraServicios_digital, 3, Me.ComboBox14.SelectedValue)
        ''    End If
        ''End If
    End Sub

    Private Sub ValidaNombreUsuarioSeparado(ByVal Nombre As String, ByVal ApellidoPaterno As String, ByVal ApellidoMaterno As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaNombreUsuarioSeparado", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Nombre", SqlDbType.VarChar, 250)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Nombre
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@ApellidoPaterno", SqlDbType.VarChar, 250)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = ApellidoPaterno
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@ApellidoMaterno", SqlDbType.VarChar, 250)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = ApellidoMaterno
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Res", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Output
        parametro4.Value = 0
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Msg", SqlDbType.VarChar, 150)
        parametro5.Direction = ParameterDirection.Output
        parametro5.Value = ""
        comando.Parameters.Add(parametro5)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eResValida = CInt(parametro4.Value.ToString)
            eMsgValida = parametro5.Value.ToString
            conexion.Close()
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ConClientesApellidos(ByVal ContratoNombres As String)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("ConClientesApellidos", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@Contrato", ContratoNombres)

        Dim READER As SqlDataReader

        Try
            CON.Open()
            READER = CMD.ExecuteReader()

            While (READER.Read)
                Me.TextBox27.Text = READER(0).ToString
                Me.TextBox4.Text = READER(1).ToString
                Me.TextBox7.Text = READER(2).ToString
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub
    Private Sub GuardaClientesApellidos()


        Dim conApe As New SqlConnection(MiConexion)
        Dim CmdAp As New SqlCommand
        conApe.Open()
        With CmdAp
            .CommandText = "GuardaClientesApellidos"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = conApe

            Dim Pmt As New SqlParameter("@Contrato", SqlDbType.BigInt)
            Pmt.Direction = ParameterDirection.Input
            If IsNumeric(Me.CONTRATOTextBox.Text) = False Then
                Pmt.Value = 0
            Else
                Pmt.Value = Me.CONTRATOTextBox.Text
            End If

            Dim Pmt2 As New SqlParameter("@Nombre", SqlDbType.VarChar, 250)
            Pmt2.Direction = ParameterDirection.Input
            Pmt2.Value = Me.TextBox27.Text

            Dim Pmt3 As New SqlParameter("@ApellidoPaterno", SqlDbType.VarChar, 250)
            Pmt3.Direction = ParameterDirection.Input
            Pmt3.Value = Me.TextBox4.Text

            Dim Pmt4 As New SqlParameter("@ApellidoMaterno", SqlDbType.VarChar, 250)
            Pmt4.Direction = ParameterDirection.Input
            Pmt4.Value = Me.TextBox7.Text

            .Parameters.Add(Pmt)
            .Parameters.Add(Pmt2)
            .Parameters.Add(Pmt3)
            .Parameters.Add(Pmt4)

            .ExecuteNonQuery()
        End With
    End Sub

    Private Sub MUESTRATIPOSCLIENTE()
        Dim CON As New SqlConnection(MiConexion)
        Dim STRSQL As New StringBuilder

        STRSQL.Append("EXEC MUESTRATIPOSCLIENTE ")

        Dim DA As New SqlDataAdapter(STRSQL.ToString, CON)
        Dim DT As New DataTable
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.TipoClienteCombo.DataSource = BS.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub
    Private Sub MUESTRASUBTIPOSCLIENTE(ByVal TIPOCLIENTE As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim STRSQL As New StringBuilder

        STRSQL.Append("EXEC MUESTRASUBTIPOSCLIENTE ")
        STRSQL.Append(CStr(TIPOCLIENTE))

        Dim DA As New SqlDataAdapter(STRSQL.ToString, CON)
        Dim DT As New DataTable
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.TipoSubClienteCombo.DataSource = BS.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub CONTIPOSCLIENTE(ByVal CONTRATOTIPCLIENTE As Long)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("CONTIPOSCLIENTE", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@CONTRATO", CONTRATOTIPCLIENTE)

        Dim Reader As SqlDataReader

        Try
            CON.Open()
            Reader = CMD.ExecuteReader()

            While (Reader.Read)
                Me.TipoClienteCombo.SelectedValue = Reader(0).ToString
                Me.TipoSubClienteCombo.SelectedValue = Reader(1).ToString
                Me.GeneroCombo.SelectedValue = Reader(2).ToString
                Me.Referencia1Text.Text = Reader(3).ToString
                Me.Referencia2Text.Text = Reader(4).ToString
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub
    Private Sub ActualizaDatosTipoCliente(ByVal ContratoTipCliente As Long, ByVal Clv_TipoCliente As Integer, ByVal Clv_SubTipoCliente As Integer, _
                                          ByVal Clv_Genero As String, ByVal Referencia1 As String, ByVal Referencia2 As String)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("ActualizaDatosTipoCliente", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@CONTRATO", ContratoTipCliente)
        CMD.Parameters.AddWithValue("@Clv_TipoCliente", Clv_TipoCliente)
        CMD.Parameters.AddWithValue("@Clv_SubTipoCliente", Clv_SubTipoCliente)
        CMD.Parameters.AddWithValue("@Clv_Genero", Clv_Genero)
        CMD.Parameters.AddWithValue("@Referencia1", Referencia1)
        CMD.Parameters.AddWithValue("@Referencia2", Referencia2)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub TipoClienteCombo_SelectedIndexChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TipoClienteCombo.SelectedIndexChanged
        MUESTRASUBTIPOSCLIENTE(Me.TipoClienteCombo.SelectedValue)
    End Sub

    Private Sub CONSULTAGENERO()
        Dim CON As New SqlConnection(MiConexion)
        Dim STRSQL As New StringBuilder

        STRSQL.Append("EXEC CONSULTAGENERO ")

        Dim DA As New SqlDataAdapter(STRSQL.ToString, CON)
        Dim DT As New DataTable
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.GeneroCombo.DataSource = BS.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub
    Private Sub MUESTRATIPOCORTESIA()
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSql As New StringBuilder

        StrSql.Append("EXEC MUESTRATIPOCORTESIA ")

        Dim DA As New SqlDataAdapter(StrSql.ToString, CON)
        Dim DT As New DataTable

        Try
            CON.Open()
            DA.Fill(DT)
            Me.TipoCortesiaComboBox.DataSource = DT
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub DAMETIPOCORTESIACLIENTE(ByVal Clv_UnicaNetCortesia As Long, ByVal Clv_TipSerCortesia As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("DAMETIPOCORTESIACLIENTE", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@CLV_UNICANET", Clv_UnicaNetCortesia)
        CMD.Parameters.AddWithValue("@Clv_TipSer", Clv_TipSerCortesia)

        Dim Reader As SqlDataReader

        Try
            CON.Open()
            Reader = CMD.ExecuteReader
            While (Reader.Read)
                Me.TipoCortesiaComboBox.SelectedValue = Reader(0).ToString
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub NUETIPOCORTESIA(ByVal CLV_UNICANET As Long, ByVal CLV_CORTESIA As Integer, ByVal Clv_TipSerCortesia As Integer, ByVal OPCORTESIA As Boolean)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("NUETIPOCORTESIA", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@CLV_UNICANET", CLV_UNICANET)
        CMD.Parameters.AddWithValue("@CLV_CORTESIA", CLV_CORTESIA)
        CMD.Parameters.AddWithValue("@Clv_TipSer", Clv_TipSerCortesia)
        CMD.Parameters.AddWithValue("@OP", OPCORTESIA)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub NoInsenTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NoInsenTextBox.TextChanged
        If Len(Me.NoInsenTextBox.Text) > 0 Then
            Me.InsenPanel.BackColor = Color.WhiteSmoke
        Else
            Me.InsenPanel.BackColor = Color.Yellow
        End If
    End Sub

    Private Sub NueClienteInsen(ByVal CONTRATOINSEN As Integer, ByVal TIPOCLIENTEINSEN As Integer, ByVal NumTarjetaINSEN As String, ByVal OPINSEN As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("NueClienteInsen", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@CONTRATO", CONTRATOINSEN)
        CMD.Parameters.AddWithValue("@TIPOCLIENTE", TIPOCLIENTEINSEN)
        CMD.Parameters.AddWithValue("@NUMCREDENCIAL", NumTarjetaINSEN)
        CMD.Parameters.AddWithValue("@OP", OPINSEN)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub ConClienteInsen(ByVal CONTRATOINSEN As Integer, ByVal TIPOCLIENTEINSEN As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("ConClienteInsen", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@CONTRATO", CONTRATOINSEN)
        CMD.Parameters.AddWithValue("@TIPOCLIENTE", TIPOCLIENTEINSEN)

        Dim Reader As SqlDataReader

        Try
            CON.Open()
            Reader = CMD.ExecuteReader
            While (Reader.Read)
                Me.NoInsenTextBox.Text = Reader(0).ToString
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub ConsultaAparatosContratacion()
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSql As New StringBuilder

        StrSql.Append("EXEC ConsultaAparatosContratacion ")
        If Len(Me.ComboBox12.SelectedValue) = 0 Then
            StrSql.Append(CStr(0))
        Else
            StrSql.Append(CStr(Me.ComboBox12.SelectedValue))
        End If

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(StrSql.ToString(), CON)
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.AparatosComboBox.DataSource = BS.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub DimeSiEsPrincipal()
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("DimeSiEsPrincipal", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@CLV_SERVICIO", SqlDbType.Int)
        PRM1.Direction = ParameterDirection.Input
        If Len(Me.ComboBox12.SelectedValue) = 0 Then
            PRM1.Value = 0
        Else
            PRM1.Value = Me.ComboBox12.SelectedValue
        End If
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@PRINCIPAL", SqlDbType.Bit)
        PRM2.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM2)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            If PRM2.Value = True Then
                Me.AparatosComboBox.Visible = True
                Me.Label55.Visible = True
            Else
                Me.AparatosComboBox.Visible = False
                Me.Label55.Visible = False
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub ComboBox12_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox12.SelectedIndexChanged
        DimeSiEsPrincipal()
        ConsultaAparatosContratacion()
    End Sub

    Private Sub GuardaRelClientesAparatos(ByVal ContratoAparato As Integer, ByVal ContratoNetAparato As Integer, ByVal ClvServicioAparato As Integer, ByVal IdArticuloAparato As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("GuardaRelClientesAparatos", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@CONTRATO", ContratoAparato)
        CMD.Parameters.AddWithValue("@CONTRATONET", ContratoNetAparato)
        CMD.Parameters.AddWithValue("@CLV_SERVICIO", ClvServicioAparato)
        CMD.Parameters.AddWithValue("@IDARTICULO", IdArticuloAparato)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub ConsultaRelClientesAparatos(ByVal ContratoNetAparato As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("ConsultaRelClientesAparatos", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = CInt(Me.CONTRATOTextBox.Text)
        CMD.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@ContratoNet", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Input
        par2.Value = ContratoNetAparato
        CMD.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@MARCA", SqlDbType.VarChar, 250)
        par3.Direction = ParameterDirection.Output
        CMD.Parameters.Add(par3)

        'CMD.Parameters.AddWithValue("@CONTRATO", Me.CONTRATOTextBox.Text)
        'CMD.Parameters.AddWithValue("@ContratoNet", ContratoNetAparato)
        'CMD.Parameters.AddWithValue("@MARCA", "").Direction = ParameterDirection.Output
        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            Me.MarcaAparatoTextBox.Text = par3.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Function MuestraClientesAnt(ByVal Contrato As Integer) As DataTable

        Dim conexion As New SqlConnection(MiConexion)
        Dim sBuilder As New StringBuilder("EXEC MuestraClientesAnt " + Contrato.ToString())
        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
        Dim dTable As New DataTable

        Try
            dAdapter.Fill(dTable)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

        Return dTable

    End Function

    Function MuestraClientesDig(ByVal ContratoAnt As Integer) As DataTable

        Dim conexion As New SqlConnection(MiConexion)
        Dim sBuilder As New StringBuilder("EXEC MuestraClientesDig " + ContratoAnt.ToString())
        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
        Dim dTable As New DataTable

        Try
            dAdapter.Fill(dTable)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

        Return dTable

    End Function

    Private Sub ConClientesAnt(ByVal ContratoAnt As Long)

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ConClientesAnt", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim reader As SqlDataReader
        Dim status As String

        Dim par1 As New SqlParameter("@ContratoAnt", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = ContratoAnt
        comando.Parameters.Add(par1)

        Try
            conexion.Open()
            reader = comando.ExecuteReader()

            While (reader.Read())
                status = reader(0).ToString
                tbAntActivacion.Text = reader(1).ToString()
                tbAntSuspencion.Text = reader(2).ToString()
                tbAntBaja.Text = reader(3).ToString()
                tbAntObs.Text = reader(4).ToString()
                lbAntMacCablemodem.Text = reader(5).ToString()
                lblAntenaConMarca.Text = reader(6).ToString()
            End While

            If tbAntActivacion.Text = "01/01/1900" Then tbAntActivacion.Text = ""
            If tbAntSuspencion.Text = "01/01/1900" Then tbAntSuspencion.Text = ""
            If tbAntBaja.Text = "01/01/1900" Then tbAntBaja.Text = ""

            StatusAntena(ContratoAnt)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Close()
        End Try

    End Sub

    Private Sub ModClientesAnt(ByVal ContratoAnt As Long, ByVal Status As String, ByVal FechaActivacion As DateTime, ByVal FechaSuspencion As DateTime, ByVal FechaBaja As DateTime, ByVal Obs As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ModClientesAnt", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par0 As New SqlParameter("@ContratoAnt", SqlDbType.BigInt)
        par0.Direction = ParameterDirection.Input
        par0.Value = ContratoAnt
        comando.Parameters.Add(par0)

        Dim par1 As New SqlParameter("@Status", SqlDbType.VarChar, 1)
        par1.Direction = ParameterDirection.Input
        par1.Value = Status
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@FechaActivacion", SqlDbType.DateTime)
        par2.Direction = ParameterDirection.Input
        par2.Value = FechaActivacion
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@FechaSuspencion", SqlDbType.DateTime)
        par3.Direction = ParameterDirection.Input
        par3.Value = FechaSuspencion
        comando.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@FechaBaja", SqlDbType.DateTime)
        par4.Direction = ParameterDirection.Input
        par4.Value = FechaBaja
        comando.Parameters.Add(par4)

        Dim par5 As New SqlParameter("@Obs", SqlDbType.VarChar, 250)
        par5.Direction = ParameterDirection.Input
        par5.Value = Obs
        comando.Parameters.Add(par5)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()

            MsgBox(mensaje5, MsgBoxStyle.Information)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub StatusAntena(ByVal ContratoAnt As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim sBuilder As New StringBuilder("EXEC StatusAntena " + ContratoAnt.ToString())
        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
        Dim dTable As New DataTable
        Dim bSource As New BindingSource

        Try
            dAdapter.Fill(dTable)
            bSource.DataSource = dTable
            cbAntStatus.DataSource = bSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub tsbAntGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbAntGuardar.Click

        If LocGloContratoAnt = 0 Then Exit Sub

        If tbAntActivacion.Text = "" Then tbAntActivacion.Text = "01/01/1900"
        If tbAntSuspencion.Text = "" Then tbAntSuspencion.Text = "01/01/1900"
        If tbAntBaja.Text = "" Then tbAntBaja.Text = "01/01/1900"

        ModClientesAnt(LocGloContratoAnt, cbAntStatus.SelectedValue, tbAntActivacion.Text, tbAntSuspencion.Text, tbAntBaja.Text, tbAntObs.Text)

    End Sub

    Private Sub ConRelClientesDatosAdicionales(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ConRelClientesDatosAdicionales", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0
        Dim reader As SqlDataReader

        Dim par1 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = Contrato
        comando.Parameters.Add(par1)

        Try
            conexion.Open()
            reader = comando.ExecuteReader

            While (reader.Read())
                cbCasaPropia.Checked = Boolean.Parse(reader(0).ToString)
                tbTelefonoTrabajo.Text = reader(1).ToString
                tbIFE.Text = reader(2).ToString
            End While

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub NueRelClientesDatosAdicionales(ByVal Contrato As Long, ByVal CasaPropia As Boolean, ByVal TelefonoTrabajo As String, ByVal IFE As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueRelClientesDatosAdicionales", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = Contrato
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@CasaPropia", SqlDbType.Bit)
        par2.Direction = ParameterDirection.Input
        par2.Value = CasaPropia
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@TelefonoTrabajo", SqlDbType.VarChar, 50)
        par3.Direction = ParameterDirection.Input
        par3.Value = TelefonoTrabajo
        comando.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@IFE", SqlDbType.VarChar, 50)
        par4.Direction = ParameterDirection.Input
        par4.Value = IFE
        comando.Parameters.Add(par4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub ValidaIFE(ByVal IFE As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaIFE", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0
        Dim Contrato As Long = 0

        If CONTRATOTextBox.Text.Length = 0 Then Contrato = 0
        If CONTRATOTextBox.Text.Length > 0 Then Contrato = CONTRATOTextBox.Text

        Dim par0 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        par0.Direction = ParameterDirection.Input
        par0.Value = Contrato
        comando.Parameters.Add(par0)

        Dim par1 As New SqlParameter("@IFE", SqlDbType.VarChar, 50)
        par1.Direction = ParameterDirection.Input
        par1.Value = IFE
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        par2.Direction = ParameterDirection.Output
        comando.Parameters.Add(par2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eMsj = ""
            eMsj = par2.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try


    End Sub


    Private Sub ValidaTipoUsuario(ByVal Op As Integer, ByVal Clv_Usuario As String, ByVal Pasaporte As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaTipoUsuario", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Op", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = Op
        comando.Parameters.Add(par1)


        Dim par2 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar, 5)
        par2.Direction = ParameterDirection.Input
        par2.Value = Clv_Usuario
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Pasaporte", SqlDbType.VarChar, 20)
        par3.Direction = ParameterDirection.Input
        par3.Value = Pasaporte
        comando.Parameters.Add(par3)

        Dim par5 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        par5.Direction = ParameterDirection.Output
        comando.Parameters.Add(par5)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eMsj = ""
            eMsj = par5.Value

            If eMsj.Length = 0 Then
                eBndAutorizacionIFERepetido = True
                eBndAutorizacionNombreRepetido = True
                eBndAutorizacionDireccionRepetido = True
            Else
                eBndAutorizacionIFERepetido = False
                eBndAutorizacionNombreRepetido = False
                eBndAutorizacionDireccionRepetido = False
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub tbTelefonoTrabajo_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles tbTelefonoTrabajo.KeyPress
        'e.KeyChar = ChrW(ValidaKey(tbTelefonoTrabajo, Asc(e.KeyChar), "N"))
        Dim caracter As Char = e.KeyChar
        If (Char.IsNumber(caracter)) Or _
                (caracter = ChrW(Keys.Back)) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub DAMESECTORORDENES(ByVal CLVCOLONIA As Long)
        Dim CON As New SqlConnection(MiConexion)
        Dim STRSQL As New StringBuilder

        STRSQL.Append("EXEC DAMESECTORORDENESCLIENTES ")
        STRSQL.Append(CStr(CLVCOLONIA))

        Dim DA As New SqlDataAdapter(STRSQL.ToString, CON)
        Dim DT As New DataTable
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.cmbSector.DataSource = BS.DataSource
            If (Me.CONTRATOTextBox.Text.Trim.Equals("")) Then
            Else
                DAMETAPYSECTORCLIENTE(Me.CONTRATOTextBox.Text)
            End If


        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub DAMETAPYSECTORCLIENTE(ByVal ContratosSecTap As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("DAMETAPYSECTORCLIENTE", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@CONTRATO", ContratosSecTap)
        CMD.Parameters.AddWithValue("@CLV_SECTOR", 0).Direction = ParameterDirection.Output

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            Me.cmbSector.SelectedValue = CMD.Parameters("@CLV_SECTOR").Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub ACTUALIZASECTORCLIENTE(ByVal ContratoTap As Long, ByVal CLVSECTOR As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("ACTUALIZASECTORCLIENTE", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@CONTRATO", ContratoTap)
        CMD.Parameters.AddWithValue("@CLV_SECTOR", CLVSECTOR)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub tbIFE_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles tbIFE.KeyPress
        Dim caracter As Char = e.KeyChar
        If (Char.IsNumber(caracter)) Or _
                (caracter = ChrW(Keys.Back)) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub



    '---    Validación de Domicilio Duplicado
    Private Function ValidaDireccionDuplicadaContratoModificar(ByVal Sesion As Integer, ByVal Colonia As Integer, ByVal Calle As Integer, ByVal Numero As String, ByVal Contrato As Integer) As Boolean
        Dim Duplicada As Boolean = False
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, Sesion)
            BaseII.CreateMyParameter("@clv_colonia", SqlDbType.Int, Colonia)
            BaseII.CreateMyParameter("@clv_calle", SqlDbType.Int, Calle)
            BaseII.CreateMyParameter("@numero", SqlDbType.VarChar, Numero)
            BaseII.CreateMyParameter("@error", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@ContratoModificado", SqlDbType.Int, Contrato)
            Dim listaDeParametros As Dictionary(Of String, Object) = BaseII.ProcedimientoOutPut("uspValidaDireccionDuplicadaContratoModificar")
            If (CInt(listaDeParametros("@error").ToString)) = 1 Then
                Duplicada = True
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
        Return Duplicada
    End Function
    Private Function ValidaNombreDuplicadoContratoModificar(ByVal Nombre As String, ByVal ApellidoPaterno As String, ByVal ApellidoMaterno As String, ByVal Contrato As Integer) As Boolean
        Dim Duplicado As Boolean = False

        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, Nombre, 250)
            BaseII.CreateMyParameter("@ApellidoPaterno", SqlDbType.VarChar, ApellidoPaterno, 250)
            BaseII.CreateMyParameter("@ApellidoMaterno", SqlDbType.VarChar, ApellidoMaterno, 250)
            BaseII.CreateMyParameter("@Res", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@Msg", ParameterDirection.Output, SqlDbType.VarChar, 250)
            BaseII.CreateMyParameter("@ContratoModificado", SqlDbType.Int, Contrato)

            Dim listaDeParametros As Dictionary(Of String, Object) = BaseII.ProcedimientoOutPut("uspValidaNombreDuplicadoContratoModificar")

            If (CInt(listaDeParametros("@Res").ToString)) = 1 Then
                Duplicado = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
        Return Duplicado

    End Function


    Private Function Valida_Direccion1(ByVal prmClvSession As Long, ByVal prmClvColonia As Long, ByVal prmClvCalle As Long, ByVal prmNumero As String) As Integer
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, prmClvSession)
        BaseII.CreateMyParameter("@clv_colonia", SqlDbType.Int, prmClvColonia)
        BaseII.CreateMyParameter("@clv_calle", SqlDbType.Int, prmClvCalle)
        BaseII.CreateMyParameter("@numero", SqlDbType.VarChar, prmNumero, 10)
        BaseII.CreateMyParameter("@error", ParameterDirection.Output, SqlDbType.Int)

        BaseII.ProcedimientoOutPut("Valida_Direccion1")

        Valida_Direccion1 = CInt(BaseII.dicoPar("@error").ToString)
    End Function

    Private Sub MuestraServicios_digital(ByVal Clv_TipSer As Integer, ByVal ContratoNet As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strBuilder As New StringBuilder("EXEC MuestraServicios_digital " + Clv_TipSer.ToString() + ", " + ContratoNet.ToString())
        Dim dAdapter As New SqlDataAdapter(strBuilder.ToString(), conexion)
        Dim dTable As New DataTable

        Try
            dAdapter.Fill(dTable)
            ComboBox12.DataSource = dTable
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub MUESTRADIGITALDELCLI(ByVal Contrato As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strBuilder As New StringBuilder("EXEC MUESTRADIGITALDELCLI " + Contrato.ToString())
        Dim dAdapter As New SqlDataAdapter(strBuilder.ToString(), conexion)
        Dim dTable As New DataTable

        Try
            dAdapter.Fill(dTable)
            ComboBox14.DataSource = dTable
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Function MuestraClientesDigCable(ByVal Contrato As Integer) As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strBuilder As New StringBuilder("EXEC MUESTRADIGITALDELCLICABLE " + Contrato.ToString())
        Dim dAdapter As New SqlDataAdapter(strBuilder.ToString(), conexion)
        Dim dTable As New DataTable

        Try
            dAdapter.Fill(dTable)
            Return dTable
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Function

    Private Sub MUESTRADIGITALDELCLICABLE(ByVal Contrato As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strBuilder As New StringBuilder("EXEC MUESTRADIGITALDELCLICABLE " + Contrato.ToString())
        Dim dAdapter As New SqlDataAdapter(strBuilder.ToString(), conexion)
        Dim dTable As New DataTable

        Try
            dAdapter.Fill(dTable)
            ComboBox14.DataSource = dTable
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Function MUESTRACONTDIGCABLE(ByVal ContratoNet As Integer) As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strBuilder As New StringBuilder("EXEC MUESTRACONTDIGCABLE " + ContratoNet.ToString())
        Dim dAdapter As New SqlDataAdapter(strBuilder.ToString(), conexion)
        Dim dTable As New DataTable

        Try
            dAdapter.Fill(dTable)
            Return dTable
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Function

    Private Sub NueClientesAnt(ByVal Contrato As Integer, ByVal ContratoNet As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueClientesAnt", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Contrato", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = Contrato
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@ContratoNet", SqlDbType.Int)
        par2.Direction = ParameterDirection.Input
        par2.Value = ContratoNet
        comando.Parameters.Add(par2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub VALIDAClientesDig(ByVal Contrato As Integer, ByVal Clv_TipSer As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("VALIDAClientesDig", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Contrato", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = Contrato
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
        par2.Direction = ParameterDirection.Input
        par2.Value = Clv_TipSer
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        par3.Direction = ParameterDirection.Output
        comando.Parameters.Add(par3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eMsj = ""
            eMsj = par3.Value.ToString()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub NUEClientesDigTipSer(ByVal ContratoNet As Integer, ByVal Clv_TipSer As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NUEClientesDigTipSer", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@ContratoNet", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = ContratoNet
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
        par2.Direction = ParameterDirection.Input
        par2.Value = Clv_TipSer
        comando.Parameters.Add(par2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub DAMEClienteClvTipSer(ByVal Contrato As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("DAMEClienteClvTipSer", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Contrato", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = Contrato
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@ClvTipSer", SqlDbType.Int)
        par2.Direction = ParameterDirection.Output
        comando.Parameters.Add(par2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            ClvTipSer = CInt(par2.Value.ToString())
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub MuestraServicioDigitalTvCable()
        If Me.Button8.Visible = True Then Me.Button8.Enabled = True
        If Me.Button7.Visible = True Then Me.Button7.Enabled = False
        If Me.Button11.Visible = True Then Me.Button11.Enabled = True
        If Me.Button28.Visible = True Then Me.Button28.Enabled = True
        eClv_TipSer = 6

        If Me.Button7.Text = "&Internet" Then
            BndEsInternet = True
            frmctr.MdiParent = Me
            frmInternet2.MdiParent = Me
            frmctr.WindowState = FormWindowState.Normal
            frmInternet2.Show()
            frmctr.Show()
            frmctr.Boton_Internet()
            frmctr.TreeView1.ExpandAll()
            Me.Panel7.Hide()
        ElseIf Me.Button7.Text = "&Televisión" Then
            BndEsInternet = False
            Me.Panel7.Visible = False
            frmctr.Hide()
            frmInternet2.Hide()
        ElseIf Me.Button7.Text = "&Tv Digital" Or Me.Button7.Text = "&Premium" Or Me.Button7.Text = "&Tv Digital MMDS" Then
            eClv_TipSer = 3
            BndEsInternet = False
            CREAARBOLDIGITAL2()
            Panel7.Visible = True
            frmctr.Hide()
            frmInternet2.Hide()

            If IsNumeric(ComboBox14.SelectedValue) = True Then
                If ComboBox14.SelectedValue > 0 Then
                    MuestraServicios_digital(eClv_TipSer, ComboBox14.SelectedValue)
                End If
            End If
        ElseIf Me.Button7.Text = "Tv Digital &Cable" Then
            eClv_TipSer = 6
            BndEsInternet = False
            CREARARBOLDIGITALCABLE()
            Panel7.Visible = True
            frmctr.Hide()
            frmInternet2.Hide()

            If IsNumeric(ComboBox14.SelectedValue) = True Then
                If ComboBox14.SelectedValue > 0 Then
                    MuestraServicios_digital(eClv_TipSer, ComboBox14.SelectedValue)
                End If
            End If
        ElseIf Me.Button7.Text = "&Telefonia" Then
            muestra_telefonia()
        End If

        eEntraUM = True
        eEntraUMB = False

        'If Me.Button7.Text = "&Internet" Then

        '    BndEsInternet = True
        '    frmctr.MdiParent = Me
        '    frmInternet2.MdiParent = Me
        '    frmctr.WindowState = FormWindowState.Normal
        '    frmInternet2.Show()
        '    frmctr.Show()
        '    frmctr.Boton_Internet()
        '    frmctr.TreeView1.ExpandAll()

        '    Me.Panel2.Hide()
        '    Me.Panel4.Hide()
        '    Me.Panel7.Hide()
        '    '--Controlar el Efecto de los botones de Television y Internet
        '    If Me.Button28.Enabled = False And Me.Button28.Visible = True And IdSistema = "LO" Then
        '        Me.Button28.Enabled = True
        '        Button8.Enabled = False
        '    End If
        '    If Me.Button28.Visible = True And Me.Button8.Visible = True Then
        '        Button8.Enabled = True
        '    End If

        '    Me.Button8.Enabled = True
        '    Me.Button7.Enabled = False
        '    Me.Button28.Enabled = True

        'ElseIf Me.Button7.Text = "&Tv Digital" Or Me.Button7.Text = "&Premium" Then
        '    BndEsInternet = False
        '    Me.Panel2.Visible = False
        '    Me.Panel4.Visible = False
        '    Me.Panel7.Visible = True
        '    frmctr.Hide()
        '    'No lo Usas frmTelefonia.Hide()
        '    frmInternet2.Hide()

        '    If IsNumeric(ComboBox14.SelectedValue) = True Then
        '        If ComboBox14.SelectedValue > 0 Then
        '            Dim CON As New SqlConnection(MiConexion)
        '            CON.Open()
        '            Me.MuestraServicios_digitalTableAdapter.Connection = CON
        '            Me.MuestraServicios_digitalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraServicios_digital, 3, Me.ComboBox14.SelectedValue)
        '            CON.Close()
        '            BuscaDescDig()
        '        End If
        '    End If
        'End If
        ''Eric----------
        'BuscaDescTV()
        ''--------------
        ''Estas dos Líneas las Puso Eric. Actualizan Las fechas.
        'eEntraUM = True
        'eEntraUMB = False
    End Sub
#Region "Combo Tipo Instalaciones"
    Dim tipoInstalacion As classTipoInstalacion

    Private Sub llenaComboTipoInstalacion(ByVal prmOpReporte As Integer)
        tipoInstalacion = New classTipoInstalacion
        tipoInstalacion.opActivo = prmOpReporte
        Me.cmbTipoInstalacion.DataSource = tipoInstalacion.uspConsultaTipoInstalacionCombo
    End Sub

    Private Sub guardaRelacionTipoInstalacion(ByVal prmContrato As Long, ByVal prmIdTipoInstalacion As Integer)
        tipoInstalacion = New classTipoInstalacion
        tipoInstalacion.contrato = prmContrato
        tipoInstalacion.idTipoInstalacion = prmIdTipoInstalacion
        tipoInstalacion.uspInsertaTblRelClientesTipoInstalacion()
    End Sub

    Private Sub consultaRelacionTipoInstalacion(ByVal prmContrato As Long)
        tipoInstalacion = New classTipoInstalacion
        tipoInstalacion.contrato = prmContrato
        Me.cmbTipoInstalacion.SelectedValue = tipoInstalacion.uspConsultaTblRelClientesTipoInstalacion()
    End Sub
#End Region

    Private Sub VALIDAContratacionPaquete(ByVal CONTRATO As Integer, ByVal CONTRATONET As Integer, ByVal CLV_UNICANET As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CONTRATO", SqlDbType.Int, CONTRATO)
        BaseII.CreateMyParameter("@CONTRATONET", SqlDbType.Int, CONTRATONET)
        BaseII.CreateMyParameter("@CLV_UNICANET", SqlDbType.Int, CLV_UNICANET)
        BaseII.CreateMyParameter("@MSJ", ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.ProcedimientoOutPut("VALIDAContratacionPaquete")
        eMsj = ""
        eMsj = BaseII.dicoPar("@MSJ").ToString
    End Sub


End Class