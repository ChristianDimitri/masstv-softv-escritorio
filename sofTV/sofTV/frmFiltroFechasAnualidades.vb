﻿Public Class frmFiltroFechasAnualidades
#Region "VARIABLES"
    Public locFiltroClvSessionAnualidad As Long
#End Region

#Region "CONTROLES"
    Private Sub frmFiltroFechasAnualidades_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        'generaClientesAnualidad(locFiltroClvSessionAnualidad)
        llenaComboMeses(locFiltroClvSessionAnualidad)
    End Sub

    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        If IsNumeric(Me.cmbFechasAnualidad.SelectedValue) = True Then
            If Me.cmbFechasAnualidad.SelectedValue > 0 Then
                Me.DialogResult = Windows.Forms.DialogResult.OK
                Me.Close()
            Else
                MsgBox("¡Seleccione un mes válido!", MsgBoxStyle.Information)
            End If
        Else
            MsgBox("¡Seleccione al menos un mes para consultar!", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub
#End Region

#Region "MÉTODOS"

    'Private Sub generaClientesAnualidad(ByVal prmClvSession As Long)
    '    Dim reporteAnual As New classReportes

    '    reporteAnual.clvSession = prmClvSession
    '    reporteAnual.uspGeneraClientesConAnualidad()
    'End Sub

    Private Sub llenaComboMeses(ByVal prmClvSession As Long)
        Dim reporteAnual As New classReportes

        reporteAnual.clvSession = prmClvSession
        Me.cmbFechasAnualidad.DataSource = reporteAnual.uspChecaFechasAnualidad
    End Sub
#End Region
End Class