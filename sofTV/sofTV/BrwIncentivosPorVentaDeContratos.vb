﻿Imports sofTV.BAL
Imports System.Collections.Generic
Public Class BrwIncentivosPorVentaDeContratos
    Public IdIncentivoPorContratos As Integer
    Private Sub BrwIncentivosPorVentaDeContratos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        recargaGrid()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        eOpcion = "N"
        Dim frm As FrmIncentivoPorContratos = New FrmIncentivoPorContratos()
        frm.ShowDialog()
        recargaGrid()
    End Sub

    Private Sub dgvDatos_CurrentCellChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvDatos.CurrentCellChanged
        Try

            IdIncentivoPorContratos = Integer.Parse(Me.dgvDatos.CurrentRow.Cells(0).Value)

        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        eOpcion = "M"
        Dim frm As FrmIncentivoPorContratos = New FrmIncentivoPorContratos()
        frm.IdIncentivoPorContratos = IdIncentivoPorContratos
        frm.ShowDialog()
        recargaGrid()
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If (dgvDatos.SelectedRows.Count > 0) Then
            If (MessageBox.Show("Desea Eliminar El Registro con la Clave " + IdIncentivoPorContratos.ToString() + " ", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes) Then

                ProductividadParaTecnico.Delete(IdIncentivoPorContratos)
                recargaGrid()
            End If
        Else
            MessageBox.Show("No Hay mas Valores A Eliminar")
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Public Sub recargaGrid()
        dgvDatos.DataSource = Servicio.GetServicioBySale_en_Cartera(True)
        OcultarColumnas()
    End Sub

    Public Sub OcultarColumnas()
        Dim columnas As New List(Of Integer)
        columnas.AddRange({2, 10})
        For i As Integer = 0 To dgvDatos.Columns.Count - 1
            If (Not columnas.Contains(i)) Then
                dgvDatos.Columns(i).Visible = False
            Else
                dgvDatos.Columns(i).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

            End If
        Next
    End Sub
End Class