﻿Imports System.Data.SqlClient
Imports System.Text
Imports Microsoft.VisualBasic
Imports System.IO

Public Class FrmSelCajasDisponiblesvb

    Dim TieneMarcaAsignada As Boolean = False
    Dim TieneDetalleAparato As Boolean = False

    Private Sub FrmSelCajasDisponiblesvb_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If BNDCAJAS = True Then
            BNDCAJAS = False
            'MuestraContNetCajasDig(ContratoCajasDig)
            MuestraCajasDisponibles(Me.ContNetCombo.SelectedValue)

            If OpICAJA = "E" Then
                Me.AceptarButton.Enabled = False
                Me.ContNetCombo.Enabled = False
                Me.CajasComboBox.Enabled = False
                ' Me.CajasBindingNavigator.Enabled = False
            End If

            If OpICAJA = "P" Then
                Me.AceptarButton.Enabled = True
                Me.ContNetCombo.Enabled = True
                Me.CajasComboBox.Enabled = True
                'Me.CajasBindingNavigator.Enabled = True
                ConClientesCajasDig()
            End If
        End If
    End Sub

    Private Sub FrmSelCajasDisponiblesvb_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'colorea(Me, Me.Name)
        Me.CMBLabel3.Visible = False
        'Se Comento por que no se Encontro el Procedimiento por Edgar 20/06/2011
        'ChecaSiTieneDetalleAparato(GloDetClave)
        If TieneDetalleAparato = False Then
            MuestraContNetCajasDig(ContratoCajasDig)
            If IsNumeric(Me.ContNetCombo.SelectedValue) = False Then
                MsgBox("No hay tarjetas para asignar", MsgBoxStyle.Information)
                Me.CMBLabel3.Visible = False
                GloBndClv_CablemodemSel = True
                'Me.Close()
                Me.AceptarButton.Enabled = False
                Exit Sub
            Else
                Me.AceptarButton.Enabled = True
            End If
        ElseIf TieneDetalleAparato = True Then
            Me.ContNetCombo.Enabled = False
            Me.CajasComboBox.Enabled = False
            Me.AceptarButton.Enabled = False
            ConRelDetOrdenesCajas(GloDetClave)
        End If

        'ChecaSiTieneDetalleAparato(GloDetClave)
        If TieneDetalleAparato = False Then
            DimeSiTieneMarcaAsignada(Me.ContNetCombo.SelectedValue)
            If TieneMarcaAsignada = True Then
                MuestraCajasDisponibles(Me.ContNetCombo.SelectedValue)

                If OpICAJA = "E" Then
                    Me.AceptarButton.Enabled = False
                    Me.ContNetCombo.Enabled = False
                    Me.CajasComboBox.Enabled = False
                    ' Me.CajasBindingNavigator.Enabled = False
                End If

                If OpICAJA = "P" Then
                    Me.AceptarButton.Enabled = True
                    Me.ContNetCombo.Enabled = True
                    Me.CajasComboBox.Enabled = True
                    'Me.CajasBindingNavigator.Enabled = True
                    ConClientesCajasDig()
                End If
                'Else
                '    BNDCAJAS = True
                '    Dim RES = MsgBox("Primero debe seleccionar la marca de la Caja Digital", MsgBoxStyle.OkCancel)
                '    If RES = MsgBoxResult.Ok Then
                '        ContratoNetCajasDig = Me.ContNetCombo.SelectedValue
                '        FrmSelMarcaCajas.Show()
                '    Else
                '        BNDCAJAS = False
                '        Me.AceptarButton.Enabled = False
                '        Exit Sub
                '        Me.Close()
                '    End If
            End If
        End If
    End Sub



    Private Sub MuestraContNetCajasDig(ByVal ContratoCajas As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSql As New StringBuilder

        StrSql.Append("EXEC MuestraContNetCajasDig ")
        StrSql.Append(CStr(ContratoCajas))

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(StrSql.ToString(), CON)
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.ContNetCombo.DataSource = BS.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub NueClientesCajasDig(ByVal OpCaja As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("NueClientesCajasDig", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@ContratoNet", SqlDbType.Int)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = Me.ContNetCombo.SelectedValue
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@SERIE", SqlDbType.VarChar, 50)
        PRM2.Direction = ParameterDirection.Input
        PRM2.Value = Me.CajasComboBox.Text
        CMD.Parameters.Add(PRM2)

        Dim PRM3 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar, 10)
        PRM3.Direction = ParameterDirection.Input
        PRM3.Value = GloUsuario
        CMD.Parameters.Add(PRM3)

        Dim PRM4 As New SqlParameter("@OP", SqlDbType.Int)
        PRM4.Direction = ParameterDirection.Input
        PRM4.Value = OpCaja
        CMD.Parameters.Add(PRM4)

        Dim PRM5 As New SqlParameter("@CLV_ORDEN", SqlDbType.Int)
        PRM5.Direction = ParameterDirection.Input
        PRM5.Value = gloClv_Orden
        CMD.Parameters.Add(PRM5)
        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            MsgBox("Aparato almacenado satisfactoriamente", MsgBoxStyle.Information)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub EliClientesCajasDig(ByVal contnetdetord As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("EliClientesCajasDig", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@CONTRATONET", SqlDbType.Int)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = contnetdetord
        CMD.Parameters.Add(PRM1)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            'MsgBox("Registro eliminado Exitosamente", MsgBoxStyle.Information)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub
    Private Sub ConClientesCajasDig()
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("ConClientesCajasDig", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@CONTRATONET", SqlDbType.Int)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = Me.ContNetCombo.SelectedValue
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@CLV_APARATO", SqlDbType.Int)
        PRM2.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM2)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            If PRM2.Value > 0 Then
                'Me.CajasComboBox.SelectedValue = PRM2.Value
                Me.CMBLabel3.Visible = True
                CajasToolStrip.Enabled = True
            Else
                Me.CMBLabel3.Visible = False
                CajasToolStrip.Enabled = False
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub
    Private Sub MuestraCajasDisponibles(ByVal contnetdetord As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSql As New StringBuilder

        StrSql.Append("EXEC MuestraCajasDisponibles ")
        StrSql.Append(CStr(Locclv_tec) & ",")
        StrSql.Append(CStr(contnetdetord))

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(StrSql.ToString(), CON)
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT

            Me.CajasComboBox.ValueMember = "Clv_Cablemodem"
            Me.CajasComboBox.DisplayMember = "MACCABLEMODEM"

            Me.CajasComboBox.DataSource = BS.DataSource

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub
    Private Sub AceptarButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AceptarButton.Click
        If IsNumeric(Me.ContNetCombo.SelectedValue) = False Then
            MsgBox("No se ha seleccionado un servicio al cual se le asignará una Caja ", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.CajasComboBox.SelectedValue) = True Then
            GloClv_CablemodemSel = Me.CajasComboBox.SelectedValue
            GloMacCablemodemSel = Me.CajasComboBox.Text
            GloBndClv_CablemodemSel = True
        Else
            GloClv_CablemodemSel = 0
            GloMacCablemodemSel = ""
            GloBndClv_CablemodemSel = False
            MsgBox("No se Seleccionado una Caja", MsgBoxStyle.Information)
            Exit Sub
        End If
        NueClientesCajasDig(1)
        NueRelDetOrdenesCajas(GloDetClave, gloClv_Orden, Me.ContNetCombo.SelectedValue, Me.CajasComboBox.SelectedValue, Me.CajasComboBox.Text)
        GloBloqueaDetalle = True
        BndEliminar = True
        Me.Close()
    End Sub

    Private Sub DimeSiTieneMarcaAsignada(ByVal CONTNETCAJAS As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("DimeSiTieneMarcaAsignada", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@CONTRATONET", SqlDbType.Int)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = CONTNETCAJAS
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@BNDMARCA", SqlDbType.Bit)
        PRM2.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM2)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            TieneMarcaAsignada = PRM2.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub NueRelDetOrdenesCajas(ByVal ClvDetalleOrden As Long, ByVal ClvOrdenCajas As Long, ByVal ContratoNetDetOrden As Long _
                                      , ByVal ClvAparatoDerOrden As Long, ByVal SerieDerOrden As String)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("NueRelDetOrdenesCajas", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@ClvDetalleOrden", SqlDbType.BigInt)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = ClvDetalleOrden
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@ClvOrden", SqlDbType.BigInt)
        PRM2.Direction = ParameterDirection.Input
        PRM2.Value = ClvOrdenCajas
        CMD.Parameters.Add(PRM2)

        Dim PRM3 As New SqlParameter("@ContratoNet", SqlDbType.BigInt)
        PRM3.Direction = ParameterDirection.Input
        PRM3.Value = ContratoNetDetOrden
        CMD.Parameters.Add(PRM3)

        Dim PRM4 As New SqlParameter("@Clv_Aparato", SqlDbType.BigInt)
        PRM4.Direction = ParameterDirection.Input
        PRM4.Value = ClvAparatoDerOrden
        CMD.Parameters.Add(PRM4)

        Dim PRM5 As New SqlParameter("@SERIE", SqlDbType.VarChar, 250)
        PRM5.Direction = ParameterDirection.Input
        PRM5.Value = SerieDerOrden
        CMD.Parameters.Add(PRM5)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub EliRelDetOrdenesCajas(ByVal ClvDetalleOrden As Long)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("EliRelDetOrdenesCajas", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@ClvDetalleOrden", SqlDbType.BigInt)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = ClvDetalleOrden
        CMD.Parameters.Add(PRM1)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub ChecaSiTieneDetalleAparato(ByVal ClvDetalleOrden As Long)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("ChecaSiTieneDetalleAparato", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@ClvDetalleOrden", SqlDbType.BigInt)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = ClvDetalleOrden
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@BndAparato", SqlDbType.Bit)
        PRM2.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM2)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            TieneDetalleAparato = PRM2.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub ConRelDetOrdenesCajas(ByVal ClvDetalleOrden As Long)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("ConRelDetOrdenesCajas", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@ClvDetalleOrden", SqlDbType.BigInt)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = ClvDetalleOrden
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@ClvAparato", SqlDbType.BigInt)
        PRM2.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM2)

        Dim PRM3 As New SqlParameter("@SerieAparato", SqlDbType.VarChar, 250)
        PRM3.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM3)

        Dim PRM4 As New SqlParameter("@ContratoNet", SqlDbType.BigInt)
        PRM4.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM4)

        Dim PRM5 As New SqlParameter("@Tarjeta", SqlDbType.VarChar, 250)
        PRM5.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM5)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()

            Me.ContNetCombo.Text = PRM5.Value
            Me.ContNetCombo.SelectedValue = PRM4.Value
            ContratoNetDetCajas = PRM4.Value

            Me.CajasComboBox.Text = PRM3.Value
            Me.CajasComboBox.SelectedValue = PRM2.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub EliMarcasTemporales(ByVal ContratoNetMarca As Long)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("EliMarcasTemporales", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@ContratoNet", SqlDbType.BigInt)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = ContratoNetMarca
        CMD.Parameters.Add(PRM1)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub
    Private Sub CerrarButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CerrarButton.Click
        GloBndClv_CablemodemSel = False
        GloBloqueaDetalle = True
        'Fue Comentado  por Edgar Ya que los Procedimientos no se encontraron por Ningun Lado 20/06/2011
        'If TieneDetalleAparato Then 
        '    EliMarcasTemporales(Me.ContNetCombo.SelectedValue)
        'End If
        Me.Close()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim resp = MsgBox("¿Realmente deseas Eliminar la caja asignada anteriormente?", MsgBoxStyle.OkCancel)
        If resp = MsgBoxResult.Ok Then
            EliClientesCajasDig(Me.ContNetCombo.SelectedValue)
            MuestraCajasDisponibles(Me.ContNetCombo.SelectedValue)
        Else
            Exit Sub
            Me.Close()
        End If
    End Sub

    Private Sub ContNetCombo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContNetCombo.SelectedIndexChanged
        If TieneDetalleAparato = False Then
            If OpICAJA = "P" Then
                DimeSiTieneMarcaAsignada(Me.ContNetCombo.SelectedValue)
                Me.AceptarButton.Enabled = True
                If TieneMarcaAsignada = True Then
                    ConClientesCajasDig()
                    MuestraCajasDisponibles(Me.ContNetCombo.SelectedValue)
                Else
                    'BNDCAJAS = True
                    Dim RES = MsgBox("Primero debe seleccionar la marca de la Caja Digital", MsgBoxStyle.OkOnly)
                    If RES = MsgBoxResult.Ok Then
                        ContratoNetCajasDig = Me.ContNetCombo.SelectedValue
                        FrmSelMarcaCajas.Show()
                    ElseIf RES = MsgBoxResult.Cancel Then
                        BNDCAJAS = False
                        Me.CMBLabel3.Visible = False
                        Me.AceptarButton.Enabled = False
                        Me.CajasComboBox.DataSource = Nothing
                        Me.CajasComboBox.Text = ""
                        CajasToolStrip.Enabled = False
                        Exit Sub
                        Me.Close()
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub DeleteToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteToolStripButton.Click
        Dim resp = MsgBox("¿Realmente deseas Eliminar la caja asignada anteriormente?", MsgBoxStyle.OkCancel)
        If resp = MsgBoxResult.Ok Then
            TieneDetalleAparato = False
            If TieneDetalleAparato = False Then
                EliClientesCajasDig(Me.ContNetCombo.SelectedValue)
                MuestraContNetCajasDig(ContratoCajasDig)
                MuestraCajasDisponibles(Me.ContNetCombo.SelectedValue)
            Else
                EliClientesCajasDig(ContratoNetDetCajas)
                MuestraContNetCajasDig(ContratoCajasDig)
                MuestraCajasDisponibles(ContratoNetDetCajas)
                EliRelDetOrdenesCajas(GloDetClave)
            End If

            Me.ContNetCombo.Enabled = True
            Me.CajasComboBox.Enabled = True
            Me.AceptarButton.Enabled = True
            Me.CajasToolStrip.Enabled = False

            MuestraContNetCajasDig(ContratoCajasDig)

            DimeSiTieneMarcaAsignada(Me.ContNetCombo.SelectedValue)
            If TieneMarcaAsignada = False Then
                Dim RES = MsgBox("Primero debe seleccionar la marca de la Caja Digital", MsgBoxStyle.OkOnly)
                If RES = MsgBoxResult.Ok Then
                    ContratoNetCajasDig = Me.ContNetCombo.SelectedValue
                    FrmSelMarcaCajas.Show()
                ElseIf RES = MsgBoxResult.Cancel Then
                    BNDCAJAS = False
                    Me.CMBLabel3.Visible = False
                    Me.AceptarButton.Enabled = False
                    Me.CajasComboBox.DataSource = Nothing
                    Me.CajasComboBox.Text = ""
                    CajasToolStrip.Enabled = False
                    Exit Sub
                    Me.Close()
                End If

            End If
        Else
            Exit Sub
            Me.Close()
        End If
    End Sub
End Class