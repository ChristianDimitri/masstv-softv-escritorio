﻿Public Class frmFiltroTipoInstalacion
#Region "Controles"
    Private Sub frmFiltroTipoInstalacion_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        llenaCombo(1) 'LLENAMOS EL COMBO
    End Sub

    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        If IsNumeric(Me.cmbTipoInstalacion.SelectedValue) = True Then
            Me.DialogResult = Windows.Forms.DialogResult.OK
        Else
            MsgBox("Seleccione al menos un concepto", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub dtpFechaInicial_ValueChanged(sender As System.Object, e As System.EventArgs) Handles dtpFechaInicial.ValueChanged
        Me.dtpFechaFinal.MinDate = Me.dtpFechaInicial.Value
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub
#End Region

#Region "Métodos"
    Dim tipoInstalacion As classTipoInstalacion


    Private Sub llenaCombo(ByVal prmOpReporte As Integer)
        tipoInstalacion = New classTipoInstalacion
        tipoInstalacion.opActivo = prmOpReporte
        Me.cmbTipoInstalacion.DataSource = tipoInstalacion.uspConsultaTipoInstalacionCombo
    End Sub
#End Region
End Class