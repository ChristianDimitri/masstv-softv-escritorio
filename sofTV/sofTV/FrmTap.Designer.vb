<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTap
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ClaveLabel As System.Windows.Forms.Label
        Dim IngenieriaLabel As System.Windows.Forms.Label
        Dim NoCasasLabel As System.Windows.Forms.Label
        Dim NoNegociosLabel As System.Windows.Forms.Label
        Dim NoLotesLabel As System.Windows.Forms.Label
        Dim NoServiciosLabel As System.Windows.Forms.Label
        Dim FrenteANumeroLabel As System.Windows.Forms.Label
        Dim Clv_TxtLabel As System.Windows.Forms.Label
        Dim NombreLabel1 As System.Windows.Forms.Label
        Dim NOMBRELabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmTap))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.PosteComboBox = New System.Windows.Forms.ComboBox()
        Me.Clv_TxtComboBox = New System.Windows.Forms.ComboBox()
        Me.ConSectorBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.Clv_SectorTextBox1 = New System.Windows.Forms.TextBox()
        Me.ClaveTextBox = New System.Windows.Forms.TextBox()
        Me.ConTapBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.IngenieriaTextBox = New System.Windows.Forms.TextBox()
        Me.SalidasTextBox = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.NOMBREComboBox = New System.Windows.Forms.ComboBox()
        Me.MuestraCalleSecBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Clv_CalleTextBox1 = New System.Windows.Forms.TextBox()
        Me.NombreComboBox1 = New System.Windows.Forms.ComboBox()
        Me.MuestraColoniaSecBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Clv_ColoniaTextBox1 = New System.Windows.Forms.TextBox()
        Me.NoNegociosTextBox = New System.Windows.Forms.TextBox()
        Me.FrenteANumeroTextBox = New System.Windows.Forms.TextBox()
        Me.NoServiciosTextBox = New System.Windows.Forms.TextBox()
        Me.NoLotesTextBox = New System.Windows.Forms.TextBox()
        Me.NoCasasTextBox = New System.Windows.Forms.TextBox()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.Clv_CalleTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_ColoniaTextBox = New System.Windows.Forms.TextBox()
        Me.ConsecutivoTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_SectorTextBox = New System.Windows.Forms.TextBox()
        Me.ConTapTableAdapter = New sofTV.DataSetEricTableAdapters.ConTapTableAdapter()
        Me.NueTapBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NueTapTableAdapter = New sofTV.DataSetEricTableAdapters.NueTapTableAdapter()
        Me.ModTapBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ModTapTableAdapter = New sofTV.DataSetEricTableAdapters.ModTapTableAdapter()
        Me.ConSectorTableAdapter = New sofTV.DataSetEricTableAdapters.ConSectorTableAdapter()
        Me.MuestraColoniaSecTableAdapter = New sofTV.DataSetEricTableAdapters.MuestraColoniaSecTableAdapter()
        Me.ConsecutivoAux = New System.Windows.Forms.TextBox()
        Me.MuestraCalleSecTableAdapter = New sofTV.DataSetEricTableAdapters.MuestraCalleSecTableAdapter()
        ClaveLabel = New System.Windows.Forms.Label()
        IngenieriaLabel = New System.Windows.Forms.Label()
        NoCasasLabel = New System.Windows.Forms.Label()
        NoNegociosLabel = New System.Windows.Forms.Label()
        NoLotesLabel = New System.Windows.Forms.Label()
        NoServiciosLabel = New System.Windows.Forms.Label()
        FrenteANumeroLabel = New System.Windows.Forms.Label()
        Clv_TxtLabel = New System.Windows.Forms.Label()
        NombreLabel1 = New System.Windows.Forms.Label()
        NOMBRELabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        CType(Me.ConSectorBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConTapBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.MuestraCalleSecBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraColoniaSecBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        CType(Me.NueTapBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ModTapBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ClaveLabel
        '
        ClaveLabel.AutoSize = True
        ClaveLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ClaveLabel.Location = New System.Drawing.Point(136, 29)
        ClaveLabel.Name = "ClaveLabel"
        ClaveLabel.Size = New System.Drawing.Size(56, 16)
        ClaveLabel.TabIndex = 2
        ClaveLabel.Text = "Clave :"
        '
        'IngenieriaLabel
        '
        IngenieriaLabel.AutoSize = True
        IngenieriaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        IngenieriaLabel.Location = New System.Drawing.Point(387, 99)
        IngenieriaLabel.Name = "IngenieriaLabel"
        IngenieriaLabel.Size = New System.Drawing.Size(102, 16)
        IngenieriaLabel.TabIndex = 8
        IngenieriaLabel.Text = "Tap / Salidas"
        '
        'NoCasasLabel
        '
        NoCasasLabel.AutoSize = True
        NoCasasLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NoCasasLabel.Location = New System.Drawing.Point(142, 196)
        NoCasasLabel.Name = "NoCasasLabel"
        NoCasasLabel.Size = New System.Drawing.Size(94, 16)
        NoCasasLabel.TabIndex = 16
        NoCasasLabel.Text = "# de Casas :"
        '
        'NoNegociosLabel
        '
        NoNegociosLabel.AutoSize = True
        NoNegociosLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NoNegociosLabel.Location = New System.Drawing.Point(119, 222)
        NoNegociosLabel.Name = "NoNegociosLabel"
        NoNegociosLabel.Size = New System.Drawing.Size(117, 16)
        NoNegociosLabel.TabIndex = 18
        NoNegociosLabel.Text = "# de Negocios :"
        '
        'NoLotesLabel
        '
        NoLotesLabel.AutoSize = True
        NoLotesLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NoLotesLabel.Location = New System.Drawing.Point(148, 248)
        NoLotesLabel.Name = "NoLotesLabel"
        NoLotesLabel.Size = New System.Drawing.Size(88, 16)
        NoLotesLabel.TabIndex = 20
        NoLotesLabel.Text = "# de Lotes :"
        '
        'NoServiciosLabel
        '
        NoServiciosLabel.AutoSize = True
        NoServiciosLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NoServiciosLabel.Location = New System.Drawing.Point(121, 274)
        NoServiciosLabel.Name = "NoServiciosLabel"
        NoServiciosLabel.Size = New System.Drawing.Size(115, 16)
        NoServiciosLabel.TabIndex = 22
        NoServiciosLabel.Text = "# de Servicios :"
        '
        'FrenteANumeroLabel
        '
        FrenteANumeroLabel.AutoSize = True
        FrenteANumeroLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FrenteANumeroLabel.Location = New System.Drawing.Point(101, 300)
        FrenteANumeroLabel.Name = "FrenteANumeroLabel"
        FrenteANumeroLabel.Size = New System.Drawing.Size(135, 16)
        FrenteANumeroLabel.TabIndex = 24
        FrenteANumeroLabel.Text = "Frente al Número :"
        '
        'Clv_TxtLabel
        '
        Clv_TxtLabel.AutoSize = True
        Clv_TxtLabel.Location = New System.Drawing.Point(58, 95)
        Clv_TxtLabel.Name = "Clv_TxtLabel"
        Clv_TxtLabel.Size = New System.Drawing.Size(61, 16)
        Clv_TxtLabel.TabIndex = 11
        Clv_TxtLabel.Text = "Sector :"
        '
        'NombreLabel1
        '
        NombreLabel1.AutoSize = True
        NombreLabel1.Location = New System.Drawing.Point(95, 34)
        NombreLabel1.Name = "NombreLabel1"
        NombreLabel1.Size = New System.Drawing.Size(87, 16)
        NombreLabel1.TabIndex = 26
        NombreLabel1.Text = "Colonia(s) :"
        '
        'NOMBRELabel
        '
        NOMBRELabel.AutoSize = True
        NOMBRELabel.Location = New System.Drawing.Point(95, 94)
        NOMBRELabel.Name = "NOMBRELabel"
        NOMBRELabel.Size = New System.Drawing.Size(70, 16)
        NOMBRELabel.TabIndex = 28
        NOMBRELabel.Text = "Calle(s) :"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Location = New System.Drawing.Point(236, 95)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(56, 16)
        Label1.TabIndex = 15
        Label1.Text = "Poste :"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Label1)
        Me.GroupBox1.Controls.Add(Me.PosteComboBox)
        Me.GroupBox1.Controls.Add(Me.Clv_TxtComboBox)
        Me.GroupBox1.Controls.Add(Me.Clv_SectorTextBox1)
        Me.GroupBox1.Controls.Add(Clv_TxtLabel)
        Me.GroupBox1.Controls.Add(Me.ClaveTextBox)
        Me.GroupBox1.Controls.Add(ClaveLabel)
        Me.GroupBox1.Controls.Add(IngenieriaLabel)
        Me.GroupBox1.Controls.Add(Me.IngenieriaTextBox)
        Me.GroupBox1.Controls.Add(Me.SalidasTextBox)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(53, 46)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(533, 168)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Clave del Tap :"
        '
        'PosteComboBox
        '
        Me.PosteComboBox.DisplayMember = "Descripcion"
        Me.PosteComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.PosteComboBox.FormattingEnabled = True
        Me.PosteComboBox.Location = New System.Drawing.Point(226, 116)
        Me.PosteComboBox.Name = "PosteComboBox"
        Me.PosteComboBox.Size = New System.Drawing.Size(134, 24)
        Me.PosteComboBox.TabIndex = 14
        Me.PosteComboBox.ValueMember = "clave"
        '
        'Clv_TxtComboBox
        '
        Me.Clv_TxtComboBox.DataSource = Me.ConSectorBindingSource
        Me.Clv_TxtComboBox.DisplayMember = "Clv_Txt"
        Me.Clv_TxtComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Clv_TxtComboBox.FormattingEnabled = True
        Me.Clv_TxtComboBox.Location = New System.Drawing.Point(61, 116)
        Me.Clv_TxtComboBox.Name = "Clv_TxtComboBox"
        Me.Clv_TxtComboBox.Size = New System.Drawing.Size(121, 24)
        Me.Clv_TxtComboBox.TabIndex = 0
        Me.Clv_TxtComboBox.ValueMember = "Clv_Sector"
        '
        'ConSectorBindingSource
        '
        Me.ConSectorBindingSource.DataMember = "ConSector"
        Me.ConSectorBindingSource.DataSource = Me.DataSetEric
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Clv_SectorTextBox1
        '
        Me.Clv_SectorTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConSectorBindingSource, "Clv_Sector", True))
        Me.Clv_SectorTextBox1.Location = New System.Drawing.Point(128, 116)
        Me.Clv_SectorTextBox1.Name = "Clv_SectorTextBox1"
        Me.Clv_SectorTextBox1.ReadOnly = True
        Me.Clv_SectorTextBox1.Size = New System.Drawing.Size(10, 22)
        Me.Clv_SectorTextBox1.TabIndex = 13
        Me.Clv_SectorTextBox1.TabStop = False
        '
        'ClaveTextBox
        '
        Me.ClaveTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ClaveTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConTapBindingSource, "Clave", True))
        Me.ClaveTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClaveTextBox.Location = New System.Drawing.Point(198, 21)
        Me.ClaveTextBox.Name = "ClaveTextBox"
        Me.ClaveTextBox.ReadOnly = True
        Me.ClaveTextBox.Size = New System.Drawing.Size(192, 26)
        Me.ClaveTextBox.TabIndex = 3
        Me.ClaveTextBox.TabStop = False
        Me.ClaveTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'ConTapBindingSource
        '
        Me.ConTapBindingSource.DataMember = "ConTap"
        Me.ConTapBindingSource.DataSource = Me.DataSetEric
        '
        'IngenieriaTextBox
        '
        Me.IngenieriaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.IngenieriaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConTapBindingSource, "Ingenieria", True))
        Me.IngenieriaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IngenieriaTextBox.Location = New System.Drawing.Point(371, 118)
        Me.IngenieriaTextBox.Name = "IngenieriaTextBox"
        Me.IngenieriaTextBox.Size = New System.Drawing.Size(51, 22)
        Me.IngenieriaTextBox.TabIndex = 1
        '
        'SalidasTextBox
        '
        Me.SalidasTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SalidasTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConTapBindingSource, "Salidas", True))
        Me.SalidasTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SalidasTextBox.Location = New System.Drawing.Point(428, 118)
        Me.SalidasTextBox.Name = "SalidasTextBox"
        Me.SalidasTextBox.Size = New System.Drawing.Size(61, 22)
        Me.SalidasTextBox.TabIndex = 2
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(522, 653)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 10
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.NOMBREComboBox)
        Me.GroupBox2.Controls.Add(Me.Clv_CalleTextBox1)
        Me.GroupBox2.Controls.Add(NOMBRELabel)
        Me.GroupBox2.Controls.Add(Me.NombreComboBox1)
        Me.GroupBox2.Controls.Add(Me.Clv_ColoniaTextBox1)
        Me.GroupBox2.Controls.Add(NombreLabel1)
        Me.GroupBox2.Controls.Add(Me.NoNegociosTextBox)
        Me.GroupBox2.Controls.Add(Me.FrenteANumeroTextBox)
        Me.GroupBox2.Controls.Add(FrenteANumeroLabel)
        Me.GroupBox2.Controls.Add(Me.NoServiciosTextBox)
        Me.GroupBox2.Controls.Add(NoServiciosLabel)
        Me.GroupBox2.Controls.Add(Me.NoLotesTextBox)
        Me.GroupBox2.Controls.Add(NoLotesLabel)
        Me.GroupBox2.Controls.Add(NoNegociosLabel)
        Me.GroupBox2.Controls.Add(Me.NoCasasTextBox)
        Me.GroupBox2.Controls.Add(NoCasasLabel)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(53, 227)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(533, 391)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Datos del Tap :"
        '
        'NOMBREComboBox
        '
        Me.NOMBREComboBox.DisplayMember = "Nombre"
        Me.NOMBREComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.NOMBREComboBox.FormattingEnabled = True
        Me.NOMBREComboBox.Location = New System.Drawing.Point(98, 113)
        Me.NOMBREComboBox.Name = "NOMBREComboBox"
        Me.NOMBREComboBox.Size = New System.Drawing.Size(324, 24)
        Me.NOMBREComboBox.TabIndex = 4
        Me.NOMBREComboBox.ValueMember = "Clv_Calle"
        '
        'MuestraCalleSecBindingSource
        '
        Me.MuestraCalleSecBindingSource.DataMember = "MuestraCalleSec"
        Me.MuestraCalleSecBindingSource.DataSource = Me.DataSetEric
        '
        'Clv_CalleTextBox1
        '
        Me.Clv_CalleTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraCalleSecBindingSource, "Clv_Calle", True))
        Me.Clv_CalleTextBox1.Location = New System.Drawing.Point(389, 115)
        Me.Clv_CalleTextBox1.Name = "Clv_CalleTextBox1"
        Me.Clv_CalleTextBox1.ReadOnly = True
        Me.Clv_CalleTextBox1.Size = New System.Drawing.Size(10, 22)
        Me.Clv_CalleTextBox1.TabIndex = 30
        Me.Clv_CalleTextBox1.TabStop = False
        '
        'NombreComboBox1
        '
        Me.NombreComboBox1.DisplayMember = "Nombre"
        Me.NombreComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.NombreComboBox1.FormattingEnabled = True
        Me.NombreComboBox1.Location = New System.Drawing.Point(100, 53)
        Me.NombreComboBox1.Name = "NombreComboBox1"
        Me.NombreComboBox1.Size = New System.Drawing.Size(322, 24)
        Me.NombreComboBox1.TabIndex = 3
        Me.NombreComboBox1.ValueMember = "Clv_Colonia"
        '
        'MuestraColoniaSecBindingSource
        '
        Me.MuestraColoniaSecBindingSource.DataMember = "MuestraColoniaSec"
        Me.MuestraColoniaSecBindingSource.DataSource = Me.DataSetEric
        '
        'Clv_ColoniaTextBox1
        '
        Me.Clv_ColoniaTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraColoniaSecBindingSource, "Clv_Colonia", True))
        Me.Clv_ColoniaTextBox1.Location = New System.Drawing.Point(330, 53)
        Me.Clv_ColoniaTextBox1.Name = "Clv_ColoniaTextBox1"
        Me.Clv_ColoniaTextBox1.ReadOnly = True
        Me.Clv_ColoniaTextBox1.Size = New System.Drawing.Size(10, 22)
        Me.Clv_ColoniaTextBox1.TabIndex = 28
        Me.Clv_ColoniaTextBox1.TabStop = False
        '
        'NoNegociosTextBox
        '
        Me.NoNegociosTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NoNegociosTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConTapBindingSource, "NoNegocios", True))
        Me.NoNegociosTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NoNegociosTextBox.Location = New System.Drawing.Point(252, 216)
        Me.NoNegociosTextBox.Name = "NoNegociosTextBox"
        Me.NoNegociosTextBox.Size = New System.Drawing.Size(100, 22)
        Me.NoNegociosTextBox.TabIndex = 6
        '
        'FrenteANumeroTextBox
        '
        Me.FrenteANumeroTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FrenteANumeroTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.FrenteANumeroTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConTapBindingSource, "FrenteANumero", True))
        Me.FrenteANumeroTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FrenteANumeroTextBox.Location = New System.Drawing.Point(252, 294)
        Me.FrenteANumeroTextBox.Name = "FrenteANumeroTextBox"
        Me.FrenteANumeroTextBox.Size = New System.Drawing.Size(100, 22)
        Me.FrenteANumeroTextBox.TabIndex = 9
        '
        'NoServiciosTextBox
        '
        Me.NoServiciosTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NoServiciosTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConTapBindingSource, "NoServicios", True))
        Me.NoServiciosTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NoServiciosTextBox.Location = New System.Drawing.Point(252, 268)
        Me.NoServiciosTextBox.Name = "NoServiciosTextBox"
        Me.NoServiciosTextBox.Size = New System.Drawing.Size(100, 22)
        Me.NoServiciosTextBox.TabIndex = 8
        '
        'NoLotesTextBox
        '
        Me.NoLotesTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NoLotesTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConTapBindingSource, "NoLotes", True))
        Me.NoLotesTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NoLotesTextBox.Location = New System.Drawing.Point(252, 242)
        Me.NoLotesTextBox.Name = "NoLotesTextBox"
        Me.NoLotesTextBox.Size = New System.Drawing.Size(100, 22)
        Me.NoLotesTextBox.TabIndex = 7
        '
        'NoCasasTextBox
        '
        Me.NoCasasTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NoCasasTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConTapBindingSource, "NoCasas", True))
        Me.NoCasasTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NoCasasTextBox.Location = New System.Drawing.Point(252, 190)
        Me.NoCasasTextBox.Name = "NoCasasTextBox"
        Me.NoCasasTextBox.Size = New System.Drawing.Size(100, 22)
        Me.NoCasasTextBox.TabIndex = 5
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.Image = CType(resources.GetObject("ToolStripButton2.Image"), System.Drawing.Image)
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(91, 22)
        Me.ToolStripButton2.Text = "&GUARDAR"
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Nothing
        Me.BindingNavigator1.CountItem = Nothing
        Me.BindingNavigator1.DeleteItem = Nothing
        Me.BindingNavigator1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton2})
        Me.BindingNavigator1.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigator1.MoveFirstItem = Nothing
        Me.BindingNavigator1.MoveLastItem = Nothing
        Me.BindingNavigator1.MoveNextItem = Nothing
        Me.BindingNavigator1.MovePreviousItem = Nothing
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Nothing
        Me.BindingNavigator1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.BindingNavigator1.Size = New System.Drawing.Size(683, 25)
        Me.BindingNavigator1.TabIndex = 1
        Me.BindingNavigator1.TabStop = True
        Me.BindingNavigator1.Text = "BindingNavigator1"
        '
        'Clv_CalleTextBox
        '
        Me.Clv_CalleTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConTapBindingSource, "Clv_Calle", True))
        Me.Clv_CalleTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_CalleTextBox.Location = New System.Drawing.Point(618, 662)
        Me.Clv_CalleTextBox.Name = "Clv_CalleTextBox"
        Me.Clv_CalleTextBox.ReadOnly = True
        Me.Clv_CalleTextBox.Size = New System.Drawing.Size(10, 22)
        Me.Clv_CalleTextBox.TabIndex = 15
        Me.Clv_CalleTextBox.TabStop = False
        '
        'Clv_ColoniaTextBox
        '
        Me.Clv_ColoniaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConTapBindingSource, "Clv_Colonia", True))
        Me.Clv_ColoniaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_ColoniaTextBox.Location = New System.Drawing.Point(602, 662)
        Me.Clv_ColoniaTextBox.Name = "Clv_ColoniaTextBox"
        Me.Clv_ColoniaTextBox.ReadOnly = True
        Me.Clv_ColoniaTextBox.Size = New System.Drawing.Size(10, 22)
        Me.Clv_ColoniaTextBox.TabIndex = 13
        Me.Clv_ColoniaTextBox.TabStop = False
        '
        'ConsecutivoTextBox
        '
        Me.ConsecutivoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConTapBindingSource, "Consecutivo", True))
        Me.ConsecutivoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConsecutivoTextBox.Location = New System.Drawing.Point(554, 662)
        Me.ConsecutivoTextBox.Name = "ConsecutivoTextBox"
        Me.ConsecutivoTextBox.ReadOnly = True
        Me.ConsecutivoTextBox.Size = New System.Drawing.Size(10, 22)
        Me.ConsecutivoTextBox.TabIndex = 7
        Me.ConsecutivoTextBox.TabStop = False
        '
        'Clv_SectorTextBox
        '
        Me.Clv_SectorTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConTapBindingSource, "Clv_Sector", True))
        Me.Clv_SectorTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_SectorTextBox.Location = New System.Drawing.Point(586, 662)
        Me.Clv_SectorTextBox.Name = "Clv_SectorTextBox"
        Me.Clv_SectorTextBox.ReadOnly = True
        Me.Clv_SectorTextBox.Size = New System.Drawing.Size(10, 22)
        Me.Clv_SectorTextBox.TabIndex = 5
        Me.Clv_SectorTextBox.TabStop = False
        '
        'ConTapTableAdapter
        '
        Me.ConTapTableAdapter.ClearBeforeFill = True
        '
        'NueTapBindingSource
        '
        Me.NueTapBindingSource.DataMember = "NueTap"
        Me.NueTapBindingSource.DataSource = Me.DataSetEric
        '
        'NueTapTableAdapter
        '
        Me.NueTapTableAdapter.ClearBeforeFill = True
        '
        'ModTapBindingSource
        '
        Me.ModTapBindingSource.DataMember = "ModTap"
        Me.ModTapBindingSource.DataSource = Me.DataSetEric
        '
        'ModTapTableAdapter
        '
        Me.ModTapTableAdapter.ClearBeforeFill = True
        '
        'ConSectorTableAdapter
        '
        Me.ConSectorTableAdapter.ClearBeforeFill = True
        '
        'MuestraColoniaSecTableAdapter
        '
        Me.MuestraColoniaSecTableAdapter.ClearBeforeFill = True
        '
        'ConsecutivoAux
        '
        Me.ConsecutivoAux.Location = New System.Drawing.Point(570, 662)
        Me.ConsecutivoAux.Name = "ConsecutivoAux"
        Me.ConsecutivoAux.ReadOnly = True
        Me.ConsecutivoAux.Size = New System.Drawing.Size(10, 20)
        Me.ConsecutivoAux.TabIndex = 35
        Me.ConsecutivoAux.TabStop = False
        '
        'MuestraCalleSecTableAdapter
        '
        Me.MuestraCalleSecTableAdapter.ClearBeforeFill = True
        '
        'FrmTap
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(683, 721)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ConsecutivoAux)
        Me.Controls.Add(Me.BindingNavigator1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Clv_SectorTextBox)
        Me.Controls.Add(Me.Clv_ColoniaTextBox)
        Me.Controls.Add(Me.Clv_CalleTextBox)
        Me.Controls.Add(Me.ConsecutivoTextBox)
        Me.Name = "FrmTap"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Taps"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.ConSectorBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConTapBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.MuestraCalleSecBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraColoniaSecBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        CType(Me.NueTapBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ModTapBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents ClaveTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ConTapBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents IngenieriaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SalidasTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NoNegociosTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FrenteANumeroTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NoServiciosTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NoLotesTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NoCasasTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents ConTapTableAdapter As sofTV.DataSetEricTableAdapters.ConTapTableAdapter
    Friend WithEvents Clv_CalleTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_ColoniaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ConsecutivoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_SectorTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NueTapBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NueTapTableAdapter As sofTV.DataSetEricTableAdapters.NueTapTableAdapter
    Friend WithEvents ModTapBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ModTapTableAdapter As sofTV.DataSetEricTableAdapters.ModTapTableAdapter
    Friend WithEvents ConSectorBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConSectorTableAdapter As sofTV.DataSetEricTableAdapters.ConSectorTableAdapter
    Friend WithEvents Clv_TxtComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents MuestraColoniaSecBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraColoniaSecTableAdapter As sofTV.DataSetEricTableAdapters.MuestraColoniaSecTableAdapter
    Friend WithEvents NombreComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents ConsecutivoAux As System.Windows.Forms.TextBox
    Friend WithEvents Clv_ColoniaTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Clv_SectorTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents MuestraCalleSecBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraCalleSecTableAdapter As sofTV.DataSetEricTableAdapters.MuestraCalleSecTableAdapter
    Friend WithEvents NOMBREComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Clv_CalleTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents PosteComboBox As System.Windows.Forms.ComboBox
End Class
