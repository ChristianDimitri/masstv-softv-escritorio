Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient

Public Class FrmCarteras
    Private customersByCityReport As ReportDocument
    Private op As String = Nothing
    Private Titulo As String = Nothing


    Private Sub ConfigureCrystalReportsCarteraXSD(ByVal op As String, ByVal Titulo As String)
        customersByCityReport = New ReportDocument
        Dim cnn As New SqlConnection(MiConexion)
        Try

            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0", Op6 As String = "0"
            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
            Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"
            Dim Status As String = Nothing

            Dim mySelectFormula As String = Titulo
            Dim ciudades1 As String = Nothing
            ciudades1 = " Ciudad(es): " + LocCiudades

            Dim reportPath As String = Nothing


            reportPath = RutaReportes + "\CarterasDigXSD.rpt"

            Dim cmd As New SqlCommand("CarterasDigXSD", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0

            Dim parametro1 As New SqlParameter("@Clv_Cartera", SqlDbType.BigInt)
            parametro1.Direction = ParameterDirection.Input
            parametro1.Value = GLOClv_Cartera
            cmd.Parameters.Add(parametro1)

            cnn.Open()
            cmd.ExecuteNonQuery()


            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet()
            da.Fill(ds)

            ds.Tables(0).TableName = "CatalogoConceptosCartera"
            ds.Tables(1).TableName = "DetCartera"
            ds.Tables(2).TableName = "SeleccionaCartera"
            ds.Tables(3).TableName = "Servicios"
            customersByCityReport.Load(reportPath)
            mySelectFormula = "Estado de Cartera de " & Me.ComboBox1.Text
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloSucursal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & ciudades1 & "'"

            customersByCityReport.SetDataSource(ds)
            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(100)
            CrystalReportViewer1.ShowExportButton = True
            CrystalReportViewer1.ShowPrintButton = True
            CrystalReportViewer1.ShowRefreshButton = True

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            cnn.Close()
            cnn.Dispose()
        End Try
    End Sub

    Private Sub ConfigureCrystalReports(ByVal op As String, ByVal Titulo As String)
        Try
            If GloClv_TipSer > 0 And GLOClv_Cartera > 0 Then
                customersByCityReport = New ReportDocument
                Dim connectionInfo As New ConnectionInfo
                
                connectionInfo.ServerName = GloServerName
                connectionInfo.DatabaseName = GloDatabaseName
                connectionInfo.UserID = GloUserID
                connectionInfo.Password = GloPassword

                Dim mySelectFormula As String = Titulo
                Dim ciudades1 As String = Nothing
                ciudades1 = " Ciudad(es): " + LocCiudades

                Dim reportPath As String = Nothing
                If Me.ComboPeriodo.SelectedValue = 0 Then
                    Select Case op
                        Case 4
                            reportPath = RutaReportes + "\ValorResumenCarterasrespaldo.rpt"
                        Case Else
                                    reportPath = RutaReportes + "\CarterasDig.rpt"
                    End Select
                Else
                    Select Case op
                        Case 4
                            reportPath = RutaReportes + "\ValorResumenCarterasrespaldo.rpt"
                        Case Else
                                    reportPath = RutaReportes + "\CarteraDig_PorPeriodo.rpt"
                    End Select
                End If
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                customersByCityReport.SetParameterValue(0, GLOClv_Cartera)

                customersByCityReport.SetParameterValue(1, GloClv_TipSer)
                Dim loc_tipser As Integer = 1
                If IsNumeric(Me.ComboBox1.SelectedValue) = True Then loc_tipser = Me.ComboBox1.SelectedValue
                If Me.ComboPeriodo.SelectedValue = 0 Then
                    Select Case op
                        Case 4
                                    mySelectFormula = "Valor de Cartera de " & Me.ComboBox1.Text
                        Case Else
                                    mySelectFormula = "Estado de Cartera de " & Me.ComboBox1.Text
                    End Select
                Else
                    Select Case op
                        Case 4
                                    mySelectFormula = "Valor de Cartera de " & Me.ComboBox1.Text
                        Case Else
                            mySelectFormula = "Estado de Cartera de " & Me.ComboBox1.Text & " del " & Me.ComboPeriodo.Text
                    End Select
                End If

                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
                customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloSucursal & "'"
                customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & ciudades1 & "'"

                CrystalReportViewer1.ReportSource = customersByCityReport
                SetDBLogonForReport2(connectionInfo)
                customersByCityReport = Nothing
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FrmCarteras_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim resp As MsgBoxResult = MsgBoxResult.Yes
        If execar = True Then
            execar = False

            If Me.ComboPeriodo.SelectedValue = 0 Then
                GENERAMICARTERA()
            Else
                GENERAMICARTERA_PorPeriodo()
            End If

        ElseIf execar = False Then
        End If

            If GloBndGenCartera = True Then
            GloBndGenCartera = False

            If op = 4 Then
                ConfigureCrystalReports(op, "")
            Else
                ConfigureCrystalReportsCarteraXSD(op, "")
            End If

                If GloBndGenCartera_Pregunta = True Then
                    GloBndGenCartera_Pregunta = False
                    resp = MsgBox("Deseas Guardar la Cartera ", MsgBoxStyle.YesNo)
                    If resp = MsgBoxResult.No Then
                        Me.BORRACARTERASTableAdapter.Connection = CON
                        Me.BORRACARTERASTableAdapter.Fill(Me.DataSetEDGAR.BORRACARTERAS, GLOClv_Cartera, GloClv_TipSer)
                    End If
                End If
            End If
            CON.Close()
    End Sub

    Private Sub FrmCarteras_CausesValidationChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.CausesValidationChanged

    End Sub

    Private Sub FrmCarteras_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GloClv_TipSer = 0
    End Sub


    Private Sub FrmCarteras_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        colorea(Me, Me.Name)
        Me.Label1.ForeColor = Color.Black
        Me.Rep_CarterasDataGridView.BackgroundColor = Color.Gray
        Label1.Visible = False

        Dim CON As New SqlConnection(MiConexion)
        CON.Open()

        Me.Text = "Estados De Cartera"
        If IsNumeric(ComboBox1.SelectedValue) = True Then
            GloClv_TipSer = Me.ComboBox1.SelectedValue
        Else
            GloClv_TipSer = glotiposervicioppal
        End If

        Try

            Me.PorPerido_SeleccioneTableAdapter.Connection = CON
            Me.PorPerido_SeleccioneTableAdapter.Fill(Me.DataSetEdgarRev2.PorPerido_Seleccione)
            Me.ComboPeriodo.SelectedIndex = 0
            Me.ComboPeriodo.Visible = False
            gloPorClv_Periodo = 0
            Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
            Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.DataSetEDGAR.MuestraTipSerPrincipal)
            If IsNumeric(ComboBox1.SelectedValue) = True Then
                Me.Rep_CarterasTableAdapter.Connection = CON
                Me.Rep_CarterasTableAdapter.Fill(Me.DataSetEDGAR.Rep_Carteras, Me.ComboBox1.SelectedValue, 0)
                ConfigureCrystalReportsCarteraXSD(op, "")
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
        If IdSistema = "AG" Or IdSistema = "VA" Then
            Me.Label1.Visible = False
            Me.ComboPeriodo.Visible = False
        End If
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            If IsNumeric(ComboBox1.SelectedValue) = True Then
                GloClv_TipSer = Me.ComboBox1.SelectedValue
                Me.Rep_CarterasTableAdapter.Connection = CON
                Me.Rep_CarterasTableAdapter.Fill(Me.DataSetEDGAR.Rep_Carteras, Me.ComboBox1.SelectedValue, 0)
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub SetDBLogonForReport2(ByVal myConnectionInfo As ConnectionInfo)
        Dim myTableLogOnInfos As TableLogOnInfos = Me.CrystalReportViewer1.LogOnInfo
        For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
            myTableLogOnInfo.ConnectionInfo = myConnectionInfo
        Next
    End Sub

    Private Sub Rep_CarterasDataGridView_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Rep_CarterasDataGridView.CellClick   
        If Me.Rep_CarterasDataGridView.SelectedCells(0).Value = 0 Then
            op = 0
            FrmSelCd_Cartera.Show()
        ElseIf Me.Rep_CarterasDataGridView.SelectedCells(0).Value = 1 Then
            op = 1
            FrmSelCuidadesCartera.Show()
        ElseIf Me.Rep_CarterasDataGridView.SelectedCells(0).Value = 4 Then
            op = 4
            FrmSelCuidadesCartera.Show()
        End If
    End Sub


    Private Sub GENERAMICARTERA()
        Dim comando As SqlClient.SqlCommand
        Dim resp As MsgBoxResult = MsgBoxResult.Yes
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            If GloClv_TipSer <> 3 Then
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CON
                    .CommandText = "Genera_Cartera "
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0

                    Dim prm As New SqlParameter("@Clv_Cartera", SqlDbType.BigInt)
                    Dim prm1 As New SqlParameter("@clv_session", SqlDbType.BigInt)
                    Dim prm2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
                    Dim prm3 As New SqlParameter("@TipoCartera", SqlDbType.Int)
                    Dim prm4 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar)
                    prm.Direction = ParameterDirection.Output
                    prm1.Direction = ParameterDirection.Input
                    prm2.Direction = ParameterDirection.Input
                    prm3.Direction = ParameterDirection.Input
                    prm4.Direction = ParameterDirection.Input
                    prm.Value = 0
                    prm1.Value = glosessioncar
                    prm2.Value = GloClv_TipSer
                    prm3.Value = 0
                    prm4.Value = GloUsuario

                    .Parameters.Add(prm1)
                    .Parameters.Add(prm2)
                    .Parameters.Add(prm3)
                    .Parameters.Add(prm4)
                    .Parameters.Add(prm)
                    Dim i As Integer = comando.ExecuteNonQuery()
                    GLOClv_Cartera = prm.Value
                End With
            Else

                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CON
                    .CommandText = "Genera_CarteraDIG "
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0

                    Dim prm As New SqlParameter("@Clv_Cartera", SqlDbType.BigInt)
                    Dim prm1 As New SqlParameter("@clv_session2", SqlDbType.BigInt)
                    Dim prm2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
                    Dim prm3 As New SqlParameter("@TipoCartera", SqlDbType.Int)
                    Dim prm4 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar)
                    prm.Direction = ParameterDirection.Output
                    prm1.Direction = ParameterDirection.Input
                    prm2.Direction = ParameterDirection.Input
                    prm3.Direction = ParameterDirection.Input
                    prm4.Direction = ParameterDirection.Input
                    prm.Value = 0
                    prm1.Value = glosessioncar
                    prm2.Value = GloClv_TipSer
                    prm3.Value = 0
                    prm4.Value = GloUsuario
                    .Parameters.Add(prm1)
                    .Parameters.Add(prm2)
                    .Parameters.Add(prm3)
                    .Parameters.Add(prm4)
                    .Parameters.Add(prm)
                    Dim i As Integer = comando.ExecuteNonQuery()
                    GLOClv_Cartera = prm.Value
                End With
            End If

            Me.Dame_ciudad_carteraTableAdapter.Connection = CON
            Me.Dame_ciudad_carteraTableAdapter.Fill(Me.ProcedimientosArnoldo2.Dame_ciudad_cartera, glosessioncar, LocCiudades)

            ConfigureCrystalReportsCarteraXSD(op, "")


            resp = MsgBox("Deseas Guardar la Cartera ", MsgBoxStyle.YesNo)
            If resp = MsgBoxResult.No Then
                Me.BORRACARTERASTableAdapter.Connection = CON
                Me.BORRACARTERASTableAdapter.Fill(Me.DataSetEDGAR.BORRACARTERAS, GLOClv_Cartera, GloClv_TipSer)
            End If
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub GENERAMICARTERA_PorPeriodo()
        Dim comando As SqlClient.SqlCommand
        Dim resp As MsgBoxResult = MsgBoxResult.Yes
        Try
            
            If Me.ComboPeriodo.SelectedValue >= 1 Then
                Dim CON2 As New SqlConnection(MiConexion)
                CON2.Open()
                Dim comando2 As SqlClient.SqlCommand
                comando2 = New SqlClient.SqlCommand
                With comando2
                    .Connection = CON2
                    .CommandText = "PorPeriodo_GrabaPeriodoTmp "
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0
                    ' Create a SqlParameter for each parameter in the stored procedure.
                    Dim prm1 As New SqlParameter("@clv_session", SqlDbType.BigInt)
                    Dim prm2 As New SqlParameter("@Clv_Periodo", SqlDbType.Int)


                    prm1.Direction = ParameterDirection.Input
                    prm2.Direction = ParameterDirection.Input

                    prm1.Value = glosessioncar
                    prm2.Value = Me.ComboPeriodo.SelectedValue


                    .Parameters.Add(prm1)
                    .Parameters.Add(prm2)
                    Dim i As Integer = comando2.ExecuteNonQuery()
                End With
                CON2.Close()
            End If
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            If GloClv_TipSer <> 3 Then
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CON
                    .CommandText = "Genera_Cartera_Por_Periodo "
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0
                    ' Create a SqlParameter for each parameter in the stored procedure.
                    Dim prm As New SqlParameter("@Clv_Cartera", SqlDbType.BigInt)
                    Dim prm1 As New SqlParameter("@clv_session", SqlDbType.BigInt)
                    Dim prm2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
                    Dim prm3 As New SqlParameter("@TipoCartera", SqlDbType.Int)
                    Dim prm4 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar)
                    prm.Direction = ParameterDirection.Output
                    prm1.Direction = ParameterDirection.Input
                    prm2.Direction = ParameterDirection.Input
                    prm3.Direction = ParameterDirection.Input
                    prm4.Direction = ParameterDirection.Input
                    prm.Value = 0
                    prm1.Value = glosessioncar
                    prm2.Value = GloClv_TipSer
                    prm3.Value = 0
                    prm4.Value = GloUsuario

                    .Parameters.Add(prm1)
                    .Parameters.Add(prm2)
                    .Parameters.Add(prm3)
                    .Parameters.Add(prm4)
                    .Parameters.Add(prm)
                    Dim i As Integer = comando.ExecuteNonQuery()
                    GLOClv_Cartera = prm.Value
                End With
            Else
                If IdSistema = "SA" Then
                    comando = New SqlClient.SqlCommand
                    With comando
                        .Connection = CON
                        .CommandText = "Genera_Cartera_Por_Periodo "
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = 0
                        ' Create a SqlParameter for each parameter in the stored procedure.
                        Dim prm As New SqlParameter("@Clv_Cartera", SqlDbType.BigInt)
                        Dim prm1 As New SqlParameter("@clv_session", SqlDbType.BigInt)
                        Dim prm2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
                        Dim prm3 As New SqlParameter("@TipoCartera", SqlDbType.Int)
                        Dim prm4 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar)
                        prm.Direction = ParameterDirection.Output
                        prm1.Direction = ParameterDirection.Input
                        prm2.Direction = ParameterDirection.Input
                        prm3.Direction = ParameterDirection.Input
                        prm4.Direction = ParameterDirection.Input
                        prm.Value = 0
                        prm1.Value = glosessioncar
                        prm2.Value = GloClv_TipSer
                        prm3.Value = 0
                        prm4.Value = GloUsuario

                        .Parameters.Add(prm1)
                        .Parameters.Add(prm2)
                        .Parameters.Add(prm3)
                        .Parameters.Add(prm4)
                        .Parameters.Add(prm)
                        Dim i As Integer = comando.ExecuteNonQuery()
                        GLOClv_Cartera = prm.Value
                    End With
                Else
                    comando = New SqlClient.SqlCommand
                    With comando
                        .Connection = CON
                        .CommandText = "Genera_CarteraDIG_PorPeriodo "
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = 0

                        ' Create a SqlParameter for each parameter in the stored procedure.
                        Dim prm As New SqlParameter("@Clv_Cartera", SqlDbType.BigInt)
                        Dim prm1 As New SqlParameter("@clv_session2", SqlDbType.BigInt)
                        Dim prm2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
                        Dim prm3 As New SqlParameter("@TipoCartera", SqlDbType.Int)
                        Dim prm4 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar)
                        prm.Direction = ParameterDirection.Output
                        prm1.Direction = ParameterDirection.Input
                        prm2.Direction = ParameterDirection.Input
                        prm3.Direction = ParameterDirection.Input
                        prm4.Direction = ParameterDirection.Input
                        prm.Value = 0
                        prm1.Value = glosessioncar
                        prm2.Value = GloClv_TipSer
                        prm3.Value = 0
                        prm4.Value = GloUsuario
                        .Parameters.Add(prm1)
                        .Parameters.Add(prm2)
                        .Parameters.Add(prm3)
                        .Parameters.Add(prm4)
                        .Parameters.Add(prm)
                        Dim i As Integer = comando.ExecuteNonQuery()
                        GLOClv_Cartera = prm.Value
                    End With
                End If
            End If
            Me.Dame_ciudad_carteraTableAdapter.Connection = CON
            Me.Dame_ciudad_carteraTableAdapter.Fill(Me.ProcedimientosArnoldo2.Dame_ciudad_cartera, glosessioncar, LocCiudades)
            ConfigureCrystalReports(op, "")

            CON.Close()
            resp = MsgBox("Deseas Guardar la Cartera ", MsgBoxStyle.YesNo)
            If resp = MsgBoxResult.No Then

                Dim CON3 As New SqlConnection(MiConexion)
                CON3.Open()
                Dim comando3 As SqlClient.SqlCommand
                comando3 = New SqlClient.SqlCommand
                With comando3
                    .Connection = CON3
                    .CommandText = "PorPeriodo_BORRACARTERAS"
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0

                    Dim prm1 As New SqlParameter("@Clv_Cartera", SqlDbType.BigInt)
                    Dim prm2 As New SqlParameter("@Clv_Tipser", SqlDbType.Int)


                    prm1.Direction = ParameterDirection.Input
                    prm2.Direction = ParameterDirection.Input

                    prm1.Value = GLOClv_Cartera
                    prm2.Value = GloClv_TipSer


                    .Parameters.Add(prm1)
                    .Parameters.Add(prm2)
                    Dim i As Integer = comando3.ExecuteNonQuery()
                End With
                CON3.Close()
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ComboPeriodo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboPeriodo.SelectedIndexChanged
        If IsNumeric(Me.ComboPeriodo.SelectedValue) = True Then
            gloPorClv_Periodo = Me.ComboPeriodo.SelectedValue
        Else
            gloPorClv_Periodo = 0
        End If
    End Sub

    Private Sub Rep_CarterasDataGridView_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Rep_CarterasDataGridView.CellContentClick

    End Sub
End Class