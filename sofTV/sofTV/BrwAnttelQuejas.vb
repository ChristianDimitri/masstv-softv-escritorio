Imports System.Data.SqlClient
Imports System.Text

Public Class BrwAnttelQuejas
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opcion = "N"
        GloClv_TipSer = 0
        If ComboBox2.SelectedValue > 0 Then GloClv_TipSer = Me.ComboBox2.SelectedValue
        FrmQuejasAntTel.Show()
    End Sub

    Private Sub consultar()
        If gloClave > 0 Then
            opcion = "C"
            gloClave = Me.DataGridView1.SelectedCells(0).Value
            GloClv_TipSer = Me.DataGridView1.SelectedCells(6).Value
            FrmQuejasAntTel.Show()
        Else
            MsgBox(mensaje2)
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.DataGridView1.RowCount > 0 Then
            gloClave = Me.DataGridView1.SelectedCells(0).Value
            consultar()
        Else
            MsgBox(mensaje2)
        End If
    End Sub

    Private Sub modificar()
        If gloClave > 0 Then
            opcion = "M"
            gloClave = DataGridView1.SelectedCells(0).Value
            If IsNumeric(Me.Label9.Text) = True Then
                GloClv_TipSer = DataGridView1.SelectedCells(6).Value
            Else
                GloClv_TipSer = 0
            End If

            FrmQuejasAntTel.Show()
        Else
            MsgBox("Seleccione algun Tipo de Servicio")
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Me.DataGridView1.RowCount > 0 Then
            gloClave = DataGridView1.SelectedCells(0).Value

            Me.BuscaBloqueadoTableAdapter.Connection = CON
            Me.BuscaBloqueadoTableAdapter.Fill(Me.DataSetLidia.BuscaBloqueado, CInt(DataGridView1.SelectedCells(2).Value.ToString), NUM, num2)
            If NUM = 0 Or num2 = 0 Then
                modificar()
            Else
                MsgBox("El Cliente " + DataGridView1.SelectedCells(2).Value.ToString + " Ha Sido Bloqueado por lo que no se Podr� Llevar a cabo la Queja", MsgBoxStyle.Exclamation)
            End If

        Else
            MsgBox(mensaje1)
        End If
        CON.Close()
    End Sub


    Private Sub BrwAnttelQuejas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Label7.ForeColor = Color.Black
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        TextBox3.BackColor = CMBNombreTextBox.BackColor
        'Me.DamePermisosFormTableAdapter.Fill(Me.NewSofTvDataSet.DamePermisosForm, GloTipoUsuario, Me.Name, 1, glolec, gloescr, gloctr)
        If gloescr = 1 Then
            Me.Button2.Enabled = False
            Me.Button4.Enabled = False
        End If
        Me.MuestraTipSerPrincipal2TableAdapter.Connection = CON
        Me.MuestraTipSerPrincipal2TableAdapter.Fill(Me.DataSetLidia2.MuestraTipSerPrincipal2)
        Me.MUESTRAUSUARIOSTableAdapter.Connection = CON
        Me.MUESTRAUSUARIOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRAUSUARIOS, 2)
        GloClv_TipSer = Me.ComboBox2.SelectedValue
        Busca(4)
        GloBnd = False
        CON.Close()
    End Sub

    Private Sub Busca(ByVal op As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim sTATUS As Integer = 0
        Try

            If Me.CheckBox1.Checked = True And Me.CheckBox2.Checked = False Then
                sTATUS = 1
            ElseIf Me.CheckBox2.Checked = True And Me.CheckBox1.Checked = False Then
                sTATUS = 2
            Else
                sTATUS = 0
            End If

            If IsNumeric(Me.ComboBox2.SelectedValue) = True Then
                If op = 0 Then 'contrato
                    If IsNumeric(Me.TextBox1.Text) = True Then
                        'Me.BUSCALLAMADASDEINTERNETTableAdapter.Connection = CON
                        'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, New System.Nullable(Of Integer)(CType(Me.ComboBox2.SelectedValue, Integer)), 0, Me.TextBox1.Text, "", "", "", New System.Nullable(Of Integer)(CType(0, Integer)), 0, sTATUS)
                        'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, 0, 0, Me.TextBox1.Text, "", "", "", New System.Nullable(Of Integer)(CType(0, Integer)), 0, sTATUS)
                        BUSCALLAMADASDEINTERNET_SEPARADO(Me.ComboBox2.SelectedValue, 0, Me.TextBox1.Text, "", "", "", "", "", "", "", 0, 0, sTATUS)
                    Else
                        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                    End If

                ElseIf op = 1 Then
                    If Len(Trim(Me.TextBox2.Text)) > 0 Or Len(Trim(Me.APaternoTextBox.Text)) > 0 Or Len(Trim(Me.AMaternoTextBox.Text)) > 0 Then
                        'Me.BUSCALLAMADASDEINTERNETTableAdapter.Connection = CON
                        'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, New System.Nullable(Of Integer)(CType(Me.ComboBox2.SelectedValue, Integer)), 0, 0, Me.TextBox2.Text, "", "", New System.Nullable(Of Integer)(CType(1, Integer)), 0, sTATUS)
                        'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, 0, 0, 0, Me.TextBox2.Text, "", "", New System.Nullable(Of Integer)(CType(1, Integer)), 0, sTATUS)
                        BUSCALLAMADASDEINTERNET_SEPARADO(Me.ComboBox2.SelectedValue, 0, 0, Me.TextBox2.Text, Me.APaternoTextBox.Text, Me.AMaternoTextBox.Text, "", "", "", "", 1, 0, sTATUS)
                    Else
                        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                    End If
                ElseIf op = 2 Then 'Calle y numero
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Connection = CON
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, New System.Nullable(Of Integer)(CType(Me.ComboBox2.SelectedValue, Integer)), 0, 0, "", Me.BCALLE.Text, Me.BNUMERO.Text, New System.Nullable(Of Integer)(CType(2, Integer)), 0, sTATUS)
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, 0, 0, 0, "", Me.BCALLE.Text, Me.BNUMERO.Text, New System.Nullable(Of Integer)(CType(2, Integer)), 0, sTATUS)
                    BUSCALLAMADASDEINTERNET_SEPARADO(Me.ComboBox2.SelectedValue, 0, 0, "", "", "", Me.BCALLE.Text, Me.BNUMERO.Text, "", "", 2, 0, sTATUS)
                ElseIf op = 11 Then 'Usuario
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Connection = CON
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, New System.Nullable(Of Integer)(CType(Me.ComboBox2.SelectedValue, Integer)), 0, 0, "", "", "", New System.Nullable(Of Integer)(CType(11, Integer)), Me.ComboBox1.SelectedValue, sTATUS)
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, 0, 0, 0, "", "", "", New System.Nullable(Of Integer)(CType(11, Integer)), Me.ComboBox1.SelectedValue, sTATUS)
                    BUSCALLAMADASDEINTERNET_SEPARADO(Me.ComboBox2.SelectedValue, 0, 0, "", "", "", "", "", "", "", 11, Me.ComboBox1.SelectedValue, sTATUS)
                ElseIf op = 12 Then 'con queja
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Connection = CON
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, New System.Nullable(Of Integer)(CType(Me.ComboBox2.SelectedValue, Integer)), 0, 0, "", "", "", New System.Nullable(Of Integer)(CType(12, Integer)), 0, sTATUS)
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, 0, 0, 0, "", "", "", New System.Nullable(Of Integer)(CType(12, Integer)), 0, sTATUS)
                    BUSCALLAMADASDEINTERNET_SEPARADO(Me.ComboBox2.SelectedValue, 0, 0, "", "", "", "", "", "", "", 12, 0, sTATUS)
                ElseIf op = 13 Then 'Sin queja
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Connection = CON
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, New System.Nullable(Of Integer)(CType(Me.ComboBox2.SelectedValue, Integer)), 0, 0, "", "", "", New System.Nullable(Of Integer)(CType(13, Integer)), 0, sTATUS)
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, 0, 0, 0, "", "", "", New System.Nullable(Of Integer)(CType(13, Integer)), 0, sTATUS)
                    BUSCALLAMADASDEINTERNET_SEPARADO(Me.ComboBox2.SelectedValue, 0, 0, "", "", "", "", "", "", "", 13, 0, sTATUS)
                ElseIf op = 199 Then 'Status
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Connection = CON
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, New System.Nullable(Of Integer)(CType(Me.ComboBox2.SelectedValue, Integer)), 0, 0, sTATUS, "", "", New System.Nullable(Of Integer)(CType(199, Integer)), 0, sTATUS)
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, 0, 0, 0, sTATUS, "", "", New System.Nullable(Of Integer)(CType(199, Integer)), 0, sTATUS)
                    BUSCALLAMADASDEINTERNET_SEPARADO(Me.ComboBox2.SelectedValue, 0, 0, sTATUS, "", "", "", "", "", "", 199, 0, sTATUS)
                ElseIf op = 3 Then 'clv_Orden
                    If IsNumeric(Me.Clv_llamada.Text) = True Then
                        'Me.BUSCALLAMADASDEINTERNETTableAdapter.Connection = CON
                        'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, New System.Nullable(Of Integer)(CType(Me.ComboBox2.SelectedValue, Integer)), Me.Clv_llamada.Text, 0, "", "", "", New System.Nullable(Of Integer)(CType(3, Integer)), 0, sTATUS)
                        'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, 0, Me.Clv_llamada.Text, 0, "", "", "", New System.Nullable(Of Integer)(CType(3, Integer)), 0, sTATUS)
                        BUSCALLAMADASDEINTERNET_SEPARADO(Me.ComboBox2.SelectedValue, Me.Clv_llamada.Text, 0, sTATUS, "", "", "", "", "", "", 3, 0, sTATUS)
                    Else
                        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                    End If
                ElseIf op = 5 Then
                    If Len(Trim(Me.TxtSetUpBox.Text)) > 0 Or Len(Trim(Me.TxtTarjeta.Text)) > 0 Then
                        'Me.BUSCALLAMADASDEINTERNETTableAdapter.Connection = CON
                        'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, New System.Nullable(Of Integer)(CType(Me.ComboBox2.SelectedValue, Integer)), 0, 0, Me.TextBox2.Text, "", "", New System.Nullable(Of Integer)(CType(1, Integer)), 0, sTATUS)
                        'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, 0, 0, 0, Me.TextBox2.Text, "", "", New System.Nullable(Of Integer)(CType(1, Integer)), 0, sTATUS)
                        BUSCALLAMADASDEINTERNET_SEPARADO(Me.ComboBox2.SelectedValue, 0, 0, "", "", "", "", "", Me.TxtSetUpBox.Text, Me.TxtTarjeta.Text, 5, 0, sTATUS)
                    Else
                        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                    End If

                Else
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Connection = CON
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, New System.Nullable(Of Integer)(CType(Me.ComboBox2.SelectedValue, Integer)), 0, 0, "", "", "", New System.Nullable(Of Integer)(CType(4, Integer)), 0, sTATUS)
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, 0, 0, 0, "", "", "", New System.Nullable(Of Integer)(CType(4, Integer)), 0, sTATUS)
                    'BUSCALLAMADASDEINTERNET_SEPARADO(Me.ComboBox2.SelectedValue, 0, 0, sTATUS, "", "", "", "", 4, "", "", 0, sTATUS)
                    BUSCALLAMADASDEINTERNET_SEPARADO(Me.ComboBox2.SelectedValue, 0, 0, "", "", "", "", "", "", "", 4, 0, sTATUS)
                End If
                Me.TextBox1.Clear()
                Me.TextBox2.Clear()
                Me.Clv_llamada.Clear()
                Me.BNUMERO.Clear()
                Me.BCALLE.Clear()
            Else
                MsgBox("Seleccione el Tipo de Servicio")
            End If
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub



    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged

        Busca(12)

    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged

        Busca(13)

    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        If IsNumeric(Me.ComboBox1.SelectedValue) = True Then
            Busca(11)
        End If
    End Sub
    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Busca(0)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Busca(1)
        TextBox2.Text = ""
        APaternoTextBox.Text = ""
        AMaternoTextBox.Text = ""
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(0)
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
        End If
    End Sub


    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub


    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        Busca(3)
    End Sub


    Private Sub Clv_calleLabel2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Clv_calleLabel2.TextChanged
        gloClave = Me.Clv_calleLabel2.Text
    End Sub

    Private Sub BrwAnttelQuejas_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            Me.ComboBox2.SelectedValue = GloClv_TipSer
            Me.ComboBox2.Text = GloNom_TipSer
            Me.ComboBox2.FindString(GloNom_TipSer)
            Me.ComboBox2.Text = GloNom_TipSer
            Busca(4)


        End If
    End Sub



    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Busca(2)
    End Sub

    Private Sub BCALLE_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BCALLE.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
        End If
    End Sub

   

    Private Sub BNUMERO_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BNUMERO.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
        End If
    End Sub

    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick


        Me.Clv_calleLabel2.Text = Me.DataGridView1.SelectedCells(0).Value
        Me.ContratoLabel1.Text = Me.DataGridView1.SelectedCells(2).Value
        Me.CMBNombreTextBox.Text = Me.DataGridView1.SelectedCells(3).Value

        Dame_DescripcionLlamada(Me.DataGridView1.SelectedCells(0).Value)
    
    End Sub

    Private Function Dame_DescripcionLlamada(ByVal prmClv_llamada As Long) As Integer
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_llamada", SqlDbType.BigInt, prmClv_llamada)
        BaseII.CreateMyParameter("@Descripcion", ParameterDirection.Output, SqlDbType.VarChar, 500)
        BaseII.ProcedimientoOutPut("Dame_DescripcionLlamada")

        TextBox3.Text = BaseII.dicoPar("@Descripcion").ToString()
    End Function

    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If Button4.Enabled = True Then
            modificar()
        ElseIf Button3.Enabled = True Then
            consultar()
        End If
    End Sub

    Private Sub Clv_llamada_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Clv_llamada.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(3)
        End If
    End Sub


    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        If IsNumeric(Me.ComboBox1.SelectedValue) = True Then
            Busca(11)
        End If
    End Sub
    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        If Me.ComboBox2.SelectedIndex <> -1 Then
            GloClv_TipSer = Me.ComboBox2.SelectedValue
            Busca(4)
        End If
    End Sub
    Private Sub BUSCALLAMADASDEINTERNET_SEPARADO(ByVal Clv_TipSerAtn As Integer, ByVal Clv_LlamdaAtn As Long, ByVal ContratoAtn As Long, ByVal NombreAtn As String, _
                                                 ByVal ApePaternoAtn As String, ByVal ApeMaternoAtn As String, ByVal CalleAtn As String, ByVal NumeroAtn As String, _
                                                 ByVal SetUpBox As String, ByVal Tarjeta As String, ByVal OpAtn As Integer, ByVal Clv_UsuarioAtn As Integer, ByVal TipoQuejaAtn As Integer)

        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder

        StrSQL.Append("EXEC BUSCALLAMADASDEINTERNET_SEPARADO ")
        StrSQL.Append(CStr(Clv_TipSerAtn) & ", ")
        StrSQL.Append(CStr(Clv_LlamdaAtn) & ", ")
        StrSQL.Append(CStr(ContratoAtn) & ", ")
        StrSQL.Append("'" & NombreAtn & "', ")
        StrSQL.Append("'" & ApePaternoAtn & "', ")
        StrSQL.Append("'" & ApeMaternoAtn & "', ")
        StrSQL.Append("'" & CalleAtn & "', ")
        StrSQL.Append("'" & NumeroAtn & "', ")
        StrSQL.Append("'" & SetUpBox & "', ")
        StrSQL.Append("'" & Tarjeta & "', ")
        StrSQL.Append(CStr(OpAtn) & ", ")
        StrSQL.Append(CStr(Clv_UsuarioAtn) & ", ")
        StrSQL.Append(CStr(TipoQuejaAtn))

        Dim DA As New SqlDataAdapter(StrSQL.ToString, CON)
        Dim DT As New DataTable
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.DataGridView1.DataSource = BS.DataSource
            'gloClave = Me.DataGridView1.SelectedCells(0).Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub Button14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button14.Click
        Busca(5)
        TxtSetUpBox.Text = ""
        TxtTarjeta.Text = ""
    End Sub
End Class