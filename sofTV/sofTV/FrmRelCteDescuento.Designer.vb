<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRelCteDescuento
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DescuentoLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmRelCteDescuento))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.ConRelCteDescuentoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConRelCteDescuentoTableAdapter = New sofTV.DataSetEricTableAdapters.ConRelCteDescuentoTableAdapter()
        Me.DescuentoNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.bnDescuentos = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbGuardar = New System.Windows.Forms.ToolStripButton()
        Me.tsbEliminar = New System.Windows.Forms.ToolStripButton()
        DescuentoLabel = New System.Windows.Forms.Label()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConRelCteDescuentoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DescuentoNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bnDescuentos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnDescuentos.SuspendLayout()
        Me.SuspendLayout()
        '
        'DescuentoLabel
        '
        DescuentoLabel.AutoSize = True
        DescuentoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescuentoLabel.Location = New System.Drawing.Point(97, 52)
        DescuentoLabel.Name = "DescuentoLabel"
        DescuentoLabel.Size = New System.Drawing.Size(79, 15)
        DescuentoLabel.TabIndex = 5
        DescuentoLabel.Text = "Descuento:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(214, 72)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(21, 16)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "%"
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(196, 119)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(105, 31)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ConRelCteDescuentoBindingSource
        '
        Me.ConRelCteDescuentoBindingSource.DataMember = "ConRelCteDescuento"
        Me.ConRelCteDescuentoBindingSource.DataSource = Me.DataSetEric
        '
        'ConRelCteDescuentoTableAdapter
        '
        Me.ConRelCteDescuentoTableAdapter.ClearBeforeFill = True
        '
        'DescuentoNumericUpDown
        '
        Me.DescuentoNumericUpDown.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.ConRelCteDescuentoBindingSource, "Descuento", True))
        Me.DescuentoNumericUpDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescuentoNumericUpDown.Location = New System.Drawing.Point(100, 70)
        Me.DescuentoNumericUpDown.Name = "DescuentoNumericUpDown"
        Me.DescuentoNumericUpDown.Size = New System.Drawing.Size(93, 21)
        Me.DescuentoNumericUpDown.TabIndex = 6
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'bnDescuentos
        '
        Me.bnDescuentos.AddNewItem = Nothing
        Me.bnDescuentos.CountItem = Nothing
        Me.bnDescuentos.DeleteItem = Nothing
        Me.bnDescuentos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnDescuentos.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbEliminar, Me.tsbGuardar})
        Me.bnDescuentos.Location = New System.Drawing.Point(0, 0)
        Me.bnDescuentos.MoveFirstItem = Nothing
        Me.bnDescuentos.MoveLastItem = Nothing
        Me.bnDescuentos.MoveNextItem = Nothing
        Me.bnDescuentos.MovePreviousItem = Nothing
        Me.bnDescuentos.Name = "bnDescuentos"
        Me.bnDescuentos.PositionItem = Nothing
        Me.bnDescuentos.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnDescuentos.Size = New System.Drawing.Size(321, 25)
        Me.bnDescuentos.TabIndex = 7
        Me.bnDescuentos.Text = "BindingNavigator1"
        '
        'tsbGuardar
        '
        Me.tsbGuardar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.tsbGuardar.Image = CType(resources.GetObject("tsbGuardar.Image"), System.Drawing.Image)
        Me.tsbGuardar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbGuardar.Name = "tsbGuardar"
        Me.tsbGuardar.Size = New System.Drawing.Size(63, 22)
        Me.tsbGuardar.Text = "&Guardar"
        '
        'tsbEliminar
        '
        Me.tsbEliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.tsbEliminar.Image = CType(resources.GetObject("tsbEliminar.Image"), System.Drawing.Image)
        Me.tsbEliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbEliminar.Name = "tsbEliminar"
        Me.tsbEliminar.Size = New System.Drawing.Size(65, 22)
        Me.tsbEliminar.Text = "&Eliminar"
        '
        'FrmRelCteDescuento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(321, 170)
        Me.Controls.Add(Me.bnDescuentos)
        Me.Controls.Add(DescuentoLabel)
        Me.Controls.Add(Me.DescuentoNumericUpDown)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label1)
        Me.Name = "FrmRelCteDescuento"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Descuento Cliente"
        Me.TopMost = True
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConRelCteDescuentoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DescuentoNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bnDescuentos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnDescuentos.ResumeLayout(False)
        Me.bnDescuentos.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents ConRelCteDescuentoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConRelCteDescuentoTableAdapter As sofTV.DataSetEricTableAdapters.ConRelCteDescuentoTableAdapter
    Friend WithEvents DescuentoNumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents bnDescuentos As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbGuardar As System.Windows.Forms.ToolStripButton
End Class
