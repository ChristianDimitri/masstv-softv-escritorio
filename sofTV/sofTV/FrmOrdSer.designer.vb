﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmOrdSer
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_OrdenLabel As System.Windows.Forms.Label
        Dim ContratoLabel As System.Windows.Forms.Label
        Dim StatusLabel As System.Windows.Forms.Label
        Dim Clv_TecnicoLabel As System.Windows.Forms.Label
        Dim IMPRESALabel As System.Windows.Forms.Label
        Dim ObsLabel As System.Windows.Forms.Label
        Dim ServicioLabel As System.Windows.Forms.Label
        Dim SOLOINTERNETLabel1 As System.Windows.Forms.Label
        Dim CMBLabel3 As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label14 As System.Windows.Forms.Label
        Dim Fecha_SoliciutudLabel As System.Windows.Forms.Label
        Dim Visita1Label1 As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim Label15 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmOrdSer))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.TxtRefOxxo = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Visita3TextBox = New System.Windows.Forms.MaskedTextBox()
        Me.Visita3TextBox1 = New System.Windows.Forms.TextBox()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.dtpHoraVisita3 = New System.Windows.Forms.DateTimePicker()
        Me.dtpHoraVisita2 = New System.Windows.Forms.DateTimePicker()
        Me.dtpHoraVisita1 = New System.Windows.Forms.DateTimePicker()
        Me.Hora_sol = New System.Windows.Forms.DateTimePicker()
        Me.Hora_Eje = New System.Windows.Forms.DateTimePicker()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.EjecutoLabel1 = New System.Windows.Forms.Label()
        Me.Consulta_RelOrdenUsuarioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric2 = New sofTV.DataSetEric2()
        Me.FolioTextBox = New System.Windows.Forms.TextBox()
        Me.DimeSiTieneunaBitacoraBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEdgarRev2 = New sofTV.DataSetEdgarRev2()
        Me.GeneroLabel1 = New System.Windows.Forms.Label()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Muestra_no_ordenBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetarnoldo = New sofTV.DataSetarnoldo()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Fec_EjeTextBox = New System.Windows.Forms.MaskedTextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Fec_EjeTextBox1 = New System.Windows.Forms.TextBox()
        Me.CONORDSERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.Visita2TextBox = New System.Windows.Forms.MaskedTextBox()
        Me.Visita1TextBox = New System.Windows.Forms.MaskedTextBox()
        Me.ContratoTextBox = New System.Windows.Forms.TextBox()
        Me.ComboBox5 = New System.Windows.Forms.ComboBox()
        Me.MuestraTipSerPrincipalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CMBLabel7 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Fecha_SoliciutudMaskedTextBox = New System.Windows.Forms.DateTimePicker()
        Me.StatusTextBox = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ESHOTELCheckBox = New System.Windows.Forms.CheckBox()
        Me.BUSCLIPORCONTRATOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SOLOINTERNETCheckBox = New System.Windows.Forms.CheckBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.NOMBRELabel1 = New System.Windows.Forms.Label()
        Me.CALLELabel1 = New System.Windows.Forms.Label()
        Me.COLONIALabel1 = New System.Windows.Forms.Label()
        Me.NUMEROLabel1 = New System.Windows.Forms.Label()
        Me.CIUDADLabel1 = New System.Windows.Forms.Label()
        Me.Clv_TecnicoTextBox = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.MUESTRATECNICOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.RadioButton3 = New System.Windows.Forms.RadioButton()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.CMBTextBox4 = New System.Windows.Forms.TextBox()
        Me.CONORDSERBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.CONORDSERBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Clv_OrdenTextBox = New System.Windows.Forms.TextBox()
        Me.IMPRESACheckBox = New System.Windows.Forms.CheckBox()
        Me.Visita2TextBox1 = New System.Windows.Forms.TextBox()
        Me.Visita1TextBox1 = New System.Windows.Forms.TextBox()
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.cbCuadrilla = New System.Windows.Forms.ComboBox()
        Me.Tecnico = New System.Windows.Forms.ComboBox()
        Me.MuestraTecnicosAlmacenBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.SectorLabel = New System.Windows.Forms.Label()
        Me.SectorCombo = New System.Windows.Forms.ComboBox()
        Me.ObsTextBox = New System.Windows.Forms.TextBox()
        Me.MUESTRATECNICOSBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.ValidarNuevoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BUSCADetOrdSerDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Accion = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BUSCADetOrdSerBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ValidacionTextBox = New System.Windows.Forms.TextBox()
        Me.Valida_DetOrdenBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FechaDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.Dame_fecha_hora_servBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameUltimo_dia_del_MesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRATRABAJOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONDetOrdSerBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button5 = New System.Windows.Forms.Button()
        Me.CONORDSERTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONORDSERTableAdapter()
        Me.MUESTRATECNICOSTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRATECNICOSTableAdapter()
        Me.BUSCLIPORCONTRATOTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.BUSCLIPORCONTRATOTableAdapter()
        Me.DameSerDELCliBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameSerDELCliTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.dameSerDELCliTableAdapter()
        Me.CONDetOrdSerTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONDetOrdSerTableAdapter()
        Me.MUESTRATRABAJOSTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRATRABAJOSTableAdapter()
        Me.BUSCADetOrdSerTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.BUSCADetOrdSerTableAdapter()
        Me.DameUltimo_dia_del_MesTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.DameUltimo_dia_del_MesTableAdapter()
        Me.Valida_DetOrdenTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.Valida_DetOrdenTableAdapter()
        Me.MuestraTipSerPrincipalTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MuestraTipSerPrincipalTableAdapter()
        Me.ClaveTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_OrdenTextBox1 = New System.Windows.Forms.TextBox()
        Me.Clv_TrabajoTextBox = New System.Windows.Forms.TextBox()
        Me.PREEJECUTAOrdSerBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PREEJECUTAOrdSerTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.PREEJECUTAOrdSerTableAdapter()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.DimesigrabaordBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLidia = New sofTV.DataSetLidia()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.dgvDetOrdSer = New System.Windows.Forms.DataGridView()
        Me.clave = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clv_orden = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clv_trabajo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.trabajo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.descripciondet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.acciondet = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.observacionesdet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.serealizadet = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.seretiro = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.ValidaTrabajosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ValidaTrabajosTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.ValidaTrabajosTableAdapter()
        Me.CONTADORTextBox = New System.Windows.Forms.TextBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.ValidarNuevoTableAdapter = New sofTV.DataSetarnoldoTableAdapters.ValidarNuevoTableAdapter()
        Me.Muestra_Tecnicos_AlmacenTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Muestra_Tecnicos_AlmacenTableAdapter()
        Me.DameClv_Session_TecnicosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameClv_Session_TecnicosTableAdapter = New sofTV.DataSetarnoldoTableAdapters.DameClv_Session_TecnicosTableAdapter()
        Me.Dame_FolioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_FolioTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Dame_FolioTableAdapter()
        Me.Inserta_Bitacora_tecBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Bitacora_tecTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Inserta_Bitacora_tecTableAdapter()
        Me.Inserta_DetBitTecBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_DetBitTecTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Inserta_DetBitTecTableAdapter()
        Me.Inserta_Rel_Bitacora_OrdenBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Rel_Bitacora_OrdenTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Inserta_Rel_Bitacora_OrdenTableAdapter()
        Me.Muestra_no_ordenTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Muestra_no_ordenTableAdapter()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.ComboBoxTap = New System.Windows.Forms.ComboBox()
        Me.LabelTab = New System.Windows.Forms.Label()
        Me.TextBoxTab = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.PlacaTextBox = New System.Windows.Forms.TextBox()
        Me.ConRelCtePlacaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.DimeSiTieneunaBitacoraTableAdapter = New sofTV.DataSetEdgarRev2TableAdapters.DimeSiTieneunaBitacoraTableAdapter()
        Me.Dame_Impresora_OrdenesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_Impresora_OrdenesTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Dame_Impresora_OrdenesTableAdapter()
        Me.Dame_fecha_hora_servTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Dame_fecha_hora_servTableAdapter()
        Me.DimesigrabaordTableAdapter = New sofTV.DataSetLidiaTableAdapters.dimesigrabaordTableAdapter()
        Me.BuscaBloqueadoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BuscaBloqueadoTableAdapter = New sofTV.DataSetLidiaTableAdapters.BuscaBloqueadoTableAdapter()
        Me.Grabar_det_ordenBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Grabar_det_ordenTableAdapter = New sofTV.DataSetarnoldoTableAdapters.grabar_det_ordenTableAdapter()
        Me.ValidaSiEsAcometidaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ValidaSiEsAcometidaTableAdapter = New sofTV.DataSetEricTableAdapters.ValidaSiEsAcometidaTableAdapter()
        Me.ConRelCtePlacaTableAdapter = New sofTV.DataSetEricTableAdapters.ConRelCtePlacaTableAdapter()
        Me.Inserta_RelCobraDescBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_RelCobraDescTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_RelCobraDescTableAdapter()
        Me.Imprime_OrdenBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Imprime_OrdenTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Imprime_OrdenTableAdapter()
        Me.Cambia_Tipo_cablemodemBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Cambia_Tipo_cablemodemTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Cambia_Tipo_cablemodemTableAdapter()
        Me.Guarda_Comentario_CCABMBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Guarda_Comentario_CCABMTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Guarda_Comentario_CCABMTableAdapter()
        Me.ChecaOrdSerRetiroBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ChecaOrdSerRetiroTableAdapter = New sofTV.DataSetEricTableAdapters.ChecaOrdSerRetiroTableAdapter()
        Me.BorraMotivoCanServBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorraMotivoCanServTableAdapter = New sofTV.DataSetEricTableAdapters.BorraMotivoCanServTableAdapter()
        Me.ChecaMotivoCanServBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ChecaMotivoCanServTableAdapter = New sofTV.DataSetEricTableAdapters.ChecaMotivoCanServTableAdapter()
        Me.InsertMotCanServBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertMotCanServTableAdapter = New sofTV.DataSetEricTableAdapters.InsertMotCanServTableAdapter()
        Me.NueRelOrdenUsuarioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NueRelOrdenUsuarioTableAdapter = New sofTV.DataSetEricTableAdapters.NueRelOrdenUsuarioTableAdapter()
        Me.Consulta_RelOrdenUsuarioTableAdapter = New sofTV.DataSetEric2TableAdapters.Consulta_RelOrdenUsuarioTableAdapter()
        Me.btnGuardaRetiroParcial = New System.Windows.Forms.Button()
        Me.btnEjecutarRetiroParcial = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter5 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter6 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter7 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter8 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter9 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter10 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter11 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter12 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter13 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Clv_OrdenLabel = New System.Windows.Forms.Label()
        ContratoLabel = New System.Windows.Forms.Label()
        StatusLabel = New System.Windows.Forms.Label()
        Clv_TecnicoLabel = New System.Windows.Forms.Label()
        IMPRESALabel = New System.Windows.Forms.Label()
        ObsLabel = New System.Windows.Forms.Label()
        ServicioLabel = New System.Windows.Forms.Label()
        SOLOINTERNETLabel1 = New System.Windows.Forms.Label()
        CMBLabel3 = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label14 = New System.Windows.Forms.Label()
        Fecha_SoliciutudLabel = New System.Windows.Forms.Label()
        Visita1Label1 = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        Label15 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.Panel11.SuspendLayout()
        CType(Me.Consulta_RelOrdenUsuarioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DimeSiTieneunaBitacoraBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Muestra_no_ordenBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        CType(Me.CONORDSERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTipSerPrincipalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.BUSCLIPORCONTRATOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRATECNICOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.CONORDSERBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONORDSERBindingNavigator.SuspendLayout()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel6.SuspendLayout()
        CType(Me.MuestraTecnicosAlmacenBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.MUESTRATECNICOSBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ValidarNuevoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BUSCADetOrdSerDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BUSCADetOrdSerBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Valida_DetOrdenBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_fecha_hora_servBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameUltimo_dia_del_MesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRATRABAJOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONDetOrdSerBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameSerDELCliBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PREEJECUTAOrdSerBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        CType(Me.DimesigrabaordBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel10.SuspendLayout()
        CType(Me.dgvDetOrdSer, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ValidaTrabajosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameClv_Session_TecnicosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_FolioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Bitacora_tecBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_DetBitTecBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Rel_Bitacora_OrdenBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel7.SuspendLayout()
        CType(Me.ConRelCtePlacaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel8.SuspendLayout()
        CType(Me.Dame_Impresora_OrdenesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BuscaBloqueadoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Grabar_det_ordenBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ValidaSiEsAcometidaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_RelCobraDescBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Imprime_OrdenBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Cambia_Tipo_cablemodemBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Guarda_Comentario_CCABMBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ChecaOrdSerRetiroBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorraMotivoCanServBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ChecaMotivoCanServBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertMotCanServBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NueRelOrdenUsuarioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Clv_OrdenLabel
        '
        Clv_OrdenLabel.AutoSize = True
        Clv_OrdenLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_OrdenLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_OrdenLabel.Location = New System.Drawing.Point(31, 47)
        Clv_OrdenLabel.Name = "Clv_OrdenLabel"
        Clv_OrdenLabel.Size = New System.Drawing.Size(66, 15)
        Clv_OrdenLabel.TabIndex = 0
        Clv_OrdenLabel.Text = "# Orden :"
        '
        'ContratoLabel
        '
        ContratoLabel.AutoSize = True
        ContratoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ContratoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ContratoLabel.Location = New System.Drawing.Point(27, 76)
        ContratoLabel.Name = "ContratoLabel"
        ContratoLabel.Size = New System.Drawing.Size(65, 15)
        ContratoLabel.TabIndex = 4
        ContratoLabel.Text = "Contrato:"
        '
        'StatusLabel
        '
        StatusLabel.AutoSize = True
        StatusLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        StatusLabel.ForeColor = System.Drawing.Color.OrangeRed
        StatusLabel.Location = New System.Drawing.Point(23, 4)
        StatusLabel.Name = "StatusLabel"
        StatusLabel.Size = New System.Drawing.Size(67, 20)
        StatusLabel.TabIndex = 14
        StatusLabel.Text = "Status:"
        '
        'Clv_TecnicoLabel
        '
        Clv_TecnicoLabel.AutoSize = True
        Clv_TecnicoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_TecnicoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_TecnicoLabel.Location = New System.Drawing.Point(59, 195)
        Clv_TecnicoLabel.Name = "Clv_TecnicoLabel"
        Clv_TecnicoLabel.Size = New System.Drawing.Size(65, 15)
        Clv_TecnicoLabel.TabIndex = 16
        Clv_TecnicoLabel.Text = "Técnico :"
        '
        'IMPRESALabel
        '
        IMPRESALabel.AutoSize = True
        IMPRESALabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        IMPRESALabel.ForeColor = System.Drawing.Color.DarkOrange
        IMPRESALabel.Location = New System.Drawing.Point(127, 224)
        IMPRESALabel.Name = "IMPRESALabel"
        IMPRESALabel.Size = New System.Drawing.Size(202, 18)
        IMPRESALabel.TabIndex = 18
        IMPRESALabel.Text = "La Orden ya fue Impresa :"
        '
        'ObsLabel
        '
        ObsLabel.AutoSize = True
        ObsLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ObsLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ObsLabel.Location = New System.Drawing.Point(5, 0)
        ObsLabel.Name = "ObsLabel"
        ObsLabel.Size = New System.Drawing.Size(109, 15)
        ObsLabel.TabIndex = 22
        ObsLabel.Text = "Observaciones :"
        '
        'ServicioLabel
        '
        ServicioLabel.AutoSize = True
        ServicioLabel.Location = New System.Drawing.Point(9, 96)
        ServicioLabel.Name = "ServicioLabel"
        ServicioLabel.Size = New System.Drawing.Size(151, 16)
        ServicioLabel.TabIndex = 18
        ServicioLabel.Text = "Servicio Asignados :"
        '
        'SOLOINTERNETLabel1
        '
        SOLOINTERNETLabel1.AutoSize = True
        SOLOINTERNETLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SOLOINTERNETLabel1.ForeColor = System.Drawing.Color.White
        SOLOINTERNETLabel1.Location = New System.Drawing.Point(300, 73)
        SOLOINTERNETLabel1.Name = "SOLOINTERNETLabel1"
        SOLOINTERNETLabel1.Size = New System.Drawing.Size(97, 15)
        SOLOINTERNETLabel1.TabIndex = 16
        SOLOINTERNETLabel1.Text = "Solo Internet :"
        '
        'CMBLabel3
        '
        CMBLabel3.AutoSize = True
        CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabel3.ForeColor = System.Drawing.Color.Black
        CMBLabel3.Location = New System.Drawing.Point(9, 408)
        CMBLabel3.Name = "CMBLabel3"
        CMBLabel3.Size = New System.Drawing.Size(246, 18)
        CMBLabel3.TabIndex = 26
        CMBLabel3.Text = "Detalle de la Orden de Servicio:"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(22, 12)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(65, 15)
        Label1.TabIndex = 427
        Label1.Text = "Técnico :"
        '
        'Label14
        '
        Label14.AutoSize = True
        Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label14.ForeColor = System.Drawing.Color.LightSlateGray
        Label14.Location = New System.Drawing.Point(14, 38)
        Label14.Name = "Label14"
        Label14.Size = New System.Drawing.Size(73, 15)
        Label14.TabIndex = 429
        Label14.Text = "Cuadrilla :"
        '
        'Fecha_SoliciutudLabel
        '
        Fecha_SoliciutudLabel.AutoSize = True
        Fecha_SoliciutudLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_SoliciutudLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Fecha_SoliciutudLabel.Location = New System.Drawing.Point(625, 148)
        Fecha_SoliciutudLabel.Name = "Fecha_SoliciutudLabel"
        Fecha_SoliciutudLabel.Size = New System.Drawing.Size(71, 15)
        Fecha_SoliciutudLabel.TabIndex = 404
        Fecha_SoliciutudLabel.Text = "Solicitud :"
        '
        'Visita1Label1
        '
        Visita1Label1.AutoSize = True
        Visita1Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Visita1Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Visita1Label1.Location = New System.Drawing.Point(641, 202)
        Visita1Label1.Name = "Visita1Label1"
        Visita1Label1.Size = New System.Drawing.Size(54, 15)
        Visita1Label1.TabIndex = 417
        Visita1Label1.Text = "Visita1:"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label5.ForeColor = System.Drawing.Color.LightSlateGray
        Label5.Location = New System.Drawing.Point(641, 229)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(54, 15)
        Label5.TabIndex = 418
        Label5.Text = "Visita2:"
        '
        'Label15
        '
        Label15.AutoSize = True
        Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label15.ForeColor = System.Drawing.Color.LightSlateGray
        Label15.Location = New System.Drawing.Point(641, 254)
        Label15.Name = "Label15"
        Label15.Size = New System.Drawing.Size(54, 15)
        Label15.TabIndex = 440
        Label15.Text = "Visita3:"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.Panel11)
        Me.Panel1.Controls.Add(Me.Visita3TextBox)
        Me.Panel1.Controls.Add(Me.Visita3TextBox1)
        Me.Panel1.Controls.Add(Me.Panel9)
        Me.Panel1.Controls.Add(Me.dtpHoraVisita3)
        Me.Panel1.Controls.Add(Me.dtpHoraVisita2)
        Me.Panel1.Controls.Add(Me.dtpHoraVisita1)
        Me.Panel1.Controls.Add(Label15)
        Me.Panel1.Controls.Add(Me.Hora_sol)
        Me.Panel1.Controls.Add(Me.Hora_Eje)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.EjecutoLabel1)
        Me.Panel1.Controls.Add(Me.FolioTextBox)
        Me.Panel1.Controls.Add(Me.GeneroLabel1)
        Me.Panel1.Controls.Add(Me.TextBox3)
        Me.Panel1.Controls.Add(Me.Panel5)
        Me.Panel1.Controls.Add(Me.Visita2TextBox)
        Me.Panel1.Controls.Add(Me.Visita1TextBox)
        Me.Panel1.Controls.Add(Label5)
        Me.Panel1.Controls.Add(Visita1Label1)
        Me.Panel1.Controls.Add(Me.ContratoTextBox)
        Me.Panel1.Controls.Add(Me.ComboBox5)
        Me.Panel1.Controls.Add(Me.CMBLabel7)
        Me.Panel1.Controls.Add(Me.TextBox2)
        Me.Panel1.Controls.Add(Me.TextBox1)
        Me.Panel1.Controls.Add(Me.Fecha_SoliciutudMaskedTextBox)
        Me.Panel1.Controls.Add(Fecha_SoliciutudLabel)
        Me.Panel1.Controls.Add(Me.StatusTextBox)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(Me.ComboBox1)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.CMBTextBox4)
        Me.Panel1.Controls.Add(Me.CONORDSERBindingNavigator)
        Me.Panel1.Controls.Add(Clv_OrdenLabel)
        Me.Panel1.Controls.Add(Me.Clv_OrdenTextBox)
        Me.Panel1.Controls.Add(ContratoLabel)
        Me.Panel1.Controls.Add(Clv_TecnicoLabel)
        Me.Panel1.Controls.Add(IMPRESALabel)
        Me.Panel1.Controls.Add(Me.IMPRESACheckBox)
        Me.Panel1.Controls.Add(Me.Visita2TextBox1)
        Me.Panel1.Controls.Add(Me.Visita1TextBox1)
        Me.Panel1.Location = New System.Drawing.Point(12, 6)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1000, 289)
        Me.Panel1.TabIndex = 0
        Me.Panel1.TabStop = True
        '
        'Panel11
        '
        Me.Panel11.Controls.Add(Me.TxtRefOxxo)
        Me.Panel11.Controls.Add(Me.Label19)
        Me.Panel11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel11.Location = New System.Drawing.Point(258, 69)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(305, 27)
        Me.Panel11.TabIndex = 446
        '
        'TxtRefOxxo
        '
        Me.TxtRefOxxo.Location = New System.Drawing.Point(112, 2)
        Me.TxtRefOxxo.Name = "TxtRefOxxo"
        Me.TxtRefOxxo.Size = New System.Drawing.Size(162, 21)
        Me.TxtRefOxxo.TabIndex = 1
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label19.Location = New System.Drawing.Point(29, 5)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(77, 15)
        Me.Label19.TabIndex = 0
        Me.Label19.Text = "Ref. Oxxo :"
        '
        'Visita3TextBox
        '
        Me.Visita3TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Visita3TextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Visita3TextBox.Location = New System.Drawing.Point(702, 252)
        Me.Visita3TextBox.Mask = "00/00/0000"
        Me.Visita3TextBox.Name = "Visita3TextBox"
        Me.Visita3TextBox.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.Visita3TextBox.Size = New System.Drawing.Size(107, 21)
        Me.Visita3TextBox.TabIndex = 441
        Me.Visita3TextBox.TabStop = False
        Me.Visita3TextBox.ValidatingType = GetType(Date)
        '
        'Visita3TextBox1
        '
        Me.Visita3TextBox1.Location = New System.Drawing.Point(736, 252)
        Me.Visita3TextBox1.Name = "Visita3TextBox1"
        Me.Visita3TextBox1.Size = New System.Drawing.Size(72, 20)
        Me.Visita3TextBox1.TabIndex = 445
        '
        'Panel9
        '
        Me.Panel9.Location = New System.Drawing.Point(933, 145)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(51, 133)
        Me.Panel9.TabIndex = 439
        '
        'dtpHoraVisita3
        '
        Me.dtpHoraVisita3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpHoraVisita3.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtpHoraVisita3.Location = New System.Drawing.Point(831, 254)
        Me.dtpHoraVisita3.Name = "dtpHoraVisita3"
        Me.dtpHoraVisita3.Size = New System.Drawing.Size(145, 21)
        Me.dtpHoraVisita3.TabIndex = 444
        '
        'dtpHoraVisita2
        '
        Me.dtpHoraVisita2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpHoraVisita2.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtpHoraVisita2.Location = New System.Drawing.Point(831, 226)
        Me.dtpHoraVisita2.Name = "dtpHoraVisita2"
        Me.dtpHoraVisita2.Size = New System.Drawing.Size(145, 21)
        Me.dtpHoraVisita2.TabIndex = 443
        '
        'dtpHoraVisita1
        '
        Me.dtpHoraVisita1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpHoraVisita1.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtpHoraVisita1.Location = New System.Drawing.Point(831, 200)
        Me.dtpHoraVisita1.Name = "dtpHoraVisita1"
        Me.dtpHoraVisita1.Size = New System.Drawing.Size(145, 21)
        Me.dtpHoraVisita1.TabIndex = 442
        '
        'Hora_sol
        '
        Me.Hora_sol.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Hora_sol.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.Hora_sol.Location = New System.Drawing.Point(834, 145)
        Me.Hora_sol.Name = "Hora_sol"
        Me.Hora_sol.Size = New System.Drawing.Size(145, 21)
        Me.Hora_sol.TabIndex = 438
        Me.Hora_sol.Visible = False
        '
        'Hora_Eje
        '
        Me.Hora_Eje.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Hora_Eje.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.Hora_Eje.Location = New System.Drawing.Point(831, 174)
        Me.Hora_Eje.Name = "Hora_Eje"
        Me.Hora_Eje.Size = New System.Drawing.Size(145, 21)
        Me.Hora_Eje.TabIndex = 437
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(607, 101)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(63, 15)
        Me.Label12.TabIndex = 436
        Me.Label12.Text = "Ejecutó :"
        Me.Label12.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(608, 77)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(62, 15)
        Me.Label7.TabIndex = 435
        Me.Label7.Text = "Generó :"
        Me.Label7.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(288, 35)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(180, 16)
        Me.Label3.TabIndex = 434
        Me.Label3.Text = "No. Folio de la Bitacora :"
        Me.Label3.Visible = False
        '
        'EjecutoLabel1
        '
        Me.EjecutoLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_RelOrdenUsuarioBindingSource, "Ejecuto", True))
        Me.EjecutoLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EjecutoLabel1.Location = New System.Drawing.Point(673, 93)
        Me.EjecutoLabel1.Name = "EjecutoLabel1"
        Me.EjecutoLabel1.Size = New System.Drawing.Size(306, 23)
        Me.EjecutoLabel1.TabIndex = 37
        Me.EjecutoLabel1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        Me.EjecutoLabel1.Visible = False
        '
        'Consulta_RelOrdenUsuarioBindingSource
        '
        Me.Consulta_RelOrdenUsuarioBindingSource.DataMember = "Consulta_RelOrdenUsuario"
        Me.Consulta_RelOrdenUsuarioBindingSource.DataSource = Me.DataSetEric2
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric2"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'FolioTextBox
        '
        Me.FolioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FolioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DimeSiTieneunaBitacoraBindingSource, "Folio", True))
        Me.FolioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FolioTextBox.Location = New System.Drawing.Point(474, 33)
        Me.FolioTextBox.Name = "FolioTextBox"
        Me.FolioTextBox.Size = New System.Drawing.Size(100, 22)
        Me.FolioTextBox.TabIndex = 433
        Me.FolioTextBox.TabStop = False
        Me.FolioTextBox.Visible = False
        '
        'DimeSiTieneunaBitacoraBindingSource
        '
        Me.DimeSiTieneunaBitacoraBindingSource.DataMember = "DimeSiTieneunaBitacora"
        Me.DimeSiTieneunaBitacoraBindingSource.DataSource = Me.DataSetEdgarRev2
        '
        'DataSetEdgarRev2
        '
        Me.DataSetEdgarRev2.DataSetName = "DataSetEdgarRev2"
        Me.DataSetEdgarRev2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GeneroLabel1
        '
        Me.GeneroLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_RelOrdenUsuarioBindingSource, "Genero", True))
        Me.GeneroLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GeneroLabel1.Location = New System.Drawing.Point(670, 69)
        Me.GeneroLabel1.Name = "GeneroLabel1"
        Me.GeneroLabel1.Size = New System.Drawing.Size(309, 23)
        Me.GeneroLabel1.TabIndex = 36
        Me.GeneroLabel1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        Me.GeneroLabel1.Visible = False
        '
        'TextBox3
        '
        Me.TextBox3.AllowDrop = True
        Me.TextBox3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Muestra_no_ordenBindingSource, "NO_BITACORA", True))
        Me.TextBox3.Enabled = False
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(609, 358)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(100, 14)
        Me.TextBox3.TabIndex = 432
        Me.TextBox3.TabStop = False
        '
        'Muestra_no_ordenBindingSource
        '
        Me.Muestra_no_ordenBindingSource.DataMember = "Muestra_no_orden"
        Me.Muestra_no_ordenBindingSource.DataSource = Me.DataSetarnoldo
        '
        'DataSetarnoldo
        '
        Me.DataSetarnoldo.DataSetName = "DataSetarnoldo"
        Me.DataSetarnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel5.Controls.Add(Me.Fec_EjeTextBox)
        Me.Panel5.Controls.Add(Me.Label6)
        Me.Panel5.Controls.Add(Me.Fec_EjeTextBox1)
        Me.Panel5.Location = New System.Drawing.Point(604, 167)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(221, 30)
        Me.Panel5.TabIndex = 22
        Me.Panel5.TabStop = True
        '
        'Fec_EjeTextBox
        '
        Me.Fec_EjeTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Fec_EjeTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fec_EjeTextBox.Location = New System.Drawing.Point(98, 6)
        Me.Fec_EjeTextBox.Mask = "00/00/0000"
        Me.Fec_EjeTextBox.Name = "Fec_EjeTextBox"
        Me.Fec_EjeTextBox.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.Fec_EjeTextBox.Size = New System.Drawing.Size(107, 21)
        Me.Fec_EjeTextBox.TabIndex = 22
        Me.Fec_EjeTextBox.ValidatingType = GetType(Date)
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label6.Location = New System.Drawing.Point(13, 7)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(78, 15)
        Me.Label6.TabIndex = 416
        Me.Label6.Text = "Ejecución :"
        '
        'Fec_EjeTextBox1
        '
        Me.Fec_EjeTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONORDSERBindingSource, "Fec_Eje", True))
        Me.Fec_EjeTextBox1.Location = New System.Drawing.Point(111, 6)
        Me.Fec_EjeTextBox1.Name = "Fec_EjeTextBox1"
        Me.Fec_EjeTextBox1.Size = New System.Drawing.Size(94, 20)
        Me.Fec_EjeTextBox1.TabIndex = 422
        Me.Fec_EjeTextBox1.TabStop = False
        '
        'CONORDSERBindingSource
        '
        Me.CONORDSERBindingSource.DataMember = "CONORDSER"
        Me.CONORDSERBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Visita2TextBox
        '
        Me.Visita2TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Visita2TextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Visita2TextBox.Location = New System.Drawing.Point(702, 227)
        Me.Visita2TextBox.Mask = "00/00/0000"
        Me.Visita2TextBox.Name = "Visita2TextBox"
        Me.Visita2TextBox.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.Visita2TextBox.Size = New System.Drawing.Size(107, 21)
        Me.Visita2TextBox.TabIndex = 421
        Me.Visita2TextBox.TabStop = False
        Me.Visita2TextBox.ValidatingType = GetType(Date)
        '
        'Visita1TextBox
        '
        Me.Visita1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Visita1TextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Visita1TextBox.Location = New System.Drawing.Point(702, 201)
        Me.Visita1TextBox.Mask = "00/00/0000"
        Me.Visita1TextBox.Name = "Visita1TextBox"
        Me.Visita1TextBox.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.Visita1TextBox.Size = New System.Drawing.Size(107, 21)
        Me.Visita1TextBox.TabIndex = 420
        Me.Visita1TextBox.TabStop = False
        Me.Visita1TextBox.ValidatingType = GetType(Date)
        '
        'ContratoTextBox
        '
        Me.ContratoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ContratoTextBox.CausesValidation = False
        Me.ContratoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONORDSERBindingSource, "Contrato", True))
        Me.ContratoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContratoTextBox.Location = New System.Drawing.Point(101, 75)
        Me.ContratoTextBox.Name = "ContratoTextBox"
        Me.ContratoTextBox.Size = New System.Drawing.Size(104, 21)
        Me.ContratoTextBox.TabIndex = 0
        '
        'ComboBox5
        '
        Me.ComboBox5.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONORDSERBindingSource, "Clv_TipSer", True))
        Me.ComboBox5.DataSource = Me.MuestraTipSerPrincipalBindingSource
        Me.ComboBox5.DisplayMember = "Concepto"
        Me.ComboBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox5.FormattingEnabled = True
        Me.ComboBox5.Location = New System.Drawing.Point(115, 75)
        Me.ComboBox5.Name = "ComboBox5"
        Me.ComboBox5.Size = New System.Drawing.Size(29, 21)
        Me.ComboBox5.TabIndex = 410
        Me.ComboBox5.TabStop = False
        Me.ComboBox5.ValueMember = "Clv_TipSerPrincipal"
        '
        'MuestraTipSerPrincipalBindingSource
        '
        Me.MuestraTipSerPrincipalBindingSource.DataMember = "MuestraTipSerPrincipal"
        Me.MuestraTipSerPrincipalBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CMBLabel7
        '
        Me.CMBLabel7.AutoSize = True
        Me.CMBLabel7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel7.ForeColor = System.Drawing.Color.Black
        Me.CMBLabel7.Location = New System.Drawing.Point(12, 22)
        Me.CMBLabel7.Name = "CMBLabel7"
        Me.CMBLabel7.Size = New System.Drawing.Size(154, 18)
        Me.CMBLabel7.TabIndex = 411
        Me.CMBLabel7.Text = "Tipo de Servicios  :"
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.ForeColor = System.Drawing.Color.Red
        Me.TextBox2.Location = New System.Drawing.Point(171, 30)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(280, 19)
        Me.TextBox2.TabIndex = 409
        Me.TextBox2.TabStop = False
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(736, 101)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(201, 22)
        Me.TextBox1.TabIndex = 408
        Me.TextBox1.TabStop = False
        '
        'Fecha_SoliciutudMaskedTextBox
        '
        Me.Fecha_SoliciutudMaskedTextBox.CustomFormat = "dd/MMM/yyyy"
        Me.Fecha_SoliciutudMaskedTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONORDSERBindingSource, "Fec_Sol", True))
        Me.Fecha_SoliciutudMaskedTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.CONORDSERBindingSource, "Fec_Sol", True))
        Me.Fecha_SoliciutudMaskedTextBox.Enabled = False
        Me.Fecha_SoliciutudMaskedTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fecha_SoliciutudMaskedTextBox.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.Fecha_SoliciutudMaskedTextBox.Location = New System.Drawing.Point(702, 145)
        Me.Fecha_SoliciutudMaskedTextBox.Name = "Fecha_SoliciutudMaskedTextBox"
        Me.Fecha_SoliciutudMaskedTextBox.Size = New System.Drawing.Size(124, 22)
        Me.Fecha_SoliciutudMaskedTextBox.TabIndex = 407
        Me.Fecha_SoliciutudMaskedTextBox.TabStop = False
        '
        'StatusTextBox
        '
        Me.StatusTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.StatusTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.StatusTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONORDSERBindingSource, "Status", True))
        Me.StatusTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusTextBox.ForeColor = System.Drawing.Color.Maroon
        Me.StatusTextBox.Location = New System.Drawing.Point(770, 148)
        Me.StatusTextBox.Name = "StatusTextBox"
        Me.StatusTextBox.Size = New System.Drawing.Size(24, 14)
        Me.StatusTextBox.TabIndex = 150
        Me.StatusTextBox.TabStop = False
        Me.StatusTextBox.Text = "P"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Gray
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.TreeView1)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(ServicioLabel)
        Me.GroupBox1.Controls.Add(Me.ESHOTELCheckBox)
        Me.GroupBox1.Controls.Add(SOLOINTERNETLabel1)
        Me.GroupBox1.Controls.Add(Me.SOLOINTERNETCheckBox)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.NOMBRELabel1)
        Me.GroupBox1.Controls.Add(Me.CALLELabel1)
        Me.GroupBox1.Controls.Add(Me.COLONIALabel1)
        Me.GroupBox1.Controls.Add(Me.NUMEROLabel1)
        Me.GroupBox1.Controls.Add(Me.CIUDADLabel1)
        Me.GroupBox1.Controls.Add(Me.Clv_TecnicoTextBox)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.White
        Me.GroupBox1.Location = New System.Drawing.Point(27, 100)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(553, 184)
        Me.GroupBox1.TabIndex = 172
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Cliente"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(430, 73)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(67, 15)
        Me.Label13.TabIndex = 432
        Me.Label13.Text = "Es hotel :"
        '
        'TreeView1
        '
        Me.TreeView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView1.Location = New System.Drawing.Point(165, 99)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(382, 79)
        Me.TreeView1.TabIndex = 19
        Me.TreeView1.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(166, 107)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(55, 16)
        Me.Label2.TabIndex = 431
        Me.Label2.Text = "Label2"
        Me.Label2.Visible = False
        '
        'ESHOTELCheckBox
        '
        Me.ESHOTELCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.BUSCLIPORCONTRATOBindingSource, "ESHOTEL", True))
        Me.ESHOTELCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ESHOTELCheckBox.Location = New System.Drawing.Point(503, 67)
        Me.ESHOTELCheckBox.Name = "ESHOTELCheckBox"
        Me.ESHOTELCheckBox.Size = New System.Drawing.Size(21, 24)
        Me.ESHOTELCheckBox.TabIndex = 18
        Me.ESHOTELCheckBox.TabStop = False
        '
        'BUSCLIPORCONTRATOBindingSource
        '
        Me.BUSCLIPORCONTRATOBindingSource.DataMember = "BUSCLIPORCONTRATO"
        Me.BUSCLIPORCONTRATOBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'SOLOINTERNETCheckBox
        '
        Me.SOLOINTERNETCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.BUSCLIPORCONTRATOBindingSource, "SOLOINTERNET", True))
        Me.SOLOINTERNETCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.SOLOINTERNETCheckBox.Location = New System.Drawing.Point(403, 67)
        Me.SOLOINTERNETCheckBox.Name = "SOLOINTERNETCheckBox"
        Me.SOLOINTERNETCheckBox.Size = New System.Drawing.Size(21, 24)
        Me.SOLOINTERNETCheckBox.TabIndex = 17
        Me.SOLOINTERNETCheckBox.TabStop = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(378, 41)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(31, 13)
        Me.Label11.TabIndex = 16
        Me.Label11.Text = "Col. :"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(299, 41)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(20, 13)
        Me.Label10.TabIndex = 15
        Me.Label10.Text = "# :"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(9, 41)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(58, 13)
        Me.Label9.TabIndex = 14
        Me.Label9.Text = "Dirección :"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(10, 18)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(50, 13)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Nombre :"
        '
        'NOMBRELabel1
        '
        Me.NOMBRELabel1.AutoEllipsis = True
        Me.NOMBRELabel1.BackColor = System.Drawing.Color.Gray
        Me.NOMBRELabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOBindingSource, "NOMBRE", True))
        Me.NOMBRELabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NOMBRELabel1.Location = New System.Drawing.Point(69, 18)
        Me.NOMBRELabel1.Name = "NOMBRELabel1"
        Me.NOMBRELabel1.Size = New System.Drawing.Size(467, 23)
        Me.NOMBRELabel1.TabIndex = 3
        '
        'CALLELabel1
        '
        Me.CALLELabel1.AutoEllipsis = True
        Me.CALLELabel1.BackColor = System.Drawing.Color.Gray
        Me.CALLELabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOBindingSource, "CALLE", True))
        Me.CALLELabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CALLELabel1.Location = New System.Drawing.Point(69, 41)
        Me.CALLELabel1.Name = "CALLELabel1"
        Me.CALLELabel1.Size = New System.Drawing.Size(224, 23)
        Me.CALLELabel1.TabIndex = 5
        '
        'COLONIALabel1
        '
        Me.COLONIALabel1.AutoEllipsis = True
        Me.COLONIALabel1.BackColor = System.Drawing.Color.Gray
        Me.COLONIALabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOBindingSource, "COLONIA", True))
        Me.COLONIALabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.COLONIALabel1.Location = New System.Drawing.Point(415, 41)
        Me.COLONIALabel1.Name = "COLONIALabel1"
        Me.COLONIALabel1.Size = New System.Drawing.Size(121, 23)
        Me.COLONIALabel1.TabIndex = 7
        '
        'NUMEROLabel1
        '
        Me.NUMEROLabel1.AutoEllipsis = True
        Me.NUMEROLabel1.BackColor = System.Drawing.Color.Gray
        Me.NUMEROLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOBindingSource, "NUMERO", True))
        Me.NUMEROLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NUMEROLabel1.Location = New System.Drawing.Point(325, 41)
        Me.NUMEROLabel1.Name = "NUMEROLabel1"
        Me.NUMEROLabel1.Size = New System.Drawing.Size(47, 23)
        Me.NUMEROLabel1.TabIndex = 9
        '
        'CIUDADLabel1
        '
        Me.CIUDADLabel1.AutoEllipsis = True
        Me.CIUDADLabel1.BackColor = System.Drawing.Color.Gray
        Me.CIUDADLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOBindingSource, "CIUDAD", True))
        Me.CIUDADLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CIUDADLabel1.Location = New System.Drawing.Point(69, 65)
        Me.CIUDADLabel1.Name = "CIUDADLabel1"
        Me.CIUDADLabel1.Size = New System.Drawing.Size(224, 23)
        Me.CIUDADLabel1.TabIndex = 11
        '
        'Clv_TecnicoTextBox
        '
        Me.Clv_TecnicoTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TecnicoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_TecnicoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONORDSERBindingSource, "Clv_Tecnico", True))
        Me.Clv_TecnicoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_TecnicoTextBox.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TecnicoTextBox.Location = New System.Drawing.Point(236, 129)
        Me.Clv_TecnicoTextBox.Name = "Clv_TecnicoTextBox"
        Me.Clv_TecnicoTextBox.Size = New System.Drawing.Size(24, 14)
        Me.Clv_TecnicoTextBox.TabIndex = 170
        Me.Clv_TecnicoTextBox.TabStop = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.SystemColors.Control
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(208, 74)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(30, 22)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "..."
        Me.Button1.UseVisualStyleBackColor = False
        '
        'ComboBox1
        '
        Me.ComboBox1.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONORDSERBindingSource, "Clv_Tecnico", True))
        Me.ComboBox1.DataSource = Me.MUESTRATECNICOSBindingSource
        Me.ComboBox1.DisplayMember = "nombre"
        Me.ComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(130, 192)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(371, 23)
        Me.ComboBox1.TabIndex = 50
        Me.ComboBox1.TabStop = False
        Me.ComboBox1.ValueMember = "clave"
        '
        'MUESTRATECNICOSBindingSource
        '
        Me.MUESTRATECNICOSBindingSource.DataMember = "MUESTRATECNICOS"
        Me.MUESTRATECNICOSBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(StatusLabel)
        Me.Panel2.Controls.Add(Me.RadioButton3)
        Me.Panel2.Controls.Add(Me.RadioButton2)
        Me.Panel2.Controls.Add(Me.RadioButton1)
        Me.Panel2.Location = New System.Drawing.Point(580, 31)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(402, 31)
        Me.Panel2.TabIndex = 49
        '
        'RadioButton3
        '
        Me.RadioButton3.AutoSize = True
        Me.RadioButton3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton3.ForeColor = System.Drawing.Color.Black
        Me.RadioButton3.Location = New System.Drawing.Point(328, 3)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RadioButton3.Size = New System.Drawing.Size(66, 22)
        Me.RadioButton3.TabIndex = 17
        Me.RadioButton3.Text = "Visita"
        Me.RadioButton3.UseVisualStyleBackColor = True
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton2.ForeColor = System.Drawing.Color.Black
        Me.RadioButton2.Location = New System.Drawing.Point(223, 3)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(99, 22)
        Me.RadioButton2.TabIndex = 16
        Me.RadioButton2.Text = "Ejecutada"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Checked = True
        Me.RadioButton1.Enabled = False
        Me.RadioButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton1.ForeColor = System.Drawing.Color.Black
        Me.RadioButton1.Location = New System.Drawing.Point(113, 3)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(99, 22)
        Me.RadioButton1.TabIndex = 15
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Pendiente"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'CMBTextBox4
        '
        Me.CMBTextBox4.BackColor = System.Drawing.Color.Gray
        Me.CMBTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox4.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox4.Location = New System.Drawing.Point(604, 124)
        Me.CMBTextBox4.Name = "CMBTextBox4"
        Me.CMBTextBox4.Size = New System.Drawing.Size(375, 19)
        Me.CMBTextBox4.TabIndex = 48
        Me.CMBTextBox4.TabStop = False
        Me.CMBTextBox4.Text = "Fechas de "
        Me.CMBTextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CONORDSERBindingNavigator
        '
        Me.CONORDSERBindingNavigator.AddNewItem = Nothing
        Me.CONORDSERBindingNavigator.BindingSource = Me.CONORDSERBindingSource
        Me.CONORDSERBindingNavigator.CountItem = Nothing
        Me.CONORDSERBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONORDSERBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.CONORDSERBindingNavigatorSaveItem, Me.BindingNavigatorDeleteItem})
        Me.CONORDSERBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONORDSERBindingNavigator.MoveFirstItem = Nothing
        Me.CONORDSERBindingNavigator.MoveLastItem = Nothing
        Me.CONORDSERBindingNavigator.MoveNextItem = Nothing
        Me.CONORDSERBindingNavigator.MovePreviousItem = Nothing
        Me.CONORDSERBindingNavigator.Name = "CONORDSERBindingNavigator"
        Me.CONORDSERBindingNavigator.PositionItem = Nothing
        Me.CONORDSERBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONORDSERBindingNavigator.Size = New System.Drawing.Size(1000, 25)
        Me.CONORDSERBindingNavigator.TabIndex = 26
        Me.CONORDSERBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(90, 22)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        Me.BindingNavigatorDeleteItem.Visible = False
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(79, 22)
        Me.ToolStripButton1.Text = "&CANCELAR"
        '
        'CONORDSERBindingNavigatorSaveItem
        '
        Me.CONORDSERBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONORDSERBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONORDSERBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONORDSERBindingNavigatorSaveItem.Name = "CONORDSERBindingNavigatorSaveItem"
        Me.CONORDSERBindingNavigatorSaveItem.Size = New System.Drawing.Size(91, 22)
        Me.CONORDSERBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'Clv_OrdenTextBox
        '
        Me.Clv_OrdenTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_OrdenTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONORDSERBindingSource, "Clv_Orden", True))
        Me.Clv_OrdenTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_OrdenTextBox.Location = New System.Drawing.Point(101, 49)
        Me.Clv_OrdenTextBox.Name = "Clv_OrdenTextBox"
        Me.Clv_OrdenTextBox.ReadOnly = True
        Me.Clv_OrdenTextBox.Size = New System.Drawing.Size(104, 21)
        Me.Clv_OrdenTextBox.TabIndex = 100
        Me.Clv_OrdenTextBox.TabStop = False
        '
        'IMPRESACheckBox
        '
        Me.IMPRESACheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONORDSERBindingSource, "IMPRESA", True))
        Me.IMPRESACheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.IMPRESACheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IMPRESACheckBox.Location = New System.Drawing.Point(347, 221)
        Me.IMPRESACheckBox.Name = "IMPRESACheckBox"
        Me.IMPRESACheckBox.Size = New System.Drawing.Size(104, 24)
        Me.IMPRESACheckBox.TabIndex = 19
        Me.IMPRESACheckBox.TabStop = False
        '
        'Visita2TextBox1
        '
        Me.Visita2TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONORDSERBindingSource, "Visita2", True))
        Me.Visita2TextBox1.Location = New System.Drawing.Point(713, 227)
        Me.Visita2TextBox1.Name = "Visita2TextBox1"
        Me.Visita2TextBox1.Size = New System.Drawing.Size(95, 20)
        Me.Visita2TextBox1.TabIndex = 424
        Me.Visita2TextBox1.TabStop = False
        '
        'Visita1TextBox1
        '
        Me.Visita1TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONORDSERBindingSource, "Visita1", True))
        Me.Visita1TextBox1.Location = New System.Drawing.Point(714, 202)
        Me.Visita1TextBox1.Name = "Visita1TextBox1"
        Me.Visita1TextBox1.Size = New System.Drawing.Size(95, 20)
        Me.Visita1TextBox1.TabIndex = 423
        Me.Visita1TextBox1.TabStop = False
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.SystemColors.Control
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(154, 12)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(241, 33)
        Me.Button3.TabIndex = 3
        Me.Button3.Text = "&Descarga de Material."
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel6.Controls.Add(Label14)
        Me.Panel6.Controls.Add(Me.cbCuadrilla)
        Me.Panel6.Controls.Add(Me.Tecnico)
        Me.Panel6.Controls.Add(Label1)
        Me.Panel6.Location = New System.Drawing.Point(12, 301)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(501, 59)
        Me.Panel6.TabIndex = 2
        '
        'cbCuadrilla
        '
        Me.cbCuadrilla.DisplayMember = "Nombre"
        Me.cbCuadrilla.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbCuadrilla.FormattingEnabled = True
        Me.cbCuadrilla.Location = New System.Drawing.Point(93, 30)
        Me.cbCuadrilla.Name = "cbCuadrilla"
        Me.cbCuadrilla.Size = New System.Drawing.Size(374, 23)
        Me.cbCuadrilla.TabIndex = 428
        Me.cbCuadrilla.ValueMember = "clv_Tecnico"
        '
        'Tecnico
        '
        Me.Tecnico.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONORDSERBindingSource, "Clv_Tecnico", True))
        Me.Tecnico.DisplayMember = "Nombre"
        Me.Tecnico.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Tecnico.FormattingEnabled = True
        Me.Tecnico.Location = New System.Drawing.Point(93, 4)
        Me.Tecnico.Name = "Tecnico"
        Me.Tecnico.Size = New System.Drawing.Size(374, 23)
        Me.Tecnico.TabIndex = 2
        Me.Tecnico.ValueMember = "clv_Tecnico"
        '
        'MuestraTecnicosAlmacenBindingSource
        '
        Me.MuestraTecnicosAlmacenBindingSource.DataMember = "Muestra_Tecnicos_Almacen"
        Me.MuestraTecnicosAlmacenBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel3.Controls.Add(Me.Button9)
        Me.Panel3.Controls.Add(Me.Button2)
        Me.Panel3.Location = New System.Drawing.Point(12, 430)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(320, 35)
        Me.Panel3.TabIndex = 5
        Me.Panel3.TabStop = True
        '
        'Button9
        '
        Me.Button9.BackColor = System.Drawing.SystemColors.Control
        Me.Button9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button9.ForeColor = System.Drawing.Color.Black
        Me.Button9.Location = New System.Drawing.Point(8, 3)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(139, 26)
        Me.Button9.TabIndex = 5
        Me.Button9.Text = "&AGREGAR"
        Me.Button9.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.SystemColors.Control
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(178, 3)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(139, 26)
        Me.Button2.TabIndex = 6
        Me.Button2.Text = "&ELIMINAR"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'SectorLabel
        '
        Me.SectorLabel.AutoSize = True
        Me.SectorLabel.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SectorLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SectorLabel.ForeColor = System.Drawing.Color.Black
        Me.SectorLabel.Location = New System.Drawing.Point(693, 410)
        Me.SectorLabel.Name = "SectorLabel"
        Me.SectorLabel.Size = New System.Drawing.Size(68, 18)
        Me.SectorLabel.TabIndex = 29
        Me.SectorLabel.Text = "Sector :"
        Me.SectorLabel.Visible = False
        '
        'SectorCombo
        '
        Me.SectorCombo.DisplayMember = "Descripcion"
        Me.SectorCombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SectorCombo.FormattingEnabled = True
        Me.SectorCombo.Location = New System.Drawing.Point(767, 409)
        Me.SectorCombo.Name = "SectorCombo"
        Me.SectorCombo.Size = New System.Drawing.Size(245, 24)
        Me.SectorCombo.TabIndex = 28
        Me.SectorCombo.ValueMember = "Clv_Sector"
        '
        'ObsTextBox
        '
        Me.ObsTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ObsTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ObsTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONORDSERBindingSource, "Obs", True))
        Me.ObsTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ObsTextBox.Location = New System.Drawing.Point(120, 0)
        Me.ObsTextBox.MaxLength = 250
        Me.ObsTextBox.Multiline = True
        Me.ObsTextBox.Name = "ObsTextBox"
        Me.ObsTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.ObsTextBox.Size = New System.Drawing.Size(428, 40)
        Me.ObsTextBox.TabIndex = 4
        '
        'MUESTRATECNICOSBindingSource1
        '
        Me.MUESTRATECNICOSBindingSource1.DataMember = "MUESTRATECNICOS"
        Me.MUESTRATECNICOSBindingSource1.DataSource = Me.NewSofTvDataSet
        '
        'ValidarNuevoBindingSource
        '
        Me.ValidarNuevoBindingSource.DataMember = "ValidarNuevo"
        Me.ValidarNuevoBindingSource.DataSource = Me.DataSetarnoldo
        '
        'BUSCADetOrdSerDataGridView
        '
        Me.BUSCADetOrdSerDataGridView.AllowUserToAddRows = False
        Me.BUSCADetOrdSerDataGridView.AllowUserToDeleteRows = False
        Me.BUSCADetOrdSerDataGridView.AutoGenerateColumns = False
        Me.BUSCADetOrdSerDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.BUSCADetOrdSerDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.BUSCADetOrdSerDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.Descripcion, Me.Accion, Me.DataGridViewTextBoxColumn5})
        Me.BUSCADetOrdSerDataGridView.DataSource = Me.BUSCADetOrdSerBindingSource
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.BUSCADetOrdSerDataGridView.DefaultCellStyle = DataGridViewCellStyle5
        Me.BUSCADetOrdSerDataGridView.Location = New System.Drawing.Point(7, 7)
        Me.BUSCADetOrdSerDataGridView.Name = "BUSCADetOrdSerDataGridView"
        Me.BUSCADetOrdSerDataGridView.ReadOnly = True
        Me.BUSCADetOrdSerDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.BUSCADetOrdSerDataGridView.Size = New System.Drawing.Size(975, 63)
        Me.BUSCADetOrdSerDataGridView.TabIndex = 23
        Me.BUSCADetOrdSerDataGridView.TabStop = False
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Clave"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Clave"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Clv_Orden"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Clv_Orden"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Clv_Trabajo"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Clv_Trabajo"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'Descripcion
        '
        Me.Descripcion.DataPropertyName = "Descripcion"
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Descripcion.DefaultCellStyle = DataGridViewCellStyle2
        Me.Descripcion.HeaderText = "Descripcion"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        Me.Descripcion.Width = 300
        '
        'Accion
        '
        Me.Accion.DataPropertyName = "Accion"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.White
        Me.Accion.DefaultCellStyle = DataGridViewCellStyle3
        Me.Accion.HeaderText = "Accion"
        Me.Accion.Name = "Accion"
        Me.Accion.ReadOnly = True
        Me.Accion.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Accion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Accion.Width = 150
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Obs"
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn5.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridViewTextBoxColumn5.HeaderText = "Observaciones"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 350
        '
        'BUSCADetOrdSerBindingSource
        '
        Me.BUSCADetOrdSerBindingSource.DataMember = "BUSCADetOrdSer"
        Me.BUSCADetOrdSerBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'ValidacionTextBox
        '
        Me.ValidacionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Valida_DetOrdenBindingSource, "Validacion", True))
        Me.ValidacionTextBox.Location = New System.Drawing.Point(947, 692)
        Me.ValidacionTextBox.Name = "ValidacionTextBox"
        Me.ValidacionTextBox.Size = New System.Drawing.Size(33, 20)
        Me.ValidacionTextBox.TabIndex = 28
        Me.ValidacionTextBox.TabStop = False
        '
        'Valida_DetOrdenBindingSource
        '
        Me.Valida_DetOrdenBindingSource.DataMember = "Valida_DetOrden"
        Me.Valida_DetOrdenBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'FechaDateTimePicker
        '
        Me.FechaDateTimePicker.CustomFormat = "dd/MMM/yyyy"
        Me.FechaDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.Dame_fecha_hora_servBindingSource, "Fecha", True))
        Me.FechaDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FechaDateTimePicker.Location = New System.Drawing.Point(971, 693)
        Me.FechaDateTimePicker.Name = "FechaDateTimePicker"
        Me.FechaDateTimePicker.Size = New System.Drawing.Size(26, 20)
        Me.FechaDateTimePicker.TabIndex = 24
        Me.FechaDateTimePicker.TabStop = False
        Me.FechaDateTimePicker.Value = New Date(2007, 5, 30, 0, 0, 0, 0)
        '
        'Dame_fecha_hora_servBindingSource
        '
        Me.Dame_fecha_hora_servBindingSource.DataMember = "Dame_fecha_hora_serv"
        Me.Dame_fecha_hora_servBindingSource.DataSource = Me.DataSetarnoldo
        '
        'DameUltimo_dia_del_MesBindingSource
        '
        Me.DameUltimo_dia_del_MesBindingSource.DataMember = "DameUltimo_dia_del_Mes"
        Me.DameUltimo_dia_del_MesBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRATRABAJOSBindingSource
        '
        Me.MUESTRATRABAJOSBindingSource.DataMember = "MUESTRATRABAJOS"
        Me.MUESTRATRABAJOSBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CONDetOrdSerBindingSource
        '
        Me.CONDetOrdSerBindingSource.DataMember = "CONDetOrdSer"
        Me.CONDetOrdSerBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.SystemColors.Control
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(889, 689)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(128, 40)
        Me.Button5.TabIndex = 7
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'CONORDSERTableAdapter
        '
        Me.CONORDSERTableAdapter.ClearBeforeFill = True
        '
        'MUESTRATECNICOSTableAdapter
        '
        Me.MUESTRATECNICOSTableAdapter.ClearBeforeFill = True
        '
        'BUSCLIPORCONTRATOTableAdapter
        '
        Me.BUSCLIPORCONTRATOTableAdapter.ClearBeforeFill = True
        '
        'DameSerDELCliBindingSource
        '
        Me.DameSerDELCliBindingSource.DataMember = "dameSerDELCli"
        Me.DameSerDELCliBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'DameSerDELCliTableAdapter
        '
        Me.DameSerDELCliTableAdapter.ClearBeforeFill = True
        '
        'CONDetOrdSerTableAdapter
        '
        Me.CONDetOrdSerTableAdapter.ClearBeforeFill = True
        '
        'MUESTRATRABAJOSTableAdapter
        '
        Me.MUESTRATRABAJOSTableAdapter.ClearBeforeFill = True
        '
        'BUSCADetOrdSerTableAdapter
        '
        Me.BUSCADetOrdSerTableAdapter.ClearBeforeFill = True
        '
        'DameUltimo_dia_del_MesTableAdapter
        '
        Me.DameUltimo_dia_del_MesTableAdapter.ClearBeforeFill = True
        '
        'Valida_DetOrdenTableAdapter
        '
        Me.Valida_DetOrdenTableAdapter.ClearBeforeFill = True
        '
        'MuestraTipSerPrincipalTableAdapter
        '
        Me.MuestraTipSerPrincipalTableAdapter.ClearBeforeFill = True
        '
        'ClaveTextBox
        '
        Me.ClaveTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCADetOrdSerBindingSource, "Clave", True))
        Me.ClaveTextBox.Location = New System.Drawing.Point(951, 696)
        Me.ClaveTextBox.Name = "ClaveTextBox"
        Me.ClaveTextBox.Size = New System.Drawing.Size(46, 20)
        Me.ClaveTextBox.TabIndex = 29
        Me.ClaveTextBox.TabStop = False
        '
        'Clv_OrdenTextBox1
        '
        Me.Clv_OrdenTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCADetOrdSerBindingSource, "Clv_Orden", True))
        Me.Clv_OrdenTextBox1.Location = New System.Drawing.Point(951, 696)
        Me.Clv_OrdenTextBox1.Name = "Clv_OrdenTextBox1"
        Me.Clv_OrdenTextBox1.Size = New System.Drawing.Size(29, 20)
        Me.Clv_OrdenTextBox1.TabIndex = 30
        Me.Clv_OrdenTextBox1.TabStop = False
        '
        'Clv_TrabajoTextBox
        '
        Me.Clv_TrabajoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCADetOrdSerBindingSource, "Clv_Trabajo", True))
        Me.Clv_TrabajoTextBox.Location = New System.Drawing.Point(951, 692)
        Me.Clv_TrabajoTextBox.Name = "Clv_TrabajoTextBox"
        Me.Clv_TrabajoTextBox.Size = New System.Drawing.Size(40, 20)
        Me.Clv_TrabajoTextBox.TabIndex = 31
        Me.Clv_TrabajoTextBox.TabStop = False
        '
        'PREEJECUTAOrdSerBindingSource
        '
        Me.PREEJECUTAOrdSerBindingSource.DataMember = "PREEJECUTAOrdSer"
        Me.PREEJECUTAOrdSerBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'PREEJECUTAOrdSerTableAdapter
        '
        Me.PREEJECUTAOrdSerTableAdapter.ClearBeforeFill = True
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel4.Controls.Add(Me.BUSCADetOrdSerDataGridView)
        Me.Panel4.Controls.Add(Me.TextBox4)
        Me.Panel4.Location = New System.Drawing.Point(12, 497)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(13, 11)
        Me.Panel4.TabIndex = 32
        '
        'TextBox4
        '
        Me.TextBox4.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DimesigrabaordBindingSource, "Column1", True))
        Me.TextBox4.Location = New System.Drawing.Point(472, 180)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(111, 20)
        Me.TextBox4.TabIndex = 433
        Me.TextBox4.TabStop = False
        '
        'DimesigrabaordBindingSource
        '
        Me.DimesigrabaordBindingSource.DataMember = "dimesigrabaord"
        Me.DimesigrabaordBindingSource.DataSource = Me.DataSetLidia
        '
        'DataSetLidia
        '
        Me.DataSetLidia.DataSetName = "DataSetLidia"
        Me.DataSetLidia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Panel10
        '
        Me.Panel10.Controls.Add(Me.dgvDetOrdSer)
        Me.Panel10.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel10.Location = New System.Drawing.Point(12, 471)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(1005, 215)
        Me.Panel10.TabIndex = 434
        '
        'dgvDetOrdSer
        '
        Me.dgvDetOrdSer.AllowDrop = True
        Me.dgvDetOrdSer.AllowUserToAddRows = False
        Me.dgvDetOrdSer.AllowUserToDeleteRows = False
        Me.dgvDetOrdSer.AllowUserToOrderColumns = True
        Me.dgvDetOrdSer.AllowUserToResizeColumns = False
        Me.dgvDetOrdSer.AllowUserToResizeRows = False
        Me.dgvDetOrdSer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetOrdSer.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.clave, Me.clv_orden, Me.clv_trabajo, Me.trabajo, Me.descripciondet, Me.acciondet, Me.observacionesdet, Me.serealizadet, Me.seretiro})
        Me.dgvDetOrdSer.Location = New System.Drawing.Point(0, 0)
        Me.dgvDetOrdSer.Name = "dgvDetOrdSer"
        Me.dgvDetOrdSer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDetOrdSer.Size = New System.Drawing.Size(1004, 212)
        Me.dgvDetOrdSer.TabIndex = 434
        '
        'clave
        '
        Me.clave.DataPropertyName = "clave"
        Me.clave.HeaderText = "clave"
        Me.clave.Name = "clave"
        Me.clave.Visible = False
        '
        'clv_orden
        '
        Me.clv_orden.DataPropertyName = "clv_orden"
        Me.clv_orden.HeaderText = "clv_orden"
        Me.clv_orden.Name = "clv_orden"
        Me.clv_orden.Visible = False
        '
        'clv_trabajo
        '
        Me.clv_trabajo.DataPropertyName = "clv_trabajo"
        Me.clv_trabajo.HeaderText = "clv_trabajo"
        Me.clv_trabajo.Name = "clv_trabajo"
        Me.clv_trabajo.Visible = False
        '
        'trabajo
        '
        Me.trabajo.DataPropertyName = "trabajo"
        Me.trabajo.HeaderText = "trabajo"
        Me.trabajo.Name = "trabajo"
        Me.trabajo.Visible = False
        '
        'descripciondet
        '
        Me.descripciondet.DataPropertyName = "descripciondet"
        Me.descripciondet.HeaderText = "Descripción"
        Me.descripciondet.Name = "descripciondet"
        Me.descripciondet.Width = 330
        '
        'acciondet
        '
        Me.acciondet.DataPropertyName = "acciondet"
        Me.acciondet.HeaderText = "Acción"
        Me.acciondet.Name = "acciondet"
        Me.acciondet.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.acciondet.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.acciondet.ToolTipText = "Haz clic para realizar la acción"
        Me.acciondet.Width = 180
        '
        'observacionesdet
        '
        Me.observacionesdet.DataPropertyName = "observacionesdet"
        Me.observacionesdet.HeaderText = "Observaciones"
        Me.observacionesdet.Name = "observacionesdet"
        Me.observacionesdet.Width = 340
        '
        'serealizadet
        '
        Me.serealizadet.DataPropertyName = "serealizadet"
        Me.serealizadet.HeaderText = "serealizadet"
        Me.serealizadet.Name = "serealizadet"
        Me.serealizadet.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.serealizadet.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.serealizadet.Visible = False
        Me.serealizadet.Width = 50
        '
        'seretiro
        '
        Me.seretiro.DataPropertyName = "seretiro"
        Me.seretiro.HeaderText = "Se Retiró el Aparato"
        Me.seretiro.Name = "seretiro"
        Me.seretiro.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.seretiro.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.seretiro.Width = 110
        '
        'ValidaTrabajosBindingSource
        '
        Me.ValidaTrabajosBindingSource.DataMember = "ValidaTrabajos"
        Me.ValidaTrabajosBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'ValidaTrabajosTableAdapter
        '
        Me.ValidaTrabajosTableAdapter.ClearBeforeFill = True
        '
        'CONTADORTextBox
        '
        Me.CONTADORTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ValidaTrabajosBindingSource, "CONTADOR", True))
        Me.CONTADORTextBox.Location = New System.Drawing.Point(958, 692)
        Me.CONTADORTextBox.Name = "CONTADORTextBox"
        Me.CONTADORTextBox.Size = New System.Drawing.Size(54, 20)
        Me.CONTADORTextBox.TabIndex = 34
        Me.CONTADORTextBox.TabStop = False
        '
        'Timer1
        '
        Me.Timer1.Interval = 500
        '
        'ValidarNuevoTableAdapter
        '
        Me.ValidarNuevoTableAdapter.ClearBeforeFill = True
        '
        'Muestra_Tecnicos_AlmacenTableAdapter
        '
        Me.Muestra_Tecnicos_AlmacenTableAdapter.ClearBeforeFill = True
        '
        'DameClv_Session_TecnicosBindingSource
        '
        Me.DameClv_Session_TecnicosBindingSource.DataMember = "DameClv_Session_Tecnicos"
        Me.DameClv_Session_TecnicosBindingSource.DataSource = Me.DataSetarnoldo
        '
        'DameClv_Session_TecnicosTableAdapter
        '
        Me.DameClv_Session_TecnicosTableAdapter.ClearBeforeFill = True
        '
        'Dame_FolioBindingSource
        '
        Me.Dame_FolioBindingSource.DataMember = "Dame_Folio"
        Me.Dame_FolioBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Dame_FolioTableAdapter
        '
        Me.Dame_FolioTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Bitacora_tecBindingSource
        '
        Me.Inserta_Bitacora_tecBindingSource.DataMember = "Inserta_Bitacora_tec"
        Me.Inserta_Bitacora_tecBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Inserta_Bitacora_tecTableAdapter
        '
        Me.Inserta_Bitacora_tecTableAdapter.ClearBeforeFill = True
        '
        'Inserta_DetBitTecBindingSource
        '
        Me.Inserta_DetBitTecBindingSource.DataMember = "Inserta_DetBitTec"
        Me.Inserta_DetBitTecBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Inserta_DetBitTecTableAdapter
        '
        Me.Inserta_DetBitTecTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Rel_Bitacora_OrdenBindingSource
        '
        Me.Inserta_Rel_Bitacora_OrdenBindingSource.DataMember = "Inserta_Rel_Bitacora_Orden"
        Me.Inserta_Rel_Bitacora_OrdenBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Inserta_Rel_Bitacora_OrdenTableAdapter
        '
        Me.Inserta_Rel_Bitacora_OrdenTableAdapter.ClearBeforeFill = True
        '
        'Muestra_no_ordenTableAdapter
        '
        Me.Muestra_no_ordenTableAdapter.ClearBeforeFill = True
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel7.Controls.Add(Me.ComboBoxTap)
        Me.Panel7.Controls.Add(Me.LabelTab)
        Me.Panel7.Controls.Add(Me.TextBoxTab)
        Me.Panel7.Controls.Add(Me.Label4)
        Me.Panel7.Controls.Add(Me.PlacaTextBox)
        Me.Panel7.Controls.Add(Me.ObsTextBox)
        Me.Panel7.Controls.Add(ObsLabel)
        Me.Panel7.Location = New System.Drawing.Point(12, 363)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(1000, 43)
        Me.Panel7.TabIndex = 4
        Me.Panel7.TabStop = True
        '
        'ComboBoxTap
        '
        Me.ComboBoxTap.DisplayMember = "clave"
        Me.ComboBoxTap.FormattingEnabled = True
        Me.ComboBoxTap.Location = New System.Drawing.Point(652, 3)
        Me.ComboBoxTap.Name = "ComboBoxTap"
        Me.ComboBoxTap.Size = New System.Drawing.Size(140, 21)
        Me.ComboBoxTap.TabIndex = 32
        Me.ComboBoxTap.ValueMember = "consecutivo"
        Me.ComboBoxTap.Visible = False
        '
        'LabelTab
        '
        Me.LabelTab.AutoSize = True
        Me.LabelTab.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelTab.ForeColor = System.Drawing.Color.LightSlateGray
        Me.LabelTab.Location = New System.Drawing.Point(608, 4)
        Me.LabelTab.Name = "LabelTab"
        Me.LabelTab.Size = New System.Drawing.Size(39, 15)
        Me.LabelTab.TabIndex = 31
        Me.LabelTab.Text = "Tap :"
        Me.LabelTab.Visible = False
        '
        'TextBoxTab
        '
        Me.TextBoxTab.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TextBoxTab.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxTab.Location = New System.Drawing.Point(799, 3)
        Me.TextBoxTab.MaxLength = 50
        Me.TextBoxTab.Name = "TextBoxTab"
        Me.TextBoxTab.Size = New System.Drawing.Size(46, 21)
        Me.TextBoxTab.TabIndex = 30
        Me.TextBoxTab.Visible = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(851, 5)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 15)
        Me.Label4.TabIndex = 26
        Me.Label4.Text = "Placa :"
        Me.Label4.Visible = False
        '
        'PlacaTextBox
        '
        Me.PlacaTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PlacaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConRelCtePlacaBindingSource, "Placa", True))
        Me.PlacaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PlacaTextBox.Location = New System.Drawing.Point(908, 5)
        Me.PlacaTextBox.Name = "PlacaTextBox"
        Me.PlacaTextBox.Size = New System.Drawing.Size(76, 21)
        Me.PlacaTextBox.TabIndex = 25
        Me.PlacaTextBox.Visible = False
        '
        'ConRelCtePlacaBindingSource
        '
        Me.ConRelCtePlacaBindingSource.DataMember = "ConRelCtePlaca"
        Me.ConRelCtePlacaBindingSource.DataSource = Me.DataSetEric
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Panel8
        '
        Me.Panel8.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel8.Controls.Add(Me.Button3)
        Me.Panel8.Location = New System.Drawing.Point(519, 301)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(498, 61)
        Me.Panel8.TabIndex = 3
        Me.Panel8.TabStop = True
        '
        'DimeSiTieneunaBitacoraTableAdapter
        '
        Me.DimeSiTieneunaBitacoraTableAdapter.ClearBeforeFill = True
        '
        'Dame_Impresora_OrdenesBindingSource
        '
        Me.Dame_Impresora_OrdenesBindingSource.DataMember = "Dame_Impresora_Ordenes"
        Me.Dame_Impresora_OrdenesBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Dame_Impresora_OrdenesTableAdapter
        '
        Me.Dame_Impresora_OrdenesTableAdapter.ClearBeforeFill = True
        '
        'Dame_fecha_hora_servTableAdapter
        '
        Me.Dame_fecha_hora_servTableAdapter.ClearBeforeFill = True
        '
        'DimesigrabaordTableAdapter
        '
        Me.DimesigrabaordTableAdapter.ClearBeforeFill = True
        '
        'BuscaBloqueadoBindingSource
        '
        Me.BuscaBloqueadoBindingSource.DataMember = "BuscaBloqueado"
        Me.BuscaBloqueadoBindingSource.DataSource = Me.DataSetLidia
        '
        'BuscaBloqueadoTableAdapter
        '
        Me.BuscaBloqueadoTableAdapter.ClearBeforeFill = True
        '
        'Grabar_det_ordenBindingSource
        '
        Me.Grabar_det_ordenBindingSource.DataMember = "grabar_det_orden"
        Me.Grabar_det_ordenBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Grabar_det_ordenTableAdapter
        '
        Me.Grabar_det_ordenTableAdapter.ClearBeforeFill = True
        '
        'ValidaSiEsAcometidaBindingSource
        '
        Me.ValidaSiEsAcometidaBindingSource.DataMember = "ValidaSiEsAcometida"
        Me.ValidaSiEsAcometidaBindingSource.DataSource = Me.DataSetEric
        '
        'ValidaSiEsAcometidaTableAdapter
        '
        Me.ValidaSiEsAcometidaTableAdapter.ClearBeforeFill = True
        '
        'ConRelCtePlacaTableAdapter
        '
        Me.ConRelCtePlacaTableAdapter.ClearBeforeFill = True
        '
        'Inserta_RelCobraDescBindingSource
        '
        Me.Inserta_RelCobraDescBindingSource.DataMember = "Inserta_RelCobraDesc"
        Me.Inserta_RelCobraDescBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Inserta_RelCobraDescTableAdapter
        '
        Me.Inserta_RelCobraDescTableAdapter.ClearBeforeFill = True
        '
        'Imprime_OrdenBindingSource
        '
        Me.Imprime_OrdenBindingSource.DataMember = "Imprime_Orden"
        Me.Imprime_OrdenBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Imprime_OrdenTableAdapter
        '
        Me.Imprime_OrdenTableAdapter.ClearBeforeFill = True
        '
        'Cambia_Tipo_cablemodemBindingSource
        '
        Me.Cambia_Tipo_cablemodemBindingSource.DataMember = "Cambia_Tipo_cablemodem"
        Me.Cambia_Tipo_cablemodemBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Cambia_Tipo_cablemodemTableAdapter
        '
        Me.Cambia_Tipo_cablemodemTableAdapter.ClearBeforeFill = True
        '
        'Guarda_Comentario_CCABMBindingSource
        '
        Me.Guarda_Comentario_CCABMBindingSource.DataMember = "Guarda_Comentario_CCABM"
        Me.Guarda_Comentario_CCABMBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Guarda_Comentario_CCABMTableAdapter
        '
        Me.Guarda_Comentario_CCABMTableAdapter.ClearBeforeFill = True
        '
        'ChecaOrdSerRetiroBindingSource
        '
        Me.ChecaOrdSerRetiroBindingSource.DataMember = "ChecaOrdSerRetiro"
        Me.ChecaOrdSerRetiroBindingSource.DataSource = Me.DataSetEric
        '
        'ChecaOrdSerRetiroTableAdapter
        '
        Me.ChecaOrdSerRetiroTableAdapter.ClearBeforeFill = True
        '
        'BorraMotivoCanServBindingSource
        '
        Me.BorraMotivoCanServBindingSource.DataMember = "BorraMotivoCanServ"
        Me.BorraMotivoCanServBindingSource.DataSource = Me.DataSetEric
        '
        'BorraMotivoCanServTableAdapter
        '
        Me.BorraMotivoCanServTableAdapter.ClearBeforeFill = True
        '
        'ChecaMotivoCanServBindingSource
        '
        Me.ChecaMotivoCanServBindingSource.DataMember = "ChecaMotivoCanServ"
        Me.ChecaMotivoCanServBindingSource.DataSource = Me.DataSetEric
        '
        'ChecaMotivoCanServTableAdapter
        '
        Me.ChecaMotivoCanServTableAdapter.ClearBeforeFill = True
        '
        'InsertMotCanServBindingSource
        '
        Me.InsertMotCanServBindingSource.DataMember = "InsertMotCanServ"
        Me.InsertMotCanServBindingSource.DataSource = Me.DataSetEric
        '
        'InsertMotCanServTableAdapter
        '
        Me.InsertMotCanServTableAdapter.ClearBeforeFill = True
        '
        'NueRelOrdenUsuarioBindingSource
        '
        Me.NueRelOrdenUsuarioBindingSource.DataMember = "NueRelOrdenUsuario"
        Me.NueRelOrdenUsuarioBindingSource.DataSource = Me.DataSetEric
        '
        'NueRelOrdenUsuarioTableAdapter
        '
        Me.NueRelOrdenUsuarioTableAdapter.ClearBeforeFill = True
        '
        'Consulta_RelOrdenUsuarioTableAdapter
        '
        Me.Consulta_RelOrdenUsuarioTableAdapter.ClearBeforeFill = True
        '
        'btnGuardaRetiroParcial
        '
        Me.btnGuardaRetiroParcial.BackColor = System.Drawing.SystemColors.Control
        Me.btnGuardaRetiroParcial.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardaRetiroParcial.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardaRetiroParcial.ForeColor = System.Drawing.Color.Black
        Me.btnGuardaRetiroParcial.Location = New System.Drawing.Point(12, 692)
        Me.btnGuardaRetiroParcial.Name = "btnGuardaRetiroParcial"
        Me.btnGuardaRetiroParcial.Size = New System.Drawing.Size(190, 39)
        Me.btnGuardaRetiroParcial.TabIndex = 35
        Me.btnGuardaRetiroParcial.Text = "Guardar Retiro Parcial"
        Me.btnGuardaRetiroParcial.UseVisualStyleBackColor = False
        '
        'btnEjecutarRetiroParcial
        '
        Me.btnEjecutarRetiroParcial.BackColor = System.Drawing.SystemColors.Control
        Me.btnEjecutarRetiroParcial.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEjecutarRetiroParcial.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEjecutarRetiroParcial.ForeColor = System.Drawing.Color.Black
        Me.btnEjecutarRetiroParcial.Location = New System.Drawing.Point(221, 692)
        Me.btnEjecutarRetiroParcial.Name = "btnEjecutarRetiroParcial"
        Me.btnEjecutarRetiroParcial.Size = New System.Drawing.Size(190, 39)
        Me.btnEjecutarRetiroParcial.TabIndex = 435
        Me.btnEjecutarRetiroParcial.Text = "Ejecutar Retiro Parcial"
        Me.btnEjecutarRetiroParcial.UseVisualStyleBackColor = False
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter5
        '
        Me.Muestra_ServiciosDigitalesTableAdapter5.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter6
        '
        Me.Muestra_ServiciosDigitalesTableAdapter6.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter7
        '
        Me.Muestra_ServiciosDigitalesTableAdapter7.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter8
        '
        Me.Muestra_ServiciosDigitalesTableAdapter8.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter9
        '
        Me.Muestra_ServiciosDigitalesTableAdapter9.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter10
        '
        Me.Muestra_ServiciosDigitalesTableAdapter10.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter11
        '
        Me.Muestra_ServiciosDigitalesTableAdapter11.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter12
        '
        Me.Muestra_ServiciosDigitalesTableAdapter12.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter13
        '
        Me.Muestra_ServiciosDigitalesTableAdapter13.ClearBeforeFill = True
        '
        'FrmOrdSer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1028, 737)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnEjecutarRetiroParcial)
        Me.Controls.Add(Me.Panel10)
        Me.Controls.Add(CMBLabel3)
        Me.Controls.Add(Me.btnGuardaRetiroParcial)
        Me.Controls.Add(Me.SectorLabel)
        Me.Controls.Add(Me.Panel6)
        Me.Controls.Add(Me.SectorCombo)
        Me.Controls.Add(Me.Panel8)
        Me.Controls.Add(Me.Panel7)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.CONTADORTextBox)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Clv_TrabajoTextBox)
        Me.Controls.Add(Me.Clv_OrdenTextBox1)
        Me.Controls.Add(Me.ClaveTextBox)
        Me.Controls.Add(Me.ValidacionTextBox)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.FechaDateTimePicker)
        Me.Name = "FrmOrdSer"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Ordenes de Servicio"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel11.ResumeLayout(False)
        Me.Panel11.PerformLayout()
        CType(Me.Consulta_RelOrdenUsuarioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DimeSiTieneunaBitacoraBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Muestra_no_ordenBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.CONORDSERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTipSerPrincipalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.BUSCLIPORCONTRATOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRATECNICOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.CONORDSERBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONORDSERBindingNavigator.ResumeLayout(False)
        Me.CONORDSERBindingNavigator.PerformLayout()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        CType(Me.MuestraTecnicosAlmacenBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        CType(Me.MUESTRATECNICOSBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ValidarNuevoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BUSCADetOrdSerDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BUSCADetOrdSerBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Valida_DetOrdenBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_fecha_hora_servBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameUltimo_dia_del_MesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRATRABAJOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONDetOrdSerBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameSerDELCliBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PREEJECUTAOrdSerBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.DimesigrabaordBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel10.ResumeLayout(False)
        CType(Me.dgvDetOrdSer, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ValidaTrabajosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameClv_Session_TecnicosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_FolioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Bitacora_tecBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_DetBitTecBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Rel_Bitacora_OrdenBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        CType(Me.ConRelCtePlacaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel8.ResumeLayout(False)
        CType(Me.Dame_Impresora_OrdenesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BuscaBloqueadoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Grabar_det_ordenBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ValidaSiEsAcometidaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_RelCobraDescBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Imprime_OrdenBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Cambia_Tipo_cablemodemBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Guarda_Comentario_CCABMBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChecaOrdSerRetiroBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorraMotivoCanServBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChecaMotivoCanServBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertMotCanServBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NueRelOrdenUsuarioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONORDSERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONORDSERTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONORDSERTableAdapter
    Friend WithEvents CONORDSERBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONORDSERBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Clv_OrdenTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ContratoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_TecnicoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents IMPRESACheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents ObsTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CMBTextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton3 As System.Windows.Forms.RadioButton
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents MUESTRATECNICOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRATECNICOSTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRATECNICOSTableAdapter
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents ESHOTELCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents SOLOINTERNETCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents NOMBRELabel1 As System.Windows.Forms.Label
    Friend WithEvents CALLELabel1 As System.Windows.Forms.Label
    Friend WithEvents COLONIALabel1 As System.Windows.Forms.Label
    Friend WithEvents NUMEROLabel1 As System.Windows.Forms.Label
    Friend WithEvents CIUDADLabel1 As System.Windows.Forms.Label
    Friend WithEvents BUSCLIPORCONTRATOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BUSCLIPORCONTRATOTableAdapter As sofTV.NewSofTvDataSetTableAdapters.BUSCLIPORCONTRATOTableAdapter
    Friend WithEvents DameSerDELCliBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameSerDELCliTableAdapter As sofTV.NewSofTvDataSetTableAdapters.dameSerDELCliTableAdapter
    Friend WithEvents CONDetOrdSerBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONDetOrdSerTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONDetOrdSerTableAdapter
    Friend WithEvents MUESTRATRABAJOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRATRABAJOSTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRATRABAJOSTableAdapter
    Friend WithEvents BUSCADetOrdSerBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BUSCADetOrdSerTableAdapter As sofTV.NewSofTvDataSetTableAdapters.BUSCADetOrdSerTableAdapter
    Friend WithEvents DameUltimo_dia_del_MesTableAdapter As sofTV.NewSofTvDataSetTableAdapters.DameUltimo_dia_del_MesTableAdapter
    Friend WithEvents DameUltimo_dia_del_MesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Valida_DetOrdenBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Valida_DetOrdenTableAdapter As sofTV.NewSofTvDataSetTableAdapters.Valida_DetOrdenTableAdapter
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel7 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox5 As System.Windows.Forms.ComboBox
    Friend WithEvents MuestraTipSerPrincipalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipSerPrincipalTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MuestraTipSerPrincipalTableAdapter
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents BUSCADetOrdSerDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents FechaDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents ValidacionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ClaveTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_OrdenTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Clv_TrabajoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PREEJECUTAOrdSerBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PREEJECUTAOrdSerTableAdapter As sofTV.NewSofTvDataSetTableAdapters.PREEJECUTAOrdSerTableAdapter
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents MUESTRATECNICOSBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents ValidaTrabajosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ValidaTrabajosTableAdapter As sofTV.NewSofTvDataSetTableAdapters.ValidaTrabajosTableAdapter
    Friend WithEvents CONTADORTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents DataSetarnoldo As sofTV.DataSetarnoldo
    Friend WithEvents ValidarNuevoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ValidarNuevoTableAdapter As sofTV.DataSetarnoldoTableAdapters.ValidarNuevoTableAdapter
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents MuestraTecnicosAlmacenBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Tecnicos_AlmacenTableAdapter As sofTV.DataSetarnoldoTableAdapters.Muestra_Tecnicos_AlmacenTableAdapter
    Friend WithEvents DameClv_Session_TecnicosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameClv_Session_TecnicosTableAdapter As sofTV.DataSetarnoldoTableAdapters.DameClv_Session_TecnicosTableAdapter
    Friend WithEvents Dame_FolioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_FolioTableAdapter As sofTV.DataSetarnoldoTableAdapters.Dame_FolioTableAdapter
    Friend WithEvents Inserta_Bitacora_tecBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Bitacora_tecTableAdapter As sofTV.DataSetarnoldoTableAdapters.Inserta_Bitacora_tecTableAdapter
    Friend WithEvents Inserta_DetBitTecBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_DetBitTecTableAdapter As sofTV.DataSetarnoldoTableAdapters.Inserta_DetBitTecTableAdapter
    Friend WithEvents Inserta_Rel_Bitacora_OrdenBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Rel_Bitacora_OrdenTableAdapter As sofTV.DataSetarnoldoTableAdapters.Inserta_Rel_Bitacora_OrdenTableAdapter
    Friend WithEvents Muestra_no_ordenBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_no_ordenTableAdapter As sofTV.DataSetarnoldoTableAdapters.Muestra_no_ordenTableAdapter
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents DataSetEdgarRev2 As sofTV.DataSetEdgarRev2
    Friend WithEvents DimeSiTieneunaBitacoraBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DimeSiTieneunaBitacoraTableAdapter As sofTV.DataSetEdgarRev2TableAdapters.DimeSiTieneunaBitacoraTableAdapter
    Friend WithEvents FolioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Dame_Impresora_OrdenesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_Impresora_OrdenesTableAdapter As sofTV.DataSetarnoldoTableAdapters.Dame_Impresora_OrdenesTableAdapter
    Friend WithEvents Dame_fecha_hora_servBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_fecha_hora_servTableAdapter As sofTV.DataSetarnoldoTableAdapters.Dame_fecha_hora_servTableAdapter
    Friend WithEvents DataSetLidia As sofTV.DataSetLidia
    Friend WithEvents DimesigrabaordBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DimesigrabaordTableAdapter As sofTV.DataSetLidiaTableAdapters.dimesigrabaordTableAdapter
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents BuscaBloqueadoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BuscaBloqueadoTableAdapter As sofTV.DataSetLidiaTableAdapters.BuscaBloqueadoTableAdapter
    Friend WithEvents Grabar_det_ordenBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Grabar_det_ordenTableAdapter As sofTV.DataSetarnoldoTableAdapters.grabar_det_ordenTableAdapter
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents ValidaSiEsAcometidaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ValidaSiEsAcometidaTableAdapter As sofTV.DataSetEricTableAdapters.ValidaSiEsAcometidaTableAdapter
    Friend WithEvents ConRelCtePlacaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConRelCtePlacaTableAdapter As sofTV.DataSetEricTableAdapters.ConRelCtePlacaTableAdapter
    Friend WithEvents PlacaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents Inserta_RelCobraDescBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_RelCobraDescTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_RelCobraDescTableAdapter
    Friend WithEvents Imprime_OrdenBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Imprime_OrdenTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Imprime_OrdenTableAdapter
    Friend WithEvents Cambia_Tipo_cablemodemBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Cambia_Tipo_cablemodemTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Cambia_Tipo_cablemodemTableAdapter
    Friend WithEvents Guarda_Comentario_CCABMBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Guarda_Comentario_CCABMTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Guarda_Comentario_CCABMTableAdapter
    Friend WithEvents ChecaOrdSerRetiroBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ChecaOrdSerRetiroTableAdapter As sofTV.DataSetEricTableAdapters.ChecaOrdSerRetiroTableAdapter
    Friend WithEvents BorraMotivoCanServBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorraMotivoCanServTableAdapter As sofTV.DataSetEricTableAdapters.BorraMotivoCanServTableAdapter
    Friend WithEvents ChecaMotivoCanServBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ChecaMotivoCanServTableAdapter As sofTV.DataSetEricTableAdapters.ChecaMotivoCanServTableAdapter
    Friend WithEvents InsertMotCanServBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertMotCanServTableAdapter As sofTV.DataSetEricTableAdapters.InsertMotCanServTableAdapter
    Friend WithEvents NueRelOrdenUsuarioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NueRelOrdenUsuarioTableAdapter As sofTV.DataSetEricTableAdapters.NueRelOrdenUsuarioTableAdapter
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric2
    Friend WithEvents Consulta_RelOrdenUsuarioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_RelOrdenUsuarioTableAdapter As sofTV.DataSetEric2TableAdapters.Consulta_RelOrdenUsuarioTableAdapter
    Friend WithEvents GeneroLabel1 As System.Windows.Forms.Label
    Friend WithEvents EjecutoLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Tecnico As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxTap As System.Windows.Forms.ComboBox
    Friend WithEvents LabelTab As System.Windows.Forms.Label
    Friend WithEvents TextBoxTab As System.Windows.Forms.TextBox
    Friend WithEvents cbCuadrilla As System.Windows.Forms.ComboBox
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents dtpHoraVisita3 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpHoraVisita2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpHoraVisita1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Visita3TextBox As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Hora_sol As System.Windows.Forms.DateTimePicker
    Friend WithEvents Hora_Eje As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Fec_EjeTextBox As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Fec_EjeTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Visita2TextBox As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Visita1TextBox As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Fecha_SoliciutudMaskedTextBox As System.Windows.Forms.DateTimePicker
    Friend WithEvents StatusTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Visita2TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Visita1TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Visita3TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents SectorLabel As System.Windows.Forms.Label
    Friend WithEvents SectorCombo As System.Windows.Forms.ComboBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Accion As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvDetOrdSer As System.Windows.Forms.DataGridView
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents btnGuardaRetiroParcial As System.Windows.Forms.Button
    Friend WithEvents clave As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clv_orden As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clv_trabajo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents trabajo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents descripciondet As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents acciondet As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents observacionesdet As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents serealizadet As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents seretiro As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents btnEjecutarRetiroParcial As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Panel11 As System.Windows.Forms.Panel
    Friend WithEvents TxtRefOxxo As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter5 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter6 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter7 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter8 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter9 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter10 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter11 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter12 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter13 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
