﻿Public Class ClassDepartamentos
    Dim clase As New BaseIII

    Public Function uspConsultaTblDepartamentos(ByVal prmClvDepto As Long, ByVal prmDescripcion As String, ByVal prmOpcion As Integer) As DataTable
        clase.limpiaParametros()
        clase.CreateMyParameter("@clvDepto", SqlDbType.BigInt, prmClvDepto)
        clase.CreateMyParameter("@descripcion", SqlDbType.VarChar, prmDescripcion, 250)
        clase.CreateMyParameter("@op", SqlDbType.Int, prmOpcion)

        uspConsultaTblDepartamentos = clase.ConsultaDT("uspConsultaTblDepartamentos")
    End Function

    Public Function uspInsertaTblDepartamentos(ByVal prmDescripcion As String) As Long
        clase.limpiaParametros()
        clase.CreateMyParameter("@descripcion", SqlDbType.VarChar, prmDescripcion, 250)
        clase.CreateMyParameter("@clvDepto", ParameterDirection.Output, SqlDbType.BigInt)

        clase.ProcedimientoOutPut("uspInsertaTblDepartamentos")
        uspInsertaTblDepartamentos = CLng(clase.dicoPar("@clvDepto").ToString())
    End Function

    Public Sub uspModificaTblDepartamentos(ByVal prmClvDepto As Long, ByVal prmDescripcion As String)
        clase.limpiaParametros()
        clase.CreateMyParameter("@clvDepto", SqlDbType.BigInt, prmClvDepto)
        clase.CreateMyParameter("@descripcion", SqlDbType.VarChar, prmDescripcion, 250)

        clase.Inserta("uspModificaTblDepartamentos")
    End Sub

    Public Function uspEliminaTblDepartamentos(ByVal prmClvDepto As Long) As Boolean
        clase.limpiaParametros()
        clase.CreateMyParameter("@clvDepto", SqlDbType.BigInt, prmClvDepto)
        clase.CreateMyParameter("@bndEliminarDepto", ParameterDirection.Output, SqlDbType.Bit)

        clase.ProcedimientoOutPut("uspEliminaTblDepartamentos")
        uspEliminaTblDepartamentos = CBool(clase.dicoPar("@bndEliminarDepto").ToString)
    End Function
End Class
