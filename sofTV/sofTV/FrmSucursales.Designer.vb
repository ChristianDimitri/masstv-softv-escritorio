﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSucursales
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_SucursalLabel As System.Windows.Forms.Label
        Dim NombreLabel As System.Windows.Forms.Label
        Dim IPLabel As System.Windows.Forms.Label
        Dim ImpresoraLabel As System.Windows.Forms.Label
        Dim Clv_EquivalenteLabel As System.Windows.Forms.Label
        Dim SerieLabel As System.Windows.Forms.Label
        Dim UltimoFolioUsadoLabel As System.Windows.Forms.Label
        Dim Impresora_TarjetasLabel As System.Windows.Forms.Label
        Dim Impresora_ContratosLabel As System.Windows.Forms.Label
        Dim SerieLabel1 As System.Windows.Forms.Label
        Dim No_folioLabel As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmSucursales))
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.tabDatosSucursal = New System.Windows.Forms.TabControl()
        Me.tpgFiscal = New System.Windows.Forms.TabPage()
        Me.pnlDatosFiscales = New System.Windows.Forms.Panel()
        Me.dgvCompanias = New System.Windows.Forms.DataGridView()
        Me.colId = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCompania = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSerieTicket = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFolioUsado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFacturaFiscal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFacturaGlobal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.cmbFacturaGlobal = New System.Windows.Forms.ComboBox()
        Me.lblFacturaGlobal = New System.Windows.Forms.Label()
        Me.cmbFacturaFiscal = New System.Windows.Forms.ComboBox()
        Me.lblFacturaFiscal = New System.Windows.Forms.Label()
        Me.lblUltFolioUsado = New System.Windows.Forms.Label()
        Me.txtUltFolioUsado = New System.Windows.Forms.TextBox()
        Me.lblSerieTicket = New System.Windows.Forms.Label()
        Me.txtSerieTicket = New System.Windows.Forms.TextBox()
        Me.cmbCompanias = New System.Windows.Forms.ComboBox()
        Me.lblCompania = New System.Windows.Forms.Label()
        Me.tpgDatos = New System.Windows.Forms.TabPage()
        Me.pnlDatosGenerales = New System.Windows.Forms.Panel()
        Me.txtTelefono = New System.Windows.Forms.TextBox()
        Me.txtCiudad = New System.Windows.Forms.TextBox()
        Me.lblTelefono = New System.Windows.Forms.Label()
        Me.txtCP = New System.Windows.Forms.TextBox()
        Me.lblCiudad = New System.Windows.Forms.Label()
        Me.lblCalle = New System.Windows.Forms.Label()
        Me.txtMunicipio = New System.Windows.Forms.TextBox()
        Me.lblCp = New System.Windows.Forms.Label()
        Me.lblMunicipio = New System.Windows.Forms.Label()
        Me.txtCalle = New System.Windows.Forms.TextBox()
        Me.lblColonia = New System.Windows.Forms.Label()
        Me.txtColonia = New System.Windows.Forms.TextBox()
        Me.lblNumero = New System.Windows.Forms.Label()
        Me.txtNumero = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.ProcedimientosArnoldo2 = New Softv.ProcedimientosArnoldo2()
        Me.SerieTextBox1 = New System.Windows.Forms.TextBox()
        Me.Consulta_Generales_FacturasGlobalesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetarnoldo = New Softv.DataSetarnoldo()
        Me.No_folioTextBox = New System.Windows.Forms.TextBox()
        Me.Impresora_TarjetasTextBox = New System.Windows.Forms.TextBox()
        Me.ConsultaImpresoraSucursalBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Impresora_ContratosTextBox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CONSUCURSALESBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.CONSUCURSALESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New Softv.NewSofTvDataSet()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.CONSUCURSALESBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Clv_SucursalTextBox = New System.Windows.Forms.TextBox()
        Me.NombreTextBox = New System.Windows.Forms.TextBox()
        Me.IPTextBox = New System.Windows.Forms.TextBox()
        Me.ImpresoraTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_EquivalenteTextBox = New System.Windows.Forms.TextBox()
        Me.SerieTextBox = New System.Windows.Forms.TextBox()
        Me.UltimoFolioUsadoTextBox = New System.Windows.Forms.TextBox()
        Me.ConsultaImpresoraSucursalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Consulta_Impresora_SucursalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONSUCURSALESTableAdapter = New Softv.NewSofTvDataSetTableAdapters.CONSUCURSALESTableAdapter()
        Me.Consulta_Impresora_SucursalTableAdapter = New Softv.DataSetarnoldoTableAdapters.Consulta_Impresora_SucursalTableAdapter()
        Me.Inserta_impresora_sucursalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_impresora_sucursalTableAdapter = New Softv.DataSetarnoldoTableAdapters.inserta_impresora_sucursalTableAdapter()
        Me.Borra_Impresora_SucursalesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Borra_Impresora_SucursalesTableAdapter = New Softv.DataSetarnoldoTableAdapters.Borra_Impresora_SucursalesTableAdapter()
        Me.Consulta_Generales_FacturasGlobalesTableAdapter = New Softv.DataSetarnoldoTableAdapters.Consulta_Generales_FacturasGlobalesTableAdapter()
        Me.Inserta_Generales_FacturaGlobalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Generales_FacturaGlobalTableAdapter = New Softv.DataSetarnoldoTableAdapters.Inserta_Generales_FacturaGlobalTableAdapter()
        Me.Borra_Generales_FacturasGlobalesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Borra_Generales_FacturasGlobalesTableAdapter = New Softv.DataSetarnoldoTableAdapters.Borra_Generales_FacturasGlobalesTableAdapter()
        Me.Inserta_impresora_sucursalBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_impresora_sucursalTableAdapter1 = New Softv.ProcedimientosArnoldo2TableAdapters.inserta_impresora_sucursalTableAdapter()
        Me.Consulta_Impresora_SucursalTableAdapter1 = New Softv.ProcedimientosArnoldo2TableAdapters.Consulta_Impresora_SucursalTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New Softv.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New Softv.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New Softv.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New Softv.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter5 = New Softv.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter6 = New Softv.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter7 = New Softv.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Clv_SucursalLabel = New System.Windows.Forms.Label()
        NombreLabel = New System.Windows.Forms.Label()
        IPLabel = New System.Windows.Forms.Label()
        ImpresoraLabel = New System.Windows.Forms.Label()
        Clv_EquivalenteLabel = New System.Windows.Forms.Label()
        SerieLabel = New System.Windows.Forms.Label()
        UltimoFolioUsadoLabel = New System.Windows.Forms.Label()
        Impresora_TarjetasLabel = New System.Windows.Forms.Label()
        Impresora_ContratosLabel = New System.Windows.Forms.Label()
        SerieLabel1 = New System.Windows.Forms.Label()
        No_folioLabel = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.tabDatosSucursal.SuspendLayout()
        Me.tpgFiscal.SuspendLayout()
        Me.pnlDatosFiscales.SuspendLayout()
        CType(Me.dgvCompanias, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgDatos.SuspendLayout()
        Me.pnlDatosGenerales.SuspendLayout()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_Generales_FacturasGlobalesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsultaImpresoraSucursalBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONSUCURSALESBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONSUCURSALESBindingNavigator.SuspendLayout()
        CType(Me.CONSUCURSALESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsultaImpresoraSucursalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_Impresora_SucursalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_impresora_sucursalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Borra_Impresora_SucursalesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Generales_FacturaGlobalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Borra_Generales_FacturasGlobalesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_impresora_sucursalBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Clv_SucursalLabel
        '
        Clv_SucursalLabel.AutoSize = True
        Clv_SucursalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_SucursalLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_SucursalLabel.Location = New System.Drawing.Point(153, 46)
        Clv_SucursalLabel.Name = "Clv_SucursalLabel"
        Clv_SucursalLabel.Size = New System.Drawing.Size(50, 15)
        Clv_SucursalLabel.TabIndex = 0
        Clv_SucursalLabel.Text = "Clave :"
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.ForeColor = System.Drawing.Color.LightSlateGray
        NombreLabel.Location = New System.Drawing.Point(112, 73)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(91, 15)
        NombreLabel.TabIndex = 2
        NombreLabel.Text = "Descripción :"
        '
        'IPLabel
        '
        IPLabel.AutoSize = True
        IPLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        IPLabel.ForeColor = System.Drawing.Color.LightSlateGray
        IPLabel.Location = New System.Drawing.Point(179, 100)
        IPLabel.Name = "IPLabel"
        IPLabel.Size = New System.Drawing.Size(24, 15)
        IPLabel.TabIndex = 4
        IPLabel.Text = "IP:"
        '
        'ImpresoraLabel
        '
        ImpresoraLabel.AutoSize = True
        ImpresoraLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ImpresoraLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ImpresoraLabel.Location = New System.Drawing.Point(11, 154)
        ImpresoraLabel.Name = "ImpresoraLabel"
        ImpresoraLabel.Size = New System.Drawing.Size(192, 15)
        ImpresoraLabel.TabIndex = 6
        ImpresoraLabel.Text = "Impresora Facturas Fiscales:"
        '
        'Clv_EquivalenteLabel
        '
        Clv_EquivalenteLabel.AutoSize = True
        Clv_EquivalenteLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_EquivalenteLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_EquivalenteLabel.Location = New System.Drawing.Point(78, 127)
        Clv_EquivalenteLabel.Name = "Clv_EquivalenteLabel"
        Clv_EquivalenteLabel.Size = New System.Drawing.Size(125, 15)
        Clv_EquivalenteLabel.TabIndex = 8
        Clv_EquivalenteLabel.Text = "Clave Equivalente:"
        '
        'SerieLabel
        '
        SerieLabel.AutoSize = True
        SerieLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SerieLabel.ForeColor = System.Drawing.Color.LightSlateGray
        SerieLabel.Location = New System.Drawing.Point(655, 128)
        SerieLabel.Name = "SerieLabel"
        SerieLabel.Size = New System.Drawing.Size(45, 15)
        SerieLabel.TabIndex = 10
        SerieLabel.Text = "Serie:"
        SerieLabel.Visible = False
        '
        'UltimoFolioUsadoLabel
        '
        UltimoFolioUsadoLabel.AutoSize = True
        UltimoFolioUsadoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        UltimoFolioUsadoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        UltimoFolioUsadoLabel.Location = New System.Drawing.Point(569, 155)
        UltimoFolioUsadoLabel.Name = "UltimoFolioUsadoLabel"
        UltimoFolioUsadoLabel.Size = New System.Drawing.Size(134, 15)
        UltimoFolioUsadoLabel.TabIndex = 12
        UltimoFolioUsadoLabel.Text = "Ultimo Folio Usado:"
        UltimoFolioUsadoLabel.Visible = False
        '
        'Impresora_TarjetasLabel
        '
        Impresora_TarjetasLabel.AutoSize = True
        Impresora_TarjetasLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Impresora_TarjetasLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Impresora_TarjetasLabel.Location = New System.Drawing.Point(71, 181)
        Impresora_TarjetasLabel.Name = "Impresora_TarjetasLabel"
        Impresora_TarjetasLabel.Size = New System.Drawing.Size(132, 15)
        Impresora_TarjetasLabel.TabIndex = 22
        Impresora_TarjetasLabel.Text = "Impresora Tarjetas:"
        '
        'Impresora_ContratosLabel
        '
        Impresora_ContratosLabel.AutoSize = True
        Impresora_ContratosLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Impresora_ContratosLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Impresora_ContratosLabel.Location = New System.Drawing.Point(62, 208)
        Impresora_ContratosLabel.Name = "Impresora_ContratosLabel"
        Impresora_ContratosLabel.Size = New System.Drawing.Size(141, 15)
        Impresora_ContratosLabel.TabIndex = 24
        Impresora_ContratosLabel.Text = "Impresora Contratos:"
        '
        'SerieLabel1
        '
        SerieLabel1.AutoSize = True
        SerieLabel1.Cursor = System.Windows.Forms.Cursors.Default
        SerieLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        SerieLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        SerieLabel1.Location = New System.Drawing.Point(557, 70)
        SerieLabel1.Name = "SerieLabel1"
        SerieLabel1.Size = New System.Drawing.Size(143, 15)
        SerieLabel1.TabIndex = 25
        SerieLabel1.Text = "Serie Factura Global:"
        SerieLabel1.Visible = False
        '
        'No_folioLabel
        '
        No_folioLabel.AutoSize = True
        No_folioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        No_folioLabel.ForeColor = System.Drawing.Color.LightSlateGray
        No_folioLabel.Location = New System.Drawing.Point(537, 99)
        No_folioLabel.Name = "No_folioLabel"
        No_folioLabel.Size = New System.Drawing.Size(163, 15)
        No_folioLabel.TabIndex = 27
        No_folioLabel.Text = "No Folio Factura Global:"
        No_folioLabel.Visible = False
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(76, 236)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(125, 15)
        Label2.TabIndex = 29
        Label2.Text = "Impresora Tickets:"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(690, 598)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 11
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.tabDatosSucursal)
        Me.Panel1.Controls.Add(Label2)
        Me.Panel1.Controls.Add(Me.TextBox1)
        Me.Panel1.Controls.Add(SerieLabel1)
        Me.Panel1.Controls.Add(Me.SerieTextBox1)
        Me.Panel1.Controls.Add(No_folioLabel)
        Me.Panel1.Controls.Add(Me.No_folioTextBox)
        Me.Panel1.Controls.Add(Impresora_TarjetasLabel)
        Me.Panel1.Controls.Add(Me.Impresora_TarjetasTextBox)
        Me.Panel1.Controls.Add(Impresora_ContratosLabel)
        Me.Panel1.Controls.Add(Me.Impresora_ContratosTextBox)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.CONSUCURSALESBindingNavigator)
        Me.Panel1.Controls.Add(Clv_SucursalLabel)
        Me.Panel1.Controls.Add(Me.Clv_SucursalTextBox)
        Me.Panel1.Controls.Add(NombreLabel)
        Me.Panel1.Controls.Add(Me.NombreTextBox)
        Me.Panel1.Controls.Add(IPLabel)
        Me.Panel1.Controls.Add(Me.IPTextBox)
        Me.Panel1.Controls.Add(ImpresoraLabel)
        Me.Panel1.Controls.Add(Me.ImpresoraTextBox)
        Me.Panel1.Controls.Add(Clv_EquivalenteLabel)
        Me.Panel1.Controls.Add(Me.Clv_EquivalenteTextBox)
        Me.Panel1.Controls.Add(SerieLabel)
        Me.Panel1.Controls.Add(Me.SerieTextBox)
        Me.Panel1.Controls.Add(UltimoFolioUsadoLabel)
        Me.Panel1.Controls.Add(Me.UltimoFolioUsadoTextBox)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(825, 580)
        Me.Panel1.TabIndex = 19
        '
        'tabDatosSucursal
        '
        Me.tabDatosSucursal.Controls.Add(Me.tpgFiscal)
        Me.tabDatosSucursal.Controls.Add(Me.tpgDatos)
        Me.tabDatosSucursal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabDatosSucursal.Location = New System.Drawing.Point(3, 260)
        Me.tabDatosSucursal.Name = "tabDatosSucursal"
        Me.tabDatosSucursal.SelectedIndex = 0
        Me.tabDatosSucursal.Size = New System.Drawing.Size(811, 317)
        Me.tabDatosSucursal.TabIndex = 7
        '
        'tpgFiscal
        '
        Me.tpgFiscal.BackColor = System.Drawing.SystemColors.Control
        Me.tpgFiscal.Controls.Add(Me.pnlDatosFiscales)
        Me.tpgFiscal.Location = New System.Drawing.Point(4, 25)
        Me.tpgFiscal.Name = "tpgFiscal"
        Me.tpgFiscal.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgFiscal.Size = New System.Drawing.Size(803, 288)
        Me.tpgFiscal.TabIndex = 1
        Me.tpgFiscal.Text = "Datos Facturas y Tickets"
        '
        'pnlDatosFiscales
        '
        Me.pnlDatosFiscales.Controls.Add(Me.dgvCompanias)
        Me.pnlDatosFiscales.Controls.Add(Me.btnEliminar)
        Me.pnlDatosFiscales.Controls.Add(Me.btnAgregar)
        Me.pnlDatosFiscales.Controls.Add(Me.cmbFacturaGlobal)
        Me.pnlDatosFiscales.Controls.Add(Me.lblFacturaGlobal)
        Me.pnlDatosFiscales.Controls.Add(Me.cmbFacturaFiscal)
        Me.pnlDatosFiscales.Controls.Add(Me.lblFacturaFiscal)
        Me.pnlDatosFiscales.Controls.Add(Me.lblUltFolioUsado)
        Me.pnlDatosFiscales.Controls.Add(Me.txtUltFolioUsado)
        Me.pnlDatosFiscales.Controls.Add(Me.lblSerieTicket)
        Me.pnlDatosFiscales.Controls.Add(Me.txtSerieTicket)
        Me.pnlDatosFiscales.Controls.Add(Me.cmbCompanias)
        Me.pnlDatosFiscales.Controls.Add(Me.lblCompania)
        Me.pnlDatosFiscales.Location = New System.Drawing.Point(3, 6)
        Me.pnlDatosFiscales.Name = "pnlDatosFiscales"
        Me.pnlDatosFiscales.Size = New System.Drawing.Size(794, 279)
        Me.pnlDatosFiscales.TabIndex = 13
        '
        'dgvCompanias
        '
        Me.dgvCompanias.AllowUserToAddRows = False
        Me.dgvCompanias.AllowUserToDeleteRows = False
        Me.dgvCompanias.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgvCompanias.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCompanias.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colId, Me.colCompania, Me.colSerieTicket, Me.colFolioUsado, Me.colFacturaFiscal, Me.colFacturaGlobal})
        Me.dgvCompanias.Location = New System.Drawing.Point(4, 125)
        Me.dgvCompanias.Name = "dgvCompanias"
        Me.dgvCompanias.ReadOnly = True
        Me.dgvCompanias.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCompanias.Size = New System.Drawing.Size(787, 151)
        Me.dgvCompanias.TabIndex = 23
        '
        'colId
        '
        Me.colId.DataPropertyName = "id"
        Me.colId.HeaderText = "Id"
        Me.colId.Name = "colId"
        Me.colId.ReadOnly = True
        Me.colId.Visible = False
        Me.colId.Width = 50
        '
        'colCompania
        '
        Me.colCompania.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colCompania.DataPropertyName = "nombreCompania"
        Me.colCompania.HeaderText = "Compañía"
        Me.colCompania.Name = "colCompania"
        Me.colCompania.ReadOnly = True
        '
        'colSerieTicket
        '
        Me.colSerieTicket.DataPropertyName = "serieTicket"
        Me.colSerieTicket.HeaderText = "Serie Ticket"
        Me.colSerieTicket.Name = "colSerieTicket"
        Me.colSerieTicket.ReadOnly = True
        Me.colSerieTicket.Width = 60
        '
        'colFolioUsado
        '
        Me.colFolioUsado.DataPropertyName = "ultFolioUsado"
        Me.colFolioUsado.HeaderText = "Ult Folio Usado"
        Me.colFolioUsado.Name = "colFolioUsado"
        Me.colFolioUsado.ReadOnly = True
        Me.colFolioUsado.Width = 90
        '
        'colFacturaFiscal
        '
        Me.colFacturaFiscal.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colFacturaFiscal.DataPropertyName = "serieFacFiscal"
        Me.colFacturaFiscal.HeaderText = "Factura Fiscal"
        Me.colFacturaFiscal.Name = "colFacturaFiscal"
        Me.colFacturaFiscal.ReadOnly = True
        '
        'colFacturaGlobal
        '
        Me.colFacturaGlobal.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colFacturaGlobal.DataPropertyName = "serieFacGlobal"
        Me.colFacturaGlobal.HeaderText = "Factura Global"
        Me.colFacturaGlobal.Name = "colFacturaGlobal"
        Me.colFacturaGlobal.ReadOnly = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(662, 95)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(126, 24)
        Me.btnEliminar.TabIndex = 22
        Me.btnEliminar.Text = "&Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnAgregar
        '
        Me.btnAgregar.Location = New System.Drawing.Point(530, 95)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(126, 24)
        Me.btnAgregar.TabIndex = 21
        Me.btnAgregar.Text = "&Agregar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'cmbFacturaGlobal
        '
        Me.cmbFacturaGlobal.DisplayMember = "serieFolio"
        Me.cmbFacturaGlobal.FormattingEnabled = True
        Me.cmbFacturaGlobal.Location = New System.Drawing.Point(117, 91)
        Me.cmbFacturaGlobal.Name = "cmbFacturaGlobal"
        Me.cmbFacturaGlobal.Size = New System.Drawing.Size(384, 24)
        Me.cmbFacturaGlobal.TabIndex = 16
        Me.cmbFacturaGlobal.ValueMember = "clvSerie"
        '
        'lblFacturaGlobal
        '
        Me.lblFacturaGlobal.AutoSize = True
        Me.lblFacturaGlobal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFacturaGlobal.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblFacturaGlobal.Location = New System.Drawing.Point(8, 94)
        Me.lblFacturaGlobal.Name = "lblFacturaGlobal"
        Me.lblFacturaGlobal.Size = New System.Drawing.Size(109, 15)
        Me.lblFacturaGlobal.TabIndex = 20
        Me.lblFacturaGlobal.Text = "Factura Global :"
        '
        'cmbFacturaFiscal
        '
        Me.cmbFacturaFiscal.DisplayMember = "serieFolio"
        Me.cmbFacturaFiscal.FormattingEnabled = True
        Me.cmbFacturaFiscal.Location = New System.Drawing.Point(117, 61)
        Me.cmbFacturaFiscal.Name = "cmbFacturaFiscal"
        Me.cmbFacturaFiscal.Size = New System.Drawing.Size(384, 24)
        Me.cmbFacturaFiscal.TabIndex = 15
        Me.cmbFacturaFiscal.ValueMember = "clvSerie"
        '
        'lblFacturaFiscal
        '
        Me.lblFacturaFiscal.AutoSize = True
        Me.lblFacturaFiscal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFacturaFiscal.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblFacturaFiscal.Location = New System.Drawing.Point(12, 64)
        Me.lblFacturaFiscal.Name = "lblFacturaFiscal"
        Me.lblFacturaFiscal.Size = New System.Drawing.Size(105, 15)
        Me.lblFacturaFiscal.TabIndex = 19
        Me.lblFacturaFiscal.Text = "Factura Fiscal :"
        '
        'lblUltFolioUsado
        '
        Me.lblUltFolioUsado.AutoSize = True
        Me.lblUltFolioUsado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUltFolioUsado.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblUltFolioUsado.Location = New System.Drawing.Point(243, 35)
        Me.lblUltFolioUsado.Name = "lblUltFolioUsado"
        Me.lblUltFolioUsado.Size = New System.Drawing.Size(138, 15)
        Me.lblUltFolioUsado.TabIndex = 18
        Me.lblUltFolioUsado.Text = "Último Folio Usado :"
        '
        'txtUltFolioUsado
        '
        Me.txtUltFolioUsado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtUltFolioUsado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUltFolioUsado.Location = New System.Drawing.Point(384, 32)
        Me.txtUltFolioUsado.Name = "txtUltFolioUsado"
        Me.txtUltFolioUsado.Size = New System.Drawing.Size(116, 21)
        Me.txtUltFolioUsado.TabIndex = 14
        '
        'lblSerieTicket
        '
        Me.lblSerieTicket.AutoSize = True
        Me.lblSerieTicket.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSerieTicket.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblSerieTicket.Location = New System.Drawing.Point(26, 35)
        Me.lblSerieTicket.Name = "lblSerieTicket"
        Me.lblSerieTicket.Size = New System.Drawing.Size(91, 15)
        Me.lblSerieTicket.TabIndex = 17
        Me.lblSerieTicket.Text = "Serie Ticket :"
        '
        'txtSerieTicket
        '
        Me.txtSerieTicket.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSerieTicket.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSerieTicket.Location = New System.Drawing.Point(118, 33)
        Me.txtSerieTicket.Name = "txtSerieTicket"
        Me.txtSerieTicket.Size = New System.Drawing.Size(116, 21)
        Me.txtSerieTicket.TabIndex = 12
        '
        'cmbCompanias
        '
        Me.cmbCompanias.DisplayMember = "nombreCompania"
        Me.cmbCompanias.FormattingEnabled = True
        Me.cmbCompanias.Location = New System.Drawing.Point(117, 3)
        Me.cmbCompanias.Name = "cmbCompanias"
        Me.cmbCompanias.Size = New System.Drawing.Size(384, 24)
        Me.cmbCompanias.TabIndex = 11
        Me.cmbCompanias.ValueMember = "clvCompania"
        '
        'lblCompania
        '
        Me.lblCompania.AutoSize = True
        Me.lblCompania.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompania.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblCompania.Location = New System.Drawing.Point(37, 3)
        Me.lblCompania.Name = "lblCompania"
        Me.lblCompania.Size = New System.Drawing.Size(80, 15)
        Me.lblCompania.TabIndex = 13
        Me.lblCompania.Text = "Compañía :"
        '
        'tpgDatos
        '
        Me.tpgDatos.BackColor = System.Drawing.SystemColors.Control
        Me.tpgDatos.Controls.Add(Me.pnlDatosGenerales)
        Me.tpgDatos.Location = New System.Drawing.Point(4, 25)
        Me.tpgDatos.Name = "tpgDatos"
        Me.tpgDatos.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgDatos.Size = New System.Drawing.Size(803, 288)
        Me.tpgDatos.TabIndex = 0
        Me.tpgDatos.Text = "Dirección Sucursal"
        '
        'pnlDatosGenerales
        '
        Me.pnlDatosGenerales.Controls.Add(Me.txtTelefono)
        Me.pnlDatosGenerales.Controls.Add(Me.txtCiudad)
        Me.pnlDatosGenerales.Controls.Add(Me.lblTelefono)
        Me.pnlDatosGenerales.Controls.Add(Me.txtCP)
        Me.pnlDatosGenerales.Controls.Add(Me.lblCiudad)
        Me.pnlDatosGenerales.Controls.Add(Me.lblCalle)
        Me.pnlDatosGenerales.Controls.Add(Me.txtMunicipio)
        Me.pnlDatosGenerales.Controls.Add(Me.lblCp)
        Me.pnlDatosGenerales.Controls.Add(Me.lblMunicipio)
        Me.pnlDatosGenerales.Controls.Add(Me.txtCalle)
        Me.pnlDatosGenerales.Controls.Add(Me.lblColonia)
        Me.pnlDatosGenerales.Controls.Add(Me.txtColonia)
        Me.pnlDatosGenerales.Controls.Add(Me.lblNumero)
        Me.pnlDatosGenerales.Controls.Add(Me.txtNumero)
        Me.pnlDatosGenerales.Location = New System.Drawing.Point(6, 6)
        Me.pnlDatosGenerales.Name = "pnlDatosGenerales"
        Me.pnlDatosGenerales.Size = New System.Drawing.Size(791, 279)
        Me.pnlDatosGenerales.TabIndex = 13
        '
        'txtTelefono
        '
        Me.txtTelefono.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTelefono.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTelefono.Location = New System.Drawing.Point(390, 115)
        Me.txtTelefono.Name = "txtTelefono"
        Me.txtTelefono.Size = New System.Drawing.Size(313, 21)
        Me.txtTelefono.TabIndex = 21
        '
        'txtCiudad
        '
        Me.txtCiudad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCiudad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCiudad.Location = New System.Drawing.Point(11, 157)
        Me.txtCiudad.Name = "txtCiudad"
        Me.txtCiudad.Size = New System.Drawing.Size(364, 21)
        Me.txtCiudad.TabIndex = 23
        '
        'lblTelefono
        '
        Me.lblTelefono.AutoSize = True
        Me.lblTelefono.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTelefono.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblTelefono.Location = New System.Drawing.Point(394, 97)
        Me.lblTelefono.Name = "lblTelefono"
        Me.lblTelefono.Size = New System.Drawing.Size(92, 15)
        Me.lblTelefono.TabIndex = 22
        Me.lblTelefono.Text = "Teléfono (s) :"
        '
        'txtCP
        '
        Me.txtCP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCP.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCP.Location = New System.Drawing.Point(587, 73)
        Me.txtCP.Name = "txtCP"
        Me.txtCP.Size = New System.Drawing.Size(116, 21)
        Me.txtCP.TabIndex = 18
        '
        'lblCiudad
        '
        Me.lblCiudad.AutoSize = True
        Me.lblCiudad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCiudad.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblCiudad.Location = New System.Drawing.Point(14, 139)
        Me.lblCiudad.Name = "lblCiudad"
        Me.lblCiudad.Size = New System.Drawing.Size(60, 15)
        Me.lblCiudad.TabIndex = 25
        Me.lblCiudad.Text = "Ciudad :"
        '
        'lblCalle
        '
        Me.lblCalle.AutoSize = True
        Me.lblCalle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCalle.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblCalle.Location = New System.Drawing.Point(15, 13)
        Me.lblCalle.Name = "lblCalle"
        Me.lblCalle.Size = New System.Drawing.Size(48, 15)
        Me.lblCalle.TabIndex = 13
        Me.lblCalle.Text = "Calle :"
        '
        'txtMunicipio
        '
        Me.txtMunicipio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtMunicipio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMunicipio.Location = New System.Drawing.Point(11, 115)
        Me.txtMunicipio.Name = "txtMunicipio"
        Me.txtMunicipio.Size = New System.Drawing.Size(364, 21)
        Me.txtMunicipio.TabIndex = 20
        '
        'lblCp
        '
        Me.lblCp.AutoSize = True
        Me.lblCp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCp.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblCp.Location = New System.Drawing.Point(587, 55)
        Me.lblCp.Name = "lblCp"
        Me.lblCp.Size = New System.Drawing.Size(41, 15)
        Me.lblCp.TabIndex = 26
        Me.lblCp.Text = "C.P. :"
        '
        'lblMunicipio
        '
        Me.lblMunicipio.AutoSize = True
        Me.lblMunicipio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMunicipio.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblMunicipio.Location = New System.Drawing.Point(14, 97)
        Me.lblMunicipio.Name = "lblMunicipio"
        Me.lblMunicipio.Size = New System.Drawing.Size(78, 15)
        Me.lblMunicipio.TabIndex = 24
        Me.lblMunicipio.Text = "Municipio :"
        '
        'txtCalle
        '
        Me.txtCalle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCalle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCalle.Location = New System.Drawing.Point(11, 31)
        Me.txtCalle.Name = "txtCalle"
        Me.txtCalle.Size = New System.Drawing.Size(559, 21)
        Me.txtCalle.TabIndex = 14
        '
        'lblColonia
        '
        Me.lblColonia.AutoSize = True
        Me.lblColonia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblColonia.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblColonia.Location = New System.Drawing.Point(14, 55)
        Me.lblColonia.Name = "lblColonia"
        Me.lblColonia.Size = New System.Drawing.Size(64, 15)
        Me.lblColonia.TabIndex = 19
        Me.lblColonia.Text = "Colonia :"
        '
        'txtColonia
        '
        Me.txtColonia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtColonia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtColonia.Location = New System.Drawing.Point(11, 73)
        Me.txtColonia.Name = "txtColonia"
        Me.txtColonia.Size = New System.Drawing.Size(559, 21)
        Me.txtColonia.TabIndex = 17
        '
        'lblNumero
        '
        Me.lblNumero.AutoSize = True
        Me.lblNumero.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNumero.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblNumero.Location = New System.Drawing.Point(591, 13)
        Me.lblNumero.Name = "lblNumero"
        Me.lblNumero.Size = New System.Drawing.Size(37, 15)
        Me.lblNumero.TabIndex = 16
        Me.lblNumero.Text = "No. :"
        '
        'txtNumero
        '
        Me.txtNumero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumero.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumero.Location = New System.Drawing.Point(587, 31)
        Me.txtNumero.Name = "txtNumero"
        Me.txtNumero.Size = New System.Drawing.Size(116, 21)
        Me.txtNumero.TabIndex = 15
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProcedimientosArnoldo2, "Consulta_Impresora_Sucursal.Impresora_Tickets", True))
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.TextBox1.Location = New System.Drawing.Point(209, 233)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(321, 21)
        Me.TextBox1.TabIndex = 6
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SerieTextBox1
        '
        Me.SerieTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SerieTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_Generales_FacturasGlobalesBindingSource, "serie", True))
        Me.SerieTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.SerieTextBox1.Location = New System.Drawing.Point(706, 68)
        Me.SerieTextBox1.Name = "SerieTextBox1"
        Me.SerieTextBox1.Size = New System.Drawing.Size(100, 21)
        Me.SerieTextBox1.TabIndex = 8
        Me.SerieTextBox1.Visible = False
        '
        'Consulta_Generales_FacturasGlobalesBindingSource
        '
        Me.Consulta_Generales_FacturasGlobalesBindingSource.DataMember = "Consulta_Generales_FacturasGlobales"
        Me.Consulta_Generales_FacturasGlobalesBindingSource.DataSource = Me.DataSetarnoldo
        '
        'DataSetarnoldo
        '
        Me.DataSetarnoldo.DataSetName = "DataSetarnoldo"
        Me.DataSetarnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'No_folioTextBox
        '
        Me.No_folioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.No_folioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_Generales_FacturasGlobalesBindingSource, "no_folio", True))
        Me.No_folioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.No_folioTextBox.Location = New System.Drawing.Point(706, 94)
        Me.No_folioTextBox.Name = "No_folioTextBox"
        Me.No_folioTextBox.Size = New System.Drawing.Size(100, 21)
        Me.No_folioTextBox.TabIndex = 9
        Me.No_folioTextBox.Visible = False
        '
        'Impresora_TarjetasTextBox
        '
        Me.Impresora_TarjetasTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Impresora_TarjetasTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConsultaImpresoraSucursalBindingSource1, "Impresora_Tarjetas", True))
        Me.Impresora_TarjetasTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Impresora_TarjetasTextBox.Location = New System.Drawing.Point(209, 178)
        Me.Impresora_TarjetasTextBox.Name = "Impresora_TarjetasTextBox"
        Me.Impresora_TarjetasTextBox.Size = New System.Drawing.Size(321, 21)
        Me.Impresora_TarjetasTextBox.TabIndex = 4
        '
        'ConsultaImpresoraSucursalBindingSource1
        '
        Me.ConsultaImpresoraSucursalBindingSource1.DataMember = "Consulta_Impresora_Sucursal"
        Me.ConsultaImpresoraSucursalBindingSource1.DataSource = Me.ProcedimientosArnoldo2
        '
        'Impresora_ContratosTextBox
        '
        Me.Impresora_ContratosTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Impresora_ContratosTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProcedimientosArnoldo2, "Consulta_Impresora_Sucursal.Impresora_Contratos", True))
        Me.Impresora_ContratosTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Impresora_ContratosTextBox.Location = New System.Drawing.Point(209, 205)
        Me.Impresora_ContratosTextBox.Name = "Impresora_ContratosTextBox"
        Me.Impresora_ContratosTextBox.Size = New System.Drawing.Size(321, 21)
        Me.Impresora_ContratosTextBox.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(351, 99)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(179, 13)
        Me.Label1.TabIndex = 22
        Me.Label1.Text = " Nota : Ejemplo 192.168.(""  60 "") .25"
        '
        'CONSUCURSALESBindingNavigator
        '
        Me.CONSUCURSALESBindingNavigator.AddNewItem = Nothing
        Me.CONSUCURSALESBindingNavigator.BindingSource = Me.CONSUCURSALESBindingSource
        Me.CONSUCURSALESBindingNavigator.CountItem = Nothing
        Me.CONSUCURSALESBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONSUCURSALESBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.BindingNavigatorDeleteItem, Me.CONSUCURSALESBindingNavigatorSaveItem})
        Me.CONSUCURSALESBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONSUCURSALESBindingNavigator.MoveFirstItem = Nothing
        Me.CONSUCURSALESBindingNavigator.MoveLastItem = Nothing
        Me.CONSUCURSALESBindingNavigator.MoveNextItem = Nothing
        Me.CONSUCURSALESBindingNavigator.MovePreviousItem = Nothing
        Me.CONSUCURSALESBindingNavigator.Name = "CONSUCURSALESBindingNavigator"
        Me.CONSUCURSALESBindingNavigator.PositionItem = Nothing
        Me.CONSUCURSALESBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONSUCURSALESBindingNavigator.Size = New System.Drawing.Size(825, 25)
        Me.CONSUCURSALESBindingNavigator.TabIndex = 10
        Me.CONSUCURSALESBindingNavigator.TabStop = True
        Me.CONSUCURSALESBindingNavigator.Text = "BindingNavigator1"
        '
        'CONSUCURSALESBindingSource
        '
        Me.CONSUCURSALESBindingSource.DataMember = "CONSUCURSALES"
        Me.CONSUCURSALESBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(90, 22)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(79, 22)
        Me.ToolStripButton1.Text = "&CANCELAR"
        '
        'CONSUCURSALESBindingNavigatorSaveItem
        '
        Me.CONSUCURSALESBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONSUCURSALESBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONSUCURSALESBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONSUCURSALESBindingNavigatorSaveItem.Name = "CONSUCURSALESBindingNavigatorSaveItem"
        Me.CONSUCURSALESBindingNavigatorSaveItem.Size = New System.Drawing.Size(91, 22)
        Me.CONSUCURSALESBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'Clv_SucursalTextBox
        '
        Me.Clv_SucursalTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_SucursalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSUCURSALESBindingSource, "Clv_Sucursal", True))
        Me.Clv_SucursalTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_SucursalTextBox.Location = New System.Drawing.Point(209, 43)
        Me.Clv_SucursalTextBox.Name = "Clv_SucursalTextBox"
        Me.Clv_SucursalTextBox.ReadOnly = True
        Me.Clv_SucursalTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Clv_SucursalTextBox.TabIndex = 10
        Me.Clv_SucursalTextBox.TabStop = False
        '
        'NombreTextBox
        '
        Me.NombreTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NombreTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSUCURSALESBindingSource, "Nombre", True))
        Me.NombreTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreTextBox.Location = New System.Drawing.Point(209, 70)
        Me.NombreTextBox.Name = "NombreTextBox"
        Me.NombreTextBox.Size = New System.Drawing.Size(321, 21)
        Me.NombreTextBox.TabIndex = 0
        '
        'IPTextBox
        '
        Me.IPTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.IPTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.IPTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSUCURSALESBindingSource, "IP", True))
        Me.IPTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IPTextBox.Location = New System.Drawing.Point(209, 97)
        Me.IPTextBox.Name = "IPTextBox"
        Me.IPTextBox.Size = New System.Drawing.Size(136, 21)
        Me.IPTextBox.TabIndex = 1
        '
        'ImpresoraTextBox
        '
        Me.ImpresoraTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ImpresoraTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ImpresoraTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSUCURSALESBindingSource, "Impresora", True))
        Me.ImpresoraTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ImpresoraTextBox.Location = New System.Drawing.Point(209, 151)
        Me.ImpresoraTextBox.Name = "ImpresoraTextBox"
        Me.ImpresoraTextBox.Size = New System.Drawing.Size(321, 21)
        Me.ImpresoraTextBox.TabIndex = 3
        '
        'Clv_EquivalenteTextBox
        '
        Me.Clv_EquivalenteTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_EquivalenteTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Clv_EquivalenteTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSUCURSALESBindingSource, "Clv_Equivalente", True))
        Me.Clv_EquivalenteTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_EquivalenteTextBox.Location = New System.Drawing.Point(209, 124)
        Me.Clv_EquivalenteTextBox.Name = "Clv_EquivalenteTextBox"
        Me.Clv_EquivalenteTextBox.Size = New System.Drawing.Size(136, 21)
        Me.Clv_EquivalenteTextBox.TabIndex = 2
        '
        'SerieTextBox
        '
        Me.SerieTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SerieTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.SerieTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSUCURSALESBindingSource, "Serie", True))
        Me.SerieTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SerieTextBox.Location = New System.Drawing.Point(706, 126)
        Me.SerieTextBox.Name = "SerieTextBox"
        Me.SerieTextBox.Size = New System.Drawing.Size(100, 21)
        Me.SerieTextBox.TabIndex = 4
        Me.SerieTextBox.Visible = False
        '
        'UltimoFolioUsadoTextBox
        '
        Me.UltimoFolioUsadoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.UltimoFolioUsadoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSUCURSALESBindingSource, "UltimoFolioUsado", True))
        Me.UltimoFolioUsadoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UltimoFolioUsadoTextBox.Location = New System.Drawing.Point(706, 153)
        Me.UltimoFolioUsadoTextBox.Name = "UltimoFolioUsadoTextBox"
        Me.UltimoFolioUsadoTextBox.Size = New System.Drawing.Size(100, 21)
        Me.UltimoFolioUsadoTextBox.TabIndex = 5
        Me.UltimoFolioUsadoTextBox.Visible = False
        '
        'ConsultaImpresoraSucursalBindingSource
        '
        Me.ConsultaImpresoraSucursalBindingSource.DataMember = "Consulta_Impresora_Sucursal"
        Me.ConsultaImpresoraSucursalBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Consulta_Impresora_SucursalBindingSource
        '
        Me.Consulta_Impresora_SucursalBindingSource.DataMember = "Consulta_Impresora_Sucursal"
        Me.Consulta_Impresora_SucursalBindingSource.DataSource = Me.DataSetarnoldo
        '
        'CONSUCURSALESTableAdapter
        '
        Me.CONSUCURSALESTableAdapter.ClearBeforeFill = True
        '
        'Consulta_Impresora_SucursalTableAdapter
        '
        Me.Consulta_Impresora_SucursalTableAdapter.ClearBeforeFill = True
        '
        'Inserta_impresora_sucursalBindingSource
        '
        Me.Inserta_impresora_sucursalBindingSource.DataMember = "inserta_impresora_sucursal"
        Me.Inserta_impresora_sucursalBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Inserta_impresora_sucursalTableAdapter
        '
        Me.Inserta_impresora_sucursalTableAdapter.ClearBeforeFill = True
        '
        'Borra_Impresora_SucursalesBindingSource
        '
        Me.Borra_Impresora_SucursalesBindingSource.DataMember = "Borra_Impresora_Sucursales"
        Me.Borra_Impresora_SucursalesBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Borra_Impresora_SucursalesTableAdapter
        '
        Me.Borra_Impresora_SucursalesTableAdapter.ClearBeforeFill = True
        '
        'Consulta_Generales_FacturasGlobalesTableAdapter
        '
        Me.Consulta_Generales_FacturasGlobalesTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Generales_FacturaGlobalBindingSource
        '
        Me.Inserta_Generales_FacturaGlobalBindingSource.DataMember = "Inserta_Generales_FacturaGlobal"
        Me.Inserta_Generales_FacturaGlobalBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Inserta_Generales_FacturaGlobalTableAdapter
        '
        Me.Inserta_Generales_FacturaGlobalTableAdapter.ClearBeforeFill = True
        '
        'Borra_Generales_FacturasGlobalesBindingSource
        '
        Me.Borra_Generales_FacturasGlobalesBindingSource.DataMember = "Borra_Generales_FacturasGlobales"
        Me.Borra_Generales_FacturasGlobalesBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Borra_Generales_FacturasGlobalesTableAdapter
        '
        Me.Borra_Generales_FacturasGlobalesTableAdapter.ClearBeforeFill = True
        '
        'Inserta_impresora_sucursalBindingSource1
        '
        Me.Inserta_impresora_sucursalBindingSource1.DataMember = "inserta_impresora_sucursal"
        Me.Inserta_impresora_sucursalBindingSource1.DataSource = Me.ProcedimientosArnoldo2
        '
        'Inserta_impresora_sucursalTableAdapter1
        '
        Me.Inserta_impresora_sucursalTableAdapter1.ClearBeforeFill = True
        '
        'Consulta_Impresora_SucursalTableAdapter1
        '
        Me.Consulta_Impresora_SucursalTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter5
        '
        Me.Muestra_ServiciosDigitalesTableAdapter5.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter6
        '
        Me.Muestra_ServiciosDigitalesTableAdapter6.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter7
        '
        Me.Muestra_ServiciosDigitalesTableAdapter7.ClearBeforeFill = True
        '
        'FrmSucursales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(841, 639)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Button5)
        Me.Name = "FrmSucursales"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Sucursales"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.tabDatosSucursal.ResumeLayout(False)
        Me.tpgFiscal.ResumeLayout(False)
        Me.pnlDatosFiscales.ResumeLayout(False)
        Me.pnlDatosFiscales.PerformLayout()
        CType(Me.dgvCompanias, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgDatos.ResumeLayout(False)
        Me.pnlDatosGenerales.ResumeLayout(False)
        Me.pnlDatosGenerales.PerformLayout()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_Generales_FacturasGlobalesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsultaImpresoraSucursalBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONSUCURSALESBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONSUCURSALESBindingNavigator.ResumeLayout(False)
        Me.CONSUCURSALESBindingNavigator.PerformLayout()
        CType(Me.CONSUCURSALESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsultaImpresoraSucursalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_Impresora_SucursalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_impresora_sucursalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Borra_Impresora_SucursalesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Generales_FacturaGlobalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Borra_Generales_FacturasGlobalesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_impresora_sucursalBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONSUCURSALESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONSUCURSALESTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONSUCURSALESTableAdapter
    Friend WithEvents Clv_SucursalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NombreTextBox As System.Windows.Forms.TextBox
    Friend WithEvents IPTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ImpresoraTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_EquivalenteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SerieTextBox As System.Windows.Forms.TextBox
    Friend WithEvents UltimoFolioUsadoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CONSUCURSALESBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONSUCURSALESBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DataSetarnoldo As sofTV.DataSetarnoldo
    Friend WithEvents Consulta_Impresora_SucursalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_Impresora_SucursalTableAdapter As sofTV.DataSetarnoldoTableAdapters.Consulta_Impresora_SucursalTableAdapter
    Friend WithEvents Impresora_TarjetasTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Impresora_ContratosTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Inserta_impresora_sucursalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_impresora_sucursalTableAdapter As sofTV.DataSetarnoldoTableAdapters.inserta_impresora_sucursalTableAdapter
    Friend WithEvents Borra_Impresora_SucursalesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Borra_Impresora_SucursalesTableAdapter As sofTV.DataSetarnoldoTableAdapters.Borra_Impresora_SucursalesTableAdapter
    Friend WithEvents Consulta_Generales_FacturasGlobalesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_Generales_FacturasGlobalesTableAdapter As sofTV.DataSetarnoldoTableAdapters.Consulta_Generales_FacturasGlobalesTableAdapter
    Friend WithEvents SerieTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents No_folioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Inserta_Generales_FacturaGlobalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Generales_FacturaGlobalTableAdapter As sofTV.DataSetarnoldoTableAdapters.Inserta_Generales_FacturaGlobalTableAdapter
    Friend WithEvents Borra_Generales_FacturasGlobalesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Borra_Generales_FacturasGlobalesTableAdapter As sofTV.DataSetarnoldoTableAdapters.Borra_Generales_FacturasGlobalesTableAdapter
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents Inserta_impresora_sucursalBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_impresora_sucursalTableAdapter1 As sofTV.ProcedimientosArnoldo2TableAdapters.inserta_impresora_sucursalTableAdapter
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents ConsultaImpresoraSucursalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_Impresora_SucursalTableAdapter1 As sofTV.ProcedimientosArnoldo2TableAdapters.Consulta_Impresora_SucursalTableAdapter
    Friend WithEvents ConsultaImpresoraSucursalBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents tabDatosSucursal As System.Windows.Forms.TabControl
    Friend WithEvents tpgDatos As System.Windows.Forms.TabPage
    Friend WithEvents tpgFiscal As System.Windows.Forms.TabPage
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As Softv.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As Softv.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter5 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter6 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents pnlDatosFiscales As System.Windows.Forms.Panel
    Friend WithEvents dgvCompanias As System.Windows.Forms.DataGridView
    Friend WithEvents colId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCompania As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colSerieTicket As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colFolioUsado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colFacturaFiscal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colFacturaGlobal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents cmbFacturaGlobal As System.Windows.Forms.ComboBox
    Friend WithEvents lblFacturaGlobal As System.Windows.Forms.Label
    Friend WithEvents cmbFacturaFiscal As System.Windows.Forms.ComboBox
    Friend WithEvents lblFacturaFiscal As System.Windows.Forms.Label
    Friend WithEvents lblUltFolioUsado As System.Windows.Forms.Label
    Friend WithEvents txtUltFolioUsado As System.Windows.Forms.TextBox
    Friend WithEvents lblSerieTicket As System.Windows.Forms.Label
    Friend WithEvents txtSerieTicket As System.Windows.Forms.TextBox
    Friend WithEvents cmbCompanias As System.Windows.Forms.ComboBox
    Friend WithEvents lblCompania As System.Windows.Forms.Label
    Friend WithEvents pnlDatosGenerales As System.Windows.Forms.Panel
    Friend WithEvents txtTelefono As System.Windows.Forms.TextBox
    Friend WithEvents txtCiudad As System.Windows.Forms.TextBox
    Friend WithEvents lblTelefono As System.Windows.Forms.Label
    Friend WithEvents txtCP As System.Windows.Forms.TextBox
    Friend WithEvents lblCiudad As System.Windows.Forms.Label
    Friend WithEvents lblCalle As System.Windows.Forms.Label
    Friend WithEvents txtMunicipio As System.Windows.Forms.TextBox
    Friend WithEvents lblCp As System.Windows.Forms.Label
    Friend WithEvents lblMunicipio As System.Windows.Forms.Label
    Friend WithEvents txtCalle As System.Windows.Forms.TextBox
    Friend WithEvents lblColonia As System.Windows.Forms.Label
    Friend WithEvents txtColonia As System.Windows.Forms.TextBox
    Friend WithEvents lblNumero As System.Windows.Forms.Label
    Friend WithEvents txtNumero As System.Windows.Forms.TextBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter7 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
