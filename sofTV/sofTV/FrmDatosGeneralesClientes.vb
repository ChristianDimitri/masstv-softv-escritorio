﻿Imports System.IO
Imports System.Text
Imports System.Collections
Imports System.Collections.Generic
Imports System.Data.SqlClient
Public Class FrmDatosGeneralesClientes
    Public Shared Contrato As Integer = 0

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Public Sub CargaRelTrabajosRetiroAparato()

        Dim I As Integer = 0
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder

        Try

            Dim cmd As New SqlCommand("CargaDatosGeneralesCliente", conexion)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@Contrato", SqlDbType.Int).Value = Contrato

            Dim dr As SqlDataReader
            conexion.Open()
            dr = cmd.ExecuteReader()


            Using dr
                While dr.Read
                    txtContrato.Text = dr("CONTRATO").ToString().Trim()
                    txtNombre.Text = dr("NombreCliente").ToString().Trim()
                    cmbxCalle.Text = dr("CALLE").ToString().Trim()
                    txtNumExt.Text = dr("NUMERO").ToString().Trim()
                    txtEntreCalles.Text = dr("ENTRECALLES").ToString().Trim()
                    cmbxColonia.Text = dr("COLONIA").ToString().Trim()
                    txtCP.Text = dr("CodigoPostal").ToString().Trim()
                    txtTelCasa.Text = dr("TELEFONO").ToString().Trim()
                    txtTelCel.Text = dr("CELULAR").ToString().Trim()
                    cmbxSector.Text = dr("Sector").ToString().Trim()
                    cmbxTipoCliente.Text = dr("TipoCliente").ToString().Trim()
                    cmbxSubTipoCliente.Text = dr("SubTipoCliente").ToString().Trim()
                    cmbxCiudad.Text = dr("CIUDAD").ToString().Trim()
                    txtEmail.Text = dr("Email").ToString().Trim()
                    chbxCasaPropia.Checked = dr("CasaPropia").ToString().Trim()
                    txtIFE.Text = dr("IFE").ToString().Trim()
                    txtTelTrabajo.Text = dr("TelefonoTrabajo").ToString().Trim()
                    txtApellidoPaterno.Text = dr("Apellido_Paterno").ToString().Trim()
                    txtApellidoMaterno.Text = dr("Apellido_Materno").ToString().Trim()
                    Referencia1Text.Text = dr("Referencia1").ToString().Trim()
                    Referencia2Text.Text = dr("Referencia2").ToString().Trim()
                End While
                dr.Close()

            End Using

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

   
    Private Sub FrmDatosGeneralesClientes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        CargaRelTrabajosRetiroAparato()
    End Sub
End Class