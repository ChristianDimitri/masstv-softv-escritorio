﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDatosGeneralesClientes
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Label11 As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim LblNumInt As System.Windows.Forms.Label
        Dim Label10 As System.Windows.Forms.Label
        Dim Label8 As System.Windows.Forms.Label
        Dim CONTRATOLabel As System.Windows.Forms.Label
        Dim CALLELabel As System.Windows.Forms.Label
        Dim Label51 As System.Windows.Forms.Label
        Dim NUMEROLabel As System.Windows.Forms.Label
        Dim ENTRECALLESLabel As System.Windows.Forms.Label
        Dim COLONIALabel As System.Windows.Forms.Label
        Dim Label50 As System.Windows.Forms.Label
        Dim CodigoPostalLabel As System.Windows.Forms.Label
        Dim CIUDADLabel As System.Windows.Forms.Label
        Dim Label52 As System.Windows.Forms.Label
        Dim CELULARLabel As System.Windows.Forms.Label
        Dim TELEFONOLabel As System.Windows.Forms.Label
        Dim EmailLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmbxSubTipoCliente = New System.Windows.Forms.ComboBox()
        Me.cmbxTipoCliente = New System.Windows.Forms.ComboBox()
        Me.txtTelTrabajo = New System.Windows.Forms.TextBox()
        Me.Referencia2Text = New System.Windows.Forms.TextBox()
        Me.Referencia1Text = New System.Windows.Forms.TextBox()
        Me.txtNumInt = New System.Windows.Forms.TextBox()
        Me.cmbxCalle = New System.Windows.Forms.ComboBox()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.cmbxSector = New System.Windows.Forms.ComboBox()
        Me.chbxCasaPropia = New System.Windows.Forms.CheckBox()
        Me.txtIFE = New System.Windows.Forms.TextBox()
        Me.cmbxColonia = New System.Windows.Forms.ComboBox()
        Me.cmbxCiudad = New System.Windows.Forms.ComboBox()
        Me.txtApellidoPaterno = New System.Windows.Forms.TextBox()
        Me.txtNumExt = New System.Windows.Forms.TextBox()
        Me.txtEntreCalles = New System.Windows.Forms.TextBox()
        Me.txtApellidoMaterno = New System.Windows.Forms.TextBox()
        Me.txtCP = New System.Windows.Forms.TextBox()
        Me.txtTelCel = New System.Windows.Forms.TextBox()
        Me.txtTelCasa = New System.Windows.Forms.TextBox()
        Me.txtContrato = New System.Windows.Forms.TextBox()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Label11 = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        Label4 = New System.Windows.Forms.Label()
        LblNumInt = New System.Windows.Forms.Label()
        Label10 = New System.Windows.Forms.Label()
        Label8 = New System.Windows.Forms.Label()
        CONTRATOLabel = New System.Windows.Forms.Label()
        CALLELabel = New System.Windows.Forms.Label()
        Label51 = New System.Windows.Forms.Label()
        NUMEROLabel = New System.Windows.Forms.Label()
        ENTRECALLESLabel = New System.Windows.Forms.Label()
        COLONIALabel = New System.Windows.Forms.Label()
        Label50 = New System.Windows.Forms.Label()
        CodigoPostalLabel = New System.Windows.Forms.Label()
        CIUDADLabel = New System.Windows.Forms.Label()
        Label52 = New System.Windows.Forms.Label()
        CELULARLabel = New System.Windows.Forms.Label()
        TELEFONOLabel = New System.Windows.Forms.Label()
        EmailLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label11
        '
        Label11.AutoSize = True
        Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label11.ForeColor = System.Drawing.Color.LightSlateGray
        Label11.Location = New System.Drawing.Point(63, 272)
        Label11.Name = "Label11"
        Label11.Size = New System.Drawing.Size(48, 15)
        Label11.TabIndex = 136
        Label11.Text = "Sector"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label5.ForeColor = System.Drawing.Color.LightSlateGray
        Label5.Location = New System.Drawing.Point(663, 271)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(80, 13)
        Label5.TabIndex = 130
        Label5.Text = "Referencia 2"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Label4.Location = New System.Drawing.Point(663, 233)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(80, 13)
        Label4.TabIndex = 128
        Label4.Text = "Referencia 1"
        '
        'LblNumInt
        '
        LblNumInt.AutoSize = True
        LblNumInt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        LblNumInt.ForeColor = System.Drawing.Color.LightSlateGray
        LblNumInt.Location = New System.Drawing.Point(737, 104)
        LblNumInt.Name = "LblNumInt"
        LblNumInt.Size = New System.Drawing.Size(82, 15)
        LblNumInt.TabIndex = 126
        LblNumInt.Text = "Numero Int."
        '
        'Label10
        '
        Label10.AutoSize = True
        Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label10.ForeColor = System.Drawing.Color.LightSlateGray
        Label10.Location = New System.Drawing.Point(438, 270)
        Label10.Name = "Label10"
        Label10.Size = New System.Drawing.Size(28, 15)
        Label10.TabIndex = 123
        Label10.Text = "IFE"
        '
        'Label8
        '
        Label8.AutoSize = True
        Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label8.ForeColor = System.Drawing.Color.LightSlateGray
        Label8.Location = New System.Drawing.Point(704, 155)
        Label8.Name = "Label8"
        Label8.Size = New System.Drawing.Size(84, 15)
        Label8.TabIndex = 132
        Label8.Text = "Tel. Trabajo"
        '
        'CONTRATOLabel
        '
        CONTRATOLabel.AutoSize = True
        CONTRATOLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CONTRATOLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CONTRATOLabel.Location = New System.Drawing.Point(6, 50)
        CONTRATOLabel.Name = "CONTRATOLabel"
        CONTRATOLabel.Size = New System.Drawing.Size(61, 15)
        CONTRATOLabel.TabIndex = 125
        CONTRATOLabel.Text = "Contrato"
        '
        'CALLELabel
        '
        CALLELabel.AutoSize = True
        CALLELabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CALLELabel.ForeColor = System.Drawing.Color.LightSlateGray
        CALLELabel.Location = New System.Drawing.Point(6, 100)
        CALLELabel.Name = "CALLELabel"
        CALLELabel.Size = New System.Drawing.Size(44, 15)
        CALLELabel.TabIndex = 111
        CALLELabel.Text = "Calle "
        '
        'Label51
        '
        Label51.AutoSize = True
        Label51.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label51.ForeColor = System.Drawing.Color.LightSlateGray
        Label51.Location = New System.Drawing.Point(432, 50)
        Label51.Name = "Label51"
        Label51.Size = New System.Drawing.Size(116, 15)
        Label51.TabIndex = 93
        Label51.Text = "Apellido Materno"
        '
        'NUMEROLabel
        '
        NUMEROLabel.AutoSize = True
        NUMEROLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NUMEROLabel.ForeColor = System.Drawing.Color.LightSlateGray
        NUMEROLabel.Location = New System.Drawing.Point(517, 102)
        NUMEROLabel.Name = "NUMEROLabel"
        NUMEROLabel.Size = New System.Drawing.Size(86, 15)
        NUMEROLabel.TabIndex = 112
        NUMEROLabel.Text = "Numero Ext."
        '
        'ENTRECALLESLabel
        '
        ENTRECALLESLabel.AutoSize = True
        ENTRECALLESLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ENTRECALLESLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ENTRECALLESLabel.Location = New System.Drawing.Point(122, 155)
        ENTRECALLESLabel.Name = "ENTRECALLESLabel"
        ENTRECALLESLabel.Size = New System.Drawing.Size(83, 15)
        ENTRECALLESLabel.TabIndex = 113
        ENTRECALLESLabel.Text = "Entrecalles "
        '
        'COLONIALabel
        '
        COLONIALabel.AutoSize = True
        COLONIALabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        COLONIALabel.ForeColor = System.Drawing.Color.LightSlateGray
        COLONIALabel.Location = New System.Drawing.Point(135, 192)
        COLONIALabel.Name = "COLONIALabel"
        COLONIALabel.Size = New System.Drawing.Size(56, 15)
        COLONIALabel.TabIndex = 114
        COLONIALabel.Text = "Colonia"
        '
        'Label50
        '
        Label50.AutoSize = True
        Label50.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label50.ForeColor = System.Drawing.Color.LightSlateGray
        Label50.Location = New System.Drawing.Point(138, 50)
        Label50.Name = "Label50"
        Label50.Size = New System.Drawing.Size(113, 15)
        Label50.TabIndex = 95
        Label50.Text = "Apellido Paterno"
        '
        'CodigoPostalLabel
        '
        CodigoPostalLabel.AutoSize = True
        CodigoPostalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CodigoPostalLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CodigoPostalLabel.Location = New System.Drawing.Point(322, 155)
        CodigoPostalLabel.Name = "CodigoPostalLabel"
        CodigoPostalLabel.Size = New System.Drawing.Size(96, 15)
        CodigoPostalLabel.TabIndex = 115
        CodigoPostalLabel.Text = "Código Postal"
        '
        'CIUDADLabel
        '
        CIUDADLabel.AutoSize = True
        CIUDADLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CIUDADLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CIUDADLabel.Location = New System.Drawing.Point(72, 232)
        CIUDADLabel.Name = "CIUDADLabel"
        CIUDADLabel.Size = New System.Drawing.Size(173, 15)
        CIUDADLabel.TabIndex = 118
        CIUDADLabel.Text = "Ciudad/Region/Municipio "
        '
        'Label52
        '
        Label52.AutoSize = True
        Label52.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label52.ForeColor = System.Drawing.Color.LightSlateGray
        Label52.Location = New System.Drawing.Point(694, 50)
        Label52.Name = "Label52"
        Label52.Size = New System.Drawing.Size(58, 15)
        Label52.TabIndex = 94
        Label52.Text = "Nombre"
        '
        'CELULARLabel
        '
        CELULARLabel.AutoSize = True
        CELULARLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CELULARLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CELULARLabel.Location = New System.Drawing.Point(578, 155)
        CELULARLabel.Name = "CELULARLabel"
        CELULARLabel.Size = New System.Drawing.Size(81, 15)
        CELULARLabel.TabIndex = 117
        CELULARLabel.Text = "Tel. Celular"
        '
        'TELEFONOLabel
        '
        TELEFONOLabel.AutoSize = True
        TELEFONOLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TELEFONOLabel.ForeColor = System.Drawing.Color.LightSlateGray
        TELEFONOLabel.Location = New System.Drawing.Point(451, 155)
        TELEFONOLabel.Name = "TELEFONOLabel"
        TELEFONOLabel.Size = New System.Drawing.Size(67, 15)
        TELEFONOLabel.TabIndex = 116
        TELEFONOLabel.Text = "Tel. Casa"
        '
        'EmailLabel
        '
        EmailLabel.AutoSize = True
        EmailLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        EmailLabel.ForeColor = System.Drawing.Color.LightSlateGray
        EmailLabel.Location = New System.Drawing.Point(681, 194)
        EmailLabel.Name = "EmailLabel"
        EmailLabel.Size = New System.Drawing.Size(37, 13)
        EmailLabel.TabIndex = 120
        EmailLabel.Text = "Email"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(405, 195)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(84, 15)
        Label1.TabIndex = 137
        Label1.Text = "Tipo Cliente"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(424, 233)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(56, 15)
        Label2.TabIndex = 138
        Label2.Text = "Subtipo"
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.DarkOrange
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.ForeColor = System.Drawing.Color.Black
        Me.btnSalir.Location = New System.Drawing.Point(775, 357)
        Me.btnSalir.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(116, 43)
        Me.btnSalir.TabIndex = 79
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmbxSubTipoCliente)
        Me.GroupBox1.Controls.Add(Me.cmbxTipoCliente)
        Me.GroupBox1.Controls.Add(Label1)
        Me.GroupBox1.Controls.Add(Label2)
        Me.GroupBox1.Controls.Add(Label11)
        Me.GroupBox1.Controls.Add(Me.txtTelTrabajo)
        Me.GroupBox1.Controls.Add(Label5)
        Me.GroupBox1.Controls.Add(Me.Referencia2Text)
        Me.GroupBox1.Controls.Add(Label4)
        Me.GroupBox1.Controls.Add(Me.Referencia1Text)
        Me.GroupBox1.Controls.Add(Me.txtNumInt)
        Me.GroupBox1.Controls.Add(LblNumInt)
        Me.GroupBox1.Controls.Add(Me.cmbxCalle)
        Me.GroupBox1.Controls.Add(Me.txtEmail)
        Me.GroupBox1.Controls.Add(Me.cmbxSector)
        Me.GroupBox1.Controls.Add(Me.chbxCasaPropia)
        Me.GroupBox1.Controls.Add(Label10)
        Me.GroupBox1.Controls.Add(Me.txtIFE)
        Me.GroupBox1.Controls.Add(Label8)
        Me.GroupBox1.Controls.Add(Me.cmbxColonia)
        Me.GroupBox1.Controls.Add(Me.cmbxCiudad)
        Me.GroupBox1.Controls.Add(CONTRATOLabel)
        Me.GroupBox1.Controls.Add(CALLELabel)
        Me.GroupBox1.Controls.Add(Label51)
        Me.GroupBox1.Controls.Add(Me.txtApellidoPaterno)
        Me.GroupBox1.Controls.Add(NUMEROLabel)
        Me.GroupBox1.Controls.Add(Me.txtNumExt)
        Me.GroupBox1.Controls.Add(ENTRECALLESLabel)
        Me.GroupBox1.Controls.Add(Me.txtEntreCalles)
        Me.GroupBox1.Controls.Add(COLONIALabel)
        Me.GroupBox1.Controls.Add(Me.txtApellidoMaterno)
        Me.GroupBox1.Controls.Add(Label50)
        Me.GroupBox1.Controls.Add(CodigoPostalLabel)
        Me.GroupBox1.Controls.Add(Me.txtCP)
        Me.GroupBox1.Controls.Add(Me.txtTelCel)
        Me.GroupBox1.Controls.Add(Me.txtTelCasa)
        Me.GroupBox1.Controls.Add(CIUDADLabel)
        Me.GroupBox1.Controls.Add(Label52)
        Me.GroupBox1.Controls.Add(CELULARLabel)
        Me.GroupBox1.Controls.Add(TELEFONOLabel)
        Me.GroupBox1.Controls.Add(EmailLabel)
        Me.GroupBox1.Controls.Add(Me.txtContrato)
        Me.GroupBox1.Controls.Add(Me.txtNombre)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(879, 338)
        Me.GroupBox1.TabIndex = 80
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos Generales del Cliente"
        '
        'cmbxSubTipoCliente
        '
        Me.cmbxSubTipoCliente.BackColor = System.Drawing.Color.White
        Me.cmbxSubTipoCliente.DisplayMember = "Descripcion"
        Me.cmbxSubTipoCliente.Enabled = False
        Me.cmbxSubTipoCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmbxSubTipoCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbxSubTipoCliente.FormattingEnabled = True
        Me.cmbxSubTipoCliente.Location = New System.Drawing.Point(320, 249)
        Me.cmbxSubTipoCliente.Name = "cmbxSubTipoCliente"
        Me.cmbxSubTipoCliente.Size = New System.Drawing.Size(271, 24)
        Me.cmbxSubTipoCliente.TabIndex = 140
        Me.cmbxSubTipoCliente.ValueMember = "Clv_SubTipoCliente"
        '
        'cmbxTipoCliente
        '
        Me.cmbxTipoCliente.BackColor = System.Drawing.Color.White
        Me.cmbxTipoCliente.DisplayMember = "Descripcion"
        Me.cmbxTipoCliente.Enabled = False
        Me.cmbxTipoCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmbxTipoCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbxTipoCliente.FormattingEnabled = True
        Me.cmbxTipoCliente.Location = New System.Drawing.Point(320, 213)
        Me.cmbxTipoCliente.Name = "cmbxTipoCliente"
        Me.cmbxTipoCliente.Size = New System.Drawing.Size(272, 24)
        Me.cmbxTipoCliente.TabIndex = 139
        Me.cmbxTipoCliente.ValueMember = "Clv_TipoCliente"
        '
        'txtTelTrabajo
        '
        Me.txtTelTrabajo.BackColor = System.Drawing.Color.White
        Me.txtTelTrabajo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTelTrabajo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTelTrabajo.Enabled = False
        Me.txtTelTrabajo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.txtTelTrabajo.Location = New System.Drawing.Point(712, 172)
        Me.txtTelTrabajo.MaxLength = 50
        Me.txtTelTrabajo.Name = "txtTelTrabajo"
        Me.txtTelTrabajo.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.txtTelTrabajo.Size = New System.Drawing.Size(153, 20)
        Me.txtTelTrabajo.TabIndex = 131
        Me.txtTelTrabajo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Referencia2Text
        '
        Me.Referencia2Text.BackColor = System.Drawing.Color.White
        Me.Referencia2Text.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Referencia2Text.Enabled = False
        Me.Referencia2Text.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Referencia2Text.Location = New System.Drawing.Point(597, 286)
        Me.Referencia2Text.Name = "Referencia2Text"
        Me.Referencia2Text.Size = New System.Drawing.Size(268, 20)
        Me.Referencia2Text.TabIndex = 129
        '
        'Referencia1Text
        '
        Me.Referencia1Text.BackColor = System.Drawing.Color.White
        Me.Referencia1Text.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Referencia1Text.Enabled = False
        Me.Referencia1Text.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Referencia1Text.Location = New System.Drawing.Point(597, 248)
        Me.Referencia1Text.Name = "Referencia1Text"
        Me.Referencia1Text.Size = New System.Drawing.Size(268, 20)
        Me.Referencia1Text.TabIndex = 127
        '
        'txtNumInt
        '
        Me.txtNumInt.BackColor = System.Drawing.Color.White
        Me.txtNumInt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumInt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumInt.Enabled = False
        Me.txtNumInt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.txtNumInt.Location = New System.Drawing.Point(700, 120)
        Me.txtNumInt.MaxLength = 50
        Me.txtNumInt.Multiline = True
        Me.txtNumInt.Name = "txtNumInt"
        Me.txtNumInt.Size = New System.Drawing.Size(165, 22)
        Me.txtNumInt.TabIndex = 101
        '
        'cmbxCalle
        '
        Me.cmbxCalle.BackColor = System.Drawing.Color.White
        Me.cmbxCalle.DisplayMember = "NOMBRE"
        Me.cmbxCalle.Enabled = False
        Me.cmbxCalle.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmbxCalle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbxCalle.FormattingEnabled = True
        Me.cmbxCalle.Location = New System.Drawing.Point(6, 118)
        Me.cmbxCalle.Name = "cmbxCalle"
        Me.cmbxCalle.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbxCalle.Size = New System.Drawing.Size(412, 24)
        Me.cmbxCalle.TabIndex = 99
        Me.cmbxCalle.ValueMember = "Clv_Calle"
        '
        'txtEmail
        '
        Me.txtEmail.BackColor = System.Drawing.Color.White
        Me.txtEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtEmail.Enabled = False
        Me.txtEmail.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmail.Location = New System.Drawing.Point(598, 206)
        Me.txtEmail.MaxLength = 30
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(267, 20)
        Me.txtEmail.TabIndex = 108
        '
        'cmbxSector
        '
        Me.cmbxSector.BackColor = System.Drawing.Color.White
        Me.cmbxSector.DisplayMember = "Descripcion"
        Me.cmbxSector.Enabled = False
        Me.cmbxSector.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmbxSector.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbxSector.FormattingEnabled = True
        Me.cmbxSector.Location = New System.Drawing.Point(8, 285)
        Me.cmbxSector.Name = "cmbxSector"
        Me.cmbxSector.Size = New System.Drawing.Size(306, 21)
        Me.cmbxSector.TabIndex = 135
        Me.cmbxSector.ValueMember = "Clv_Sector"
        '
        'chbxCasaPropia
        '
        Me.chbxCasaPropia.AutoSize = True
        Me.chbxCasaPropia.BackColor = System.Drawing.Color.White
        Me.chbxCasaPropia.Enabled = False
        Me.chbxCasaPropia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.chbxCasaPropia.ForeColor = System.Drawing.Color.LightSlateGray
        Me.chbxCasaPropia.Location = New System.Drawing.Point(8, 312)
        Me.chbxCasaPropia.Name = "chbxCasaPropia"
        Me.chbxCasaPropia.Size = New System.Drawing.Size(104, 19)
        Me.chbxCasaPropia.TabIndex = 134
        Me.chbxCasaPropia.Text = "Casa Propia"
        Me.chbxCasaPropia.UseVisualStyleBackColor = False
        '
        'txtIFE
        '
        Me.txtIFE.BackColor = System.Drawing.Color.White
        Me.txtIFE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtIFE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIFE.Enabled = False
        Me.txtIFE.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.txtIFE.Location = New System.Drawing.Point(321, 286)
        Me.txtIFE.Name = "txtIFE"
        Me.txtIFE.Size = New System.Drawing.Size(271, 20)
        Me.txtIFE.TabIndex = 133
        '
        'cmbxColonia
        '
        Me.cmbxColonia.BackColor = System.Drawing.Color.White
        Me.cmbxColonia.DisplayMember = "COLONIA"
        Me.cmbxColonia.Enabled = False
        Me.cmbxColonia.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmbxColonia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbxColonia.FormattingEnabled = True
        Me.cmbxColonia.Location = New System.Drawing.Point(6, 210)
        Me.cmbxColonia.Name = "cmbxColonia"
        Me.cmbxColonia.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbxColonia.Size = New System.Drawing.Size(308, 24)
        Me.cmbxColonia.TabIndex = 104
        Me.cmbxColonia.ValueMember = "CLV_COLONIA"
        '
        'cmbxCiudad
        '
        Me.cmbxCiudad.BackColor = System.Drawing.Color.White
        Me.cmbxCiudad.DisplayMember = "Nombre"
        Me.cmbxCiudad.Enabled = False
        Me.cmbxCiudad.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmbxCiudad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbxCiudad.FormattingEnabled = True
        Me.cmbxCiudad.Location = New System.Drawing.Point(6, 248)
        Me.cmbxCiudad.Name = "cmbxCiudad"
        Me.cmbxCiudad.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbxCiudad.Size = New System.Drawing.Size(308, 24)
        Me.cmbxCiudad.TabIndex = 105
        Me.cmbxCiudad.ValueMember = "Clv_Ciudad"
        '
        'txtApellidoPaterno
        '
        Me.txtApellidoPaterno.BackColor = System.Drawing.Color.White
        Me.txtApellidoPaterno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtApellidoPaterno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtApellidoPaterno.Enabled = False
        Me.txtApellidoPaterno.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApellidoPaterno.Location = New System.Drawing.Point(125, 68)
        Me.txtApellidoPaterno.Name = "txtApellidoPaterno"
        Me.txtApellidoPaterno.Size = New System.Drawing.Size(223, 21)
        Me.txtApellidoPaterno.TabIndex = 96
        '
        'txtNumExt
        '
        Me.txtNumExt.BackColor = System.Drawing.Color.White
        Me.txtNumExt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumExt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumExt.Enabled = False
        Me.txtNumExt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumExt.Location = New System.Drawing.Point(479, 120)
        Me.txtNumExt.MaxLength = 50
        Me.txtNumExt.Name = "txtNumExt"
        Me.txtNumExt.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtNumExt.Size = New System.Drawing.Size(161, 22)
        Me.txtNumExt.TabIndex = 100
        '
        'txtEntreCalles
        '
        Me.txtEntreCalles.BackColor = System.Drawing.Color.White
        Me.txtEntreCalles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtEntreCalles.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEntreCalles.Enabled = False
        Me.txtEntreCalles.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEntreCalles.Location = New System.Drawing.Point(8, 172)
        Me.txtEntreCalles.MaxLength = 150
        Me.txtEntreCalles.Multiline = True
        Me.txtEntreCalles.Name = "txtEntreCalles"
        Me.txtEntreCalles.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtEntreCalles.Size = New System.Drawing.Size(306, 20)
        Me.txtEntreCalles.TabIndex = 102
        '
        'txtApellidoMaterno
        '
        Me.txtApellidoMaterno.BackColor = System.Drawing.Color.White
        Me.txtApellidoMaterno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtApellidoMaterno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtApellidoMaterno.Enabled = False
        Me.txtApellidoMaterno.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApellidoMaterno.Location = New System.Drawing.Point(354, 68)
        Me.txtApellidoMaterno.Name = "txtApellidoMaterno"
        Me.txtApellidoMaterno.Size = New System.Drawing.Size(241, 21)
        Me.txtApellidoMaterno.TabIndex = 97
        '
        'txtCP
        '
        Me.txtCP.BackColor = System.Drawing.Color.White
        Me.txtCP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCP.Enabled = False
        Me.txtCP.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCP.Location = New System.Drawing.Point(321, 172)
        Me.txtCP.Multiline = True
        Me.txtCP.Name = "txtCP"
        Me.txtCP.ReadOnly = True
        Me.txtCP.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCP.Size = New System.Drawing.Size(96, 20)
        Me.txtCP.TabIndex = 103
        Me.txtCP.TabStop = False
        '
        'txtTelCel
        '
        Me.txtTelCel.BackColor = System.Drawing.Color.White
        Me.txtTelCel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTelCel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTelCel.Enabled = False
        Me.txtTelCel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTelCel.Location = New System.Drawing.Point(554, 172)
        Me.txtTelCel.MaxLength = 30
        Me.txtTelCel.Name = "txtTelCel"
        Me.txtTelCel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtTelCel.Size = New System.Drawing.Size(152, 20)
        Me.txtTelCel.TabIndex = 106
        Me.txtTelCel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtTelCasa
        '
        Me.txtTelCasa.BackColor = System.Drawing.Color.White
        Me.txtTelCasa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTelCasa.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTelCasa.Enabled = False
        Me.txtTelCasa.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTelCasa.Location = New System.Drawing.Point(423, 172)
        Me.txtTelCasa.MaxLength = 30
        Me.txtTelCasa.Name = "txtTelCasa"
        Me.txtTelCasa.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtTelCasa.Size = New System.Drawing.Size(125, 20)
        Me.txtTelCasa.TabIndex = 107
        Me.txtTelCasa.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtContrato
        '
        Me.txtContrato.BackColor = System.Drawing.Color.LightSteelBlue
        Me.txtContrato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtContrato.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtContrato.ForeColor = System.Drawing.Color.Black
        Me.txtContrato.Location = New System.Drawing.Point(9, 66)
        Me.txtContrato.Name = "txtContrato"
        Me.txtContrato.ReadOnly = True
        Me.txtContrato.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtContrato.Size = New System.Drawing.Size(105, 22)
        Me.txtContrato.TabIndex = 110
        Me.txtContrato.TabStop = False
        '
        'txtNombre
        '
        Me.txtNombre.BackColor = System.Drawing.Color.White
        Me.txtNombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombre.Enabled = False
        Me.txtNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNombre.Location = New System.Drawing.Point(612, 68)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(253, 21)
        Me.txtNombre.TabIndex = 98
        '
        'FrmDatosGeneralesClientes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 18.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(914, 407)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnSalir)
        Me.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.Name = "FrmDatosGeneralesClientes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Datos Generales del Clientes"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtTelTrabajo As System.Windows.Forms.TextBox
    Friend WithEvents Referencia2Text As System.Windows.Forms.TextBox
    Friend WithEvents Referencia1Text As System.Windows.Forms.TextBox
    Friend WithEvents txtNumInt As System.Windows.Forms.TextBox
    Friend WithEvents cmbxCalle As System.Windows.Forms.ComboBox
    Friend WithEvents txtEmail As System.Windows.Forms.TextBox
    Friend WithEvents cmbxSector As System.Windows.Forms.ComboBox
    Friend WithEvents chbxCasaPropia As System.Windows.Forms.CheckBox
    Friend WithEvents txtIFE As System.Windows.Forms.TextBox
    Friend WithEvents cmbxColonia As System.Windows.Forms.ComboBox
    Friend WithEvents cmbxCiudad As System.Windows.Forms.ComboBox
    Friend WithEvents txtApellidoPaterno As System.Windows.Forms.TextBox
    Friend WithEvents txtNumExt As System.Windows.Forms.TextBox
    Friend WithEvents txtEntreCalles As System.Windows.Forms.TextBox
    Friend WithEvents txtApellidoMaterno As System.Windows.Forms.TextBox
    Friend WithEvents txtCP As System.Windows.Forms.TextBox
    Friend WithEvents txtTelCel As System.Windows.Forms.TextBox
    Friend WithEvents txtTelCasa As System.Windows.Forms.TextBox
    Friend WithEvents txtContrato As System.Windows.Forms.TextBox
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents cmbxSubTipoCliente As System.Windows.Forms.ComboBox
    Friend WithEvents cmbxTipoCliente As System.Windows.Forms.ComboBox
End Class
