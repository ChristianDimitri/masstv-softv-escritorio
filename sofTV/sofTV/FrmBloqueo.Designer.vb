<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmBloqueo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.CMBLabel3 = New System.Windows.Forms.Label()
        Me.Consulta_ClientesBloqBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLidia = New sofTV.DataSetLidia()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Consulta_ClientesBloqTableAdapter = New sofTV.DataSetLidiaTableAdapters.Consulta_ClientesBloqTableAdapter()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox()
        CType(Me.Consulta_ClientesBloqBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(247, 241)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(168, 42)
        Me.Button1.TabIndex = 7
        Me.Button1.Text = "Aceptar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'CMBLabel3
        '
        Me.CMBLabel3.AutoSize = True
        Me.CMBLabel3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ClientesBloqBindingSource, "Observaciones", True))
        Me.CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.Location = New System.Drawing.Point(603, 293)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(55, 16)
        Me.CMBLabel3.TabIndex = 6
        Me.CMBLabel3.Text = "Label3"
        Me.CMBLabel3.Visible = False
        '
        'Consulta_ClientesBloqBindingSource
        '
        Me.Consulta_ClientesBloqBindingSource.DataMember = "Consulta_ClientesBloq"
        Me.Consulta_ClientesBloqBindingSource.DataSource = Me.DataSetLidia
        '
        'DataSetLidia
        '
        Me.DataSetLidia.DataSetName = "DataSetLidia"
        Me.DataSetLidia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(24, 63)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(233, 18)
        Me.CMBLabel2.TabIndex = 5
        Me.CMBLabel2.Text = "El Cliente fue Bloqueado por :"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(-20, -1)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(690, 48)
        Me.Panel1.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Red
        Me.Label1.Location = New System.Drawing.Point(193, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(269, 33)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Cliente Bloqueado"
        '
        'Consulta_ClientesBloqTableAdapter
        '
        Me.Consulta_ClientesBloqTableAdapter.ClearBeforeFill = True
        '
        'Timer1
        '
        '
        'RichTextBox1
        '
        Me.RichTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ClientesBloqBindingSource, "Observaciones", True))
        Me.RichTextBox1.Location = New System.Drawing.Point(27, 84)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.Size = New System.Drawing.Size(595, 149)
        Me.RichTextBox1.TabIndex = 8
        Me.RichTextBox1.Text = ""
        '
        'FrmBloqueo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(645, 295)
        Me.Controls.Add(Me.RichTextBox1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.CMBLabel3)
        Me.MaximizeBox = False
        Me.Name = "FrmBloqueo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cliente Bloqueado"
        CType(Me.Consulta_ClientesBloqBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DataSetLidia As sofTV.DataSetLidia
    Friend WithEvents Consulta_ClientesBloqBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_ClientesBloqTableAdapter As sofTV.DataSetLidiaTableAdapters.Consulta_ClientesBloqTableAdapter
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents RichTextBox1 As System.Windows.Forms.RichTextBox
End Class
