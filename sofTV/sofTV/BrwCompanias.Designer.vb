﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwCompanias
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.btnBuscarNombreCompania = New System.Windows.Forms.Button()
        Me.txtClave = New System.Windows.Forms.TextBox()
        Me.btnBuscarClave = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.CMBlblClave = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.id_companiaLbl = New System.Windows.Forms.Label()
        Me.DescripcionLabel = New System.Windows.Forms.Label()
        Me.gbxCompanias = New System.Windows.Forms.GroupBox()
        Me.cbxActivo = New System.Windows.Forms.CheckBox()
        Me.CMBlblActivo = New System.Windows.Forms.Label()
        Me.CMBlblRfc = New System.Windows.Forms.Label()
        Me.CMBblRfcMostrar = New System.Windows.Forms.Label()
        Me.CMBlblBusqueda = New System.Windows.Forms.Label()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.CMBlblRazon = New System.Windows.Forms.Label()
        Me.txtNombreCompania = New System.Windows.Forms.TextBox()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnConsultar = New System.Windows.Forms.Button()
        Me.dgvDatos = New System.Windows.Forms.DataGridView()
        Me.id_compania = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.razon_social = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.rfc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.activo = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.clvCompaniaMizar = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.gbxCompanias.SuspendLayout()
        CType(Me.dgvDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnBuscarNombreCompania
        '
        Me.btnBuscarNombreCompania.BackColor = System.Drawing.Color.DarkOrange
        Me.btnBuscarNombreCompania.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuscarNombreCompania.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscarNombreCompania.ForeColor = System.Drawing.Color.Black
        Me.btnBuscarNombreCompania.Location = New System.Drawing.Point(169, 174)
        Me.btnBuscarNombreCompania.Name = "btnBuscarNombreCompania"
        Me.btnBuscarNombreCompania.Size = New System.Drawing.Size(88, 23)
        Me.btnBuscarNombreCompania.TabIndex = 3
        Me.btnBuscarNombreCompania.Text = "&Buscar"
        Me.btnBuscarNombreCompania.UseVisualStyleBackColor = False
        '
        'txtClave
        '
        Me.txtClave.BackColor = System.Drawing.Color.LightGray
        Me.txtClave.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtClave.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClave.Location = New System.Drawing.Point(7, 71)
        Me.txtClave.Name = "txtClave"
        Me.txtClave.Size = New System.Drawing.Size(250, 24)
        Me.txtClave.TabIndex = 0
        '
        'btnBuscarClave
        '
        Me.btnBuscarClave.BackColor = System.Drawing.Color.DarkOrange
        Me.btnBuscarClave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuscarClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscarClave.ForeColor = System.Drawing.Color.Black
        Me.btnBuscarClave.Location = New System.Drawing.Point(169, 101)
        Me.btnBuscarClave.Name = "btnBuscarClave"
        Me.btnBuscarClave.Size = New System.Drawing.Size(88, 23)
        Me.btnBuscarClave.TabIndex = 1
        Me.btnBuscarClave.Text = "&Buscar"
        Me.btnBuscarClave.UseVisualStyleBackColor = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Black
        Me.Label8.Location = New System.Drawing.Point(10, 72)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(117, 13)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Nombre Compania :"
        '
        'CMBlblClave
        '
        Me.CMBlblClave.AutoSize = True
        Me.CMBlblClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblClave.Location = New System.Drawing.Point(4, 53)
        Me.CMBlblClave.Name = "CMBlblClave"
        Me.CMBlblClave.Size = New System.Drawing.Size(50, 15)
        Me.CMBlblClave.TabIndex = 88
        Me.CMBlblClave.Text = "Clave :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(11, 18)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(51, 13)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "Clave  :"
        '
        'id_companiaLbl
        '
        Me.id_companiaLbl.BackColor = System.Drawing.SystemColors.Control
        Me.id_companiaLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.id_companiaLbl.Location = New System.Drawing.Point(10, 31)
        Me.id_companiaLbl.Name = "id_companiaLbl"
        Me.id_companiaLbl.Size = New System.Drawing.Size(184, 23)
        Me.id_companiaLbl.TabIndex = 1
        '
        'DescripcionLabel
        '
        Me.DescripcionLabel.AutoEllipsis = True
        Me.DescripcionLabel.BackColor = System.Drawing.SystemColors.Control
        Me.DescripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionLabel.Location = New System.Drawing.Point(11, 85)
        Me.DescripcionLabel.Name = "DescripcionLabel"
        Me.DescripcionLabel.Size = New System.Drawing.Size(239, 38)
        Me.DescripcionLabel.TabIndex = 3
        '
        'gbxCompanias
        '
        Me.gbxCompanias.BackColor = System.Drawing.SystemColors.Control
        Me.gbxCompanias.Controls.Add(Me.cbxActivo)
        Me.gbxCompanias.Controls.Add(Me.CMBlblActivo)
        Me.gbxCompanias.Controls.Add(Me.CMBlblRfc)
        Me.gbxCompanias.Controls.Add(Me.CMBblRfcMostrar)
        Me.gbxCompanias.Controls.Add(Me.Label8)
        Me.gbxCompanias.Controls.Add(Me.Label7)
        Me.gbxCompanias.Controls.Add(Me.id_companiaLbl)
        Me.gbxCompanias.Controls.Add(Me.DescripcionLabel)
        Me.gbxCompanias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxCompanias.ForeColor = System.Drawing.Color.Black
        Me.gbxCompanias.Location = New System.Drawing.Point(7, 475)
        Me.gbxCompanias.Name = "gbxCompanias"
        Me.gbxCompanias.Size = New System.Drawing.Size(274, 225)
        Me.gbxCompanias.TabIndex = 93
        Me.gbxCompanias.TabStop = False
        Me.gbxCompanias.Text = "Datos de la Compañia"
        '
        'cbxActivo
        '
        Me.cbxActivo.AutoSize = True
        Me.cbxActivo.Enabled = False
        Me.cbxActivo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cbxActivo.Location = New System.Drawing.Point(69, 175)
        Me.cbxActivo.Name = "cbxActivo"
        Me.cbxActivo.Size = New System.Drawing.Size(12, 11)
        Me.cbxActivo.TabIndex = 98
        Me.cbxActivo.UseVisualStyleBackColor = True
        '
        'CMBlblActivo
        '
        Me.CMBlblActivo.AutoSize = True
        Me.CMBlblActivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblActivo.ForeColor = System.Drawing.Color.Black
        Me.CMBlblActivo.Location = New System.Drawing.Point(12, 175)
        Me.CMBlblActivo.Name = "CMBlblActivo"
        Me.CMBlblActivo.Size = New System.Drawing.Size(51, 13)
        Me.CMBlblActivo.TabIndex = 16
        Me.CMBlblActivo.Text = "Activo :"
        '
        'CMBlblRfc
        '
        Me.CMBlblRfc.AutoSize = True
        Me.CMBlblRfc.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblRfc.ForeColor = System.Drawing.Color.Black
        Me.CMBlblRfc.Location = New System.Drawing.Point(11, 123)
        Me.CMBlblRfc.Name = "CMBlblRfc"
        Me.CMBlblRfc.Size = New System.Drawing.Size(39, 13)
        Me.CMBlblRfc.TabIndex = 15
        Me.CMBlblRfc.Text = "RFC :"
        '
        'CMBblRfcMostrar
        '
        Me.CMBblRfcMostrar.AutoEllipsis = True
        Me.CMBblRfcMostrar.BackColor = System.Drawing.SystemColors.Control
        Me.CMBblRfcMostrar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBblRfcMostrar.Location = New System.Drawing.Point(12, 136)
        Me.CMBblRfcMostrar.Name = "CMBblRfcMostrar"
        Me.CMBblRfcMostrar.Size = New System.Drawing.Size(238, 23)
        Me.CMBblRfcMostrar.TabIndex = 14
        '
        'CMBlblBusqueda
        '
        Me.CMBlblBusqueda.AutoSize = True
        Me.CMBlblBusqueda.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblBusqueda.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBlblBusqueda.Location = New System.Drawing.Point(0, 9)
        Me.CMBlblBusqueda.Name = "CMBlblBusqueda"
        Me.CMBlblBusqueda.Size = New System.Drawing.Size(224, 24)
        Me.CMBlblBusqueda.TabIndex = 86
        Me.CMBlblBusqueda.Text = "Buscar Compañia Por :"
        '
        'btnSalir
        '
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(858, 664)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(136, 36)
        Me.btnSalir.TabIndex = 8
        Me.btnSalir.Text = "&SALIR"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnModificar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnModificar.Location = New System.Drawing.Point(860, 95)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(136, 36)
        Me.btnModificar.TabIndex = 7
        Me.btnModificar.Text = "&MODIFICAR"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'CMBlblRazon
        '
        Me.CMBlblRazon.AutoSize = True
        Me.CMBlblRazon.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblRazon.Location = New System.Drawing.Point(4, 126)
        Me.CMBlblRazon.Name = "CMBlblRazon"
        Me.CMBlblRazon.Size = New System.Drawing.Size(135, 15)
        Me.CMBlblRazon.TabIndex = 87
        Me.CMBlblRazon.Text = "Nombre Compañía :"
        '
        'txtNombreCompania
        '
        Me.txtNombreCompania.BackColor = System.Drawing.Color.LightGray
        Me.txtNombreCompania.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNombreCompania.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombreCompania.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNombreCompania.Location = New System.Drawing.Point(7, 144)
        Me.txtNombreCompania.Name = "txtNombreCompania"
        Me.txtNombreCompania.Size = New System.Drawing.Size(250, 24)
        Me.txtNombreCompania.TabIndex = 2
        '
        'btnNuevo
        '
        Me.btnNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuevo.Location = New System.Drawing.Point(860, 11)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(136, 36)
        Me.btnNuevo.TabIndex = 5
        Me.btnNuevo.Text = "&NUEVO"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnConsultar
        '
        Me.btnConsultar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnConsultar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConsultar.Location = New System.Drawing.Point(860, 53)
        Me.btnConsultar.Name = "btnConsultar"
        Me.btnConsultar.Size = New System.Drawing.Size(136, 36)
        Me.btnConsultar.TabIndex = 6
        Me.btnConsultar.Text = "&CONSULTAR"
        Me.btnConsultar.UseVisualStyleBackColor = True
        '
        'dgvDatos
        '
        Me.dgvDatos.AllowUserToAddRows = False
        Me.dgvDatos.AllowUserToDeleteRows = False
        Me.dgvDatos.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        Me.dgvDatos.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvDatos.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgvDatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDatos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.id_compania, Me.razon_social, Me.rfc, Me.activo, Me.clvCompaniaMizar})
        Me.dgvDatos.Location = New System.Drawing.Point(283, 12)
        Me.dgvDatos.Name = "dgvDatos"
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black
        Me.dgvDatos.RowsDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvDatos.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgvDatos.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black
        Me.dgvDatos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDatos.Size = New System.Drawing.Size(569, 688)
        Me.dgvDatos.TabIndex = 4
        '
        'id_compania
        '
        Me.id_compania.DataPropertyName = "clvCompania"
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Blue
        Me.id_compania.DefaultCellStyle = DataGridViewCellStyle2
        Me.id_compania.HeaderText = "Clave"
        Me.id_compania.Name = "id_compania"
        '
        'razon_social
        '
        Me.razon_social.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.razon_social.DataPropertyName = "nombreCompania"
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Blue
        Me.razon_social.DefaultCellStyle = DataGridViewCellStyle3
        Me.razon_social.HeaderText = "Nombre Compañía"
        Me.razon_social.Name = "razon_social"
        '
        'rfc
        '
        Me.rfc.DataPropertyName = "rfc"
        Me.rfc.HeaderText = "RFC"
        Me.rfc.Name = "rfc"
        Me.rfc.Width = 150
        '
        'activo
        '
        Me.activo.DataPropertyName = "activo"
        Me.activo.HeaderText = "Activo"
        Me.activo.Name = "activo"
        Me.activo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.activo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.activo.Width = 50
        '
        'clvCompaniaMizar
        '
        Me.clvCompaniaMizar.DataPropertyName = "clvCompaniaMizar"
        Me.clvCompaniaMizar.HeaderText = "Clave Mizar"
        Me.clvCompaniaMizar.Name = "clvCompaniaMizar"
        Me.clvCompaniaMizar.Visible = False
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'BrwCompanias
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1008, 730)
        Me.Controls.Add(Me.dgvDatos)
        Me.Controls.Add(Me.btnConsultar)
        Me.Controls.Add(Me.btnBuscarNombreCompania)
        Me.Controls.Add(Me.txtClave)
        Me.Controls.Add(Me.btnBuscarClave)
        Me.Controls.Add(Me.CMBlblClave)
        Me.Controls.Add(Me.gbxCompanias)
        Me.Controls.Add(Me.CMBlblBusqueda)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.CMBlblRazon)
        Me.Controls.Add(Me.txtNombreCompania)
        Me.Controls.Add(Me.btnNuevo)
        Me.Name = "BrwCompanias"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catalogo de Compañias"
        Me.gbxCompanias.ResumeLayout(False)
        Me.gbxCompanias.PerformLayout()
        CType(Me.dgvDatos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnBuscarNombreCompania As System.Windows.Forms.Button
    Friend WithEvents txtClave As System.Windows.Forms.TextBox
    Friend WithEvents btnBuscarClave As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents CMBlblClave As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents id_companiaLbl As System.Windows.Forms.Label
    Friend WithEvents DescripcionLabel As System.Windows.Forms.Label
    Friend WithEvents gbxCompanias As System.Windows.Forms.GroupBox
    Friend WithEvents CMBlblBusqueda As System.Windows.Forms.Label
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents CMBlblRazon As System.Windows.Forms.Label
    Friend WithEvents txtNombreCompania As System.Windows.Forms.TextBox
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents btnConsultar As System.Windows.Forms.Button
    Friend WithEvents dgvDatos As System.Windows.Forms.DataGridView
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents cbxActivo As System.Windows.Forms.CheckBox
    Friend WithEvents CMBlblActivo As System.Windows.Forms.Label
    Friend WithEvents CMBlblRfc As System.Windows.Forms.Label
    Friend WithEvents CMBblRfcMostrar As System.Windows.Forms.Label
    Friend WithEvents id_compania As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents razon_social As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents rfc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents activo As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents clvCompaniaMizar As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
