﻿Imports System.Collections
Imports System.Collections.Generic
Imports System.Xml
Imports System.IO
Public Class FrmRelTrabajosRetiroAparato

    Private Sub lblTitulo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblTitulo.Click

    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Try
            Dim strXML = GeneraXML()
            If Len(strXML) > 0 Then
                DAEquipoExtra.GuardaConfiguracionesRelTrabajosRetiroAparato(strXML)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub FrmRelTrabajosRetiroAparato_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        lblTitulo.ForeColor = Color.Black
        'Obtenemos los Trabajos y su relación con el Retiro de Aparato
        Try
            Dim TrabajosRetiroAparatos As New List(Of TrabajosRetiroAparato)
            TrabajosRetiroAparatos = DAEquipoExtra.CargaRelTrabajosRetiroAparato
            If (TrabajosRetiroAparatos.Count > 0) Then
                dgvRelTrabajosRetiroAparato.DataSource = TrabajosRetiroAparatos
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Function GeneraXML() As String

        Dim sw As New StringWriter()
        Dim w As New XmlTextWriter(sw)
        Dim i As Integer
        Dim strXML As String = ""

        Try

            w.Formatting = Formatting.Indented
            w.WriteStartElement("Trabajos")

            For i = 0 To dgvRelTrabajosRetiroAparato.RowCount - 1

                w.WriteStartElement("Trabajo")
                w.WriteAttributeString("Clv_Trabajo", Me.dgvRelTrabajosRetiroAparato.Rows(i).Cells("ident").Value.ToString)
                w.WriteAttributeString("Trabajo", Me.dgvRelTrabajosRetiroAparato.Rows(i).Cells("Trabajo").Value.ToString)
                w.WriteAttributeString("AplicaRetiroAparato", Me.dgvRelTrabajosRetiroAparato.Rows(i).Cells("AplicaRetiroAparato").Value.ToString)
                w.WriteEndElement()

            Next

            w.WriteEndElement()
            w.Close()

            strXML = ""
            strXML = sw.ToString()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try

        Return strXML
    End Function
End Class