<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCarteras
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.ComboPeriodo = New System.Windows.Forms.ComboBox()
        Me.PorPeridoSeleccioneBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEdgarRev2 = New sofTV.DataSetEdgarRev2()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.MuestraTipSerPrincipalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEDGAR = New sofTV.DataSetEDGAR()
        Me.Rep_CarterasDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Rep_CarterasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Rep_CarterasTableAdapter = New sofTV.DataSetEDGARTableAdapters.Rep_CarterasTableAdapter()
        Me.MuestraTipSerPrincipalTableAdapter = New sofTV.DataSetEDGARTableAdapters.MuestraTipSerPrincipalTableAdapter()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.BORRACARTERASBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BORRACARTERASTableAdapter = New sofTV.DataSetEDGARTableAdapters.BORRACARTERASTableAdapter()
        Me.Genera_CarteraBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Genera_CarteraTableAdapter = New sofTV.DataSetEDGARTableAdapters.Genera_CarteraTableAdapter()
        Me.Genera_CarteraDIGBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Genera_CarteraDIGTableAdapter = New sofTV.DataSetEdgarRev2TableAdapters.Genera_CarteraDIGTableAdapter()
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2()
        Me.Dame_ciudad_carteraBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_ciudad_carteraTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Dame_ciudad_carteraTableAdapter()
        Me.Procedimientosarnoldo4 = New sofTV.Procedimientosarnoldo4()
        Me.Borra_sessionCarteraBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Borra_sessionCarteraTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Borra_sessionCarteraTableAdapter()
        Me.PorPerido_SeleccioneTableAdapter = New sofTV.DataSetEdgarRev2TableAdapters.PorPerido_SeleccioneTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Panel1.SuspendLayout()
        CType(Me.PorPeridoSeleccioneBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTipSerPrincipalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Rep_CarterasDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Rep_CarterasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.BORRACARTERASBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Genera_CarteraBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Genera_CarteraDIGBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_ciudad_carteraBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Borra_sessionCarteraBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.ComboPeriodo)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.CMBLabel1)
        Me.Panel1.Controls.Add(Me.ComboBox1)
        Me.Panel1.Controls.Add(Me.Rep_CarterasDataGridView)
        Me.Panel1.Location = New System.Drawing.Point(8, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(260, 648)
        Me.Panel1.TabIndex = 0
        '
        'ComboPeriodo
        '
        Me.ComboPeriodo.DataSource = Me.PorPeridoSeleccioneBindingSource
        Me.ComboPeriodo.DisplayMember = "Descripcion"
        Me.ComboPeriodo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboPeriodo.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboPeriodo.FormattingEnabled = True
        Me.ComboPeriodo.Location = New System.Drawing.Point(15, 93)
        Me.ComboPeriodo.Name = "ComboPeriodo"
        Me.ComboPeriodo.Size = New System.Drawing.Size(239, 26)
        Me.ComboPeriodo.TabIndex = 4
        Me.ComboPeriodo.ValueMember = "Clv_Periodo"
        Me.ComboPeriodo.Visible = False
        '
        'PorPeridoSeleccioneBindingSource
        '
        Me.PorPeridoSeleccioneBindingSource.DataMember = "PorPerido_Seleccione"
        Me.PorPeridoSeleccioneBindingSource.DataSource = Me.DataSetEdgarRev2
        '
        'DataSetEdgarRev2
        '
        Me.DataSetEdgarRev2.DataSetName = "DataSetEdgarRev2"
        Me.DataSetEdgarRev2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.SaddleBrown
        Me.Label1.Location = New System.Drawing.Point(12, 72)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(183, 18)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Seleccione el Periodo :"
        Me.Label1.Visible = False
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.SaddleBrown
        Me.CMBLabel1.Location = New System.Drawing.Point(12, 7)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(149, 18)
        Me.CMBLabel1.TabIndex = 2
        Me.CMBLabel1.Text = "Tipos de Servicio :"
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.MuestraTipSerPrincipalBindingSource
        Me.ComboBox1.DisplayMember = "Concepto"
        Me.ComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(15, 32)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(239, 26)
        Me.ComboBox1.TabIndex = 0
        Me.ComboBox1.ValueMember = "Clv_TipSerPrincipal"
        '
        'MuestraTipSerPrincipalBindingSource
        '
        Me.MuestraTipSerPrincipalBindingSource.DataMember = "MuestraTipSerPrincipal"
        Me.MuestraTipSerPrincipalBindingSource.DataSource = Me.DataSetEDGAR
        '
        'DataSetEDGAR
        '
        Me.DataSetEDGAR.DataSetName = "DataSetEDGAR"
        Me.DataSetEDGAR.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Rep_CarterasDataGridView
        '
        Me.Rep_CarterasDataGridView.AllowUserToAddRows = False
        Me.Rep_CarterasDataGridView.AllowUserToDeleteRows = False
        Me.Rep_CarterasDataGridView.AutoGenerateColumns = False
        Me.Rep_CarterasDataGridView.BackgroundColor = System.Drawing.Color.Gray
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.DarkKhaki
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Rep_CarterasDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.Rep_CarterasDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2})
        Me.Rep_CarterasDataGridView.DataSource = Me.Rep_CarterasBindingSource
        Me.Rep_CarterasDataGridView.GridColor = System.Drawing.Color.LemonChiffon
        Me.Rep_CarterasDataGridView.Location = New System.Drawing.Point(-39, 142)
        Me.Rep_CarterasDataGridView.Name = "Rep_CarterasDataGridView"
        Me.Rep_CarterasDataGridView.ReadOnly = True
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.DarkKhaki
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Rep_CarterasDataGridView.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.Rep_CarterasDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Rep_CarterasDataGridView.Size = New System.Drawing.Size(293, 276)
        Me.Rep_CarterasDataGridView.TabIndex = 0
        Me.Rep_CarterasDataGridView.TabStop = False
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Clave"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.DataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridViewTextBoxColumn1.HeaderText = "Clave"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Reporte"
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.PaleGoldenrod
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.PaleGoldenrod
        Me.DataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewTextBoxColumn2.HeaderText = "Catalógo de  Reportes"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 250
        '
        'Rep_CarterasBindingSource
        '
        Me.Rep_CarterasBindingSource.DataMember = "Rep_Carteras"
        Me.Rep_CarterasBindingSource.DataSource = Me.DataSetEDGAR
        '
        'Rep_CarterasTableAdapter
        '
        Me.Rep_CarterasTableAdapter.ClearBeforeFill = True
        '
        'MuestraTipSerPrincipalTableAdapter
        '
        Me.MuestraTipSerPrincipalTableAdapter.ClearBeforeFill = True
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Olive
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(49, 676)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(176, 38)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.CrystalReportViewer1)
        Me.Panel2.Location = New System.Drawing.Point(268, 12)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(736, 702)
        Me.Panel2.TabIndex = 2
        '
        'CrystalReportViewer1
        '
        Me.CrystalReportViewer1.ActiveViewIndex = -1
        Me.CrystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CrystalReportViewer1.DisplayGroupTree = False
        Me.CrystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrystalReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
        Me.CrystalReportViewer1.SelectionFormula = ""
        Me.CrystalReportViewer1.Size = New System.Drawing.Size(736, 702)
        Me.CrystalReportViewer1.TabIndex = 0
        Me.CrystalReportViewer1.TabStop = False
        Me.CrystalReportViewer1.ViewTimeSelectionFormula = ""
        '
        'BORRACARTERASBindingSource
        '
        Me.BORRACARTERASBindingSource.DataMember = "BORRACARTERAS"
        Me.BORRACARTERASBindingSource.DataSource = Me.DataSetEDGAR
        '
        'BORRACARTERASTableAdapter
        '
        Me.BORRACARTERASTableAdapter.ClearBeforeFill = True
        '
        'Genera_CarteraBindingSource
        '
        Me.Genera_CarteraBindingSource.DataMember = "Genera_Cartera"
        Me.Genera_CarteraBindingSource.DataSource = Me.DataSetEDGAR
        '
        'Genera_CarteraTableAdapter
        '
        Me.Genera_CarteraTableAdapter.ClearBeforeFill = True
        '
        'Genera_CarteraDIGBindingSource
        '
        Me.Genera_CarteraDIGBindingSource.DataMember = "Genera_CarteraDIG"
        Me.Genera_CarteraDIGBindingSource.DataSource = Me.DataSetEdgarRev2
        '
        'Genera_CarteraDIGTableAdapter
        '
        Me.Genera_CarteraDIGTableAdapter.ClearBeforeFill = True
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Dame_ciudad_carteraBindingSource
        '
        Me.Dame_ciudad_carteraBindingSource.DataMember = "Dame_ciudad_cartera"
        Me.Dame_ciudad_carteraBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Dame_ciudad_carteraTableAdapter
        '
        Me.Dame_ciudad_carteraTableAdapter.ClearBeforeFill = True
        '
        'Procedimientosarnoldo4
        '
        Me.Procedimientosarnoldo4.DataSetName = "Procedimientosarnoldo4"
        Me.Procedimientosarnoldo4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Borra_sessionCarteraBindingSource
        '
        Me.Borra_sessionCarteraBindingSource.DataMember = "Borra_sessionCartera"
        Me.Borra_sessionCarteraBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Borra_sessionCarteraTableAdapter
        '
        Me.Borra_sessionCarteraTableAdapter.ClearBeforeFill = True
        '
        'PorPerido_SeleccioneTableAdapter
        '
        Me.PorPerido_SeleccioneTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'FrmCarteras
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1016, 741)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "FrmCarteras"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Carteras"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PorPeridoSeleccioneBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTipSerPrincipalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Rep_CarterasDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Rep_CarterasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        CType(Me.BORRACARTERASBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Genera_CarteraBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Genera_CarteraDIGBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_ciudad_carteraBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Borra_sessionCarteraBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents DataSetEDGAR As sofTV.DataSetEDGAR
    Friend WithEvents Rep_CarterasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Rep_CarterasTableAdapter As sofTV.DataSetEDGARTableAdapters.Rep_CarterasTableAdapter
    Friend WithEvents Rep_CarterasDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents MuestraTipSerPrincipalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipSerPrincipalTableAdapter As sofTV.DataSetEDGARTableAdapters.MuestraTipSerPrincipalTableAdapter
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BORRACARTERASBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BORRACARTERASTableAdapter As sofTV.DataSetEDGARTableAdapters.BORRACARTERASTableAdapter
    Friend WithEvents Genera_CarteraBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Genera_CarteraTableAdapter As sofTV.DataSetEDGARTableAdapters.Genera_CarteraTableAdapter
    Friend WithEvents DataSetEdgarRev2 As sofTV.DataSetEdgarRev2
    Friend WithEvents Genera_CarteraDIGBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Genera_CarteraDIGTableAdapter As sofTV.DataSetEdgarRev2TableAdapters.Genera_CarteraDIGTableAdapter
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents Dame_ciudad_carteraBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_ciudad_carteraTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Dame_ciudad_carteraTableAdapter
    Friend WithEvents Procedimientosarnoldo4 As sofTV.Procedimientosarnoldo4
    Friend WithEvents Borra_sessionCarteraBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Borra_sessionCarteraTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Borra_sessionCarteraTableAdapter
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ComboPeriodo As System.Windows.Forms.ComboBox
    Friend WithEvents PorPeridoSeleccioneBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PorPerido_SeleccioneTableAdapter As sofTV.DataSetEdgarRev2TableAdapters.PorPerido_SeleccioneTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Private WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
