﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFiltroTipoServicio2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.gbxTipoServicio = New System.Windows.Forms.GroupBox()
        Me.lblTipoServicio = New System.Windows.Forms.Label()
        Me.cmbTipoServicio2 = New System.Windows.Forms.ComboBox()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.gbxTipoServicio.SuspendLayout()
        Me.SuspendLayout()
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.Location = New System.Drawing.Point(54, 116)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(116, 31)
        Me.btnAceptar.TabIndex = 0
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'gbxTipoServicio
        '
        Me.gbxTipoServicio.Controls.Add(Me.lblTipoServicio)
        Me.gbxTipoServicio.Controls.Add(Me.cmbTipoServicio2)
        Me.gbxTipoServicio.Location = New System.Drawing.Point(12, 12)
        Me.gbxTipoServicio.Name = "gbxTipoServicio"
        Me.gbxTipoServicio.Size = New System.Drawing.Size(348, 98)
        Me.gbxTipoServicio.TabIndex = 2
        Me.gbxTipoServicio.TabStop = False
        '
        'lblTipoServicio
        '
        Me.lblTipoServicio.AutoSize = True
        Me.lblTipoServicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoServicio.Location = New System.Drawing.Point(10, 28)
        Me.lblTipoServicio.Name = "lblTipoServicio"
        Me.lblTipoServicio.Size = New System.Drawing.Size(140, 18)
        Me.lblTipoServicio.TabIndex = 1
        Me.lblTipoServicio.Text = "Tipo de Servicio :"
        '
        'cmbTipoServicio2
        '
        Me.cmbTipoServicio2.DisplayMember = "Concepto"
        Me.cmbTipoServicio2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbTipoServicio2.FormattingEnabled = True
        Me.cmbTipoServicio2.Location = New System.Drawing.Point(6, 51)
        Me.cmbTipoServicio2.Name = "cmbTipoServicio2"
        Me.cmbTipoServicio2.Size = New System.Drawing.Size(335, 26)
        Me.cmbTipoServicio2.TabIndex = 0
        Me.cmbTipoServicio2.ValueMember = "Clv_TipSer"
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(202, 116)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(116, 31)
        Me.btnCancelar.TabIndex = 3
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'frmFiltroTipoServicio2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(369, 155)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.gbxTipoServicio)
        Me.Controls.Add(Me.btnAceptar)
        Me.Name = "frmFiltroTipoServicio2"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Seleccione el Tipo de Servicio"
        Me.gbxTipoServicio.ResumeLayout(False)
        Me.gbxTipoServicio.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents gbxTipoServicio As System.Windows.Forms.GroupBox
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents lblTipoServicio As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Public WithEvents cmbTipoServicio2 As System.Windows.Forms.ComboBox
End Class
