<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCamServCte
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CONTRATOLabel As System.Windows.Forms.Label
        Dim NOMBRELabel As System.Windows.Forms.Label
        Dim CALLELabel As System.Windows.Forms.Label
        Dim COLONIALabel As System.Windows.Forms.Label
        Dim NUMEROLabel As System.Windows.Forms.Label
        Dim CIUDADLabel As System.Windows.Forms.Label
        Dim ConceptoLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCamServCte))
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.DameServCteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.DameServPosiblesCteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.NOMBRETextBox = New System.Windows.Forms.TextBox()
        Me.DameClientesActivosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CALLETextBox = New System.Windows.Forms.TextBox()
        Me.COLONIATextBox = New System.Windows.Forms.TextBox()
        Me.NUMEROTextBox = New System.Windows.Forms.TextBox()
        Me.CIUDADTextBox = New System.Windows.Forms.TextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.ConceptoComboBox = New System.Windows.Forms.ComboBox()
        Me.MuestraTipServEricBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.GroupBoxPosible = New System.Windows.Forms.GroupBox()
        Me.DataGridViewPosible = New System.Windows.Forms.DataGridView()
        Me.Clv_ServicioAux = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ServicioAux = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MensualidadProp = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBoxActual = New System.Windows.Forms.GroupBox()
        Me.DataGridViewActual = New System.Windows.Forms.DataGridView()
        Me.Contrato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ContratoNet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_UnicaNet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_TipSer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Servicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MacCablemodem = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Servicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DameServCteTableAdapter = New sofTV.DataSetEricTableAdapters.DameServCteTableAdapter()
        Me.DameServPosiblesCteTableAdapter = New sofTV.DataSetEricTableAdapters.DameServPosiblesCteTableAdapter()
        Me.DameClientesActivosTableAdapter = New sofTV.DataSetEricTableAdapters.DameClientesActivosTableAdapter()
        Me.MuestraTipServEricTableAdapter = New sofTV.DataSetEricTableAdapters.MuestraTipServEricTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        CONTRATOLabel = New System.Windows.Forms.Label()
        NOMBRELabel = New System.Windows.Forms.Label()
        CALLELabel = New System.Windows.Forms.Label()
        COLONIALabel = New System.Windows.Forms.Label()
        NUMEROLabel = New System.Windows.Forms.Label()
        CIUDADLabel = New System.Windows.Forms.Label()
        ConceptoLabel = New System.Windows.Forms.Label()
        CType(Me.DameServCteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameServPosiblesCteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameClientesActivosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.MuestraTipServEricBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        Me.GroupBoxPosible.SuspendLayout()
        CType(Me.DataGridViewPosible, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBoxActual.SuspendLayout()
        CType(Me.DataGridViewActual, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CONTRATOLabel
        '
        CONTRATOLabel.AutoSize = True
        CONTRATOLabel.Location = New System.Drawing.Point(27, 46)
        CONTRATOLabel.Name = "CONTRATOLabel"
        CONTRATOLabel.Size = New System.Drawing.Size(69, 15)
        CONTRATOLabel.TabIndex = 39
        CONTRATOLabel.Text = "Contrato :"
        '
        'NOMBRELabel
        '
        NOMBRELabel.AutoSize = True
        NOMBRELabel.Location = New System.Drawing.Point(30, 74)
        NOMBRELabel.Name = "NOMBRELabel"
        NOMBRELabel.Size = New System.Drawing.Size(66, 15)
        NOMBRELabel.TabIndex = 40
        NOMBRELabel.Text = "Nombre :"
        '
        'CALLELabel
        '
        CALLELabel.AutoSize = True
        CALLELabel.Location = New System.Drawing.Point(49, 103)
        CALLELabel.Name = "CALLELabel"
        CALLELabel.Size = New System.Drawing.Size(48, 15)
        CALLELabel.TabIndex = 41
        CALLELabel.Text = "Calle :"
        '
        'COLONIALabel
        '
        COLONIALabel.AutoSize = True
        COLONIALabel.Location = New System.Drawing.Point(32, 131)
        COLONIALabel.Name = "COLONIALabel"
        COLONIALabel.Size = New System.Drawing.Size(64, 15)
        COLONIALabel.TabIndex = 42
        COLONIALabel.Text = "Colonia :"
        '
        'NUMEROLabel
        '
        NUMEROLabel.AutoSize = True
        NUMEROLabel.Location = New System.Drawing.Point(398, 99)
        NUMEROLabel.Name = "NUMEROLabel"
        NUMEROLabel.Size = New System.Drawing.Size(23, 15)
        NUMEROLabel.TabIndex = 43
        NUMEROLabel.Text = "# :"
        '
        'CIUDADLabel
        '
        CIUDADLabel.AutoSize = True
        CIUDADLabel.Location = New System.Drawing.Point(36, 164)
        CIUDADLabel.Name = "CIUDADLabel"
        CIUDADLabel.Size = New System.Drawing.Size(60, 15)
        CIUDADLabel.TabIndex = 44
        CIUDADLabel.Text = "Ciudad :"
        '
        'ConceptoLabel
        '
        ConceptoLabel.AutoSize = True
        ConceptoLabel.Location = New System.Drawing.Point(684, 24)
        ConceptoLabel.Name = "ConceptoLabel"
        ConceptoLabel.Size = New System.Drawing.Size(118, 15)
        ConceptoLabel.TabIndex = 45
        ConceptoLabel.Text = "Tipo de Servicio :"
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(107, 39)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(100, 21)
        Me.TextBox1.TabIndex = 2
        '
        'DameServCteBindingSource
        '
        Me.DameServCteBindingSource.DataMember = "DameServCte"
        Me.DameServCteBindingSource.DataSource = Me.DataSetEric
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DameServPosiblesCteBindingSource
        '
        Me.DameServPosiblesCteBindingSource.DataMember = "DameServPosiblesCte"
        Me.DameServPosiblesCteBindingSource.DataSource = Me.DataSetEric
        '
        'Button3
        '
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Button3.Location = New System.Drawing.Point(213, 38)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(44, 23)
        Me.Button3.TabIndex = 38
        Me.Button3.Text = "..."
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(860, 682)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 38
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'NOMBRETextBox
        '
        Me.NOMBRETextBox.BackColor = System.Drawing.Color.White
        Me.NOMBRETextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.NOMBRETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "NOMBRE", True))
        Me.NOMBRETextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NOMBRETextBox.Location = New System.Drawing.Point(107, 68)
        Me.NOMBRETextBox.Name = "NOMBRETextBox"
        Me.NOMBRETextBox.ReadOnly = True
        Me.NOMBRETextBox.Size = New System.Drawing.Size(284, 14)
        Me.NOMBRETextBox.TabIndex = 41
        Me.NOMBRETextBox.TabStop = False
        '
        'DameClientesActivosBindingSource
        '
        Me.DameClientesActivosBindingSource.DataMember = "DameClientesActivos"
        Me.DameClientesActivosBindingSource.DataSource = Me.DataSetEric
        '
        'CALLETextBox
        '
        Me.CALLETextBox.BackColor = System.Drawing.Color.White
        Me.CALLETextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CALLETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "CALLE", True))
        Me.CALLETextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CALLETextBox.Location = New System.Drawing.Point(107, 97)
        Me.CALLETextBox.Name = "CALLETextBox"
        Me.CALLETextBox.ReadOnly = True
        Me.CALLETextBox.Size = New System.Drawing.Size(284, 14)
        Me.CALLETextBox.TabIndex = 42
        Me.CALLETextBox.TabStop = False
        '
        'COLONIATextBox
        '
        Me.COLONIATextBox.BackColor = System.Drawing.Color.White
        Me.COLONIATextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.COLONIATextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "COLONIA", True))
        Me.COLONIATextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.COLONIATextBox.Location = New System.Drawing.Point(107, 125)
        Me.COLONIATextBox.Name = "COLONIATextBox"
        Me.COLONIATextBox.ReadOnly = True
        Me.COLONIATextBox.Size = New System.Drawing.Size(284, 14)
        Me.COLONIATextBox.TabIndex = 43
        Me.COLONIATextBox.TabStop = False
        '
        'NUMEROTextBox
        '
        Me.NUMEROTextBox.BackColor = System.Drawing.Color.White
        Me.NUMEROTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.NUMEROTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "NUMERO", True))
        Me.NUMEROTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NUMEROTextBox.Location = New System.Drawing.Point(428, 97)
        Me.NUMEROTextBox.Name = "NUMEROTextBox"
        Me.NUMEROTextBox.ReadOnly = True
        Me.NUMEROTextBox.Size = New System.Drawing.Size(172, 14)
        Me.NUMEROTextBox.TabIndex = 44
        Me.NUMEROTextBox.TabStop = False
        '
        'CIUDADTextBox
        '
        Me.CIUDADTextBox.BackColor = System.Drawing.Color.White
        Me.CIUDADTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CIUDADTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "CIUDAD", True))
        Me.CIUDADTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CIUDADTextBox.Location = New System.Drawing.Point(107, 158)
        Me.CIUDADTextBox.Name = "CIUDADTextBox"
        Me.CIUDADTextBox.ReadOnly = True
        Me.CIUDADTextBox.Size = New System.Drawing.Size(284, 14)
        Me.CIUDADTextBox.TabIndex = 45
        Me.CIUDADTextBox.TabStop = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(ConceptoLabel)
        Me.GroupBox3.Controls.Add(Me.ConceptoComboBox)
        Me.GroupBox3.Controls.Add(CONTRATOLabel)
        Me.GroupBox3.Controls.Add(Me.Button3)
        Me.GroupBox3.Controls.Add(CIUDADLabel)
        Me.GroupBox3.Controls.Add(Me.CIUDADTextBox)
        Me.GroupBox3.Controls.Add(NUMEROLabel)
        Me.GroupBox3.Controls.Add(Me.NOMBRETextBox)
        Me.GroupBox3.Controls.Add(Me.NUMEROTextBox)
        Me.GroupBox3.Controls.Add(NOMBRELabel)
        Me.GroupBox3.Controls.Add(Me.TextBox1)
        Me.GroupBox3.Controls.Add(Me.CALLETextBox)
        Me.GroupBox3.Controls.Add(COLONIALabel)
        Me.GroupBox3.Controls.Add(CALLELabel)
        Me.GroupBox3.Controls.Add(Me.COLONIATextBox)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(12, 36)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(984, 200)
        Me.GroupBox3.TabIndex = 46
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Datos del Cliente"
        '
        'ConceptoComboBox
        '
        Me.ConceptoComboBox.DataSource = Me.MuestraTipServEricBindingSource
        Me.ConceptoComboBox.DisplayMember = "Concepto"
        Me.ConceptoComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConceptoComboBox.FormattingEnabled = True
        Me.ConceptoComboBox.Location = New System.Drawing.Point(687, 43)
        Me.ConceptoComboBox.Name = "ConceptoComboBox"
        Me.ConceptoComboBox.Size = New System.Drawing.Size(276, 23)
        Me.ConceptoComboBox.TabIndex = 46
        Me.ConceptoComboBox.ValueMember = "Clv_TipSer"
        '
        'MuestraTipServEricBindingSource
        '
        Me.MuestraTipServEricBindingSource.DataMember = "MuestraTipServEric"
        Me.MuestraTipServEricBindingSource.DataSource = Me.DataSetEric
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Nothing
        Me.BindingNavigator1.CountItem = Nothing
        Me.BindingNavigator1.DeleteItem = Nothing
        Me.BindingNavigator1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton2})
        Me.BindingNavigator1.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigator1.MoveFirstItem = Nothing
        Me.BindingNavigator1.MoveLastItem = Nothing
        Me.BindingNavigator1.MoveNextItem = Nothing
        Me.BindingNavigator1.MovePreviousItem = Nothing
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Nothing
        Me.BindingNavigator1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.BindingNavigator1.Size = New System.Drawing.Size(1008, 25)
        Me.BindingNavigator1.TabIndex = 47
        Me.BindingNavigator1.TabStop = True
        Me.BindingNavigator1.Text = "BindingNavigator1"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton2.Image = CType(resources.GetObject("ToolStripButton2.Image"), System.Drawing.Image)
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(123, 22)
        Me.ToolStripButton2.Text = "&HACER CAMBIO"
        '
        'GroupBoxPosible
        '
        Me.GroupBoxPosible.Controls.Add(Me.DataGridViewPosible)
        Me.GroupBoxPosible.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBoxPosible.ForeColor = System.Drawing.Color.Red
        Me.GroupBoxPosible.Location = New System.Drawing.Point(505, 243)
        Me.GroupBoxPosible.Name = "GroupBoxPosible"
        Me.GroupBoxPosible.Size = New System.Drawing.Size(491, 416)
        Me.GroupBoxPosible.TabIndex = 38
        Me.GroupBoxPosible.TabStop = False
        Me.GroupBoxPosible.Text = "Posibles Cambios de Servicio"
        Me.GroupBoxPosible.Visible = False
        '
        'DataGridViewPosible
        '
        Me.DataGridViewPosible.AllowUserToAddRows = False
        Me.DataGridViewPosible.AllowUserToDeleteRows = False
        Me.DataGridViewPosible.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.DataGridViewPosible.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewPosible.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_ServicioAux, Me.ServicioAux, Me.MensualidadProp})
        Me.DataGridViewPosible.Location = New System.Drawing.Point(6, 20)
        Me.DataGridViewPosible.Name = "DataGridViewPosible"
        Me.DataGridViewPosible.ReadOnly = True
        Me.DataGridViewPosible.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridViewPosible.Size = New System.Drawing.Size(481, 390)
        Me.DataGridViewPosible.TabIndex = 38
        '
        'Clv_ServicioAux
        '
        Me.Clv_ServicioAux.DataPropertyName = "Clv_Servicio"
        Me.Clv_ServicioAux.HeaderText = "Clv_Servicio"
        Me.Clv_ServicioAux.Name = "Clv_ServicioAux"
        Me.Clv_ServicioAux.ReadOnly = True
        Me.Clv_ServicioAux.Visible = False
        '
        'ServicioAux
        '
        Me.ServicioAux.DataPropertyName = "Servicio"
        Me.ServicioAux.HeaderText = "Servicio"
        Me.ServicioAux.Name = "ServicioAux"
        Me.ServicioAux.ReadOnly = True
        Me.ServicioAux.Width = 250
        '
        'MensualidadProp
        '
        Me.MensualidadProp.DataPropertyName = "MensualidadProp"
        DataGridViewCellStyle2.Format = "C2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.MensualidadProp.DefaultCellStyle = DataGridViewCellStyle2
        Me.MensualidadProp.HeaderText = "Mensualidad Proporcional"
        Me.MensualidadProp.Name = "MensualidadProp"
        Me.MensualidadProp.ReadOnly = True
        Me.MensualidadProp.Width = 180
        '
        'GroupBoxActual
        '
        Me.GroupBoxActual.Controls.Add(Me.DataGridViewActual)
        Me.GroupBoxActual.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBoxActual.Location = New System.Drawing.Point(12, 243)
        Me.GroupBoxActual.Name = "GroupBoxActual"
        Me.GroupBoxActual.Size = New System.Drawing.Size(487, 416)
        Me.GroupBoxActual.TabIndex = 37
        Me.GroupBoxActual.TabStop = False
        Me.GroupBoxActual.Text = "Servicios Actuales del Cliente"
        Me.GroupBoxActual.Visible = False
        '
        'DataGridViewActual
        '
        Me.DataGridViewActual.AllowUserToAddRows = False
        Me.DataGridViewActual.AllowUserToDeleteRows = False
        Me.DataGridViewActual.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.DataGridViewActual.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewActual.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Contrato, Me.ContratoNet, Me.Clv_UnicaNet, Me.Clv_TipSer, Me.Clv_Servicio, Me.MacCablemodem, Me.Servicio})
        Me.DataGridViewActual.Location = New System.Drawing.Point(6, 20)
        Me.DataGridViewActual.Name = "DataGridViewActual"
        Me.DataGridViewActual.ReadOnly = True
        Me.DataGridViewActual.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders
        Me.DataGridViewActual.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridViewActual.Size = New System.Drawing.Size(475, 390)
        Me.DataGridViewActual.TabIndex = 37
        '
        'Contrato
        '
        Me.Contrato.DataPropertyName = "Contrato"
        Me.Contrato.HeaderText = "Contrato"
        Me.Contrato.Name = "Contrato"
        Me.Contrato.ReadOnly = True
        Me.Contrato.Visible = False
        '
        'ContratoNet
        '
        Me.ContratoNet.DataPropertyName = "ContratoNet"
        Me.ContratoNet.HeaderText = "ContratoNet"
        Me.ContratoNet.Name = "ContratoNet"
        Me.ContratoNet.ReadOnly = True
        Me.ContratoNet.Visible = False
        '
        'Clv_UnicaNet
        '
        Me.Clv_UnicaNet.DataPropertyName = "Clv_UnicaNet"
        Me.Clv_UnicaNet.HeaderText = "Clv_UnicaNet"
        Me.Clv_UnicaNet.Name = "Clv_UnicaNet"
        Me.Clv_UnicaNet.ReadOnly = True
        Me.Clv_UnicaNet.Visible = False
        '
        'Clv_TipSer
        '
        Me.Clv_TipSer.DataPropertyName = "Clv_TipSer"
        Me.Clv_TipSer.HeaderText = "Clv_TipSer"
        Me.Clv_TipSer.Name = "Clv_TipSer"
        Me.Clv_TipSer.ReadOnly = True
        Me.Clv_TipSer.Visible = False
        '
        'Clv_Servicio
        '
        Me.Clv_Servicio.DataPropertyName = "Clv_Servicio"
        Me.Clv_Servicio.HeaderText = "Clv_Servicio"
        Me.Clv_Servicio.Name = "Clv_Servicio"
        Me.Clv_Servicio.ReadOnly = True
        Me.Clv_Servicio.Visible = False
        '
        'MacCablemodem
        '
        Me.MacCablemodem.DataPropertyName = "MacCablemodem"
        Me.MacCablemodem.HeaderText = "Aparato"
        Me.MacCablemodem.Name = "MacCablemodem"
        Me.MacCablemodem.ReadOnly = True
        Me.MacCablemodem.Width = 150
        '
        'Servicio
        '
        Me.Servicio.DataPropertyName = "Servicio"
        Me.Servicio.HeaderText = "Servicio"
        Me.Servicio.Name = "Servicio"
        Me.Servicio.ReadOnly = True
        Me.Servicio.Width = 290
        '
        'DameServCteTableAdapter
        '
        Me.DameServCteTableAdapter.ClearBeforeFill = True
        '
        'DameServPosiblesCteTableAdapter
        '
        Me.DameServPosiblesCteTableAdapter.ClearBeforeFill = True
        '
        'DameClientesActivosTableAdapter
        '
        Me.DameClientesActivosTableAdapter.ClearBeforeFill = True
        '
        'MuestraTipServEricTableAdapter
        '
        Me.MuestraTipServEricTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'FrmCamServCte
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1008, 730)
        Me.Controls.Add(Me.GroupBoxPosible)
        Me.Controls.Add(Me.BindingNavigator1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.GroupBoxActual)
        Me.Controls.Add(Me.GroupBox3)
        Me.Name = "FrmCamServCte"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cambio de Servicios"
        CType(Me.DameServCteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameServPosiblesCteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameClientesActivosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.MuestraTipServEricBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        Me.GroupBoxPosible.ResumeLayout(False)
        CType(Me.DataGridViewPosible, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBoxActual.ResumeLayout(False)
        CType(Me.DataGridViewActual, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents DameServCteBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameServCteTableAdapter As sofTV.DataSetEricTableAdapters.DameServCteTableAdapter
    Friend WithEvents DameServPosiblesCteBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameServPosiblesCteTableAdapter As sofTV.DataSetEricTableAdapters.DameServPosiblesCteTableAdapter
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents DameClientesActivosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameClientesActivosTableAdapter As sofTV.DataSetEricTableAdapters.DameClientesActivosTableAdapter
    Friend WithEvents NOMBRETextBox As System.Windows.Forms.TextBox
    Friend WithEvents CALLETextBox As System.Windows.Forms.TextBox
    Friend WithEvents COLONIATextBox As System.Windows.Forms.TextBox
    Friend WithEvents NUMEROTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CIUDADTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents MuestraTipServEricBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipServEricTableAdapter As sofTV.DataSetEricTableAdapters.MuestraTipServEricTableAdapter
    Friend WithEvents ConceptoComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBoxPosible As System.Windows.Forms.GroupBox
    Friend WithEvents DataGridViewPosible As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBoxActual As System.Windows.Forms.GroupBox
    Friend WithEvents DataGridViewActual As System.Windows.Forms.DataGridView
    Friend WithEvents Clv_ServicioAux As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ServicioAux As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MensualidadProp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Contrato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ContratoNet As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_UnicaNet As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_TipSer As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Servicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MacCablemodem As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Servicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
