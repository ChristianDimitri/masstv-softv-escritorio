<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRelCablemodemClientesDigital
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.TreeView2 = New System.Windows.Forms.TreeView()
        Me.MacCableModem1 = New System.Windows.Forms.TextBox()
        Me.Contratonet1 = New System.Windows.Forms.TextBox()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.MacCableModem = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Contratonet = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.MUESTRACABLEMODEMSDELCLIporOpcionDIGBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACABLEMODEMSDELCLI_porOpcionDIGTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACABLEMODEMSDELCLI_porOpcionDIGTableAdapter()
        Me.MUESTRACONTDIGBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACONTDIGTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACONTDIGTableAdapter()
        Me.NUEIAPAR_SOLDIGBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NUEIAPAR_SOLDIGTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.NUEIAPAR_SOLDIGTableAdapter()
        Me.BorIAPAR_SOLBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorIAPAR_SOLTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.borIAPAR_SOLTableAdapter()
        Me.BORDetOrdSer_INTELIGENTEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BORDetOrdSer_INTELIGENTETableAdapter = New sofTV.NewSofTvDataSetTableAdapters.BORDetOrdSer_INTELIGENTETableAdapter()
        Me.MUESTRAICAM_porSOLDIGBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRAICAM_porSOLDIGTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRAICAM_porSOLDIGTableAdapter()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACABLEMODEMSDELCLIporOpcionDIGBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACONTDIGBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NUEIAPAR_SOLDIGBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorIAPAR_SOLBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BORDetOrdSer_INTELIGENTEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAICAM_porSOLDIGBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.SystemColors.Control
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(673, 329)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 4
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label4.Location = New System.Drawing.Point(503, 19)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(287, 16)
        Me.Label4.TabIndex = 37
        Me.Label4.Text = "Instalar y Asignar a estos Cablemodems"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label3.Location = New System.Drawing.Point(12, 19)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(270, 16)
        Me.Label3.TabIndex = 36
        Me.Label3.Text = "Cablemodems del Cliente por Asignar"
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.SystemColors.Control
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.Black
        Me.Button4.Location = New System.Drawing.Point(331, 153)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(143, 30)
        Me.Button4.TabIndex = 3
        Me.Button4.Text = "<< "
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.SystemColors.Control
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(331, 115)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(143, 30)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = " >>"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'TreeView2
        '
        Me.TreeView2.BackColor = System.Drawing.Color.Gainsboro
        Me.TreeView2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TreeView2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView2.ForeColor = System.Drawing.Color.Navy
        Me.TreeView2.Location = New System.Drawing.Point(497, 38)
        Me.TreeView2.Name = "TreeView2"
        Me.TreeView2.Size = New System.Drawing.Size(303, 269)
        Me.TreeView2.TabIndex = 25
        '
        'MacCableModem1
        '
        Me.MacCableModem1.Location = New System.Drawing.Point(528, 163)
        Me.MacCableModem1.Name = "MacCableModem1"
        Me.MacCableModem1.Size = New System.Drawing.Size(127, 20)
        Me.MacCableModem1.TabIndex = 33
        Me.MacCableModem1.TabStop = False
        '
        'Contratonet1
        '
        Me.Contratonet1.Location = New System.Drawing.Point(528, 137)
        Me.Contratonet1.Name = "Contratonet1"
        Me.Contratonet1.Size = New System.Drawing.Size(127, 20)
        Me.Contratonet1.TabIndex = 32
        Me.Contratonet1.TabStop = False
        '
        'TreeView1
        '
        Me.TreeView1.BackColor = System.Drawing.Color.Gainsboro
        Me.TreeView1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TreeView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView1.ForeColor = System.Drawing.Color.Navy
        Me.TreeView1.Location = New System.Drawing.Point(12, 38)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(292, 269)
        Me.TreeView1.TabIndex = 24
        Me.TreeView1.TabStop = False
        '
        'MacCableModem
        '
        Me.MacCableModem.Location = New System.Drawing.Point(161, 156)
        Me.MacCableModem.Name = "MacCableModem"
        Me.MacCableModem.Size = New System.Drawing.Size(109, 20)
        Me.MacCableModem.TabIndex = 31
        Me.MacCableModem.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(178, 105)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(92, 13)
        Me.Label2.TabIndex = 30
        Me.Label2.Text = "Mac Cablemodem"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(33, 74)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 13)
        Me.Label1.TabIndex = 29
        Me.Label1.Text = "ContratoNet"
        '
        'Contratonet
        '
        Me.Contratonet.Location = New System.Drawing.Point(80, 98)
        Me.Contratonet.Name = "Contratonet"
        Me.Contratonet.Size = New System.Drawing.Size(127, 20)
        Me.Contratonet.TabIndex = 28
        Me.Contratonet.TabStop = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.SystemColors.Control
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(331, 79)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(143, 30)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "< "
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.SystemColors.Control
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(331, 43)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(143, 30)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = " >"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MUESTRACABLEMODEMSDELCLIporOpcionDIGBindingSource
        '
        Me.MUESTRACABLEMODEMSDELCLIporOpcionDIGBindingSource.DataMember = "MUESTRACABLEMODEMSDELCLI_porOpcionDIG"
        Me.MUESTRACABLEMODEMSDELCLIporOpcionDIGBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRACABLEMODEMSDELCLI_porOpcionDIGTableAdapter
        '
        Me.MUESTRACABLEMODEMSDELCLI_porOpcionDIGTableAdapter.ClearBeforeFill = True
        '
        'MUESTRACONTDIGBindingSource
        '
        Me.MUESTRACONTDIGBindingSource.DataMember = "MUESTRACONTDIG"
        Me.MUESTRACONTDIGBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRACONTDIGTableAdapter
        '
        Me.MUESTRACONTDIGTableAdapter.ClearBeforeFill = True
        '
        'NUEIAPAR_SOLDIGBindingSource
        '
        Me.NUEIAPAR_SOLDIGBindingSource.DataMember = "NUEIAPAR_SOLDIG"
        Me.NUEIAPAR_SOLDIGBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NUEIAPAR_SOLDIGTableAdapter
        '
        Me.NUEIAPAR_SOLDIGTableAdapter.ClearBeforeFill = True
        '
        'BorIAPAR_SOLBindingSource
        '
        Me.BorIAPAR_SOLBindingSource.DataMember = "borIAPAR_SOL"
        Me.BorIAPAR_SOLBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'BorIAPAR_SOLTableAdapter
        '
        Me.BorIAPAR_SOLTableAdapter.ClearBeforeFill = True
        '
        'BORDetOrdSer_INTELIGENTEBindingSource
        '
        Me.BORDetOrdSer_INTELIGENTEBindingSource.DataMember = "BORDetOrdSer_INTELIGENTE"
        Me.BORDetOrdSer_INTELIGENTEBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'BORDetOrdSer_INTELIGENTETableAdapter
        '
        Me.BORDetOrdSer_INTELIGENTETableAdapter.ClearBeforeFill = True
        '
        'MUESTRAICAM_porSOLDIGBindingSource
        '
        Me.MUESTRAICAM_porSOLDIGBindingSource.DataMember = "MUESTRAICAM_porSOLDIG"
        Me.MUESTRAICAM_porSOLDIGBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRAICAM_porSOLDIGTableAdapter
        '
        Me.MUESTRAICAM_porSOLDIGTableAdapter.ClearBeforeFill = True
        '
        'FrmRelCablemodemClientesDigital
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(829, 372)
        Me.Controls.Add(Me.TreeView2)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.MacCableModem1)
        Me.Controls.Add(Me.Contratonet1)
        Me.Controls.Add(Me.TreeView1)
        Me.Controls.Add(Me.MacCableModem)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Contratonet)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Name = "FrmRelCablemodemClientesDigital"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Relación Cablemodem Cliente Digital"
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACABLEMODEMSDELCLIporOpcionDIGBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACONTDIGBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NUEIAPAR_SOLDIGBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorIAPAR_SOLBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BORDetOrdSer_INTELIGENTEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAICAM_porSOLDIGBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents TreeView2 As System.Windows.Forms.TreeView
    Friend WithEvents MacCableModem1 As System.Windows.Forms.TextBox
    Friend WithEvents Contratonet1 As System.Windows.Forms.TextBox
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents MacCableModem As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Contratonet As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents MUESTRACABLEMODEMSDELCLIporOpcionDIGBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACABLEMODEMSDELCLI_porOpcionDIGTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACABLEMODEMSDELCLI_porOpcionDIGTableAdapter
    Friend WithEvents MUESTRACONTDIGBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACONTDIGTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACONTDIGTableAdapter
    Friend WithEvents NUEIAPAR_SOLDIGBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NUEIAPAR_SOLDIGTableAdapter As sofTV.NewSofTvDataSetTableAdapters.NUEIAPAR_SOLDIGTableAdapter
    Friend WithEvents BorIAPAR_SOLBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorIAPAR_SOLTableAdapter As sofTV.NewSofTvDataSetTableAdapters.borIAPAR_SOLTableAdapter
    Friend WithEvents BORDetOrdSer_INTELIGENTEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BORDetOrdSer_INTELIGENTETableAdapter As sofTV.NewSofTvDataSetTableAdapters.BORDetOrdSer_INTELIGENTETableAdapter
    Friend WithEvents MUESTRAICAM_porSOLDIGBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAICAM_porSOLDIGTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRAICAM_porSOLDIGTableAdapter
End Class
