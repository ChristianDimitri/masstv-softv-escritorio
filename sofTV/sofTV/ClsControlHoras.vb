﻿Imports System.Collections.Generic
Public Class ControlHoras
    Private _ID As Integer
    Public Property ID As Integer
        Get
            Return _ID
        End Get
        Set(ByVal value As Integer)
            _ID = value
        End Set
    End Property

    Private _Orden As Integer
    Public Property Orden As Integer
        Get
            Return _Orden
        End Get
        Set(ByVal value As Integer)
            _Orden = value
        End Set
    End Property

    Private _FechaInicio As Date
    Public Property FechaInicio As Date
        Get
            Return _FechaInicio
        End Get
        Set(ByVal value As Date)
            _FechaInicio = value
        End Set
    End Property

    Private _FechaFin As Date
    Public Property FechaFin As Date
        Get
            Return _FechaFin
        End Get
        Set(ByVal value As Date)
            _FechaFin = value
        End Set
    End Property

    Private _UsuarioID As Integer
    Public Property UsuarioID As Integer
        Get
            Return _UsuarioID
        End Get
        Set(ByVal value As Integer)
            _UsuarioID = value
        End Set
    End Property

#Region "Constructor"
    Public Sub New()
        _ID = 0
        _Orden = 0
    End Sub

    Public Sub New(ByVal pID As Integer, ByVal pOrden As Integer)
        _ID = pID
        _Orden = pOrden
    End Sub


    Public Sub New(ByVal pControlID As Integer)
        Try
            _ID = 0
            _Orden = 0
            BaseII.limpiaParametros()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Metodos"
    Public Function Insertar() As Integer
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@ControlHorasID", ParameterDirection.Output, SqlDbType.BigInt, CObj(Me.ID))
            BaseII.CreateMyParameter("@ControlID", SqlDbType.BigInt, 32, ParameterDirection.Output, CObj(Me._ID))
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, CObj(Me._Orden))
            BaseII.CreateMyParameter("@FechaInicio", SqlDbType.DateTime, CObj(Me._FechaInicio))
            BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, CObj(Me._FechaFin))
            BaseII.CreateMyParameter("@UsuarioID", SqlDbType.BigInt, CObj(Me._UsuarioID))
            Dim Diccionario As Dictionary(Of String, Object) = BaseII.ProcedimientoOutPut("uspInsertaControlHorasOrdenes")
            Me._ID = CInt(Diccionario("@ControlID"))
            Return Me._ID
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Modificar() As Integer
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@ControlID", SqlDbType.BigInt, CObj(Me._ID))
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, CObj(Me._Orden))
            BaseII.CreateMyParameter("@FechaInicio", SqlDbType.DateTime, CObj(Me._FechaInicio))
            BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, CObj(Me._FechaFin))
            BaseII.CreateMyParameter("@UsuarioID", SqlDbType.BigInt, CObj(Me._UsuarioID))
            BaseII.Inserta("uspModificaControlHorasOrdenes")

        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function listar() As DataTable
        Try
            BaseII.limpiaParametros()
            Return BaseII.ConsultaDT("uspGetControlHorasOrdenes")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ListarTecnicos() As DataTable
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLV_ORDEN", SqlDbType.BigInt, CObj(Me._Orden))
            Return BaseII.ConsultaDT("MuestraRelOrdenesTecnicos")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ListarCuadrilla(ByVal pClaveTecnico As Integer) As DataTable
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLV_ORDEN", SqlDbType.BigInt, CObj(Me._Orden))
            BaseII.CreateMyParameter("@CLV_TECNICO", SqlDbType.BigInt, CObj(pClaveTecnico))
            Return BaseII.ConsultaDT("MuestraRelOrdSerCuadrilla")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#End Region
End Class
