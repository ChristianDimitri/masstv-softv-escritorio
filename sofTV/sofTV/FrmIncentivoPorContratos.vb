﻿Imports sofTV.BAL
Public Class FrmIncentivoPorContratos

    Public IdIncentivoPorContratos As Integer
    Private Sub FrmIncentivoPorContratos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim serv As Servicio = Servicio.GetOne(IdIncentivoPorContratos)
        txtDescripcion.Text = serv.Descripcion
        If (serv.IncentivoPorVentas.HasValue) Then
            txtIncentivo.Text = serv.IncentivoPorVentas
        End If

    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If (IsNumeric(txtIncentivo.Text)) Then
            Dim serv As Servicio = Servicio.GetOne(IdIncentivoPorContratos)
            serv.IncentivoPorVentas = Decimal.Parse(txtIncentivo.Text)
            serv.Edit()
            Me.Close()
        Else
            MessageBox.Show("Valor No Valido Para Incentivo")

        End If
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
End Class