﻿Imports sofTV.BAL

Public Class FrmMetaIncentivo
    Public IdMetaIncentivo As Integer

    Private Sub FrmMetaIncentivo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim serv As MetaIncentivo = MetaIncentivo.GetOne(IdMetaIncentivo)
        txtDescripcion.Text = serv.Descripcion
        If (serv.Servicios.HasValue) Then
            txtServicios.Text = serv.Servicios
        End If

    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If (IsNumeric(txtServicios.Text)) Then
            Dim serv As MetaIncentivo = MetaIncentivo.GetOne(IdMetaIncentivo)
            serv.Servicios = Integer.Parse(txtServicios.Text)
            serv.Edit()
            Me.Close()
        Else
            MessageBox.Show("Valor No Valido Para Incentivo")

        End If
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
End Class