<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPolizaDiario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Clv_llaveLabel As System.Windows.Forms.Label
        Dim Clv_TipSerLabel As System.Windows.Forms.Label
        Dim CuentaLabel As System.Windows.Forms.Label
        Dim PosicionLabel As System.Windows.Forms.Label
        Dim DescripcionLabel As System.Windows.Forms.Label
        Dim Se_CapturaLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmPolizaDiario))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.TabPage3 = New System.Windows.Forms.TabPage
        Me.TabPage4 = New System.Windows.Forms.TabPage
        Me.TabPage5 = New System.Windows.Forms.TabPage
        Me.DataSetEdgarRev2 = New sofTV.DataSetEdgarRev2
        Me.Consultar_Tabla_CategoriaServiciosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Consultar_Tabla_CategoriaServiciosTableAdapter = New sofTV.DataSetEdgarRev2TableAdapters.Consultar_Tabla_CategoriaServiciosTableAdapter
        Me.Consultar_Tabla_CategoriaServiciosBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.Consultar_Tabla_CategoriaServiciosBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton
        Me.Clv_llaveTextBox = New System.Windows.Forms.TextBox
        Me.Clv_TipSerTextBox = New System.Windows.Forms.TextBox
        Me.CuentaTextBox = New System.Windows.Forms.TextBox
        Me.PosicionTextBox = New System.Windows.Forms.TextBox
        Me.DescripcionTextBox = New System.Windows.Forms.TextBox
        Me.Se_CapturaCheckBox = New System.Windows.Forms.CheckBox
        Me.Consultar_Tabla_CategoriaServiciosDataGridView = New System.Windows.Forms.DataGridView
        Me.ToolStripContainer2 = New System.Windows.Forms.ToolStripContainer
        Me.TabPage6 = New System.Windows.Forms.TabPage
        Me.Button2 = New System.Windows.Forms.Button
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewCheckBoxColumn1 = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Clv_llaveLabel = New System.Windows.Forms.Label
        Clv_TipSerLabel = New System.Windows.Forms.Label
        CuentaLabel = New System.Windows.Forms.Label
        PosicionLabel = New System.Windows.Forms.Label
        DescripcionLabel = New System.Windows.Forms.Label
        Se_CapturaLabel = New System.Windows.Forms.Label
        Me.TabControl1.SuspendLayout()
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consultar_Tabla_CategoriaServiciosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consultar_Tabla_CategoriaServiciosBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Consultar_Tabla_CategoriaServiciosBindingNavigator.SuspendLayout()
        CType(Me.Consultar_Tabla_CategoriaServiciosDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStripContainer2.TopToolStripPanel.SuspendLayout()
        Me.ToolStripContainer2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Clv_llaveLabel
        '
        Clv_llaveLabel.AutoSize = True
        Clv_llaveLabel.Location = New System.Drawing.Point(67, 376)
        Clv_llaveLabel.Name = "Clv_llaveLabel"
        Clv_llaveLabel.Size = New System.Drawing.Size(50, 13)
        Clv_llaveLabel.TabIndex = 0
        Clv_llaveLabel.Text = "Clv llave:"
        '
        'Clv_TipSerLabel
        '
        Clv_TipSerLabel.AutoSize = True
        Clv_TipSerLabel.Location = New System.Drawing.Point(67, 406)
        Clv_TipSerLabel.Name = "Clv_TipSerLabel"
        Clv_TipSerLabel.Size = New System.Drawing.Size(62, 13)
        Clv_TipSerLabel.TabIndex = 2
        Clv_TipSerLabel.Text = "Clv Tip Ser:"
        '
        'CuentaLabel
        '
        CuentaLabel.AutoSize = True
        CuentaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CuentaLabel.Location = New System.Drawing.Point(23, 120)
        CuentaLabel.Name = "CuentaLabel"
        CuentaLabel.Size = New System.Drawing.Size(56, 15)
        CuentaLabel.TabIndex = 4
        CuentaLabel.Text = "Cuenta:"
        '
        'PosicionLabel
        '
        PosicionLabel.AutoSize = True
        PosicionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PosicionLabel.Location = New System.Drawing.Point(23, 150)
        PosicionLabel.Name = "PosicionLabel"
        PosicionLabel.Size = New System.Drawing.Size(66, 15)
        PosicionLabel.TabIndex = 6
        PosicionLabel.Text = "Posicion:"
        '
        'DescripcionLabel
        '
        DescripcionLabel.AutoSize = True
        DescripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescripcionLabel.Location = New System.Drawing.Point(23, 180)
        DescripcionLabel.Name = "DescripcionLabel"
        DescripcionLabel.Size = New System.Drawing.Size(87, 15)
        DescripcionLabel.TabIndex = 8
        DescripcionLabel.Text = "Descripcion:"
        '
        'Se_CapturaLabel
        '
        Se_CapturaLabel.AutoSize = True
        Se_CapturaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Se_CapturaLabel.Location = New System.Drawing.Point(23, 212)
        Se_CapturaLabel.Name = "Se_CapturaLabel"
        Se_CapturaLabel.Size = New System.Drawing.Size(82, 15)
        Se_CapturaLabel.TabIndex = 10
        Se_CapturaLabel.Text = "Se Captura:"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(12, 31)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(992, 32)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.Gainsboro
        Me.TabPage1.Location = New System.Drawing.Point(4, 27)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(984, 1)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Conceptos Fijos"
        '
        'TabPage2
        '
        Me.TabPage2.Location = New System.Drawing.Point(4, 27)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(984, 1)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Servicio Basico Tv"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'TabPage3
        '
        Me.TabPage3.Location = New System.Drawing.Point(4, 27)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(984, 1)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Canales Premium o Digital"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'TabPage4
        '
        Me.TabPage4.Location = New System.Drawing.Point(4, 27)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(984, 1)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Internet"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'TabPage5
        '
        Me.TabPage5.Location = New System.Drawing.Point(4, 27)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(984, 1)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "Material"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'DataSetEdgarRev2
        '
        Me.DataSetEdgarRev2.DataSetName = "DataSetEdgarRev2"
        Me.DataSetEdgarRev2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Consultar_Tabla_CategoriaServiciosBindingSource
        '
        Me.Consultar_Tabla_CategoriaServiciosBindingSource.DataMember = "Consultar_Tabla_CategoriaServicios"
        Me.Consultar_Tabla_CategoriaServiciosBindingSource.DataSource = Me.DataSetEdgarRev2
        '
        'Consultar_Tabla_CategoriaServiciosTableAdapter
        '
        Me.Consultar_Tabla_CategoriaServiciosTableAdapter.ClearBeforeFill = True
        '
        'Consultar_Tabla_CategoriaServiciosBindingNavigator
        '
        Me.Consultar_Tabla_CategoriaServiciosBindingNavigator.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.Consultar_Tabla_CategoriaServiciosBindingNavigator.BindingSource = Me.Consultar_Tabla_CategoriaServiciosBindingSource
        Me.Consultar_Tabla_CategoriaServiciosBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.Consultar_Tabla_CategoriaServiciosBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.Consultar_Tabla_CategoriaServiciosBindingNavigator.Dock = System.Windows.Forms.DockStyle.None
        Me.Consultar_Tabla_CategoriaServiciosBindingNavigator.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Consultar_Tabla_CategoriaServiciosBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.BindingNavigatorAddNewItem, Me.BindingNavigatorDeleteItem, Me.Consultar_Tabla_CategoriaServiciosBindingNavigatorSaveItem})
        Me.Consultar_Tabla_CategoriaServiciosBindingNavigator.Location = New System.Drawing.Point(3, 0)
        Me.Consultar_Tabla_CategoriaServiciosBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.Consultar_Tabla_CategoriaServiciosBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.Consultar_Tabla_CategoriaServiciosBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.Consultar_Tabla_CategoriaServiciosBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.Consultar_Tabla_CategoriaServiciosBindingNavigator.Name = "Consultar_Tabla_CategoriaServiciosBindingNavigator"
        Me.Consultar_Tabla_CategoriaServiciosBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.Consultar_Tabla_CategoriaServiciosBindingNavigator.Size = New System.Drawing.Size(526, 25)
        Me.Consultar_Tabla_CategoriaServiciosBindingNavigator.TabIndex = 1
        Me.Consultar_Tabla_CategoriaServiciosBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(118, 22)
        Me.BindingNavigatorAddNewItem.Text = "Agregar nuevo"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(48, 22)
        Me.BindingNavigatorCountItem.Text = "de {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Número total de elementos"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(74, 22)
        Me.BindingNavigatorDeleteItem.Text = "Eliminar"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Mover primero"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Mover anterior"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Posición"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 21)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Posición actual"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Mover siguiente"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Mover último"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'Consultar_Tabla_CategoriaServiciosBindingNavigatorSaveItem
        '
        Me.Consultar_Tabla_CategoriaServiciosBindingNavigatorSaveItem.Image = CType(resources.GetObject("Consultar_Tabla_CategoriaServiciosBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.Consultar_Tabla_CategoriaServiciosBindingNavigatorSaveItem.Name = "Consultar_Tabla_CategoriaServiciosBindingNavigatorSaveItem"
        Me.Consultar_Tabla_CategoriaServiciosBindingNavigatorSaveItem.Size = New System.Drawing.Size(114, 22)
        Me.Consultar_Tabla_CategoriaServiciosBindingNavigatorSaveItem.Text = "Guardar datos"
        '
        'Clv_llaveTextBox
        '
        Me.Clv_llaveTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consultar_Tabla_CategoriaServiciosBindingSource, "Clv_llave", True))
        Me.Clv_llaveTextBox.Location = New System.Drawing.Point(176, 373)
        Me.Clv_llaveTextBox.Name = "Clv_llaveTextBox"
        Me.Clv_llaveTextBox.Size = New System.Drawing.Size(277, 20)
        Me.Clv_llaveTextBox.TabIndex = 1
        '
        'Clv_TipSerTextBox
        '
        Me.Clv_TipSerTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consultar_Tabla_CategoriaServiciosBindingSource, "Clv_TipSer", True))
        Me.Clv_TipSerTextBox.Location = New System.Drawing.Point(176, 403)
        Me.Clv_TipSerTextBox.Name = "Clv_TipSerTextBox"
        Me.Clv_TipSerTextBox.Size = New System.Drawing.Size(49, 20)
        Me.Clv_TipSerTextBox.TabIndex = 3
        '
        'CuentaTextBox
        '
        Me.CuentaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CuentaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consultar_Tabla_CategoriaServiciosBindingSource, "Cuenta", True))
        Me.CuentaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CuentaTextBox.Location = New System.Drawing.Point(132, 117)
        Me.CuentaTextBox.Name = "CuentaTextBox"
        Me.CuentaTextBox.Size = New System.Drawing.Size(170, 21)
        Me.CuentaTextBox.TabIndex = 5
        '
        'PosicionTextBox
        '
        Me.PosicionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PosicionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consultar_Tabla_CategoriaServiciosBindingSource, "Posicion", True))
        Me.PosicionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PosicionTextBox.Location = New System.Drawing.Point(132, 147)
        Me.PosicionTextBox.Name = "PosicionTextBox"
        Me.PosicionTextBox.Size = New System.Drawing.Size(49, 21)
        Me.PosicionTextBox.TabIndex = 7
        '
        'DescripcionTextBox
        '
        Me.DescripcionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DescripcionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consultar_Tabla_CategoriaServiciosBindingSource, "Descripcion", True))
        Me.DescripcionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionTextBox.Location = New System.Drawing.Point(132, 177)
        Me.DescripcionTextBox.Name = "DescripcionTextBox"
        Me.DescripcionTextBox.Size = New System.Drawing.Size(511, 21)
        Me.DescripcionTextBox.TabIndex = 9
        '
        'Se_CapturaCheckBox
        '
        Me.Se_CapturaCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.Consultar_Tabla_CategoriaServiciosBindingSource, "Se_Captura", True))
        Me.Se_CapturaCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Se_CapturaCheckBox.Location = New System.Drawing.Point(132, 207)
        Me.Se_CapturaCheckBox.Name = "Se_CapturaCheckBox"
        Me.Se_CapturaCheckBox.Size = New System.Drawing.Size(104, 24)
        Me.Se_CapturaCheckBox.TabIndex = 11
        '
        'Consultar_Tabla_CategoriaServiciosDataGridView
        '
        Me.Consultar_Tabla_CategoriaServiciosDataGridView.AllowUserToOrderColumns = True
        Me.Consultar_Tabla_CategoriaServiciosDataGridView.AutoGenerateColumns = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Consultar_Tabla_CategoriaServiciosDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.Consultar_Tabla_CategoriaServiciosDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn4, Me.DataGridViewCheckBoxColumn1, Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2})
        Me.Consultar_Tabla_CategoriaServiciosDataGridView.DataSource = Me.Consultar_Tabla_CategoriaServiciosBindingSource
        Me.Consultar_Tabla_CategoriaServiciosDataGridView.Location = New System.Drawing.Point(19, 328)
        Me.Consultar_Tabla_CategoriaServiciosDataGridView.Name = "Consultar_Tabla_CategoriaServiciosDataGridView"
        Me.Consultar_Tabla_CategoriaServiciosDataGridView.Size = New System.Drawing.Size(978, 325)
        Me.Consultar_Tabla_CategoriaServiciosDataGridView.TabIndex = 12
        '
        'ToolStripContainer2
        '
        '
        'ToolStripContainer2.ContentPanel
        '
        Me.ToolStripContainer2.ContentPanel.Size = New System.Drawing.Size(978, 7)
        Me.ToolStripContainer2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripContainer2.Location = New System.Drawing.Point(19, 279)
        Me.ToolStripContainer2.Name = "ToolStripContainer2"
        Me.ToolStripContainer2.Size = New System.Drawing.Size(978, 32)
        Me.ToolStripContainer2.TabIndex = 2
        Me.ToolStripContainer2.Text = "ToolStripContainer2"
        '
        'ToolStripContainer2.TopToolStripPanel
        '
        Me.ToolStripContainer2.TopToolStripPanel.Controls.Add(Me.Consultar_Tabla_CategoriaServiciosBindingNavigator)
        '
        'TabPage6
        '
        Me.TabPage6.Location = New System.Drawing.Point(4, 22)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage6.Size = New System.Drawing.Size(253, 275)
        Me.TabPage6.TabIndex = 0
        Me.TabPage6.Text = "TabPage6"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(861, 673)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(136, 36)
        Me.Button2.TabIndex = 13
        Me.Button2.Text = "&CERRAR"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Cuenta"
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridViewTextBoxColumn3.HeaderText = "Cuenta"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Width = 150
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Descripcion"
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn5.DefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewTextBoxColumn5.HeaderText = "Descripcion"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Width = 350
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Posicion"
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridViewTextBoxColumn4.HeaderText = "Posicion"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        '
        'DataGridViewCheckBoxColumn1
        '
        Me.DataGridViewCheckBoxColumn1.DataPropertyName = "Se_Captura"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.NullValue = False
        Me.DataGridViewCheckBoxColumn1.DefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridViewCheckBoxColumn1.HeaderText = "Se_Captura"
        Me.DataGridViewCheckBoxColumn1.Name = "DataGridViewCheckBoxColumn1"
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Clv_llave"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Clv_llave"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 5
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Clv_TipSer"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Clv_TipSer"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Width = 5
        '
        'FrmPolizaDiario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1016, 741)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.ToolStripContainer2)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Consultar_Tabla_CategoriaServiciosDataGridView)
        Me.Controls.Add(Clv_llaveLabel)
        Me.Controls.Add(Me.Clv_llaveTextBox)
        Me.Controls.Add(Me.Se_CapturaCheckBox)
        Me.Controls.Add(Clv_TipSerLabel)
        Me.Controls.Add(Se_CapturaLabel)
        Me.Controls.Add(Me.Clv_TipSerTextBox)
        Me.Controls.Add(Me.DescripcionTextBox)
        Me.Controls.Add(CuentaLabel)
        Me.Controls.Add(DescripcionLabel)
        Me.Controls.Add(Me.CuentaTextBox)
        Me.Controls.Add(Me.PosicionTextBox)
        Me.Controls.Add(PosicionLabel)
        Me.MaximizeBox = False
        Me.Name = "FrmPolizaDiario"
        Me.Text = "Poliza"
        Me.TabControl1.ResumeLayout(False)
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consultar_Tabla_CategoriaServiciosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consultar_Tabla_CategoriaServiciosBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Consultar_Tabla_CategoriaServiciosBindingNavigator.ResumeLayout(False)
        Me.Consultar_Tabla_CategoriaServiciosBindingNavigator.PerformLayout()
        CType(Me.Consultar_Tabla_CategoriaServiciosDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStripContainer2.TopToolStripPanel.ResumeLayout(False)
        Me.ToolStripContainer2.TopToolStripPanel.PerformLayout()
        Me.ToolStripContainer2.ResumeLayout(False)
        Me.ToolStripContainer2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents DataSetEdgarRev2 As sofTV.DataSetEdgarRev2
    Friend WithEvents Consultar_Tabla_CategoriaServiciosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consultar_Tabla_CategoriaServiciosTableAdapter As sofTV.DataSetEdgarRev2TableAdapters.Consultar_Tabla_CategoriaServiciosTableAdapter
    Friend WithEvents Consultar_Tabla_CategoriaServiciosBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Consultar_Tabla_CategoriaServiciosBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Consultar_Tabla_CategoriaServiciosDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents Clv_llaveTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_TipSerTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CuentaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PosicionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DescripcionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Se_CapturaCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents ToolStripContainer2 As System.Windows.Forms.ToolStripContainer
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn1 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
