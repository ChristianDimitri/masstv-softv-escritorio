Imports System.Data.SqlClient

Public Class FrmrRelPaquetesdelClienteDigital
    Private eContratoNet As Long = 0
    ''
    Public Sub CREAARBOL()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim I As Integer = 0
            Dim X As Integer = 0
            Me.MUESTRACABLEMODEMSDELCLI_porOpcionDIGTableAdapter.Connection = CON
            Me.MUESTRACABLEMODEMSDELCLI_porOpcionDIGTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACABLEMODEMSDELCLI_porOpcionDIG, New System.Nullable(Of Long)(CType(Contrato, Long)), "P", 14)
            Dim FilaRow As DataRow
            Dim FilacontNet As DataRow
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.MUESTRACABLEMODEMSDELCLI_porOpcionDIG.Rows
                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                Me.TreeView1.Nodes.Add(Trim(FilaRow("CONTRATONET").ToString()), Trim(FilaRow("MACCABLEMODEM").ToString()))
                Me.TreeView1.Nodes(I).Tag = Trim(FilaRow("CONTRATONET").ToString())
                If GLOTRABAJO = "IPAQD" Or GLOTRABAJO = "IPAQD6" Then
                    Me.MUESTRACONTDIG_PorOpcionTableAdapter.Connection = CON
                    Me.MUESTRACONTDIG_PorOpcionTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTDIG_PorOpcion, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)), "", 1, gloClv_Orden, GloDetClave)
                ElseIf GLOTRABAJO = "BPAQD" Or GLOTRABAJO = "BPAQD6" Then
                    Me.MUESTRACONTDIG_PorOpcionTableAdapter.Connection = CON
                    Me.MUESTRACONTDIG_PorOpcionTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTDIG_PorOpcion, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)), "", 10, gloClv_Orden, GloDetClave)
                ElseIf GLOTRABAJO = "DPAQD" Or GLOTRABAJO = "DPAQD6" Then
                    Me.MUESTRACONTDIG_PorOpcionTableAdapter.Connection = CON
                    Me.MUESTRACONTDIG_PorOpcionTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTDIG_PorOpcion, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)), "", 20, gloClv_Orden, GloDetClave)
                ElseIf GLOTRABAJO = "RPAQD" Or GLOTRABAJO = "RPAQD6" Then
                    Me.MUESTRACONTDIG_PorOpcionTableAdapter.Connection = CON
                    Me.MUESTRACONTDIG_PorOpcionTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTDIG_PorOpcion, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)), "", 21, gloClv_Orden, GloDetClave)
                End If
                Me.TreeView1.Nodes(I).ForeColor = Color.Black
                For Each FilacontNet In Me.NewSofTvDataSet.MUESTRACONTDIG_PorOpcion.Rows
                    Me.TreeView1.Nodes(I).Nodes.Add(Trim(FilacontNet("CLV_UNICANET").ToString()), Trim(FilacontNet("DESCRIPCION").ToString()) & " : " & Trim(FilacontNet("STATUS").ToString()))
                    Me.TreeView1.Nodes(I).Nodes(X).Tag = Trim(FilacontNet("CLV_UNICANET").ToString())
                    If Trim(FilacontNet("STATUS").ToString()) = "Suspendido" Then
                        Me.TreeView1.Nodes(I).Nodes(X).ForeColor = Color.Olive
                    ElseIf Trim(FilacontNet("STATUS").ToString()) = "Instalado" Or Trim(FilacontNet("STATUS").ToString()) = "Contratado" Then
                        Me.TreeView1.Nodes(I).Nodes(X).ForeColor = Color.Navy
                    Else
                        Me.TreeView1.Nodes(I).Nodes(X).ForeColor = Color.Red
                    End If
                    X += 1
                Next
                I += 1
            Next
            CON.Close()
            Me.TreeView1.ExpandAll()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Public Sub CREAARBOL2()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim I As Integer = 0
            Dim X As Integer = 0
            Me.MUESTRAIPAQD_porSOLDigTableAdapter.Connection = CON
            Me.MUESTRAIPAQD_porSOLDigTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRAIPAQD_porSOLDig, GloDetClave, gloClv_Orden, 0)
            Dim FilaRow As DataRow
            Dim FilacontNet As DataRow
            Me.TreeView2.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.MUESTRAIPAQD_porSOLDig.Rows
                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                Me.TreeView2.Nodes.Add(Trim(FilaRow("CONTRATONET").ToString()), Trim(FilaRow("MACCABLEMODEM").ToString()))
                Me.TreeView2.Nodes(I).Tag = Trim(FilaRow("CONTRATONET").ToString())
                Me.MUESTRACONTDIG_PorOpcionTableAdapter.Connection = CON
                Me.MUESTRACONTDIG_PorOpcionTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTDIG_PorOpcion, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)), "", 2, gloClv_Orden, GloDetClave)
                Me.TreeView2.Nodes(I).ForeColor = Color.Black
                For Each FilacontNet In Me.NewSofTvDataSet.MUESTRACONTDIG_PorOpcion.Rows
                    Me.TreeView2.Nodes(I).Nodes.Add(Trim(FilacontNet("CLV_UNICANET").ToString()), Trim(FilacontNet("DESCRIPCION").ToString()) & " : " & Trim(FilacontNet("STATUS").ToString()))
                    Me.TreeView2.Nodes(I).Nodes(X).Tag = Trim(FilacontNet("CLV_UNICANET").ToString())
                    If Trim(FilacontNet("STATUS").ToString()) = "Suspendido" Then
                        Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Olive
                    ElseIf Trim(FilacontNet("STATUS").ToString()) = "Instalado" Or Trim(FilacontNet("STATUS").ToString()) = "Contratado" Then
                        Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Navy
                    Else
                        Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Red
                    End If
                    X += 1
                Next
                I += 1
            Next
            CON.Close()
            Me.TreeView2.ExpandAll()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click

        Me.Close()
    End Sub

    Private Sub FrmrRelPaquetesdelCliente_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.BORDetOrdSer_INTELIGENTETableAdapter.Connection = CON
        Me.BORDetOrdSer_INTELIGENTETableAdapter.Fill(Me.NewSofTvDataSet.BORDetOrdSer_INTELIGENTE, New System.Nullable(Of Long)(CType(GloDetClave, Long)))
        CON.Close()
        GloBndTrabajo = True
        GloBloqueaDetalle = True
    End Sub

    'Private Sub FrmrRelPaquetesdelCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    'End Sub

    Private Sub TreeView1_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView1.AfterSelect
        Try
            If e.Node.Level = 0 Then
                If IsNumeric(e.Node.Tag) = True Then
                    'Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))
                    Contratonet.Text = e.Node.Tag
                    MacCableModem.Text = e.Node.Text
                    Clv_Unicanet.Text = 0
                    '    Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))

                    'Else
                    '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                    'End If
                    'Else
                    'If IsNumeric(e.Node.Tag) Then
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                    'Else
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                    'End If
                End If
            Else
                If IsNumeric(e.Node.Tag) = True Then
                    'Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))
                    Clv_Unicanet.Text = e.Node.Tag
                    If IsNumeric(Me.Contratonet.Text) = True Then
                        Dim CON As New SqlConnection(MiConexion)
                        CON.Open()
                        Me.DameContratoNetDigTableAdapter.Connection = CON
                        Me.DameContratoNetDigTableAdapter.Fill(Me.NewSofTvDataSet.DameContratoNetDig, Me.Clv_Unicanet.Text, eContratoNet)
                        CON.Close()
                    End If
                    Me.Contratonet.Text = eContratoNet
                    'MacCableModem.Text = e.Node.Text
                    '    Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))
                    'Else
                    '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                    'End If
                    'Else
                    'If IsNumeric(e.Node.Tag) Then
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                    'Else
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                    'End If
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If IsNumeric(Me.Clv_Unicanet.Text) = False Then Me.Clv_Unicanet.Text = 0
            If Me.Clv_Unicanet.Text > 0 Then
                If GLOTRABAJO = "IPAQD" Or GLOTRABAJO = "IPAQD6" Then
                    Me.NUEIPAQD_SOLDIGTableAdapter.Connection = CON
                    Me.NUEIPAQD_SOLDIGTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQD_SOLDIG, GloDetClave, gloClv_Orden, Me.Contratonet.Text, Me.Clv_Unicanet.Text, 0, "I")
                ElseIf GLOTRABAJO = "BPAQD" Or GLOTRABAJO = "BPAQD6" Then
                    Me.NUEIPAQD_SOLDIGTableAdapter.Connection = CON
                    Me.NUEIPAQD_SOLDIGTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQD_SOLDIG, GloDetClave, gloClv_Orden, Me.Contratonet.Text, Me.Clv_Unicanet.Text, 0, "B")
                    Me.GuardaMotivoCanServTableAdapter.Connection = CON
                    Me.GuardaMotivoCanServTableAdapter.Fill(Me.DataSetEric.GuardaMotivoCanServ, gloClv_Orden, 3, Me.Contratonet.Text, 0, 0)

                ElseIf GLOTRABAJO = "DPAQD" Or GLOTRABAJO = "DPAQD6" Then
                    Me.NUEIPAQD_SOLDIGTableAdapter.Connection = CON
                    Me.NUEIPAQD_SOLDIGTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQD_SOLDIG, GloDetClave, gloClv_Orden, Me.Contratonet.Text, Me.Clv_Unicanet.Text, 0, "S")
                ElseIf GLOTRABAJO = "RPAQD" Or GLOTRABAJO = "RPAQD6" Then
                    Me.NUEIPAQD_SOLDIGTableAdapter.Connection = CON
                    Me.NUEIPAQD_SOLDIGTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQD_SOLDIG, GloDetClave, gloClv_Orden, Me.Contratonet.Text, Me.Clv_Unicanet.Text, 0, "I")
                End If
            Else
                If IsNumeric(Me.Contratonet.Text) = False Then Me.Contratonet.Text = 0
                If Me.Contratonet.Text > 0 Then
                    If GLOTRABAJO = "IPAQD" Or GLOTRABAJO = "IPAQD6" Then
                        Me.NUEIPAQD_SOLDIGTableAdapter.Connection = CON
                        Me.NUEIPAQD_SOLDIGTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQD_SOLDIG, GloDetClave, gloClv_Orden, Me.Contratonet.Text, 0, 1, "I")
                    ElseIf GLOTRABAJO = "BPAQD" Or GLOTRABAJO = "BPAQD6" Then
                        Me.NUEIPAQD_SOLDIGTableAdapter.Connection = CON
                        Me.NUEIPAQD_SOLDIGTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQD_SOLDIG, GloDetClave, gloClv_Orden, Me.Contratonet.Text, 0, 3, "B")
                        Me.GuardaMotivoCanServTableAdapter.Connection = CON
                        Me.GuardaMotivoCanServTableAdapter.Fill(Me.DataSetEric.GuardaMotivoCanServ, gloClv_Orden, 3, Me.Contratonet.Text, 0, 0)
                    ElseIf GLOTRABAJO = "DPAQD" Or GLOTRABAJO = "DPAQD6" Then
                        Me.NUEIPAQD_SOLDIGTableAdapter.Connection = CON
                        Me.NUEIPAQD_SOLDIGTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQD_SOLDIG, GloDetClave, gloClv_Orden, Me.Contratonet.Text, 0, 3, "S")
                    ElseIf GLOTRABAJO = "RPAQD" Or GLOTRABAJO = "RPAQD" Then
                        Me.NUEIPAQD_SOLDIGTableAdapter.Connection = CON
                        Me.NUEIPAQD_SOLDIGTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQD_SOLDIG, GloDetClave, gloClv_Orden, Me.Contratonet.Text, 0, 33, "I")
                    End If
                End If
            End If
            CON.Close()
            Me.CREAARBOL2()
            Me.Contratonet.Text = 0
            Me.MacCableModem.Text = ""
            Me.Clv_Unicanet.Text = 0
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Try


            If gloClv_Orden > 0 And GloDetClave > 0 Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                If GLOTRABAJO = "IPAQD" Or GLOTRABAJO = "IPAQD6" Then
                    Me.NUEIPAQD_SOLDIGTableAdapter.Connection = CON
                    Me.NUEIPAQD_SOLDIGTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQD_SOLDIG, GloDetClave, gloClv_Orden, 0, 0, 2, "I")
                ElseIf GLOTRABAJO = "BPAQD" Or GLOTRABAJO = "BPAQD6" Then
                    Me.NUEIPAQD_SOLDIGTableAdapter.Connection = CON
                    Me.NUEIPAQD_SOLDIGTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQD_SOLDIG, GloDetClave, gloClv_Orden, 0, 0, 4, "B")
                    Me.GuardaMotivoCanServTableAdapter.Connection = CON
                    Me.GuardaMotivoCanServTableAdapter.Fill(Me.DataSetEric.GuardaMotivoCanServ, gloClv_Orden, 3, 0, 0, 1)
                ElseIf GLOTRABAJO = "DPAQD" Or GLOTRABAJO = "DPAQD6" Then
                    Me.NUEIPAQD_SOLDIGTableAdapter.Connection = CON
                    Me.NUEIPAQD_SOLDIGTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQD_SOLDIG, GloDetClave, gloClv_Orden, 0, 0, 4, "S")
                ElseIf GLOTRABAJO = "RPAQD" Or GLOTRABAJO = "RPAQD6" Then
                    Me.NUEIPAQD_SOLDIGTableAdapter.Connection = CON
                    Me.NUEIPAQD_SOLDIGTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQD_SOLDIG, GloDetClave, gloClv_Orden, 0, 0, 44, "I")
                End If
                CON.Close()
                Me.CREAARBOL2()
            End If
            Me.Contratonet.Text = 0
            Me.MacCableModem.Text = ""
            Me.Clv_Unicanet.Text = 0
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If IsNumeric(Me.Clv_Unicanet1.Text) = False Then Me.Clv_Unicanet1.Text = 0
            If Me.Clv_Unicanet1.Text > 0 Then
                Me.BorIPAQD_SOLDIGTableAdapter.Connection = CON
                Me.BorIPAQD_SOLDIGTableAdapter.Fill(Me.NewSofTvDataSet.borIPAQD_SOLDIG, GloDetClave, gloClv_Orden, Me.Contratonet1.Text, Me.Clv_Unicanet1.Text, 0)
                If GLOTRABAJO = "BPAQD" Or GLOTRABAJO = "BPAQD6" Then
                    Me.BorraMotivoCanServTableAdapter.Connection = CON
                    Me.BorraMotivoCanServTableAdapter.Fill(Me.DataSetEric.BorraMotivoCanServ, gloClv_Orden, 3, Me.Contratonet1.Text, 0, 0)
                End If
            Else
                If Me.Contratonet1.Text > 0 Then
                    Me.BorIPAQD_SOLDIGTableAdapter.Connection = CON
                    Me.BorIPAQD_SOLDIGTableAdapter.Fill(Me.NewSofTvDataSet.borIPAQD_SOLDIG, GloDetClave, gloClv_Orden, Me.Contratonet1.Text, Me.Clv_Unicanet1.Text, 1)
                    If GLOTRABAJO = "BPAQD" Or GLOTRABAJO = "BPAQD6" Then
                        Me.BorraMotivoCanServTableAdapter.Connection = CON
                        Me.BorraMotivoCanServTableAdapter.Fill(Me.DataSetEric.BorraMotivoCanServ, gloClv_Orden, 3, Me.Contratonet1.Text, 0, 0)
                    End If
                End If
            End If
            CON.Close()
            Me.CREAARBOL2()
            Me.Contratonet1.Text = 0
            Me.MacCableModem1.Text = ""
            Me.Clv_Unicanet1.Text = 0
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub TreeView2_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView2.AfterSelect
        Try
            If e.Node.Level = 0 Then
                If IsNumeric(e.Node.Tag) = True Then
                    'Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))
                    Contratonet1.Text = e.Node.Tag
                    MacCableModem1.Text = e.Node.Text
                    Clv_Unicanet1.Text = 0
                    '    Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))

                    'Else
                    '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                    'End If
                    'Else
                    'If IsNumeric(e.Node.Tag) Then
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                    'Else
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                    'End If
                End If
            Else
                If IsNumeric(e.Node.Tag) = True Then
                    'Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))                    
                    Clv_Unicanet1.Text = e.Node.Tag
                    If IsNumeric(Me.Contratonet1.Text) = True Then
                        Dim CON As New SqlConnection(MiConexion)
                        CON.Open()
                        Me.DameContratoNetDigTableAdapter.Connection = CON
                        Me.DameContratoNetDigTableAdapter.Fill(Me.NewSofTvDataSet.DameContratoNetDig, Me.Clv_Unicanet1.Text, eContratoNet)
                        CON.Close()
                    End If
                    Me.Contratonet1.Text = eContratoNet
                    'MacCableModem.Text = e.Node.Text
                    '    Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))
                    'Else
                    '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                    'End If
                    'Else
                    'If IsNumeric(e.Node.Tag) Then
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                    'Else
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                    'End If
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.BorIPAQD_SOLDIGTableAdapter.Connection = CON
            Me.BorIPAQD_SOLDIGTableAdapter.Fill(Me.NewSofTvDataSet.borIPAQD_SOLDIG, GloDetClave, gloClv_Orden, 0, 0, 2)
            If (GLOTRABAJO = "BPAQD" Or GLOTRABAJO = "BPAQD6") And (Me.Contratonet1.Text.Length > 0) Then
                Me.BorraMotivoCanServTableAdapter.Connection = CON
                Me.BorraMotivoCanServTableAdapter.Fill(Me.DataSetEric.BorraMotivoCanServ, gloClv_Orden, 3, 0, 0, 1)
            End If
            CON.Close()
            Me.CREAARBOL2()
            Me.Contratonet1.Text = 0
            Me.MacCableModem1.Text = ""
            Me.Clv_Unicanet1.Text = 0
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub



    Private Sub TreeView1_NodeMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles TreeView1.NodeMouseClick
        Try
            If e.Node.Level = 0 Then
                If IsNumeric(e.Node.Tag) = True Then
                    'Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))
                    Contratonet.Text = e.Node.Tag
                    MacCableModem.Text = e.Node.Text
                    Clv_Unicanet.Text = 0
                    '    Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))

                    'Else
                    '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                    'End If
                    'Else
                    'If IsNumeric(e.Node.Tag) Then
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                    'Else
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                    'End If
                End If
            Else
                If IsNumeric(e.Node.Tag) = True Then
                    'Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))
                    Clv_Unicanet.Text = e.Node.Tag
                    If IsNumeric(Me.Contratonet.Text) = True Then
                        Dim CON As New SqlConnection(MiConexion)
                        CON.Open()
                        Me.DameContratoNetDigTableAdapter.Connection = CON
                        Me.DameContratoNetDigTableAdapter.Fill(Me.NewSofTvDataSet.DameContratoNetDig, Me.Clv_Unicanet.Text, eContratoNet)
                        CON.Close()
                    End If
                    Me.Contratonet.Text = eContratoNet
                    'MacCableModem.Text = e.Node.Text
                    '    Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))
                    'Else
                    '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                    'End If
                    'Else
                    'If IsNumeric(e.Node.Tag) Then
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                    'Else
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                    'End If
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub TreeView2_NodeMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles TreeView2.NodeMouseClick
        Try
            If e.Node.Level = 0 Then
                If IsNumeric(e.Node.Tag) = True Then
                    'Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))
                    Contratonet1.Text = e.Node.Tag
                    MacCableModem1.Text = e.Node.Text
                    Clv_Unicanet1.Text = 0
                    '    Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))

                    'Else
                    '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                    'End If
                    'Else
                    'If IsNumeric(e.Node.Tag) Then
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                    'Else
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                    'End If
                End If
            Else
                If IsNumeric(e.Node.Tag) = True Then
                    'Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))                    
                    Clv_Unicanet1.Text = e.Node.Tag
                    If IsNumeric(Me.Contratonet1.Text) = True Then
                        Dim CON As New SqlConnection(MiConexion)
                        CON.Open()
                        Me.DameContratoNetDigTableAdapter.Connection = CON
                        Me.DameContratoNetDigTableAdapter.Fill(Me.NewSofTvDataSet.DameContratoNetDig, Me.Clv_Unicanet1.Text, eContratoNet)
                        CON.Close()
                    End If
                    Me.Contratonet1.Text = eContratoNet
                    'MacCableModem.Text = e.Node.Text
                    '    Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))
                    'Else
                    '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                    'End If
                    'Else
                    'If IsNumeric(e.Node.Tag) Then
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                    'Else
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                    'End If
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    ''

    Private Sub FrmrRelPaquetesdelClienteDigital_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        CREAARBOL()
        Me.CREAARBOL2()
        If GLOTRABAJO = "IPAQD" Or GLOTRABAJO = "IPAQD6" Then
            Me.Text = "Instalación de Paquetes Digitales"
            Me.Label3.Text = "Paquetes Digitales Pendientes de Instalar"
            Me.Label4.Text = "Instalar estos Paquetes Digitales"
        ElseIf GLOTRABAJO = "BPAQD" Or GLOTRABAJO = "BPAQD6" Then
            Me.Text = "Baja de Paquetes Digitales"
            Me.Label3.Text = "Paquetes Digitales Activos"
            Me.Label4.Text = "Pasar a Baja estos Paquetes Digitales"
        ElseIf GLOTRABAJO = "RPAQD" Or GLOTRABAJO = "RPAQD6" Then
            Me.Text = "Reconexión de Paquetes Digitales"
            Me.Label3.Text = "Paquetes Digitales Suspendidos"
            Me.Label4.Text = "Activar estos Paquetes Digitales"
        ElseIf GLOTRABAJO = "DPAQD" Or GLOTRABAJO = "DPAQD6" Then
            Me.Text = "Desconexión de Paquetes Digitales"
            Me.Label3.Text = "Paquetes Digitales Activos"
            Me.Label4.Text = "Suspender estos Paquetes Digitales"
        End If
        If Bloquea = True Or opcion = "M" Then
            Me.Button1.Enabled = False
            Me.Button2.Enabled = False
            Me.Button3.Enabled = False
            Me.Button4.Enabled = False
        End If
    End Sub



End Class