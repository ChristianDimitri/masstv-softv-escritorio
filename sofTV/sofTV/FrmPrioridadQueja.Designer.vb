﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPrioridadQueja
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim lblClave As System.Windows.Forms.Label
        Dim lblConcepto As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmPrioridadQueja))
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.txtConcepto = New System.Windows.Forms.TextBox()
        Me.CONBANCOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.CONBANCOSBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.btnEliminar = New System.Windows.Forms.ToolStripButton()
        Me.btnCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnGuardar = New System.Windows.Forms.ToolStripButton()
        Me.CONBANCOSTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONBANCOSTableAdapter()
        Me.txtClave = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        lblClave = New System.Windows.Forms.Label()
        lblConcepto = New System.Windows.Forms.Label()
        CType(Me.CONBANCOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONBANCOSBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONBANCOSBindingNavigator.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblClave
        '
        lblClave.AutoSize = True
        lblClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        lblClave.ForeColor = System.Drawing.Color.LightSlateGray
        lblClave.Location = New System.Drawing.Point(84, 59)
        lblClave.Name = "lblClave"
        lblClave.Size = New System.Drawing.Size(50, 15)
        lblClave.TabIndex = 0
        lblClave.Text = "Clave :"
        '
        'lblConcepto
        '
        lblConcepto.AutoSize = True
        lblConcepto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        lblConcepto.ForeColor = System.Drawing.Color.LightSlateGray
        lblConcepto.Location = New System.Drawing.Point(84, 103)
        lblConcepto.Name = "lblConcepto"
        lblConcepto.Size = New System.Drawing.Size(75, 15)
        lblConcepto.TabIndex = 2
        lblConcepto.Text = "Concepto :"
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.DarkOrange
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.ForeColor = System.Drawing.Color.Black
        Me.btnSalir.Location = New System.Drawing.Point(442, 305)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(136, 33)
        Me.btnSalir.TabIndex = 5
        Me.btnSalir.Text = "&SALIR"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'txtConcepto
        '
        Me.txtConcepto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtConcepto.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONBANCOSBindingSource, "nombre", True))
        Me.txtConcepto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtConcepto.Location = New System.Drawing.Point(193, 97)
        Me.txtConcepto.MaxLength = 50
        Me.txtConcepto.Multiline = True
        Me.txtConcepto.Name = "txtConcepto"
        Me.txtConcepto.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtConcepto.Size = New System.Drawing.Size(290, 84)
        Me.txtConcepto.TabIndex = 1
        '
        'CONBANCOSBindingSource
        '
        Me.CONBANCOSBindingSource.DataMember = "CONBANCOS"
        Me.CONBANCOSBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CONBANCOSBindingNavigator
        '
        Me.CONBANCOSBindingNavigator.AddNewItem = Nothing
        Me.CONBANCOSBindingNavigator.BindingSource = Me.CONBANCOSBindingSource
        Me.CONBANCOSBindingNavigator.CountItem = Nothing
        Me.CONBANCOSBindingNavigator.DeleteItem = Me.btnEliminar
        Me.CONBANCOSBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnCancelar, Me.btnEliminar, Me.btnGuardar})
        Me.CONBANCOSBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONBANCOSBindingNavigator.MoveFirstItem = Nothing
        Me.CONBANCOSBindingNavigator.MoveLastItem = Nothing
        Me.CONBANCOSBindingNavigator.MoveNextItem = Nothing
        Me.CONBANCOSBindingNavigator.MovePreviousItem = Nothing
        Me.CONBANCOSBindingNavigator.Name = "CONBANCOSBindingNavigator"
        Me.CONBANCOSBindingNavigator.PositionItem = Nothing
        Me.CONBANCOSBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONBANCOSBindingNavigator.Size = New System.Drawing.Size(566, 25)
        Me.CONBANCOSBindingNavigator.TabIndex = 2
        Me.CONBANCOSBindingNavigator.TabStop = True
        Me.CONBANCOSBindingNavigator.Text = "BindingNavigator1"
        '
        'btnEliminar
        '
        Me.btnEliminar.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminar.Image = CType(resources.GetObject("btnEliminar.Image"), System.Drawing.Image)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.RightToLeftAutoMirrorImage = True
        Me.btnEliminar.Size = New System.Drawing.Size(90, 22)
        Me.btnEliminar.Text = "&ELIMINAR"
        '
        'btnCancelar
        '
        Me.btnCancelar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.btnCancelar.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Image = CType(resources.GetObject("btnCancelar.Image"), System.Drawing.Image)
        Me.btnCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(79, 22)
        Me.btnCancelar.Text = "&CANCELAR"
        '
        'btnGuardar
        '
        Me.btnGuardar.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.Image = CType(resources.GetObject("btnGuardar.Image"), System.Drawing.Image)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(91, 22)
        Me.btnGuardar.Text = "&GUARDAR"
        '
        'CONBANCOSTableAdapter
        '
        Me.CONBANCOSTableAdapter.ClearBeforeFill = True
        '
        'txtClave
        '
        Me.txtClave.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtClave.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONBANCOSBindingSource, "clave", True))
        Me.txtClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClave.Location = New System.Drawing.Point(193, 59)
        Me.txtClave.Name = "txtClave"
        Me.txtClave.ReadOnly = True
        Me.txtClave.Size = New System.Drawing.Size(100, 21)
        Me.txtClave.TabIndex = 100
        Me.txtClave.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.txtConcepto)
        Me.Panel1.Controls.Add(Me.CONBANCOSBindingNavigator)
        Me.Panel1.Controls.Add(lblClave)
        Me.Panel1.Controls.Add(Me.txtClave)
        Me.Panel1.Controls.Add(lblConcepto)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(566, 287)
        Me.Panel1.TabIndex = 4
        '
        'FrmPrioridadQueja
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(590, 350)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "FrmPrioridadQueja"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Prioridad de Queja"
        CType(Me.CONBANCOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONBANCOSBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONBANCOSBindingNavigator.ResumeLayout(False)
        Me.CONBANCOSBindingNavigator.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents txtConcepto As System.Windows.Forms.TextBox
    Friend WithEvents CONBANCOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONBANCOSBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents btnEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnGuardar As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONBANCOSTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONBANCOSTableAdapter
    Friend WithEvents txtClave As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
End Class
