﻿Public Class FrmTECNICOS
    Private locclv_TipSer As Integer = 0
   
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
    
    
    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Me.CONTECNICOSBindingSource.CancelEdit()
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Me.CONTECNICOSTableAdapter.Delete(gloClave)
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub CONTECNICOSBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONTECNICOSBindingNavigatorSaveItem.Click
        If Me.ComboBox1.Text = "" Then
            MsgBox("Selecciona primero un puesto", MsgBoxStyle.Information)
        Else
            Me.Validate()
            Me.CONTECNICOSBindingSource.EndEdit()
            Me.CONTECNICOSTableAdapter.Update(Me.NewSofTvDataSet.CONTECNICOS)
            MsgBox(mensaje5)
            GloBnd = True
            MsgBox("Los Datos se Guardaron con Exito", MsgBoxStyle.Information)
            Me.Close()
        End If
    End Sub
    Private Sub BUSCA(ByVal CLAVE As Integer)
        Try
            Me.CONTECNICOSTableAdapter.Fill(Me.NewSofTvDataSet.CONTECNICOS, New System.Nullable(Of Integer)(CType(CLAVE, Integer)))
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub activa()
        If GloClv_tecnico > 0 Then

            Me.Panel3.Enabled = True
        Else

            Me.Panel3.Enabled = False
        End If
    End Sub


    Private Sub FrmTECNICOS_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MUESTRApuestoTec' Puede moverla o quitarla según sea necesario.
        Me.MUESTRApuestoTecTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRApuestoTec)
        Me.ComboBox1.Text = ""
        'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerPrincipal' Puede moverla o quitarla según sea necesario.
        GloClv_tecnico = gloClave

        Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)
        If opcion = "N" Then
            Me.CONTECNICOSBindingSource.AddNew()
            Panel1.Enabled = True
            GloClv_tecnico = 0
        ElseIf opcion = "C" Then
            Panel1.Enabled = False
            BUSCA(gloClave)
            CREAARBOLRELCOLONIASSER()
        ElseIf opcion = "M" Then
            Panel1.Enabled = True
            BUSCA(gloClave)
            CREAARBOLRELCOLONIASSER()
        End If
        activa()
    End Sub

    Public Sub CREAARBOLRELCOLONIASSER()
        Try
            Dim I As Integer = 0
            Dim X As Integer = 0
            Me.CONRelSerTecTableAdapter.Fill(Me.NewSofTvDataSet.CONRelSerTec, New System.Nullable(Of Integer)(CType(GloClv_tecnico, Integer)))
            Dim FilaRow As DataRow
            Me.TreeView2.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.CONRelSertec.Rows
                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                If IsNumeric(Trim(FilaRow("Clv_TIPSER").ToString())) = True Then
                    Me.TreeView2.Nodes.Add(Trim(FilaRow("Clv_TIPSER").ToString()), Trim(FilaRow("CONCEPTO").ToString()))
                    Me.TreeView2.Nodes(I).Tag = Trim(FilaRow("Clv_TIPSER").ToString())
                    I += 1
                End If
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub

   
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim bnd As Boolean = True
        Try
            Me.Validate()
            Me.CONRelSerTecBindingSource.EndEdit()
            Me.CONRelSerTecTableAdapter.Update(Me.NewSofTvDataSet.CONRelSerTec)
            '--MsgBox("Se Guardo con Exíto", MsgBoxStyle.Information)
            Me.CONRelSerTecTableAdapter.Insert(GloClv_tecnico, Me.ComboBox4.SelectedValue)
            Me.Validate()
            Me.CONRelSerTecBindingSource.EndEdit()
            Me.CONRelSerTecTableAdapter.Update(Me.NewSofTvDataSet.CONRelSerTec)
            If bnd = True Then
                CREAARBOLRELCOLONIASSER()
            End If
        Catch ex As System.Exception

            bnd = False
            Me.CONRelSerTecBindingSource.CancelEdit()
            MsgBox("El Servicio Ya Exíste en la Lista")
            '--System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If locclv_TipSer > 0 Then
            Me.CONRelSerTecTableAdapter.Delete(GloClv_tecnico, Me.locclv_TipSer)
            Me.Validate()
            Me.CONRelSerTecBindingSource.EndEdit()
            Me.CONRelSerTecTableAdapter.Update(Me.NewSofTvDataSet.CONRelSerTec)
            Me.CREAARBOLRELCOLONIASSER()
            locclv_TipSer = 0
        Else
            MsgBox("Seleccione lo que desea Quitar de la Lista", MsgBoxStyle.Information)
        End If
    End Sub


    Private Sub ClaveTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClaveTextBox.TextChanged
        gloClave = Me.ClaveTextBox.Text
        GloClv_tecnico = gloClave
        activa()
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Me.PuestoTextBox.Text = Me.ComboBox1.SelectedValue
    End Sub

    Private Sub TreeView2_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView2.AfterSelect
        locclv_TipSer = e.Node.Tag
    End Sub

    Private Sub NombreTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NombreTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(NombreTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub NombreTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NombreTextBox.TextChanged

    End Sub
End Class