Imports System.Data.SqlClient
Imports System.IO

Public Class Acceso_TipoServicios

    Private Sub Permiso()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Dim bnd As Integer = 0
            Dim tipo_usuario As Integer = 0
            If GloTipoUsuario = 40 Then
                eAccesoAdmin = True
            End If
            If eAccesoAdmin = True Then
                bnd = 1
            Else
                Me.Dame_clvtipusuarioTableAdapter.Connection = CON
                Me.Dame_clvtipusuarioTableAdapter.Fill(Me.ProcedimientosArnoldo2.Dame_clvtipusuario, Me.Clv_UsuarioTextBox.Text, Me.PasaporteTextBox.Text, tipo_usuario)
                Select Case tipo_usuario
                    Case 1
                        Me.VerAcceso_ChecaTableAdapter.Connection = CON
                        Me.VerAcceso_ChecaTableAdapter.Fill(Me.DataSetEdgarRev2.VerAcceso_Checa, Me.Clv_UsuarioTextBox.Text, Me.PasaporteTextBox.Text, 1, bnd)
                    Case 40
                        Me.VerAcceso_ChecaTableAdapter.Connection = CON
                        Me.VerAcceso_ChecaTableAdapter.Fill(Me.DataSetEdgarRev2.VerAcceso_Checa, Me.Clv_UsuarioTextBox.Text, Me.PasaporteTextBox.Text, 40, bnd)
                    Case Else
                        bnd = 0
                End Select

            End If
            If bnd = 1 Then
                If opcFrm = 1 Then
                    BrwTiposServicios.Show()
                End If
                If opcFrm = 2 Then
                    FrmGenerales_Sistema.Show()
                End If
                If opcFrm = 3 Then
                    BRWBUSCACNR.Show()
                End If
                If opcFrm = 4 Then
                    BRWCNRDIG.Show()
                End If
                If opcFrm = 5 Then
                    FrmGenerales_Internet.Show()
                End If
                If opcFrm = 6 Then
                    BrwGeneralesBancos.Show()
                End If
                If opcFrm = 7 Then
                    FrmGenerales_Digital.Show()
                End If
                If opcFrm = 8 Then
                    Frmoxxo.Show()
                End If
                If opcFrm = 9 Then
                    FrmServicios.Show()
                End If
                If opcFrm = 10 Then
                    FrmMiMenu.desconexion()
                End If
                If opcFrm = 11 Then
                    FrmMiMenu.desconexion2()
                ElseIf opcFrm = 110 Then
                    FrmMiMenu.desconexion3()
                End If
                If opcFrm = 12 Then
                    Dim txt As String
                    Dim myProcess As New Process()
                    Using sw As StreamWriter = File.CreateText("C:\PPE\SofConex.txt")
                        txt = MiConexion
                        sw.Write(txt)
                        sw.Close()
                    End Using
                    Using sw As StreamWriter = File.CreateText(RutaReportes + "\SofPa.txt")
                        txt = "FrmBloqueo" + "," + CStr(Contrato) + "," + CStr(glopar)
                        sw.Write(txt)
                        sw.Close()
                    End Using
                    Dim myProcessStartInfo As New ProcessStartInfo(RutaReportes + "\SoftvPPE.application")
                    myProcess.StartInfo = myProcessStartInfo
                    myProcess.Start()
                End If
                If opcFrm = 13 Then
                    FrmDesconexionTempo.Show()
                End If

                If opcFrm = 14 Then
                    FrmRoboDeSe�al.Show()
                End If
                If opcFrm = 15 Then
                    FrmContratoMaestro.Show()
                End If
                If opcFrm = 16 Or opcFrm = 26 Then
                    FrmSeguridad.Show()
                End If
                If opcFrm = 17 Then
                    FrmSeguridad.Show()
                End If
                If opcFrm = 18 Then
                    FrmReset.Show()
                End If
                If opcFrm = 19 Then
                    BitacoraDelSistema.Show()
                End If
                If opcFrm = 20 Then
                    Generales_Prosa_Bancomer.Show()
                End If
                If opcFrm = 21 Then
                    BRWCNRDIGX.Show()
                End If
                Me.Close()
            Else
                MsgBox("No Tiene Acceso ", MsgBoxStyle.Information)
                Me.Close()
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    'Private Sub Permiso()
    '    Try
    '        Dim bnd As Integer = 0
    '        If GloTipoUsuario = 40 Then
    '            eAccesoAdmin = True
    '        End If
    '        If eAccesoAdmin = True Then
    '            bnd = 1
    '        Else
    '            Me.VerAcceso_ChecaTableAdapter.Fill(Me.DataSetEdgarRev2.VerAcceso_Checa, Me.Clv_UsuarioTextBox.Text, Me.PasaporteTextBox.Text, 1, bnd)
    '        End If

    '        If bnd = 1 Then
    '            If opcFrm = 1 Then
    '                BrwTiposServicios.Show()
    '            End If
    '            If opcFrm = 2 Then
    '                FrmGenerales_Sistema.Show()
    '            End If
    '            If opcFrm = 3 Then
    '                BRWBUSCACNR.Show()
    '            End If
    '            If opcFrm = 4 Then
    '                BRWCNRDIG.Show()
    '            End If
    '            If opcFrm = 5 Then
    '                FrmGenerales_Internet.Show()
    '            End If
    '            If opcFrm = 6 Then
    '                BrwGeneralesBancos.Show()
    '            End If
    '            If opcFrm = 7 Then
    '                FrmGenerales_Digital.Show()
    '            End If
    '            If opcFrm = 8 Then
    '                Frmoxxo.Show()
    '            End If
    '            If opcFrm = 9 Then
    '                FrmServicios.Show()
    '            End If
    '            If opcFrm = 10 Then
    '                My.Forms.FrmMiMenu.desconexion()
    '            End If
    '            If opcFrm = 11 Then
    '                My.Forms.FrmMiMenu.desconexion2()
    '            End If
    '            If opcFrm = 12 Then
    '                Dim txt As String
    '                Dim myProcess As New Process()
    '                Using sw As StreamWriter = File.CreateText(RutaReportes + "\SofPa.txt")
    '                    txt = "FrmBloqueo" + "," + CStr(Contrato) + "," + CStr(glopar)
    '                    sw.Write(txt)
    '                    sw.Close()
    '                End Using
    '                Dim myProcessStartInfo As New ProcessStartInfo(RutaReportes + "\SoftvPPE.application")
    '                myProcess.StartInfo = myProcessStartInfo
    '                myProcess.Start()
    '            End If
    '            If opcFrm = 13 Then
    '                FrmDesconexionTempo.Show()
    '            End If

    '            If opcFrm = 14 Then
    '                FrmRoboDeSe�al.Show()
    '            End If

    '            If opcFrm = 15 Then
    '                FrmContratoMaestro.Show()
    '            End If

    '            Me.Close()
    '        Else
    '            If GloTipoUsuario = 40 Then
    '                eAccesoAdmin = False
    '            End If
    '            MsgBox("No Tiene Acceso ", MsgBoxStyle.Information)
    '            Me.Close()
    '        End If
    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try

    'End Sub
    Private Sub Acceso_TipoServicios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If eAccesoAdmin = True Then
            Permiso()
        Else
            If GloOpPermiso = 0 Then
                Me.CMBClv_UsuarioLabel.Text = "Login del Administrador : "
                Me.CMBPasswordLabel.Text = "Contrase�a del Administrador :"
            End If
        End If
    End Sub

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        Permiso()
    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        Me.Close()

    End Sub
    Private Sub Clv_UsuarioTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Clv_UsuarioTextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Permiso()
        End If
    End Sub

    Private Sub Clv_UsuarioTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_UsuarioTextBox.TextChanged

    End Sub

    Private Sub PasaporteTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles PasaporteTextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Permiso()
        End If
    End Sub

    Private Sub CMBClv_UsuarioLabel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBClv_UsuarioLabel.Click

    End Sub

    Private Sub PasaporteTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PasaporteTextBox.TextChanged

    End Sub

    
End Class