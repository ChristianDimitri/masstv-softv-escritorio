Imports System.Data.SqlClient
Imports System.Text

Public Class FrmInternet
    Dim locclv_servicio As Long
    Dim NombreMAC As String
    Dim contador As Integer
    '''Digital e Internet'''''''''''
    Private status As String = Nothing
    Private fecha_solicitud As String = Nothing
    Private fecha_instalacio As String = Nothing
    Private fecha_suspension As String = Nothing
    Private fecha_baja As String = Nothing
    Private fecha_Fuera_Area As String = Nothing
    Private FECHA_ULT_PAGO As String = Nothing
    Private PrimerMensualidad As Boolean = False
    Private ultimo_mes As Integer = 0
    Private ultimo_anio As Integer = 0
    Private primerMesAnt As Boolean = False
    Private statusAnt As String = Nothing
    Private facturaAnt As String = Nothing
    Private GENERAOSINSTA As Boolean = False
    Private factura As String = Nothing
    Private Clv_Vendedor As Integer = 0
    Private Clv_Promocion As Integer = 0
    Private Obs As String = Nothing
    Private DESCRIPCION As String = Nothing
    Private Cortesia As Boolean = False
    Private Descuento As String = Nothing
    Private statusTarjeta As String = Nothing
    Private Activacion As String = Nothing
    Private Suspension As String = Nothing
    Private Baja As String = Nothing
    Private serenta As String = Nothing
    Private macasignada As String = Nothing
    Private Email As String = Nothing

    'Internet1
    Private Locmarca As String = Nothing
    Private LocTipoApar As String = Nothing
    Private LocTipservcabl As String = Nothing
    Private LocTipoCablemodem As String = Nothing
    Private LocTranspaso As String = Nothing
    Private Loc1pago As String = Nothing
    Private Loc2pago As String = Nothing
    Dim cortesiaint As Boolean = True

    Dim CmdInt As New SqlCommand
    Dim ConInt As New SqlConnection(MiConexion)
    Private Sub Checa_Forzoso(ByVal Clv_unicanet As Long, ByVal Contrato As Long)
        Dim Con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        'Contrato
        If Contrato = Nothing Then Contrato = 0
        Try
            'Muestra_Plazo_Forzozo (@contrato bigint,@Clv_unicanet bigint,@Plazo varchar(10) output)
            cmd = New SqlCommand()
            Con.Open()
            With cmd
                .CommandText = "Muestra_Plazo_Forzozo"
                .Connection = Con
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@contrato", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = Contrato
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@Clv_unicanet", SqlDbType.BigInt)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = Clv_unicanet
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@Plazo", SqlDbType.VarChar, 10)
                prm2.Direction = ParameterDirection.Output
                prm2.Value = 0
                .Parameters.Add(prm2)

                Dim ia As Integer = .ExecuteNonQuery()

                Me.LblPlazoForzoso1.Text = prm2.Value

            End With
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub guardabitacoranet(ByVal op As Integer)
        Try
            Dim CON10 As New SqlConnection(MiConexion)
            Dim cmd As New SqlClient.SqlCommand()

            If OpcionCli = "M" Then
                Dim valida1 As String = Nothing
                Dim Valida2 As String = Nothing
                Dim Valida3 As String = Nothing
                Dim valida4 As String = Nothing
                Dim valida5 As String = Nothing

                Select Case op
                    'Case 0
                    '    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Agrego un Paquete Digital", " ", "Se agrego el Paquete Digital:" & GLoPaqueteAgrega, LocClv_Ciudad)
                    'Case 1
                    '    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Elimino un paquete Digital", " ", "Se Elimino el Paquete Digital:" & NombrePaqueteElimino, LocClv_Ciudad)
                    Case 2


                        cmd = New SqlClient.SqlCommand()
                        CON10.Open()
                        With cmd
                            .CommandText = "Dame_Mac_CableDeco"
                            .Connection = CON10
                            .CommandTimeout = 0
                            .CommandType = CommandType.StoredProcedure

                            '@contratonet bigint,@clv_unicanet bigint, @op int,@Mac varchar(max) output
                            Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
                            Dim prm1 As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
                            Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
                            Dim prm3 As New SqlParameter("@Mac", SqlDbType.VarChar, 200)

                            prm.Direction = ParameterDirection.Input
                            prm1.Direction = ParameterDirection.Input
                            prm2.Direction = ParameterDirection.Input
                            prm3.Direction = ParameterDirection.Output

                            prm.Value = LocGloContratoNet
                            prm1.Value = 0
                            prm2.Value = GloOp
                            prm3.Value = ""

                            .Parameters.Add(prm)
                            .Parameters.Add(prm1)
                            .Parameters.Add(prm2)
                            .Parameters.Add(prm3)

                            Dim i As Integer = cmd.ExecuteNonQuery()

                            NombreMAC = prm3.Value

                        End With
                        CON10.Close()


                        bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Elimino Un Servicio De Internet: " + NombreMAC, " ", "Se Elimino El Servicio de Itnernet : " & NombrePaqueteElimino, LocClv_Ciudad)
                        'Case 3
                        '    bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Se Agrego un Equipo Digital", " ", "Se Agrego Un Equipo Digital Con El ContratoNEt:" & GloContratoDig_Nuevo, LocClv_Ciudad)
                    Case 4
                        'COntNet

                        cmd = New SqlClient.SqlCommand()
                        CON10.Open()
                        With cmd
                            .CommandText = "Dame_Mac_CableDeco"
                            .Connection = CON10
                            .CommandTimeout = 0
                            .CommandType = CommandType.StoredProcedure

                            '@contratonet bigint,@clv_unicanet bigint, @op int,@Mac varchar(max) output
                            Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
                            Dim prm1 As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
                            Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
                            Dim prm3 As New SqlParameter("@Mac", SqlDbType.VarChar, 200)

                            prm.Direction = ParameterDirection.Input
                            prm1.Direction = ParameterDirection.Input
                            prm2.Direction = ParameterDirection.Input
                            prm3.Direction = ParameterDirection.Output

                            prm.Value = 0
                            prm1.Value = LocGloClv_unianet
                            prm2.Value = GloOp
                            prm3.Value = ""

                            .Parameters.Add(prm)
                            .Parameters.Add(prm1)
                            .Parameters.Add(prm2)
                            .Parameters.Add(prm3)

                            Dim i As Integer = cmd.ExecuteNonQuery()

                            NombreMAC = prm3.Value

                        End With
                        CON10.Close()

                        cmd = New SqlClient.SqlCommand
                        CON10.Open()
                        With cmd
                            .CommandText = "Dame_Servicio_Cab_Deco"
                            .Connection = CON10
                            .CommandTimeout = 0
                            .CommandType = CommandType.StoredProcedure
                            '@clv_unicanet bigint, @op int,@Paquete varchar(max) output
                            Dim prm As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
                            Dim prm1 As New SqlParameter("@op", SqlDbType.Int)
                            Dim prm2 As New SqlParameter("@Paquete", SqlDbType.VarChar, 200)

                            prm.Direction = ParameterDirection.Input
                            prm1.Direction = ParameterDirection.Input
                            prm2.Direction = ParameterDirection.Output

                            prm.Value = LocGloClv_unianet
                            prm1.Value = GloOp
                            prm2.Value = ""

                            .Parameters.Add(prm)
                            .Parameters.Add(prm1)
                            .Parameters.Add(prm2)

                            Dim i As Integer = cmd.ExecuteNonQuery()

                            NombrePaqueteElimino = prm2.Value

                        End With
                        CON10.Close()

                        'status = prm1.Value
                        bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Status: ", status, Me.ComboBox5.SelectedValue, LocClv_Ciudad)
                        'fecha_solicitud = prm2.Value
                        If fecha_solicitud = "01/01/1900" Then fecha_solicitud = ""
                        bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Fecha De solicitud: ", fecha_solicitud, Me.Fecha_solicitudTextBox1.Text, LocClv_Ciudad)
                        'fecha_instalacio = prm3.Value
                        If fecha_instalacio = "01/01/1900" Then fecha_instalacio = ""
                        bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Fecha De Instalacion: ", fecha_instalacio, Me.Fecha_instalacioTextBox.Text, LocClv_Ciudad)
                        'fecha_suspension = prm4.Value
                        If fecha_suspension = "01/01/1900" Then fecha_suspension = ""
                        bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Fecha De Suspension: ", fecha_suspension, Me.Fecha_suspensionTextBox.Text, LocClv_Ciudad)
                        'fecha_baja = prm5.Value
                        If fecha_baja = "01/01/1900" Then fecha_baja = ""
                        bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Fecha De Baja: ", fecha_baja, Me.Fecha_bajaTextBox.Text, LocClv_Ciudad)
                        'fecha_Fuera_Area = prm6.Value
                        If fecha_Fuera_Area = "01/01/1900" Then fecha_Fuera_Area = ""
                        bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Fecha De Fuera De Area: ", fecha_Fuera_Area, Me.Fecha_Fuera_AreaTextBox.Text, LocClv_Ciudad)
                        'FECHA_ULT_PAGO = prm7.Value
                        If FECHA_ULT_PAGO = "01/01/1900" Then FECHA_ULT_PAGO = ""
                        bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Fecha De Ultimo Pago: ", FECHA_ULT_PAGO, Me.FECHA_ULT_PAGOTextBox1.Text, LocClv_Ciudad)
                        'PrimerMensualidad = prm8.Value
                        If Me.PrimerMensualidadCheckBox.CheckState = CheckState.Checked Then
                            valida1 = "True"
                        ElseIf Me.PrimerMensualidadCheckBox.CheckState = CheckState.Unchecked Then
                            valida1 = "False"
                        End If
                        bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Primer Mensualidad: ", PrimerMensualidad, valida1, LocClv_Ciudad)
                        'ultimo_mes = prm9.Value
                        If Me.Ultimo_mesTextBox1.Text = "0" Or IsNumeric(Me.Ultimo_mesTextBox1.Text) = False Then
                            Valida2 = "0"
                        Else
                            Valida2 = Me.Ultimo_mesTextBox1.Text
                        End If
                        bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Ultimo mes: ", ultimo_mes, Valida2, LocClv_Ciudad)
                        'ultimo_anio = prm10.Value
                        bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Ultimo a�o: ", ultimo_anio, Me.Ultimo_anioTextBox1.Text, LocClv_Ciudad)
                        'primerMesAnt = prm11.Value
                        'statusAnt = prm12.Value
                        'facturaAnt = prm13.Value
                        'GENERAOSINSTA = prm14.Value
                        'factura = prm15.Value
                        bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Factura: ", factura, Me.FacturaTextBox.Text, LocClv_Ciudad)
                        'Clv_Vendedor = prm16.Value
                        If Me.ComboBox6.Text = "" Then
                            Valida3 = "0"
                        Else
                            Valida3 = CStr(Me.ComboBox6.SelectedValue)
                        End If
                        bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Vendedor: ", Clv_Vendedor, Valida3, LocClv_Ciudad)
                        'Clv_Promocion = prm17.Value
                        'If Me.ComboBox8.Text = "" Then
                        '    valida4 = "0"
                        'Else
                        '    valida4 = CStr(Me.ComboBox8.SelectedValue)
                        'End If
                        'bitsist(GloUsuario, CLng(Me.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Clv_Promocion" + "-" + NombrePaqueteElimino, Clv_Promocion, valida4, LocClv_Ciudad)
                        'Email = prm18.Value
                        bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Email: ", Email, Me.EmailTextBox1.Text, LocClv_Ciudad)
                        'Obs = prm19.Value
                        bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Observaciones: ", Obs, Me.ObsTextBox2.Text, LocClv_Ciudad)
                        'DESCRIPCION = prm20.Value
                        'Cortesia = prm21.Value
                        If Me.CortesiaCheckBox2.CheckState = CheckState.Checked Then
                            valida5 = "True"
                        ElseIf Me.CortesiaCheckBox2.CheckState = CheckState.Unchecked Then
                            valida5 = "False"
                        End If
                        bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Cortesia: ", Cortesia, valida5, LocClv_Ciudad)
                        'descuento
                        bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + NombrePaqueteElimino + " - " + "Descuento: ", Descuento, Me.DescuentoLabel3.Text, LocClv_Ciudad)
                    Case 5

                        cmd = New SqlClient.SqlCommand()
                        CON10.Open()
                        With cmd
                            .CommandText = "Dame_Mac_CableDeco"
                            .Connection = CON10
                            .CommandTimeout = 0
                            .CommandType = CommandType.StoredProcedure

                            '@contratonet bigint,@clv_unicanet bigint, @op int,@Mac varchar(max) output
                            Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
                            Dim prm1 As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
                            Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
                            Dim prm3 As New SqlParameter("@Mac", SqlDbType.VarChar, 200)

                            prm.Direction = ParameterDirection.Input
                            prm1.Direction = ParameterDirection.Input
                            prm2.Direction = ParameterDirection.Input
                            prm3.Direction = ParameterDirection.Output

                            prm.Value = LocGloContratoNet
                            prm1.Value = 0
                            prm2.Value = GloOp
                            prm3.Value = ""

                            .Parameters.Add(prm)
                            .Parameters.Add(prm1)
                            .Parameters.Add(prm2)
                            .Parameters.Add(prm3)

                            Dim i As Integer = cmd.ExecuteNonQuery()

                            NombreMAC = prm3.Value

                        End With
                        CON10.Close()


                        'Locmarca = Me.MARCALabel1.Text
                        bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Marca: ", Locmarca, Me.MARCALabel1.Text, LocClv_Ciudad)
                        'LocTipoApar = Me.TIPOAPARATOLabel1.Text
                        bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Tipo de Aparato: ", LocTipoApar, Me.TIPOAPARATOLabel1.Text, LocClv_Ciudad)
                        'statusTarjeta = Me.ComboBox2.Text
                        bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Status: ", statusTarjeta, Me.ComboBox2.Text, LocClv_Ciudad)
                        'LocTipservcabl = Me.ComboBox3.Text
                        bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Tipo Servicio : ", LocTipservcabl, Me.ComboBox3.Text, LocClv_Ciudad)
                        'LocTipoCablemodem = Me.ComboBox4.Text
                        bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Tipo de Cablemodem: ", LocTipoCablemodem, Me.ComboBox4.Text, LocClv_Ciudad)
                        'Obs = Me.ObsTextBox1.Text
                        bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Observaciones: ", Obs, Me.ObsTextBox1.Text, LocClv_Ciudad)
                        'Activacion = Me.Fecha_ActivacionTextBox.Text
                        bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Fecha De Activacion: ", Activacion, Me.Fecha_ActivacionTextBox.Text, LocClv_Ciudad)
                        'Suspension = Me.Fecha_suspensionTextBox.Text
                        bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Fecha de Supension: ", Suspension, Me.Fecha_suspensionTextBox.Text, LocClv_Ciudad)
                        'LocTranspaso = Me.Fecha_TraspasoTextBox.Text
                        bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Fecha De Transpaso: ", LocTranspaso, Me.Fecha_TraspasoTextBox.Text, LocClv_Ciudad)
                        'Baja = Me.Fecha_bajaTextBox.Text
                        bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Fecha De Baja: ", Baja, Me.Fecha_bajaTextBox.Text, LocClv_Ciudad)

                        '1er Pago
                        If Me.Ventacablemodem1CheckBox.CheckState = CheckState.Checked Then
                            valida1 = "True"
                        ElseIf Me.Ventacablemodem1CheckBox.CheckState = CheckState.Unchecked Then
                            valida1 = "False"
                        End If

                        bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Se Vende El Cablemodem 1er Pago: ", Loc1pago, valida1, LocClv_Ciudad)

                        '2do pago Loc2pago

                        If Me.Ventacablemodem2CheckBox.CheckState = CheckState.Checked Then
                            Valida2 = "True"
                        ElseIf Me.Ventacablemodem2CheckBox.CheckState = CheckState.Unchecked Then
                            Valida2 = "False"
                        End If

                        bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Se Vende El Cablemodem 2do Pago: ", Loc2pago, Valida2, LocClv_Ciudad)

                        'Se Renta serenta
                        If Me.SeRentaCheckBox.CheckState = CheckState.Checked Then
                            Valida3 = "True"
                        ElseIf Me.SeRentaCheckBox.CheckState = CheckState.Unchecked Then
                            Valida3 = "False"
                        End If

                        bitsist(GloUsuario, CLng(FrmClientes.CONTRATOTextBox.Text), LocGloSistema, "Clientes", "Cablemodem: " + NombreMAC + " - " + "Se Renta El Cablemodem: ", serenta, Valida3, LocClv_Ciudad)
                End Select
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ToolStripButton8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton8.Click
        'VALIDACIONES DE FECHAS
        If Me.CortesiaCheckBox2.Checked And Me.TipoCortesiaComboBox.SelectedValue = 0 Then
            MsgBox("Se Requiere que Seleccione un Tipo de Cortes�a", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Me.ComboBox5.Text = "Contratado" Then
            If IsDate(Me.Fecha_solicitudTextBox1.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Contrataci�n", MsgBoxStyle.Information, "Atenci�n")
            ElseIf CDate(Me.Fecha_solicitudTextBox1.Text) > CDate(fecha_Hoy) Then
                MsgBox("La Fecha de Contrataci�n no puede ser Mayor a la Fecha Actual", MsgBoxStyle.Information, "Atenci�n")
            Else
                guardanet()
            End If
        End If
        If Me.ComboBox5.Text = "Suspendido" Or Me.ComboBox5.Text = "Desconectado" Or Me.ComboBox5.Text = "Desconectado Temporal" Then
            If IsDate(Me.Fecha_suspensionTextBox.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Suspensi�n o Desconexi�n", MsgBoxStyle.Information, "Atenci�n")
            ElseIf (CDate(Me.Fecha_solicitudTextBox1.Text) <= CDate(Me.Fecha_suspensionTextBox.Text)) And (CDate(Me.Fecha_suspensionTextBox.Text) <= CDate(fecha_Hoy)) Then
                guardanet()
            Else
                MsgBox("La Fecha de Suspensi�n o Desconexi�n debe Encontrarse entre la Fecha de Contratci�n y la Fecha Actual", MsgBoxStyle.Exclamation, "Atenci�n")
            End If
        End If
        If Me.ComboBox5.Text = "Instalado" Then
            If IsDate(Me.Fecha_instalacioTextBox.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Instalaci�n", MsgBoxStyle.Information, "Atenci�n")
            ElseIf Me.Ultimo_mesTextBox1.Text = "" Or CInt(Me.Ultimo_mesTextBox1.Text) = 0 Or CInt(Me.Ultimo_mesTextBox1.Text) > 12 Then
                MsgBox("Se Requiere que Capture el �ltimo Mes", MsgBoxStyle.Exclamation, "Atenci�n")
            ElseIf (Me.Ultimo_anioTextBox1.Text).Length <> 4 Then
                MsgBox("Se Requiere que Capture el �ltimo A�o", MsgBoxStyle.Exclamation, "Atenci�n")
            ElseIf (CDate(Me.Fecha_solicitudTextBox1.Text) <= CDate(Me.Fecha_instalacioTextBox.Text)) And (CDate(Me.Fecha_instalacioTextBox.Text) <= CDate(fecha_Hoy)) Then
                guardanet()
            Else
                MsgBox("La Fecha de Instalaci�n debe Encontrarse entre la Fecha de Contratci�n y la Fecha Actual", MsgBoxStyle.Information, "Atenci�n")

            End If
        End If
        If Me.ComboBox5.Text = "Fuera de Area" Then
            If IsDate(Me.Fecha_Fuera_AreaTextBox.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Fuera de �rea", MsgBoxStyle.Exclamation, "Atenci�n")
            ElseIf (CDate(Me.Fecha_solicitudTextBox1.Text) <= CDate(Me.Fecha_Fuera_AreaTextBox.Text)) And (CDate(Me.Fecha_Fuera_AreaTextBox.Text) <= CDate(fecha_Hoy)) Then
                guardanet()
            Else
                MsgBox("La Fecha de Fuera de �rea debe Encontrarse entre la Fecha de Contratci�n y la Fecha Actual", MsgBoxStyle.Information, "Atenci�n")
            End If
        End If
        If Me.ComboBox5.Text = "Baja" Then
            If IsDate(Me.Fecha_bajaTextBox.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Baja", MsgBoxStyle.Exclamation, "Atenci�n")
            ElseIf (CDate(Me.Fecha_solicitudTextBox1.Text) <= CDate(Me.Fecha_bajaTextBox.Text)) And (CDate(Me.Fecha_bajaTextBox.Text) <= CDate(fecha_Hoy)) Then
                guardanet()
            Else
                MsgBox("La Fecha de Baja debe Encontrarse entre la Fecha de Contratci�n y la Fecha Actual", MsgBoxStyle.Information, "Atenci�n")
            End If
        End If
        'MsgBox(GloClvUnicaNet)

    End Sub
    Private Sub guardanet()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.CONSULTACONTNETBindingSource.EndEdit()
        Me.CONSULTACONTNETTableAdapter.Connection = CON
        Me.CONSULTACONTNETTableAdapter.Update(Me.NewSofTvDataSet.CONSULTACONTNET)
        Me.Inserta_Rel_cortesia_FechaTableAdapter.Connection = CON
        Me.Inserta_Rel_cortesia_FechaTableAdapter.Fill(Me.Procedimientosarnoldo4.Inserta_Rel_cortesia_Fecha, GloClvUnicaNet, cortesiaint, 2)
        guardabitacoranet(4)
        CON.Close()
        Actualiza_usuario(GloClvUnicaNet, CInt(Me.ComboBox1.SelectedValue), 2)
        damedatosnet(GloClvUnicaNet)
        'JUANJO TIPOS CORTESIAS
        NUETIPOCORTESIA(LocGloClv_unianet, Me.TipoCortesiaComboBox.SelectedValue, 2, Me.CortesiaCheckBox2.CheckState)
        'FIN JUANJO TIPOS CORTESIAS
        GloGuardarNet = False
        MsgBox("Se ha Guardado con Ex�to", MsgBoxStyle.Information)
    End Sub
    Public Sub damedatosnet(ByVal clv_unicanet As Integer)
        Try
            If OpcionCli = "M" Then

                Dim comando As New SqlClient.SqlCommand
                Dim con90 As New SqlConnection(MiConexion)
                con90.Open()

                status = ""
                fecha_solicitud = ""
                fecha_instalacio = ""
                fecha_suspension = ""
                fecha_baja = ""
                fecha_Fuera_Area = ""
                FECHA_ULT_PAGO = ""
                PrimerMensualidad = False
                ultimo_mes = 0
                ultimo_anio = 0
                primerMesAnt = False
                statusAnt = ""
                facturaAnt = ""
                GENERAOSINSTA = False
                factura = ""
                Clv_Vendedor = 0
                Clv_Promocion = 0
                Email = ""
                Obs = ""
                DESCRIPCION = ""
                Cortesia = False



                comando = New SqlClient.SqlCommand

                With comando
                    .Connection = con90
                    .CommandText = "CONSULTACONTNET_Oledb"
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0

                    '@CLV_UNICANET BIGINT, @status varchar(1) output, @fecha_solicitud varchar(20) output, @fecha_instalacio varchar(20) output, 
                    '@fecha_suspension varchar(20) output , @fecha_baja varchar(20) output, @fecha_Fuera_Area varchar(20) output, @FECHA_ULT_PAGO varchar(max) output , 
                    '@PrimerMensualidad bit output, @ultimo_mes int output, @ultimo_anio int output, @primerMesAnt bit output ,
                    ' @statusAnt varchar(50) output, @facturaAnt varchar(50) output, @GENERAOSINSTA bit output, @factura varchar(50) output,
                    ' @Clv_Vendedor int output, @Clv_Promocion int output, @Email varchar(50) output, @Obs varchar(250) output,
                    '@DESCRIPCION varchar(250) output,@Cortesia bit output

                    Dim prm As New SqlParameter("@CLV_UNICANET", SqlDbType.BigInt)
                    Dim prm1 As New SqlParameter("@status", SqlDbType.VarChar, 1)
                    Dim prm2 As New SqlParameter("@fecha_solicitud", SqlDbType.VarChar, 20)
                    Dim prm3 As New SqlParameter("@fecha_instalacio", SqlDbType.VarChar, 20)
                    Dim prm4 As New SqlParameter("@fecha_suspension", SqlDbType.VarChar, 20)
                    Dim prm5 As New SqlParameter("@fecha_baja", SqlDbType.VarChar, 20)
                    Dim prm6 As New SqlParameter("@fecha_Fuera_Area", SqlDbType.VarChar, 20)
                    Dim prm7 As New SqlParameter("@FECHA_ULT_PAGO", SqlDbType.VarChar, 20)
                    Dim prm8 As New SqlParameter("@PrimerMensualidad", SqlDbType.Bit)
                    Dim prm9 As New SqlParameter("@ultimo_mes", SqlDbType.Int)
                    Dim prm10 As New SqlParameter("@ultimo_anio", SqlDbType.Int)
                    Dim prm11 As New SqlParameter("@primerMesAnt", SqlDbType.Bit)
                    Dim prm12 As New SqlParameter("@statusAnt", SqlDbType.VarChar, 50)
                    Dim prm13 As New SqlParameter("@facturaAnt", SqlDbType.VarChar, 50)
                    Dim prm14 As New SqlParameter("@GENERAOSINSTA", SqlDbType.Bit)
                    Dim prm15 As New SqlParameter("@factura", SqlDbType.VarChar, 50)
                    Dim prm16 As New SqlParameter("@Clv_Vendedor", SqlDbType.Int)
                    Dim prm17 As New SqlParameter("@Clv_Promocion", SqlDbType.Int)
                    Dim prm18 As New SqlParameter("@Email", SqlDbType.VarChar, 50)
                    Dim prm19 As New SqlParameter("@Obs", SqlDbType.VarChar, 250)
                    Dim prm20 As New SqlParameter("@DESCRIPCION", SqlDbType.VarChar, 250)
                    Dim prm21 As New SqlParameter("@Cortesia", SqlDbType.Bit)
                    Dim prm22 As New SqlParameter("@motivo", SqlDbType.VarChar, 500)



                    prm.Direction = ParameterDirection.Input
                    prm1.Direction = ParameterDirection.Output
                    prm2.Direction = ParameterDirection.Output
                    prm3.Direction = ParameterDirection.Output
                    prm4.Direction = ParameterDirection.Output
                    prm5.Direction = ParameterDirection.Output
                    prm6.Direction = ParameterDirection.Output
                    prm7.Direction = ParameterDirection.Output
                    prm8.Direction = ParameterDirection.Output
                    prm9.Direction = ParameterDirection.Output
                    prm10.Direction = ParameterDirection.Output
                    prm11.Direction = ParameterDirection.Output
                    prm12.Direction = ParameterDirection.Output
                    prm13.Direction = ParameterDirection.Output
                    prm14.Direction = ParameterDirection.Output
                    prm15.Direction = ParameterDirection.Output
                    prm16.Direction = ParameterDirection.Output
                    prm17.Direction = ParameterDirection.Output
                    prm18.Direction = ParameterDirection.Output
                    prm19.Direction = ParameterDirection.Output
                    prm20.Direction = ParameterDirection.Output
                    prm21.Direction = ParameterDirection.Output
                    prm22.Direction = ParameterDirection.Output



                    prm.Value = clv_unicanet
                    prm1.Value = " "
                    prm2.Value = " "
                    prm3.Value = " "
                    prm4.Value = " "
                    prm5.Value = " "
                    prm6.Value = " "
                    prm7.Value = " "
                    prm8.Value = False
                    prm9.Value = 0
                    prm10.Value = 0
                    prm11.Value = False
                    prm12.Value = " "
                    prm13.Value = " "
                    prm14.Value = False
                    prm15.Value = " "
                    prm16.Value = 0
                    prm17.Value = 0
                    prm18.Value = " "
                    prm19.Value = " "
                    prm20.Value = " "
                    prm21.Value = False
                    prm22.Value = " "

                    .Parameters.Add(prm)
                    .Parameters.Add(prm1)
                    .Parameters.Add(prm2)
                    .Parameters.Add(prm3)
                    .Parameters.Add(prm4)
                    .Parameters.Add(prm5)
                    .Parameters.Add(prm6)
                    .Parameters.Add(prm7)
                    .Parameters.Add(prm8)
                    .Parameters.Add(prm9)
                    .Parameters.Add(prm10)
                    .Parameters.Add(prm11)
                    .Parameters.Add(prm12)
                    .Parameters.Add(prm13)
                    .Parameters.Add(prm14)
                    .Parameters.Add(prm15)
                    .Parameters.Add(prm16)
                    .Parameters.Add(prm17)
                    .Parameters.Add(prm18)
                    .Parameters.Add(prm19)
                    .Parameters.Add(prm20)
                    .Parameters.Add(prm21)
                    .Parameters.Add(prm22)


                    Dim i As Integer = comando.ExecuteNonQuery()
                    status = prm1.Value
                    fecha_solicitud = prm2.Value
                    fecha_instalacio = prm3.Value
                    fecha_suspension = prm4.Value
                    fecha_baja = prm5.Value
                    fecha_Fuera_Area = prm6.Value
                    FECHA_ULT_PAGO = prm7.Value
                    PrimerMensualidad = prm8.Value
                    ultimo_mes = prm9.Value
                    ultimo_anio = prm10.Value
                    primerMesAnt = prm11.Value
                    statusAnt = prm12.Value
                    facturaAnt = prm13.Value
                    GENERAOSINSTA = prm14.Value
                    factura = prm15.Value
                    Clv_Vendedor = prm16.Value
                    Clv_Promocion = prm17.Value
                    Email = prm18.Value
                    Obs = prm19.Value
                    DESCRIPCION = prm20.Value
                    Cortesia = prm21.Value
                    Me.Label49.Text = prm22.Value

                End With
                Descuento = Me.DescuentoLabel3.Text
                con90.Close()
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button18_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button18.Click
        GloOpPermiso = 3
        GloPermisoCortesia = 0
        FrmAccesopoUsuario.Show()
    End Sub
    Public Sub Consulta_Internet()
        Dim habilita As String
        Dim contv As Integer
        Dim con13 As New SqlClient.SqlConnection(MiConexion)
        'GloOp = 1
        con13.Open()
        Me.VerAparatodelClienteTableAdapter.Connection = con13
        Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, GloContratonet)
        Me.CONSULTACLIENTESNETTableAdapter.Connection = con13
        Me.CONSULTACLIENTESNETTableAdapter.Fill(Me.DataSetLidia2.CONSULTACLIENTESNET, Contrato, GloContratonet)
        Me.DameFechaHabilitarTableAdapter.Connection = con13
        Me.DameFechaHabilitarTableAdapter.Fill(Me.DataSetLidia.DameFechaHabilitar, GloContratonet, 1, habilita)
        con13.Close()
        If habilita <> "01/01/1900" Then
            Me.Label5.Visible = True
            Me.Label5.Text = (" El Servicio es de Prueba y Vence en:" & " " & habilita)
            Me.Label5.BackColor = Color.Yellow
            Me.Label5.ForeColor = Color.Red
        End If
        damedatosnet2()
    End Sub
    
    Public Sub Consulta_ContInt()
        Dim con27 As New SqlClient.SqlConnection(MiConexion)
        damedatosnet(GloClvUnicaNet)
        If IdSistema = "LO" Or IdSistema = "YU" Then
            Checa_Forzoso(GloClvUnicaNet, Contrato)
        End If
        con27.Open()
        Me.CONSULTACONTNETTableAdapter.Connection = con27
        Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, GloClvUnicaNet)
        Me.CONRel_ContNet_UsuariosTableAdapter.Connection = con27
        Me.CONRel_ContNet_UsuariosTableAdapter.Fill(Me.DataSetEDGAR.CONRel_ContNet_Usuarios, GloClvUnicaNet)
        Muestra_Usuarios(GloClvUnicaNet, 2)
        con27.Close()
        BuscaDescNet()
    End Sub
    Private Sub BuscaDescNet()
        Dim CON2 As New SqlConnection(MiConexion)
        Dim eRes As Integer = 0
        Dim eMsg As String = Nothing
        If GloClvUnicaNet > 0 Then
            CON2.Open()
            Me.ChecaRelCteDescuentoTableAdapter.Connection = CON2
            Me.ChecaRelCteDescuentoTableAdapter.Fill(Me.DataSetEric.ChecaRelCteDescuento, GloClvUnicaNet, 2, eRes, eMsg)
            CON2.Close()
            If eRes = 1 Then
                CON2.Open()
                Me.ConRelCteDescuentoTableAdapter.Connection = CON2
                Me.ConRelCteDescuentoTableAdapter.Fill(Me.DataSetEric.ConRelCteDescuento, GloClvUnicaNet, 2)
                CON2.Close()
                Me.DescuentoLabel3.Visible = True
                Me.Label44.Visible = True
            Else
                Me.DescuentoLabel3.Visible = False
                Me.Label44.Visible = False
            End If
        End If
    End Sub
    Private Sub ToolStripButton7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton7.Click
        Me.CONSULTACONTNETBindingSource.CancelEdit()
    End Sub

    Private Sub Button23_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button23.Click
        If Me.CortesiaCheckBox2.Checked = False Then
            eTipSer = 2
            If eAccesoAdmin = True Then
                eClv_UnicaNet = GloClvUnicaNet
                FrmRelCteDescuento.Show()
            Else
                eGloDescuento = 0
                eGloTipSerDesc = 2
                FrmAccesopoUsuario.Show()
            End If
        Else
            MsgBox("No Puede Aplicar un Descuento, ya que cuenta con una Cortes�a", , "Atenci�n")
        End If
    End Sub

    Private Sub ToolStripButton6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton6.Click
        If Me.ComboBox2.Text = "Instalado" Then
            If IsDate(Me.Fecha_ActivacionTextBox.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Activaci�n de Manera Correcta", MsgBoxStyle.Information)
            Else
                guardanet2()
            End If
        ElseIf Me.ComboBox2.Text = "Baja" Then
            If IsDate(Me.Fecha_bajaTextBox.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Baja", MsgBoxStyle.Information)
            Else
                guardanet2()
            End If
        ElseIf Me.ComboBox2.Text = "Traspasado" Then
            If IsDate(Me.Fecha_TraspasoTextBox.Text) = False Then
                MsgBox("Se Requiere que se Capture la Fecha de Activaci�n", MsgBoxStyle.Information)
            Else
                guardanet2()
            End If
        ElseIf Me.ComboBox2.Text = "Por Asignar" Then
            guardanet2()
        End If
    End Sub
    Public Sub guardanet2()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.CONSULTACLIENTESNETBindingSource.EndEdit()
        Me.CONSULTACLIENTESNETTableAdapter.Connection = CON
        Me.CONSULTACLIENTESNETTableAdapter.Update(Me.DataSetLidia2.CONSULTACLIENTESNET)
        If (Me.txtMacCablModem.Text.ToUpper) <> (Me.MACCABLEMODEMLabel1.Text.ToUpper) Then
            bec_bnd = True
            bndcambiocable = True
            maccablemodem = Me.txtMacCablModem.Text
            Me.Activa()
        End If
        guardabitacoranet(5)
        CON.Close()
        'MsgBox("Se ha Guardado con Ex�to", MsgBoxStyle.Information)
        damedatosnet2()
    End Sub
    Public Sub damedatosnet2()
        If OpcionCli = "M" Then

            Locmarca = Me.MARCALabel1.Text
            LocTipoApar = Me.TIPOAPARATOLabel1.Text
            statusTarjeta = Me.ComboBox2.Text
            LocTipservcabl = Me.ComboBox3.Text
            LocTipoCablemodem = Me.ComboBox4.Text
            Obs = Me.ObsTextBox1.Text
            Activacion = Me.Fecha_ActivacionTextBox.Text
            Suspension = Me.Fecha_suspensionTextBox.Text
            LocTranspaso = Me.Fecha_TraspasoTextBox.Text
            Baja = Me.Fecha_bajaTextBox.Text

            If Me.Ventacablemodem1CheckBox.CheckState = CheckState.Checked Then
                Loc1pago = "True"
            ElseIf Me.Ventacablemodem1CheckBox.CheckState = CheckState.Unchecked Then
                Loc1pago = "False"
            End If

            If Me.Ventacablemodem2CheckBox.CheckState = CheckState.Checked Then
                Loc2pago = "True"
            ElseIf Me.Ventacablemodem2CheckBox.CheckState = CheckState.Unchecked Then
                Loc2pago = "False"
            End If

            If Me.SeRentaCheckBox.CheckState = CheckState.Checked Then
                serenta = "True"
            ElseIf Me.SeRentaCheckBox.CheckState = CheckState.Unchecked Then
                serenta = "False"
            End If

        End If
    End Sub
    Public Sub Elimina_internet()
        Dim CONT, cont2 As Integer
        Dim PPAL As Boolean
        Dim CON As New SqlConnection(MiConexion)

        If Me.Panel6.Visible = True Then
            CON.Open()
            Me.PrimerMesCLIENTESTableAdapter.Connection = CON
            Me.PrimerMesCLIENTESTableAdapter.Fill(Me.DataSetLidia.PrimerMesCLIENTES, GloClvUnicaNet, 0, CONT, PPAL)
            CON.Close()
            'Me.CONTARCLIENTESTableAdapter.Connection = CON
            'Me.CONTARCLIENTESTableAdapter.Fill(Me.DataSetLidia.CONTARCLIENTES, GloContratoNet2, 5, cont2)
            CONT = Checar_Equipos(GloContratoNet2, 5)
            If CONT = 1 Then
                If PPAL = False Then
                    guardabitacoranet(2)
                    CON.Open()
                    Me.CONSULTACONTNETTableAdapter.Connection = CON
                    Me.CONSULTACONTNETTableAdapter.Delete(GloClvUnicaNet)
                    CON.Close()
                End If
                If PPAL = True Then
                    If cont2 > 0 Then
                        MsgBox("No se Puede Eliminar el Paquete Principal Sin Antes Haber Eliminado los Paquetes Adicionales", MsgBoxStyle.Information)
                    ElseIf contador = 0 Then
                        guardabitacoranet(2)
                        CON.Open()
                        Me.CONSULTACONTNETTableAdapter.Connection = CON
                        Me.CONSULTACONTNETTableAdapter.Delete(GloClvUnicaNet)
                        CON.Close()
                        ' Me.CREAARBOL()
                    End If
                End If

            ElseIf CONT = 0 Then
                MsgBox("No Se Puede Eliminar El Servicio ", MsgBoxStyle.Information)
            End If
        End If
        If Me.Panel5.Visible = True Then
            'Me.CONTARCLIENTESTableAdapter.Connection = CON
            'Me.CONTARCLIENTESTableAdapter.Fill(DataSetLidia.CONTARCLIENTES, GloContratoNet2, 0, CONT)
            CONT = Checar_Equipos(GloContratoNet2, 0)
            If CONT = 0 Then
                guardabitacoranet(2)
                CON.Open()
                Me.CONSULTACLIENTESNETTableAdapter.Connection = CON
                Me.CONSULTACLIENTESNETTableAdapter.Delete(Contrato, GloContratoNet2)
                CON.Close()
            ElseIf CONT > 0 Then
                MsgBox("Primero Debes Eliminar Los Servicios ", MsgBoxStyle.Information)
            End If
            'Me.CREAARBOL()
        End If
    End Sub
    Private Sub ToolStripButton5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton5.Click
        Me.CONSULTACLIENTESNETBindingSource.CancelEdit()
    End Sub

    Private Sub Fecha_ActivacionTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fecha_ActivacionTextBox.TextChanged
        Try
            If DateValue(Me.Fecha_ActivacionTextBox.Text) = DateValue("01/01/1900") Then
                Me.Fecha_ActivacionTextBox.Text = ""
                Exit Sub
            End If
        Catch
        End Try
    End Sub

    Private Sub Fecha_SuspencionTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fecha_SuspencionTextBox.TextChanged
        Try
            If DateValue(Me.Fecha_SuspencionTextBox.Text) = DateValue("01/01/1900") Then
                Me.Fecha_SuspencionTextBox.Text = ""
            End If
        Catch
        End Try
    End Sub

    Private Sub Fecha_TraspasoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fecha_TraspasoTextBox.TextChanged
        Try
            If DateValue(Me.Fecha_TraspasoTextBox.Text) = DateValue("01/01/1900") Then
                Me.Fecha_TraspasoTextBox.Text = ""
                Exit Sub
            End If
        Catch
        End Try
    End Sub

    Private Sub Fecha_BajaTextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fecha_BajaTextBox1.TextChanged
        Try
            If DateValue(Me.Fecha_BajaTextBox1.Text) = DateValue("01/01/1900") Then
                Me.Fecha_BajaTextBox1.Text = ""
                Exit Sub
            End If
        Catch
        End Try
    End Sub

    Private Sub Fecha_solicitudTextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fecha_solicitudTextBox1.TextChanged
        Try
            If DateValue(Me.Fecha_solicitudTextBox1.Text) = DateValue("01/01/1900") Then
                Me.Fecha_solicitudTextBox1.Text = ""
            End If
        Catch
        End Try
    End Sub

    Private Sub Fecha_instalacioTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fecha_instalacioTextBox.TextChanged
        Try
            If DateValue(Me.Fecha_instalacioTextBox.Text) = DateValue("01/01/1900") Then
                Me.Fecha_instalacioTextBox.Text = ""
            End If
        Catch
        End Try
    End Sub

    Private Sub Fecha_suspensionTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fecha_suspensionTextBox.TextChanged
        Try
            If DateValue(Me.Fecha_suspensionTextBox.Text) = DateValue("01/01/1900") Then
                Me.Fecha_suspensionTextBox.Text = ""
            End If
        Catch
        End Try
    End Sub

    Private Sub Fecha_bajaTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fecha_bajaTextBox.TextChanged
        Try
            If DateValue(Me.Fecha_bajaTextBox.Text) = DateValue("01/01/1900") Then
                Me.Fecha_bajaTextBox.Text = ""
            End If
        Catch
        End Try
    End Sub

    Private Sub Fecha_Fuera_AreaTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fecha_Fuera_AreaTextBox.TextChanged
        Try
            If DateValue(Me.Fecha_Fuera_AreaTextBox.Text) = DateValue("01/01/1900") Then
                Me.Fecha_Fuera_AreaTextBox.Text = ""
            End If
        Catch
        End Try
    End Sub

    Private Sub FECHA_ULT_PAGOTextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FECHA_ULT_PAGOTextBox1.TextChanged
        Try
            If DateValue(Me.FECHA_ULT_PAGOTextBox1.Text) = DateValue("01/01/1900") Then
                Me.FECHA_ULT_PAGOTextBox1.Text = ""
            End If
        Catch
        End Try
    End Sub

    Private Sub FrmInternet_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'If eGloDescuento = 1 And eGloTipSerDesc = 2 Then
        '    eGloDescuento = 0
        '    eGloTipSerDesc = 0
        '    eTipSer = 2
        '    eClv_UnicaNet = GloClvUnicaNet
        '    FrmRelCteDescuento.Show()
        'End If
        'If eBndDesc = True And eTipSer = 2 Then
        '    eBndDesc = False
        '    BuscaDescNet()
        'End If
        If eClv_TipoCliente = 3 Then 'And IdSistema = "VA"
            Me.Button26.Visible = True
        Else
            Me.Button26.Visible = False
        End If

        'If bec_bnd = True Then
        '    Activa()
        'End If
        ''Activa()
    End Sub
    Public Sub Activa()
        If bec_bnd = True Then
            bec_bnd = False
            ConInt.Open()
            With CmdInt
                .CommandText = "Cambiar_Mac"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = ConInt
                Dim Prm1 As New SqlParameter("@Contrato", SqlDbType.BigInt)
                Dim Prm2 As New SqlParameter("@Contratonet", SqlDbType.BigInt)
                Dim Prm3 As New SqlParameter("@Mac", SqlDbType.VarChar)
                Dim Prm4 As New SqlParameter("@Clv_cablemodem", SqlDbType.BigInt)
                Dim Prm5 As New SqlParameter("@Respuesta", SqlDbType.VarChar)

                Prm1.Direction = ParameterDirection.Input
                Prm2.Direction = ParameterDirection.Input
                Prm3.Direction = ParameterDirection.Input
                Prm4.Direction = ParameterDirection.Input
                Prm5.Direction = ParameterDirection.Input

                Prm1.Value = Contrato
                Prm2.Value = GloContratonet
                Prm3.Value = maccablemodem
                Prm4.Value = GloClv_Cablemodem
                Prm5.Value = ""

                .Parameters.Add(Prm1)
                .Parameters.Add(Prm2)
                .Parameters.Add(Prm3)
                .Parameters.Add(Prm4)
                .Parameters.Add(Prm5)


                Dim j As Integer = .ExecuteNonQuery


            End With
            ConInt.Close()
            MsgBox("Se Ha Cambiado Con �xito", MsgBoxStyle.Information)
            Me.Consulta_Internet()

        End If
    End Sub

    Private Sub FrmInternet_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.GotFocus

    End Sub

    Private Sub FrmInternet_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Leave

    End Sub

    Private Sub FrmInternet_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        restringir()
        'colorea(Me, Me.Name)
        Dim Conp As New SqlClient.SqlConnection(MiConexion)
        Conp.Open()
        ''TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerInternet' Puede moverla o quitarla seg�n sea necesario.
        Me.MuestraTipSerInternetTableAdapter.Connection = Conp
        Me.MuestraTipSerInternetTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerInternet)
        Conp.Close()
        ''TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.TipoCablemodem' Puede moverla o quitarla seg�n sea necesario.
        Conp.Open()
        Me.TipoCablemodemTableAdapter.Connection = Conp
        Me.TipoCablemodemTableAdapter.Fill(Me.NewSofTvDataSet.TipoCablemodem)
        Conp.Close()
        ''TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.StatusCableModem' Puede moverla o quitarla seg�n sea necesario.
        Conp.Open()
        Me.StatusCableModemTableAdapter.Connection = Conp
        Me.StatusCableModemTableAdapter.Fill(Me.NewSofTvDataSet.StatusCableModem)
        Conp.Close()
        ''TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetEDGAR.MuestraPromotoresNet' Puede moverla o quitarla seg�n sea necesario.
        Conp.Open()
        Me.MuestraPromotoresNetTableAdapter.Connection = Conp
        Me.MuestraPromotoresNetTableAdapter.Fill(Me.DataSetEDGAR.MuestraPromotoresNet)
        Conp.Close()
        ''TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.StatusNet' Puede moverla o quitarla seg�n sea necesario.
        Conp.Open()
        Me.StatusNetTableAdapter.Connection = Conp
        Me.StatusNetTableAdapter.Fill(Me.NewSofTvDataSet.StatusNet)
        Conp.Close()
        My.Application.DoEvents()
        If OpcionCli = "C" Then
            Me.BindingNavigator1.Enabled = False
            Me.BindingNavigator2.Enabled = False
            Me.ComboBox2.Enabled = False
            Me.ComboBox3.Enabled = False
            Me.ComboBox4.Enabled = False
            Me.Button1.Enabled = False
            Me.Button18.Enabled = False
            Me.Button23.Enabled = False
            Me.EmailTextBox1.Enabled = False
            Me.TextBox5.Enabled = False
            Me.Ultimo_anioTextBox1.Enabled = False
            Me.ComboBox1.Enabled = False
        Else
            Me.TextBox5.Enabled = True
            Me.Ultimo_anioTextBox1.Enabled = True
            Me.EmailTextBox1.Enabled = True
            Me.Panel5.Enabled = True
            Me.Panel6.Enabled = True
            Me.BindingNavigator1.Enabled = True
            Me.BindingNavigator2.Enabled = True
            Me.ComboBox2.Enabled = True
            Me.ComboBox3.Enabled = True
            Me.ComboBox4.Enabled = True
            Me.Button1.Enabled = True
            Me.Button18.Enabled = True
            Me.Button23.Enabled = True
            Me.ComboBox1.Enabled = True

        End If
        If IdSistema = "AG" Or IdSistema = "VA" Then
            Me.CheckBox1.Enabled = False
        End If

        If IdSistema = "LO" Or IdSistema = "VA" Or IdSistema = "YU" Then
            Me.Button1.Visible = True
            Me.LblPlazoForzoso1.Visible = False
            Me.LblPlazoForzoso.Visible = False

            Me.txtMacCablModem.Enabled = True
        ElseIf IdSistema <> "LO" And IdSistema <> "VA" And IdSistema <> "YU" Then
            Me.Button1.Enabled = False
        End If

        'If IdSistema <> "LO" And IdSistema <> "" Then
        '    Me.Button1.Visible = False
        '    Me.txtMacCablModem.Enabled = True
        'Else
        '    Me.Button1.Visible = True
        '    Me.txtMacCablModem.Enabled = False
        'End If
        If GloTipoUsuario <> 40 Then
            Me.ComboBox1.Enabled = False

        ElseIf Me.ComboBox1.Enabled = True Then
        End If

        If eClv_TipoCliente = 3 And IdSistema = "VA" Then
            Me.Button26.Visible = True
        Else
            Me.Button26.Visible = False
        End If
        'MsgBox("Entre a : " + Me.Name)
        restringir()
        Muestra_Usuarios(GloClvUnicaNet, 2)
        'JUANJO TIPO CORTESIAS
        If Me.CortesiaCheckBox2.Checked Then
            Me.TipoCortesiaComboBox.Visible = True
            MUESTRATIPOCORTESIA()
            DAMETIPOCORTESIACLIENTE(LoClv_Unicanet, 2)
        Else
            Me.TipoCortesiaComboBox.Visible = False
        End If
    End Sub

    Private Sub TextBox5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox5.KeyPress
        eEntraUMB = True
        eEntraUM = False
    End Sub

    Private Sub TextBox5_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox5.LostFocus
        If Me.TextBox5.Text.Length < 3 Then
            MsgBox("Captura el Mes.", , "Atenci�n")
            Me.TextBox5.Focus()
        End If
    End Sub

    Private Sub TextBox5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox5.TextChanged
        If eEntraUMB = True Then
            If Me.TextBox5.Text.Length = 3 Then

                If Me.TextBox5.Text = "ENE" Then
                    Me.Ultimo_mesTextBox1.Text = 1
                ElseIf Me.TextBox5.Text = "FEB" Then
                    Me.Ultimo_mesTextBox1.Text = 2
                ElseIf Me.TextBox5.Text = "MAR" Then
                    Me.Ultimo_mesTextBox1.Text = 3
                ElseIf Me.TextBox5.Text = "ABR" Then
                    Me.Ultimo_mesTextBox1.Text = 4
                ElseIf Me.TextBox5.Text = "MAY" Then
                    Me.Ultimo_mesTextBox1.Text = 5
                ElseIf Me.TextBox5.Text = "JUN" Then
                    Me.Ultimo_mesTextBox1.Text = 6
                ElseIf Me.TextBox5.Text = "JUL" Then
                    Me.Ultimo_mesTextBox1.Text = 7
                ElseIf Me.TextBox5.Text = "AGO" Then
                    Me.Ultimo_mesTextBox1.Text = 8
                ElseIf Me.TextBox5.Text = "SEP" Then
                    Me.Ultimo_mesTextBox1.Text = 9
                ElseIf Me.TextBox5.Text = "OCT" Then
                    Me.Ultimo_mesTextBox1.Text = 10
                ElseIf Me.TextBox5.Text = "NOV" Then
                    Me.Ultimo_mesTextBox1.Text = 11
                ElseIf Me.TextBox5.Text = "DIC" Then
                    Me.Ultimo_mesTextBox1.Text = 12
                Else
                    MsgBox("El Mes Tecleado es Incorrecto.", , "Error")
                    Me.TextBox5.Clear()
                End If

            End If
        End If
    End Sub

    Private Sub Button26_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button26.Click
        LClv_Servicio = CInt(Me.Clv_ServicioTextBox.Text)
        LServicio = Me.DESCRIPCIONLabel1.Text
        LTip_Ser = 2
        LClvUnica = GloClvUnicaNet
        FrmPreciosHoteles.Show()
    End Sub

    

    Public Function Checar_Equipos(ByVal optcontrato As Integer, ByVal opp As Integer) As Integer
        Dim cmd As New SqlCommand
        Dim ConLidia As New SqlConnection(MiConexion)
        ConLidia.Open()
        With cmd
            .CommandText = "Contarclientes"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = ConLidia
            Dim prm As New SqlParameter("@Contratonet", SqlDbType.BigInt)
            Dim prm2 As New SqlParameter("@Opc", SqlDbType.Int)
            Dim prm3 As New SqlParameter("@Cont", SqlDbType.Int)
            prm.Direction = ParameterDirection.Input
            prm2.Direction = ParameterDirection.Input
            prm3.Direction = ParameterDirection.Output
            prm.Value = GloContratonet 'optcontrato
            prm2.Value = opp
            prm3.Value = 0
            .Parameters.Add(prm)
            .Parameters.Add(prm2)
            .Parameters.Add(prm3)
            Dim i As Integer = .ExecuteNonQuery
            Checar_Equipos = prm3.Value
        End With
        ConLidia.Close()
        'CREAARBOL()
    End Function
    Public Sub Borrar_Internet()
        Dim CONT As Integer
        Dim PPAL As Boolean
        Dim CON As New SqlConnection(MiConexion)

        If Me.Panel6.Visible = True Then
            CON.Open()
            Me.PrimerMesCLIENTESTableAdapter.Connection = CON
            Me.PrimerMesCLIENTESTableAdapter.Fill(Me.DataSetLidia.PrimerMesCLIENTES, GloClvUnicaNet, 0, CONT, PPAL)
            CON.Close()
            CONT = Checar_Equipos(GloClvUnicaNet, 0)
            'Me.CONTARCLIENTESTableAdapter.Connection = CON
            'Me.CONTARCLIENTESTableAdapter.Fill(Me.DataSetLidia.CONTARCLIENTES, GloContratoNet2, 5, cont2)

            If CONT = 1 Then
                If PPAL = False Then
                    guardabitacoranet(2)
                    CON.Open()
                    Me.CONSULTACONTNETTableAdapter.Connection = CON
                    Me.CONSULTACONTNETTableAdapter.Delete(GloClvUnicaNet)
                    CON.Close()
                    GloClvUnicaNet = 0
                End If
                If PPAL = True Then
                    If contador > 0 Then
                        MsgBox("No se Puede Eliminar el Paquete Principal Sin Antes Haber Eliminado los Paquetes Adicionales", MsgBoxStyle.Information)
                    ElseIf contador = 0 Then
                        guardabitacoranet(2)
                        CON.Open()
                        Me.CONSULTACONTNETTableAdapter.Connection = CON
                        Me.CONSULTACONTNETTableAdapter.Delete(GloClvUnicaNet)
                        GloClvUnicaNet = 0
                        CON.Close()
                    End If
                End If


            ElseIf CONT = 0 Then
                MsgBox("No Se Puede Eliminar El Servicio ", MsgBoxStyle.Information)
            End If
        End If
        If Me.Panel5.Visible = True Then
            contador = Checar_Equipos(GloContratonet, 0)
            'Me.CONTARCLIENTESTableAdapter.Connection = CON
            'Me.CONTARCLIENTESTableAdapter.Fill(DataSetLidia.CONTARCLIENTES, GloContratoNet2, 0, CONT)
            If contador = 0 Then
                guardabitacoranet(2)
                CON.Open()
                Me.CONSULTACLIENTESNETTableAdapter.Connection = CON
                Me.CONSULTACLIENTESNETTableAdapter.Delete(Contrato, GloContratonet)
                GloContratonet = 0
                Me.CONSULTACLIENTESNETTableAdapter.Fill(Me.DataSetLidia2.CONSULTACLIENTESNET, Contrato, 0)
                CON.Close()
            ElseIf contador > 0 Then
                MsgBox("Primero Debes Eliminar Los Servicios ", MsgBoxStyle.Information)
            End If
        End If
    End Sub

    Public Sub Activa_clientesNet()
        Dim con As New SqlClient.SqlConnection(MiConexion)
        'Try



        Dim cmd As New SqlCommand
        If (GloClv_Servicio > 0 Or GloClv_servTel > 0) And GLOMOVNET = 1 Then
            con.Open()
            locclv_servicio = GloClv_Servicio

            GloClv_Servicio = 0

            Me.Validate()
            'eCabModPropio Fu� reemplazado por True
            Me.CONSULTACLIENTESNETTableAdapter.Connection = con
            'MsgBox(CDate("01/01/1900").ToString("yyyyMMdd"))
            'Me.CONSULTACLIENTESNETTableAdapter.Insert(Contrato, "P", GloClv_Cablemodem, 0, False, False, 1, "1900-01-01", "1900-01-01", "1900-01-01", "1900-01-01", GloTipoCablemodem, "", eCabModPropio, 0, Equip_tel, LoContratonet)

            'Solo Puede haber un Cablemodem
            Me.CONSULTACLIENTESNETTableAdapter.Insert(Contrato, "P", GloClv_Cablemodem, 0, ePropio, False, 1, "1900-01-01", "1900-01-01", "1900-01-01", "1900-01-01", GloTipoCablemodem, "", eCabModPropio, 0, Equip_tel, LoContratonet)
            GloContratonet_Nuevo = LoContratonet
            GloContratonet = LoContratonet
            Me.CONSULTACLIENTESNETTableAdapter.Connection = con
            Me.CONSULTACLIENTESNETTableAdapter.Update(Me.DataSetLidia2.CONSULTACLIENTESNET)

            If De_Internet = True Then
                De_Internet = False
                If locclv_servicio > 0 Then
                    Me.CONSULTACONTNETTableAdapter.Connection = con
                    Me.CONSULTACONTNETTableAdapter.Insert(LoContratonet, locclv_servicio, "C", "1900-01-01", "1900-01-01", "1900-01-01", "1900-01-01", "1900-01-01", "1900-01-01", True, 0, 0, False, "C", "", True, "", 0, 0, "", "", Me.CortesiaCheckBox2.CheckState, LoClv_Unicanet)
                    GloClv_UnicaNet_Nuevo = LoClv_Unicanet
                    Me.CONSULTACONTNETTableAdapter.Connection = con
                    Me.CONSULTACONTNETTableAdapter.Update(Me.NewSofTvDataSet.CONSULTACONTNET)
                    Me.GUARDARRel_ContNet_UsuariosTableAdapter.Connection = con
                    Me.GUARDARRel_ContNet_UsuariosTableAdapter.Fill(Me.DataSetEDGAR.GUARDARRel_ContNet_Usuarios, GloClv_UnicaNet_Nuevo, GloClvUsuario)
                    'Muestra_Usuarios(LoClv_Unicanet, 2)
                End If
            End If

            'PEriodo
            Dime_Que_Periodo()
            ''Poner que Periodo es el Cliente

            ''
            ''Y Esto para Que no mames FrmClientes.Consulta_cliente()


            'Me.DIMEQUEPERIODODECORTETableAdapter.Connection = CON
            'Me.DIMEQUEPERIODODECORTETableAdapter.Fill(Me.DataSetLidia.DIMEQUEPERIODODECORTE, Contrato)
            'Me.CONSULTARCLIENTETableAdapter.Connection = CON
            'Me.CONSULTARCLIENTETableAdapter.Fill(Me.NewSofTvDataSet.CONSULTARCLIENTE, Me.CONTRATOTextBox.Text)
            'Me.CREAARBOL()
            Me.Panel5.Hide()
            Me.Panel6.Show()
            Me.VerAparatodelClienteTableAdapter.Connection = con
            Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(LoContratonet, Long))
            Me.CONSULTACLIENTESNETTableAdapter.Connection = con
            Me.CONSULTACLIENTESNETTableAdapter.Fill(Me.DataSetLidia2.CONSULTACLIENTESNET, Contrato, CType(LoContratonet, Long))
            Me.CONSULTACONTNETTableAdapter.Connection = con
            Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(LoClv_Unicanet, Long)))
            Me.CONRel_ContNet_UsuariosTableAdapter.Connection = con
            Me.CONRel_ContNet_UsuariosTableAdapter.Fill(Me.DataSetEDGAR.CONRel_ContNet_Usuarios, LoClv_Unicanet)
            'Muestra_Usuarios(LoClv_Unicanet, 2)
            con.Close()
            If De_Tele = True Then
                De_Tele = False
                'GloContratonet = LoContratonet
                frmTelefonia.Guardar_ContTel(1)
            End If
            cmd = New SqlClient.SqlCommand()
            con.Open()
            With cmd
                .CommandText = "Dame_Mac_CableDeco"
                .Connection = con
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                '@contratonet bigint,@clv_unicanet bigint, @op int,@Mac varchar(max) output
                Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
                Dim prm1 As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
                Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
                Dim prm3 As New SqlParameter("@Mac", SqlDbType.VarChar, 200)

                prm.Direction = ParameterDirection.Input
                prm1.Direction = ParameterDirection.Input
                prm2.Direction = ParameterDirection.Input
                prm3.Direction = ParameterDirection.Output

                prm.Value = LoContratonet
                prm1.Value = 0
                prm2.Value = 1
                prm3.Value = ""

                .Parameters.Add(prm)
                .Parameters.Add(prm1)
                .Parameters.Add(prm2)
                .Parameters.Add(prm3)

                Dim i As Integer = cmd.ExecuteNonQuery()

                If IsDBNull(prm3.Value) = True Then
                    NombreMAC = ""
                Else

                    NombreMAC = prm3.Value

                End If

            End With
            con.Close()

            'Aqui stoy
            bitsist(GloUsuario, Contrato, LocGloSistema, "Clientes", "Se Agrego un Cablemodem Nuevo: " + NombreMAC, " ", " Se Agrego un Cablemodem Nuevo ", LocClv_Ciudad)


            '--Dim lnivel As Integer = 0
            'Dim etree As System.Windows.Forms.TreeViewEventArgs = Me.TreeView1.Nodes
            'For lnivel = 0 To etree.Node.Level
            'Next
            GloClv_Servicio = 0
            GloClv_Cablemodem = 0
            GLOMOVNET = 0
            'GloContratonet = LoContratonet
        ElseIf GloClv_Servicio > 0 And GLOMOVNET = 2 Then
            con.Open()
            locclv_servicio = GloClv_Servicio
            GloClv_Servicio = 0
            LoContratonet = GloContratonet
            Me.Validate()
            '--Me.CONSULTACLIENTESNETTableAdapter.Insert(Contrato, "P", GloClv_Cablemodem, 0, False, False, 1, "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", 1, "", False, LoContratonet)
            '--Me.CONSULTACLIENTESNETTableAdapter.Update(Me.NewSofTvDataSet.CONSULTACLIENTESNET)



            Me.HABILITACABLEMODEMTableAdapter.Connection = con
            Me.HABILITACABLEMODEMTableAdapter.Fill(Me.NewSofTvDataSet.HABILITACABLEMODEM, New System.Nullable(Of Long)(CType(GloContratonet, Long)))
            Me.CONSULTACONTNETTableAdapter.Connection = con
            Me.CONSULTACONTNETTableAdapter.Insert(GloContratonet, locclv_servicio, "C", "1900-01-01", "1900-01-01", "1900-01-01", "1900-01-01", "1900-01-01", "1900-01-01", True, 0, 0, False, "C", "", True, "", 0, 0, "", "", Me.CortesiaCheckBox2.CheckState, LoClv_Unicanet)
            Me.CONSULTACONTNETTableAdapter.Connection = con
            Me.CONSULTACONTNETTableAdapter.Update(Me.NewSofTvDataSet.CONSULTACONTNET)
            Me.GUARDARRel_ContNet_UsuariosTableAdapter.Connection = con
            Me.GUARDARRel_ContNet_UsuariosTableAdapter.Fill(Me.DataSetEDGAR.GUARDARRel_ContNet_Usuarios, LoClv_Unicanet, GloClvUsuario)
            'Muestra_Usuarios(LoClv_Unicanet, 2)
            ''PEriodo
            Dime_Que_Periodo()
            '14Ene2008 edgar FrmClientes.Consulta_cliente()
            'Me.DIMEQUEPERIODODECORTETableAdapter.Connection = CON
            'Me.DIMEQUEPERIODODECORTETableAdapter.Fill(Me.DataSetLidia.DIMEQUEPERIODODECORTE, GloContratonet)
            'Me.CONSULTARCLIENTETableAdapter.Connection = CON
            'Me.CONSULTARCLIENTETableAdapter.Fill(Me.NewSofTvDataSet.CONSULTARCLIENTE, Me.CONTRATOTextBox.Text)
            con.Close()

            Me.Panel5.Hide()
            Me.Panel6.Show()
            con.Open()
            Me.VerAparatodelClienteTableAdapter.Connection = con
            Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(LoContratonet, Long))
            Me.CONSULTACLIENTESNETTableAdapter.Connection = con
            Me.CONSULTACLIENTESNETTableAdapter.Fill(Me.DataSetLidia2.CONSULTACLIENTESNET, Contrato, CType(LoContratonet, Long))
            Me.CONSULTACONTNETTableAdapter.Connection = con
            Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(LoClv_Unicanet, Long)))
            Me.CONRel_ContNet_UsuariosTableAdapter.Connection = con
            Me.CONRel_ContNet_UsuariosTableAdapter.Fill(Me.DataSetEDGAR.CONRel_ContNet_Usuarios, LoClv_Unicanet)
            con.Close()
            'Muestra_Usuarios(LoClv_Unicanet, 2)


            cmd = New SqlClient.SqlCommand()
            con.Open()
            With cmd
                .CommandText = "Dame_Mac_CableDeco"
                .Connection = con
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                '@contratonet bigint,@clv_unicanet bigint, @op int,@Mac varchar(max) output
                Dim prm As New SqlParameter("@contratonet", SqlDbType.BigInt)
                Dim prm1 As New SqlParameter("@clv_unicanet", SqlDbType.BigInt)
                Dim prm2 As New SqlParameter("@op", SqlDbType.Int)
                Dim prm3 As New SqlParameter("@Mac", SqlDbType.VarChar, 200)

                prm.Direction = ParameterDirection.Input
                prm1.Direction = ParameterDirection.Input
                prm2.Direction = ParameterDirection.Input
                prm3.Direction = ParameterDirection.Output

                prm.Value = LoContratonet
                prm1.Value = 0
                prm2.Value = 1
                prm3.Value = ""

                .Parameters.Add(prm)
                .Parameters.Add(prm1)
                .Parameters.Add(prm2)
                .Parameters.Add(prm3)

                Dim i As Integer = cmd.ExecuteNonQuery()

                NombreMAC = prm3.Value

            End With
            con.Close()

            'Aqui stoy
            'bitsist(GloUsuario, Contrato, LocGloSistema, "Clientes", "Se Agrego un Cablemodem Nuevo: " + NombreMAC, " ", "Se Agrego un Cablemodem Nuevo", LocClv_Ciudad)


            '--Dim lnivel As Integer = 0
            'Dim etree As System.Windows.Forms.TreeViewEventArgs = Me.TreeView1.Nodes
            'For lnivel = 0 To etree.Node.Level
            'Next
            GloClv_Servicio = 0
            GloClv_Cablemodem = 0
            GLOMOVNET = 0
            'Equip_Int = False
            'Equip_tel = False
        End If

        Muestra_Usuarios(LoClv_Unicanet, 2)
        'Catch ex As Exception

        '    'con.Close()
        '    'MsgBox(ex.Message)
        'End Try
    End Sub
    Private Sub Ultimo_mesTextBox1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Ultimo_mesTextBox1.Click
        eEntraUM = True
        eEntraUMB = False
    End Sub

    Private Sub Ultimo_mesTextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ultimo_mesTextBox1.TextChanged
        If eEntraUM = True Then
            If IsNumeric(Ultimo_mesTextBox1.Text) = True Then
                If Me.Ultimo_mesTextBox1.Text = 1 Then
                    Me.TextBox5.Text = "ENE"
                ElseIf Me.Ultimo_mesTextBox1.Text = 2 Then
                    Me.TextBox5.Text = "FEB"
                ElseIf Me.Ultimo_mesTextBox1.Text = 3 Then
                    Me.TextBox5.Text = "MAR"
                ElseIf Me.Ultimo_mesTextBox1.Text = 4 Then
                    Me.TextBox5.Text = "ABR"
                ElseIf Me.Ultimo_mesTextBox1.Text = 5 Then
                    Me.TextBox5.Text = "MAY"
                ElseIf Me.Ultimo_mesTextBox1.Text = 6 Then
                    Me.TextBox5.Text = "JUN"
                ElseIf Me.Ultimo_mesTextBox1.Text = 7 Then
                    Me.TextBox5.Text = "JUL"
                ElseIf Me.Ultimo_mesTextBox1.Text = 8 Then
                    Me.TextBox5.Text = "AGO"
                ElseIf Me.Ultimo_mesTextBox1.Text = 9 Then
                    Me.TextBox5.Text = "SEP"
                ElseIf Me.Ultimo_mesTextBox1.Text = 10 Then
                    Me.TextBox5.Text = "OCT"
                ElseIf Me.Ultimo_mesTextBox1.Text = 11 Then
                    Me.TextBox5.Text = "NOV"
                ElseIf Me.Ultimo_mesTextBox1.Text = 12 Then
                    Me.TextBox5.Text = "DIC"
                Else
                    Me.TextBox5.Text = "---"
                End If
            End If
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        FrmSelTecnicos.Show()
    End Sub
    Private Sub Dime_Que_Periodo()
        Try
            Dim ConPer As New SqlConnection(MiConexion)
            Dim Cmd As New SqlCommand
            ConPer.Open()
            With Cmd
                .CommandText = "DIMEQUEPERIODODECORTE"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = ConPer
                Dim Pmt As New SqlParameter("@contrato", SqlDbType.BigInt)
                Pmt.Direction = ParameterDirection.Input
                Pmt.Value = Contrato
                .Parameters.Add(Pmt)
                .ExecuteNonQuery()
            End With
            ConPer.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button26_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button26.LostFocus
        If eClv_TipoCliente = 3 Then 'And IdSistema = "VA"
            Me.Button26.Visible = True
        Else
            Me.Button26.Visible = False
        End If
    End Sub
    Private Sub MACCABLEMODEMLabel1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles MACCABLEMODEMLabel1.TextChanged
        Me.txtMacCablModem.Text = Me.MACCABLEMODEMLabel1.Text
    End Sub
    Private Sub Actualiza_usuario(ByVal clv_unicanet As Long, ByVal clv_usuario As Integer, ByVal tipo_serv As Integer)

        Dim con As New SqlConnection(MiConexion)
        Dim str As New StringBuilder

        str.Append("Exec Actualiza_usuarios ")
        str.Append(CStr(clv_unicanet) & ", ")
        str.Append(CStr(clv_usuario) & ", ")
        str.Append(CStr(tipo_serv))

        Dim dataadapter As New SqlDataAdapter(str.ToString, con)
        Dim datatable As New DataTable
        Dim binding As New BindingSource

        Try
            con.Open()
            dataadapter.Fill(datatable)
            binding.DataSource = datatable
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            con.Close()
            con.Dispose()
        End Try

    End Sub
    Private Sub Muestra_Usuarios(ByVal clv_unicanet As Long, ByVal tipo_serv As Integer)



        Dim con As New SqlConnection(MiConexion)
        Dim str As New StringBuilder

        str.Append("Exec Muestra_Usuarios ")
        str.Append(CStr(clv_unicanet) & ", ")
        str.Append(CStr(tipo_serv))

        Dim dataadapter As New SqlDataAdapter(str.ToString, con)
        Dim datatable As New DataTable
        Dim binding As New BindingSource

        Try
            con.Open()
            dataadapter.Fill(datatable)
            binding.DataSource = datatable
            Me.ComboBox1.DataSource = binding
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            con.Close()
            con.Dispose()
        End Try


    End Sub

    Private Sub restringir()

        If GloTipoUsuario <> 40 Then
            Me.Fecha_ActivacionTextBox.Enabled = False
            Me.Fecha_bajaTextBox.Enabled = False
            Me.Fecha_bajaTextBox.Enabled = False
            Me.Fecha_solicitudTextBox1.Enabled = False
            Me.Fecha_instalacioTextBox.Enabled = False
            Me.Fecha_SuspencionTextBox.Enabled = False
            Me.Fecha_Fuera_AreaTextBox.Enabled = False
            Me.FECHA_ULT_PAGOTextBox1.Enabled = False
            Me.ComboBox1.Enabled = False
            Me.ComboBox5.Enabled = False
            Me.ComboBox6.Enabled = False
            Me.TextBox5.Enabled = False
            Me.Ultimo_anioTextBox1.Enabled = False
            Me.CortesiaCheckBox2.Enabled = False
            ToolStripButton8.Enabled = False
            Me.EmailTextBox1.Enabled = False
        Else
            Me.Fecha_ActivacionTextBox.Enabled = True
            Me.Fecha_bajaTextBox.Enabled = True
            Me.Fecha_bajaTextBox.Enabled = True
            Me.Fecha_solicitudTextBox1.Enabled = True
            Me.Fecha_instalacioTextBox.Enabled = True
            Me.Fecha_SuspencionTextBox.Enabled = True
            Me.Fecha_Fuera_AreaTextBox.Enabled = True
            Me.FECHA_ULT_PAGOTextBox1.Enabled = True
            Me.ComboBox1.Enabled = True
            Me.ComboBox5.Enabled = True
            Me.ComboBox6.Enabled = True
            Me.TextBox5.Enabled = True
            Me.Ultimo_anioTextBox1.Enabled = True
            Me.CortesiaCheckBox2.Enabled = True
            ToolStripButton8.Enabled = True
            Me.EmailTextBox1.Enabled = True
        End If

    End Sub

    Private Sub CortesiaCheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CortesiaCheckBox2.CheckedChanged
        If Me.CortesiaCheckBox2.Checked Then
            Me.TipoCortesiaComboBox.Visible = True
            MUESTRATIPOCORTESIA()
            DAMETIPOCORTESIACLIENTE(GloClvUnicaNet, 2)
        Else
            Me.TipoCortesiaComboBox.Visible = False
        End If
    End Sub

    Private Sub MUESTRATIPOCORTESIA()
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSql As New StringBuilder

        StrSql.Append("EXEC MUESTRATIPOCORTESIA ")

        Dim DA As New SqlDataAdapter(StrSql.ToString, CON)
        Dim DT As New DataTable

        Try
            CON.Open()
            DA.Fill(DT)
            Me.TipoCortesiaComboBox.DataSource = DT
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub DAMETIPOCORTESIACLIENTE(ByVal Clv_UnicaNetCortesia As Long, ByVal Clv_TipSerCortesia As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("DAMETIPOCORTESIACLIENTE", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@CLV_UNICANET", Clv_UnicaNetCortesia)
        CMD.Parameters.AddWithValue("@Clv_TipSer", Clv_TipSerCortesia)

        Dim Reader As SqlDataReader

        Try
            CON.Open()
            Reader = CMD.ExecuteReader
            While (Reader.Read)
                Me.TipoCortesiaComboBox.SelectedValue = Reader(0).ToString
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub NUETIPOCORTESIA(ByVal CLV_UNICANET As Long, ByVal CLV_CORTESIA As Integer, ByVal Clv_TipSerCortesia As Integer, ByVal OPCORTESIA As Boolean)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("NUETIPOCORTESIA", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@CLV_UNICANET", CLV_UNICANET)
        CMD.Parameters.AddWithValue("@CLV_CORTESIA", CLV_CORTESIA)
        CMD.Parameters.AddWithValue("@Clv_TipSer", Clv_TipSerCortesia)
        CMD.Parameters.AddWithValue("@OP", OPCORTESIA)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        FrmDatosDeInstalacionInt.Show()
    End Sub
End Class