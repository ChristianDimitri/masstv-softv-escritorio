<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmICABMNew
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Procedimientosarnoldo4 = New sofTV.Procedimientosarnoldo4()
        Me.MUESTRACONTTELBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACONTTELTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.MUESTRACONTTELTableAdapter()
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.MUESTRAICAM_porSOLBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRAICAM_porSOLTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRAICAM_porSOLTableAdapter()
        Me.MUESTRACONTNETBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACONTNETTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACONTNETTableAdapter()
        Me.MUESTRACABLEMODEMSDELCLI_porOpcionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter()
        Me.CONICABMBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONICABMTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONICABMTableAdapter()
        Me.NUECCABMBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NUECCABMTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.NUECCABMTableAdapter()
        Me.BORCCABMBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BORCCABMTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.BORCCABMTableAdapter()
        Me.TreeView2 = New System.Windows.Forms.TreeView()
        Me.MUESTRACABLEMODEMS_disponiblesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACABLEMODEMS_disponiblesTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACABLEMODEMS_disponiblesTableAdapter()
        Me.BORDetOrdSer_INTELIGENTEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BORDetOrdSer_INTELIGENTETableAdapter = New sofTV.NewSofTvDataSetTableAdapters.BORDetOrdSer_INTELIGENTETableAdapter()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACONTTELBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAICAM_porSOLBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACONTNETBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACABLEMODEMSDELCLI_porOpcionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONICABMBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NUECCABMBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BORCCABMBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACABLEMODEMS_disponiblesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BORDetOrdSer_INTELIGENTEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Procedimientosarnoldo4
        '
        Me.Procedimientosarnoldo4.DataSetName = "Procedimientosarnoldo4"
        Me.Procedimientosarnoldo4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MUESTRACONTTELBindingSource
        '
        Me.MUESTRACONTTELBindingSource.DataMember = "MUESTRACONTTEL"
        Me.MUESTRACONTTELBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'MUESTRACONTTELTableAdapter
        '
        Me.MUESTRACONTTELTableAdapter.ClearBeforeFill = True
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MUESTRAICAM_porSOLBindingSource
        '
        Me.MUESTRAICAM_porSOLBindingSource.DataMember = "MUESTRAICAM_porSOL"
        Me.MUESTRAICAM_porSOLBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRAICAM_porSOLTableAdapter
        '
        Me.MUESTRAICAM_porSOLTableAdapter.ClearBeforeFill = True
        '
        'MUESTRACONTNETBindingSource
        '
        Me.MUESTRACONTNETBindingSource.DataMember = "MUESTRACONTNET"
        Me.MUESTRACONTNETBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRACONTNETTableAdapter
        '
        Me.MUESTRACONTNETTableAdapter.ClearBeforeFill = True
        '
        'MUESTRACABLEMODEMSDELCLI_porOpcionBindingSource
        '
        Me.MUESTRACABLEMODEMSDELCLI_porOpcionBindingSource.DataMember = "MUESTRACABLEMODEMSDELCLI_porOpcion"
        Me.MUESTRACABLEMODEMSDELCLI_porOpcionBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter
        '
        Me.MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter.ClearBeforeFill = True
        '
        'CONICABMBindingSource
        '
        Me.CONICABMBindingSource.DataMember = "CONICABM"
        Me.CONICABMBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CONICABMTableAdapter
        '
        Me.CONICABMTableAdapter.ClearBeforeFill = True
        '
        'NUECCABMBindingSource
        '
        Me.NUECCABMBindingSource.DataMember = "NUECCABM"
        Me.NUECCABMBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NUECCABMTableAdapter
        '
        Me.NUECCABMTableAdapter.ClearBeforeFill = True
        '
        'BORCCABMBindingSource
        '
        Me.BORCCABMBindingSource.DataMember = "BORCCABM"
        Me.BORCCABMBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'BORCCABMTableAdapter
        '
        Me.BORCCABMTableAdapter.ClearBeforeFill = True
        '
        'TreeView2
        '
        Me.TreeView2.Location = New System.Drawing.Point(293, 228)
        Me.TreeView2.Name = "TreeView2"
        Me.TreeView2.Size = New System.Drawing.Size(121, 97)
        Me.TreeView2.TabIndex = 0
        '
        'MUESTRACABLEMODEMS_disponiblesBindingSource
        '
        Me.MUESTRACABLEMODEMS_disponiblesBindingSource.DataMember = "MUESTRACABLEMODEMS_disponibles"
        Me.MUESTRACABLEMODEMS_disponiblesBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRACABLEMODEMS_disponiblesTableAdapter
        '
        Me.MUESTRACABLEMODEMS_disponiblesTableAdapter.ClearBeforeFill = True
        '
        'BORDetOrdSer_INTELIGENTEBindingSource
        '
        Me.BORDetOrdSer_INTELIGENTEBindingSource.DataMember = "BORDetOrdSer_INTELIGENTE"
        Me.BORDetOrdSer_INTELIGENTEBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'BORDetOrdSer_INTELIGENTETableAdapter
        '
        Me.BORDetOrdSer_INTELIGENTETableAdapter.ClearBeforeFill = True
        '
        'FrmICABMNew
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1162, 570)
        Me.Controls.Add(Me.TreeView2)
        Me.Name = "FrmICABMNew"
        Me.Text = "FrmICABMNew"
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACONTTELBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAICAM_porSOLBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACONTNETBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACABLEMODEMSDELCLI_porOpcionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONICABMBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NUECCABMBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BORCCABMBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACABLEMODEMS_disponiblesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BORDetOrdSer_INTELIGENTEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Procedimientosarnoldo4 As sofTV.Procedimientosarnoldo4
    Friend WithEvents MUESTRACONTTELBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACONTTELTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.MUESTRACONTTELTableAdapter
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents MUESTRAICAM_porSOLBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAICAM_porSOLTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRAICAM_porSOLTableAdapter
    Friend WithEvents MUESTRACONTNETBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACONTNETTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACONTNETTableAdapter
    Friend WithEvents MUESTRACABLEMODEMSDELCLI_porOpcionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter
    Friend WithEvents CONICABMBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONICABMTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONICABMTableAdapter
    Friend WithEvents NUECCABMBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NUECCABMTableAdapter As sofTV.NewSofTvDataSetTableAdapters.NUECCABMTableAdapter
    Friend WithEvents BORCCABMBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BORCCABMTableAdapter As sofTV.NewSofTvDataSetTableAdapters.BORCCABMTableAdapter
    Friend WithEvents TreeView2 As System.Windows.Forms.TreeView
    Friend WithEvents MUESTRACABLEMODEMS_disponiblesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACABLEMODEMS_disponiblesTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACABLEMODEMS_disponiblesTableAdapter
    Friend WithEvents BORDetOrdSer_INTELIGENTEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BORDetOrdSer_INTELIGENTETableAdapter As sofTV.NewSofTvDataSetTableAdapters.BORDetOrdSer_INTELIGENTETableAdapter
End Class
