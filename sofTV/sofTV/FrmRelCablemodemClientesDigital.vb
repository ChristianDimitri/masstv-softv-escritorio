Imports System.Data.SqlClient

Public Class FrmRelCablemodemClientesDigital

    Public Sub CREAARBOL()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim I As Integer = 0
            Dim X As Integer = 0
            If GLOTRABAJO = "IAPAR" Or GLOTRABAJO = "IAPAR6" Then
                Me.MUESTRACABLEMODEMSDELCLI_porOpcionDIGTableAdapter.Connection = CON
                Me.MUESTRACABLEMODEMSDELCLI_porOpcionDIGTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACABLEMODEMSDELCLI_porOpcionDIG, New System.Nullable(Of Long)(CType(Contrato, Long)), "P", 0)
            ElseIf GLOTRABAJO = "BAPAR" Or GLOTRABAJO = "BAPAR6" Then
                Me.MUESTRACABLEMODEMSDELCLI_porOpcionDIGTableAdapter.Connection = CON
                Me.MUESTRACABLEMODEMSDELCLI_porOpcionDIGTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACABLEMODEMSDELCLI_porOpcionDIG, New System.Nullable(Of Long)(CType(Contrato, Long)), "P", 15)
            ElseIf GLOTRABAJO = "CAPAR" Or GLOTRABAJO = "CAPAR6" Then
                Me.MUESTRACABLEMODEMSDELCLI_porOpcionDIGTableAdapter.Connection = CON
                Me.MUESTRACABLEMODEMSDELCLI_porOpcionDIGTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACABLEMODEMSDELCLI_porOpcionDIG, New System.Nullable(Of Long)(CType(Contrato, Long)), "P", 16)
            End If
            Dim FilaRow As DataRow
            Dim FilacontNet As DataRow
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.MUESTRACABLEMODEMSDELCLI_porOpcionDIG.Rows
                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                Me.TreeView1.Nodes.Add(Trim(FilaRow("CONTRATONET").ToString()), Trim(FilaRow("MACCABLEMODEM").ToString()))
                Me.TreeView1.Nodes(I).Tag = Trim(FilaRow("CONTRATONET").ToString())
                Me.MUESTRACONTDIGTableAdapter.Connection = CON
                Me.MUESTRACONTDIGTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTDIG, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)))
                Me.TreeView1.Nodes(I).ForeColor = Color.Black
                For Each FilacontNet In Me.NewSofTvDataSet.MUESTRACONTDIG.Rows
                    Me.TreeView1.Nodes(I).Nodes.Add(Trim(FilacontNet("CLV_UNICANET").ToString()), Trim(FilacontNet("DESCRIPCION").ToString()) & " : " & Trim(FilacontNet("STATUS").ToString()))
                    Me.TreeView1.Nodes(I).Nodes(X).Tag = Trim(FilacontNet("CLV_UNICANET").ToString())
                    If Trim(FilacontNet("STATUS").ToString()) = "Suspendido" Then
                        Me.TreeView1.Nodes(I).Nodes(X).ForeColor = Color.Olive
                    ElseIf Trim(FilacontNet("STATUS").ToString()) = "Instalado" Or Trim(FilacontNet("STATUS").ToString()) = "Contratado" Then
                        Me.TreeView1.Nodes(I).Nodes(X).ForeColor = Color.Navy
                    Else
                        Me.TreeView1.Nodes(I).Nodes(X).ForeColor = Color.Red
                    End If
                    X += 1
                Next
                I += 1
            Next
            CON.Close()
            Me.TreeView1.ExpandAll()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Public Sub CREAARBOL2()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim I As Integer = 0
            Dim X As Integer = 0
            If (GLOTRABAJO = "IAPAR" Or GLOTRABAJO = "BAPAR") Or (GLOTRABAJO = "IAPAR" Or GLOTRABAJO = "BAPAR") Then
                Me.MUESTRAICAM_porSOLDIGTableAdapter.Connection = CON
                Me.MUESTRAICAM_porSOLDIGTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRAICAM_porSOLDIG, GloDetClave, gloClv_Orden, 1)
            ElseIf GLOTRABAJO = "CAPAR" Or GLOTRABAJO = "CAPAR6" Then
                Me.MUESTRAICAM_porSOLDIGTableAdapter.Connection = CON
                Me.MUESTRAICAM_porSOLDIGTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRAICAM_porSOLDIG, GloDetClave, gloClv_Orden, 2)
            End If
            Dim FilaRow As DataRow
            Dim FilacontNet As DataRow
            Me.TreeView2.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.MUESTRAICAM_porSOLDIG.Rows
                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                Me.TreeView2.Nodes.Add(Trim(FilaRow("CONTRATONET").ToString()), Trim(FilaRow("MACCABLEMODEM").ToString()))
                Me.TreeView2.Nodes(I).Tag = Trim(FilaRow("CONTRATONET").ToString())
                Me.MUESTRACONTDIGTableAdapter.Connection = CON
                Me.MUESTRACONTDIGTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTDIG, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)))
                Me.TreeView2.Nodes(I).ForeColor = Color.Black
                For Each FilacontNet In Me.NewSofTvDataSet.MUESTRACONTDIG.Rows
                    Me.TreeView2.Nodes(I).Nodes.Add(Trim(FilacontNet("CLV_UNICANET").ToString()), Trim(FilacontNet("DESCRIPCION").ToString()) & " : " & Trim(FilacontNet("STATUS").ToString()))
                    Me.TreeView2.Nodes(I).Nodes(X).Tag = Trim(FilacontNet("CLV_UNICANET").ToString())
                    If Trim(FilacontNet("STATUS").ToString()) = "Suspendido" Then
                        Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Olive
                    ElseIf Trim(FilacontNet("STATUS").ToString()) = "Instalado" Or Trim(FilacontNet("STATUS").ToString()) = "Contratado" Then
                        Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Navy
                    Else
                        Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Red
                    End If
                    X += 1
                Next
                I += 1
            Next
            CON.Close()
            Me.TreeView2.ExpandAll()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub FrmRelCablemodemClientesDigital_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.BORDetOrdSer_INTELIGENTETableAdapter.Connection = CON
        Me.BORDetOrdSer_INTELIGENTETableAdapter.Fill(Me.NewSofTvDataSet.BORDetOrdSer_INTELIGENTE, New System.Nullable(Of Long)(CType(GloDetClave, Long)))
        CON.Close()
        GloBndTrabajo = True
        GloBloqueaDetalle = True
    End Sub



    Private Sub FrmRelCablemodemClientesDigital_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        CREAARBOL()
        Me.CREAARBOL2()
        If GLOTRABAJO = "IAPAR" Or GLOTRABAJO = "IAPAR6" Then
            Me.Text = "Instalación de Aparato Digital"
            Me.Label3.Text = "Aparatos Digitales del Cliente por Asignar"
            Me.Label4.Text = "Instalar y Asignar a estos Aparatos Digitales"
        ElseIf GLOTRABAJO = "BAPAR" Or GLOTRABAJO = "BAPAR6" Then
            Me.Text = "Baja de Aparato Digital"
            Me.Label3.Text = "Aparatos Digitales del Cliente Activos"
            Me.Label4.Text = "Aparatos Digitales del Cliente Ejecutados a Baja"
        End If
        If Bloquea = True Or opcion = "M" Then
            Me.Button1.Enabled = False
            Me.Button2.Enabled = False
            Me.Button3.Enabled = False
            Me.Button4.Enabled = False
        End If
    End Sub



    Private Sub TreeView1_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView1.AfterSelect
        Try
            If e.Node.Level = 0 Then
                If IsNumeric(e.Node.Tag) = True Then
                    'Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))
                    Contratonet.Text = e.Node.Tag
                    MacCableModem.Text = e.Node.Text
                    '    Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))
                    'Else
                    '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                    'End If
                    'Else
                    'If IsNumeric(e.Node.Tag) Then
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                    'Else
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                    'End If
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If GLOTRABAJO = "IAPAR" Or GLOTRABAJO = "IAPAR6" Then
                Me.NUEIAPAR_SOLDIGTableAdapter.Connection = CON
                Me.NUEIAPAR_SOLDIGTableAdapter.Fill(Me.NewSofTvDataSet.NUEIAPAR_SOLDIG, GloDetClave, gloClv_Orden, Me.Contratonet.Text, 0)
            ElseIf GLOTRABAJO = "BAPAR" Or GLOTRABAJO = "BAPAR6" Then
                Me.NUEIAPAR_SOLDIGTableAdapter.Connection = CON
                Me.NUEIAPAR_SOLDIGTableAdapter.Fill(Me.NewSofTvDataSet.NUEIAPAR_SOLDIG, GloDetClave, gloClv_Orden, Me.Contratonet.Text, 2)
            ElseIf GLOTRABAJO = "CAPAR" Or GLOTRABAJO = "CAPAR6" Then
                Me.NUEIAPAR_SOLDIGTableAdapter.Connection = CON
                Me.NUEIAPAR_SOLDIGTableAdapter.Fill(Me.NewSofTvDataSet.NUEIAPAR_SOLDIG, GloDetClave, gloClv_Orden, Me.Contratonet.Text, 4)
            End If
            CON.Close()
            Me.CREAARBOL2()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If GLOTRABAJO = "IAPAR" Or GLOTRABAJO = "IAPAR6" Then
                If IsNumeric(Me.Contratonet1.Text) = True Then
                    Me.BorIAPAR_SOLTableAdapter.Connection = CON
                    Me.BorIAPAR_SOLTableAdapter.Fill(Me.NewSofTvDataSet.borIAPAR_SOL, GloDetClave, gloClv_Orden, Me.Contratonet1.Text, 0)
                End If
            ElseIf GLOTRABAJO = "CAPAR" Or GLOTRABAJO = "CAPAR6" Then
                If IsNumeric(Me.Contratonet1.Text) = True Then
                    Me.BorIAPAR_SOLTableAdapter.Connection = CON
                    Me.BorIAPAR_SOLTableAdapter.Fill(Me.NewSofTvDataSet.borIAPAR_SOL, GloDetClave, gloClv_Orden, Me.Contratonet1.Text, 2)
                End If
            End If
            CON.Close()
            Me.CREAARBOL2()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub TreeView2_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView2.AfterSelect
        Try
            If e.Node.Level = 0 Then
                If IsNumeric(e.Node.Tag) = True Then
                    'Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))
                    Contratonet1.Text = e.Node.Tag
                    MacCableModem1.Text = e.Node.Text
                    '    Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))
                    'Else
                    '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                    'End If
                    'Else
                    'If IsNumeric(e.Node.Tag) Then
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                    'Else
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                    'End If
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            If GLOTRABAJO = "IAPAR" Or GLOTRABAJO = "IAPAR6" Then
                Me.BorIAPAR_SOLTableAdapter.Connection = CON
                Me.BorIAPAR_SOLTableAdapter.Fill(Me.NewSofTvDataSet.borIAPAR_SOL, GloDetClave, gloClv_Orden, 0, 1)
            ElseIf GLOTRABAJO = "CAPAR" Or GLOTRABAJO = "CAPAR6" Then
                Me.BorIAPAR_SOLTableAdapter.Connection = CON
                Me.BorIAPAR_SOLTableAdapter.Fill(Me.NewSofTvDataSet.borIAPAR_SOL, GloDetClave, gloClv_Orden, 0, 3)
            End If
            CON.Close()
            Me.CREAARBOL2()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If GLOTRABAJO = "IAPAR" Or GLOTRABAJO = "IAPAR6" Then
                Me.NUEIAPAR_SOLDIGTableAdapter.Connection = CON
                Me.NUEIAPAR_SOLDIGTableAdapter.Fill(Me.NewSofTvDataSet.NUEIAPAR_SOLDIG, GloDetClave, gloClv_Orden, 0, 1)
            ElseIf GLOTRABAJO = "BAPAR" Or GLOTRABAJO = "BAPAR6" Then
                Me.NUEIAPAR_SOLDIGTableAdapter.Connection = CON
                Me.NUEIAPAR_SOLDIGTableAdapter.Fill(Me.NewSofTvDataSet.NUEIAPAR_SOLDIG, GloDetClave, gloClv_Orden, 0, 3)
            ElseIf GLOTRABAJO = "CAPAR" Or GLOTRABAJO = "CAPAR6" Then
                Me.NUEIAPAR_SOLDIGTableAdapter.Connection = CON
                Me.NUEIAPAR_SOLDIGTableAdapter.Fill(Me.NewSofTvDataSet.NUEIAPAR_SOLDIG, GloDetClave, gloClv_Orden, 0, 5)
            End If
            CON.Close()
            Me.CREAARBOL2()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Try

            Me.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub






End Class