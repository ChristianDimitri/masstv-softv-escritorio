Imports System.Data.SqlClient
Module ModuleArnoldo
    Public GloIp As String = Nothing
    Public LocOpRepcombos As Integer = Nothing
    Public bndcambiocable As Boolean = False
    Public fecha_cartera As String = Nothing
    Public bndcarteraejec As Boolean = False
    Public bndimportemens As Boolean = False
    Public LReportecombo As Boolean = False
    Public LReportecombo1 As Boolean = False
    Public GloContratoord As Long = Nothing
    Public bnddir As Boolean = False
    Public bndCAMDO As Boolean = False
    Public fecha_Hoy As String
    Public LocGloClienteTel As Integer = Nothing
    Public OpcionTel As String = Nothing
    Public optTel As Integer = Nothing
    Public globndTel As Boolean = False
    Public GloBndNum As Boolean = False
    Public GloBndSer As Boolean = False
    Public GloBndAdic As Boolean = False
    Public globndclitel As Boolean = False
    Public globndcotel As Boolean = False
    Public globndpaqtel As Boolean = False
    Public clv_paqAdic As Integer
    Public GloClv_servTel As Integer = Nothing
    Public Clv_NumTelefonico As Integer
    Public Clv_EquipoTel As Integer = 0
    Public Clv_UnicaTel As Integer = 0
    Public contratoNetTel As Integer = 0
    Public bndreporte As Boolean = False
    Public LReporte1 As Boolean = False
    Public LReporte2 As Boolean = False
    Public loccontratoordenes As Long = Nothing
    Public De_Internet As Boolean = False
    Public De_Tele As Boolean = False
    Public sel_inter As Boolean = False
    Public numtel As Boolean = False
    Public NombrePaqueteElimino As String = Nothing
    Public GloOp As Integer = 0
    Public LocGloContratoNet As Long = 0
    Public LocGloClv_unianet As Long = 0
    Public LTXTServ As String
    Public LEdo_Cuenta As Boolean = False
    Public LEdo_Cuenta2 As Boolean = False
    Public Clv_Marcacion As Long = Nothing
    Public Clv_Equipo As Long = Nothing
    Public clv_codigomex As Integer = Nothing
    Public LocContratoLog As Integer = Nothing
    Public LocbndContratacionCombo As Boolean = False
    Public locGloClv_servicioPromocion As Long = Nothing
    Public Gloclv_servicioPromocion As Integer = Nothing
    Public LocGloContratoIni As Long = Nothing
    Public LocGloContratoFin As Long = Nothing
    Public LocbndImpresionEdoCuentaLog As Boolean = False
    Public LocBndAgendaClientes As Boolean = False
    'MDI
    Public maccablemodem As String = Nothing
    Public Tipo_Servicio As Char
    Public Equip_tel As Boolean = False
    Public Equip_Int As Boolean = False
    Public frmctr As New FrmCtrl_ServiciosCli
    Public ctlMDI As MdiClient
    Public frmInternet2 As New FrmInternet
    Public frmTelefonia As New FrmClientesTel
    Public Const SC_CLOSE = &HF060
    Public Const MF_BYCOMMAND = &H0
    Public Declare Function GetSystemMenu Lib "user32" _
    (ByVal hwnd As Long, ByVal bRevert As Long) As Long
    Public Declare Function DeleteMenu Lib "user32" _
    (ByVal hMenu As Long, ByVal nPosition As Long, ByVal wFlags As Long) As Long
    Declare Function DrawMenuBar Lib "user32" (ByVal hwnd As Long) As Long
    Public gloclv_servicioDigital As Integer
    Public LocbndEdoCuentaLog As Integer = Nothing
    Public bnd_Canc_Sin_Mens As Boolean = False
    Public bnd_Canc_Sin_Mens_buena As Boolean = False
    Public bnd_tipopago As Boolean = False

    Public Function Borra_Tablas_Reporte_mensualidades(ByVal clv_Session As Long)
        Dim CON As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Try
            CON.Open()
            cmd = New SqlClient.SqlCommand()
            With cmd
                .CommandText = "Borra_Tablas_Reporte_mensualidades"
                .Connection = CON
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = clv_Session
                .Parameters.Add(prm)

                Dim i As Integer = cmd.ExecuteNonQuery()
            End With
            CON.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Function
    Public Function Dame_Perfil_Usuario(ByVal Clv_Usuario As String, ByVal Op As Integer) As Integer
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand()
        Dim Clv_tipo_Usuario As Integer = Nothing
        Dim bnd As Integer = Nothing
        Try
            CON.Open()
            CMD = New SqlCommand()
            With CMD
                .CommandText = "Dame_Perfil_Usuario"
                .CommandTimeout = 0
                .Connection = CON
                .CommandType = CommandType.StoredProcedure

                '@Clv_Usuario varchar(5),@Op int,@Clv_Tipo_Usuario bigint output
                Dim prm As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar, 5)
                prm.Direction = ParameterDirection.Input
                prm.Value = Clv_Usuario
                .Parameters.Add(prm)

                prm = New SqlParameter("@Op", SqlDbType.Int)
                prm.Direction = ParameterDirection.Input
                prm.Value = 0
                .Parameters.Add(prm)

                prm = New SqlParameter("@Clv_Tipo_Usuario", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Output
                prm.Value = 0
                .Parameters.Add(prm)

                Dim i As Integer = .ExecuteNonQuery()

                Clv_tipo_Usuario = prm.Value
            End With

            If Clv_tipo_Usuario <> 3 And Clv_tipo_Usuario <> 33 Then '-->Por El momento solo Telemarketing y Soporte T�cnico Pasa
                bnd = 1
            Else
                bnd = 0
            End If

            Return bnd

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Function

    Public Function GCheca_si_Aplica_promocion(ByVal contrato As Long, ByVal Muestra As Integer, ByVal opcion_checar As Integer, ByVal clv_servicio As Long) As Integer
        Dim CON As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Dim error1 As Integer = Nothing
        Muestra = 0
        Try
            cmd = New SqlClient.SqlCommand()
            CON.Open()
            With cmd
                .CommandText = "Checa_si_Aplica_promocion"
                .Connection = CON
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                '@contrato bigint,@Muestra int,@error int output,@opcion_checar int,@clv_servicio bigint)
                Dim prm As New SqlParameter("@contrato", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = Contrato
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@Muestra", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = Muestra
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@error", SqlDbType.Int)
                prm2.Direction = ParameterDirection.Output
                prm2.Value = 0
                .Parameters.Add(prm2)

                Dim prm3 As New SqlParameter("@opcion_checar", SqlDbType.Int)
                prm3.Direction = ParameterDirection.Input
                prm3.Value = opcion_checar
                .Parameters.Add(prm3)

                Dim prm4 As New SqlParameter("@clv_servicio", SqlDbType.BigInt)
                prm4.Direction = ParameterDirection.Input
                prm4.Value = clv_servicio
                .Parameters.Add(prm4)

                Dim i As Integer = .ExecuteNonQuery()
                error1 = prm2.Value


            End With
            CON.Close()


            Gloclv_servicioPromocion = opcion_checar
            LocContratoLog = Contrato
            locGloClv_servicioPromocion = clv_servicio



            Return error1
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Function
End Module
