Imports System.Data.SqlClient
Public Class FrmCAMDO

    Private opcionlocal As String

    Private Sub CONCAMDOBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONCAMDOBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.Clv_CalleComboBox.SelectedValue) = False Then
            MsgBox("Seleccione la Calle por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.Clv_ColoniaComboBox.SelectedValue) = False Then
            MsgBox("Seleccione la Colonia por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.Clv_CiudadComboBox.SelectedValue) = False Then
            MsgBox("Seleccione la Ciudad por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Len(Trim(Me.NUMEROTextBox.Text)) = 0 Then
            MsgBox("Capture el Numero por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Len(Trim(Me.ENTRECALLESTextBox.Text)) = 0 Then
            MsgBox("Capture el Numero por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Len(Trim(Me.TELEFONOTextBox.Text)) = 0 Then
            MsgBox("Capture el Numero Telef�nico por favor ", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.Clv_CiudadComboBox.SelectedValue) = False Then
            MsgBox("Capture el Sector por favor ", MsgBoxStyle.Information)
            Exit Sub
        End If
        Me.Validate()
        'Me.CONCAMDOBindingSource.EndEdit()
        'Me.CONCAMDOTableAdapter.Connection = CON
        'Me.CONCAMDOTableAdapter.Update(Me.NewSofTvDataSet.CONCAMDO)
        CAMDO2(GloDetClave, gloClv_Orden, Contrato, CInt(Clv_CalleTextBox.Text), CInt(NUMEROTextBox.Text), ENTRECALLESTextBox.Text, CInt(Clv_ColoniaTextBox.Text), TELEFONOTextBox.Text, LocClv_tecnico, CInt(Clv_CiudadTextBox.Text), CInt(Me.cmbSector.SelectedValue), 2)
        MsgBox(mensaje5)
        bndCAMDO = True
        Me.Close()
        CON.Close()
    End Sub

    Private Sub Busca()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.CONCAMDOTableAdapter.Connection = CON
            Me.CONCAMDOTableAdapter.Fill(Me.NewSofTvDataSet.CONCAMDO, New System.Nullable(Of Long)(CType(GloDetClave, Long)), New System.Nullable(Of Long)(CType(gloClv_Orden, Long)), New System.Nullable(Of Long)(CType(Contrato, Long)))
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub asignacolonia()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.Clv_ColoniaTextBox.Text = Me.Clv_ColoniaComboBox.SelectedValue
            If IsNumeric(Me.Clv_ColoniaComboBox.SelectedValue) = True Then
                'Me.MuestraCVECOLCIUTableAdapter.Connection = CON
                'Me.MuestraCVECOLCIUTableAdapter.Fill(Me.NewSofTvDataSet.MuestraCVECOLCIU, Me.Clv_ColoniaComboBox.SelectedValue)
                MuestraCVECOLCIU(Me.Clv_ColoniaComboBox.SelectedValue)
                If opcionlocal = "N" Then Me.Clv_CiudadComboBox.Text = ""
                Me.Clv_CiudadComboBox.Enabled = True
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub asiganacalle()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.Clv_CalleTextBox.Text = Me.Clv_CalleComboBox.SelectedValue
            If IsNumeric(Me.Clv_CalleComboBox.SelectedValue) = True Then
                'Me.DAMECOLONIA_CALLETableAdapter.Connection = CON
                'Me.DAMECOLONIA_CALLETableAdapter.Fill(Me.NewSofTvDataSet.DAMECOLONIA_CALLE, New System.Nullable(Of Integer)(CType(Me.Clv_CalleComboBox.SelectedValue, Integer)))
                DAMECOLONIA_CALLE(Me.Clv_CalleComboBox.SelectedValue)
                Me.Clv_ColoniaComboBox.Enabled = True
                If opcionlocal = "N" Then Me.Clv_ColoniaComboBox.Text = ""
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub FrmCAMDO_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.BORDetOrdSer_INTELIGENTETableAdapter.Connection = CON
        Me.BORDetOrdSer_INTELIGENTETableAdapter.Fill(Me.NewSofTvDataSet.BORDetOrdSer_INTELIGENTE, New System.Nullable(Of Long)(CType(GloDetClave, Long)))
        GloBndTrabajo = True
        GloBloqueaDetalle = True
        CON.Close()
    End Sub
    Private Sub CAMDO2(ByVal prmclave As Long, ByVal prmclvorden As Long, ByVal prmcontrato As Long, ByVal prmClvcalle As Integer, ByVal prmnumero As Integer, ByVal prmEntreCalles As String, ByVal prmClvColonia As Integer, _
                ByVal prmTelefono As String, ByVal prmclvtecnica As Integer, ByVal prmclvciudad As Integer, ByVal prmClvsector As Integer, ByVal prmop As Integer)
        '@CLAVE BIGINT, @Clv_Orden BIGINT, @CONTRATO BIGINT,@ClvCalle int,@NUMERO int, @ENTRECALLES varchar(500), @Clv_Colonia int, @TELEFONO varchar(12), @ClvTecnica int ,@Clv_Ciudad int ,@Op INT
        Dim DT As New DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLAVE", SqlDbType.BigInt, prmclave)
        BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, prmclvorden)
        BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, prmcontrato)
        BaseII.CreateMyParameter("@ClvCalle", SqlDbType.Int, prmClvcalle)
        BaseII.CreateMyParameter("@NUMERO", SqlDbType.Int, prmnumero)
        BaseII.CreateMyParameter("@ENTRECALLES", SqlDbType.VarChar, prmEntreCalles, 500)
        BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, prmClvColonia)
        BaseII.CreateMyParameter("@TELEFONO", SqlDbType.VarChar, prmTelefono, 50)
        BaseII.CreateMyParameter("@ClvTecnica", SqlDbType.Int, prmclvtecnica)
        BaseII.CreateMyParameter("@Clv_Ciudad", SqlDbType.Int, prmclvciudad)
        BaseII.CreateMyParameter("@CLVSECTOR", SqlDbType.Int, prmClvsector)
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, prmop)

        If prmop = 1 Then
            DT = BaseII.ConsultaDT("CONCAMDO2")
            If DT.Rows.Count > 0 Then
                Me.CLAVETextBox.Text = DT.Rows.Item(0)(0).ToString
                Me.Clv_OrdenTextBox.Text = DT.Rows.Item(0)(1).ToString
                Me.CONTRATOTextBox.Text = DT.Rows.Item(0)(2).ToString
                Me.Clv_CalleTextBox.Text = DT.Rows.Item(0)(3).ToString
                Me.NUMEROTextBox.Text = DT.Rows.Item(0)(4).ToString
                Me.ENTRECALLESTextBox.Text = DT.Rows.Item(0)(5).ToString
                Me.Clv_ColoniaTextBox.Text = DT.Rows.Item(0)(6).ToString
                Me.TELEFONOTextBox.Text = DT.Rows.Item(0)(7).ToString
                LocClv_tecnico = DT.Rows.Item(0)(8).ToString
                Me.Clv_CiudadTextBox.Text = DT.Rows.Item(0)(9).ToString
                Me.Clv_CalleComboBox.SelectedValue = CInt(DT.Rows.Item(0)(3).ToString)
                Me.cmbSector.SelectedValue = DT.Rows.Item(0)(10).ToString
                'Me.Clv_ColoniaComboBox.SelectedValue = CInt(DT.Rows.Item(0)(6).ToString)
                'Me.Clv_CiudadComboBox.SelectedValue = CInt(DT.Rows.Item(0)(9).ToString)
            Else
                Me.CLAVETextBox.Text = ""
                Me.Clv_OrdenTextBox.Text = ""
                Me.CONTRATOTextBox.Text = ""
                Me.Clv_CalleTextBox.Text = ""
                Me.NUMEROTextBox.Text = ""
                Me.ENTRECALLESTextBox.Text = ""
                Me.Clv_ColoniaTextBox.Text = ""
                Me.TELEFONOTextBox.Text = ""
                LocClv_tecnico = 0
                Me.Clv_CiudadTextBox.Text = ""
                Me.Clv_CalleComboBox.SelectedValue = 0
                Me.Clv_ColoniaComboBox.SelectedValue = 0
                Me.Clv_CiudadComboBox.SelectedValue = 0
                Me.cmbSector.SelectedValue = 0
            End If
        ElseIf prmop = 2 Or prmop = 3 Then
            BaseII.Inserta("CONCAMDO2")
        End If

    End Sub
    Private Sub DAMECOLONIA_CALLE(ByVal prmClvCalle As Integer)
        Dim DT As New DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_CALLE", SqlDbType.Int, prmClvCalle)
        DT = BaseII.ConsultaDT("DAMECOLONIA_CALLE")
        Me.Clv_ColoniaComboBox.DataSource = DT
    End Sub
    Private Sub MuestraCVECOLCIU(ByVal prmClvCol As Integer)
        Dim DT As New DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, prmClvCol)
        DT = BaseII.ConsultaDT("MuestraCVECOLCIU")
        Me.Clv_CiudadComboBox.DataSource = DT
    End Sub

    Private Sub FrmCAMDO_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MUESTRACALLES' Puede moverla o quitarla seg�n sea necesario.
        Me.MUESTRACALLESTableAdapter.Connection = CON
        Me.MUESTRACALLESTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACALLES)
        Me.Clv_ColoniaComboBox.Enabled = False
        Me.Clv_CiudadComboBox.Enabled = False
        UspMuestraSectores()

        'Busca()
        CAMDO2(GloDetClave, gloClv_Orden, Contrato, 0, 0, String.Empty, 0, String.Empty, 0, 0, Me.cmbSector.SelectedValue, 1)
        If IsNumeric(Me.Clv_OrdenTextBox.Text) = False Then
            opcionlocal = "N"
            'Me.CONCAMDOBindingSource.AddNew()
            Me.CONTRATOTextBox.Text = Contrato
            Me.Clv_OrdenTextBox.Text = gloClv_Orden
            Me.CLAVETextBox.Text = GloDetClave
            Me.CONCAMDOBindingNavigator.Enabled = True
            Me.Panel1.Enabled = True

        Else
            opcionlocal = "M"
            'Me.DAMECOLONIA_CALLETableAdapter.Connection = CON
            'Me.DAMECOLONIA_CALLETableAdapter.Fill(Me.NewSofTvDataSet.DAMECOLONIA_CALLE, Me.Clv_CalleTextBox.Text)
            DAMECOLONIA_CALLE(Me.Clv_CalleTextBox.Text)
            MuestraCVECOLCIU(Me.Clv_ColoniaTextBox.Text)
            'Me.MuestraCVECOLCIUTableAdapter.Connection = CON
            'Me.MuestraCVECOLCIUTableAdapter.Fill(Me.NewSofTvDataSet.MuestraCVECOLCIU, Me.Clv_ColoniaTextBox.Text)
            'Busca()
            'CAMDO2(GloDetClave, gloClv_Orden, Contrato, 0, 0, String.Empty, 0, String.Empty, 0, 0, 1)
        End If
        If Bloquea = True Or opcion = "M" Then
            Me.CONCAMDOBindingNavigator.Enabled = False
            Me.Panel1.Enabled = False
        Else
            Me.BindingNavigatorDeleteItem.Enabled = True
            Me.Clv_CiudadComboBox.Enabled = True
        End If
        'Busca()
        CON.Close()
    End Sub

    Private Sub UspMuestraSectores()
        Try
            BaseII.limpiaParametros()
            Me.cmbSector.DataSource = BaseII.ConsultaDT("UspMuestraSectores")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub Clv_CalleComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_CalleComboBox.SelectedIndexChanged
        asiganacalle()
    End Sub

    Private Sub Clv_ColoniaComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_ColoniaComboBox.SelectedIndexChanged
        Me.asignacolonia()
    End Sub



    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.CLAVETextBox.Text) = False Then
            MsgBox("No se encontr� datos para eliminar ", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.Clv_OrdenTextBox.Text) = False Then
            MsgBox("No se encontr� datos para eliminar ", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.CONTRATOTextBox.Text) = False Then
            MsgBox("No se encontr� datos para eliminar ", MsgBoxStyle.Information)
            Exit Sub
        End If
        'Me.CONCAMDOTableAdapter.Connection = CON
        'Me.CONCAMDOTableAdapter.Delete(Me.CLAVETextBox.Text, Me.Clv_OrdenTextBox.Text, Me.CONTRATOTextBox.Text)
        CAMDO2(GloDetClave, gloClv_Orden, Contrato, 0, 0, String.Empty, 0, String.Empty, 0, 0, 3, Me.cmbSector.SelectedValue)
        CON.Close()
        Me.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        bndCAMDO = True
        Me.Close()
    End Sub

    Private Sub Clv_CiudadComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_CiudadComboBox.SelectedIndexChanged
        Me.Clv_CiudadTextBox.Text = Me.Clv_CiudadComboBox.SelectedValue
        If IsNumeric(Me.Clv_CiudadComboBox.SelectedValue) = True Then
            Me.Clv_CiudadComboBox.Enabled = True
        End If
    End Sub

    Private Sub Clv_CiudadTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_CiudadTextBox.TextChanged

    End Sub

    Private Sub Clv_CiudadLabel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub


End Class