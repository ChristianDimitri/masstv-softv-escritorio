﻿Public Class FrmHistorialTarjetas

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
    End Sub

    Private Sub FrmHistorialTarjetas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.Panel1.BackColor = Color.LightGray
        Me.lbContrato.BackColor = Color.LightGray
        Me.lbColonia.BackColor = Color.LightGray
        Me.lbDireccion.BackColor = Color.LightGray
        Me.lbNombre.BackColor = Color.LightGray
        Me.Label1.BackColor = Color.LightGray
        Me.Label2.BackColor = Color.LightGray
        Me.Label3.BackColor = Color.LightGray
        Me.Label4.BackColor = Color.LightGray

        Dim DT As New DataTable
        DT = UspTarjetasOXXO(GloContrato, 2)
        Me.dgvHistorialTarjetas.DataSource = DT

        If DT.Rows.Count > 0 Then
            Me.lbContrato.Text = DT.Rows(0)(2).ToString
            Me.lbNombre.Text = DT.Rows(0)(7).ToString
            Me.lbDireccion.Text = DT.Rows(0)(8).ToString
            Me.lbColonia.Text = DT.Rows(0)(9).ToString
        End If
     



    End Sub
    Private Function UspTarjetasOXXO(ByVal prmcontrato As Long, ByVal PRMOP As Integer) As DataTable

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@ClvRECIBO", SqlDbType.BigInt, 0)
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, prmcontrato)
        BaseII.CreateMyParameter("@FechaImpresion", SqlDbType.Date, "1900-01-01")
        BaseII.CreateMyParameter("@importe", SqlDbType.Decimal, 0)
        BaseII.CreateMyParameter("@cancelada", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@TXT", SqlDbType.VarChar, String.Empty, 150)
        BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, String.Empty, 250)
        BaseII.CreateMyParameter("@Direccion", SqlDbType.VarChar, String.Empty, 250)
        BaseII.CreateMyParameter("@Colonia", SqlDbType.VarChar, String.Empty, 250)
        BaseII.CreateMyParameter("@Municipio", SqlDbType.VarChar, String.Empty, 250)
        BaseII.CreateMyParameter("@Estado", SqlDbType.VarChar, String.Empty, 250)
        BaseII.CreateMyParameter("@CP", SqlDbType.VarChar, String.Empty, 250)
        BaseII.CreateMyParameter("@EntreCalle", SqlDbType.VarChar, String.Empty, 250)
        BaseII.CreateMyParameter("@Telefono", SqlDbType.VarChar, String.Empty, 150)
        BaseII.CreateMyParameter("@CantidadLetra", SqlDbType.VarChar, String.Empty, 250)
        BaseII.CreateMyParameter("@Tipo", SqlDbType.VarChar, String.Empty, 1)
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, PRMOP)
        UspTarjetasOXXO = BaseII.ConsultaDT("UspTarjetasOXXO")
    End Function
End Class