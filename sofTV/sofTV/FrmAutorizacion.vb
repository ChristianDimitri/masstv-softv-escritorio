﻿Imports System.Text
Imports System.Data.SqlClient

Public Class FrmAutorizacion
    Public Shared OpcionValidar As String = ""
    Public Shared IFEAutorizado As Boolean = False
    Public Shared NombreAutorizado As Boolean = False
    Public Shared DireccionAutorizado As Boolean = False


    Private Sub ValidaTipoUsuario(ByVal Op As Integer, ByVal Clv_Usuario As String, ByVal Pasaporte As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaTipoUsuario", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Op", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = Op
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar, 5)
        par2.Direction = ParameterDirection.Input
        par2.Value = Clv_Usuario
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Pasaporte", SqlDbType.VarChar, 20)
        par3.Direction = ParameterDirection.Input
        par3.Value = Pasaporte
        comando.Parameters.Add(par3)

        Dim par5 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        par5.Direction = ParameterDirection.Output
        comando.Parameters.Add(par5)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eMsj = ""
            eMsj = par5.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub bnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnAceptar.Click
        If tbUsuario.Text.Length = 0 Then
            MsgBox("Captura usuario.", MsgBoxStyle.Information)
            Exit Sub
        End If

        If tbPasaporte.Text.Length = 0 Then
            MsgBox("Captura pasaporte.", MsgBoxStyle.Information)
            Exit Sub
        End If

        ValidaTipoUsuario(1, tbUsuario.Text, tbPasaporte.Text)

        If (OpcionValidar = "IFE") Then
            If eMsj.Length = 0 Then
                eBndAutorizacionIFERepetido = True
                Locbndguardar = True
                eBndEntraNombre = True
                eBndEntraDire = True
                FrmAutorizacion.IFEAutorizado = True
                Me.Close()
            Else
                eBndAutorizacionIFERepetido = False
                Locbndguardar = False
                eBndEntraNombre = False
                eBndEntraDire = False
                MsgBox(eMsj, MsgBoxStyle.Exclamation)
            End If
        End If

        If (OpcionValidar = "NOMBRE") Then
            If eMsj.Length = 0 Then
                eBndAutorizacionNombreRepetido = True
                Locbndguardar = True
                eBndEntraDire = True
                FrmAutorizacion.NombreAutorizado = True
                Me.Close()
            Else
                eBndAutorizacionNombreRepetido = False
                Locbndguardar = False
                eBndEntraDire = False
                MsgBox(eMsj, MsgBoxStyle.Exclamation)
            End If
        End If

        If (OpcionValidar = "DIRECCION") Then
            If eMsj.Length = 0 Then
                eBndAutorizacionDireccionRepetido = True
                Locbndguardar = True
                Locbnd2clientes = True
                FrmAutorizacion.DireccionAutorizado = True
                Me.Close()
            Else
                eBndAutorizacionDireccionRepetido = False
                Locbndguardar = False
                Locbnd2clientes = False
                MsgBox(eMsj, MsgBoxStyle.Exclamation)
            End If
        End If


    End Sub

    Private Sub FrmAutorizacion_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        eBndAutorizacionIFERepetido = False
        eBndAutorizacionNombreRepetido = False
        eBndAutorizacionIFERepetido = False
    End Sub

    Private Sub bnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnCancelar.Click
        eBndAutorizacionIFERepetido = False
        Locbndguardar = False
        eBndEntraNombre = False
        eBndEntraDire = False
        Me.Close()
    End Sub
End Class