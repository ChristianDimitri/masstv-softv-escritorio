Imports System.Data.SqlClient
Public Class BrwMotAtenTel

    Private Sub BrwMotAtenTel_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConMotAtenTelTableAdapter.Connection = CON
        Me.ConMotAtenTelTableAdapter.Fill(Me.DataSetEric.ConMotAtenTel, 0, "", 0)
        CON.Close()
    End Sub


    Private Sub BrwMotAtenTel_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Me.Label1.ForeColor = Color.Black
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConMotAtenTelTableAdapter.Connection = CON
        Me.ConMotAtenTelTableAdapter.Fill(Me.DataSetEric.ConMotAtenTel, 0, "", 0)
        CON.Close()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim CON As New SqlConnection(MiConexion)
        If Me.TextBox1.Text.Length > 0 Then
            CON.Open()
            Me.ConMotAtenTelTableAdapter.Connection = CON
            Me.ConMotAtenTelTableAdapter.Fill(Me.DataSetEric.ConMotAtenTel, 0, Me.TextBox1.Text, 1)
            CON.Close()
            Me.TextBox1.Clear()
        End If
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        If Asc(e.KeyChar) = 13 And Me.TextBox1.Text.Length > 0 Then
            CON.Open()
            Me.ConMotAtenTelTableAdapter.Connection = CON
            Me.ConMotAtenTelTableAdapter.Fill(Me.DataSetEric.ConMotAtenTel, 0, Me.TextBox1.Text, 1)
            CON.Close()
            Me.TextBox1.Clear()
        End If
    End Sub

    
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eOpcion = "N"
        FrmMotAtenTel.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Me.ConMotAtenTelDataGridView.RowCount > 0 Then
            eOpcion = "M"
            eClv_MotAtenTel = Me.ConMotAtenTelDataGridView.SelectedCells.Item(0).Value
            FrmMotAtenTel.Show()
        Else
            MsgBox("Selecciona un Motivo para Modificar", , "Atenci�n")
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub
End Class