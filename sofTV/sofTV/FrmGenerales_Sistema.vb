Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic
Imports System.Data
Imports sofTV.BAL.RelArticuloAccesorio
Public Class FrmGenerales_Sistema
    Dim eRes As Integer = 0
    Dim eMsg As String = String.Empty
    '---Variables Panel Basico ----
    '****Parte 1 panel basico****
    Dim Importeextras As String = Nothing
    Dim cargatrabajo As String = Nothing
    Dim cargatrabajoins As String = Nothing
    Dim impresoraord As String = Nothing
    '*******2da parte*******
    Dim anio11 As String = Nothing
    Dim anio12 As String = Nothing
    Dim puntos1 As String = Nothing
    Dim anio21 As String = Nothing
    Dim anio22 As String = Nothing
    Dim puntos2 As String = Nothing
    Dim anio31 As String = Nothing
    Dim anio32 As String = Nothing
    Dim puntos3 As String = Nothing
    Dim anio41 As String = Nothing
    Dim anio42 As String = Nothing
    Dim puntos4 As String = Nothing
    '*******3era parte basico***
    Dim retirofiltro As String = Nothing
    Dim instfiltro As String = Nothing
    '******4ta parte******
    Dim numext As String = Nothing
    Dim costoext As String = Nothing

    '---------------Variable Panel Datos-----------
    Dim clv_plaza As String = Nothing
    Dim empresa As String = Nothing
    Dim direccion As String = Nothing
    Dim colonia As String = Nothing
    Dim ciudad As String = Nothing
    Dim RFC As String = Nothing
    Dim Tel As String = Nothing
    Dim CP As String = Nothing
    Private eMensaje As String = Nothing
    Dim Tipo As String
    '----------Variable Panel2(Impresion)------------
    Dim ImpresoraFiscal As String = Nothing
    Dim ImpresoraTarjetas As String = Nothing
    Dim ImpresoraTickets As String = Nothing
    Dim ImpresoraContratos As String = Nothing
    '-----Variables Panel1(Periodos)-----------
    Dim Periodo As String = Nothing
    Dim Deshabilita As String = Nothing
    Dim DesdeDia As String = Nothing
    Dim Hastadia As String = Nothing
    Dim ProcDesconexion As String = Nothing
    Dim Pprontopago As String = Nothing
    Dim Pporantiguedad As String = Nothing
    '---Variables Panel4(correo)------
    Dim cuentacorreo As String = Nothing
    Dim passcorreo As String = Nothing
    Dim hostcorreo As String = Nothing
    Dim Portcorreo As String = Nothing
    '-----Variables Panel6(FacturasGlobales)-----
    Dim Genfactglobal As String = Nothing
    Dim diaria As String = Nothing
    Dim Mensual As String = Nothing
    '--------Variables Panel7(MensajeTickets)------
    Dim MensajeTickets As String = Nothing
    Dim ActivoTickets As String = Nothing
    '--------Variables Panel3(CobroDeAdeudo)---------
    Dim Tipseradeudo As String = Nothing
    Dim MesesAdeudo As String = Nothing
    '---------Variables para Panel 8 (Alertas)
    Dim newdirec As String
    Dim Direcciones As String
    Dim Mensaje As String
    Dim Monto As String
    Dim Bnd As Integer = 0

    Dim Clv_Familia As Long = 0
    Dim clv_Tecnico As Long = 0
    Dim Clv_PuestoOrd As Long = 0
    Dim Clv_TecnicoOrd As Long = 0

    Dim aiva As Double = 0
    Dim aieps As Double = 0

    Public Sub BorRel_Tecnicos_Quejas(ByVal Clave_Familia As Long, ByVal Clave_Tecnico As Long)
        Dim CON01 As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Try
            CON01.Open()
            cmd = New SqlCommand()
            With cmd
                'BorRel_Tecnicos_Quejas] (@Clave_Familia bigint, @Clave_Tecnico bigint)
                .CommandText = "BorRel_Tecnicos_Quejas"
                .Connection = CON01
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure


                Dim prm As New SqlParameter("@Clave_Familia", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = Clave_Familia
                .Parameters.Add(prm)

                prm = New SqlParameter("@Clave_Tecnico", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = Clave_Tecnico
                .Parameters.Add(prm)

                Dim i As Integer = .ExecuteNonQuery()
            End With
            CON01.Close()
            Me.Llena_arbolTec_quejas()
        Catch ex As Exception
            If CON01.State = ConnectionState.Open Then
                CON01.Close()
            End If
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub



    Public Sub Llena_arbolTec_quejas()


        Dim nod0 As Integer = 0
        Dim nod1 As Integer = 0
        Dim CON As New SqlConnection(MiConexion)


        Me.TreViewTecnicos.Nodes.Clear()


        Dim FilaRow As DataRow
        Dim filaTec As DataRow
        'padre = ConsultaRel_Quejas_Tec_Familia()
        CON.Open()
        Me.ConsultaRel_Quejas_Tec_FamiliaTableAdapter.Connection = CON
        Me.ConsultaRel_Quejas_Tec_FamiliaTableAdapter.Fill(Me.Procedimientosarnoldo4.ConsultaRel_Quejas_Tec_Familia, 0)
        CON.Close()
        For Each FilaRow In Me.Procedimientosarnoldo4.ConsultaRel_Quejas_Tec_Familia.Rows
            nod1 = 0
            Me.TreViewTecnicos.Nodes.Add(FilaRow("DEPARTAMENTO").ToString()).ForeColor = Color.Black
            Me.TreViewTecnicos.Nodes(nod0).Tag = FilaRow("CONSECUTIVO_FAMILIA")
            'Me.TreViewTecnicos.Nodes(nod0).Name = FilaRow("Departamento").ToString()
            'hijo = ConsultaRel_Tecnicos_Quejas(FilaRow("Consecutivo_familia"))
            CON.Open()
            Me.ConsultaRel_Tecnicos_QuejasTableAdapter.Connection = CON
            Me.ConsultaRel_Tecnicos_QuejasTableAdapter.Fill(Me.Procedimientosarnoldo4.ConsultaRel_Tecnicos_Quejas, 0, FilaRow("Consecutivo_Familia"))
            CON.Close()
            For Each filaTec In Me.Procedimientosarnoldo4.ConsultaRel_Tecnicos_Quejas.Rows
                Me.TreViewTecnicos.Nodes(nod0).Nodes.Add(filaTec("TECNICO").ToString()).ForeColor = Color.Black
                Me.TreViewTecnicos.Nodes(nod0).Nodes(nod1).Tag = filaTec("CONSECUTIVO")
                nod1 += 1
            Next
            nod0 += 1
        Next

        Me.TreViewTecnicos.ExpandAll()

    End Sub




    Private Sub NueRel_Tecnicos_Quejas(ByVal familia As Long, ByVal clv_tecnico As Long)
        Dim CON01 As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Try
            CON01.Open()
            cmd = New SqlCommand()
            With cmd
                .CommandText = "NueRel_Tecnicos_Quejas"
                .Connection = CON01
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                '@familia bigint,@clv_tecnico bigint

                Dim prm As New SqlParameter("@familia", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = familia
                .Parameters.Add(prm)

                prm = New SqlParameter("@clv_tecnico", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = clv_tecnico
                .Parameters.Add(prm)

                Dim ia As Integer = .ExecuteNonQuery()
            End With
            CON01.Close()
            'Aqui refresco el tree view
            ' ConsultaRel_Tecnicos_Quejas()
            Llena_arbolTec_quejas()

        Catch ex As Exception
            If CON01.State = ConnectionState.Open Then
                CON01.Close()
            End If
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub LlenaCombosQuejas(ByVal op As Integer, ByVal clv_depto As Long)

        Dim CON As New SqlClient.SqlConnection(MiConexion)
        Dim consulta As String = Nothing


        Try


            If op = 0 Then '-->Departamentos
                'If Me.CmBDepto.Items.Count > 0 Then
                '    Me.CmBDepto.Items.Clear()
                '    Me.CmBDepto.Text = ""
                'End If
                CON.Open()
                'Me.ConsultaRel_Quejas_Tec_FamiliaTableAdapter.Connection = CON
                Me.Muestra_tecnicosDepartamentos_AlmacenTableAdapter.Connection = CON
                Me.Muestra_tecnicosDepartamentos_AlmacenTableAdapter.Fill(Me.Procedimientosarnoldo4.Muestra_tecnicosDepartamentos_Almacen, 0)
                'Me.ConsultaRel_Quejas_Tec_FamiliaTableAdapter.Fill(Me.Procedimientosarnoldo4.ConsultaRel_Quejas_Tec_Familia, 0)
                CON.Close()
                If CmBDepto.Items.Count() > 0 Then
                    Me.CmBDepto.SelectedIndex = 0
                End If

            ElseIf op = 1 Then
                'If Me.CmbTecnicos.Items.Count > 0 Then
                '    Me.CmbTecnicos.Items.Clear()
                '    Me.CmbTecnicos.Text = ""
                'End If

                CON.Open()
                'Me.ConsultaRel_Tecnicos_QuejasTableAdapter.Connection = CON
                Me.Muestra_TecnicosByFamiliTableAdapter.Connection = CON
                Me.Muestra_TecnicosByFamiliTableAdapter.Fill(Me.Procedimientosarnoldo4.Muestra_TecnicosByFamili, 1, clv_depto)
                'Me.ConsultaRel_Tecnicos_QuejasTableAdapter.Fill(Me.Procedimientosarnoldo4.ConsultaRel_Tecnicos_Quejas, 1, clv_depto)
                CON.Close()


                If CmbTecnicos.Items.Count() > 0 Then
                    CmbTecnicos.SelectedIndex = 0
                End If

            End If
            'End With


        Catch ex As Exception
            If CON.State = ConnectionState.Open Then
                CON.Close()
            End If
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Guarda_Clv_Adic()
        Dim Conarnold As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Dim Estado As New StringBuilder
        Dim Ciudad As New StringBuilder


        If Me.TxtEdoClvAdic.Text.Length > 0 Then
            If Me.TxtEdoClvAdic.Text.Length < 2 Then
                Estado.Insert(1, "0", 2 - Me.TxtEdoClvAdic.Text.Length)
            ElseIf Me.TxtEdoClvAdic.Text.Length = 2 Then
                Estado.Append(Me.TxtEdoClvAdic.Text)
            End If
        Else
            Estado.Append("")
        End If

        If Me.TxtCdClvAdic.Text.Length > 0 Then
            If Me.TxtCdClvAdic.Text.Length < 2 Then
                Ciudad.Insert(1, "0", 2 - Me.TxtEdoClvAdic.Text.Length)
            ElseIf Me.TxtCdClvAdic.Text.Length = 2 Then
                Ciudad.Append(Me.TxtCdClvAdic.Text)
            End If
        Else
            Ciudad.Append("")
        End If

        Try
            cmd = New SqlCommand()
            Conarnold.Open()
            With cmd
                ' New_Tbl_Clv_Ref_Logi (@Estado varchar(3),@Ciudad varchar(3))
                .CommandText = "New_Tbl_Clv_Ref_Logi"
                .Connection = Conarnold
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@Estado", SqlDbType.VarChar, 3)
                prm.Direction = ParameterDirection.Input
                prm.Value = Estado.ToString()
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@Ciudad", SqlDbType.VarChar, 3)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = Ciudad.ToString()
                .Parameters.Add(prm1)

                Dim i As Integer = .ExecuteNonQuery()
            End With
            Conarnold.Close()
        Catch ex As Exception
            If Conarnold.State = ConnectionState.Open Then
                Conarnold.Close()
            End If
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Consulta_Clv_Adic()
        Dim CON As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Try
            cmd = New SqlCommand()
            CON.Open()
            With cmd
                'Consulta_Tbl_Clv_Ref_Logi (@op int)	
                .CommandText = "Consulta_Tbl_Clv_Ref_Logi"
                .Connection = CON
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@op", SqlDbType.Int)
                prm.Direction = ParameterDirection.Input
                prm.Value = 0
                .Parameters.Add(prm)

                Dim reader As SqlDataReader = .ExecuteReader()
                While (reader.Read)
                    Me.TxtEdoClvAdic.Text = reader.GetValue(0).ToString
                    Me.TxtCdClvAdic.Text = reader.GetValue(1).ToString
                End While
            End With
            CON.Close()
        Catch ex As Exception
            If CON.State = ConnectionState.Open Then
                CON.Close()
            End If
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub datosbitpanel3()
        Try
            Tipseradeudo = CStr(Me.ConceptoComboBox.SelectedValue)
            MesesAdeudo = Me.MesesTextBox.Text
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub guardabitpanel3()
        Try
            'MesesAdeudo = Me.MesesTextBox.Text
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.MesesTextBox.Name + ", Servicio:" + Tipseradeudo, MesesAdeudo, Me.MesesTextBox.Text, LocClv_Ciudad)
            datosbitpanel3()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub datosbitpanel7()
        Try
            MensajeTickets = Me.MensajeTextBox.Text
            If Me.ActivoCheckBox.CheckState = CheckState.Checked Then
                ActivoTickets = "True"
            Else
                ActivoTickets = "False"
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub guardabitpanel7()
        Try
            Dim validacion1 As String = Nothing
            'MensajeTickets = Me.MensajeTextBox.Text
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.MensajeTextBox.Name + ", Panel7", MensajeTickets, Me.MensajeTextBox.Text, LocClv_Ciudad)
            'Activo
            If Me.ActivoCheckBox.CheckState = CheckState.Checked Then
                'ActivoTickets = "True"
                validacion1 = "True"
            Else
                'ActivoTickets = "False"
                validacion1 = "False"
            End If
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.ActivoCheckBox.Name + ", Panel7", ActivoTickets, validacion1, LocClv_Ciudad)
            datosbitpanel7()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub datosbitpanel6()
        Try
            If Me.Si_se_generaCheckBox.CheckState = CheckState.Checked Then
                Genfactglobal = "True"
            Else
                Genfactglobal = "False"
            End If
            If Me.RadioButton1.Checked = True Then
                diaria = "True"
            Else
                diaria = "False"
            End If
            If Me.RadioButton2.Checked = True Then
                Mensual = "True"
            Else
                Mensual = "False"
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub guardabitpanel6()
        Try
            Dim validacion1 As String = Nothing
            Dim validacion2 As String = Nothing
            Dim validacion3 As String = Nothing

            'Se genera factura global auto
            If Me.Si_se_generaCheckBox.CheckState = CheckState.Checked Then
                'Genfactglobal = "True"
                validacion1 = "True"
            Else
                'Genfactglobal = "False"
                validacion1 = "False"
            End If
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.Si_se_generaCheckBox.Name + ", Panel6", Genfactglobal, validacion1, LocClv_Ciudad)
            'Diaria
            If Me.RadioButton1.Checked = True Then
                'diaria = "True"
                validacion2 = "True"
            Else
                'diaria = "False"
                validacion2 = "False"
            End If
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Diaria, Panel6", diaria, validacion2, LocClv_Ciudad)
            'Mensual
            If Me.RadioButton2.Checked = True Then
                'Mensual = "True"
                validacion3 = "True"
            Else
                'Mensual = "False"
                validacion3 = "False"
            End If
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Mensual, Panel6", Mensual, validacion3, LocClv_Ciudad)

            datosbitpanel6()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub datosbitpanel4()
        Try
            cuentacorreo = Me.CuentaTextBox.Text
            passcorreo = Me.PasswordTextBox.Text
            hostcorreo = Me.HostTextBox.Text
            Portcorreo = Me.PortTextBox.Text
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub guardabitpanel4()
        Try
            'cuentacorreo = Me.CuentaTextBox.Text
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.CuentaTextBox.Name + ", Panel4", cuentacorreo, Me.CuentaTextBox.Text, LocClv_Ciudad)
            'passcorreo = Me.PasswordTextBox.Text
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.PasswordTextBox.Name + ", Panel4", passcorreo, Me.PasswordTextBox.Text, LocClv_Ciudad)
            'hostcorreo = Me.HostTextBox.Text
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.HostTextBox.Name + ", Panel4", hostcorreo, Me.HostTextBox.Text, LocClv_Ciudad)
            'Portcorreo = Me.PortTextBox.Text
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.PortTextBox.Name + ", Panel4", Portcorreo, Me.PortTextBox.Text, LocClv_Ciudad)
            datosbitpanel4()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub datosbitpanel1()
        Try
            Periodo = CStr(Me.ComboBox1.SelectedValue)
            If Me.CheckBox1.CheckState = CheckState.Checked Then
                Deshabilita = "True"
            Else
                Deshabilita = "False"
            End If
            DesdeDia = Me.TextBox14.Text
            Hastadia = Me.TextBox13.Text
            ProcDesconexion = Me.TextBox7.Text
            Pprontopago = Me.TextBox11.Text
            Pporantiguedad = Me.TextBox10.Text
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub guardabitpanel1()
        Try
            Dim validacion1 As String = Nothing
            Select Case Periodo
                Case "1"
                    'DesdeDia = Me.TextBox14.Text
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "DesdeDia, Periodo 1", DesdeDia, Me.TextBox14.Text, LocClv_Ciudad)
                    'Hastadia = Me.TextBox13.Text
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "HastaDia, Periodo 1", Hastadia, Me.TextBox13.Text, LocClv_Ciudad)
                    'ProcDesconexion = Me.TextBox7.Text
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "ProcDesconexion, Periodo 1", ProcDesconexion, Me.TextBox7.Text, LocClv_Ciudad)
                    'Pprontopago = Me.TextBox11.Text
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Pprontopago, Periodo 1", Pprontopago, Me.TextBox11.Text, LocClv_Ciudad)
                    'Pporantiguedad = Me.TextBox10.Text
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Pporantiguedad, Periodo 1", Pporantiguedad, Me.TextBox10.Text, LocClv_Ciudad)
                Case "2"
                    'Habilita Periodo
                    If Me.CheckBox1.CheckState = CheckState.Checked Then
                        'Deshabilita = "True"
                        validacion1 = "True"
                    Else
                        'Deshabilita = "False"
                        validacion1 = "False"
                    End If
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Habilito el Periodo2", Deshabilita, validacion1, LocClv_Ciudad)
                    'DesdeDia = Me.TextBox14.Text
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "DesdeDia, Periodo 2", DesdeDia, Me.TextBox14.Text, LocClv_Ciudad)
                    'Hastadia = Me.TextBox13.Text
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "HastaDia, Periodo 2", Hastadia, Me.TextBox13.Text, LocClv_Ciudad)
                    'ProcDesconexion = Me.TextBox7.Text
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "ProcDesconexion, Periodo 2", ProcDesconexion, Me.TextBox7.Text, LocClv_Ciudad)
                    'Pprontopago = Me.TextBox11.Text
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Pprontopago, Periodo 2", Pprontopago, Me.TextBox11.Text, LocClv_Ciudad)
                    'Pporantiguedad = Me.TextBox10.Text
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Pporantiguedad, Periodo 2", Pporantiguedad, Me.TextBox10.Text, LocClv_Ciudad)
            End Select
            datosbitpanel1()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)

        End Try
    End Sub

    Private Sub datosbitpanel2()
        Try
            ImpresoraFiscal = Me.ImpresoraFiscalTextBox.Text
            ImpresoraTarjetas = Me.ImpresoraTarjetasTextBox.Text
            ImpresoraTickets = Me.ImpresoraTicketsTextBox.Text
            ImpresoraContratos = Me.ImpresoraContratosTextBox.Text
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub guardabitpanel2()
        Try
            'ImpresoraFiscal = Me.ImpresoraFiscalTextBox.Text
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.ImpresoraFiscalTextBox.Name + ",  Panel2", ImpresoraFiscal, Me.ImpresoraFiscalTextBox.Text, LocClv_Ciudad)
            'ImpresoraTarjetas = Me.ImpresoraTarjetasTextBox.Text
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.ImpresoraTarjetasTextBox.Name + ", Panel2", ImpresoraTarjetas, Me.ImpresoraTarjetasTextBox.Text, LocClv_Ciudad)
            'ImpresoraTickets = Me.ImpresoraTicketsTextBox.Text
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.ImpresoraTicketsTextBox.Name + ", Panel2", ImpresoraTickets, Me.ImpresoraTicketsTextBox.Text, LocClv_Ciudad)
            'ImpresoraContratos = Me.ImpresoraContratosTextBox.Text
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.ImpresoraContratosTextBox.Name + ", Panel2", ImpresoraContratos, Me.ImpresoraContratosTextBox.Text, LocClv_Ciudad)
            datosbitpanel2()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub datosbitpaneldatos()
        Try
            clv_plaza = Me.Id_sucursalTextBox.Text
            empresa = Me.NombreTextBox.Text
            direccion = Me.DireccionTextBox.Text
            colonia = Me.ColoniaTextBox.Text
            ciudad = Me.CiudadTextBox.Text
            RFC = Me.RfcTextBox.Text
            Tel = Me.TELefonosTextBox.Text
            CP = Me.TextBox1.Text
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub guardabitpaneldata()
        Try
            'clv_plaza = Me.Id_sucursalTextBox.Text
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.Id_sucursalTextBox.Name + ", Panel Datos", clv_plaza, Me.Id_sucursalTextBox.Text, LocClv_Ciudad)
            'empresa = Me.NombreTextBox.Text
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.NombreTextBox.Name + ", Panel Datos", empresa, Me.NombreTextBox.Text, LocClv_Ciudad)
            'direccion = Me.DireccionTextBox.Text
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.DireccionTextBox.Name + ", Panel Datos", direccion, Me.DireccionTextBox.Text, LocClv_Ciudad)
            'colonia = Me.ColoniaTextBox.Text
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.ColoniaTextBox.Name + ", Panel Datos", colonia, Me.ColoniaTextBox.Text, LocClv_Ciudad)
            'ciudad = Me.CiudadTextBox.Text
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.CiudadTextBox.Name + ", Panel Datos", ciudad, Me.CiudadTextBox.Text, LocClv_Ciudad)
            'RFC = Me.RfcTextBox.Text
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.RfcTextBox.Name + ", Panel Datos", RFC, Me.RfcTextBox.Text, LocClv_Ciudad)
            'Tel = Me.TELefonosTextBox.Text
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.TELefonosTextBox.Name + ", Panel Datos", Tel, Me.TELefonosTextBox.Text, LocClv_Ciudad)
            'CP = Me.TextBox1.Text
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "CodigoPostal, Panel Datos", CP, Me.TextBox1.Text, LocClv_Ciudad)
            datosbitpaneldatos()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub



    Private Sub Datosbitpanelbasic()
        Try
            '------------------------Primera parte----------------------------
            Importeextras = Me.ImportePorExtrasTextBox.Text
            cargatrabajo = Me.CargaDeTrabajoTextBox.Text
            cargatrabajoins = Me.Carga_trab_insTextBox.Text
            impresoraord = Me.ImpresoraOrdenesTextBox.Text
            '------------------------Segunda parte-----------------------------
            anio11 = Me.NumericUpDown1.Value
            anio12 = Me.NumericUpDown2.Value
            puntos1 = Me.Puntos1TextBox.Text
            anio21 = Me.NumericUpDown3.Value
            anio22 = Me.NumericUpDown4.Value
            puntos2 = Me.Puntos2TextBox.Text
            anio31 = Me.NumericUpDown5.Value
            anio32 = Me.NumericUpDown6.Value
            puntos3 = Me.Puntos5TextBox.Text
            anio41 = Me.NumericUpDown7.Value
            anio42 = Me.NumericUpDown8.Value
            puntos4 = Me.Puntos10TextBox.Text
            '------------------------Tercera Parte------------------------------
            If Me.CheckBox2.CheckState = CheckState.Checked Then
                retirofiltro = "True"
            Else
                retirofiltro = "False"
            End If
            If Me.CheckBox3.CheckState = CheckState.Checked Then
                instfiltro = "True"
            Else
                instfiltro = "False"
            End If
            '------------------------Cuarta Parte-------------------------------
            numext = Me.NumeroExtTextBox.Text
            costoext = Me.CostoExtTextBox.Text

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub guardapanelbasicobit()
        Try
            Dim valida1 As String = Nothing
            Dim valida2 As String = Nothing
            ''------------------------Primera parte----------------------------
            'Importeextras = Me.ImportePorExtrasTextBox.Text
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.ImportePorExtrasTextBox.Name + ", Servicios de Television", Importeextras, Me.ImportePorExtrasTextBox.Text, LocClv_Ciudad)
            'cargatrabajo = Me.CargaDeTrabajoTextBox.Text
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.CargaDeTrabajoTextBox.Name + ", Servicios de Television", cargatrabajo, Me.CargaDeTrabajoTextBox.Text, LocClv_Ciudad)
            'cargatrabajoins = Me.Carga_trab_insTextBox.Text
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.Carga_trab_insTextBox.Name + ", Servicios de Television", cargatrabajoins, Me.Carga_trab_insTextBox.Text, LocClv_Ciudad)
            'impresoraord = Me.ImpresoraOrdenesTextBox.Text
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.ImpresoraOrdenesTextBox.Name + ", Servicios de Television", impresoraord, Me.ImpresoraOrdenesTextBox.Text, LocClv_Ciudad)
            ''------------------------Segunda parte-----------------------------
            'anio11 = Me.NumericUpDown1.Value
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Anio11, Servicio de Television", anio11, CStr(Me.NumericUpDown1.Value), LocClv_Ciudad)
            'anio12 = Me.NumericUpDown2.Value
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Anio12, Servicio de Television", anio12, CStr(Me.NumericUpDown2.Value), LocClv_Ciudad)

            'puntos1 = Me.Puntos1TextBox.Text
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.Puntos1TextBox.Name + ", Servicios de Television", puntos1, Me.Puntos1TextBox.Text, LocClv_Ciudad)
            'anio21 = Me.NumericUpDown3.Value
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Anio21, Servicio de Television", anio21, CStr(Me.NumericUpDown3.Value), LocClv_Ciudad)
            'anio22 = Me.NumericUpDown4.Value
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Anio22, Servicio de Television", anio22, CStr(Me.NumericUpDown4.Value), LocClv_Ciudad)
            'puntos2 = Me.Puntos2TextBox.Text
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.Puntos2TextBox.Name + ", Servicios de Television", puntos2, Me.Puntos2TextBox.Text, LocClv_Ciudad)

            'anio31 = Me.NumericUpDown5.Value
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Anio31, Servicio de Television", anio31, CStr(Me.NumericUpDown5.Value), LocClv_Ciudad)

            'anio32 = Me.NumericUpDown6.Value
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Anio32, Servicio de Television", anio32, CStr(Me.NumericUpDown6.Value), LocClv_Ciudad)

            'puntos3 = Me.Puntos5TextBox.Text
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.Puntos5TextBox.Name + ", Servicios de Television", puntos3, Me.Puntos5TextBox.Text, LocClv_Ciudad)

            'anio41 = Me.NumericUpDown7.Value
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Anio41, Servicio de Television", anio41, CStr(Me.NumericUpDown7.Value), LocClv_Ciudad)

            'anio42 = Me.NumericUpDown8.Value
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Anio42, Servicio de Television", anio42, CStr(Me.NumericUpDown8.Value), LocClv_Ciudad)

            'puntos4 = Me.Puntos10TextBox.Text
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.Puntos10TextBox.Name + ", Servicios de Television", puntos4, Me.Puntos10TextBox.Text, LocClv_Ciudad)

            ''------------------------Tercera Parte------------------------------
            If Me.CheckBox2.CheckState = CheckState.Checked Then
                ' retirofiltro = "True"
                valida1 = "True"
            Else
                'retirofiltro = "False"
                valida1 = "False"
            End If
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "RetiroDeFiltroSoloInt, Servicios de Television", retirofiltro, valida1, LocClv_Ciudad)

            If Me.CheckBox3.CheckState = CheckState.Checked Then
                ' instfiltro = "True"
                valida2 = "True"
            Else
                'instfiltro = "False"
                valida2 = "False"
            End If
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "InstDeFiltroSoloInt, Servicios de Television", instfiltro, valida2, LocClv_Ciudad)
            ''------------------------Cuarta Parte-------------------------------
            'numext = Me.NumeroExtTextBox.Text
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.NumeroExtTextBox.Name + ", Servicio de Television", numext, Me.NumeroExtTextBox.Text, LocClv_Ciudad)
            'costoext = Me.CostoExtTextBox.Text
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.CostoExtTextBox.Name + ", Servicio de Television", costoext, Me.CostoExtTextBox.Text, LocClv_Ciudad)

            Datosbitpanelbasic()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FrmGenerales_Sistema_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        
    End Sub


    Private Sub FrmGenerales_Sistema_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta l�nea de c�digo carga datos en la tabla 'Procedimientosarnoldo4.ConGeneralAntiguedad' Puede moverla o quitarla seg�n sea necesario.
        'Me.ConGeneralAntiguedadTableAdapter1.Fill(Me.Procedimientosarnoldo4.ConGeneralAntiguedad)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'Procedimientosarnoldo4.ConGeneral' Puede moverla o quitarla seg�n sea necesario.
        'Me.ConGeneralTableAdapter1.Fill(Me.Procedimientosarnoldo4.ConGeneral)


        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetarnoldo.Consulta_Cp_Gene' Puede moverla o quitarla seg�n sea necesario.
        Me.Consulta_Cp_GeneTableAdapter.Connection = CON
        Me.Consulta_Cp_GeneTableAdapter.Fill(Me.DataSetarnoldo.Consulta_Cp_Gene)

        Me.ConGeneralAntiguedadTableAdapter1.Connection = CON
        Me.ConGeneralAntiguedadTableAdapter1.Fill(Me.Procedimientosarnoldo4.ConGeneralAntiguedad)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.ConGeneral' Puede moverla o quitarla seg�n sea necesario.
        Me.ConGeneralTableAdapter1.Connection = CON
        Me.ConGeneralTableAdapter1.Fill(Me.Procedimientosarnoldo4.ConGeneral)
        Me.COnsulta_Gral_factura_globalTableAdapter.Connection = CON
        Me.COnsulta_Gral_factura_globalTableAdapter.Fill(Me.ProcedimientosArnoldo2.COnsulta_Gral_factura_global, 0)
        CON.Close()
        'colorea(Me, Me.Name)
        'Me.noseve()
       
        'Me.PanelDatos.Visible = True
        datosbitpaneldatos()
        Me.ComboBox1.Text = ""
        Me.CheckBox1.Visible = False

        'Licencia, acceso solo a Siste
        If GloUsuario <> "SISTE" Then
            Me.tbcAccesoriosInstalacion.TabPages.Remove(Me.TabPageCorteSistema)
        End If

        'ESTOS BOTONES ESTABAN OCULTOS POR DEFAULT
        Me.tbcAccesoriosInstalacion.TabPages.Remove(Me.TabPage5)
        Me.tbcAccesoriosInstalacion.TabPages.Remove(Me.TabPage8)
        Me.tbcAccesoriosInstalacion.TabPages.Remove(Me.TabPage9)
        Me.tbcAccesoriosInstalacion.TabPages.Remove(Me.TabPage10)
        Me.tbcAccesoriosInstalacion.TabPages.Remove(Me.TabPage11)
        Me.tbcAccesoriosInstalacion.TabPages.Remove(Me.TabPage12)
        Me.tbcAccesoriosInstalacion.TabPages.Remove(Me.TabPage13)
        Me.tbcAccesoriosInstalacion.TabPages.Remove(Me.TabPage14)
        Me.tbcAccesoriosInstalacion.TabPages.Remove(Me.TabPage16)
        Me.tbcAccesoriosInstalacion.TabPages.Remove(tbEstadoCuenta)

        If IdSistema <> "VA" And IdSistema <> "AG" Then
            'Me.Button22.Visible = False
            Me.tbcAccesoriosInstalacion.TabPages.Remove(Me.TabPage10)
        End If
        If IdSistema = "LO" Or IdSistema = "YU" Then
            'Me.Button27.Visible = True
            'Me.Button1.Text = "Varios"
            'Me.Button29.Visible = True
            'Me.Button32.Visible = True
            Me.TabPage2.Text = "Varios"
            Me.tbcAccesoriosInstalacion.TabPages.Add(Me.TabPage9)
            Me.tbcAccesoriosInstalacion.TabPages.Add(Me.TabPage13)
            Me.tbcAccesoriosInstalacion.TabPages.Add(Me.TabPage14)
        End If
        ConRelGeneralNumero()
        Muestra_Generales_antiguiedad()
    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim cone2 As New SqlClient.SqlConnection(MiConexion)
        If Bnd = 1 And IsNumeric(Monto) = True Then
            cone2.Open()
            Me.NueGeneralAlertaTableAdapter.Connection = cone2
            Me.NueGeneralAlertaTableAdapter.Fill(Me.DataSetLidia2.NueGeneralAlerta, CDec(Monto), Mensaje, Direcciones)
            cone2.Close()
        End If
        Me.Close()
    End Sub
    Private Sub DireccionTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.KeyChar = Chr((ValidaKey(Me.DireccionTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub
    Private Sub ColoniaTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.KeyChar = Chr((ValidaKey(Me.ColoniaTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub
    Private Sub CiudadTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.KeyChar = Chr((ValidaKey(Me.CiudadTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub NombreTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.KeyChar = Chr((ValidaKey(Me.NombreTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    'Private Sub noseve()
    '    Me.PanelBasico.Visible = False
    '    Me.PanelDatos.Visible = False
    '    Me.Panel1.Visible = False
    '    Me.Panel3.Visible = False
    '    Me.Panel4.Visible = False
    '    Me.Panel6.Visible = False
    '    Me.Panel7.Visible = False
    '    Me.Panel8.Visible = False
    '    Me.Panel9.Visible = False
    '    Me.Panel10.Visible = False
    '    Me.Panel11.Visible = False
    '    Me.PanelDatosXml.Visible = False
    '    Me.Panel12.Visible = False
    'End Sub

    Private Sub Muestra_Generales_antiguiedad()
        Dim CONE As New SqlConnection(MiConexion)
        Try
            CONE.Open()
            Dim comando As SqlClient.SqlCommand
            Dim reader As SqlDataReader
            comando = New SqlClient.SqlCommand
            With comando
                .Connection = CONE
                .CommandText = "EXEC MUESTRA_GENERALANTIGUEDAD_2 "
                .CommandType = CommandType.Text
                .CommandTimeout = 0
                reader = comando.ExecuteReader()
                Using reader
                    While reader.Read
                        Me.NumericUpDown1.Value = reader.GetValue(0)
                        Me.NumericUpDown2.Value = reader.GetValue(1)
                        '
                        Me.NumericUpDown3.Value = reader.GetValue(2)
                        Me.NumericUpDown4.Value = reader.GetValue(3)
                        '
                        Me.NumericUpDown5.Value = reader.GetValue(4)
                        Me.NumericUpDown6.Value = reader.GetValue(5)
                        '
                        Me.NumericUpDown7.Value = reader.GetValue(6)
                        Me.NumericUpDown8.Value = reader.GetValue(7)
                    End While
                End Using
            End With
            CONE.Close()
        Catch ex As Exception
            If CONE.State = ConnectionState.Open Then
                CONE.Close()
            End If
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Guarda_Generales_antiguiedad()
        Dim CON As New SqlConnection(MiConexion)
        Try
            Dim P1Anio1 As Integer = 0
            Dim P1Anio2 As Integer = 0
            Dim P2Anio1 As Integer = 0
            Dim P2Anio2 As Integer = 0
            Dim P5Anio1 As Integer = 0
            Dim P5Anio2 As Integer = 0
            Dim P10Anio1 As Integer = 0
            Dim P10Anio2 As Integer = 0
            '
            If IsNumeric(Me.NumericUpDown1.Value) = False Then P1Anio1 = 0 Else P1Anio1 = Me.NumericUpDown1.Value
            If IsNumeric(Me.NumericUpDown2.Value) = False Then P1Anio2 = 0 Else P1Anio2 = Me.NumericUpDown2.Value
            '
            If IsNumeric(Me.NumericUpDown3.Value) = False Then P2Anio1 = 0 Else P2Anio1 = Me.NumericUpDown3.Value
            If IsNumeric(Me.NumericUpDown4.Value) = False Then P2Anio2 = 0 Else P2Anio2 = Me.NumericUpDown4.Value
            '
            If IsNumeric(Me.NumericUpDown5.Value) = False Then P5Anio1 = 0 Else P5Anio1 = Me.NumericUpDown5.Value
            If IsNumeric(Me.NumericUpDown6.Value) = False Then P5Anio2 = 0 Else P5Anio2 = Me.NumericUpDown6.Value
            '
            If IsNumeric(Me.NumericUpDown7.Value) = False Then P10Anio1 = 0 Else P10Anio1 = Me.NumericUpDown7.Value
            If IsNumeric(Me.NumericUpDown8.Value) = False Then P10Anio2 = 0 Else P10Anio2 = Me.NumericUpDown8.Value

            CON.Open()
            Dim comando As SqlClient.SqlCommand
            comando = New SqlClient.SqlCommand
            With comando
                .Connection = CON
                .CommandText = "EXEC GUARDAR_GENERALANTIGUEDAD_2 " & P1Anio1 & "," & P1Anio2 & "," & P2Anio1 & "," & P2Anio2 & "," & P5Anio1 & "," & P5Anio2 & "," & P10Anio1 & "," & P10Anio2
                .CommandType = CommandType.Text
                .CommandTimeout = 0
                .ExecuteReader()
            End With
            CON.Close()

        Catch ex As Exception
            If CON.State = ConnectionState.Open Then
                CON.Close()
            End If
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button3_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim CON As New SqlConnection(MiConexion)
        Try
            Dim filtro1 As Integer = 0
            Dim filtro2 As Integer = 0

            CON.Open()
            If IsNumeric(Me.CargaDeTrabajoTextBox.Text) = False Then Me.CargaDeTrabajoTextBox.Text = 0
            If IsNumeric(Me.Carga_trab_insTextBox.Text) = False Then Me.Carga_trab_insTextBox.Text = 0
            If IsNumeric(Me.ImportePorExtrasTextBox.Text) = False Then Me.ImportePorExtrasTextBox.Text = 0
            If IsNumeric(Me.NumeroExtTextBox.Text) = False Then Me.NumeroExtTextBox.Text = 0
            If IsNumeric(Me.CostoExtTextBox.Text) = False Then Me.CostoExtTextBox.Text = 0
            If IsNumeric(Me.TextBox14.Text) = False Then Me.TextBox14.Text = 0
            If IsNumeric(Me.TextBox13.Text) = False Then Me.TextBox13.Text = 0
            If IsNumeric(Me.ImportePorExtrasTextBox.Text) = False Then Me.ImportePorExtrasTextBox.Text = 0
            If IsNumeric(Me.NumeroExtTextBox.Text) = False Then Me.NumeroExtTextBox.Text = 0
            If IsNumeric(Me.CostoExtTextBox.Text) = False Then Me.CostoExtTextBox.Text = 0
            If IsNumeric(Me.Puntos1TextBox.Text) = False Then Me.Puntos1TextBox.Text = 0
            If IsNumeric(Me.Puntos2TextBox.Text) = False Then Me.Puntos2TextBox.Text = 0
            If IsNumeric(Me.Puntos5TextBox.Text) = False Then Me.Puntos5TextBox.Text = 0
            If IsNumeric(Me.Puntos10TextBox.Text) = False Then Me.Puntos10TextBox.Text = 0
            If Me.CheckBox2.CheckState = CheckState.Checked Then
                filtro1 = 1
            ElseIf Me.CheckBox2.CheckState = CheckState.Unchecked Then
                filtro1 = 0
            End If

            If Me.CheckBox3.CheckState = CheckState.Checked Then
                filtro2 = 1
            ElseIf Me.CheckBox3.CheckState = CheckState.Unchecked Then
                filtro2 = 0
            End If

            'Me.NueGeneralTableAdapter.Connection = CON
            'Me.NueGeneralTableAdapter.Fill(Me.NewSofTvDataSet.NueGeneral, Me.Id_sucursalTextBox.Text, Me.NombreTextBox.Text, Me.DireccionTextBox.Text, Me.ColoniaTextBox.Text, Me.CiudadTextBox.Text, "", Me.RfcTextBox.Text, Me.TELefonosTextBox.Text, False, True, Me.CargaDeTrabajoTextBox.Text, Me.Carga_trab_insTextBox.Text, Me.TextBox14.Text, Me.TextBox13.Text, Me.ImportePorExtrasTextBox.Text, 0, 0, 0, 0, 0, 0, 0, "", Me.ImpresoraOrdenesTextBox.Text, "", 0, 0, Me.NumeroExtTextBox.Text, Me.CostoExtTextBox.Text, 0, 0, 0, 0, "", "")
            Me.NueGeneralTableAdapter1.Connection = CON
            Me.NueGeneralTableAdapter1.Fill(Me.DataSetEric2.NueGeneral, Me.Id_sucursalTextBox.Text, Me.NombreTextBox.Text, Me.DireccionTextBox.Text, Me.ColoniaTextBox.Text, Me.CiudadTextBox.Text, "", Me.RfcTextBox.Text, Me.TELefonosTextBox.Text, False, True, Me.CargaDeTrabajoTextBox.Text, Me.Carga_trab_insTextBox.Text, Me.TextBox14.Text, Me.TextBox13.Text, Me.ImportePorExtrasTextBox.Text, 0, 0, 0, 0, 0, 0, 0, "", Me.ImpresoraOrdenesTextBox.Text, "", 0, 0, Me.NumeroExtTextBox.Text, Me.CostoExtTextBox.Text, 0, 0, 0, 0, "", "")
            'Me.NUEGeneralAntiguedadTableAdapter.Connection = CON
            'Me.NUEGeneralAntiguedadTableAdapter.Fill(Me.NewSofTvDataSet.NUEGeneralAntiguedad, New System.Nullable(Of Integer)(CType(Me.Puntos1TextBox.Text, Integer)), New System.Nullable(Of Integer)(CType(Me.Puntos2TextBox.Text, Integer)), New System.Nullable(Of Integer)(CType(Me.Puntos5TextBox.Text, Integer)), New System.Nullable(Of Integer)(CType(Me.Puntos10TextBox.Text, Integer)))
            Me.NUEGeneralAntiguedadTableAdapter1.Connection = CON
            Me.NUEGeneralAntiguedadTableAdapter1.Fill(Me.DataSetEric2.NUEGeneralAntiguedad, New System.Nullable(Of Integer)(CType(Me.Puntos1TextBox.Text, Integer)), New System.Nullable(Of Integer)(CType(Me.Puntos2TextBox.Text, Integer)), New System.Nullable(Of Integer)(CType(Me.Puntos5TextBox.Text, Integer)), New System.Nullable(Of Integer)(CType(Me.Puntos10TextBox.Text, Integer)))
            Me.Inserta_Generales_Filtros_OrdenesTableAdapter.Connection = CON
            Me.Inserta_Generales_Filtros_OrdenesTableAdapter.Fill(Me.ProcedimientosArnoldo2.Inserta_Generales_Filtros_Ordenes, filtro1, filtro2)
            CON.Close()
            Guarda_Generales_antiguiedad()
            uspInsertaTblImpresionOrdenQueja(Me.cbxImprimeOrdenesSoftv.Checked, Me.cbxImprimeOrdenesFac.Checked, Me.cbxImprimeQuejasSoftv.Checked) 'GUARDA RELACION IMPRESION AUTOM�TICA ORDENES Y QUEJAS
            MsgBox(mensaje5)
            guardapanelbasicobit()

            'Me.Close()
        Catch ex As System.Exception
            If CON.State = ConnectionState.Open Then
                CON.Close()
            End If
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Dim CON As New SqlConnection(MiConexion)
        Try

            CON.Open()
            If IsNumeric(Me.CargaDeTrabajoTextBox.Text) = False Then Me.CargaDeTrabajoTextBox.Text = 0
            If IsNumeric(Me.Carga_trab_insTextBox.Text) = False Then Me.Carga_trab_insTextBox.Text = 0
            If IsNumeric(Me.ImportePorExtrasTextBox.Text) = False Then Me.ImportePorExtrasTextBox.Text = 0
            If IsNumeric(Me.NumeroExtTextBox.Text) = False Then Me.NumeroExtTextBox.Text = 0
            If IsNumeric(Me.CostoExtTextBox.Text) = False Then Me.CostoExtTextBox.Text = 0
            If IsNumeric(Me.TextBox1.Text) = False Then Me.TextBox1.Text = 0
            'Me.NueGeneralTableAdapter.Connection = CON
            'Me.NueGeneralTableAdapter.Fill(Me.NewSofTvDataSet.NueGeneral, Me.Id_sucursalTextBox.Text, Me.NombreTextBox.Text, Me.DireccionTextBox.Text, Me.ColoniaTextBox.Text, Me.CiudadTextBox.Text, "", Me.RfcTextBox.Text, Me.TELefonosTextBox.Text, False, True, Me.CargaDeTrabajoTextBox.Text, Me.Carga_trab_insTextBox.Text, 0, 0, Me.ImportePorExtrasTextBox.Text, 0, 0, 0, 0, 0, 0, 0, "", Me.ImpresoraOrdenesTextBox.Text, "", 0, 0, Me.NumeroExtTextBox.Text, Me.CostoExtTextBox.Text, 0, 0, 0, 0, "", "")
            Me.NueGeneralTableAdapter1.Connection = CON
            Me.NueGeneralTableAdapter1.Fill(Me.DataSetEric2.NueGeneral, Me.Id_sucursalTextBox.Text, Me.NombreTextBox.Text, Me.DireccionTextBox.Text, Me.ColoniaTextBox.Text, Me.CiudadTextBox.Text, "", Me.RfcTextBox.Text, Me.TELefonosTextBox.Text, False, True, Me.CargaDeTrabajoTextBox.Text, Me.Carga_trab_insTextBox.Text, 0, 0, Me.ImportePorExtrasTextBox.Text, 0, 0, 0, 0, 0, 0, 0, "", Me.ImpresoraOrdenesTextBox.Text, "", 0, 0, Me.NumeroExtTextBox.Text, Me.CostoExtTextBox.Text, 0, 0, 0, 0, "", "")
            Me.Inserta_Mod_Cp_GenTableAdapter.Connection = CON
            Me.Inserta_Mod_Cp_GenTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Mod_Cp_Gen, Me.Id_sucursalTextBox.Text, CInt(Me.TextBox1.Text))
            CON.Close()
            NueRelGeneralNumero(Me.TextBoxNumeroInt.Text, Me.TextBoxNumeroExt.Text)
            guardabitpaneldata()
            MsgBox(mensaje5)

            'Me.Close()
        Catch ex As System.Exception
            If CON.State = ConnectionState.Open Then
                CON.Close()
            End If
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub FillToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click

        Try
            If Me.ComboBox1.Text = "" Then
                MsgBox("Seleccione un Periodo Valido")
            Else
                Dim CON2 As New SqlConnection(MiConexion)
                CON2.Open()
                If IsNumeric(Me.ComboBox1.SelectedValue) = False Then Me.ComboBox1.SelectedValue = 0
                If IsNumeric(Me.TextBox14.Text) = False Then Me.TextBox14.Text = 0
                If IsNumeric(Me.TextBox13.Text) = False Then Me.TextBox13.Text = 0
                If IsNumeric(Me.TextBox16.Text) = False Then Me.TextBox16.Text = 0
                If IsNumeric(Me.TextBox15.Text) = False Then Me.TextBox15.Text = 0
                If IsNumeric(Me.TextBox7.Text) = False Then Me.TextBox7.Text = 0
                If IsNumeric(Me.TextBox10.Text) = False Then Me.TextBox10.Text = 0
                If IsNumeric(Me.TextBox11.Text) = False Then Me.TextBox11.Text = 0

                'Me.MODIFCAGENERALESDESCTableAdapter.Connection = CON
                'Me.MODIFCAGENERALESDESCTableAdapter.Fill(Me.NewSofTvDataSet.MODIFCAGENERALESDESC, Me.ComboBox1.SelectedValue, Me.TextBox14.Text, Me.TextBox13.Text, Me.TextBox11.Text, Me.TextBox10.Text, Me.TextBox7.Text)
                Me.MODIFCAGENERALESDESCTableAdapter1.Connection = CON2
                Me.MODIFCAGENERALESDESCTableAdapter1.Fill(Me.DataSetEric2.MODIFCAGENERALESDESC, Me.ComboBox1.SelectedValue, CInt(Me.TextBox14.Text), CInt(Me.TextBox13.Text), CInt(Me.TextBox11.Text), CInt(Me.TextBox10.Text), CInt(Me.TextBox7.Text))
                CON2.Close()
                'Dim conexion200 As New SqlConnection(MiConexion)
                MODIFCAPeriodos()
                'conexion200.Open()
                ''Me.MODIFCAPeriodosTableAdapter.Connection = conexion200
                ''Me.MODIFCAPeriodosTableAdapter.Fill(Me.DataSetLidia.MODIFCAPeriodos, Me.ComboBox1.SelectedValue, Me.CheckBox1.Checked)
                'conexion200.Close()
                guardabitpanel1()
                MsgBox("Se Guardo Con Exito", MsgBoxStyle.Information)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub MODIFCAPeriodos()
        Dim cmd15 As New SqlClient.SqlCommand
        Dim cone15 As New SqlClient.SqlConnection(MiConexion)

        cone15.Open()
        With cmd15
            .CommandText = "MODIFCAPeriodos"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = cone15
            Dim prm As New SqlParameter("@Clv_Periodo", SqlDbType.Int)
            Dim prm2 As New SqlParameter("@Habilitar", SqlDbType.Int)

            prm.Direction = ParameterDirection.Input
            prm2.Direction = ParameterDirection.Input

            prm.Value = Me.ComboBox1.SelectedValue
            prm2.Value = Me.CheckBox1.Checked

            .Parameters.Add(prm)
            .Parameters.Add(prm2)

            Dim i As Integer = cmd15.ExecuteNonQuery
        End With
        cone15.Close()

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Dim CON100 As New SqlConnection(MiConexion)
        CON100.Open()
        If Me.ComboBox1.SelectedValue = 2 Then
            Me.CheckBox1.Visible = True
        End If

        If Me.ComboBox1.SelectedValue = 1 Then
            Me.CheckBox1.Visible = False
        End If
        Me.CONSULTAGENERALESDESCTableAdapter1.Connection = CON100
        Me.CONSULTAGENERALESDESCTableAdapter1.Fill(Me.Procedimientosarnoldo4.CONSULTAGENERALESDESC, Me.ComboBox1.SelectedValue)
        CON100.Close()
        datosbitpanel1()

    End Sub



    Private Sub Label12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Consulta_ImpresorasTableAdapter.Connection = CON
        Me.Consulta_ImpresorasTableAdapter.Update(Me.ImpresoraTicketsTextBox.Text, Me.ImpresoraFiscalTextBox.Text, Me.ImpresoraTarjetasTextBox.Text, Me.ImpresoraContratosTextBox.Text)
        MsgBox(mensaje5, MsgBoxStyle.Information)
        guardabitpanel2()
        CON.Close()
    End Sub



    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        GuardaAdeudo()
    End Sub

    Private Sub GuardaAdeudo()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.MesesTextBox.Text) = False Then
            Me.MesesTextBox.Text = 0
        Else
            Me.ConMesesCobroAdeudoTableAdapter.Connection = CON
            Me.ConMesesCobroAdeudoTableAdapter.Insert(Me.Clv_TipSerTextBox.Text, Me.MesesTextBox.Text)
            MsgBox(mensaje5)
            guardabitpanel3()
        End If
        CON.Close()
    End Sub

    Private Sub MesesTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MesesTextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            GuardaAdeudo()
        End If
    End Sub




    Private Sub ConceptoComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConceptoComboBox.SelectedIndexChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConMesesCobroAdeudoTableAdapter.Connection = CON
        Me.ConMesesCobroAdeudoTableAdapter.Fill(Me.DataSetEric.ConMesesCobroAdeudo, Me.ConceptoComboBox.SelectedValue, 1)
        datosbitpanel3()
        CON.Close()
    End Sub

    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click
        Dim CON As New SqlConnection(MiConexion)

        If Me.CuentaTextBox.Text.Length = 0 Then
            MsgBox("Captura la Cuenta de Correo.")
            Exit Sub
        End If
        If Me.PasswordTextBox.Text.Length = 0 Then
            MsgBox("Captura el Password de la Cuenta de Correo.")
            Exit Sub
        End If
        If Me.HostTextBox.Text.Length = 0 Then
            MsgBox("Captura el Nombre del Host.")
            Exit Sub
        End If
        If IsNumeric(Me.PortTextBox.Text) = False Then
            MsgBox("El Valor del Puerto debe de ser Num�rico.")
            Exit Sub
        End If
        Try
            CON.Open()
            Me.ConGeneralCorreoTableAdapter.Connection = CON
            Me.ConGeneralCorreoTableAdapter.Insert(Me.CuentaTextBox.Text, Me.PasswordTextBox.Text, Me.HostTextBox.Text, CType(Me.PortTextBox.Text, Integer))
            CON.Close()
            MsgBox(mensaje5)
            guardabitpanel4()
        Catch
            If CON.State = ConnectionState.Open Then
                CON.Close()
            End If
            MsgBox("Se ha Producido un Error.", , "Atenc��n")
        End Try
    End Sub





    Private Sub Label35_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label35.Click

    End Sub


    Private Sub Button15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button15.Click
        'If Tipo = "Diario" Then
        '    Me.RadioButton1.Checked = True
        '    Me.RadioButton2.Checked = False
        'Else
        '    Me.RadioButton1.Checked = False
        '    Me.RadioButton2.Checked = True

        'End If
        Dim con As New SqlConnection(MiConexion)
        Dim ComandoLidia As New SqlClient.SqlCommand

        Dim bit As Integer
        Dim si_se_checa As Boolean = False
        If Me.Si_se_generaCheckBox.CheckState = CheckState.Checked Then
            si_se_checa = True
        Else
            si_se_checa = False
        End If
        If si_se_checa = False Then
            bit = 0
        ElseIf si_se_checa = True Then
            bit = 1
        End If
        If Me.RadioButton1.Checked = True And Me.RadioButton2.Checked = False Then
            Tipo = "Diario"
        ElseIf Me.RadioButton1.Checked = False And Me.RadioButton2.Checked = True Then
            Tipo = "Mensual"
        End If
        If Me.RadioButton1.Checked = False And Me.RadioButton2.Checked = False Then
            Tipo = ""
        End If

        con.Open()
        Me.Inserta_Gral_factura_globalTableAdapter.Connection = con
        Me.Inserta_Gral_factura_globalTableAdapter.Fill(Me.ProcedimientosArnoldo2.Inserta_Gral_factura_global, si_se_checa)
        With ComandoLidia
            .CommandText = "FacturasAuto"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = con
            Dim prm As New SqlParameter("@bit", SqlDbType.Bit)
            Dim prm2 As New SqlParameter("@tipo", SqlDbType.VarChar)
            prm.Direction = ParameterDirection.Input
            prm2.Direction = ParameterDirection.Input
            prm.Value = bit
            prm2.Value = Tipo
            .Parameters.Add(prm)
            .Parameters.Add(prm2)
            Dim i As Integer = ComandoLidia.ExecuteNonQuery
        End With
        con.Close()
        guardabitpanel6()
        MsgBox("Se guardo con Exito", MsgBoxStyle.Information)


    End Sub

    Private Sub Button16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button16.Click
        Dim CON As New SqlConnection(MiConexion)
        Try
            CON.Open()
            Me.NueGeneralMsjTicketsTableAdapter.Connection = CON
            Me.NueGeneralMsjTicketsTableAdapter.Fill(Me.DataSetEric2.NueGeneralMsjTickets, Me.MensajeTextBox.Text, Me.ActivoCheckBox.Checked)
            CON.Close()
            'bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Mensaje en Tickets", eMensaje, Me.MensajeTextBox.Text, LocClv_Ciudad)
            guardabitpanel7()
            MsgBox(mensaje5)
        Catch ex As Exception
            If CON.State = ConnectionState.Open Then
                CON.Close()
            End If
            MsgBox(ex.Message)
        End Try
    End Sub



    Private Sub Si_se_generaCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Si_se_generaCheckBox.CheckedChanged
        If Me.Si_se_generaCheckBox.Checked = False Then
            Me.RadioButton1.Enabled = False
            Me.RadioButton2.Enabled = False
        ElseIf Me.Si_se_generaCheckBox.Checked = True Then
            Me.RadioButton1.Enabled = True
            Me.RadioButton2.Enabled = True
        End If
    End Sub



    Private Sub GuardaAlertas()
        Dim ConLidia2 As New SqlClient.SqlConnection(MiConexion)
        Try
            ConLidia2.Open()
            Me.NueGeneralAlertaTableAdapter.Connection = ConLidia2
            Me.NueGeneralAlertaTableAdapter.Fill(Me.DataSetLidia2.NueGeneralAlerta, Me.TextBox4.Text, Me.TextBox3.Text, newdirec)
            ConLidia2.Close()
        Catch ex As System.Exception
            If ConLidia2.State = ConnectionState.Open Then
                ConLidia2.Close()
            End If
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Dame_Ips()
        Dim CON As New SqlConnection(MiConexion)
        Try

            Dim I As Integer = 0
            Dim X As Integer = 0

            If Len(Direcciones) > 0 Then
                CON.Open()
                Me.Dame_Direcciones_IpTableAdapter.Connection = CON
                Me.Dame_Direcciones_IpTableAdapter.Fill(Me.DataSetLidia2.Dame_Direcciones_Ip)
                CON.Close()
                Dim FilaRow As DataRow
                Me.TreeView1.Nodes.Clear()
                For Each FilaRow In Me.DataSetLidia2.Dame_Direcciones_Ip.Rows
                    Me.TreeView1.Nodes.Add(Trim(FilaRow("Ip").ToString()))
                    I += 1
                Next
                Me.TreeView1.ExpandAll()
            End If
            Me.TextBox5.Text = ""
        Catch ex As System.Exception
            If CON.State = ConnectionState.Open Then
                CON.Close()
            End If
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Button20_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button20.Click
        Dim conlidia3 As New SqlClient.SqlConnection(MiConexion)
        Try


            Dim comando As New SqlClient.SqlCommand
            Dim cont As Integer

            conlidia3.Open()
            With comando
                .CommandText = "agrega_Direcciones_Ip"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = conlidia3
                Dim Prm As New SqlParameter("@Ip", SqlDbType.VarChar, 30)
                Dim Prm2 As New SqlParameter("@Cont", SqlDbType.Int)
                Prm.Direction = ParameterDirection.Input
                Prm2.Direction = ParameterDirection.Output
                Prm.Value = Me.TextBox5.Text
                Prm2.Value = 0
                .Parameters.Add(Prm)
                .Parameters.Add(Prm2)
                Dim i As Integer = comando.ExecuteNonQuery
                cont = Prm2.Value
            End With
            conlidia3.Close()
            If cont > 0 Then
                MsgBox("La Direccion Ip ya se Encuentra en la Lista", MsgBoxStyle.Information)
            Else
                newdirec = newdirec + "," + Me.TextBox5.Text
            End If
        Catch ex As Exception
            conlidia3.Close()
            MsgBox("La Direccion Ip ya se Encuentra en la Lista", MsgBoxStyle.Information)
        End Try

        Dame_Ips()
    End Sub



    Private Sub Button21_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button21.Click
        Dim ConLidia5 As New SqlClient.SqlConnection(MiConexion)
        Dim Comando As New SqlClient.SqlCommand
        Dim Cont As Integer = 0
        If Len(Me.TextBox5.Text) > 0 Then
            ConLidia5.Open()
            With Comando
                .CommandText = "Quita_Direcciones_Ip"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = ConLidia5
                Dim Prm As New SqlParameter("@Ip", SqlDbType.VarChar, 30)
                Dim Prm2 As New SqlParameter("@Cont", SqlDbType.Int)
                Prm.Direction = ParameterDirection.Input
                Prm2.Direction = ParameterDirection.Output
                Prm.Value = Me.TextBox5.Text
                Prm2.Value = 0
                .Parameters.Add(Prm)
                .Parameters.Add(Prm2)
                Dim i As Integer = Comando.ExecuteNonQuery
                Cont = Prm2.Value
            End With
            Me.ConGeneralAlertaTableAdapter.Connection = ConLidia5
            Me.ConGeneralAlertaTableAdapter.Fill(Me.DataSetLidia2.ConGeneralAlerta)
            newdirec = Me.TextBox6.Text
            ConLidia5.Close()
            If Cont = 0 Then
                MsgBox("La Ip Capturada no Existe en la Lista", MsgBoxStyle.Information)
                Me.TextBox5.Clear()
            End If
            Dame_Ips()
        End If
    End Sub

    Private Sub TreeView1_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView1.AfterSelect
        Me.TextBox5.Text = e.Node.Text
    End Sub



    Private Sub Panel8_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Panel8.VisibleChanged
        Dim cone3 As New SqlClient.SqlConnection(MiConexion)
        If Bnd = 1 And IsNumeric(Monto) = True Then
            cone3.Open()
            Me.NueGeneralAlertaTableAdapter.Connection = cone3
            Me.NueGeneralAlertaTableAdapter.Fill(Me.DataSetLidia2.NueGeneralAlerta, CDec(Monto), Mensaje, Direcciones)
            cone3.Close()
        End If
    End Sub


    Private Sub MontocablemodeminaTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Button23_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button23.Click
        Dim CON As New SqlConnection(MiConexion)
        Try

            If Me.CablemodemsCheckBox.CheckState = CheckState.Checked Then
                If CLng(Me.PregcablTextBox.Text) = 0 Then
                    MsgBox("El N�mero de Pagos Para Un Cablemodem Debe Ser Al Menos 1", MsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            'If Me.DecodersCheckBox.CheckState = CheckState.Checked Then
            '    If CLng(Me.PregdecoTextBox.Text) = 0 Then
            '        MsgBox("El N�mero de Pagos Para Un Aparato Digital Debe Ser Al Menos 1", MsgBoxStyle.Information)
            '        Exit Sub
            '    End If
            'End If


            Dim cmd As New SqlClient.SqlCommand
            CON.Open()
            With cmd
                .CommandText = "Insert_Cobrodepositobueno"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = CON
                Dim prm As New SqlParameter("@cobrocablemodem", SqlDbType.Decimal)
                Dim prm1 As New SqlParameter("@cobrodecos", SqlDbType.Decimal)
                Dim prm2 As New SqlParameter("@preguntacabl", SqlDbType.Decimal)
                Dim prm3 As New SqlParameter("@preguntasdecos", SqlDbType.Decimal)
                prm.Direction = ParameterDirection.Input
                prm1.Direction = ParameterDirection.Input
                prm2.Direction = ParameterDirection.Input
                prm3.Direction = ParameterDirection.Input
                prm.Value = Me.CablemodemsCheckBox.CheckState
                prm1.Value = Me.DecodersCheckBox.CheckState
                prm2.Value = CLng(Me.PregcablTextBox.Text)
                prm3.Value = CLng(Me.PregcablTextBox.Text)
                .Parameters.Add(prm)
                .Parameters.Add(prm1)
                .Parameters.Add(prm2)
                .Parameters.Add(prm3)
                Dim x As Integer = cmd.ExecuteNonQuery
            End With
            '@cobrocablemodem bit,@cobrodecos bit,@preguntacabl bigint,@preguntasdecos bigint
            MsgBox("Se Guardo Con Exito", MsgBoxStyle.Information)

            Me.Consulta_cobrodepositobuenoTableAdapter.Connection = CON
            Me.Consulta_cobrodepositobuenoTableAdapter.Fill(Me.Procedimientosarnoldo4.Consulta_cobrodepositobueno, 0)
            CON.Close()
        Catch ex As Exception
            If CON.State = ConnectionState.Open Then
                CON.Close()
            End If
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub




    Private Sub PregcablTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles PregcablTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.PregcablTextBox, Asc(LCase(e.KeyChar)), "L")))
    End Sub


    Private Sub PregcablTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PregcablTextBox.TextChanged
        Me.PregdecoTextBox.Text = Me.PregcablTextBox.Text
    End Sub

    Private Sub PregdecoTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles PregdecoTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.PregdecoTextBox, Asc(LCase(e.KeyChar)), "L")))
    End Sub

    Private Sub PregdecoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PregdecoTextBox.TextChanged

    End Sub

    Private Sub CablemodemsCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CablemodemsCheckBox.CheckedChanged
        If Me.CablemodemsCheckBox.CheckState = CheckState.Checked And Me.DecodersCheckBox.CheckState = CheckState.Checked Then
            Me.PregcablTextBox.Enabled = True
        ElseIf Me.CablemodemsCheckBox.CheckState = CheckState.Unchecked And Me.DecodersCheckBox.CheckState = CheckState.Checked Then
            Me.PregcablTextBox.Enabled = True
        ElseIf Me.CablemodemsCheckBox.CheckState = CheckState.Checked And Me.DecodersCheckBox.CheckState = CheckState.Unchecked Then
            Me.PregcablTextBox.Enabled = True
        ElseIf Me.CablemodemsCheckBox.CheckState = CheckState.Unchecked And Me.DecodersCheckBox.CheckState = CheckState.Unchecked Then
            Me.PregcablTextBox.Enabled = False
        End If
    End Sub

    Private Sub DecodersCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DecodersCheckBox.CheckedChanged
        If Me.CablemodemsCheckBox.CheckState = CheckState.Checked And Me.DecodersCheckBox.CheckState = CheckState.Checked Then
            Me.PregcablTextBox.Enabled = True
        ElseIf Me.CablemodemsCheckBox.CheckState = CheckState.Unchecked And Me.DecodersCheckBox.CheckState = CheckState.Checked Then
            Me.PregcablTextBox.Enabled = True
        ElseIf Me.CablemodemsCheckBox.CheckState = CheckState.Checked And Me.DecodersCheckBox.CheckState = CheckState.Unchecked Then
            Me.PregcablTextBox.Enabled = True
        ElseIf Me.CablemodemsCheckBox.CheckState = CheckState.Unchecked And Me.DecodersCheckBox.CheckState = CheckState.Unchecked Then
            Me.PregcablTextBox.Enabled = False
        End If
    End Sub

    Private Sub Button25_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button25.Click
        Dim CON As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Dim error1 As Integer

        Try
            If IsNumeric(Me.TextBox8.Text) = True Then
                CON.Open()
                cmd = New SqlClient.SqlCommand()

                With cmd
                    .CommandText = "Inserta_Rel_PaquetesDigTel"
                    .Connection = CON
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0

                    '@nopaquetes bigint, @precio money,@error int output
                    Dim prm1 As New SqlParameter("@nopaquetes", SqlDbType.BigInt)
                    prm1.Direction = ParameterDirection.Input
                    prm1.Value = Me.NumericUpDown9.Value
                    .Parameters.Add(prm1)

                    Dim prm2 As New SqlParameter("@precio", SqlDbType.Money)
                    prm2.Direction = ParameterDirection.Input
                    prm2.Value = CDbl(Me.TextBox8.Text)
                    .Parameters.Add(prm2)

                    Dim prm3 As New SqlParameter("@error", SqlDbType.Int)
                    prm3.Direction = ParameterDirection.Output
                    prm3.Value = 0
                    .Parameters.Add(prm3)

                    Dim i As Integer = cmd.ExecuteNonQuery()

                    error1 = prm3.Value()
                End With
                CON.Close()
            Else
                MsgBox("Falta Capturar el Monto", MsgBoxStyle.Information)
                Exit Sub
            End If


            If error1 = 1 Then
                MsgBox("El Numero De Paquetes Ya Existe En La Lista", MsgBoxStyle.Information)
                Me.TextBox8.Text = ""
                Exit Sub
            End If

            CON.Open()
            Me.Consulta_Rel_PaquetesDigTelTableAdapter.Connection = CON
            Me.Consulta_Rel_PaquetesDigTelTableAdapter.Fill(Me.Procedimientosarnoldo4.Consulta_Rel_PaquetesDigTel)
            CON.Close()

            bitsist(GloUsuario, 0, LocGloSistema, "Generales Del Sistema", "Se Agrego Una Tarifa Para Paquetes Digitales De Telefonia:", "No. De Paquetes: " + CStr(Me.NumericUpDown9.Value), "Tarifa ($): " + CStr(Me.TextBox8.Text), LocClv_Ciudad)

            Me.TextBox8.Text = ""



        Catch ex As Exception
            If CON.State = ConnectionState.Open Then
                CON.Close()
            End If
            System.Windows.Forms.MessageBox.Show(ex.Message)

        End Try

    End Sub


    Private Sub Button26_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button26.Click
        Dim CON1 As New SqlConnection(MiConexion)
        Dim cmd1 As New SqlClient.SqlCommand

        Try

            If IsNumeric(Me.ConsecutivoTextBox.Text) = False Then
                MsgBox("Seleccione Un Registro Para Borrar", MsgBoxStyle.Information)
                Exit Sub
            End If

            CON1.Open()
            cmd1 = New SqlClient.SqlCommand()

            With cmd1
                .CommandText = "Borra_Rel_PaquetesDigTel"
                .Connection = CON1
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@consecutivo", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = CLng(Me.ConsecutivoTextBox.Text)
                .Parameters.Add(prm)


                Dim ia As Integer = cmd1.ExecuteNonQuery()
            End With

            Me.Consulta_Rel_PaquetesDigTelTableAdapter.Connection = CON1
            Me.Consulta_Rel_PaquetesDigTelTableAdapter.Fill(Me.Procedimientosarnoldo4.Consulta_Rel_PaquetesDigTel)
            CON1.Close()

            bitsist(GloUsuario, 0, LocGloSistema, "Generales Del Sistema", "Se Elimino Una Tarifa Para Paquetes Digitales De Tel�fonia: ", "Clave :" + Me.ConsecutivoTextBox.Text, "", LocClv_Ciudad)

            'Me.ConsecutivoTextBox.Clear()

        Catch ex As Exception
            If CON1.State = ConnectionState.Open Then
                CON1.Close()
            End If
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub TextBox8_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox8.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox8, Asc(LCase(e.KeyChar)), "L")))
    End Sub





    Private Sub Button28_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button28.Click

        If Me.TextBoxserie.Text = "" Then
            Me.TextBoxserie.Focus()
            MsgBox("Introduzca la Serie", MsgBoxStyle.Information, "Mensaje")
            Exit Sub
        Else
            If IsNumeric(Me.TextBoxfolio.Text) Then
                Guardar()
            Else
                Me.TextBoxfolio.Focus()
                Me.TextBoxfolio.Text = ""
                MsgBox("Introduzca el Folio", MsgBoxStyle.Information, "Mensaje")
                Exit Sub
            End If
        End If


        'BOTON GUARDAR DE GENERALES XML

        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("GUARDAR_Generales_Xml", con)
        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New SqlParameter( _
                  "@Nom_Archivo", SqlDbType.VarChar, 250)
        prm.Direction = ParameterDirection.Input
        prm.Value = Me.Nom_ArchivoTextBox.Text
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                  "@Fecha_Inicio", SqlDbType.DateTime)
        prm.Direction = ParameterDirection.Input
        prm.Value = Me.Fecha_InicioDateTimePicker.Value
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                 "@Fecha_Facturas", SqlDbType.DateTime)
        prm.Direction = ParameterDirection.Input
        prm.Value = Me.Fecha_FacturasDateTimePicker.Value
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                 "@Periodo_inicial", SqlDbType.Int)
        prm.Direction = ParameterDirection.Input
        If IsNumeric(Me.Periodo_inicialTextBox.Text) Then
            prm.Value = CInt(Me.Periodo_inicialTextBox.Text)
        Else
            prm.Value = 0
        End If
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                "@Periodo_final", SqlDbType.Int)
        prm.Direction = ParameterDirection.Input
        If IsNumeric(Me.Periodo_finalTextBox.Text) Then
            prm.Value = CInt(Me.Periodo_finalTextBox.Text)
        Else
            prm.Value = 0
        End If
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                  "@Numero_Inicio", SqlDbType.Int)
        prm.Direction = ParameterDirection.Input
        If IsNumeric(Me.Numero_InicioTextBox) Then
            prm.Value = CInt(Me.Numero_InicioTextBox.Text)
        Else
            prm.Value = 0
        End If
        cmd.Parameters.Add(prm)


        prm = New SqlParameter( _
                  "@Formato", SqlDbType.Int)
        prm.Direction = ParameterDirection.Input
        If IsNumeric(Me.FormatoTextBox.Text) Then
            prm.Value = CInt(Me.FormatoTextBox.Text)
        Else
            prm.Value = 0
        End If
        cmd.Parameters.Add(prm)


        prm = New SqlParameter( _
                  "@Ruta", SqlDbType.VarChar, 250)
        prm.Direction = ParameterDirection.Input
        prm.Value = Me.RutaTextBox.Text
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                 "@Numero_Local", SqlDbType.VarChar, 50)
        prm.Direction = ParameterDirection.Input
        prm.Value = Me.Numero_LocalTextBox.Text
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                 "@Pais_Local", SqlDbType.VarChar, 50)
        prm.Direction = ParameterDirection.Input
        prm.Value = Me.Pais_LocalTextBox.Text
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
               "@Clave044", SqlDbType.VarChar, 50)
        prm.Direction = ParameterDirection.Input
        prm.Value = Me.Clave044TextBox.Text
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
              "@Clave045", SqlDbType.VarChar, 50)
        prm.Direction = ParameterDirection.Input
        prm.Value = Me.Clave045TextBox.Text
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
            "@Clave01800", SqlDbType.VarChar, 50)
        prm.Direction = ParameterDirection.Input
        prm.Value = Me.Clave01800TextBox.Text
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
           "@Clave01900", SqlDbType.VarChar, 50)
        prm.Direction = ParameterDirection.Input
        prm.Value = Me.Clave01900TextBox.Text
        cmd.Parameters.Add(prm)

        Try
            con.Open()
            Dim i As Integer = cmd.ExecuteNonQuery()
            Guarda_Clv_Adic()
            MsgBox("Se Guardo con �xito", MsgBoxStyle.Information, "Mensaje")

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        con.Close()
        Me.Close()

    End Sub

    Private Sub Busca()

        Dim con As New SqlConnection(MiConexion)
        Dim reader As SqlDataReader
        Dim cmd As New SqlCommand("CONSULTA_Folios", con)
        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New SqlParameter( _
                  "@ID", SqlDbType.Int)
        prm.Direction = ParameterDirection.Input
        prm.Value = 1
        cmd.Parameters.Add(prm)

        Try
            con.Open()
            reader = cmd.ExecuteReader()
            Using reader
                While reader.Read
                    Me.TextBoxserie.Text = reader.GetValue(0)
                    Me.TextBoxfolio.Text = reader.GetValue(1)
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        con.Close()

    End Sub

    Private Sub Guardar()

        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("GUARDAR_Folios", con)
        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New SqlParameter( _
                  "@Serie", SqlDbType.VarChar, 15)
        prm.Direction = ParameterDirection.Input
        prm.Value = Me.TextBoxserie.Text
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                  "@Folio", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.TextBoxfolio.Text)
        cmd.Parameters.Add(prm)

        Try
            con.Open()
            Dim i As Integer = cmd.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        con.Close()
        'Me.Close()
    End Sub

    Private Sub ConDiasPorRecuperar()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ConDiasPorRecuperar", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Dias", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = 0
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub NueDiasPorRecuperar(ByVal Dias As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueDiasPorRecuperar", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("Dias", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Dias
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message)
        End Try
    End Sub



    Private Sub Button30_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button30.Click

        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("GUARDAR_Generales_Santander", con)
        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New SqlParameter( _
                  "@Referencia", SqlDbType.VarChar, 50)
        prm.Direction = ParameterDirection.Input
        prm.Value = Me.ReferenciaTextBox.Text
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                  "@Constante", SqlDbType.Int)
        prm.Direction = ParameterDirection.Input
        If IsNumeric(Me.ConstanteTextBox.Text) Then
            prm.Value = CInt(Me.ConstanteTextBox.Text)
        Else
            prm.Value = 0
        End If
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                 "@Modulo", SqlDbType.Int)
        prm.Direction = ParameterDirection.Input
        If IsNumeric(Me.ModuloTextBox.Text) Then
            If CInt(Me.ModuloTextBox.Text) = 0 Then
                prm.Value = 97
            Else
                prm.Value = CInt(Me.ModuloTextBox.Text)
            End If
        Else
            prm.Value = 97
        End If
        cmd.Parameters.Add(prm)

        Try
            con.Open()
            Dim i As Integer = cmd.ExecuteNonQuery()
            MsgBox("Se Guardo con �xito", MsgBoxStyle.Information, "Mensaje")

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        con.Close()
    End Sub



    Private Sub Button31_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button31.Click

        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("GUARDAR_Generales_Hsbc", con)
        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New SqlParameter( _
                  "@Referencia", SqlDbType.VarChar, 50)
        prm.Direction = ParameterDirection.Input
        prm.Value = Me.ReferenciaTextBox1.Text
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                 "@Modulo", SqlDbType.Int)
        prm.Direction = ParameterDirection.Input
        If IsNumeric(Me.ModuloTextBox1.Text) Then
            If CInt(Me.ModuloTextBox1.Text) = 0 Then
                prm.Value = 10
            Else
                prm.Value = CInt(Me.ModuloTextBox1.Text)
            End If
        Else
            prm.Value = 10
        End If
        cmd.Parameters.Add(prm)

        Try
            con.Open()
            Dim i As Integer = cmd.ExecuteNonQuery()
            MsgBox("Se Guardo con �xito", MsgBoxStyle.Information, "Mensaje")

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        con.Close()

    End Sub

    Private Sub TxtEdoClvAdic_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtEdoClvAdic.KeyPress
        e.KeyChar = ChrW(ValidaKey(TxtEdoClvAdic, Asc(e.KeyChar), "N"))
    End Sub

    Private Sub TxtCdClvAdic_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtCdClvAdic.KeyPress
        e.KeyChar = ChrW(ValidaKey(TxtCdClvAdic, Asc(e.KeyChar), "N"))
    End Sub

    Private Sub ConRelGeneralNumero()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ConRelGeneralNumero", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@NumeroInt", SqlDbType.VarChar, 20)
        parametro.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@NumeroExt", SqlDbType.VarChar, 20)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            Me.TextBoxNumeroInt.Text = parametro.Value
            Me.TextBoxNumeroExt.Text = parametro2.Value
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub NueRelGeneralNumero(ByVal NumeroInt As String, ByVal NumeroExt As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueRelGeneralNumero", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@NumeroInt", SqlDbType.VarChar, 20)
        parametro.Value = NumeroInt
        parametro.Direction = ParameterDirection.Input
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@NumeroExt", SqlDbType.VarChar, 20)
        parametro2.Value = NumeroExt
        parametro2.Direction = ParameterDirection.Input
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub TabControl1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tbcAccesoriosInstalacion.SelectedIndexChanged
        If Me.tbcAccesoriosInstalacion.SelectedTab.Name = "TabPage1" Then
            DatosDeLaEmpresa()
        ElseIf Me.tbcAccesoriosInstalacion.SelectedTab.Name = "TabPage2" Then
            ServiciosDeTelevision()
        ElseIf Me.tbcAccesoriosInstalacion.SelectedTab.Name = "TabPage3" Then
            PeriodoDeCorte()
        ElseIf Me.tbcAccesoriosInstalacion.SelectedTab.Name = "TabPage4" Then
            CobroDeAdeudo()
        ElseIf Me.tbcAccesoriosInstalacion.SelectedTab.Name = "TabPage5" Then
            Correo()
        ElseIf Me.tbcAccesoriosInstalacion.SelectedTab.Name = "TabPage6" Then
            FacturasGlobales()
        ElseIf Me.tbcAccesoriosInstalacion.SelectedTab.Name = "TabPage7" Then
            MensajeEnTickets()
        ElseIf Me.tbcAccesoriosInstalacion.SelectedTab.Name = "TabPage8" Then
            Alertas()
        ElseIf Me.tbcAccesoriosInstalacion.SelectedTab.Name = "TabPage9" Then
            Telefonia()
        ElseIf Me.tbcAccesoriosInstalacion.SelectedTab.Name = "TabPage10" Then
            Depositos()
        ElseIf Me.tbcAccesoriosInstalacion.SelectedTab.Name = "TabPage11" Then
            PaquetesDigitalesTelefonia()
        ElseIf Me.tbcAccesoriosInstalacion.SelectedTab.Name = "TabPage12" Then
            Impresion()
        ElseIf Me.tbcAccesoriosInstalacion.SelectedTab.Name = "TabPage13" Then
            ReferenciaSantander()
        ElseIf Me.tbcAccesoriosInstalacion.SelectedTab.Name = "TabPage14" Then
            ReferenciaHSBC()
        ElseIf Me.tbcAccesoriosInstalacion.SelectedTab.Name = "TabPage15" Then
            Me.LlenaCombosQuejas(0, 0)
            Llena_arbolTec_quejas()
            LLenaTreeViewOrdenes()
        ElseIf Me.tbcAccesoriosInstalacion.SelectedTab.Name = "TabPage16" Then
            ConGeneralCarteraEjecutiva()
        ElseIf Me.tbcAccesoriosInstalacion.SelectedTab.Name = "TabPage17" Then
            Me.ConTabla_Impuestos(1)
        ElseIf Me.tbcAccesoriosInstalacion.SelectedTab.Name = "TabOxxo" Then
            UspComisionOxxo(0, 1)
            UspPrefijoOxxo(String.Empty, 1)
        ElseIf Me.tbcAccesoriosInstalacion.SelectedTab.Name = "tbEstadoCuenta" Then
            MUESTRAPERIODOS(0)
            ConEstadoCuentaReferencias()
            If cbPeriodo.Text.Length = 0 Then Exit Sub
            ConGeneralEstadoCuenta(cbPeriodo.SelectedValue)
        ElseIf Me.tbcAccesoriosInstalacion.SelectedTab.Name = "tbpAccesoriosInstalacion" Then

            'Carga los Datos Iniciales del Tab
            Try
                EnlazaDatosAccesoriosDeInstalacion()
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            End Try
        ElseIf Me.tbcAccesoriosInstalacion.SelectedTab.Name = "tpCobroMaterial" Then
            Me.dgvRangosMaterial.DataSource = DACobroDeMaterial.spConsultaRangosCobroMaterial()
        ElseIf Me.tbcAccesoriosInstalacion.SelectedTab.Name = "Mizar" Then
            MuestraCompaNia()
        ElseIf Me.tbcAccesoriosInstalacion.SelectedTab.Name = "TabPageCorteSistema" Then
            dameDatosCorteSATV()
        End If

        If Me.TextBox2.Text = "Diario" Then
            Me.RadioButton1.Checked = True
            Me.RadioButton2.Checked = False
        ElseIf Me.TextBox2.Text = "Mensual" Then
            Me.RadioButton2.Checked = True
            Me.RadioButton1.Checked = False
        End If
    End Sub

    Private Sub MuestraCompaNia()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clvCompania", SqlDbType.Int, 0)
        Dim DataT As New DataTable()
        DataT = BaseII.ConsultaDT("MUESTRAtblCompanias")
        cmbCompanias.DataSource = DataT
    End Sub

    Private Sub DatosDeLaEmpresa()
        Dim CON As New SqlConnection(MiConexion)
        ConRelGeneralNumero()
        CON.Open()
        'noseve()
        'Me.PanelDatos.Visible = True
        'Me.Panel2.Visible = False
        'Me.Panel6.Visible = False
        Me.Consulta_Cp_GeneTableAdapter.Connection = CON
        Me.Consulta_Cp_GeneTableAdapter.Fill(Me.DataSetarnoldo.Consulta_Cp_Gene)
        datosbitpaneldatos()
        CON.Close()

    End Sub

    Private Sub ServiciosDeTelevision()
        Dim Con As New SqlConnection(MiConexion)
        Con.Open()
        'noseve()
        'Me.PanelBasico.Visible = True
        'Me.Panel2.Visible = False
        'Me.Panel6.Visible = False
        Me.Consulta_Generales_Filtros_OrdenesTableAdapter.Connection = Con
        Me.Consulta_Generales_Filtros_OrdenesTableAdapter.Fill(Me.ProcedimientosArnoldo2.Consulta_Generales_Filtros_Ordenes, LocBndFiltro1, LocBndFiltro2)
        If LocBndFiltro1 = 1 Then
            Me.CheckBox2.Checked = True
        Else
            Me.CheckBox2.Checked = False
        End If
        If LocBndFiltro2 = 1 Then
            Me.CheckBox3.Checked = True
        Else
            Me.CheckBox3.Checked = False
        End If
        Datosbitpanelbasic()
        uspConsultaTblImpresionOrdenQueja() 'SE LLENAN LOS VALORES DE LOS CHECK DE IMPRESIONES AUTOM�TCIAS
        Con.Close()
    End Sub

    Private Sub PeriodoDeCorte()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'noseve()
        'Me.Panel1.Visible = True
        'Me.Panel2.Visible = False
        'Me.Panel6.Visible = False
        Me.MUESTRAPERIODOSTableAdapter1.Connection = CON
        Me.MUESTRAPERIODOSTableAdapter1.Fill(Me.Procedimientosarnoldo4.MUESTRAPERIODOS, 1)
        Me.ComboBox1.Text = ""
        CON.Close()
    End Sub

    Private Sub CobroDeAdeudo()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'noseve()
        Me.MuestraTipServEricTableAdapter.Connection = CON
        Me.MuestraTipServEricTableAdapter.Fill(Me.DataSetEric.MuestraTipServEric, 0, 0)
        Me.ConMesesCobroAdeudoTableAdapter.Connection = CON
        Me.ConMesesCobroAdeudoTableAdapter.Fill(Me.DataSetEric.ConMesesCobroAdeudo, Me.ConceptoComboBox.SelectedValue, 1)
        'Me.Panel3.Visible = True
        'Me.Panel6.Visible = False
        datosbitpanel3()
        CON.Close()
    End Sub

    Private Sub Correo()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConGeneralCorreoTableAdapter.Connection = CON
        Me.ConGeneralCorreoTableAdapter.Fill(Me.DataSetEric.ConGeneralCorreo)
        CON.Close()
        'noseve()
        'Me.Panel4.Visible = True
        'Me.Panel6.Visible = False
        datosbitpanel4()
    End Sub

    Private Sub FacturasGlobales()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'noseve()
        'Me.Panel6.Visible = True
        Me.COnsulta_Gral_factura_globalTableAdapter.Connection = CON
        Me.COnsulta_Gral_factura_globalTableAdapter.Fill(Me.ProcedimientosArnoldo2.COnsulta_Gral_factura_global, 0)
        datosbitpanel6()
        CON.Close()
    End Sub

    Private Sub MensajeEnTickets()
        Try
            'noseve()
            'Me.Panel7.Visible = True
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.ConGeneralMsjTicketsTableAdapter.Connection = CON
            Me.ConGeneralMsjTicketsTableAdapter.Fill(Me.DataSetEric2.ConGeneralMsjTickets)
            CON.Close()
            datosbitpanel7()
            eMensaje = Me.MensajeTextBox.Text


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Alertas()
        Dim conlidia4 As New SqlClient.SqlConnection(MiConexion)
        Dim cmd2 As New SqlClient.SqlCommand
        conlidia4.Open()
        Me.ConGeneralAlertaTableAdapter.Connection = conlidia4
        Me.ConGeneralAlertaTableAdapter.Fill(Me.DataSetLidia2.ConGeneralAlerta)
        With cmd2
            .CommandText = "AlarmasAuto"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = conlidia4
            Dim prm As New SqlParameter("@bit", SqlDbType.Bit)
            Dim prm2 As New SqlParameter("@bit2", SqlDbType.Bit)
            Dim prm3 As New SqlParameter("@opc", SqlDbType.Int)
            prm.Direction = ParameterDirection.Input
            prm2.Direction = ParameterDirection.Output
            prm3.Direction = ParameterDirection.Input
            prm.Value = 0
            prm2.Value = 0
            prm3.Value = 2
            .Parameters.Add(prm)
            .Parameters.Add(prm2)
            .Parameters.Add(prm3)
            Dim i As Integer = cmd2.ExecuteNonQuery
            If IsDBNull(prm2.Value) Then
                prm2.Value = 0
            Else
                prm2.Value = 1
            End If
            Me.CheckBox4.Checked = prm2.Value
        End With
        conlidia4.Close()
        'noseve()
        'Me.Panel8.Visible = True
        Mensaje = Me.TextBox3.Text
        Monto = Me.TextBox4.Text
        Direcciones = Me.TextBox6.Text
        newdirec = Me.TextBox6.Text
        Bnd = 1
        Dame_Ips()
    End Sub

    Private Sub Telefonia()
        Dim CON As New SqlConnection(MiConexion)

        'noseve()
        'Me.Panel2.Visible = False
        'Me.Panel6.Visible = False
        'Me.PanelDatosXml.Visible = True
        Busca()

        Try

            CON.Open()
            'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetyahve.CONSULTA_General_Xml' Puede moverla o quitarla seg�n sea necesario.
            Me.CONSULTA_General_XmlTableAdapter.Connection = CON
            Me.CONSULTA_General_XmlTableAdapter.Fill(Me.DataSetyahve.CONSULTA_General_Xml)
            CON.Close()
            Consulta_Clv_Adic()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Depositos()
        Dim con As New SqlConnection(MiConexion)
        'noseve()
        'Me.Panel9.Visible = True
        'Me.Panel6.Visible = False
        con.Open()
        'Me.Consulta_cobrodepositoTableAdapter.Connection = con
        'Me.Consulta_cobrodepositoTableAdapter.Fill(Me.Procedimientosarnoldo4.Consulta_cobrodeposito, 0)
        Me.Consulta_cobrodepositobuenoTableAdapter.Connection = con
        Me.Consulta_cobrodepositobuenoTableAdapter.Fill(Me.Procedimientosarnoldo4.Consulta_cobrodepositobueno, 0)
        If Me.CablemodemsCheckBox.CheckState = CheckState.Checked And Me.DecodersCheckBox.CheckState = CheckState.Checked Then
            Me.PregcablTextBox.Enabled = True
        ElseIf Me.CablemodemsCheckBox.CheckState = CheckState.Unchecked And Me.DecodersCheckBox.CheckState = CheckState.Checked Then
            Me.PregcablTextBox.Enabled = True
        ElseIf Me.CablemodemsCheckBox.CheckState = CheckState.Checked And Me.DecodersCheckBox.CheckState = CheckState.Unchecked Then
            Me.PregcablTextBox.Enabled = True
        ElseIf Me.CablemodemsCheckBox.CheckState = CheckState.Unchecked And Me.DecodersCheckBox.CheckState = CheckState.Unchecked Then
            Me.PregcablTextBox.Enabled = False
        End If
        con.Close()
    End Sub

    Private Sub PaquetesDigitalesTelefonia()
        Try
            'noseve()
            'Me.Panel10.Visible = True
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.Consulta_Rel_PaquetesDigTelTableAdapter.Connection = CON
            Me.Consulta_Rel_PaquetesDigTelTableAdapter.Fill(Me.Procedimientosarnoldo4.Consulta_Rel_PaquetesDigTel)
            CON.Close()
            'datosbitpanel7()
            'eMensaje = Me.MensajeTextBox.Text
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Impresion()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'noseve()
        'Me.Panel2.Visible = True
        'Me.Panel6.Visible = False
        Me.Consulta_ImpresorasTableAdapter.Connection = CON
        Me.Consulta_ImpresorasTableAdapter.Fill(Me.DataSetarnoldo.Consulta_Impresoras)
        datosbitpanel2()
        CON.Close()
    End Sub

    Private Sub ReferenciaSantander()
        Dim CON As New SqlConnection(MiConexion)

        'noseve()
        'Me.Panel2.Visible = False
        'Me.Panel6.Visible = False
        'Me.Panel11.Visible = True

        Try
            CON.Open()
            Me.CONSULTA_General_SantanderTableAdapter.Connection = CON
            Me.CONSULTA_General_SantanderTableAdapter.Fill(Me.DataSetyahve.CONSULTA_General_Santander)
            CON.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ReferenciaHSBC()
        Dim CON As New SqlConnection(MiConexion)

        'noseve()
        'Me.Panel2.Visible = False
        'Me.Panel6.Visible = False
        'Me.Panel12.Visible = True

        Try
            CON.Open()
            Me.CONSULTA_General_HsbcTableAdapter.Connection = CON
            Me.CONSULTA_General_HsbcTableAdapter.Fill(Me.DataSetyahve.CONSULTA_General_Hsbc)
            CON.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub CmBDepto_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmBDepto.SelectedValueChanged
        If IsNumeric(Me.CmBDepto.SelectedValue) = True Then
            If Me.CmBDepto.SelectedValue > 0 Then
                Me.LlenaCombosQuejas(1, CmBDepto.SelectedValue)
            End If
        End If
    End Sub

    Private Sub Button2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        If Me.CmBDepto.SelectedIndex = 0 Then
            MsgBox("Seleccione un Departamento.", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Me.CmbTecnicos.Items.Count > 0 Then
            NueRel_Tecnicos_Quejas(Me.CmBDepto.SelectedValue, Me.CmbTecnicos.SelectedValue)
        ElseIf Me.CmbTecnicos.Items.Count = 0 Then
            MsgBox("No se encontraron T�cnicos para ese Departamento.", MsgBoxStyle.Information)
        End If
    End Sub


    Private Sub TreViewTecnicos_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreViewTecnicos.AfterSelect
        Clv_Familia = 0
        clv_Tecnico = 0
        If e.Node.Level = 0 Then
            If IsNumeric(e.Node.Tag) = True Then
                Clv_Familia = e.Node.Tag
            End If
        ElseIf e.Node.Level = 1 Then
            If IsNumeric(e.Node.Tag) = True Then
                clv_Tecnico = e.Node.Tag
            End If
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If Clv_Familia > 0 Or clv_Tecnico > 0 Then
            Me.BorRel_Tecnicos_Quejas(Clv_Familia, clv_Tecnico)
            Clv_Familia = 0
            clv_Tecnico = 0
        End If
    End Sub

    Private Sub ConGeneralCarteraEjecutiva()
        Dim conexion As New SqlConnection(MiConexion)
        Dim dataAdapter As New SqlDataAdapter("EXEC ConGeneralCarteraEjecutiva", conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.DataGridView1.DataSource = bindingSource
            Me.LabelNota.Text = ""
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub NueGeneralCarteraEjecutiva(ByVal Clv_Grupo As Integer, ByVal Porcentaje As Integer, ByVal Activo As Boolean)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueGeneralCarteraEjecutiva", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Grupo", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Grupo
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Porcentaje", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Porcentaje
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Activo", SqlDbType.Bit)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Activo
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub ValidaGeneralCarteraEjecutiva()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaGeneralCarteraEjecutiva", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Res", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Msg", SqlDbType.VarChar, 150)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = parametro.Value
            eMsg = parametro2.Value
            If eRes = 1 Then
                Me.LabelNota.Text = eMsg
            Else
                Me.LabelNota.Text = ""
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub



    Private Sub TabPage16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TabPage16.Click

    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Dim i As Integer
        Try
            For i = 0 To Me.DataGridView1.RowCount - 1
                NueGeneralCarteraEjecutiva(Me.DataGridView1.Item(0, i).Value, Me.DataGridView1.Item(2, i).Value, Me.DataGridView1.Item(3, i).Value)
            Next
            MsgBox(mensaje5, MsgBoxStyle.Information)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
        ValidaGeneralCarteraEjecutiva()
    End Sub

    Private Sub NueRelOrdenesTecnicos(ByVal Clv_Tecnico As Long, ByVal Clv_Puesto As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueRelOrdenesTecnicos", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Tecnico", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Tecnico
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_Puesto", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_Puesto
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            LLenaTreeViewOrdenes()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub BorRelOrdenesTecnicos(ByVal Clv_Tecnico As Long, ByVal Clv_Puesto As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorRelOrdenesTecnicos", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Tecnico", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Tecnico
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_Puesto", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_Puesto
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            LLenaTreeViewOrdenes()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Function ConPuestos() As DataTable

        Dim conexion As New SqlConnection(MiConexion)
        Dim dataAdapter As New SqlDataAdapter("EXEC ConPuestos", conexion)
        Dim dataTable As New DataTable

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            Return dataTable
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Function

    Function ConRelOrdenesTecnicos(ByVal Clv_Puesto As Integer) As DataTable

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC ConRelOrdenesTecnicos ")
        strSQL.Append(CStr(Clv_Puesto))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            Return dataTable
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Function

    Private Sub LLenaTreeViewOrdenes()

        Dim dataTablePuesto As New DataTable()
        Dim dataTableTecnico As New DataTable()
        Dim NodoPuesto As Integer
        Dim NodoTecnico As Integer
        Dim FilaPuesto As DataRow
        Dim FilaTecnico As DataRow

        dataTablePuesto = ConPuestos()
        NodoPuesto = 0
        NodoTecnico = 0

        Me.TreeViewOrdenes.Nodes.Clear()

        For Each FilaPuesto In dataTablePuesto.Rows
            Me.TreeViewOrdenes.Nodes.Add(FilaPuesto("Descripcion").ToString()).ForeColor = Color.Black
            Me.TreeViewOrdenes.Nodes(NodoPuesto).Tag = FilaPuesto("Clv_Puesto")
            'Me.TreeViewOrdenes.Nodes(NodoPuesto).Name = FilaPuesto("Descripcion").ToString()
            dataTableTecnico = ConRelOrdenesTecnicos(FilaPuesto("Clv_Puesto"))
            NodoTecnico = 0
            For Each FilaTecnico In dataTableTecnico.Rows
                Me.TreeViewOrdenes.Nodes(NodoPuesto).Nodes.Add(FilaTecnico("Nombre").ToString()).ForeColor = Color.Black
                Me.TreeViewOrdenes.Nodes(NodoPuesto).Nodes(NodoTecnico).Tag = FilaTecnico("Clv_Tecnico")
                NodoTecnico += 1
            Next
            NodoPuesto += 1
        Next
        Me.TreeViewOrdenes.ExpandAll()

    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        If CmBDepto.Items.Count = 0 Then
            MsgBox("Seleccione un Departamento.", MsgBoxStyle.Information)
            Exit Sub
        End If
        If CmbTecnicos.Items.Count = 0 Then
            MsgBox("No se encontraron T�cnicos para ese Departamento.", MsgBoxStyle.Information)
            Exit Sub
        End If
        NueRelOrdenesTecnicos(Me.CmbTecnicos.SelectedValue, Me.CmBDepto.SelectedValue)
    End Sub

    Private Sub TreeViewOrdenes_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeViewOrdenes.AfterSelect
        Clv_PuestoOrd = 0
        Clv_TecnicoOrd = 0
        If e.Node.Level = 0 Then
            If IsNumeric(e.Node.Tag) = True Then
                Clv_PuestoOrd = e.Node.Tag
            End If
        ElseIf e.Node.Level = 1 Then
            If IsNumeric(e.Node.Tag) = True Then
                Clv_TecnicoOrd = e.Node.Tag
            End If
        End If
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        If Clv_PuestoOrd > 0 Or Clv_TecnicoOrd > 0 Then
            BorRelOrdenesTecnicos(Clv_TecnicoOrd, Clv_PuestoOrd)
            Clv_PuestoOrd = 0
            Clv_TecnicoOrd = 0
        End If
    End Sub

    Private Sub Button19_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button19.Click

        If Me.TextBox4.Text = "" Then
            MsgBox("Introduzca un valor correcto")
        Else
            Bnd = 2
            Dim cmd As New SqlClient.SqlCommand
            Dim cone As New SqlClient.SqlConnection(MiConexion)
            GuardaAlertas()
            cone.Open()
            With cmd
                .CommandText = "AlarmasAuto"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = cone
                Dim prm As New SqlParameter("@bit", SqlDbType.Bit)
                Dim prm2 As New SqlParameter("@bit2", SqlDbType.Bit)
                Dim prm3 As New SqlParameter("@opc", SqlDbType.Int)
                prm.Direction = ParameterDirection.Input
                prm2.Direction = ParameterDirection.Output
                prm3.Direction = ParameterDirection.Input
                prm.Value = CInt(Me.CheckBox4.Checked)
                prm2.Value = 0
                prm3.Value = 1
                .Parameters.Add(prm)
                .Parameters.Add(prm2)
                .Parameters.Add(prm3)
                Dim i As Integer = cmd.ExecuteNonQuery
            End With
            cone.Close()
            MsgBox("Los Cambios han Sido Guardados con �xito", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub Button14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button14.Click
        Try
            If IsNumeric(TextIva.Text) = False Then TextIva.Text = 0
            If IsNumeric(TextIEPS.Text) = False Then TextIEPS.Text = 0
            NueTabla_Impuestos(1, TextIva.Text, TextIEPS.Text)

            If Me.CheckIEPS.Checked = False Then
                MsgBox("Si desahibilita el IEPS surtir�n efecto varios procesos de Eliminaci�n de IEPS", MsgBoxStyle.YesNo)
                ModificaIEPS()
            End If
            MsgBox(mensaje5)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub NueTabla_Impuestos(ByVal id As Integer, ByVal Iva As Double, ByVal Ieps As Double)

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueTabla_Impuestos", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@id", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = id
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@iva", SqlDbType.Money)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Iva
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@ieps", SqlDbType.Money)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Ieps
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@SiIeps", SqlDbType.Bit)
        parametro4.Direction = ParameterDirection.Input
        If Me.CheckIEPS.CheckState = CheckState.Checked Then
            parametro4.Value = 1
        Else
            parametro4.Value = 0
        End If
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Cta_IEPS", SqlDbType.VarChar, 50)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = Me.CtaIepsText.Text
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@Calculo1", SqlDbType.Int)
        parametro6.Direction = ParameterDirection.Input
        If Me.ChkCalculo1.CheckState = CheckState.Checked Then
            parametro6.Value = 1
        Else
            parametro6.Value = 0
        End If
        comando.Parameters.Add(parametro6)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            If conexion.State <> ConnectionState.Closed Then conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub ConTabla_Impuestos(ByVal id As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ConTabla_Impuestos", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@id", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = id
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@iva", SqlDbType.Money)
        parametro2.Direction = ParameterDirection.Output
        parametro2.Value = 0
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@ieps", SqlDbType.Money)
        parametro3.Direction = ParameterDirection.Output
        parametro3.Value = 0
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@SiIeps", SqlDbType.Bit)
        parametro4.Direction = ParameterDirection.Output
        parametro4.Value = 0
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Cta_IEPS", SqlDbType.VarChar, 50)
        parametro5.Direction = ParameterDirection.Output
        parametro5.Value = 0
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@Calculo1", SqlDbType.Int)
        parametro6.Direction = ParameterDirection.Output
        parametro6.Value = 0
        comando.Parameters.Add(parametro6)

        Try
            Me.NumericUpDownIeps.Value = 0
            Me.NumericUpDownIva.Value = 0
            conexion.Open()
            comando.ExecuteNonQuery()
            aiva = parametro2.Value
            Me.TextIva.Text = aiva
            Me.CtaIepsText.Text = parametro5.Value.ToString
            aieps = parametro3.Value
            Me.TextIEPS.Text = aieps
            If parametro4.Value = True Then
                CheckIEPS.CheckState = CheckState.Checked
            Else
                CheckIEPS.CheckState = CheckState.Unchecked
            End If
            If parametro6.Value = 1 Then
                ChkCalculo1.CheckState = CheckState.Checked
            Else
                ChkCalculo1.CheckState = CheckState.Unchecked
            End If
            If Me.TextIva.Text > 0 Then
                Me.NumericUpDownIva.Value = Me.TextIva.Text * 100
            End If
            If Me.TextIEPS.Text > 0 Then
                Me.NumericUpDownIeps.Value = Me.TextIEPS.Text * 100
            End If

        Catch ex As Exception
            If conexion.State <> ConnectionState.Closed Then conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub CheckIEPS_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If Me.CheckIEPS.CheckState = CheckState.Checked Then
            Me.TextIEPS.Visible = True
            Me.CtaIepsText.Visible = True
            Me.LabelCtaIEPS.Visible = True
            Me.LabelIEPS.Visible = True
            ChkCalculo1.Visible = True
            Me.LabelCtaIEPS.Visible = True
            Me.LabelIEPS.Visible = True
        Else
            ChkCalculo1.Checked = False
            Me.TextIEPS.Text = 0
            Me.TextIEPS.Visible = False
            Me.CtaIepsText.Text = ""
            Me.CtaIepsText.Visible = False
            ChkCalculo1.Visible = False
            Me.LabelCtaIEPS.Visible = False
            Me.LabelIEPS.Visible = False
        End If
    End Sub

    Private Sub CheckIEPS_CheckedChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckIEPS.CheckedChanged
        If Me.CheckIEPS.CheckState = CheckState.Checked Then
            Me.TextIEPS.Visible = True
            Me.CtaIepsText.Visible = True
            Me.LabelCtaIEPS.Visible = True
            Me.LabelIEPS.Visible = True
            ChkCalculo1.Visible = True
            Me.LabelCtaIEPS.Visible = True
            Me.LabelIEPS.Visible = True
            Me.NumericUpDownIeps.Visible = True
            LblPorIeps.Visible = True
        Else
            LblPorIeps.Visible = False
            ChkCalculo1.Checked = False
            Me.TextIEPS.Text = 0
            Me.TextIEPS.Visible = False
            Me.CtaIepsText.Text = ""
            Me.CtaIepsText.Visible = False
            ChkCalculo1.Visible = False
            Me.LabelCtaIEPS.Visible = False
            Me.LabelIEPS.Visible = False
            Me.NumericUpDownIeps.Visible = False
        End If
    End Sub

    Private Sub NumericUpDownIva_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles NumericUpDownIva.LostFocus
        If IsNumeric(Me.NumericUpDownIva.Value) = False Then
            Me.TextIva.Text = 0
        End If
    End Sub

    Private Sub NumericUpDownIva_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumericUpDownIva.ValueChanged
        If Me.NumericUpDownIva.Value > 0 Then
            Me.TextIva.Text = Me.NumericUpDownIva.Value / 100
        Else
            Me.TextIva.Text = 0
        End If
    End Sub

    Private Sub NumericUpDownIeps_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles NumericUpDownIeps.LostFocus
        If IsNumeric(Me.NumericUpDownIeps.Value) = False Then
            Me.TextIEPS.Text = 0
        End If

    End Sub

    Private Sub NumericUpDownIeps_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumericUpDownIeps.ValueChanged
        If Me.NumericUpDownIeps.Value > 0 Then
            Me.TextIEPS.Text = Me.NumericUpDownIeps.Value / 100
        Else
            Me.TextIEPS.Text = 0
        End If
    End Sub

    Private Sub ModificaIEPS()
        Dim conexion As New SqlConnection(MiConexion)
        Dim command As New SqlCommand("ModificaIEPS", conexion)
        command.CommandType = CommandType.StoredProcedure

        Try
            conexion.Open()
            command.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub


    Private Sub ConGeneralEstadoCuenta(ByVal Clv_Periodo As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ConGeneralEstadoCuenta", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim reader As SqlDataReader

        Dim parametro As New SqlParameter("@Clv_Periodo", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Periodo
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            reader = comando.ExecuteReader()


            tbDiaIni.Clear()
            tbDiaFin.Clear()
            tbDiaGeneracion.Clear()
            tbDiaEnvio.Clear()

            While (reader.Read())
                tbDiaIni.Text = reader(0).ToString()
                tbDiaFin.Text = reader(1).ToString()
                tbDiaGeneracion.Text = reader(2).ToString()
                tbDiaEnvio.Text = reader(3).ToString()
            End While

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try


    End Sub


    Private Sub NueGeneralEstadoCuenta(ByVal Clv_Periodo As Integer, ByVal DiaIni As Integer, ByVal DiaFin As Integer, ByVal DiaGeneracion As Integer, ByVal DiaEnvioDeco As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueGeneralEstadoCuenta", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Clv_Periodo", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = Clv_Periodo
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@DiaIni", SqlDbType.Int)
        par2.Direction = ParameterDirection.Input
        par2.Value = DiaIni
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@DiaFin", SqlDbType.Int)
        par3.Direction = ParameterDirection.Input
        par3.Value = DiaFin
        comando.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@DiaGeneracion", SqlDbType.Int)
        par4.Direction = ParameterDirection.Input
        par4.Value = DiaGeneracion
        comando.Parameters.Add(par4)

        Dim par5 As New SqlParameter("@DiaEnvioDeco", SqlDbType.Int)
        par5.Direction = ParameterDirection.Input
        par5.Value = DiaEnvioDeco
        comando.Parameters.Add(par5)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            MsgBox(mensaje5)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try



    End Sub

    Private Sub MUESTRAPERIODOS(ByVal Clv_Periodo As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim stringBuilder As New StringBuilder("EXEC MUESTRAPERIODOS " + Clv_Periodo.ToString())
        Dim dataAdapter As New SqlDataAdapter(stringBuilder.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindignSource As New BindingSource


        Try
            dataAdapter.Fill(dataTable)
            bindignSource.DataSource = dataTable
            cbPeriodo.DataSource = bindignSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try


    End Sub


    Private Sub cbPeriodo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbPeriodo.SelectedIndexChanged

        If cbPeriodo.Text.Length = 0 Then Exit Sub

        ConGeneralEstadoCuenta(cbPeriodo.SelectedValue)

    End Sub


    Private Sub bnEstadoCuentaGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnEstadoCuentaGuardar.Click
        If cbPeriodo.Text.Length = 0 Then
            Exit Sub
        End If

        If tbDiaIni.Text.Length = 0 Then tbDiaIni.Text = 0
        If IsNumeric(tbDiaIni.Text) = False Then tbDiaIni.Text = 0

        If tbDiaFin.Text.Length = 0 Then tbDiaFin.Text = 0
        If IsNumeric(tbDiaFin.Text) = False Then tbDiaFin.Text = 0

        If tbDiaGeneracion.Text.Length = 0 Then tbDiaGeneracion.Text = 0
        If IsNumeric(tbDiaGeneracion.Text) = False Then tbDiaGeneracion.Text = 0

        If tbDiaEnvio.Text.Length = 0 Then tbDiaEnvio.Text = 0
        If IsNumeric(tbDiaEnvio.Text) = False Then tbDiaEnvio.Text = 0

        If tbDiaIni.Text.Length = 0 Then tbDiaIni.Text = 0
        If IsNumeric(tbDiaIni.Text) = False Then tbDiaIni.Text = 0

        If tbPrefijoOxxo.Text.Length = 0 Then tbPrefijoOxxo.Text = 0
        If IsNumeric(tbPrefijoOxxo.Text) = False Then tbPrefijoOxxo.Text = 0

        NueGeneralEstadoCuenta(cbPeriodo.SelectedValue, tbDiaIni.Text, tbDiaFin.Text, tbDiaGeneracion.Text, tbDiaEnvio.Text)
        NueEstadoCuentaReferencias(tbLetraInicialBancomer.Text, tbPrefijoOxxo.Text, tbConvenioBancomer.Text)
    End Sub

    Private Sub GeneraEstadoCuenta(ByVal Clv_Periodo As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("GeneraEstadoCuenta", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Clv_Periodo", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = Clv_Periodo
        comando.Parameters.Add(par1)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub bnGenerarEstado_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnGenerarEstado.Click
        If cbPeriodo.Text.Length = 0 Then
            MsgBox("Selecciona el periodo a generar.")
            Exit Sub
        End If
        eClv_Periodo = cbPeriodo.SelectedValue
        FrmEstadoCuenta.Show()
    End Sub

    Private Sub ConEstadoCuentaReferencias()
        Dim conexion As New SqlConnection(MiConexion)
        Dim sBuilder As New StringBuilder("EXEC ConEstadoCuentaReferencias")
        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
        Dim dTable As New DataTable
        Dim dRow As DataRow

        Try
            dAdapter.Fill(dTable)
            For Each dRow In dTable.Rows
                tbLetraInicialBancomer.Text = dRow(0).ToString()
                tbPrefijoOxxo.Text = dRow(1).ToString()
                tbConvenioBancomer.Text = dRow(2).ToString()
            Next
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub NueEstadoCuentaReferencias(ByVal LetraInicialBancomer As String, ByVal PrefijoOxxo As String, ByVal ConvenioBancomer As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueEstadoCuentaReferencias", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@LetraInicialBancomer", SqlDbType.VarChar, 5)
        par1.Direction = ParameterDirection.Input
        par1.Value = LetraInicialBancomer
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@PrefijoOxxo", SqlDbType.VarChar, 5)
        par2.Direction = ParameterDirection.Input
        par2.Value = PrefijoOxxo
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@ConvenioBancomer", SqlDbType.VarChar, 50)
        par3.Direction = ParameterDirection.Input
        par3.Value = ConvenioBancomer
        comando.Parameters.Add(par3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub


#Region "Accesorios de Instalaci�n"

    'Insertar
    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click

        Try
            If (Softv.BAL.RelTipoCajaAccesorioInstalacion.Add(0, cmbTipoDeCaja.SelectedValue, cmbDescArticulo.SelectedValue, cmbAccesorioInst.SelectedValue)) > 0 Then
                bitsist(GloUsuario, 0, "Softv", "FrmGenerales_Sistema - Accesorios de Instalaci�n", "Agregar: Accesorio de Instalaci�n", "Se agreg� la relaci�n: ", cmbTipoDeCaja.Text & " --> " & cmbDescArticulo.Text & " ( " & cmbAccesorioInst.Text & " ) ", IdSistema)
                MsgBox("Se ha guardado la relaci�n.", MsgBoxStyle.Information, "GUARDADA")
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            EnlazaDatosAccesoriosDeInstalacion()
        End Try


    End Sub

    'Eliminar
    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Try
            If (dgvRelArticuloAccesorioInst.RowCount > 0) Then
                Softv.BAL.RelTipoCajaAccesorioInstalacion.Delete(dgvRelArticuloAccesorioInst.CurrentRow.Cells("ID").Value)
                bitsist(GloUsuario, 0, "Softv", "FrmGenerales_Sistema - Accesorios de Instalaci�n", "Eliminar: Accesorio de Instalaci�n", "Se elimin� la relaci�n: ", "ID: " & dgvRelArticuloAccesorioInst.CurrentRow.Cells("ID").Value.ToString & " - " & dgvRelArticuloAccesorioInst.CurrentRow.Cells("idTipoCaja").Value.ToString & " --> " & dgvRelArticuloAccesorioInst.CurrentRow.Cells("Articulo").Value.ToString & " ( " & dgvRelArticuloAccesorioInst.CurrentRow.Cells("Accesorio").Value.ToString & " ) ", IdSistema)
                MsgBox("Se ha eliminado la relaci�n.", MsgBoxStyle.Information, "ELIMINADA")
            Else
                MsgBox("No ha relaciones que eliminar.", MsgBoxStyle.Information)
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            EnlazaDatosAccesoriosDeInstalacion()
        End Try
    End Sub

    'Carga los datos iniciales para el Formulario
    Private Sub EnlazaDatosAccesoriosDeInstalacion()
        'Cargamos los el arbol del Almacen y las realaciones previamente guardadas
        Dim ds As New DataSet

        Try
            ds = DAEquipoExtra.uspConsultaArbolMaterial()
            ds.Tables(0).TableName = "tbl_clasificacionArticulos"
            ds.Tables(1).TableName = "tbl_tipoArticulos"
            ds.Tables(2).TableName = "tbl_articulos"
            ds.Tables(3).TableName = "tbl_AccesoriosDeInstalacion"
            ds.Tables(4).TableName = "tblTiposDeCajas"
            ds.Tables(5).TableName = "tblRelArticuloAccesorio"

            'AGREGAMOS LA RELACI�N ENTRE LAS DOS TABLAS 
            ds.Relations.Add("FK_Clasificacion", ds.Tables("tbl_clasificacionArticulos").Columns("Clave"), ds.Tables("tbl_tipoArticulos").Columns("Clave_Clasificacion"))
            ds.Relations.Add("FK_TipoArticulo", ds.Tables("tbl_tipoArticulos").Columns("ClaveUnicaX"), ds.Tables("tbl_articulos").Columns("ClaveUnicaX"))

            'CARGAMOS LAS FUNTES DE DATOS
            Dim bsClasificaciones As New BindingSource
            bsClasificaciones.DataSource = ds.Tables("tbl_clasificacionArticulos")
            Dim bsTipoArticulo As New BindingSource(bsClasificaciones, "FK_Clasificacion")

            'Dim bsTipo As New BindingSource
            Dim bsServicios As New BindingSource(bsTipoArticulo, "FK_TipoArticulo")

            'Clasificaciones
            cmbClasificacionArticulos.DisplayMember = "Clasificacion"
            cmbClasificacionArticulos.ValueMember = "Clave"
            cmbClasificacionArticulos.DataSource = bsClasificaciones

            'Tipo de Artpiculos
            cmbClasifMaterial.DisplayMember = "Descripcion"
            cmbClasifMaterial.ValueMember = "idTipo"
            cmbClasifMaterial.DataSource = bsTipoArticulo

            'Art�culos
            cmbDescArticulo.DisplayMember = "Descripcion"
            cmbDescArticulo.ValueMember = "idArticulo"
            cmbDescArticulo.DataSource = bsServicios

            'Tipos de Cajas
            cmbTipoDeCaja.DisplayMember = "Descripcion"
            cmbTipoDeCaja.ValueMember = "idCaja"
            cmbTipoDeCaja.DataSource = ds.Tables("tblTiposDeCajas")

            'Accesorios de Instalaci�n
            cmbAccesorioInst.DisplayMember = "Descripcion"
            cmbAccesorioInst.ValueMember = "idAccesorio"
            cmbAccesorioInst.DataSource = ds.Tables("tbl_AccesoriosDeInstalacion")

            'Cargamos las Relaciones
            dgvRelArticuloAccesorioInst.DataSource = ds.Tables("tblRelArticuloAccesorio")

            If Not (dgvRelArticuloAccesorioInst.RowCount > 0) Then
                btnEliminar.Enabled = False
            Else
                btnEliminar.Enabled = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

#End Region

#Region "Cobro Parcial de Material"
    Private Sub btnAgregarRango_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregarRango.Click
        If Len(Me.txtRango1.Text) = 0 Or Len(Me.txtRango2.Text) = 0 Then
            MsgBox("El Rango Inicial o Final no son v�lidos")
            Exit Sub
        End If

        If (CDec(Me.txtRango1.Text) > CDec(Me.txtRango1.Text)) Then
            MsgBox("El Rango Inicial no puede ser mayor que el rango final.")
            Exit Sub
        End If

        If Not (ValidaCamposCobroMaterial()) Then
            MsgBox("El Rango Inicial o Final no son v�lidos")
            Exit Sub
        End If

        If (DACobroDeMaterial.spAgregaRangosCobroMaterial(Me.txtRango1.Text, Me.txtRango2.Text, Me.nudPagos.Value)) >= 0 Then
            MsgBox("Rango Almacenado Satisfactoriamente", MsgBoxStyle.Information)
            MuestraRangosCobroMaterial()
            LimpiaCamposCobroMaterial()
        Else
            MsgBox("El rango ya se encuentra en la lista", MsgBoxStyle.Information)
            Exit Sub
        End If
    End Sub

    Private Sub btnEliminarRango_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminarRango.Click
        If (Me.dgvRangosMaterial.SelectedRows.Count > 0) Then
            If (MsgBox("�Deseas eliminar este rango?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes) Then
                DACobroDeMaterial.spEliminaRangosCobroMaterial(Me.dgvRangosMaterial.CurrentRow.Cells("identity").Value)
                MsgBox("Rango Eliminado Satisfactoriamente", MsgBoxStyle.Information)
                MuestraRangosCobroMaterial()
                LimpiaCamposCobroMaterial()
            End If
        End If
    End Sub

    Private Function ValidaCamposCobroMaterial() As Boolean
        ValidaCamposCobroMaterial = True

        If Not (IsNumeric(Me.txtRango1.Text)) Then
            ValidaCamposCobroMaterial = False
        End If
        If Not (IsNumeric(Me.txtRango2.Text)) Then
            ValidaCamposCobroMaterial = False
        End If
    End Function

    Private Sub MuestraRangosCobroMaterial()
        Dim dt As DataTable
        dt = DACobroDeMaterial.spConsultaRangosCobroMaterial()
        Me.dgvRangosMaterial.DataSource = dt
    End Sub

    Private Sub LimpiaCamposCobroMaterial()
        Me.txtRango1.Text = "0.0"
        Me.txtRango2.Text = "0.0"

        If (Me.dgvRangosMaterial.RowCount > 0) Then
            Me.btnEliminar.Enabled = True
        Else
            Me.btnEliminar.Enabled = False
        End If
    End Sub
#End Region


    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If cmbCompanias.SelectedValue = 0 Then
            MsgBox("Seleccione una compa��a")
            Exit Sub
        End If
        If Len(txtSerieG.Text) = 0 Or Len(txtSerieF.Text) = 0 Then
            MsgBox("Insertar Series Validas")
            Exit Sub
        End If
        If txtSerieF.Text = txtSerieG.Text Then
            MsgBox("Las series de Factura Fiscal y Factura Global deben de ser diferentes")
        End If
        If IsNumeric(txtUltFolioG.Text) And IsNumeric(txtUltFolioF.Text) Then
            GuardaFac()
        Else
            MsgBox("Insertar N�mero de Folios Validos")
            Exit Sub
        End If
    End Sub
    Private Sub GuardaFac()
        Dim activo As Integer
        If cbxMizar.Checked = True Then
            activo = 1
        Else
            activo = 0
        End If
        Dim conexion As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("uspInsertatblFacGeneral", conexion)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Activo", SqlDbType.Bit)
        par1.Direction = ParameterDirection.Input
        par1.Value = activo
        cmd.Parameters.Add(par1)
        Dim par2 As New SqlParameter("@SerieG", SqlDbType.VarChar, 50)
        par2.Direction = ParameterDirection.Input
        par2.Value = txtSerieG.Text
        cmd.Parameters.Add(par2)
        Dim par3 As New SqlParameter("@UltimoFolioG", SqlDbType.Int)
        par3.Direction = ParameterDirection.Input
        par3.Value = txtUltFolioG.Text
        cmd.Parameters.Add(par3)
        Dim par4 As New SqlParameter("@SerieF", SqlDbType.VarChar, 50)
        par4.Direction = ParameterDirection.Input
        par4.Value = txtSerieF.Text
        cmd.Parameters.Add(par4)
        Dim par5 As New SqlParameter("@UltimoFolioF", SqlDbType.Int)
        par5.Direction = ParameterDirection.Input
        par5.Value = txtUltFolioF.Text
        cmd.Parameters.Add(par5)
        Dim par6 As New SqlParameter("@clvCompania", SqlDbType.BigInt)
        par6.Direction = ParameterDirection.Input
        par6.Value = cmbCompanias.SelectedValue
        cmd.Parameters.Add(par6)
        Dim par7 As New SqlParameter("@bndGuardo", SqlDbType.Bit)
        par7.Direction = ParameterDirection.Output
        par7.Value = 0
        cmd.Parameters.Add(par7)
        Dim par8 As New SqlParameter("@Msj", SqlDbType.VarChar, 250)
        par8.Direction = ParameterDirection.Output
        par8.Value = ""
        cmd.Parameters.Add(par8)
        Try
            conexion.Open()
            cmd.ExecuteNonQuery()
            eRes = par7.Value
            eMsj = par8.Value
            If eRes = 0 Then
                MsgBox(eMsj)
            Else
                MsgBox("Se guardo con �xito!")
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub cmbCompanias_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCompanias.SelectedIndexChanged
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clvCompania", SqlDbType.BigInt, cmbCompanias.SelectedValue)
        Dim Dtable As New DataTable
        Dim DRows As DataRow
        Try
            Dtable = BaseII.ConsultaDT("uspConsultatblFacGeneral")
            For Each DRows In Dtable.Rows
                txtSerieG.Text = DRows(0).ToString()
                txtUltFolioG.Text = DRows(1).ToString()
                txtSerieF.Text = DRows(2).ToString()
                txtUltFolioF.Text = DRows(3).ToString()
                If cmbCompanias.SelectedValue <> 0 Then
                    If DRows(4) = True Then
                        cbxMizar.Checked = True
                        cbxMizarS.Checked = False
                    Else
                        cbxMizar.Checked = False
                        cbxMizarS.Checked = True
                    End If
                End If
            Next
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub cbxMizarS_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxMizarS.CheckedChanged
        If cbxMizarS.Checked = True Then
            cbxMizar.Checked = False
            If cmbCompanias.SelectedValue <> 0 Then
                DesHabilitaFacturas()
            End If
        Else
            cbxMizar.Checked = True
            If cmbCompanias.SelectedValue <> 0 Then
                HabilitaFacturas()
            End If
        End If
    End Sub

    Private Sub cbxMizar_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxMizar.CheckedChanged
        If cbxMizar.Checked = True Then
            cbxMizarS.Checked = False
            If cmbCompanias.SelectedValue <> 0 Then
                HabilitaFacturas()
            End If
        Else
            cbxMizarS.Checked = True
            If cmbCompanias.SelectedValue <> 0 Then
                DesHabilitaFacturas()
            End If
        End If
    End Sub
    Private Sub HabilitaFacturas()
        'cmbCompanias.Enabled = True
        txtUltFolioF.Enabled = True
        txtUltFolioG.Enabled = True
        txtSerieF.Enabled = True
        txtSerieG.Enabled = True
    End Sub
    Private Sub DesHabilitaFacturas()
        'cmbCompanias.Enabled = False
        txtUltFolioF.Enabled = False
        txtUltFolioG.Enabled = False
        txtSerieF.Enabled = False
        txtSerieG.Enabled = False
    End Sub


    Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button13.Click
        UspComisionOxxo(CDec(Me.txtComisionOxxo.Text), 2)
        UspPrefijoOxxo(Me.txtPrefijo.Text, 2)
    End Sub

    Private Sub UspComisionOxxo(ByVal prmcomision As Decimal, ByVal prmOP As Integer)
        Dim DT As New DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@comision", SqlDbType.Money, prmcomision)
        BaseII.CreateMyParameter("@op", SqlDbType.Int, prmOP)
        If prmOP = 1 Then
            DT = BaseII.ConsultaDT("UspComisionOxxo")
            Me.txtComisionOxxo.Text = DT.Rows(0)(0).ToString
        ElseIf prmOP = 2 Then
            BaseII.Inserta("UspComisionOxxo")

        End If

    End Sub
    Private Sub UspPrefijoOxxo(ByVal prmPrefijo As String, ByVal prmOP As Integer)
        Dim DT As New DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@PREFIJO", SqlDbType.VarChar, prmPrefijo, 2)
        BaseII.CreateMyParameter("@op", SqlDbType.Int, prmOP)
        If prmOP = 1 Then
            DT = BaseII.ConsultaDT("UspPrefijoOxxo")
            Me.txtPrefijo.Text = DT.Rows(0)(0).ToString
        ElseIf prmOP = 2 Then
            BaseII.Inserta("UspPrefijoOxxo")
            MsgBox("Fue Guardado con exito")
        End If

    End Sub
    Function SoloNumeros(ByVal Keyascii As Short) As Short
        If InStr("1234567890.", Chr(Keyascii)) = 0 Then
            SoloNumeros = 0
        Else
            SoloNumeros = Keyascii
        End If
        Select Case Keyascii
            Case 8
                SoloNumeros = Keyascii
            Case 13
                SoloNumeros = Keyascii
        End Select
    End Function

    Private Sub txtPrefijo_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPrefijo.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtComisionOxxo_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtComisionOxxo.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
    End Sub

#Region "IMPRESIONES AUTOM�TICAS ORDENES QUEJAS"
    Private Sub uspInsertaTblImpresionOrdenQueja(ByVal prmImprimeOrdenSoftv As Boolean, ByVal prmImprimeOrdenFac As Boolean, ByVal prmImprimeQuejasSoftv As Boolean)
        Dim ordenQueja As New classGenerales

        ordenQueja.imprimeOrdenesSoftv = prmImprimeOrdenSoftv
        ordenQueja.imprimeOrdenesFac = prmImprimeOrdenFac
        ordenQueja.imprimeQuejasSoftv = prmImprimeQuejasSoftv
        ordenQueja.uspInsertaTblImpresionOrdenQueja()
    End Sub

    Private Sub uspConsultaTblImpresionOrdenQueja()
        Dim ordenQueja As New classGenerales

        ordenQueja.uspConsultaTblImpresionOrdenQueja()
        Me.cbxImprimeOrdenesSoftv.Checked = ordenQueja.imprimeOrdenesSoftv
        Me.cbxImprimeOrdenesFac.Checked = ordenQueja.imprimeOrdenesFac
        Me.cbxImprimeQuejasSoftv.Checked = ordenQueja.imprimeQuejasSoftv
    End Sub
#End Region

    Private Sub Button22_Click(sender As System.Object, e As System.EventArgs) Handles Button22.Click
        If TextBoxFecCorteSatv.Text.Length > 0 And IsNumeric(TextBoxDiasAvisoCorte.Text) And TextBoxMensajeAvisoCorte.Text.Length > 0 And TextBoxMensajeCorte.Text.Length > 0 Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@FechaCorteSistema", SqlDbType.Date, TextBoxFecCorteSatv.Text)
            BaseII.CreateMyParameter("@DiasAviso", SqlDbType.SmallInt, TextBoxDiasAvisoCorte.Text)
            BaseII.CreateMyParameter("@MensajeAviso", SqlDbType.VarChar, TextBoxMensajeAvisoCorte.Text, 800)
            BaseII.CreateMyParameter("@MensajeCorte", SqlDbType.VarChar, TextBoxMensajeCorte.Text, 800)
            BaseII.CreateMyParameter("@Activo", SqlDbType.Bit, CheckBoxCorteSATV.Checked)
            BaseII.Inserta("Sp_GuardarCorteSistemaSatv")
            MsgBox("Datos guardados correctamente.")
        Else
            MsgBox("Faltan datos por completar")
        End If
    End Sub

    Private Sub dameDatosCorteSATV()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@FechaCorteSistema", ParameterDirection.Output, SqlDbType.Date)
        BaseII.CreateMyParameter("@DiasAviso", ParameterDirection.Output, SqlDbType.SmallInt)
        BaseII.CreateMyParameter("@MensajeAviso", ParameterDirection.Output, SqlDbType.VarChar, 800)
        BaseII.CreateMyParameter("@MensajeCorte", ParameterDirection.Output, SqlDbType.VarChar, 800)
        BaseII.CreateMyParameter("@Activo", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.ProcedimientoOutPut("Sp_DameCorteSistemaSatv")
        TextBoxFecCorteSatv.Text = BaseII.dicoPar("@FechaCorteSistema").ToString()
        TextBoxDiasAvisoCorte.Text = BaseII.dicoPar("@DiasAviso").ToString()
        TextBoxMensajeAvisoCorte.Text = BaseII.dicoPar("@MensajeAviso").ToString()
        TextBoxMensajeCorte.Text = BaseII.dicoPar("@MensajeCorte").ToString()
        CheckBoxCorteSATV.Checked = CBool(BaseII.dicoPar("@Activo").ToString())

    End Sub
End Class