﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPuntosImporte
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.bnGuardar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgPuntos = New System.Windows.Forms.DataGridView()
        Me.Id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Puntos = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Importe = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tbPuntos = New System.Windows.Forms.TextBox()
        Me.tbPuntosAdicionales = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.tbImporte = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.bnAgregar = New System.Windows.Forms.Button()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.bnEliminar = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.tbImporteFacturas = New System.Windows.Forms.TextBox()
        Me.tbPuntosFactura = New System.Windows.Forms.TextBox()
        Me.bnGuardarFacturas = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        CType(Me.dgPuntos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'bnGuardar
        '
        Me.bnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnGuardar.Location = New System.Drawing.Point(111, 541)
        Me.bnGuardar.Name = "bnGuardar"
        Me.bnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.bnGuardar.TabIndex = 5
        Me.bnGuardar.Text = "&Guardar"
        Me.bnGuardar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(180, 34)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 15)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Puntos :"
        '
        'dgPuntos
        '
        Me.dgPuntos.AllowUserToAddRows = False
        Me.dgPuntos.AllowUserToDeleteRows = False
        Me.dgPuntos.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgPuntos.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgPuntos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgPuntos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Id, Me.Puntos, Me.Importe})
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgPuntos.DefaultCellStyle = DataGridViewCellStyle6
        Me.dgPuntos.Location = New System.Drawing.Point(95, 94)
        Me.dgPuntos.Name = "dgPuntos"
        Me.dgPuntos.ReadOnly = True
        Me.dgPuntos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgPuntos.Size = New System.Drawing.Size(371, 405)
        Me.dgPuntos.TabIndex = 2
        '
        'Id
        '
        Me.Id.DataPropertyName = "Id"
        Me.Id.HeaderText = "Id"
        Me.Id.Name = "Id"
        Me.Id.ReadOnly = True
        Me.Id.Visible = False
        '
        'Puntos
        '
        Me.Puntos.DataPropertyName = "Puntos"
        Me.Puntos.HeaderText = "Puntos"
        Me.Puntos.Name = "Puntos"
        Me.Puntos.ReadOnly = True
        Me.Puntos.Width = 150
        '
        'Importe
        '
        Me.Importe.DataPropertyName = "Importe"
        DataGridViewCellStyle5.Format = "C2"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.Importe.DefaultCellStyle = DataGridViewCellStyle5
        Me.Importe.HeaderText = "Importe"
        Me.Importe.Name = "Importe"
        Me.Importe.ReadOnly = True
        Me.Importe.Width = 150
        '
        'tbPuntos
        '
        Me.tbPuntos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbPuntos.Location = New System.Drawing.Point(244, 28)
        Me.tbPuntos.Name = "tbPuntos"
        Me.tbPuntos.Size = New System.Drawing.Size(152, 21)
        Me.tbPuntos.TabIndex = 0
        '
        'tbPuntosAdicionales
        '
        Me.tbPuntosAdicionales.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbPuntosAdicionales.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbPuntosAdicionales.Location = New System.Drawing.Point(298, 517)
        Me.tbPuntosAdicionales.Name = "tbPuntosAdicionales"
        Me.tbPuntosAdicionales.Size = New System.Drawing.Size(98, 21)
        Me.tbPuntosAdicionales.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(108, 523)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(184, 15)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Importe por punto adicional"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tbImporte
        '
        Me.tbImporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbImporte.Location = New System.Drawing.Point(244, 55)
        Me.tbImporte.Name = "tbImporte"
        Me.tbImporte.Size = New System.Drawing.Size(152, 21)
        Me.tbImporte.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(174, 61)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(64, 15)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Importe :"
        '
        'bnAgregar
        '
        Me.bnAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnAgregar.Location = New System.Drawing.Point(501, 44)
        Me.bnAgregar.Name = "bnAgregar"
        Me.bnAgregar.Size = New System.Drawing.Size(84, 32)
        Me.bnAgregar.TabIndex = 2
        Me.bnAgregar.Text = "&Agregar"
        Me.bnAgregar.UseVisualStyleBackColor = True
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(472, 694)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(136, 36)
        Me.bnSalir.TabIndex = 6
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'bnEliminar
        '
        Me.bnEliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnEliminar.Location = New System.Drawing.Point(501, 94)
        Me.bnEliminar.Name = "bnEliminar"
        Me.bnEliminar.Size = New System.Drawing.Size(84, 32)
        Me.bnEliminar.TabIndex = 3
        Me.bnEliminar.Text = "&Eliminar"
        Me.bnEliminar.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(108, 589)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(64, 15)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Por cada"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(282, 589)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(241, 15)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "pesos en conceptos de Mensualidad"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(106, 616)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(186, 15)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = " y Reconexión, se otorgarán"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(402, 616)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(54, 15)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "puntos."
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tbImporteFacturas
        '
        Me.tbImporteFacturas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbImporteFacturas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbImporteFacturas.Location = New System.Drawing.Point(178, 583)
        Me.tbImporteFacturas.Name = "tbImporteFacturas"
        Me.tbImporteFacturas.Size = New System.Drawing.Size(98, 21)
        Me.tbImporteFacturas.TabIndex = 12
        '
        'tbPuntosFactura
        '
        Me.tbPuntosFactura.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbPuntosFactura.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbPuntosFactura.Location = New System.Drawing.Point(298, 610)
        Me.tbPuntosFactura.Name = "tbPuntosFactura"
        Me.tbPuntosFactura.Size = New System.Drawing.Size(98, 21)
        Me.tbPuntosFactura.TabIndex = 13
        '
        'bnGuardarFacturas
        '
        Me.bnGuardarFacturas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnGuardarFacturas.Location = New System.Drawing.Point(111, 634)
        Me.bnGuardarFacturas.Name = "bnGuardarFacturas"
        Me.bnGuardarFacturas.Size = New System.Drawing.Size(75, 23)
        Me.bnGuardarFacturas.TabIndex = 14
        Me.bnGuardarFacturas.Text = "&Guardar"
        Me.bnGuardarFacturas.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(402, 523)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(49, 15)
        Me.Label8.TabIndex = 15
        Me.Label8.Text = "pesos."
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'FrmPuntosImporte
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(613, 742)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.bnGuardarFacturas)
        Me.Controls.Add(Me.tbPuntosFactura)
        Me.Controls.Add(Me.tbImporteFacturas)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.bnEliminar)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.bnAgregar)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.tbImporte)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.tbPuntosAdicionales)
        Me.Controls.Add(Me.tbPuntos)
        Me.Controls.Add(Me.dgPuntos)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.bnGuardar)
        Me.Name = "FrmPuntosImporte"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Incentivos a Técnicos"
        CType(Me.dgPuntos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bnGuardar As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dgPuntos As System.Windows.Forms.DataGridView
    Friend WithEvents Id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Puntos As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Importe As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tbPuntos As System.Windows.Forms.TextBox
    Friend WithEvents tbPuntosAdicionales As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tbImporte As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents bnAgregar As System.Windows.Forms.Button
    Friend WithEvents bnSalir As System.Windows.Forms.Button
    Friend WithEvents bnEliminar As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents tbImporteFacturas As System.Windows.Forms.TextBox
    Friend WithEvents tbPuntosFactura As System.Windows.Forms.TextBox
    Friend WithEvents bnGuardarFacturas As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
End Class
