Imports System.Data.SqlClient
Public Class FrmTvAdicionales

    Private Sub FrmTvAdicionales_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        CLITVSINPAGO = 0
        CLITVCONPAGO = 0
    End Sub

    Private Sub CMBBtnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBBtnAceptar.Click
        CLITVSINPAGO = Me.NumericUpDown1.Value
        CLITVCONPAGO = Me.NumericUpDown2.Value
        If IsNumeric(LocContratoLog) = True Then
            Guarda_TvAdicionales(LocContratoLog, 0, 0)
        End If
        Me.Close()
    End Sub

    Private Sub Guarda_TvAdicionales(ByVal contrato As Long, ByVal TvSinpago As Integer, ByVal TvConPAgo As Integer)
        Try
            Dim CON78 As New SqlClient.SqlConnection(MiConexion)
            Dim cmd As New SqlClient.SqlCommand()
            CON78.Open()
            With cmd
                .CommandText = "Guarda_TvAdicionales"
                .Connection = CON78
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                '@contrato bigint,@clv_combo bigint,@Tipo_Cablemodem int

                Dim Prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                Prm.Direction = ParameterDirection.Input
                Prm.Value = contrato
                .Parameters.Add(Prm)


                Dim prm3 As New SqlParameter("@TvSinPago", SqlDbType.Int)
                prm3.Direction = ParameterDirection.Input
                prm3.Value = TvSinpago
                .Parameters.Add(prm3)

                Dim prm4 As New SqlParameter("@TvConPago", SqlDbType.Int)
                prm4.Direction = ParameterDirection.Input
                prm4.Value = TvConPAgo
                .Parameters.Add(prm4)

                Dim i As Integer = cmd.ExecuteNonQuery()
            End With
            CON78.Close()


        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBLabel1.Click

    End Sub
End Class