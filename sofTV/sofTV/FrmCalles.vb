Imports System.Data.SqlClient
Public Class FrmCalles
    Private locclv_colonia As Integer = 0
    Private clvcoloniatxt As String = Nothing

    Private nomcalle As String = Nothing
    Private bnd1 As Boolean = False
    Private Sub damedatosbitacora()
        Try
            If opcion = "M" Then
                nomcalle = Me.NOMBRETextBox.Text
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub guardabitacora(ByVal op As Integer)
        Try
            Select Case op
                Case 0
                    If opcion = "N" Then
                        If bnd1 = False Then
                            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se guardo un nueva nueva calle", "", "Se guardo un nueva nueva calle: " + Me.NOMBRETextBox.Text, LocClv_Ciudad)
                            bnd1 = True
                        End If
                    ElseIf opcion = "M" Then
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.NOMBRETextBox.Name, nomcalle, Me.NOMBRETextBox.Text, LocClv_Ciudad)
                    End If
                Case 1
                    'clvcoloniatxt
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Agrego Una Nueva Colonia A La Calle", "Colonia: " + Me.ComboBox1.Text, "Se Agrego Una Nueva Colonia A La Calle: " + Me.NOMBRETextBox.Text, LocClv_Ciudad)
                Case 2
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Elimino Una Colonia A La Calle", "Colonia: " + clvcoloniatxt, "Se Elimino Una Colonia A La Calle: " + Me.NOMBRETextBox.Text, LocClv_Ciudad)
            End Select
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub guardarcalles()


    End Sub

    Private Sub mostrardatos()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.CONCALLESTableAdapter.Connection = CON
            Me.CONCALLESTableAdapter.Fill(Me.NewSofTvDataSet.CONCALLES, New System.Nullable(Of Integer)(CType(GloClv_Calle, Integer)))
            damedatosbitacora()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Public Sub CREAARBOL()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Dim I As Integer = 0
            Dim X As Integer = 0
            Me.DAMECOLONIA_CALLETableAdapter.Connection = CON
            Me.DAMECOLONIA_CALLETableAdapter.Fill(Me.NewSofTvDataSet.DAMECOLONIA_CALLE, New System.Nullable(Of Integer)(CType(GloClv_Calle, Integer)))
            Dim FilaRow As DataRow
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.DAMECOLONIA_CALLE.Rows
                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                If IsNumeric(Trim(FilaRow("Clv_colonia").ToString())) = True Then
                    Me.TreeView1.Nodes.Add(Trim(FilaRow("Clv_colonia").ToString()), Trim(FilaRow("Colonia").ToString()))
                    Me.TreeView1.Nodes(I).Tag = Trim(FilaRow("Clv_colonia").ToString())
                    I += 1
                End If
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub FrmCalles_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        Try
            '
            CON.Open()
            colorea(Me, Me.Name)
            'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MUECOLONIAS' Puede moverla o quitarla seg�n sea necesario.
            'Me.MUECOLONIASTableAdapter.Connection = CON
            'Me.MUECOLONIASTableAdapter.Fill(Me.NewSofTvDataSet.MUECOLONIAS)
            With ComboBox1
                .DataSource = sofTV.BAL.Colonia.GetAll()
                .DisplayMember = "Nombre"
                .ValueMember = "clv_colonia"
            End With
            'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MUESTRACALLES' Puede moverla o quitarla seg�n sea necesario.
            Me.MUESTRACALLESTableAdapter.Connection = CON
            Me.MUESTRACALLESTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACALLES)
            Me.Panel1.Enabled = True
            If opcion = "N" Then
                Me.CONCALLESBindingSource.AddNew()

            ElseIf opcion = "C" Then
                Me.mostrardatos()
                Me.Panel1.Enabled = False
                Me.Panel2.Enabled = False
                Me.Panel1.Enabled = False
                Me.CREAARBOL()
            ElseIf opcion = "M" Then
                Me.mostrardatos()
                Me.CREAARBOL()
            End If
            CON.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
            CON.CLOSE()
        End Try
    End Sub

    Private Sub ToolStripLabel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripLabel1.Click
        Me.CONCALLESBindingSource.CancelEdit()
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub



    Private Sub CONCALLESBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONCALLESBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.CONCALLESBindingSource.EndEdit()
        Me.CONCALLESTableAdapter.Connection = CON
        Me.CONCALLESTableAdapter.Update(Me.NewSofTvDataSet.CONCALLES)
        guardabitacora(0)
        MsgBox("Se Guardo con Ex�to", MsgBoxStyle.Information)
        GloBnd = True
        CON.Close()
        Me.Close()
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim bnd As Boolean = True
        Try
            Me.Validate()
            Me.CONCALLESBindingSource.EndEdit()
            Me.CONCALLESTableAdapter.Connection = CON
            Me.CONCALLESTableAdapter.Update(Me.NewSofTvDataSet.CONCALLES)
            guardabitacora(0)
            '--MsgBox("Se Guardo con Ex�to", MsgBoxStyle.Information)
            Me.CONCVECAROLTableAdapter.Connection = CON
            Me.CONCVECAROLTableAdapter.Insert(GloClv_Calle, Me.ComboBox1.SelectedValue, 0, 0, 0, "", "")
            Me.Validate()
            Me.CONCVECAROLBindingSource.EndEdit()
            Me.CONCVECAROLTableAdapter.Connection = CON
            Me.CONCVECAROLTableAdapter.Update(Me.NewSofTvDataSet.CONCVECAROL)
            If bnd = True Then
                CREAARBOL()
            End If
            guardabitacora(1)
        Catch ex As System.Exception

            bnd = False
            Me.CONCVECAROLBindingSource.CancelEdit()
            MsgBox("La Colonia Ya Ex�ste en la Lista")
            '--System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If locclv_colonia > 0 Then
            If uspChecaColoniaCalleAsignada(GloClv_Calle, Me.locclv_colonia) Then
                MsgBox("La Colonia se encuentra actualmente asignada por lo cu�l no se podr� eliminar de la lista", MsgBoxStyle.Information)
                Exit Sub
            End If

            Me.CONCVECAROLTableAdapter.Connection = CON
            Me.CONCVECAROLTableAdapter.Delete(GloClv_Calle, Me.locclv_colonia)
            Me.Validate()
            Me.CONCVECAROLBindingSource.EndEdit()
            Me.CONCVECAROLTableAdapter.Connection = CON
            Me.CONCVECAROLTableAdapter.Update(Me.NewSofTvDataSet.CONCVECAROL)
            Me.CREAARBOL()
            guardabitacora(2)
            locclv_colonia = 0
        Else
            MsgBox("Seleccione lo que desea Quitar de la Lista", MsgBoxStyle.Information)
        End If
        CON.Close()

    End Sub


    Private Sub TreeView1_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView1.AfterSelect
        locclv_colonia = e.Node.Tag
        clvcoloniatxt = e.Node.Text
    End Sub

    Private Sub Clv_CalleTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_CalleTextBox.TextChanged
        GloClv_Calle = Me.Clv_CalleTextBox.Text
    End Sub

    Private Function uspChecaColoniaCalleAsignada(ByVal prmClvCalle As Integer, ByVal prmClvColonia As Integer) As Boolean
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clvCalle", SqlDbType.Int, prmClvCalle)
            BaseII.CreateMyParameter("@clvColonia", SqlDbType.Int, prmClvColonia)
            BaseII.CreateMyParameter("@bndAsignada", ParameterDirection.Output, SqlDbType.Bit)
            BaseII.ProcedimientoOutPut("uspChecaColoniaCalleAsignada")
            uspChecaColoniaCalleAsignada = CBool(BaseII.dicoPar("@bndAsignada").ToString())
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    'uspChecaColoniaCalleAsignada(@clvCalle INT,@clvColonia INT,@bndAsignada BIT)
   
End Class