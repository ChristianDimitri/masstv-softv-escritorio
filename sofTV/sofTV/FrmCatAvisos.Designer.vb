<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCatAvisos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CMBClv_avisoLabel As System.Windows.Forms.Label
        Dim CMBClv_txt_avisoLabel As System.Windows.Forms.Label
        Dim CMBNombreLabel As System.Windows.Forms.Label
        Dim CMBDescripcionLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCatAvisos))
        Me.Con_AvisoBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.Con_AvisoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetarnoldo = New sofTV.DataSetarnoldo()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.Con_AvisoBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Clv_avisoTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_txt_avisoTextBox = New System.Windows.Forms.TextBox()
        Me.NombreTextBox = New System.Windows.Forms.TextBox()
        Me.CMBPanel1 = New System.Windows.Forms.Panel()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Con_AvisoTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Con_AvisoTableAdapter()
        Me.Valida_clv_avisoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Valida_clv_avisoTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Valida_clv_avisoTableAdapter()
        Me.DescripcionTextBox = New System.Windows.Forms.TextBox()
        CMBClv_avisoLabel = New System.Windows.Forms.Label()
        CMBClv_txt_avisoLabel = New System.Windows.Forms.Label()
        CMBNombreLabel = New System.Windows.Forms.Label()
        CMBDescripcionLabel = New System.Windows.Forms.Label()
        CType(Me.Con_AvisoBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Con_AvisoBindingNavigator.SuspendLayout()
        CType(Me.Con_AvisoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CMBPanel1.SuspendLayout()
        CType(Me.Valida_clv_avisoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBClv_avisoLabel
        '
        CMBClv_avisoLabel.AutoSize = True
        CMBClv_avisoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBClv_avisoLabel.Location = New System.Drawing.Point(78, 256)
        CMBClv_avisoLabel.Name = "CMBClv_avisoLabel"
        CMBClv_avisoLabel.Size = New System.Drawing.Size(75, 16)
        CMBClv_avisoLabel.TabIndex = 2
        CMBClv_avisoLabel.Text = "No. Aviso"
        '
        'CMBClv_txt_avisoLabel
        '
        CMBClv_txt_avisoLabel.AutoSize = True
        CMBClv_txt_avisoLabel.BackColor = System.Drawing.Color.Gray
        CMBClv_txt_avisoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBClv_txt_avisoLabel.Location = New System.Drawing.Point(12, 20)
        CMBClv_txt_avisoLabel.Name = "CMBClv_txt_avisoLabel"
        CMBClv_txt_avisoLabel.Size = New System.Drawing.Size(95, 16)
        CMBClv_txt_avisoLabel.TabIndex = 4
        CMBClv_txt_avisoLabel.Text = "Clave Aviso:"
        '
        'CMBNombreLabel
        '
        CMBNombreLabel.AutoSize = True
        CMBNombreLabel.BackColor = System.Drawing.Color.Gray
        CMBNombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBNombreLabel.Location = New System.Drawing.Point(12, 52)
        CMBNombreLabel.Name = "CMBNombreLabel"
        CMBNombreLabel.Size = New System.Drawing.Size(67, 16)
        CMBNombreLabel.TabIndex = 6
        CMBNombreLabel.Text = "Nombre:"
        '
        'CMBDescripcionLabel
        '
        CMBDescripcionLabel.AutoSize = True
        CMBDescripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBDescripcionLabel.Location = New System.Drawing.Point(22, 153)
        CMBDescripcionLabel.Name = "CMBDescripcionLabel"
        CMBDescripcionLabel.Size = New System.Drawing.Size(95, 16)
        CMBDescripcionLabel.TabIndex = 22
        CMBDescripcionLabel.Text = "Descripcion:"
        '
        'Con_AvisoBindingNavigator
        '
        Me.Con_AvisoBindingNavigator.AddNewItem = Nothing
        Me.Con_AvisoBindingNavigator.BindingSource = Me.Con_AvisoBindingSource
        Me.Con_AvisoBindingNavigator.CountItem = Nothing
        Me.Con_AvisoBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.Con_AvisoBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.Con_AvisoBindingNavigatorSaveItem})
        Me.Con_AvisoBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.Con_AvisoBindingNavigator.MoveFirstItem = Nothing
        Me.Con_AvisoBindingNavigator.MoveLastItem = Nothing
        Me.Con_AvisoBindingNavigator.MoveNextItem = Nothing
        Me.Con_AvisoBindingNavigator.MovePreviousItem = Nothing
        Me.Con_AvisoBindingNavigator.Name = "Con_AvisoBindingNavigator"
        Me.Con_AvisoBindingNavigator.PositionItem = Nothing
        Me.Con_AvisoBindingNavigator.Size = New System.Drawing.Size(863, 25)
        Me.Con_AvisoBindingNavigator.TabIndex = 120
        Me.Con_AvisoBindingNavigator.TabStop = True
        Me.Con_AvisoBindingNavigator.Text = "BindingNavigator1"
        '
        'Con_AvisoBindingSource
        '
        Me.Con_AvisoBindingSource.DataMember = "Con_Aviso"
        Me.Con_AvisoBindingSource.DataSource = Me.DataSetarnoldo
        '
        'DataSetarnoldo
        '
        Me.DataSetarnoldo.DataSetName = "DataSetarnoldo"
        Me.DataSetarnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(72, 22)
        Me.BindingNavigatorDeleteItem.Text = "&Eliminar"
        '
        'Con_AvisoBindingNavigatorSaveItem
        '
        Me.Con_AvisoBindingNavigatorSaveItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.Con_AvisoBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Con_AvisoBindingNavigatorSaveItem.Image = CType(resources.GetObject("Con_AvisoBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.Con_AvisoBindingNavigatorSaveItem.Name = "Con_AvisoBindingNavigatorSaveItem"
        Me.Con_AvisoBindingNavigatorSaveItem.Size = New System.Drawing.Size(108, 22)
        Me.Con_AvisoBindingNavigatorSaveItem.Text = "&Guardar datos"
        '
        'Clv_avisoTextBox
        '
        Me.Clv_avisoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Con_AvisoBindingSource, "clv_aviso", True))
        Me.Clv_avisoTextBox.Enabled = False
        Me.Clv_avisoTextBox.Location = New System.Drawing.Point(177, 253)
        Me.Clv_avisoTextBox.Name = "Clv_avisoTextBox"
        Me.Clv_avisoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Clv_avisoTextBox.TabIndex = 3
        Me.Clv_avisoTextBox.TabStop = False
        '
        'Clv_txt_avisoTextBox
        '
        Me.Clv_txt_avisoTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Clv_txt_avisoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Con_AvisoBindingSource, "clv_txt_aviso", True))
        Me.Clv_txt_avisoTextBox.Location = New System.Drawing.Point(111, 17)
        Me.Clv_txt_avisoTextBox.MaxLength = 5
        Me.Clv_txt_avisoTextBox.Name = "Clv_txt_avisoTextBox"
        Me.Clv_txt_avisoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Clv_txt_avisoTextBox.TabIndex = 0
        '
        'NombreTextBox
        '
        Me.NombreTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Con_AvisoBindingSource, "Nombre", True))
        Me.NombreTextBox.Location = New System.Drawing.Point(111, 52)
        Me.NombreTextBox.MaxLength = 300
        Me.NombreTextBox.Name = "NombreTextBox"
        Me.NombreTextBox.Size = New System.Drawing.Size(390, 20)
        Me.NombreTextBox.TabIndex = 100
        '
        'CMBPanel1
        '
        Me.CMBPanel1.Controls.Add(CMBClv_txt_avisoLabel)
        Me.CMBPanel1.Controls.Add(Me.NombreTextBox)
        Me.CMBPanel1.Controls.Add(Me.Clv_txt_avisoTextBox)
        Me.CMBPanel1.Controls.Add(CMBNombreLabel)
        Me.CMBPanel1.Location = New System.Drawing.Point(10, 41)
        Me.CMBPanel1.Name = "CMBPanel1"
        Me.CMBPanel1.Size = New System.Drawing.Size(756, 109)
        Me.CMBPanel1.TabIndex = 10
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Orange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(691, 441)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(136, 36)
        Me.Button2.TabIndex = 130
        Me.Button2.Text = "&SALIR"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Con_AvisoTableAdapter
        '
        Me.Con_AvisoTableAdapter.ClearBeforeFill = True
        '
        'Valida_clv_avisoBindingSource
        '
        Me.Valida_clv_avisoBindingSource.DataMember = "Valida_clv_aviso"
        Me.Valida_clv_avisoBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Valida_clv_avisoTableAdapter
        '
        Me.Valida_clv_avisoTableAdapter.ClearBeforeFill = True
        '
        'DescripcionTextBox
        '
        Me.DescripcionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Con_AvisoBindingSource, "Descripcion", True))
        Me.DescripcionTextBox.Location = New System.Drawing.Point(25, 172)
        Me.DescripcionTextBox.Multiline = True
        Me.DescripcionTextBox.Name = "DescripcionTextBox"
        Me.DescripcionTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DescripcionTextBox.Size = New System.Drawing.Size(518, 239)
        Me.DescripcionTextBox.TabIndex = 110
        '
        'FrmCatAvisos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(863, 492)
        Me.Controls.Add(Me.DescripcionTextBox)
        Me.Controls.Add(CMBDescripcionLabel)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.CMBPanel1)
        Me.Controls.Add(Me.Con_AvisoBindingNavigator)
        Me.Controls.Add(CMBClv_avisoLabel)
        Me.Controls.Add(Me.Clv_avisoTextBox)
        Me.Name = "FrmCatAvisos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Avisos"
        CType(Me.Con_AvisoBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Con_AvisoBindingNavigator.ResumeLayout(False)
        Me.Con_AvisoBindingNavigator.PerformLayout()
        CType(Me.Con_AvisoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CMBPanel1.ResumeLayout(False)
        Me.CMBPanel1.PerformLayout()
        CType(Me.Valida_clv_avisoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataSetarnoldo As sofTV.DataSetarnoldo
    Friend WithEvents Con_AvisoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Con_AvisoTableAdapter As sofTV.DataSetarnoldoTableAdapters.Con_AvisoTableAdapter
    Friend WithEvents Con_AvisoBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Con_AvisoBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Clv_avisoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_txt_avisoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NombreTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CMBPanel1 As System.Windows.Forms.Panel
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Valida_clv_avisoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Valida_clv_avisoTableAdapter As sofTV.DataSetarnoldoTableAdapters.Valida_clv_avisoTableAdapter
    Friend WithEvents DescripcionTextBox As System.Windows.Forms.TextBox
End Class
