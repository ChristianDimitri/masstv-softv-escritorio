Imports System.Data.SqlClient

Public Class FrmPolizaDiario

    Private Sub FrmPolizaDiario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.Clv_TipSerTextBox.Text = 0
        BuscaServicio(0)
        Me.TabControl1.TabPages.Remove(Me.TabPage2)
        Me.TabControl1.TabPages.Remove(Me.TabPage4)
    End Sub

    Private Sub Consultar_Tabla_CategoriaServiciosBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Consultar_Tabla_CategoriaServiciosBindingNavigatorSaveItem.Click
        Dim Con As New SqlConnection(MiConexion)
        Dim I As Integer = Me.TabControl1.SelectedIndex
        'Select Case I
        '    Case 0
        '        Me.Clv_TipSerTextBox.Text = 0
        '    Case 1
        '        Me.Clv_TipSerTextBox.Text = 1
        '    Case 2
        '        Me.Clv_TipSerTextBox.Text = 3
        '    Case 3
        '        Me.Clv_TipSerTextBox.Text = 2
        '    Case 4
        '        Me.Clv_TipSerTextBox.Text = 9999
        'End Select
        Select Case Me.TabControl1.SelectedTab.Text.Trim()
            Case "Conceptos Fijos"

                Me.Clv_TipSerTextBox.Text = 0
            Case "Servicio Basico Tv"

                Me.Clv_TipSerTextBox.Text = 1
            Case "Canales Premium o Digital"

                Me.Clv_TipSerTextBox.Text = 3
            Case "Internet"

                Me.Clv_TipSerTextBox.Text = 2
            Case "Material"

                Me.Clv_TipSerTextBox.Text = 9999
        End Select
        Con.Open()
        Me.Validate()
        Me.Consultar_Tabla_CategoriaServiciosBindingSource.EndEdit()
        Me.Consultar_Tabla_CategoriaServiciosTableAdapter.Connection = Con
        Me.Consultar_Tabla_CategoriaServiciosTableAdapter.Update(Me.DataSetEdgarRev2.Consultar_Tabla_CategoriaServicios)
        Con.Close()
    End Sub

    Private Sub BuscaServicio(ByVal Clv_tipSer As Long)
        Try
            Dim Con As New SqlConnection(MiConexion)
            Con.Open()
            Me.Consultar_Tabla_CategoriaServiciosTableAdapter.Connection = Con
            Me.Consultar_Tabla_CategoriaServiciosTableAdapter.Fill(Me.DataSetEdgarRev2.Consultar_Tabla_CategoriaServicios, Clv_tipSer, 0)
            Con.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub TabControl1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabControl1.SelectedIndexChanged
        Dim I As Integer = Me.TabControl1.SelectedIndex

        Select Case Me.TabControl1.SelectedTab.Text.Trim()
            Case "Conceptos Fijos"
                Me.BuscaServicio(0)
                Me.Clv_TipSerTextBox.Text = 0
            Case "Servicio Basico Tv"
                Me.BuscaServicio(1)
                Me.Clv_TipSerTextBox.Text = 1
            Case "Canales Premium o Digital"
                Me.BuscaServicio(3)
                Me.Clv_TipSerTextBox.Text = 3
            Case "Internet"
                Me.BuscaServicio(2)
                Me.Clv_TipSerTextBox.Text = 2
            Case "Material"
                Me.BuscaServicio(9999)
                Me.Clv_TipSerTextBox.Text = 9999
        End Select
    End Sub




    Private Sub TabControl1_TabIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabControl1.TabIndexChanged

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub BindingNavigatorAddNewItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorAddNewItem.Click

    End Sub

    Private Sub Clv_TipSerTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_TipSerTextBox.TextChanged

    End Sub
End Class