Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Text
Public Class FrmImprimirFac
    Private customersByCityReport As ReportDocument

    Private op As String = Nothing
    Private Titulo As String = Nothing
    'Private Const PARAMETER_FIELD_NAME As String = "Op"    

    Private Sub ConfigureCrystalReports(ByVal Clv_Factura As Long)

        Dim rDocument As New CrystalDecisions.CrystalReports.Engine.ReportDocument

        Dim dSet As New DataSet
        Dim ruta As String
        dSet = ReportesFacturas(Clv_Factura)
        Select Case GloCity
            Case "MASSTVMIGUELAL"
                ruta = RutaReportes + "\ReporteCajasTickets.rpt"
            Case "MASSTVREYNOSA"
                ruta = RutaReportes + "\ReporteCajasTickets.rpt"
                'Case "MASSTVREYNOSA"
                '    'ruta = RutaReportes + "\ReporteCajasTickets_2(AG).rpt"
                '    ruta = RutaReportes + "\Report1.rpt"
            Case Else
                ruta = RutaReportes + "\ReporteCajasTickets.rpt"
        End Select
        'ruta = RutaReportes + "\ReporteCajasTickets.rpt"

        rDocument.Load(ruta)
        rDocument.SetDataSource(dSet)

        'Select GloCity
        '    Case "MASSTVMIGUELAL"
        '        rDocument.SetDataSource(dSet)
        '    Case "MASSTVREYNOSA"
        '        SetDBReport(dSet, rDocument)
        ''@Clv_Factura 
        'rDocument.SetParameterValue(0, Clv_Factura)
        ''@Clv_Factura_Ini
        'rDocument.SetParameterValue(1, "0")
        ''@Clv_Factura_Fin
        'rDocument.SetParameterValue(2, "0")
        ''@Fecha_Ini
        'rDocument.SetParameterValue(3, "01/01/1900")
        ''@Fecha_Fin
        'rDocument.SetParameterValue(4, "01/01/1900")
        ''@op
        'rDocument.SetParameterValue(5, "0")

        'rDocument.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
        'rDocument.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
        'rDocument.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
        'rDocument.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
        'rDocument.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
        'rDocument.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
        'End Select

        'rDocument.SetDataSource(dSet)
        CrystalReportViewer1.ReportSource = rDocument

        If GloOpFacturas = 3 Then
            CrystalReportViewer1.ShowExportButton = False
            CrystalReportViewer1.ShowPrintButton = False
            CrystalReportViewer1.ShowRefreshButton = False
        End If
        rDocument = Nothing

        'customersByCityReport = New ReportDocument
        'Dim connectionInfo As New ConnectionInfo
        ''"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        ''    "=True;User ID=DeSistema;Password=1975huli")
        'connectionInfo.ServerName = GloServerName
        'connectionInfo.DatabaseName = GloDatabaseName
        'connectionInfo.UserID = GloUserID
        'connectionInfo.Password = GloPassword

        'Dim reportPath As String = Nothing


        'If IdSistema = "TO" Then
        '    reportPath = RutaReportes + "\ReporteCajasCabSta.rpt"
        'ElseIf IdSistema = "VA" Then
        '    reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
        'Else
        '    reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
        'End If

        'customersByCityReport.Load(reportPath)
        ''If IdSistema <> "TO" Then
        ''    SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        ''End If
        'SetDBLogonForReport(connectionInfo, customersByCityReport)

        ''@Clv_Factura 
        'customersByCityReport.SetParameterValue(0, GloClv_Factura)
        ''@Clv_Factura_Ini
        'customersByCityReport.SetParameterValue(1, "0")
        ''@Clv_Factura_Fin
        'customersByCityReport.SetParameterValue(2, "0")
        ''@Fecha_Ini
        'customersByCityReport.SetParameterValue(3, "01/01/1900")
        ''@Fecha_Fin
        'customersByCityReport.SetParameterValue(4, "01/01/1900")
        ''@op
        'customersByCityReport.SetParameterValue(5, "0")


        'customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
        'customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
        'customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
        'customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
        'customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
        'customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
        'customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'Copia'"



        'CrystalReportViewer1.ReportSource = customersByCityReport

        'If GloOpFacturas = 3 Then
        '    CrystalReportViewer1.ShowExportButton = False
        '    CrystalReportViewer1.ShowPrintButton = False
        '    CrystalReportViewer1.ShowRefreshButton = False
        'End If
        ''SetDBLogonForReport2(connectionInfo)
        'customersByCityReport = Nothing
    End Sub


    Private Sub OpenSubreport(ByVal reportObjectName As String)

        ' Preview the subreport.

    End Sub

    Private Sub ConfigureCrystalReports1(ByVal No_Contrato As Integer)
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim mySelectFormula As String = Nothing
        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\Tarjetas.rpt"
        customersByCityReport.Load(reportPath)


        SetDBLogonForReport(connectionInfo, customersByCityReport)

        '@No_Contrato 
        customersByCityReport.SetParameterValue(0, No_Contrato)
        mySelectFormula = "*" & No_Contrato & "*"
        customersByCityReport.DataDefinition.FormulaFields("codigo").Text = "'" & mySelectFormula & "'"
        customersByCityReport.PrintOptions.PrinterName = "Datacard Printer"
        customersByCityReport.PrintToPrinter(1, True, 1, 1)

        'CrystalReportViewer1.ReportSource = customersByCityReport
        'CrystalReportViewer1.PrintReport()
        customersByCityReport = Nothing
    End Sub

    'Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
    '    customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
    '    'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

    '    Dim myTables As Tables = myReportDocument.Database.Tables
    '    Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
    '    For Each myTable In myTables
    '        Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
    '        myTableLogonInfo.ConnectionInfo = myConnectionInfo
    '        myTable.ApplyLogOnInfo(myTableLogonInfo)
    '        myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
    '    Next
    'End Sub

    Private Sub SetDBLogonForSubReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.Subreports(0).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        'customersByCityReport.Subreports(0).DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)

        Dim I As Integer = myReportDocument.Subreports.Count
        Dim X As Integer = 0
        For X = 0 To I - 1
            Dim myTables As Tables = myReportDocument.Subreports(X).Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Next X
    End Sub

    Private Sub SetDBLogonForReport2(ByVal myConnectionInfo As ConnectionInfo)
        Dim myTableLogOnInfos As TableLogOnInfos = Me.CrystalReportViewer1.LogOnInfo
        For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
            myTableLogOnInfo.ConnectionInfo = myConnectionInfo
        Next
    End Sub
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ConfigureCrystalReportsPaquetes()
        Dim CON As New SqlConnection(MiConexion)
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim Servicios As String = Nothing
        Dim Titulo As String = Nothing
        Dim Subtitulo As String = Nothing
        Dim Paquetes As String = Nothing
        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\ReportePaquetes.rpt"
        customersByCityReport.Load(reportPath)
        SetDBLogonForReport(connectionInfo, customersByCityReport)

        '@clv_session,, , , ,, ,, , , 
        customersByCityReport.SetParameterValue(0, LocClv_session)
        ',@contratado bit
        customersByCityReport.SetParameterValue(1, LocBndC)
        ',@Baja bit
        customersByCityReport.SetParameterValue(2, LocBndB)
        ',@Insta bit
        customersByCityReport.SetParameterValue(3, LocBndI)
        '@Desconect bit
        customersByCityReport.SetParameterValue(4, LocBndD)
        '@Suspendido bit
        customersByCityReport.SetParameterValue(5, LocBndS)
        '@Fuera bit
        customersByCityReport.SetParameterValue(6, LocBndF)
        ' @tempo
        customersByCityReport.SetParameterValue(7, LocBndDT)
        '@habilita
        customersByCityReport.SetParameterValue(8, LocValidaHab)
        '@periodo1
        customersByCityReport.SetParameterValue(9, LocPeriodo1)
        '@periodo2
        customersByCityReport.SetParameterValue(10, LocPeriodo2)
        CON.Open()
        Me.Dame_Servicios_SeleccionadosTableAdapter.Connection = CON
        Me.Dame_Servicios_SeleccionadosTableAdapter.Fill(Me.ProcedimientosArnoldo2.Dame_Servicios_Seleccionados, LocClv_session, Servicios, Paquetes)
        CON.Close()



        Titulo = "Resumen de Clientes con Servicio(s): " + Servicios
        Subtitulo = " Y con Paquetes: " + Paquetes


        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
        customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Subtitulo & "'"

        CrystalReportViewer1.ReportSource = customersByCityReport

        customersByCityReport = Nothing
    End Sub

    Private Sub FrmImprimirFac_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If LocGloOpRep = 0 Then
            If GloClv_Factura = 0 Then Me.Opacity = 0 Else Me.Opacity = 1
            ConfigureCrystalReports(GloClv_Factura)
        ElseIf LocGloOpRep = 1 Then
            'ConfigureCrystalReports1(LocTarjNo_Contrato)
        ElseIf LocGloOpRep = 5 Then
            ConfigureCrystalReportsDesPagAde()
        ElseIf LocGloOpRep = 2 Then
            Me.Text = "Reporte de Paquetes"
            ConfigureCrystalReportsPaquetes()
        ElseIf LocGloOpRep = 3 Then
            Me.Text = "Nota de Cr�dito"
            ConfigureCrystalReportsNota(gloClvNota)
        ElseIf LocGloOpRep = 6 Then
            ConfigureCrystalReports_tickets2(GloClv_Factura)
        ElseIf LocGloOpRep = 20 Then
            ConfigureCrystalReportsSeriesFioliosXml(GloSeries)
        ElseIf LocGloOpRep = 21 Then
            ConfigureCrystalReportsReimpresionFoliosXml(GloSeries)
        End If
    End Sub

    Private Sub ConfigureCrystalReportsDesPagAde()
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword
        Dim reportPath As String = Nothing
        Dim Titulo As String = Nothing
        Dim Sucursal As String = Nothing
        Dim Ciudades As String = Nothing
        Ciudades = " Ciudad(es): " + LocCiudades
        reportPath = RutaReportes + "\ClientesconPagosAdelantados.rpt"
        Titulo = "Relaci�n de Clientes con Pagos Adelantados"

        Sucursal = " Sucursal: " + GloSucursal
        customersByCityReport.Load(reportPath)

        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        SetDBLogonForReport(connectionInfo, customersByCityReport)
        'SetDBLogonForReport(connectionInfo, customersByCityReport)
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)

        '@FECHA_INI
        customersByCityReport.SetParameterValue(0, GloFecha_Ini)
        '@FECHA_FIN
        customersByCityReport.SetParameterValue(1, GloFecha_Ini)
        '@TIPO
        customersByCityReport.SetParameterValue(2, "")
        '@SUCURSAL
        customersByCityReport.SetParameterValue(3, "0")
        '@CAJA
        customersByCityReport.SetParameterValue(4, "0")
        '@CAJERA
        customersByCityReport.SetParameterValue(5, "")
        '@OP
        customersByCityReport.SetParameterValue(6, "1")
        'Clv_Session
        customersByCityReport.SetParameterValue(7, glosessioncar)



        Dim eFechaTitulo As String = Nothing
        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
        eFechaTitulo = "de la Fecha " & GloFecha_Ini
        customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFechaTitulo & "'"
        customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Sucursal & "'"
        customersByCityReport.DataDefinition.FormulaFields("Ciudades").Text = "'" & Ciudades & "'"


        CrystalReportViewer1.ReportSource = customersByCityReport
        CrystalReportViewer1.Zoom(75)

        'SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing

    End Sub

    Private Sub ConfigureCrystalReportsNota(ByVal nota As Long)
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        Dim ba As Boolean

        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim reportPath As String = Nothing

        'If GloImprimeTickets = False Then
        ' reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
        'Else
        reportPath = RutaReportes + "\ReporteNotasdeCredito.rpt"

        'busfac.Connection = CON
        'busfac.Fill(bfac, Clv_Factura, identi)
        'If IdSistema = "SA" And facnormal = True And identi > 0 Then
        '    reportPath = RutaReportes + "\ReporteCajasTvRey.rpt"
        '    ba = True
        'ElseIf IdSistema = "TO" And facnormal = True And identi > 0 Then
        '    reportPath = RutaReportes + "\ReporteCajasCabSta.rpt"
        '    ba = True

        'Else
        '    reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
        'End If

        'End If

        customersByCityReport.Load(reportPath)
        'If GloImprimeTickets = False Then
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        ' End If
        SetDBLogonForReport1(connectionInfo, customersByCityReport)
        '@Clv_Factura 
        customersByCityReport.SetParameterValue(0, nota)
        '@Clv_Factura_Ini
        customersByCityReport.SetParameterValue(1, "0")
        '@Clv_Factura_Fin
        customersByCityReport.SetParameterValue(2, "0")
        '@Fecha_Ini
        customersByCityReport.SetParameterValue(3, "01/01/1900")
        '@Fecha_Fin
        customersByCityReport.SetParameterValue(4, "01/01/1900")
        '@op
        customersByCityReport.SetParameterValue(5, "0")
        'If GloImprimeTickets = True Then
        If ba = False Then
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'Copia'"
        End If

        'If (IdSistema = "TO" Or IdSistema = "SA") Then 'And facnormal = True And identi > 0 
        '    customersByCityReport.PrintOptions.PrinterName = impresorafiscal
        'Else
        CrystalReportViewer1.ReportSource = customersByCityReport
        'customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
        ' End If

        'customersByCityReport.PrintToPrinter(1, True, 1, 1)
        'CON.Close()
        'If GloOpFacturas = 3 Then
        'CrystalReportViewer1.ShowExportButton = False
        'CrystalReportViewer1.ShowPrintButton = False
        'CrystalReportViewer1.ShowRefreshButton = False
        'End If
        'SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing
    End Sub
    Private Sub SetDBLogonForReport1(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub ConfigureCrystalReports_tickets2(ByVal Clv_Factura As Long)
        Dim ba As Boolean = False
        Select Case IdSistema
            Case "LO"
                customersByCityReport = New ReporteCajasTickets_2Log
            Case "YU"
                customersByCityReport = New ReporteCajasTickets_2Log
        End Select
        Dim connectionInfo As New ConnectionInfo
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword
        Dim reportPath As String = Nothing
        SetDBLogonForReport(connectionInfo, customersByCityReport)
        customersByCityReport.SetParameterValue(0, Clv_Factura)
        'customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
        CrystalReportViewer1.ReportSource = customersByCityReport
        CrystalReportViewer1.ShowExportButton = True
        CrystalReportViewer1.ShowPrintButton = True
        CrystalReportViewer1.ShowRefreshButton = True
        
    End Sub

    Private Sub ConfigureCrystalReportsSeriesFioliosXml(ByVal eSerie As String)
        Try
            customersByCityReport = New ReportDocument

            Dim reportPath As String = Nothing

            reportPath = RutaReportes + "\Reporte_Series_Folios.rpt"

            Dim cnn As New SqlConnection(MiConexion)

            Dim cmd As New SqlCommand("Reporte_Series_Folios", cnn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim parametro As New SqlParameter("@Serie", SqlDbType.VarChar, 50)
            parametro.Direction = ParameterDirection.Input
            parametro.Value = eSerie
            cmd.Parameters.Add(parametro)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet()
            Dim data1 As New DataTable()

            da.Fill(data1)

            data1.TableName = "Tbl_SerieFolios"

            ds.Tables.Add(data1)

            customersByCityReport.Load(reportPath)
            SetDBReport(ds, customersByCityReport)


            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Serie", System.Data.SqlDbType.VarChar, eSerie, 5)
            BaseII.CreateMyParameter("@Nombre", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Output, "")
            BaseII.CreateMyParameter("@GrupoVenta", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Output, "")
            Dim diccionario As New Dictionary(Of String, Object)
            diccionario = BaseII.ProcedimientoOutPut("uspObtenNombreVendedorGrupoVenta")

            customersByCityReport.SetParameterValue("Nombre", diccionario("@Nombre").ToString())
            customersByCityReport.SetParameterValue("Grupo", diccionario("@GrupoVenta").ToString())
            CrystalReportViewer1.ReportSource = customersByCityReport

            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait

            CrystalReportViewer1.Zoom(75)

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ConfigureCrystalReportsReimpresionFoliosXml(ByVal eSerie As String)
        Try
            customersByCityReport = New ReportDocument

            Dim reportPath As String = Nothing

            reportPath = RutaReportes + "\Reporte_Reimpresion_Folios.rpt"

            Dim cnn As New SqlConnection(MiConexion)

            Dim cmd As New SqlCommand("Reporte_Reimpresion_Folios", cnn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim parametro As New SqlParameter("@Serie", SqlDbType.VarChar, 50)
            parametro.Direction = ParameterDirection.Input
            parametro.Value = eSerie
            cmd.Parameters.Add(parametro)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet()
            Dim data1 As New DataTable()

            da.Fill(data1)

            data1.TableName = "Tbl_ReimpresionFolio"

            ds.Tables.Add(data1)

            customersByCityReport.Load(reportPath)
            SetDBReport(ds, customersByCityReport)
            CrystalReportViewer1.ReportSource = customersByCityReport

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Serie", System.Data.SqlDbType.VarChar, eSerie, 5)
            BaseII.CreateMyParameter("@Nombre", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Output, "")
            BaseII.CreateMyParameter("@GrupoVenta", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Output, "")
            Dim diccionario As New Dictionary(Of String, Object)
            diccionario = BaseII.ProcedimientoOutPut("uspObtenNombreVendedorGrupoVenta")

            customersByCityReport.SetParameterValue("Nombre", diccionario("@Nombre").ToString())
            customersByCityReport.SetParameterValue("Grupo", diccionario("@GrupoVenta").ToString())
            CrystalReportViewer1.ReportSource = customersByCityReport


            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait

            CrystalReportViewer1.Zoom(75)

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Function ReportesFacturas(ByVal Clv_Factura As Long) As DataSet
        Dim conexion As New SqlConnection(MiConexion)
        Dim sBuilder As New StringBuilder("EXEC ReportesFacturas " + Clv_Factura.ToString() + ",0,0,'19000101','19000101',0")
        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
        Dim dSet As New DataSet

        Try
            dAdapter.Fill(dSet)
            dSet.Tables(0).TableName = "CALLES"
            dSet.Tables(1).TableName = "CIUDADES"
            dSet.Tables(2).TableName = "CLIENTES"
            dSet.Tables(3).TableName = "COLONIAS"
            dSet.Tables(4).TableName = "CatalogoCajas"
            dSet.Tables(5).TableName = "DatosFiscales"
            dSet.Tables(6).TableName = "DetFacturas"
            dSet.Tables(7).TableName = "DetFacturasImpuestos"
            dSet.Tables(8).TableName = "Facturas"
            dSet.Tables(9).TableName = "GeneralDesconexion"
            dSet.Tables(10).TableName = "ReportesFacturas"
            dSet.Tables(11).TableName = "SUCURSALES"
            dSet.Tables(12).TableName = "Usuarios"
            dSet.Tables(13).TableName = "General"
            dSet.Tables(14).TableName = "tblRelSucursalDatosGenerales"
            dSet.Tables(15).TableName = "companias"
            Return dSet
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Function

End Class