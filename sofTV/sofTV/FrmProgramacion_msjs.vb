Imports System.Data.SqlClient
Public Class FrmProgramacion_msjs
    Dim contador, DIAS, dias_semana, habilitar, tip_prog, tip_frec, tip_inter, INTERVALO, ordinal, unidades, fecha_inicio, fecha_final, hora_inicio, hora_final As Integer
    Private Sub FrmProgramacion_msjs_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.GroupBox1.BackColor = Color.Gray
        Me.GroupBox2.BackColor = Color.Gray
        Me.GroupBox3.BackColor = Color.Gray
        Me.GroupBox4.BackColor = Color.Gray
        Dim conLidia As New SqlClient.SqlConnection(MiConexion)
        conLidia.Open()
        'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetLidia.Muestra_Programacion' Puede moverla o quitarla seg�n sea necesario.
        Me.Muestra_ProgramacionTableAdapter.Connection = conLidia
        Me.Muestra_ProgramacionTableAdapter.Fill(Me.DataSetLidia.Muestra_Programacion)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetLidia.Muestra_WeekDay' Puede moverla o quitarla seg�n sea necesario.
        Me.Muestra_WeekDayTableAdapter.Connection = conLidia
        Me.Muestra_WeekDayTableAdapter.Fill(Me.DataSetLidia.Muestra_WeekDay)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetLidia.Muestra_Ordinales' Puede moverla o quitarla seg�n sea necesario.
        Me.Muestra_OrdinalesTableAdapter.Connection = conLidia
        Me.Muestra_OrdinalesTableAdapter.Fill(Me.DataSetLidia.Muestra_Ordinales)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetLidia.Muestra_TipoIntervalo' Puede moverla o quitarla seg�n sea necesario.
        Me.Muestra_TipoIntervaloTableAdapter.Connection = conLidia
        Me.Muestra_TipoIntervaloTableAdapter.Fill(Me.DataSetLidia.Muestra_TipoIntervalo)
        conLidia.Close()
        colorea(Me, Me.Name)
        habilitar = 0
        tip_prog = 0
        tip_frec = 0
        tip_inter = 0
        DIAS = 0
        INTERVALO = 0
        ordinal = 0
        unidades = 0
        fecha_inicio = 0
        fecha_final = 99991231
        hora_inicio = 0
        hora_final = 235959

        Me.TabControl1.SelectedIndex = 0
        Me.GroupBox2.Enabled = False
        Me.GroupBox3.Enabled = False

        Me.CheckBox8.Checked = True
        Me.DateTimePicker7.Enabled = False
        Me.NumericUpDown6.Enabled = False
        Me.DateTimePicker3.Enabled = False
        Me.ComboBox4.Enabled = False
        Me.DateTimePicker4.Enabled = False
        Me.DateTimePicker5.Enabled = False
        Me.RadioButton5.Checked = True
        Me.RadioButton4.Checked = True

        'Primera Opci�n
        Me.DateTimePicker1.Visible = True
        Me.DateTimePicker2.Visible = True
        Me.GroupBox2.Enabled = False
        Me.GroupBox3.Enabled = False
        Me.Label2.Visible = True
        Me.Label3.Visible = True
        tip_frec = 1

    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        If Me.RadioButton1.Checked = True Then
            Me.NumericUpDown3.Enabled = True
            Me.NumericUpDown4.Enabled = True
            Me.Label10.Enabled = True
            Me.Label9.Enabled = True
            tip_frec = 16
        Else
            Me.NumericUpDown3.Enabled = False
            Me.NumericUpDown4.Enabled = False
            Me.Label10.Enabled = False
            Me.Label9.Enabled = False
        End If
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        If Me.RadioButton2.Checked = True Then
            Me.ComboBox2.Enabled = True
            Me.ComboBox3.Enabled = True
            Me.NumericUpDown5.Enabled = True
            Me.Label11.Enabled = True
            Me.Label12.Enabled = True
            tip_frec = 32
        Else
            Me.ComboBox2.Enabled = False
            Me.ComboBox3.Enabled = False
            Me.NumericUpDown5.Enabled = False
            Me.Label11.Enabled = False
            Me.Label12.Enabled = False
        End If
    End Sub
    Public Sub Genera_Job()
        Dim conlidia2 As New SqlClient.SqlConnection(MiConexion)
        conlidia2.Open()
        Dim comando As New SqlClient.SqlCommand
        ''==== CREA TRABAJOS 
        comando = New SqlClient.SqlCommand
        With comando
            .Connection = conlidia2
            .CommandText = "Crea_Trabajo "
            .CommandType = CommandType.StoredProcedure
            .CommandTimeout = 0
            ' Create a SqlParameter for each parameter in the stored procedure.
            Dim prm As New SqlParameter("@Job", SqlDbType.VarChar, 200)
            Dim prm1 As New SqlParameter("@Descrip", SqlDbType.VarChar, 300)
            prm.Direction = ParameterDirection.Input
            prm1.Direction = ParameterDirection.Input
            prm.Value = job
            prm1.Value = descrip_job
            .Parameters.Add(prm)
            .Parameters.Add(prm1)
            Dim i As Integer = comando.ExecuteNonQuery()
            'eRes = prm1.Value

        End With
        Dim consulta2 As String
        ''============================================ CREA PASOS  ===================================================
        'Me.Crea_Pasos_sqlTableAdapter.Connection = conlidia2
        'Me.Crea_Pasos_sqlTableAdapter.Fill(Me.DataSetLidia.Crea_Pasos_sql, "Mensajes", "Creaci�n de Programaciones ", "la consulta", GloDatabaseName)
        consulta2 = "use " + GloDatabaseName + vbNewLine + "declare @string varchar(max) " + vbNewLine + "set @string=" + "'" + consulta_Prog + "'" + vbNewLine + "EXEC Manda_MsjsProgramados  @string," + "'" + Mensaje_Prog + "'"
        Dim comando2 As New SqlClient.SqlCommand
        With comando2
            .Connection = conlidia2
            .CommandText = "Crea_Pasos_sql "
            .CommandType = CommandType.StoredProcedure
            .CommandTimeout = 0
            ' Create a SqlParameter for each parameter in the stored procedure.
            Dim prm As New SqlParameter("@Job", SqlDbType.VarChar, 200)
            Dim prm1 As New SqlParameter("@Paso_name", SqlDbType.VarChar, 250)
            Dim prm2 As New SqlParameter("@Comando", SqlDbType.VarChar, 18000)
            Dim prm3 As New SqlParameter("@bdd", SqlDbType.VarChar)

            prm.Direction = ParameterDirection.Input
            prm1.Direction = ParameterDirection.Input
            prm2.Direction = ParameterDirection.Input
            prm3.Direction = ParameterDirection.Input

            prm.Value = job
            prm1.Value = "Paso 1"
            prm2.Value = consulta2 '"EXEC Manda_MsjsProgramados  " + "'" + "'" + consulta_Prog + "'" + "'" + "," + "'" + Mensaje_Prog + "'"
            prm3.Value = GloDatabaseName
            .Parameters.Add(prm)
            .Parameters.Add(prm1)
            .Parameters.Add(prm2)
            .Parameters.Add(prm3)
            Dim i As Integer = comando2.ExecuteNonQuery()
            'eRes = prm1.Value

        End With

        ''=========================================== CREA LA PROGRAMACION ===================================
        'Me.Crea_ProgramacionsqlTableAdapter.Connection = conlidia2
        'Me.Crea_ProgramacionsqlTableAdapter.Fill(Me.DataSetLidia.Crea_Programacionsql, "Mensajes", "Programcion de Prueba", habilitar, DIAS, tip_frec, tip_inter, INTERVALO, ordinal, unidades, fecha_inicio, fecha_final, hora_inicio, hora_final)
        '@job varchar(200),@Prog_name varchar(250),@habilitar int,@DIAS int,@tip_frec int,@tip_inter int,@intervalo int,@ordinal int,@unidades int,
        '@fecha_inicio int,@fecha_final int,@HORA_INICIO INT,@HORA_FINAL INT)
        Dim comando3 As New SqlClient.SqlCommand
        With comando3
            .Connection = conlidia2
            .CommandText = "Crea_Programacionsql "
            .CommandType = CommandType.StoredProcedure
            .CommandTimeout = 0
            ' Create a SqlParameter for each parameter in the stored procedure.
            Dim prm As New SqlParameter("@Job", SqlDbType.VarChar, 200)
            Dim prm1 As New SqlParameter("@Prog_name", SqlDbType.VarChar, 250)
            Dim prm2 As New SqlParameter("@Habilitar", SqlDbType.Int)
            Dim prm25 As New SqlParameter("@Dias", SqlDbType.VarChar)
            Dim prm3 As New SqlParameter("@Tip_frec", SqlDbType.Int)
            Dim prm4 As New SqlParameter("@Tip_inter", SqlDbType.Int)
            Dim prm5 As New SqlParameter("@Intervalo", SqlDbType.Int)
            Dim prm6 As New SqlParameter("@Ordinal", SqlDbType.Int)
            Dim prm7 As New SqlParameter("@Unidades", SqlDbType.Int)
            Dim prm8 As New SqlParameter("@Fecha_inicio", SqlDbType.Int)
            Dim prm9 As New SqlParameter("@Fecha_final", SqlDbType.Int)
            Dim prm10 As New SqlParameter("@Hora_inicio", SqlDbType.Int)
            Dim prm11 As New SqlParameter("@Hora_final", SqlDbType.Int)
            Dim prm12 As New SqlParameter("@Clv_session", SqlDbType.BigInt)

            prm.Direction = ParameterDirection.Input
            prm1.Direction = ParameterDirection.Input
            prm2.Direction = ParameterDirection.Input
            prm25.Direction = ParameterDirection.Input
            prm3.Direction = ParameterDirection.Input
            prm4.Direction = ParameterDirection.Input
            prm5.Direction = ParameterDirection.Input
            prm6.Direction = ParameterDirection.Input
            prm7.Direction = ParameterDirection.Input
            prm8.Direction = ParameterDirection.Input
            prm9.Direction = ParameterDirection.Input
            prm10.Direction = ParameterDirection.Input
            prm11.Direction = ParameterDirection.Input
            prm12.Direction = ParameterDirection.Input

            prm.Value = job
            prm1.Value = name_prog
            prm2.Value = habilitar
            prm25.Value = DIAS
            prm3.Value = tip_frec
            prm4.Value = tip_inter
            prm5.Value = INTERVALO
            prm6.Value = ordinal
            prm7.Value = unidades
            prm8.Value = fecha_inicio
            prm9.Value = fecha_final
            prm10.Value = hora_inicio
            prm11.Value = hora_final
            prm12.Value = LocClv_session

            .Parameters.Add(prm)
            .Parameters.Add(prm1)
            .Parameters.Add(prm2)
            .Parameters.Add(prm25)
            .Parameters.Add(prm3)
            .Parameters.Add(prm4)
            .Parameters.Add(prm5)
            .Parameters.Add(prm6)
            .Parameters.Add(prm7)
            .Parameters.Add(prm8)
            .Parameters.Add(prm9)
            .Parameters.Add(prm10)
            .Parameters.Add(prm11)
            .Parameters.Add(prm12)

            Dim i As Integer = comando3.ExecuteNonQuery()
            'eRes = prm1.Value

        End With
        conlidia2.Close()
        'MsgBox("Ha" + CStr(habilitar) + " " + "Prog" + CStr(tip_prog) + " " + "FREc" + CStr(tip_frec) + " " + "TIP INter" + CStr(tip_inter) + " " + "dias  " + CStr(DIAS) + " " + "INTer" + CStr(INTERVALO) + " " + "ORD" + CStr(ordinal) + " " + "UNI" + CStr(unidades) + " " + CStr(fecha_inicio) + " " + CStr(fecha_final) + " " + CStr(hora_inicio) + " " + CStr(hora_final))
        MsgBox("La Programaci�n fue Grabada con �xito", MsgBoxStyle.Information)
        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Programo un Mensaje:" + job, "Fecha Inicial:" + Me.DateTimePicker2.Text + " Fecha Final: " + Me.DateTimePicker7.Text, "Hora Inicial: " + CStr(hora_inicio) + " Hora Final: " + CStr(hora_final), LocClv_Ciudad)
        bec_bnd = True
        habilitar = 0
        tip_prog = 0
        tip_frec = 0
        tip_inter = 0
        DIAS = 0
        INTERVALO = 0
        ordinal = 0
        unidades = 0
        fecha_inicio = 0
        fecha_final = 99991231
        hora_inicio = 0
        hora_final = 235959
        Programacion = 0
        dias_semana = 0
        FrmGuardaMensaje.Close()
        FrmSelEstado_Mensajes.Close()
        Me.Close()
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        If tip_frec = 1 Then

            DateTimePicker2.Format = DateTimePickerFormat.Custom
            DateTimePicker2.CustomFormat = "yyyyMMdd"
            fecha_inicio = CInt(DateTimePicker2.Text)
            DateTimePicker2.Format = DateTimePickerFormat.Short

            DateTimePicker1.Format = DateTimePickerFormat.Custom
            DateTimePicker1.CustomFormat = "HHmmss"
            hora_inicio = CInt(DateTimePicker1.Text)
            DateTimePicker1.Format = DateTimePickerFormat.Time
            guardaMensaje()

        Else
            If tip_frec = 4 Then  ' DIAS    ====================
                DIAS = Me.NumericUpDown1.Value
            ElseIf tip_frec = 8 Then  ' SEMANAS   =============
                unidades = Me.NumericUpDown2.Value
                Checa_dias(Me.TabPage3)
                If dias_semana = 0 Then
                    MsgBox("Se debe de Seleccionar por lo Menos un Dia de la Semana")
                    Exit Sub
                End If
                DIAS = dias_semana
            ElseIf tip_frec = 32 Then  ' MESES    ===============
                unidades = Me.NumericUpDown5.Value
                DIAS = Me.ComboBox3.SelectedValue
                ordinal = Me.ComboBox2.SelectedValue
            ElseIf tip_frec = 16 Then
                DIAS = Me.NumericUpDown3.Value
                unidades = Me.NumericUpDown4.Value
            End If

            If Me.RadioButton3.Checked = False And Me.RadioButton4.Checked = True Then
                DateTimePicker3.Format = DateTimePickerFormat.Custom
                DateTimePicker3.CustomFormat = "HHmmss"
                hora_inicio = CInt(Me.DateTimePicker3.Text)
                DateTimePicker3.Format = DateTimePickerFormat.Time
            ElseIf Me.RadioButton3.Checked = True And Me.RadioButton4.Checked = False Then
                If Me.ComboBox4.Text = " " Then
                    MsgBox("Seleccione Horas/Minutos en el Combo ")
                End If
                INTERVALO = Me.NumericUpDown6.Value
                DateTimePicker4.Format = DateTimePickerFormat.Custom
                DateTimePicker4.CustomFormat = "HHmmss"
                hora_inicio = CInt(Me.DateTimePicker4.Text)
                DateTimePicker4.Format = DateTimePickerFormat.Time

                DateTimePicker5.Format = DateTimePickerFormat.Custom
                DateTimePicker5.CustomFormat = "HHmmss"
                hora_final = CInt(Me.DateTimePicker5.Text)
                DateTimePicker5.Format = DateTimePickerFormat.Time
                tip_inter = Me.ComboBox4.SelectedValue

                If hora_inicio = hora_final Then
                    MsgBox("La hora final no puede ser igual a la hora de inicio.", MsgBoxStyle.Exclamation)
                    Exit Sub
                End If

            End If

            DateTimePicker8.Format = DateTimePickerFormat.Custom
            DateTimePicker8.CustomFormat = "yyyyMMdd"
            fecha_inicio = CInt(Me.DateTimePicker8.Text)
            DateTimePicker8.Format = DateTimePickerFormat.Short

            If Me.RadioButton6.Checked = True Then
                DateTimePicker7.Format = DateTimePickerFormat.Custom
                DateTimePicker7.CustomFormat = "yyyyMMdd"
                fecha_final = CInt(Me.DateTimePicker7.Text)
                DateTimePicker7.Format = DateTimePickerFormat.Short
            End If

            guardaMensaje()

        End If

    End Sub
    Private Sub guardaMensaje()
        If Me.TextBox1.Text = "" Then
            MsgBox("Se Debe Capturar un Nombre ")
            Exit Sub

        Else
            Dim conlidia5 As New SqlClient.SqlConnection(MiConexion)
            conlidia5.Open()
            Dim comando As New SqlClient.SqlCommand
            ''==== CREA TRABAJOS 
            comando = New SqlClient.SqlCommand
            With comando
                .Connection = conlidia5
                .CommandText = "checar_progs"
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
                ' Create a SqlParameter for each parameter in the stored procedure.
                Dim prm As New SqlParameter("@Nombre", SqlDbType.VarChar, 300)
                Dim prm1 As New SqlParameter("@cont", SqlDbType.Int)
                prm.Direction = ParameterDirection.Input
                prm1.Direction = ParameterDirection.Output
                prm.Value = Me.TextBox1.Text
                prm1.Value = 0
                .Parameters.Add(prm)
                .Parameters.Add(prm1)
                Dim i As Integer = comando.ExecuteNonQuery()
                contador = prm1.Value
            End With
            conlidia5.Close()
            If contador = 0 Then
                job = Me.TextBox1.Text
                descrip_job = ""
                name_prog = Me.TextBox1.Text
                Genera_Job()
            ElseIf contador > 0 Then
                MsgBox("El Nombre de la Programaci�n Ya Existe, Favor de Capturar otro Nombre", MsgBoxStyle.Information)
            End If

        End If
    End Sub
    Private Sub RadioButton4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton4.CheckedChanged
        If Me.RadioButton4.Checked = False Then
            Me.DateTimePicker3.Enabled = False
            Me.NumericUpDown6.Enabled = True
            Me.ComboBox4.Enabled = True
            Me.DateTimePicker4.Enabled = True
            Me.DateTimePicker5.Enabled = True
        ElseIf Me.RadioButton4.Checked = True Then
            Me.DateTimePicker3.Enabled = True
            Me.NumericUpDown6.Enabled = False
            Me.ComboBox4.Enabled = False
            Me.DateTimePicker4.Enabled = False
            Me.DateTimePicker5.Enabled = False

        Else
            Me.DateTimePicker3.Enabled = False
            Me.NumericUpDown6.Enabled = False
            Me.ComboBox4.Enabled = False
            Me.DateTimePicker4.Enabled = False
            Me.DateTimePicker5.Enabled = False
        End If
    End Sub

    Public Sub Checa_dias(ByVal formulario As TabPage) 'tab page
        Dim panel3 As Panel
        Dim label As Label
        Dim var As Integer = 0
        Dim TEXT As TextBox
        Dim CHECK As CheckBox
        Dim RADIO As RadioButton
        Dim boton As Button

        For Each ctm As Control In formulario.Controls
            If ctm.GetType Is GetType(System.Windows.Forms.CheckBox) Then
                CHECK = New CheckBox
                CHECK = ctm
                If CHECK.Checked = True Then
                    If dias_semana = 0 Then
                        var = CInt(Mid(CHECK.Name, 9, 1))
                        var = 2 ^ (var - 1)
                        dias_semana = var
                    Else
                        var = CInt(Mid(CHECK.Name, 9, 1))
                        var = 2 ^ (var - 1)
                        dias_semana = dias_semana + var
                    End If
                End If
                CHECK = Nothing
            End If
        Next
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub


    Private Sub CheckBox8_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox8.CheckedChanged
        If Me.CheckBox8.Checked = True Then
            habilitar = 1
        Else
            habilitar = 0
        End If
    End Sub



    Private Sub RadioButton6_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton6.CheckedChanged
        If Me.RadioButton6.Checked = False Then
            Me.DateTimePicker7.Enabled = False
        ElseIf Me.RadioButton6.Enabled = True Then
            Me.DateTimePicker7.Enabled = True
        End If
    End Sub

    Private Sub RadioButton5_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton5.CheckedChanged
        If Me.RadioButton5.Checked = False Then
            Me.DateTimePicker7.Enabled = True
        ElseIf Me.RadioButton5.Checked = True Then
            Me.DateTimePicker7.Enabled = False
        End If
    End Sub


    Private Sub TabControl1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabControl1.SelectedIndexChanged
        Dim I As Integer = Me.TabControl1.SelectedIndex

        Select Case I
            Case 0
                Me.DateTimePicker1.Visible = True
                Me.DateTimePicker2.Visible = True
                Me.GroupBox2.Enabled = False
                Me.GroupBox3.Enabled = False
                Me.Label2.Visible = True
                Me.Label3.Visible = True
                tip_frec = 1
            Case 1
                Me.DateTimePicker1.Visible = False
                Me.DateTimePicker2.Visible = False
                Me.Label2.Visible = False
                Me.Label3.Visible = False
                Me.GroupBox2.Enabled = True
                Me.GroupBox3.Enabled = True
                Me.TabControl1.Enabled = True
                tip_frec = 4
            Case 2

                Me.DateTimePicker1.Visible = False
                Me.DateTimePicker2.Visible = False
                Me.Label2.Visible = False
                Me.Label3.Visible = False
                Me.GroupBox2.Enabled = True
                Me.GroupBox3.Enabled = True
                tip_frec = 8
            Case 3
                Me.RadioButton1.Checked = True
                If Me.RadioButton1.Checked = True Then
                    Me.NumericUpDown5.Enabled = False
                    Me.ComboBox2.Enabled = False
                    Me.ComboBox3.Enabled = False
                    tip_frec = 16
                Else
                    Me.NumericUpDown4.Enabled = False
                    Me.NumericUpDown3.Enabled = False
                    tip_frec = 32
                End If
                Me.DateTimePicker1.Visible = False
                Me.DateTimePicker2.Visible = False
                Me.Label2.Visible = False
                Me.Label3.Visible = False
                Me.GroupBox2.Enabled = True
                Me.GroupBox3.Enabled = True


        End Select
    End Sub


End Class