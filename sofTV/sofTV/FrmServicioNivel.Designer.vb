﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmServicioNivel
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Lbl_Servicio = New System.Windows.Forms.Label()
        Me.Lbl_Puntos = New System.Windows.Forms.Label()
        Me.Txt_Puntos = New System.Windows.Forms.TextBox()
        Me.CMB_Servicio = New System.Windows.Forms.ComboBox()
        Me.Grp_ServicioNivel = New System.Windows.Forms.GroupBox()
        Me.CMB_Nivel = New System.Windows.Forms.ComboBox()
        Me.Lbl_Nivel = New System.Windows.Forms.Label()
        Me.Btn_Agregar = New System.Windows.Forms.Button()
        Me.Btn_Salir = New System.Windows.Forms.Button()
        Me.DGV_ServicioNivel = New System.Windows.Forms.DataGridView()
        Me.Id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colGrupoVentas = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Punto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Niveles = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Eliminar = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.gbxGrupoVentas = New System.Windows.Forms.GroupBox()
        Me.cmbGrupoVentas = New System.Windows.Forms.ComboBox()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Grp_ServicioNivel.SuspendLayout()
        CType(Me.DGV_ServicioNivel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxGrupoVentas.SuspendLayout()
        Me.SuspendLayout()
        '
        'Lbl_Servicio
        '
        Me.Lbl_Servicio.AutoSize = True
        Me.Lbl_Servicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_Servicio.Location = New System.Drawing.Point(116, 19)
        Me.Lbl_Servicio.Name = "Lbl_Servicio"
        Me.Lbl_Servicio.Size = New System.Drawing.Size(77, 16)
        Me.Lbl_Servicio.TabIndex = 0
        Me.Lbl_Servicio.Text = "Servicio : "
        '
        'Lbl_Puntos
        '
        Me.Lbl_Puntos.AutoSize = True
        Me.Lbl_Puntos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_Puntos.Location = New System.Drawing.Point(126, 84)
        Me.Lbl_Puntos.Name = "Lbl_Puntos"
        Me.Lbl_Puntos.Size = New System.Drawing.Size(67, 16)
        Me.Lbl_Puntos.TabIndex = 1
        Me.Lbl_Puntos.Text = "Puntos : "
        '
        'Txt_Puntos
        '
        Me.Txt_Puntos.Location = New System.Drawing.Point(208, 84)
        Me.Txt_Puntos.Name = "Txt_Puntos"
        Me.Txt_Puntos.Size = New System.Drawing.Size(147, 20)
        Me.Txt_Puntos.TabIndex = 2
        '
        'CMB_Servicio
        '
        Me.CMB_Servicio.FormattingEnabled = True
        Me.CMB_Servicio.Location = New System.Drawing.Point(208, 19)
        Me.CMB_Servicio.Name = "CMB_Servicio"
        Me.CMB_Servicio.Size = New System.Drawing.Size(352, 21)
        Me.CMB_Servicio.TabIndex = 3
        '
        'Grp_ServicioNivel
        '
        Me.Grp_ServicioNivel.Controls.Add(Me.CMB_Nivel)
        Me.Grp_ServicioNivel.Controls.Add(Me.Lbl_Nivel)
        Me.Grp_ServicioNivel.Controls.Add(Me.Btn_Agregar)
        Me.Grp_ServicioNivel.Controls.Add(Me.Lbl_Servicio)
        Me.Grp_ServicioNivel.Controls.Add(Me.CMB_Servicio)
        Me.Grp_ServicioNivel.Controls.Add(Me.Lbl_Puntos)
        Me.Grp_ServicioNivel.Controls.Add(Me.Txt_Puntos)
        Me.Grp_ServicioNivel.Location = New System.Drawing.Point(22, 61)
        Me.Grp_ServicioNivel.Name = "Grp_ServicioNivel"
        Me.Grp_ServicioNivel.Size = New System.Drawing.Size(669, 170)
        Me.Grp_ServicioNivel.TabIndex = 4
        Me.Grp_ServicioNivel.TabStop = False
        '
        'CMB_Nivel
        '
        Me.CMB_Nivel.FormattingEnabled = True
        Me.CMB_Nivel.Location = New System.Drawing.Point(208, 52)
        Me.CMB_Nivel.Name = "CMB_Nivel"
        Me.CMB_Nivel.Size = New System.Drawing.Size(352, 21)
        Me.CMB_Nivel.TabIndex = 9
        '
        'Lbl_Nivel
        '
        Me.Lbl_Nivel.AutoSize = True
        Me.Lbl_Nivel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_Nivel.Location = New System.Drawing.Point(137, 52)
        Me.Lbl_Nivel.Name = "Lbl_Nivel"
        Me.Lbl_Nivel.Size = New System.Drawing.Size(56, 16)
        Me.Lbl_Nivel.TabIndex = 8
        Me.Lbl_Nivel.Text = "Nivel : "
        '
        'Btn_Agregar
        '
        Me.Btn_Agregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Agregar.Location = New System.Drawing.Point(280, 114)
        Me.Btn_Agregar.Name = "Btn_Agregar"
        Me.Btn_Agregar.Size = New System.Drawing.Size(128, 38)
        Me.Btn_Agregar.TabIndex = 6
        Me.Btn_Agregar.Text = "&Agregar"
        Me.Btn_Agregar.UseVisualStyleBackColor = True
        '
        'Btn_Salir
        '
        Me.Btn_Salir.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Salir.Location = New System.Drawing.Point(561, 457)
        Me.Btn_Salir.Name = "Btn_Salir"
        Me.Btn_Salir.Size = New System.Drawing.Size(130, 43)
        Me.Btn_Salir.TabIndex = 7
        Me.Btn_Salir.Text = "&Salir"
        Me.Btn_Salir.UseVisualStyleBackColor = True
        '
        'DGV_ServicioNivel
        '
        Me.DGV_ServicioNivel.AllowUserToAddRows = False
        Me.DGV_ServicioNivel.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGV_ServicioNivel.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DGV_ServicioNivel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_ServicioNivel.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Id, Me.Descripcion, Me.colGrupoVentas, Me.Punto, Me.Niveles, Me.Eliminar})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGV_ServicioNivel.DefaultCellStyle = DataGridViewCellStyle2
        Me.DGV_ServicioNivel.Location = New System.Drawing.Point(22, 242)
        Me.DGV_ServicioNivel.Name = "DGV_ServicioNivel"
        Me.DGV_ServicioNivel.ReadOnly = True
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGV_ServicioNivel.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.DGV_ServicioNivel.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGV_ServicioNivel.Size = New System.Drawing.Size(669, 209)
        Me.DGV_ServicioNivel.TabIndex = 5
        '
        'Id
        '
        Me.Id.DataPropertyName = "Id"
        Me.Id.FillWeight = 10.0!
        Me.Id.HeaderText = "Id"
        Me.Id.Name = "Id"
        Me.Id.ReadOnly = True
        Me.Id.Visible = False
        Me.Id.Width = 40
        '
        'Descripcion
        '
        Me.Descripcion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Descripcion.DataPropertyName = "Descripcion"
        Me.Descripcion.HeaderText = "Descripcion"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        '
        'colGrupoVentas
        '
        Me.colGrupoVentas.DataPropertyName = "GrupoVentas"
        Me.colGrupoVentas.HeaderText = "Grupo de Ventas"
        Me.colGrupoVentas.Name = "colGrupoVentas"
        Me.colGrupoVentas.ReadOnly = True
        '
        'Punto
        '
        Me.Punto.DataPropertyName = "Puntos"
        Me.Punto.HeaderText = "Puntos"
        Me.Punto.Name = "Punto"
        Me.Punto.ReadOnly = True
        '
        'Niveles
        '
        Me.Niveles.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Niveles.DataPropertyName = "Nivel"
        Me.Niveles.HeaderText = "Niveles"
        Me.Niveles.Name = "Niveles"
        Me.Niveles.ReadOnly = True
        '
        'Eliminar
        '
        Me.Eliminar.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Eliminar.DataPropertyName = "Eliminar"
        Me.Eliminar.HeaderText = ""
        Me.Eliminar.Name = "Eliminar"
        Me.Eliminar.ReadOnly = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'gbxGrupoVentas
        '
        Me.gbxGrupoVentas.Controls.Add(Me.cmbGrupoVentas)
        Me.gbxGrupoVentas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxGrupoVentas.Location = New System.Drawing.Point(22, 6)
        Me.gbxGrupoVentas.Name = "gbxGrupoVentas"
        Me.gbxGrupoVentas.Size = New System.Drawing.Size(669, 51)
        Me.gbxGrupoVentas.TabIndex = 8
        Me.gbxGrupoVentas.TabStop = False
        Me.gbxGrupoVentas.Text = "Grupo de Ventas"
        '
        'cmbGrupoVentas
        '
        Me.cmbGrupoVentas.DisplayMember = "Grupo"
        Me.cmbGrupoVentas.FormattingEnabled = True
        Me.cmbGrupoVentas.Location = New System.Drawing.Point(208, 17)
        Me.cmbGrupoVentas.Name = "cmbGrupoVentas"
        Me.cmbGrupoVentas.Size = New System.Drawing.Size(352, 24)
        Me.cmbGrupoVentas.TabIndex = 0
        Me.cmbGrupoVentas.ValueMember = "Clv_Grupo"
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'FrmServicioNivel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(703, 506)
        Me.Controls.Add(Me.gbxGrupoVentas)
        Me.Controls.Add(Me.DGV_ServicioNivel)
        Me.Controls.Add(Me.Grp_ServicioNivel)
        Me.Controls.Add(Me.Btn_Salir)
        Me.Name = "FrmServicioNivel"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Comisión Nivel Servicio"
        Me.Grp_ServicioNivel.ResumeLayout(False)
        Me.Grp_ServicioNivel.PerformLayout()
        CType(Me.DGV_ServicioNivel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxGrupoVentas.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Lbl_Servicio As System.Windows.Forms.Label
    Friend WithEvents Lbl_Puntos As System.Windows.Forms.Label
    Friend WithEvents Txt_Puntos As System.Windows.Forms.TextBox
    Friend WithEvents CMB_Servicio As System.Windows.Forms.ComboBox
    Friend WithEvents Grp_ServicioNivel As System.Windows.Forms.GroupBox
    Friend WithEvents Btn_Agregar As System.Windows.Forms.Button
    Friend WithEvents DGV_ServicioNivel As System.Windows.Forms.DataGridView
    Friend WithEvents Btn_Salir As System.Windows.Forms.Button
    Friend WithEvents CMB_Nivel As System.Windows.Forms.ComboBox
    Friend WithEvents Lbl_Nivel As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents gbxGrupoVentas As System.Windows.Forms.GroupBox
    Friend WithEvents cmbGrupoVentas As System.Windows.Forms.ComboBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colGrupoVentas As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Punto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Niveles As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Eliminar As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
