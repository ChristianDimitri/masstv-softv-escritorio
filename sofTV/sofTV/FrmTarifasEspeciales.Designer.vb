<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTarifasEspeciales
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DescripcionLabel As System.Windows.Forms.Label
        Dim NombreLabel1 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.DataSetEric2 = New sofTV.DataSetEric2
        Me.DameClasificacionLlamadasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameClasificacionLlamadasTableAdapter = New sofTV.DataSetEric2TableAdapters.DameClasificacionLlamadasTableAdapter
        Me.DescripcionComboBox = New System.Windows.Forms.ComboBox
        Me.ConPaisOCodigoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConPaisOCodigoTableAdapter = New sofTV.DataSetEric2TableAdapters.ConPaisOCodigoTableAdapter
        Me.NombreComboBox = New System.Windows.Forms.ComboBox
        Me.MuestraTarifasEspecialesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraTarifasEspecialesTableAdapter = New sofTV.DataSetEric2TableAdapters.MuestraTarifasEspecialesTableAdapter
        Me.AgrTarifasEspecialesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AgrTarifasEspecialesTableAdapter = New sofTV.DataSetEric2TableAdapters.AgrTarifasEspecialesTableAdapter
        Me.EliTarifasEspecialesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EliTarifasEspecialesTableAdapter = New sofTV.DataSetEric2TableAdapters.EliTarifasEspecialesTableAdapter
        Me.ButtonAgregar = New System.Windows.Forms.Button
        Me.ButtonEliminar = New System.Windows.Forms.Button
        Me.ButtonSalir = New System.Windows.Forms.Button
        Me.TextBoxTarifa = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Clasificacion_LDTextBox = New System.Windows.Forms.TextBox
        Me.LadaTextBox = New System.Windows.Forms.TextBox
        Me.MuestraTarifasEspecialesDataGridView = New System.Windows.Forms.DataGridView
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        DescripcionLabel = New System.Windows.Forms.Label
        NombreLabel1 = New System.Windows.Forms.Label
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameClasificacionLlamadasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConPaisOCodigoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTarifasEspecialesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AgrTarifasEspecialesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EliTarifasEspecialesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTarifasEspecialesDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DescripcionLabel
        '
        DescripcionLabel.AutoSize = True
        DescripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescripcionLabel.Location = New System.Drawing.Point(165, 32)
        DescripcionLabel.Name = "DescripcionLabel"
        DescripcionLabel.Size = New System.Drawing.Size(93, 15)
        DescripcionLabel.TabIndex = 2
        DescripcionLabel.Text = "Clasificación:"
        '
        'NombreLabel1
        '
        NombreLabel1.AutoSize = True
        NombreLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel1.ForeColor = System.Drawing.SystemColors.Desktop
        NombreLabel1.Location = New System.Drawing.Point(44, 125)
        NombreLabel1.Name = "NombreLabel1"
        NombreLabel1.Size = New System.Drawing.Size(125, 15)
        NombreLabel1.TabIndex = 5
        NombreLabel1.Text = "Tarifas Especiales"
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric2"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DameClasificacionLlamadasBindingSource
        '
        Me.DameClasificacionLlamadasBindingSource.DataMember = "DameClasificacionLlamadas"
        Me.DameClasificacionLlamadasBindingSource.DataSource = Me.DataSetEric2
        '
        'DameClasificacionLlamadasTableAdapter
        '
        Me.DameClasificacionLlamadasTableAdapter.ClearBeforeFill = True
        '
        'DescripcionComboBox
        '
        Me.DescripcionComboBox.DataSource = Me.DameClasificacionLlamadasBindingSource
        Me.DescripcionComboBox.DisplayMember = "Descripcion"
        Me.DescripcionComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionComboBox.FormattingEnabled = True
        Me.DescripcionComboBox.Location = New System.Drawing.Point(264, 24)
        Me.DescripcionComboBox.Name = "DescripcionComboBox"
        Me.DescripcionComboBox.Size = New System.Drawing.Size(234, 23)
        Me.DescripcionComboBox.TabIndex = 3
        Me.DescripcionComboBox.ValueMember = "Clave"
        '
        'ConPaisOCodigoBindingSource
        '
        Me.ConPaisOCodigoBindingSource.DataMember = "ConPaisOCodigo"
        Me.ConPaisOCodigoBindingSource.DataSource = Me.DataSetEric2
        '
        'ConPaisOCodigoTableAdapter
        '
        Me.ConPaisOCodigoTableAdapter.ClearBeforeFill = True
        '
        'NombreComboBox
        '
        Me.NombreComboBox.DataSource = Me.ConPaisOCodigoBindingSource
        Me.NombreComboBox.DisplayMember = "Nombre"
        Me.NombreComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreComboBox.FormattingEnabled = True
        Me.NombreComboBox.Location = New System.Drawing.Point(91, 88)
        Me.NombreComboBox.Name = "NombreComboBox"
        Me.NombreComboBox.Size = New System.Drawing.Size(397, 23)
        Me.NombreComboBox.TabIndex = 5
        Me.NombreComboBox.ValueMember = "Lada"
        '
        'MuestraTarifasEspecialesBindingSource
        '
        Me.MuestraTarifasEspecialesBindingSource.DataMember = "MuestraTarifasEspeciales"
        Me.MuestraTarifasEspecialesBindingSource.DataSource = Me.DataSetEric2
        '
        'MuestraTarifasEspecialesTableAdapter
        '
        Me.MuestraTarifasEspecialesTableAdapter.ClearBeforeFill = True
        '
        'AgrTarifasEspecialesBindingSource
        '
        Me.AgrTarifasEspecialesBindingSource.DataMember = "AgrTarifasEspeciales"
        Me.AgrTarifasEspecialesBindingSource.DataSource = Me.DataSetEric2
        '
        'AgrTarifasEspecialesTableAdapter
        '
        Me.AgrTarifasEspecialesTableAdapter.ClearBeforeFill = True
        '
        'EliTarifasEspecialesBindingSource
        '
        Me.EliTarifasEspecialesBindingSource.DataMember = "EliTarifasEspeciales"
        Me.EliTarifasEspecialesBindingSource.DataSource = Me.DataSetEric2
        '
        'EliTarifasEspecialesTableAdapter
        '
        Me.EliTarifasEspecialesTableAdapter.ClearBeforeFill = True
        '
        'ButtonAgregar
        '
        Me.ButtonAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonAgregar.Location = New System.Drawing.Point(636, 90)
        Me.ButtonAgregar.Name = "ButtonAgregar"
        Me.ButtonAgregar.Size = New System.Drawing.Size(75, 23)
        Me.ButtonAgregar.TabIndex = 7
        Me.ButtonAgregar.Text = "&Agregar"
        Me.ButtonAgregar.UseVisualStyleBackColor = True
        '
        'ButtonEliminar
        '
        Me.ButtonEliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonEliminar.Location = New System.Drawing.Point(636, 143)
        Me.ButtonEliminar.Name = "ButtonEliminar"
        Me.ButtonEliminar.Size = New System.Drawing.Size(75, 23)
        Me.ButtonEliminar.TabIndex = 8
        Me.ButtonEliminar.Text = "&Eliminar"
        Me.ButtonEliminar.UseVisualStyleBackColor = True
        '
        'ButtonSalir
        '
        Me.ButtonSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonSalir.Location = New System.Drawing.Point(626, 541)
        Me.ButtonSalir.Name = "ButtonSalir"
        Me.ButtonSalir.Size = New System.Drawing.Size(136, 36)
        Me.ButtonSalir.TabIndex = 9
        Me.ButtonSalir.Text = "&SALIR"
        Me.ButtonSalir.UseVisualStyleBackColor = True
        '
        'TextBoxTarifa
        '
        Me.TextBoxTarifa.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxTarifa.Location = New System.Drawing.Point(494, 90)
        Me.TextBoxTarifa.Name = "TextBoxTarifa"
        Me.TextBoxTarifa.Size = New System.Drawing.Size(100, 21)
        Me.TextBoxTarifa.TabIndex = 10
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(88, 70)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 15)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Nacional:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(491, 72)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(48, 15)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "Tarifa:"
        '
        'Clasificacion_LDTextBox
        '
        Me.Clasificacion_LDTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraTarifasEspecialesBindingSource, "Clasificacion_LD", True))
        Me.Clasificacion_LDTextBox.Location = New System.Drawing.Point(334, 282)
        Me.Clasificacion_LDTextBox.Name = "Clasificacion_LDTextBox"
        Me.Clasificacion_LDTextBox.ReadOnly = True
        Me.Clasificacion_LDTextBox.Size = New System.Drawing.Size(10, 20)
        Me.Clasificacion_LDTextBox.TabIndex = 13
        Me.Clasificacion_LDTextBox.TabStop = False
        '
        'LadaTextBox
        '
        Me.LadaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraTarifasEspecialesBindingSource, "Lada", True))
        Me.LadaTextBox.Location = New System.Drawing.Point(318, 282)
        Me.LadaTextBox.Name = "LadaTextBox"
        Me.LadaTextBox.ReadOnly = True
        Me.LadaTextBox.Size = New System.Drawing.Size(10, 20)
        Me.LadaTextBox.TabIndex = 14
        Me.LadaTextBox.TabStop = False
        '
        'MuestraTarifasEspecialesDataGridView
        '
        Me.MuestraTarifasEspecialesDataGridView.AllowUserToAddRows = False
        Me.MuestraTarifasEspecialesDataGridView.AllowUserToDeleteRows = False
        Me.MuestraTarifasEspecialesDataGridView.AutoGenerateColumns = False
        Me.MuestraTarifasEspecialesDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MuestraTarifasEspecialesDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.MuestraTarifasEspecialesDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5})
        Me.MuestraTarifasEspecialesDataGridView.DataSource = Me.MuestraTarifasEspecialesBindingSource
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MuestraTarifasEspecialesDataGridView.DefaultCellStyle = DataGridViewCellStyle3
        Me.MuestraTarifasEspecialesDataGridView.Location = New System.Drawing.Point(47, 143)
        Me.MuestraTarifasEspecialesDataGridView.Name = "MuestraTarifasEspecialesDataGridView"
        Me.MuestraTarifasEspecialesDataGridView.ReadOnly = True
        Me.MuestraTarifasEspecialesDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MuestraTarifasEspecialesDataGridView.Size = New System.Drawing.Size(547, 391)
        Me.MuestraTarifasEspecialesDataGridView.TabIndex = 14
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Orden"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Orden"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Clasificacion_LD"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Clasificacion_LD"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Lada"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Lada"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Nombre"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Nombre"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 380
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Tarifa"
        DataGridViewCellStyle2.Format = "C2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.DataGridViewTextBoxColumn5.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridViewTextBoxColumn5.HeaderText = "Tarifa"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        '
        'FrmTarifasEspeciales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(774, 589)
        Me.Controls.Add(Me.MuestraTarifasEspecialesDataGridView)
        Me.Controls.Add(Me.LadaTextBox)
        Me.Controls.Add(Me.Clasificacion_LDTextBox)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBoxTarifa)
        Me.Controls.Add(Me.ButtonSalir)
        Me.Controls.Add(Me.ButtonEliminar)
        Me.Controls.Add(Me.ButtonAgregar)
        Me.Controls.Add(NombreLabel1)
        Me.Controls.Add(Me.NombreComboBox)
        Me.Controls.Add(DescripcionLabel)
        Me.Controls.Add(Me.DescripcionComboBox)
        Me.Name = "FrmTarifasEspeciales"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Tarifas Especiales"
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameClasificacionLlamadasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConPaisOCodigoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTarifasEspecialesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AgrTarifasEspecialesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EliTarifasEspecialesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTarifasEspecialesDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric2
    Friend WithEvents DameClasificacionLlamadasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameClasificacionLlamadasTableAdapter As sofTV.DataSetEric2TableAdapters.DameClasificacionLlamadasTableAdapter
    Friend WithEvents DescripcionComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents ConPaisOCodigoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConPaisOCodigoTableAdapter As sofTV.DataSetEric2TableAdapters.ConPaisOCodigoTableAdapter
    Friend WithEvents NombreComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents MuestraTarifasEspecialesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTarifasEspecialesTableAdapter As sofTV.DataSetEric2TableAdapters.MuestraTarifasEspecialesTableAdapter
    Friend WithEvents AgrTarifasEspecialesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AgrTarifasEspecialesTableAdapter As sofTV.DataSetEric2TableAdapters.AgrTarifasEspecialesTableAdapter
    Friend WithEvents EliTarifasEspecialesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EliTarifasEspecialesTableAdapter As sofTV.DataSetEric2TableAdapters.EliTarifasEspecialesTableAdapter
    Friend WithEvents ButtonAgregar As System.Windows.Forms.Button
    Friend WithEvents ButtonEliminar As System.Windows.Forms.Button
    Friend WithEvents ButtonSalir As System.Windows.Forms.Button
    Friend WithEvents TextBoxTarifa As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Clasificacion_LDTextBox As System.Windows.Forms.TextBox
    Friend WithEvents LadaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MuestraTarifasEspecialesDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
