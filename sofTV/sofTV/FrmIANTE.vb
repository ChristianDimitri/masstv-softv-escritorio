﻿Imports System.Text
Imports System.Data.SqlClient

Public Class FrmIANTE

    Dim ProcedeDeOrden As Boolean = False

    Private Sub NueIANTE(ByVal Clave As Long, ByVal Clv_Orden As Long, ByVal ContratoAnt As Long, ByVal Clv_Cablemodem As Integer, ByVal Trabajo As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueIANTE", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Clave", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = Clave
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Input
        par2.Value = Clv_Orden
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@ContratoAnt", SqlDbType.Int)
        par3.Direction = ParameterDirection.Input
        par3.Value = ContratoAnt
        comando.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@Clv_Cablemodem", SqlDbType.Int)
        par4.Direction = ParameterDirection.Input
        par4.Value = Clv_Cablemodem
        comando.Parameters.Add(par4)

        Dim par5 As New SqlParameter("@Trabajo", SqlDbType.VarChar, 5)
        par5.Direction = ParameterDirection.Input
        par5.Value = Trabajo
        comando.Parameters.Add(par5)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub BorIANTE(ByVal Clave As Long, ByVal Clv_Orden As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorIANTE", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Clave", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = Clave
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Input
        par2.Value = Clv_Orden
        comando.Parameters.Add(par2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub NueRANTE(ByVal Clave As Long, ByVal Clv_Orden As Long, ByVal ContratoAnt As Long, ByVal Clv_Cablemodem As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueRANTE", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Clave", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = Clave
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Input
        par2.Value = Clv_Orden
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@ContratoAnt", SqlDbType.Int)
        par3.Direction = ParameterDirection.Input
        par3.Value = ContratoAnt
        comando.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@Clv_Cablemodem", SqlDbType.Int)
        par4.Direction = ParameterDirection.Input
        par4.Value = Clv_Cablemodem
        comando.Parameters.Add(par4)


        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub BorRANTE(ByVal Clave As Long, ByVal Clv_Orden As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorRANTE", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Clave", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = Clave
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Input
        par2.Value = Clv_Orden
        comando.Parameters.Add(par2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub NueCANTE(ByVal Clave As Long, ByVal Clv_Orden As Long, ByVal ContratoAnt As Long, ByVal Clv_CablemodemOld As Integer, ByVal Clv_CablemodemNew As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueCANTE", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Clave", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = Clave
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Input
        par2.Value = Clv_Orden
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@ContratoAnt", SqlDbType.Int)
        par3.Direction = ParameterDirection.Input
        par3.Value = ContratoAnt
        comando.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@Clv_CablemodemOld", SqlDbType.Int)
        par4.Direction = ParameterDirection.Input
        par4.Value = Clv_CablemodemOld
        comando.Parameters.Add(par4)

        Dim par5 As New SqlParameter("@Clv_CablemodemNew", SqlDbType.Int)
        par5.Direction = ParameterDirection.Input
        par5.Value = Clv_CablemodemNew
        comando.Parameters.Add(par5)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub BorCANTE(ByVal Clave As Long, ByVal Clv_Orden As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorCANTE", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Clave", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = Clave
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Input
        par2.Value = Clv_Orden
        comando.Parameters.Add(par2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub MuestraAntenasDisponibles(ByVal Clave As Long, ByVal Clv_Orden As Long, ByVal Clv_Tecnico As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim sBuilder As New StringBuilder("EXEC MuestraAntenasDisponibles " + Clave.ToString() + ", " + Clv_Orden.ToString() + ", " + Clv_Tecnico.ToString())
        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
        Dim dTable As New DataTable
        Dim bSource As New BindingSource

        Try
            dAdapter.Fill(dTable)
            bSource.DataSource = dTable
            cbAntenasDisponibles.DataSource = bSource

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub MuestraClientesAntOrdSer(ByVal Contrato As Long, ByVal Clave As Long, ByVal Clv_Orden As Long, ByVal Trabajo As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim sBuilder As New StringBuilder("EXEC MuestraClientesAntOrdSer " + Contrato.ToString() + ", " + Clave.ToString() + ", " + Clv_Orden.ToString() + ", '" + Trabajo + "'")
        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
        Dim dTable As New DataTable
        Dim bSource As New BindingSource

        Try
            dAdapter.Fill(dTable)
            ProcedeDeOrden = False
            ProcedeDeOrden = dTable.Rows(0).Item(2).ToString
            bSource.DataSource = dTable
            cbAntenasPorAsignar.DataSource = bSource

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub FrmIANTE_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GloBloqueaDetalle = True
    End Sub

    Private Sub FrmIANTE_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        colorea(Me, Me.Name)
        panelIANTE.Enabled = True
        If opcion = "C" Then
            panelIANTE.Enabled = False
        End If
        If GLOTRABAJO = "IANTE" Then
            CMBAntenaPor.Text = "Antena por Asignar:"
            Me.Text = "Asignación de Antena"
        ElseIf GLOTRABAJO = "RANTE" Then
            CMBAntenaPor.Text = "Antena a Retirar:"
            Me.Text = "Retiro de Antena"
            CMBAntena.Visible = False
            cbAntenasDisponibles.Visible = False
        ElseIf GLOTRABAJO = "CANTE" Then
            CMBAntenaPor.Text = "Antena a Cambiar:"
            Me.Text = "Cambio de Antena"
        End If

        MuestraClientesAntOrdSer(Contrato, GloDetClave, gloClv_Orden, GLOTRABAJO)
        MuestraAntenasDisponibles(GloDetClave, gloClv_Orden, Locclv_tec)

    End Sub

    Private Sub tsbGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbGuardar.Click

        If cbAntenasPorAsignar.Text.Length = 0 Then
            MsgBox("Selecciona una Antena.", MsgBoxStyle.Information)
            Exit Sub
        End If

        If IsNumeric(cbAntenasPorAsignar.SelectedValue) = False Then
            MsgBox("Selecciona una Antena.", MsgBoxStyle.Information)
            Exit Sub
        End If

        If cbAntenasPorAsignar.SelectedValue = 0 Then
            MsgBox("Selecciona una Antena.", MsgBoxStyle.Information)
            Exit Sub
        End If

        If GLOTRABAJO = "IANTE" Or GLOTRABAJO = "CANTE" Then

            If cbAntenasDisponibles.Text.Length = 0 Then
                MsgBox("Selecciona una Antena disponible.", MsgBoxStyle.Information)
                Exit Sub
            End If

            If IsNumeric(cbAntenasDisponibles.SelectedValue) = False Then
                MsgBox("Selecciona una Antena disponible.", MsgBoxStyle.Information)
                Exit Sub
            End If

            If cbAntenasDisponibles.SelectedValue = 0 Then
                MsgBox("Selecciona una Antena disponible.", MsgBoxStyle.Information)
                Exit Sub
            End If

        End If

        If GLOTRABAJO = "IANTE" Then
            NueIANTE(GloDetClave, gloClv_Orden, cbAntenasPorAsignar.SelectedValue, cbAntenasDisponibles.SelectedValue, GLOTRABAJO)
        ElseIf GLOTRABAJO = "CANTE" Then
            NueCANTE(GloDetClave, gloClv_Orden, cbAntenasPorAsignar.SelectedValue, 0, cbAntenasDisponibles.SelectedValue)
        ElseIf GLOTRABAJO = "RANTE" Then
            NueRANTE(GloDetClave, gloClv_Orden, cbAntenasPorAsignar.SelectedValue, 0)
        End If

        GloBloqueaDetalle = True

        Me.Close()

    End Sub

    Private Sub tsbBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbBorrar.Click

        If GLOTRABAJO = "IANTE" Then
            BorIANTE(GloDetClave, gloClv_Orden)
        ElseIf GLOTRABAJO = "CANTE" Then
            BorCANTE(GloDetClave, gloClv_Orden)
        ElseIf GLOTRABAJO = "RANTE" Then
            BorRANTE(GloDetClave, gloClv_Orden)
        End If

        GloBloqueaDetalle = True

        Me.Close()

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        GloBloqueaDetalle = True
        Me.Close()
    End Sub
End Class