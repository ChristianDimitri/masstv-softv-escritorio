﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text
Imports System.Windows.Forms
Imports System.Collections
Imports System.Collections.Generic
Imports System.Xml
Imports System.IO

Public Class FrmControlHorasOrdenes
    Private _ID As Integer
    Public Property ID As Integer
        Get
            Return _ID
        End Get
        Set(ByVal value As Integer)
            _ID = value
        End Set
    End Property

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub FrmControlHorasOrdenes_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GLOBNDCLV_ORDEN_INI Then
            GLOBNDCLV_ORDEN_INI = False
            Me.txtOrden.Text = GLOCLV_oRDENbus
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, CObj(GLOCLV_oRDENbus))
            BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.BigInt, CObj(0))
            Dim tblOrden As DataTable = BaseII.ConsultaDT("CONORDSER")
            Dim Contrato As Integer = 0
            Dim Tecnico As Integer = 0
            For Each fOrden As DataRow In tblOrden.Rows
                Contrato = CInt(fOrden("Contrato"))
                Tecnico = CInt(IIf(IsDBNull(fOrden("clv_Tecnico")), 0, fOrden("clv_Tecnico")))
            Next
            BUSCACLIENTES(Contrato)

            Dim ContHoras As New ControlHoras(0, GLOCLV_oRDENbus)
            With Me.cbxTecnico
                .DataSource = ContHoras.ListarTecnicos
                .ValueMember = "clv_tecnico"
                .DisplayMember = "Nombre"
            End With

            With Me.cbCuadrilla
                .DataSource = ContHoras.ListarCuadrilla(Tecnico)
                .ValueMember = "clv_tecnico"
                .DisplayMember = "Nombre"
            End With
        End If
    End Sub

    Private Sub FrmControlHorasOrdenes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        GLOBNDCLV_ORDEN_INI = False
        Me.txtUsuario.Text = GloUsuario
        Me.dtpFechaI.Value = Date.Today
        Me.dtpFechaF.Value = Date.Today
        'Dim ContHoras As New ControlHoras
        'With Me.cbxTecnico
        '    .DataSource = ContHoras.ListarTecnicos
        '    .ValueMember = "clv_tecnico"
        '    .DisplayMember = "Nombre"
        'End With

        'With Me.cbCuadrilla
        '    .DataSource = ContHoras.ListarCuadrilla(0)
        '    .ValueMember = "clv_tecnico"
        '    .DisplayMember = "Nombre"
        'End With

        Select Case opcion
            Case "N"
                Me.Panel1.Enabled = True
                Me.mnuBotones.Enabled = True
            Case "C"
                Me.Panel1.Enabled = False
                Me.mnuBotones.Enabled = False
            Case "M"
                Me.Panel1.Enabled = True
                Me.mnuBotones.Enabled = True
        End Select
    End Sub


    Private Sub mnuBotones_RefreshItems(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuBotones.RefreshItems

    End Sub

    Private Sub btnBuscarOrd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscarOrd.Click
        Try
            GLOBNDCLV_ORDEN_INI = True
            GLOCLV_oRDENbus = 0
            FrmBuscaOrdSer.Show()
        Catch ex As Exception
            MsgBox(ex.Message, vbOK, "Buscar Orden")
        End Try
    End Sub


    Private Sub BUSCACLIENTES(ByVal Contrato As Integer)

        Dim CON As New SqlConnection(MiConexion)
        Dim com As SqlCommand = New SqlCommand("BUSCLIPORCONTRATO_2", CON)
        Dim tabla As DataTable = New DataTable
        Dim da As SqlDataAdapter = New SqlDataAdapter(com)
        com.CommandType = CommandType.StoredProcedure
        com.Parameters.Add(New SqlParameter("@CONTRATO", Contrato))
        com.Parameters.Add(New SqlParameter("@NOMBRE", ""))
        com.Parameters.Add(New SqlParameter("@CALLE", ""))
        com.Parameters.Add(New SqlParameter("@NUMERO", ""))
        com.Parameters.Add(New SqlParameter("@CIUDAD", ""))
        com.Parameters.Add(New SqlParameter("@OP", 0))
        com.Parameters.Add(New SqlParameter("@Clv_TipSer", 0))
        com.Parameters.Add(New SqlParameter("@Telefono", ""))
        Try
            CON.Open()
            da.Fill(tabla)
            'LLena  Controles 
            Me.NOMBRELabel1.Text = tabla.Rows(0)(1).ToString
            Me.CALLELabel1.Text = tabla.Rows(0)(2).ToString
            Me.COLONIALabel1.Text = tabla.Rows(0)(3).ToString
            Me.NUMEROLabel1.Text = tabla.Rows(0)(4).ToString
            Me.CIUDADLabel1.Text = tabla.Rows(0)(5).ToString
            Me.SOLOINTERNETCheckBox.Checked = Convert.ToBoolean(tabla.Rows(0)(6).ToString)
            Me.ESHOTELCheckBox.Checked = Convert.ToBoolean(tabla.Rows(0)(6).ToString)
            '
            'Me.BuscaBloqueadoTableAdapter.Connection = CON
            'Me.BuscaBloqueadoTableAdapter.Fill(Me.DataSetLidia.BuscaBloqueado, Me.ContratoTextBox.Text, NUM, num2)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            CON.Close()
        End Try
        CREAARBOL(Contrato)
    End Sub

    Private Sub CREAARBOL(ByVal Contrato As Integer)

        Try
            Dim CON As New SqlConnection(MiConexion)

            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim Y As Integer = 0
            Dim epasa As Boolean = True
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' msgbox(pRow("CustomerID").ToString())
            'Next
            CON.Open()
            'If IsNumeric(Me.ContratoTextBox.Text) = True Then
            '    Me.DameSerDELCliTableAdapter.Connection = CON
            '    Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)))
            'Else
            '    Me.DameSerDELCliTableAdapter.Connection = CON
            '    Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(0, Long)))
            'End If
            CON.Close()
            Dim pasa As Boolean = False
            Dim Net As Boolean = False
            Dim dig As Boolean = False
            Dim jNet As Integer = -1
            Dim PasaJNet As Boolean = False
            Dim jDig As Integer = -1
            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TreeView1.Nodes.Clear()

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CObj(Contrato))
            Dim tblArbol As DataTable = BaseII.ConsultaDT("dameSerDELCli")

            For Each FilaRow In tblArbol.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                'MsgBox(Mid(FilaRow("Servicio").ToString(), 1, 19))
                If Mid(FilaRow("Servicio").ToString(), 1, 3) = "---" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Red  ' System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    Net = False
                    dig = False
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Servicio Basico" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Red  ' System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 31) = "Servicios de Televisión Digital" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Red  ' System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 21) = "Servicios de Internet" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Red  ' System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 22) = "Servicios de Telefonía" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Red  ' System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                Else
                    If Mid(FilaRow("Servicio").ToString(), 1, 14) = "Mac Cablemodem" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jNet = jNet + 1
                        pasa = False
                        Net = True
                    ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Aparato Digital" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jDig = jDig + 1
                        pasa = False
                        dig = True
                    Else
                        If Net = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jNet).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        ElseIf dig = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jDig).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        Else
                            If epasa = True Then
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                pasa = False
                                epasa = False
                            Else
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                                epasa = False
                                pasa = False
                            End If

                        End If
                    End If
                End If
                If pasa = True Then I = I + 1
            Next

            'Me.TreeView1.Nodes(0).ExpandAll()
            For Y = 0 To (I - 1)
                Me.TreeView1.Nodes(Y).ExpandAll()
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try


    End Sub

    Private Sub Consulta_PaisesBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Consulta_PaisesBindingNavigatorSaveItem.Click

        If Me.dtpFechaI.Value > Me.dtpFechaF.Value Then
            MsgBox("La fecha inicial no puede ser mayor a la final", vbOKOnly, "Guardar")
            Return
        End If

        Dim ContHoras As New ControlHoras(_ID)
        ContHoras.Orden = CInt(Me.txtOrden.Text)
        ContHoras.FechaInicio = CDate(Me.dtpFechaI.Value)
        ContHoras.FechaFin = CDate(Me.dtpFechaF.Value)
        ContHoras.UsuarioID = GloClvUsuario
        Select Case opcion
            Case "N"
                ContHoras.Insertar()
            Case "M"
                ContHoras.Modificar()
        End Select
    End Sub

End Class