Imports System.Data.SqlClient
Public Class FrmReferencias
    Dim opcion As Integer
    Private Sub busca()
        Try
            Dim conbwr As New SqlConnection(MiConexion)
            conbwr.Open()
            Me.Consulta_ReferenciaClienteTableAdapter.Connection = conbwr
            Me.Consulta_ReferenciaClienteTableAdapter.Fill(Me.DataSetLidia2.Consulta_ReferenciaCliente, Contrato)
            conbwr.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Referencias(opcion)
        Me.TextBox1.Clear()
        Me.TextBox2.Clear()
        Me.TextBox3.Clear()
        Me.TextBox4.Clear()
        Me.Button4.Enabled = True
        Me.Button3.Enabled = True
        Me.Button5.Enabled = True
        Me.Button2.Enabled = False
        Me.GroupBox1.Enabled = False
        opcion = 0
    End Sub
    Private Sub Referencias(ByVal opt As Integer)
        Dim Cmm As New SqlCommand
        Dim ConRef As New SqlConnection(MiConexion)
        Dim nombre As String = Nothing
        If opt = 1 Then
            nombre = "Nuevo_ReferenciaCliente"
        ElseIf opt = 2 Then
            nombre = "Modifica_ReferenciaCliente"
        ElseIf opt = 3 Then
            nombre = "Borra_ReferenciaCliente"
        End If
        ConRef.Open()
        With Cmm
            .CommandText = nombre
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = ConRef

            Dim pmt As New SqlParameter("@Clv_referencia", SqlDbType.BigInt)
            Dim pmt2 As New SqlParameter("@Contrato", SqlDbType.BigInt)
            Dim pmt3 As New SqlParameter("@Nombre", SqlDbType.VarChar, 500)
            Dim pmt4 As New SqlParameter("@Direccion", SqlDbType.VarChar, 500)
            Dim pmt5 As New SqlParameter("@Email", SqlDbType.VarChar, 50)
            Dim pmt6 As New SqlParameter("@Telefono", SqlDbType.VarChar, 20)

            pmt.Direction = ParameterDirection.Input
            pmt2.Direction = ParameterDirection.Input
            pmt3.Direction = ParameterDirection.Input
            pmt4.Direction = ParameterDirection.Input
            pmt5.Direction = ParameterDirection.Input
            pmt6.Direction = ParameterDirection.Input
            If opt = 1 Then
                pmt.Value = 0
            ElseIf opt = 2 Or opt = 3 Then
                pmt.Value = Me.TextBox5.Text
            End If
            pmt2.Value = Contrato
            pmt3.Value = Me.TextBox1.Text
            pmt4.Value = Me.TextBox4.Text
            pmt5.Value = Me.TextBox2.Text
            pmt6.Value = Me.TextBox3.Text

            .Parameters.Add(pmt)
            If opt <> 3 Then
                .Parameters.Add(pmt2)
                .Parameters.Add(pmt3)
                .Parameters.Add(pmt4)
                .Parameters.Add(pmt5)
                .Parameters.Add(pmt6)
            End If
            .ExecuteNonQuery()
            If opt <> 3 Then
                MsgBox("Se ha Guardado con �xito", MsgBoxStyle.Information)
            ElseIf opt = 3 Then
                MsgBox("Se ha Eliminado con �xito", MsgBoxStyle.Information)
            End If

        End With
        ConRef.Close()
        busca()
    End Sub

    Private Sub FrmReferencias_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If OpcionCli = "C" Then
            Me.Button3.Enabled = False
            Me.Button5.Enabled = False
            Me.Button4.Enabled = False

        End If
        Me.Label1.Text = "Referencias del Contrato: " + CStr(Contrato)
        busca()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Referencias(3)
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        opcion = 1
        Me.GroupBox1.Enabled = True
        Me.Button5.Enabled = False
        Me.Button3.Enabled = False
        Me.Button4.Enabled = False
        Me.Button2.Enabled = True
        Me.Button6.Enabled = True
        Me.TextBox1.Clear()
        Me.TextBox2.Clear()
        Me.TextBox3.Clear()
        Me.TextBox4.Clear()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If IsNumeric(Me.TextBox5.Text) = True Then
            opcion = 2
            Me.Button3.Enabled = False
            Me.Button5.Enabled = False
            Me.Button4.Enabled = False
            Me.Button2.Enabled = True
            Me.Button6.Enabled = True
            Me.GroupBox1.Enabled = True
        End If
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Me.Button5.Enabled = True
        Me.Button3.Enabled = True
        Me.Button2.Enabled = False
        Me.Button6.Enabled = False
        Me.Button4.Enabled = True
        Me.GroupBox1.Enabled = False
    End Sub

End Class