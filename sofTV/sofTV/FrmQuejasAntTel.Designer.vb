﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmQuejasAntTel
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ServicioLabel As System.Windows.Forms.Label
        Dim SOLOINTERNETLabel1 As System.Windows.Forms.Label
        Dim Clv_llamadaLabel As System.Windows.Forms.Label
        Dim ContratoLabel As System.Windows.Forms.Label
        Dim DescripcionLabel As System.Windows.Forms.Label
        Dim SolucionLabel As System.Windows.Forms.Label
        Dim HoraInicialLabel As System.Windows.Forms.Label
        Dim FechaLabel As System.Windows.Forms.Label
        Dim Clv_trabajoLabel As System.Windows.Forms.Label
        Dim Clave_TecnicoLabel As System.Windows.Forms.Label
        Dim Clv_TipoQuejaLabel As System.Windows.Forms.Label
        Dim ClasificacionLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Clv_UsuarioLabel1 As System.Windows.Forms.Label
        Dim TelefonoLabel As System.Windows.Forms.Label
        Dim GeneroLabel As System.Windows.Forms.Label
        Dim Label6 As System.Windows.Forms.Label
        Dim lblTurno As System.Windows.Forms.Label
        Dim lblClasifProblema As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmQuejasAntTel))
        Me.ESHOTELLabel1 = New System.Windows.Forms.Label()
        Me.BUSCLIPORCONTRATOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRATRABAJOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameSerDELCliBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TelefonoLabel1 = New System.Windows.Forms.Label()
        Me.ConAtenTelCteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.ESHOTELCheckBox = New System.Windows.Forms.CheckBox()
        Me.SOLOINTERNETCheckBox = New System.Windows.Forms.CheckBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.NOMBRELabel1 = New System.Windows.Forms.Label()
        Me.CALLELabel1 = New System.Windows.Forms.Label()
        Me.COLONIALabel1 = New System.Windows.Forms.Label()
        Me.NUMEROLabel1 = New System.Windows.Forms.Label()
        Me.CIUDADLabel1 = New System.Windows.Forms.Label()
        Me.BUSCLIPORCONTRATOBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.BUSCLIPORCONTRATOTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.BUSCLIPORCONTRATOTableAdapter()
        Me.DameSerDELCliTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.dameSerDELCliTableAdapter()
        Me.MUESTRATRABAJOSTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRATRABAJOSTableAdapter()
        Me.CONllamadasdeinternetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONllamadasdeinternetTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONllamadasdeinternetTableAdapter()
        Me.Clv_llamadaTextBox = New System.Windows.Forms.TextBox()
        Me.DescripcionTextBox = New System.Windows.Forms.TextBox()
        Me.SolucionTextBox = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cmbClasifProblemas = New System.Windows.Forms.ComboBox()
        Me.btnConsultarCobro = New System.Windows.Forms.Button()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cmbxTurno = New System.Windows.Forms.ComboBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.EnSitio = New System.Windows.Forms.RadioButton()
        Me.EnTelefonia = New System.Windows.Forms.RadioButton()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.cmbPrioridadQueja = New System.Windows.Forms.ComboBox()
        Me.Tecnicos = New System.Windows.Forms.ComboBox()
        Me.MuestraTecnicosAlmacenBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetarnoldo = New sofTV.DataSetarnoldo()
        Me.ComboBox6 = New System.Windows.Forms.ComboBox()
        Me.MUESTRACLASIFICACIONQUEJASBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button8 = New System.Windows.Forms.Button()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.MUESTRATRABAJOSQUEJASBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.HoraInicialMaskedTextBox = New System.Windows.Forms.TextBox()
        Me.FECHATextBox1 = New System.Windows.Forms.TextBox()
        Me.DAMEFECHADELSERVIDOR_2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEdgarRev2 = New sofTV.DataSetEdgarRev2()
        Me.FechaTextBox = New System.Windows.Forms.TextBox()
        Me.DameFechadelServidorHora_2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ContratoTextBox = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Clv_quejaTextBox = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.FechaMaskedTextBox = New System.Windows.Forms.MaskedTextBox()
        Me.ComboBox5 = New System.Windows.Forms.ComboBox()
        Me.MuestraTipSerPrincipalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label7 = New System.Windows.Forms.Label()
        Me.CONllamadasdeinternetBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.CONQUEJASBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.MUESTRATIPOQUEJASBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONllamadasdeinternetBindingSource2 = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRATECNICOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Clv_UsuarioTextBox = New System.Windows.Forms.TextBox()
        Me.MUESTRAUSUARIOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRATRABAJOSBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button5 = New System.Windows.Forms.Button()
        Me.MuestraTipSerPrincipalTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MuestraTipSerPrincipalTableAdapter()
        Me.MUESTRATECNICOSTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRATECNICOSTableAdapter()
        Me.MUESTRATIPOQUEJASTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRATIPOQUEJASTableAdapter()
        Me.MUESTRACLASIFICACIONQUEJASTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACLASIFICACIONQUEJASTableAdapter()
        Me.CONQUEJASTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONQUEJASTableAdapter()
        Me.CONQUEJASBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NUE_CITASBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NUE_CITASTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.NUE_CITASTableAdapter()
        Me.CONDetCitasTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONDetCitasTableAdapter()
        Me.CONDetCitasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NUEREL_CITAS_QUEJASBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NUEREL_CITAS_QUEJASTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.NUEREL_CITAS_QUEJASTableAdapter()
        Me.MUESTRATRABAJOSQUEJASTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRATRABAJOSQUEJASTableAdapter()
        Me.MUESTRAUSUARIOSTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRAUSUARIOSTableAdapter()
        Me.Muestra_Tecnicos_AlmacenTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Muestra_Tecnicos_AlmacenTableAdapter()
        Me.DameFechadelServidorHora_2TableAdapter = New sofTV.DataSetEdgarRev2TableAdapters.DameFechadelServidorHora_2TableAdapter()
        Me.DAMEFECHADELSERVIDOR_2TableAdapter = New sofTV.DataSetEdgarRev2TableAdapters.DAMEFECHADELSERVIDOR_2TableAdapter()
        Me.CONllamadasdeinternetBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_Impresora_OrdenesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_Impresora_OrdenesTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Dame_Impresora_OrdenesTableAdapter()
        Me.DataSetLidia = New sofTV.DataSetLidia()
        Me.BuscaBloqueadoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BuscaBloqueadoTableAdapter = New sofTV.DataSetLidiaTableAdapters.BuscaBloqueadoTableAdapter()
        Me.NueRelQuejaUsuarioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NueRelQuejaUsuarioTableAdapter = New sofTV.DataSetEricTableAdapters.NueRelQuejaUsuarioTableAdapter()
        Me.ConAtenTelCteTableAdapter = New sofTV.DataSetEricTableAdapters.ConAtenTelCteTableAdapter()
        Me.DataSetEric2 = New sofTV.DataSetEric2()
        Me.ValidaQuejasAtenTelBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ValidaQuejasAtenTelTableAdapter = New sofTV.DataSetEric2TableAdapters.ValidaQuejasAtenTelTableAdapter()
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2()
        Me.InsertaRel_AtencionTelUsuarioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertaRel_AtencionTelUsuarioTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.InsertaRel_AtencionTelUsuarioTableAdapter()
        Me.Consulta_Rel_AtencionTelUsuarioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Consulta_Rel_AtencionTelUsuarioTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Consulta_Rel_AtencionTelUsuarioTableAdapter()
        Me.GeneroTextBox = New System.Windows.Forms.TextBox()
        Me.BuscaSiTieneQuejaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BuscaSiTieneQuejaTableAdapter = New sofTV.DataSetEric2TableAdapters.BuscaSiTieneQuejaTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter5 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter6 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter7 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter8 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter9 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter10 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter11 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter12 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter13 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter14 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter15 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter16 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.TxtRefOxxo = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        ServicioLabel = New System.Windows.Forms.Label()
        SOLOINTERNETLabel1 = New System.Windows.Forms.Label()
        Clv_llamadaLabel = New System.Windows.Forms.Label()
        ContratoLabel = New System.Windows.Forms.Label()
        DescripcionLabel = New System.Windows.Forms.Label()
        SolucionLabel = New System.Windows.Forms.Label()
        HoraInicialLabel = New System.Windows.Forms.Label()
        FechaLabel = New System.Windows.Forms.Label()
        Clv_trabajoLabel = New System.Windows.Forms.Label()
        Clave_TecnicoLabel = New System.Windows.Forms.Label()
        Clv_TipoQuejaLabel = New System.Windows.Forms.Label()
        ClasificacionLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Clv_UsuarioLabel1 = New System.Windows.Forms.Label()
        TelefonoLabel = New System.Windows.Forms.Label()
        GeneroLabel = New System.Windows.Forms.Label()
        Label6 = New System.Windows.Forms.Label()
        lblTurno = New System.Windows.Forms.Label()
        lblClasifProblema = New System.Windows.Forms.Label()
        CType(Me.BUSCLIPORCONTRATOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRATRABAJOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameSerDELCliBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.ConAtenTelCteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BUSCLIPORCONTRATOBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONllamadasdeinternetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.MuestraTecnicosAlmacenBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACLASIFICACIONQUEJASBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRATRABAJOSQUEJASBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAMEFECHADELSERVIDOR_2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameFechadelServidorHora_2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTipSerPrincipalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONllamadasdeinternetBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONllamadasdeinternetBindingNavigator.SuspendLayout()
        CType(Me.MUESTRATIPOQUEJASBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONllamadasdeinternetBindingSource2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRATECNICOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAUSUARIOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRATRABAJOSBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONQUEJASBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NUE_CITASBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONDetCitasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NUEREL_CITAS_QUEJASBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONllamadasdeinternetBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_Impresora_OrdenesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BuscaBloqueadoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NueRelQuejaUsuarioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ValidaQuejasAtenTelBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertaRel_AtencionTelUsuarioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_Rel_AtencionTelUsuarioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BuscaSiTieneQuejaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel12.SuspendLayout()
        Me.SuspendLayout()
        '
        'ServicioLabel
        '
        ServicioLabel.AutoSize = True
        ServicioLabel.Location = New System.Drawing.Point(9, 96)
        ServicioLabel.Name = "ServicioLabel"
        ServicioLabel.Size = New System.Drawing.Size(151, 16)
        ServicioLabel.TabIndex = 18
        ServicioLabel.Text = "Servicio Asignados :"
        '
        'SOLOINTERNETLabel1
        '
        SOLOINTERNETLabel1.AutoSize = True
        SOLOINTERNETLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SOLOINTERNETLabel1.ForeColor = System.Drawing.Color.White
        SOLOINTERNETLabel1.Location = New System.Drawing.Point(300, 73)
        SOLOINTERNETLabel1.Name = "SOLOINTERNETLabel1"
        SOLOINTERNETLabel1.Size = New System.Drawing.Size(97, 15)
        SOLOINTERNETLabel1.TabIndex = 16
        SOLOINTERNETLabel1.Text = "Solo Internet :"
        '
        'Clv_llamadaLabel
        '
        Clv_llamadaLabel.AutoSize = True
        Clv_llamadaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_llamadaLabel.ForeColor = System.Drawing.Color.Red
        Clv_llamadaLabel.Location = New System.Drawing.Point(49, 43)
        Clv_llamadaLabel.Name = "Clv_llamadaLabel"
        Clv_llamadaLabel.Size = New System.Drawing.Size(104, 20)
        Clv_llamadaLabel.TabIndex = 176
        Clv_llamadaLabel.Text = "# Reporte  :"
        '
        'ContratoLabel
        '
        ContratoLabel.AutoSize = True
        ContratoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ContratoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ContratoLabel.Location = New System.Drawing.Point(40, 165)
        ContratoLabel.Name = "ContratoLabel"
        ContratoLabel.Size = New System.Drawing.Size(69, 15)
        ContratoLabel.TabIndex = 180
        ContratoLabel.Text = "Contrato :"
        '
        'DescripcionLabel
        '
        DescripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescripcionLabel.ForeColor = System.Drawing.Color.LightSlateGray
        DescripcionLabel.Location = New System.Drawing.Point(23, 428)
        DescripcionLabel.Name = "DescripcionLabel"
        DescripcionLabel.Size = New System.Drawing.Size(86, 30)
        DescripcionLabel.TabIndex = 182
        DescripcionLabel.Text = "Reporte del Cliente :"
        '
        'SolucionLabel
        '
        SolucionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SolucionLabel.ForeColor = System.Drawing.Color.LightSlateGray
        SolucionLabel.Location = New System.Drawing.Point(35, 553)
        SolucionLabel.Name = "SolucionLabel"
        SolucionLabel.Size = New System.Drawing.Size(74, 40)
        SolucionLabel.TabIndex = 184
        SolucionLabel.Text = "Problema Real :"
        '
        'HoraInicialLabel
        '
        HoraInicialLabel.AutoSize = True
        HoraInicialLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        HoraInicialLabel.ForeColor = System.Drawing.Color.LightSlateGray
        HoraInicialLabel.Location = New System.Drawing.Point(20, 401)
        HoraInicialLabel.Name = "HoraInicialLabel"
        HoraInicialLabel.Size = New System.Drawing.Size(89, 15)
        HoraInicialLabel.TabIndex = 186
        HoraInicialLabel.Text = "Hora Inicial :"
        '
        'FechaLabel
        '
        FechaLabel.AutoSize = True
        FechaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FechaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        FechaLabel.Location = New System.Drawing.Point(239, 403)
        FechaLabel.Name = "FechaLabel"
        FechaLabel.Size = New System.Drawing.Size(54, 15)
        FechaLabel.TabIndex = 190
        FechaLabel.Text = "Fecha :"
        '
        'Clv_trabajoLabel
        '
        Clv_trabajoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_trabajoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_trabajoLabel.Location = New System.Drawing.Point(6, 634)
        Clv_trabajoLabel.Name = "Clv_trabajoLabel"
        Clv_trabajoLabel.Size = New System.Drawing.Size(103, 35)
        Clv_trabajoLabel.TabIndex = 192
        Clv_trabajoLabel.Text = "Clasificación de la Solución"
        '
        'Clave_TecnicoLabel
        '
        Clave_TecnicoLabel.AutoSize = True
        Clave_TecnicoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clave_TecnicoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clave_TecnicoLabel.Location = New System.Drawing.Point(3, 138)
        Clave_TecnicoLabel.Name = "Clave_TecnicoLabel"
        Clave_TecnicoLabel.Size = New System.Drawing.Size(61, 15)
        Clave_TecnicoLabel.TabIndex = 42
        Clave_TecnicoLabel.Text = "Técnico:"
        Clave_TecnicoLabel.Visible = False
        '
        'Clv_TipoQuejaLabel
        '
        Clv_TipoQuejaLabel.AutoSize = True
        Clv_TipoQuejaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_TipoQuejaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_TipoQuejaLabel.Location = New System.Drawing.Point(3, 89)
        Clv_TipoQuejaLabel.Name = "Clv_TipoQuejaLabel"
        Clv_TipoQuejaLabel.Size = New System.Drawing.Size(126, 15)
        Clv_TipoQuejaLabel.TabIndex = 43
        Clv_TipoQuejaLabel.Text = "Tipo de Prioridad :"
        '
        'ClasificacionLabel
        '
        ClasificacionLabel.AutoSize = True
        ClasificacionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ClasificacionLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ClasificacionLabel.Location = New System.Drawing.Point(3, 41)
        ClasificacionLabel.Name = "ClasificacionLabel"
        ClasificacionLabel.Size = New System.Drawing.Size(194, 15)
        ClasificacionLabel.TabIndex = 44
        ClasificacionLabel.Text = "Departamento Responsable :"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.DarkRed
        Label1.Location = New System.Drawing.Point(2, 4)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(265, 15)
        Label1.TabIndex = 52
        Label1.Text = "Capture los Datos para realizar la Queja"
        '
        'Clv_UsuarioLabel1
        '
        Clv_UsuarioLabel1.AutoSize = True
        Clv_UsuarioLabel1.Location = New System.Drawing.Point(139, 805)
        Clv_UsuarioLabel1.Name = "Clv_UsuarioLabel1"
        Clv_UsuarioLabel1.Size = New System.Drawing.Size(64, 13)
        Clv_UsuarioLabel1.TabIndex = 405
        Clv_UsuarioLabel1.Text = "Clv Usuario:"
        '
        'TelefonoLabel
        '
        TelefonoLabel.AutoSize = True
        TelefonoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TelefonoLabel.Location = New System.Drawing.Point(10, 126)
        TelefonoLabel.Name = "TelefonoLabel"
        TelefonoLabel.Size = New System.Drawing.Size(55, 13)
        TelefonoLabel.TabIndex = 19
        TelefonoLabel.Text = "Teléfono :"
        '
        'GeneroLabel
        '
        GeneroLabel.AutoSize = True
        GeneroLabel.Location = New System.Drawing.Point(463, 788)
        GeneroLabel.Name = "GeneroLabel"
        GeneroLabel.Size = New System.Drawing.Size(43, 13)
        GeneroLabel.TabIndex = 407
        GeneroLabel.Text = "genero:"
        '
        'Label6
        '
        Label6.AutoSize = True
        Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label6.ForeColor = System.Drawing.Color.LightSlateGray
        Label6.Location = New System.Drawing.Point(27, 121)
        Label6.Name = "Label6"
        Label6.Size = New System.Drawing.Size(126, 15)
        Label6.TabIndex = 413
        Label6.Text = "Tipo de Atención : "
        '
        'lblTurno
        '
        lblTurno.AutoSize = True
        lblTurno.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        lblTurno.ForeColor = System.Drawing.Color.LightSlateGray
        lblTurno.Location = New System.Drawing.Point(528, 401)
        lblTurno.Name = "lblTurno"
        lblTurno.Size = New System.Drawing.Size(52, 15)
        lblTurno.TabIndex = 415
        lblTurno.Text = "Turno :"
        '
        'lblClasifProblema
        '
        lblClasifProblema.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        lblClasifProblema.ForeColor = System.Drawing.Color.LightSlateGray
        lblClasifProblema.Location = New System.Drawing.Point(3, 503)
        lblClasifProblema.Name = "lblClasifProblema"
        lblClasifProblema.Size = New System.Drawing.Size(106, 40)
        lblClasifProblema.TabIndex = 418
        lblClasifProblema.Text = "Clasificacion del Problema :"
        '
        'ESHOTELLabel1
        '
        Me.ESHOTELLabel1.AutoSize = True
        Me.ESHOTELLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ESHOTELLabel1.ForeColor = System.Drawing.Color.White
        Me.ESHOTELLabel1.Location = New System.Drawing.Point(430, 73)
        Me.ESHOTELLabel1.Name = "ESHOTELLabel1"
        Me.ESHOTELLabel1.Size = New System.Drawing.Size(67, 15)
        Me.ESHOTELLabel1.TabIndex = 17
        Me.ESHOTELLabel1.Text = "Es hotel :"
        '
        'BUSCLIPORCONTRATOBindingSource
        '
        Me.BUSCLIPORCONTRATOBindingSource.DataMember = "BUSCLIPORCONTRATO"
        '
        'MUESTRATRABAJOSBindingSource
        '
        Me.MUESTRATRABAJOSBindingSource.DataMember = "MUESTRATRABAJOS"
        '
        'DameSerDELCliBindingSource
        '
        Me.DameSerDELCliBindingSource.DataMember = "dameSerDELCli"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.DarkOrange
        Me.GroupBox1.Controls.Add(TelefonoLabel)
        Me.GroupBox1.Controls.Add(Me.TelefonoLabel1)
        Me.GroupBox1.Controls.Add(Me.TreeView1)
        Me.GroupBox1.Controls.Add(ServicioLabel)
        Me.GroupBox1.Controls.Add(Me.ESHOTELLabel1)
        Me.GroupBox1.Controls.Add(Me.ESHOTELCheckBox)
        Me.GroupBox1.Controls.Add(SOLOINTERNETLabel1)
        Me.GroupBox1.Controls.Add(Me.SOLOINTERNETCheckBox)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.NOMBRELabel1)
        Me.GroupBox1.Controls.Add(Me.CALLELabel1)
        Me.GroupBox1.Controls.Add(Me.COLONIALabel1)
        Me.GroupBox1.Controls.Add(Me.NUMEROLabel1)
        Me.GroupBox1.Controls.Add(Me.CIUDADLabel1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.White
        Me.GroupBox1.Location = New System.Drawing.Point(113, 193)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(536, 193)
        Me.GroupBox1.TabIndex = 175
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Cliente"
        '
        'TelefonoLabel1
        '
        Me.TelefonoLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConAtenTelCteBindingSource, "Telefono", True))
        Me.TelefonoLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TelefonoLabel1.Location = New System.Drawing.Point(10, 139)
        Me.TelefonoLabel1.Name = "TelefonoLabel1"
        Me.TelefonoLabel1.Size = New System.Drawing.Size(150, 23)
        Me.TelefonoLabel1.TabIndex = 20
        '
        'ConAtenTelCteBindingSource
        '
        Me.ConAtenTelCteBindingSource.DataMember = "ConAtenTelCte"
        Me.ConAtenTelCteBindingSource.DataSource = Me.DataSetEric
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TreeView1
        '
        Me.TreeView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView1.Location = New System.Drawing.Point(166, 97)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(358, 90)
        Me.TreeView1.TabIndex = 19
        Me.TreeView1.TabStop = False
        '
        'ESHOTELCheckBox
        '
        Me.ESHOTELCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ESHOTELCheckBox.Location = New System.Drawing.Point(503, 67)
        Me.ESHOTELCheckBox.Name = "ESHOTELCheckBox"
        Me.ESHOTELCheckBox.Size = New System.Drawing.Size(21, 24)
        Me.ESHOTELCheckBox.TabIndex = 18
        Me.ESHOTELCheckBox.TabStop = False
        '
        'SOLOINTERNETCheckBox
        '
        Me.SOLOINTERNETCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.SOLOINTERNETCheckBox.Location = New System.Drawing.Point(403, 67)
        Me.SOLOINTERNETCheckBox.Name = "SOLOINTERNETCheckBox"
        Me.SOLOINTERNETCheckBox.Size = New System.Drawing.Size(21, 24)
        Me.SOLOINTERNETCheckBox.TabIndex = 17
        Me.SOLOINTERNETCheckBox.TabStop = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(378, 41)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(31, 13)
        Me.Label11.TabIndex = 16
        Me.Label11.Text = "Col. :"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(299, 41)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(20, 13)
        Me.Label10.TabIndex = 15
        Me.Label10.Text = "# :"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(9, 41)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(58, 13)
        Me.Label9.TabIndex = 14
        Me.Label9.Text = "Dirección :"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(10, 18)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(50, 13)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Nombre :"
        '
        'NOMBRELabel1
        '
        Me.NOMBRELabel1.AutoEllipsis = True
        Me.NOMBRELabel1.BackColor = System.Drawing.Color.DarkOrange
        Me.NOMBRELabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NOMBRELabel1.Location = New System.Drawing.Point(69, 18)
        Me.NOMBRELabel1.Name = "NOMBRELabel1"
        Me.NOMBRELabel1.Size = New System.Drawing.Size(467, 23)
        Me.NOMBRELabel1.TabIndex = 3
        '
        'CALLELabel1
        '
        Me.CALLELabel1.AutoEllipsis = True
        Me.CALLELabel1.BackColor = System.Drawing.Color.DarkOrange
        Me.CALLELabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CALLELabel1.Location = New System.Drawing.Point(69, 41)
        Me.CALLELabel1.Name = "CALLELabel1"
        Me.CALLELabel1.Size = New System.Drawing.Size(224, 23)
        Me.CALLELabel1.TabIndex = 5
        '
        'COLONIALabel1
        '
        Me.COLONIALabel1.AutoEllipsis = True
        Me.COLONIALabel1.BackColor = System.Drawing.Color.DarkOrange
        Me.COLONIALabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.COLONIALabel1.Location = New System.Drawing.Point(415, 41)
        Me.COLONIALabel1.Name = "COLONIALabel1"
        Me.COLONIALabel1.Size = New System.Drawing.Size(121, 23)
        Me.COLONIALabel1.TabIndex = 7
        '
        'NUMEROLabel1
        '
        Me.NUMEROLabel1.AutoEllipsis = True
        Me.NUMEROLabel1.BackColor = System.Drawing.Color.DarkOrange
        Me.NUMEROLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NUMEROLabel1.Location = New System.Drawing.Point(325, 41)
        Me.NUMEROLabel1.Name = "NUMEROLabel1"
        Me.NUMEROLabel1.Size = New System.Drawing.Size(47, 23)
        Me.NUMEROLabel1.TabIndex = 9
        '
        'CIUDADLabel1
        '
        Me.CIUDADLabel1.AutoEllipsis = True
        Me.CIUDADLabel1.BackColor = System.Drawing.Color.DarkOrange
        Me.CIUDADLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CIUDADLabel1.Location = New System.Drawing.Point(69, 65)
        Me.CIUDADLabel1.Name = "CIUDADLabel1"
        Me.CIUDADLabel1.Size = New System.Drawing.Size(224, 23)
        Me.CIUDADLabel1.TabIndex = 11
        '
        'BUSCLIPORCONTRATOBindingSource1
        '
        Me.BUSCLIPORCONTRATOBindingSource1.DataMember = "BUSCLIPORCONTRATO"
        Me.BUSCLIPORCONTRATOBindingSource1.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(219, 164)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(30, 22)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "..."
        Me.Button1.UseVisualStyleBackColor = False
        '
        'BUSCLIPORCONTRATOTableAdapter
        '
        Me.BUSCLIPORCONTRATOTableAdapter.ClearBeforeFill = True
        '
        'DameSerDELCliTableAdapter
        '
        Me.DameSerDELCliTableAdapter.ClearBeforeFill = True
        '
        'MUESTRATRABAJOSTableAdapter
        '
        Me.MUESTRATRABAJOSTableAdapter.ClearBeforeFill = True
        '
        'CONllamadasdeinternetBindingSource
        '
        Me.CONllamadasdeinternetBindingSource.DataMember = "CONllamadasdeinternet"
        Me.CONllamadasdeinternetBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CONllamadasdeinternetTableAdapter
        '
        Me.CONllamadasdeinternetTableAdapter.ClearBeforeFill = True
        '
        'Clv_llamadaTextBox
        '
        Me.Clv_llamadaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_llamadaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONllamadasdeinternetBindingSource, "clv_llamada", True))
        Me.Clv_llamadaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_llamadaTextBox.ForeColor = System.Drawing.Color.DarkRed
        Me.Clv_llamadaTextBox.Location = New System.Drawing.Point(164, 40)
        Me.Clv_llamadaTextBox.Name = "Clv_llamadaTextBox"
        Me.Clv_llamadaTextBox.ReadOnly = True
        Me.Clv_llamadaTextBox.Size = New System.Drawing.Size(174, 26)
        Me.Clv_llamadaTextBox.TabIndex = 177
        Me.Clv_llamadaTextBox.TabStop = False
        '
        'DescripcionTextBox
        '
        Me.DescripcionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DescripcionTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.DescripcionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONllamadasdeinternetBindingSource, "Descripcion", True))
        Me.DescripcionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionTextBox.Location = New System.Drawing.Point(115, 428)
        Me.DescripcionTextBox.Multiline = True
        Me.DescripcionTextBox.Name = "DescripcionTextBox"
        Me.DescripcionTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DescripcionTextBox.Size = New System.Drawing.Size(866, 72)
        Me.DescripcionTextBox.TabIndex = 6
        '
        'SolucionTextBox
        '
        Me.SolucionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SolucionTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.SolucionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONllamadasdeinternetBindingSource, "Solucion", True))
        Me.SolucionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SolucionTextBox.Location = New System.Drawing.Point(115, 549)
        Me.SolucionTextBox.Multiline = True
        Me.SolucionTextBox.Name = "SolucionTextBox"
        Me.SolucionTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.SolucionTextBox.Size = New System.Drawing.Size(866, 81)
        Me.SolucionTextBox.TabIndex = 7
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.Panel12)
        Me.Panel1.Controls.Add(Me.cmbClasifProblemas)
        Me.Panel1.Controls.Add(lblClasifProblema)
        Me.Panel1.Controls.Add(Me.btnConsultarCobro)
        Me.Panel1.Controls.Add(Me.Panel4)
        Me.Panel1.Controls.Add(lblTurno)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.cmbxTurno)
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Label6)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.Button8)
        Me.Panel1.Controls.Add(Me.ComboBox1)
        Me.Panel1.Controls.Add(Me.SolucionTextBox)
        Me.Panel1.Controls.Add(Me.HoraInicialMaskedTextBox)
        Me.Panel1.Controls.Add(Me.FECHATextBox1)
        Me.Panel1.Controls.Add(Me.FechaTextBox)
        Me.Panel1.Controls.Add(Me.ContratoTextBox)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Clv_quejaTextBox)
        Me.Panel1.Controls.Add(Me.Button2)
        Me.Panel1.Controls.Add(Me.FechaMaskedTextBox)
        Me.Panel1.Controls.Add(Me.ComboBox5)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.CONllamadasdeinternetBindingNavigator)
        Me.Panel1.Controls.Add(Me.Button3)
        Me.Panel1.Controls.Add(Me.Button4)
        Me.Panel1.Controls.Add(Clv_llamadaLabel)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.Clv_llamadaTextBox)
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(ContratoLabel)
        Me.Panel1.Controls.Add(Clv_trabajoLabel)
        Me.Panel1.Controls.Add(DescripcionLabel)
        Me.Panel1.Controls.Add(Me.DescripcionTextBox)
        Me.Panel1.Controls.Add(FechaLabel)
        Me.Panel1.Controls.Add(SolucionLabel)
        Me.Panel1.Controls.Add(HoraInicialLabel)
        Me.Panel1.Controls.Add(Me.TextBox2)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1002, 691)
        Me.Panel1.TabIndex = 196
        '
        'cmbClasifProblemas
        '
        Me.cmbClasifProblemas.DisplayMember = "descripcion"
        Me.cmbClasifProblemas.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmbClasifProblemas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbClasifProblemas.FormattingEnabled = True
        Me.cmbClasifProblemas.Location = New System.Drawing.Point(115, 514)
        Me.cmbClasifProblemas.Name = "cmbClasifProblemas"
        Me.cmbClasifProblemas.Size = New System.Drawing.Size(422, 23)
        Me.cmbClasifProblemas.TabIndex = 419
        Me.cmbClasifProblemas.ValueMember = "clvProblema"
        '
        'btnConsultarCobro
        '
        Me.btnConsultarCobro.BackColor = System.Drawing.Color.Chocolate
        Me.btnConsultarCobro.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnConsultarCobro.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConsultarCobro.ForeColor = System.Drawing.Color.White
        Me.btnConsultarCobro.Location = New System.Drawing.Point(723, 185)
        Me.btnConsultarCobro.Name = "btnConsultarCobro"
        Me.btnConsultarCobro.Size = New System.Drawing.Size(227, 30)
        Me.btnConsultarCobro.TabIndex = 417
        Me.btnConsultarCobro.Text = "Consultar Cobro"
        Me.btnConsultarCobro.UseVisualStyleBackColor = False
        '
        'Panel4
        '
        Me.Panel4.Location = New System.Drawing.Point(469, 386)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(193, 37)
        Me.Panel4.TabIndex = 416
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Red
        Me.Label5.Location = New System.Drawing.Point(6, 668)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(851, 17)
        Me.Label5.TabIndex = 412
        '
        'cmbxTurno
        '
        Me.cmbxTurno.DisplayMember = "turno"
        Me.cmbxTurno.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbxTurno.FormattingEnabled = True
        Me.cmbxTurno.Location = New System.Drawing.Point(482, 394)
        Me.cmbxTurno.Name = "cmbxTurno"
        Me.cmbxTurno.Size = New System.Drawing.Size(180, 24)
        Me.cmbxTurno.TabIndex = 414
        Me.cmbxTurno.ValueMember = "id"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.EnSitio)
        Me.Panel3.Controls.Add(Me.EnTelefonia)
        Me.Panel3.Location = New System.Drawing.Point(164, 116)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(298, 28)
        Me.Panel3.TabIndex = 0
        '
        'EnSitio
        '
        Me.EnSitio.AutoSize = True
        Me.EnSitio.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EnSitio.Location = New System.Drawing.Point(163, 3)
        Me.EnSitio.Name = "EnSitio"
        Me.EnSitio.Size = New System.Drawing.Size(72, 22)
        Me.EnSitio.TabIndex = 1
        Me.EnSitio.Text = "&En Sitio"
        Me.EnSitio.UseVisualStyleBackColor = True
        '
        'EnTelefonia
        '
        Me.EnTelefonia.AutoSize = True
        Me.EnTelefonia.Checked = True
        Me.EnTelefonia.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EnTelefonia.Location = New System.Drawing.Point(16, 3)
        Me.EnTelefonia.Name = "EnTelefonia"
        Me.EnTelefonia.Size = New System.Drawing.Size(113, 22)
        Me.EnTelefonia.TabIndex = 1
        Me.EnTelefonia.TabStop = True
        Me.EnTelefonia.Text = "&Vía Telefónica"
        Me.EnTelefonia.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Red
        Me.Label4.Location = New System.Drawing.Point(356, 167)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 16)
        Me.Label4.TabIndex = 411
        Me.Label4.Text = "--------"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Red
        Me.Label3.Location = New System.Drawing.Point(293, 167)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(67, 16)
        Me.Label3.TabIndex = 410
        Me.Label3.Text = "Generó :"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel2.Controls.Add(Label1)
        Me.Panel2.Controls.Add(Me.Button7)
        Me.Panel2.Controls.Add(Me.Button6)
        Me.Panel2.Controls.Add(Me.cmbPrioridadQueja)
        Me.Panel2.Controls.Add(Me.Tecnicos)
        Me.Panel2.Controls.Add(Me.ComboBox6)
        Me.Panel2.Controls.Add(Clave_TecnicoLabel)
        Me.Panel2.Controls.Add(Clv_TipoQuejaLabel)
        Me.Panel2.Controls.Add(ClasificacionLabel)
        Me.Panel2.Location = New System.Drawing.Point(678, 222)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(324, 201)
        Me.Panel2.TabIndex = 203
        Me.Panel2.Visible = False
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.DarkRed
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.ForeColor = System.Drawing.Color.White
        Me.Button7.Location = New System.Drawing.Point(161, 155)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(151, 25)
        Me.Button7.TabIndex = 5
        Me.Button7.Text = "&Cancelar Queja"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkRed
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.White
        Me.Button6.Location = New System.Drawing.Point(5, 155)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(151, 25)
        Me.Button6.TabIndex = 4
        Me.Button6.Text = "G&uardar Queja"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'cmbPrioridadQueja
        '
        Me.cmbPrioridadQueja.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmbPrioridadQueja.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbPrioridadQueja.FormattingEnabled = True
        Me.cmbPrioridadQueja.Location = New System.Drawing.Point(5, 112)
        Me.cmbPrioridadQueja.Name = "cmbPrioridadQueja"
        Me.cmbPrioridadQueja.Size = New System.Drawing.Size(307, 23)
        Me.cmbPrioridadQueja.TabIndex = 3
        '
        'Tecnicos
        '
        Me.Tecnicos.DataSource = Me.MuestraTecnicosAlmacenBindingSource
        Me.Tecnicos.DisplayMember = "nombre"
        Me.Tecnicos.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Tecnicos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Tecnicos.FormattingEnabled = True
        Me.Tecnicos.Location = New System.Drawing.Point(6, 156)
        Me.Tecnicos.Name = "Tecnicos"
        Me.Tecnicos.Size = New System.Drawing.Size(10, 23)
        Me.Tecnicos.TabIndex = 2
        Me.Tecnicos.ValueMember = "clv_Tecnico"
        Me.Tecnicos.Visible = False
        '
        'MuestraTecnicosAlmacenBindingSource
        '
        Me.MuestraTecnicosAlmacenBindingSource.DataMember = "Muestra_Tecnicos_Almacen"
        Me.MuestraTecnicosAlmacenBindingSource.DataSource = Me.DataSetarnoldo
        '
        'DataSetarnoldo
        '
        Me.DataSetarnoldo.DataSetName = "DataSetarnoldo"
        Me.DataSetarnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ComboBox6
        '
        Me.ComboBox6.DataSource = Me.MUESTRACLASIFICACIONQUEJASBindingSource
        Me.ComboBox6.DisplayMember = "Concepto"
        Me.ComboBox6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox6.FormattingEnabled = True
        Me.ComboBox6.Location = New System.Drawing.Point(6, 59)
        Me.ComboBox6.Name = "ComboBox6"
        Me.ComboBox6.Size = New System.Drawing.Size(307, 23)
        Me.ComboBox6.TabIndex = 0
        Me.ComboBox6.ValueMember = "Clave"
        '
        'MUESTRACLASIFICACIONQUEJASBindingSource
        '
        Me.MUESTRACLASIFICACIONQUEJASBindingSource.DataMember = "MUESTRACLASIFICACIONQUEJAS"
        Me.MUESTRACLASIFICACIONQUEJASBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.Chocolate
        Me.Button8.Enabled = False
        Me.Button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button8.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.White
        Me.Button8.Location = New System.Drawing.Point(723, 149)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(227, 30)
        Me.Button8.TabIndex = 409
        Me.Button8.Text = "&Historial Quejas"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'ComboBox1
        '
        Me.ComboBox1.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONllamadasdeinternetBindingSource, "clv_trabajo", True))
        Me.ComboBox1.DataSource = Me.MUESTRATRABAJOSQUEJASBindingSource
        Me.ComboBox1.DisplayMember = "Descripcion"
        Me.ComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(115, 641)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(422, 23)
        Me.ComboBox1.TabIndex = 8
        Me.ComboBox1.ValueMember = "Clv_Trabajo"
        '
        'MUESTRATRABAJOSQUEJASBindingSource
        '
        Me.MUESTRATRABAJOSQUEJASBindingSource.DataMember = "MUESTRATRABAJOSQUEJAS"
        Me.MUESTRATRABAJOSQUEJASBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'HoraInicialMaskedTextBox
        '
        Me.HoraInicialMaskedTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.HoraInicialMaskedTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONllamadasdeinternetBindingSource, "HoraInicial", True))
        Me.HoraInicialMaskedTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.HoraInicialMaskedTextBox.Location = New System.Drawing.Point(115, 401)
        Me.HoraInicialMaskedTextBox.Name = "HoraInicialMaskedTextBox"
        Me.HoraInicialMaskedTextBox.Size = New System.Drawing.Size(89, 21)
        Me.HoraInicialMaskedTextBox.TabIndex = 4
        '
        'FECHATextBox1
        '
        Me.FECHATextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DAMEFECHADELSERVIDOR_2BindingSource, "FECHA", True))
        Me.FECHATextBox1.Location = New System.Drawing.Point(327, 556)
        Me.FECHATextBox1.Name = "FECHATextBox1"
        Me.FECHATextBox1.Size = New System.Drawing.Size(100, 20)
        Me.FECHATextBox1.TabIndex = 408
        Me.FECHATextBox1.TabStop = False
        '
        'DAMEFECHADELSERVIDOR_2BindingSource
        '
        Me.DAMEFECHADELSERVIDOR_2BindingSource.DataMember = "DAMEFECHADELSERVIDOR_2"
        Me.DAMEFECHADELSERVIDOR_2BindingSource.DataSource = Me.DataSetEdgarRev2
        '
        'DataSetEdgarRev2
        '
        Me.DataSetEdgarRev2.DataSetName = "DataSetEdgarRev2"
        Me.DataSetEdgarRev2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'FechaTextBox
        '
        Me.FechaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameFechadelServidorHora_2BindingSource, "Fecha", True))
        Me.FechaTextBox.Location = New System.Drawing.Point(206, 556)
        Me.FechaTextBox.Name = "FechaTextBox"
        Me.FechaTextBox.Size = New System.Drawing.Size(82, 20)
        Me.FechaTextBox.TabIndex = 407
        Me.FechaTextBox.TabStop = False
        '
        'DameFechadelServidorHora_2BindingSource
        '
        Me.DameFechadelServidorHora_2BindingSource.DataMember = "DameFechadelServidorHora_2"
        Me.DameFechadelServidorHora_2BindingSource.DataSource = Me.DataSetEdgarRev2
        '
        'ContratoTextBox
        '
        Me.ContratoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ContratoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONllamadasdeinternetBindingSource, "Contrato", True))
        Me.ContratoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContratoTextBox.Location = New System.Drawing.Point(113, 165)
        Me.ContratoTextBox.Name = "ContratoTextBox"
        Me.ContratoTextBox.Size = New System.Drawing.Size(100, 21)
        Me.ContratoTextBox.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Red
        Me.Label2.Location = New System.Drawing.Point(408, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(166, 20)
        Me.Label2.TabIndex = 205
        Me.Label2.Text = "# Queja Generada :"
        Me.Label2.Visible = False
        '
        'Clv_quejaTextBox
        '
        Me.Clv_quejaTextBox.BackColor = System.Drawing.Color.Silver
        Me.Clv_quejaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_quejaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONllamadasdeinternetBindingSource, "clv_queja", True))
        Me.Clv_quejaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_quejaTextBox.ForeColor = System.Drawing.Color.DarkRed
        Me.Clv_quejaTextBox.Location = New System.Drawing.Point(580, 40)
        Me.Clv_quejaTextBox.Name = "Clv_quejaTextBox"
        Me.Clv_quejaTextBox.Size = New System.Drawing.Size(100, 26)
        Me.Clv_quejaTextBox.TabIndex = 204
        Me.Clv_quejaTextBox.TabStop = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Chocolate
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(723, 77)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(227, 30)
        Me.Button2.TabIndex = 11
        Me.Button2.Text = "Generar &Queja"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'FechaMaskedTextBox
        '
        Me.FechaMaskedTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FechaMaskedTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONllamadasdeinternetBindingSource, "Fecha", True))
        Me.FechaMaskedTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FechaMaskedTextBox.Location = New System.Drawing.Point(304, 401)
        Me.FechaMaskedTextBox.Mask = "00/00/0000"
        Me.FechaMaskedTextBox.Name = "FechaMaskedTextBox"
        Me.FechaMaskedTextBox.Size = New System.Drawing.Size(100, 21)
        Me.FechaMaskedTextBox.TabIndex = 5
        '
        'ComboBox5
        '
        Me.ComboBox5.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONllamadasdeinternetBindingSource, "CLV_TIPSER", True))
        Me.ComboBox5.DataSource = Me.MuestraTipSerPrincipalBindingSource
        Me.ComboBox5.DisplayMember = "Concepto"
        Me.ComboBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox5.ForeColor = System.Drawing.Color.Red
        Me.ComboBox5.FormattingEnabled = True
        Me.ComboBox5.Location = New System.Drawing.Point(164, 81)
        Me.ComboBox5.Name = "ComboBox5"
        Me.ComboBox5.Size = New System.Drawing.Size(352, 24)
        Me.ComboBox5.TabIndex = 1
        Me.ComboBox5.ValueMember = "Clv_TipSerPrincipal"
        '
        'MuestraTipSerPrincipalBindingSource
        '
        Me.MuestraTipSerPrincipalBindingSource.DataMember = "MuestraTipSerPrincipal"
        Me.MuestraTipSerPrincipalBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label7.Location = New System.Drawing.Point(17, 84)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(141, 17)
        Me.Label7.TabIndex = 199
        Me.Label7.Text = "Tipo de Servicio  :"
        '
        'CONllamadasdeinternetBindingNavigator
        '
        Me.CONllamadasdeinternetBindingNavigator.AddNewItem = Nothing
        Me.CONllamadasdeinternetBindingNavigator.BindingSource = Me.CONllamadasdeinternetBindingSource
        Me.CONllamadasdeinternetBindingNavigator.CountItem = Nothing
        Me.CONllamadasdeinternetBindingNavigator.DeleteItem = Nothing
        Me.CONllamadasdeinternetBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.CONQUEJASBindingNavigatorSaveItem})
        Me.CONllamadasdeinternetBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONllamadasdeinternetBindingNavigator.MoveFirstItem = Nothing
        Me.CONllamadasdeinternetBindingNavigator.MoveLastItem = Nothing
        Me.CONllamadasdeinternetBindingNavigator.MoveNextItem = Nothing
        Me.CONllamadasdeinternetBindingNavigator.MovePreviousItem = Nothing
        Me.CONllamadasdeinternetBindingNavigator.Name = "CONllamadasdeinternetBindingNavigator"
        Me.CONllamadasdeinternetBindingNavigator.PositionItem = Nothing
        Me.CONllamadasdeinternetBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONllamadasdeinternetBindingNavigator.Size = New System.Drawing.Size(1002, 25)
        Me.CONllamadasdeinternetBindingNavigator.TabIndex = 7
        Me.CONllamadasdeinternetBindingNavigator.TabStop = True
        Me.CONllamadasdeinternetBindingNavigator.Text = "BindingNavigator1"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(79, 22)
        Me.ToolStripButton1.Text = "&CANCELAR"
        '
        'CONQUEJASBindingNavigatorSaveItem
        '
        Me.CONQUEJASBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONQUEJASBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONQUEJASBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONQUEJASBindingNavigatorSaveItem.Name = "CONQUEJASBindingNavigatorSaveItem"
        Me.CONQUEJASBindingNavigatorSaveItem.Size = New System.Drawing.Size(91, 22)
        Me.CONQUEJASBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.Chocolate
        Me.Button3.Enabled = False
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.Location = New System.Drawing.Point(723, 41)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(227, 30)
        Me.Button3.TabIndex = 10
        Me.Button3.Text = "&Imprimir "
        Me.Button3.UseVisualStyleBackColor = False
        Me.Button3.Visible = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.Chocolate
        Me.Button4.Enabled = False
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.White
        Me.Button4.Location = New System.Drawing.Point(723, 113)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(227, 30)
        Me.Button4.TabIndex = 9
        Me.Button4.Text = "&Historial de Pagos"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.TextBox2.Location = New System.Drawing.Point(182, 83)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(280, 19)
        Me.TextBox2.TabIndex = 405
        Me.TextBox2.TabStop = False
        Me.TextBox2.Visible = False
        '
        'MUESTRATIPOQUEJASBindingSource
        '
        Me.MUESTRATIPOQUEJASBindingSource.DataMember = "MUESTRATIPOQUEJAS"
        Me.MUESTRATIPOQUEJASBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CONllamadasdeinternetBindingSource2
        '
        Me.CONllamadasdeinternetBindingSource2.DataMember = "CONllamadasdeinternet"
        Me.CONllamadasdeinternetBindingSource2.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRATECNICOSBindingSource
        '
        Me.MUESTRATECNICOSBindingSource.DataMember = "MUESTRATECNICOS"
        Me.MUESTRATECNICOSBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'Clv_UsuarioTextBox
        '
        Me.Clv_UsuarioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_UsuarioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONllamadasdeinternetBindingSource, "Clv_Usuario", True))
        Me.Clv_UsuarioTextBox.Location = New System.Drawing.Point(209, 805)
        Me.Clv_UsuarioTextBox.Name = "Clv_UsuarioTextBox"
        Me.Clv_UsuarioTextBox.Size = New System.Drawing.Size(100, 13)
        Me.Clv_UsuarioTextBox.TabIndex = 406
        Me.Clv_UsuarioTextBox.TabStop = False
        '
        'MUESTRAUSUARIOSBindingSource
        '
        Me.MUESTRAUSUARIOSBindingSource.DataMember = "MUESTRAUSUARIOS"
        Me.MUESTRAUSUARIOSBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRATRABAJOSBindingSource1
        '
        Me.MUESTRATRABAJOSBindingSource1.DataMember = "MUESTRATRABAJOS"
        Me.MUESTRATRABAJOSBindingSource1.DataSource = Me.NewSofTvDataSet
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(873, 709)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 16
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'MuestraTipSerPrincipalTableAdapter
        '
        Me.MuestraTipSerPrincipalTableAdapter.ClearBeforeFill = True
        '
        'MUESTRATECNICOSTableAdapter
        '
        Me.MUESTRATECNICOSTableAdapter.ClearBeforeFill = True
        '
        'MUESTRATIPOQUEJASTableAdapter
        '
        Me.MUESTRATIPOQUEJASTableAdapter.ClearBeforeFill = True
        '
        'MUESTRACLASIFICACIONQUEJASTableAdapter
        '
        Me.MUESTRACLASIFICACIONQUEJASTableAdapter.ClearBeforeFill = True
        '
        'CONQUEJASTableAdapter
        '
        Me.CONQUEJASTableAdapter.ClearBeforeFill = True
        '
        'CONQUEJASBindingSource
        '
        Me.CONQUEJASBindingSource.DataMember = "CONQUEJAS"
        Me.CONQUEJASBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NUE_CITASBindingSource
        '
        Me.NUE_CITASBindingSource.DataMember = "NUE_CITAS"
        Me.NUE_CITASBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NUE_CITASTableAdapter
        '
        Me.NUE_CITASTableAdapter.ClearBeforeFill = True
        '
        'CONDetCitasTableAdapter
        '
        Me.CONDetCitasTableAdapter.ClearBeforeFill = True
        '
        'CONDetCitasBindingSource
        '
        Me.CONDetCitasBindingSource.DataMember = "CONDetCitas"
        Me.CONDetCitasBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NUEREL_CITAS_QUEJASBindingSource
        '
        Me.NUEREL_CITAS_QUEJASBindingSource.DataMember = "NUEREL_CITAS_QUEJAS"
        Me.NUEREL_CITAS_QUEJASBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NUEREL_CITAS_QUEJASTableAdapter
        '
        Me.NUEREL_CITAS_QUEJASTableAdapter.ClearBeforeFill = True
        '
        'MUESTRATRABAJOSQUEJASTableAdapter
        '
        Me.MUESTRATRABAJOSQUEJASTableAdapter.ClearBeforeFill = True
        '
        'MUESTRAUSUARIOSTableAdapter
        '
        Me.MUESTRAUSUARIOSTableAdapter.ClearBeforeFill = True
        '
        'Muestra_Tecnicos_AlmacenTableAdapter
        '
        Me.Muestra_Tecnicos_AlmacenTableAdapter.ClearBeforeFill = True
        '
        'DameFechadelServidorHora_2TableAdapter
        '
        Me.DameFechadelServidorHora_2TableAdapter.ClearBeforeFill = True
        '
        'DAMEFECHADELSERVIDOR_2TableAdapter
        '
        Me.DAMEFECHADELSERVIDOR_2TableAdapter.ClearBeforeFill = True
        '
        'CONllamadasdeinternetBindingSource1
        '
        Me.CONllamadasdeinternetBindingSource1.DataMember = "CONllamadasdeinternet"
        Me.CONllamadasdeinternetBindingSource1.DataSource = Me.NewSofTvDataSet
        '
        'Dame_Impresora_OrdenesBindingSource
        '
        Me.Dame_Impresora_OrdenesBindingSource.DataMember = "Dame_Impresora_Ordenes"
        Me.Dame_Impresora_OrdenesBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Dame_Impresora_OrdenesTableAdapter
        '
        Me.Dame_Impresora_OrdenesTableAdapter.ClearBeforeFill = True
        '
        'DataSetLidia
        '
        Me.DataSetLidia.DataSetName = "DataSetLidia"
        Me.DataSetLidia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BuscaBloqueadoBindingSource
        '
        Me.BuscaBloqueadoBindingSource.DataMember = "BuscaBloqueado"
        Me.BuscaBloqueadoBindingSource.DataSource = Me.DataSetLidia
        '
        'BuscaBloqueadoTableAdapter
        '
        Me.BuscaBloqueadoTableAdapter.ClearBeforeFill = True
        '
        'NueRelQuejaUsuarioBindingSource
        '
        Me.NueRelQuejaUsuarioBindingSource.DataMember = "NueRelQuejaUsuario"
        Me.NueRelQuejaUsuarioBindingSource.DataSource = Me.DataSetEric
        '
        'NueRelQuejaUsuarioTableAdapter
        '
        Me.NueRelQuejaUsuarioTableAdapter.ClearBeforeFill = True
        '
        'ConAtenTelCteTableAdapter
        '
        Me.ConAtenTelCteTableAdapter.ClearBeforeFill = True
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric2"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ValidaQuejasAtenTelBindingSource
        '
        Me.ValidaQuejasAtenTelBindingSource.DataMember = "ValidaQuejasAtenTel"
        Me.ValidaQuejasAtenTelBindingSource.DataSource = Me.DataSetEric2
        '
        'ValidaQuejasAtenTelTableAdapter
        '
        Me.ValidaQuejasAtenTelTableAdapter.ClearBeforeFill = True
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'InsertaRel_AtencionTelUsuarioBindingSource
        '
        Me.InsertaRel_AtencionTelUsuarioBindingSource.DataMember = "InsertaRel_AtencionTelUsuario"
        Me.InsertaRel_AtencionTelUsuarioBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'InsertaRel_AtencionTelUsuarioTableAdapter
        '
        Me.InsertaRel_AtencionTelUsuarioTableAdapter.ClearBeforeFill = True
        '
        'Consulta_Rel_AtencionTelUsuarioBindingSource
        '
        Me.Consulta_Rel_AtencionTelUsuarioBindingSource.DataMember = "Consulta_Rel_AtencionTelUsuario"
        Me.Consulta_Rel_AtencionTelUsuarioBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Consulta_Rel_AtencionTelUsuarioTableAdapter
        '
        Me.Consulta_Rel_AtencionTelUsuarioTableAdapter.ClearBeforeFill = True
        '
        'GeneroTextBox
        '
        Me.GeneroTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_Rel_AtencionTelUsuarioBindingSource, "genero", True))
        Me.GeneroTextBox.Location = New System.Drawing.Point(512, 785)
        Me.GeneroTextBox.Name = "GeneroTextBox"
        Me.GeneroTextBox.Size = New System.Drawing.Size(100, 20)
        Me.GeneroTextBox.TabIndex = 408
        '
        'BuscaSiTieneQuejaBindingSource
        '
        Me.BuscaSiTieneQuejaBindingSource.DataMember = "BuscaSiTieneQueja"
        Me.BuscaSiTieneQuejaBindingSource.DataSource = Me.DataSetEric2
        '
        'BuscaSiTieneQuejaTableAdapter
        '
        Me.BuscaSiTieneQuejaTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter5
        '
        Me.Muestra_ServiciosDigitalesTableAdapter5.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter6
        '
        Me.Muestra_ServiciosDigitalesTableAdapter6.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter7
        '
        Me.Muestra_ServiciosDigitalesTableAdapter7.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter8
        '
        Me.Muestra_ServiciosDigitalesTableAdapter8.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter9
        '
        Me.Muestra_ServiciosDigitalesTableAdapter9.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter10
        '
        Me.Muestra_ServiciosDigitalesTableAdapter10.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter11
        '
        Me.Muestra_ServiciosDigitalesTableAdapter11.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter12
        '
        Me.Muestra_ServiciosDigitalesTableAdapter12.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter13
        '
        Me.Muestra_ServiciosDigitalesTableAdapter13.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter14
        '
        Me.Muestra_ServiciosDigitalesTableAdapter14.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter15
        '
        Me.Muestra_ServiciosDigitalesTableAdapter15.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter16
        '
        Me.Muestra_ServiciosDigitalesTableAdapter16.ClearBeforeFill = True
        '
        'Panel12
        '
        Me.Panel12.Controls.Add(Me.TxtRefOxxo)
        Me.Panel12.Controls.Add(Me.Label19)
        Me.Panel12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel12.Location = New System.Drawing.Point(469, 116)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(248, 27)
        Me.Panel12.TabIndex = 463
        '
        'TxtRefOxxo
        '
        Me.TxtRefOxxo.Location = New System.Drawing.Point(93, 2)
        Me.TxtRefOxxo.Name = "TxtRefOxxo"
        Me.TxtRefOxxo.Size = New System.Drawing.Size(152, 21)
        Me.TxtRefOxxo.TabIndex = 1
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label19.Location = New System.Drawing.Point(10, 5)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(77, 15)
        Me.Label19.TabIndex = 0
        Me.Label19.Text = "Ref. Oxxo :"
        '
        'FrmQuejasAntTel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1016, 747)
        Me.Controls.Add(GeneroLabel)
        Me.Controls.Add(Me.GeneroTextBox)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Clv_UsuarioTextBox)
        Me.Controls.Add(Clv_UsuarioLabel1)
        Me.MaximizeBox = False
        Me.Name = "FrmQuejasAntTel"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Atención Clientes"
        CType(Me.BUSCLIPORCONTRATOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRATRABAJOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameSerDELCliBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.ConAtenTelCteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BUSCLIPORCONTRATOBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONllamadasdeinternetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.MuestraTecnicosAlmacenBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACLASIFICACIONQUEJASBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRATRABAJOSQUEJASBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAMEFECHADELSERVIDOR_2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameFechadelServidorHora_2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTipSerPrincipalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONllamadasdeinternetBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONllamadasdeinternetBindingNavigator.ResumeLayout(False)
        Me.CONllamadasdeinternetBindingNavigator.PerformLayout()
        CType(Me.MUESTRATIPOQUEJASBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONllamadasdeinternetBindingSource2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRATECNICOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAUSUARIOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRATRABAJOSBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONQUEJASBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NUE_CITASBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONDetCitasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NUEREL_CITAS_QUEJASBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONllamadasdeinternetBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_Impresora_OrdenesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BuscaBloqueadoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NueRelQuejaUsuarioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ValidaQuejasAtenTelBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertaRel_AtencionTelUsuarioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_Rel_AtencionTelUsuarioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BuscaSiTieneQuejaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel12.ResumeLayout(False)
        Me.Panel12.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BUSCLIPORCONTRATOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BUSCLIPORCONTRATOTableAdapter As sofTV.NewSofTvDataSetTableAdapters.BUSCLIPORCONTRATOTableAdapter
    Friend WithEvents MUESTRATRABAJOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameSerDELCliTableAdapter As sofTV.NewSofTvDataSetTableAdapters.dameSerDELCliTableAdapter
    Friend WithEvents DameSerDELCliBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRATRABAJOSTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRATRABAJOSTableAdapter
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents ESHOTELCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents SOLOINTERNETCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents NOMBRELabel1 As System.Windows.Forms.Label
    Friend WithEvents CALLELabel1 As System.Windows.Forms.Label
    Friend WithEvents COLONIALabel1 As System.Windows.Forms.Label
    Friend WithEvents NUMEROLabel1 As System.Windows.Forms.Label
    Friend WithEvents CIUDADLabel1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONllamadasdeinternetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONllamadasdeinternetTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONllamadasdeinternetTableAdapter
    Friend WithEvents Clv_llamadaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DescripcionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SolucionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents CONllamadasdeinternetBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONQUEJASBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents MUESTRATRABAJOSBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents BUSCLIPORCONTRATOBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents ComboBox5 As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents MuestraTipSerPrincipalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipSerPrincipalTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MuestraTipSerPrincipalTableAdapter
    Friend WithEvents FechaMaskedTextBox As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents cmbPrioridadQueja As System.Windows.Forms.ComboBox
    Friend WithEvents Tecnicos As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox6 As System.Windows.Forms.ComboBox
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents MUESTRATECNICOSTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRATECNICOSTableAdapter
    Friend WithEvents MUESTRATECNICOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRATIPOQUEJASTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRATIPOQUEJASTableAdapter
    Friend WithEvents MUESTRATIPOQUEJASBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACLASIFICACIONQUEJASTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACLASIFICACIONQUEJASTableAdapter
    Friend WithEvents CONQUEJASTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONQUEJASTableAdapter
    Friend WithEvents CONQUEJASBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACLASIFICACIONQUEJASBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Clv_quejaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents NUE_CITASBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NUE_CITASTableAdapter As sofTV.NewSofTvDataSetTableAdapters.NUE_CITASTableAdapter
    Friend WithEvents CONDetCitasTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONDetCitasTableAdapter
    Friend WithEvents CONDetCitasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NUEREL_CITAS_QUEJASBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NUEREL_CITAS_QUEJASTableAdapter As sofTV.NewSofTvDataSetTableAdapters.NUEREL_CITAS_QUEJASTableAdapter
    Friend WithEvents MUESTRATRABAJOSQUEJASBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRATRABAJOSQUEJASTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRATRABAJOSQUEJASTableAdapter
    Friend WithEvents MUESTRAUSUARIOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAUSUARIOSTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRAUSUARIOSTableAdapter
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Clv_UsuarioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MuestraTecnicosAlmacenBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DataSetarnoldo As sofTV.DataSetarnoldo
    Friend WithEvents Muestra_Tecnicos_AlmacenTableAdapter As sofTV.DataSetarnoldoTableAdapters.Muestra_Tecnicos_AlmacenTableAdapter
    Friend WithEvents DataSetEdgarRev2 As sofTV.DataSetEdgarRev2
    Friend WithEvents DameFechadelServidorHora_2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameFechadelServidorHora_2TableAdapter As sofTV.DataSetEdgarRev2TableAdapters.DameFechadelServidorHora_2TableAdapter
    Friend WithEvents FechaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents HoraInicialMaskedTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DAMEFECHADELSERVIDOR_2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMEFECHADELSERVIDOR_2TableAdapter As sofTV.DataSetEdgarRev2TableAdapters.DAMEFECHADELSERVIDOR_2TableAdapter
    Friend WithEvents FECHATextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents CONllamadasdeinternetBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents CONllamadasdeinternetBindingSource2 As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_Impresora_OrdenesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_Impresora_OrdenesTableAdapter As sofTV.DataSetarnoldoTableAdapters.Dame_Impresora_OrdenesTableAdapter
    Friend WithEvents DataSetLidia As sofTV.DataSetLidia
    Friend WithEvents BuscaBloqueadoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BuscaBloqueadoTableAdapter As sofTV.DataSetLidiaTableAdapters.BuscaBloqueadoTableAdapter
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents NueRelQuejaUsuarioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NueRelQuejaUsuarioTableAdapter As sofTV.DataSetEricTableAdapters.NueRelQuejaUsuarioTableAdapter
    Friend WithEvents ConAtenTelCteBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConAtenTelCteTableAdapter As sofTV.DataSetEricTableAdapters.ConAtenTelCteTableAdapter
    Friend WithEvents TelefonoLabel1 As System.Windows.Forms.Label
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric2
    Friend WithEvents ValidaQuejasAtenTelBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ValidaQuejasAtenTelTableAdapter As sofTV.DataSetEric2TableAdapters.ValidaQuejasAtenTelTableAdapter
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents InsertaRel_AtencionTelUsuarioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertaRel_AtencionTelUsuarioTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.InsertaRel_AtencionTelUsuarioTableAdapter
    Friend WithEvents Consulta_Rel_AtencionTelUsuarioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_Rel_AtencionTelUsuarioTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Consulta_Rel_AtencionTelUsuarioTableAdapter
    Friend WithEvents GeneroTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents BuscaSiTieneQuejaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BuscaSiTieneQuejaTableAdapter As sofTV.DataSetEric2TableAdapters.BuscaSiTieneQuejaTableAdapter
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents ContratoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ESHOTELLabel1 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents EnSitio As System.Windows.Forms.RadioButton
    Friend WithEvents EnTelefonia As System.Windows.Forms.RadioButton
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents cmbxTurno As System.Windows.Forms.ComboBox
    Friend WithEvents btnConsultarCobro As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents cmbClasifProblemas As System.Windows.Forms.ComboBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter5 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter6 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter7 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter8 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter9 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter10 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter11 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter12 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter13 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter14 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter15 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter16 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Panel12 As System.Windows.Forms.Panel
    Friend WithEvents TxtRefOxxo As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
End Class
