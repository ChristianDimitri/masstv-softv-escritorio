<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRangosComisiones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmRangosComisiones))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.ConCatalogosRangosPrecioBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton
        Me.ConCatalogosRangosPrecioBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton
        Me.Button1 = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        Me.ComboBox3 = New System.Windows.Forms.ComboBox
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.Clv_Comision = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Clv_Grupo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Clv_Servicio = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Clv_Rango = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Grupo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Comision = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Rango = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ComboBox2 = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        CType(Me.ConCatalogosRangosPrecioBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ConCatalogosRangosPrecioBindingNavigator.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ConCatalogosRangosPrecioBindingNavigator
        '
        Me.ConCatalogosRangosPrecioBindingNavigator.AddNewItem = Nothing
        Me.ConCatalogosRangosPrecioBindingNavigator.CountItem = Nothing
        Me.ConCatalogosRangosPrecioBindingNavigator.DeleteItem = Nothing
        Me.ConCatalogosRangosPrecioBindingNavigator.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConCatalogosRangosPrecioBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.ConCatalogosRangosPrecioBindingNavigatorSaveItem})
        Me.ConCatalogosRangosPrecioBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.ConCatalogosRangosPrecioBindingNavigator.MoveFirstItem = Nothing
        Me.ConCatalogosRangosPrecioBindingNavigator.MoveLastItem = Nothing
        Me.ConCatalogosRangosPrecioBindingNavigator.MoveNextItem = Nothing
        Me.ConCatalogosRangosPrecioBindingNavigator.MovePreviousItem = Nothing
        Me.ConCatalogosRangosPrecioBindingNavigator.Name = "ConCatalogosRangosPrecioBindingNavigator"
        Me.ConCatalogosRangosPrecioBindingNavigator.PositionItem = Nothing
        Me.ConCatalogosRangosPrecioBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ConCatalogosRangosPrecioBindingNavigator.Size = New System.Drawing.Size(529, 25)
        Me.ConCatalogosRangosPrecioBindingNavigator.TabIndex = 130
        Me.ConCatalogosRangosPrecioBindingNavigator.TabStop = True
        Me.ConCatalogosRangosPrecioBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(88, 22)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ConCatalogosRangosPrecioBindingNavigatorSaveItem
        '
        Me.ConCatalogosRangosPrecioBindingNavigatorSaveItem.Image = CType(resources.GetObject("ConCatalogosRangosPrecioBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.ConCatalogosRangosPrecioBindingNavigatorSaveItem.Name = "ConCatalogosRangosPrecioBindingNavigatorSaveItem"
        Me.ConCatalogosRangosPrecioBindingNavigatorSaveItem.Size = New System.Drawing.Size(88, 22)
        Me.ConCatalogosRangosPrecioBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(381, 463)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 140
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label3.Location = New System.Drawing.Point(126, 115)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 15)
        Me.Label3.TabIndex = 143
        Me.Label3.Text = "Rango:"
        '
        'ComboBox3
        '
        Me.ComboBox3.DisplayMember = "Rango"
        Me.ComboBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox3.FormattingEnabled = True
        Me.ComboBox3.Location = New System.Drawing.Point(185, 107)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(152, 23)
        Me.ComboBox3.TabIndex = 146
        Me.ComboBox3.ValueMember = "CveRango"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_Comision, Me.Clv_Grupo, Me.Clv_Servicio, Me.Clv_Rango, Me.Grupo, Me.Comision, Me.Descripcion, Me.Rango})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView1.DefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridView1.Location = New System.Drawing.Point(68, 156)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(395, 272)
        Me.DataGridView1.TabIndex = 147
        '
        'Clv_Comision
        '
        Me.Clv_Comision.DataPropertyName = "Clv_Comision"
        Me.Clv_Comision.HeaderText = "Comision"
        Me.Clv_Comision.Name = "Clv_Comision"
        Me.Clv_Comision.Visible = False
        '
        'Clv_Grupo
        '
        Me.Clv_Grupo.DataPropertyName = "Clv_Grupo"
        Me.Clv_Grupo.HeaderText = "Clv_Grupo"
        Me.Clv_Grupo.Name = "Clv_Grupo"
        Me.Clv_Grupo.Visible = False
        '
        'Clv_Servicio
        '
        Me.Clv_Servicio.DataPropertyName = "Clv_Servicio"
        Me.Clv_Servicio.HeaderText = "Clv_Servicio"
        Me.Clv_Servicio.Name = "Clv_Servicio"
        Me.Clv_Servicio.Visible = False
        '
        'Clv_Rango
        '
        Me.Clv_Rango.DataPropertyName = "Clv_Rango"
        Me.Clv_Rango.HeaderText = "Clv_Rango"
        Me.Clv_Rango.Name = "Clv_Rango"
        Me.Clv_Rango.Visible = False
        '
        'Grupo
        '
        Me.Grupo.DataPropertyName = "Grupo"
        Me.Grupo.HeaderText = "Grupo"
        Me.Grupo.Name = "Grupo"
        Me.Grupo.Width = 200
        '
        'Comision
        '
        Me.Comision.DataPropertyName = "Comision"
        DataGridViewCellStyle2.Format = "C2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.Comision.DefaultCellStyle = DataGridViewCellStyle2
        Me.Comision.HeaderText = "Comisión"
        Me.Comision.Name = "Comision"
        Me.Comision.Width = 120
        '
        'Descripcion
        '
        Me.Descripcion.DataPropertyName = "Descripcion"
        Me.Descripcion.HeaderText = "Servicio"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.Visible = False
        '
        'Rango
        '
        Me.Rango.DataPropertyName = "Rango"
        Me.Rango.HeaderText = "Rango"
        Me.Rango.Name = "Rango"
        Me.Rango.Visible = False
        '
        'ComboBox2
        '
        Me.ComboBox2.DisplayMember = "Descripcion"
        Me.ComboBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(185, 78)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(259, 23)
        Me.ComboBox2.TabIndex = 151
        Me.ComboBox2.ValueMember = "Clv_Servicio"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label2.Location = New System.Drawing.Point(117, 86)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(62, 15)
        Me.Label2.TabIndex = 150
        Me.Label2.Text = "Servicio:"
        '
        'ComboBox1
        '
        Me.ComboBox1.DisplayMember = "Concepto"
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(185, 49)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(259, 23)
        Me.ComboBox1.TabIndex = 149
        Me.ComboBox1.ValueMember = "Clv_TipSer"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label1.Location = New System.Drawing.Point(65, 57)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(114, 15)
        Me.Label1.TabIndex = 148
        Me.Label1.Text = "Tipo de Servicio:"
        '
        'FrmRangosComisiones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(529, 511)
        Me.Controls.Add(Me.ComboBox2)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.ComboBox3)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ConCatalogosRangosPrecioBindingNavigator)
        Me.Name = "FrmRangosComisiones"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Comisiones por Servicio"
        CType(Me.ConCatalogosRangosPrecioBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ConCatalogosRangosPrecioBindingNavigator.ResumeLayout(False)
        Me.ConCatalogosRangosPrecioBindingNavigator.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ConCatalogosRangosPrecioBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ConCatalogosRangosPrecioBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Clv_Comision As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Grupo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Servicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Rango As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Grupo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Comision As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Rango As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
