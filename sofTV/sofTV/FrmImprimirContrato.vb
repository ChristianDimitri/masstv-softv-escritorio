Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic


Public Class FrmImprimirContrato
    Private customersByCityReport As ReportDocument
    Private bndrepmensulidad1 As Boolean = False
    Private bndrepcombo As Boolean = False
    Private op As String = Nothing
    Private Titulo As String = Nothing
    Delegate Sub Reporte()


    Private Sub ConfigureCrystalReportsUsp_Genera_ReportedeClientesxsd()
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo


        Dim reportPath As String = Nothing
        Dim Titulo As String = Nothing
        Dim Sucursal As String = Nothing
        Dim Ciudades As String = Nothing

        Ciudades = " Ciudad(es): " + LocCiudades
        reportPath = RutaReportes + "\Reporte_Usp_Genera_ReportedeClientes.rpt"
        Dim cnn As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("Usp_Genera_ReportedeClientes", cnn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        Dim parametro1 As New SqlParameter("@Mes1", SqlDbType.Int)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = GloMezclaMes1
        cmd.Parameters.Add(parametro1)
        Dim parametro2 As New SqlParameter("@Ano1", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = GloMezclaAno1
        cmd.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Mes2", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = GloMezclaMes2
        cmd.Parameters.Add(parametro3)
        Dim parametro4 As New SqlParameter("@Ano2", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = GloMezclaAno2
        cmd.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@ClvTipSer", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = gloClvTipSerReportes
        cmd.Parameters.Add(parametro5)

        Dim da As New SqlDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        ds.Tables(0).TableName = "Usp_Genera_ReportedeClientes"
        ds.Tables(1).TableName = "Tabla_Detalle_Derecho"
        ds.Tables(2).TableName = "Tabla_Detalle_Inferior"
        ds.Tables(3).TableName = "Tabla_Detalle_Inferior_Total"
        customersByCityReport.Load(reportPath)
        Sucursal = "Cantidad de Clientes por Mezcla de Servicios"
        customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Sucursal & "'"

        Sucursal = "Desde " & MonthName(GloMezclaMes1) & "-" & GloMezclaAno1 & "  Hasta " & MonthName(GloMezclaMes2) & "-" & GloMezclaAno2
        customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & Sucursal & "'"
        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & GloCiudadEmpresa & "'"
        'customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'"
        customersByCityReport.SetDataSource(ds)
        CrystalReportViewer1.ReportSource = customersByCityReport
        CrystalReportViewer1.Zoom(100)
        'If GloOpFacturas = 3 Then
        CrystalReportViewer1.ShowExportButton = True
        CrystalReportViewer1.ShowPrintButton = True
        CrystalReportViewer1.ShowRefreshButton = True
        'End If

    End Sub

    Private Sub ConfigureCrystalReportsUsp_Reporte_tbl_Resultado_Cajasxsd()
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo


        Dim reportPath As String = Nothing
        Dim Titulo As String = Nothing
        Dim Sucursal As String = Nothing
        Dim Ciudades As String = Nothing

        Ciudades = " Ciudad(es): " + LocCiudades
        reportPath = RutaReportes + "\Reporte_Tbl_Resultado_Cajas.rpt"
        Dim cnn As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("Usp_Reporte_tbl_Resultado_Cajas", cnn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0

        Dim parametro1 As New SqlParameter("@clvTipSer", SqlDbType.Int)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = gloClvTipSerReportes
        cmd.Parameters.Add(parametro1)

        Dim da As New SqlDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        ds.Tables(0).TableName = "tbl_Resultado_Cajas"
        ds.Tables(1).TableName = "General"

        customersByCityReport.Load(reportPath)
        customersByCityReport.SetDataSource(ds)

        CrystalReportViewer1.ReportSource = customersByCityReport
        CrystalReportViewer1.Zoom(100)

        CrystalReportViewer1.ShowExportButton = True
        CrystalReportViewer1.ShowPrintButton = True
        CrystalReportViewer1.ShowRefreshButton = True
    End Sub

    Private Sub ConfigureCrystalReportsListadoInstExt(ByVal op As String, ByVal Titulo As String)
        customersByCityReport = New ReportDocument
        Dim cnn As New SqlConnection(MiConexion)
        Try

            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0", Op6 As String = "0"
            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
            Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"
            Dim Status As String = Nothing
            Dim clave_session As Long


            Dim reportPath As String = Nothing


            reportPath = RutaReportes + "\ReporteOrdenesListadoNuevoEx.rpt"


            Dim cmd As New SqlCommand("ReporteAreaTecnicaOrdSer_Prueba_Listado_Extenciones", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0

            '@Clv_TipSer int
            Dim parametro1 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
            parametro1.Direction = ParameterDirection.Input
            parametro1.Value = 0
            cmd.Parameters.Add(parametro1)
            '@op1 smallint
            Dim parametro2 As New SqlParameter("@op1", SqlDbType.SmallInt)
            parametro2.Direction = ParameterDirection.Input
            parametro2.Value = Op1
            cmd.Parameters.Add(parametro2)
            '@op2 smallint
            Dim parametro3 As New SqlParameter("@op2", SqlDbType.SmallInt)
            parametro3.Direction = ParameterDirection.Input
            parametro3.Value = Op2
            cmd.Parameters.Add(parametro3)
            '@op3 smallint
            Dim parametro4 As New SqlParameter("@op3", SqlDbType.SmallInt)
            parametro4.Direction = ParameterDirection.Input
            parametro4.Value = Op3
            cmd.Parameters.Add(parametro4)
            '@op4 smallint
            Dim parametro5 As New SqlParameter("@op4", SqlDbType.SmallInt)
            parametro5.Direction = ParameterDirection.Input
            parametro5.Value = Op4
            cmd.Parameters.Add(parametro5)
            '@op5 smallint
            Dim parametro6 As New SqlParameter("@op5", SqlDbType.SmallInt)
            parametro6.Direction = ParameterDirection.Input
            parametro6.Value = Op5
            cmd.Parameters.Add(parametro6)
            '@StatusPen bit
            Dim parametro7 As New SqlParameter("@StatusPen", SqlDbType.Bit)
            parametro7.Direction = ParameterDirection.Input
            If StatusPen = "1" Then
                parametro7.Value = True
            Else
                parametro7.Value = False
            End If
            cmd.Parameters.Add(parametro7)
            '@StatusEje bit
            Dim parametro8 As New SqlParameter("@StatusEje", SqlDbType.Bit)
            parametro8.Direction = ParameterDirection.Input
            If StatusEje = "1" Then
                parametro8.Value = True
            Else
                parametro8.Value = False
            End If
            cmd.Parameters.Add(parametro8)
            '@StatusVis bit
            Dim parametro9 As New SqlParameter("@StatusVis", SqlDbType.Bit)
            parametro9.Direction = ParameterDirection.Input
            If StatusVis = "1" Then
                parametro9.Value = True
            Else
                parametro9.Value = False
            End If
            cmd.Parameters.Add(parametro9)
            '@Clv_OrdenIni bigint

            Dim parametro10 As New SqlParameter("@Clv_OrdenIni", SqlDbType.BigInt)
            parametro10.Direction = ParameterDirection.Input
            parametro10.Value = 0
            cmd.Parameters.Add(parametro10)
            '@Clv_OrdenFin bigint

            Dim parametro11 As New SqlParameter("@Clv_OrdenFin", SqlDbType.BigInt)
            parametro11.Direction = ParameterDirection.Input
            parametro11.Value = 0
            cmd.Parameters.Add(parametro11)
            '@Fec1Ini Datetime

            Dim parametro12 As New SqlParameter("@Fec1Ini", SqlDbType.DateTime)
            parametro12.Direction = ParameterDirection.Input
            parametro12.Value = "01/01/1900"
            cmd.Parameters.Add(parametro12)

            Dim parametro13 As New SqlParameter("@Fec1Fin", SqlDbType.DateTime)
            parametro13.Direction = ParameterDirection.Input
            parametro13.Value = "01/01/1900"
            cmd.Parameters.Add(parametro13)

            Dim parametro14 As New SqlParameter("@Fec2Ini", SqlDbType.DateTime)
            parametro14.Direction = ParameterDirection.Input
            parametro14.Value = "01/01/1900"
            cmd.Parameters.Add(parametro14)

            Dim parametro15 As New SqlParameter("@Fec2Fin", SqlDbType.DateTime)
            parametro15.Direction = ParameterDirection.Input
            parametro15.Value = "01/01/1900"
            cmd.Parameters.Add(parametro15)
            '@Clv_Trabajo int

            Dim parametro16 As New SqlParameter("@Clv_Trabajo", SqlDbType.Int)
            parametro16.Direction = ParameterDirection.Input
            parametro16.Value = 0
            cmd.Parameters.Add(parametro16)
            '@Clv_Colonia int

            Dim parametro17 As New SqlParameter("@Clv_Colonia", SqlDbType.Int)
            parametro17.Direction = ParameterDirection.Input
            parametro17.Value = 0
            cmd.Parameters.Add(parametro17)
            '@OpOrden int
            Dim parametro18 As New SqlParameter("@OpOrden", SqlDbType.Int)
            parametro18.Direction = ParameterDirection.Input
            parametro18.Value = 3
            cmd.Parameters.Add(parametro18)

            Dim parametro19 As New SqlParameter("@numeroVisitas", SqlDbType.Int)
            parametro19.Direction = ParameterDirection.Input
            parametro19.Value = 0
            cmd.Parameters.Add(parametro19)

            Dim parametro20 As New SqlParameter("@OP6", SqlDbType.SmallInt)
            parametro20.Direction = ParameterDirection.Input
            parametro20.Value = 0
            cmd.Parameters.Add(parametro20)


            Dim parametro21 As New SqlParameter("@ResVisita", SqlDbType.Int)
            parametro21.Direction = ParameterDirection.Output
            cmd.Parameters.Add(parametro21)

            Dim parametro22 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
            parametro22.Direction = ParameterDirection.Input
            parametro22.Value = LocClv_session
            cmd.Parameters.Add(parametro22)

            Dim parametro23 As New SqlParameter("@clvDepto", SqlDbType.BigInt)
            parametro23.Direction = ParameterDirection.Input
            parametro23.Value = 0
            cmd.Parameters.Add(parametro23)

            Dim parametro24 As New SqlParameter("@clvTipoServicioDigital", SqlDbType.BigInt)
            parametro24.Direction = ParameterDirection.Input
            parametro24.Value = gloClvTipSerReportes
            cmd.Parameters.Add(parametro24)

            Dim NumVisitas As String

            cnn.Open()
            cmd.ExecuteNonQuery()
            NumVisitas = parametro21.Value

            clave_session = DAMESclv_Sessionporfavor()


            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet()
            da.Fill(ds)

            ds.Tables(0).TableName = "ReporteAreaTecnicaOrdSer_Prueba_Listado"
            Dim variable As String
            variable = "Sin Filtro  #  Visitas de Tecnico"
            'ds.Tables.Add(data1)
            customersByCityReport.Load(reportPath)
            Dim mySelectFormula As String = ""
            If gloClvTipSerReportes = 3 Then
                mySelectFormula = "Listado de Ordenes de Servicio TV Digital MMDS"
            ElseIf gloClvTipSerReportes = 6 Then
                mySelectFormula = "Listado de Ordenes de Servicio TV Digital Cable"
            Else
                mySelectFormula = "Listado de Ordenes de Servicio"
            End If
            'If op = 0 Then
            customersByCityReport.DataDefinition.FormulaFields("empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloSucursal & "'"
            'If NumeroVisitas > 0 Then
            '    customersByCityReport.DataDefinition.FormulaFields("NumeroVisitas").Text = "'" & NumVisitas & "'"
            'Else
            '    customersByCityReport.DataDefinition.FormulaFields("NumeroVisitas").Text = "'" & variable & "'"
            'End If

            'SetDBReport(ds, customersByCityReport)

            'If Me.CrystalReportViewer1.InvokeRequired Then
            '    Dim d As New Reporte(AddressOf ConfigureCrystalReportsListadoInstExt)
            '    Me.Invoke(d, New Object() {op, Titulo})
            'Else
            customersByCityReport.SetDataSource(ds)
            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(100)
            'If GloOpFacturas = 3 Then
            CrystalReportViewer1.ShowExportButton = True
            CrystalReportViewer1.ShowPrintButton = True
            CrystalReportViewer1.ShowRefreshButton = True

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            cnn.Close()
            cnn.Dispose()
        End Try
        gloClvTipSerReportes = 0
    End Sub

    Private Sub FrmImprimirContrato_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If bndrepmensulidad1 = True Then
            bndrepmensulidad1 = False
            Borra_Tablas_Reporte_mensualidades(LocClv_session)
            '//End If
        ElseIf bndrepcombo = True Then
            bndrepcombo = False
            borra_tablas_Reporte_clientes_combo(LocClv_session)
            'End If
        ElseIf LEdo_Cuenta = True Then
            LEdo_Cuenta = False
            FrmEstado_Cuenta.Borrar()
        End If
        gloClvTipSerReportes = 0
    End Sub

    Public Sub borra_tablas_Reporte_clientes_combo(ByVal clv_session As Long)
        Dim con As New SqlClient.SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Try
            con.Open()
            cmd = New SqlClient.SqlCommand()
            With cmd
                .CommandText = "Borra_Tablas_Clientes_combo"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = con

                Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = clv_session
                .Parameters.Add(prm)

                Dim z As Integer = cmd.ExecuteNonQuery()

            End With
            con.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FrmImprimirContrato_HelpRequested(ByVal sender As Object, ByVal hlpevent As System.Windows.Forms.HelpEventArgs) Handles Me.HelpRequested

    End Sub
    Private Sub FrmImprimirContrato_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim conlidia As New SqlConnection(MiConexion)
        If opcionarea = 999 Then
            opcionarea = 0
            ConfigureCrystalReportsListadoInstExt("0", "")
            'SAUL
        ElseIf opcionarea = 9999 Then
            opcionarea = 0
            ConfigureCrystalReportOrdenesInsExt()
        ElseIf bndreportetec = True And GloRepTecnicos = 2 Then
            bndreportetec = False
            GloRepTecnicos = 0
            ReporteInstalacionesTecnico()
            'SAUL(FIN)
        ElseIf LocOp = 352 Then
            LocOp = 0
            ConfigureCrystalReportsUsp_Reporte_tbl_Resultado_Cajasxsd()
        ElseIf LocOp = 350 Then
            LocOp = 0
            ConfigureCrystalReportsUsp_Genera_ReportedeClientesxsd()
        ElseIf (Rpt_ProductividadTecnico) Then
            ReporteDeProductividadTecnico()
            Rpt_ProductividadTecnico = False
        Else
            If LEdo_Cuenta = True Or LEdo_Cuenta2 = True Then
                'LEdo_Cuenta = False
                conlidia.Open()
                Dim CMd As New SqlClient.SqlCommand
                With CMd

                    '@orden int,@Habilita int,@periodo1 bit,@periodo2 bit) 
                    .CommandText = "Reporte_TiposCliente_Ciudad2"
                    .CommandTimeout = 0
                    .CommandType = CommandType.StoredProcedure
                    .Connection = conlidia
                    Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                    Dim prm1 As New SqlParameter("@op", SqlDbType.Int)
                    Dim prm2 As New SqlParameter("@conectado", SqlDbType.Bit)
                    Dim prm3 As New SqlParameter("@baja", SqlDbType.Bit)
                    Dim prm4 As New SqlParameter("@insta", SqlDbType.Bit)
                    Dim prm5 As New SqlParameter("@desconect", SqlDbType.Bit)
                    Dim prm6 As New SqlParameter("@susp", SqlDbType.Bit)
                    Dim prm7 As New SqlParameter("@fuera", SqlDbType.Bit)
                    Dim prm8 As New SqlParameter("@desctmp", SqlDbType.Bit)
                    Dim prm9 As New SqlParameter("@orden", SqlDbType.Int)
                    Dim prm10 As New SqlParameter("@habilita", SqlDbType.Int)
                    Dim prm11 As New SqlParameter("@Periodo1", SqlDbType.Bit)
                    Dim prm12 As New SqlParameter("@Periodo2", SqlDbType.Bit)
                    prm.Direction = ParameterDirection.Input
                    prm1.Direction = ParameterDirection.Input
                    prm2.Direction = ParameterDirection.Input
                    prm3.Direction = ParameterDirection.Input
                    prm4.Direction = ParameterDirection.Input
                    prm5.Direction = ParameterDirection.Input
                    prm6.Direction = ParameterDirection.Input
                    prm7.Direction = ParameterDirection.Input
                    prm8.Direction = ParameterDirection.Input
                    prm9.Direction = ParameterDirection.Input
                    prm10.Direction = ParameterDirection.Input
                    prm11.Direction = ParameterDirection.Input
                    prm12.Direction = ParameterDirection.Input
                    prm.Value = LocClv_session
                    prm1.Value = 1
                    prm2.Value = LocBndC
                    prm3.Value = LocBndB
                    prm4.Value = LocBndI
                    prm5.Value = LocBndD
                    prm6.Value = LocBndS
                    prm7.Value = LocBndF
                    prm8.Value = LocBndDT
                    prm9.Value = 1
                    prm10.Value = LocValidaHab
                    prm11.Value = LocPeriodo1
                    prm12.Value = LocPeriodo2
                    .Parameters.Add(prm)
                    .Parameters.Add(prm1)
                    .Parameters.Add(prm2)
                    .Parameters.Add(prm3)
                    .Parameters.Add(prm4)
                    .Parameters.Add(prm5)
                    .Parameters.Add(prm6)
                    .Parameters.Add(prm7)
                    .Parameters.Add(prm8)
                    .Parameters.Add(prm9)
                    .Parameters.Add(prm10)
                    .Parameters.Add(prm11)
                    .Parameters.Add(prm12)
                    Dim i As Integer = CMd.ExecuteNonQuery()
                End With
                conlidia.Close()
                Me.CrystalReportViewer1.ShowPrintButton = True
                If LEdo_Cuenta = True Then
                    LEdo_Cuenta = False
                    Me.Text = "Impresi�n de Estados de Cuenta"
                    ConfigureCrystalReportsEstadodeCuenta(LocClv_session)
                ElseIf LEdo_Cuenta2 = True Then
                    LEdo_Cuenta = False
                    Me.Text = "Listado de Recuperaci�n para Cartera"
                    ConfigureCrystalReportsListaso_DST(LocClv_session)
                End If

            ElseIf LEdo_Cuenta = False Then
                If Locbndrep = 1 Then
                    Me.Text = "Reporte de Clientes Con Adeudo de Material"
                    Me.CrystalReportViewer1.ShowPrintButton = True
                    ConfigureCrystalReports1()
                ElseIf Locbndrep <> 1 And bndreportetec = False And Locbndagnd = False And Locformulario <> 3 And locbndreportbit = False And LocreportServicio = False And bndimportemens = False And LReportecombo1 = False And bndcarteraejec = False And LocbndImpresionEdoCuentaLog = False Then
                    ConfigureCrystalReports(eContrato)
                ElseIf bndreportetec = True Then
                    bndreportetec = False
                    Me.Text = "Reporte de Actividades del T�cnico"
                    Me.CrystalReportViewer1.ShowPrintButton = True
                    ConfigureCrystalReport_TEcnicos()
                ElseIf Locbndagnd = True Then
                    Locbndagnd = False
                    Me.Text = "Agenda de Actividades Del T�cnico"
                    Me.CrystalReportViewer1.ShowPrintButton = True
                    ConfigureCrystalReport_TEcnicos2()
                End If
                If Locformulario = 3 Then
                    Me.Text = "Reporte General De Clientes"
                    Me.CrystalReportViewer1.ShowPrintButton = True
                    ConfigureCrystalReport_Clientesgral()
                End If
                If locbndreportbit = True Then
                    locbndreportbit = False
                    Me.Text = "Reporte De Bitacora Del Sistema"
                    Me.CrystalReportViewer1.ShowPrintButton = True
                    ConfigureCrystalReport_Bitacora()
                End If
                If LocreportServicio = True Then
                    LocreportServicio = False
                    Me.CrystalReportViewer1.ShowPrintButton = True
                    ConfigureCrystalReport_Servicio()
                End If
                If bndimportemens = True Then
                    bndimportemens = False
                    Me.CrystalReportViewer1.ShowPrintButton = True
                    ConfigureCrystalReport_Mensualidades_adelantadas()
                End If
                If LReportecombo1 = True Then
                    LReportecombo1 = False
                    Me.Text = "Reporte Clientes Con Combo"
                    Me.CrystalReportViewer1.ShowPrintButton = True
                    ConfigureCrystalReport_Clientes_combo()
                End If
                If bndcarteraejec = True Then
                    bndcarteraejec = False
                    Me.Text = "Cartera Ejecutiva"
                    Me.CrystalReportViewer1.ShowPrintButton = True
                    ConfigureCrystalReport_CarteraEjecutiva()
                End If
                If LocbndImpresionEdoCuentaLog = True Then
                    LocbndImpresionEdoCuentaLog = False
                    'Me.Text = "Estado De Cuenta Tel�fonia"
                    'Me.CrystalReportViewer1.ShowPrintButton = True
                    'Estado_de_Cuenta_Telefonia()
                    Me.CrystalReportViewer1.ShowPrintButton = True
                    Me.Text = "Estado De Cuenta Tel�fonia"
                    Me.BackgroundWorker1.RunWorkerAsync()
                    PantallaProcesando.Show()
                End If
            End If
        End If

    End Sub
    Private Sub Estado_de_Cuenta_Telefonia()
        Try

            'customersByCityReport = New EstadoDeCuentaFinal

            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            ' Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword

            ''customersByCityReport.DataSourceConnections(GloServerName, GloDatabaseName).SetConnection(GloServerName, GloDatabaseName, False, GloPassword)

            ''customersByCityReport.Subreports(0).DataSourceConnections(GloServerName, GloDatabaseName).SetConnection(GloServerName, GloDatabaseName, False, GloPassword)


            'SetDBLogonForReport(connectionInfo, customersByCityReport)


            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\EstadoDeCuentaFinal.rpt"
            customersByCityReport.Load(reportPath)


            Dim connection As IConnectionInfo
            Dim serverName1 As String = GloServerName

            ' Establecer conexi�n con base de datos al informe principal
            For Each connection In customersByCityReport.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetConnection(serverName1, GloDatabaseName, False)
                        connection.SetLogon(GloUserID, GloPassword)
                End Select
            Next

            ' Establecer conexi�n al subinforme
            Dim subreport As ReportDocument
            For Each subreport In customersByCityReport.Subreports
                For Each connection In subreport.DataSourceConnections
                    connection.SetConnection(serverName1, GloDatabaseName, False)
                    connection.SetLogon(GloUserID, GloPassword)
                Next
            Next
            'LocGloContratoIni = Me.TxtContratoIni.Text
            'LocGloContratoFin = Me.TxtContratoFin.Text


            '@contrato1
            customersByCityReport.SetParameterValue(0, LocGloContratoIni)
            'contrato2
            customersByCityReport.SetParameterValue(1, LocGloContratoFin)

            If Me.CrystalReportViewer1.InvokeRequired Then
                Dim d As New Reporte(AddressOf Estado_de_Cuenta_Telefonia)
                Me.Invoke(d, New Object() {})
            Else
                CrystalReportViewer1.ReportSource = customersByCityReport
                CrystalReportViewer1.Zoom(75)
            End If


            'CrystalReportViewer1.ReportSource = customersByCityReport
            'CrystalReportViewer1.Zoom(75)

            'customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait

            'customersByCityReport.PrintOptions.PrinterName = Impresora
            'customersByCityReport.PrintToPrinter(1, True, 1, 0)

            'customersByCityReport.Dispose()
            customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ConfigureCrystalReport_CarteraEjecutiva()
        Try
            Dim impresora As String = Nothing
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing
            Dim Subtitulo As String = Nothing
            Dim mySelectFormula As String = Nothing

            reportPath = RutaReportes + "\CarteraEjecutiva.rpt"
            mySelectFormula = "Cartera Ejecutiva Del D�a: " + fecha_cartera


            customersByCityReport.Load(reportPath)


            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '(@op int
            customersByCityReport.SetParameterValue(0, fecha_cartera)

            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & LocGloNomEmpresa & "'"
            ' CrystalReportViewer1.ReportSource = customersByCityReport
            customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & GloSucursal & "'"



            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)
            'CrystalReportViewer1.ShowPrintButton = True
            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ConfigureCrystalReportsListaso_DST(ByVal session As Integer)
        Try
            Dim impresora As String = Nothing
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim mySelectFormula As String

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\ReportDST.rpt"
            customersByCityReport.Load(reportPath)

            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Contrato 
            customersByCityReport.SetParameterValue(0, CStr(session))
            'customersByCityReport.SetParameterValue(1, IdSistema)
            'customersByCityReport.SetParameterValue(2, "1")
            'customersByCityReport.SetParameterValue(3, "1")
            CrystalReportViewer1.ReportSource = customersByCityReport
            mySelectFormula = "Listado de Recuperaci�n para Cartera"
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            ' CrystalReportViewer1.ReportSource = customersByCityReport
            customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & GloSucursal & "'"

            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
            CrystalReportViewer1.Zoom(75)

            'customersByCityReport.PrintOptions.PrinterName = ImpresoraEstado
            'customersByCityReport.PrintToPrinter(1, True, 1, 1)
            customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ConfigureCrystalReportsEstadodeCuenta(ByVal Session As Integer)
        'Try
        '    Dim impresora As String = Nothing
        '    customersByCityReport = New ReportDocument
        '    Dim connectionInfo As New ConnectionInfo

        '    connectionInfo.ServerName = GloServerName
        '    connectionInfo.DatabaseName = GloDatabaseName
        '    connectionInfo.UserID = GloUserID
        '    connectionInfo.Password = GloPassword

        '    Dim reportPath As String = Nothing
        '    reportPath = RutaReportes + "\ReportEstado_Cuenta.rpt"
        '    customersByCityReport.Load(reportPath)

        '    SetDBLogonForReport(connectionInfo, customersByCityReport)

        '    '@Contrato 
        '    customersByCityReport.SetParameterValue(0, CStr(Session))
        '    customersByCityReport.SetParameterValue(1, IdSistema)
        '    customersByCityReport.SetParameterValue(2, "1")
        '    customersByCityReport.SetParameterValue(3, "1")
        '    CrystalReportViewer1.ReportSource = customersByCityReport
        '    'SetDBLogonForReport2(connectionInfo)
        '    'CrystalReportViewer1.ShowPrintButton = True

        '    'Me.Selecciona_ImpresoraTableAdapter.Fill(Me.DataSetarnoldo.Selecciona_Impresora, 1, impresora)
        '    customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
        '    CrystalReportViewer1.Zoom(75)

        '    'customersByCityReport.PrintOptions.PrinterName = ImpresoraEstado
        '    'customersByCityReport.PrintToPrinter(1, True, 1, 1)
        '    customersByCityReport = Nothing
        'Catch ex As Exception
        '    System.Windows.Forms.MessageBox.Show(ex.Message)
        'End Try
        Try
            Dim impresora As String = Nothing
            customersByCityReport = New Report_Estado_Cuentas
            Dim connectionInfo As New ConnectionInfo

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            ' Dim reportPath As String = Nothing
            'reportPath = RutaReportes + "\ReportEstado_Cuenta.rpt"
            'reportPath = "Report_Estado_Cuentas.rpt"
            'customersByCityReport.Load(reportPath)

            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Contrato 
            customersByCityReport.SetParameterValue(0, CStr(Session))
            customersByCityReport.SetParameterValue(1, IdSistema)
            customersByCityReport.SetParameterValue(2, "1")
            customersByCityReport.SetParameterValue(3, "1")
            CrystalReportViewer1.ReportSource = customersByCityReport
            'CrystalReportViewer1.ShowPrintButton = True
            'Me.Selecciona_ImpresoraTableAdapter.Fill(Me.DataSetarnoldo.Selecciona_Impresora, 1, impresora)

            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
            '            customersByCityReport.PrintOptions.PrinterName = ImpresoraEstado
            'customersByCityReport.PrintToPrinter(1, True, 1, Meses - 1)
            customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try


    End Sub
    Private Sub ConfigureCrystalReport_Mensualidades_adelantadas()
        Try
            Dim impresora As String = Nothing
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing
            Dim Subtitulo As String = Nothing
            Dim mySelectFormula As String = Nothing

            reportPath = RutaReportes + "\Reporte_Adelantados_Fecha.rpt"
            mySelectFormula = "Reporte De Importe De Mensualidades Adelantadas"


            customersByCityReport.Load(reportPath)


            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '(@op int
            customersByCityReport.SetParameterValue(0, LocClv_session)

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            ' CrystalReportViewer1.ReportSource = customersByCityReport
            customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & GloSucursal & "'"



            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)
            'CrystalReportViewer1.ShowPrintButton = True
            customersByCityReport = Nothing
            bndrepmensulidad1 = True

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ConfigureCrystalReport_Clientes_combo()
        Try
            Dim impresora As String = Nothing
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing
            Dim Subtitulo As String = Nothing
            Dim mySelectFormula As String = Nothing

            If LocOpRepcombos = 1 Then
                reportPath = RutaReportes + "\ReporteClientesCombo.rpt"
                mySelectFormula = "Reporte De Clientes Con Combo"
            ElseIf LocOpRepcombos = 2 Then
                reportPath = RutaReportes + "\ReporteClientesComboResumen.rpt"
                mySelectFormula = "Resumen De Clientes Con Combo"
            End If


            customersByCityReport.Load(reportPath)


            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@clv_session bigint
            customersByCityReport.SetParameterValue(0, LocClv_session)
            '@Habilita int,
            customersByCityReport.SetParameterValue(1, LocValidaHab)
            '@periodo1 bit,
            customersByCityReport.SetParameterValue(2, LocPeriodo1)
            '@periodo2 bit,
            customersByCityReport.SetParameterValue(3, LocPeriodo2)
            '@contratado bit,
            customersByCityReport.SetParameterValue(4, 0)
            '@baja bit,
            customersByCityReport.SetParameterValue(5, 0)
            '@Insta bit,
            customersByCityReport.SetParameterValue(6, 0)
            '@Desconect bit,
            customersByCityReport.SetParameterValue(7, 0)
            '@Susp bit,
            customersByCityReport.SetParameterValue(8, 0)
            '@Fuera bit,
            customersByCityReport.SetParameterValue(9, 0)
            '@DescTmp bit
            customersByCityReport.SetParameterValue(10, 0)


            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            ' CrystalReportViewer1.ReportSource = customersByCityReport
            customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & GloSucursal & "'"



            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)
            'CrystalReportViewer1.ShowPrintButton = True
            customersByCityReport = Nothing
            bndrepcombo = True
            LocOpRepcombos = 0
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ConfigureCrystalReport_Servicio()
        Try
            Dim impresora As String = Nothing
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing
            Dim Subtitulo As String = Nothing
            Dim mySelectFormula As String = Nothing

            reportPath = RutaReportes + "\Reporte_Servicios.rpt"
            mySelectFormula = "Reporte Detalle de Servicio"


            customersByCityReport.Load(reportPath)


            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '(@op int
            customersByCityReport.SetParameterValue(0, GloClv_Servicio)

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            ' CrystalReportViewer1.ReportSource = customersByCityReport
            customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & GloSucursal & "'"



            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)
            'CrystalReportViewer1.ShowPrintButton = True
            customersByCityReport = Nothing

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ConfigureCrystalReport_Bitacora()
        Try
            Dim impresora As String = Nothing
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing
            Dim Subtitulo As String = Nothing
            Dim mySelectFormula As String = Nothing

            reportPath = RutaReportes + "\ReporteBitacora.rpt"
            mySelectFormula = "Listado de Cambios En Bitacora Del Sistema"


            customersByCityReport.Load(reportPath)


            SetDBLogonForReport(connectionInfo, customersByCityReport)


            'op, Loc1clv_sistema, Loc1Fechaini, Loc1Fechafin, Loc1horaini, Loc1horafin, Loc1clv_usuario, Loc1clv_pantalla, Loc1clv_control

            '(@op int
            customersByCityReport.SetParameterValue(0, loc1op)
            ',@clv_sistema varchar(max),@fecha1 varchar(max),@fecha2 varchar(max),@hora1 varchar(max),@hora2 varchar(max),@clv_usuario varchar(max),@clv_pantalla varchar(max),@clv_control varchar(max))
            customersByCityReport.SetParameterValue(1, Loc1clv_sistema)
            '@fecha1 varchar(max)
            customersByCityReport.SetParameterValue(2, Loc1Fechaini)
            '@fecha2 varchar(max)
            customersByCityReport.SetParameterValue(3, Loc1Fechafin)
            ',@hora1 varchar(max),
            customersByCityReport.SetParameterValue(4, Loc1horaini)
            '@hora2 varchar(max),
            customersByCityReport.SetParameterValue(5, Loc1horafin)
            '@clv_usuario varchar(max),
            customersByCityReport.SetParameterValue(6, Loc1clv_usuario)
            '@clv_pantalla varchar(max),
            customersByCityReport.SetParameterValue(7, Loc1clv_pantalla)
            '@clv_control varchar(max))
            customersByCityReport.SetParameterValue(8, Loc1clv_control)
            '@contrato varchar(50)
            customersByCityReport.SetParameterValue(9, Loc1contrato)



            'Subtitulo = "Sucursal:" & GloSucusal


            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            CrystalReportViewer1.ReportSource = customersByCityReport
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloSucursal & "'"



            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)
            'CrystalReportViewer1.ShowPrintButton = True
            customersByCityReport = Nothing

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ConfigureCrystalReport_TEcnicos()
        Try
            Dim dSet As New DataSet

            dSet = ReporteActividadesDelTecnico(LocTipRepTec, LocClv_session, LocFechaTec1, LocFechaTec2)

            customersByCityReport = New ReportDocument
            Dim reportPath As String = Nothing
            Dim mySelectFormula As String = Nothing

            If LocTipRepTec = 0 Then
                reportPath = RutaReportes + "\ReporteActividadesDelTecnicoListado.rpt"
                mySelectFormula = "Listado de Actividades del T�cnico del " + LocFechaTec1.ToString("dd/MM/yyyy") + " al " + LocFechaTec2.ToString("dd/MM/yyyy")
            ElseIf LocTipRepTec = 1 Then
                reportPath = RutaReportes + "\ReporteActividadesDelTecnicoResumen.rpt"
                mySelectFormula = "Resumen de Actividades del T�cnico del " + LocFechaTec1.ToString("dd/MM/yyyy") + " al " + LocFechaTec2.ToString("dd/MM/yyyy")
            End If

            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(dSet)

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloSucursal & "'"

            CrystalReportViewer1.ReportSource = customersByCityReport

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ConfigureCrystalReport_Clientesgral()

        Try
            Dim impresora As String = Nothing
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing
            Dim Subtitulo As String = Nothing
            Dim mySelectFormula As String = Nothing

            reportPath = RutaReportes + "\ReporteDatosGralesCliente.rpt"
            mySelectFormula = "Reporte General De Clientes "

            customersByCityReport.Load(reportPath)

            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@clv_session 
            customersByCityReport.SetParameterValue(0, Glocontratoini)
            '@fecha1 datetime
            customersByCityReport.SetParameterValue(1, Glocontratofin)

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            CrystalReportViewer1.ReportSource = customersByCityReport
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloSucursal & "'"

            Locformulario = 10
            customersByCityReport = Nothing

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ConfigureCrystalReport_TEcnicos2()

        Try
            Dim fechas As String = Nothing
            customersByCityReport = New ReportDocument
            Dim cnn As New SqlConnection(MiConexion)
            customersByCityReport = New ReportDocument

            Dim reportPath As String = Nothing
            Dim Subtitulo As String = Nothing
            Dim mySelectFormula As String = Nothing

            reportPath = RutaReportes + "\ReportAgendaTecnicoRangoFechas.rpt"
            mySelectFormula = "Agenda de Actividades Del T�cnico"

            customersByCityReport.Load(reportPath)
            Dim ds As New DataSet()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@op", SqlDbType.Int, LocopAgnd)
            BaseII.CreateMyParameter("@CLV_TECNICO", SqlDbType.Int, LocClv_tecnico)
            BaseII.CreateMyParameter("@fecha1", SqlDbType.DateTime, LocFecha1Agnd)
            BaseII.CreateMyParameter("@fecha2", SqlDbType.DateTime, LocFecha2Agnd)
            BaseII.CreateMyParameter("@TURNO", SqlDbType.VarChar, LocTurnoAgnd)
            BaseII.CreateMyParameter("@clvTipSer", SqlDbType.Int, gloClvTipSerReportes)
            Dim lp As List(Of String) = New List(Of String)()
            lp.Add("ReporteAgendaTecnicoCitas")
            ds = BaseII.ConsultaDS("ReporteAgendaTecnicoCitas", lp)

            'Dim cmd As New SqlCommand("ReporteAgendaTecnicoCitas", cnn)
            'cmd.CommandType = CommandType.StoredProcedure
            ''SetDBLogonForReport(connectionInfo, customersByCityReport)
            'Dim parametro1 As New SqlParameter("@op", SqlDbType.Int)
            'parametro1.Direction = ParameterDirection.Input
            'parametro1.Value = LocopAgnd
            'cmd.Parameters.Add(parametro1)
            'Dim parametro2 As New SqlParameter("@CLV_TECNICO", SqlDbType.Int)
            'parametro2.Direction = ParameterDirection.Input
            'parametro2.Value = LocClv_tecnico
            'cmd.Parameters.Add(parametro2)
            'Dim parametro3 As New SqlParameter("@fecha1", SqlDbType.DateTime)
            'parametro3.Direction = ParameterDirection.Input
            'parametro3.Value = LocFecha1Agnd
            'cmd.Parameters.Add(parametro3)
            'Dim parametro4 As New SqlParameter("@fecha2", SqlDbType.DateTime)
            'parametro4.Direction = ParameterDirection.Input
            'parametro4.Value = LocFecha2Agnd
            'cmd.Parameters.Add(parametro4)
            'cmd.Parameters.Add("@Turno", SqlDbType.VarChar, 100).Value = LocTurnoAgnd
            ''(@op int
            'customersByCityReport.SetParameterValue(0, LocopAgnd)
            ''@CLV_TECNICO INT
            'customersByCityReport.SetParameterValue(1, LocClv_tecnico)
            ''@fecha1 datetime
            'customersByCityReport.SetParameterValue(2, LocFecha1Agnd)
            ''@fecha2 datetime
            'customersByCityReport.SetParameterValue(3, LocFecha2Agnd)
            'Subtitulo = "Sucursal:" & GloSucusal            
            'Dim da As New SqlDataAdapter(cmd)
            'Dim data1 As New DataTable()
            'Dim ds As New DataSet()
            'da.Fill(data1)
            'data1.TableName = "ReporteAgendaTecnicoCitas"
            'ds.Tables.Add(data1)

            fechas = "De la Fecha: " + LocFecha1Agnd + " A la Fecha: " + LocFecha2Agnd
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloSucursal & "'"
            customersByCityReport.DataDefinition.FormulaFields("nomservicio").Text = "'" & fechas & "'"

            customersByCityReport.SetDataSource(ds)
            CrystalReportViewer1.Zoom(75)
            CrystalReportViewer1.ReportSource = customersByCityReport


        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ConfigureCrystalReports1()
        Try
            Dim mySelectFormula As String = Nothing
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\Listado_Clientes_AMat.rpt"
            customersByCityReport.Load(reportPath)

            mySelectFormula = "Clientes Con Adeudo de Material"

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"

            CrystalReportViewer1.ReportSource = customersByCityReport
            'CrystalReportViewer1.Zoom(75)
            SetDBLogonForReport(connectionInfo, customersByCityReport)
            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ConfigureCrystalReports(ByVal Clv_Factura As Long)
        Dim CON As New SqlConnection(MiConexion)
        Try
            Dim impresora As String = Nothing
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\ReportConContrato.rpt"
            customersByCityReport.Load(reportPath)


            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Contrato 
            customersByCityReport.SetParameterValue(0, eContrato)
            'CrystalReportViewer1.ReportSource = customersByCityReport
            'CrystalReportViewer1.ShowPrintButton = True
            CON.Open()
            Me.Selecciona_ImpresoraTableAdapter.Connection = CON
            Me.Selecciona_ImpresoraTableAdapter.Fill(Me.DataSetarnoldo.Selecciona_Impresora, 1, impresora)
            CON.Close()
            If impresora = "" Then
                MsgBox("La impresora de Contratos no se encuentra en generales del sistema", MsgBoxStyle.Information)
            Else

                customersByCityReport.PrintOptions.PrinterName = impresora
                customersByCityReport.PrintToPrinter(1, True, 1, 1)
                'customersByCityReport = Nothing
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub ReporteDeProductividadTecnico()
        Try
            Dim reportPath As String = Nothing
            customersByCityReport = New ReportDocument
            Dim ds As DataSet = BAL.ProductividadParaTecnico.ReporteDeProductividadTecnicos(Rpt_ProductividadTecnicoTipoTrabajo, LocClv_session, LocFechaTec1, LocFechaTec2)
            reportPath = RutaReportes + "\ReporteDeProductividad.rpt"
            ds.Tables(0).TableName = "Datos"
            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(ds)
            SetDBReport(ds, customersByCityReport)
            customersByCityReport.SetParameterValue("filtro", "De " + LocFechaTec1.ToShortDateString() + " Al " + LocFechaTec2.ToShortDateString())
            customersByCityReport.SetParameterValue("filtro2", "Tipo de Trabajo: " + IIf(Rpt_ProductividadTecnicoTipoTrabajo = "Ambos", "Ordenes Y Quejas", Rpt_ProductividadTecnicoTipoTrabajo))
            CrystalReportViewer1.ReportSource = customersByCityReport
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CrystalReportViewer1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CrystalReportViewer1.Load

    End Sub

    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        Estado_de_Cuenta_Telefonia()
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        PantallaProcesando.Close()
        MsgBox("Impresion Terminada", MsgBoxStyle.Information)
    End Sub

    Private Sub ConfigureCrystalReportOrdenesInsExt()
        customersByCityReport = New ReportDocument
        Dim cnn As New SqlConnection(MiConexion)
        Try

            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0", Op6 As String = "0"
            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
            Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"
            Dim Status As String = Nothing
            'Dim clave_session As Long
            Dim reportPath As String = Nothing

            reportPath = RutaReportes + "\ReporteFormatoOrdenes.rpt"

            Dim cmd As New SqlCommand("ReporteAreaTecnicaOrdSer1_Extenciones", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0

            '@Clv_TipSer int
            Dim parametro1 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
            parametro1.Direction = ParameterDirection.Input
            parametro1.Value = 0
            cmd.Parameters.Add(parametro1)
            '@op1 smallint
            Dim parametro2 As New SqlParameter("@op1", SqlDbType.SmallInt)
            parametro2.Direction = ParameterDirection.Input
            parametro2.Value = Op1
            cmd.Parameters.Add(parametro2)
            '@op2 smallint
            Dim parametro3 As New SqlParameter("@op2", SqlDbType.SmallInt)
            parametro3.Direction = ParameterDirection.Input
            parametro3.Value = Op2
            cmd.Parameters.Add(parametro3)
            '@op3 smallint
            Dim parametro4 As New SqlParameter("@op3", SqlDbType.SmallInt)
            parametro4.Direction = ParameterDirection.Input
            parametro4.Value = Op3
            cmd.Parameters.Add(parametro4)
            '@op4 smallint
            Dim parametro5 As New SqlParameter("@op4", SqlDbType.SmallInt)
            parametro5.Direction = ParameterDirection.Input
            parametro5.Value = Op4
            cmd.Parameters.Add(parametro5)
            '@op5 smallint
            Dim parametro6 As New SqlParameter("@op5", SqlDbType.SmallInt)
            parametro6.Direction = ParameterDirection.Input
            parametro6.Value = Op5
            cmd.Parameters.Add(parametro6)
            '@StatusPen bit
            Dim parametro7 As New SqlParameter("@StatusPen", SqlDbType.Bit)
            parametro7.Direction = ParameterDirection.Input
            If StatusPen = "1" Then
                parametro7.Value = True
            Else
                parametro7.Value = False
            End If
            cmd.Parameters.Add(parametro7)
            '@StatusEje bit
            Dim parametro8 As New SqlParameter("@StatusEje", SqlDbType.Bit)
            parametro8.Direction = ParameterDirection.Input
            If StatusEje = "1" Then
                parametro8.Value = True
            Else
                parametro8.Value = False
            End If
            cmd.Parameters.Add(parametro8)
            '@StatusVis bit
            Dim parametro9 As New SqlParameter("@StatusVis", SqlDbType.Bit)
            parametro9.Direction = ParameterDirection.Input
            If StatusVis = "1" Then
                parametro9.Value = True
            Else
                parametro9.Value = False
            End If
            cmd.Parameters.Add(parametro9)
            '@Clv_OrdenIni bigint

            Dim parametro10 As New SqlParameter("@Clv_OrdenIni", SqlDbType.BigInt)
            parametro10.Direction = ParameterDirection.Input
            parametro10.Value = 0
            cmd.Parameters.Add(parametro10)
            '@Clv_OrdenFin bigint

            Dim parametro11 As New SqlParameter("@Clv_OrdenFin", SqlDbType.BigInt)
            parametro11.Direction = ParameterDirection.Input
            parametro11.Value = 0
            cmd.Parameters.Add(parametro11)
            '@Fec1Ini Datetime

            Dim parametro12 As New SqlParameter("@Fec1Ini", SqlDbType.DateTime)
            parametro12.Direction = ParameterDirection.Input
            parametro12.Value = "01/01/1900"
            cmd.Parameters.Add(parametro12)

            Dim parametro13 As New SqlParameter("@Fec1Fin", SqlDbType.DateTime)
            parametro13.Direction = ParameterDirection.Input
            parametro13.Value = "01/01/1900"
            cmd.Parameters.Add(parametro13)

            Dim parametro14 As New SqlParameter("@Fec2Ini", SqlDbType.DateTime)
            parametro14.Direction = ParameterDirection.Input
            parametro14.Value = "01/01/1900"
            cmd.Parameters.Add(parametro14)

            Dim parametro15 As New SqlParameter("@Fec2Fin", SqlDbType.DateTime)
            parametro15.Direction = ParameterDirection.Input
            parametro15.Value = "01/01/1900"
            cmd.Parameters.Add(parametro15)
            '@Clv_Trabajo int

            Dim parametro16 As New SqlParameter("@Clv_Trabajo", SqlDbType.Int)
            parametro16.Direction = ParameterDirection.Input
            parametro16.Value = 0
            cmd.Parameters.Add(parametro16)
            '@Clv_Colonia int

            Dim parametro17 As New SqlParameter("@Clv_Colonia", SqlDbType.Int)
            parametro17.Direction = ParameterDirection.Input
            parametro17.Value = 0
            cmd.Parameters.Add(parametro17)
            '@OpOrden int
            Dim parametro18 As New SqlParameter("@OpOrden", SqlDbType.Int)
            parametro18.Direction = ParameterDirection.Input
            parametro18.Value = 3
            cmd.Parameters.Add(parametro18)

            Dim parametro19 As New SqlParameter("@numeroVisitas", SqlDbType.Int)
            parametro19.Direction = ParameterDirection.Input
            parametro19.Value = 0
            cmd.Parameters.Add(parametro19)

            Dim parametro20 As New SqlParameter("@OP6", SqlDbType.SmallInt)
            parametro20.Direction = ParameterDirection.Input
            parametro20.Value = 0
            cmd.Parameters.Add(parametro20)


            Dim parametro21 As New SqlParameter("@ResVisita", SqlDbType.Int)
            parametro21.Direction = ParameterDirection.Output
            cmd.Parameters.Add(parametro21)

            Dim parametro22 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
            parametro22.Direction = ParameterDirection.Input
            parametro22.Value = LocClv_session
            cmd.Parameters.Add(parametro22)

            Dim parametro23 As New SqlParameter("@clvDepto", SqlDbType.BigInt)
            parametro23.Direction = ParameterDirection.Input
            parametro23.Value = 0
            cmd.Parameters.Add(parametro23)

            Dim parametro24 As New SqlParameter("@clvTipoServicioDigital", SqlDbType.BigInt)
            parametro24.Direction = ParameterDirection.Input
            parametro24.Value = gloClvTipSerReportes
            cmd.Parameters.Add(parametro24)

            Dim NumVisitas As String

            cnn.Open()
            cmd.ExecuteNonQuery()
            NumVisitas = CStr(parametro21.Value)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet()
            da.Fill(ds)

            ds.Tables(0).TableName = "ReporteAreaTecnicaOrdSer"
            ds.Tables(1).TableName = "DameDatosGenerales_2"

            'Dim variable As String
            'variable = "Sin Filtro  #  Visitas de Tecnico"
            'ds.Tables.Add(data1)
            customersByCityReport.Load(reportPath)
            'Dim mySelectFormula As String = ""
            'mySelectFormula = "Ordenes de Servicio"
            ''If op = 0 Then
            'customersByCityReport.DataDefinition.FormulaFields("empresa").Text = "'" & LocGloNomEmpresa & "'"
            'customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            'customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloSucursal & "'"
            'If NumeroVisitas > 0 Then
            '    customersByCityReport.DataDefinition.FormulaFields("NumeroVisitas").Text = "'" & NumVisitas & "'"
            'Else
            '    customersByCityReport.DataDefinition.FormulaFields("NumeroVisitas").Text = "'" & variable & "'"
            'End If

            'SetDBReport(ds, customersByCityReport)

            'If Me.CrystalReportViewer1.InvokeRequired Then
            '    Dim d As New Reporte(AddressOf ConfigureCrystalReportsListadoInstExt)
            '    Me.Invoke(d, New Object() {op, Titulo})
            'Else
            customersByCityReport.SetDataSource(ds)
            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(100)
            'If GloOpFacturas = 3 Then
            CrystalReportViewer1.ShowExportButton = True
            CrystalReportViewer1.ShowPrintButton = True
            CrystalReportViewer1.ShowRefreshButton = True

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            cnn.Close()
            cnn.Dispose()
        End Try
    End Sub

    Private Sub ReporteInstalacionesTecnico()
        Try
            customersByCityReport = New ReportDocument
            Dim cnn As New SqlConnection(MiConexion)
            Dim reportPath As String = Nothing
            Dim Subtitulo As String = Nothing
            Dim mySelectFormula As String = Nothing

            reportPath = RutaReportes + "\ReporteInstalTecnicos.rpt"

            Dim ds As New DataSet()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.BigInt, CLng(LocClv_session))
            BaseII.CreateMyParameter("@FECHAINI", SqlDbType.DateTime, CDate(GloFecha_Ini))
            BaseII.CreateMyParameter("@FECHAFIN", SqlDbType.DateTime, CDate(GloFecha_Fin))

            Dim list As List(Of String) = New List(Of String)()
            list.Add("uspConsultaInstalTecnicos")
            ds = BaseII.ConsultaDS("uspConsultaInstalTecnicos", list)

            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(ds)

            If gloClvTipSerReportes = 3 Then
                mySelectFormula = "ORDENES DE SERVICIO DE INSTALACION REALIZADAS TV Digital MMDS"
            ElseIf gloClvTipSerReportes = 6 Then
                mySelectFormula = "ORDENES DE SERVICIO DE INSTALACION REALIZADAS TV Digital Cable"
            Else
                mySelectFormula = "ORDENES DE SERVICIO DE INSTALACION REALIZADAS"
            End If
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & "Del " + GloFecha_Ini.ToShortDateString() + " Al " + GloFecha_Fin.ToShortDateString() & "'"

            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.ShowExportButton = True
            CrystalReportViewer1.ShowPrintButton = True
            CrystalReportViewer1.ShowRefreshButton = True
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        gloClvTipSerReportes = 0
    End Sub

    Private Function ReporteActividadesDelTecnico(ByVal prmOp As Integer, ByVal prmClvSession As Long, ByVal prmFechaIni As Date, ByVal prmFechaFin As Date) As DataSet
        Try
            Dim listaTabla As New List(Of String)
            listaTabla.Add("Tecnicos")

            If prmOp = 0 Then
                listaTabla.Add("Listado")
            ElseIf prmOp = 1 Then
                listaTabla.Add("Resumen")
            End If

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@OP", SqlDbType.Int, prmOp)
            BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.BigInt, prmClvSession)
            BaseII.CreateMyParameter("@FECHAINI", SqlDbType.DateTime, prmFechaIni)
            BaseII.CreateMyParameter("@FECHAFIN", SqlDbType.DateTime, prmFechaFin)
            ReporteActividadesDelTecnico = BaseII.ConsultaDS("ReporteActividadesDelTecnico", listaTabla)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class
