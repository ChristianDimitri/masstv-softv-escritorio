﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Text
Imports System.Windows.Forms
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports MizarCFD.BRL
Imports MizarCFD.DAL
Imports MizarCFDi.API.Efac

Module Module_CFD
    Public oAlta_Datosfiscales As Boolean = True

    '    Public Sub Alta_Datosfiscales(ByVal IdCompania As Long, ByVal oContrato As Long, ByVal oRazonSocial As String,
    '        ByVal oRFC As String,
    '        ByVal oCalle As String,
    '        ByVal oClaveAsociado As String,
    '        ByVal oCodigoPostal As String,
    '        ByVal oColonia As String,
    '        ByVal oEMail As String,
    '        ByVal oEntreCalles As String,
    '        ByVal oEstado As String,
    '        ByVal oIdAsociado As String,
    '        ByVal oLocalidad As String,
    '        ByVal oMunicipio As String,
    '        ByVal oNumeroExterior As String,
    '            ByVal oPais As String, ByVal oTel As String, ByVal oFax As String, ByVal oReferencia As String)

    '        oAlta_Datosfiscales = True

    '        If String.IsNullOrEmpty(oRazonSocial) = True Then oRazonSocial = ""
    '        If String.IsNullOrEmpty(oRFC) = True Then oRFC = ""
    '        If String.IsNullOrEmpty(oCalle) = True Then oCalle = ""
    '        If String.IsNullOrEmpty(oClaveAsociado) = True Then oClaveAsociado = ""
    '        If String.IsNullOrEmpty(oCodigoPostal) = True Then oCodigoPostal = ""
    '        If String.IsNullOrEmpty(oColonia) = True Then oColonia = ""
    '        If String.IsNullOrEmpty(oEMail) = True Then oEMail = ""
    '        If String.IsNullOrEmpty(oEntreCalles) = True Then oEntreCalles = ""
    '        If String.IsNullOrEmpty(oEstado) = True Then oEstado = ""
    '        If String.IsNullOrEmpty(oIdAsociado) = True Then oIdAsociado = ""
    '        If String.IsNullOrEmpty(oLocalidad) = True Then oLocalidad = ""
    '        If String.IsNullOrEmpty(oMunicipio) = True Then oMunicipio = ""
    '        If String.IsNullOrEmpty(oNumeroExterior) = True Then oNumeroExterior = ""
    '        If String.IsNullOrEmpty(oPais) = True Then oPais = ""
    '        If String.IsNullOrEmpty(oTel) = True Then oTel = ""
    '        If String.IsNullOrEmpty(oFax) = True Then oFax = ""
    '        If String.IsNullOrEmpty(oReferencia) = True Then oReferencia = ""
    '        Dim oAsociados As MizarCFD.BRL.Asociados

    '        If Existe_Checalo(" Select * from asociados  where id_asociado = " + oIdAsociado) = False Then


    '            Using Cnx As New MizarCFD.DAL.DAConexion("HL", "sa", "sa")

    '                oAsociados = New MizarCFD.BRL.Asociados
    '                oAsociados.Inicializar()
    '                oAsociados.IdCompania = IdCompania
    '                oAsociados.RazonSocial = oRazonSocial
    '                oAsociados.RFC = oRFC
    '                oAsociados.Calle = oCalle
    '                oAsociados.ClaveAsociado = CLng(oIdAsociado)
    '                oAsociados.CodigoPostal = oCodigoPostal
    '                oAsociados.Colonia = oColonia
    '                oAsociados.EMail = oEMail
    '                oAsociados.EntreCalles = oEntreCalles
    '                oAsociados.Estado = oEstado
    '                oAsociados.IdAsociado = CLng(oIdAsociado)
    '                oAsociados.Localidad = oLocalidad
    '                oAsociados.Municipio = oMunicipio
    '                oAsociados.NumeroExterior = oNumeroExterior
    '                oAsociados.Telefono = oTel
    '                oAsociados.Fax = oFax
    '                oAsociados.Referencia = oReferencia
    '                oAsociados.Pais = oPais
    '                'oAsociados.PrecioLista = 0
    '                oAsociados.IdTipoAsociado = Asociados.TipoAsociado.Cliente
    '                If oAsociados.Validar(Cnx, TipoAfectacionBD.Insertar) = True Then
    '                    If Not oAsociados.Insertar(Cnx) Then
    '                        For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
    '                            MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
    '                            oAlta_Datosfiscales = False
    '                        Next
    '                        Return
    '                    End If
    '                Else
    '                    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
    '                        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
    '                        oAlta_Datosfiscales = False
    '                    Next
    '                    Return
    '                End If





    '            End Using
    '        Else
    '            'Modificar

    '            Using Cnx As New MizarCFD.DAL.DAConexion("HL", "sa", "sa")

    '                oAsociados = New MizarCFD.BRL.Asociados
    '                oAsociados.Inicializar()
    '                oAsociados.IdCompania = IdCompania
    '                oAsociados.IdAsociado = CLng(oIdAsociado)
    '                oAsociados.ClaveAsociado = CLng(oIdAsociado)
    '                oAsociados.RazonSocial = oRazonSocial
    '                oAsociados.RFC = oRFC
    '                oAsociados.Calle = oCalle

    '                oAsociados.CodigoPostal = oCodigoPostal
    '                oAsociados.Colonia = oColonia
    '                oAsociados.EMail = oEMail
    '                oAsociados.EntreCalles = oEntreCalles
    '                oAsociados.Estado = oEstado

    '                oAsociados.Localidad = oLocalidad
    '                oAsociados.Municipio = oMunicipio
    '                oAsociados.NumeroExterior = oNumeroExterior
    '                oAsociados.Pais = oPais
    '                oAsociados.IdTipoAsociado = Asociados.TipoAsociado.Cliente
    '                If oAsociados.Validar(Cnx, TipoAfectacionBD.Modificar) = True Then
    '                    If Not oAsociados.Modificar(Cnx) Then
    '                        For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
    '                            MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
    '                            oAlta_Datosfiscales = False
    '                        Next
    '                        Return
    '                    End If
    '                Else
    '                    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
    '                        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
    '                        oAlta_Datosfiscales = False
    '                    Next
    '                    Return
    '                End If
    '            End Using
    '        End If

    '        If oAlta_Datosfiscales = True Then
    '            UspAltaEsNuevoRel_Cliente_AsociadoCFD(oContrato, oIdAsociado)
    '        End If

    '    End Sub

    Public Sub UspAltaEsNuevoRel_Cliente_AsociadoCFD(ByVal oContrato As Long, ByVal oIDASOCIADO As Long)
        Dim CON01 As New SqlConnection(MiConexion)
        Dim SQL As New SqlCommand()
        Try
            CON01.Open()
            SQL = New SqlCommand()
            With SQL
                .CommandText = "UspAltaEsNuevoRel_Cliente_AsociadoCFD"
                .Connection = CON01
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
                prm.Value = oContrato
                prm.Direction = ParameterDirection.Input
                .Parameters.Add(prm)

                prm = New SqlParameter("@IDASOCIADO", SqlDbType.BigInt)
                prm.Value = oIDASOCIADO
                prm.Direction = ParameterDirection.Input
                .Parameters.Add(prm)

                'prm = New SqlParameter("@Email", SqlDbType.VarChar, 100)
                'prm.Value = oEmail
                'prm.Direction = ParameterDirection.Input
                '.Parameters.Add(prm)

                Dim i As Integer = .ExecuteNonQuery()
            End With
            CON01.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Public Sub Alta_Datosfiscales_NEW(ByVal IdCompania As Long, ByVal oRazonSocial As String,
        ByVal oRFC As String,
        ByVal oCalle As String,
        ByVal oClaveAsociado As String,
        ByVal oCodigoPostal As String,
        ByVal oColonia As String,
        ByVal oEMail As String,
        ByVal oEntreCalles As String,
        ByVal oEstado As String,
        ByVal oIdAsociado As String,
        ByVal oLocalidad As String,
        ByVal oMunicipio As String,
        ByVal oNumeroExterior As String,
        ByVal oPais As String, ByVal oTel As String, ByVal oFax As String, ByVal oReferencia As String, ocontrato As Long)
        Dim OPRUEBA As New MizarCFDi.API.Efac

        Dim oAsociados As MizarCFD.BRL.Asociados
        Dim oId_AsociadoLlave As Long = 0
        Dim oIdDireccion As Long = 0

        If String.IsNullOrEmpty(oRazonSocial) = True Then oRazonSocial = ""
        If String.IsNullOrEmpty(oRFC) = True Then oRFC = ""
        If String.IsNullOrEmpty(oCalle) = True Then oCalle = ""
        If String.IsNullOrEmpty(oClaveAsociado) = True Then oClaveAsociado = ""
        If String.IsNullOrEmpty(oCodigoPostal) = True Then oCodigoPostal = ""
        If String.IsNullOrEmpty(oColonia) = True Then oColonia = ""
        If String.IsNullOrEmpty(oEMail) = True Then oEMail = ""
        If String.IsNullOrEmpty(oEntreCalles) = True Then oEntreCalles = ""
        If String.IsNullOrEmpty(oEstado) = True Then oEstado = ""
        If String.IsNullOrEmpty(oIdAsociado) = True Then oIdAsociado = ""
        If String.IsNullOrEmpty(oLocalidad) = True Then oLocalidad = ""
        If String.IsNullOrEmpty(oMunicipio) = True Then oMunicipio = ""
        If String.IsNullOrEmpty(oNumeroExterior) = True Then oNumeroExterior = ""
        If String.IsNullOrEmpty(oPais) = True Then oPais = ""
        If String.IsNullOrEmpty(oTel) = True Then oTel = ""
        If String.IsNullOrEmpty(oFax) = True Then oFax = ""
        If String.IsNullOrEmpty(oReferencia) = True Then oReferencia = ""
        '
        oAlta_Datosfiscales = True

        If Existe_Checalo(" Select * from asociados  where ID_asociado = " + oIdAsociado) = False Then


            'If OPRUEBA.AltaCliente(oIdAsociado, IdCompania, oRazonSocial, oCalle, oCodigoPostal, oColonia, oEMail, oEntreCalles, oEstado, oFax, oLocalidad, oMunicipio, oNumeroExterior, "", oPais, oReferencia, oRFC, oTel) = False Then
            '    MsgBox(OPRUEBA.mensajeError, MsgBoxStyle.Information)
            '    oAlta_Datosfiscales = False
            '    Exit Sub
            'End If

            UspAltaEsNuevoRel_Cliente_AsociadoCFD(oContrato, oIdAsociado)

        Else
            'Modificar

            oId_AsociadoLlave = oIdAsociado 'Existe_DevuelveValor(" Select id_asociado from asociados  where id_asociado = " + oIdAsociado)
            UspAltaEsNuevoRel_Cliente_AsociadoCFD(ocontrato, oIdAsociado)

            Using Cnx As New MizarCFD.DAL.DAConexion("HL", "sa", "sa")

                oAsociados = New MizarCFD.BRL.Asociados
                oAsociados.Inicializar()
                oAsociados.IdAsociado = oId_AsociadoLlave
                oAsociados.ClaveAsociado = oId_AsociadoLlave
                oAsociados.RazonSocial = oRazonSocial
                oAsociados.RFC = oRFC
                'oAsociados.IdCompania = IdCompania
                oAsociados.TiempoPago = 1
                oAsociados.CondicionesDePago = "Una Sola Exhibicion"
                oAsociados.PeriodoPago = 2
                oAsociados.IdMoneda = 1

                oAsociados.IdTipoAsociado = Asociados.TipoAsociado.Cliente

                If oAsociados.Validar(Cnx, TipoAfectacionBD.Modificar) = True Then

                    If Not oAsociados.Modificar(Cnx) Then
                        For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
                            MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                            oAlta_Datosfiscales = False
                        Next
                        Return
                    End If
                Else
                    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
                        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                        oAlta_Datosfiscales = False
                    Next
                    Return
                End If
            End Using

            'oIdDireccion = Existe_DevuelveValor(" Select id_direccion  from [asociados_direcciones] where id_asociado = " + oId_AsociadoLlave)
            oIdDireccion = 1
            'Guardamos la Dirreccion
            Using Cnx As New MizarCFD.DAL.DAConexion("HL", "sa", "sa")
                Dim odirecciones As New MizarCFD.BRL.AsociadosDirecciones
                Dim otipo As New MizarCFD.BRL.AsociadosDirecciones.TipoDireccion
                odirecciones.Inicializar()
                odirecciones.IdDireccion = oIdDireccion
                odirecciones.IdAsociado = oId_AsociadoLlave
                odirecciones.CodigoPostal = oCodigoPostal
                odirecciones.Calle = oCalle
                odirecciones.IdTipoDireccion = odirecciones.TipoDireccion.Principal
                odirecciones.Colonia = oColonia
                odirecciones.EMail = oEMail
                odirecciones.EntreCalles = oEntreCalles
                odirecciones.Estado = oEstado
                odirecciones.Localidad = oLocalidad
                odirecciones.Municipio = oMunicipio
                odirecciones.NumeroExterior = oNumeroExterior
                odirecciones.Telefono = oTel
                odirecciones.Fax = oFax
                odirecciones.Referencia = oReferencia
                odirecciones.Pais = oPais
                'If odirecciones.Validar(Cnx, TipoAfectacionBD.Modificar) = True Then
                If Not odirecciones.Modificar(Cnx) Then
                    For Each oMensaje As DAMensajesSistema.RegistroMensaje In odirecciones.MensajesSistema.ListaMensajes
                        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                        oAlta_Datosfiscales = False
                    Next
                    Return
                End If
                'Else
                'For Each oMensaje As DAMensajesSistema.RegistroMensaje In odirecciones.MensajesSistema.ListaMensajes
                '    MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                '    oAlta_Datosfiscales = False
                'Next
                'Return
                'End If



            End Using


        End If





    End Sub


    Public Function Existe_Checalo(ByVal oSql As String) As Boolean
        Existe_Checalo = False

        Using Cnx As New MizarCFD.DAL.DAConexion("HL", "sa", "sa")
            Cnx.DbCommand.CommandText = oSql
            Cnx.DbCommand.CommandTimeout = 0
            Cnx.DbCommand.CommandType = CommandType.Text
            'Cnx.DbConnection.Open()
            'Dim CONEXION As New SqlConnection(Cnx.DbConnection.Database)
            'Dim COMANDO As New SqlCommand(oSql, CONEXION)
            'COMANDO.CommandType = CommandType.Text
            'Try
            '    CONEXION.Open()
            Dim reader As OleDbDataReader = Cnx.DbCommand.ExecuteReader
            '    'Recorremos los Cargos obtenidos desde el Procedimiento
            While (reader.Read())
                If Len(reader(0).ToString) > 0 Then
                    Existe_Checalo = True
                End If
            End While
            'Cnx.DbConnection.Close()
            'Catch ex As Exception
            '    MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            'Finally
            '    CONEXION.Close()
            '    CONEXION.Dispose()
            'End Try
        End Using
    End Function

    Public Function Existe_DevuelveValor(ByVal oSql As String) As Long
        Existe_DevuelveValor = 0

        Using Cnx As New MizarCFD.DAL.DAConexion("HL", "sa", "sa")
            Cnx.DbCommand.CommandText = oSql
            Cnx.DbCommand.CommandTimeout = 0
            Cnx.DbCommand.CommandType = CommandType.Text
            'Cnx.DbConnection.Open()
            'Dim CONEXION As New SqlConnection(Cnx.DbConnection.Database)
            'Dim COMANDO As New SqlCommand(oSql, CONEXION)
            'COMANDO.CommandType = CommandType.Text
            'Try
            '    CONEXION.Open()
            Dim reader As OleDbDataReader = Cnx.DbCommand.ExecuteReader
            '    'Recorremos los Cargos obtenidos desde el Procedimiento
            While (reader.Read())
                If Len(reader(0).ToString) > 0 Then
                    Existe_DevuelveValor = CLng(reader(0).ToString)
                End If
            End While
            'Cnx.DbConnection.Close()
            'Catch ex As Exception
            '    MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            'Finally
            '    CONEXION.Close()
            '    CONEXION.Dispose()
            'End Try
        End Using
    End Function

    Public Sub Usp_ED_BorraRel_Cliente_AsociadoCFD(oContrato As Long)
        Dim cON_x As New SqlConnection(MiConexion)
        Try
            'Regresa La Session

            Dim cmd As New SqlCommand
            cON_x.Open()
            With cmd
                .CommandText = "Usp_ED_BorraRel_Cliente_AsociadoCFD"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = cON_x
                Dim prm2 As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = oContrato
                .Parameters.Add(prm2)
                Dim i As Integer = .ExecuteNonQuery
            End With
            cON_x.Close()
        Catch ex As Exception
            cON_x.Close()
        End Try
    End Sub

    Public Sub Alta_Datosfiscales(ByVal oRazonSocial As String,
        ByVal oRFC As String,
        ByVal oCalle As String,
        ByVal oClaveAsociado As String,
        ByVal oCodigoPostal As String,
        ByVal oColonia As String,
        ByVal oEMail As String,
        ByVal oEntreCalles As String,
        ByVal oEstado As String,
        ByVal oIdAsociado As String,
        ByVal oLocalidad As String,
        ByVal oMunicipio As String,
        ByVal oNumeroExterior As String,
        ByVal oPais As String, ByVal oTel As String, ByVal oFax As String, ByVal oReferencia As String)


        oAlta_Datosfiscales = True

        If String.IsNullOrEmpty(oRazonSocial) = True Then oRazonSocial = ""
        If String.IsNullOrEmpty(oRFC) = True Then oRFC = ""
        If String.IsNullOrEmpty(oCalle) = True Then oCalle = ""
        If String.IsNullOrEmpty(oClaveAsociado) = True Then oClaveAsociado = ""
        If String.IsNullOrEmpty(oCodigoPostal) = True Then oCodigoPostal = ""
        If String.IsNullOrEmpty(oColonia) = True Then oColonia = ""
        If String.IsNullOrEmpty(oEMail) = True Then oEMail = ""
        If String.IsNullOrEmpty(oEntreCalles) = True Then oEntreCalles = ""
        If String.IsNullOrEmpty(oEstado) = True Then oEstado = ""
        If String.IsNullOrEmpty(oIdAsociado) = True Then oIdAsociado = ""
        If String.IsNullOrEmpty(oLocalidad) = True Then oLocalidad = ""
        If String.IsNullOrEmpty(oMunicipio) = True Then oMunicipio = ""
        If String.IsNullOrEmpty(oNumeroExterior) = True Then oNumeroExterior = ""
        If String.IsNullOrEmpty(oPais) = True Then oPais = ""
        If String.IsNullOrEmpty(oTel) = True Then oTel = ""
        If String.IsNullOrEmpty(oFax) = True Then oFax = ""
        If String.IsNullOrEmpty(oReferencia) = True Then oReferencia = ""
        Dim oAsociados As MizarCFD.BRL.Asociados

        If Existe_Checalo(" Select * from asociados  where id_asociado = " + oIdAsociado) = False Then

            Dim odirecciones As New MizarCFD.BRL.AsociadosDirecciones



            Using Cnx As New MizarCFD.DAL.DAConexion("HL", "sa", "sa")

                oAsociados = New MizarCFD.BRL.Asociados
                oAsociados.Inicializar()
                oAsociados.RazonSocial = oRazonSocial
                oAsociados.RFC = oRFC
                'oAsociados.Calle = oCalle
                odirecciones.IdAsociado = oIdAsociado
                odirecciones.IdTipoDireccion = AsociadosDirecciones.TipoDireccion.Fiscal
                odirecciones.Calle = oCalle
                odirecciones.CodigoPostal = oCodigoPostal
                odirecciones.Colonia = oColonia
                odirecciones.EMail = oEMail
                odirecciones.EntreCalles = oEntreCalles
                odirecciones.Estado = oEstado
                odirecciones.Localidad = oLocalidad
                odirecciones.Municipio = oMunicipio
                odirecciones.NumeroExterior = oNumeroExterior
                odirecciones.Telefono = oTel
                odirecciones.Fax = oFax
                odirecciones.Referencia = oReferencia
                odirecciones.Pais = oPais

                oAsociados.ClaveAsociado = oIdAsociado
                oAsociados.IdAsociado = oIdAsociado

                'oAsociados.PrecioLista = 0
                oAsociados.IdTipoAsociado = Asociados.TipoAsociado.Cliente
                If oAsociados.Validar(Cnx, TipoAfectacionBD.Insertar) = True Then
                    If Not oAsociados.Insertar(Cnx) Then
                        For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
                            MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                            oAlta_Datosfiscales = False
                        Next
                        Return
                    End If
                    If odirecciones.Validar(Cnx, TipoAfectacionBD.Insertar) = True Then
                        If Not odirecciones.Insertar(Cnx) Then
                            For Each oMensaje As DAMensajesSistema.RegistroMensaje In odirecciones.MensajesSistema.ListaMensajes
                                MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                                oAlta_Datosfiscales = False
                            Next
                            Return
                        End If
                    End If

                Else
                    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
                        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                        oAlta_Datosfiscales = False
                    Next
                    Return
                End If





            End Using
        Else
            'Modificar

            Using Cnx As New MizarCFD.DAL.DAConexion("HL", "sa", "sa")

                oAsociados = New MizarCFD.BRL.Asociados
                oAsociados.Inicializar()
                oAsociados.IdAsociado = oIdAsociado
                oAsociados.ClaveAsociado = oIdAsociado
                oAsociados.RazonSocial = oRazonSocial
                oAsociados.RFC = oRFC

                oAsociados.Direcciones.Item(0).Calle = oCalle
                oAsociados.Direcciones.Item(0).CodigoPostal = oCodigoPostal
                oAsociados.Direcciones.Item(0).Colonia = oColonia
                oAsociados.Direcciones.Item(0).EMail = oEMail
                oAsociados.Direcciones.Item(0).EntreCalles = oEntreCalles
                oAsociados.Direcciones.Item(0).Estado = oEstado
                oAsociados.Direcciones.Item(0).Localidad = oLocalidad
                oAsociados.Direcciones.Item(0).Municipio = oMunicipio
                oAsociados.Direcciones.Item(0).NumeroExterior = oNumeroExterior
                oAsociados.Direcciones.Item(0).Pais = oPais

                oAsociados.IdTipoAsociado = Asociados.TipoAsociado.Cliente
                If oAsociados.Validar(Cnx, TipoAfectacionBD.Modificar) = True Then
                    If Not oAsociados.Modificar(Cnx) Then
                        For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
                            MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                            oAlta_Datosfiscales = False
                        Next
                        Return
                    End If
                Else
                    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
                        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                        oAlta_Datosfiscales = False
                    Next
                    Return
                End If
            End Using
        End If

    End Sub


    Public Function UspEsNuevoRel_Cliente_AsociadoCFD(ByVal RFC As String) As Long
        UspEsNuevoRel_Cliente_AsociadoCFD = 0
        Dim CON01 As New SqlConnection(MiConexion)
        Dim SQL As New SqlCommand()
        Try
            CON01.Open()
            SQL = New SqlCommand()
            With SQL
                .CommandText = "UspEsNuevoRel_Cliente_AsociadoCFD"
                .CommandTimeout = 0
                .Connection = CON01
                .CommandType = CommandType.StoredProcedure

                Dim PRM As New SqlParameter("@RFC", SqlDbType.VarChar, 50)
                PRM.Direction = ParameterDirection.Input
                PRM.Value = RFC
                .Parameters.Add(PRM)


                Dim reader As SqlDataReader = .ExecuteReader()

                While (reader.Read)
                    UspEsNuevoRel_Cliente_AsociadoCFD = reader.GetValue(0)
                    'EMail_TextBox.Text = reader.GetValue(2)
                End While
            End With

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            CON01.Close()
        End Try
    End Function

    Public Sub Borrar_Datosfiscales(ByVal oContrato As String)

        'Modificar
        Dim oId_AsociadoLlave As Long = 0
        Dim oIdDireccion As Long = 0
        oId_AsociadoLlave = UspEsNuevoRel_Cliente_AsociadoCFD(oContrato)
        '--oId_AsociadoLlave = Existe_DevuelveValor(" Select id_asociado from asociados  where id_asociado = " + oContrato)
        oIdDireccion = 1
        'Guardamos la Dirreccion

        Using Cnx As New MizarCFD.DAL.DAConexion("HL", "sa", "sa")
            Dim odirecciones As New MizarCFD.BRL.AsociadosDirecciones
            Dim otipo As New MizarCFD.BRL.AsociadosDirecciones.TipoDireccion
            odirecciones.Inicializar()
            odirecciones.IdDireccion = oIdDireccion
            odirecciones.IdAsociado = oId_AsociadoLlave
            If Not odirecciones.BorrarPorIdAsociado(Cnx) Then
                For Each oMensaje As DAMensajesSistema.RegistroMensaje In odirecciones.MensajesSistema.ListaMensajes
                    MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                    oAlta_Datosfiscales = False
                Next
                Return
            End If
        End Using

        Dim oAsociados As MizarCFD.BRL.Asociados
        Using Cnx As New MizarCFD.DAL.DAConexion("HL", "sa", "sa")

            oAsociados = New MizarCFD.BRL.Asociados
            oAsociados.Inicializar()
            oAsociados.IdAsociado = oId_AsociadoLlave
            oAsociados.ClaveAsociado = oId_AsociadoLlave
            'oAsociados.IdCompania = 1
            If oAsociados.Validar(Cnx, TipoAfectacionBD.Borrar) = True Then
                If Not oAsociados.Borrar(Cnx) Then
                    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
                        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                    Next
                    Return
                End If
            Else
                For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
                    MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                Next
                Return
            End If
        End Using


    End Sub

    Public Function Query_Asociado(ByVal oSql As String) As Long
        Query_Asociado = 0
        Using Cnx As New MizarCFD.DAL.DAConexion("HL", "sa", "sa")
            Cnx.DbCommand.CommandText = oSql
            Cnx.DbCommand.CommandTimeout = 0
            Cnx.DbCommand.CommandType = CommandType.Text
            'Cnx.DbConnection.Open()
            'Dim CONEXION As New SqlConnection(Cnx.DbConnection.Database)
            'Dim COMANDO As New SqlCommand(oSql, CONEXION)
            'COMANDO.CommandType = CommandType.Text
            'Try
            '    CONEXION.Open()
            Dim reader As OleDbDataReader = Cnx.DbCommand.ExecuteReader
            '    'Recorremos los Cargos obtenidos desde el Procedimiento
            While (reader.Read())
                'If Len(reader(0).ToString) > 0 Then
                If IsNumeric(reader(0).ToString) = True Then
                    Query_Asociado = CLng(reader(0).ToString)
                End If
                'End If
            End While
            'Cnx.DbConnection.Close()
            'Catch ex As Exception
            '    MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            'Finally
            '    CONEXION.Close()
            '    CONEXION.Dispose()
            'End Try
        End Using
    End Function

    Public Function Nuevo_Asociado(ByVal oSql As String) As Long
        Nuevo_Asociado = 0
        Using Cnx As New MizarCFD.DAL.DAConexion("HL", "sa", "sa")
            Cnx.DbCommand.CommandText = oSql
            Cnx.DbCommand.CommandTimeout = 0
            Cnx.DbCommand.CommandType = CommandType.Text
            'Cnx.DbConnection.Open()
            'Dim CONEXION As New SqlConnection(Cnx.DbConnection.Database)
            'Dim COMANDO As New SqlCommand(oSql, CONEXION)
            'COMANDO.CommandType = CommandType.Text
            'Try
            '    CONEXION.Open()
            Dim reader As OleDbDataReader = Cnx.DbCommand.ExecuteReader
            '    'Recorremos los Cargos obtenidos desde el Procedimiento
            While (reader.Read())
                If Len(reader(0).ToString) > 0 Then
                    If IsNumeric(reader(0).ToString) = False Then
                        Nuevo_Asociado = 1
                    Else
                        Nuevo_Asociado = reader(0).ToString + 1
                    End If
                End If
            End While
            'Cnx.DbConnection.Close()
            'Catch ex As Exception
            '    MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            'Finally
            '    CONEXION.Close()
            '    CONEXION.Dispose()
            'End Try
        End Using
    End Function

End Module


'Imports System
'Imports System.Collections.Generic
'Imports System.ComponentModel
'Imports System.Data
'Imports System.Drawing
'Imports System.Text
'Imports System.Windows.Forms
'Imports System.Xml
'Imports System.Data.SqlClient
'Imports System.Data.OleDb
'Imports MizarCFD.BRL
'Imports MizarCFD.DAL
'Imports MizarCFDi.API.Efac

'Module Module_CFD
'    Public oAlta_Datosfiscales As Boolean = True

'    Public Function Existe_Checalo(ByVal oSql As String) As Boolean
'        Existe_Checalo = False
'        Using Cnx As New MizarCFD.DAL.DAConexion("HL", "sa", "sa")
'            Cnx.DbCommand.CommandText = oSql
'            Cnx.DbCommand.CommandTimeout = 0
'            Cnx.DbCommand.CommandType = CommandType.Text
'            'Cnx.DbConnection.Open()
'            'Dim CONEXION As New SqlConnection(Cnx.DbConnection.Database)
'            'Dim COMANDO As New SqlCommand(oSql, CONEXION)
'            'COMANDO.CommandType = CommandType.Text
'            'Try
'            '    CONEXION.Open()
'            Dim reader As OleDbDataReader = Cnx.DbCommand.ExecuteReader
'            '    'Recorremos los Cargos obtenidos desde el Procedimiento
'            While (reader.Read())
'                If Len(reader(0).ToString) > 0 Then
'                    Existe_Checalo = True
'                End If
'            End While
'            'Cnx.DbConnection.Close()
'            'Catch ex As Exception
'            '    MsgBox(ex.Message, MsgBoxStyle.Exclamation)
'            'Finally
'            '    CONEXION.Close()
'            '    CONEXION.Dispose()
'            'End Try
'        End Using
'    End Function

'    Public Function Query_Asociado(ByVal oSql As String) As Long
'        Query_Asociado = 0
'        Using Cnx As New MizarCFD.DAL.DAConexion("HL", "sa", "sa")
'            Cnx.DbCommand.CommandText = oSql
'            Cnx.DbCommand.CommandTimeout = 0
'            Cnx.DbCommand.CommandType = CommandType.Text
'            'Cnx.DbConnection.Open()
'            'Dim CONEXION As New SqlConnection(Cnx.DbConnection.Database)
'            'Dim COMANDO As New SqlCommand(oSql, CONEXION)
'            'COMANDO.CommandType = CommandType.Text
'            'Try
'            '    CONEXION.Open()
'            Dim reader As OleDbDataReader = Cnx.DbCommand.ExecuteReader
'            '    'Recorremos los Cargos obtenidos desde el Procedimiento
'            While (reader.Read())
'                'If Len(reader(0).ToString) > 0 Then
'                If IsNumeric(reader(0).ToString) = True Then
'                    Query_Asociado = CLng(reader(0).ToString)
'                End If
'                'End If
'            End While
'            'Cnx.DbConnection.Close()
'            'Catch ex As Exception
'            '    MsgBox(ex.Message, MsgBoxStyle.Exclamation)
'            'Finally
'            '    CONEXION.Close()
'            '    CONEXION.Dispose()
'            'End Try
'        End Using
'    End Function

'    Public Function Nuevo_Asociado(ByVal oSql As String) As Long
'        Nuevo_Asociado = 0
'        Using Cnx As New MizarCFD.DAL.DAConexion("HL", "sa", "sa")
'            Cnx.DbCommand.CommandText = oSql
'            Cnx.DbCommand.CommandTimeout = 0
'            Cnx.DbCommand.CommandType = CommandType.Text
'            'Cnx.DbConnection.Open()
'            'Dim CONEXION As New SqlConnection(Cnx.DbConnection.Database)
'            'Dim COMANDO As New SqlCommand(oSql, CONEXION)
'            'COMANDO.CommandType = CommandType.Text
'            'Try
'            '    CONEXION.Open()
'            Dim reader As OleDbDataReader = Cnx.DbCommand.ExecuteReader
'            '    'Recorremos los Cargos obtenidos desde el Procedimiento
'            While (reader.Read())
'                If Len(reader(0).ToString) > 0 Then
'                    If IsNumeric(reader(0).ToString) = False Then
'                        Nuevo_Asociado = 1
'                    Else
'                        Nuevo_Asociado = reader(0).ToString + 1
'                    End If
'                End If
'            End While
'            'Cnx.DbConnection.Close()
'            'Catch ex As Exception
'            '    MsgBox(ex.Message, MsgBoxStyle.Exclamation)
'            'Finally
'            '    CONEXION.Close()
'            '    CONEXION.Dispose()
'            'End Try
'        End Using
'    End Function



'    Public Sub Alta_Datosfiscales(ByVal IdCompania As Long, ByVal oContrato As Long, ByVal oRazonSocial As String,
'        ByVal oRFC As String,
'        ByVal oCalle As String,
'        ByVal oClaveAsociado As String,
'        ByVal oCodigoPostal As String,
'        ByVal oColonia As String,
'        ByVal oEMail As String,
'        ByVal oEntreCalles As String,
'        ByVal oEstado As String,
'        ByVal oIdAsociado As String,
'        ByVal oLocalidad As String,
'        ByVal oMunicipio As String,
'        ByVal oNumeroExterior As String,
'            ByVal oPais As String, ByVal oTel As String, ByVal oFax As String, ByVal oReferencia As String)

'        oAlta_Datosfiscales = True

'        If String.IsNullOrEmpty(oRazonSocial) = True Then oRazonSocial = ""
'        If String.IsNullOrEmpty(oRFC) = True Then oRFC = ""
'        If String.IsNullOrEmpty(oCalle) = True Then oCalle = ""
'        If String.IsNullOrEmpty(oClaveAsociado) = True Then oClaveAsociado = ""
'        If String.IsNullOrEmpty(oCodigoPostal) = True Then oCodigoPostal = ""
'        If String.IsNullOrEmpty(oColonia) = True Then oColonia = ""
'        If String.IsNullOrEmpty(oEMail) = True Then oEMail = ""
'        If String.IsNullOrEmpty(oEntreCalles) = True Then oEntreCalles = ""
'        If String.IsNullOrEmpty(oEstado) = True Then oEstado = ""
'        If String.IsNullOrEmpty(oIdAsociado) = True Then oIdAsociado = ""
'        If String.IsNullOrEmpty(oLocalidad) = True Then oLocalidad = ""
'        If String.IsNullOrEmpty(oMunicipio) = True Then oMunicipio = ""
'        If String.IsNullOrEmpty(oNumeroExterior) = True Then oNumeroExterior = ""
'        If String.IsNullOrEmpty(oPais) = True Then oPais = ""
'        If String.IsNullOrEmpty(oTel) = True Then oTel = ""
'        If String.IsNullOrEmpty(oFax) = True Then oFax = ""
'        If String.IsNullOrEmpty(oReferencia) = True Then oReferencia = ""
'        Dim oAsociados As MizarCFD.BRL.Asociados

'        If Existe_Checalo(" Select * from asociados  where id_asociado = " + oIdAsociado) = False Then


'            Using Cnx As New MizarCFD.DAL.DAConexion("HL", "sa", "sa")

'                oAsociados = New MizarCFD.BRL.Asociados
'                oAsociados.Inicializar()
'                oAsociados.IdCompania = IdCompania
'                oAsociados.RazonSocial = oRazonSocial
'                oAsociados.RFC = oRFC
'                oAsociados.Calle = oCalle
'                oAsociados.ClaveAsociado = CLng(oIdAsociado)
'                oAsociados.CodigoPostal = oCodigoPostal
'                oAsociados.Colonia = oColonia
'                oAsociados.EMail = oEMail
'                oAsociados.EntreCalles = oEntreCalles
'                oAsociados.Estado = oEstado
'                oAsociados.IdAsociado = CLng(oIdAsociado)
'                oAsociados.Localidad = oLocalidad
'                oAsociados.Municipio = oMunicipio
'                oAsociados.NumeroExterior = oNumeroExterior
'                oAsociados.Telefono = oTel
'                oAsociados.Fax = oFax
'                oAsociados.Referencia = oReferencia
'                oAsociados.Pais = oPais
'                'oAsociados.PrecioLista = 0
'                oAsociados.IdTipoAsociado = Asociados.TipoAsociado.Cliente
'                If oAsociados.Validar(Cnx, TipoAfectacionBD.Insertar) = True Then
'                    If Not oAsociados.Insertar(Cnx) Then
'                        For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
'                            MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
'                            oAlta_Datosfiscales = False
'                        Next
'                        Return
'                    End If
'                Else
'                    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
'                        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
'                        oAlta_Datosfiscales = False
'                    Next
'                    Return
'                End If





'            End Using
'        Else
'            'Modificar

'            Using Cnx As New MizarCFD.DAL.DAConexion("HL", "sa", "sa")

'                oAsociados = New MizarCFD.BRL.Asociados
'                oAsociados.Inicializar()
'                oAsociados.IdCompania = IdCompania
'                oAsociados.IdAsociado = CLng(oIdAsociado)
'                oAsociados.ClaveAsociado = CLng(oIdAsociado)
'                oAsociados.RazonSocial = oRazonSocial
'                oAsociados.RFC = oRFC
'                oAsociados.Calle = oCalle

'                oAsociados.CodigoPostal = oCodigoPostal
'                oAsociados.Colonia = oColonia
'                oAsociados.EMail = oEMail
'                oAsociados.EntreCalles = oEntreCalles
'                oAsociados.Estado = oEstado

'                oAsociados.Localidad = oLocalidad
'                oAsociados.Municipio = oMunicipio
'                oAsociados.NumeroExterior = oNumeroExterior
'                oAsociados.Pais = oPais
'                oAsociados.IdTipoAsociado = Asociados.TipoAsociado.Cliente
'                If oAsociados.Validar(Cnx, TipoAfectacionBD.Modificar) = True Then
'                    If Not oAsociados.Modificar(Cnx) Then
'                        For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
'                            MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
'                            oAlta_Datosfiscales = False
'                        Next
'                        Return
'                    End If
'                Else
'                    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
'                        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
'                        oAlta_Datosfiscales = False
'                    Next
'                    Return
'                End If
'            End Using
'        End If

'        If oAlta_Datosfiscales = True Then
'            UspAltaEsNuevoRel_Cliente_AsociadoCFD(oContrato, oIdAsociado)
'        End If

'    End Sub

'    Public Sub Borrar_Datosfiscales(ByVal oContrato As String)

'        'Modificar
'        Dim oAsociados As MizarCFD.BRL.Asociados
'        Using Cnx As New MizarCFD.DAL.DAConexion("HL", "sa", "sa")

'            oAsociados = New MizarCFD.BRL.Asociados
'            oAsociados.Inicializar()
'            oAsociados.IdAsociado = oContrato
'            oAsociados.ClaveAsociado = oContrato

'            If oAsociados.Validar(Cnx, TipoAfectacionBD.Borrar) = True Then
'                If Not oAsociados.Borrar(Cnx) Then
'                    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
'                        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
'                    Next
'                    Return
'                End If
'            Else
'                For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
'                    MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
'                Next
'                Return
'            End If
'        End Using


'    End Sub

'End Module

