﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFiltroTipoInstalacion
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbxRangoFechas = New System.Windows.Forms.GroupBox()
        Me.lblFechaFinal = New System.Windows.Forms.Label()
        Me.lblFechaInicial = New System.Windows.Forms.Label()
        Me.dtpFechaFinal = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaInicial = New System.Windows.Forms.DateTimePicker()
        Me.gbxTipoInstalacion = New System.Windows.Forms.GroupBox()
        Me.cmbTipoInstalacion = New System.Windows.Forms.ComboBox()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.gbxRangoFechas.SuspendLayout()
        Me.gbxTipoInstalacion.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxRangoFechas
        '
        Me.gbxRangoFechas.Controls.Add(Me.lblFechaFinal)
        Me.gbxRangoFechas.Controls.Add(Me.lblFechaInicial)
        Me.gbxRangoFechas.Controls.Add(Me.dtpFechaFinal)
        Me.gbxRangoFechas.Controls.Add(Me.dtpFechaInicial)
        Me.gbxRangoFechas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxRangoFechas.Location = New System.Drawing.Point(12, 12)
        Me.gbxRangoFechas.Name = "gbxRangoFechas"
        Me.gbxRangoFechas.Size = New System.Drawing.Size(315, 87)
        Me.gbxRangoFechas.TabIndex = 0
        Me.gbxRangoFechas.TabStop = False
        Me.gbxRangoFechas.Text = "Rango de Fechas (Fecha Contratación)"
        '
        'lblFechaFinal
        '
        Me.lblFechaFinal.AutoSize = True
        Me.lblFechaFinal.Location = New System.Drawing.Point(19, 59)
        Me.lblFechaFinal.Name = "lblFechaFinal"
        Me.lblFechaFinal.Size = New System.Drawing.Size(90, 15)
        Me.lblFechaFinal.TabIndex = 2
        Me.lblFechaFinal.Text = "Fecha Final :"
        '
        'lblFechaInicial
        '
        Me.lblFechaInicial.AutoSize = True
        Me.lblFechaInicial.Location = New System.Drawing.Point(12, 27)
        Me.lblFechaInicial.Name = "lblFechaInicial"
        Me.lblFechaInicial.Size = New System.Drawing.Size(97, 15)
        Me.lblFechaInicial.TabIndex = 0
        Me.lblFechaInicial.Text = "Fecha Inicial :"
        '
        'dtpFechaFinal
        '
        Me.dtpFechaFinal.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaFinal.Location = New System.Drawing.Point(136, 54)
        Me.dtpFechaFinal.Name = "dtpFechaFinal"
        Me.dtpFechaFinal.Size = New System.Drawing.Size(131, 21)
        Me.dtpFechaFinal.TabIndex = 1
        '
        'dtpFechaInicial
        '
        Me.dtpFechaInicial.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaInicial.Location = New System.Drawing.Point(136, 24)
        Me.dtpFechaInicial.Name = "dtpFechaInicial"
        Me.dtpFechaInicial.Size = New System.Drawing.Size(131, 21)
        Me.dtpFechaInicial.TabIndex = 0
        '
        'gbxTipoInstalacion
        '
        Me.gbxTipoInstalacion.Controls.Add(Me.cmbTipoInstalacion)
        Me.gbxTipoInstalacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxTipoInstalacion.Location = New System.Drawing.Point(11, 105)
        Me.gbxTipoInstalacion.Name = "gbxTipoInstalacion"
        Me.gbxTipoInstalacion.Size = New System.Drawing.Size(315, 66)
        Me.gbxTipoInstalacion.TabIndex = 1
        Me.gbxTipoInstalacion.TabStop = False
        Me.gbxTipoInstalacion.Text = "Tipo Instalación"
        '
        'cmbTipoInstalacion
        '
        Me.cmbTipoInstalacion.DisplayMember = "descripcionTipo"
        Me.cmbTipoInstalacion.FormattingEnabled = True
        Me.cmbTipoInstalacion.Location = New System.Drawing.Point(6, 30)
        Me.cmbTipoInstalacion.Name = "cmbTipoInstalacion"
        Me.cmbTipoInstalacion.Size = New System.Drawing.Size(303, 23)
        Me.cmbTipoInstalacion.TabIndex = 0
        Me.cmbTipoInstalacion.ValueMember = "idTipoInstalacion"
        '
        'btnAceptar
        '
        Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.Location = New System.Drawing.Point(47, 177)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(106, 31)
        Me.btnAceptar.TabIndex = 2
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(190, 177)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(106, 31)
        Me.btnCancelar.TabIndex = 3
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'frmFiltroTipoInstalacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(338, 218)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.gbxTipoInstalacion)
        Me.Controls.Add(Me.gbxRangoFechas)
        Me.Name = "frmFiltroTipoInstalacion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmFiltroTipoInstalacion"
        Me.gbxRangoFechas.ResumeLayout(False)
        Me.gbxRangoFechas.PerformLayout()
        Me.gbxTipoInstalacion.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxRangoFechas As System.Windows.Forms.GroupBox
    Friend WithEvents lblFechaFinal As System.Windows.Forms.Label
    Friend WithEvents lblFechaInicial As System.Windows.Forms.Label
    Public WithEvents dtpFechaFinal As System.Windows.Forms.DateTimePicker
    Public WithEvents dtpFechaInicial As System.Windows.Forms.DateTimePicker
    Friend WithEvents gbxTipoInstalacion As System.Windows.Forms.GroupBox
    Public WithEvents cmbTipoInstalacion As System.Windows.Forms.ComboBox
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
End Class
