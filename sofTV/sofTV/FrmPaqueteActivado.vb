Imports System.Data.SqlClient

Public Class FrmPaqueteActivado
    Dim eRes As Integer = 0
    Dim eMsg As String = Nothing

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            Dim i As Integer = Nothing
            i = 0
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            eRes = 0
            If IsNumeric(Me.TextBox1.Text) = True Then
                If Me.MAcCableModemComboBox.Text.Length > 0 Then
                    If Me.DescripcionComboBox.Text.Length > 0 Then
                        'Me.ChecaPaqueteActivadoTableAdapter.Fill(Me.DataSetEric.ChecaPaqueteActivado, Me.TextBox1.Text, Me.MAcCableModemComboBox.SelectedValue, Me.DescripcionComboBox.SelectedValue, eRes)
                        Me.ChecaPaqueteActivadoTableAdapter.Connection = CON
                        Me.ChecaPaqueteActivadoTableAdapter.Fill(Me.DataSetEric.ChecaPaqueteActivado, Me.TextBox1.Text, Me.MAcCableModemComboBox.SelectedValue, Me.DescripcionComboBox.SelectedValue, eRes, eMsg)
                        If eRes = 0 Then
                            Me.MANDA_CNR_DIG_2TableAdapter.Connection = CON
                            Me.MANDA_CNR_DIG_2TableAdapter.Fill(Me.DataSetEric.MANDA_CNR_DIG_2, Me.TextBox1.Text, 0, Me.MAcCableModemComboBox.SelectedValue, Me.DescripcionComboBox.SelectedValue, "SCENTL", 0, 0, 1, eClv_Usuario, Me.NumericUpDown1.Value)
                            bitsist(GloUsuario, CLng(Me.TextBox1.Text), LocGloSistema, Me.Name, "Se Activo Un Paquete De Prueba", "Tarjeta: " + Me.MAcCableModemComboBox.Text, "Se Activo Un Paquete De Prueba: " + Me.DescripcionComboBox.Text, LocClv_Ciudad)
                            MsgBox("Orden Efectuada.")
                        Else
                            MsgBox(eMsg, , "Error")
                        End If
                        'While MAcCableModemComboBox.Items.Count > 0
                        'MAcCableModemComboBox.Items.Remove(MAcCableModemComboBox.SelectedItem)
                        'MAcCableModemComboBox.Items.Clear()

                        'End While
                        MAcCableModemComboBox.Text = ""
                        Me.TextBox1.Text = ""
                        Me.DamePaquetesDigTableAdapter.Connection = CON
                        Me.DamePaquetesDigTableAdapter.Fill(Me.DataSetEric.DamePaquetesDig)
                        Me.NumericUpDown1.Value = 1
                    Else
                        MsgBox("Selecciona un Paquete.", , "Atenci�n")
                    End If
                Else
                    MsgBox("Selecciona el No. de la Tarjeta.", , "Atenci�n")
                End If
            Else
                MsgBox("Captura el Contrato.", , "Atenci�n")
            End If
            CON.Close()
        Catch
            MsgBox("Se ha producido un Error.")
        End Try
    End Sub

    Private Sub FrmPaqueteActivado_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.DamePaquetesDigTableAdapter.Connection = CON
        Me.DamePaquetesDigTableAdapter.Fill(Me.DataSetEric.DamePaquetesDig)
        CON.Close()
        colorea(Me, Me.Name)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If IsNumeric(Me.TextBox1.Text) = False Then
                MsgBox("�Ingrese un Contrato V�lido!", MsgBoxStyle.Information)
                Exit Sub
            End If
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If Me.TextBox1.Text.Length = 0 Then
                Me.DameTarjetasClientesDigTableAdapter.Connection = CON
                Me.DameTarjetasClientesDigTableAdapter.Fill(Me.DataSetEric.DameTarjetasClientesDig, 0)
            Else
                If IsNumeric(Me.TextBox1.Text) = True Then
                    Me.DameTarjetasClientesDigTableAdapter.Connection = CON
                    Me.DameTarjetasClientesDigTableAdapter.Fill(Me.DataSetEric.DameTarjetasClientesDig, Me.TextBox1.Text)
                End If
            End If
            CON.Close()
        End If
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'If Me.TextBox1.Text.Length = 0 Then
        '    Me.DameTarjetasClientesDigTableAdapter.Connection = CON
        '    Me.DameTarjetasClientesDigTableAdapter.Fill(Me.DataSetEric.DameTarjetasClientesDig, 0)
        'Else
        '    If IsNumeric(Me.TextBox1.Text) = True Then
        '        Me.DameTarjetasClientesDigTableAdapter.Connection = CON
        '        Me.DameTarjetasClientesDigTableAdapter.Fill(Me.DataSetEric.DameTarjetasClientesDig, Me.TextBox1.Text)
        '    End If
        'End If
        'CON.Close()
    End Sub

    Private Sub btnBusCliente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusCliente.Click
        Dim frmsc As FrmSelCliente = New FrmSelCliente()
        frmsc.ShowDialog()
        If (frmsc.DialogResult = Windows.Forms.DialogResult.OK) Then
            If (frmsc.CONTRATOLabel1.Text.Length > 0) Then

                Me.TextBox1.Text = frmsc.CONTRATOLabel1.Text

                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                If Me.TextBox1.Text.Length = 0 Then
                    Me.DameTarjetasClientesDigTableAdapter.Connection = CON
                    Me.DameTarjetasClientesDigTableAdapter.Fill(Me.DataSetEric.DameTarjetasClientesDig, 0)
                Else
                    If IsNumeric(Me.TextBox1.Text) = True Then
                        Me.DameTarjetasClientesDigTableAdapter.Connection = CON
                        Me.DameTarjetasClientesDigTableAdapter.Fill(Me.DataSetEric.DameTarjetasClientesDig, Me.TextBox1.Text)
                    End If
                End If
                CON.Close()
            End If
        End If
    End Sub
End Class