﻿Imports System.Data.SqlClient
Imports System.Text
Public Class BRWORDSER
    Private bnd As Boolean = False
    'Saul
    Private eRespuesta As Integer = 0
    Private eMensaje As String = Nothing
    Dim bndAcceso As Integer = 0
    Dim bndOrdRetir As Integer = 0

    Public Sub CREAARBOL()
        Dim CON As New SqlConnection(MiConexion)
        Try

            Dim I As Integer = 0
            Dim X As Integer = 0
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' Console.WriteLine(pRow("CustomerID").ToString())
            'Next
            If IsNumeric(Me.Clv_calleLabel2.Text) = True Then
                CON.Open()
                Me.Dame_DetOrdSerTableAdapter.Connection = CON
                Me.Dame_DetOrdSerTableAdapter.Fill(Me.DataSetEDGAR.Dame_DetOrdSer, New System.Nullable(Of Long)(CType(Me.Clv_calleLabel2.Text, Long)))
                CON.Close()
                Dim FilaRow As DataRow
                Me.TreeView1.Nodes.Clear()
                For Each FilaRow In Me.DataSetEDGAR.Dame_DetOrdSer.Rows
                    Me.TreeView1.Nodes.Add(Trim(FilaRow("descripcion").ToString()))
                    I += 1
                Next
                Me.TreeView1.ExpandAll()
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opcion = "N"
        GloBnd = False
        GloGuardo = True
        FrmOrdSer.Show()
    End Sub

    Private Sub consultar()
        If gloClave > 0 Then
            opcion = "C"
            GloBnd = False
            ' Me.ContratoLabel1.Text = Me.DataGridView1.SelectedCells(1).Value
            loccontratoordenes = Me.DataGridView1.SelectedCells(0).Value
            'GloClv_TipSer = 1000
            gloClave = Me.Clv_calleLabel2.Text
            FrmOrdSer.Show()
        Else
            MsgBox(mensaje2)
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        consultar()
    End Sub

    Private Sub modificar()
        If gloClave > 0 Then
            opcion = "M"
            GloBnd = False
            gloClave = Me.Clv_calleLabel2.Text
            loccontratoordenes = Me.DataGridView1.SelectedCells(0).Value
            'GloClv_TipSer = 1000
            eStatusOrdSer = Me.DataGridView1.SelectedCells(1).Value
            uspDimeTrabajo()
            If GloTrabajoR = 1 And eStatusOrdSer = "P" Then
                FrmRetiros.Show()
            Else
                FrmOrdSer.Show()
            End If
        Else
            MsgBox("Seleccione algun Tipo de Servicio")
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim CON As New SqlConnection(MiConexion)

        If Me.ContratoLabel1.Text <> "" Then
            'CON.Open()
            'Me.BuscaBloqueadoTableAdapter.Connection = CON
            'Me.BuscaBloqueadoTableAdapter.Fill(Me.DataSetLidia.BuscaBloqueado, Me.ContratoLabel1.Text, NUM, num2)
            'CON.Close()
            BuscaBloqueado(CInt(Me.ContratoLabel1.Text))
            If NUM = 0 Or num2 = 0 Then
                'SAUL
                If gloClave > 0 Then
                    bndOrdRetir = 0
                    uspOrdenesRetiro()
                    If bndOrdRetir = 0 Then
                        bndAcceso = 0
                        uspAutorizadoQuejasOrdenes()
                        If bndAcceso = 0 Then
                            eRespuesta = 0
                            eMensaje = ""
                            uspValidaOrdenes()
                            If eRespuesta = 1 Then
                                MsgBox("El Status del Cliente con el Serv. Digital es Moroso ó Baja!. No se Puede ejecutar Ordenes de Servicio")
                                Exit Sub
                            Else
                                modificar()
                            End If
                        Else
                            modificar()
                        End If
                        eRespuesta = 0
                        eMensaje = ""
                    Else
                        modificar()
                    End If
                Else
                    modificar()
                End If
                '(Fin)
                'modificar()
            ElseIf num2 = 1 Then
                MsgBox("El Cliente " + Me.ContratoLabel1.Text + " Ha Sido Bloqueado por lo que no se Podrá Ejecutar la Orden ", MsgBoxStyle.Exclamation)
            End If
        End If

    End Sub

    'SAUL
    Private Sub uspValidaOrdenes()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CInt(Me.ContratoLabel1.Text))
        BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@Res", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@Msg", SqlDbType.VarChar, 150, ParameterDirection.Output, "")
        'ByVal prmNombre As String, ByRef prmTipo As SqlDbType, ByRef prmTamanio As Integer, ByRef prmDireccion As ParameterDirection, ByRef prmValor As Object
        BaseII.ProcedimientoOutPut("uspValidaOrdenes")
        eRespuesta = CInt(BaseII.dicoPar("@Res").ToString)
        eMensaje = BaseII.dicoPar("@Msg").ToString
    End Sub
    '(Fin)

    Private Sub busca1(ByVal Clv_TipSer As Integer, ByVal Clv_Orden As Long, ByVal Contrato As Integer, ByVal NOMBRE As String, ByVal CALLE As String, ByVal NUMERO As String, ByVal OP As Integer, ByVal procauto As Boolean)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlDataAdapter()
        Dim consulta As String = Nothing
        Try
            CON.Open()
            If Len(NOMBRE) = 0 Then
                NOMBRE = "''"
            ElseIf Len(NOMBRE) > 0 Then
                NOMBRE = "'" + NOMBRE + "'"
            End If
            If Len(CALLE) = 0 Then
                CALLE = "''"
            ElseIf Len(CALLE) > 0 Then
                CALLE = "'" + CALLE + "'"
            End If
            If Len(NUMERO) = 0 Then
                NUMERO = "''"
            ElseIf Len(NUMERO) > 0 Then
                NUMERO = "'" + NUMERO + "'"
            End If
            If Me.txtTelefono.Text.Length = 0 Then
                Me.txtTelefono.Text = "0"
            End If
            consulta = "Exec BUSCAORDSER 0," + CStr(Clv_Orden) + "," + CStr(Contrato) + "," + CStr(NOMBRE) + "," + CStr(CALLE) + "," + CStr(NUMERO) + "," + CStr(OP) + "," + CStr(procauto) + "," + Me.txtTelefono.Text.ToString
            'MsgBox(consulta, MsgBoxStyle.Information)
            CMD = New SqlDataAdapter(consulta, CON)

            Dim dt As New DataTable


            CMD.Fill(dt)


            Me.BUSCAORDSERBindingSource.DataSource = dt

            With DataGridView1
                .DataSource = Me.BUSCAORDSERBindingSource.DataSource
            End With

            'bnd = True


            'CMD = New SqlClient.SqlCommand()
            'With CMD
            '    .CommandText = "BUSCAORDSER"
            '    .Connection = CON
            '    .CommandType = CommandType.StoredProcedure
            '    .CommandTimeout = 0
            '    ByVal Clv_TipSer As Integer, ByVal Clv_Orden As Long, ByVal Contrato As Integer,
            '     ByVal NOMBRE As String, ByVal CALLE As String, ByVal NUMERO As String, ByVal OP As Integer, 
            '    ByVal procauto As Boolean)

            '    Dim PRM As New SqlParameter("@CLV_TIPSER", SqlDbType.Int)
            '    Dim PRM2 As New SqlParameter("@CLV_ORDEN", SqlDbType.BigInt)
            '    Dim PRM3 As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
            '    Dim PRM4 As New SqlParameter("@NOMBRE", SqlDbType.VarChar, 150)
            '    Dim PRM5 As New SqlParameter("@CALLE", SqlDbType.VarChar, 150)
            '    Dim PRM6 As New SqlParameter("@NUMERO", SqlDbType.VarChar, 50)
            '    Dim PRM7 As New SqlParameter("@OP", SqlDbType.Int)
            '    Dim PRM8 As New SqlParameter("@PROCAUTO", SqlDbType.Bit)

            '    PRM.Direction = ParameterDirection.Input
            '    PRM2.Direction = ParameterDirection.Input
            '    PRM3.Direction = ParameterDirection.Input
            '    PRM4.Direction = ParameterDirection.Input
            '    PRM5.Direction = ParameterDirection.Input
            '    PRM6.Direction = ParameterDirection.Input
            '    PRM7.Direction = ParameterDirection.Input
            '    PRM8.Direction = ParameterDirection.Input

            '    PRM.Value = Clv_TipSer
            '    PRM2.Value = Clv_Orden
            '    PRM3.Value = Contrato
            '    PRM4.Value = NOMBRE
            '    PRM5.Value = CALLE
            '    PRM6.Value = NUMERO
            '    PRM7.Value = OP
            '    PRM8.Value = procauto

            '    .Parameters.Add(PRM)
            '    .Parameters.Add(PRM2)
            '    .Parameters.Add(PRM3)
            '    .Parameters.Add(PRM4)
            '    .Parameters.Add(PRM5)
            '    .Parameters.Add(PRM6)
            '    .Parameters.Add(PRM7)
            '    .Parameters.Add(PRM8)

            '    Dim I As Integer = CMD.ExecuteNonQuery()
            '    Me.BUSCAORDSERTableAdapter.Fill(CMD.ExecuteNonQuery())
            '    Me.BUSCAORDSERBindingSource.DataSource = CMD
            'End With


            'With Me.DataGridView1
            '    .DataSource = BuscaBloqueadoBindingSource.DataSource
            'End With

            CON.Close()

            Me.TextBox1.Clear()
            Me.TextBox2.Clear()
            Me.TextBox3.Clear()
            Me.BNUMERO.Clear()
            Me.BCALLE.Clear()
            Me.txtTelefono.Text = ""

            bnd = True



        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try


    End Sub

    Private Sub busca(ByVal op As Integer)
        Dim sTATUS As String = "P"
        Dim autom As Boolean = False

        If Me.RadioButton1.Checked = True Then
            sTATUS = "P"
        ElseIf Me.RadioButton2.Checked = True Then
            sTATUS = "E"
        ElseIf Me.RadioButton3.Checked = True Then
            sTATUS = "V"
        End If
        If Me.CheckBox1.Checked = True Then
            autom = True
        Else
            autom = False
        End If

        Select Case op
            Case 0
                If Me.TextBox1.Text = "" Or Len(Me.TextBox1.Text) = 0 Then
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                Else
                    'busca1(0, 0, Me.TextBox1.Text, "", "", "", 30, autom)
                    BuscaSeparado(Me.cmbTipoServicio.SelectedValue, 0, CInt(Me.TextBox1.Text), "", "", "", "", "", "", "", "", 30, autom)
                End If
            Case 1

                If Len(Trim(Me.TextBox2.Text)) > 0 Or Len(Trim(Me.APaternoTextBox.Text)) > 0 Or Len(Trim(Me.AMaternoTextBox.Text)) > 0 Then
                    BuscaSeparado(Me.cmbTipoServicio.SelectedValue, 0, 0, Me.TextBox2.Text, Me.APaternoTextBox.Text, Me.AMaternoTextBox.Text, "", "", "", "", "", 31, autom)
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If
            Case 2
                BuscaSeparado(Me.cmbTipoServicio.SelectedValue, 0, 0, "", "", "", Me.BCALLE.Text, Me.BNUMERO.Text, "", "", "", 32, autom)
                'busca1(0, 0, 0, "", Me.BCALLE.Text, Me.BNUMERO.Text, 32, autom)
            Case 3
                If IsNumeric(Me.TextBox3.Text) = True Then
                    'busca1(0, Me.TextBox3.Text, 0, "", "", "", 33, autom)
                    BuscaSeparado(Me.cmbTipoServicio.SelectedValue, CInt(Me.TextBox3.Text), 0, "", "", "", "", "", "", "", "", 33, autom)
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If
            Case 199
                BuscaSeparado(Me.cmbTipoServicio.SelectedValue, 0, 0, sTATUS, "", "", "", "", "", "", "", 399, autom)
                'busca1(0, 0, 0, sTATUS, "", "", 399, autom)
            Case 4
                BuscaSeparado(Me.cmbTipoServicio.SelectedValue, 0, 0, "", "", "", "", "", "", "", "", 49, autom)
                'busca1(0, 0, 0, "", "", "", 49, autom)
            Case 29
                BuscaSeparado(Me.cmbTipoServicio.SelectedValue, 0, 0, "", "", "", "", "", "", "", Me.txtTelefono.Text, 29, autom)
            Case 5
                If Len(Trim(Me.TxtSetUpBox.Text)) > 0 Or Len(Trim(Me.TxtTarjeta.Text)) > 0 Then
                    BuscaSeparado(Me.cmbTipoServicio.SelectedValue, 0, 0, "", "", "", "", "", TxtSetUpBox.Text, TxtTarjeta.Text, "", 66, autom)
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If
        End Select
    End Sub


    'Private Sub Busca(ByVal op As Integer)
    '    Dim sTATUS As String = "P"
    '    Dim autom As Boolean = False
    '    Dim CON As New SqlConnection(MiConexion)

    '    Try
    '        If Me.RadioButton1.Checked = True Then
    '            sTATUS = "P"
    '        ElseIf Me.RadioButton2.Checked = True Then
    '            sTATUS = "E"
    '        ElseIf Me.RadioButton3.Checked = True Then
    '            sTATUS = "V"
    '        End If
    '        If Me.CheckBox1.Checked = True Then
    '            autom = True
    '        Else
    '            autom = False
    '        End If
    '        'If IsNumeric(Me.ComboBox4.SelectedValue) = True Then
    '        If op = 0 Then 'contrato
    '            If IsNumeric(Me.TextBox1.Text) = True Then
    '                CON.Open()
    '                Me.BUSCAORDSERTableAdapter.Connection = CON
    '                Me.BUSCAORDSERTableAdapter.Fill(Me.DataSetLidia2.BUSCAORDSER, New System.Nullable(Of Integer)(CType(Me.ComboBox4.SelectedValue, Integer)), 0, Me.TextBox1.Text, "", "", "", New System.Nullable(Of Integer)(CType(30, Integer)), autom)
    '                CON.Close()
    '            Else
    '                MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
    '            End If
    '        ElseIf op = 199 Then 'contrato
    '            CON.Open()
    '            Me.BUSCAORDSERTableAdapter.Connection = CON
    '            Me.BUSCAORDSERTableAdapter.Fill(Me.DataSetLidia2.BUSCAORDSER, CInt(Me.ComboBox4.SelectedValue), 0, 0, sTATUS, "", "", 399, autom)
    '            CON.Close()
    '        ElseIf op = 1 Then
    '            If Len(Trim(Me.TextBox2.Text)) > 0 Then
    '                CON.Open()
    '                Me.BUSCAORDSERTableAdapter.Connection = CON
    '                Me.BUSCAORDSERTableAdapter.Fill(Me.DataSetLidia2.BUSCAORDSER, New System.Nullable(Of Integer)(CType(Me.ComboBox4.SelectedValue, Integer)), 0, 0, Me.TextBox2.Text, "", "", New System.Nullable(Of Integer)(CType(31, Integer)), autom)
    '                CON.Close()
    '            Else
    '                MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
    '            End If
    '        ElseIf op = 2 And CInt(Me.ComboBox4.SelectedValue) <> 0 Then 'Calle y numero
    '            CON.Open()
    '            Me.BUSCAORDSERTableAdapter.Connection = CON
    '            Me.BUSCAORDSERTableAdapter.Fill(Me.DataSetLidia2.BUSCAORDSER, New System.Nullable(Of Integer)(CType(Me.ComboBox4.SelectedValue, Integer)), 0, 0, "", Me.BCALLE.Text, Me.BNUMERO.Text, New System.Nullable(Of Integer)(CType(32, Integer)), autom)
    '            CON.Close()
    '        ElseIf op = 3 Then 'clv_Orden
    '            If IsNumeric(Me.TextBox3.Text) = True Then
    '                CON.Open()
    '                Me.BUSCAORDSERTableAdapter.Connection = CON
    '                Me.BUSCAORDSERTableAdapter.Fill(Me.DataSetLidia2.BUSCAORDSER, New System.Nullable(Of Integer)(CType(Me.ComboBox4.SelectedValue, Integer)), Me.TextBox3.Text, 0, "", "", "", New System.Nullable(Of Integer)(CType(33, Integer)), autom)
    '                CON.Close()
    '            Else
    '                MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
    '            End If

    '        Else
    '            'MsgBox(Me.ComboBox4.SelectedValue)
    '            'MsgBox("0,0,"","","",4")
    '            'MsgBox(autom)

    '            CON.Open()
    '            Me.BUSCAORDSERTableAdapter.Connection = CON
    '            Me.BUSCAORDSERTableAdapter.Fill(Me.DataSetLidia2.BUSCAORDSER, Me.ComboBox4.SelectedValue, 0, 0, "", "", "", New System.Nullable(Of Integer)(CType(49, Integer)), autom)
    '            CON.Close()
    '        End If
    '        Me.TextBox1.Clear()
    '        Me.TextBox2.Clear()
    '        Me.TextBox3.Clear()
    '        Me.BNUMERO.Clear()
    '        Me.BCALLE.Clear()
    '        'Else
    '        '    MsgBox("Seleccione el Tipo de Servicio")
    '        'End If

    '        Exit Sub
    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '        'Resume Next
    '    End Try

    'End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        busca(0)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        busca(1)
        TextBox2.Text = ""
        APaternoTextBox.Text = ""
        AMaternoTextBox.Text = ""
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busca(0)
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busca(1)
        End If
    End Sub

    Private Sub BRWORDSER_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            'Me.ComboBox4.SelectedValue = GloClv_TipSer
            'Me.ComboBox4.Text = GloNom_TipSer
            'Me.ComboBox4.FindString(GloNom_TipSer)
            'Me.ComboBox4.Text = GloNom_TipSer
            busca(4)
        End If
        eActTecnico = True
    End Sub

    Private Sub BRWORDSER_CursorChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.CursorChanged

    End Sub

    Private Sub BRWORDSER_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        colorea(Me, Me.Name)
        If gloescr = 1 And GloTipoUsuario <> 3 And GloTipoUsuario <> 44 Then
            If GloTipoUsuario = 3 Then
                Me.Button2.Enabled = False
                Me.Button4.Enabled = True
            Else
                Me.Button2.Enabled = False
                Me.Button4.Enabled = False
            End If
        End If
        'PARA LLENAR EL COMBO DE LOS TIPOS DE SERVICIO (INICIO) <JUANJO>
        llenaCmbTipoServicio()
        'PARA LLENAR EL COMBO DE LOS TIPOS DE SERVICIO (FIN) <JUANJO>
        'busca(4)
        ''PARA LLENAR EL RESUMEN DE ORDENES (INICIO) --JUANJO
        'uspChecaCuantasOrdenesQuejas()
        ''PARA LLENAR EL RESUMEN DE ORDENES (FIN) --JUANJO
        bnd = True
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub



    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busca(3)
        End If
    End Sub
    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        busca(3)
    End Sub
    Private Sub Clv_calleLabel2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Clv_calleLabel2.TextChanged
        gloClave = Me.Clv_calleLabel2.Text
        Me.CREAARBOL()
    End Sub

    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        Me.Clv_calleLabel2.Text = Me.DataGridView1.SelectedCells(0).Value
        Me.ContratoLabel1.Text = Me.DataGridView1.SelectedCells(2).Value
        Me.CMBNombreTextBox.Text = Me.DataGridView1.SelectedCells(3).Value
        Me.CALLELabel1.Text = Me.DataGridView1.SelectedCells(4).Value
        Me.NUMEROLabel1.Text = Me.DataGridView1.SelectedCells(5).Value
    End Sub

    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If Button3.Enabled = True Then
            consultar()
        ElseIf Button4.Enabled = True Then
            modificar()
        End If
    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        busca(199)
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        busca(199)
    End Sub

    Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton3.CheckedChanged
        busca(199)
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If Me.RadioButton1.Checked = True Or Me.RadioButton2.Checked = True Or Me.RadioButton3.Checked = True Then
            busca(199)
        End If
    End Sub


    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        busca(2)
    End Sub


    Private Sub BNUMERO_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BNUMERO.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busca(2)
        End If
    End Sub


    Private Sub BCALLE_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BCALLE.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busca(2)
        End If
    End Sub


    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        'Me.Clv_calleLabel2.Text = Me.DataGridView1.SelectedCells(0).Value
        'Me.ContratoLabel1.Text = Me.DataGridView1.SelectedCells(2).Value
        'Me.CMBNombreTextBox.Text = Me.DataGridView1.SelectedCells(3).Value
        'Me.CALLELabel1.Text = Me.DataGridView1.SelectedCells(4).Value
        'Me.NUMEROLabel1.Text = Me.DataGridView1.SelectedCells(5).Value
    End Sub



    Private Sub DataGridView1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged
        Try
            If bnd = True Then
                If Len(CStr(Me.DataGridView1.SelectedCells(0).Value)) > 0 Then
                    Me.Clv_calleLabel2.Text = CStr(Me.DataGridView1.SelectedCells(0).Value)
                    Me.ContratoLabel1.Text = CStr(Me.DataGridView1.SelectedCells(2).Value)
                    Me.CMBNombreTextBox.Text = CStr(Me.DataGridView1.SelectedCells(3).Value)
                    Me.CALLELabel1.Text = CStr(Me.DataGridView1.SelectedCells(4).Value)
                    Me.NUMEROLabel1.Text = CStr(Me.DataGridView1.SelectedCells(5).Value)
                End If
            End If
        Catch
            Exit Sub
        End Try
    End Sub

    Private Sub DataGridView1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles DataGridView1.KeyPress

    End Sub

    Private Sub btnBtelefono_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBtelefono.Click
        Me.busca(29)
        txtTelefono.Text = ""
    End Sub

    Private Sub txtTelefono_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTelefono.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.busca(29)
        End If
    End Sub

    Private Sub BuscaSeparado(ByVal Clv_TipSer As Integer, ByVal Clv_Orden As Long, ByVal Contrato As Integer, ByVal NOMBRE As String, ByVal ApePaterno As String, _
                              ByVal ApeMaterno As String, ByVal CALLE As String, ByVal NUMERO As String, ByVal SetUpBox As String, ByVal Tarjeta As String, ByVal Telefono As String, ByVal OP As Integer, ByVal procauto As Boolean)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlDataAdapter()
        Dim consulta As String = Nothing
        Try
            CON.Open()
            If Len(NOMBRE) = 0 Then
                NOMBRE = "''"
            ElseIf Len(NOMBRE) > 0 Then
                NOMBRE = "'" + NOMBRE + "'"
            End If
            If Len(ApePaterno) = 0 Then
                ApePaterno = "''"
            ElseIf Len(ApePaterno) > 0 Then
                ApePaterno = "'" + ApePaterno + "'"
            End If
            If Len(ApeMaterno) = 0 Then
                ApeMaterno = "''"
            ElseIf Len(ApeMaterno) > 0 Then
                ApeMaterno = "'" + ApeMaterno + "'"
            End If
            If Len(CALLE) = 0 Then
                CALLE = "''"
            ElseIf Len(CALLE) > 0 Then
                CALLE = "'" + CALLE + "'"
            End If
            If Len(NUMERO) = 0 Then
                NUMERO = "''"
            ElseIf Len(NUMERO) > 0 Then
                NUMERO = "'" + NUMERO + "'"
            End If
            If Len(SetUpBox) = 0 Then
                SetUpBox = "''"
            ElseIf Len(SetUpBox) > 0 Then
                SetUpBox = "'" + SetUpBox + "'"
            End If
            If Len(Tarjeta) = 0 Then
                Tarjeta = "''"
            ElseIf Len(Tarjeta) > 0 Then
                Tarjeta = "'" + Tarjeta + "'"
            End If
            If Len(Telefono) = 0 Then
                Telefono = "''"
            ElseIf Len(Telefono) > 0 Then
                Telefono = "'" + Telefono + "'"
            End If
            consulta = "Exec BuscaOrdSerSeparado " + CStr(Clv_TipSer) + "," + CStr(Clv_Orden) + "," + CStr(Contrato) + "," + CStr(NOMBRE) + "," + CStr(ApePaterno) + "," + CStr(ApeMaterno) + "," + CStr(CALLE) + "," + CStr(NUMERO) + "," + CStr(SetUpBox) + "," + CStr(Tarjeta) + "," + CStr(Telefono) + "," + CStr(OP) + "," + CStr(procauto)
            'MsgBox(consulta, MsgBoxStyle.Information)
            CMD = New SqlDataAdapter(consulta, CON)
            Dim dt As New DataTable
            CMD.Fill(dt)

            Me.BUSCAORDSERBindingSource.DataSource = dt

            With DataGridView1
                .DataSource = Me.BUSCAORDSERBindingSource.DataSource
            End With

            CON.Close()

            Me.TextBox1.Clear()
            Me.TextBox2.Clear()
            Me.TextBox3.Clear()
            Me.BNUMERO.Clear()
            Me.BCALLE.Clear()
            Me.AMaternoTextBox.Clear()
            Me.APaternoTextBox.Clear()
            Me.TxtSetUpBox.Clear()
            Me.TxtTarjeta.Clear()
            bnd = True
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button14.Click
        Me.busca(5)
        TxtSetUpBox.Text = ""
        TxtTarjeta.Text = ""
    End Sub

    Public Sub borraOrdenes(ByVal prmClvOrden As Long, ByVal prmTipo As String)
        Dim claseOrdenes As New OrdenesDeServicioClass

        claseOrdenes.uspBorraQuejasOrdenes(prmClvOrden, prmTipo)
        If CBool(claseOrdenes.Diccionario("@bndOrdenQueja")) = True Then
            MsgBox("No se pueden eliminar Ordenes con status Ejecutada", MsgBoxStyle.Information)
            Exit Sub
        ElseIf CBool(claseOrdenes.Diccionario("@bndDescarga")) = True Then
            MsgBox("No se puede eliminar la Orden ya que tiene Descarga de Material Asignada ", MsgBoxStyle.Information)
            Exit Sub
        End If
    End Sub

    Private Sub btnEliminarOrdenes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminarOrdenes.Click
        If Me.DataGridView1.RowCount > 0 Then
            If IsNumeric(Me.DataGridView1.SelectedCells(0).Value) Then
                Dim Res = MsgBox("Realmente Desea Eliminar la Orden # " + CStr(Me.DataGridView1.SelectedCells(0).Value), MsgBoxStyle.YesNo)

                If Res = MsgBoxResult.Yes Then
                    borraOrdenes(Me.DataGridView1.SelectedCells(0).Value, "O")
                    busca(4)
                Else
                    Exit Sub
                End If
            Else
                MsgBox("Seleccione un Registro Válido", MsgBoxStyle.Information)
                Exit Sub
            End If
        Else
            MsgBox("No Hay Registros en la Lista", MsgBoxStyle.Information)
            Exit Sub
        End If
    End Sub

#Region "Total Ordenes <JUANJO>"
    Private Sub uspChecaCuantasOrdenesQuejas(ByVal prmClvTipSer As Integer)
        Dim Problema As New ClassClasificacionProblemas
        Dim DT As New DataTable

        Problema.OpAccion = 2
        Problema.clvTipSer = prmClvTipSer
        DT = Problema.uspChecaCuantasOrdenesQuejas()

        For Each fila As DataRow In DT.Rows
            Me.lblTotalPendientes.Text = fila("pendietnes").ToString()
            Me.lblTotalConVisita.Text = fila("visitas").ToString()
            Me.lblTotalEnProceso.Text = fila("proceso").ToString()
        Next
    End Sub
#End Region

#Region "Busca Cliente Bloqueado <JUANJO>"
    Private Sub BuscaBloqueado(ByVal prmContratoBloqueado As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@contrato", SqlDbType.Int, prmContratoBloqueado)
        BaseII.CreateMyParameter("@NUM", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@bloq", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("BuscaBloqueado")

        NUM = CInt(BaseII.dicoPar("@NUM").ToString())
        num2 = CInt(BaseII.dicoPar("@bloq").ToString())
    End Sub
#End Region

#Region "Llena Combo Tipos de Servicio <JUANJO>"
    Private Sub llenaCmbTipoServicio()
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("MuestraTipSerPrincipal2", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Try
            Dim DA As New SqlDataAdapter(CMD)
            Dim DT As New DataTable
            DA.Fill(DT)
            Me.cmbTipoServicio.DataSource = DT
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub
#End Region

    Private Sub DataGridView1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DataGridView1.KeyDown

        If e.KeyCode = 121 Then
            GloeClave = 0
            GloeClv_Orden = Me.Clv_calleLabel2.Text
            GloeClv_Trabajo = 0
            GloeTrabajo = ""
            GloeOp = 1
            FormMuestra_Que_Hize_Orden_Servicio.ShowDialog()
        End If
    End Sub

    Private Sub GroupBox1_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox1.Enter

    End Sub

    Private Sub cmbTipoServicio_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTipoServicio.SelectedIndexChanged
        Try
            If Me.cmbTipoServicio.Items.Count > 0 Then
                busca(4)
                'PARA LLENAR EL RESUMEN DE ORDENES (INICIO) --JUANJO
                uspChecaCuantasOrdenesQuejas(CInt(Me.cmbTipoServicio.SelectedValue))
                'PARA LLENAR EL RESUMEN DE ORDENES (FIN) --JUANJO
            End If
        Catch ex As Exception

        End Try
    End Sub

    'SAUL
    Private Sub uspAutorizadoQuejasOrdenes()
        'GloUsuario-bndOxxo
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Usuario", SqlDbType.VarChar, GloUsuario, 10)
        BaseII.CreateMyParameter("@bnd", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("uspAutorizadoQuejasOrdenes")
        bndAcceso = CInt(BaseII.dicoPar("@bnd").ToString)
    End Sub

    Private Sub uspOrdenesRetiro()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_orden", SqlDbType.BigInt, gloClave)
        BaseII.CreateMyParameter("@bnd", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("uspOrdenesRetiro")
        bndOrdRetir = CInt(BaseII.dicoPar("@bnd").ToString)
    End Sub
    '(Fin)
    Private Sub uspDimeTrabajo()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CInt(Me.ContratoLabel1.Text))
        BaseII.CreateMyParameter("@Orden", SqlDbType.Int, gloClave)
        BaseII.CreateMyParameter("@Desco", ParameterDirection.Output, SqlDbType.Int)
        'ByVal prmNombre As String, ByRef prmTipo As SqlDbType, ByRef prmTamanio As Integer, ByRef prmDireccion As ParameterDirection, ByRef prmValor As Object
        BaseII.ProcedimientoOutPut("uspDimeTrabajo")
        GloTrabajoR = CInt(BaseII.dicoPar("@Desco").ToString)
    End Sub

End Class
