﻿Public Class ClassBrwtblRelVendedoresSeries
#Region "Propiedades"

    Private _OP As Integer
    Public Property OP As Integer
        Get
            Return _OP
        End Get
        Set(ByVal value As Integer)
            _OP = value
        End Set
    End Property

    Private _IDDETALLE As Integer
    Public Property IDDETALLE As Integer
        Get
            Return _IDDETALLE
        End Get
        Set(ByVal value As Integer)
            _IDDETALLE = value
        End Set
    End Property


    Private _CLVVENDEDOR As Integer
    Public Property CLVVENDEDOR As Integer
        Get
            Return _CLVVENDEDOR
        End Get
        Set(ByVal value As Integer)
            _CLVVENDEDOR = value
        End Set
    End Property

    Private _ULTIMOFOLIOUSADO As Integer
    Public Property ULTIMOFOLIOUSADO As Integer
        Get
            Return _ULTIMOFOLIOUSADO
        End Get
        Set(ByVal value As Integer)
            _ULTIMOFOLIOUSADO = value
        End Set
    End Property

    Private _FOLIOUSADOS As Integer
    Public Property FOLIOUSADOS As Integer
        Get
            Return _FOLIOUSADOS
        End Get
        Set(ByVal value As Integer)
            _FOLIOUSADOS = value
        End Set
    End Property

    Private _SERIE As String
    Public Property SERIE As String
        Get
            Return _SERIE
        End Get
        Set(ByVal value As String)
            _SERIE = value
        End Set
    End Property

    Private _CLVCOMPANY As Integer
    Public Property CLVCOMPANY As Integer
        Get
            Return _CLVCOMPANY
        End Get
        Set(ByVal value As Integer)
            _CLVCOMPANY = value
        End Set
    End Property

    Private _ID As Integer
    Public Property ID As Integer
        Get
            Return _ID
        End Get
        Set(ByVal value As Integer)
            _ID = value
        End Set
    End Property

    Private _NOMBRE As String
    Public Property NOMBRE As String
        Get
            Return _NOMBRE
        End Get
        Set(ByVal value As String)
            _NOMBRE = value
        End Set
    End Property
#End Region

#Region "Constructor"
    Public Sub New()
        _OP = 0
        _ID = 0
        _NOMBRE = ""
        _CLVCOMPANY = 0
        _FOLIOUSADOS = 0
        _ULTIMOFOLIOUSADO = 0
        _SERIE = ""
        _IDDETALLE = 0
    End Sub
#End Region

#Region "Constructor"
    Dim folio As New BaseIII

    Public Function CONtblRelVendedoresSeries() As DataTable
        Try
            folio.limpiaParametros()
            folio.CreateMyParameter("@OP", SqlDbType.Int, _OP)
            folio.CreateMyParameter("@ID", SqlDbType.Int, _ID)
            folio.CreateMyParameter("@NOMBRE", SqlDbType.VarChar, _NOMBRE, 250)
            CONtblRelVendedoresSeries = folio.ConsultaDT("CONtblRelVendedoresSeries")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function CONtblDetRelVendedoresSeries() As DataTable
        Try
            folio.limpiaParametros()
            folio.CreateMyParameter("@ID", SqlDbType.Int, _ID)
            CONtblDetRelVendedoresSeries = folio.ConsultaDT("CONtblDetRelVendedoresSeries")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function MUESTRAtblCompanias() As DataTable
        Try
            folio.limpiaParametros()
            folio.CreateMyParameter("@OP", SqlDbType.Int, _OP)
            folio.CreateMyParameter("@clvCompania", SqlDbType.Int, _CLVCOMPANY)
            MUESTRAtblCompanias = folio.ConsultaDT("MUESTRAtblCompanias")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function NUEtblDetRelVendedoresSeries() As DataTable
        Try
            folio.limpiaParametros()
            folio.CreateMyParameter("@ID", SqlDbType.Int, _ID)
            folio.CreateMyParameter("@CLVCOMPANIA", SqlDbType.Int, _CLVCOMPANY)
            folio.CreateMyParameter("@FOLIOSUSADOS", SqlDbType.Int, _FOLIOUSADOS)
            folio.CreateMyParameter("@SERIE", SqlDbType.VarChar, _SERIE, 250)
            folio.CreateMyParameter("@ULTIMOFOLIOUSADO", SqlDbType.Int, _ULTIMOFOLIOUSADO)
            NUEtblDetRelVendedoresSeries = folio.ConsultaDT("NUEtblDetRelVendedoresSeries")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function MODtblDetRelVendedoresSeries() As DataTable
        Try
            folio.limpiaParametros()
            folio.CreateMyParameter("@IDDETALLE", SqlDbType.Int, _IDDETALLE)
            folio.CreateMyParameter("@ID", SqlDbType.Int, _ID)
            folio.CreateMyParameter("@CLVCOMPANIA", SqlDbType.Int, _CLVCOMPANY)
            folio.CreateMyParameter("@FOLIOSUSADOS", SqlDbType.Int, _FOLIOUSADOS)
            folio.CreateMyParameter("@SERIE", SqlDbType.VarChar, _SERIE, 250)
            folio.CreateMyParameter("@ULTIMOFOLIOUSADO", SqlDbType.Int, _ULTIMOFOLIOUSADO)
            MODtblDetRelVendedoresSeries = folio.ConsultaDT("MODtblDetRelVendedoresSeries")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub BORtblDetRelVendedoresSeries()
        Try
            folio.limpiaParametros()
            folio.CreateMyParameter("@IDDETALLE", SqlDbType.Int, _IDDETALLE)
            folio.Inserta(" BORtblDetRelVendedoresSeries")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub NUEtblRelVendedoresSeries()
        Try
            folio.limpiaParametros()
            folio.CreateMyParameter("@CLV_VENDEDOR", SqlDbType.Int, _CLVVENDEDOR)
            folio.CreateMyParameter("@ID", ParameterDirection.Output, SqlDbType.Int)
            folio.ProcedimientoOutPut("NUEtblRelVendedoresSeries")
            _ID = CInt(folio.dicoPar("@ID").ToString())
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

End Class
