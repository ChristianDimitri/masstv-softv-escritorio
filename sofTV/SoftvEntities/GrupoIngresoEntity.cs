using System;

namespace Softv.Entities
{
  /// <summary>
  /// Class                   : Softv.Entities.GrupoIngresoEntity.cs
  /// Generated by            : Desarroll@, Class Generator (c) 2009
  /// Description             : GrupoIngreso entity
  /// File                    : GrupoIngresoEntity.cs
  /// Creation date           : 02/08/2011
  /// Creation time           : 06:53:12 p.m.
  /// </summary>
  public class GrupoIngresoEntity
  {
  
      #region attributes
  
      private int? _IdGrupoIngresos;
      /// <summary>
      /// IdGrupoIngresos
      /// </summary>
      public int? IdGrupoIngresos
      {
          get { return _IdGrupoIngresos; }
          set { _IdGrupoIngresos = value; }
      }
  
      private String _Concepto;
      /// <summary>
      /// Concepto
      /// </summary>
      public String Concepto
      {
          get { return _Concepto; }
          set { _Concepto = value; }
      }
  
      private int? _Pos;
      /// <summary>
      /// Pos
      /// </summary>
      public int? Pos
      {
          get { return _Pos; }
          set { _Pos = value; }
      }

      #endregion

      #region Constructors
      /// <summary>
      /// Default constructor
      /// </summary>

      public GrupoIngresoEntity()
      {
      }

      /// <summary>
      ///  Parameterized constructor
      /// </summary>
      /// <param name="IdGrupoIngresos"></param>
      /// <param name="Concepto"></param>
      /// <param name="Pos"></param>
      public GrupoIngresoEntity(int?  IdGrupoIngresos,String Concepto,int?  Pos)
      {
          this._IdGrupoIngresos = IdGrupoIngresos;
          this._Concepto = Concepto;
          this._Pos = Pos;
      }
      #endregion

  }
}
