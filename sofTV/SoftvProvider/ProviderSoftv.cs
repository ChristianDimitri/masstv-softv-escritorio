﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Softv.Providers
{
    public class ProviderSoftv
    {
        public static ProductividadParaTecnicoProvider ProductividadParaTecnico
        {
            get { return ProductividadParaTecnicoProvider.Instance; }
        }

        public static RangoLimiteDeCreditoProvider RangoLimiteDeCredito
        {
            get { return RangoLimiteDeCreditoProvider.Instance; }
        }

        public static LimiteDeCreditoProvider LimiteDeCredito
        {
            get { return LimiteDeCreditoProvider.Instance; }
        }

        public static DetLimiteDeCreditoProvider DetLimiteDeCredito
        {
            get { return DetLimiteDeCreditoProvider.Instance; }
        }

        public static ColoniaProvider Colonia
        {
            get { return ColoniaProvider.Instance; }
        }

        public static CiudadProvider Ciudad
        {
            get { return CiudadProvider.Instance; }
        }

        public static ServicioProvider Servicio
        {
            get { return ServicioProvider.Instance; }
        }

        public static MesaControlUsuarioProvider MesaControlUsuario
        {
            get { return MesaControlUsuarioProvider.Instance; }
        }

        public static CvecolciuProvider Cvecolciu
        {
            get { return CvecolciuProvider.Instance; }
        }

        public static RelColoniasSerProvider RelColoniasSer
        {
            get { return RelColoniasSerProvider.Instance; }
        }

        public static MotAtenTelProvider MotAtenTel
        {
            get { return MotAtenTelProvider.Instance; }
        }
        
        public static AtenTelProvider AtenTel
        {
            get { return AtenTelProvider.Instance; }
        }

        public static DescargaMaterialProvider DescargaMaterial
        {
            get { return DescargaMaterialProvider.Instance; }
        }

        public static IncentivoPorContratoProvider IncentivoPorContrato
        {
            get { return IncentivoPorContratoProvider.Instance; }
        }

        public static MetaIncentivoProvider MetaIncentivo
        {
            get { return MetaIncentivoProvider.Instance; }
        }

        public static AtencionDeLlamadasProvider AtencionDeLlamadas
        {
            get { return AtencionDeLlamadasProvider.Instance; }
        }

        public static DatosComplementariosContratoProvider DatosComplementariosContrato
        {
            get { return DatosComplementariosContratoProvider.Instance; }
        }

        public static RelArticuloAccesorioProvider RelArticuloAccesorio
        {
            get { return RelArticuloAccesorioProvider.Instance; }
        }
        public static RelTipoCajaAccesorioInstalacionProvider RelTipoCajaAccesorioInstalacion
        {
            get { return RelTipoCajaAccesorioInstalacionProvider.Instance; }
        }
        public static PrioridadQuejaProvider PrioridadQueja
        {
            get { return PrioridadQuejaProvider.Instance; }
        }
        public static TrabajoProvider Trabajo
        {
            get { return TrabajoProvider.Instance; }
        }
        public static RelMaterialTrabajoProvider RelMaterialTrabajo
        {
            get { return RelMaterialTrabajoProvider.Instance; }
        }
        public static GrupoConceptoIngresoProvider GrupoConceptoIngreso
        {
            get { return GrupoConceptoIngresoProvider.Instance; }
        }
        public static GrupoIngresoProvider GrupoIngreso
        {
            get { return GrupoIngresoProvider.Instance; }
        }
    }
}
